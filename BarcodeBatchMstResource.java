package com.maple.restserver.resource;

import java.util.HashMap;

/**
 *@GetMapping("{companymstid}/acceptstocks")
 * 
 *@PostMapping("{companymstid}/acceptstock")
 *@Valid @RequestBody AcceptStock acceptStock
 * 
 */

import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.maple.restserver.entity.AcceptStock;
import com.maple.restserver.entity.BarcodeBatchMst;
import com.maple.restserver.entity.CompanyMst;
import com.maple.restserver.entity.LmsQueueMst;
import com.maple.restserver.exception.ResourceNotFoundException;
import com.maple.restserver.repository.AcceptStockRepository;
import com.maple.restserver.repository.BarcodeBatchMstRepository;
import com.maple.restserver.repository.CompanyMstRepository;
import com.maple.restserver.repository.ItemBatchDtlRepository;
import com.maple.restserver.repository.LmsQueueMstRepository;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumber;
import com.maple.restserver.resource.vouchernumber.service.VoucherNumberService;
import com.maple.restserver.utils.EventBusFactory;
import com.maple.restserver.utils.SystemSetting;

import sun.util.calendar.BaseCalendar.Date;

@RestController
@Transactional
public class BarcodeBatchMstResource {
	@Autowired
	private BarcodeBatchMstRepository barcodeBatchMstRepository;

	@Autowired
	CompanyMstRepository companyMstRepo;

	@Autowired
	ItemBatchDtlRepository itemBatchDtlRepository;
	
	@Autowired
	private VoucherNumberService voucherService;

	
	@Autowired
	LmsQueueMstRepository lmsQueueMstRepository;

	
	EventBus eventBus = EventBusFactory.getEventBus();

	@PostMapping("{companymstid}/barcodebatchmstresource/barcodebatchmst")
	public BarcodeBatchMst createBarcodeBatchMst(@PathVariable(value = "companymstid") String companymstid,
			@Valid @RequestBody BarcodeBatchMst barcodeBatchMstReq) {

		Optional<CompanyMst> companyMstOpt =  companyMstRepo.findById(companymstid);
		
		if(!companyMstOpt.isPresent()) {
			return null;
		}
		
		CompanyMst companyMst = companyMstOpt.get();
			
			String date = SystemSetting.getFinancialYear();
			

			String year = date.charAt(2)+"";
			year = year + date.charAt(3)+"";
			
			VoucherNumber voucherNo = voucherService.generateInvoice(
					year,companymstid);

			String vcNo = voucherNo.getCode();

			barcodeBatchMstReq.setId(vcNo);
			barcodeBatchMstReq.setCompanyMst(companyMst);
			
			barcodeBatchMstReq = barcodeBatchMstRepository.saveAndFlush(barcodeBatchMstReq);
			
			
			LmsQueueMst lmsQueueMst = new LmsQueueMst();

			lmsQueueMst.setCompanyMst(companyMst);
			// lmsQueueMst.setVoucherDate((java.util.Date) variables.get("voucherDate"));

			java.util.Date uDate = SystemSetting.getSystemDate();
			java.sql.Date sqlVDate = SystemSetting.UtilDateToSQLDate(uDate);
			lmsQueueMst.setVoucherDate(sqlVDate);

			lmsQueueMst.setVoucherNumber(barcodeBatchMstReq.getId());
			lmsQueueMst.setVoucherType("BarcodeBatchMst");
			lmsQueueMst.setPostedToServer("NO");
			lmsQueueMst.setJobClass("BarcodeBatchMst");
			lmsQueueMst.setCronJob(true);
			lmsQueueMst.setJobName("BarcodeBatchMst");
			lmsQueueMst.setJobGroup("BarcodeBatchMst");
			lmsQueueMst.setRepeatTime(60000L);
			lmsQueueMst.setSourceObjectId(barcodeBatchMstReq.getId());

			lmsQueueMst.setBranchCode(barcodeBatchMstReq.getBranchCode());

			lmsQueueMst = lmsQueueMstRepository.saveAndFlush(lmsQueueMst);

			
			
			
			Map<String, Object> variables = new HashMap<String, Object>();

			variables.put("companyid", companyMst);

			variables.put("voucherNumber",barcodeBatchMstReq.getId());
			variables.put("voucherDate", SystemSetting.getSystemDate());
			variables.put("id", barcodeBatchMstReq.getId());

			variables.put("branchcode", barcodeBatchMstReq.getBranchCode());
			variables.put("REST", 1);
			 

			// variables.put("voucherDate", purchase.getVoucherDate());
			// variables.put("id",purchase.getId());
			variables.put("inet", 0);

			variables.put("WF", "BarcodeBatchMst");

			String workflow = (String) variables.get("WF");
			String voucherNumber = (String) variables.get("voucherNumber");
			String sourceID = (String) variables.get("id");
			
			variables.put("lmsqid", lmsQueueMst.getId());

			eventBus.post(variables);
			
			
			

			return barcodeBatchMstReq;

	}
	
	@GetMapping("{companymstid}/barcodebatchmstresource/getbarcodebatchmstbyid/{id}")
	public Optional<BarcodeBatchMst> retrieveBarcodeBatchMstById(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "id") String id) {
		return barcodeBatchMstRepository.findById(id);
	}
	
	@GetMapping("{companymstid}/barcodebatchmstresource/getbarcodebatchmstbybarcodeandbatch/{barcode}/{batch}/{branchcode}")
	public Optional<BarcodeBatchMst> retrieveBarcodeBatchMstByBarcodeAndBatch(
			@PathVariable(value = "companymstid") String companymstid,
			@PathVariable(value = "barcode") String barcode,
			@PathVariable(value = "branchcode") String branchcode,
			@PathVariable(value = "batch") String batch) {
		return barcodeBatchMstRepository.findByBarcodeAndBatchCodeAndBranchCode(barcode,batch,branchcode);
	}

}
