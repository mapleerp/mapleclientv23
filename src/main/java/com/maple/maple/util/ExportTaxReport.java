package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.TaxReport;

import javafx.application.HostServices;

public class ExportTaxReport {


	public void exportToExcel( String FileName, List<TaxReport> salesTaxReportList,List<TaxReport> purchaseTaxReportList )  {


		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 1;
		int cellnum = 0;
		 Row row1 = sheet.createRow(0);
		 Cell cell1 = row1.createCell(4);
		 cell1.setCellValue("Sales Tax Report");
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Tax Rate");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Sgst Amount");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Cgst Amount");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Igst Amount");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Cess Amount");
			cell = row.createCell(cellnum++);

			cell.setCellValue("TotalTax Amount");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Assesable Amnt");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Net Amount");
			cell = row.createCell(cellnum++);

			
		
		  int rownumPurchase =9;
		  int cellnumPurchase = 0;
		
		  Row row2 =sheet.createRow(8); 
		  Cell cell2 = row2.createCell(4);
		  cell2.setCellValue("Purchase Tax Report");
		 
		  Row rowP =
		            sheet.createRow(rownumPurchase++); 
		  Cell cellP =
		  rowP.createCell(cellnumPurchase++);
		  
		  cellP.setCellValue("Tax Rate");
		  
		  cellP = rowP.createCell(cellnumPurchase++);
		  cellP.setCellValue("Sgst Amount");
		  cellP =
		  rowP.createCell(cellnumPurchase++);
		  cellP.setCellValue("Cgst Amount");
		  cellP
		  = rowP.createCell(cellnumPurchase++);
		  
		  cellP.setCellValue("Igst Amount"); 
		  cellP =
		  rowP.createCell(cellnumPurchase++);
		  
		  cellP.setCellValue("Cess Amount"); 
		  cellP =
		  rowP.createCell(cellnumPurchase++);
		  
		  cellP.setCellValue("TotalTax Amount");
		  cellP =
		  rowP.createCell(cellnumPurchase++);
		  
		  cellP.setCellValue("Assesable Amnt");
		  cellP =
		  rowP.createCell(cellnumPurchase++);
		  
		  cellP.setCellValue("Net Amount"); 
		  cellP = rowP.createCell(cellnumPurchase++);
		  
		 
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		for(int i=0;i<salesTaxReportList.size();i++)

                             {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
					
					 Object salesTaxRate =  salesTaxReportList.get(i).getSalesTaxRate();;
			Object salesSgstAmount=salesTaxReportList.get(i).getSalesSgstAmount();
Object salesCgstAmount=salesTaxReportList.get(i).getSalesCgstAmount();
Object salesIgstAmount=salesTaxReportList.get(i).getSalesIgstAmount();
Object salesCessAmount=salesTaxReportList.get(i).getSalesCessAmount();
Object salesTotalTaxAmount=salesTaxReportList.get(i).getTotalSaleTaxAmount();
Object salesAssesableAmount=salesTaxReportList.get(i).getSalesTaxAssesableAmount();					
Object salesNetAmount=salesTaxReportList.get(i).getNetSalesAmount();

cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)salesTaxRate);
					

cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)salesSgstAmount);
					 

cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)salesCgstAmount);
					

cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)salesIgstAmount);
					
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)salesCessAmount);

					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)salesTotalTaxAmount);
					 
					 
					 
					 
					 
					 
					 
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)salesAssesableAmount);
					 
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)salesNetAmount);
					
					

					
				 
				}
		

		for(int j=0;j<purchaseTaxReportList.size();j++)

        {

row = sheet.createRow(rownumPurchase++);
cellnum = 0;

 Object purchaseTaxRate =  purchaseTaxReportList.get(j).getPurchaseTaxRate();;
Object purchaseCgstAmount=purchaseTaxReportList.get(j).getPurchaseCgstAmount();

Object purchaseSgstAmount=purchaseTaxReportList.get(j).getPurchaseSgstAmount();
Object purchaseIgstAmount=purchaseTaxReportList.get(j).getPurchaseIgstAmount();
Object purchaseCessAmount=purchaseTaxReportList.get(j).getPurchaseCessAmount();
Object purchaseTotalTaxAmount=purchaseTaxReportList.get(j).getTotalPurchaseTaxAmount();					
Object purchaseAssesableAmount=purchaseTaxReportList.get(j).getPurchaseTaxAssesableAmount();
Object purchaseNetAmount=purchaseTaxReportList.get(j).getNetPurchaseAmount();

cell = row.createCell(cellnum++);
 cell.setCellValue((Double)purchaseTaxRate);


  cell = row.createCell(cellnum++);
 cell.setCellValue((Double)purchaseSgstAmount);
 

 cell = row.createCell(cellnum++);
 cell.setCellValue((Double)purchaseCgstAmount);


 cell = row.createCell(cellnum++);
 if(null==purchaseIgstAmount) {
	 cell.setCellValue(0.0);
 }else {
 cell.setCellValue((Double)purchaseIgstAmount);
 }
 cell = row.createCell(cellnum++);
 cell.setCellValue((Double)purchaseCessAmount);

 cell = row.createCell(cellnum++);
 cell.setCellValue((Double)purchaseTotalTaxAmount);
 
 
 cell = row.createCell(cellnum++);
 cell.setCellValue((Double)purchaseAssesableAmount);
 
 cell = row.createCell(cellnum++);
 cell.setCellValue((Double)purchaseNetAmount);



        }

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	}
	 
}
