package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.B2bSalesReport;

import javafx.application.HostServices;

public class ExportB2bSalesDtlToExcel {


	
	public void exportToExcel( String FileName, List<B2bSalesReport> aHM)  {


		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("VOUCHER NUMBER");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("CUSTOMER");
			cell = row.createCell(cellnum++);
			cell.setCellValue("TIN NO");
			cell = row.createCell(cellnum++);

			cell.setCellValue("INVOICE DATE");
			cell = row.createCell(cellnum++);

			cell.setCellValue("INVOICE AMOUNT");
			cell = row.createCell(cellnum++);

			cell.setCellValue("TAX RATE");
			cell = row.createCell(cellnum++);

			cell.setCellValue("TAXABLE VALUE");
			cell = row.createCell(cellnum++);


		/*
		 * cell.setCellValue("TOTAL"); cell = row.createCell(cellnum++);
		 */
		
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
			
			for(int count=0; count<aHM.size(); count++)
				 {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
				
					 Object customer = aHM.get(count).getCustomerName();
					 Object tinno = aHM.get(count).getCustomerGst();
					 Object invoiceamount = aHM.get(count).getNetTotal();
					 
					 Object invoiceno = aHM.get(count).getVoucherNo();
					 Object vocherdate = aHM.get(count).getSvoucherDate();
					 Object taxrate = aHM.get(count).getTaxRate();
					 Object taxablevalue = aHM.get(count).getTaxableValue();
					

					 
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)invoiceno);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)customer);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)tinno);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)vocherdate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)invoiceamount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)taxrate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)taxablevalue);
				/*
				 * cell = row.createCell(cellnum++); cell.setCellValue((Double)total);
				 */
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	try {
		workbook.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	}
	 

}
