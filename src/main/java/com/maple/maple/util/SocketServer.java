package com.maple.maple.util;
// A Java program for a Server 
import java.net.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;



 

import java.io.*; 

public class SocketServer extends Thread
{ 
	//initialize socket and input stream 
	private Socket		 socket = null; 
	private ServerSocket server = null; 
	private DataInputStream in	 = null; 
	private int PORT ;

	
	
	
	    private static Socket s;
	    
	    private static InputStreamReader isr;
	    private static BufferedReader br;
	    private static PrintWriter pr;

	    
	    
 
public SocketServer(int port) {
	this.PORT = port;
	}


 

	
	public void run() {

		
		
		ServerRun( PORT) ;

	}
	
	
	// constructor with port 
	public void ServerRun(int port) 
	{

		try {

			// starts server and waits for a connection

			server = new ServerSocket(port);
			System.out.println("Server started" + PORT);

			System.out.println("Waiting for a client ..." + PORT);

			while (true) {

				socket = server.accept();

				try {

					System.out.println("Client accepted" + PORT);

					// takes input from the client socket
					in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

					PrintStream PS = new PrintStream(socket.getOutputStream()); // printstream prints data to socket

					String line = "";

					// reads message from client until "Over" is sent

					while (!line.equals("Over")) {
						try {
							line = in.readLine();

							if (null == line) {

								// socket.close();
								// in.close();
								break;
							}
							System.out.println(line);

							try {
								
								switch (PORT) {
									case 5000: {
										processMessage(line);
										break;
									}
									
									
								}
								
								 
								PS.println("DONE");
								PS.flush();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} catch (IOException i) {
							System.out.println(i);
						}

					}

					System.out.println("Closing connection");

					// close connection

					in.close();

					socket.close();

				} catch (Exception i) {
					System.out.println(i);
				}

			}

		} catch (Exception i) {
			System.out.println(i);
		}

	} 
	
	
private void processMessage(String line) {
	
}



} 
