package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.report.entity.HsnCodeSaleReport;
import com.maple.report.entity.TallyImportReport;

import javafx.application.HostServices;

public class ExportJournalReport {


	
	public void exportToExcel( String FileName, List<JournalDtl>journalDtlReportList,String fdate,String tdate )  {

	 

		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		
		
		String ColList = "";
		int rownum = 1;
		int cellnum = 0;
		 Row row0 = sheet.createRow(0);
		 Cell cell0 = row0.createCell(cellnum++);
		 cell0.setCellValue("From Date");
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Account Name");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Debit amount");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Credit Amount");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Remark");
		
			
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
			for(int count=0; count<journalDtlReportList.size(); count++)
			{
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
			
				
					 Object accountHead = journalDtlReportList.get(count).getAccountHead();
					 Object debitAmount = journalDtlReportList.get(count).getDebitAmount();
					 Object creditAmount = journalDtlReportList.get(count).getCreditAmount();
					 
					 Object remarks = journalDtlReportList.get(count).getRemarks();
					 
					 row0.createCell(cellnum++);
					 cell0.setCellValue((String)fdate);
					 
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)accountHead);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)debitAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)creditAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)remarks);
					
					 
					 
					
					
					
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	}
	

}
