package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;

import javafx.application.HostServices;

public class ExportLocalCustomerToExcel {

	


	
	public void exportToExcel( String FileName, List<LocalCustomerMst> aHM )  {


		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Customer Name");
			
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Phone Number");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Address");
			cell=row.createCell(cellnum++);
			cell.setCellValue("Address1");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Date Of Birth");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Wedding Date");
			cell = row.createCell(cellnum++);

			// Iterate aHM
		try {
		
			for(int count=0; count< aHM.size(); count++)
			{
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
			
				
					 Object address = aHM.get(count).getAddress();
					 Object address1 = aHM.get(count).getAddressLine1();
					 Object customername = aHM.get(count).getLocalcustomerName();
					 Object phonenumber = aHM.get(count).getPhoneNo();
					 Object dateofBirth = aHM.get(count).getDateOfBirth();
					 Object weddingDate = aHM.get(count).getWeddingDate();
					 String sdateofBirth = null,sweddingDate = null;
					 if(null != dateofBirth)
					 {
					 sdateofBirth = SystemSetting.UtilDateToString((Date)dateofBirth, "yyyy-MM-dd");
					 }
					 if(null != weddingDate)
					 {
					  sweddingDate = SystemSetting.UtilDateToString((Date)weddingDate, "yyyy-MM-dd");
					 }
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)customername);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)phonenumber);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)address);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)address1);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)sdateofBirth);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)sweddingDate);
					

					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	}
	

}
