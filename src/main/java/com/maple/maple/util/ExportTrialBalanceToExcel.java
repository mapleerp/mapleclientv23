package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.AccountBalanceReport;

import javafx.application.HostServices;

public class ExportTrialBalanceToExcel {

	public void exportToExcel(String FileName, ArrayList items, String vDate, String totalCredit) {

		HSSFWorkbook workbook = new HSSFWorkbook();

		HSSFSheet sheet = workbook.createSheet("Ssql Result");

		Map<String, Object[]> data = new HashMap<String, Object[]>();

		String userBranch = SystemSetting.getUser().getBranchCode();
		String ColList = "";
		int rownum = 1;
		int cellnum = 0;

		Row row0 = sheet.createRow(rownum++);
		Cell cell0 = row0.createCell(2);
		cell0.setCellValue(userBranch);
		Row row = sheet.createRow(rownum++);
		Cell cell = row.createCell(cellnum++);

		cell.setCellValue("Date");

		cell = row.createCell(cellnum++);
		cell.setCellValue("Account");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DebitAmount");
		cell = row.createCell(cellnum++);

		cell.setCellValue("CreditAmount");
		cell = row.createCell(cellnum++);

		// Iterate aHM
		try {

//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}

			String account = "";
			Double debitAmount = 0.0;
			Double creditAmount = 0.0;
			Double amount = 0.0;
			Double totalDebitmount = 0.0;
			Double totalCreditAmount = 0.0;
			Iterator itr = items.iterator();
			while (itr.hasNext()) {

				List element = (List) itr.next();
				account = (String) element.get(0);
				debitAmount = (Double) element.get(2);
				totalDebitmount += debitAmount;

				creditAmount = (Double) element.get(1);
				totalCreditAmount = creditAmount;
				if (debitAmount == 0 && creditAmount == 0) {
					continue;
				}
				row = sheet.createRow(rownum++);
				cellnum = 0;

				// amount = (Double) element.get(3);
				if (null != account) {

					AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
					accountBalanceReport.setAccountHeads(account);
					accountBalanceReport.setDebit(debitAmount);
					accountBalanceReport.setCredit(creditAmount);

					cell = row.createCell(cellnum++);
					cell.setCellValue((String) vDate);
					cell = row.createCell(cellnum++);
					cell.setCellValue((String) account);
					cell = row.createCell(cellnum++);
					cell.setCellValue((Double) debitAmount);
					cell = row.createCell(cellnum++);
					/*
					 * cell.setCellValue((Double)totalDebitmount); cell = row.createCell(cellnum++);
					 */
					cell.setCellValue((Double) creditAmount);
					/*
					 * cell = row.createCell(cellnum++);
					 * cell.setCellValue((Double)totalCreditAmount);
					 */

				}
			}

			Row row2 = sheet.createRow(rownum++);
			Cell cell2 = row2.createCell(2);
			cell2.setCellValue(totalDebitmount);
			cell2 = row2.createCell(3);
			cell2.setCellValue(totalCredit);

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			FileOutputStream out = new FileOutputStream(new File(FileName));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(FileName);

			// Desktop desktop = Desktop.getDesktop();
			// File file = new File(FileName);
			// desktop.open(file);

		} catch (FileNotFoundException e1) {
			System.out.println(e1.toString());
		} catch (IOException e2) {
			System.out.println(e2.toString());
		}

	}
}
