package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.report.entity.StockReport;
import com.maple.report.entity.StockSummaryDtlReport;
import com.maple.report.entity.TallyImportReport;

import javafx.application.HostServices;

public class ExportItemWiseStockMovmntReportToExcel {



	
	public void exportToExcel( String FileName, List<StockSummaryDtlReport>stockReportList)  {
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
	
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		
		  Row row0 = sheet.createRow(rownum++); 
		  Cell cell0 =row0.createCell(2);
		  cell0.setCellValue("ItemName");
		  Object itemName =  stockReportList.get(0).getItemName();
		  Cell cell1 =row0.createCell(3);
		  cell1.setCellValue((String)itemName);
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			cell.setCellValue("Date");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Particulars");
			cell = row.createCell(cellnum++);
			cell.setCellValue("InwardQty");
			cell = row.createCell(cellnum++);


			cell.setCellValue("Inward Value");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Outward Qty");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Outward Value");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Closing Qty");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Closing Value");
			cell = row.createCell(cellnum++);
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
			
			for(int count=0; count< stockReportList.size(); count++)
			{
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
			
				
					 Object rdate = stockReportList.get(count).getrDate();
					 String srdate = SystemSetting.UtilDateToString((Date)rdate, "yyyy-MM-dd");
					 Object particulars = stockReportList.get(count).getParticulars();
					 Object inwardQty = stockReportList.get(count).getInwardQty();
					 
					 Object inwardValue = stockReportList.get(count).getInwardValue();
					 Object outwardQty=stockReportList.get(count).getOutwardQty();
					 Object outWardValue=stockReportList.get(count).getOutwardValue();
					 Object closingQty=stockReportList.get(count).getClosingQty();
					 Object closingValue=stockReportList.get(count).getClosingValue();
					
					 
				
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)srdate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)particulars);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)inwardQty);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)inwardValue);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)outwardQty);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)outWardValue);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)closingQty);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)closingValue);
				
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	}}
	

	

