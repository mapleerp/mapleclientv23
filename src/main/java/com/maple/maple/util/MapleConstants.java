package com.maple.maple.util;

public class MapleConstants {
	public static final String CUSTOMER = "CUSTOMER";
	public static final String SUPPLIER = "SUPPLIER";
	public static final String BOTH = "BOTH";
	public static final String  ONBASISOFBASEPRICE= "ON BASIS OF BASE PRICE";
	public static final String  ONBASISOFMRP= "ON BASIS OF MRP";
	public static final String ONBASISOFDISCOUNTINCLUDINGTAX = "ON BASIS OF DISCOUNT INCLUDING TAX";
	public static final String OFFLINECUSTOMERCREATION = "OFFLINE CUSTOMER CREATION";
	public static final String YES="YES";
	public static final String OFFLINE="OFFLINE";
	public static final String ONLINE="ONLINE";
	public static final String STOCKOK="STOCKOK";
	public static final String COSTPRICE="COST PRICE" ;
	public static final String NOBATCH="NOBATCH";
	public static final String SUCCESS="SUCCESS";
	public static final String FAILED="FAILED";
}
