package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.ItemMst;

import javafx.application.HostServices;

public class ExportItemMstToExcel {

	


	
	public void exportToExcel( String FileName, ArrayList<ItemMst> aHM )  {


		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Item Name");
			
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Category Name");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Standard Price");
			cell=row.createCell(cellnum++);
			cell.setCellValue("Tax Rate");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Unit");
			cell = row.createCell(cellnum++);

			cell.setCellValue("HsnCode");
			cell = row.createCell(cellnum++);

			cell.setCellValue("BarCode");
			cell = row.createCell(cellnum++);

			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
				Iterator itr = aHM.iterator();
				while (itr.hasNext()) {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
					 LinkedHashMap element = (LinkedHashMap) itr.next();
					 System.out.println("accaccaccacc33"+element.get("id"));
					 Object itemName = (String) element.get("itemName");
					 Object categoryName = (String) element.get("categoryName");
					 Object standardPrice = (String) element.get("standardPrice");
					 Object taxRate = (String) element.get("taxRate");
					 
					 Object unitName = (String) element.get("unitName");
					 Object hsnCode = (String) element.get("hsnCode");
					 Object barCode = (String) element.get("barCode");
					 
					 
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)itemName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)categoryName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)standardPrice);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)taxRate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)unitName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)hsnCode);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)barCode);
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
	
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	}
	

}
