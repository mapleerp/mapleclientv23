package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.B2bSalesReport;
import com.maple.report.entity.HsnCodeSaleReport;
import com.maple.report.entity.HsnWisePurchaseDtlReport;
import com.maple.report.entity.HsnWiseSalesDtlReport;

import javafx.application.HostServices;

public class ExportGstSalesToExcel {

	




	
	public void exportToExcel( String FileName, List<B2bSalesReport>hsnCodePurchaseReportList)  {

	 

		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 1;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
		 cell.setCellValue("Sales Mode");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Tax Rate");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Taxable Vale");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Cgst Amount");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Sgst Amount");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Cess Amount");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Igst Amount");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Total Amunt");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Tax Amount");
			cell = row.createCell(cellnum++);
			
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
			for(int count=0; count<hsnCodePurchaseReportList.size(); count++)
			{
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
			
				
					 Object salesMode = hsnCodePurchaseReportList.get(count).getSalesMode();
					 Object taxRate = hsnCodePurchaseReportList.get(count).getTaxRate();
					 Object taxableValue = hsnCodePurchaseReportList.get(count).getTaxableAmount();
					 
					 Object cgstAmount = hsnCodePurchaseReportList.get(count).getCgstAmount();
					 
					 
					 Object sgstAmount = hsnCodePurchaseReportList.get(count).getSgstAmount();
					 
					 Object cessAmount = hsnCodePurchaseReportList.get(count).getCessAmount();
					 Object igstAmount = hsnCodePurchaseReportList.get(count).getIgstAmount();
					 
					 Object totalAmount = hsnCodePurchaseReportList.get(count).getAmount();
					 Object taxAmount = hsnCodePurchaseReportList.get(count).getTaxAmount();

						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)salesMode);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)taxRate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)taxableValue);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)cgstAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)sgstAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)cessAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)igstAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)totalAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)taxAmount);
				

					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	}
	


}
