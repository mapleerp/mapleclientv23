package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.AccountDtls;
import com.maple.mapleclient.entity.MemberWiseReceipt;

import javafx.application.HostServices;

public class ExportAllMemberWiseReceiptReport {


	public void exportToExcel( String FileName, ArrayList<MemberWiseReceipt> aHM )  {


		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("HEAD OF FAMILY");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("FAMILY NAME");
	
			
			
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
				Iterator itr = aHM.iterator();
				while (itr.hasNext()) {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
					 LinkedHashMap element = (LinkedHashMap) itr.next();
					 System.out.println("accaccaccacc33"+element.get("id"));
					 Object headOfFamily = (String) element.get("headOfFamily");
					 Object familyName = (String) element.get("familyName");
				
					 List <AccountDtls>accountDtlList=(List <AccountDtls>) element.get("accountDtls");
						System.out.print(accountDtlList.size()+"accountlist size isssssssssssssssssssssssssssssssssssss");
					 
						
						
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)headOfFamily);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)familyName);
					 cell = row.createCell(cellnum++);
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	
	try {
		workbook.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	}
	
	
}
