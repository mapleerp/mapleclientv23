package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.AccountBalanceReport;
import com.maple.report.entity.HsnCodeSaleReport;

import javafx.application.HostServices;

public class ExportStatementOfAccountToExcel {

	

	public void exportToExcel( String FileName, List<AccountBalanceReport> aHM,String StartDate,String endDate ,String accountName,Double openingBalance,Integer slNo,Date date )  {

	 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 5;
		int cellnum = 0;
		String userBranch=SystemSetting.getUser().getBranchCode();
		
		Row row0 = sheet.createRow(0);
		 
		 Row row = sheet.createRow(rownum++);
		 Row row1 = sheet.createRow(1);
		 Row row2 = sheet.createRow(2);
		 Row row3 = sheet.createRow(3);
		 Row row4 = sheet.createRow(4);
	
		 Cell cell = row1.createCell(0);
			 
			cell.setCellValue("From Date");
			
			cell = row2.createCell(0);
			cell.setCellValue("To Date");
			cell = row3.createCell(0);
			cell.setCellValue("Account Name");
		/*
		 * cell = row4.createCell(2); cell.setCellValue("Opening Balance Type");
		 */
			cell = row4.createCell(0);
			cell.setCellValue("Opening Balance");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Sl NO");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Date");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Invoice Number");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Debit");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Credit");
			cell = row.createCell(cellnum++);


	    	cell.setCellValue("Closing Balance Type");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Closing Balance");
			
		
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		for(int count=0; count< aHM.size(); count++)
			
			 {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
				
		
					 Object StartDatet=StartDate;
					 Object EndDate = endDate;
					 Object AccountName =accountName;
					 Object openingbalance=openingBalance;
					 Object SlNo = count;
				 Object Date =  aHM.get(count).getvDate();
					 Object InvoiceNumber = aHM.get(count).getRemark();
					 Object openingBalanceType= aHM.get(count).getOpeningBalanceType();
					 Object closingBalanceType=aHM.get(count).getClosingBalanceType();	 
					 Object Debit = aHM.get(count).getDebit();
					 Object Credit = aHM.get(count).getCredit();
					 Object ClosingBalance = aHM.get(count).getClosingBalance();
					 
					 Cell cell0 = row0.createCell(0);
					 cell0.setCellValue(userBranch);
					
						cell = row1.createCell(1);
					 cell.setCellValue((String)StartDatet);
					 cell = row2.createCell(1);
					 cell.setCellValue((String)EndDate);
					 cell = row3.createCell(1);
					 cell.setCellValue((String)AccountName);
					 cell=row4.createCell(2);
					 cell.setCellValue((String)openingBalanceType);
					 cell=row4.createCell(1);
					 cell.setCellValue((Double)openingbalance);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Integer)SlNo);
					 cell = row.createCell(cellnum++);
					 System.out.print(Date+"date isssssss#############################################################################################################################################");
					 cell.setCellValue((String)Date);
					 cell = row.createCell(cellnum++);
					 
					 cell.setCellValue((String)InvoiceNumber);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)Debit);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)Credit);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)closingBalanceType);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)ClosingBalance);
					
									
				}

		} catch (Exception e1) {
		
			e1.printStackTrace();
			System.out.print(e1.getMessage());
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	}
	
}
