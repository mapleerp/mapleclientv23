package com.maple.maple.util;

import java.time.LocalDate;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.entity.BarcodeBatchMst;
import com.maple.mapleclient.entity.BarcodeConfigurationMst;
import com.maple.mapleclient.entity.BarcodeFormatMst;
import com.maple.mapleclient.entity.ItemMst;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.maple.javapos.print.PosTaxLayoutPrint;

import com.maple.mapleclient.entity.NutritionValueDtl;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.restService.RestCaller;
import com.sun.jna.Library;
import com.sun.jna.Native;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class TSCBarcode {
	private static final Logger log = LoggerFactory.getLogger(TSCBarcode.class);

	public interface TscLibDll extends Library {
		// public final static Logger log = Logger.getLogger(TscLibDll.class);
		// "E:\\TSCLIB.dll"
		TscLibDll INSTANCE = (TscLibDll) Native.loadLibrary(SystemSetting.DLLPATH, TscLibDll.class);

		int about();

		int openport(String pirnterName);

		int closeport();

		int sendcommand(String printerCommand);

		int setup(String width, String height, String speed, String density, String sensor, String vertical,
				String offset);

		int downloadpcx(String filename, String image_name);

		int barcode(String x, String y, String type, String height, String readable, String rotation, String narrow,
				String wide, String code);

		int printerfont(String x, String y, String fonttype, String rotation, String xmul, String ymul, String text);

		int clearbuffer();

		int printlabel(String set, String copy);

		int formfeed();

		int nobackfeed();

		int windowsfont(int x, int y, int fontheight, int rotation, int fontstyle, int fontunderline, String szFaceName,
				String content);
	}

	public void PrintBarcodeOld(String NumberOfCopies, String ManufactureDate, String ExpiryDate, String ItemName,
			String Barcode, String Mrp, String PrinterNAme) {

		TscLibDll.INSTANCE.openport(PrinterNAme);

		log.info("TSC Loaded");

		// TscLibDll.INSTANCE.downloadpcx("C:\\UL.PCX", "UL.PCX");
		TscLibDll.INSTANCE.sendcommand("REM ***** This is a print by JAVA. *****");

		TscLibDll.INSTANCE.setup("100", "40", "5", "8", "0", "0", "0");
		TscLibDll.INSTANCE.clearbuffer();
		// TscLibDll.INSTANCE.sendcommand("PUTPCX 550,10,\"UL.PCX\"");
		/*
		 * TscLibDll.INSTANCE.printerfont ("100", "10", "3", "0", "1", "1",
		 * "(JAVA) DLL Test!!");
		 */

		TscLibDll.INSTANCE.barcode("340", "50", "128", "50", "1", "0", "2", "2", Barcode);

		TscLibDll.INSTANCE.windowsfont(340, 130, 28, 0, 0, 0, "arial", ItemName);

		TscLibDll.INSTANCE.windowsfont(310, 170, 24, 0, 0, 0, "arial", ManufactureDate);
		TscLibDll.INSTANCE.windowsfont(310, 200, 24, 0, 0, 0, "arial", ExpiryDate);
		TscLibDll.INSTANCE.windowsfont(510, 180, 35, 0, 0, 0, "arial", Mrp);

		// TscLibDll.INSTANCE.printerfont("300", "200", "48", "0", "3", "1", "DEG 0");
		/*
		 * TscLibDll.INSTANCE.windowsfont(400, 200, 48, 90, 3, 1, "arial", "DEG 90");
		 * TscLibDll.INSTANCE.windowsfont(400, 200, 48, 180, 3, 1, "arial", "DEG 180");
		 * TscLibDll.INSTANCE.windowsfont(400, 200, 48, 270, 3, 1, "arial", "DEG 270");
		 */

		// 2 = Number of Copies
		TscLibDll.INSTANCE.printlabel("1", NumberOfCopies);
		TscLibDll.INSTANCE.closeport();
	}

	public static void manualPrint(String NumberOfCopies, String ManufactureDate, String ExpiryDate, String ItemName,
			String Barcode, String Mrp, String IngLine1, String IngLine2, String PrinterNAme, String netWt,
			String batch) {

		System.out.println("SSSSSSSSSSSSSSSSSSSS BARCODE fORMAT SSSSSSSSSSSSSSSSSSSSSSS");
		System.out.println(SystemSetting.BARCODE_FORMAT);

		TscLibDll.INSTANCE.openport(PrinterNAme);

		TscLibDll.INSTANCE.sendcommand("CLS");
		// TscLibDll.INSTANCE.clearbuffer();

		String BARCODE_SIZE = SystemSetting.BARCODE_SIZE;

		if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
			BARCODE_SIZE = "SIZE 3,1.5";
		}

		// TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

		String BARCODE_FORMAT = SystemSetting.BARCODE_FORMAT;

		if (null == BARCODE_FORMAT || BARCODE_FORMAT.length() < 3) {
			BARCODE_SIZE = "FORMAT0";
		}

		ResponseEntity<ParamValueConfig> paramval = RestCaller.getParamValueConfig("BARCODEFORMAT");
		if (null != paramval.getBody()) {
			ResponseEntity<List<BarcodeFormatMst>> barcodelist = RestCaller
					.getBarcodeConfigMstByFormatName(paramval.getBody().getValue());
			if (barcodelist.getBody().size() > 0) {
				setBarcodeProperty(barcodelist.getBody());
			}
			SystemSetting.BARCODE_FORMAT = paramval.getBody().getValue();
			BARCODE_FORMAT = SystemSetting.BARCODE_FORMAT;
		}

		if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT0")) {

			if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
				BARCODE_SIZE = "SIZE 3,1.5";
			}

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 200,10, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 200,90, \"2\",0,1,1,\"" + "Packed:" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 200,120, \"2\",0,1,1,\"" + "BestBefore:" + ExpiryDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 200,150, \"2\",0,1,1,\"" + ItemName + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 200,180, \"2\",0,1,1,\"" + "NetWt:" + netWt + "\"");

			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT 200,210, \"2\",0,1,1,\"" + "Ingredients:" + IngLine1 + "\"");
			}

			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT 200,240, \"2\",0,1,1,\"" + IngLine2 + "\"");
			}

			// String batchCode = SystemSetting.UtilDateToString(SystemSetting.systemDate,
			// "yyddMM");
			// batchCode = batchCode.replace("20", "A");

			String batchLine = "Batch:" + batch;

			TscLibDll.INSTANCE.sendcommand("TEXT 200,230, \"2\",0,1,1,\"" + "Batch:" + batchLine + "\"");

			/*
			 * FCCI is stored in Nutrition title is
			 */
			TscLibDll.INSTANCE.sendcommand("TEXT 200,250, \"2\",0,1,1,\"" + SystemSetting.getNUTRITIONTITLE() + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,10, \"3\",90,1,1,\"" + "Mrp:" + Mrp + "\"");

			/*
			 * Place is stored in nutrition fact. To be corrected.
			 * 
			 * 
			 * 
			 */
			TscLibDll.INSTANCE.sendcommand("TEXT 140,10, \"3\",90,1,1,\"" + SystemSetting.getNutritionfact() + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 170,10, \"3\",90,1,1,\"" + SystemSetting.getBARCODETITLE() + "\"");

		} else if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT1")) {

			if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
				BARCODE_SIZE = "SIZE 3,1.5";
			}

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + ExpiryDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + ItemName + "\"");

			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" + IngLine1 + "\"");
			}

			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + IngLine2 + "\"");
			}

			TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" + Mrp + "\"");

		} else if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT2")) {

			if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
				BARCODE_SIZE = "SIZE 3,1.5";
			}

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			ResponseEntity<BarcodeConfigurationMst> barcodeConfiguration = RestCaller.getBarcodeConfig();

			BarcodeConfigurationMst barcodeConfigurationMst = barcodeConfiguration.getBody();
			String BARCODEx = barcodeConfigurationMst.getBarcodeX();
			String BARCODEy = barcodeConfigurationMst.getBarcodeY();
			String MFGDATEx = barcodeConfigurationMst.getManufactureDateX();
			String MFGDATEy = barcodeConfigurationMst.getManufactureDateY();
			String EXPDATEx = barcodeConfigurationMst.getExpiryDateX();
			String EXPDATEy = barcodeConfigurationMst.getExpiryDateY();
			String ITEMNAMEx = barcodeConfigurationMst.getItemNameX();
			String ITEMNAMEy = barcodeConfigurationMst.getItemNameY();
			String INGLINE1x = barcodeConfigurationMst.getIngLine1X();
			String INGLINE1y = barcodeConfigurationMst.getIngLine1Y();
			String INGLINE2x = barcodeConfigurationMst.getIngLine2X();
			String INGLINE2y = barcodeConfigurationMst.getIngLine2Y();
			String MRPx = barcodeConfigurationMst.getBarcodeX();
			String MRPy = barcodeConfigurationMst.getBarcodeY();
			System.out.print(BARCODEx + "barcodex");
			System.out.print(BARCODEy + "barcodey");
			System.out.print(MFGDATEx + "mfgdatex");
			System.out.print(MFGDATEy + "mfgdatey");
			System.out.print(ITEMNAMEx + "itemnamex");
			System.out.print(ITEMNAMEy + "itemnamey");
			System.out.print(INGLINE1x + "ingredient line1x");
			System.out.print(INGLINE1y + "ingredient line1y");
			System.out.print(INGLINE2x + "ingredient line2x");
			System.out.print(INGLINE2y + "ingredient line2y");
			System.out.print(EXPDATEx + "exp datex");
			System.out.print(EXPDATEy + "expdatey");

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE BARCODEx,BARCODEy, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT MFGDATEx,MFGDATEy, \"2\",0,1,1,\"" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT EXPDATEx,EXPDATEy, \"2\",0,1,1,\"" + ExpiryDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT ITEMNAMEx,ITEMNAMEy, \"2\",0,1,1,\"" + ItemName + "\"");

			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT INGLINE1x,INGLINE1y, \"2\",0,1,1,\"" + IngLine1 + "\"");
			}

			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT INGLINE2x,INGLINE2y, \"2\",0,1,1,\"" + IngLine2 + "\"");
			}

			TscLibDll.INSTANCE.sendcommand("TEXT EXPDATEx,EXPDATEy, \"3\",90,1,1,\"" + Mrp + "\"");

		} else if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT3") || 
				BARCODE_FORMAT.equalsIgnoreCase("JAWAHAR FOODS")) { // LEKSHMI BAKERY
			
			String packagingLicenceNo = SystemSetting.PACKAGINGLICENCE;
			String FSSAI = SystemSetting.FSSAI;

			BARCODE_SIZE = "SIZE 3,1.5";
			String barcodeOrgin = null;
			ResponseEntity<BarcodeBatchMst> barcodeBatchMstResp = RestCaller.getBarcodeBatchMstById(Barcode);
			BarcodeBatchMst barcodeBatchMst = barcodeBatchMstResp.getBody();

			if (null != barcodeBatchMst) {
				barcodeOrgin = barcodeBatchMst.getBarcode();
			}

			ResponseEntity<List<ItemMst>> itemMstResp = RestCaller.getItemByBarcode(barcodeOrgin);
			List<ItemMst> itemMstList = itemMstResp.getBody();

			ItemMst itemMst = null;

			if (itemMstList.size() > 0) {
				itemMst = itemMstList.get(0);
			} else {
				ResponseEntity<ItemMst> itemMstResp1 = RestCaller.getItemByNameRequestParam(ItemName);
				itemMst = itemMstResp1.getBody();
			}
			
			String hscCode = itemMst.getHsnCode();
			if(null == itemMst.getHsnCode())
			{
				hscCode = "";
			}
			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			
			TscLibDll.INSTANCE.sendcommand("CLS");

			TscLibDll.INSTANCE.sendcommand("TEXT 10,20, \"2\",0,1,1,\"MRP RS. " + Mrp + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 10,40, \"1\",0,1,1,\"(Incl. of all Taxes)\"");


			TscLibDll.INSTANCE.sendcommand("TEXT 250,20, \"2\",0,1,1,\"Mfg.&Pkd by:JAWAHAR FOODS\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 250,45, \"1\",0,1,1,\"Kottarakkara ,Kollam,\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 250,60, \"1\",0,1,1,\"PH:9633232731\"");

			
			TscLibDll.INSTANCE.sendcommand("TEXT 10,80, \"2\",0,1,1,\"" + ItemName + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 350,80, \"1\",0,1,1,\"NUTRITION FACTS\"");


			
			
			TscLibDll.INSTANCE.sendcommand("TEXT 10,110, \"1\",0,1,1,\"HSN:" + hscCode + "\"");

			
			String batchDate = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());

			TscLibDll.INSTANCE.sendcommand("TEXT 200,110, \"1\",0,1,1,\"Batch " + batchDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 0,130, \"1\",0,1,1,\" Net Wt/Qty" + netWt + "\"");
			
			TscLibDll.INSTANCE.sendcommand("TEXT 10,150, \"2\",0,1,1,\"Packed: " + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 10,180, \"2\",0,1,1,\"Best Before :" + ExpiryDate + "\"");
			
			
			TscLibDll.INSTANCE.sendcommand("TEXT 10,210, \"1\",0,1,1,\"Packg. licence:"+packagingLicenceNo+"\"");

		
			
			

			
			TscLibDll.INSTANCE.sendcommand("BOX 350,100,530,300,3 ");
			
			if(null != itemMst)
			{
				if(null != itemMst.getNutrition1())
				{
					String nutrition = itemMst.getNutrition1();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,110, \"1\",0,1,1,\"" + nutrition + "\"");
				}
				if(null != itemMst.getNutrition2())
				{
					String nutrition = itemMst.getNutrition2();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,130, \"1\",0,1,1,\"" + nutrition + "\"");
				}
				if(null != itemMst.getNutrition3())
				{
					String nutrition = itemMst.getNutrition3();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,150, \"1\",0,1,1,\"" + nutrition + "\"");
				}
				if(null != itemMst.getNutrition4())
				{
					String nutrition = itemMst.getNutrition4();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,170, \"1\",0,1,1,\"" + nutrition + "\"");
				}
				if(null != itemMst.getNutrition5())
				{
					String nutrition = itemMst.getNutrition5();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,190, \"1\",0,1,1,\"" + nutrition + "\"");
				}
				if(null != itemMst.getNutrition6())
				{
					String nutrition = itemMst.getNutrition6();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,210, \"1\",0,1,1,\"" + nutrition + "\"");
				}
				
				if(null != itemMst.getNutrition7())
				{
					String nutrition = itemMst.getNutrition7();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,230, \"1\",0,1,1,\"" + nutrition + "\"");
				}
				
				if(null != itemMst.getNutrition8())
				{
					String nutrition = itemMst.getNutrition8();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,250, \"1\",0,1,1,\"" + nutrition + "\"");
				}

				if(null != itemMst.getNutrition9())
				{
					String nutrition = itemMst.getNutrition9();
					if(nutrition.length() > 17)
					{
						nutrition = nutrition.substring(0, 16);
					}
					TscLibDll.INSTANCE.sendcommand("TEXT 360,270, \"1\",0,1,1,\"" + nutrition + "\"");
				}
			}
			
			
			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT 540,300, \"1\",270,1,1,\"" + "Ingredients : \"");
				TscLibDll.INSTANCE.sendcommand("TEXT 560,300, \"1\",270,1,1,\"" +   IngLine1 + "\"");
			}
			
			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT 580,300, \"1\",270,1,1,\"" + IngLine2 + "\"");
			}
			TscLibDll.INSTANCE.sendcommand("TEXT 600,300, \"1\",270,1,1,\"FSSAI:"+FSSAI+"\"");

			
			TscLibDll.INSTANCE.sendcommand("BARCODE 10,230, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");
//			TscLibDll.INSTANCE.sendcommand("TEXT 120,280, \"2\",0,1,1,\""+Barcode+"\"");

		} else if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT4")) {

			if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
				BARCODE_SIZE = "SIZE 3,1.5";
			}

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + ExpiryDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + ItemName + "\"");

			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" + IngLine1 + "\"");
			}

			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + IngLine2 + "\"");
			}

			TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" + Mrp + "\"");

		} else if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT5")) { // NO INGREDIENTS

			if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
				BARCODE_SIZE = "SIZE 3,1.5";
			}

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + ExpiryDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + ItemName + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" + Mrp + "\"");

		} else if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT6")) { // 2 SMALL STICKER IN ROW

			if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
				BARCODE_SIZE = "SIZE 3,1.5";
			}

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("TEXT 10,20, \"2\",0,1,1,\"" + SystemSetting.BARCODETITLE + "\"");

			TscLibDll.INSTANCE.sendcommand("BARCODE 10,40, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			/*
			 * TscLibDll.INSTANCE.sendcommand("TEXT 10,120, \"2\",0,1,1,\"" +
			 * ManufactureDate + "\"");
			 * TscLibDll.INSTANCE.sendcommand("TEXT 10,150, \"2\",0,1,1,\"" + ExpiryDate +
			 * "\"");
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 10,180, \"2\",0,1,1,\"" + ItemName +
			 * "\"");
			 * 
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 270,40, \"3\",90,1,1,\"" + Mrp + "\"");
			 * 
			 * 
			 * // Second Barcode
			 * 
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 340,20, \"2\",0,1,1,\"" +
			 * SystemSetting.BARCODETITLE + "\"");
			 * 
			 * TscLibDll.INSTANCE.sendcommand("BARCODE 340,40, \"128\",50,3,0,2,2,\"" +
			 * Barcode + "\" ");
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 340,120, \"2\",0,1,1,\"" +
			 * ManufactureDate + "\"");
			 * TscLibDll.INSTANCE.sendcommand("TEXT 340,150, \"2\",0,1,1,\"" + ExpiryDate +
			 * "\"");
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 340,180, \"2\",0,1,1,\"" + ItemName +
			 * "\"");
			 * 
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 600,40, \"3\",90,1,1,\"" + Mrp + "\"");
			 */

		}
		/*
		 * For Ambrosia
		 */
		if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT7") || BARCODE_FORMAT.equalsIgnoreCase("BISCOTTI")) {

			/*
			 * Prineter Label size - 40.0 mm width 120.0 mm height 8dpmm
			 * 
			 */

			// AMBROSIA BISCOTTI
			String barcodeX = null;
			String barcodeY = null;

			String barcodeItemNameX = null;
			String barcodeItemNameY = null;

			String barcodeMrpX = null;
			String barcodeMrpY = null;

			String barcodeNetWtX = null;
			String barcodeNetWtY = null;

			String manufacturingDateX = null;
			String manufacturingDateY = null;

			String expiryDateX = null;
			String expiryDateY = null;

			String barcodeBatchX = null;
			String barcodeBatchY = null;

			String barcodeIngLine1X = null;
			String barcodeIngLine1Y = null;

			String barcodeIngLine2X = null;
			String barcodeIngLine2Y = null;

//		
//	    ResponseEntity<BarcodeConfigurationMst>barcodeConfiguration = RestCaller.getBarcodeConfig();
//		
//			BarcodeConfigurationMst barcodeConfigurationMst=barcodeConfiguration.getBody();
//			  barcodeX= barcodeConfigurationMst.getBarcodeX();
//			  barcodeY=barcodeConfigurationMst.getBarcodeY();
//			  manufacturingDateX=barcodeConfigurationMst.getManufactureDateX();
//			  manufacturingDateY=barcodeConfigurationMst.getManufactureDateY();
//			  expiryDateX=barcodeConfigurationMst.getExpiryDateX();
//			  expiryDateY=barcodeConfigurationMst.getExpiryDateY();
//			  barcodeItemNameX=barcodeConfigurationMst.getItemNameX();
//			  barcodeItemNameY=barcodeConfigurationMst.getItemNameY();
//			  barcodeIngLine1X=barcodeConfigurationMst.getIngLine1X();
//			  barcodeIngLine1Y=barcodeConfigurationMst.getIngLine1Y();
//			  barcodeIngLine2X=barcodeConfigurationMst.getIngLine2X();
//			  barcodeIngLine2Y=barcodeConfigurationMst.getIngLine2Y();
//			  barcodeMrpX=barcodeConfigurationMst.getBarcodeX();
//			  barcodeMrpY=barcodeConfigurationMst.getBarcodeY();

			/*
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeX =
			 * RestCaller.getParamValueConfig("BARCODE_X"); if(null !=
			 * paramValueBarcodeX.getBody()) { barcodeX =
			 * paramValueBarcodeX.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeY =
			 * RestCaller.getParamValueConfig("BARCODE_Y"); if(null !=
			 * paramValueBarcodeY.getBody()) { barcodeY =
			 * paramValueBarcodeY.getBody().getValue(); } else { return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeItemNameX =
			 * RestCaller.getParamValueConfig("ITEMNAME_X"); if(null !=
			 * paramValueBarcodeItemNameX.getBody()) { barcodeItemNameX =
			 * paramValueBarcodeItemNameX.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeItemNameY =
			 * RestCaller.getParamValueConfig("ITEMNAME_Y"); if(null !=
			 * paramValueBarcodeItemNameY.getBody()) { barcodeItemNameY =
			 * paramValueBarcodeItemNameY.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeMrpX =
			 * RestCaller.getParamValueConfig("MRP_X"); if(null !=
			 * paramValueBarcodeMrpX.getBody()) { barcodeMrpX =
			 * paramValueBarcodeMrpX.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeMrpY =
			 * RestCaller.getParamValueConfig("MRP_Y"); if(null !=
			 * paramValueBarcodeMrpY.getBody()) { barcodeMrpY =
			 * paramValueBarcodeMrpY.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeNetWtX =
			 * RestCaller.getParamValueConfig("NETWT_X"); if(null !=
			 * paramValueBarcodeNetWtX.getBody()) { barcodeNetWtX =
			 * paramValueBarcodeNetWtX.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeNetWtY =
			 * RestCaller.getParamValueConfig("NETWT_Y"); if(null !=
			 * paramValueBarcodeNetWtY.getBody()) { barcodeNetWtY =
			 * paramValueBarcodeNetWtY.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueManufacturingX =
			 * RestCaller.getParamValueConfig("MFGDATE_X"); if(null !=
			 * paramValueManufacturingX.getBody()) { manufacturingDateX =
			 * paramValueManufacturingX.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueManufacturingY =
			 * RestCaller.getParamValueConfig("MFGDATE_Y"); if(null !=
			 * paramValueManufacturingY.getBody()) { manufacturingDateY =
			 * paramValueManufacturingY.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueExpiryDateX =
			 * RestCaller.getParamValueConfig("EXPDATE_X"); if(null !=
			 * paramValueExpiryDateX.getBody()) { expiryDateX =
			 * paramValueExpiryDateX.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueExpiryDateY =
			 * RestCaller.getParamValueConfig("EXPDATE_Y"); if(null !=
			 * paramValueExpiryDateY.getBody()) { expiryDateY =
			 * paramValueExpiryDateY.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeBatchX=
			 * RestCaller.getParamValueConfig("BATCH_X"); if(null !=
			 * paramValueBarcodeBatchX.getBody()) { barcodeBatchX =
			 * paramValueBarcodeBatchX.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeBatchY =
			 * RestCaller.getParamValueConfig("BATCH_Y"); if(null !=
			 * paramValueBarcodeBatchY.getBody()) { barcodeBatchY =
			 * paramValueBarcodeBatchY.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeIngLine1X =
			 * RestCaller.getParamValueConfig("INGLINE1_X"); if(null !=
			 * paramValueBarcodeIngLine1X.getBody()) { barcodeIngLine1X =
			 * paramValueBarcodeIngLine1X.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeIngLine1Y =
			 * RestCaller.getParamValueConfig("INGLINE1_Y"); if(null !=
			 * paramValueBarcodeIngLine1Y.getBody()) { barcodeIngLine1Y =
			 * paramValueBarcodeIngLine1Y.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeIngLine2Y =
			 * RestCaller.getParamValueConfig("INGLINE2_Y"); if(null !=
			 * paramValueBarcodeIngLine2Y.getBody()) { barcodeIngLine2Y =
			 * paramValueBarcodeIngLine2Y.getBody().getValue(); } else {
			 * 
			 * return; }
			 * 
			 * 
			 * ResponseEntity<ParamValueConfig> paramValueBarcodeIngLine2X =
			 * RestCaller.getParamValueConfig("INGLINE2_X"); if(null !=
			 * paramValueBarcodeIngLine2X.getBody()) { barcodeIngLine2X =
			 * paramValueBarcodeIngLine2X.getBody().getValue(); } else {
			 * 
			 * return; }
			 */

			BARCODE_SIZE = "SIZE 3.5,4.5";

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			// TscLibDll.INSTANCE.sendcommand("GAPDETECT");

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			// TscLibDll.INSTANCE.sendcommand("BARCODE "+barcodeX+","+barcodeY+",
			// \"128\",40,3,90,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("BARCODE " + 150 + "," + 500 + ", \"128\",40,3,90,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("BARCODE " + 500 + "," + 500 + ", \"128\",40,3,90,2,2,\"" + Barcode + "\" ");

			// TscLibDll.INSTANCE.sendcommand("TEXT
			// "+barcodeItemNameX+","+barcodeItemNameY+", \"2\",0,1,1,\"" + ItemName +
			// "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 150 + "," + 375 + ", \"2\",90,1,1,\"" + Mrp + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 500 + "," + 375 + ", \"2\",90,1,1,\"" + Mrp + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+150+","+375+", \"2\",90,1,1,\"" + Mrp
			// + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+barcodeNetWtX+","+barcodeNetWtY+",
			// \"2\",0,1,1,\"" + netWt + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 125 + "," + 375 + ", \"2\",90,1,1,\"" + netWt + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 475 + "," + 375 + ", \"2\",90,1,1,\"" + netWt + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT
			// "+manufacturingDateX+","+manufacturingDateY+", \"2\",0,1,1,\"" +
			// ManufactureDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 100 + "," + 375 + ", \"2\",90,1,1,\"" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 450 + "," + 375 + ", \"2\",90,1,1,\"" + ManufactureDate + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+expiryDateX+","+expiryDateY+",
			// \"2\",0,1,1,\"" + ExpiryDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 75 + "," + 375 + ", \"2\",90,1,1,\"" + ExpiryDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 425 + "," + 375 + ", \"2\",90,1,1,\"" + ExpiryDate + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+barcodeBatchX+","+barcodeBatchY+",
			// \"2\",0,1,1,\"" + batch + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 50 + "," + 375 + ", \"2\",90,1,1,\"" + batch + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 400 + "," + 375 + ", \"2\",90,1,1,\"" + batch + "\"");

			/*
			 * if (IngLine1.length() > 0) {
			 * TscLibDll.INSTANCE.sendcommand("TEXT "+barcodeIngLine1X+","+
			 * barcodeIngLine1Y+", \"2\",0,1,1,\"" + IngLine1 + "\""); }
			 * 
			 * if (IngLine2.length() > 0) {
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT "+barcodeIngLine2X+","+
			 * barcodeIngLine2Y+", \"2\",0,1,1,\"" + IngLine2 + "\""); }
			 */

		}
		if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT8") || BARCODE_FORMAT.equalsIgnoreCase("CHOCOLATE")) {

			/*
			 * CHOCOLATE Prineter Label size - 85.0 mm mm width 70.0 mm height 8dpmm
			 * 
			 */

			// AMBROSIA BISCOTTI
			String barcodeX = null;
			String barcodeY = null;

			String barcodeItemNameX = null;
			String barcodeItemNameY = null;

			String barcodeMrpX = null;
			String barcodeMrpY = null;

			String barcodeNetWtX = null;
			String barcodeNetWtY = null;

			String manufacturingDateX = null;
			String manufacturingDateY = null;

			String expiryDateX = null;
			String expiryDateY = null;

			String barcodeBatchX = null;
			String barcodeBatchY = null;

			String barcodeIngLine1X = null;
			String barcodeIngLine1Y = null;

			String barcodeIngLine2X = null;
			String barcodeIngLine2Y = null;

//			ResponseEntity<BarcodeConfigurationMst> barcodeConfiguration = RestCaller.getBarcodeConfig();
//
//			BarcodeConfigurationMst barcodeConfigurationMst = barcodeConfiguration.getBody();
//			barcodeX = barcodeConfigurationMst.getBarcodeX();
//			barcodeY = barcodeConfigurationMst.getBarcodeY();
//			manufacturingDateX = barcodeConfigurationMst.getManufactureDateX();
//			manufacturingDateY = barcodeConfigurationMst.getManufactureDateY();
//			expiryDateX = barcodeConfigurationMst.getExpiryDateX();
//			expiryDateY = barcodeConfigurationMst.getExpiryDateY();
//			barcodeItemNameX = barcodeConfigurationMst.getItemNameX();
//			barcodeItemNameY = barcodeConfigurationMst.getItemNameY();
//			barcodeIngLine1X = barcodeConfigurationMst.getIngLine1X();
//			barcodeIngLine1Y = barcodeConfigurationMst.getIngLine1Y();
//			barcodeIngLine2X = barcodeConfigurationMst.getIngLine2X();
//			barcodeIngLine2Y = barcodeConfigurationMst.getIngLine2Y();
//			barcodeMrpX = barcodeConfigurationMst.getBarcodeX();
//			barcodeMrpY = barcodeConfigurationMst.getBarcodeY();

			BARCODE_SIZE = "SIZE 3.5,2.5";

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			// TscLibDll.INSTANCE.sendcommand("GAPDETECT");

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			// TscLibDll.INSTANCE.sendcommand("BARCODE "+barcodeX+","+barcodeY+",
			// \"128\",40,3,90,2,2,\"" + Barcode + "\" ");

			String barcodeOrgin = null;
			ResponseEntity<BarcodeBatchMst> barcodeBatchMstResp = RestCaller.getBarcodeBatchMstById(Barcode);
			BarcodeBatchMst barcodeBatchMst = barcodeBatchMstResp.getBody();

			if (null != barcodeBatchMst) {
				barcodeOrgin = barcodeBatchMst.getBarcode();
			}

			ResponseEntity<List<ItemMst>> itemMstResp = RestCaller.getItemByBarcode(barcodeOrgin);
			List<ItemMst> itemMstList = itemMstResp.getBody();

			ItemMst itemMst = new ItemMst();

			if (itemMstList.size() > 0) {
				itemMst = itemMstList.get(0);
			}

			TscLibDll.INSTANCE.sendcommand("BARCODE " + 450 + "," + 150 + ", \"128\",40,3,90,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("BOX 460,30,650,380,4 ");

			if (null != itemMst) {
				if (null != itemMst.getNutrition1()) {

					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 475 + "," + 80 + ", \"1\",0,1,1,\"" + itemMst.getNutrition1() + "\"");
				}
				if (null != itemMst.getNutrition2()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 475 + "," + 100 + ", \"1\",0,1,1,\"" + itemMst.getNutrition2() + "\"");
				}
				if (null != itemMst.getNutrition3()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 120 + ", \"1\",0,1,1,\"" + itemMst.getNutrition3() + "\"");
				}
				if (null != itemMst.getNutrition4()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 140 + ", \"1\",0,1,1,\"" + itemMst.getNutrition4() + "\"");

				}
				if (null != itemMst.getNutrition5()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 160 + ", \"1\",0,1,1,\"" + itemMst.getNutrition5() + "\"");
				}
				if (null != itemMst.getNutrition6()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 180 + ", \"1\",0,1,1,\"" + itemMst.getNutrition6() + "\"");
				}
				if (null != itemMst.getNutrition7()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 200 + ", \"1\",0,1,1,\"" + itemMst.getNutrition7() + "\"");
				}
				if (null != itemMst.getNutrition8()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 220 + ", \"1\",0,1,1,\"" + itemMst.getNutrition8() + "\"");
				}
				if (null != itemMst.getNutrition9()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 240 + ", \"1\",0,1,1,\"" + itemMst.getNutrition9() + "\"");
				}
				if (null != itemMst.getNutrition10()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 260 + ", \"1\",0,1,1,\"" + itemMst.getNutrition10() + "\"");
				}
				if (null != itemMst.getNutrition11()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 280 + ", \"1\",0,1,1,\"" + itemMst.getNutrition11() + "\"");
				}
				if (null != itemMst.getNutrition12()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 300 + ", \"1\",0,1,1,\"" + itemMst.getNutrition12() + "\"");
				}
				if (null != itemMst.getNutrition13()) {
					TscLibDll.INSTANCE.sendcommand(
							"TEXT " + 470 + "," + 320 + ", \"1\",0,1,1,\"" + itemMst.getNutrition13() + "\"");
				}

			}
			if (ItemName.trim().length() > 17) {
				ItemName = ItemName.substring(0, 16);

			}
			TscLibDll.INSTANCE.sendcommand("TEXT " + 160 + "," + 120 + ", \"2\",0,1,1,\"" + ItemName + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 160 + "," + 155 + ", \"3\",0,1,1,\"" + Mrp + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+500+","+375+", \"2\",90,1,1,\"" + Mrp
			// + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+150+","+375+", \"2\",90,1,1,\"" + Mrp
			// + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+barcodeNetWtX+","+barcodeNetWtY+",
			// \"2\",0,1,1,\"" + netWt + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 160 + "," + 180 + ", \"2\",0,1,1,\"" + netWt + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+475+","+375+", \"2\",90,1,1,\"" +
			// netWt + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT
			// "+manufacturingDateX+","+manufacturingDateY+", \"2\",0,1,1,\"" +
			// ManufactureDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 160 + "," + 220 + ", \"2\",0,1,1,\"" + ManufactureDate + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+450+","+375+", \"2\",90,1,1,\"" +
			// ManufactureDate + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+expiryDateX+","+expiryDateY+",
			// \"2\",0,1,1,\"" + ExpiryDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 160 + "," + 260 + ", \"2\",0,1,1,\"" + ExpiryDate + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+425+","+375+", \"2\",90,1,1,\"" +
			// ExpiryDate + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+barcodeBatchX+","+barcodeBatchY+",
			// \"2\",0,1,1,\"" + batch + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 160 + "," + 290 + ", \"2\",0,1,1,\"" + batch + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+400+","+375+", \"2\",90,1,1,\"" +
			// batch + "\"");

			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT " + 160 + "," + 320 + ", \"1\",0,1,1,\"" + IngLine1 + "\"");
			}

			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT " + 30 + "," + 360 + ", \"1\",0,1,1,\"" + IngLine2 + "\"");
			}

		}

		if (BARCODE_FORMAT.equalsIgnoreCase("FORMAT9") || BARCODE_FORMAT.equalsIgnoreCase("CHICKEN")) {

			/*
			 * CHICKEN Prineter Label size - 45.0 mm mm width 25.0 mm height 8dpmm
			 * 
			 */

			// AMBROSIA BISCOTTI
			String barcodeX = null;
			String barcodeY = null;

			String barcodeItemNameX = null;
			String barcodeItemNameY = null;

			String barcodeMrpX = null;
			String barcodeMrpY = null;

			String barcodeNetWtX = null;
			String barcodeNetWtY = null;

			String manufacturingDateX = null;
			String manufacturingDateY = null;

			String expiryDateX = null;
			String expiryDateY = null;

			String barcodeBatchX = null;
			String barcodeBatchY = null;

			String barcodeIngLine1X = null;
			String barcodeIngLine1Y = null;

			String barcodeIngLine2X = null;
			String barcodeIngLine2Y = null;

//			ResponseEntity<BarcodeConfigurationMst> barcodeConfiguration = RestCaller.getBarcodeConfig();
//
//			BarcodeConfigurationMst barcodeConfigurationMst = barcodeConfiguration.getBody();
//			barcodeX = barcodeConfigurationMst.getBarcodeX();
//			barcodeY = barcodeConfigurationMst.getBarcodeY();
//			manufacturingDateX = barcodeConfigurationMst.getManufactureDateX();
//			manufacturingDateY = barcodeConfigurationMst.getManufactureDateY();
//			expiryDateX = barcodeConfigurationMst.getExpiryDateX();
//			expiryDateY = barcodeConfigurationMst.getExpiryDateY();
//			barcodeItemNameX = barcodeConfigurationMst.getItemNameX();
//			barcodeItemNameY = barcodeConfigurationMst.getItemNameY();
//			barcodeIngLine1X = barcodeConfigurationMst.getIngLine1X();
//			barcodeIngLine1Y = barcodeConfigurationMst.getIngLine1Y();
//			barcodeIngLine2X = barcodeConfigurationMst.getIngLine2X();
//			barcodeIngLine2Y = barcodeConfigurationMst.getIngLine2Y();
//			barcodeMrpX = barcodeConfigurationMst.getBarcodeX();
//			barcodeMrpY = barcodeConfigurationMst.getBarcodeY();

			BARCODE_SIZE = "SIZE 4,1";

			TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

			// TscLibDll.INSTANCE.sendcommand("GAPDETECT");

			TscLibDll.INSTANCE.sendcommand("DIRECTION 0");
			TscLibDll.INSTANCE.sendcommand("CLS");
			// TscLibDll.INSTANCE.sendcommand("BARCODE "+barcodeX+","+barcodeY+",
			// \"128\",40,3,90,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 2 + "," + 1 + ", \"1\",0,1,1,\"" + ItemName + "\"");

			TscLibDll.INSTANCE.sendcommand("BARCODE " + 20 + "," + 15 + ", \"128\",40,0,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE
					.sendcommand("TEXT " + 3 + "," + 60 + ", \"1\",0,1,1,\"INGREDIENTS: chicken, Garlic, ckilly\"");
			TscLibDll.INSTANCE
					.sendcommand("TEXT " + 3 + "," + 75 + ", \"1\",0,1,1,\"Mfg & Pkgd:WHOLEGRAINS THE BAKERS HUB\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 3 + "," + 90 + ", \"1\",0,1,1,\"FSSAI NO 11315001000343\"");
			TscLibDll.INSTANCE.sendcommand(
					"TEXT " + 3 + "," + 105 + ", \"1\",0,1,1,\"For any complaints Pls Contact above addr\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 3 + "," + 115 + ", \"1\",0,1,1,\"Plot No 41 Kinfra park \"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 3 + "," + 130 + ", \"1\",0,1,1,\"Meenamkulam ph.9388343683 \"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 300 + "," + 130 + ", \"1\",0,1,1,\"MRP:" + Mrp + "\"");

			TscLibDll.INSTANCE
					.sendcommand("TEXT " + 3 + "," + 145 + ", \"1\",0,1,1,\"MFG DATE " + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 3 + "," + 160 + ", \"1\",0,1,1,\"EXP DATE " + ExpiryDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 210 + "," + 160 + ", \"1\",0,1,1,\"Batch:" + batch + "\"");

			// Second Barcode
			TscLibDll.INSTANCE.sendcommand("TEXT " + 410 + "," + 1 + ", \"1\",0,1,1,\"" + ItemName + "\"");

			TscLibDll.INSTANCE.sendcommand("BARCODE " + 410 + "," + 15 + ", \"128\",40,0,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE
					.sendcommand("TEXT " + 410 + "," + 60 + ", \"1\",0,1,1,\"INGREDIENTS: chicken, Garlic, ckilly\"");
			TscLibDll.INSTANCE
					.sendcommand("TEXT " + 410 + "," + 75 + ", \"1\",0,1,1,\"Mfg & Pkgd:WHOLEGRAINS THE BAKERS HUB\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 410 + "," + 90 + ", \"1\",0,1,1,\"FSSAI NO 11315001000343\"");
			TscLibDll.INSTANCE.sendcommand(
					"TEXT " + 410 + "," + 105 + ", \"1\",0,1,1,\"For any complaints Pls Contact above addr\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 410 + "," + 115 + ", \"1\",0,1,1,\"Plot No 41 Kinfra park \"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 410 + "," + 130 + ", \"1\",0,1,1,\"Meenamkulam ph.9388343683 \"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 700 + "," + 130 + ", \"1\",0,1,1,\"MRP:" + Mrp + "\"");

			TscLibDll.INSTANCE
					.sendcommand("TEXT " + 410 + "," + 145 + ", \"1\",0,1,1,\"MFG DATE " + ManufactureDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT " + 410 + "," + 160 + ", \"1\",0,1,1,\"EXP DATE " + ExpiryDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT " + 610 + "," + 160 + ", \"1\",0,1,1,\"Batch:" + batch + "\"");

			if (ItemName.trim().length() > 17) {
				ItemName = ItemName.substring(0, 16);

			}
			// TscLibDll.INSTANCE.sendcommand("TEXT "+2+","+5+", \"2\",0,1,1,\"" + ItemName
			// + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+160+","+205+", \"3\",0,1,1,\"" + Mrp +
			// "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+500+","+375+", \"2\",90,1,1,\"" + Mrp
			// + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+150+","+375+", \"2\",90,1,1,\"" + Mrp
			// + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+barcodeNetWtX+","+barcodeNetWtY+",
			// \"2\",0,1,1,\"" + netWt + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+160+","+240+", \"2\",0,1,1,\"" + netWt
			// + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+475+","+375+", \"2\",90,1,1,\"" +
			// netWt + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT
			// "+manufacturingDateX+","+manufacturingDateY+", \"2\",0,1,1,\"" +
			// ManufactureDate + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+160+","+280+", \"2\",0,1,1,\"" +
			// ManufactureDate + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+450+","+375+", \"2\",90,1,1,\"" +
			// ManufactureDate + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+expiryDateX+","+expiryDateY+",
			// \"2\",0,1,1,\"" + ExpiryDate + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+160+","+310+", \"2\",0,1,1,\"" +
			// ExpiryDate + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+425+","+375+", \"2\",90,1,1,\"" +
			// ExpiryDate + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT "+barcodeBatchX+","+barcodeBatchY+",
			// \"2\",0,1,1,\"" + batch + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+160+","+350+", \"2\",0,1,1,\"" + batch
			// + "\"");
			// TscLibDll.INSTANCE.sendcommand("TEXT "+400+","+375+", \"2\",90,1,1,\"" +
			// batch + "\"");

		}
		TscLibDll.INSTANCE.sendcommand("PRINT " + NumberOfCopies);

		TscLibDll.INSTANCE.closeport();

	}

	private static void setBarcodeProperty(List<BarcodeFormatMst> body) {

		for (int i = 0; i < body.size(); i++) {
			if (body.get(i).getPropertyName().equalsIgnoreCase("NUTRITION_SIZE")) {
				SystemSetting.NUTRITION_SIZE = body.get(i).getPropertyValue();
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("NUTRITION_FORMAT")) {
				SystemSetting.NUTRITION_FORMAT = body.get(i).getPropertyValue();
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BARCODETITLE")) {
				SystemSetting.BARCODETITLE = body.get(i).getPropertyValue();
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BARCODE_FORMAT")) {
				SystemSetting.BARCODE_FORMAT = body.get(i).getPropertyValue();
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BARCODE_SIZE")) {
				SystemSetting.BARCODE_SIZE = body.get(i).getPropertyValue();
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("NUTRITIONTITLE")) {
				SystemSetting.NUTRITIONTITLE = body.get(i).getPropertyValue();
				SystemSetting.setNUTRITIONTITLE(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BARCODE_X")) {
				SystemSetting.barcodex = body.get(i).getPropertyValue();
				SystemSetting.setBarcodex(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BARCODE_Y")) {
				SystemSetting.barcodey = body.get(i).getPropertyValue();
				SystemSetting.setBarcodey(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("MFGDATE_X")) {
				SystemSetting.barcodeManufactureDatex = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeManufactureDatex(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("MFGDATE_Y")) {
				SystemSetting.barcodeManufactureDatey = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeManufactureDatey(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("EXPDATE_X")) {
				SystemSetting.barcodeExpiryDatex = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeExpiryDatex(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("EXPDATE_Y")) {
				SystemSetting.barcodeExpiryDatey = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeExpiryDatey(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("ITEMNAME_X")) {
				SystemSetting.barcodeItemNamex = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeItemNamex(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("ITEMNAME_Y")) {
				SystemSetting.barcodeItemNamey = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeItemNamey(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("INGLINE1_X")) {
				SystemSetting.barcodeIngLine1x = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeIngLine1x(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("INGLINE1_Y")) {
				SystemSetting.barcodeIngLine1y = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeIngLine1y(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("INGLINE2_X")) {
				SystemSetting.barcodeIngLine2x = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeIngLine2x(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("INGLINE2_Y")) {
				SystemSetting.barcodeIngLine2y = body.get(i).getPropertyValue();
				SystemSetting.setBarcodeIngLine2y(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("MRP_X")) {
				SystemSetting.barcodeMrpx = body.get(i).getPropertyValue();
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("MRP_Y")) {
				SystemSetting.barcodeMrpy = body.get(i).getPropertyValue();
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("NETWT_X")) {
				SystemSetting.setBarcodeNetWtx(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("NETWT_Y")) {
				SystemSetting.setBarcodeNetWty(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BATCH_X")) {
				SystemSetting.setBatchLinex(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BATCH_Y")) {
				SystemSetting.setBatchLiney(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("NUTRITIONTITLE_X")) {
				SystemSetting.setNutritionTitleX(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("NUTRITIONTITLE_Y")) {
				SystemSetting.setNutritionTitleY(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("NUTRITIONFACT_X")) {
				SystemSetting.setNutritionFactX(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("NUTRITIONFACT_Y")) {
				SystemSetting.setNutritionFactY(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BARCODETITLE_X")) {
				SystemSetting.setBarcodeTitleX(body.get(i).getPropertyValue());
			} else if (body.get(i).getPropertyName().equalsIgnoreCase("BARCODETITLE_Y")) {
				SystemSetting.setBarcodeTitleY(body.get(i).getPropertyValue());
			}

		}

	}

	public static void barcodeNutritionValeuCombained(String printerName, String itemName, String servingSize,
			String calories, String fat, List<NutritionValueDtl> dtlList, Integer NumberOfCopies,
			String ManufactureDate, String ExpiryDate, String Barcode, String Mrp, String IngLine1, String IngLine2,
			String batch) {

		TscLibDll.INSTANCE.openport(printerName);

		TscLibDll.INSTANCE.sendcommand("CLS");
		// TscLibDll.INSTANCE.clearbuffer();

		String BARCODE_SIZE = SystemSetting.BARCODE_SIZE;

		if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
			BARCODE_SIZE = "SIZE 3,1.5";
		}

		TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

		String BARCODE_FORMAT = SystemSetting.BARCODE_FORMAT;

//		 String BARCODE_X=SystemSetting.combinedbarcodex;
//         String BARCODE_Y=SystemSetting.combinedbarcodey;
//         String NAME_X= SystemSetting.combineditemnamex;
//         String NAME_Y=SystemSetting.combineditemnamey;
//         String MRP_X=SystemSetting.combinedmrpx;
//         String MRP_Y=SystemSetting.combinedmrpy;
//         String MFD_X=SystemSetting.combinedpkdx;
//         String MFD_Y=SystemSetting.combinedpkdy;
//         String EXP_X = SystemSetting.combinedexpx;
//         String EXP_Y = SystemSetting.combinedexpy;
//         String ING_LINE1_X= SystemSetting.combinedingline1x;
//         String ING_LINE1_Y= SystemSetting.combinedingline1y;
//         String ING_LINE2_X= SystemSetting.combinedingline2x;
//         String ING_LINE2_Y= SystemSetting.combinedingline2y;
//         String BATCH_X= SystemSetting.combinebatchx;
//         String BATCH_Y= SystemSetting.combinedbatchy;
//         String NUTRITION_INFORMATION_X = SystemSetting.combinednutritioninformx;
//         String NUTRITION_INFORMATION_Y = SystemSetting.combinednutritioninformy;
//         String SERVING_SIZE_X = SystemSetting.combinedservingsizex;
//         String SERVING_SIZE_Y = SystemSetting.combinedservingsizey;
//         String CALORIES_X = SystemSetting.combinedcaloriex;
//         String CALORIES_Y = SystemSetting.combinedcaloriey;  
//         String FAT_X = SystemSetting.combinedfatx;
//         String FAT_Y = SystemSetting.combinedfaty;
//         String NUTRITION_FACT = SystemSetting.nutritionfact;

		if (null == BARCODE_FORMAT || BARCODE_FORMAT.length() < 3) {
			BARCODE_SIZE = "FORMAT0";
		}

		TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
		TscLibDll.INSTANCE.sendcommand("CLS");
		TscLibDll.INSTANCE.sendcommand("BARCODE BARCODE_X,BARCODE_Y, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

		TscLibDll.INSTANCE.sendcommand("TEXT MFD_X,MFD_Y, \"2\",0,1,1,\"" + ManufactureDate + "\"");
		TscLibDll.INSTANCE.sendcommand("TEXT EXP_X,EXP_Y, \"2\",0,1,1,\"" + ExpiryDate + "\"");

		TscLibDll.INSTANCE.sendcommand("TEXT NAME_X,NAME_Y, \"2\",0,1,1,\"" + itemName + "\"");
		TscLibDll.INSTANCE.sendcommand("TEXT BATCH_X,BATCH_Y, \"2\",0,1,1,\"" + batch + "\"");

		if (IngLine1.length() > 0) {
			TscLibDll.INSTANCE.sendcommand("TEXT ING_LINE1_X,180, \"2\",0,1,1,\"" + IngLine1 + "\"");
		}

		if (IngLine2.length() > 0) {

			TscLibDll.INSTANCE.sendcommand("TEXT ING_LINE2_X,ING_LINE2_Y, \"2\",0,1,1,\"" + IngLine2 + "\"");
		}

		TscLibDll.INSTANCE.sendcommand("TEXT MRP_X,MRP_Y, \"3\",90,1,1,\"" + Mrp + "\"");

		System.out.println("Inside FORMAT088***************");

		TscLibDll.INSTANCE.sendcommand(
				"TEXT NUTRITION_INFORMATION_X,NUTRITION_INFORMATION_Y, \"2\",0,1,1,\" NUTRITION INFORMATION" + "\"");

		TscLibDll.INSTANCE
				.sendcommand("TEXT SERVING_SIZE_X,SERVING_SIZE_Y, \"2\",0,1,1,\" Serving size" + servingSize + "\"");
		TscLibDll.INSTANCE.sendcommand("TEXT combinedcaloriex,combinedcaloriey, \"2\",0,1,1,\" Calories " + calories
				+ " from Fat  " + fat + "\"");

		TscLibDll.INSTANCE.sendcommand("TEXT FAT_X,FAT_Y, \"2\",0,1,1,\" from Fat" + fat + "\"");

		int xValue = 70;

		if (dtlList.size() > 0) {

			for (NutritionValueDtl dtl : dtlList) {
				System.out.println("nutrition dtl" + dtl.getNutrition());
				xValue = xValue + 30;
				TscLibDll.INSTANCE
						.sendcommand("TEXT NUTRITION_FACT," + xValue + "  , \"2\",0,1,1,\"" + dtl.getNutrition().trim()
								+ " " + dtl.getValue().trim() + " " + dtl.getPercentage().trim() + "\"");

			}

		}

		TscLibDll.INSTANCE.sendcommand("PRINT " + NumberOfCopies);

		TscLibDll.INSTANCE.closeport();
		System.out.println("PRINTING COMPLETED***************");
	}

	public static void nutritionPrint(String printerName, String itemName, String servingSize, String calories,
			String fat, List<NutritionValueDtl> dtlList, int NumberOfCopies) {

		System.out.println("Nutrition" + dtlList.get(0).getNutrition());
		System.out.println("Percentage" + dtlList.get(0).getPercentage());
		System.out.println("Serial" + dtlList.get(0).getSerial());
		System.out.println("Value" + dtlList.get(0).getValue());
		// String NumberOfCopies = "5";
		//// String ManufactureDate = "Mfg Date: 01/08/2017" ;
		// String ExpiryDate = "Exp Date: 01/08/2017" ;
		// String ItemName = "Biscut 100 Gm";
		// String Barcode = "1234567890122";
		// String IngLine1 = "This line 1 ingredients";
		// String IngLine2 = "This line 2 ingredients";

		// String Mrp = " Rs 100 / No";
		System.out.print(
				"inside printing meathod printing started here >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..");

		TscLibDll.INSTANCE.openport(printerName);

		String NUTRITION_SIZE = SystemSetting.NUTRITION_SIZE;
		TscLibDll.INSTANCE.sendcommand("CLS");

		System.out.println("Nutrition Size" + NUTRITION_SIZE);

		String NUTRITION_FORMAT = SystemSetting.NUTRITION_FORMAT;

		if (NUTRITION_FORMAT.equalsIgnoreCase("FORMAT0")) {

			/*
			 * TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			 * TscLibDll.INSTANCE.sendcommand("CLS");
			 * 
			 * 
			 * // Second Barcode
			 * 
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 340,20, \"2\",0,1,1,\"" +
			 * SystemSetting.BARCODETITLE + "\"");
			 * 
			 * TscLibDll.INSTANCE.sendcommand("BARCODE 340,40, \"128\",50,3,0,2,2,\"" +
			 * Barcode + "\" ");
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 340,120, \"2\",0,1,1,\"" +
			 * ManufactureDate + "\"");
			 * TscLibDll.INSTANCE.sendcommand("TEXT 340,150, \"2\",0,1,1,\"" + ExpiryDate +
			 * "\"");
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 340,180, \"2\",0,1,1,\"" + ItemName +
			 * "\"");
			 * 
			 * 
			 * TscLibDll.INSTANCE.sendcommand("TEXT 600,40, \"3\",90,1,1,\"" + Mrp + "\"");
			 * 
			 * 
			 */

			System.out.println("Inside FORMAT088***************");

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			// TscLibDll.INSTANCE.sendcommand("BARCODE 300,10, \"128\",50,3,0,2,2,\"" +
			// itemName + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 250,20, \"2\",0,1,1,\" NUTRITION FACTS" + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 250,40, \"2\",0,1,1,\" Serving size" + servingSize + "\"");
			TscLibDll.INSTANCE
					.sendcommand("TEXT 250,70, \"2\",0,1,1,\" Calories " + calories + " from Fat  " + fat + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT 250,100, \"2\",0,1,1,\" from Fat" + fat
			// + "\"");

			int xValue = 70;

			if (dtlList.size() > 0) {

				for (NutritionValueDtl dtl : dtlList) {
					System.out.println("nutrition dtl" + dtl.getNutrition());
					xValue = xValue + 30;
					TscLibDll.INSTANCE
							.sendcommand("TEXT 250," + xValue + "  , \"2\",0,1,1,\"" + dtl.getNutrition().trim() + " "
									+ dtl.getValue().trim() + " " + dtl.getPercentage().trim() + "\"");

				}

			}

		} else if (NUTRITION_FORMAT.equalsIgnoreCase("FORMAT1")) {

			System.out.println("Inside Format1");
			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" + itemName + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + servingSize + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + calories + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + fat + "\"");

			if (dtlList.size() > 0) {

				for (NutritionValueDtl dtl : dtlList)
					TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" + dtl.getNutrition() + "\"");
			}

			if (dtlList.size() > 0) {

				for (NutritionValueDtl dtl : dtlList)
					TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + dtl.getValue() + "\"");
			}

			if (dtlList.size() > 0) {

				for (NutritionValueDtl dtl : dtlList) {
					TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + dtl.getPercentage() + "\"");
				}
			}

//		TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" + Mrp + "\"");

		} else if (NUTRITION_FORMAT.equalsIgnoreCase("FORMAT2")) {
			System.out.println("Inside Format2");
			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 300,10, \"128\",50,3,0,2,2,\"" + itemName + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 300,90, \"2\",0,1,1,\"" + servingSize + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 300,120, \"2\",0,1,1,\"" + calories + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 300,150, \"2\",0,1,1,\"" + fat + "\"");

			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)
					TscLibDll.INSTANCE.sendcommand("TEXT 300,180, \"2\",0,1,1,\"" + dtl.getNutrition() + "\"");
			}
			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)

					TscLibDll.INSTANCE.sendcommand("TEXT 300,210, \"2\",0,1,1,\"" + dtl.getValue() + "\"");
			}

			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)

					TscLibDll.INSTANCE.sendcommand("TEXT 300,210, \"2\",0,1,1,\"" + dtl.getPercentage() + "\"");
			}
//		TscLibDll.INSTANCE.sendcommand("TEXT 250,10, \"3\",90,1,1,\"" + Mrp + "\"");

		} else if (NUTRITION_FORMAT.equalsIgnoreCase("FORMAT3")) { // LEKSHMI

			System.out.println("Inside Format3");

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,10, \"2\",0,1,1,\"" + SystemSetting.BARCODETITLE + "\"");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,30, \"128\",50,3,0,2,2,\"" + itemName + "\" ");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,80, \"2\",0,1,1,\"" + " serving size" + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + servingSize + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + calories + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + fat + "\"");

			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)

					TscLibDll.INSTANCE
							.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" + "Ingredients:" + dtl.getNutrition() + "\"");
			}

			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)

					TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + dtl.getValue() + "\"");
			}

			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)

					TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + dtl.getPercentage() + "\"");
			}
//		TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",0,1,1,\"" + Mrp + "\"");

		} else if (NUTRITION_FORMAT.equalsIgnoreCase("FORMAT4")) {

			System.out.println("Inside Format4");
			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" + itemName + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + servingSize + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + calories + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + fat + "\"");

			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)

					TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" + dtl.getNutrition() + "\"");
			}

			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)

					TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + dtl.getPercentage() + "\"");
			}

			if (dtlList.size() > 0) {
				for (NutritionValueDtl dtl : dtlList)

					TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + dtl.getValue() + "\"");
			}
			// TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" + Mrp + "\"");

		} else if (NUTRITION_FORMAT.equalsIgnoreCase("FORMAT5")) { // NO INGREDIENTS

			System.out.println("Inside Format5");
			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" + itemName + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + servingSize + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + calories + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + fat + "\"");

//		TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" + Mrp + "\"");

		} else if (NUTRITION_FORMAT.equalsIgnoreCase("FORMAT6")) { // POURNAMI 2 SMALL STICKER IN ROW

			System.out.println("Inside Format6");
			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("TEXT 10,20, \"2\",0,1,1,\"" + SystemSetting.BARCODETITLE + "\"");

			TscLibDll.INSTANCE.sendcommand("BARCODE 10,40, \"128\",50,3,0,2,2,\"" + itemName + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 10,120, \"2\",0,1,1,\"" + servingSize + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 10,150, \"2\",0,1,1,\"" + calories + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 10,180, \"2\",0,1,1,\"" + fat + "\"");

///		TscLibDll.INSTANCE.sendcommand("TEXT 270,40, \"3\",90,1,1,\"" + Mrp + "\"");

			// Second Barcode

			TscLibDll.INSTANCE.sendcommand("TEXT 340,20, \"2\",0,1,1,\"" + SystemSetting.BARCODETITLE + "\"");

			TscLibDll.INSTANCE.sendcommand("BARCODE 340,40, \"128\",50,3,0,2,2,\"" + itemName + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 340,120, \"2\",0,1,1,\"" + servingSize + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 340,150, \"2\",0,1,1,\"" + calories + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 340,180, \"2\",0,1,1,\"" + fat + "\"");

			// TscLibDll.INSTANCE.sendcommand("TEXT 600,40, \"3\",90,1,1,\"" + Mrp + "\"");

		}

		TscLibDll.INSTANCE.sendcommand("PRINT " + NumberOfCopies);
		System.out.println("Exiting Format1");

		TscLibDll.INSTANCE.closeport();
		System.out.println("Closing port..........................");

	}

	public static void manualPrintLekshmi(String NumberOfCopies, String ManufactureDate, String ExpiryDate,
			String ItemName, String Barcode, String Mrp, String IngLine1, String IngLine2, String PrinterNAme) {

		// String NumberOfCopies = "5";
		//// String ManufactureDate = "Mfg Date: 01/08/2017" ;
		// String ExpiryDate = "Exp Date: 01/08/2017" ;
		// String ItemName = "Biscut 100 Gm";
		// String Barcode = "1234567890122";
		// String IngLine1 = "This line 1 ingredients";
		// String IngLine2 = "This line 2 ingredients";

		// String Mrp = " Rs 100 / No";

		TscLibDll.INSTANCE.openport(PrinterNAme);

		TscLibDll.INSTANCE.sendcommand("CLS");
		// TscLibDll.INSTANCE.clearbuffer();

		String BARCODE_SIZE = SystemSetting.BARCODE_SIZE;
		if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
			BARCODE_SIZE = "SIZE 3,1.5";
		}

		TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);

		String BARCODE_FORMAT = SystemSetting.BARCODE_FORMAT;
		String BARCODE_STARTX = SystemSetting.BARCODE_FORMAT;

		if (null == BARCODE_FORMAT || BARCODE_FORMAT.length() < 3) {
			BARCODE_SIZE = "FOMAT0";
		}

		if (BARCODE_SIZE.equalsIgnoreCase("FOMAT0")) {
			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 300,10, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 300,90, \"2\",0,1,1,\"" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 300,120, \"2\",0,1,1,\"" + ExpiryDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 300,150, \"2\",0,1,1,\"" + ItemName + "\"");

			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT 300,180, \"2\",0,1,1,\"" + IngLine1 + "\"");
			}

			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT 300,210, \"2\",0,1,1,\"" + IngLine2 + "\"");
			}

			TscLibDll.INSTANCE.sendcommand("TEXT 250,10, \"3\",90,1,1,\"" + Mrp + "\"");
		}

		if (BARCODE_SIZE.equalsIgnoreCase("LEKSHMI")) {

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,10, \"2\",0,1,1,\"" + SystemSetting.BARCODETITLE + "\"");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,30, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + ExpiryDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + ItemName + "\"");

			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" + "Ingredients:" + IngLine1 + "\"");
			}

			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + IngLine2 + "\"");
			}

			TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",0,1,1,\"" + Mrp + "\"");

		} else if (BARCODE_SIZE.equalsIgnoreCase("FOMAT1")) {

			TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
			TscLibDll.INSTANCE.sendcommand("CLS");
			TscLibDll.INSTANCE.sendcommand("BARCODE 100,40, \"128\",50,3,0,2,2,\"" + Barcode + "\" ");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,120, \"2\",0,1,1,\"" + ManufactureDate + "\"");
			TscLibDll.INSTANCE.sendcommand("TEXT 100,150, \"2\",0,1,1,\"" + ExpiryDate + "\"");

			TscLibDll.INSTANCE.sendcommand("TEXT 100,180, \"2\",0,1,1,\"" + ItemName + "\"");

			if (IngLine1.length() > 0) {
				TscLibDll.INSTANCE.sendcommand("TEXT 100,210, \"2\",0,1,1,\"" + IngLine1 + "\"");
			}

			if (IngLine2.length() > 0) {

				TscLibDll.INSTANCE.sendcommand("TEXT 100,240, \"2\",0,1,1,\"" + IngLine2 + "\"");
			}

			TscLibDll.INSTANCE.sendcommand("TEXT 100,270, \"3\",90,1,1,\"" + Mrp + "\"");

		}

		TscLibDll.INSTANCE.sendcommand("PRINT " + NumberOfCopies);

		TscLibDll.INSTANCE.closeport();

	}

	public static void caliberate(String PrinterNAme) {

		String BARCODE_SIZE = SystemSetting.BARCODE_SIZE;

		if (null == BARCODE_SIZE || BARCODE_SIZE.length() < 3) {
			BARCODE_SIZE = "SIZE 3,1.5";
		}

		// String Mrp = " Rs 100 / No";

		TscLibDll.INSTANCE.openport(PrinterNAme);

		TscLibDll.INSTANCE.sendcommand("CLS");
		// TscLibDll.INSTANCE.clearbuffer();
		TscLibDll.INSTANCE.sendcommand(BARCODE_SIZE);
		// TscLibDll.INSTANCE.sendcommand("SIZE 4,1");
		TscLibDll.INSTANCE.sendcommand("GAPDETECT");
		// TscLibDll.INSTANCE.sendcommand("SPEED 5");
		// TscLibDll.INSTANCE.sendcommand("DENSITY 8");
		// TscLibDll.INSTANCE.sendcommand("REFERENCE 0,0");
		// TscLibDll.INSTANCE.sendcommand("CLS");

		TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
		TscLibDll.INSTANCE.sendcommand("CLS");

		/*
		 * TscLibDll.INSTANCE.barcode("340", "50", "128", "50", "1", "0", "2", "2",
		 * "123456789");
		 * 
		 * TscLibDll.INSTANCE.windowsfont(340, 130, 28, 0, 0, 0, "arial", ItemName);
		 * 
		 * 
		 * 
		 * TscLibDll.INSTANCE.windowsfont(310, 170, 24, 0, 0, 0, "arial",
		 * ManufactureDate); TscLibDll.INSTANCE.windowsfont(310, 200, 24, 0, 0, 0,
		 * "arial", ExpiryDate); TscLibDll.INSTANCE.windowsfont(510, 180, 35, 0, 0, 0,
		 * "arial", Mrp);
		 * 
		 * 
		 * //TscLibDll.INSTANCE.printlabel("1", NumberOfCopies);
		 * TscLibDll.INSTANCE.clearbuffer();
		 */
		// TscLibDll.INSTANCE.sendcommand("KILL \"*\"");
		TscLibDll.INSTANCE.closeport();

	}
//itemName, servingSize, calories, fat,dtlList

	/*
	 * format2 TscLibDll.INSTANCE.sendcommand("DIRECTION 1");
	 * TscLibDll.INSTANCE.sendcommand("CLS");
	 * TscLibDll.INSTANCE.sendcommand("BARCODE 300,10, \"128\",50,3,0,2,2,\"" +
	 * Barcode + "\" ");
	 * 
	 * TscLibDll.INSTANCE.sendcommand("TEXT 300,90, \"2\",0,1,1,\"" +
	 * ManufactureDate + "\"");
	 * TscLibDll.INSTANCE.sendcommand("TEXT 300,120, \"2\",0,1,1,\"" + ExpiryDate +
	 * "\"");
	 * 
	 * TscLibDll.INSTANCE.sendcommand("TEXT 300,150, \"2\",0,1,1,\"" + ItemName +
	 * "\"");
	 * 
	 * if (IngLine1.length() > 0) {
	 * TscLibDll.INSTANCE.sendcommand("TEXT 300,180, \"2\",0,1,1,\"" + IngLine1 +
	 * "\"");
	 */

}

//TscLibDll INSTANCE = (TscLibDll) Native.loadLibrary ("D:\\TscLibDllTestInJave\\TSCLIB.dll", TscLibDll.class);