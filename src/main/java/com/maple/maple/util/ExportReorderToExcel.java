package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.B2bSalesReport;
import com.maple.report.entity.ReorderReport;

import javafx.application.HostServices;

public class ExportReorderToExcel {


	
	public void exportToExcel( String FileName, ArrayList<ReorderReport> aHM)  {


		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
	//	Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
		    cell.setCellValue("BRANCH NAME");
		    
		    
			cell = row.createCell(cellnum++);
			cell.setCellValue("BRANCH ADDRESS");
			
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("BRANCH PHONE");
			
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("SUPPLIER NAME");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("SUPPLIER ADDRESS");

			
			cell = row.createCell(cellnum++);
			cell.setCellValue("SUPPLIER PHONE");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("DATE");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("VOUCHER NUMBER");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("ITEM NAME");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("ITEM QTY");
			
			cell = row.createCell(cellnum++);

			cell.setCellValue("UNIT NAME");
			
			cell = row.createCell(cellnum++);

			cell.setCellValue("Minimum Qty");
			
			cell = row.createCell(cellnum++);

			cell.setCellValue("Offer Description");

		/*
		 * cell.setCellValue("TOTAL"); cell = row.createCell(cellnum++);
		 */
		
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
			System.out.println(aHM.size()+"list size issssssssssssssssssssssssssssssss");
			Iterator itr = aHM.iterator();
			while (itr.hasNext()) {
			
				row = sheet.createRow(rownum++);
				cellnum = 0;
				 LinkedHashMap element = (LinkedHashMap) itr.next();
				
				 Object branchName = (String) element.get("branchName");
				 Object branchAddress = (String) element.get("branchAddress");
				 Object branchPhone = (String) element.get("branchPhone");
				 
				 Object supplierName = (String) element.get("supplierName");
				 Object supplierAddress = (String) element.get("supplierAddress");
				 Object supplierPhn = (String) element.get("supplierPhn");
				 Object date = (String) element.get("date");
				 Object voucherNumber = (String) element.get("voucherNumber");
				 Object itemName = (String) element.get("itemName");
				 Object itemQty = (Double) element.get("itemQty");
				 Object unitName = (String) element.get("unitName");
				 Object minQty = (Double) element.get("minimumQty");
				 Object offerDesc = (String) element.get("offerDescription");
				
					cell = row.createCell(cellnum++);
				 cell.setCellValue((String)branchName);
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)branchAddress);
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)branchPhone);
				 
				 
				 
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)supplierName);
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)supplierAddress);
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)supplierPhn);
				 
				 
				 
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)date);
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)voucherNumber);
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)itemName);
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((Double)itemQty);
				 cell = row.createCell(cellnum++);
				 cell.setCellValue((String)unitName);
				
				 cell = row.createCell(cellnum++);
				 if(null != minQty )
				 {
				 cell.setCellValue((Double)minQty);
				 }
				 
				 cell = row.createCell(cellnum++);
				 if(null !=offerDesc  )
				 {
				 cell.setCellValue((String)offerDesc);
				 }
				
				
				 
				
			}

	} catch (Exception e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
				/*
				 * cell = row.createCell(cellnum++); cell.setCellValue((Double)total);
				 */
					
	
		
	
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	}
	

}
