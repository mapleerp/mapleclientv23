package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.AccountBalanceReport;

import javafx.application.HostServices;

public class SundaryDebtorBalanceToExcel {



	
	public void exportToExcel( String FileName, ArrayList<AccountBalanceReport> aHM )  {


		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Account Name");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Debit Amount");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Credit Amount");
			
			
			// Iterate aHM
	 	try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		System.out.print(aHM.size()+"list size issssssssssssssssssszzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
				
			for(int i =0;i<aHM.size();i++) {
				
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
					
;
					 Object accountName = aHM.get(i).getAccountHeads();
					 System.out.print(aHM.size()+"size isssssssssssssssssssssssssssssssssssssss");
					 Object debitAmount =aHM.get(i).getDebit();
					 Object creditAmount = aHM.get(i).getCredit();
					 
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)accountName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)debitAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)creditAmount);
				}

		
		  } catch (Exception e1) { // TODO Auto-generated catch block
		  e1.printStackTrace(); }
		 
	 	try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
	
		try {
		    FileOutputStream out = 
		            new FileOutputStream(new File(FileName));
		    workbook.write(out);
		    out.close();
		    System.out.println("Excel written successfully..");
		    
		    
			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(FileName);
			
			//Desktop desktop = Desktop.getDesktop();
			//File file = new File(FileName);
			//desktop.open(file);
			
		     
		} catch (FileNotFoundException e1) {
		   System.out.println(e1.toString());
		} catch (IOException e2) {
			 System.out.println(e2.toString());
		}
		
	}

}
