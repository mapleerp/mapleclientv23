package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.controllers.ReceiptModeWiseReport;

import javafx.application.HostServices;

public class ReceiptModeWiseDtlExport {

	public void exportToExcel( String FileName, ArrayList<ReceiptModeWiseReport> aHM )  {


		 
		

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Voucher Number");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Voucher Date");
			cell = row.createCell(cellnum++);
			cell.setCellValue("ReceiptMode");
			cell = row.createCell(cellnum++);

			
			

			cell.setCellValue("Receipt Amount");
			

			
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
				Iterator itr = aHM.iterator();
				while (itr.hasNext()) {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
					 LinkedHashMap element = (LinkedHashMap) itr.next();
					 System.out.println("accaccaccacc33"+element.get("id"));
					 Object voucherNumber = (String) element.get("voucherNumber");
					 Object voucherDate = (String) element.get("voucherDate");
					 Object receiptMode = (String) element.get("receiptMode");
					 
					 Object receiptAmount = (Double) element.get("receiptAmount");
				
				
					
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)voucherNumber);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)voucherDate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)receiptMode);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)receiptAmount);
					
					
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	}
}
