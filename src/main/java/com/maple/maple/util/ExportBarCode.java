package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.ItemMst;

import javafx.application.HostServices;

public class ExportBarCode {

	
	

	
	public void exportToExcel( String FileName, ItemMst itemmst,String Barcode,String ManufactureDate,String batch,LocalDate ldate ,String NumberOfCopies)  {


		 
		

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Item Name");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("BarCode");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Batch Code");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Best Before");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Manufacture Date");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Exp Date");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Qty");
			cell = row.createCell(cellnum++);

		
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			row = sheet.createRow(rownum++);
			cellnum = 0;
			
			
		
			java.util.Date uProdDate = SystemSetting.localToUtilDate(ldate);
			String sProdDate = SystemSetting.UtilDateToString(uProdDate, "dd-MM-yyyy");
			
//			String ManufactureDate =  sProdDate;
			LocalDate expdate = ldate.plusDays(itemmst.getBestBefore());
			java.util.Date uexpdate = SystemSetting.localToUtilDate(expdate);
			
			String bestBefore=String.valueOf(itemmst.getBestBefore());
			String ExpiryDate = SystemSetting.UtilDateToString(uexpdate, "dd-MM-yyyy");
			String ItemName = itemmst.getItemName();
		//	String Barcode = purchaseList.getBody().get(i).getBarcode()+"#"+purchaseList.getBody().get(i).getBatch();
		//	String NumberOfCopies = purchaseList.getBody().get(i).getQty().toString();
			String Mrp = itemmst.getStandardPrice().toString();
			String IngLine1 =  itemmst.getBarCodeLine1();
			String IngLine2 = itemmst.getBarCodeLine2();
			String PrinterNAme = SystemSetting.printer_name;
			String netWt = itemmst.getNetWeight();
	//		String batch =  purchaseList.getBody().get(i).getBatch();
		
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)ItemName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)Barcode);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)batch);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String) bestBefore);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)ManufactureDate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)ExpiryDate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)NumberOfCopies);
					 

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	
	try {
		workbook.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	}
	
}
