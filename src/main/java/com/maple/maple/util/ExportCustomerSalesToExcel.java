package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.TallyImportReport;

import javafx.application.HostServices;

public class ExportCustomerSalesToExcel {

	
	public void exportToExcel( String FileName, ArrayList<TallyImportReport> aHM )  {


		 
		

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Invoice Number");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Invoice Date");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Voucher Type");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Customer Name");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Address Line1");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Address Line2");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Gst Reg Type");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Party Type");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Gst Number");
			cell = row.createCell(cellnum++);

			cell.setCellValue("State");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Country");
			cell = row.createCell(cellnum++);

			cell.setCellValue("BankName");
			cell = row.createCell(cellnum++);

			cell.setCellValue("BankAccountNo");
			cell = row.createCell(cellnum++);

			cell.setCellValue("BankIfsc");
			cell = row.createCell(cellnum++);

			cell.setCellValue("DateOfSupply");
			cell = row.createCell(cellnum++);

			cell.setCellValue("VehicleNo");
			cell = row.createCell(cellnum++);

			cell.setCellValue("DriverName");
			cell = row.createCell(cellnum++);

			cell.setCellValue("ItemName");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Group");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Description");
			cell = row.createCell(cellnum++);

			cell.setCellValue("HsnCode");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Taxability");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Quantity");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Unit");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Uqc");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Rate");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Value");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Discount");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Taxable");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Tax");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Itemcgst");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Itemsgst");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Itemigst");
			cell = row.createCell(cellnum++);

			cell.setCellValue("KeralaFloodCess");
			cell = row.createCell(cellnum++);

			cell.setCellValue("RoundOff");
			cell = row.createCell(cellnum++);

			cell.setCellValue("NetValue");
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
				Iterator itr = aHM.iterator();
				while (itr.hasNext()) {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
					 LinkedHashMap element = (LinkedHashMap) itr.next();
					 System.out.println("accaccaccacc33"+element.get("id"));
					 Object voucherNumber = (String) element.get("voucherNumber");
					 Object invoiceDate = (String) element.get("invoiceDate");
					 Object voucherType = (String) element.get("voucherType");
					 
					 Object customerName = (String) element.get("customerName");
					 Object addressLine1 = (String) element.get("addressLine1");
					 Object addressLine2 = (String) element.get("addressLine2");
					 Object gstRegType = (String) element.get("gstRegType");
					 Object partyType = (String) element.get("partyType");
					 Object gstNumber = (String) element.get("gstNumber");
					 Object state = (String) element.get("state");
					 Object country = (String) element.get("country");
					 Object bankName = (String) element.get("bankName");
					 Object bankAccountName = (String) element.get("bankAccountName");
					 Object bankIfsc = (String) element.get("bankIfsc");
					 Object dateOfSupply = (String) element.get("dateOfSupply");
					 Object driverName = (String) element.get("driverName");
					 Object vehicleNo = (String) element.get("vehicleNo");
					 Object itemName = (String) element.get("itemName");
					 Object group = (String) element.get("group");
					 Object description = (String) element.get("description");
					 Object hsnCode = (String) element.get("hsncode");
					 Object taxable = (Double) element.get("taxable");
					 Object quantity = (Double) element.get("quantity");
					 Object unit = (String) element.get("unit");
					 Object uqc = (String) element.get("uqc");
					 Object rate = (Double) element.get("rate");
					 Object value = (Double) element.get("value");
					 Object discount = (Double) element.get("discount");
					 Object taxability = (String) element.get("taxability");
					 Object tax = (Double) element.get("tax");
					 Object itemcgst = (Double) element.get("itemcgst");
					 Object itemsgst = (Double) element.get("itemsgst");
					 Object itemigst = (Double) element.get("itemigst");
					 Object keralaFloodCess = (Double) element.get("keralaFloodCess");
					 Object roundOff = (Double) element.get("roundOff");
					 Object netValue = (Double) element.get("netValue");
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)voucherNumber);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)invoiceDate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)voucherType);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)customerName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)addressLine1);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)addressLine2);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)gstRegType);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)partyType);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)gstNumber);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)state);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)country);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)bankName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)bankAccountName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)bankIfsc);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)dateOfSupply);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)driverName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)vehicleNo);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)itemName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)group);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)description);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)hsnCode);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)taxability);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)quantity);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)unit);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)uqc); 
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)rate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)value);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)discount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)taxable);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)tax);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)itemcgst);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)itemsgst);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)itemigst);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)keralaFloodCess);
					 cell = row.createCell(cellnum++);
					 if(null != roundOff)
					 {
					 cell.setCellValue((Double)roundOff);
					 }
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)netValue);
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	}
	
}
