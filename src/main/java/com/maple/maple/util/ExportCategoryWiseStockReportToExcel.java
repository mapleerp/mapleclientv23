package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.report.entity.StockReport;
import com.maple.report.entity.TallyImportReport;

import javafx.application.HostServices;

public class ExportCategoryWiseStockReportToExcel {



	
	public void exportToExcel( String FileName, List<StockReport>stockReportList,String date )  {

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
	
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Item Name");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Qty");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Std Price");
			cell = row.createCell(cellnum++);


			cell.setCellValue("Category Name");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Branch Name");
			cell = row.createCell(cellnum++);

			

			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		

			for(int count=0; count< stockReportList.size(); count++)
			{
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
			
				
					 Object itemName = stockReportList.get(count).getItemName();
					 Object Qty = stockReportList.get(count).getQty();
					 Object standrdPrice = stockReportList.get(count).getStandardPrice();
					 
					 Object branchName = stockReportList.get(count).getBranchName();
					 Object categoryName=stockReportList.get(count).getCategory();
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)itemName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)Qty);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)standrdPrice);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)categoryName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)branchName);
					
				
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	 
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	}
	

	
}
