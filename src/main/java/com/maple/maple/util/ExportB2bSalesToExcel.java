package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.B2bSalesReport;

import javafx.application.HostServices;

public class ExportB2bSalesToExcel {


	
	public void exportToExcel( String FileName, List<B2bSalesReport> aHM)  {


		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("PARTY NAME");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("GSTIN");
			cell = row.createCell(cellnum++);
			cell.setCellValue("SALES MAN");
			cell = row.createCell(cellnum++);

			cell.setCellValue("SA BILLNO");
			cell = row.createCell(cellnum++);

			cell.setCellValue("ADVANCE");
			cell = row.createCell(cellnum++);

			cell.setCellValue("CASH");
			cell = row.createCell(cellnum++);

			cell.setCellValue("CREDIT");
			cell = row.createCell(cellnum++);

			cell.setCellValue("CARD PAYMENT");
			cell = row.createCell(cellnum++);

			cell.setCellValue("NET TOTAL");
			cell = row.createCell(cellnum++);

		/*
		 * cell.setCellValue("TOTAL"); cell = row.createCell(cellnum++);
		 */
		
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
			
			for(int count=0; count<aHM.size(); count++)
				 {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
				
					 Object partyName = aHM.get(count).getCustomerName();
					 Object gstin = aHM.get(count).getGstIn();
					 Object salesMan = aHM.get(count).getSalesMan();
					 
					 Object sabillno = aHM.get(count).getSaBillNo();
					 Object advance = aHM.get(count).getAdvance();
					 Object cash = aHM.get(count).getCash();
					 Object credit = aHM.get(count).getCredit();
					 Object cardPayment = aHM.get(count).getCardPayment();
					 Object netTotal = aHM.get(count).getNetTotal();
					 Object total=aHM.get(count).getTotal();
					 
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)partyName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)gstin);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)salesMan);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)sabillno);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)advance);
					 cell = row.createCell(cellnum++);
					 if(null!=aHM.get(count).getCash())
					 {
					 cell.setCellValue((Double)cash);
					 }
					 cell = row.createCell(cellnum++);
					 if(null!=aHM.get(count).getCredit()) {
					 cell.setCellValue((Double)credit);
					 }
					 cell = row.createCell(cellnum++);
					 if(null!=aHM.get(count).getCardPayment()) {
					 cell.setCellValue((Double)cardPayment);
					 }
					 cell = row.createCell(cellnum++);
					 if(null!=aHM.get(count).getNetTotal()) {
					 cell.setCellValue((Double)netTotal);
					 }
				/*
				 * cell = row.createCell(cellnum++); cell.setCellValue((Double)total);
				 */
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	
	try {
		workbook.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	}
	

}
