package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.HsnCodeSaleReport;

import javafx.application.HostServices;

public class ExportHsnWIseSalesToExcel {

	




	
	public void exportToExcel( String FileName, List<HsnCodeSaleReport>hsnCodeSalesReportList,String fdate,String tdate )  {

	 

		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 1;
		int cellnum = 0;
		 Row row0 = sheet.createRow(0);
		 Cell cell0 = row0.createCell(cellnum++);
		 cell0.setCellValue("From Date");
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Voucher Date");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Hsn Code");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Total Qty Sold");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Sgst");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Cgst");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Value");
			cell = row.createCell(cellnum++);
			
			
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
			for(int count=0; count<hsnCodeSalesReportList.size(); count++)
			{
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
			
				
					 Object VoucherDate = hsnCodeSalesReportList.get(count).getVoucherDate();
					 Object HsnCode = hsnCodeSalesReportList.get(count).getHsnCode();
					 Object TotalSoldQty = hsnCodeSalesReportList.get(count).getQty();
					 
					 Object Sgst = hsnCodeSalesReportList.get(count).getSgst();
					 
					 
					 Object Cgst = hsnCodeSalesReportList.get(count).getCgst();
					 
					 Object Value = hsnCodeSalesReportList.get(count).getValue();
					 row0.createCell(cellnum++);
					 cell0.setCellValue((String)fdate);
					 
						cell = row.createCell(cellnum++);
					 cell.setCellValue((Date)VoucherDate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)HsnCode);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)TotalSoldQty);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)Sgst);
					
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)Cgst);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)Value);
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	}
	


}
