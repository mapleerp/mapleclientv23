package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.HsnCodeSaleReport;
import com.maple.report.entity.HsnWisePurchaseDtlReport;

import javafx.application.HostServices;

public class ExportHsnWIsePurchaseDtlToExcel {

	




	
	public void exportToExcel( String FileName, List<HsnWisePurchaseDtlReport>hsnCodePurchaseReportList)  {

	 

		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 1;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
		 cell.setCellValue("Item Name");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("HSN");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Unit");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Qty");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Value");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("Rate");
			cell = row.createCell(cellnum++);
			
			cell.setCellValue("GST%");
			cell = row.createCell(cellnum++);
			
			
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
			for(int count=0; count<hsnCodePurchaseReportList.size(); count++)
			{
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
			
				
					 Object itemName = hsnCodePurchaseReportList.get(count).getItemName();
					 Object HsnCode = hsnCodePurchaseReportList.get(count).getHsnCode();
					 Object unit = hsnCodePurchaseReportList.get(count).getUnitName();
					 
					 Object Qty = hsnCodePurchaseReportList.get(count).getQty();
					 
					 
					 Object value = hsnCodePurchaseReportList.get(count).getValue();
					 
					 Object rate = hsnCodePurchaseReportList.get(count).getRate();
					 Object gst = hsnCodePurchaseReportList.get(count).getTaxRate();
					 
					

						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)itemName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)HsnCode);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)unit);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)Qty);
					
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)value);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)rate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)gst);
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	}
	


}
