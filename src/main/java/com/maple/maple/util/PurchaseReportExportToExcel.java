package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.report.entity.TallyImportReport;


import javafx.application.HostServices;

public class PurchaseReportExportToExcel {

	
	

	public void exportToExcel( String FileName, ArrayList<TallyImportReport> aHM )  {


		 

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
		
		 
			cell.setCellValue("Invoice Number");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Invoice Date");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Invoice Amount");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Supplier Name");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Supplier GST");
			
			cell = row.createCell(cellnum++);

			cell.setCellValue("Item Name");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Qty");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Unit");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Tax Rate");
			
			cell = row.createCell(cellnum++);

			cell.setCellValue("Tax Amount");
			
			
			cell = row.createCell(cellnum++);

			cell.setCellValue("Amount");
			cell = row.createCell(cellnum++);

			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
				Iterator itr = aHM.iterator();
				while (itr.hasNext()) {
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
					 LinkedHashMap element = (LinkedHashMap) itr.next();
					 System.out.println("accaccaccacc33"+element.get("id"));
					 Object voucherNumber = (String) element.get("voucherNumber");
					 Object invoiceDate = (String) element.get("voucherDate");
					 Object invoiceAmount = (Double) element.get("amountTotal");
					 Object supplierName = (String) element.get("supplierName");
					 Object supplierGST = (String) element.get("supplierGst");
					 
					 
					 Object itemName = (String) element.get("itemName");
					 Object qty = (Double) element.get("qty");
					 Object unit = (String) element.get("unit");
					 Object taxRate = (Double) element.get("taxRate");
					 Object taxAmnt = (Double) element.get("taxAmount");
					 
					 
					 Object amount = (Double) element.get("amount");
					 
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)voucherNumber);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)invoiceDate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)invoiceAmount);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)supplierName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)supplierGST);
					 				 
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)itemName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)qty);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)unit);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)taxRate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double) taxAmnt	);
							 
					 
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((Double)amount);
					
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	}
	
	
	
}
