package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.ItemMst;

import javafx.application.HostServices;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
 
import org.apache.poi.EncryptedDocumentException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
public class ExportBarcodeFromBarcodePrinting {

	
	


	
	public void exportToExcel( String FileName, String itemName,String Barcode,String ManufactureDate,String expDate,String batch,String qty,String mrp,String NumberOfCopies)  {


		 
		

		
		 
		
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Item Name");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("BarCode");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Batch Code");
			cell = row.createCell(cellnum++);

		//	cell.setCellValue("Best Before");
		//	cell = row.createCell(cellnum++);

			cell.setCellValue("Manufacture Date");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Exp Date");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Qty");
			cell = row.createCell(cellnum++);

		
			cell.setCellValue("MRP");
			cell = row.createCell(cellnum++);
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			row = sheet.createRow(rownum++);
			cellnum = 0;
			
			
		
	
	//		String batch =  purchaseList.getBody().get(i).getBatch();
		
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)itemName);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)Barcode);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)batch);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)ManufactureDate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)expDate);
				//	 cell = row.createCell(cellnum++);
				//	 cell.setCellValue((String)NumberOfCopies);
					 
					 
					 cell = row.createCell(cellnum++);
						 cell.setCellValue((String)qty);
					 
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)mrp);
					 cell = row.createCell(cellnum++);
					 

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	}
	
	
	
public void excelUpdate(String FileName, String itemName,String Barcode,String ManufactureDate,String expDate,String batch,String qty,String mrp,String NumberOfCopies) {

	 
	 

        String excelFilePath = FileName;
         
        try {
            FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
            Workbook workbook = WorkbookFactory.create(inputStream);
 
            Sheet sheet = workbook.getSheetAt(0);
 
		
 
            int rowCount = sheet.getLastRowNum();
 
   
                Row row = sheet.createRow(++rowCount);
 
                int columnCount = 0;
                
               
                Cell cell = row.createCell(columnCount);
                cell.setCellValue(itemName);
                 
                
                
                
       
                    cell = row.createCell(++columnCount);
                    cell.setCellValue((String) Barcode);
                    cell = row.createCell(++columnCount);
                    cell.setCellValue((String) batch);
                    cell = row.createCell(++columnCount);
                    cell.setCellValue((String) ManufactureDate);
                    cell = row.createCell(++columnCount);
                    cell.setCellValue((String) expDate);
                    cell = row.createCell(++columnCount);
                    cell.setCellValue((String) qty);
                    
                    cell = row.createCell(++columnCount);
                    cell.setCellValue((String) mrp);
                     
                    
 
   
 
            inputStream.close();
 
            FileOutputStream outputStream = new FileOutputStream(FileName);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
            
            System.out.print("excel updated !!!!!!!!!!!!!!!!!!!!!!!!");
             
            
            try {
        		workbook.close();
        	} catch (IOException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        	}
            
        } catch (Exception e) {
        	e.printStackTrace();
		}
    	
}

}
