package com.maple.maple.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.OrgReceiptReport;
import com.maple.report.entity.TallyImportReport;

import javafx.application.HostServices;

public class OrgReceiptExport {

	
	public void exportToExcel( String FileName, List<OrgReceiptReport> orgReceiptList)  {


		 
		

		
		 
		HSSFWorkbook workbook = new HSSFWorkbook();
	 
		  
		HSSFSheet sheet = workbook.createSheet("Ssql Result");
		 
		Map<String, Object[]> data = new HashMap<String, Object[]>();
		
		
		String ColList = "";
		int rownum = 0;
		int cellnum = 0;
		 Row row = sheet.createRow(rownum++);
		 Cell cell = row.createCell(cellnum++);
			 
			cell.setCellValue("Voucher Date");
			
			cell = row.createCell(cellnum++);
			cell.setCellValue("Voucher Number");
			cell = row.createCell(cellnum++);
			cell.setCellValue("Voucher Type");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Member");
			cell = row.createCell(cellnum++);

			cell.setCellValue("Amount");
			
			
			// Iterate aHM
		try {
		
//			Iterator itr = aHM.iterator();
//			
//			while (itr.hasNext()) {
//				 Object accountName = (String) element.get("accountName");
//			}
			
		
			for(int i=0;i<orgReceiptList.size();i++) {
				
				
					row = sheet.createRow(rownum++);
					cellnum = 0;
					
					// System.out.println("accaccaccacc33"+element.get("id"));
					 Object voucherDate =orgReceiptList.get(i).getVoucherDate();
					 Object VoucherNumber = orgReceiptList.get(i).getVoucherNumber();
					 Object voucherType =orgReceiptList.get(i).getVoucherType();
					 
					 Object memberNmae = orgReceiptList.get(i).getMemberNmae();
					 Object amount = orgReceiptList.get(i).getAmount();
					
					 String strAmount=String.valueOf(amount);
					 String strDate=String.valueOf(voucherDate);
						cell = row.createCell(cellnum++);
					 cell.setCellValue((String)strDate);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)VoucherNumber);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)voucherType);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)memberNmae);
					 cell = row.createCell(cellnum++);
					 cell.setCellValue((String)strAmount);
					 cell = row.createCell(cellnum++);
					
					
				}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
    		workbook.close();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
	
 				 
	try {
	    FileOutputStream out = 
	            new FileOutputStream(new File(FileName));
	    workbook.write(out);
	    out.close();
	    System.out.println("Excel written successfully..");
	    
	    
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(FileName);
		
		//Desktop desktop = Desktop.getDesktop();
		//File file = new File(FileName);
		//desktop.open(file);
		
	     
	} catch (FileNotFoundException e1) {
	   System.out.println(e1.toString());
	} catch (IOException e2) {
		 System.out.println(e2.toString());
	}
	
	

	
	
	
	}

	 
	
}
