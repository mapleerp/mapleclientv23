package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProcessMst {

	String id;
	String processName;
	String processDescription;
 	String branchCode;
 	private String  processInstanceId;
	private String taskId;
 	
	private String processType;

 	@JsonIgnore
 	private StringProperty processNameProperty;
	
	@JsonIgnore
	private StringProperty processDescriptionProperty;
	
	@JsonIgnore
 	private StringProperty processTypeProperty;
	

	@JsonProperty
  	public String getId() {
 
  		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getProcessDescription() {
		return processDescription;
	}
	public void setProcessDescription(String processDescription) {
		this.processDescription = processDescription;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public ProcessMst() {
		this.processNameProperty =new SimpleStringProperty("");
		this.processDescriptionProperty = new SimpleStringProperty("");
	}
	@JsonIgnore
	public StringProperty getprocessNameProperty() {
		processNameProperty.set(processName);
		return processNameProperty;
	}
	public void setprocessNameProperty(String processName) {
		this.processName = processName;
	}
	
	@JsonIgnore
	public StringProperty getprocessDescriptionProperty() {
		processDescriptionProperty.set(processDescription);
		return processDescriptionProperty;
	}
	public void setprocessDescriptionProperty(String processDescription) {
		this.processDescription = processDescription;
	}
	
	
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public StringProperty getProcessTypeProperty() {
		this.processTypeProperty =new SimpleStringProperty("");
		processTypeProperty.set(processType);
		return processTypeProperty;
	}
	public void setProcessTypeProperty(StringProperty processTypeProperty) {
		this.processTypeProperty = processTypeProperty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ProcessMst [id=" + id + ", processName=" + processName + ", processDescription=" + processDescription
				+ ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", processType=" + processType + "]";
	}

	
	
	
}
