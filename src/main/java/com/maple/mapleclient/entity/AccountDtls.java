package com.maple.mapleclient.entity;

public class AccountDtls {

	String accouName;
	Double amount;
	private String  processInstanceId;
	private String taskId;
	public String getAccouName() {
		return accouName;
	}
	public void setAccouName(String accouName) {
		this.accouName = accouName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "AccountDtls [accouName=" + accouName + ", amount=" + amount + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}

}
