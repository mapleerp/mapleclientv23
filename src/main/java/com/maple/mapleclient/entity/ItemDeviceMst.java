package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemDeviceMst{
	String id;
	String itemId;
	String deviceType;
	String branchCode;

	String itemName;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty deviceTypeproperty;
	
	public ItemDeviceMst() {
		this.itemNameProperty = new SimpleStringProperty();
		this.deviceTypeproperty = new SimpleStringProperty();
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}

	public StringProperty getDeviceTypeproperty() {
		deviceTypeproperty.set(deviceType);
		return deviceTypeproperty;
	}

	public void setDeviceTypeproperty(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ItemDeviceMst [id=" + id + ", itemId=" + itemId + ", deviceType=" + deviceType + ", branchCode="
				+ branchCode + ", itemName=" + itemName + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}



}
