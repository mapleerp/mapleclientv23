package com.maple.mapleclient.entity;





import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;

import org.apache.commons.lang.ObjectUtils.Null;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
 

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
 
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PaymentDtl {
	
	String id;
	String accountId;
	String account;
	private String creditAccountId;
	String memberId;
	Double amount;
    String remark;
    private String  processInstanceId;
	private String taskId;

//	@JsonIgnore
//	private	StringProperty id;
	@JsonIgnore
	private StringProperty accountProperty;

	@JsonIgnore
	private StringProperty modeOfPayment;
	@JsonIgnore
	private StringProperty remarkProperty;
	@JsonIgnore
	private DoubleProperty amountProperty;
	@JsonIgnore
	private StringProperty instrumentNumber;
	@JsonIgnore
	private StringProperty bankAccount;
	@JsonIgnore
	private StringProperty memberIdProperty;
	
	@JsonIgnore
	private StringProperty creditAccountIdProperty;
	@JsonIgnore
	 private   ObjectProperty<LocalDate> instrumentDate;
	
	@JsonIgnore
	 private   ObjectProperty<LocalDate> transDate;
	
//	@JsonProperty("instrumentDate")
//private java.sql.Date instrumentDate ;
	
	public ObjectProperty<LocalDate> getInstrumentDateProperty( ) {
		return instrumentDate;
	}
 
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setInstrumentDateProperty(ObjectProperty<LocalDate> instrumentDate) {
		this.instrumentDate = instrumentDate;
	}
 
		@JsonProperty("instrumentDate")
		public java.sql.Date getInstrumentDate( ) {
			if(null==this.instrumentDate.get() ) {
				return null;
			}else {
				return Date.valueOf(this.instrumentDate.get() );
			}
		 
			
		}
		
	   public void setInstrumentDate(java.sql.Date instrumentDate) {
							if(null!=instrumentDate)
			this.instrumentDate.set(instrumentDate.toLocalDate());
		}


	   public ObjectProperty<LocalDate> gettransDateProperty( ) {
			return transDate;
		}
	   

	 
		public void settransDatePropertyProperty(ObjectProperty<LocalDate> transDate) {
			this.transDate = transDate;
		}
	 
			@JsonProperty("transDate")
			public java.sql.Date getTransDate( ) {
				if(null==this.transDate.get() ) {
					return null;
				}else {
					return Date.valueOf(this.transDate.get() );
				}
			 
				
			}
			
		   public void setTransDate(java.sql.Date transDate) {
								if(null!=transDate)
				this.transDate.set(transDate.toLocalDate());
			}




	PaymentHdr paymenthdr;
	
	
	public PaymentHdr getPaymenthdr() {
		return paymenthdr;
	}
	public void setPaymenthdr(PaymentHdr paymenthdr) {
		this.paymenthdr = paymenthdr;
	}
	
	

	public PaymentDtl()
	{
		this.accountProperty=new SimpleStringProperty("");
		this.modeOfPayment=new SimpleStringProperty("");
		this.remarkProperty=new SimpleStringProperty("");
		this.amountProperty=new SimpleDoubleProperty();
		this.instrumentNumber=new SimpleStringProperty();
		this.bankAccount=new SimpleStringProperty("");
		this.instrumentDate= new SimpleObjectProperty<LocalDate>(LocalDate.now());
		this.instrumentDate= new SimpleObjectProperty<LocalDate>(null);
		this.transDate = new SimpleObjectProperty<LocalDate>(null);
		this.memberIdProperty=new SimpleStringProperty();
		 this.creditAccountIdProperty=new SimpleStringProperty("");
		
	}
	
	
	public StringProperty getMemberIdProperty() {
		memberIdProperty.set(memberId);
		return memberIdProperty;
	}

	public void setMemberIdProperty(StringProperty memberIdProperty) {
		this.memberIdProperty = memberIdProperty;
	}

	@JsonIgnore
	public StringProperty getAccountProperty() {
		accountProperty.set(accountId);
		
		return accountProperty;
	}

	public void setAccountProperty(String account) {
		
		this.account = account;
	}
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		accountProperty.set(account);
		this.account = account;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	@JsonIgnore
	public StringProperty getModeOfPaymentProperty() {
		return modeOfPayment;
	}
	public void setModeOfPaymentProperty(StringProperty modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	
	@JsonProperty("modeOfPayment")
	public String  getModeOfPayment() {
		return modeOfPayment.get();
	}
	public void setModeOfPayment(String  modeOfPayment) {
		this.modeOfPayment.set( modeOfPayment);
	}
	
	
	
	

	
	@JsonIgnore
	public StringProperty getInstrumentNumberProperty() {
		return instrumentNumber;
	}
	public void setInstrumentNumberProperty(StringProperty instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	
	
	@JsonProperty("instrumentNumber")
	public String getInstrumentNumber() {
		return instrumentNumber.get();
	}
	
	
	public void setInstrumentNumber(String  instrumentNumber) {
		this.instrumentNumber.set(instrumentNumber);
	}
	@JsonIgnore
	public StringProperty getBankAccountNumberProperty() {
		return bankAccount;
	}
	public void setBankAccountNumberProperty(StringProperty bankAccount) {
		this.bankAccount = bankAccount;
	}
	@JsonProperty("bankAccountNumber")
	public String getBankAccountNumber () {
		return bankAccount.get();
	}
	public void setBankAccountNumber (String  bankAccount) {
		this.bankAccount.set(bankAccount);
	}

	
	public String getCreditAccountId() {
		return creditAccountId;
	}

	public void setCreditAccountId(String creditAccountId) {
		this.creditAccountId = creditAccountId;
	}
	
	

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	

	public StringProperty getCreditAccountIdProperty() {
		creditAccountIdProperty.set(creditAccountId);
		
		return creditAccountIdProperty;
	}

	public void setCreditAccountIdProperty(StringProperty creditAccountIdProperty) {
		this.creditAccountIdProperty = creditAccountIdProperty;
	}
	
	



	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
    @JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	public StringProperty getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(StringProperty bankAccount) {
		this.bankAccount = bankAccount;
	}

	public void setAccountProperty(StringProperty accountProperty) {
		this.accountProperty = accountProperty;
	}

	public void setModeOfPayment(StringProperty modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}

	

	public void setInstrumentNumber(StringProperty instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
   @JsonIgnore
	public StringProperty getRemarkProperty() {
	remarkProperty.set(remark);
		return remarkProperty;
	}

	public void setRemarkProperty(StringProperty remarkProperty) {
		this.remarkProperty = remarkProperty;
	}

	




	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PaymentDtl [id=" + id + ", accountId=" + accountId + ", account=" + account + ", creditAccountId="
				+ creditAccountId + ", memberId=" + memberId + ", amount=" + amount + ", remark=" + remark
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", paymenthdr=" + paymenthdr
				+ "]";
	}

	


	
	
	
	

}
