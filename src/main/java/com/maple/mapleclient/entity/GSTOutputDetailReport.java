package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GSTOutputDetailReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String id;
	String invoiceDate;
	String invoiceNumber;
	String customerName;
	String customerTin;
	String branch;
	String branchActivity;
	Double totalSalesExcludingGst;
	Double gstAmount;
	Double excemptedSales;
	Double totalSales;
	String gstPercent;
	
	


	

	

	@JsonIgnore
	private StringProperty invoiceDateProperty;
	
	@JsonIgnore
	private StringProperty invoiceNumberProperty;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private StringProperty customerTinProperty;
	
	@JsonIgnore
	private StringProperty branchProperty;
	
	@JsonIgnore
	private StringProperty branchActivityProperty;
	
	@JsonIgnore
	private DoubleProperty totalSalesExcludingGstProperty;
	
	@JsonIgnore
	private DoubleProperty gstAmountProperty;
	
	@JsonIgnore
	private DoubleProperty excemptedSalesProperty;
	
	@JsonIgnore
	private DoubleProperty totalSalesProperty;
	
	

	@JsonIgnore
	private StringProperty gstPercentProperty;
	
	


	public GSTOutputDetailReport() {
		
		this.invoiceDateProperty = new SimpleStringProperty("");
		this.invoiceNumberProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.customerTinProperty = new SimpleStringProperty("");
		this.branchProperty = new SimpleStringProperty("");
		this.branchActivityProperty = new SimpleStringProperty("");
		this.totalSalesExcludingGstProperty = new SimpleDoubleProperty(0.0);
		this.gstAmountProperty = new SimpleDoubleProperty(0.0);
		this.excemptedSalesProperty = new SimpleDoubleProperty(0.0);
		this.totalSalesProperty = new SimpleDoubleProperty(0.0);
		this.gstPercentProperty=new SimpleStringProperty("");

	}
	
	@JsonIgnore
	public StringProperty getInvoiceDateProperty() {
		invoiceDateProperty.set(invoiceDate);
		return invoiceDateProperty;
	}

	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}
	
	@JsonIgnore
	public StringProperty getInvoiceNumberProperty() {
		invoiceNumberProperty.set(invoiceNumber);
		return invoiceNumberProperty;
	}

	public void setInvoiceNumberProperty(StringProperty invoiceNumberProperty) {
		this.invoiceNumberProperty = invoiceNumberProperty;
	}

	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}

	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}

	@JsonIgnore
	public StringProperty getCustomerTinProperty() {
		if(null != customerTin)
		{
			customerTinProperty.set(customerTin);

		}
		return customerTinProperty;
	}

	public void setCustomerTinProperty(StringProperty customerTinProperty) {
		this.customerTinProperty = customerTinProperty;
	}

	@JsonIgnore
	public StringProperty getBranchProperty() {
		if(null != branch)
		{
			branchProperty.set(branch);

		}
		return branchProperty;
	}

	public void setBranchProperty(StringProperty branchProperty) {
		this.branchProperty = branchProperty;
	}

	@JsonIgnore
	public StringProperty getBranchActivityProperty() {
		branchActivityProperty.set(branchActivity);
		return branchActivityProperty;
	}

	public void setBranchActivityProperty(StringProperty branchActivityProperty) {
		this.branchActivityProperty = branchActivityProperty;
	}

	@JsonIgnore
	public DoubleProperty getTotalSalesExcludingGstProperty() {
		if(null != totalSalesExcludingGst)
		{
			totalSalesExcludingGstProperty.set(totalSalesExcludingGst);

		}
		return totalSalesExcludingGstProperty;
	}

	public void setTotalSalesExcludingGstProperty(Double totalSalesExcludingGst) {
		this.totalSalesExcludingGst = totalSalesExcludingGst;
	}

	@JsonIgnore
	public DoubleProperty getGstAmountProperty() {
		if(null != gstAmount)
		{
			gstAmountProperty.set(gstAmount);

		}
		return gstAmountProperty;
	}

	public void setGstAmountProperty(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	@JsonIgnore
	public DoubleProperty getExcemptedSalesProperty() {
		if(null != excemptedSales)
		{
			excemptedSalesProperty.set(excemptedSales);

		}
		return excemptedSalesProperty;
	}

	public void setExcemptedSalesProperty(Double excemptedSales) {
		this.excemptedSales = excemptedSales;
	}

	@JsonIgnore
	public DoubleProperty getTotalSalesProperty() {
		if(null != totalSales)
		{
			totalSalesProperty.set(totalSales);

		}
		return totalSalesProperty;
	}

	public void setTotalSalesProperty(Double totalSales) {
		this.totalSales = totalSales;
	}
	
	@JsonIgnore
	public StringProperty getGstPercentProperty() {
		if(null != gstPercent)
		{
			gstPercentProperty.set(gstPercent);

		}
		return gstPercentProperty;
	}

	public void setGstPercentProperty(StringProperty gstPercentProperty) {
		this.gstPercentProperty = gstPercentProperty;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerTin() {
		return customerTin;
	}

	public void setCustomerTin(String customerTin) {
		this.customerTin = customerTin;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBranchActivity() {
		return branchActivity;
	}

	public void setBranchActivity(String branchActivity) {
		this.branchActivity = branchActivity;
	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getGstPercent() {
		return gstPercent;
	}

	public void setGstPercent(String gstPercent) {
		this.gstPercent = gstPercent;
	}

	
	
	
	
	public Double getTotalSalesExcludingGst() {
		return totalSalesExcludingGst;
	}

	public void setTotalSalesExcludingGst(Double totalSalesExcludingGst) {
		this.totalSalesExcludingGst = totalSalesExcludingGst;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getExcemptedSales() {
		return excemptedSales;
	}

	public void setExcemptedSales(Double excemptedSales) {
		this.excemptedSales = excemptedSales;
	}

	public Double getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(Double totalSales) {
		this.totalSales = totalSales;
	}

	public void setTotalSalesExcludingGstProperty(DoubleProperty totalSalesExcludingGstProperty) {
		this.totalSalesExcludingGstProperty = totalSalesExcludingGstProperty;
	}

	public void setGstAmountProperty(DoubleProperty gstAmountProperty) {
		this.gstAmountProperty = gstAmountProperty;
	}

	public void setExcemptedSalesProperty(DoubleProperty excemptedSalesProperty) {
		this.excemptedSalesProperty = excemptedSalesProperty;
	}

	public void setTotalSalesProperty(DoubleProperty totalSalesProperty) {
		this.totalSalesProperty = totalSalesProperty;
	}

	@Override
	public String toString() {
		return "GSTOutputDetailReport [id=" + id + ", invoiceDate=" + invoiceDate + ", invoiceNumber=" + invoiceNumber
				+ ", customerName=" + customerName + ", customerTin=" + customerTin + ", branch=" + branch
				+ ", branchActivity=" + branchActivity + ", totalSalesExcludingGst=" + totalSalesExcludingGst
				+ ", gstAmount=" + gstAmount + ", excemptedSales=" + excemptedSales + ", totalSales=" + totalSales
				+ ", gstPercent=" + gstPercent + ", invoiceDateProperty=" + invoiceDateProperty
				+ ", invoiceNumberProperty=" + invoiceNumberProperty + ", customerNameProperty=" + customerNameProperty
				+ ", customerTinProperty=" + customerTinProperty + ", branchProperty=" + branchProperty
				+ ", branchActivityProperty=" + branchActivityProperty + ", totalSalesExcludingGstProperty="
				+ totalSalesExcludingGstProperty + ", gstAmountProperty=" + gstAmountProperty
				+ ", excemptedSalesProperty=" + excemptedSalesProperty + ", totalSalesProperty=" + totalSalesProperty
				+ ", gstPercentProperty=" + gstPercentProperty + "]";
	}

	
	

}
