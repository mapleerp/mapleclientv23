package com.maple.mapleclient.entity;

/*
 * R.G
 * 
 * Entity Corrected on 5/6
 * 
 */


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class UnitMst 
{

	 
	private String id;
 
    private StringProperty unitName;
 
    private StringProperty unitDescription;
	 
	private DoubleProperty unitPrecision;
	
	String branchCode;
	
	private String  processInstanceId;
	private String taskId;
	public UnitMst( )
	{
		
		//this.id = new SimpleIntegerProperty(Id);
		this.unitName= new SimpleStringProperty("");
		this.unitDescription = new SimpleStringProperty("");
		this.unitPrecision = new SimpleDoubleProperty();
	}
	
	
	@JsonProperty("id")
	
	public String getId() {
		return id;
	}

	

	public void setId(String id) {
		this.id = id;
	}


	@JsonIgnore
	public StringProperty getUnitNameProperty() {
		return unitName;
	}
	
	


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public void setUnitNameProperty(StringProperty unitName) {
		this.unitName = unitName;
	}
	@JsonProperty("unitName")
	public String getUnitName() {
		return unitName.get();
	}
	public void setUnitName(String unitName) {
		this.unitName.set(unitName);
	}
	@JsonIgnore
	public StringProperty getUnitDescriptionProperty() {
		return unitDescription;
	}
	public void setUnitDescription(StringProperty unitDescription) {
		this.unitDescription = unitDescription;
	}
	@JsonProperty("unitDescription")
	public String getUnitDescription() {
		return unitDescription.get();
	}
	public void setUnitDescription(String unitDescription) {
		this.unitDescription.set(unitDescription);
	}
	@JsonIgnore
	public DoubleProperty getUnitPrecisionProperty() {
		return unitPrecision;
	}
	public void setUnitPrecisionProperty(DoubleProperty unitPrecision) {
		this.unitPrecision = unitPrecision;
	}
	@JsonProperty("unitPrecision")
	public Double getUnitPrecision() {
		return unitPrecision.get();
	}
	public void setUnitPrecision(Double unitPrecision) {
		this.unitPrecision.set(unitPrecision);
	}

	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "UnitMst [id=" + id + ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
	
	
}
