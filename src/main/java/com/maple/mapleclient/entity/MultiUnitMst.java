package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MultiUnitMst {
	String id;
	String itemId;
	String unit1;
	String unit2;
	Double qty1;
	Double qty2;
	String itemName;
	String barCode;
	Double price;
	@JsonIgnore
	String unit1Name;
	@JsonIgnore
	String unit2Name;
	
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty itemIdProperty;
	

	@JsonIgnore
	private StringProperty unit1Property;

	@JsonIgnore
	private StringProperty barCodeProperty;

	@JsonIgnore
	private StringProperty unit2Property;
	

	@JsonIgnore
	private DoubleProperty qty1Property;
	

	@JsonIgnore
	private DoubleProperty qty2Property;
	@JsonIgnore
	private DoubleProperty priceProperty;

	
	public MultiUnitMst() {
		this.itemIdProperty = new SimpleStringProperty();
		this.unit1Property = new SimpleStringProperty();
		this.unit2Property = new SimpleStringProperty();
		this.qty1Property = new SimpleDoubleProperty();
		this.qty2Property = new SimpleDoubleProperty();
		this.itemNameProperty = new SimpleStringProperty();
		this.barCodeProperty = new SimpleStringProperty();
		this.priceProperty = new SimpleDoubleProperty();
		this.price=0.0;
	}


	@JsonProperty

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getItemId() {
		return itemId;
	}


	public void setItemId(String itemId) {
		this.itemId = itemId;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


	public String getUnit1Name() {
		return unit1Name;
	}


	public void setUnit1Name(String unit1Name) {
		this.unit1Name = unit1Name;
	}


	public String getUnit2Name() {
		return unit2Name;
	}


	public void setUnit2Name(String unit2Name) {
		this.unit2Name = unit2Name;
	}


	public String getBarCode() {
		return barCode;
	}


	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}


	public String getUnit1() {
		return unit1;
	}


	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}


	public String getUnit2() {
		return unit2;
	}


	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}


	public Double getQty1() {
		return qty1;
	}


	public void setQty1(Double qty1) {
		this.qty1 = qty1;
	}


	public Double getQty2() {
		return qty2;
	}


	public void setQty2(Double qty2) {
		this.qty2 = qty2;
	}

	@JsonIgnore
	public StringProperty getitemIdProperty() {

		itemIdProperty.set(itemId);
		return itemIdProperty;
	}

	public void setitemIdProperty(String itemId) {
		itemId = itemId;
	}
	@JsonIgnore
	public StringProperty getitemNameProperty() {

		itemIdProperty.set(itemName);
		return itemIdProperty;
	}

	public void setitemNameProperty(String itemName) {
		itemName = itemName;
	}
	@JsonIgnore
	public StringProperty getbarcodeProperty() {

		barCodeProperty.set(barCode);
		return barCodeProperty;
	}

	public void setbarcodeProperty(String barCode) {
		this.barCode = barCode;
	}
	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	@JsonIgnore
	public StringProperty getunit1Property() {

		unit1Property.set(unit1Name);
		return unit1Property;
	}

	public void setunit1Property(String unit1Name) {
		unit1Name = unit1Name;
	}
	@JsonIgnore
	public StringProperty getunit2Property() {

		unit2Property.set(unit2Name);
		return unit2Property;
	}

	public void setunit2Property(String unit2Name) {
		unit2Name = unit2Name;
	}
	@JsonIgnore
	public DoubleProperty getpriceProperty() {

		if(null != price)
		{
		priceProperty.set(price);
		}
		return priceProperty;
	}

	public void setunit2Property(Double price) {
		this.price = price;
	}
	@JsonIgnore
	public DoubleProperty getqty1Property() {

		qty1Property.set(qty1);
		return qty1Property;
	}

	public void setqty1Property(Double qty1) {
		qty1 = qty1;
	}
	@JsonIgnore
	public DoubleProperty getqty2Property() {

		qty2Property.set(qty2);
		return qty2Property;
	}

	public void setqty2Property(Double qty2) {
		qty2 = qty2;
	}

	

	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}





	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "MultiUnitMst [id=" + id + ", itemId=" + itemId + ", unit1=" + unit1 + ", unit2=" + unit2 + ", qty1="
				+ qty1 + ", qty2=" + qty2 + ", itemName=" + itemName + ", barCode=" + barCode + ", price=" + price
				+ ", unit1Name=" + unit1Name + ", unit2Name=" + unit2Name + ", branchCode=" + branchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	

}
