package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
public class AcceptStock {

	Integer id;
	
	@JsonProperty("userId")
	Integer userId;
	@JsonProperty("intent")
	private Integer intent;
	
	@JsonProperty("branch")
	private String branch;
	
	private String senderVoucherId;
	
	@JsonProperty("recId")
	private String recId;
	
	@JsonProperty("voucherNumber")
	private Integer voucherNumber;
	
	@JsonProperty("intentDate")
	private String intentDate;

	@JsonProperty("pendingStatus")
	private String pendingStatus;
	private String  processInstanceId;
	private String taskId;

	
	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public String getPendingStatus() {
		return pendingStatus;
	}



	public void setPendingStatus(String pendingStatus) {
		this.pendingStatus = pendingStatus;
	}



	public Integer getId() {
		return id;
	}
	
	

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getIntent() {
		return intent;
	}

	public void setIntent(Integer intent) {
		this.intent = intent;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getRecId() {
		return recId;
	}

	public void setRecId(String recId) {
		this.recId = recId;
	}

	public Integer getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(Integer voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getIntentDate() {
		return intentDate;
	}

	public void setIntentDate(String intentDate) {
		this.intentDate = intentDate;
	}



	public String getSenderVoucherId() {
		return senderVoucherId;
	}



	public void setSenderVoucherId(String senderVoucherId) {
		this.senderVoucherId = senderVoucherId;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	@Override
	public String toString() {
		return "AcceptStock [id=" + id + ", userId=" + userId + ", intent=" + intent + ", branch=" + branch
				+ ", senderVoucherId=" + senderVoucherId + ", recId=" + recId + ", voucherNumber=" + voucherNumber
				+ ", intentDate=" + intentDate + ", pendingStatus=" + pendingStatus + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}





	
	
	

}
