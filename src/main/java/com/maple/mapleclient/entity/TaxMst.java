package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TaxMst 

{

	String Id;
	String taxId;
	
	String  itemId;
    Date  startDate;
    Date  endDate;
    Double  taxRate;
    
    String itemName;
    String unitName;
    private String  processInstanceId;
	private String taskId;
    
    @JsonIgnore
    private DoubleProperty taxRateProperty;
    
    @JsonIgnore
    private StringProperty taxIdProperty;
    
    @JsonIgnore
    private StringProperty itemNameProperty;
    
    @JsonIgnore
    private StringProperty unitProperty;
    
    @JsonIgnore
    private StringProperty startDateProperty;
    
    @JsonIgnore
    private StringProperty endDateProperty;
    
    public TaxMst() {
		this.taxIdProperty = new SimpleStringProperty();
		this.itemNameProperty = new SimpleStringProperty();
		this.unitProperty = new SimpleStringProperty();
		this.taxRateProperty = new SimpleDoubleProperty();
		this.startDateProperty = new SimpleStringProperty("");
		this.endDateProperty = new SimpleStringProperty("");
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
    
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	
	@JsonIgnore
	public StringProperty getItemNameProperty() {

		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	

	@JsonIgnore
	public StringProperty getstartDateProperty() {

		if(null != startDate)
		{
		startDateProperty.set(SystemSetting.UtilDateToString(startDate));
		
		}
		return startDateProperty;
	}

	public void setstartDateProperty(Date startDate) {
		this.startDate = startDate;
	}
	
	@JsonIgnore
	public StringProperty getendDateProperty() {
		if(null != endDate)
		{
		endDateProperty.set(SystemSetting.UtilDateToString(endDate));
		}
		return endDateProperty;
	}

	public void setendDateProperty(Date endDate) {
		this.endDate = endDate;
	}
	
	@JsonIgnore
	public StringProperty gettaxIdProperty() {

		taxIdProperty.set(taxId);
		return taxIdProperty;
	}

	public void settaxIdProperty(String taxId) {
		this.taxId = taxId;
	}

	
	@JsonIgnore
	public StringProperty getunitProperty() {

		unitProperty.set(unitName);
		return unitProperty;
	}

	public void setunitProperty(String unitName) {
		this.unitName = unitName;
	}

	
	@JsonIgnore
	public DoubleProperty gettaxRateProperty() {

		taxRateProperty.set(taxRate);
		return taxRateProperty;
	}

	public void settaxRateProperty(Double taxRate) {
		this.taxRate = taxRate;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "TaxMst [Id=" + Id + ", taxId=" + taxId + ", itemId=" + itemId + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", taxRate=" + taxRate + ", itemName=" + itemName + ", unitName=" + unitName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	

}
