package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OpeningBalanceConfigMst {
	private String id;

	String accountId;

	String accountName;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty accountNameProperty;
	
	public OpeningBalanceConfigMst() {
		

		this.accountNameProperty = new SimpleStringProperty();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	
	public StringProperty getaccountNameProperty() {
		accountNameProperty.set(accountName);
		return accountNameProperty;
	}

	public void setaccountNameProperty(String accountName) {
		this.accountName = accountName;
	}

	
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "OpeningBalanceConfigMst [id=" + id + ", accountId=" + accountId + ", accountName=" + accountName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
