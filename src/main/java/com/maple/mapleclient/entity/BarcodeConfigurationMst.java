package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BarcodeConfigurationMst  implements Serializable{

	private static final long serialVersionUID = 1L;
	  private String id;
		
      String branchCode;
	
    	String barcodeX;
	    String barcodeY;
       String manufactureDateX;
       String manufactureDateY;
       String expiryDateX;
       String expiryDateY;
       String itemNameX;
       String itemNameY;
       String ingLine1X;
       String ingLine1Y;
       String ingLine2X;
       String ingLine2Y;
       String mrpX;
       String mrpy;
	
     CompanyMst companyMst;
     
     private String  processInstanceId;
 	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBarcodeX() {
		return barcodeX;
	}

	public void setBarcodeX(String barcodeX) {
		this.barcodeX = barcodeX;
	}

	public String getBarcodeY() {
		return barcodeY;
	}

	public void setBarcodeY(String barcodeY) {
		this.barcodeY = barcodeY;
	}

	public String getManufactureDateX() {
		return manufactureDateX;
	}

	public void setManufactureDateX(String manufactureDateX) {
		this.manufactureDateX = manufactureDateX;
	}

	public String getManufactureDateY() {
		return manufactureDateY;
	}

	public void setManufactureDateY(String manufactureDateY) {
		this.manufactureDateY = manufactureDateY;
	}

	public String getExpiryDateX() {
		return expiryDateX;
	}

	public void setExpiryDateX(String expiryDateX) {
		this.expiryDateX = expiryDateX;
	}

	public String getExpiryDateY() {
		return expiryDateY;
	}

	public void setExpiryDateY(String expiryDateY) {
		this.expiryDateY = expiryDateY;
	}

	public String getItemNameX() {
		return itemNameX;
	}

	public void setItemNameX(String itemNameX) {
		this.itemNameX = itemNameX;
	}

	public String getItemNameY() {
		return itemNameY;
	}

	public void setItemNameY(String itemNameY) {
		this.itemNameY = itemNameY;
	}

	public String getIngLine1X() {
		return ingLine1X;
	}

	public void setIngLine1X(String ingLine1X) {
		this.ingLine1X = ingLine1X;
	}

	public String getIngLine1Y() {
		return ingLine1Y;
	}

	public void setIngLine1Y(String ingLine1Y) {
		this.ingLine1Y = ingLine1Y;
	}

	public String getIngLine2X() {
		return ingLine2X;
	}

	public void setIngLine2X(String ingLine2X) {
		this.ingLine2X = ingLine2X;
	}

	public String getIngLine2Y() {
		return ingLine2Y;
	}

	public void setIngLine2Y(String ingLine2Y) {
		this.ingLine2Y = ingLine2Y;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMrpX() {
		return mrpX;
	}

	public void setMrpX(String mrpX) {
		this.mrpX = mrpX;
	}

	public String getMrpy() {
		return mrpy;
	}

	public void setMrpy(String mrpy) {
		this.mrpy = mrpy;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "BarcodeConfigurationMst [id=" + id + ", branchCode=" + branchCode + ", barcodeX=" + barcodeX
				+ ", barcodeY=" + barcodeY + ", manufactureDateX=" + manufactureDateX + ", manufactureDateY="
				+ manufactureDateY + ", expiryDateX=" + expiryDateX + ", expiryDateY=" + expiryDateY + ", itemNameX="
				+ itemNameX + ", itemNameY=" + itemNameY + ", ingLine1X=" + ingLine1X + ", ingLine1Y=" + ingLine1Y
				+ ", ingLine2X=" + ingLine2X + ", ingLine2Y=" + ingLine2Y + ", mrpX=" + mrpX + ", mrpy=" + mrpy
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
     
	/*
	 * @JsonIgnore private StringProperty barcodeProperty;
	 * 
	 * @JsonIgnore private StringProperty barcodeXProperty;
	 * 
	 * @JsonIgnore private StringProperty manufactureDateXProperty;
	 * 
	 * @JsonIgnore private StringProperty manufactureDateYProperty;
	 * 
	 * @JsonIgnore private StringProperty itemNameXProperty;
	 * 
	 * 
	 * @JsonIgnore private StringProperty itemNameYProperty;
	 * 
	 * 
	 * @JsonIgnore private StringProperty ingLine1XProperty;
	 * 
	 * @JsonIgnore private StringProperty ingLine1YProperty;
	 * 
	 * @JsonIgnore private StringProperty expiryDateXProperty;
	 * 
	 * 
	 * 
	 * @JsonIgnore private StringProperty ingLine2XProperty;
	 * 
	 * @JsonIgnore private StringProperty iingLine2YProperty;
	 * 
	 * 
	 * 
	 * @JsonIgnore private StringProperty expiryDateYProperty;
	 * 
	 * public BarcodeConfigurationMst() {
	 * 
	 * this.barcodeProperty = new SimpleStringProperty(); this.barcodeXProperty =
	 * new SimpleStringProperty(); this.manufactureDateXProperty = new
	 * SimpleStringProperty(); this.manufactureDateYProperty=new
	 * SimpleStringProperty(); this.itemNameXProperty=new SimpleStringProperty();
	 * this.itemNameYProperty = new SimpleStringProperty(); this.ingLine1XProperty =
	 * new SimpleStringProperty(); this.ingLine1YProperty = new
	 * SimpleStringProperty(); this.expiryDateYProperty=new SimpleStringProperty();
	 * this.iingLine2YProperty=new SimpleStringProperty();
	 * this.expiryDateXProperty=new SimpleStringProperty();
	 * 
	 * 
	 * }
	 * 
	 * 
	 * public String getBranchCode() { return branchCode; } public void
	 * setBranchCode(String branchCode) { this.branchCode = branchCode; } public
	 * String getBarcodeX() { return barcodeX; } public void setBarcodeX(String
	 * barcodeX) { this.barcodeX = barcodeX; } public String getBarcodeY() { return
	 * barcodeY; } public void setBarcodeY(String barcodeY) { this.barcodeY =
	 * barcodeY; } public String getManufactureDateX() { return manufactureDateX; }
	 * public void setManufactureDateX(String manufactureDateX) {
	 * this.manufactureDateX = manufactureDateX; } public String
	 * getManufactureDateY() { return manufactureDateY; } public void
	 * setManufactureDateY(String manufactureDateY) { this.manufactureDateY =
	 * manufactureDateY; } public String getExpiryDateX() { return expiryDateX; }
	 * public void setExpiryDateX(String expiryDateX) { this.expiryDateX =
	 * expiryDateX; } public String getExpiryDateY() { return expiryDateY; } public
	 * void setExpiryDateY(String expiryDateY) { this.expiryDateY = expiryDateY; }
	 * public String getItemNameX() { return itemNameX; } public void
	 * setItemNameX(Strin   String mrpX;
        String mrpy;g itemNameX) { this.itemNameX = itemNameX; } public String
	 * getItemNameY() { return itemNameY; } public void setItemNameY(String
	 * itemNameY) { this.itemNameY = itemNameY; } public String getIngLine1X() {
	 * return ingLine1X; } public void setIngLine1X(String ingLine1X) {
	 * this.ingLine1X = ingLine1X; } public String getIngLine1Y() { return
	 * ingLine1Y; } public void setIngLine1Y(String ingLine1Y) { this.ingLine1Y =
	 * ingLine1Y; } public String getIngLine2X() { return ingLine2X; } public void
	 * setIngLine2X(String ingLine2X) { this.ingLine2X = ingLine2X; } public String
	 * getIngLine2Y() { return ingLine2Y; } public void setIngLine2Y(String
	 * ingLine2Y) { this.ingLine2Y = ingLine2Y; } public static long
	 * getSerialversionuid() { return serialVersionUID; } public String getId() {
	 * return id; } public void setId(String id) { this.id = id; }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @JsonIgnore public StringProperty getBarcodeXProperty() {
	 * 
	 * barcodeXProperty.set(barcodeX); return barcodeXProperty; }
	 * 
	 * 
	 * public void setBarcodeXProperty(StringProperty barcodeXProperty) {
	 * this.barcodeXProperty = barcodeXProperty; }
	 * 
	 * @JsonIgnore public StringProperty getManufactureDateXProperty() {
	 * manufactureDateXProperty.set(manufactureDateX); return
	 * manufactureDateXProperty; }
	 * 
	 * 
	 * public void setManufactureDateXProperty(StringProperty
	 * manufactureDateXProperty) { this.manufactureDateXProperty =
	 * manufactureDateXProperty; }
	 * 
	 * @JsonIgnore public StringProperty getManufactureDateYProperty() {
	 * manufactureDateYProperty.set(manufactureDateY); return
	 * manufactureDateYProperty; }
	 * 
	 * 
	 * public void setManufactureDateYProperty(StringProperty
	 * manufactureDateYProperty) { this.manufactureDateYProperty =
	 * manufactureDateYProperty; }
	 * 
	 * @JsonIgnore public StringProperty getItemNameXProperty() {
	 * 
	 * itemNameXProperty.set(itemNameX); return itemNameXProperty; }
	 * 
	 * 
	 * public void setItemNameXProperty(StringProperty itemNameXProperty) {
	 * this.itemNameXProperty = itemNameXProperty; }
	 * 
	 * 
	 * @JsonIgnore public StringProperty getItemNameYProperty() {
	 * 
	 * itemNameYProperty.set(itemNameY); return itemNameYProperty; }
	 * 
	 * 
	 * public void setItemNameYProperty(StringProperty itemNameYProperty) {
	 * this.itemNameYProperty = itemNameYProperty; }
	 * 
	 * @JsonIgnore public StringProperty getIngLine1XProperty() {
	 * ingLine1XProperty.set(ingLine1X); return ingLine1XProperty; }
	 * 
	 * 
	 * public void setIngLine1XProperty(StringProperty ingLine1XProperty) {
	 * this.ingLine1XProperty = ingLine1XProperty; }
	 * 
	 * 
	 * public StringProperty getIngLine1YProperty() { return ingLine1YProperty; }
	 * 
	 * 
	 * public void setIngLine1YProperty(StringProperty ingLine1YProperty) {
	 * this.ingLine1YProperty = ingLine1YProperty; }
	 * 
	 * @JsonIgnore public StringProperty getExpiryDateXProperty() {
	 * expiryDateXProperty.set(expiryDateX);
	 * 
	 * return expiryDateXProperty; }
	 * 
	 * 
	 * public void setExpiryDateXProperty(StringProperty expiryDateXProperty) {
	 * this.expiryDateXProperty = expiryDateXProperty; }
	 * 
	 * @JsonIgnore public StringProperty getIngLine2XProperty() {
	 * 
	 * ingLine2XProperty.set(ingLine2X); return ingLine2XProperty; }
	 * 
	 * 
	 * public void setIngLine2XProperty(StringProperty ingLine2XProperty) {
	 * this.ingLine2XProperty = ingLine2XProperty; }
	 * 
	 * 
	 * public StringProperty getIingLine2YProperty() { return iingLine2YProperty; }
	 * 
	 * 
	 * public void setIingLine2YProperty(StringProperty iingLine2YProperty) {
	 * this.iingLine2YProperty = iingLine2YProperty; }
	 * 
	 * @JsonIgnore public StringProperty getExpiryDateYProperty() {
	 * expiryDateYProperty.set(expiryDateY);
	 * 
	 * return expiryDateYProperty; }
	 * 
	 * 
	 * 
	 * 
	 * public void setExpiryDateYProperty(StringProperty expiryDateYProperty) {
	 * this.expiryDateYProperty = expiryDateYProperty; }
	 * 
	 * @JsonIgnore public StringProperty getBarcodeProperty() {
	 * 
	 * barcodeProperty.set(branchCode); return barcodeProperty; }
	 * 
	 * 
	 * public void setBarcodeProperty(StringProperty barcodeProperty) {
	 * this.barcodeProperty = barcodeProperty; }
	 * 
	 * 
	 * public CompanyMst getCompanyMst() { return companyMst; }
	 * 
	 * 
	 * public void setCompanyMst(CompanyMst companyMst) { this.companyMst =
	 * companyMst; }
	 * 
	 * 
	 */



	
	
     
}
