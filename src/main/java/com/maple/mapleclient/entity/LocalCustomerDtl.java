package com.maple.mapleclient.entity;

 
import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LocalCustomerDtl implements Serializable  {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String name;
	private String relation;
	private Date dateOfBirth; 
	
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	
	
	public LocalCustomerDtl() {
		this.nameProperty = new SimpleStringProperty("");
		this.relationProperty = new SimpleStringProperty("");
		this.dobProperty = new SimpleStringProperty("");

	}

	LocalCustomerMst localCustomerMst;
	
	@JsonIgnore
	private StringProperty nameProperty;
	
	@JsonIgnore
	private StringProperty relationProperty;

	@JsonIgnore
	private StringProperty dobProperty;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	
	public LocalCustomerMst getLocalCustomerMst() {
		return localCustomerMst;
	}

	public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}
	
	

	public StringProperty getNameProperty() {
		nameProperty.set(name);
		return nameProperty;
	}

	public void setNameProperty(StringProperty nameProperty) {
		this.nameProperty = nameProperty;
	}

	public StringProperty getRelationProperty() {
		relationProperty.set(relation);
		return relationProperty;
	}

	public void setRelationProperty(StringProperty relationProperty) {
		this.relationProperty = relationProperty;
	}

	public StringProperty getDobProperty() {
		
		if(null != dateOfBirth)
		{
			dobProperty.set(SystemSetting.UtilDateToString(dateOfBirth, "yyyy-MM-dd"));
		}
		return dobProperty;
	}

	public void setDobProperty(StringProperty dobProperty) {
		this.dobProperty = dobProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "LocalCustomerDtl [id=" + id + ", name=" + name + ", relation=" + relation + ", dateOfBirth="
				+ dateOfBirth + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", localCustomerMst=" + localCustomerMst + "]";
	}

	
	

	
}
