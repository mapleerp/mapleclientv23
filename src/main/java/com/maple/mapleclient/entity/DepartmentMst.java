package com.maple.mapleclient.entity;

public class DepartmentMst {

	
	String id;
	String deleted;
	String userId;
	String departmentName;
	String processInstanceId;
	String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "DepartmentMst [id=" + id + ", deleted=" + deleted + ", userId=" + userId + ", departmentName="
				+ departmentName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
}
