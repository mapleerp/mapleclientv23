package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MixCategoryMst implements Serializable {
	private static final long serialVersionUID = 1L;
   String id;
	String mixName;

	String mechine;

	String mechineId;
	String branchCode;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty subCateNameProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	
	@JsonIgnore
	private StringProperty mechineProperty;
	
	
	
	public MixCategoryMst() {
		this.subCateNameProperty=new SimpleStringProperty("");
		this.itemNameProperty=new SimpleStringProperty("");
		this.mechineProperty=new SimpleStringProperty("");
	}


	public String getMechine() {
		return mechine;
	}
	public void setMechine(String mechine) {
		this.mechine = mechine;
	}



	public void setSubCateNameProperty(StringProperty subCateNameProperty) {
		this.subCateNameProperty = subCateNameProperty;
	}
	
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	@JsonIgnore
	public StringProperty getMechineProperty() {
		mechineProperty.setValue(mechineId);
		
		return mechineProperty;
	}
	public void setMechineProperty(StringProperty mechineProperty) {
		this.mechineProperty = mechineProperty;
	}

	

	public String getMixName() {
		return mixName;
	}


	public void setMixName(String mixName) {
		this.mixName = mixName;
	}


	public String getMechineId() {
		return mechineId;
	}

	public void setMechineId(String mechineId) {
		this.mechineId = mechineId;
	}
	@JsonIgnore
	public StringProperty getSubCateNameProperty() {
		subCateNameProperty.setValue(mixName);
		return subCateNameProperty;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "MixCategoryMst [id=" + id + ", mixName=" + mixName + ", mechine=" + mechine + ", mechineId=" + mechineId
				+ ", branchCode=" + branchCode + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	
}
