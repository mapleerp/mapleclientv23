package com.maple.mapleclient.entity;

import java.sql.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class PurchaseReturnDtl {
	
	 private String id;
	
	Double purchseRate;
	
	CompanyMst companyMst;
	


	private Double amount;
	String status;
	private String batch;
	Double receivedQty;

	private Integer itemSerial;

	private String barcode;

	private Double taxAmt;

	private Double cessAmt;

	private Double previousMRP;

	private Date expiryDate;

	private Integer freeQty;

	private Double qty;

	private Double taxRate;

	private Double discount;

	private String changePriceStatus;

	private Integer unit;

	private Double mrp;

	private Double cessRate;

	private Date manufactureDate;

	private String ItemId;

	private String BinNo;

 
	private String unitId;

	private Double CessRate2;

	private Double NetCost;
	
	private String itemPropertyAsJson;
	
	private String itemName;
	
	@JsonIgnore
	private String unitName;
	
	
	private Double purchaseRate;
	
	private String purchaseDtlId;

	
	private String  processInstanceId;
	private String taskId;
	
	private PurchaseReturnHdr purchaseReturnHdr;
public	PurchaseReturnDtl(){
		this.amountProperty=new SimpleDoubleProperty();
		this.barcodeProperty=new SimpleStringProperty();
		this.batchProperty=new SimpleStringProperty();
		this.itemNameProperty=new SimpleStringProperty();
		this.qtyProperty=new SimpleDoubleProperty();
		this.taxRateProperty=new SimpleDoubleProperty();
		
		this.expiryDateProperty=new SimpleStringProperty();
		this.mrpProperty=new SimpleDoubleProperty();
		this.unitNameProperty=new SimpleStringProperty();

		this.taxRateProperty=new SimpleDoubleProperty();
		this.purchaseRateProperty=new SimpleDoubleProperty();
		
		
	}
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private DoubleProperty purchaseRateProperty;
	
	
	@JsonIgnore
	private StringProperty barcodeProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty taxRateProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private StringProperty expiryDateProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private DoubleProperty mrpProperty;
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	@JsonIgnore
	public StringProperty getBarcodeProperty() {
		barcodeProperty.set(barcode);
		return barcodeProperty;
	}
	public void setBarcodeProperty(StringProperty barcodeProperty) {
		this.barcodeProperty = barcodeProperty;
	}
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	@JsonIgnore
	public DoubleProperty getTaxRateProperty() {
		taxRateProperty.set(taxRate);
		return taxRateProperty;
	}
	public void setTaxRateProperty(DoubleProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}
	
	@JsonIgnore
	public StringProperty getExpiryDateProperty() {
		String expDate=SystemSetting.SqlDateTostring(expiryDate);
		expiryDateProperty.set(expDate);
		return expiryDateProperty;
	}
	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}
	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}
	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}
	
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	public DoubleProperty getMrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}
	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Double getPurchseRate() {
		return purchseRate;
	}
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate = purchseRate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getReceivedQty() {
		return receivedQty;
	}
	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}
	public Integer getItemSerial() {
		return itemSerial;
	}
	public void setItemSerial(Integer itemSerial) {
		this.itemSerial = itemSerial;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Double getTaxAmt() {
		return taxAmt;
	}
	public void setTaxAmt(Double taxAmt) {
		this.taxAmt = taxAmt;
	}
	public Double getCessAmt() {
		return cessAmt;
	}
	public void setCessAmt(Double cessAmt) {
		this.cessAmt = cessAmt;
	}
	public Double getPreviousMRP() {
		return previousMRP;
	}
	public void setPreviousMRP(Double previousMRP) {
		this.previousMRP = previousMRP;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Integer getFreeQty() {
		return freeQty;
	}
	public void setFreeQty(Integer freeQty) {
		this.freeQty = freeQty;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public String getChangePriceStatus() {
		return changePriceStatus;
	}
	public void setChangePriceStatus(String changePriceStatus) {
		this.changePriceStatus = changePriceStatus;
	}
	public Integer getUnit() {
		return unit;
	}
	public void setUnit(Integer unit) {
		this.unit = unit;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getCessRate() {
		return cessRate;
	}
	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}
	public Date getManufactureDate() {
		return manufactureDate;
	}
	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}
	public String getItemId() {
		return ItemId;
	}
	public void setItemId(String itemId) {
		ItemId = itemId;
	}
	public String getBinNo() {
		return BinNo;
	}
	public void setBinNo(String binNo) {
		BinNo = binNo;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Double getCessRate2() {
		return CessRate2;
	}
	public void setCessRate2(Double cessRate2) {
		CessRate2 = cessRate2;
	}
	public Double getNetCost() {
		return NetCost;
	}
	public void setNetCost(Double netCost) {
		NetCost = netCost;
	}
	public String getItemPropertyAsJson() {
		return itemPropertyAsJson;
	}
	public void setItemPropertyAsJson(String itemPropertyAsJson) {
		this.itemPropertyAsJson = itemPropertyAsJson;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	
	
	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}
	
	public DoubleProperty getPurchaseRateProperty() {
		purchaseRateProperty.set(purchaseRate);
		return purchaseRateProperty;
	}
	public void setPurchaseRateProperty(DoubleProperty purchaseRateProperty) {
		this.purchaseRateProperty = purchaseRateProperty;
	}
	public Double getPurchaseRate() {
		return purchaseRate;
	}
	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}
	
	
	
	

	public PurchaseReturnHdr getPurchaseReturnHdr() {
		return purchaseReturnHdr;
	}
	public void setPurchaseReturnHdr(PurchaseReturnHdr purchaseReturnHdr) {
		this.purchaseReturnHdr = purchaseReturnHdr;
	}
	
	public String getPurchaseDtlId() {
		return purchaseDtlId;
	}
	public void setPurchaseDtlId(String purchaseDtlId) {
		this.purchaseDtlId = purchaseDtlId;
	}
	@Override
	public String toString() {
		return "PurchaseReturnDtl [id=" + id + ", purchseRate=" + purchseRate + ", companyMst=" + companyMst
				+ ", amount=" + amount + ", status=" + status + ", batch=" + batch + ", receivedQty=" + receivedQty
				+ ", itemSerial=" + itemSerial + ", barcode=" + barcode + ", taxAmt=" + taxAmt + ", cessAmt=" + cessAmt
				+ ", previousMRP=" + previousMRP + ", expiryDate=" + expiryDate + ", freeQty=" + freeQty + ", qty="
				+ qty + ", taxRate=" + taxRate + ", discount=" + discount + ", changePriceStatus=" + changePriceStatus
				+ ", unit=" + unit + ", mrp=" + mrp + ", cessRate=" + cessRate + ", manufactureDate=" + manufactureDate
				+ ", ItemId=" + ItemId + ", BinNo=" + BinNo + ", unitId=" + unitId + ", CessRate2=" + CessRate2
				+ ", NetCost=" + NetCost + ", itemPropertyAsJson=" + itemPropertyAsJson + ", itemName=" + itemName
				+ ", unitName=" + unitName + ", purchaseRate=" + purchaseRate + ", purchaseDtlId=" + purchaseDtlId
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", purchaseReturnHdr="
				+ purchaseReturnHdr + ", itemNameProperty=" + itemNameProperty + ", purchaseRateProperty="
				+ purchaseRateProperty + ", barcodeProperty=" + barcodeProperty + ", qtyProperty=" + qtyProperty
				+ ", taxRateProperty=" + taxRateProperty + ", unitNameProperty=" + unitNameProperty
				+ ", expiryDateProperty=" + expiryDateProperty + ", batchProperty=" + batchProperty
				+ ", amountProperty=" + amountProperty + ", mrpProperty=" + mrpProperty + "]";
	}
	
	
	
}
