package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;
public class SchSelectionAttribInst implements Serializable{
	private static final long serialVersionUID = 1L;

	private String id;
	
	String  schemeId ;
	String attributeName ;
	String attributeValue ;
	String attributeType ;
	String selectionId;
	
	String branchCode;
	private CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getSelectionId() {
		return selectionId;
	}

	public void setSelectionId(String selectionId) {
		this.selectionId = selectionId;
	}

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public String getSchemeId() {
		return schemeId;
	}

	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	public String getAttributeType() {
		return attributeType;
	}

	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SchSelectionAttribInst [id=" + id + ", schemeId=" + schemeId + ", attributeName=" + attributeName
				+ ", attributeValue=" + attributeValue + ", attributeType=" + attributeType + ", selectionId="
				+ selectionId + ", branchCode=" + branchCode + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}



}
