package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AccountHeadsProperties implements Serializable {
		private static final long serialVersionUID = 1L;
		private String id;
		private String branchCode;
		private String accountId;
		private String propertyType;
		private String propertyName;
		
		private String  processInstanceId;
		private String taskId;
		@JsonIgnore
		private String accountName;
		@JsonIgnore
		private StringProperty accountNameProperty;
		@JsonIgnore
		private StringProperty propertyTypeProperty;
		@JsonIgnore
		private StringProperty propertyNameProperty;
		
		
		public AccountHeadsProperties() {
			this.accountNameProperty = new SimpleStringProperty();
			this.propertyTypeProperty = new SimpleStringProperty();
			this.propertyNameProperty = new SimpleStringProperty();
		}


		public String getId() {
			return id;
		}


		public void setId(String id) {
			this.id = id;
		}


		public String getBranchCode() {
			return branchCode;
		}


		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}


		public String getAccountId() {
			return accountId;
		}


		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}

		
		public String getPropertyType() {
		
			return propertyType;
		}


		public void setPropertyType(String propertyType) {
			this.propertyType = propertyType;
		}


		public String getPropertyName() {
			return propertyName;
		}


		public void setPropertyName(String propertyName) {
			this.propertyName = propertyName;
		}


		public String getProcessInstanceId() {
			return processInstanceId;
		}


		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}


		public String getTaskId() {
			return taskId;
		}


		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}


		public String getAccountName() {
			return accountName;
		}


		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}

		@JsonIgnore
		public StringProperty getAccountNameProperty() {
			accountNameProperty.set(accountName);
			return accountNameProperty;
		}


		public void setAccountNameProperty(String accountName) {
			this.accountName = accountName;
		}

		@JsonIgnore
		public StringProperty getPropertyTypeProperty() {
			propertyTypeProperty.set(propertyType);
			return propertyTypeProperty;
		}


		public void setPropertyTypeProperty(StringProperty propertyTypeProperty) {
			this.propertyTypeProperty = propertyTypeProperty;
		}

		@JsonIgnore
		public StringProperty getPropertyNameProperty() {
			propertyNameProperty.set(propertyName);
			return propertyNameProperty;
		}


		public void setPropertyNameProperty(StringProperty propertyNameProperty) {
			this.propertyNameProperty = propertyNameProperty;
		}


		@Override
		public String toString() {
			return "AccountHeadsProperties [id=" + id + ", branchCode=" + branchCode + ", accountId=" + accountId
					+ ", propertyType=" + propertyType + ", propertyName=" + propertyName + ", processInstanceId="
					+ processInstanceId + ", taskId=" + taskId + ", accountName=" + accountName
					+ ", accountNameProperty=" + accountNameProperty + ", propertyTypeProperty=" + propertyTypeProperty
					+ ", propertyNameProperty=" + propertyNameProperty + "]";
		}
		
		
}
