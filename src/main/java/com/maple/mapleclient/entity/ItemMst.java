package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemMst {
	private String id;
	private String itemName;
	private String unitId;
	private Double standardPrice;
	private String barCode;
	private String branchCode;
	private String machineId;
	private String itemGroupId;
	private String categoryId;
	private String itemGenericName;
	private String itemCode;
	private String binNo;
	private Double itemDiscount;
	private String itemDtl;
	private Integer bestBefore;
	private String barCodeLine1;
	private String barCodeLine2;
	private String supplierId;
	private Double taxRate;
	private Double sgst;
	private Double cgst;
	private String hsnCode;
	private String unitName;
	private String categoryName;
	private String isDeleted;
	private String deletedStatus;
	private String itemDescription;
	private String serviceOrGoods;
	private String itemPriceLock;
	private Integer reorderWaitingPeriod;
	// private Integer qty;
	private Double cess;
	private int isKit;
	String branchName;
	String companyName;

	Integer itemRank;
	private String productId;

	private String brandId;
	private String model;

	String netWeight;

	private String negativeBillLock;

	@JsonIgnore
	private String productName;
	@JsonIgnore
	private String brandName;

	String barcodeFormat;
	private String processInstanceId;
	private String taskId;

	private String nutrition1;
	private String nutrition2;
	private String nutrition3;
	private String nutrition4;
	private String nutrition5;
	private String nutrition6;
	private String nutrition7;
	private String nutrition8;
	private String nutrition9;
	private String nutrition10;
	private String nutrition11;
	private String nutrition12;
	private String nutrition13;
	private String nutrition14;

	public ItemMst() {

		this.itemNameProperty = new SimpleStringProperty("");
		this.standardPriceProperty = new SimpleDoubleProperty(0.0);
		this.taxRateProperty = new SimpleDoubleProperty(0.0);
		this.barCodeProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.categoryNameProperty = new SimpleStringProperty("");
		this.deletedStatusProperty = new SimpleStringProperty("");
		this.cessPrty = new SimpleDoubleProperty(0.0);
		this.productNameProperty = new SimpleStringProperty("");
		this.brandNameProperty = new SimpleStringProperty("");
		this.modelProperty = new SimpleStringProperty("");

	}

	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private DoubleProperty cessPrty;
	@JsonIgnore
	private DoubleProperty standardPriceProperty;
	@JsonIgnore
	private DoubleProperty taxRateProperty;
	@JsonIgnore
	private StringProperty barCodeProperty;
	@JsonIgnore
	private StringProperty itemCodeProperty;
	@JsonIgnore
	private StringProperty unitNameProperty;
	@JsonIgnore
	private StringProperty categoryNameProperty;
	@JsonIgnore
	private IntegerProperty qtyPrty;
	@JsonIgnore
	private StringProperty deletedStatusProperty;

	@JsonIgnore
	private StringProperty productNameProperty;
	@JsonIgnore
	private StringProperty brandNameProperty;
	@JsonIgnore
	private StringProperty modelProperty;

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getReorderWaitingPeriod() {
		return reorderWaitingPeriod;
	}

	public void setReorderWaitingPeriod(Integer reorderWaitingPeriod) {
		this.reorderWaitingPeriod = reorderWaitingPeriod;
	}

	public Integer getRank() {
		return itemRank;
	}

	public void setRank(Integer rank) {
		this.itemRank = rank;
	}

	public String getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(String netWeight) {
		this.netWeight = netWeight;
	}

	public Double getCess() {
		return cess;
	}

	public String getItemPriceLock() {
		return itemPriceLock;
	}

	public void setItemPriceLock(String itemPriceLock) {
		this.itemPriceLock = itemPriceLock;
	}

	public void setCess(Double cess) {
		this.cess = cess;
	}

	public String getDeletedStatus() {
		return deletedStatus;
	}

	public void setDeletedStatus(String deletedStatus) {
		this.deletedStatus = deletedStatus;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public Double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getItemGroupId() {
		return itemGroupId;
	}

	public void setItemGroupId(String itemGroupId) {
		this.itemGroupId = itemGroupId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getItemGenericName() {
		return itemGenericName;
	}

	public void setItemGenericName(String itemGenericName) {
		this.itemGenericName = itemGenericName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBinNo() {
		return binNo;
	}

	public void setBinNo(String binNo) {
		this.binNo = binNo;
	}

	public Double getItemDiscount() {
		return itemDiscount;
	}

	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}

	public String getItemDtl() {
		return itemDtl;
	}

	public void setItemDtl(String itemDtl) {
		this.itemDtl = itemDtl;
	}

	public Integer getBestBefore() {
		return bestBefore;
	}

	public void setBestBefore(Integer bestBefore) {
		this.bestBefore = bestBefore;
	}

	public String getBarCodeLine1() {
		return barCodeLine1;
	}

	public void setBarCodeLine1(String barCodeLine1) {
		this.barCodeLine1 = barCodeLine1;
	}

	public String getBarCodeLine2() {
		return barCodeLine2;
	}

	public void setBarCodeLine2(String barCodeLine2) {
		this.barCodeLine2 = barCodeLine2;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getSgst() {
		return sgst;
	}

	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}

	public Double getCgst() {
		return cgst;
	}

	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public StringProperty getItemNameProperty() {

		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		itemNameProperty = itemNameProperty;
	}

	public DoubleProperty getStandardPriceProperty() {

		standardPriceProperty.set(standardPrice);
		return standardPriceProperty;
	}

	public void setStandardPriceProperty(DoubleProperty standardPriceProperty) {
		standardPriceProperty = standardPriceProperty;
	}

	public DoubleProperty getTaxRateProperty() {

		taxRateProperty.set(taxRate);
		return taxRateProperty;
	}

	public void setTaxRateProperty(DoubleProperty taxRateProperty) {
		taxRateProperty = taxRateProperty;
	}

	public void setQtyProperty(IntegerProperty qtyPrty) {
		qtyPrty = qtyPrty;
	}

	public DoubleProperty getCessProperty() {

		cessPrty.set(cess);
		return cessPrty;
	}

	public void setCessProperty(DoubleProperty cessPrty) {
		cessPrty = cessPrty;
	}

	public StringProperty getBarCodeProperty() {

		barCodeProperty.set(barCode);
		return barCodeProperty;
	}

	public void setBarCodeProperty(StringProperty barCodeProperty) {
		barCodeProperty = barCodeProperty;
	}

	public StringProperty getItemCodeProperty() {

		itemCodeProperty.set(itemCode);
		return itemCodeProperty;
	}

	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		itemCodeProperty = itemCodeProperty;
	}

	public StringProperty getUnitNameProperty() {

		unitNameProperty.set(unitName);
		return unitNameProperty;
	}

	public void setUnitNameProperty(String unitName) {
		this.unitName = unitName;
	}

	public StringProperty getCategoryNameProperty() {

		categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}

	public void setCategoryNameProperty(StringProperty categoryNameProperty) {
		categoryNameProperty = categoryNameProperty;
	}

	public StringProperty getDeletedStatusProperty() {

		deletedStatusProperty.set(deletedStatus);
		return deletedStatusProperty;
	}

	public void setDeletedStatusProperty(StringProperty deletedStatusProperty) {
		deletedStatusProperty = deletedStatusProperty;
	}

	public DoubleProperty getCessPrty() {
		return cessPrty;
	}

	public void setCessPrty(DoubleProperty cessPrty) {
		this.cessPrty = cessPrty;
	}

	public IntegerProperty getQtyPrty() {
		return qtyPrty;
	}

	public void setQtyPrty(IntegerProperty qtyPrty) {
		this.qtyPrty = qtyPrty;
	}

	public int getIsKit() {
		return isKit;
	}

	public void setIsKit(int isKit) {
		this.isKit = isKit;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public StringProperty getProductNameProperty() {
		productNameProperty.set(productName);
		return productNameProperty;
	}

	public void setProductNameProperty(StringProperty productNameProperty) {
		this.productNameProperty = productNameProperty;
	}

	public StringProperty getBrandNameProperty() {
		brandNameProperty.set(brandName);
		return brandNameProperty;
	}

	public void setBrandNameProperty(StringProperty brandNameProperty) {
		this.brandNameProperty = brandNameProperty;
	}

	public StringProperty getModelProperty() {
		modelProperty.set(model);
		return modelProperty;
	}

	public void setModelProperty(StringProperty modelProperty) {
		this.modelProperty = modelProperty;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getServiceOrGoods() {
		return serviceOrGoods;
	}

	public void setServiceOrGoods(String serviceOrGoods) {
		this.serviceOrGoods = serviceOrGoods;
	}

	public String getNegativeBillLock() {
		return negativeBillLock;
	}

	public void setNegativeBillLock(String negativeBillLock) {
		this.negativeBillLock = negativeBillLock;
	}

	public String getBarcodeFormat() {
		return barcodeFormat;
	}

	public void setBarcodeFormat(String barcodeFormat) {
		this.barcodeFormat = barcodeFormat;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ItemMst [id=" + id + ", itemName=" + itemName + ", unitId=" + unitId + ", standardPrice="
				+ standardPrice + ", barCode=" + barCode + ", branchCode=" + branchCode + ", machineId=" + machineId
				+ ", itemGroupId=" + itemGroupId + ", categoryId=" + categoryId + ", itemGenericName=" + itemGenericName
				+ ", itemCode=" + itemCode + ", binNo=" + binNo + ", itemDiscount=" + itemDiscount + ", itemDtl="
				+ itemDtl + ", bestBefore=" + bestBefore + ", barCodeLine1=" + barCodeLine1 + ", barCodeLine2="
				+ barCodeLine2 + ", supplierId=" + supplierId + ", taxRate=" + taxRate + ", sgst=" + sgst + ", cgst="
				+ cgst + ", hsnCode=" + hsnCode + ", unitName=" + unitName + ", categoryName=" + categoryName
				+ ", isDeleted=" + isDeleted + ", deletedStatus=" + deletedStatus + ", itemDescription="
				+ itemDescription + ", serviceOrGoods=" + serviceOrGoods + ", itemPriceLock=" + itemPriceLock
				+ ", reorderWaitingPeriod=" + reorderWaitingPeriod + ", cess=" + cess + ", isKit=" + isKit
				+ ", branchName=" + branchName + ", companyName=" + companyName + ", itemRank=" + itemRank
				+ ", productId=" + productId + ", brandId=" + brandId + ", model=" + model + ", netWeight=" + netWeight
				+ ", negativeBillLock=" + negativeBillLock + ", productName=" + productName + ", brandName=" + brandName
				+ ", barcodeFormat=" + barcodeFormat + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}

	public String getNutrition1() {
		return nutrition1;
	}

	public void setNutrition1(String nutrition1) {
		this.nutrition1 = nutrition1;
	}

	public String getNutrition2() {
		return nutrition2;
	}

	public void setNutrition2(String nutrition2) {
		this.nutrition2 = nutrition2;
	}

	public String getNutrition3() {
		return nutrition3;
	}

	public void setNutrition3(String nutrition3) {
		this.nutrition3 = nutrition3;
	}

	public String getNutrition4() {
		return nutrition4;
	}

	public void setNutrition4(String nutrition4) {
		this.nutrition4 = nutrition4;
	}

	public String getNutrition5() {
		return nutrition5;
	}

	public void setNutrition5(String nutrition5) {
		this.nutrition5 = nutrition5;
	}

	public String getNutrition6() {
		return nutrition6;
	}

	public void setNutrition6(String nutrition6) {
		this.nutrition6 = nutrition6;
	}

	public String getNutrition7() {
		return nutrition7;
	}

	public void setNutrition7(String nutrition7) {
		this.nutrition7 = nutrition7;
	}

	public String getNutrition8() {
		return nutrition8;
	}

	public void setNutrition8(String nutrition8) {
		this.nutrition8 = nutrition8;
	}

	public String getNutrition9() {
		return nutrition9;
	}

	public void setNutrition9(String nutrition9) {
		this.nutrition9 = nutrition9;
	}

	public String getNutrition10() {
		return nutrition10;
	}

	public void setNutrition10(String nutrition10) {
		this.nutrition10 = nutrition10;
	}

	public String getNutrition11() {
		return nutrition11;
	}

	public void setNutrition11(String nutrition11) {
		this.nutrition11 = nutrition11;
	}

	public String getNutrition12() {
		return nutrition12;
	}

	public void setNutrition12(String nutrition12) {
		this.nutrition12 = nutrition12;
	}

	public String getNutrition13() {
		return nutrition13;
	}

	public void setNutrition13(String nutrition13) {
		this.nutrition13 = nutrition13;
	}

	public String getNutrition14() {
		return nutrition14;
	}

	public void setNutrition14(String nutrition14) {
		this.nutrition14 = nutrition14;
	}

}
