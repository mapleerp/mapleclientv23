package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesManMst {

	
	private String id;
	String salesManName;
	String branchCode;
	String salesManContactNo;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty salesManNameProperty;
	@JsonIgnore
    private	StringProperty  branchCodeProperty;
	
	@JsonIgnore
	 private StringProperty salesManContactNoProperty;
	
	
	
	
	public SalesManMst(){
        this.salesManNameProperty= new SimpleStringProperty("");
		this.branchCodeProperty=new SimpleStringProperty("");
		this.salesManContactNoProperty=new SimpleStringProperty("");
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSalesManName() {
		return salesManName;
	}
	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getSalesManContactNo() {
		return salesManContactNo;
	}
	public void setSalesManContactNo(String salesManContactNo) {
		this.salesManContactNo = salesManContactNo;
	}
	@JsonIgnore
	public StringProperty getSalesManNameProperty() {
		salesManNameProperty.set(salesManName);
		return salesManNameProperty;
	}


	public void setSalesManNameProperty(StringProperty salesManNameProperty) {
		this.salesManNameProperty = salesManNameProperty;
	}

	@JsonIgnore
	public StringProperty getBranchCodeProperty() {
		branchCodeProperty.set(branchCode);
		return branchCodeProperty;
	}


	public void setBranchCodeProperty(StringProperty branchCodeProperty) {
		this.branchCodeProperty = branchCodeProperty;
	}

	@JsonIgnore
	public StringProperty getSalesManContactNoProperty() {
		
		salesManContactNoProperty.set(salesManContactNo);
		return salesManContactNoProperty;
	}


	public void setSalesManContactNoProperty(StringProperty salesManContactNoProperty) {
		this.salesManContactNoProperty = salesManContactNoProperty;
	}





	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "SalesManMst [id=" + id + ", salesManName=" + salesManName + ", branchCode=" + branchCode
				+ ", salesManContactNo=" + salesManContactNo + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
}
