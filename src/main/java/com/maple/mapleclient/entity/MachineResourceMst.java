package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MachineResourceMst {
    String id;
	String machineResourceName;
	String unitId;
	Double capcity;
	String unitName;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty machineResourceNameProperty;
	
	@JsonIgnore
	private DoubleProperty capcityProperty;
	
	
	@JsonIgnore
	private StringProperty unitIdProperty;
	
	
	
	public MachineResourceMst(){
		this.machineResourceNameProperty =new SimpleStringProperty("");
		this.unitIdProperty =new SimpleStringProperty("");
		this.capcityProperty =new SimpleDoubleProperty(0.0);
		
	}




	@JsonIgnore
	public StringProperty getMachineResourceNameProperty() {
		machineResourceNameProperty.setValue(machineResourceName);
		
		return machineResourceNameProperty;
	}




	public void setMachineResourceNameProperty(StringProperty machineResourceNameProperty) {
		this.machineResourceNameProperty = machineResourceNameProperty;
	}



	@JsonIgnore
	public DoubleProperty getCapcityProperty() {
		capcityProperty.setValue(capcity);
		
		return capcityProperty;
	}




	public void setCapcityProperty(DoubleProperty capcityProperty) {
		this.capcityProperty = capcityProperty;
	}




	@JsonIgnore
	public StringProperty getUnitIdProperty() {
		unitIdProperty.setValue(unitId);
		
		return unitIdProperty;
	}




	public void setUnitIdProperty(StringProperty unitIdProperty) {
		this.unitIdProperty = unitIdProperty;
	}




	public String getMachineResourceName() {
		return machineResourceName;
	}




	public void setMachineResourceName(String machineResourceName) {
		this.machineResourceName = machineResourceName;
	}







	public String getUnitId() {
		return unitId;
	}




	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}




	public Double getCapcity() {
		return capcity;
	}




	public void setCapcity(Double capcity) {
		this.capcity = capcity;
	}




	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}




	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}




	public String getTaskId() {
		return taskId;
	}




	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}




	@Override
	public String toString() {
		return "MachineResourceMst [id=" + id + ", machineResourceName=" + machineResourceName + ", unitId=" + unitId
				+ ", capcity=" + capcity + ", unitName=" + unitName + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
	
}
