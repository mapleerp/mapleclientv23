package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
public class StockTransferLinkIntent {

	private String id;
	
	private String intentDtlId;

	private String stocktransferDtlId;

	private String allocatedQty;
	private String  processInstanceId;
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIntentDtlId() {
		return intentDtlId;
	}

	public void setIntentDtlId(String intentDtlId) {
		this.intentDtlId = intentDtlId;
	}


	public String getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(String allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public String getStocktransferDtlId() {
		return stocktransferDtlId;
	}

	public void setStocktransferDtlId(String stocktransferDtlId) {
		this.stocktransferDtlId = stocktransferDtlId;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "StockTransferLinkIntent [id=" + id + ", intentDtlId=" + intentDtlId + ", stocktransferDtlId="
				+ stocktransferDtlId + ", allocatedQty=" + allocatedQty + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}

	
	
}
