package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UnitDtl 
{
	

	@JsonIgnore
	private String  itemId;
	@JsonIgnore
	private String  primaryUnitId;
	
	@JsonIgnore
	private String  SecondaryUnitId;
	@JsonIgnore
	private StringProperty itemName;
	@JsonIgnore
    private StringProperty secondaryUnitName;
	@JsonIgnore
    private StringProperty primaryUnitName;
	@JsonIgnore
	private Double unitConversion;
	private String  processInstanceId;
	private String taskId;
	
	public UnitDtl()
	{
		this.itemName= new SimpleStringProperty("");
	
		this.secondaryUnitName = new SimpleStringProperty("");
	
		this.primaryUnitName = new SimpleStringProperty("");
		 
	}
	@JsonIgnore
	public StringProperty getItemNameProperty() {
		return itemName;
	}
	public void setItemName(StringProperty itemName) {
		this.itemName = itemName;
	}
	
	@JsonProperty("itemName")
	public String getItemName() {
		return itemName.get();
	}
	public void setItemName(String itemName) {
		this.itemName.set(itemName);
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "UnitDtl [itemId=" + itemId + ", primaryUnitId=" + primaryUnitId + ", SecondaryUnitId=" + SecondaryUnitId
				+ ", unitConversion=" + unitConversion + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	 
	
	 
	 
	 
	
	
	
}
