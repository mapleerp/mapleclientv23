package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WatchStrapMst {

	private String id;
	private String strapName;
	private String branchCode;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	public WatchStrapMst() {
		this.strapNameProperty = new SimpleStringProperty("");
	}
	@JsonIgnore
	private StringProperty strapNameProperty;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStrapName() {
		return strapName;
	}
	public void setStrapName(String strapName) {
		this.strapName = strapName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public StringProperty getStrapNameProperty() {
		strapNameProperty.set(strapName);
		return strapNameProperty;
	}
	public void setStrapNameProperty(StringProperty strapNameProperty) {
		this.strapNameProperty = strapNameProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "WatchStrapMst [id=" + id + ", strapName=" + strapName + ", branchCode=" + branchCode + ", companyMst="
				+ companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	

	
}
