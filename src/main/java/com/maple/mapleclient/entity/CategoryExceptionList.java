package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CategoryExceptionList {

	private String id;
	private String categoryId;

	private String reportName;
	
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private String categoryName;

	@JsonIgnore
	private StringProperty categoryNameProperty;
	
	@JsonIgnore
	private StringProperty reportNameProperty;

	
	
	public CategoryExceptionList() {
		this.categoryNameProperty = new SimpleStringProperty("");
		this.reportNameProperty = new SimpleStringProperty("");

	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	

	public StringProperty getCategoryNameProperty() {
		categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}

	public void setCategoryNameProperty(StringProperty categoryNameProperty) {
		this.categoryNameProperty = categoryNameProperty;
	}

	public StringProperty getReportNameProperty() {
		reportNameProperty.set(reportName);
		return reportNameProperty;
	}

	public void setReportNameProperty(StringProperty reportNameProperty) {
		this.reportNameProperty = reportNameProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "CategoryExceptionList [id=" + id + ", categoryId=" + categoryId + ", reportName=" + reportName
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", categoryName=" + categoryName + "]";
	}
	
	

}
