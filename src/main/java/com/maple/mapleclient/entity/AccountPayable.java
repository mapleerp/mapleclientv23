package com.maple.mapleclient.entity;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AccountPayable {
	
	String id;
	
	
	String voucherNumber;
	Date voucherDate;
	
	Date dueDate;
	String accountId;
	String remark;
	Double dueAmount;
	Double paidAmount;
	AccountHeads accountHeads;
	PurchaseHdr purchaseHdr;
	
	Double balanceAmount;
	
	private String  processInstanceId;
	private String taskId;
	public AccountPayable() {

		this.invoiceNumberProperty = new SimpleStringProperty();
		this.dueAmountProperty =new SimpleDoubleProperty(0.0);
		this.paidAmountProperty = new SimpleDoubleProperty(0.0);
		this.balanceAmountProperty = new SimpleDoubleProperty(0.0);
		this.balanceAmount = 0.0;
	}

	
	@JsonIgnore
	private StringProperty invoiceNumberProperty;
	
	@JsonIgnore
	private DoubleProperty dueAmountProperty;
	
	@JsonIgnore
	private DoubleProperty paidAmountProperty;
	
	@JsonIgnore
	private DoubleProperty balanceAmountProperty;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
		public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}
	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}
	
	
	
	
	
	public Double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public StringProperty getInvoiceNumberProperty() {
		invoiceNumberProperty.set(voucherNumber);
		return invoiceNumberProperty;
	}
	public void setInvoiceNumberProperty(StringProperty invoiceNumberProperty) {
		this.invoiceNumberProperty = invoiceNumberProperty;
	}
	public DoubleProperty getDueAmountProperty() {
		dueAmountProperty.set(dueAmount);
		return dueAmountProperty;
	}
	public void setDueAmountProperty(DoubleProperty dueAmountProperty) {
		this.dueAmountProperty = dueAmountProperty;
	}
	public DoubleProperty getPaidAmountProperty() {
		paidAmountProperty.set(paidAmount);
		return paidAmountProperty;
	}
	public void setPaidAmountProperty(DoubleProperty paidAmountProperty) {
		this.paidAmountProperty = paidAmountProperty;
	}
	public DoubleProperty getBalanceAmountProperty() {
		balanceAmountProperty.set(balanceAmount);
		return balanceAmountProperty;
	}
	public void setBalanceAmountProperty(DoubleProperty balanceAmountProperty) {
		this.balanceAmountProperty = balanceAmountProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public AccountHeads getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}
	@Override
	public String toString() {
		return "AccountPayable [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", dueDate=" + dueDate + ", accountId=" + accountId + ", remark=" + remark + ", dueAmount="
				+ dueAmount + ", paidAmount=" + paidAmount + ", accountHeads=" + accountHeads + ", purchaseHdr="
				+ purchaseHdr + ", balanceAmount=" + balanceAmount + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", invoiceNumberProperty=" + invoiceNumberProperty + ", dueAmountProperty="
				+ dueAmountProperty + ", paidAmountProperty=" + paidAmountProperty + ", balanceAmountProperty="
				+ balanceAmountProperty + "]";
	}


	
	

}
