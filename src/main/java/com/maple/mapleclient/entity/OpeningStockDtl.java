package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OpeningStockDtl {
	
	
private String id;
	
	private String itemId;
	private String batch ;
	private String barcode;
	private Double qtyIn;
	private Double qtyOut;
	private Double mrp;
	private String voucherNumber;
	private Date voucherDate;
	private String branchCode;
	private Date expDate;
	
	private String store;
	
	private String  processInstanceId;
	private String taskId;
	private String particulars;
	
	String itemName;
	
	 @JsonIgnore
	 private StringProperty itemIdProperty;
	 @JsonIgnore
	 private StringProperty itemNameProperty;
	 @JsonIgnore
	 private StringProperty batchProperty ;
	 @JsonIgnore
	 private StringProperty barcodeProperty;
	 
	 @JsonIgnore
	 private DoubleProperty qtyOutProperty;
	 
	 @JsonIgnore
	 private DoubleProperty qtyInProperty;
	 
	 @JsonIgnore
	 private DoubleProperty mrpProperty;
	 
	 public OpeningStockDtl() {
	
		  this.itemIdProperty = new SimpleStringProperty();
		  this.batchProperty = new SimpleStringProperty();
		  this.barcodeProperty = new SimpleStringProperty();
		  this.qtyOutProperty = new SimpleDoubleProperty();
		  this.qtyInProperty = new SimpleDoubleProperty();
		  this.mrpProperty = new SimpleDoubleProperty();
		  this.itemNameProperty = new SimpleStringProperty();
	
	
	}
	 
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Double getQtyIn() {
		return qtyIn;
	}
	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}
	public Double getQtyOut() {
		return qtyOut;
	}
	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	


	public StringProperty getItemIdProperty() {
		
		if(null!=itemId)
		{
		itemIdProperty.set(itemId);
		}
		return itemIdProperty;
	}



	public void setItemIdProperty(StringProperty itemIdProperty) {
		this.itemIdProperty = itemIdProperty;
	}



	public StringProperty getBatchProperty() {
		if(null!=batch)
		{
		batchProperty.set(batch);
		}
		return batchProperty;
	}



	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}



	public StringProperty getBarcodeProperty() {
		if(null!=barcode)
		{
		barcodeProperty.set(barcode);
		}
		return barcodeProperty;
	}



	public void setBarcodeProperty(StringProperty barcodeProperty) {
		this.barcodeProperty = barcodeProperty;
	}



	public DoubleProperty getQtyOutProperty() {
		if(null!=qtyOut)
		{
		qtyOutProperty.set(qtyOut);
		}
		return qtyOutProperty;
	}



	public void setQtyOutProperty(DoubleProperty qtyOutProperty) {
		this.qtyOutProperty = qtyOutProperty;
	}



	public DoubleProperty getQtyInProperty() {
		if(null!=qtyIn)
		{
		qtyInProperty.set(qtyIn);
		}
		return qtyInProperty;
	}



	public void setQtyInProperty(DoubleProperty qtyInProperty) {
		this.qtyInProperty = qtyInProperty;
	}



	public DoubleProperty getMrpProperty() {
		if(null!=mrp)
		{
		mrpProperty.set(mrp);
	}
		return mrpProperty;
	}



	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}



	@Override
	public String toString() {
		return "OpeningStockDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", barcode=" + barcode
				+ ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", mrp=" + mrp + ", voucherNumber=" + voucherNumber
				+ ", voucherDate=" + voucherDate + ", expDate=" + expDate + ", store=" + store + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", particulars=" + particulars + ", itemIdProperty="
				+ itemIdProperty + ", batchProperty=" + batchProperty + ", barcodeProperty=" + barcodeProperty
				+ ", qtyOutProperty=" + qtyOutProperty + ", qtyInProperty=" + qtyInProperty + ", mrpProperty="
				+ mrpProperty + "]";
	}



	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}



	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}



	public String getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

}
