package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProfitAndLossIncome {
	String id;
	String accountId;
	String serialNo;
	private String processInstanceId;
	private String taskId;
	
	CompanyMst companyMst;

	public ProfitAndLossIncome() {
		this.serialNoProperty = new SimpleStringProperty();
		this.accountNameProperty = new SimpleStringProperty();
	}
	@JsonIgnore
	String accountName;
	@JsonIgnore
	StringProperty serialNoProperty;

	@JsonIgnore
	StringProperty accountNameProperty;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public StringProperty getSerialNoProperty() {
		if(null != serialNo)
		serialNoProperty.set(serialNo);
		return serialNoProperty;
	}

	public void setSerialNoProperty(StringProperty serialNoProperty) {
		this.serialNoProperty = serialNoProperty;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public StringProperty getAccountNameProperty() {
		accountNameProperty.set(accountName);
		return accountNameProperty;
	}

	public void setAccountNameProperty(StringProperty accountNameProperty) {
		this.accountNameProperty = accountNameProperty;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Override
	public String toString() {
		return "ProfitAndLossIncome [id=" + id + ", accountId=" + accountId + ", serialNo=" + serialNo
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ ", accountName=" + accountName + ", serialNoProperty=" + serialNoProperty + ", accountNameProperty="
				+ accountNameProperty + "]";
	}
	
	
	
	

}
