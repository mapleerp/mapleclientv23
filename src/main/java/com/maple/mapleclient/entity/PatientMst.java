package com.maple.mapleclient.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PatientMst {
	String id;
	String patientName;
	String nationalId;
	String address1;
	String address2;
	String hospitalId;
	String contactPerson;
	String insuranceCompanyId;
	String insuranceCard;
	String policyType;
	String gender;
	String percentageCoverage;
	String phoneNumber;
	Date dateOfBirth;
	String nationality;
	String patientType;
	Date insuranceCardExpiry;
	private String  processInstanceId;
	private String taskId;
	String insuranceCompanyName;
	
	AccountHeads accountHeads;
	
	@JsonIgnore
	String customerName;
	
	public String getCustomerName() {
		
		customerName = accountHeads.getAccountName();
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	
	@JsonIgnore
	StringProperty patientNameProperty;
	
	@JsonIgnore
	StringProperty addressProperty;
	
	@JsonIgnore
	StringProperty hospitalIdProperty;
	
	@JsonIgnore
	StringProperty contactPersonProperty;
	
	@JsonIgnore
	StringProperty contactNoProperty;
	
	@JsonIgnore
	StringProperty insuranceCompanyProperty;
	
	@JsonIgnore
	StringProperty customerNameProperty;
	
	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		if(null != accountHeads) {
			if(null !=  accountHeads.getAccountName()) {
				customerNameProperty.set(  accountHeads.getAccountName());
			}
		}
		return customerNameProperty;
		
	}

	public void setCustomerNameProperty(String customerName) {
		customerName =accountHeads.getAccountName();
		this.customerName = customerName;
	}

	public PatientMst() {
		
		this.patientNameProperty = new SimpleStringProperty();
		this.addressProperty = new SimpleStringProperty();
		this.hospitalIdProperty = new SimpleStringProperty();
		this.contactPersonProperty = new SimpleStringProperty();
		this.contactNoProperty = new SimpleStringProperty();
		this.insuranceCompanyProperty = new SimpleStringProperty();
		this.customerNameProperty=new SimpleStringProperty();
		
	}
	
	@JsonIgnore
	public StringProperty getpatientNameProperty() {
		patientNameProperty.set(patientName);
		 return patientNameProperty;
	}

	public void setpatientNameProperty(String patientName) {
		this.patientName = patientName;
	}
	
	@JsonIgnore
	public StringProperty getaddressProperty() {
		addressProperty.set(address1);
		 return addressProperty;
	}

	public void setaddressProperty(String address1) {
		this.address1 = address1;
	}
	
	@JsonIgnore
	public StringProperty gethospitalIdProperty() {
		hospitalIdProperty.set(hospitalId);
		 return hospitalIdProperty;
	}

	public void sethospitalIdProperty(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	
	@JsonIgnore
	public StringProperty getcontactPersonProperty() {
		contactPersonProperty.set(contactPerson);
		 return contactPersonProperty;
	}

	public void setcontactPersonProperty(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	
	@JsonIgnore
	public StringProperty getcontactNoProperty() {
		contactNoProperty.set(phoneNumber);
		 return contactNoProperty;
	}

	public void setcontactNoProperty(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@JsonIgnore
	public StringProperty getinsuranceCompanyProperty() {
		insuranceCompanyProperty.set(insuranceCompanyName);
		 return insuranceCompanyProperty;
	}

	public void setinsuranceCompanyProperty(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}
	
	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getHospitalId() {
		return hospitalId;
	}
	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getInsuranceCompanyId() {
		return insuranceCompanyId;
	}
	public void setInsuranceCompanyId(String list) {
		this.insuranceCompanyId = list;
	}
	public String getInsuranceCard() {
		return insuranceCard;
	}
	public void setInsuranceCard(String insuranceCard) {
		this.insuranceCard = insuranceCard;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPercentageCoverage() {
		return percentageCoverage;
	}
	public void setPercentageCoverage(String percentageCoverage) {
		this.percentageCoverage = percentageCoverage;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public Date getInsuranceCardExpiry() {
		return insuranceCardExpiry;
	}
	public void setInsuranceCardExpiry(Date insuranceCardExpiry) {
		this.insuranceCardExpiry = insuranceCardExpiry;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	

	public String getPatientType() {
		return patientType;
	}

	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}

	

	

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	@Override
	public String toString() {
		return "PatientMst [id=" + id + ", patientName=" + patientName + ", nationalId=" + nationalId + ", address1="
				+ address1 + ", address2=" + address2 + ", hospitalId=" + hospitalId + ", contactPerson="
				+ contactPerson + ", insuranceCompanyId=" + insuranceCompanyId + ", insuranceCard=" + insuranceCard
				+ ", policyType=" + policyType + ", gender=" + gender + ", percentageCoverage=" + percentageCoverage
				+ ", phoneNumber=" + phoneNumber + ", dateOfBirth=" + dateOfBirth + ", nationality=" + nationality
				+ ", patientType=" + patientType + ", insuranceCardExpiry=" + insuranceCardExpiry
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", insuranceCompanyName="
				+ insuranceCompanyName + ", accountHeads=" + accountHeads + ", customerName=" + customerName
				+ ", patientNameProperty=" + patientNameProperty + ", addressProperty=" + addressProperty
				+ ", hospitalIdProperty=" + hospitalIdProperty + ", contactPersonProperty=" + contactPersonProperty
				+ ", contactNoProperty=" + contactNoProperty + ", insuranceCompanyProperty=" + insuranceCompanyProperty
				+ ", customerNameProperty=" + customerNameProperty + "]";
	}

	
	
}
