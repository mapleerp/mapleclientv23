package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class WeighBridgeTareWeight implements Serializable {
	private static final long serialVersionUID = 1L;
	
 
   private String id;
	
	@JsonProperty("vehicleno")
	String vehicleno;
	
	
	@JsonProperty("tareweight")
	private Integer tareweight;
	
 
	@JsonIgnore
	private  StringProperty vehiclenoProperty;
	
	@JsonIgnore
	private IntegerProperty tareweightProperty;
	private String  processInstanceId;
	private String taskId;
	

	public WeighBridgeTareWeight() {
		
		this.vehiclenoProperty = new SimpleStringProperty();
		this.tareweightProperty = new SimpleIntegerProperty();
	}

	public StringProperty getvehiclenoProperty() {
		vehiclenoProperty.set(vehicleno);
		return vehiclenoProperty;
	}
	
	
	public void setvehiclenoProperty(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	@JsonIgnore
	public IntegerProperty gettareweightProperty() {
		tareweightProperty.set(tareweight);
		return tareweightProperty;
	}
	
	
	public void settareweightProperty(Integer tareweight) {
		this.tareweight = tareweight;
	}


	private CompanyMst companyMst;
	
 
	private BranchMst branchMst;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getVehicleno() {
		return vehicleno;
	}


	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}


	public Integer getTareweight() {
		return tareweight;
	}


	public void setTareweight(Integer tareweight) {
		this.tareweight = tareweight;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public BranchMst getBranchMst() {
		return branchMst;
	}


	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}


	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "WeighBridgeTareWeight [id=" + id + ", vehicleno=" + vehicleno + ", tareweight=" + tareweight
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ ", branchMst=" + branchMst + "]";
	}


	  
	
	

}
