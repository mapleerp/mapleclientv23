package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DebitNote {
	private String id;
	String debitAccount;
	String creditAccount;
	Double amount;
	String remark;
	Date voucherDate;
	String voucherNumber;
	CompanyMst companyMst;
	String branch;
	String userId;

	String userName;
	
	@JsonIgnore
	StringProperty debitAccountProperty;
	
	@JsonIgnore
	StringProperty creditAccountProperty;
	@JsonIgnore
	DoubleProperty amountProperty;
	@JsonIgnore
	StringProperty voucherDateProperty;
	@JsonIgnore
	StringProperty voucherNumberProperty;
	@JsonIgnore
	StringProperty branchNameProperty;
	@JsonIgnore
	StringProperty userNameProperty;
	
	
	 public DebitNote() {
	
this.creditAccountProperty= new SimpleStringProperty();
this.debitAccountProperty= new SimpleStringProperty();
this.amountProperty= new SimpleDoubleProperty();
this.voucherDateProperty= new SimpleStringProperty();
this.voucherNumberProperty= new SimpleStringProperty();
this.branchNameProperty= new SimpleStringProperty();
this.userNameProperty= new SimpleStringProperty();
}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getDebitAccount() {
		return debitAccount;
	}


	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}


	public String getCreditAccount() {
		return creditAccount;
	}


	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public Date getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}




	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonIgnore
	public StringProperty getDebitAccountProperty() {
		debitAccountProperty.set(debitAccount);
		return debitAccountProperty;
	}


	public void setDebitAccountProperty(StringProperty debitAccountProperty) {
		this.debitAccountProperty = debitAccountProperty;
	}

@JsonIgnore
	public StringProperty getCreditAccountProperty() {
	creditAccountProperty.set(creditAccount);
		return creditAccountProperty;
	}


	public void setCreditAccountProperty(StringProperty creditAccountProperty) {
		this.creditAccountProperty = creditAccountProperty;
	}


	public DoubleProperty getAmountProperty() {
		if(null!=amount) {
			amountProperty.set(amount);
			}else {
				amountProperty.set(0.0);
			}
				return amountProperty;
			
	}


	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

@JsonIgnore
	public StringProperty getVoucherDateProperty() {
		String date=SystemSetting.UtilDateToString(voucherDate);
		voucherDateProperty.set(date);
		return voucherDateProperty;
	}


	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}

	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}


	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}

	@JsonIgnore
	public StringProperty getBranchNameProperty() {
		
		branchNameProperty.set(branch);
		return branchNameProperty;
	}


	public void setBranchNameProperty(StringProperty branchNameProperty) {
		this.branchNameProperty = branchNameProperty;
	}

	@JsonIgnore
	public StringProperty getUserNameProperty() {
	//	if(null!=userId) {
			userNameProperty.set(userId);
	//		}
		//userNameProperty.set(userName);
		return userNameProperty;
	
	}


	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	@Override
	public String toString() {
		return "DebitNote [id=" + id + ", debitAccount=" + debitAccount + ", creditAccount=" + creditAccount
				+ ", amount=" + amount + ", remark=" + remark + ", voucherDate=" + voucherDate + ", voucherNumber="
				+ voucherNumber + ", companyMst=" + companyMst + ", branch=" + branch + ", userId=" + userId
				+ ", userName=" + userName + ", debitAccountProperty=" + debitAccountProperty
				+ ", creditAccountProperty=" + creditAccountProperty + ", amountProperty=" + amountProperty
				+ ", voucherDateProperty=" + voucherDateProperty + ", voucherNumberProperty=" + voucherNumberProperty
				+ ", branchNameProperty=" + branchNameProperty + ", userNameProperty=" + userNameProperty + "]";
	}


	
	
	

}
