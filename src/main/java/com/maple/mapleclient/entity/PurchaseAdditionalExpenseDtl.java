package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PurchaseAdditionalExpenseDtl{
	
    private String id;
	
	String expense;
	Double amount;
	String itemName;
	String batch;
	String calculated;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty expenseProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	

	public PurchaseAdditionalExpenseDtl() {
		this.itemNameProperty = new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
		this.expenseProperty = new SimpleStringProperty();
		this.amountProperty =  new SimpleDoubleProperty();
	}
	PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr;
		 public StringProperty getexpenseProperty() {
			 expenseProperty.set(expense);
					return expenseProperty;
				}
				public void setexpenseProperty(String expense) {
					this.expense = expense;
				}
		public DoubleProperty getamountProperty() {
			amountProperty.set(amount);
				return amountProperty;
			}
		public void setamountProperty(Double amount) {
				this.amount = amount;
			}
		 public StringProperty getitemNameProperty() {
			 itemNameProperty.set(itemName);
					return itemNameProperty;
				}
				public void setitemNameProperty(String itemName) {
					this.itemName = itemName;
				}
				 public StringProperty getbatchProperty() {
					 batchProperty.set(batch);
							return batchProperty;
						}
						public void setbatchProperty(String batch) {
							this.batch = batch;
						}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCalculated() {
		return calculated;
	}
	public void setCalculated(String calculated) {
		this.calculated = calculated;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getExpense() {
		return expense;
	}
	public void setExpense(String expense) {
		this.expense = expense;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public PurchaseAdditionalExpenseHdr getPurchaseAdditionalExpenseHdr() {
		return purchaseAdditionalExpenseHdr;
	}
	public void setPurchaseAdditionalExpenseHdr(PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr) {
		this.purchaseAdditionalExpenseHdr = purchaseAdditionalExpenseHdr;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PurchaseAdditionalExpenseDtl [id=" + id + ", expense=" + expense + ", amount=" + amount + ", itemName="
				+ itemName + ", batch=" + batch + ", calculated=" + calculated + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", purchaseAdditionalExpenseHdr="
				+ purchaseAdditionalExpenseHdr + "]";
	}
	

}
