package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibm.icu.math.BigDecimal;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class MonthlySalesTransHdr implements Serializable {

	
	private static final long serialVersionUID = 1L;
	    private String id;
	  
 		private CompanyMst companyMst;
		
	
		private AccountHeads accountHeads;
		
		private LocalCustomerMst localCustomerMst;
		
		private String customerId;
		private String salesManId;
		private String deliveryBoyId;
		
		private String salesMode;
		private String creditOrCash;
		private String userId;
		
		//version2.9
		private String sourceBranchCode;
		//version2.9ends
		
		String takeOrderNumber;
		
	
		String branchCode;
		 
		String fbKey;
		private String  processInstanceId;
		private String taskId;



		private String invoiceNumberPrefix;
		
		private Long numericVoucherNumber;
		
		private String machineId;
		
		private String voucherNumber;
		
	 
		private Date voucherDate;
	  
		private Double  invoiceAmount;
	 
		private Double invoiceDiscount;
	 

		private Double  cashPay;
		
		
		private String discount;
	 
		

		private Double amountTendered;
	 
		private Double itemDiscount;
	 
		private Double paidAmount;
	 
		private Double changeAmount;
	 
		private String cardNo;
		
		private String cardType;
		 
		private Double cardamount;
		
		private String voucherType;
		
		private String servingTableName;
		
		private String isBranchSales;
		
		private Double sodexoAmount;
		
		private Double paytmAmount;
		
		private Double creditAmount;
		
		private String saleOrderHrdId;
		
		private String salesReceiptsVoucherNumber;
		
		private String performaInvoicePrinted;
		


		private String SourceIP ;

		private String  SourcePort ;

		@JsonIgnore
		private StringProperty voucherNumberProperty;
		
		@JsonIgnore
		private StringProperty receiptModeProperty;
		
		@JsonIgnore
		private StringProperty customerNameProperty;
		
		@JsonIgnore
		private DoubleProperty amountProperty;
	
		@JsonIgnore
		private StringProperty voucherDateProperty;
		MonthlySalesTransHdr(){
			
			
			this.receiptModeProperty = new SimpleStringProperty();
			this.voucherNumberProperty = new SimpleStringProperty();
			this.customerNameProperty =new SimpleStringProperty();
			this.amountProperty = new SimpleDoubleProperty();
			this.voucherDateProperty = new SimpleStringProperty();
		}
		
		
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public CompanyMst getCompanyMst() {
			return companyMst;
		}

		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}

	

		public LocalCustomerMst getLocalCustomerMst() {
			return localCustomerMst;
		}

		public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
			this.localCustomerMst = localCustomerMst;
		}

		public String getCustomerId() {
			return customerId;
		}

		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}

		public String getSalesManId() {
			return salesManId;
		}

		public void setSalesManId(String salesManId) {
			this.salesManId = salesManId;
		}

		public String getDeliveryBoyId() {
			return deliveryBoyId;
		}

		public void setDeliveryBoyId(String deliveryBoyId) {
			this.deliveryBoyId = deliveryBoyId;
		}

		public String getSalesMode() {
			return salesMode;
		}

		public void setSalesMode(String salesMode) {
			this.salesMode = salesMode;
		}

		public String getCreditOrCash() {
			return creditOrCash;
		}

		public void setCreditOrCash(String creditOrCash) {
			this.creditOrCash = creditOrCash;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getSourceBranchCode() {
			return sourceBranchCode;
		}

		public void setSourceBranchCode(String sourceBranchCode) {
			this.sourceBranchCode = sourceBranchCode;
		}

		public String getTakeOrderNumber() {
			return takeOrderNumber;
		}

		public void setTakeOrderNumber(String takeOrderNumber) {
			this.takeOrderNumber = takeOrderNumber;
		}

		public String getBranchCode() {
			return branchCode;
		}

		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}

		public String getFbKey() {
			return fbKey;
		}

		public void setFbKey(String fbKey) {
			this.fbKey = fbKey;
		}

		public String getInvoiceNumberPrefix() {
			return invoiceNumberPrefix;
		}

		public void setInvoiceNumberPrefix(String invoiceNumberPrefix) {
			this.invoiceNumberPrefix = invoiceNumberPrefix;
		}

		public Long getNumericVoucherNumber() {
			return numericVoucherNumber;
		}

		public void setNumericVoucherNumber(Long numericVoucherNumber) {
			this.numericVoucherNumber = numericVoucherNumber;
		}

		public String getMachineId() {
			return machineId;
		}

		public void setMachineId(String machineId) {
			this.machineId = machineId;
		}

		public String getVoucherNumber() {
			return voucherNumber;
		}

		public void setVoucherNumber(String voucherNumber) {
			this.voucherNumber = voucherNumber;
		}

		public Date getVoucherDate() {
			return voucherDate;
		}

		public void setVoucherDate(Date voucherDate) {
			this.voucherDate = voucherDate;
		}

		public Double getInvoiceAmount() {
			return invoiceAmount;
		}

		public void setInvoiceAmount(Double invoiceAmount) {
			this.invoiceAmount = invoiceAmount;
		}

		public Double getInvoiceDiscount() {
			return invoiceDiscount;
		}

		public void setInvoiceDiscount(Double invoiceDiscount) {
			this.invoiceDiscount = invoiceDiscount;
		}

		public Double getCashPay() {
			return cashPay;
		}

		public void setCashPay(Double cashPay) {
			this.cashPay = cashPay;
		}

		public String getDiscount() {
			return discount;
		}

		public void setDiscount(String discount) {
			this.discount = discount;
		}

		public Double getAmountTendered() {
			return amountTendered;
		}

		public void setAmountTendered(Double amountTendered) {
			this.amountTendered = amountTendered;
		}

		public Double getItemDiscount() {
			return itemDiscount;
		}

		public void setItemDiscount(Double itemDiscount) {
			this.itemDiscount = itemDiscount;
		}

		public Double getPaidAmount() {
			return paidAmount;
		}

		public void setPaidAmount(Double paidAmount) {
			this.paidAmount = paidAmount;
		}

		public Double getChangeAmount() {
			return changeAmount;
		}

		public void setChangeAmount(Double changeAmount) {
			this.changeAmount = changeAmount;
		}

		public String getCardNo() {
			return cardNo;
		}

		public void setCardNo(String cardNo) {
			this.cardNo = cardNo;
		}

		public String getCardType() {
			return cardType;
		}

		public void setCardType(String cardType) {
			this.cardType = cardType;
		}

		public Double getCardamount() {
			return cardamount;
		}

		public void setCardamount(Double cardamount) {
			this.cardamount = cardamount;
		}

		public String getVoucherType() {
			return voucherType;
		}

		public void setVoucherType(String voucherType) {
			this.voucherType = voucherType;
		}

		public String getServingTableName() {
			return servingTableName;
		}

		public void setServingTableName(String servingTableName) {
			this.servingTableName = servingTableName;
		}

		public String getIsBranchSales() {
			return isBranchSales;
		}

		public void setIsBranchSales(String isBranchSales) {
			this.isBranchSales = isBranchSales;
		}

		public Double getSodexoAmount() {
			return sodexoAmount;
		}

		public void setSodexoAmount(Double sodexoAmount) {
			this.sodexoAmount = sodexoAmount;
		}

		public Double getPaytmAmount() {
			return paytmAmount;
		}

		public void setPaytmAmount(Double paytmAmount) {
			this.paytmAmount = paytmAmount;
		}

		public Double getCreditAmount() {
			return creditAmount;
		}

		public void setCreditAmount(Double creditAmount) {
			this.creditAmount = creditAmount;
		}

		public String getSaleOrderHrdId() {
			return saleOrderHrdId;
		}

		public void setSaleOrderHrdId(String saleOrderHrdId) {
			this.saleOrderHrdId = saleOrderHrdId;
		}

		public String getSalesReceiptsVoucherNumber() {
			return salesReceiptsVoucherNumber;
		}

		public void setSalesReceiptsVoucherNumber(String salesReceiptsVoucherNumber) {
			this.salesReceiptsVoucherNumber = salesReceiptsVoucherNumber;
		}

		public String getPerformaInvoicePrinted() {
			return performaInvoicePrinted;
		}

		public void setPerformaInvoicePrinted(String performaInvoicePrinted) {
			this.performaInvoicePrinted = performaInvoicePrinted;
		}

		public String getSourceIP() {
			return SourceIP;
		}

		public void setSourceIP(String sourceIP) {
			SourceIP = sourceIP;
		}

		public String getSourcePort() {
			return SourcePort;
		}

		public void setSourcePort(String sourcePort) {
			SourcePort = sourcePort;
		}

		
		@JsonIgnore
		public StringProperty getVoucherNumberProperty() {
			
			voucherNumberProperty.set(voucherNumber);
			return voucherNumberProperty;
		}



		public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
			this.voucherNumberProperty = voucherNumberProperty;
		}


        @JsonIgnore
		public StringProperty getReceiptModeProperty() {
	     receiptModeProperty.set(salesMode);
			return receiptModeProperty;
		}



		public void setReceiptModeProperty(StringProperty receiptModeProperty) {
			this.receiptModeProperty = receiptModeProperty;
		}


@JsonIgnore
		public StringProperty getCustomerNameProperty() {
	   customerNameProperty.set(accountHeads.getAccountName());
			return customerNameProperty;
		}



		public void setCustomerNameProperty(StringProperty customerNameProperty) {
			this.customerNameProperty = customerNameProperty;
		}


       @JsonIgnore
		public DoubleProperty getAmountProperty() {
    		
	    amountProperty.set(invoiceAmount);
			return amountProperty;
		}



		public void setAmountProperty(DoubleProperty amountProperty) {
			this.amountProperty = amountProperty;
		}


      @JsonIgnore
		public StringProperty getVoucherDateProperty() {
	   String vDate=SystemSetting.UtilDateToString(voucherDate,"yyyy-MM-dd" );
	    voucherDateProperty.set(vDate);
			return voucherDateProperty;
		}



		public void setVoucherDateProperty(StringProperty voucherDateProperty) {
			this.voucherDateProperty = voucherDateProperty;
		}



		public static long getSerialversionuid() {
			return serialVersionUID;
		}



		



		public String getProcessInstanceId() {
			return processInstanceId;
		}



		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}



		public String getTaskId() {
			return taskId;
		}



		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}



		public AccountHeads getAccountHeads() {
			return accountHeads;
		}



		public void setAccountHeads(AccountHeads accountHeads) {
			this.accountHeads = accountHeads;
		}



		@Override
		public String toString() {
			return "MonthlySalesTransHdr [id=" + id + ", companyMst=" + companyMst + ", accountHeads=" + accountHeads
					+ ", localCustomerMst=" + localCustomerMst + ", customerId=" + customerId + ", salesManId="
					+ salesManId + ", deliveryBoyId=" + deliveryBoyId + ", salesMode=" + salesMode + ", creditOrCash="
					+ creditOrCash + ", userId=" + userId + ", sourceBranchCode=" + sourceBranchCode
					+ ", takeOrderNumber=" + takeOrderNumber + ", branchCode=" + branchCode + ", fbKey=" + fbKey
					+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", invoiceNumberPrefix="
					+ invoiceNumberPrefix + ", numericVoucherNumber=" + numericVoucherNumber + ", machineId="
					+ machineId + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
					+ ", invoiceAmount=" + invoiceAmount + ", invoiceDiscount=" + invoiceDiscount + ", cashPay="
					+ cashPay + ", discount=" + discount + ", amountTendered=" + amountTendered + ", itemDiscount="
					+ itemDiscount + ", paidAmount=" + paidAmount + ", changeAmount=" + changeAmount + ", cardNo="
					+ cardNo + ", cardType=" + cardType + ", cardamount=" + cardamount + ", voucherType=" + voucherType
					+ ", servingTableName=" + servingTableName + ", isBranchSales=" + isBranchSales + ", sodexoAmount="
					+ sodexoAmount + ", paytmAmount=" + paytmAmount + ", creditAmount=" + creditAmount
					+ ", saleOrderHrdId=" + saleOrderHrdId + ", salesReceiptsVoucherNumber="
					+ salesReceiptsVoucherNumber + ", performaInvoicePrinted=" + performaInvoicePrinted + ", SourceIP="
					+ SourceIP + ", SourcePort=" + SourcePort + ", voucherNumberProperty=" + voucherNumberProperty
					+ ", receiptModeProperty=" + receiptModeProperty + ", customerNameProperty=" + customerNameProperty
					+ ", amountProperty=" + amountProperty + ", voucherDateProperty=" + voucherDateProperty + "]";
		}



		
	
	
}
