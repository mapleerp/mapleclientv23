package com.maple.mapleclient.entity;

public class VoucherNumberDtlReport {

	
	Integer posStartingBillNumber;
	Integer posEndingBillNumber;
	Integer posBillCount;
	Integer wholeSaleStartingBillNumber; 
	Integer wholeSaleEndingBillNumber; 
	Integer  wholeSaleBillCount;
	private String  processInstanceId;
	private String taskId;
	public Integer getPosStartingBillNumber() {
		return posStartingBillNumber;
	}
	public void setPosStartingBillNumber(Integer posStartingBillNumber) {
		this.posStartingBillNumber = posStartingBillNumber;
	}
	public Integer getPosEndingBillNumber() {
		return posEndingBillNumber;
	}
	public void setPosEndingBillNumber(Integer posEndingBillNumber) {
		this.posEndingBillNumber = posEndingBillNumber;
	}
	public Integer getPosBillCount() {
		return posBillCount;
	}
	public void setPosBillCount(Integer posBillCount) {
		this.posBillCount = posBillCount;
	}
	public Integer getWholeSaleStartingBillNumber() {
		return wholeSaleStartingBillNumber;
	}
	public void setWholeSaleStartingBillNumber(Integer wholeSaleStartingBillNumber) {
		this.wholeSaleStartingBillNumber = wholeSaleStartingBillNumber;
	}
	public Integer getWholeSaleEndingBillNumber() {
		return wholeSaleEndingBillNumber;
	}
	public void setWholeSaleEndingBillNumber(Integer wholeSaleEndingBillNumber) {
		this.wholeSaleEndingBillNumber = wholeSaleEndingBillNumber;
	}
	public Integer getWholeSaleBillCount() {
		return wholeSaleBillCount;
	}
	public void setWholeSaleBillCount(Integer wholeSaleBillCount) {
		this.wholeSaleBillCount = wholeSaleBillCount;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "VoucherNumberDtlReport [posStartingBillNumber=" + posStartingBillNumber + ", posEndingBillNumber="
				+ posEndingBillNumber + ", posBillCount=" + posBillCount + ", wholeSaleStartingBillNumber="
				+ wholeSaleStartingBillNumber + ", wholeSaleEndingBillNumber=" + wholeSaleEndingBillNumber
				+ ", wholeSaleBillCount=" + wholeSaleBillCount + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
}
