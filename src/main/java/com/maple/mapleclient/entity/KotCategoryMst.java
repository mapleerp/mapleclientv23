package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KotCategoryMst {
	
	String id;
	String categoryId;
	String printerId;
	String printerName;
	String categoryName;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty categoryNameProperty;
	
	@JsonIgnore
	private StringProperty printerNameProperty;
	
	
	public KotCategoryMst() {
	
		this.categoryNameProperty = new SimpleStringProperty();
		this.printerNameProperty = new SimpleStringProperty();
	}
	
	@JsonIgnore
	public StringProperty getcategoryNameProperty() {
		categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}

	public void setcategoryNameProperty(String categoryName) {
		this.categoryName = categoryName;
	}
	
	@JsonIgnore
	public StringProperty getprinterNameProperty() {
		printerNameProperty.set(printerName);
		return printerNameProperty;
	}

	public void setprinterNameProperty(String printerName) {
		this.printerName = printerName;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getPrinterId() {
		return printerId;
	}
	public void setPrinterId(String printerId) {
		this.printerId = printerId;
	}
	public String getPrinterName() {
		return printerName;
	}
	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "KotCategoryMst [id=" + id + ", categoryId=" + categoryId + ", printerId=" + printerId + ", printerName="
				+ printerName + ", categoryName=" + categoryName + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	

}
