package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PurchaseHdrReport {

String voucherNumber;
String supplierName;


@JsonIgnore
StringProperty voucherNumberProperty;
	
@JsonIgnore
StringProperty supplierNameProperty;


public PurchaseHdrReport(){
	
	this.voucherNumberProperty = new SimpleStringProperty("");
	this.supplierNameProperty = new SimpleStringProperty("");
}


public String getVoucherNumber() {
	return voucherNumber;
}


public void setVoucherNumber(String voucherNumber) {
	this.voucherNumber = voucherNumber;
}


public String getSupplierName() {
	return supplierName;
}


public void setSupplierName(String supplierName) {
	this.supplierName = supplierName;
}

@JsonIgnore
public StringProperty getVoucherNumberProperty() {
	
	voucherNumberProperty.set(voucherNumber);
	return voucherNumberProperty;
}


public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
	this.voucherNumberProperty = voucherNumberProperty;
}

@JsonIgnore
public StringProperty getSupplierNameProperty() {
	supplierNameProperty.set(supplierName);
	
	return supplierNameProperty;
}


public void setSupplierNameProperty(StringProperty supplierNameProperty) {
	this.supplierNameProperty = supplierNameProperty;
}












}