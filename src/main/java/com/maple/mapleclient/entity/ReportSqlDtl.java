package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ReportSqlDtl {
	String id;
	String reportSqlHdrId;
	String paramSequence;
	String paramName;
	private String  processInstanceId;
	private String taskId;
	
	public ReportSqlDtl() {
		this.paramSequenceProperty=new SimpleStringProperty();
		this.reportNameProperty = new SimpleStringProperty();
		this.sqlStringProperty = new SimpleStringProperty();
		this.paramNameProperty = new SimpleStringProperty();
		
		}
	
	
	
	@JsonIgnore
	String reportName;
	@JsonIgnore
	String sqlString;
	
	
	
	@JsonIgnore
	StringProperty paramNameProperty;
	@JsonIgnore
	StringProperty paramSequenceProperty;
	
	@JsonIgnore
	StringProperty reportNameProperty;
	
	@JsonIgnore
	StringProperty sqlStringProperty;
	
	
	
	
	
	public String getReportName() {
		return reportName;
	}
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	public String getSqlString() {
		return sqlString;
	}
	public void setSqlString(String sqlString) {
		this.sqlString = sqlString;
	}
	public StringProperty getParamNameProperty() {
		paramNameProperty.set(paramName);
		return paramNameProperty;
	}
	public void setParamNameProperty(StringProperty paramNameProperty) {
		this.paramNameProperty = paramNameProperty;
	}
	public StringProperty getParamSequenceProperty() {
		paramSequenceProperty.set(paramSequence);
		return paramSequenceProperty;
	}
	public void setParamSequenceProperty(StringProperty paramSequenceProperty) {
		this.paramSequenceProperty = paramSequenceProperty;
	}
	public StringProperty getReportNameProperty() {
		reportNameProperty.set(reportName);
		return reportNameProperty;
	}
	public void setReportNameProperty(StringProperty reportNameProperty) {
		this.reportNameProperty = reportNameProperty;
	}
	public StringProperty getSqlStringProperty() {
		sqlStringProperty.set(sqlString);
		return sqlStringProperty;
	}
	public void setSqlStringProperty(StringProperty sqlStringProperty) {
		this.sqlStringProperty = sqlStringProperty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReportSqlHdrId() {
		return reportSqlHdrId;
	}
	public void setReportSqlHdrId(String reportSqlHdrId) {
		this.reportSqlHdrId = reportSqlHdrId;
	}
	public String getParamSequence() {
		return paramSequence;
	}
	public void setParamSequence(String paramSequence) {
		this.paramSequence = paramSequence;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ReportSqlDtl [id=" + id + ", reportSqlHdrId=" + reportSqlHdrId + ", paramSequence=" + paramSequence
				+ ", paramName=" + paramName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", reportName=" + reportName + ", sqlString=" + sqlString + ", paramNameProperty=" + paramNameProperty
				+ ", paramSequenceProperty=" + paramSequenceProperty + ", reportNameProperty=" + reportNameProperty
				+ ", sqlStringProperty=" + sqlStringProperty + "]";
	}
	
}
