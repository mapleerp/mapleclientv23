package com.maple.mapleclient.entity;

public class WfParamValueConfig {

	private String id;
	private String paramName;
	private String paramValue;
	private String  processInstanceId;
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "WfParamValueConfig [id=" + id + ", paramName=" + paramName + ", paramValue=" + paramValue
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

}
