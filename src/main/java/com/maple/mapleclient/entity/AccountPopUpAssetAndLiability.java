package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AccountPopUpAssetAndLiability implements Serializable{
	private String id;
	private String accountName;
	private String parentId;
	private String parentName;
	
	CompanyMst companyMst;
	
	private String  processInstanceId;
	private String taskId;
	

	public AccountPopUpAssetAndLiability() {
		this.accountNameProperty = new SimpleStringProperty("");
		this.parentNameProperty = new SimpleStringProperty("");
	}
	
	@JsonIgnore
	private StringProperty accountNameProperty;
	@JsonIgnore
	private StringProperty parentNameProperty;


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public StringProperty getAccountNameProperty() {
		if(null != accountName) {
			accountNameProperty.set(accountName);	
		}
		return accountNameProperty;
	}
	public void setAccountNameProperty(StringProperty accountNameProperty) {
		this.accountNameProperty = accountNameProperty;
	}
	public StringProperty getParentNameProperty() {
		if(null != parentName) {
			parentNameProperty.set(parentName);
		}
		return parentNameProperty;
	}
	public void setParentNameProperty(StringProperty parentNameProperty) {
		this.parentNameProperty = parentNameProperty;
	}
	@Override
	public String toString() {
		return "AccountPopUpAssetAndLiability [id=" + id + ", accountName=" + accountName + ", parentId=" + parentId
				+ ", parentName=" + parentName + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", accountNameProperty=" + accountNameProperty
				+ ", parentNameProperty=" + parentNameProperty + "]";
	}
	
	
	

}
