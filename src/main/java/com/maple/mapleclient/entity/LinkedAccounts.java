package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LinkedAccounts {
String id;
String parentAccount;
String childAccount;
CompanyMst companyMst;
String branch;

@JsonIgnore
String parentAccountName;
@JsonIgnore
String childAccountName;

@JsonIgnore
private StringProperty parentAccountProperty;

@JsonIgnore
private StringProperty childAccountProperty;

public LinkedAccounts() {
	this.parentAccountProperty = new SimpleStringProperty("");
	this.childAccountProperty = new SimpleStringProperty("");
	
}




public String getParentAccountName() {
	return parentAccountName;
}

public void setParentAccountName(String parentAccountName) {
	this.parentAccountName = parentAccountName;
}

public String getChildAccountName() {
	return childAccountName;
}

public void setChildAccountName(String childAccountName) {
	this.childAccountName = childAccountName;
}

public StringProperty getParentAccountProperty() {
	if(null != parentAccountName) {
	parentAccountProperty.set(parentAccountName);
	}
	return parentAccountProperty;
}

public void setParentAccountProperty(StringProperty parentAccountProperty) {
	this.parentAccountProperty = parentAccountProperty;
}



public StringProperty getChildAccountProperty() {
	if(null != childAccountName) {
		childAccountProperty.set(childAccountName);
		}
	return childAccountProperty;
}



public void setChildAccountProperty(StringProperty childAccountProperty) {
	this.childAccountProperty = childAccountProperty;
}



public String getBranch() {
	return branch;
}

public void setBranch(String branch) {
	this.branch = branch;
}

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getParentAccount() {
	return parentAccount;
}
public void setParentAccount(String parentAccount) {
	this.parentAccount = parentAccount;
}
public String getChildAccount() {
	return childAccount;
}
public void setChildAccount(String childAccount) {
	this.childAccount = childAccount;
}

public CompanyMst getCompanyMst() {
	return companyMst;
}
public void setCompanyMst(CompanyMst companyMst) {
	this.companyMst = companyMst;
}




@Override
public String toString() {
	return "LinkedAccounts [id=" + id + ", parentAccount=" + parentAccount + ", childAccount=" + childAccount
			+ ", companyMst=" + companyMst + ", branch=" + branch + ", parentAccountName=" + parentAccountName
			+ ", childAccountName=" + childAccountName + ", parentAccountProperty=" + parentAccountProperty
			+ ", childAccountProperty=" + childAccountProperty + "]";
}







}
