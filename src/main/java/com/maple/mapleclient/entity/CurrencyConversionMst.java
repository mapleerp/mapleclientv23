package com.maple.mapleclient.entity;

import java.time.LocalDateTime;



import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CurrencyConversionMst {
	
	
	
	String id;
	String toCurrencyId;
	Double conversionRate;
	String branchCode;

	String fromCurrencyId;
	
	Double fromQunatity;

	private LocalDateTime updatedTime;
	
	@JsonIgnore
	String fromCurrencyName;
	@JsonIgnore
	String toCurrencyName;
	
	public CurrencyConversionMst() {
		
		this.fromCurrencyNameProperty = new SimpleStringProperty("");
		this.toCurrencyNameProperty = new SimpleStringProperty("");
		this.currencyRateProperty = new SimpleDoubleProperty(0.0);

		
	}
	@JsonIgnore
	StringProperty fromCurrencyNameProperty;
	
	@JsonIgnore
	StringProperty toCurrencyNameProperty;
	
	@JsonIgnore
	DoubleProperty currencyRateProperty;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getToCurrencyId() {
		return toCurrencyId;
	}

	public void setToCurrencyId(String toCurrencyId) {
		this.toCurrencyId = toCurrencyId;
	}

	public Double getConversionRate() {
		return conversionRate;
	}

	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getFromCurrencyId() {
		return fromCurrencyId;
	}

	public void setFromCurrencyId(String fromCurrencyId) {
		this.fromCurrencyId = fromCurrencyId;
	}

	public Double getFromQunatity() {
		return fromQunatity;
	}

	public void setFromQunatity(Double fromQunatity) {
		this.fromQunatity = fromQunatity;
	}

	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getFromCurrencyName() {
		return fromCurrencyName;
	}

	public void setFromCurrencyName(String fromCurrencyName) {
		this.fromCurrencyName = fromCurrencyName;
	}

	public String getToCurrencyName() {
		return toCurrencyName;
	}

	public void setToCurrencyName(String toCurrencyName) {
		this.toCurrencyName = toCurrencyName;
	}

	public StringProperty getFromCurrencyNameProperty() {
		if(null != fromCurrencyName) {
			fromCurrencyNameProperty.set(fromCurrencyName);
		}
		return fromCurrencyNameProperty;
	}

	public void setFromCurrencyNameProperty(StringProperty fromCurrencyNameProperty) {
		this.fromCurrencyNameProperty = fromCurrencyNameProperty;
	}

	public StringProperty getToCurrencyNameProperty() {
		if(null != toCurrencyName) {
			toCurrencyNameProperty.set(toCurrencyName);
		}
		return toCurrencyNameProperty;
	}

	public void setToCurrencyNameProperty(StringProperty toCurrencyNameProperty) {
		this.toCurrencyNameProperty = toCurrencyNameProperty;
	}

	public DoubleProperty getCurrencyRateProperty() {
		if(null != conversionRate) {
			currencyRateProperty.set(conversionRate);
		}
		return currencyRateProperty;
	}

	public void setCurrencyRateProperty(DoubleProperty currencyRateProperty) {
		this.currencyRateProperty = currencyRateProperty;
	}

	@Override
	public String toString() {
		return "CurrencyConversionMst [id=" + id + ", toCurrencyId=" + toCurrencyId + ", conversionRate="
				+ conversionRate + ", branchCode=" + branchCode + ", fromCurrencyId=" + fromCurrencyId
				+ ", fromQunatity=" + fromQunatity + ", updatedTime=" + updatedTime + ", fromCurrencyName="
				+ fromCurrencyName + ", toCurrencyName=" + toCurrencyName + ", fromCurrencyNameProperty="
				+ fromCurrencyNameProperty + ", toCurrencyNameProperty=" + toCurrencyNameProperty
				+ ", currencyRateProperty=" + currencyRateProperty + "]";
	}
	
	
	
	
//	
//	public String getId() {
//		return id;
//	}
//	public void setId(String id) {
//		this.id = id;
//	}
//	
//	public Double getConversionRate() {
//		return conversionRate;
//	}
//	public void setConversionRate(Double conversionRate) {
//		this.conversionRate = conversionRate;
//	}
//	public String getBranchCode() {
//		return branchCode;
//	}
//	public void setBranchCode(String branchCode) {
//		this.branchCode = branchCode;
//	}
//	
//	
//	
//	public StringProperty getCurrencyNameProperty() {
//		currencyNameProperty.set(currencyName);
//		return currencyNameProperty;
//	}
//	public void setCurrencyNameProperty(StringProperty currencyNameProperty) {
//		this.currencyNameProperty = currencyNameProperty;
//	}
//	public DoubleProperty getCurrencyRateProperty() {
//		currencyRateProperty.set(conversionRate);
//		return currencyRateProperty;
//	}
//	public void setCurrencyRateProperty(DoubleProperty currencyRateProperty) {
//		this.currencyRateProperty = currencyRateProperty;
//	}
//	public String getCurrencyName() {
//		return currencyName;
//	}
//	public void setCurrencyName(String currencyName) {
//		this.currencyName = currencyName;
//	}
//	public String getProcessInstanceId() {
//		return processInstanceId;
//	}
//	public void setProcessInstanceId(String processInstanceId) {
//		this.processInstanceId = processInstanceId;
//	}
//	public String getTaskId() {
//		return taskId;
//	}
//	public void setTaskId(String taskId) {
//		this.taskId = taskId;
//	}
//	@Override
//	public String toString() {
//		return "CurrencyConversionMst [id=" + id + ", currencyId=" + currencyId + ", conversionRate=" + conversionRate
//				+ ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
//				+ ", currencyName=" + currencyName + "]";
//	}
//	
//	
//	
}
