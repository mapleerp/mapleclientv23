package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class DailySalesSummary {
	
	 private String id;
		
	
		String openingBillNo;
		String closingBillNo;
		Integer totalBillNo;
		Double openingPettyCash;
		Double cashReceived;
		Double expensePaid;
		Double closingBalance;
		Double cashSalesOrder;
		Double onlineSalesOrder;
		Double debitCardSalesOrder;
		String branchCode;
		
		private String  processInstanceId;
		private String taskId;
		

		@JsonProperty("recordDate")
		public java.sql.Date getRecordDate( ) {
			if(null==this.recordDate.get() ) {
				return null;
			}else {
				return Date.valueOf(this.recordDate.get() );
			}
		 
			
		}
		
	   public void setRecordDate (java.sql.Date recordDateProperty) {
							if(null!=recordDateProperty)
			this.recordDate.set(recordDateProperty.toLocalDate());
		}
	   
		public DailySalesSummary() {
			this.openingBillNoProperty = new SimpleStringProperty("");
			this.closingBillNoProperty = new SimpleStringProperty("");
			this.branchCodeProperty = new SimpleStringProperty("");
			this.openingPettyCashProperty = new SimpleDoubleProperty();
			this.cashReceivedProperty = new SimpleDoubleProperty();
			this.expensePaidProperty = new SimpleDoubleProperty();
			this.closingBalanceProperty = new SimpleDoubleProperty();
			this.cashSalesOrderProperty = new SimpleDoubleProperty();
			this.onlineSalesOrderProperty = new SimpleDoubleProperty();
			this.debitCardSalesOrderProperty = new SimpleDoubleProperty();
			this.totalBillNoProperty = new SimpleIntegerProperty();
			this.recordDate = new SimpleObjectProperty<LocalDate>(null);
		}

	   
	   @JsonIgnore
		private StringProperty openingBillNoProperty;
	   
	   @JsonIgnore
		private StringProperty closingBillNoProperty;
	   
	   @JsonIgnore
		private IntegerProperty totalBillNoProperty;
	   
	   @JsonIgnore
		private DoubleProperty openingPettyCashProperty;
	   
	   @JsonIgnore
	 		private DoubleProperty cashReceivedProperty;
	   
	   @JsonIgnore
		private DoubleProperty expensePaidProperty;
	   
	   @JsonIgnore
		private DoubleProperty closingBalanceProperty;
	   
	   @JsonIgnore
		private DoubleProperty cashSalesOrderProperty;
	   
	   @JsonIgnore
		private DoubleProperty onlineSalesOrderProperty;
	   
	   @JsonIgnore
		private DoubleProperty debitCardSalesOrderProperty;
	   
	   @JsonIgnore
		private StringProperty branchCodeProperty;
	   
	   @JsonIgnore
		private ObjectProperty<LocalDate> recordDate;
	   
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getOpeningBillNo() {
			return openingBillNo;
		}
		public void setOpeningBillNo(String openingBillNo) {
			this.openingBillNo = openingBillNo;
		}
		public String getClosingBillNo() {
			return closingBillNo;
		}
		public void setClosingBillNo(String closingBillNo) {
			this.closingBillNo = closingBillNo;
		}
		public Integer getTotalBillNo() {
			return totalBillNo;
		}
		public void setTotalBillNo(Integer totalBillNo) {
			this.totalBillNo = totalBillNo;
		}
		public Double getOpeningPettyCash() {
			return openingPettyCash;
		}
		public void setOpeningPettyCash(Double openingPettyCash) {
			this.openingPettyCash = openingPettyCash;
		}
		public Double getCashReceived() {
			return cashReceived;
		}
		public void setCashReceived(Double cashReceived) {
			this.cashReceived = cashReceived;
		}
		public Double getExpensePaid() {
			return expensePaid;
		}
		public void setExpensePaid(Double expensePaid) {
			this.expensePaid = expensePaid;
		}
		public Double getClosingBalance() {
			return closingBalance;
		}
		public void setClosingBalance(Double closingBalance) {
			this.closingBalance = closingBalance;
		}
		public Double getCashSalesOrder() {
			return cashSalesOrder;
		}
		public void setCashSalesOrder(Double cashSalesOrder) {
			this.cashSalesOrder = cashSalesOrder;
		}
		public Double getOnlineSalesOrder() {
			return onlineSalesOrder;
		}
		public void setOnlineSalesOrder(Double onlineSalesOrder) {
			this.onlineSalesOrder = onlineSalesOrder;
		}
		public Double getDebitCardSalesOrder() {
			return debitCardSalesOrder;
		}
		public void setDebitCardSalesOrder(Double debitCardSalesOrder) {
			this.debitCardSalesOrder = debitCardSalesOrder;
		}
		public String getBranchCode() {
			return branchCode;
		}
		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}
		
		
		
		
		public ObjectProperty<LocalDate> getRecordDateProperty( ) {
			return recordDate;
		}
	 
		public void setRecordDateProperty(ObjectProperty<LocalDate> recordDateProperty) {
			this.recordDate = recordDate;
		}
		
		public StringProperty getOpeningBillNoProperty() {

			openingBillNoProperty.set(openingBillNo);
			return openingBillNoProperty;
		}

		public void setItemNameProperty(StringProperty openingBillNoProperty) {
			openingBillNoProperty = openingBillNoProperty;
		}
		
		public StringProperty getClosingBillNoProperty() {

			closingBillNoProperty.set(closingBillNo);
			return closingBillNoProperty;
		}

		public void setClosingBillNoProperty(StringProperty closingBillNoProperty) {
			closingBillNoProperty = closingBillNoProperty;
		}
		
		public StringProperty getBranchCodeProperty() {

			branchCodeProperty.set(branchCode);
			return branchCodeProperty;
		}

		public void setBranchCodeProperty(StringProperty branchCodeProperty) {
			branchCodeProperty = branchCodeProperty;
		}
		
		@JsonIgnore
		public DoubleProperty getOpeningPettyCashProperty() {
			openingPettyCashProperty.set(openingPettyCash);
			return openingPettyCashProperty;
		}
		public void setOpeningPettyCashProperty(DoubleProperty openingPettyCashProperty) {
			this.openingPettyCashProperty = openingPettyCashProperty;
		}
		
		@JsonIgnore
		public DoubleProperty getCashReceivedProperty() {
			cashReceivedProperty.set(cashReceived);
			return cashReceivedProperty;
		}
		public void setCashReceivedProperty(DoubleProperty cashReceivedProperty) {
			this.cashReceivedProperty = cashReceivedProperty;
		}

		@JsonIgnore
		public DoubleProperty getExpensePaidProperty() {
			expensePaidProperty.set(expensePaid);
			return expensePaidProperty;
		}
		public void setExpensePaidProperty(DoubleProperty expensePaidProperty) {
			this.expensePaidProperty = expensePaidProperty;
		}
		
		@JsonIgnore
		public DoubleProperty getClosingBalanceProperty() {
			closingBalanceProperty.set(closingBalance);
			return closingBalanceProperty;
		}
		public void setClosingBalanceProperty(DoubleProperty closingBalanceProperty) {
			this.closingBalanceProperty = closingBalanceProperty;
		}
		
		@JsonIgnore
		public DoubleProperty getCashSalesOrderProperty() {
			cashSalesOrderProperty.set(cashSalesOrder);
			return cashSalesOrderProperty;
		}
		public void setCashSalesOrderProperty(DoubleProperty cashSalesOrderProperty) {
			this.cashSalesOrderProperty = cashSalesOrderProperty;
		}
		
		@JsonIgnore
		public DoubleProperty getOnlineSalesOrderProperty() {
			onlineSalesOrderProperty.set(onlineSalesOrder);
			return onlineSalesOrderProperty;
		}
		public void setOnlineSalesOrderProperty(DoubleProperty onlineSalesOrderProperty) {
			this.onlineSalesOrderProperty = onlineSalesOrderProperty;
		}
		
		@JsonIgnore
		public DoubleProperty getDebitCardSalesOrderProperty() {
			debitCardSalesOrderProperty.set(debitCardSalesOrder);
			return debitCardSalesOrderProperty;
		}
		public void setDebitCardSalesOrderProperty(DoubleProperty debitCardSalesOrderProperty) {
			this.debitCardSalesOrderProperty = debitCardSalesOrderProperty;
		}
		
		@JsonIgnore
		public IntegerProperty getTotalBillNoProperty() {
			totalBillNoProperty.set(totalBillNo);
			return totalBillNoProperty;
		}
		public void setTotalBillNoProperty(IntegerProperty totalBillNoProperty) {
			this.totalBillNoProperty = totalBillNoProperty;
		}

		
		
		public java.util.Date convertToDateViaInstant(LocalDate dateToConvert) {
			return java.util.Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
		}

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "DailySalesSummary [id=" + id + ", openingBillNo=" + openingBillNo + ", closingBillNo="
					+ closingBillNo + ", totalBillNo=" + totalBillNo + ", openingPettyCash=" + openingPettyCash
					+ ", cashReceived=" + cashReceived + ", expensePaid=" + expensePaid + ", closingBalance="
					+ closingBalance + ", cashSalesOrder=" + cashSalesOrder + ", onlineSalesOrder=" + onlineSalesOrder
					+ ", debitCardSalesOrder=" + debitCardSalesOrder + ", branchCode=" + branchCode
					+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
		}
		

}
