package com.maple.mapleclient.entity;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LoginPopup {
	StringProperty username;
	StringProperty password;
	private String  processInstanceId;
	private String taskId;

	
	public LoginPopup() {
		this.username = new SimpleStringProperty("");
		this.password = new SimpleStringProperty("");
	 
		
}
	
	public StringProperty getUsername() {
	return username;
}



public void setUsername(StringProperty username) {
	this.username = username;
}



public StringProperty getPassword() {
	return password;
}



public void setPassword(StringProperty password) {
	this.password = password;
}

public String getProcessInstanceId() {
	return processInstanceId;
}

public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}

public String getTaskId() {
	return taskId;
}

public void setTaskId(String taskId) {
	this.taskId = taskId;
}

@Override
public String toString() {
	return "LoginPopup [processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
}

}

	
