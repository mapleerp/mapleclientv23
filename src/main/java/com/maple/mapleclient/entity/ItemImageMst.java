package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

 

 
public class ItemImageMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	 
	String id;
	
	 
	CompanyMst companyMst;
	
	
 
	private ItemMst itemMst;
	
	String branchCode;
	 
private String itemImage;
	
	private int priorioty;
	
	private Date dateUploaded;
	
	private int activePicture;
	
	private int pictureNumber;

	private String pictureName;
	
	
	private Double offerPrice;
	
	private Double mrp;
	
	private String ingredients;
	
	private String applicableTime;
	
	private String specialOfferDescription;
	  
	private String category;

	private String  processInstanceId;
	private String taskId;
	public Double getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(Double offerPrice) {
		this.offerPrice = offerPrice;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public String getIngredients() {
		return ingredients;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public String getApplicableTime() {
		return applicableTime;
	}

	public void setApplicableTime(String applicableTime) {
		this.applicableTime = applicableTime;
	}

	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ItemMst getItemMst() {
		return itemMst;
	}

	public void setItemMst(ItemMst itemMst) {
		this.itemMst = itemMst;
	}

	 
	public int getPriorioty() {
		return priorioty;
	}

	public void setPriorioty(int priorioty) {
		this.priorioty = priorioty;
	}

	public Date getDateUploaded() {
		return dateUploaded;
	}

	public void setDateUploaded(Date dateUploaded) {
		this.dateUploaded = dateUploaded;
	}

	public int getActivePicture() {
		return activePicture;
	}

	public void setActivePicture(int activePicture) {
		this.activePicture = activePicture;
	}

	public int getPictureNumber() {
		return pictureNumber;
	}

	public void setPictureNumber(int pictureNumber) {
		this.pictureNumber = pictureNumber;
	}

	public String getPictureName() {
		return pictureName;
	}

	public void setPictureName(String pictureName) {
		this.pictureName = pictureName;
	}

 
	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getSpecialOfferDescription() {
		return specialOfferDescription;
	}

	public void setSpecialOfferDescription(String specialOfferDescription) {
		this.specialOfferDescription = specialOfferDescription;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getItemImage() {
		return itemImage;
	}

	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ItemImageMst [id=" + id + ", companyMst=" + companyMst + ", itemMst=" + itemMst + ", branchCode="
				+ branchCode + ", itemImage=" + itemImage + ", priorioty=" + priorioty + ", dateUploaded="
				+ dateUploaded + ", activePicture=" + activePicture + ", pictureNumber=" + pictureNumber
				+ ", pictureName=" + pictureName + ", offerPrice=" + offerPrice + ", mrp=" + mrp + ", ingredients="
				+ ingredients + ", applicableTime=" + applicableTime + ", specialOfferDescription="
				+ specialOfferDescription + ", category=" + category + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}



}
