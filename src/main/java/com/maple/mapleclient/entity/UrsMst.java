package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UrsMst implements Serializable{
	
	
	  private String id;
		String itemId;
		Double systemStock;
		Double enteredStock;
		String userId;
		String branchCode;
		private String  processInstanceId;
		private String taskId;
		java.util.Date transDate;
		
		@JsonIgnore
		String itemName;
		
		
		@JsonIgnore
		private StringProperty itemNameProprty;
		
		@JsonIgnore
		private DoubleProperty enteredStockProperty;
		
		
		public UrsMst() {
			
			this.itemNameProprty = new SimpleStringProperty();
			this.enteredStockProperty = new SimpleDoubleProperty();
		}
		
		@JsonIgnore
		public StringProperty getitemNameProprty () {
			itemNameProprty .set(itemName);
			return itemNameProprty ;
		}
		public void setitemNameProprty (StringProperty itemNameProprty ) {
			this.itemNameProprty  = itemNameProprty ;
		}
		@JsonIgnore
		public DoubleProperty getenteredStockProperty() {
			enteredStockProperty .set(enteredStock);
			return enteredStockProperty ;
		}
		
		public java.util.Date getTransDate() {
			return transDate;
		}

		public void setTransDate(java.util.Date transDate) {
			this.transDate = transDate;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public void setenteredStockProperty(DoubleProperty enteredStockProperty ) {
			this.enteredStockProperty  = enteredStockProperty ;
		}
		public String getBranchCode()
		{
			return branchCode;
		}
		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getItemId() {
			return itemId;
		}
		public void setItemId(String itemId) {
			this.itemId = itemId;
		}
		public Double getSystemStock() {
			return systemStock;
		}
		public void setSystemStock(Double systemStock) {
			this.systemStock = systemStock;
		}
		public Double getEnteredStock() {
			return enteredStock;
		}
		public void setEnteredStock(Double enteredStock) {
			this.enteredStock = enteredStock;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
	

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "UrsMst [id=" + id + ", itemId=" + itemId + ", systemStock=" + systemStock + ", enteredStock="
					+ enteredStock + ", userId=" + userId + ", branchCode=" + branchCode + ", processInstanceId="
					+ processInstanceId + ", taskId=" + taskId + ", transDate=" + transDate + ", itemName=" + itemName
					+ "]";
		}
		
}
