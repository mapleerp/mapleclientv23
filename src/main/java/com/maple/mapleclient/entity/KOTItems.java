package com.maple.mapleclient.entity;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KOTItems {

	String tables;
	String waiter;
	ArrayList<SalesDtl> arrayofsalesdtl;
	SalesTransHdr salesTransHdr;
	
	@JsonIgnore
	private StringProperty tablesProperty;
	
	@JsonIgnore
	private StringProperty waiterProperty;
	
	
	public KOTItems() {
		this.tablesProperty = new SimpleStringProperty("");
		this.waiterProperty = new SimpleStringProperty("");
	}
	
	
	
	
	
	
	
	
	
	
	public String getTables() {
		return tables;
	}
	public void setTables(String tables) {
		this.tables = tables;
	}
	public String getWaiter() {
		return waiter;
	}
	public void setWaiter(String waiter) {
		this.waiter = waiter;
	}
	public ArrayList<SalesDtl> getArrayofsalesdtl() {
		return arrayofsalesdtl;
	}
	public void setArrayofsalesdtl(ArrayList<SalesDtl> arrayofsalesdtl) {
		this.arrayofsalesdtl = arrayofsalesdtl;
	}
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	public StringProperty getTablesProperty() {
		tablesProperty.set(tables);
		return tablesProperty;
	}
	public void setTablesProperty(StringProperty tablesProperty) {
		this.tablesProperty = tablesProperty;
	}
	public StringProperty getWaiterProperty() {
		waiterProperty.set(waiter);
		return waiterProperty;
	}
	public void setWaiterProperty(StringProperty waiterProperty) {
		this.waiterProperty = waiterProperty;
	}
	@Override
	public String toString() {
		return "KOTItems [tables=" + tables + ", waiter=" + waiter + ", arrayofsalesdtl=" + arrayofsalesdtl
				+ ", salesTransHdr=" + salesTransHdr + "]";
	}
	
	
}
