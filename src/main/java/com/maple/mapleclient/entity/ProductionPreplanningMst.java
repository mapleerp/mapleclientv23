package com.maple.mapleclient.entity;

import java.util.Date;





public class ProductionPreplanningMst {

	
	
	    String id;
	    String branchCode;
		String voucherNumber;
	    Date voucherDate;
		CompanyMst companyMst;
		private String  processInstanceId;
		private String taskId;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getBranchCode() {
			return branchCode;
		}
		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}
		public String getVoucherNumber() {
			return voucherNumber;
		}
		public void setVoucherNumber(String voucherNumber) {
			this.voucherNumber = voucherNumber;
		}
		public Date getVoucherDate() {
			return voucherDate;
		}
		public void setVoucherDate(Date voucherDate) {
			this.voucherDate = voucherDate;
		}
		public CompanyMst getCompanyMst() {
			return companyMst;
		}
		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}
		public String getProcessInstanceId() {
			return processInstanceId;
		}
		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}
		public String getTaskId() {
			return taskId;
		}
		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}
		@Override
		public String toString() {
			return "ProductionPreplanningMst [id=" + id + ", branchCode=" + branchCode + ", voucherNumber="
					+ voucherNumber + ", voucherDate=" + voucherDate + ", companyMst=" + companyMst
					+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
		}
		
		
		
	
		
}
