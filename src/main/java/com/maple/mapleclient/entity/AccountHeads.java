package com.maple.mapleclient.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AccountHeads {

	

	
	
	
	
	String partyName;
 
	String partyAddress;
	String partyContact;

	String partyId;
   

	

	@JsonIgnore
	private StringProperty partyNameProperty;
	
	@JsonIgnore
	private StringProperty partyAddressProperty;
	@JsonIgnore
	private StringProperty partyMailProperty;
	@JsonIgnore
	private StringProperty partyGstProperty;
	@JsonIgnore
	private StringProperty partyIdProperty;
	

	 String id;
		
		

		String accountName;
		

		String parentId;
		

		String groupOnly;
		

		String machineId;
		

		String taxId;
	

		String oldId;
		
		String companyName;


		String voucherType;
		

		String currencyId;
		
		String rootParentId;
		
		
		Integer serialNumber;


		private String  processInstanceId;
		

		private String taskId;
		

		
		String partyAddress1;
		
		String partyAddress2;
		
		String partyMail;
		
		String partyGst;
		
		String customerContact;
		String customerGroup;
		private String customerState;
		Integer customerRank;
		
		private String priceTypeId;
		
		private Integer creditPeriod;
		
		private Double customerDiscount;

	
		private String customerCountry;
	
		private String customerGstType;
	
		private String customerType;
		
		private String discountProperty;
		
		private String bankName;
		
		String taxInvoiceFormat;
		
		private String  bankAccountName;
		
		private String bankIfsc;
	
		private String drugLicenseNumber;

		
		

		String parentName;
		
		
		CompanyMst companyMst;
		
		
		String customerOrSupplier;
	
	     String branchName;

	
	
		
			@JsonIgnore
			private StringProperty accountNameProperty;
			
			@JsonIgnore
			private StringProperty parentNameProperty;
			
			@JsonIgnore
			private StringProperty groupProperty;
			
			@JsonIgnore
			private IntegerProperty serialNumberProperty;
			@JsonIgnore
		    private StringProperty customerorSupplierProperty;
			
			@JsonIgnore
		    private StringProperty customerAddressProperty;
			@JsonIgnore
		    private StringProperty customerContactProperty;
			@JsonIgnore
		    private StringProperty customerMailProperty;
			@JsonIgnore
		    private StringProperty customerGstProperty;
			@JsonIgnore
			private StringProperty SearchString;
			@JsonIgnore
		    private StringProperty priceTypeProperty;
			@JsonIgnore
		    private StringProperty stateProperty;
			@JsonIgnore
		    private IntegerProperty creditPeriodProperty;
			
			
			@JsonIgnore
			private StringProperty bankNameStringProperty;
			@JsonIgnore
		    private StringProperty bankAccountNameStringProperty;
			@JsonIgnore
		    private StringProperty bankIfscStringProperty;
			

			public AccountHeads() {
				
				this.accountNameProperty = new SimpleStringProperty("");
				this.parentNameProperty = new SimpleStringProperty("");
				this.groupProperty = new SimpleStringProperty("");
				this.serialNumberProperty = new SimpleIntegerProperty();
				this.customerorSupplierProperty= new SimpleStringProperty("");
				this.customerAddressProperty =new SimpleStringProperty("");
				this.customerContactProperty=new SimpleStringProperty("");
				this.customerMailProperty= new SimpleStringProperty("");
				this.customerGstProperty= new SimpleStringProperty("");
				this.priceTypeProperty= new SimpleStringProperty("");
				this.stateProperty = new SimpleStringProperty("");
				this.creditPeriod=0;
			}
			
			




			public String getCompanyName() {
				return companyName;
			}






			public void setCompanyName(String companyName) {
				this.companyName = companyName;
			}






			public String getOldId() {
				return oldId;
			}






			public void setOldId(String oldId) {
				this.oldId = oldId;
			}






			public String getPartyAddress1() {
				return partyAddress1;
			}






			public void setPartyAddress1(String partyAddress1) {
				this.partyAddress1 = partyAddress1;
			}






			public String getPartyAddress2() {
				return partyAddress2;
			}






			public void setPartyAddress2(String partyAddress2) {
				this.partyAddress2 = partyAddress2;
			}


			public String getCustomerContact() {
				return customerContact;
			}






			public void setCustomerContact(String customerContact) {
				this.customerContact = customerContact;
			}






			public String getCustomerGroup() {
				return customerGroup;
			}






			public void setCustomerGroup(String customerGroup) {
				this.customerGroup = customerGroup;
			}






			public String getCustomerState() {
				return customerState;
			}






			public void setCustomerState(String customerState) {
				this.customerState = customerState;
			}






			public Integer getCustomerRank() {
				return customerRank;
			}






			public void setCustomerRank(Integer customerRank) {
				this.customerRank = customerRank;
			}






			public String getPriceTypeId() {
				return priceTypeId;
			}






			public void setPriceTypeId(String priceTypeId) {
				this.priceTypeId = priceTypeId;
			}


			public Double getCustomerDiscount() {
				return customerDiscount;
			}






			public void setCustomerDiscount(Double customerDiscount) {
				this.customerDiscount = customerDiscount;
			}






			public String getCustomerCountry() {
				return customerCountry;
			}






			public void setCustomerCountry(String customerCountry) {
				this.customerCountry = customerCountry;
			}






			public String getCustomerGstType() {
				return customerGstType;
			}






			public void setCustomerGstType(String customerGstType) {
				this.customerGstType = customerGstType;
			}






			public String getCustomerType() {
				return customerType;
			}






			public void setCustomerType(String customerType) {
				this.customerType = customerType;
			}






			public String getDiscountProperty() {
				return discountProperty;
			}






			public void setDiscountProperty(String discountProperty) {
				this.discountProperty = discountProperty;
			}






			public String getBankName() {
				return bankName;
			}






			public void setBankName(String bankName) {
				this.bankName = bankName;
			}






			public String getTaxInvoiceFormat() {
				return taxInvoiceFormat;
			}






			public void setTaxInvoiceFormat(String taxInvoiceFormat) {
				this.taxInvoiceFormat = taxInvoiceFormat;
			}






			public String getBankAccountName() {
				return bankAccountName;
			}






			public void setBankAccountName(String bankAccountName) {
				this.bankAccountName = bankAccountName;
			}






			public String getBankIfsc() {
				return bankIfsc;
			}






			public void setBankIfsc(String bankIfsc) {
				this.bankIfsc = bankIfsc;
			}






			public String getDrugLicenseNumber() {
				return drugLicenseNumber;
			}






			public void setDrugLicenseNumber(String drugLicenseNumber) {
				this.drugLicenseNumber = drugLicenseNumber;
			}






			public CompanyMst getCompanyMst() {
				return companyMst;
			}






			public void setCompanyMst(CompanyMst companyMst) {
				this.companyMst = companyMst;
			}






			public String getCustomerOrSupplier() {
				return customerOrSupplier;
			}






			public void setCustomerOrSupplier(String customerOrSupplier) {
				this.customerOrSupplier = customerOrSupplier;
			}







			public Integer getSerialNumber() {
				return serialNumber;
			}



			public void setSerialNumber(Integer serialNumber) {
				this.serialNumber = serialNumber;
			}



			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public String getCurrencyId() {
				return currencyId;
			}

			public String getRootParentId() {
				return rootParentId;
			}

			public void setRootParentId(String rootParentId) {
				this.rootParentId = rootParentId;
			}

			public void setCurrencyId(String currencyId) {
				this.currencyId = currencyId;
			}

			public String getAccountName() {
				return accountName;
			}

			public void setAccountName(String accountName) {
				this.accountName = accountName;
			}

			public String getParentId() {
				return parentId;
			}

			public void setParentId(String parentId) {
				this.parentId = parentId;
			}

			public String getGroupOnly() {
				return groupOnly;
			}

			public void setGroupOnly(String groupOnly) {
				this.groupOnly = groupOnly;
			}

			public String getMachineId() {
				return machineId;
			}

			public void setMachineId(String machineId) {
				this.machineId = machineId;
			}

			public String getTaxId() {
				return taxId;
			}

			public void setTaxId(String taxId) {
				this.taxId = taxId;
			}

			@JsonIgnore
			public StringProperty getAccountNameProperty() {

				accountNameProperty.set(accountName);
				return accountNameProperty;
			}

			public void setAccountNameProperty(StringProperty accountNameProperty) {
				accountNameProperty = accountNameProperty;
			}
			
			public String getParentName() {
				return parentName;
			}

			public void setParentName(String parentName) {
				this.parentName = parentName;
			}

			@JsonIgnore
			public StringProperty getparentNameProperty() {

				parentNameProperty.set(parentName);
				return parentNameProperty;
			}

			public void setparentNameProperty(String parentName) {
				parentName = parentName;
			}
			@JsonIgnore
			public StringProperty getgroupProperty() {

				groupProperty.set(groupOnly);
				return groupProperty;
			}

			public void setgroupProperty(String groupOnly) {
				groupOnly = groupOnly;
			}

			
			public String getVoucherType() {
				return voucherType;
			}

			public void setVoucherType(String voucherType) {
				this.voucherType = voucherType;
			}

			

			public String getBranchName() {
				return branchName;
			}

			public void setBranchName(String branchName) {
				this.branchName = branchName;
			}
			
			

			public IntegerProperty getSerialNumberProperty() {
				serialNumberProperty.set(serialNumber);
				return serialNumberProperty;
			}

			public void setSerialNumberProperty(IntegerProperty serialNumberProperty) {
				this.serialNumberProperty = serialNumberProperty;
			}

			



			public String getProcessInstanceId() {
				return processInstanceId;
			}



			public void setProcessInstanceId(String processInstanceId) {
				this.processInstanceId = processInstanceId;
			}



			public String getTaskId() {
				return taskId;
			}



			public void setTaskId(String taskId) {
				this.taskId = taskId;
			}






			public StringProperty getCustomerAddressProperty() {
				customerAddressProperty.set(partyAddress1);
				return customerAddressProperty;
			}






			public void setCustomerAddressProperty(StringProperty customerAddressProperty) {
				this.customerAddressProperty = customerAddressProperty;
			}






			public StringProperty getCustomerContactProperty() {
				customerContactProperty.set(customerContact);
				return customerContactProperty;
			}






			public void setCustomerContactProperty(StringProperty customerContactProperty) {
				this.customerContactProperty = customerContactProperty;
			}






			public StringProperty getCustomerMailProperty() {
				customerMailProperty.set(partyMail);
				return customerMailProperty;
			}






			public void setCustomerMailProperty(StringProperty customerMailProperty) {
				this.customerMailProperty = customerMailProperty;
			}






			public StringProperty getCustomerGstProperty() {
				customerGstProperty.set(partyGst);
				return customerGstProperty;
			}






			public void setCustomerGstProperty(StringProperty customerGstProperty) {
				this.customerGstProperty = customerGstProperty;
			}






			public StringProperty getSearchString() {
				return SearchString;
			}






			public void setSearchString(StringProperty searchString) {
				SearchString = searchString;
			}






			public StringProperty getPriceTypeProperty() {
				priceTypeProperty.set(priceTypeId);
				return priceTypeProperty;
			}






			public void setPriceTypeProperty(StringProperty priceTypeProperty) {
				this.priceTypeProperty = priceTypeProperty;
			}






			public StringProperty getStateProperty() {
				stateProperty.set(customerState);
				return stateProperty;
			}






			public void setStateProperty(StringProperty stateProperty) {
				this.stateProperty = stateProperty;
			}






			public IntegerProperty getCreditPeriodProperty() {
				creditPeriodProperty.set(creditPeriod);
				return creditPeriodProperty;
			}






			public void setCreditPeriodProperty(IntegerProperty creditPeriodProperty) {
				this.creditPeriodProperty = creditPeriodProperty;
			}






			public StringProperty getBankNameStringProperty() {
				bankNameStringProperty.set(bankName);
				return bankNameStringProperty;
			}






			public void setBankNameStringProperty(StringProperty bankNameStringProperty) {
				this.bankNameStringProperty = bankNameStringProperty;
			}






			public StringProperty getBankAccountNameStringProperty() {
				bankAccountNameStringProperty.set(bankAccountName);
				return bankAccountNameStringProperty;
			}






			public void setBankAccountNameStringProperty(StringProperty bankAccountNameStringProperty) {
				
				this.bankAccountNameStringProperty = bankAccountNameStringProperty;
			}






			public StringProperty getBankIfscStringProperty() {
				bankIfscStringProperty.set(bankIfsc);
				return bankIfscStringProperty;
			}






			public void setBankIfscStringProperty(StringProperty bankIfscStringProperty) {
				this.bankIfscStringProperty = bankIfscStringProperty;
			}






			public StringProperty getCustomerorSupplierProperty() {
				customerorSupplierProperty.set(customerOrSupplier);
				return customerorSupplierProperty;
			}






			public void setCustomerorSupplierProperty(StringProperty customerorSupplierProperty) {
				this.customerorSupplierProperty = customerorSupplierProperty;
			}

	public String getPartyName() {
		return partyName;
	}



	public String getPartyAddress() {
		return partyAddress;
	}



			


	public String getPartyContact() {
		return partyContact;
	}



	public String getPartyMail() {
		return partyMail;
	}


		







	public String getPartyGst() {
		return partyGst;
	}



	public String getPartyId() {
		return partyId;
	}



	public Integer getCreditPeriod() {
		return creditPeriod;
	}



	public StringProperty getParentNameProperty() {
		return parentNameProperty;
	}



	public StringProperty getGroupProperty() {
		return groupProperty;
	}



	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}



	public void setPartyAddress(String partyAddress) {
		this.partyAddress = partyAddress;
	}



	public void setPartyContact(String partyContact) {
		this.partyContact = partyContact;
	}



	public void setPartyMail(String partyMail) {
		this.partyMail = partyMail;
	}



	public void setPartyGst(String partyGst) {
		this.partyGst = partyGst;
	}



	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}



	public void setCreditPeriod(Integer creditPeriod) {
		this.creditPeriod = creditPeriod;
	}



	public void setParentNameProperty(StringProperty parentNameProperty) {
		this.parentNameProperty = parentNameProperty;
	}



	public void setGroupProperty(StringProperty groupProperty) {
		this.groupProperty = groupProperty;
	}



	public StringProperty getPartyNameProperty() {
		
		partyNameProperty.set(partyName);
	    return partyNameProperty;
	}



	public StringProperty getPartyAddressProperty() {
		
		if(null!=partyAddress) {
			partyAddressProperty.set(partyAddress);
		}
		return partyAddressProperty;
	}



	public StringProperty getPartyMailProperty() {
		
		partyMailProperty.set(partyMail);
	    return partyMailProperty;
	}



	public StringProperty getPartyGstProperty() {
		
		partyGstProperty.set(partyGst);
        return partyGstProperty;
	}



	public StringProperty getPartyIdProperty() {
		
		partyIdProperty.set(partyId);
        return partyIdProperty;
	}



	public void setPartyNameProperty(StringProperty partyNameProperty) {
		this.partyNameProperty = partyNameProperty;
	}



	public void setPartyAddressProperty(StringProperty partyAddressProperty) {
		this.partyAddressProperty = partyAddressProperty;
	}



	public void setPartyMailProperty(StringProperty partyMailProperty) {
		this.partyMailProperty = partyMailProperty;
	}



	public void setPartyGstProperty(StringProperty partyGstProperty) {
		this.partyGstProperty = partyGstProperty;
	}



	public void setPartyIdProperty(StringProperty partyIdProperty) {
		this.partyIdProperty = partyIdProperty;
	}






	@Override
	public String toString() {
		return "AccountHeads [companyName=" + companyName + ", partyName=" + partyName + ", partyAddress="
				+ partyAddress + ", partyContact=" + partyContact + ", partyId=" + partyId + ", partyNameProperty="
				+ partyNameProperty + ", partyAddressProperty=" + partyAddressProperty + ", partyMailProperty="
				+ partyMailProperty + ", partyGstProperty=" + partyGstProperty + ", partyIdProperty=" + partyIdProperty
				+ ", id=" + id + ", accountName=" + accountName + ", parentId=" + parentId + ", groupOnly=" + groupOnly
				+ ", machineId=" + machineId + ", taxId=" + taxId + ", oldId=" + oldId + ", voucherType=" + voucherType
				+ ", currencyId=" + currencyId + ", rootParentId=" + rootParentId + ", serialNumber=" + serialNumber
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", partyAddress1=" + partyAddress1
				+ ", partyAddress2=" + partyAddress2 + ", partyMail=" + partyMail + ", partyGst=" + partyGst
				+ ", customerContact=" + customerContact + ", customerGroup=" + customerGroup + ", customerState="
				+ customerState + ", customerRank=" + customerRank + ", priceTypeId=" + priceTypeId + ", creditPeriod="
				+ creditPeriod + ", customerDiscount=" + customerDiscount + ", customerCountry=" + customerCountry
				+ ", customerGstType=" + customerGstType + ", customerType=" + customerType + ", discountProperty="
				+ discountProperty + ", bankName=" + bankName + ", taxInvoiceFormat=" + taxInvoiceFormat
				+ ", bankAccountName=" + bankAccountName + ", bankIfsc=" + bankIfsc + ", drugLicenseNumber="
				+ drugLicenseNumber + ", parentName=" + parentName + ", companyMst=" + companyMst
				+ ", customerOrSupplier=" + customerOrSupplier + ", branchName=" + branchName + ", accountNameProperty="
				+ accountNameProperty + ", parentNameProperty=" + parentNameProperty + ", groupProperty="
				+ groupProperty + ", serialNumberProperty=" + serialNumberProperty + ", customerorSupplierProperty="
				+ customerorSupplierProperty + ", customerAddressProperty=" + customerAddressProperty
				+ ", customerContactProperty=" + customerContactProperty + ", customerMailProperty="
				+ customerMailProperty + ", customerGstProperty=" + customerGstProperty + ", SearchString="
				+ SearchString + ", priceTypeProperty=" + priceTypeProperty + ", stateProperty=" + stateProperty
				+ ", creditPeriodProperty=" + creditPeriodProperty + ", bankNameStringProperty="
				+ bankNameStringProperty + ", bankAccountNameStringProperty=" + bankAccountNameStringProperty
				+ ", bankIfscStringProperty=" + bankIfscStringProperty + "]";
	}





	

}
