package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PriceDefinition {
	
	private String id;
	private String itemId;
	private String priceId;
	private double amount;
	private String itemName;
	private String priceType;
	private String unitId;
	private String  processInstanceId;
	private String taskId;
	
CompanyMst companyMst;
	
	String branchCode;
	
	
	
	public PriceDefinition() {
		
		this.itemNameProperty = new SimpleStringProperty("");
		this.priceTypeProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty();
		this.startDate = new SimpleObjectProperty<LocalDate>(null);
		this.endDate = new SimpleObjectProperty<LocalDate>(null);
		this.unitNameProperty = new SimpleStringProperty("");
	}

	@JsonProperty("startDate")
	public java.sql.Date getStartDate( ) {
		if(null==this.startDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.startDate.get() );
		}
	 
		
	}
	
   public void setStartDate (java.sql.Date startDateProperty) {
						if(null!=startDateProperty)
		this.startDate.set(startDateProperty.toLocalDate());
	}/////
   @JsonProperty("endDate")
	public java.sql.Date getEndDate ( ) {
		if(null==this.endDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.endDate.get() );
		}
	 
		
	}
	
  public void setEndDate (java.sql.Date endDateProperty) {
						if(null!=endDateProperty)
		this.endDate.set(endDateProperty.toLocalDate());
	}

	@JsonIgnore
	private ObjectProperty<LocalDate> startDate;
	String unitName;
	@JsonIgnore
	private ObjectProperty<LocalDate> endDate;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty priceTypeProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;

	
	@JsonIgnore
	private StringProperty unitNameProperty;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	


	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	@JsonIgnore
	public String getItemName() {
		return itemName;
	}
	@JsonIgnore
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getPriceType() {
		return priceType;
	}
	@JsonIgnore
	  
	public String getUnitName() {
	return unitName;
}

public void setUnitName(String unitName) {
	if(null != unitName)
	this.unitName = unitName;
}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public String getPriceId() {
		return priceId;
	}

	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public ObjectProperty<LocalDate> getStartDateProperty( ) {
		return startDate;
	}
 
	public void setStartDateProperty(ObjectProperty<LocalDate> startDateProperty) {
		this.startDate = startDate;
	}
	
	public ObjectProperty<LocalDate> getEndDateProperty( ) {
		return endDate;
	}
 
	public void setEndDateProperty(ObjectProperty<LocalDate> endDate) {
		this.endDate = endDate;
	}
	
	public StringProperty getPriceTypeProperty() {

		priceTypeProperty.set(priceType);
		return priceTypeProperty;
	}

	public void setPriceTypeProperty(StringProperty priceTypeProperty) {
		priceTypeProperty = priceTypeProperty;
	}
	
	public StringProperty getItemNameProperty() {

		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		itemNameProperty = itemNameProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	public StringProperty getUnitNameProperty() {

		unitNameProperty.set(unitName);
		return unitNameProperty;
	}

	public void setUnitNameProperty(String unitName) {
		unitName = unitName;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PriceDefinition [id=" + id + ", itemId=" + itemId + ", priceId=" + priceId + ", amount=" + amount
				+ ", itemName=" + itemName + ", priceType=" + priceType + ", unitId=" + unitId + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst + ", branchCode=" + branchCode
				+ ", unitName=" + unitName + "]";
	}

	
	

}
