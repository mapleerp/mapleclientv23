package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PurchaseAdditionalExpenseHdr{

    private String id;
	String itemId;

	CompanyMst companyMst;

	PurchaseDtl purchaseDtl;
	String voucherNumber;
	String itemName;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	public PurchaseAdditionalExpenseHdr() {
		
		this.itemNameProperty = new SimpleStringProperty();
	}
	
	 public StringProperty getitemNameProperty() {
		 itemNameProperty.set(itemName);
				return itemNameProperty;
			}
			public void setbatchProperty(String itemName) {
				this.itemName = itemName;
			}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public PurchaseDtl getPurchaseDtl() {
		return purchaseDtl;
	}
	public void setPurchaseDtl(PurchaseDtl purchaseDtl) {
		this.purchaseDtl = purchaseDtl;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PurchaseAdditionalExpenseHdr [id=" + id + ", itemId=" + itemId + ", companyMst=" + companyMst
				+ ", purchaseDtl=" + purchaseDtl + ", voucherNumber=" + voucherNumber + ", itemName=" + itemName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
