package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FamilyMst {


	public FamilyMst()
	{
		this.familyNameProperty= new SimpleStringProperty("");
		this.familyMstIdProperty= new SimpleStringProperty("");
		
		this.orgIdProperty= new SimpleStringProperty("");
		this.familyLocationPlaceProperty= new SimpleStringProperty("");
		this.familyLocationLatitudeProperty= new SimpleStringProperty("");
		this.familyLocationLongtitudeProperty= new SimpleStringProperty("");
		this.familyLandLineProperty= new SimpleStringProperty("");
		this.headOfFamilyProperty= new SimpleStringProperty("");
		
	}
	
	
	
	
	 @JsonIgnore
	    private StringProperty familyNameProperty;
	  @JsonIgnore
	    private StringProperty orgIdProperty;
	  @JsonIgnore
	    private StringProperty familyLocationPlaceProperty;
	  @JsonIgnore
	    private StringProperty familyLocationLatitudeProperty;
	  @JsonIgnore
	    private StringProperty familyLocationLongtitudeProperty;
	  @JsonIgnore
	    private StringProperty familyLandLineProperty;
	  @JsonIgnore
	    private StringProperty headOfFamilyProperty;
	  @JsonIgnore
	    private StringProperty familyMstIdProperty;
	  
	  
	  
	public StringProperty getFamilyMstIdProperty() {
		familyMstIdProperty.set(familyMstId);
		return familyMstIdProperty;
	}


	public void setFamilyMstIdProperty(StringProperty familyMstIdProperty) {
		this.familyMstIdProperty = familyMstIdProperty;
	}



	String id;
	
	OrgMst orgMst;
	
	String familyMstId;
	

	String headOfFamily;

	String familyName;
	
	
	String familyLocationLatitude;

	String familyLocationLongitude;

	String familyLocationPlace;
	

	String familyLandLine;
	
	String orgId;
	
	
	private Date creationDate;
	    

	private int createdBy;
	
	private String  processInstanceId;
	private String taskId;
	


	public StringProperty getFamilyNameProperty() {
		familyNameProperty.set(familyName);
		return familyNameProperty;
	}


	public void setFamilyNameProperty(StringProperty familyNameProperty) {
		this.familyNameProperty = familyNameProperty;
	}


	public StringProperty getOrgIdProperty() {
		orgIdProperty.set(orgId);
		return orgIdProperty;
	}


	public void setOrgIdProperty(StringProperty orgIdProperty) {
		this.orgIdProperty = orgIdProperty;
	}


	public StringProperty getFamilyLocationPlaceProperty() {
		familyLocationPlaceProperty.set(familyLocationPlace);
		return familyLocationPlaceProperty;
	}


	public void setFamilyLocationPlaceProperty(StringProperty familyLocationPlaceProperty) {
		this.familyLocationPlaceProperty = familyLocationPlaceProperty;
	}


	public StringProperty getFamilyLocationLatitudeProperty() {
		familyLocationLatitudeProperty.set(familyLocationLatitude);
		return familyLocationLatitudeProperty;
	}


	public void setFamilyLocationLatitudeProperty(StringProperty familyLocationLatitudeProperty) {
		
		this.familyLocationLatitudeProperty = familyLocationLatitudeProperty;
	}


	public StringProperty getFamilyLocationLongtitudeProperty() {
		familyLocationLongtitudeProperty.set(familyLocationLongitude);
		return familyLocationLongtitudeProperty;
	}


	public void setFamilyLocationLongtitudeProperty(StringProperty familyLocationLongtitudeProperty) {
		this.familyLocationLongtitudeProperty = familyLocationLongtitudeProperty;
	}


	public StringProperty getFamilyLandLineProperty() {
		familyLandLineProperty.set(familyLandLine);
		return familyLandLineProperty;
	}


	public void setFamilyLandLineProperty(StringProperty familyLandLineProperty) {
		this.familyLandLineProperty = familyLandLineProperty;
	}


	public StringProperty getHeadOfFamilyProperty() {
		headOfFamilyProperty.set(headOfFamily);
		return headOfFamilyProperty;
	}


	public void setHeadOfFamilyProperty(StringProperty headOfFamilyProperty) {
		this.headOfFamilyProperty = headOfFamilyProperty;
	}


	public OrgMst getOrgMst() {
		return orgMst;
}


	public void setOrgMst(OrgMst orgMst) {
		this.orgMst = orgMst;
	}


	public String getFamilyMstId() {
		return familyMstId;
	}


	public void setFamilyMstId(String familyMstId) {
		this.familyMstId = familyMstId;
	}


	public String getHeadOfFamily() {
		return headOfFamily;
	}


	public void setHeadOfFamily(String headOfFamily) {
		this.headOfFamily = headOfFamily;
	}


	public String getFamilyName() {
		return familyName;
	}


	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}


	public String getFamilyLocationLatitude() {
		return familyLocationLatitude;
	}


	public void setFamilyLocationLatitude(String familyLocationLatitude) {
		this.familyLocationLatitude = familyLocationLatitude;
	}


	public String getFamilyLocationLongitude() {
		return familyLocationLongitude;
	}


	public void setFamilyLocationLongitude(String familyLocationLongitude) {
		this.familyLocationLongitude = familyLocationLongitude;
	}


	public String getFamilyLocationPlace() {
		return familyLocationPlace;
	}


	public void setFamilyLocationPlace(String familyLocationPlace) {
		this.familyLocationPlace = familyLocationPlace;
	}


	public String getFamilyLandLine() {
		return familyLandLine;
	}


	public void setFamilyLandLine(String familyLandLine) {
		this.familyLandLine = familyLandLine;
	}


	public String getOrgId() {
		return orgId;
	}


	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}


	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public int getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "FamilyMst [id=" + id + ", orgMst=" + orgMst + ", familyMstId=" + familyMstId + ", headOfFamily="
				+ headOfFamily + ", familyName=" + familyName + ", familyLocationLatitude=" + familyLocationLatitude
				+ ", familyLocationLongitude=" + familyLocationLongitude + ", familyLocationPlace="
				+ familyLocationPlace + ", familyLandLine=" + familyLandLine + ", orgId=" + orgId + ", creationDate="
				+ creationDate + ", createdBy=" + createdBy + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
	
}
