package com.maple.mapleclient.entity;

import com.maple.report.entity.DayBook;

public class SalesFinalSave {

	
	SalesTransHdr salesTransHdr;
	SalesReceipts salesReceipts;
	AccountReceivable accountReceivable;
	DayBook dayBook;
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	public SalesReceipts getSalesReceipts() {
		return salesReceipts;
	}
	public void setSalesReceipts(SalesReceipts salesReceipts) {
		this.salesReceipts = salesReceipts;
	}
	public AccountReceivable getAccountReceivable() {
		return accountReceivable;
	}
	public void setAccountReceivable(AccountReceivable accountReceivable) {
		this.accountReceivable = accountReceivable;
	}
	public DayBook getDayBook() {
		return dayBook;
	}
	public void setDayBook(DayBook dayBook) {
		this.dayBook = dayBook;
	}
	
	
}
