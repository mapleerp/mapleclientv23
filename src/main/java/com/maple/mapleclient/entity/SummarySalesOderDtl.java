package com.maple.mapleclient.entity;

public class SummarySalesOderDtl {
 
private Double totalQty;
    
	private Double totalAmount;
    
	private Double totalDiscount;
   
	private Double totalTax;
    
    private Double totalCessAmt;
    private String  processInstanceId;
	private String taskId;
	public Double getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(Double totalQty) {
		this.totalQty = totalQty;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(Double totalTax) {
		this.totalTax = totalTax;
	}

	public Double getTotalCessAmt() {
		return totalCessAmt;
	}

	public void setTotalCessAmt(Double totalCessAmt) {
		this.totalCessAmt = totalCessAmt;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SummarySalesOderDtl [totalQty=" + totalQty + ", totalAmount=" + totalAmount + ", totalDiscount="
				+ totalDiscount + ", totalTax=" + totalTax + ", totalCessAmt=" + totalCessAmt + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

    
    
}
