package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductMst {

	String id;
	String productName;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty productNameProperty;
	
	
	public ProductMst() {
		
		this.productNameProperty = new SimpleStringProperty();
	}
	
	@JsonIgnore
	 public StringProperty getproductNameProperty() {
		productNameProperty.set(productName);
			return productNameProperty;
		}
		
		public void setproductNameProperty(String productName) {
			this.productName = productName;
		}
		
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	 
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ProductMst [id=" + id + ", productName=" + productName + ", branchCode=" + branchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
}
