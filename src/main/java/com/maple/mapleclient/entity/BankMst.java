package com.maple.mapleclient.entity;

public class BankMst {
	String id;

	String bankName;
	String bankPlace;
	String bankAccount;
	String ifcCode;
	String printInInvoice;
	
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankPlace() {
		return bankPlace;
	}
	public void setBankPlace(String bankPlace) {
		this.bankPlace = bankPlace;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getIfcCode() {
		return ifcCode;
	}
	public void setIfcCode(String ifcCode) {
		this.ifcCode = ifcCode;
	}
	public String getPrintInInvoice() {
		return printInInvoice;
	}
	public void setPrintInInvoice(String printInInvoice) {
		this.printInInvoice = printInInvoice;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "BankMst [id=" + id + ", bankName=" + bankName + ", bankPlace=" + bankPlace + ", bankAccount="
				+ bankAccount + ", ifcCode=" + ifcCode + ", printInInvoice=" + printInInvoice + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
