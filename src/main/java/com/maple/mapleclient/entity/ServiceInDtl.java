package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ServiceInDtl {
	 
		
	     String id;
		
		 Double qty;
		 String model;
		 String productId;
		 String brandId;
		 String serialId;
		 String complaints;
		 String underWarranty;
		 String observation;
		
		 ServiceInHdr serviceInHdr;
		 
		 String itemName;
		 String brandName;
		 String productName;
		 String LadiesOrGents;
		 String strap;
		 String Battery;
		 private String  processInstanceId;
			private String taskId;
		 @JsonIgnore
		 private StringProperty complaintProperty;
		 
		 @JsonIgnore
		 private StringProperty itemNameProperty;
		 
		 @JsonIgnore
		 private StringProperty modelProperty;
		 
		 @JsonIgnore
		 private StringProperty productProperty;
		 
		 @JsonIgnore
		 private StringProperty genderProperty ;
		 
		 
		 @JsonIgnore
		 private StringProperty StrapProperty;
		 
		 
		 @JsonIgnore
		 private StringProperty warrantyProperty;
		 
		 
		 @JsonIgnore
		 private StringProperty batteryProperty;
		 
		 
		 @JsonIgnore
		 private StringProperty brandProperty;
		 
		 @JsonIgnore 
		 private DoubleProperty qtyProperty;
		 
		 
		public ServiceInDtl() {
			
			this.itemNameProperty = new SimpleStringProperty("");
			this.modelProperty = new SimpleStringProperty("");
			this.productProperty =new SimpleStringProperty("");
			this.brandProperty = new SimpleStringProperty("");
			this.qtyProperty = new SimpleDoubleProperty();
			this.complaintProperty = new SimpleStringProperty("");
			this.StrapProperty = new SimpleStringProperty("");
			this.genderProperty = new SimpleStringProperty("");
			this.warrantyProperty = new SimpleStringProperty("");
			this.batteryProperty = new SimpleStringProperty("");

		}
		public String getItemName() {
			return itemName;
		}
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
	
		public Double getQty() {
			return qty;
		}
		public void setQty(Double qty) {
			this.qty = qty;
		}
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model = model;
		}
		public String getProductId() {
			return productId;
		}
		public void setProductId(String productId) {
			this.productId = productId;
		}
		public String getBrandId() {
			return brandId;
		}
		public void setBrandId(String brandId) {
			this.brandId = brandId;
		}
		
	
		public String getSerialId() {
			return serialId;
		}
		public void setSerialId(String serialId) {
			this.serialId = serialId;
		}
		public String getComplaints() {
			return complaints;
		}
		public void setComplaints(String complaints) {
			this.complaints = complaints;
		}
		public String getUnderWarranty() {
			return underWarranty;
		}
		public void setUnderWarranty(String underWarranty) {
			this.underWarranty = underWarranty;
		}
		public String getObservation() {
			return observation;
		}
		public void setObservation(String observation) {
			this.observation = observation;
		}
		
		public ServiceInHdr getServiceInHdr() {
			return serviceInHdr;
		}
		public void setServiceInHdr(ServiceInHdr serviceInHdr) {
			this.serviceInHdr = serviceInHdr;
		}
		
		@JsonIgnore
		
		public String getBrandName() {
			return brandName;
		}
		public void setBrandName(String brandName) {
			this.brandName = brandName;
		}
		public String getProductName() {
			return productName;
		}
		public void setProductName(String productName) {
			this.productName = productName;
		}

	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	public StringProperty getcomplaintProperty() {
		complaintProperty.set(complaints);
		return complaintProperty;
	}

	public void setcomplaintProperty(String complaints) {
		this.complaints = complaints;
	}
	
	
	@JsonIgnore
	public StringProperty getModelProperty() {
		modelProperty.set(model);
		return modelProperty;
	}

	public void setModelProperty(String model) {
		this.model = model;
	}
	
	@JsonIgnore
	public StringProperty getproductProperty() {
		productProperty.set(productName);
		return productProperty;
	}

	public void setproductProperty(String productName) {
		this.productName = productName;
	}
	@JsonIgnore
	public StringProperty getbrandProperty() {
		brandProperty.set(brandName);
		return brandProperty;
	}

	public void setbrandProperty(String brandName) {
		this.brandName = brandName;
	}
	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	
	public String getLadiesOrGents() {
		return LadiesOrGents;
	}
	public void setLadiesOrGents(String ladiesOrGents) {
		LadiesOrGents = ladiesOrGents;
	}
	
	public String getStrap() {
		return strap;
	}
	public void setStrap(String strap) {
		this.strap = strap;
	}
	public String getBattery() {
		return Battery;
	}
	public void setBattery(String battery) {
		Battery = battery;
	}
	
	
	
	
	public StringProperty getGenderProperty() {
		genderProperty.set(LadiesOrGents);
		return genderProperty;
	}
	public void setGenderProperty(StringProperty genderProperty) {
		this.genderProperty = genderProperty;
	}
	public StringProperty getStrapProperty() {
		StrapProperty.set(strap);
		return StrapProperty;
	}
	public void setStrapProperty(StringProperty strapProperty) {
		StrapProperty = strapProperty;
	}
	public StringProperty getWarrantyProperty() {
		warrantyProperty.set(underWarranty);
		return warrantyProperty;
	}
	public void setWarrantyProperty(StringProperty warrantyProperty) {
		this.warrantyProperty = warrantyProperty;
	}
	public StringProperty getBatteryProperty() {
		batteryProperty.set(Battery);
		return batteryProperty;
	}
	public void setBatteryProperty(StringProperty batteryProperty) {
		this.batteryProperty = batteryProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ServiceInDtl [id=" + id + ", qty=" + qty + ", model=" + model + ", productId=" + productId
				+ ", brandId=" + brandId + ", serialId=" + serialId + ", complaints=" + complaints + ", underWarranty="
				+ underWarranty + ", observation=" + observation + ", serviceInHdr=" + serviceInHdr + ", itemName="
				+ itemName + ", brandName=" + brandName + ", productName=" + productName + ", LadiesOrGents="
				+ LadiesOrGents + ", strap=" + strap + ", Battery=" + Battery + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
