package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockTransferInReturnDtl {
	
		String id;
	
		String barCode;
		String batchCode;
		Double rate;
		Double qty;
		Double mrp;
		Date expDate;
		Double amount;
		String itemName;
		
		private String  processInstanceId;
		private String taskId;
		StockTransferInReturnHdr stockTransferInReturnHdr;
		
		@JsonIgnore
		private StringProperty batchCodeProperty;
		@JsonIgnore
		private StringProperty barCodeProperty;
		@JsonIgnore
		private DoubleProperty rateProperty;
		@JsonIgnore
		private DoubleProperty qtyProperty;
		@JsonIgnore
		private DoubleProperty mrpProperty;
		
		@JsonIgnore
		private ObjectProperty<LocalDate>expDateProperty;
		@JsonIgnore
		private DoubleProperty amountProperty;
		@JsonIgnore
		private StringProperty itemNameProperty;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getBarCode() {
			return barCode;
		}
		public void setBarCode(String barCode) {
			this.barCode = barCode;
		}
		public String getBatchCode() {
			return batchCode;
		}
		public void setBatchCode(String batchCode) {
			this.batchCode = batchCode;
		}
		public Double getRate() {
			return rate;
		}
		public void setRate(Double rate) {
			this.rate = rate;
		}
		public Double getQty() {
			return qty;
		}
		public void setQty(Double qty) {
			this.qty = qty;
		}
		public Double getMrp() {
			return mrp;
		}
		public void setMrp(Double mrp) {
			this.mrp = mrp;
		}
		public Date getExpDate() {
			return expDate;
		}
		public void setExpDate(Date expDate) {
			this.expDate = expDate;
		}
		public Double getAmount() {
			return amount;
		}
		public void setAmount(Double amount) {
			this.amount = amount;
		}
		public String getItemName() {
			return itemName;
		}
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}
		public String getProcessInstanceId() {
			return processInstanceId;
		}
		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}
		public String getTaskId() {
			return taskId;
		}
		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}
	
		
public	StockTransferInReturnDtl ()
						{
			
			this.batchCodeProperty = new SimpleStringProperty();
			this.barCodeProperty =new SimpleStringProperty();
			this.rateProperty =new SimpleDoubleProperty();
			this.qtyProperty =new SimpleDoubleProperty();
			this.mrpProperty =new SimpleDoubleProperty();
			this.expDateProperty =new SimpleObjectProperty<LocalDate>(null);
			this.amountProperty =new SimpleDoubleProperty();
			this.itemNameProperty =new SimpleStringProperty();
						}
		@JsonIgnore
		public StringProperty getBatchCodeProperty() {
			batchCodeProperty.set(batchCode);
			return batchCodeProperty;
		}
		public void setBatchCodeProperty(StringProperty batchCodeProperty) {
			this.batchCodeProperty = batchCodeProperty;
		}
		@JsonIgnore
		public StringProperty getBarCodeProperty() {
			barCodeProperty.set(barCode);	
			return barCodeProperty;
		}
		public void setBarCodeProperty(StringProperty barCodeProperty) {
			this.barCodeProperty = barCodeProperty;
		}
		@JsonIgnore
		public DoubleProperty getRateProperty() {
			rateProperty.set(rate);
			return rateProperty;
		}
		public void setRateProperty(DoubleProperty rateProperty) {
			this.rateProperty = rateProperty;
		}
		@JsonIgnore
		public DoubleProperty getQtyProperty() {
			qtyProperty.set(qty);
			return qtyProperty;
		}
		public void setQtyProperty(DoubleProperty qtyProperty) {
			this.qtyProperty = qtyProperty;
		}
		@JsonIgnore
		public DoubleProperty getMrpProperty() {
			mrpProperty.set(mrp);
			return mrpProperty;
		}
		public void setMrpProperty(DoubleProperty mrpProperty) {
			this.mrpProperty = mrpProperty;
		}
		@JsonIgnore
		public ObjectProperty<LocalDate> getExpDateProperty() {
			
			return expDateProperty;
		}
		public void setExpDateProperty(ObjectProperty<LocalDate> expDateProperty) {
			this.expDateProperty = expDateProperty;
		}
		@JsonIgnore
		public DoubleProperty getAmountProperty() {
			amountProperty.set(amount);
			return amountProperty;
		}
		public void setAmountProperty(DoubleProperty amountProperty) {
			this.amountProperty = amountProperty;
		}
		@JsonIgnore
		public StringProperty getItemNameProperty() {
			itemNameProperty.set(itemName);
			return itemNameProperty;
		}
		public void setItemNameProperty(StringProperty itemNameProperty) {
			this.itemNameProperty = itemNameProperty;
		}
		public StockTransferInReturnHdr getStockTransferInReturnHdr() {
			return stockTransferInReturnHdr;
		}
		public void setStockTransferInReturnHdr(StockTransferInReturnHdr stockTransferInReturnHdr) {
			this.stockTransferInReturnHdr = stockTransferInReturnHdr;
		}
		@Override
		public String toString() {
			return "StockTransferInReturnDtl [id=" + id + ", barCode=" + barCode + ", batchCode=" + batchCode
					+ ", rate=" + rate + ", qty=" + qty + ", mrp=" + mrp + ", expDate=" + expDate + ", amount=" + amount
					+ ", itemName=" + itemName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
					+ ", stockTransferInReturnHdr=" + stockTransferInReturnHdr + ", batchCodeProperty="
					+ batchCodeProperty + ", barCodeProperty=" + barCodeProperty + ", rateProperty=" + rateProperty
					+ ", qtyProperty=" + qtyProperty + ", mrpProperty=" + mrpProperty + ", expDateProperty="
					+ expDateProperty + ", amountProperty=" + amountProperty + ", itemNameProperty=" + itemNameProperty
					+ "]";
		}
		
		
		
}
