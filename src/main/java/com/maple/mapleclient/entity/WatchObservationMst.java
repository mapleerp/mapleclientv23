package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WatchObservationMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String id;
	String observation;
	String branchCode;
	CompanyMst companyMst;
	
	private String  processInstanceId;
	private String taskId;
	
	public WatchObservationMst() {
		this.observationProperty = new SimpleStringProperty("");
	}
	@JsonIgnore
	private StringProperty observationProperty;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	
	public StringProperty getObservationProperty() {
		observationProperty.set(observation);
		return observationProperty;
	}
	public void setObservationProperty(StringProperty observationProperty) {
		this.observationProperty = observationProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "WatchObservationMst [id=" + id + ", observation=" + observation + ", branchCode=" + branchCode
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	

}
