package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class IntentDtl {
	
	private String id;
	
	public void setId(String id) {
		this.id = id;
	}
	private String itemId;
	private String UnitName;
	private String UnitId;
	private Double Qty;
	private String toBranch;
	
	private Double rate;
	
	private String remark;
	private String  processInstanceId;
	private String taskId;
	
	private String itemPropertyAsJson;

	
	@JsonIgnore
	private StringProperty ItemIdProperty;
	
	@JsonIgnore
	private StringProperty UnitNameProperty;
	
	@JsonIgnore
	private DoubleProperty QtyProperty;
	
	@JsonIgnore
	private StringProperty toBranchProperty;
	

	@JsonIgnore
	private StringProperty remarkProperty;
	
	@JsonIgnore
    String itemName;
	
	IntentHdr intentHdr;
	

	public IntentHdr getIntentHdr()
	{
		return intentHdr;
	}
	public void setIntentHdr(IntentHdr intentHdr) {
		this.intentHdr = intentHdr;
	}
	public IntentDtl() {
		this.QtyProperty = new SimpleDoubleProperty();
		this.UnitNameProperty = new SimpleStringProperty("");
		this.ItemIdProperty = new SimpleStringProperty("");
		this.toBranchProperty = new SimpleStringProperty("");
		this.remarkProperty = new SimpleStringProperty("");

	}
	public String getId() {
		return id;
	}
	
	public String getToBranch() {
		return toBranch;
	}
	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}
	public StringProperty getItemNameProperty() {
		ItemIdProperty.set(itemName);
		return ItemIdProperty;
	}
	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	
	public StringProperty gettoBranchProperty() {
		toBranchProperty.set(toBranch);
		return toBranchProperty;
	}
	public void settoBranchProperty(String toBranch) {
		this.toBranch = toBranch;
	}
	public StringProperty getUnitNameProperty() {
		UnitNameProperty.set(UnitName);
		return UnitNameProperty;
	}
	public void setUnitNameProperty(StringProperty unitNameProperty) {
		UnitNameProperty = unitNameProperty;
	}
	public DoubleProperty getQtyProperty() {
		QtyProperty.set(Qty);
		return QtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		QtyProperty = qtyProperty;
	}
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getUnitId() {
		return UnitId;
	}
	public void setUnitId(String unitId) {
		UnitId = unitId;
	}
	
	public Double getQty() {
		return Qty;
	}
	public void setQty(Double qty) {
		Qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	@JsonIgnore

	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	@JsonIgnore
	public String getUnitName() {
		return UnitName;
	}
	public void setUnitName(String unitName) {
		UnitName = unitName;
	}
	
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	public StringProperty getRemarkProperty() {
		
		remarkProperty.set(remark);
		return remarkProperty;
	}
	public void setRemarkProperty(StringProperty remarkProperty) {
		this.remarkProperty = remarkProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	
	public String getItemPropertyAsJson() {
		return itemPropertyAsJson;
	}
	public void setItemPropertyAsJson(String itemPropertyAsJson) {
		this.itemPropertyAsJson = itemPropertyAsJson;
	}
	@Override
	public String toString() {
		return "IntentDtl [id=" + id + ", itemId=" + itemId + ", UnitName=" + UnitName + ", UnitId=" + UnitId + ", Qty="
				+ Qty + ", toBranch=" + toBranch + ", rate=" + rate + ", remark=" + remark + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", itemPropertyAsJson=" + itemPropertyAsJson
				+ ", itemName=" + itemName + ", intentHdr=" + intentHdr + "]";
	}
	
	
	

}
