package com.maple.mapleclient.entity;


import java.time.LocalDate;
import java.util.Date;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class SalesReturnDtl {
    private String id;
	 
	String itemId;
	Double qty;
	Double rate;
	Double cgstTaxRate;
	Double sgstTaxRate;
	Double cessRate;
	Double addCessRate;
	Double igstTaxRate;
	 
	String itemTaxaxId;
	String unitId;
	String itemName;
	Date expiryDate;
	String batch;
	String barcode;
	
	Double taxRate;
	Double mrp;

	Double amount;
	String unitName;
    Double discount;
    Double cgstAmount;
    Double sgstAmount;
    Double igstAmount;
    
    private String  processInstanceId;
	private String taskId;
    
    Double cessAmount;
	StringProperty itemNameProperty;
	StringProperty unitNameProperty;
	StringProperty itemCodeProperty;
	DoubleProperty mrpProperty;
	DoubleProperty taxRateProperty;
	StringProperty barcodeProperty;
	StringProperty batchProperty;
	DoubleProperty qtyProperty;
	DoubleProperty amountProperty;
	String sales_dtl_id;
	  Double addCessAmount;

	public Double getAddCessAmount() {
		return addCessAmount;
	}
	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}
	public String getSales_dtl_id() {
		return sales_dtl_id;
	}
	public void setSales_dtl_id(String sales_dtl_id) {
		this.sales_dtl_id = sales_dtl_id;
	}
	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDateProperty;
	
	DoubleProperty cessRateProperty;
	 public SalesReturnDtl() {
				this.itemNameProperty = new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.mrpProperty = new SimpleDoubleProperty();
		this.taxRateProperty = new SimpleDoubleProperty();
		this.barcodeProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.cessRateProperty = new SimpleDoubleProperty();
		
		
		    this.discount = 0.0;
		    this. igstTaxRate =0.0;
		    this. cgstAmount =0.0;
		    this. sgstAmount =0.0;
		    this. igstAmount =0.0;
		    this. cessAmount=0.0;
		    this.cgstTaxRate =0.0;
		    this.sgstTaxRate =0.0;
		    this.addCessRate=0.0;
		    this.cessRate=0.0;
		    this.mrp=0.0;
		    
		    
		    
		
	}
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}

	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}

	public void setUnitNameProperty(String unitName) {
		this.unitName = unitName;
	}

	public StringProperty getItemCodeProperty() {
		return itemCodeProperty;
	}

	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	public DoubleProperty getMrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}

	public void setMrpProperty(Double mrp) {
		this.mrp = mrp;
	}

	public DoubleProperty getTaxRateProperty() {
		taxRateProperty.set(taxRate);
		return taxRateProperty;
	}

	public void setTaxRateProperty(Double taxRate) {
		this.taxRate = taxRate;
	}

	public StringProperty getBarcodeProperty() {
		barcodeProperty.set(barcode);
		return barcodeProperty;
	}

	public void setBarcodeProperty(String barcode) {
		this.barcode = barcode;
	}

	public StringProperty getBatchCodeProperty() {
		batchProperty.set(batch); 
		return batchProperty;
	}

	public void setBatchCodeProperty(String batch) {
		this.batch = batch;
	}

	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setQtyProperty(Double qty) {
		this.qty = qty;
	}

	public DoubleProperty getCessRateProperty() {
		cessRateProperty.set(cessRate);
		return cessRateProperty;
	}

	public void setCessRateProperty(Double cessRate) {
		this.cessRate = cessRate;
	}
	
	public ObjectProperty<LocalDate> getExpiryDateProperty() {
		return expiryDateProperty;
	}

	public void setExpiryDateProperty(ObjectProperty<LocalDate> expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}

	public void setAmountProperty(Double amount) {
		this.amount = amount;
	}
	private SalesTransHdr salesTransHdr;

	private CompanyMst companyMst;
	private SalesReturnHdr salesReturnHdr;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}
	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}
	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}
	public Double getCessRate() {
		return cessRate;
	}
	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}
	public Double getAddCessRate() {
		return addCessRate;
	}
	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}
	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}
	public String getItemTaxaxId() {
		return itemTaxaxId;
	}
	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getCgstAmount() {
		return cgstAmount;
	}
	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}
	public Double getSgstAmount() {
		return sgstAmount;
	}
	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}
	public Double getIgstAmount() {
		return igstAmount;
	}
	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}
	public Double getCessAmount() {
		return cessAmount;
	}
	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public SalesReturnHdr getSalesReturnHdr() {
		return salesReturnHdr;
	}
	public void setSalesReturnHdr(SalesReturnHdr salesReturnHdr) {
		this.salesReturnHdr = salesReturnHdr;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SalesReturnDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", rate=" + rate + ", cgstTaxRate="
				+ cgstTaxRate + ", sgstTaxRate=" + sgstTaxRate + ", cessRate=" + cessRate + ", addCessRate="
				+ addCessRate + ", igstTaxRate=" + igstTaxRate + ", itemTaxaxId=" + itemTaxaxId + ", unitId=" + unitId
				+ ", itemName=" + itemName + ", expiryDate=" + expiryDate + ", batch=" + batch + ", barcode=" + barcode
				+ ", taxRate=" + taxRate + ", mrp=" + mrp + ", amount=" + amount + ", unitName=" + unitName
				+ ", discount=" + discount + ", cgstAmount=" + cgstAmount + ", sgstAmount=" + sgstAmount
				+ ", igstAmount=" + igstAmount + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", cessAmount=" + cessAmount + ", sales_dtl_id=" + sales_dtl_id + ", addCessAmount=" + addCessAmount
				+ ", salesTransHdr=" + salesTransHdr + ", companyMst=" + companyMst + ", salesReturnHdr="
				+ salesReturnHdr + "]";
	}

}
