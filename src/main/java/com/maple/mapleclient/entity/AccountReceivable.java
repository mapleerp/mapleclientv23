package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AccountReceivable {
	
	
	String id;
	String voucherNumber;
	Date dueDate;
	Date voucherDate;
	String accountId;
	String remark;
	Double dueAmount;
	Double paidAmount;
	
	SalesTransHdr salesTransHdr;
	SalesOrderTransHdr salesOrderTransHdr;
	Double balanceAmount;
	
	/*
	 * adding new field = account_heads for the purpose of 
	 * integrate customer and supplier to account_heads ====05/01/2022
	 */
	private AccountHeads accountHeads;
	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}

	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}

	public StringProperty getInvoiceNumberProperty() {
		return invoiceNumberProperty;
	}

	public void setInvoiceNumberProperty(StringProperty invoiceNumberProperty) {
		this.invoiceNumberProperty = invoiceNumberProperty;
	}

	public StringProperty getVoucher_DateProperty() {
		return voucher_DateProperty;
	}

	public void setVoucher_DateProperty(StringProperty voucher_DateProperty) {
		this.voucher_DateProperty = voucher_DateProperty;
	}

	public DoubleProperty getDueAmountProperty() {
		return dueAmountProperty;
	}

	public void setDueAmountProperty(DoubleProperty dueAmountProperty) {
		this.dueAmountProperty = dueAmountProperty;
	}

	public DoubleProperty getReceivedAmountProperty() {
		return receivedAmountProperty;
	}

	public void setReceivedAmountProperty(DoubleProperty receivedAmountProperty) {
		this.receivedAmountProperty = receivedAmountProperty;
	}

	public DoubleProperty getBalanceAmountProperty() {
		return balanceAmountProperty;
	}

	public void setBalanceAmountProperty(DoubleProperty balanceAmountProperty) {
		this.balanceAmountProperty = balanceAmountProperty;
	}

	public DoubleProperty getPaidAmountProperty() {
		return paidAmountProperty;
	}

	public void setPaidAmountProperty(DoubleProperty paidAmountProperty) {
		this.paidAmountProperty = paidAmountProperty;
	}

	private String  processInstanceId;
	private String taskId;


	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}

	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}

	@JsonIgnore
	private StringProperty invoiceNumberProperty;

	@JsonIgnore
	private StringProperty voucher_DateProperty;

	@JsonIgnore
	private DoubleProperty dueAmountProperty;

	@JsonIgnore
	private DoubleProperty receivedAmountProperty;
//	@JsonIgnore
//	private SimpleObjectProperty<LocalDate> voucherDate;
	@JsonIgnore
	private DoubleProperty balanceAmountProperty;
	@JsonIgnore
	private DoubleProperty paidAmountProperty;

	public AccountReceivable() {

		this.invoiceNumberProperty = new SimpleStringProperty();
		this.dueAmountProperty = new SimpleDoubleProperty();
//		this.voucherDate = new SimpleObjectProperty<LocalDate>(null);
		this.receivedAmountProperty = new SimpleDoubleProperty();
		this.balanceAmountProperty = new SimpleDoubleProperty(0.0);
		this.paidAmountProperty = new SimpleDoubleProperty();
		this.voucher_DateProperty = new SimpleStringProperty();
		this.balanceAmount = 0.0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	@JsonIgnore
	public StringProperty getinvoiceNumberProperty() {
		invoiceNumberProperty.set(voucherNumber);
		return invoiceNumberProperty;
	}

	public void setinvoiceNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	@JsonIgnore
	public DoubleProperty getdueAmountProperty() {
		dueAmountProperty.set(dueAmount);
		return dueAmountProperty;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

//	public Date getVoucherDate() {
//		return voucherDate;
//	}
//
//	public void setVoucherDate(Date voucherDate) {
//		this.voucherDate = voucherDate;
//	}

	public void setdueAmountProperty(Double dueAmount) {
		this.dueAmount = dueAmount;
	}

//	 public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
//			return voucherDateProperty;
//		}
//		public void setvoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
//			this.voucherDateProperty = (SimpleObjectProperty<LocalDate>) voucherDate;
//		}
	@JsonIgnore
	public DoubleProperty getpaidAmountProperty() {
		paidAmountProperty.set(paidAmount);
		return paidAmountProperty;
	}

	public void setpaidAmountProperty(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	@JsonIgnore
	public StringProperty getvoucher_DateProperty() {

		voucher_DateProperty.set(SystemSetting.UtilDateToString(voucherDate));
		return voucher_DateProperty;
	}

	public void setvoucher_DateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	@JsonIgnore
	public DoubleProperty getbalanceAmountProperty() {
		balanceAmountProperty.set(balanceAmount);
		return balanceAmountProperty;
	}

	public void setbalanceAmountProperty(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "AccountReceivable [id=" + id + ", voucherNumber=" + voucherNumber + ", dueDate=" + dueDate
				+ ", voucherDate=" + voucherDate + ", accountId=" + accountId + ", remark=" + remark + ", dueAmount="
				+ dueAmount + ", paidAmount=" + paidAmount + ", salesTransHdr=" + salesTransHdr
				+ ", salesOrderTransHdr=" + salesOrderTransHdr + ", balanceAmount=" + balanceAmount + ", accountHeads="
				+ accountHeads + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", invoiceNumberProperty=" + invoiceNumberProperty + ", voucher_DateProperty=" + voucher_DateProperty
				+ ", dueAmountProperty=" + dueAmountProperty + ", receivedAmountProperty=" + receivedAmountProperty
				+ ", balanceAmountProperty=" + balanceAmountProperty + ", paidAmountProperty=" + paidAmountProperty
				+ "]";
	}

	

	

}
