package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Date;




import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class LmsQueueTallyMst implements Serializable{
	
	private static final long serialVersionUID = 1L;


	private String id;

	private String voucherNumber;

	private Date voucherDate;

	private String voucherType;
	private Date postedDate;

	private String postedToTally;

	private String messageId;

	private String jobName;

	private String jobGroup;

	private String jobClass;

	private String cronExpression;

	private Long repeatTime;

	private Boolean cronJob;
	
	private String sourceObjectId;
	
	private String branchCode;
	
	
	
	
	public LmsQueueTallyMst() {
		
		this.brabchProperty = new SimpleStringProperty("");
		this.voucherNoProperty = new SimpleStringProperty("");
		this.voucherDateProperty = new SimpleStringProperty("");
		this.voucherTypeProperty = new SimpleStringProperty("");

	}

	@JsonIgnore
	private StringProperty brabchProperty;
	
	@JsonIgnore
	private StringProperty voucherNoProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty voucherTypeProperty;
	
	
	private String  processInstanceId;
	private String taskId;

	
	CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public Date getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}

	 

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	 

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getJobClass() {
		return jobClass;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Long getRepeatTime() {
		return repeatTime;
	}

	public void setRepeatTime(Long repeatTime) {
		this.repeatTime = repeatTime;
	}

	public Boolean getCronJob() {
		return cronJob;
	}

	public void setCronJob(Boolean cronJob) {
		this.cronJob = cronJob;
	}

 

	public String getSourceObjectId() {
		return sourceObjectId;
	}

	public void setSourceObjectId(String sourceObjectId) {
		this.sourceObjectId = sourceObjectId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getPostedToTally() {
		return postedToTally;
	}

	public void setPostedToTally(String postedToTally) {
		this.postedToTally = postedToTally;
	}

	
	
	
	
	public StringProperty getBrabchProperty() {
		brabchProperty.set(branchCode);
		return brabchProperty;
	}

	public void setBrabchProperty(StringProperty brabchProperty) {
		this.brabchProperty = brabchProperty;
	}

	public StringProperty getVoucherNoProperty() {
		voucherNoProperty.set(voucherNumber);
		return voucherNoProperty;
	}

	public void setVoucherNoProperty(StringProperty voucherNoProperty) {
		this.voucherNoProperty = voucherNoProperty;
	}

	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		return voucherDateProperty;
	}

	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}

	public StringProperty getVoucherTypeProperty() {
		voucherTypeProperty.set(voucherType);
		return voucherTypeProperty;
	}

	public void setVoucherTypeProperty(StringProperty voucherTypeProperty) {
		this.voucherTypeProperty = voucherTypeProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "LmsQueueTallyMst [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", voucherType=" + voucherType + ", postedDate=" + postedDate + ", postedToTally=" + postedToTally
				+ ", messageId=" + messageId + ", jobName=" + jobName + ", jobGroup=" + jobGroup + ", jobClass="
				+ jobClass + ", cronExpression=" + cronExpression + ", repeatTime=" + repeatTime + ", cronJob="
				+ cronJob + ", sourceObjectId=" + sourceObjectId + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst + "]";
	}

	 
}
