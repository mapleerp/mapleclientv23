package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemBatchExpiryDtl {
	String id;
	String itemId;
	String batch;
	Date expiryDate;
	Date updatedDate;
	String branchCode;
	
	private String  processInstanceId;
	private String taskId;
	
	String itemName;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	
	
	public ItemBatchExpiryDtl() {
		this.itemNameProperty = new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public StringProperty getItemNameProperty() {
		if(null != itemName)
		{
		itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}
	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	public StringProperty getBatchProperty() {
		if(null !=batch )
		{
		batchProperty.set(batch);
		}
		return batchProperty;
	}
	public void setBatchProperty(String batch) {
		this.batch = batch;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ItemBatchExpiryDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", expiryDate=" + expiryDate
				+ ", updatedDate=" + updatedDate + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", itemName=" + itemName + "]";
	}


}
