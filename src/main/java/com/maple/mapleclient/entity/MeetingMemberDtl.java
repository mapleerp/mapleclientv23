package com.maple.mapleclient.entity;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MeetingMemberDtl {
	String id;
	String branchCode;
	String Mmid;
	String meetingId;
	String memberId;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
    private StringProperty MmidProperty;
	@JsonIgnore
    private StringProperty meetingIdProperty;
	@JsonIgnore
    private StringProperty memberIdProperty;
	
public MeetingMemberDtl() {
		
		this.MmidProperty = new SimpleStringProperty("");
		this.meetingIdProperty = new SimpleStringProperty("");
		this.memberIdProperty = new SimpleStringProperty("");
		
	}
public StringProperty getMemberIdProperty() {
	memberIdProperty.set(memberId);
	return memberIdProperty;
}
public void setMemberIdProperty(String memberId) {
	this.memberId = memberId;
}
	public StringProperty getMmidProperty() {
		MmidProperty.set(Mmid);
		return MmidProperty;
	}
	public void setMmidProperty(String mmid) {
		Mmid = mmid;
	}
	
	
	public StringProperty getMeetingIdProperty() {
		meetingIdProperty.set(meetingId);
		return meetingIdProperty;
	}
	public void setMeetingIdProperty(String meetingId) {
		this.meetingId = meetingId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getMmid() {
		return Mmid;
	}
	public void setMmid(String mmid) {
		Mmid = mmid;
	}
	public String getMeetingId() {
		return meetingId;
	}
	public void setMeetingId(String meetingId) {
		this.meetingId = meetingId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MeetingMemberDtl [id=" + id + ", branchCode=" + branchCode + ", Mmid=" + Mmid + ", meetingId="
				+ meetingId + ", memberId=" + memberId + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
}

