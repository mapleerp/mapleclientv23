package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class IntentInHdr {

	 String id;
	 String userId;
	 Integer companyId;
	 String machineId;
	 String branchCode;
	 String inVoucherNumber;
	 String fromBranch;
	 String intentStatus;
	 Date inVoucherDate;
	 String voucherType;
	 
	 private String  processInstanceId;
	 private String taskId;
	
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	@JsonIgnore
		private StringProperty idProperty;
	 @JsonIgnore
	 private StringProperty fromBranchProperty;
	 
	 @JsonIgnore
	 private StringProperty voucherNumberProperty;
	 
	 @JsonIgnore
	 private StringProperty voucherDateProperty;
		
	public IntentInHdr() {
		this.idProperty = new SimpleStringProperty();
		this.fromBranchProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();
	}
	@JsonIgnore
	public StringProperty getidProperty() {
		idProperty.set(id);
		return idProperty;
	}

	public void setidProperty(String id) {
		this.id = id;
	}
	@JsonIgnore
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(inVoucherNumber);
		return voucherNumberProperty;
	}

	public void setvoucherNumberProperty(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}
	
	@JsonIgnore
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(inVoucherDate));
		return voucherDateProperty;
	}

	public void setvoucherDateProperty(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}
	
	
	@JsonIgnore
	public StringProperty gefromBranchProperty() {
		fromBranchProperty.set(fromBranch);
		return fromBranchProperty;
	}

	public void setfromBranchProperty(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getMachineId() {
		return machineId;
	}
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getIntentStatus() {
		return intentStatus;
	}
	public void setIntentStatus(String intentStatus) {
		this.intentStatus = intentStatus;
	}
	public String getInVoucherNumber() {
		return inVoucherNumber;
	}
	public void setInVoucherNumber(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}
	public Date getInVoucherDate() {
		return inVoucherDate;
	}
	public void setInVoucherDate(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "IntentInHdr [id=" + id + ", userId=" + userId + ", companyId=" + companyId + ", machineId=" + machineId
				+ ", branchCode=" + branchCode + ", inVoucherNumber=" + inVoucherNumber + ", fromBranch=" + fromBranch
				+ ", intentStatus=" + intentStatus + ", inVoucherDate=" + inVoucherDate + ", voucherType=" + voucherType
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

}
