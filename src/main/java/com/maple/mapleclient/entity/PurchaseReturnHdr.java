package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.StringProperty;

public class PurchaseReturnHdr {
	
	 private String id;
	 
	 
		private String supplierId;
	 
	 
		private String finalSavedStatus;
		private String machineId;
		private String branchCode;
		private String deletedStatus;
	 	private String narration;
		private String purchaseType;
		private String pONum;
		private Date poDate;
		private String supplierInvNo;
		private String voucherNumber;
		private Double invoiceTotal;
		private String currency;
		private Date supplierInvDate;
		private Date voucherDate;
		private Date tansactionEntryDate;
	 
		private Integer enableBatchStatus;
		private String userId;
		private String voucherType;
		
		private String  processInstanceId;
		private String taskId;
		
		private String returnVoucherNumber;
		private String returnVoucherDate;
		private String store;
		
		
	

		CompanyMst companyMst;
		 
		 PurchaseHdr purchaseHdr;

		public PurchaseHdr getPurchaseHdr() {
			return purchaseHdr;
		}

		public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
			this.purchaseHdr = purchaseHdr;
		}
		

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getSupplierId() {
			return supplierId;
		}

		public void setSupplierId(String supplierId) {
			this.supplierId = supplierId;
		}

		public String getFinalSavedStatus() {
			return finalSavedStatus;
		}

		public void setFinalSavedStatus(String finalSavedStatus) {
			this.finalSavedStatus = finalSavedStatus;
		}

		public String getMachineId() {
			return machineId;
		}

		public void setMachineId(String machineId) {
			this.machineId = machineId;
		}

		public String getBranchCode() {
			return branchCode;
		}

		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}

		public String getDeletedStatus() {
			return deletedStatus;
		}

		public void setDeletedStatus(String deletedStatus) {
			this.deletedStatus = deletedStatus;
		}

		public String getNarration() {
			return narration;
		}

		public void setNarration(String narration) {
			this.narration = narration;
		}

		public String getPurchaseType() {
			return purchaseType;
		}

		public void setPurchaseType(String purchaseType) {
			this.purchaseType = purchaseType;
		}

		public String getpONum() {
			return pONum;
		}

		public void setpONum(String pONum) {
			this.pONum = pONum;
		}

		public Date getPoDate() {
			return poDate;
		}

		public void setPoDate(Date poDate) {
			this.poDate = poDate;
		}

		public String getSupplierInvNo() {
			return supplierInvNo;
		}

		public void setSupplierInvNo(String supplierInvNo) {
			this.supplierInvNo = supplierInvNo;
		}

		public String getVoucherNumber() {
			return voucherNumber;
		}

		public void setVoucherNumber(String voucherNumber) {
			this.voucherNumber = voucherNumber;
		}

		public Double getInvoiceTotal() {
			return invoiceTotal;
		}

		public void setInvoiceTotal(Double invoiceTotal) {
			this.invoiceTotal = invoiceTotal;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

		public Date getSupplierInvDate() {
			return supplierInvDate;
		}

		public void setSupplierInvDate(Date supplierInvDate) {
			this.supplierInvDate = supplierInvDate;
		}

		public Date getVoucherDate() {
			return voucherDate;
		}

		public void setVoucherDate(Date voucherDate) {
			this.voucherDate = voucherDate;
		}

		public Date getTansactionEntryDate() {
			return tansactionEntryDate;
		}

		public void setTansactionEntryDate(Date tansactionEntryDate) {
			this.tansactionEntryDate = tansactionEntryDate;
		}

		public Integer getEnableBatchStatus() {
			return enableBatchStatus;
		}

		public void setEnableBatchStatus(Integer enableBatchStatus) {
			this.enableBatchStatus = enableBatchStatus;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getVoucherType() {
			return voucherType;
		}

		public void setVoucherType(String voucherType) {
			this.voucherType = voucherType;
		}

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		public String getReturnVoucherNumber() {
			return returnVoucherNumber;
		}

		public void setReturnVoucherNumber(String returnVoucherNumber) {
			this.returnVoucherNumber = returnVoucherNumber;
		}

		public String getReturnVoucherDate() {
			return returnVoucherDate;
		}

		public void setReturnVoucherDate(String returnVoucherDate) {
			this.returnVoucherDate = returnVoucherDate;
		}

		public CompanyMst getCompanyMst() {
			return companyMst;
		}

		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}
		

		@Override
		public String toString() {
			return "PurchaseReturnHdr [id=" + id + ", supplierId=" + supplierId + ", finalSavedStatus="
					+ finalSavedStatus + ", machineId=" + machineId + ", branchCode=" + branchCode + ", deletedStatus="
					+ deletedStatus + ", narration=" + narration + ", purchaseType=" + purchaseType + ", pONum=" + pONum
					+ ", poDate=" + poDate + ", supplierInvNo=" + supplierInvNo + ", voucherNumber=" + voucherNumber
					+ ", invoiceTotal=" + invoiceTotal + ", currency=" + currency + ", supplierInvDate="
					+ supplierInvDate + ", voucherDate=" + voucherDate + ", tansactionEntryDate=" + tansactionEntryDate
					+ ", enableBatchStatus=" + enableBatchStatus + ", userId=" + userId + ", voucherType=" + voucherType
					+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", returnVoucherNumber="
					+ returnVoucherNumber + ", returnVoucherDate=" + returnVoucherDate + ", companyMst=" + companyMst
					+ ", purchaseHdr=" + purchaseHdr + "]";
		}

		public String getStore() {
			return store;
		}

		public void setStore(String store) {
			this.store = store;
		}

	

}


