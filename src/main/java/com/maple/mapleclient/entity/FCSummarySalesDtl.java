package com.maple.mapleclient.entity;


import org.springframework.data.annotation.Transient;


 
public class FCSummarySalesDtl {
	@Transient
	private Double totalQty;
	@Transient
	private Double fctotalAmount;
	@Transient
	private Double fctotalDiscount;
	@Transient
	private Double fctotalTax;
	@Transient
    private Double fctotalCessAmt;
	private String  processInstanceId;
	private String taskId;


   

	public Double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(Double totalQty) {
		this.totalQty = totalQty;
	}
	public Double getFctotalAmount() {
		return fctotalAmount;
	}
	public void setFctotalAmount(Double fctotalAmount) {
		this.fctotalAmount = fctotalAmount;
	}
	public Double getFctotalDiscount() {
		return fctotalDiscount;
	}
	public void setFctotalDiscount(Double fctotalDiscount) {
		this.fctotalDiscount = fctotalDiscount;
	}
	public Double getFctotalTax() {
		return fctotalTax;
	}
	public void setFctotalTax(Double fctotalTax) {
		this.fctotalTax = fctotalTax;
	}
	public Double getFctotalCessAmt() {
		return fctotalCessAmt;
	}
	public void setFctotalCessAmt(Double fctotalCessAmt) {
		this.fctotalCessAmt = fctotalCessAmt;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "FCSummarySalesDtl [totalQty=" + totalQty + ", fctotalAmount=" + fctotalAmount + ", fctotalDiscount="
				+ fctotalDiscount + ", fctotalTax=" + fctotalTax + ", fctotalCessAmt=" + fctotalCessAmt
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


    
    
}
