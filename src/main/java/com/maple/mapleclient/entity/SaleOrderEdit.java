package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SaleOrderEdit {
	
	@JsonProperty
	String id;
	@JsonProperty
	String itemId;
	@JsonProperty
	Double rate;
	@JsonProperty
	Double cgstTaxRate;
	@JsonProperty
	Double sgstTaxRate;
	@JsonProperty
	Double cessRate;
	@JsonProperty
	Double qty;
	@JsonProperty
	Double addCessRate;
	@JsonProperty
	String itemTaxaxId;
	@JsonProperty
	String unitId;
	@JsonProperty
	String itemName;
	
	
    Double addCessAmount;
    private String  processInstanceId;
	private String taskId;

	
	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDate;
	
	@JsonProperty
	String batch;
	@JsonProperty
	String barode;
	@JsonProperty
	Double taxRate;
	@JsonProperty
	Double mrp;
	@JsonProperty
	String itemCode;
	@JsonProperty
	String unitName;
	String branchCode;
	
	   Double discount;
	    Double igstTaxRate;
	    Double cgstAmount;
	    Double sgstAmount;
	    Double igstAmount;
	    Double cessAmount;
	    Double amount;
	    

	    Double standardPrice;
	    Double costPrice;
	    
	StringProperty itemNameProperty;
	StringProperty unitNameProperty;
	StringProperty itemCodeProperty;
	StringProperty mrpProperty;
	StringProperty taxRateProperty;
	StringProperty barodeProperty;
	StringProperty batchProperty;
	StringProperty qtyProperty;
	StringProperty amountProperty;
	StringProperty cessRateProperty;
	StringProperty rateProperty;
	String rDate;
	
	private SalesOrderTransHdr salesOrderTransHdr;

	public SaleOrderEdit () {
		
		this.itemNameProperty = new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.mrpProperty = new SimpleStringProperty("");
		this.taxRateProperty = new SimpleStringProperty("");
		this.barodeProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleStringProperty("");
		this.rateProperty = new SimpleStringProperty("");
		
		this.qtyProperty = new SimpleStringProperty("");
		this.cessRateProperty = new SimpleStringProperty("");
		
		 this.discount = 0.0;
		 this.rate = 0.0;
		    this. igstTaxRate =0.0;
		    this. cgstAmount =0.0;
		    this. sgstAmount =0.0;
		    this. igstAmount =0.0;
		    this. cessAmount=0.0;
		    this.cgstTaxRate =0.0;
		    this.sgstTaxRate =0.0;
		    this.addCessRate=0.0;
		    this.mrp=0.0;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		
		this.id = id;
	}

	


	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getCgstAmount() {
		return cgstAmount;
	}

	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public Double getSgstAmount() {
		return sgstAmount;
	}

	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public Double getIgstAmount() {
		return igstAmount;
	}

	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}

	public Double getCessAmount() {
		return cessAmount;
	}

	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}

	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		 
		this.itemId = itemId;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		rateProperty.set(rate+"");
		this.rate = rate;
	}

	public StringProperty getRateProperty() {
		return rateProperty;
	}

	public void setRateProperty(StringProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		
		
		this.cgstTaxRate = cgstTaxRate;
	}

	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		qtyProperty.set(qty+"");
		this.qty = qty;
	}

	public Double getAddCessRate() {
		return addCessRate;
	}

	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}

	public String getItemTaxaxId() {
		return itemTaxaxId;
	}

	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		
		this.itemNameProperty.set(itemName);
		this.itemName = itemName;
	}

	

	public String getBatchCode() {
		return batch;
	}

	public void setBatchCode(String batch) {
		this.batchProperty.set(batch);
		this.batch = batch;
	}

	public String getBarcode() {
		return barode;
	}

	public void setBarcode(String barCode) {
		this.barodeProperty.set(barCode);
		this.barode = barCode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRateProperty.set(taxRate+"");
		this.taxRate = taxRate;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrpProperty.set(mrp+"");
		this.mrp = mrp;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCodeProperty.set(itemCode);
		this.itemCode = itemCode;
	}

	public String getUnitName() {
		
		
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitNameProperty.set(unitName);
		this.unitName = unitName;
	}

	public StringProperty getItemNameProperty() {
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getUnitNameProperty() {
		return unitNameProperty;
	}

	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}

	public StringProperty getItemCodeProperty() {
		return itemCodeProperty;
	}

	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	public StringProperty getMrpProperty() {
		return mrpProperty;
	}

	public void setMrpProperty(StringProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}

	public StringProperty getTaxRateProperty() {
		
		return taxRateProperty;
	}

	public void setTaxRateProperty(StringProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}

	public StringProperty getBarcodeProperty() {
		return barodeProperty;
	}

	public void setBarcodeProperty(StringProperty barcodeProperty) {
		this.barodeProperty = barodeProperty;
	}

	public StringProperty getBatchCodeProperty() {
		return batchProperty;
	}

	public void setBatchCodeProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public StringProperty getQtyProperty() {
		return qtyProperty;
	}

	public void setQtyProperty(StringProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public StringProperty getCessRateProperty() {
		return cessRateProperty;
	}

	public void setCessRateProperty(StringProperty cessRateProperty) {
		this.cessRateProperty = cessRateProperty;
	}


	
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}
	
	

	public Double getAmount() {
		
		return amount;
	}

	public void setAmount(Double amount) {
		this.amountProperty.set(amount+"");
		this.amount = amount;
	}

	@JsonIgnore
	
	public ObjectProperty<LocalDate> getexpiryDateProperty() {
		return expiryDate;
	}
	
	public void setexpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {
		
		this.expiryDate = expiryDate;
	}
	
	@JsonProperty("expiryDate")
	
	public java.sql.Date getexpiryDate() {
		 
		return null== expiryDate ? null : Date.valueOf(this.expiryDate.get() );
	}
	
	public void setexpiryDate(java.sql.Date expiryDate) {
		if(null==expiryDate)
			return;
		this.expiryDate.set(expiryDate.toLocalDate());
	
	}
	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	
	public String getrDate() {
		return rDate;
	}

	public void setrDate(String rDate) {
		this.rDate = rDate;
	}
	
	

	public StringProperty getAmountProperty() {
		
		return amountProperty;
	}

	public void setAmountProperty(StringProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	
	
	public Double getAddCessAmount() {
		return addCessAmount;
	}

	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}

	
	
	public Double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SaleOrderEdit [id=" + id + ", itemId=" + itemId + ", rate=" + rate + ", cgstTaxRate=" + cgstTaxRate
				+ ", sgstTaxRate=" + sgstTaxRate + ", cessRate=" + cessRate + ", qty=" + qty + ", addCessRate="
				+ addCessRate + ", itemTaxaxId=" + itemTaxaxId + ", unitId=" + unitId + ", itemName=" + itemName
				+ ", addCessAmount=" + addCessAmount + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", batch=" + batch + ", barode=" + barode + ", taxRate=" + taxRate + ", mrp=" + mrp + ", itemCode="
				+ itemCode + ", unitName=" + unitName + ", branchCode=" + branchCode + ", discount=" + discount
				+ ", igstTaxRate=" + igstTaxRate + ", cgstAmount=" + cgstAmount + ", sgstAmount=" + sgstAmount
				+ ", igstAmount=" + igstAmount + ", cessAmount=" + cessAmount + ", amount=" + amount
				+ ", standardPrice=" + standardPrice + ", costPrice=" + costPrice + ", rDate=" + rDate
				+ ", salesOrderTransHdr=" + salesOrderTransHdr + "]";
	}

	

}
