package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KitDefinitionDtl {
	
	String id;
	String itemName;
	String barcode;
	Double qty;
	
	String unitId;
	String unitName;
	String itemId;
	
	//version1.8
	Double mltiUnitQty;
	String customisedUnitId;
	String customisedUnitName;
	
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore 
	StringProperty  customisedUnitNameProperty;
	
	@JsonIgnore
	DoubleProperty multiUnitQtyProperty;
	//version1.8ends.. bt continues..
	
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty barcodeProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;

	
	public KitDefinitionDtl() {
		//version1.8
		this.customisedUnitNameProperty = new SimpleStringProperty("");
		this.multiUnitQtyProperty = new SimpleDoubleProperty();

		//version1.8ends
		
		this.itemNameProperty = new SimpleStringProperty("");
		this.barcodeProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty();
		this.unitNameProperty = new SimpleStringProperty("");
	}
	
	KitDefinitionMst kitDifenitionmst;
	
	
	public KitDefinitionMst getKitDefenitionmst() {
		return kitDifenitionmst;
	}

	public void setKitDefenitionmst(KitDefinitionMst kitDefenitionmst) {
		this.kitDifenitionmst = kitDefenitionmst;
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@JsonProperty
	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	@JsonProperty
	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}
	@JsonProperty
	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	@JsonProperty
	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	
	@JsonIgnore
	public StringProperty getbarcodeProperty() {
		barcodeProperty.set(barcode);
		return barcodeProperty;
	}
	

	public void sebarcodeProperty(String barcode) {
		this.barcode = barcode;
	}

	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	
	
	//version1.8
		public Double getMltiUnitQty() {
			return mltiUnitQty;
		}

		public void setMltiUnitQty(Double mltiUnitQty) {
			this.mltiUnitQty = mltiUnitQty;
		}

		public String getCustomisedUnitId() {
			return customisedUnitId;
		}

		public void setCustomisedUnitId(String customisedUnitId) {
			this.customisedUnitId = customisedUnitId;
		}

		public String getCustomisedUnitName() {
			return customisedUnitName;
		}

		public void setCustomisedUnitName(String customisedUnitName) {
			this.customisedUnitName = customisedUnitName;
		}
		@JsonIgnore
		public StringProperty getcustomisedUnitNameProperty() {
			customisedUnitNameProperty.set(customisedUnitName);
			return customisedUnitNameProperty;
		}
		public void setcustomisedUnitNameProperty(String customisedUnitName) {
			this.customisedUnitName = customisedUnitName;
		}
		@JsonIgnore
		public DoubleProperty getmultiUnitqtyProperty() {
			multiUnitQtyProperty.set(mltiUnitQty);
			return multiUnitQtyProperty;
		}
		public void setmultiUnitqtyProperty(Double mltiUnitQty) {
			this.mltiUnitQty = mltiUnitQty;
		}
		//version1.8ends
		
	@JsonIgnore
	public StringProperty getunitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setunitNameProperty(String unitName) {
		this.unitName = unitName;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "KitDefinitionDtl [id=" + id + ", itemName=" + itemName + ", barcode=" + barcode + ", qty=" + qty
				+ ", unitId=" + unitId + ", unitName=" + unitName + ", itemId=" + itemId + ", mltiUnitQty="
				+ mltiUnitQty + ", customisedUnitId=" + customisedUnitId + ", customisedUnitName=" + customisedUnitName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", kitDifenitionmst="
				+ kitDifenitionmst + "]";
	}


}
