package com.maple.mapleclient.entity;

public class CurrencyMst {

	String id;
	String currencyName;
	String branchCode;
	
	private String  processInstanceId;
	private String taskId;
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCurrencyName() {
		return currencyName;
	}
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "CurrencyMst [id=" + id + ", currencyName=" + currencyName + ", branchCode=" + branchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
