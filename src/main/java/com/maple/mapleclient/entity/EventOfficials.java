package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

public class EventOfficials {

	
	String evOffId;
	

	String pasterId;
	
	
	String orgId;
	

	String titleName;

	String holdingMember;
	
	
	String eventId;
	private String  processInstanceId;
	private String taskId;


	public String getEvOffId() {
		return evOffId;
	}


	public void setEvOffId(String evOffId) {
		this.evOffId = evOffId;
	}


	public String getPasterId() {
		return pasterId;
	}


	public void setPasterId(String pasterId) {
		this.pasterId = pasterId;
	}


	public String getOrgId() {
		return orgId;
	}


	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}


	public String getTitleName() {
		return titleName;
	}


	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}


	public String getHoldingMember() {
		return holdingMember;
	}


	public void setHoldingMember(String holdingMember) {
		this.holdingMember = holdingMember;
	}


	public String getEventId() {
		return eventId;
	}


	public void setEventId(String eventId) {
		this.eventId = eventId;
	}


	

	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "EventOfficials [evOffId=" + evOffId + ", pasterId=" + pasterId + ", orgId=" + orgId + ", titleName="
				+ titleName + ", holdingMember=" + holdingMember + ", eventId=" + eventId + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
	
}
