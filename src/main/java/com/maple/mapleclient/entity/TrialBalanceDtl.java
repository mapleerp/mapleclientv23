package com.maple.mapleclient.entity;

public class TrialBalanceDtl {

	String id;
	String accountId;
	Double creditAmount;
	Double debitAmount;
	
	
	TrialBalanceHdr trialBalanceHdr;
	
	
	
	public TrialBalanceHdr getTrialBalanceHdr() {
		return trialBalanceHdr;
	}
	public void setTrialBalanceHdr(TrialBalanceHdr trialBalanceHdr) {
		this.trialBalanceHdr = trialBalanceHdr;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public Double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	@Override
	public String toString() {
		return "TrialBalanceDtl [id=" + id + ", accountId=" + accountId + ", creditAmount=" + creditAmount
				+ ", debitAmount=" + debitAmount + ", trialBalanceHdr=" + trialBalanceHdr + "]";
	}
	
	
	
	
}
