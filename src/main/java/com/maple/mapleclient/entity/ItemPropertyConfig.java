package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemPropertyConfig implements Serializable {
	private static final long serialVersionUID = 1L;
   private String id;
	private String branchCode;
	private String itemId;
	private String propertyType;
	private String propertyName;
	
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private String
 itemName;
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty propertyTypeProperty;
	
	@JsonIgnore
	private StringProperty propertyNameProperty;
	
	
	public ItemPropertyConfig() {
		this.itemNameProperty = new SimpleStringProperty();
		this.propertyTypeProperty = new SimpleStringProperty();
		this.propertyNameProperty = new SimpleStringProperty();
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}

	public StringProperty getPropertyTypeProperty() {
		propertyTypeProperty.set(propertyType);
		return propertyTypeProperty;
	}

	public void setPropertyTypeProperty(String propertyType) {
		this.propertyType = propertyType;
	}



	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public StringProperty getPropertyNameProperty() {
		propertyNameProperty.set(propertyName);
		return propertyNameProperty;
	}

	public void setPropertyNameProperty(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	public String getItemId() {
		return itemId;
	}



	public void setItemId(String itemId) {
		this.itemId = itemId;
	}



	public String getPropertyType() {
		return propertyType;
	}



	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}



	public String getPropertyName() {
		return propertyName;
	}



	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}





	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ItemPropertyConfig [id=" + id + ", branchCode=" + branchCode + ", itemId=" + itemId + ", propertyType="
				+ propertyType + ", propertyName=" + propertyName + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", itemName=" + itemName + "]";
	}



	
}
