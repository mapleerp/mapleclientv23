package com.maple.mapleclient.entity;

import java.util.Date;

public class ProductionMst {
	String id;
	String voucherNumber;
	Date voucherDate;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	private String store;
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	@Override
	public String toString() {
		return "ProductionMst [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", store=" + store + "]";
	}

	
	

}
