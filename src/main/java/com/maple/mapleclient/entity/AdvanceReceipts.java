package com.maple.mapleclient.entity;

import java.util.Date;



public class AdvanceReceipts {

	 
	String Id;
	String receiptMode;
	Double advanceReceiptAmount;
	Date receiptDate;
	String salesOrderTransHdrId;
	Double realizedAmount;
	
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public Double getAdvanceReceiptAmount() {
		return advanceReceiptAmount;
	}
	public void setAdvanceReceiptAmount(Double advanceReceiptAmount) {
		this.advanceReceiptAmount = advanceReceiptAmount;
	}
	public Date getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getSalesOrderTransHdrId() {
		return salesOrderTransHdrId;
	}
	public void setSalesOrderTransHdrId(String salesOrderTransHdrId) {
		this.salesOrderTransHdrId = salesOrderTransHdrId;
	}
	public Double getRealizedAmount() {
		return realizedAmount;
	}
	public void setRealizedAmount(Double realizedAmount) {
		this.realizedAmount = realizedAmount;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "AdvanceReceipts [Id=" + Id + ", receiptMode=" + receiptMode + ", advanceReceiptAmount="
				+ advanceReceiptAmount + ", receiptDate=" + receiptDate + ", salesOrderTransHdrId="
				+ salesOrderTransHdrId + ", realizedAmount=" + realizedAmount + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
	
}
