package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Task {
	String Name;

	String Assignee;
	String CandidateGroup;
	String CreatedOn;
	String DueDate;
	String voucherNumber;
	String voucherDate;
	String id;
	String businessId;
	String windowName;
	String accountId;
	String parentTaskId;
	String processInstanceId;
	
	private String taskId;
	
	StringProperty NameProperty;
	StringProperty CreatedOnProperty;
	StringProperty DueDatenProperty;
	StringProperty CandidateGroupProperty;
	StringProperty voucherNumberProperty;
	StringProperty voucherDateProperty;
	StringProperty idProperty;
	StringProperty businessIdProperty;
	StringProperty windowNameProperty;

	public Task() {
		this.NameProperty = new SimpleStringProperty("");
		this.CreatedOnProperty = new SimpleStringProperty("");
		this.DueDatenProperty = new SimpleStringProperty("");
		this.CandidateGroupProperty = new SimpleStringProperty("");
		this.voucherNumberProperty = new SimpleStringProperty("");
		this.voucherDateProperty = new SimpleStringProperty("");
		this.idProperty = new SimpleStringProperty("");
		this.businessIdProperty = new SimpleStringProperty("");

		this.windowNameProperty = new SimpleStringProperty("");

	}

	public String getAssignee() {
		return Assignee;
	}

	public void setAssignee(String assignee) {
		Assignee = assignee;
	}

	public String getCandidateGroup() {
		return CandidateGroup;
	}

	public String getAccountId() {
		return accountId;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public void setCandidateGroup(String candidateGroup) {
		CandidateGroup = candidateGroup;
	}

	public String getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(String createdOn) {
		CreatedOn = createdOn;
	}

	public String getDueDate() {
		return DueDate;
	}

	public StringProperty getDueDateProperty() {
		CreatedOnProperty.set(CreatedOn);
		return CreatedOnProperty;
	}

	public void setDueDate(String dueDate) {
		DueDate = dueDate;
	}

	public String getName() {
		return Name;
	}

	public StringProperty getNameProperty() {
		NameProperty.set(Name);
		return NameProperty;
	}

	public void setName(String name) {
		Name = name;
	}

	public StringProperty getCandidateGroupProperty() {
		CandidateGroupProperty.set(CandidateGroup);
		return CandidateGroupProperty;
	}

	public void setCandidateGroupProperty(StringProperty candidateGroupProperty) {
		CandidateGroupProperty = candidateGroupProperty;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}

	public StringProperty getVoucherNumberProperty() {

		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}

	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}

	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(voucherDate);

		return voucherDateProperty;
	}

	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StringProperty getIdProperty() {
		idProperty.set(id);

		return idProperty;
	}

	public void setIdProperty(StringProperty idProperty) {
		this.idProperty = idProperty;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public StringProperty getCreatedOnProperty() {
		return CreatedOnProperty;
	}

	public void setCreatedOnProperty(StringProperty createdOnProperty) {
		CreatedOnProperty = createdOnProperty;
	}

	public StringProperty getDueDatenProperty() {
		return DueDatenProperty;
	}

	public void setDueDatenProperty(StringProperty dueDatenProperty) {
		DueDatenProperty = dueDatenProperty;
	}

	public StringProperty getBusinessIdProperty() {
		businessIdProperty.set(businessId);
		return businessIdProperty;
	}

	public void setBusinessIdProperty(StringProperty businessIdProperty) {
		this.businessIdProperty = businessIdProperty;
	}

	public void setNameProperty(StringProperty nameProperty) {
		NameProperty = nameProperty;
	}

	public String getWindowName() {
		return windowName;
	}

	public void setWindowName(String windowName) {
		this.windowName = windowName;
	}

	public StringProperty getWindowNameProperty() {

		windowNameProperty.set(windowName);

		return windowNameProperty;
	}

	public void setWindowNameProperty(StringProperty windowNameProperty) {
		this.windowNameProperty = windowNameProperty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "Task [Name=" + Name + ", Assignee=" + Assignee + ", CandidateGroup=" + CandidateGroup + ", CreatedOn="
				+ CreatedOn + ", DueDate=" + DueDate + ", voucherNumber=" + voucherNumber + ", voucherDate="
				+ voucherDate + ", id=" + id + ", businessId=" + businessId + ", windowName=" + windowName
				+ ", accountId=" + accountId + ", parentTaskId=" + parentTaskId + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

}
