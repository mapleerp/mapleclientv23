package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DeliveryBoyMst {
	
	String id;
	String deliveryBoyName;
	String branchCode;
	String status;
	
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty deliveryBoyNameProperty;
	
	@JsonIgnore
	private StringProperty statusProperty;
	
	
	
	
	
	public DeliveryBoyMst() {
		
		this.deliveryBoyNameProperty = new SimpleStringProperty();
		this.statusProperty =new SimpleStringProperty();
	}
	@JsonProperty
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDeliveryBoyName() {
		return deliveryBoyName;
	}
	public void setDeliveryBoyName(String deliveryBoyName) {
		this.deliveryBoyName = deliveryBoyName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@JsonIgnore
	public StringProperty getdeliveryBoyNameProperty() {
		deliveryBoyNameProperty.set(deliveryBoyName);
		return deliveryBoyNameProperty;
	}
	

	public void setdeliveryBoyNameProperty(String deliveryBoyName) {
		this.deliveryBoyName = deliveryBoyName;
	}
	@JsonIgnore
	public StringProperty getstatusProperty() {
		statusProperty.set(status);
		return statusProperty;
	}
	

	public void setstatusProperty(String status) {
		this.status = status;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "DeliveryBoyMst [id=" + id + ", deliveryBoyName=" + deliveryBoyName + ", branchCode=" + branchCode
				+ ", status=" + status + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
