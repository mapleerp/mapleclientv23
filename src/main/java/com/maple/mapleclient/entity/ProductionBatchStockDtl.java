package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductionBatchStockDtl {

	String id;
	String itemId;
	String batch;
	Double qty;
	Double allocatedQty;
	String itemName;
	private String  processInstanceId;
	private String taskId;
	
	ActualProductionDtl actualProductionDtl;
	
	
	public ActualProductionDtl getActualProductionDtl() {
		return actualProductionDtl;
	}
	public void setActualProductionDtl(ActualProductionDtl actualProductionDtl) {
		this.actualProductionDtl = actualProductionDtl;
	}

	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDate;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty allocatedQtyProperty;
	
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public ProductionBatchStockDtl() {
		
		this.itemNameProperty = new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
		this.expiryDate = new  SimpleObjectProperty<LocalDate>(null);
		this.qtyProperty =new SimpleDoubleProperty();
		this.allocatedQtyProperty = new SimpleDoubleProperty(0.0);
		this.allocatedQty = 0.0;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAllocatedQty() {
		return allocatedQty;
	}
	public void setAllocatedQty(Double allocatedQty) {
		this.allocatedQty = allocatedQty;
	}
	public ObjectProperty<LocalDate> getExpiryDateProperty( ) {
		return expiryDate;
	}
 
	public void setExpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {
		this.expiryDate = expiryDate;
	}
	@JsonIgnore
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	
	@JsonIgnore
	
	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setbatchProperty(String batch) {
		this.batch = batch;
	}
	@JsonIgnore
	
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	
	@JsonIgnore
	
	public DoubleProperty getallocatedQtyProperty() {
		allocatedQtyProperty.set(allocatedQty);
		return allocatedQtyProperty;
	}

	public void setallocatedQtyProperty(Double allocatedQty) {
		this.allocatedQty = allocatedQty;
	}
	
	@JsonProperty("expiryDate")
	public java.sql.Date getexpiryDate ( ) {
		if(null==this.expiryDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.expiryDate.get() );
		}
	 
		
	}
	
   public void setexpiryDate (java.sql.Date expiryDateProperty) {
						if(null!=expiryDateProperty)
		this.expiryDate.set(expiryDateProperty.toLocalDate());
	}

public String getProcessInstanceId() {
	return processInstanceId;
}
public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}
public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
@Override
public String toString() {
	return "ProductionBatchStockDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", qty=" + qty
			+ ", allocatedQty=" + allocatedQty + ", itemName=" + itemName + ", processInstanceId=" + processInstanceId
			+ ", taskId=" + taskId + ", actualProductionDtl=" + actualProductionDtl + "]";
}
 
	
	
}
