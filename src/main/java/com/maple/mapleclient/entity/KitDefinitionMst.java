package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KitDefinitionMst
{
	String id;
	String itemId;
	String kitName;
	String unitId;
	String unitName;
	Double minimumQty;
	String barcode;
	Double productionCost;
	Double taxRate;
	String itemCode;
	String finalSave;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	//version3.6sari
	@JsonIgnore
	StringProperty kitnameProperty;
	
	@JsonIgnore
	StringProperty unitNameProperty;
	
	@JsonIgnore
	DoubleProperty prodCostProperty;
	
	@JsonIgnore
	DoubleProperty minQtyProperty;
	
	
	
	
	
	public KitDefinitionMst() {
		
		this.kitnameProperty = new SimpleStringProperty();
		this.unitNameProperty = new SimpleStringProperty();
		this.prodCostProperty = new SimpleDoubleProperty();
		this.minQtyProperty = new SimpleDoubleProperty();
	}
	
	@JsonIgnore
	public StringProperty getkitnameProperty() {
		kitnameProperty.set( kitName);
		return kitnameProperty;
	}
	

	public void setkitnameProperty(String kitName) {
		this. kitName =  kitName;
	}
	@JsonIgnore
	public StringProperty getunitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	

	public void setunitNameProperty(String unitName) {
		this. unitName =  unitName;
	}
	@JsonIgnore
	public DoubleProperty getprodCostProperty() {
		prodCostProperty.set(productionCost);
		return prodCostProperty;
	}
	

	public void setprodCostProperty(Double productionCost) {
		this. productionCost =  productionCost;
	}
	@JsonIgnore
	public DoubleProperty getminQtyProperty() {
		minQtyProperty.set(minimumQty);
		return minQtyProperty;
	}
	

	public void setminQtyProperty(Double minimumQty) {
		this. minimumQty =  minimumQty;
	}
	//version3.6 ends
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getFinalSave() {
		return finalSave;
	}
	public void setFinalSave(String finalSave) {
		this.finalSave = finalSave;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getKitName() {
		return kitName;
	}
	public void setKitName(String kitName) {
		this.kitName = kitName;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getMinimumQty() {
		return minimumQty;
	}
	public void setMinimumQty(Double minimumQty) {
		this.minimumQty = minimumQty;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Double getProductionCost() {
		return productionCost;
	}
	public void setProductionCost(Double productionCost) {
		this.productionCost = productionCost;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "KitDefinitionMst [id=" + id + ", itemId=" + itemId + ", kitName=" + kitName + ", unitId=" + unitId
				+ ", unitName=" + unitName + ", minimumQty=" + minimumQty + ", barcode=" + barcode + ", productionCost="
				+ productionCost + ", taxRate=" + taxRate + ", itemCode=" + itemCode + ", finalSave=" + finalSave
				+ ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	

	

}