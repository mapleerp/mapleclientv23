package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
 * This class is used from Item pop-up only. Not persisted in database
 */
public class ItemStockPopUp {

	StringProperty barcode;
	StringProperty unitname;
	StringProperty itemName;
	DoubleProperty cess;
	DoubleProperty tax;
	StringProperty unitId;
	StringProperty itemId;
	StringProperty itemcode;
	DoubleProperty mrp;
	DoubleProperty qty;
	StringProperty batch;
	ObjectProperty<LocalDate> expDate;
	StringProperty itemPriceLock;
	String stockName;
	String storeName;

	private String processInstanceId;
	private String taskId;

	public ItemStockPopUp() {
		this.barcode = new SimpleStringProperty("");
		this.unitname = new SimpleStringProperty("");
		this.itemName = new SimpleStringProperty("");
		this.cess = new SimpleDoubleProperty(0);
		this.tax = new SimpleDoubleProperty(0.0);
		this.unitId = new SimpleStringProperty("");
		this.itemId = new SimpleStringProperty("");
		this.itemcode = new SimpleStringProperty("");
		this.mrp = new SimpleDoubleProperty(0.0);
		this.expDate = new SimpleObjectProperty(null);
		this.qty = new SimpleDoubleProperty(0.0);
		;
		this.batch = new SimpleStringProperty("");
		this.itemPriceLock = new SimpleStringProperty();
		this.stockNameProperty = new SimpleStringProperty();
		this.storeNameProperty = new SimpleStringProperty();

	}

	@JsonIgnore
	StringProperty stockNameProperty;
	@JsonIgnore
	StringProperty storeNameProperty;

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {

		this.storeName = storeName;
	}

	@JsonIgnore
	public StringProperty getStoreNameProperty() {
		if (null != storeName) {
			storeNameProperty.set(storeName);
		}
		return storeNameProperty;
	}

	public void setStoreNameProperty(StringProperty storeNameProperty) {
		this.storeNameProperty = storeNameProperty;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public StringProperty getStockNameProperty() {
		if (null != stockName) {
			stockNameProperty.set(stockName);
		}
		return stockNameProperty;
	}

	public void setStockNameProperty(StringProperty stockNameProperty) {
		this.stockNameProperty = stockNameProperty;
	}

	public StringProperty getBarcodeProperty() {
		return barcode;
	}

	public void setBarcodeProperty(StringProperty barcode) {
		this.barcode = barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode.set(barcode);
	}

	public String getBarcode() {
		return barcode.get();
	}

	public StringProperty getUnitnameProperty() {
		return unitname;
	}

	public String getUnitname() {
		return unitname.get();
	}

	public void setUnitnameProperty(StringProperty unitname) {
		this.unitname = unitname;
	}

	public void setUnitname(String unitname) {
		this.unitname.set(unitname);
	}

	public StringProperty getItemNameProperty() {
		return itemName;
	}

	public String getItemName() {
		return itemName.get();
	}

	public void setItemNameProperty(StringProperty itemName) {
		this.itemName = itemName;
	}

	public void setItemName(String itemName) {
		this.itemName.set(itemName);
	}

	public DoubleProperty getCessProperty() {
		return cess;
	}

	public void setCessProperty(DoubleProperty cess) {
		this.cess = cess;
	}

	public Double getCess() {
		return cess.get();
	}

	public void setCess(Double cess) {
		this.cess.set(cess);
	}

	public DoubleProperty getTaxProperty() {
		return tax;
	}

	public void setTaxProperty(DoubleProperty tax) {
		this.tax = tax;
	}

	public Double getTax() {
		return tax.get();
	}

	public void setTax(Double tax) {
		this.tax.set(tax);
	}

	public ObjectProperty<LocalDate> getExpDateProperty() {
		return expDate;
	}

	public void setExpDateProperty(ObjectProperty<LocalDate> expDateProperty) {
		this.expDate = expDateProperty;
	}

	@JsonProperty("expDate")
	public java.sql.Date getexpiryDate() {
		if (null == this.expDate.get()) {
			return null;
		} else {
			return java.sql.Date.valueOf(this.expDate.get());
		}

	}

	public void setexpiryDate(java.sql.Date expiryDateProperty) {
		if (null != expiryDateProperty)
			this.expDate.set(expiryDateProperty.toLocalDate());
	}

	public StringProperty getUnitIdProperty() {
		return unitId;
	}

	public void setUnitIdProperty(StringProperty unitId) {
		this.unitId = unitId;
	}

	public String getUnitId() {
		return unitId.get();
	}

	public void setUnitId(String unitId) {
		this.unitId.set(unitId);
	}

	public StringProperty getItemIdProperty() {
		return itemId;
	}

	public void setItemIdProperty(StringProperty itemId) {
		this.itemId = itemId;
	}

	public String getItemId() {
		return itemId.get();
	}

	public void setItemId(String itemId) {
		this.itemId.set(itemId);
	}

	public StringProperty getItemcodeProperty() {
		return itemcode;
	}

	public void setItemcodeProperty(StringProperty itemcode) {
		this.itemcode = itemcode;
	}

	public String getItemcode() {
		return itemcode.get();
	}

	public void setItemcode(String itemcode) {
		this.itemcode.set(itemcode);
	}

	public DoubleProperty getMrpProperty() {
		return mrp;
	}

	public void setMrpProperty(DoubleProperty mrp) {
		this.mrp = mrp;
	}

	public Double getMrp() {
		return mrp.get();
	}

	public void setMrp(Double mrp) {
		this.mrp.set(mrp);
	}

	public DoubleProperty getQtyProperty() {
		return qty;
	}

	public void setQtyProperty(DoubleProperty qty) {
		this.qty = qty;
	}

	public Double getQty() {
		return qty.get();
	}

	public void setQty(Double qty) {
		this.qty.set(qty);
	}

	public StringProperty getBatchProperty() {
		return batch;
	}

	public void setBatchProperty(StringProperty batch) {
		this.batch = batch;
	}

	public StringProperty getItemPriceLockProperty() {
		return itemPriceLock;
	}

	public void setitemPriceLock(StringProperty itemPriceLock) {
		this.itemPriceLock = itemPriceLock;
	}

	public void setitemPriceLock(String itemPriceLock) {
		this.itemPriceLock.set(itemPriceLock);
	}

	public String getitemPriceLock() {
		return itemPriceLock.get();
	}

	public String getBatch() {
		return batch.get();
	}

	public void setBatch(String batch) {
		this.batch.set(batch);
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ItemStockPopUp [processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

}
