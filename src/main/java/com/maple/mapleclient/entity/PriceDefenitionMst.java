package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PriceDefenitionMst {
	
	String id;
	private String priceLevelName;
	private String priceCalculator;
	private String  processInstanceId;
	private String taskId;
	
	
	
	public PriceDefenitionMst() {
		this.priceLevelNameProperty = new SimpleStringProperty("");
		this.priceCalculatorNameProperty = new SimpleStringProperty("");
	}
	@JsonIgnore
	private StringProperty priceLevelNameProperty;
	
	@JsonIgnore
	private StringProperty priceCalculatorNameProperty;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPriceLevelName() {
		return priceLevelName;
	}
	public void setPriceLevelName(String priceLevelName) {
		this.priceLevelName = priceLevelName;
	}
	public String getPriceCalculator() {
		return priceCalculator;
	}
	public void setPriceCalculator(String priceCalculator) {
		this.priceCalculator = priceCalculator;
	}
	public StringProperty getPriceLevelNameProperty() {
		
		priceLevelNameProperty.set(priceLevelName);
		return priceLevelNameProperty;
	}
	public void setPriceLevelNameProperty(StringProperty priceLevelNameProperty) {
		this.priceLevelNameProperty = priceLevelNameProperty;
	}
	public StringProperty getPriceCalculatorNameProperty() {
		priceCalculatorNameProperty.set(priceCalculator);
		return priceCalculatorNameProperty;
	}
	public void setPriceCalculatorNameProperty(StringProperty priceCalculatorNameProperty) {
		this.priceCalculatorNameProperty = priceCalculatorNameProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PriceDefenitionMst [id=" + id + ", priceLevelName=" + priceLevelName + ", priceCalculator="
				+ priceCalculator + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	

}
