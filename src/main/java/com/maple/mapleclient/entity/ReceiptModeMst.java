package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ReceiptModeMst {
	
	String id;
	
	String status;
	
	private String receiptMode;
	private String parentId;
	private String parentName;
	
	CompanyMst companyMst;
	 String creditCardStatus;
	 private String  processInstanceId;
		private String taskId;
	private StringProperty receiptModeProperty;
	private StringProperty statusModeProperty;
	
	private StringProperty creditCardStatusProperty;
	public ReceiptModeMst() {
		this.receiptModeProperty = new SimpleStringProperty("");
		this.statusModeProperty = new SimpleStringProperty("");
		this.parentNameProperty = new SimpleStringProperty();
		this.creditCardStatusProperty=new SimpleStringProperty("");
	}
	
	@JsonIgnore
	private StringProperty parentNameProperty;

	


	public StringProperty getParentNameProperty() {
		parentNameProperty.set(parentName);
		return parentNameProperty;
	}


	public void setParentNameProperty(StringProperty parentNameProperty) {
		this.parentNameProperty = parentNameProperty;
	}


	public String getParentId() {
		return parentId;
	}


	public void setParentId(String parentId) {
		this.parentId = parentId;
	}


	public String getParentName() {
		return parentName;
	}


	public void setParentName(String parentName) {
		this.parentName = parentName;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		receiptModeProperty.set(receiptMode);
		
		this.receiptMode = receiptMode;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		statusModeProperty.set(status);
		this.status = status;
	}
	
	
	@JsonIgnore
	public StringProperty getReceiptModeProperty() {
		receiptModeProperty.set(receiptMode);
		
		return receiptModeProperty;
	}
	public void setReceiptModeProperty(StringProperty receiptModeProperty) {
		this.receiptModeProperty = receiptModeProperty;
	}
	@JsonIgnore
	public StringProperty getStatusModeProperty() {
		
		statusModeProperty.set(status);
		return statusModeProperty;
	}
	public void setStatusModeProperty(StringProperty statusModeProperty) {
		this.statusModeProperty = statusModeProperty;
	}


	public String getCreditCardStatus() {
		return creditCardStatus;
	}


	public void setCreditCardStatus(String creditCardStatus) {
		this.creditCardStatus = creditCardStatus;
	}

   @JsonIgnore
	public StringProperty getCreditCardStatusProperty() {
	
	creditCardStatusProperty.setValue(creditCardStatus);
		return creditCardStatusProperty;
	}


	public void setCreditCardStatusProperty(StringProperty creditCardStatusProperty) {
		this.creditCardStatusProperty = creditCardStatusProperty;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "ReceiptModeMst [id=" + id + ", status=" + status + ", receiptMode=" + receiptMode + ", parentId="
				+ parentId + ", parentName=" + parentName + ", companyMst=" + companyMst + ", creditCardStatus="
				+ creditCardStatus + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", receiptModeProperty=" + receiptModeProperty + ", statusModeProperty=" + statusModeProperty
				+ ", creditCardStatusProperty=" + creditCardStatusProperty + ", parentNameProperty="
				+ parentNameProperty + "]";
	}




	

}
