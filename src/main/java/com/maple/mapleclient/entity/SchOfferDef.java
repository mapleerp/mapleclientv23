package com.maple.mapleclient.entity;

import java.io.Serializable;



import org.springframework.stereotype.Component;
 
public class SchOfferDef implements Serializable{
	private static final long serialVersionUID = 1L;

	String id;
	
	String offerType;
	
	String branchCode;
	private String  processInstanceId;
	private String taskId;

	private CompanyMst companyMst;
	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SchOfferDef [id=" + id + ", offerType=" + offerType + ", branchCode=" + branchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ "]";
	}


}
