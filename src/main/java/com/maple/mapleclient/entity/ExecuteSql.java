package com.maple.mapleclient.entity;

import java.io.Serializable;

 

import com.fasterxml.jackson.annotation.JsonProperty;


 
public class ExecuteSql implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	 
 
   private String id;
	 
 
	private CompanyMst companyMst;
	
	
 
  String sqlToExecute;
  
  String brachCode;
  private String  processInstanceId;
  private String taskId;
  
  boolean executeInServer;

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public CompanyMst getCompanyMst() {
	return companyMst;
}

public void setCompanyMst(CompanyMst companyMst) {
	this.companyMst = companyMst;
}

public String getSqlToExecute() {
	return sqlToExecute;
}

public void setSqlToExecute(String sqlToExecute) {
	this.sqlToExecute = sqlToExecute;
}

public String getBrachCode() {
	return brachCode;
}

public void setBrachCode(String brachCode) {
	this.brachCode = brachCode;
}

public boolean isExecuteInServer() {
	return executeInServer;
}

public void setExecuteInServer(boolean executeInServer) {
	this.executeInServer = executeInServer;
}

public String getProcessInstanceId() {
	return processInstanceId;
}

public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}

public String getTaskId() {
	return taskId;
}

public void setTaskId(String taskId) {
	this.taskId = taskId;
}

@Override
public String toString() {
	return "ExecuteSql [id=" + id + ", companyMst=" + companyMst + ", sqlToExecute=" + sqlToExecute + ", brachCode="
			+ brachCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", executeInServer="
			+ executeInServer + "]";
}
  
	
	

}
