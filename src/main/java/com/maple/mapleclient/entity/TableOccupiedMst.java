package com.maple.mapleclient.entity;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

public class TableOccupiedMst {

	String id;
	String tableId;
	Timestamp occupiedTime;
	Timestamp orderedTime;
	Timestamp billingTime;
	Timestamp deliveryTime;
	Timestamp kitchenReadyTime;
	Double invoiceAmount;
	String waiterId;
	
	
	
	SalesTransHdr salesTransHdr;
	private String  processInstanceId;
	private String taskId;
	
	

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getTableId() {
		return tableId;
	}



	public void setTableId(String tableId) {
		this.tableId = tableId;
	}



	public Timestamp getOccupiedTime() {
		return occupiedTime;
	}



	public void setOccupiedTime(Timestamp occupiedTime) {
		this.occupiedTime = occupiedTime;
	}



	public Timestamp getOrderedTime() {
		return orderedTime;
	}



	public void setOrderedTime(Timestamp orderedTime) {
		this.orderedTime = orderedTime;
	}



	public Timestamp getBillingTime() {
		return billingTime;
	}



	public void setBillingTime(Timestamp billingTime) {
		this.billingTime = billingTime;
	}



	public Timestamp getDeliveryTime() {
		return deliveryTime;
	}



	public void setDeliveryTime(Timestamp deliveryTime) {
		this.deliveryTime = deliveryTime;
	}



	public Timestamp getKitchenReadyTime() {
		return kitchenReadyTime;
	}



	public void setKitchenReadyTime(Timestamp kitchenReadyTime) {
		this.kitchenReadyTime = kitchenReadyTime;
	}



	public Double getInvoiceAmount() {
		return invoiceAmount;
	}



	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}



	public String getWaiterId() {
		return waiterId;
	}



	public void setWaiterId(String waiterId) {
		this.waiterId = waiterId;
	}



	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}



	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}



	


	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "TableOccupiedMst [id=" + id + ", tableId=" + tableId + ", occupiedTime=" + occupiedTime
				+ ", orderedTime=" + orderedTime + ", billingTime=" + billingTime + ", deliveryTime=" + deliveryTime
				+ ", kitchenReadyTime=" + kitchenReadyTime + ", invoiceAmount=" + invoiceAmount + ", waiterId="
				+ waiterId + ", salesTransHdr=" + salesTransHdr + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
}
