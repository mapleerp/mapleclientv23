package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BarcodeFormatMst  {
	private String id;
	String propertyName;
	String propertyValue;
	String barcodeFormatName;
	
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty propertyNameProperty;
	
	@JsonIgnore
	private StringProperty propertyValueProperty;
	
	@JsonIgnore
	private StringProperty barcodeFormatNameProperty;
	
	public BarcodeFormatMst() {
		this.propertyNameProperty = new SimpleStringProperty();
		this.propertyValueProperty = new SimpleStringProperty();
		this.barcodeFormatNameProperty = new SimpleStringProperty();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
	public String getBarcodeFormatName() {
		return barcodeFormatName;
	}
	public void setBarcodeFormatName(String barcodeFormatName) {
		this.barcodeFormatName = barcodeFormatName;
	}
	
	public StringProperty getPropertyNameProperty() {
		propertyNameProperty.set(propertyName);
		return propertyNameProperty;
	}
	public void setPropertyNameProperty(String propertyName) {
		this.propertyName = propertyName;
	}
	public StringProperty getPropertyValueProperty() {
		propertyValueProperty.set(propertyValue);
		return propertyValueProperty;
	}
	public void setPropertyValueProperty(String propertyValue) {
		this.propertyValue = propertyValue;
	}
	public StringProperty getBarcodeFormatNameProperty() {
		barcodeFormatNameProperty.set(barcodeFormatName);
		return barcodeFormatNameProperty;
	}
	public void setBarcodeFormatNameProperty(String barcodeFormatName) {
		this.barcodeFormatName = barcodeFormatName;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "BarcodeFormatMst [id=" + id + ", propertyName=" + propertyName + ", propertyValue=" + propertyValue
				+ ", barcodeFormatName=" + barcodeFormatName + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	

}
