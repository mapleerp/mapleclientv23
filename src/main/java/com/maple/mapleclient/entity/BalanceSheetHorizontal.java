package com.maple.mapleclient.entity;

import java.io.Serializable;

 

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class BalanceSheetHorizontal {


   private String id;
	
	String financialYear;
	String assetAccountId;
	String liabilityAccountId;
	Double assetAmount;
	Double liabilityAmount;
	String slNo;
	
	private String  processInstanceId;
	private String taskId;
	
	
	@JsonIgnore
	private StringProperty assetAccountNameProperty;
	
	@JsonIgnore
	private StringProperty liabilityAccountNameProperty;
	
	@JsonIgnore
	private DoubleProperty assetAmountProperty;
	
	@JsonIgnore
	private DoubleProperty liabilityAmountProperty;
	
	@JsonIgnore
	private StringProperty slNoProperty;
	
	@JsonIgnore
	String assetAccountName;
	
	@JsonIgnore
	String liabilityAccountName;
	

	public BalanceSheetHorizontal() {
	

		this.assetAccountNameProperty = new SimpleStringProperty();
		this.liabilityAccountNameProperty = new SimpleStringProperty();
		this.assetAmountProperty = new SimpleDoubleProperty(0.0);
		this.liabilityAmountProperty =  new SimpleDoubleProperty(0.0);
		this.slNoProperty =new SimpleStringProperty();
	}

	public StringProperty getAssetAccountNameProperty() {
		assetAccountNameProperty.set(assetAccountName);
		return assetAccountNameProperty;
	}

	public void setAssetAccountNameProperty(String assetAccountName) {
		this.assetAccountName = assetAccountName;
	}

	public StringProperty getLiabilityAccountNameProperty() {
		liabilityAccountNameProperty.set(liabilityAccountName);
		return liabilityAccountNameProperty;
	}

	public void setLiabilityAccountNameProperty(String liabilityAccountName) {
		this.liabilityAccountName = liabilityAccountName;
	}

	public DoubleProperty getAssetAmountProperty() {
		if(null != assetAmount)
		{
		assetAmountProperty.set(assetAmount);
		}
		return assetAmountProperty;
	}

	public void setAssetAmountProperty(Double assetAmount) {
		this.assetAmount = assetAmount;
	}

	public DoubleProperty getLiabilityAmountProperty() {
		if(null != liabilityAmount)
		{
		liabilityAmountProperty.set(liabilityAmount);
		}
		return liabilityAmountProperty;
	}

	public void setLiabilityAmountProperty(Double liabilityAmount) {
		this.liabilityAmount = liabilityAmount;
	}

	public StringProperty getSlNoProperty() {
		slNoProperty.set(slNo);
		return slNoProperty;
	}

	public void setSlNoProperty(String slNo) {
		this.slNo = slNo;
	}

	public String getAssetAccountName() {
		return assetAccountName;
	}

	public void setAssetAccountName(String assetAccountName) {
		this.assetAccountName = assetAccountName;
	}

	public String getLiabilityAccountName() {
		return liabilityAccountName;
	}

	public void setLiabilityAccountName(String liabilityAccountName) {
		this.liabilityAccountName = liabilityAccountName;
	}

	public String getId() {
		return id;
	}
	
	public String getSlNo() {
		return slNo;
	}

	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getFinancialYear() {
		return financialYear;
	}
	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public String getAssetAccountId() {
		return assetAccountId;
	}

	public void setAssetAccountId(String assetAccountId) {
		this.assetAccountId = assetAccountId;
	}

	public String getLiabilityAccountId() {
		return liabilityAccountId;
	}

	public void setLiabilityAccountId(String liabilityAccountId) {
		this.liabilityAccountId = liabilityAccountId;
	}

	public Double getAssetAmount() {
		return assetAmount;
	}

	public void setAssetAmount(Double assetAmount) {
		this.assetAmount = assetAmount;
	}

	public Double getLiabilityAmount() {
		return liabilityAmount;
	}

	public void setLiabilityAmount(Double liabilityAmount) {
		this.liabilityAmount = liabilityAmount;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "BalanceSheetHorizontal [id=" + id + ", financialYear=" + financialYear + ", assetAccountId="
				+ assetAccountId + ", liabilityAccountId=" + liabilityAccountId + ", assetAmount=" + assetAmount
				+ ", liabilityAmount=" + liabilityAmount + ", slNo=" + slNo + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", assetAccountName=" + assetAccountName + ", liabilityAccountName="
				+ liabilityAccountName + "]";
	}




}
