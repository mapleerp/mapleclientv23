package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SiteMst {

	AccountHeads accountHeads;
	String id;
	String customerName;
	String siteName;
	String addressLine1;
	String addressLine2;
	String mobileNumber;
	String landMark;
	LocalCustomerMst localCustomerMst;
	@JsonIgnore
	private StringProperty customerNameProperty;
	@JsonIgnore
	private StringProperty siteNameProperty;
	@JsonIgnore
	private StringProperty addressLine1Property;
	@JsonIgnore
	private StringProperty mobileNumberProperty;
	@JsonIgnore
	private StringProperty landMarkProperty;
	private String  processInstanceId;
	private String taskId;
	public SiteMst() {



		this.customerNameProperty =new SimpleStringProperty();
		this.siteNameProperty = new SimpleStringProperty();
		this.addressLine1Property = new SimpleStringProperty();
		this.mobileNumberProperty =new SimpleStringProperty();
		this.landMarkProperty = new SimpleStringProperty();
		
	}

	
	public LocalCustomerMst getLocalCustomerMst() {
		return localCustomerMst;
	}
	public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}
	
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
	
	
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}


	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}


	public StringProperty getSiteNameProperty() {
		siteNameProperty.set(siteName);
		return siteNameProperty;
	}


	public void setSiteNameProperty(StringProperty siteNameProperty) {
		this.siteNameProperty = siteNameProperty;
	}





	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public AccountHeads getAccountHeads() {
		return accountHeads;
	}


	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}


	@Override
	public String toString() {
		return "SiteMst [accountHeads=" + accountHeads + ", id=" + id + ", customerName=" + customerName + ", siteName="
				+ siteName + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", mobileNumber="
				+ mobileNumber + ", landMark=" + landMark + ", localCustomerMst=" + localCustomerMst
				+ ", customerNameProperty=" + customerNameProperty + ", siteNameProperty=" + siteNameProperty
				+ ", addressLine1Property=" + addressLine1Property + ", mobileNumberProperty=" + mobileNumberProperty
				+ ", landMarkProperty=" + landMarkProperty + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}


	
	
}
