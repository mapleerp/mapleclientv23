package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class JobCardDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	
    private String id;
	
	private String itemId;
	private Double qty;

	CompanyMst companyMst;
	JobCardHdr jobCardHdr;
	
	private String  processInstanceId;
	private String taskId;
	
	private String locationId;
	
	
	
	public JobCardDtl() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.locationProperty = new SimpleStringProperty("");

		this.qtyProperty = new SimpleDoubleProperty(0.0);
		this.mrpProperty = new SimpleDoubleProperty(0.0);
		this.cessProperty = new SimpleDoubleProperty(0.0);
		this.taxProperty = new SimpleDoubleProperty(0.0);
		this.amountProperty = new SimpleDoubleProperty(0.0);
		

	}
	@JsonIgnore
	String itemName;
	
	@JsonIgnore
	String location;
	
	@JsonIgnore
	Double mrp;
	
	@JsonIgnore
	Double amount;
	
	@JsonIgnore
	DoubleProperty mrpProperty;
	
	@JsonIgnore
	Double cess;
	
	@JsonIgnore
	DoubleProperty cessProperty;
	
	@JsonIgnore
	Double tax;
	
	@JsonIgnore
	DoubleProperty taxProperty;
	
	@JsonIgnore
	StringProperty itemNameProperty;
	
	@JsonIgnore
	StringProperty locationProperty;
	
	@JsonIgnore
	DoubleProperty qtyProperty;
	
	@JsonIgnore
	DoubleProperty amountProperty;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public JobCardHdr getJobCardHdr() {
		return jobCardHdr;
	}
	public void setJobCardHdr(JobCardHdr jobCardHdr) {
		this.jobCardHdr = jobCardHdr;
	}
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	
	
	
	
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public DoubleProperty getMrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}
	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}
	public Double getCess() {
		return cess;
	}
	public void setCess(Double cess) {
		this.cess = cess;
	}
	public DoubleProperty getCessProperty() {
		cessProperty.set(cess);
		return cessProperty;
	}
	public void setCessProperty(DoubleProperty cessProperty) {
		this.cessProperty = cessProperty;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public DoubleProperty getTaxProperty() {
		taxProperty.set(tax);
		return taxProperty;
	}
	public void setTaxProperty(DoubleProperty taxProperty) {
		this.taxProperty = taxProperty;
	}
	
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public StringProperty getLocationProperty() {
		locationProperty.set(location);
		return locationProperty;
	}
	public void setLocationProperty(StringProperty locationProperty) {
		this.locationProperty = locationProperty;
	}
	
	
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public DoubleProperty getAmountProperty() {
		
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "JobCardDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", companyMst=" + companyMst
				+ ", jobCardHdr=" + jobCardHdr + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", locationId=" + locationId + ", itemName=" + itemName + ", location=" + location + ", mrp=" + mrp
				+ ", amount=" + amount + ", cess=" + cess + ", tax=" + tax + "]";
	}

	
	
	
	
	
	
}
