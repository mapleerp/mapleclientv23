package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StoreChangeMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String id;
	String voucherNumber;
	Date voucherDate;
	String store;
	String toStore;

	Date transDate;
	String fromBranch;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty storeProperty;
	
	@JsonIgnore
	private StringProperty toStoreProperty;
	
	
	
	public StoreChangeMst() {
		

		this.voucherDateProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.storeProperty =  new SimpleStringProperty();
		this.toStoreProperty =  new SimpleStringProperty();
	}
	
	
	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public StringProperty getVoucherNumberProperty() {
		voucherDateProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public StringProperty getStoreProperty() {
		storeProperty.set(store);
		return storeProperty;
	}
	public void setStoreProperty(String store) {
		this.store = store;
	}
	public StringProperty getToStoreProperty() {
		toStoreProperty.set(toStore);
		return toStoreProperty;
	}
	public void setToStoreProperty(String toStore) {
		this.toStore = toStore;
	}


	CompanyMst companyMst = null;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public Date getTransDate() {
		return transDate;
	}
	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	
	
	public String getToStore() {
		return toStore;
	}
	public void setToStore(String toStore) {
		this.toStore = toStore;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "StoreChangeMst [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", store=" + store + ", toStore=" + toStore + ", transDate=" + transDate + ", fromBranch="
				+ fromBranch + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst="
				+ companyMst + "]";
	}

	
	
}
