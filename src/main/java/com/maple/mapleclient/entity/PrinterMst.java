package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PrinterMst {
	
	String id;
	String printerName;
	String printerDescription;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty printerNameProperty;
	
	@JsonIgnore
	private StringProperty printerDescriptionProperty;
	
	
	public PrinterMst() {
	
		this.printerNameProperty = new SimpleStringProperty();
		this.printerDescriptionProperty = new SimpleStringProperty();
	}
	
	@JsonIgnore
	public StringProperty getprinterNameProperty() {
		printerNameProperty.set(printerName);
		return printerNameProperty;
	}

	public void setprinterNameProperty(String printerName) {
		this.printerName = printerName;
	}
	
	@JsonIgnore
	public StringProperty getprinterDescriptionProperty() {
		printerDescriptionProperty.set(printerDescription);
		return printerDescriptionProperty;
	}

	public void setprinterDescriptionProperty(String printerDescription) {
		this.printerDescription = printerDescription;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrinterName() {
		return printerName;
	}
	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}
	public String getPrinterDescription() {
		return printerDescription;
	}
	public void setPrinterDescription(String printerDescription) {
		this.printerDescription = printerDescription;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PrinterMst [id=" + id + ", printerName=" + printerName + ", printerDescription=" + printerDescription
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	

}
