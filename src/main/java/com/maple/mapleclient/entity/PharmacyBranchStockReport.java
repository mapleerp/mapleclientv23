package com.maple.mapleclient.entity;

public class PharmacyBranchStockReport {
	
	private String groupName;
	
	private String itemName;
	
	private String batchCode;
	
	private String itemCode;
	
	private String expiryDate;
	
	private String qty;
	
	private String rate;
	
	private String amount;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "PharmacyBranchStockReport [groupName=" + groupName + ", itemName=" + itemName + ", batchCode="
				+ batchCode + ", itemCode=" + itemCode + ", expiryDate=" + expiryDate + ", qty=" + qty + ", rate="
				+ rate + ", amount=" + amount + "]";
	}
	

}
