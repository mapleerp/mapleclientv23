package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EventMst {
	
	
	
	public EventMst()
	{
		
		this.eventVenueProperty= new SimpleStringProperty("");
		this.eventNameProperty= new SimpleStringProperty("");

		this.eventIdProperty= new SimpleStringProperty("");
		this.orgIdProperty= new SimpleStringProperty("");
		
		
		this.totalIncomeProperty= new SimpleDoubleProperty();
		this.totalExpenseProperty= new SimpleDoubleProperty();

		
	}
	

	@JsonIgnore
    private StringProperty eventVenueProperty;
  @JsonIgnore
    private StringProperty eventNameProperty;
   @JsonIgnore
	private ObjectProperty<LocalDate> eventStartDateProperty;
   @JsonIgnore
  	private ObjectProperty<LocalDate> eventEndDateProperty;
  @JsonIgnore
    private DoubleProperty totalIncomeProperty;
  @JsonIgnore
    private DoubleProperty totalExpenseProperty;
  @JsonIgnore
    private StringProperty eventIdProperty;
   @JsonIgnore
    private StringProperty orgIdProperty;

  
    String id;
    

	OrgMst orgMst;

	String eventId;
	
	String orgId;
	
	String eventName;
	

	Date eventStartDate;

	String eventVenue;
	
	
	Date eventEndDate;
	
	double totalIncome;
	
	double totalExpense;
	
	String subGroup;
	
	private String  processInstanceId;
	private String taskId;

	public OrgMst getOrgMst() {
		return orgMst;
	}

	public void setOrgMst(OrgMst orgMst) {
		this.orgMst = orgMst;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StringProperty getEventIdProperty() {
		eventIdProperty.set(eventId);
		return eventIdProperty;
	}

	public void setEventIdProperty(StringProperty eventIdProperty) {
		this.eventIdProperty = eventIdProperty;
	}

	public StringProperty getOrgIdProperty() {
		orgIdProperty.set(orgId);
		return orgIdProperty;
	}

	public void setOrgIdProperty(StringProperty orgIdProperty) {
		this.orgIdProperty = orgIdProperty;
	}

	public ObjectProperty<LocalDate> getEventStartDateProperty() {
		return eventStartDateProperty;
	}

	public void setEventStartDateProperty(ObjectProperty<LocalDate> eventStartDateProperty) {
		this.eventStartDateProperty = eventStartDateProperty;
	}

	public ObjectProperty<LocalDate> getEventEndDateProperty() {
		return eventEndDateProperty;
	}

	public void setEventEndDateProperty(ObjectProperty<LocalDate> eventEndDateProperty) {
		this.eventEndDateProperty = eventEndDateProperty;
	}

	public StringProperty getEventVenueProperty() {
		eventVenueProperty.set(eventVenue);
		return eventVenueProperty;
	}

	public void setEventVenueProperty(StringProperty eventVenueProperty) {
		this.eventVenueProperty = eventVenueProperty;
	}

	public StringProperty getEventNameProperty() {
		eventNameProperty.set(eventName);
		return eventNameProperty;
	}

	public void setEventNameProperty(StringProperty eventNameProperty) {
		this.eventNameProperty = eventNameProperty;
	}

	

	public DoubleProperty getTotalIncomeProperty() {
		totalIncomeProperty.set(totalIncome);
		return totalIncomeProperty;
	}

	public void setTotalIncomeProperty(DoubleProperty totalIncomeProperty) {
		this.totalIncomeProperty = totalIncomeProperty;
	}

	public DoubleProperty getTotalExpenseProperty() {
		totalExpenseProperty.set(totalExpense);
		return totalExpenseProperty;
	}

	public void setTotalExpenseProperty(DoubleProperty totalExpenseProperty) {
		this.totalExpenseProperty = totalExpenseProperty;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Date getEventStartDate() {
		return eventStartDate;
	}

	public void setEventStartDate(Date eventStartDate) {
		this.eventStartDate = eventStartDate;
	}

	public String getEventVenue() {
		return eventVenue;
	}

	public void setEventVenue(String eventVenue) {
		this.eventVenue = eventVenue;
	}

	public Date getEventEndDate() {
		return eventEndDate;
	}

	public void setEventEndDate(Date eventEndDate) {
		this.eventEndDate = eventEndDate;
	}

	public double getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(double totalIncome) {
		this.totalIncome = totalIncome;
	}

	public double getTotalExpense() {
		return totalExpense;
	}

	public void setTotalExpense(double totalExpense) {
		this.totalExpense = totalExpense;
	}

	public String getSubGroup() {
		return subGroup;
	}

	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "EventMst [id=" + id + ", orgMst=" + orgMst + ", eventId=" + eventId + ", orgId=" + orgId
				+ ", eventName=" + eventName + ", eventStartDate=" + eventStartDate + ", eventVenue=" + eventVenue
				+ ", eventEndDate=" + eventEndDate + ", totalIncome=" + totalIncome + ", totalExpense=" + totalExpense
				+ ", subGroup=" + subGroup + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
	
		
	
}
