package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

public class EventNotificationMst {
	
	
	

	String eventId;
	

	String notificationMessage;

	String messageId;
	

	Date messageDate;
	
	String orgId;
	private String  processInstanceId;
	private String taskId;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getNotificationMessage() {
		return notificationMessage;
	}

	public void setNotificationMessage(String notificationMessage) {
		this.notificationMessage = notificationMessage;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Date getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "EventNotificationMst [eventId=" + eventId + ", notificationMessage=" + notificationMessage
				+ ", messageId=" + messageId + ", messageDate=" + messageDate + ", orgId=" + orgId
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


}
