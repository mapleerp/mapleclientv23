package com.maple.mapleclient.entity;

import java.io.Serializable;


import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class PDCReconcileDtl implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	   String id;
		
		String bank;
		
//		Date transDate;
		
		String intrumentNo;
		
		String account;
		
		Double amount;
		
		String transactionDetails;
		
		CompanyMst companyMst;
		
		PDCReconcileHdr pdcReconcileHdr;
		
		String pdcId;
		
		String remark;
		private String  processInstanceId;
		private String taskId;
		
		@JsonIgnore
		String accountName;
		
		@JsonIgnore
		StringProperty accountNameProperty;
		
		@JsonIgnore
		StringProperty intrumentNoProperty;
		
		@JsonIgnore
		DoubleProperty amountProperty;
		
		@JsonIgnore
		StringProperty transactionDetailsProperty;
		
		@JsonIgnore
		StringProperty bankProperty;
		

		
		public PDCReconcileDtl() {
			this.transDate = new SimpleObjectProperty<LocalDate>(null);
			this.accountNameProperty = new SimpleStringProperty("");
			this.intrumentNoProperty = new SimpleStringProperty("");
			this.amountProperty = new SimpleDoubleProperty();
			this.transactionDetailsProperty = new SimpleStringProperty("");
			this.bankProperty = new SimpleStringProperty("");
		}

		@JsonIgnore
		private ObjectProperty<LocalDate> transDate;
		
		@JsonProperty("transDate")
		public Date getTransDate() {
			if (null == this.transDate.get()) {
				return null;
			} else {
				return Date.valueOf(this.transDate.get());
			}
		}

		public void setTransDate(Date transDate) {
			if (null != transDate)
				this.transDate.set(transDate.toLocalDate());
		}
		
		
		public ObjectProperty<LocalDate> getTransDateProperty() {
			return transDate;
		}

		public void setTransDateProperty(ObjectProperty<LocalDate> transDateProperty) {
			this.transDate = transDateProperty;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getBank() {
			return bank;
		}

		public void setBank(String bank) {
			this.bank = bank;
		}

		public String getIntrumentNo() {
			return intrumentNo;
		}

		public void setIntrumentNo(String intrumentNo) {
			this.intrumentNo = intrumentNo;
		}

		public String getAccount() {
			return account;
		}

		public void setAccount(String account) {
			this.account = account;
		}

		public Double getAmount() {
			return amount;
		}

		public void setAmount(Double amount) {
			this.amount = amount;
		}

		public String getTransactionDetails() {
			return transactionDetails;
		}

		public void setTransactionDetails(String transactionDetails) {
			this.transactionDetails = transactionDetails;
		}

		public CompanyMst getCompanyMst() {
			return companyMst;
		}

		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}

		public String getAccountName() {
			return accountName;
		}

		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}
			

		public PDCReconcileHdr getPdcReconcileHdr() {
			return pdcReconcileHdr;
		}

		public void setPdcReconcileHdr(PDCReconcileHdr pdcReconcileHdr) {
			this.pdcReconcileHdr = pdcReconcileHdr;
		}

		public StringProperty getAccountNameProperty() {
			accountNameProperty.set(accountName);
			return accountNameProperty;
		}

		public void setAccountNameProperty(StringProperty accountNameProperty) {
			this.accountNameProperty = accountNameProperty;
		}

		public StringProperty getIntrumentNoProperty() {
			intrumentNoProperty.set(intrumentNo);
			return intrumentNoProperty;
		}

		public void setIntrumentNoProperty(StringProperty intrumentNoProperty) {
			this.intrumentNoProperty = intrumentNoProperty;
		}

		public DoubleProperty getAmountProperty() {
			amountProperty.set(amount);
			return amountProperty;
		}

		public void setAmountProperty(DoubleProperty amountProperty) {
			this.amountProperty = amountProperty;
		}

		public StringProperty getTransactionDetailsProperty() {
			transactionDetailsProperty.set(transactionDetails);
			return transactionDetailsProperty;
		}

		public void setTransactionDetailsProperty(StringProperty transactionDetailsProperty) {
			this.transactionDetailsProperty = transactionDetailsProperty;
		}

		public StringProperty getBankProperty() {
			
			bankProperty.set(bank);
			return bankProperty;
		}

		public void setBankProperty(StringProperty bankProperty) {
			this.bankProperty = bankProperty;
		}

		

		public String getPdcId() {
			return pdcId;
		}

		public void setPdcId(String pdcId) {
			this.pdcId = pdcId;
		}
		
		

		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}


		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "PDCReconcileDtl [id=" + id + ", bank=" + bank + ", intrumentNo=" + intrumentNo + ", account="
					+ account + ", amount=" + amount + ", transactionDetails=" + transactionDetails + ", companyMst="
					+ companyMst + ", pdcReconcileHdr=" + pdcReconcileHdr + ", pdcId=" + pdcId + ", remark=" + remark
					+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", accountName=" + accountName
					+ "]";
		}

		
		

}
