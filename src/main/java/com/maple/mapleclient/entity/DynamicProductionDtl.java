package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class DynamicProductionDtl {
	
    private String id;
	String itemId;
	Double qty;
	String unitId;
	String batch;
	private DynamicProductionHdr dynamicProductionHdr;

	private String unitName;
	private String itemName;
	
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty itemNameproperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	public DynamicProductionDtl() {
		this.itemNameproperty = new SimpleStringProperty();
		this.batchProperty =  new SimpleStringProperty();
		this.qtyProperty =new SimpleDoubleProperty();
		this.unitNameProperty =  new SimpleStringProperty();
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public StringProperty getItemNameproperty() {
		itemNameproperty.set(itemName);
		return itemNameproperty;
	}

	public void setItemNameproperty(String itemName) {
		this.itemName = itemName;
	}

	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(String batch) {
		this.batch = batch;
	}

	
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setQtyProperty(Double qty) {
		this.qty = qty;
	}

	public StringProperty getUnitNameProperty() {
		return unitNameProperty;
	}

	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	


	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public DynamicProductionHdr getDynamicProductionHdr() {
		return dynamicProductionHdr;
	}

	public void setDynamicProductionHdr(DynamicProductionHdr dynamicProductionHdr) {
		this.dynamicProductionHdr = dynamicProductionHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "DynamicProductionDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", unitId=" + unitId
				+ ", batch=" + batch + ", dynamicProductionHdr=" + dynamicProductionHdr + ", unitName=" + unitName
				+ ", itemName=" + itemName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}




}
