package com.maple.mapleclient.entity;

import java.util.Date;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PurchasePriceDefinitionMst {

	private String id;

	String purchaseVoucherNumber;

	String itemId;

	String itemName;

	String batch;

	String unitId;

	String unitName;

	String status;

	String branchCode;

	Date purchaseVoucherDate;
	
	CompanyMst companyMst;
	
	String purchaseHdrId;

	
	@JsonProperty
	StringProperty itemNameProperty;
	
	@JsonProperty
	StringProperty unitNameProperty;
	
	@JsonProperty
	StringProperty batchProperty;
	
	

	public PurchasePriceDefinitionMst() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPurchaseVoucherNumber() {
		return purchaseVoucherNumber;
	}

	public void setPurchaseVoucherNumber(String purchaseVoucherNumber) {
		this.purchaseVoucherNumber = purchaseVoucherNumber;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Date getPurchaseVoucherDate() {
		return purchaseVoucherDate;
	}

	public void setPurchaseVoucherDate(Date purchaseVoucherDate) {
		this.purchaseVoucherDate = purchaseVoucherDate;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	
	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public String getPurchaseHdrId() {
		return purchaseHdrId;
	}

	public void setPurchaseHdrId(String purchaseHdrId) {
		this.purchaseHdrId = purchaseHdrId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitId);
		return unitNameProperty;
	}

	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}

	
	
}
