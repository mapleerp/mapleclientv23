package com.maple.mapleclient.entity;

public class NutritionValueMst {
	
	String id;
	String servingSize;
	String calories;
	String fat;
	String printerName;
	
	ItemMst itemMst;
	private String  processInstanceId;
	private String taskId;
	
	public ItemMst getItemMst() {
		return itemMst;
	}
	public void setItemMst(ItemMst itemMst) {
		this.itemMst = itemMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}


	public String getServingSize() {
		return servingSize;
	}
	public void setServingSize(String servingSize) {
		this.servingSize = servingSize;
	}
	public String getCalories() {
		return calories;
	}
	public void setCalories(String calories) {
		this.calories = calories;
	}
	public String getFat() {
		return fat;
	}
	public void setFat(String fat) {
		this.fat = fat;
	}
	
	public String getPrinterName() {
		return printerName;
	}
	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "NutritionValueMst [id=" + id + ", servingSize=" + servingSize + ", calories=" + calories + ", fat="
				+ fat + ", printerName=" + printerName + ", itemMst=" + itemMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

}
