package com.maple.mapleclient.entity;

public class KitchenOrder {

	String itemName;
	Double qty;
	Double mrp;
	String ServingTableName;
	private String  processInstanceId;
	private String taskId;
	
	public String getServingTableName() {
		return ServingTableName;
	}
	public void setServingTableName(String servingTableName) {
		ServingTableName = servingTableName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "KitchenOrder [itemName=" + itemName + ", qty=" + qty + ", mrp=" + mrp + ", ServingTableName="
				+ ServingTableName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
