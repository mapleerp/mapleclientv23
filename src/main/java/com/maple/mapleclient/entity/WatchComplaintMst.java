package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WatchComplaintMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String id;
	String complaint;
	String branchCode;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	
	public WatchComplaintMst() {
		this.compliantProperty = new SimpleStringProperty("");
	}
	@JsonIgnore
	private StringProperty compliantProperty;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getComplaint() {
		return complaint;
	}
	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	
	public StringProperty getCompliantProperty() {
		compliantProperty.set(complaint);
		return compliantProperty;
	}
	public void setCompliantProperty(StringProperty compliantProperty) {
		this.compliantProperty = compliantProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "WatchComplaintMst [id=" + id + ", complaint=" + complaint + ", branchCode=" + branchCode
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}


}
