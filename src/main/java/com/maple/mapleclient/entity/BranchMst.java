package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BranchMst {

	String id;

	String branchName;
	String branchCode;
	String branchGst;
	String branchState;
	String myBranch;
	String bankName;
	String accountNumber;
	String ifsc;
	String bankBranch;

	private String processInstanceId;
	private String taskId;

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	String branchEmail;
	String branchWebsite;
	String branchTelNo;
	String branchAddress1;
	String branchAddress2;
	@JsonIgnore
	private StringProperty branchAddressProperty;
	@JsonIgnore
	private StringProperty branchNameProperty;
	@JsonIgnore
	private StringProperty branchCodeProperty;
	@JsonIgnore
	private StringProperty branchGSTProperty;
	@JsonIgnore
	private StringProperty branchStateProperty;

	public BranchMst() {
		this.branchAddressProperty = new SimpleStringProperty("");
		this.branchNameProperty = new SimpleStringProperty("");
		this.branchCodeProperty = new SimpleStringProperty("");
		this.branchGSTProperty = new SimpleStringProperty("");
		this.branchStateProperty = new SimpleStringProperty("");

	}

	public String getBranchEmail() {
		return branchEmail;
	}

	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}

	public String getBranchWebsite() {
		return branchWebsite;
	}

	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}

	public StringProperty getBranchNameProperty() {
		branchNameProperty.set(branchName);
		return branchNameProperty;
	}

	public void setBranchNameProperty(StringProperty branchNameProperty) {
		this.branchNameProperty = branchNameProperty;
	}

	public StringProperty getBranchAddressProperty() {
		branchAddressProperty.set(branchAddress1);
		return branchAddressProperty;
	}

	public void setBranchAddressProperty(StringProperty branchAddressProperty) {
		this.branchAddressProperty = branchAddressProperty;
	}

	public StringProperty getBranchCodeProperty() {
		branchCodeProperty.set(branchCode);
		return branchCodeProperty;
	}

	public void setBranchCodeProperty(StringProperty branchCodeProperty) {
		this.branchCodeProperty = branchCodeProperty;
	}

	public StringProperty getBranchGSTProperty() {
		branchGSTProperty.set(branchGst);
		return branchGSTProperty;
	}

	public void setBranchGSTProperty(StringProperty branchGSTProperty) {
		this.branchGSTProperty = branchGSTProperty;
	}

	public StringProperty getBranchStateProperty() {
		branchStateProperty.set(branchState);
		return branchStateProperty;
	}

	public void setBranchStateProperty(StringProperty branchStateProperty) {
		this.branchStateProperty = branchStateProperty;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchState() {
		return branchState;
	}

	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}

	public String getBranchGst() {
		return branchGst;
	}

	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}

	public String getMyBranch() {
		return myBranch;
	}

	public void setMyBranch(String myBranch) {
		this.myBranch = myBranch;
	}

	public String getBranchTelNo() {
		return branchTelNo;
	}

	public void setBranchTelNo(String branchTelNo) {
		this.branchTelNo = branchTelNo;
	}

	public String getBranchAddress1() {
		return branchAddress1;
	}

	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}

	public String getBranchAddress2() {
		return branchAddress2;
	}

	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "BranchMst [id=" + id + ", branchName=" + branchName + ", branchCode=" + branchCode + ", branchGst="
				+ branchGst + ", branchState=" + branchState + ", myBranch=" + myBranch + ", bankName=" + bankName
				+ ", accountNumber=" + accountNumber + ", ifsc=" + ifsc + ", bankBranch=" + bankBranch
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", branchEmail=" + branchEmail
				+ ", branchWebsite=" + branchWebsite + ", branchTelNo=" + branchTelNo + ", branchAddress1="
				+ branchAddress1 + ", branchAddress2=" + branchAddress2 + "]";
	}

}
