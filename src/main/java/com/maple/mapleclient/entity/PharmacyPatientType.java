package com.maple.mapleclient.entity;


public class PharmacyPatientType {
	
	
	private String id;
	private String patientType;
	
	private String  processInstanceId;
	private String taskId;
	
	CompanyMst companyMst;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPatientType() {
		return patientType;
	}
	public void setPatientType(String patientType) {
		this.patientType = patientType;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	@Override
	public String toString() {
		return "PharmacyPatientType [id=" + id + ", patientType=" + patientType + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst + "]";
	}
	

}
