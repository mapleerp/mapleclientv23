package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BalanceSheetConfigAsset {

	
	String id;
	String serialNumber;
	
	String accountId;
	String accountName;
	
	
	public BalanceSheetConfigAsset() {
		this.serialNumberProperty=new SimpleStringProperty();
		this.accountNameProperty=new SimpleStringProperty();
		
	}
	
	@JsonIgnore
	StringProperty serialNumberProperty;
	
	@JsonIgnore
	StringProperty accountNameProperty;
	
	
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public StringProperty getSerialNumberProperty() {
		if(null != serialNumber) {
			serialNumberProperty.set(serialNumber);
		}
		return serialNumberProperty;
	}
	public void setSerialNumberProperty(StringProperty serialNumberProperty) {
		this.serialNumberProperty = serialNumberProperty;
	}
	public StringProperty getAccountNameProperty() {
		if(null != accountName) {
			accountNameProperty.set(accountName);
		}
		return accountNameProperty;
	}
	public void setAccountNameProperty(StringProperty accountNameProperty) {
		this.accountNameProperty = accountNameProperty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	@Override
	public String toString() {
		return "BalanceSheetConfigAsset [id=" + id + ", serialNumber=" + serialNumber + ", accountId=" + accountId
				+ ", accountName=" + accountName + ", serialNumberProperty=" + serialNumberProperty
				+ ", accountNameProperty=" + accountNameProperty + "]";
	}
	
	
	
	
}
