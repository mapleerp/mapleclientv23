package com.maple.mapleclient.entity;


import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class FinanceMst implements Serializable {
	private static final long serialVersionUID = 1L;

	String id;
	String name;
	String phoneNo;
	String branchCode;
	
	CompanyMst companyMst;
	
	private StringProperty nameProperty;
	private StringProperty phoneNoProperty;
	
	
	private String  processInstanceId;
	private String taskId;
	
	
	public FinanceMst() {
		this.nameProperty = new SimpleStringProperty("");
		this.phoneNoProperty = new SimpleStringProperty("");
		

		
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public StringProperty getNameProperty() {
		nameProperty.set(name);
		return nameProperty;
	}
	public void setNameProperty(StringProperty nameProperty) {
		this.nameProperty = nameProperty;
	}
	public StringProperty getPhoneNoProperty() {
		phoneNoProperty.set(phoneNo);
		return phoneNoProperty;
	}
	public void setPhoneNoProperty(StringProperty phoneNoProperty) {
		this.phoneNoProperty = phoneNoProperty;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "FinanceMst [id=" + id + ", name=" + name + ", phoneNo=" + phoneNo + ", branchCode=" + branchCode
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	
	
	

}
