package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class ItemNameInLanguage {
	
	

   private String id;
	
	private String itemId;
	private String itemNameInLang;
	private String langName;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private String itemName;
	
	
	
	public ItemNameInLanguage() {
	
		this.itemNameInLangProperty = new SimpleStringProperty("");
		this.langNameProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
	}
	@JsonIgnore
	private StringProperty itemNameInLangProperty;
	
	@JsonIgnore
	private StringProperty langNameProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemNameInLang() {
		return itemNameInLang;
	}
	public void setItemNameInLang(String itemNameInLang) {
		this.itemNameInLang = itemNameInLang;
	}
	public String getLangName() {
		return langName;
	}
	public void setLangName(String langName) {
		this.langName = langName;
	}
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public StringProperty getItemNameInLangProperty() {
		itemNameInLangProperty.set(itemNameInLang);
		return itemNameInLangProperty;
	}
	public void setItemNameInLangProperty(StringProperty itemNameInLangProperty) {
		this.itemNameInLangProperty = itemNameInLangProperty;
	}
	public StringProperty getLangNameProperty() {
		langNameProperty.set(langName);
		return langNameProperty;
	}
	public void setLangNameProperty(StringProperty langNameProperty) {
		this.langNameProperty = langNameProperty;
	}
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ItemNameInLanguage [id=" + id + ", itemId=" + itemId + ", itemNameInLang=" + itemNameInLang
				+ ", langName=" + langName + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", itemName=" + itemName + "]";
	}
	
	
	
	
}
