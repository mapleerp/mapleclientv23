package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class TermsAndConditionsMst {

	String id;
	String invoiceFormat;
	String serialNumber;
	String conditions;
	private String  processInstanceId;
	private String taskId;
	
	CompanyMst companyMst;
	@JsonIgnore
	String conditionandcondition;

	
	@JsonIgnore
	private StringProperty invoiceFormatProperty;
	
	@JsonIgnore
	private StringProperty serialNumberProperty;
	
	@JsonIgnore
	private StringProperty conditionProperty;
	
	
	
	public TermsAndConditionsMst() {
		
		this.invoiceFormatProperty = new SimpleStringProperty("");
		this.serialNumberProperty = new SimpleStringProperty("");
		this.conditionProperty = new SimpleStringProperty("");

	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceFormat() {
		return invoiceFormat;
	}
	public void setInvoiceFormat(String invoiceFormat) {
		this.invoiceFormat = invoiceFormat;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	
	public StringProperty getInvoiceFormatProperty() {
		invoiceFormatProperty.set(invoiceFormat);
		return invoiceFormatProperty;
	}
	public void setInvoiceFormatProperty(StringProperty invoiceFormatProperty) {
		this.invoiceFormatProperty = invoiceFormatProperty;
	}
	public StringProperty getSerialNumberProperty() {
		serialNumberProperty.set(serialNumber);
		return serialNumberProperty;
	}
	public void setSerialNumberProperty(StringProperty serialNumberProperty) {
		this.serialNumberProperty = serialNumberProperty;
	}
	public StringProperty getConditionProperty() {
		conditionProperty.set(conditions);
		return conditionProperty;
	}
	public void setConditionProperty(StringProperty conditionProperty) {
		this.conditionProperty = conditionProperty;
	}
	public String getConditions() {
		return conditions;
	}
	public void setConditions(String conditions) {
		this.conditions = conditions;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	public String getConditionandcondition() {
		conditionandcondition = conditions;
		return conditionandcondition;
	}
	public void setConditionandcondition(String conditionandcondition) {
		conditionandcondition = conditions;
		this.conditionandcondition = conditionandcondition;
	}
	@Override
	public String toString() {
		return "TermsAndConditionsMst [id=" + id + ", invoiceFormat=" + invoiceFormat + ", serialNumber=" + serialNumber
				+ ", conditions=" + conditions + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", companyMst=" + companyMst + ", conditionandcondition=" + conditionandcondition
				+ ", invoiceFormatProperty=" + invoiceFormatProperty + ", serialNumberProperty=" + serialNumberProperty
				+ ", conditionProperty=" + conditionProperty + "]";
	}
	
	
	

}
