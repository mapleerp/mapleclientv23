package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MixCatItemLinkMst {
    String id;
	String itemName;
	String itemId;
	String resourceCatId;
	String mixName;
	private String  processInstanceId;
	private String taskId;
	StringProperty itemNameProperty;
	
	StringProperty resourceCatNameProperty;
	
	
	public MixCatItemLinkMst() {
		this.itemNameProperty=new SimpleStringProperty("");
		this.resourceCatNameProperty=new SimpleStringProperty("");
		
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getResourceCatId() {
		return resourceCatId;
	}
	public void setResourceCatId(String resourceCatId) {
		this.resourceCatId = resourceCatId;
	}
	public String getResourceCatName() {
		return mixName;
	}
	public void setResourceCatName(String resourceCatName) {
		this.mixName = resourceCatName;
	}
	
	@JsonIgnore
	public StringProperty getItemNameProperty() {
		
		itemNameProperty.setValue(itemId);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		
		this.itemNameProperty = itemNameProperty;
	}
	@JsonIgnore
	public StringProperty getResourceCatNameProperty() {
		resourceCatNameProperty.setValue(resourceCatId);
		return resourceCatNameProperty;
	}
	public void setResourceCatNameProperty(StringProperty resourceCatNameProperty) {
		this.resourceCatNameProperty = resourceCatNameProperty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MixCatItemLinkMst [id=" + id + ", itemName=" + itemName + ", itemId=" + itemId + ", resourceCatId="
				+ resourceCatId + ", mixName=" + mixName + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
}
