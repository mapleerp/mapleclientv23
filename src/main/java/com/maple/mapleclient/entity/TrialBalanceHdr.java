package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TrialBalanceHdr {
	
	String id;
	String reportId;
	String userId;
	Date fromDate;
	Date toDate;
	Date reportDate;
	
	public TrialBalanceHdr() {
		this.reportIdProperty = new SimpleStringProperty("");
	}
	
	@JsonIgnore
	StringProperty reportIdProperty;
	
	@JsonIgnore
	public StringProperty getReportIdProperty() {
		if(null != reportId) {
			reportIdProperty.set(reportId);
		}
		return reportIdProperty;
	}
	public void setReportIdProperty(StringProperty reportIdProperty) {
		this.reportIdProperty = reportIdProperty;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	@Override
	public String toString() {
		return "TrialBalanceHdr [id=" + id + ", reportId=" + reportId + ", userId=" + userId + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + ", reportDate=" + reportDate + ", reportIdProperty=" + reportIdProperty + "]";
	}
	
	
	

}
