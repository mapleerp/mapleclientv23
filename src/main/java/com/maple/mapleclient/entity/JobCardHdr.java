package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class JobCardHdr implements Serializable{
	private static final long serialVersionUID = 1L;

	private String id;
	private String voucherNumber;
	private String customerId;
	private String status;
	private String branchCode;

	private String salesTransHdrId;
	CompanyMst companyMst;
	ServiceInDtl serviceInDtl;
	private String  processInstanceId;
	private String taskId;
	
	
	
	public JobCardHdr() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.serviceNoProperty = new SimpleStringProperty("");
		this.statusProperty = new SimpleStringProperty("");

		this.voucherNoProperty = new SimpleStringProperty("");
		this.voucherDate = new SimpleObjectProperty<LocalDate>();

	}
	
	
	
	
	 @JsonIgnore
		private SimpleObjectProperty<LocalDate> voucherDate;
	 
		@JsonProperty("voucherDate")
		public java.sql.Date getVoucherDate( ) {
			if(null==this.voucherDate.get() ) {
				return null;
			}else {
				return Date.valueOf(this.voucherDate.get() );
			}	
		}

	   public void setVoucherDate (java.sql.Date voucherDateProperty) {
							if(null!=voucherDateProperty)
			this.voucherDate.set(voucherDateProperty.toLocalDate());
		}
	   public ObjectProperty<LocalDate> getVoucherDateProperty( ) {
			return voucherDate;
		}

		public void setVoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
			this.voucherDate = (SimpleObjectProperty<LocalDate>) voucherDate;
		}
		
		
	@JsonIgnore
	String customerName;
	
	@JsonIgnore
	String itemName;
	
	@JsonIgnore
	String serviceNo;
	
	@JsonIgnore 
	StringProperty itemNameProperty;
	
	@JsonIgnore 
	StringProperty statusProperty;
	
	@JsonIgnore 
	StringProperty voucherNoProperty;
	
	@JsonIgnore 
	StringProperty customerNameProperty;
	
	@JsonIgnore 
	StringProperty serviceNoProperty;
	
	
	public String getSalesTransHdrId() {
		return salesTransHdrId;
	}
	public void setSalesTransHdrId(String salesTransHdrId) {
		this.salesTransHdrId = salesTransHdrId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public ServiceInDtl getServiceInDtl() {
		return serviceInDtl;
	}
	public void setServiceInDtl(ServiceInDtl serviceInDtl) {
		this.serviceInDtl = serviceInDtl;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getServiceNo() {
		return serviceNo;
	}
	public void setServiceNo(String serviceNo) {
		this.serviceNo = serviceNo;
	}
	public StringProperty getItemNameProperty() {
		
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}
	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}
	public StringProperty getServiceNoProperty() {
		serviceNoProperty.set(serviceNo);
		return serviceNoProperty;
	}
	public void setServiceNoProperty(StringProperty serviceNoProperty) {
		this.serviceNoProperty = serviceNoProperty;
	}
	
	
	public StringProperty getStatusProperty() {
		statusProperty.set(status);
		return statusProperty;
	}
	public void setStatusProperty(StringProperty statusProperty) {
		this.statusProperty = statusProperty;
	}
	public StringProperty getVoucherNoProperty() {
		voucherNoProperty.set(voucherNumber);
		return voucherNoProperty;
	}
	public void setVoucherNoProperty(StringProperty voucherNoProperty) {
		this.voucherNoProperty = voucherNoProperty;
	}

	
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "JobCardHdr [id=" + id + ", voucherNumber=" + voucherNumber + ", customerId=" + customerId + ", status="
				+ status + ", branchCode=" + branchCode + ", salesTransHdrId=" + salesTransHdrId + ", companyMst="
				+ companyMst + ", serviceInDtl=" + serviceInDtl + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", customerName=" + customerName + ", itemName=" + itemName + ", serviceNo="
				+ serviceNo + "]";
	}

	
	
	
	
}
