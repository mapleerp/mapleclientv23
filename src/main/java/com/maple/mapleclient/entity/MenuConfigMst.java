package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class MenuConfigMst {
	
	private String id;

	private String menuName;
	private String menuFxml;
	private String menuDescription;

	private CompanyMst companyMst;
	private String customiseSalesMode;
	
	
	private String  processInstanceId;
	private String taskId;
	public String getCustomiseSalesMode() {
		return customiseSalesMode;
		
	}

	public void setCustomiseSalesMode(String customiseSalesMode) {
		this.customiseSalesMode = customiseSalesMode;
	}

	public MenuConfigMst() {
	
		this.menuNameProperty = new SimpleStringProperty();
		this.menuFxmlProperty = new SimpleStringProperty();
		this.menuDescriptionProperty = new SimpleStringProperty();
		this.customiseSalesModeProperty = new SimpleStringProperty();

	}
	@JsonIgnore
	private StringProperty customiseSalesModeProperty;

	@JsonIgnore
	private StringProperty menuNameProperty;
	
	@JsonIgnore
	private StringProperty menuFxmlProperty;
	
	@JsonIgnore
	private StringProperty menuDescriptionProperty;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuFxml() {
		return menuFxml;
	}

	public void setMenuFxml(String menuFxml) {
		this.menuFxml = menuFxml;
	}

	public String getMenuDescription() {
		return menuDescription;
	}

	public void setMenuDescription(String menuDescription) {
		this.menuDescription = menuDescription;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public StringProperty getMenuNameProperty() {
		menuNameProperty.set(menuName);
		return menuNameProperty;
	}

	public void setMenuNameProperty(StringProperty menuNameProperty) {
		this.menuNameProperty = menuNameProperty;
	}

	public StringProperty getMenuFxmlProperty() {
		menuFxmlProperty.set(menuFxml);
		return menuFxmlProperty;
	}

	public StringProperty getCustomiseSalesModeProperty() {
		customiseSalesModeProperty.set(customiseSalesMode);
		return customiseSalesModeProperty;
	}

	public void setCustomiseSalesModeProperty(String customiseSalesMode) {
		this.customiseSalesMode = customiseSalesMode;
	}

	public void setMenuFxmlProperty(StringProperty menuFxmlProperty) {
		this.menuFxmlProperty = menuFxmlProperty;
	}

	public StringProperty getMenuDescriptionProperty() {
		menuDescriptionProperty.set(menuDescription);
		return menuDescriptionProperty;
	}

	public void setMenuDescriptionProperty(StringProperty menuDescriptionProperty) {
		this.menuDescriptionProperty = menuDescriptionProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "MenuConfigMst [id=" + id + ", menuName=" + menuName + ", menuFxml=" + menuFxml + ", menuDescription="
				+ menuDescription + ", companyMst=" + companyMst + ", customiseSalesMode=" + customiseSalesMode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
}
