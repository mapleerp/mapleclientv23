package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AddKotWaiter {

	@JsonProperty
	String id;
	String status;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	@JsonProperty
	String waiterName;
	
 	StringProperty waiterNameProperty;
 	@JsonIgnore
 	private StringProperty statusProperty;
	
	public AddKotWaiter () {
 		this.waiterNameProperty = new SimpleStringProperty("");
 		this.statusProperty = new SimpleStringProperty("");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWaiterName() {
		return waiterName;
	}

	public void setWaiterName(String waiterName) {
		this.waiterName = waiterName;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonIgnore
	public StringProperty getWaiterNameProperty() {
		waiterNameProperty.set(waiterName);
		return waiterNameProperty;
	}
	

	public void setWaiterNameProperty(String waiterName) {
		this.waiterName = waiterName;
	}
	@JsonIgnore
	public StringProperty getstatusProperty() {
		statusProperty.set(status);
		return statusProperty;
	}
	

	public void setstatusProperty(String status) {
		this.waiterName = status;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "AddKotWaiter [id=" + id + ", status=" + status + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", waiterName=" + waiterName + "]";
	}

	
}