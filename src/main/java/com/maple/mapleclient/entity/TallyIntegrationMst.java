package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class TallyIntegrationMst {

	String id;
	
	String tallyUrl;
	
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	StringProperty tallyUrlProperty;
	
	
	
	

	public TallyIntegrationMst() {
		this.tallyUrlProperty = new SimpleStringProperty("");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTallyUrl() {
		return tallyUrl;
	}

	public void setTallyUrl(String tallyUrl) {
		this.tallyUrl = tallyUrl;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	
	public StringProperty getTallyUrlProperty() {
		tallyUrlProperty.set(tallyUrl);
		return tallyUrlProperty;
	}

	public void setTallyUrlProperty(StringProperty tallyUrlProperty) {
		this.tallyUrlProperty = tallyUrlProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "TallyIntegrationMst [id=" + id + ", tallyUrl=" + tallyUrl + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
}
