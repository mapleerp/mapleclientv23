package com.maple.mapleclient.entity;

import java.util.Date;
 
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class ItemBatchDtl {
	
	 
	
	private String id;
	
	private String itemId;
	private String batch ;
	private String barcode;
	private Double qtyIn;
	private Double qtyOut;
	private Double mrp;
	private String voucherNumber;
	private Date voucherDate;
	private String sourceVoucherNumber;
	private java.sql.Date sourceVoucherDate;
	private Date expDate;
	
	private String store;
	private String sourceParentId;
	private String sourceDtlId;
	private String  processInstanceId;
	private String taskId;
	private String particulars;

 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
 
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	 
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQtyIn() {
		return qtyIn;
	}
	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}
	public Double getQtyOut() {
		return qtyOut;
	}
	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getSourceVoucherNumber() {
		return sourceVoucherNumber;
	}
	public void setSourceVoucherNumber(String sourceVoucherNumber) {
		this.sourceVoucherNumber = sourceVoucherNumber;
	}
	public java.sql.Date getSourceVoucherDate() {
		return sourceVoucherDate;
	}
	public void setSourceVoucherDate(java.sql.Date sourceVoucherDate) {
		this.sourceVoucherDate = sourceVoucherDate;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	
	public String getSourceParentId() {
		return sourceParentId;
	}
	public void setSourceParentId(String sourceParentId) {
		this.sourceParentId = sourceParentId;
	}
	public String getSourceDtlId() {
		return sourceDtlId;
	}
	public void setSourceDtlId(String sourceDtlId) {
		this.sourceDtlId = sourceDtlId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	@Override
	public String toString() {
		return "ItemBatchDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", barcode=" + barcode
				+ ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", mrp=" + mrp + ", voucherNumber=" + voucherNumber
				+ ", voucherDate=" + voucherDate + ", sourceVoucherNumber=" + sourceVoucherNumber
				+ ", sourceVoucherDate=" + sourceVoucherDate + ", expDate=" + expDate + ", store=" + store
				+ ", sourceParentId=" + sourceParentId + ", sourceDtlId=" + sourceDtlId + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", particulars=" + particulars + "]";
	}
	
	
	 

}
