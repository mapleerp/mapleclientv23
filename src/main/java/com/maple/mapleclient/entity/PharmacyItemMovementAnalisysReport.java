package com.maple.mapleclient.entity;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyItemMovementAnalisysReport {

	String groupName;
	String itemName;
	String itemCode;
	String batchCode;
	String description;
	String unitName;
	String partyName;
	String voucherNumber;
	Double openingBalance;
	Double inwardQty;
	Double outwardQty;
	Double balance;
	Double cost;
	Double value;
	String trEntryDate;
	private String  processInstanceId;
	private String taskId;
	
	
	
public PharmacyItemMovementAnalisysReport() {
		
		this.trEntryDateProperty = new SimpleStringProperty();
		this.groupNameProperty = new SimpleStringProperty();
		this.itemNameProperty = new SimpleStringProperty();
		this.unitNameProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.partyNameProperty = new SimpleStringProperty();
		this.descriptionProperty = new SimpleStringProperty();
		this.itemCodeProperty = new SimpleStringProperty();
		this.batchCodeProperty = new SimpleStringProperty();
		
		this.balanceProperty = new SimpleDoubleProperty();
		this.costProperty = new SimpleDoubleProperty();
		this.valueProperty = new SimpleDoubleProperty();
		this.openingBalanceProperty = new SimpleDoubleProperty();
		this.inwardQtyProperty = new SimpleDoubleProperty();
		this.outwardQtyProperty = new SimpleDoubleProperty();
		
//		this.expiryDate = new SimpleObjectProperty<LocalDate>(null);
//		this.updatedDate = new SimpleObjectProperty<LocalDate>(null);
}
	
	
	@JsonIgnore
	private StringProperty trEntryDateProperty;

	
	@JsonIgnore
	 private StringProperty groupNameProperty;
	
	@JsonIgnore
	 private StringProperty itemNameProperty;
	
	@JsonIgnore
	 private StringProperty  unitNameProperty;
	
	@JsonIgnore
	 private StringProperty  voucherNumberProperty;
	
	
	@JsonIgnore
	 private DoubleProperty balanceProperty;
	
	@JsonIgnore
	 private DoubleProperty  costProperty;
	
	@JsonIgnore
	 private DoubleProperty  valueProperty;
	
	@JsonIgnore
	 private DoubleProperty openingBalanceProperty;
	
	@JsonIgnore
	 private DoubleProperty  inwardQtyProperty;
	
	@JsonIgnore
	 private DoubleProperty  outwardQtyProperty;

	@JsonIgnore
	 private StringProperty  partyNameProperty;
	
	
	@JsonIgnore
	 private StringProperty descriptionProperty;
	
	@JsonIgnore
	 private StringProperty  itemCodeProperty;
	
	@JsonIgnore
	 private StringProperty  batchCodeProperty;
	
	
	public String getTrEntryDate() {
		return trEntryDate;
	}
	public void setTrEntryDate(String trEntryDate) {
		this.trEntryDate = trEntryDate;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}
	public Double getInwardQty() {
		return inwardQty;
	}
	public void setInwardQty(Double inwardQty) {
		this.inwardQty = inwardQty;
	}
	public Double getOutwardQty() {
		return outwardQty;
	}
	public void setOutwardQty(Double outwardQty) {
		this.outwardQty = outwardQty;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}

	@JsonIgnore
	public StringProperty getTrEntryDateProperty() {
		if(null != trEntryDate) {
		trEntryDateProperty.set(trEntryDate);
		}
		return trEntryDateProperty;
	}
	public void setTrEntryDateProperty(StringProperty trEntryDateProperty) {
		this.trEntryDateProperty = trEntryDateProperty;
	}


	@JsonIgnore
	public StringProperty getGroupNameProperty() {
		groupNameProperty.set(groupName);
		return groupNameProperty;
	}
	public void setGroupNamePrvalueoperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}

	@JsonIgnore
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	@JsonIgnore
	public StringProperty getUnitNameProperty() {
		
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}

	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}

	@JsonIgnore
	public DoubleProperty getBalanceProperty() {
		
		balanceProperty.set(balance);
		return balanceProperty;
	}
	public void setBalanceProperty(DoubleProperty balanceProperty) {
		this.balanceProperty = balanceProperty;
	}

	@JsonIgnore
	public DoubleProperty getCostProperty() {
		costProperty.set(cost);
		return costProperty;
	}
	public void setCostProperty(DoubleProperty costProperty) {
		this.costProperty = costProperty;
	}

	@JsonIgnore
	public DoubleProperty getValueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}
	public void setValueProperty(DoubleProperty valueProperty) {
		this.valueProperty = valueProperty;
	}

	@JsonIgnore
	public DoubleProperty getOpeningBalanceProperty() {
		openingBalanceProperty.set(openingBalance);
		return openingBalanceProperty;
	}
	public void setOpeningBalanceProperty(DoubleProperty openingBalanceProperty) {
		this.openingBalanceProperty = openingBalanceProperty;
	}

	@JsonIgnore
	public DoubleProperty getInwardQtyProperty() {
		inwardQtyProperty.set(inwardQty);
		return inwardQtyProperty;
	}
	public void setInwardQtyProperty(DoubleProperty inwardQtyProperty) {
		this.inwardQtyProperty = inwardQtyProperty;
	}

	@JsonIgnore
	public DoubleProperty getOutwardQtyProperty() {
		outwardQtyProperty.set(outwardQty);
		return outwardQtyProperty;
	}
	public void setOutwardQtyProperty(DoubleProperty outwardQtyProperty) {
		this.outwardQtyProperty = outwardQtyProperty;
	}

	@JsonIgnore
	public StringProperty getPartyNameProperty() {
		
		partyNameProperty.set(partyName);
		return partyNameProperty;
	}
	public void setPartyNameProperty(StringProperty partyNameProperty) {
		this.partyNameProperty = partyNameProperty;
	}

	@JsonIgnore
	public StringProperty getDescriptionProperty() {
		descriptionProperty.set(description);
		return descriptionProperty;
	}
	public void setDescriptionProperty(StringProperty descriptionProperty) {
		this.descriptionProperty = descriptionProperty;
	}

	@JsonIgnore
	public StringProperty getItemCodeProperty() {
		itemCodeProperty.set(itemCode);
		return itemCodeProperty;
	}
	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	@JsonIgnore
	public StringProperty getBatchCodeProperty() {
		batchCodeProperty.set(batchCode);
		return batchCodeProperty;
	}
	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PharmacyItemMovementAnalisysReport [groupName=" + groupName + ", itemName=" + itemName + ", itemCode="
				+ itemCode + ", batchCode=" + batchCode + ", description=" + description + ", unitName=" + unitName
				+ ", partyName=" + partyName + ", voucherNumber=" + voucherNumber + ", openingBalance=" + openingBalance
				+ ", inwardQty=" + inwardQty + ", outwardQty=" + outwardQty + ", balance=" + balance + ", cost=" + cost
				+ ", value=" + value + ", trEntryDate=" + trEntryDate + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}

	
		
	
	
		
		
}
