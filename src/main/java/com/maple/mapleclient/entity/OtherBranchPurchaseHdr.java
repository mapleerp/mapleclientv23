package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.WritableDoubleValue;

public class OtherBranchPurchaseHdr implements Serializable{

	public OtherBranchPurchaseHdr() {

//	this.purchaseDtls = purchaseDtls;
//	this.additionalExpenses = additionalExpenses;
		this.narration = new SimpleStringProperty("");
		this.purchaseType = new SimpleStringProperty("");
		this.pONum = new SimpleStringProperty("");
		this.supplierInvNo = new SimpleStringProperty("");
		this.voucherNumberProperty = new SimpleStringProperty("");
		this.invoiceTotal = new SimpleDoubleProperty();
		this.currency = new SimpleStringProperty("");
		this.currencyInAmt = new SimpleDoubleProperty();
		this.enableBatchStatus = new SimpleStringProperty("");
		this.finalSavedStatus = new SimpleStringProperty("");
		this.machineId = new SimpleStringProperty("");
		this.branchId = new SimpleStringProperty("");
		this.companyId = new SimpleIntegerProperty();
		this.userId = new SimpleStringProperty("");
		this.supplierId = new SimpleStringProperty("");
		this.deletedStatus = new SimpleStringProperty("");
		this.tansactionEntryDate = new SimpleObjectProperty();
		this.tansactionEntryDate.set(LocalDate.now());
		this.poDate = new SimpleObjectProperty();
		this.poDate.set(LocalDate.now());
		this.supplierAddressProperty = new SimpleStringProperty("");
		this.supplierNameProperty = new SimpleStringProperty("");
		this.voucherDate = new SimpleObjectProperty<LocalDate>(null);
		this.supplierInvDate = new SimpleObjectProperty<LocalDate>(null);

	}

	private String id;
	private String supplierName;
	private String supplierAddress;
	private String voucherType;
	private String voucherNumber;
	String branchCode;
	private Double fcInvoiceTotal;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setPurchaseType(StringProperty purchaseType) {
		this.purchaseType = purchaseType;
	}

	public Double getFcInvoiceTotal() {
		return fcInvoiceTotal;
	}

	public void setFcInvoiceTotal(Double fcInvoiceTotal) {
		this.fcInvoiceTotal = fcInvoiceTotal;
	}

	@JsonIgnore
	private StringProperty supplierId;


	
	@JsonIgnore
	private StringProperty finalSavedStatus;

	@JsonIgnore
	private StringProperty machineId;

	@JsonIgnore
	private StringProperty branchId;

	@JsonIgnore
	private IntegerProperty companyId;

	@JsonIgnore
	private StringProperty deletedStatus;

	

	@JsonIgnore
	private StringProperty narration;

	@JsonIgnore
	private StringProperty purchaseType;

	@JsonIgnore
	private StringProperty pONum;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> poDate;

	@JsonIgnore
	private StringProperty supplierInvNo;

	@JsonIgnore
	private StringProperty voucherNumberProperty;

	@JsonIgnore
	private DoubleProperty invoiceTotal;

	@JsonIgnore
	private StringProperty currency;

	@JsonIgnore
	private ObjectProperty<LocalDate> supplierInvDate;

	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;

	@JsonIgnore
	private ObjectProperty<LocalDate> tansactionEntryDate;

	@JsonIgnore
	private DoubleProperty currencyInAmt;

	@JsonIgnore
	private StringProperty enableBatchStatus;

	@JsonIgnore
	private StringProperty userId;

	@JsonIgnore
	private StringProperty supplierNameProperty;
	
	@JsonIgnore
	private StringProperty supplierAddressProperty;
	 
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	@JsonIgnore
	public StringProperty getUserIdProperty() {
		return userId;
	}
	@JsonIgnore
	public void setUserIdProperty(StringProperty userId) {
		this.userId = userId;
	}
	




	
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	
	@JsonIgnore
	public StringProperty getNarrationProperty() {
		return narration;
	}
	@JsonIgnore
	public void setNarrationProperty(StringProperty narration) {
		this.narration = narration;
	}
	@JsonProperty("narration")
	public String getNarration() {
		return narration.get();
	}
	@JsonProperty("narration")
	public void setNarration(String narration) {
		this.narration.set(narration);
	}
	
	
	@JsonIgnore
	public StringProperty getPurchaseTypeProperty() {
		return purchaseType;
	}
	@JsonIgnore
	public void setPurchaseTypeProperty(StringProperty purchaseType) {
		this.purchaseType = purchaseType;
	}
	@JsonProperty("purchaseType")
	public String getPurchaseType() {
		return purchaseType.get();
	}
	@JsonProperty("purchaseType")
	public void setPurchaseType(String purchaseType) {
		this.purchaseType.set(purchaseType);
	}
	@JsonIgnore
	public StringProperty getpONumProperty() {
		return pONum;
	}
	@JsonIgnore
	public void setpONumProperty(StringProperty pONum) {
		this.pONum = pONum;
	}
	@JsonProperty("pONum")
	public String getpONum() {
		return pONum.get();
	}
	@JsonProperty("pONum")
	public void setpONum(String pONum) {
		this.pONum.set(pONum);
	}
	@JsonIgnore
	public StringProperty getSupplierInvNoProperty() {
		return supplierInvNo;
	}
	
	@JsonIgnore
	public void setSupplierInvNoProperty(StringProperty supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}
	@JsonProperty("supplierInvNo")
	public String getSupplierInvNo() {
		return supplierInvNo.get();
	}
	
	@JsonProperty("supplierInvNo")
	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo.set(supplierInvNo);
	}
	
	
	@JsonIgnore
	public DoubleProperty getInvoiceTotalProperty() {
	
		
		return invoiceTotal;
	}
	@JsonIgnore
	public void setInvoiceTotalProperty(DoubleProperty invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	@JsonProperty("invoiceTotal")
	public Double getInvoiceTotal() {
		return invoiceTotal.get();
	}
	@JsonProperty("invoiceTotal")
	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal.set(invoiceTotal);
	}
	@JsonIgnore
	public StringProperty getCurrencyProperty() {
		return currency;
	}
	@JsonIgnore
	public void setCurrencyProperty(StringProperty currency) {
		this.currency = currency;
	}
	@JsonProperty("currency")
	public String getCurrency() {
		return currency.get();
	}
	@JsonProperty("currency")
	public void setCurrency(String currency) {
		this.currency.set(currency);
	}

	// -------------------object-------change if any problm occcureds------------

	public DoubleProperty getCurrencyInAmtProperty() {
		return currencyInAmt;
	}

	public void setCurrencyInAmtProperty(DoubleProperty currencyInAmt) {
		this.currencyInAmt = currencyInAmt;
	}
	@JsonProperty("currencyInAmt")
	public Double getCurrencyInAmt() {
		return currencyInAmt.get();
	}
	@JsonProperty("currencyInAmt")
	public void setCurrencyInAmt(double currencyInAmt) {
		this.currencyInAmt.set(currencyInAmt);
	}
	@JsonIgnore
	public StringProperty getFinalSavedStatusProperty() {
		return finalSavedStatus;
	}
	@JsonIgnore
	public void setFinalSavedStatusProperty(StringProperty finalSavedStatus) {
		this.finalSavedStatus = finalSavedStatus;
	}
	@JsonProperty("finalSavedStatus")
	public String getFinalSavedStatus() {
		return finalSavedStatus.get();
	}
	@JsonProperty("finalSavedStatus")
	public void setFinalSavedStatus(String finalSavedStatus) {
		this.finalSavedStatus.set(finalSavedStatus);
	}
	@JsonIgnore
	public StringProperty getMachineIdProperty() {
		return machineId;
	}
	@JsonIgnore
	public void setMachineIdProperty(StringProperty machineId) {
		this.machineId = machineId;
	}
	@JsonProperty("machineId")
	public String getMachineId() {
		return machineId.get();
	}
	@JsonProperty("machineId")
	public void setMachineId(String machineId) {
		this.machineId.set(machineId);
	}

	public StringProperty getBranchIdProperty() {
		return branchId;
	}

	public void setBranchIdProperty(StringProperty branchId) {
		this.branchId = branchId;
	}
	@JsonProperty("branchId")
	public String getBranchId() {
		return branchId.get();
	}
	@JsonProperty("branchId")
	public void setBranchId(String branchId) {
		this.branchId.set(branchId);
	}

	public IntegerProperty getCompanyIdProperty() {
		return companyId;
	}

	public void setCompanyIdProperty(IntegerProperty companyId) {
		this.companyId = companyId;
	}
	@JsonProperty("companyId")
	public Integer getCompanyId() {
		return companyId.get();
	}
	@JsonProperty("companyId")
	public void setCompanyId(Integer companyId) {
		this.companyId.set(companyId);
	}

	public StringProperty getDeletedStatusProperty() {
		return deletedStatus;
	}

	public void setDeletedStatusProperty(StringProperty deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	@JsonProperty("deletedStatus")
	public String getDeletedStatus() {
		return deletedStatus.get();
	}
	@JsonProperty("deletedStatus")
	public void setDeletedStatus(String deletedStatus) {
		this.deletedStatus.set(deletedStatus);
	}
	@JsonIgnore
	public StringProperty getEnableBatchStatusProperty() {
		return enableBatchStatus;
	}
	@JsonIgnore
	public void setEnableBatchStatusProperty(StringProperty enableBatchStatus) {
		this.enableBatchStatus = enableBatchStatus;
	}
	
	public String getEnableBatchStatus() {
		return enableBatchStatus.get();
	}
	
	public void setEnableBatchStatus(String enableBatchStatus) {
		this.enableBatchStatus.set(enableBatchStatus);
	}

	@JsonIgnore

	public ObjectProperty<LocalDate> getPoDateProperty() {
		return poDate;
	}
	@JsonIgnore
	public void setPoDateProperty(ObjectProperty<LocalDate> poDate) {
		this.supplierInvDate = supplierInvDate;
	}

	@JsonProperty("poDate")
	public java.sql.Date getPoDate() {

		return Date.valueOf(this.poDate.get());
	}

	public void setPoDate(java.sql.Date poDate) {

	if(null!=poDate)
		this.poDate.set(poDate.toLocalDate());
		
	}
	
	

   /////////////
	@JsonProperty("supplierInvDate")
	public java.sql.Date getInvoiceDate( ) {
		if(null==this.supplierInvDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.supplierInvDate.get() );
		}
	 
		
	}
	
	
  public void setInvoiceDate (java.sql.Date supplierInvDateProperty) {
						if(null!=supplierInvDateProperty)
		this.supplierInvDate.set(supplierInvDateProperty.toLocalDate());
	}
  //////
   
   public ObjectProperty<LocalDate> getVoucherDateProperty( ) {
		return voucherDate;
	}

	public void setVoucherDateProperty(ObjectProperty<LocalDate> voucherDateProperty) {
		this.voucherDate = voucherDate;
	}
   
	
	public ObjectProperty<LocalDate> getInvoiceDateProperty( ) {
		return supplierInvDate;
	}

	public void setInvoiceDateProperty(ObjectProperty<LocalDate> invoiceDatesProperty) {
		this.supplierInvDate = supplierInvDate;
	}
   /////

	
	
	


	
	@JsonProperty("voucherDate")
	public java.sql.Date getourVoucherDate( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}
	
	
   public void setourVoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}
   

	@JsonIgnore

	public ObjectProperty<LocalDate> getTansactionEntryDateProperty() {
		return tansactionEntryDate;
	}
	@JsonIgnore
	public void setTansactionEntryDateProperty(ObjectProperty<LocalDate> tansactionEntryDate) {
		this.tansactionEntryDate = tansactionEntryDate;
	}

	@JsonProperty("tansactionEntryDate")
	public java.sql.Date gettansactionEntryDate() {

		return Date.valueOf(this.tansactionEntryDate.get());
	}

	public void settansactionEntryDate(java.sql.Date tansactionEntryDate) {
		if(null != tansactionEntryDate)

		this.tansactionEntryDate.set(tansactionEntryDate.toLocalDate());
	}
	
	
	@JsonIgnore
	public StringProperty getSupplierIdProperty() {
		return supplierId;
	}
	public void setSupplierIdProperty(StringProperty supplierId) {
		this.supplierId = supplierId;
	}
	
	@JsonProperty("supplierId")
	public String getSupplierId() {
		return supplierId.get();
	}

	public void setSupplierId(String supplierId) {
		this.supplierId.set(supplierId);
	}
	
	public StringProperty getSupplierNameProperty() {

		supplierNameProperty.set(supplierName);
		return supplierNameProperty;
	}

	public void setSupplierNameProperty(StringProperty supplierNameProperty) {
		supplierNameProperty = supplierNameProperty;
	}
	
	public StringProperty getSupplierAddressProperty() {

		supplierAddressProperty.set(supplierAddress);
		return supplierAddressProperty;
	}

	public void setSupplierAddressProperty(StringProperty supplierAddressProperty) {
		supplierAddressProperty = supplierAddressProperty;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "OtherBranchPurchaseHdr [id=" + id + ", supplierName=" + supplierName + ", supplierAddress="
				+ supplierAddress + ", voucherType=" + voucherType + ", voucherNumber=" + voucherNumber
				+ ", branchCode=" + branchCode + ", fcInvoiceTotal=" + fcInvoiceTotal + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}







	
	

}
	
	
	

	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	

	

	

	

	

	

	
	
	


