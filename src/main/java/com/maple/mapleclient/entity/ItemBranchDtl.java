package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemBranchDtl {
	
	String id;
	String branch;
	String itemId;
	String itemName;
	
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty branchProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;

	public ItemBranchDtl() {
	
		this.branchProperty =new SimpleStringProperty();
		this.itemNameProperty = new SimpleStringProperty();
	}
	
	@JsonProperty
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	ItemBranchHdr itemBranchHdr;
	
	public ItemBranchHdr getItemBranchHdr() {
		return itemBranchHdr;
	}

	public void setItemBranchHdr(ItemBranchHdr itemBranchHdr) {
		this.itemBranchHdr = itemBranchHdr;
	}
	@JsonIgnore
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	@JsonIgnore
	public StringProperty getbranchProperty() {
		branchProperty.set(branch);
		return branchProperty;
	}
	

	public void setbranchProperty(String branch) {
		this.branch = branch;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ItemBranchDtl [id=" + id + ", branch=" + branch + ", itemId=" + itemId + ", itemName=" + itemName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", itemBranchHdr=" + itemBranchHdr
				+ "]";
	}
	

	

}
