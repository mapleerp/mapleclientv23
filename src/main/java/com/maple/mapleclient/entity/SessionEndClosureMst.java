package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
public class SessionEndClosureMst implements Serializable {

	private static final long serialVersionUID = 1L;
	

	String id;
	String userId;
	String branchCode;
	Double cashSale;
	Double cardSale;
	Double cardSale2;
	Double physicalCash;
	Date sessionDate;
	private LocalDateTime updatedTime;
	Integer slNo;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	
	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Double getCashSale() {
		return cashSale;
	}
	public void setCashSale(Double cashSale) {
		this.cashSale = cashSale;
	}
	public Double getCardSale() {
		return cardSale;
	}
	public void setCardSale(Double cardSale) {
		this.cardSale = cardSale;
	}
	public Double getCardSale2() {
		return cardSale2;
	}
	public void setCardSale2(Double cardSale2) {
		this.cardSale2 = cardSale2;
	}
	public Double getPhysicalCash() {
		return physicalCash;
	}
	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}
	public Date getSessionDate() {
		return sessionDate;
	}
	public void setSessionDate(Date sessionDate) {
		this.sessionDate = sessionDate;
	}
	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SessionEndClosureMst [id=" + id + ", userId=" + userId + ", branchCode=" + branchCode + ", cashSale="
				+ cashSale + ", cardSale=" + cardSale + ", cardSale2=" + cardSale2 + ", physicalCash=" + physicalCash
				+ ", sessionDate=" + sessionDate + ", updatedTime=" + updatedTime + ", slNo=" + slNo + ", companyMst="
				+ companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
	
}
