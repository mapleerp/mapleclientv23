package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SchemeInstance implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	 String id;
	 String schemeName;
	 String selectionId;
	 String eligibilityId;
	 String  offerId;
	 String isActive;
	 String schemeWholesaleRetail;
	 String selection;
	 String eligibility;
	 String offer;
	 String branchCode;
	 private String  processInstanceId;
		private String taskId;
	 @JsonIgnore
	 private StringProperty schemeNameProperty;
	 
	 @JsonIgnore
	 private StringProperty selectionProperty;
	 
	 @JsonIgnore
	 private StringProperty eligiblityProperty;
	 
	 @JsonIgnore
	 private StringProperty offerProperty;
	 
	 
	 @JsonIgnore
	 private StringProperty activeProperty;
	 
	 @JsonIgnore
	 private StringProperty schemeWholesaleRetailProperty;

		public SchemeInstance() {
		this.schemeNameProperty = new SimpleStringProperty();
		this.selectionProperty = new SimpleStringProperty();
		this.eligiblityProperty =  new SimpleStringProperty();
		this.offerProperty =  new SimpleStringProperty();
		this.activeProperty =  new SimpleStringProperty();
		this.schemeWholesaleRetailProperty =  new SimpleStringProperty();
	}

		
		
		
		

	public String getBranchCode() {
			return branchCode;
		}






		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}






	public String getSchemeName() {
			return schemeName;
		}

		public void setSchemeName(String schemeName) {
			this.schemeName = schemeName;
		}

		public String getSelection() {
			return selection;
		}

		public void setSelection(String selection) {
			this.selection = selection;
		}

		public String getEligibility() {
			return eligibility;
		}
		public void setEligibility(String eligibility) {
			this.eligibility = eligibility;
		}


		public String getOffer() {
			return offer;
		}

		public void setOffer(String offer) {
			this.offer = offer;
		}

		public StringProperty getSelectionProperty() {
			return selectionProperty;
		}
		public void setSelectionProperty(StringProperty selectionProperty) {
			this.selectionProperty = selectionProperty;
		}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSelectionId() {
		return selectionId;
	}
	public void setSelectionId(String selectionId) {
		this.selectionId = selectionId;
	}
	public String getEligibilityId() {
		return eligibilityId;
	}

	public void setEligibilityId(String eligibilityId) {
		this.eligibilityId = eligibilityId;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getSchemeWholesaleRetail() {
		return schemeWholesaleRetail;
	}

	public void setSchemeWholesaleRetail(String schemeWholesaleRetail) {
		this.schemeWholesaleRetail = schemeWholesaleRetail;
	}

	
@JsonIgnore
	
	public StringProperty getschemeNameProperty() {
	schemeNameProperty.set(schemeName);
		return schemeNameProperty;
	}
	public void setschemeNameProperty(String schemeName) {
		this.schemeName = schemeName;
	}
	
@JsonIgnore
	
	public StringProperty getselectionProperty() {
	selectionProperty.set(selection);
		return selectionProperty;
	}
	public void setselectionProperty(String selection) {
		this.selection = selection;
	}
@JsonIgnore
	
	public StringProperty geteligiblityProperty() {
	eligiblityProperty.set(eligibility);
		return eligiblityProperty;
	}
	public void seteligiblityProperty(String eligibility) {
		this.eligibility = eligibility;
	}
	
@JsonIgnore
	
	public StringProperty getofferProperty() {
	offerProperty.set(offer);
		return offerProperty;
	}
	public void setofferProperty(String offer) {
		this.offer = offer;
	}

@JsonIgnore
	
	public StringProperty getactiveProperty() {
	activeProperty.set(isActive);
		return activeProperty;
	}
	public void setactiveProperty(String isActive) {
		this.isActive = isActive;
	}
@JsonIgnore
	
	public StringProperty getschemeWholesaleRetailProperty() {
	schemeWholesaleRetailProperty.set(schemeWholesaleRetail);
		return schemeWholesaleRetailProperty;
	}
	public void setschemeWholesaleRetailProperty(String schemeWholesaleRetail) {
		this.schemeWholesaleRetail = schemeWholesaleRetail;
	}









	public String getProcessInstanceId() {
		return processInstanceId;
	}






	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}






	public String getTaskId() {
		return taskId;
	}






	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}






	@Override
	public String toString() {
		return "SchemeInstance [id=" + id + ", schemeName=" + schemeName + ", selectionId=" + selectionId
				+ ", eligibilityId=" + eligibilityId + ", offerId=" + offerId + ", isActive=" + isActive
				+ ", schemeWholesaleRetail=" + schemeWholesaleRetail + ", selection=" + selection + ", eligibility="
				+ eligibility + ", offer=" + offer + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
}
