package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockTransferOutHdr {
	String id;
	String intentNumber;
	String voucherNumber;
	Date voucherDate;
	String toBranch;
	String voucherType;
	String fromBranch;
	Integer slNo;
	private String  processInstanceId;
	private String taskId;
	@JsonProperty("deleted")
	String deleted;
	@JsonProperty
	

	String status;
	

	private String toBranchName;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty statusProperty;
	
	@JsonIgnore
	private StringProperty toBranchProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty intentNumberProperty;
	
	
	public StockTransferOutHdr() {
		this.statusProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.toBranchProperty = new SimpleStringProperty();
		this.voucherDateProperty =new SimpleStringProperty();
		this.intentNumberProperty =new SimpleStringProperty();
	}
	
	@JsonIgnore
	
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	

	@JsonIgnore
	
	public StringProperty getstatusProperty() {
		statusProperty.set(status);
		return statusProperty;
	}
	public void setstatusProperty(String status) {
		this.status = status;
	}


	
@JsonIgnore
	
	public StringProperty getvoucherDateProperty() {
	voucherDateProperty.set(SystemSetting.SqlDateTostring(voucherDate));
		return voucherDateProperty;
	}
	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
@JsonIgnore
	
	public StringProperty getintentNumberProperty() {
	intentNumberProperty.set(intentNumber);
		return intentNumberProperty;
	}
	public void setintentNumberProperty(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	

	public String getId() {
		return id;
	}

		public String getIntentNumber() {
		return intentNumber;
	}


	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	@JsonProperty
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	@JsonProperty
	public String getToBranch() {
		return toBranch;
	}
	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}
	@JsonProperty

	public String getVoucherType() {
		return voucherType;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}


	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getFromBranch() {
		return fromBranch;
	}

	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

	public StringProperty getToBranchProperty() {
		
		
		toBranchProperty.set(toBranch);
		return toBranchProperty;
	}

	public void setToBranchProperty(StringProperty toBranchProperty) {
		this.toBranchProperty = toBranchProperty;
	}

	


	public String getToBranchName() {
		return toBranchName;
	}

	public void setToBranchName(String toBranchName) {
		this.toBranchName = toBranchName;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "StockTransferOutHdr [id=" + id + ", intentNumber=" + intentNumber + ", voucherNumber=" + voucherNumber
				+ ", voucherDate=" + voucherDate + ", toBranch=" + toBranch + ", voucherType=" + voucherType
				+ ", fromBranch=" + fromBranch + ", slNo=" + slNo + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", deleted=" + deleted + ", status=" + status + ", toBranchName="
				+ toBranchName + "]";
	}

	

}
