package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacySalesReturnReport {

	String invoiceDate;
	String customerName;
	String voucherNumber;
	String currency;
	
	
	String salesVoucherNumber;
	String itemName;
	String itemCode;
	String groupName;
	String batchCode;
	String expiryDate;
	Double quantity;
	Double rate;
	Double value;
	String salesMan;
	Double amountBeforeGst;
	Double gst;
	Double amount;
	String userName;
	
	
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty invoiceDateProperty;
	@JsonIgnore
    private	StringProperty  customerNameProperty;
	
	@JsonIgnore
	 private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	 private StringProperty salesVoucherNumberProperty;
	
	@JsonIgnore
	 private StringProperty itemNameProperty;
	
	@JsonIgnore
	 private StringProperty itemCodeProperty;
	
	@JsonIgnore
	 private StringProperty groupNameProperty;
	
	@JsonIgnore
	 private StringProperty batchCodeProperty;
	
	@JsonIgnore
	 private StringProperty expiryDateProperty;
	
	@JsonIgnore
	 private StringProperty salesManProperty;
	
	@JsonIgnore
	 private StringProperty userNameProperty;
	
	@JsonIgnore
	 private DoubleProperty quantityProperty;
	
	@JsonIgnore
	 private DoubleProperty rateProperty;
	
	@JsonIgnore
	 private DoubleProperty valueProperty;
	
	@JsonIgnore
	 private DoubleProperty amountBeforeGstProperty;
	
	@JsonIgnore
	 private DoubleProperty gstProperty;
	
	@JsonIgnore
	 private DoubleProperty amountProperty;
	
	
	
	
	@JsonIgnore
	 private StringProperty currencyProperty;
	
	
	public PharmacySalesReturnReport (){
		
		this.invoiceDateProperty= new SimpleStringProperty("");
		this.customerNameProperty=new SimpleStringProperty("");
		this.voucherNumberProperty=new SimpleStringProperty("");
		this.currencyProperty= new SimpleStringProperty("");
		
		this.salesVoucherNumberProperty= new SimpleStringProperty("");
		this.itemNameProperty= new SimpleStringProperty("");
		this.itemCodeProperty= new SimpleStringProperty("");
		this.groupNameProperty= new SimpleStringProperty("");
		this.batchCodeProperty= new SimpleStringProperty("");
		this.expiryDateProperty= new SimpleStringProperty("");
		this.salesManProperty= new SimpleStringProperty("");
		this.userNameProperty= new SimpleStringProperty("");
		
		this.quantityProperty= new SimpleDoubleProperty();
		this.rateProperty= new SimpleDoubleProperty();
		this.valueProperty= new SimpleDoubleProperty();
		this.amountBeforeGstProperty= new SimpleDoubleProperty();
		this.gstProperty= new SimpleDoubleProperty();
		this.amountProperty= new SimpleDoubleProperty();
		
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@JsonIgnore
	public StringProperty getInvoiceDateProperty() {
		invoiceDateProperty.set(invoiceDate);
		return invoiceDateProperty;
	}

	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}
	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}

	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}
	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}

	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	@JsonIgnore
	public StringProperty getCurrencyProperty() {
	currencyProperty.set(currency);
		return currencyProperty;
	}

	public void setCurrencyProperty(StringProperty currencyProperty) {
		this.currencyProperty = currencyProperty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getSalesVoucherNumber() {
		return salesVoucherNumber;
	}

	public void setSalesVoucherNumber(String salesVoucherNumber) {
		this.salesVoucherNumber = salesVoucherNumber;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getSalesMan() {
		return salesMan;
	}

	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	public Double getAmountBeforeGst() {
		return amountBeforeGst;
	}

	public void setAmountBeforeGst(Double amountBeforeGst) {
		this.amountBeforeGst = amountBeforeGst;
	}

	public Double getGst() {
		return gst;
	}

	public void setGst(Double gst) {
		this.gst = gst;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public StringProperty getSalesVoucherNumberProperty() {
		salesVoucherNumberProperty.set(salesVoucherNumber);
		return salesVoucherNumberProperty;
	}

	public void setSalesVoucherNumberProperty(StringProperty salesVoucherNumberProperty) {
		this.salesVoucherNumberProperty = salesVoucherNumberProperty;
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getItemCodeProperty() {
		itemCodeProperty.set(itemCode);
		return itemCodeProperty;
	}

	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	public StringProperty getGroupNameProperty() {
		groupNameProperty.set(groupName);
		return groupNameProperty;
	}

	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}

	public StringProperty getBatchCodeProperty() {
		batchCodeProperty.set(batchCode);
		return batchCodeProperty;
	}

	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}

	public StringProperty getExpiryDateProperty() {
		expiryDateProperty.set(expiryDate);
		return expiryDateProperty;
	}

	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}

	public StringProperty getSalesManProperty() {
		salesManProperty.set(salesMan);
		return salesManProperty;
	}

	public void setSalesManProperty(StringProperty salesManProperty) {
		this.salesManProperty = salesManProperty;
	}

	public StringProperty getUserNameProperty() {
		userNameProperty.set(userName);
		return userNameProperty;
	}

	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}

	public DoubleProperty getQuantityProperty() {
		quantityProperty.set(quantity);
		return quantityProperty;
	}

	public void setQuantityProperty(DoubleProperty quantityProperty) {
		this.quantityProperty = quantityProperty;
	}

	public DoubleProperty getRateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}

	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	public DoubleProperty getValueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}

	public void setValueProperty(DoubleProperty valueProperty) {
		this.valueProperty = valueProperty;
	}

	public DoubleProperty getAmountBeforeGstProperty() {
		amountBeforeGstProperty.set(amountBeforeGst);
		return amountBeforeGstProperty;
	}

	public void setAmountBeforeGstProperty(DoubleProperty amountBeforeGstProperty) {
		this.amountBeforeGstProperty = amountBeforeGstProperty;
	}

	public DoubleProperty getGstProperty() {
		gstProperty.set(gst);
		return gstProperty;
	}

	public void setGstProperty(DoubleProperty gstProperty) {
		this.gstProperty = gstProperty;
	}

	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	@Override
	public String toString() {
		return "PharmacySalesReturnReport [invoiceDate=" + invoiceDate + ", customerName=" + customerName
				+ ", voucherNumber=" + voucherNumber + ", currency=" + currency + ", salesVoucherNumber="
				+ salesVoucherNumber + ", itemName=" + itemName + ", itemCode=" + itemCode + ", groupName=" + groupName
				+ ", batchCode=" + batchCode + ", expiryDate=" + expiryDate + ", quantity=" + quantity + ", rate="
				+ rate + ", value=" + value + ", salesMan=" + salesMan + ", amountBeforeGst=" + amountBeforeGst
				+ ", gst=" + gst + ", amount=" + amount + ", userName=" + userName + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", invoiceDateProperty=" + invoiceDateProperty
				+ ", customerNameProperty=" + customerNameProperty + ", voucherNumberProperty=" + voucherNumberProperty
				+ ", salesVoucherNumberProperty=" + salesVoucherNumberProperty + ", itemNameProperty="
				+ itemNameProperty + ", itemCodeProperty=" + itemCodeProperty + ", groupNameProperty="
				+ groupNameProperty + ", batchCodeProperty=" + batchCodeProperty + ", expiryDateProperty="
				+ expiryDateProperty + ", salesManProperty=" + salesManProperty + ", userNameProperty="
				+ userNameProperty + ", quantityProperty=" + quantityProperty + ", rateProperty=" + rateProperty
				+ ", valueProperty=" + valueProperty + ", amountBeforeGstProperty=" + amountBeforeGstProperty
				+ ", gstProperty=" + gstProperty + ", amountProperty=" + amountProperty + ", currencyProperty="
				+ currencyProperty + "]";
	}

	



}
