package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.util.Callback;

public class SalesOrderDtl {
	
	
	@JsonProperty
	String id;
	@JsonProperty
	String itemId;
	@JsonProperty
	Double rate;
	@JsonProperty
	Double cgstTaxRate;
	@JsonProperty
	Double sgstTaxRate;
	@JsonProperty
	Double cessRate;
	@JsonProperty
	Double qty;
	@JsonProperty
	Double addCessRate;
	@JsonProperty
	String itemTaxaxId;
	@JsonProperty
	String unitId;
	@JsonProperty
	String itemName;
	@JsonProperty
	String companyName;
	
	
	
	//Date expiryDate;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDateProperty;
	
	@JsonProperty
	String batch;
	@JsonProperty
	Double amount;
	@JsonProperty
	String barode;
	@JsonProperty
	Double taxRate;
	@JsonProperty
	Double mrp;
	@JsonProperty
	String itemCode;
	@JsonProperty
	String unitName;
	String branchCode;
	 Double costPrice;
	 String offerReferenceId;
	   Double discount;
	   String warrantySerial;
	    Double igstTaxRate;
	    Double cgstAmount;
	    Double sgstAmount;
	    Double igstAmount;
	    Double cessAmount;
	    Double addCessAmount;
	    private String  processInstanceId;
		private String taskId;
	StringProperty itemNameProperty;
	StringProperty unitNameProperty;
	StringProperty itemCodeProperty;
	StringProperty mrpProperty;
	StringProperty taxRateProperty;
	StringProperty barodeProperty;
	StringProperty batchProperty;
	StringProperty qtyProperty;
	StringProperty rateProperty;
	StringProperty orderMsgProperty;
	DoubleProperty amountProperty;
	StringProperty cessRateProperty;
	
	String rDate;
	 String orderMsg;
	private SalesOrderTransHdr salesOrderTransHdr;
    Double standardPrice;
    String schemeId;
    Double listPrice;
    
    
    Date expiryDate;
	public SalesOrderDtl () {
		
		this.orderMsgProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.mrpProperty = new SimpleStringProperty("");
		this.taxRateProperty = new SimpleStringProperty("");
		this.barodeProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty();
		this.qtyProperty = new SimpleStringProperty("");
		this.rateProperty = new SimpleStringProperty("");
		this.cessRateProperty = new SimpleStringProperty("");
		this.expiryDateProperty = new SimpleObjectProperty<LocalDate>(null);
		 this.discount = 0.0;
		    this. igstTaxRate =0.0;
		    this. cgstAmount =0.0;
		    this. sgstAmount =0.0;
		    this. igstAmount =0.0;
		    this. cessAmount=0.0;
		    this.cgstTaxRate =0.0;
		    this.sgstTaxRate =0.0;
		    this.addCessRate=0.0;
		    this.rate=0.0;
		    this.mrp=0.0;
	}

	public Double getListPrice() {
		return listPrice;
	}

	public void setListPrice(Double listPrice) {
		this.listPrice = listPrice;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public String getWarrantySerial() {
		return warrantySerial;
	}

	public void setWarrantySerial(String warrantySerial) {
		this.warrantySerial = warrantySerial;
	}

	public String getSchemeId() {
		return schemeId;
	}

	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}
	

	public String getOfferReferenceId() {
		return offerReferenceId;
	}

	public void setOfferReferenceId(String offerReferenceId) {
		this.offerReferenceId = offerReferenceId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		
		this.id = id;
	}

	



	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getBarode() {
		return barode;
	}

	public void setBarode(String barode) {
		this.barode = barode;
	}

	public StringProperty getBarodeProperty() {
		return barodeProperty;
	}

	public void setBarodeProperty(StringProperty barodeProperty) {
		this.barodeProperty = barodeProperty;
	}

	public StringProperty getBatchProperty() {
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public Double getRateBeforeDiscount() {
		return rateBeforeDiscount;
	}
	 Double rateBeforeDiscount;
	public void setRateBeforeDiscount(Double rateBeforeDiscount) {
		this.rateBeforeDiscount = rateBeforeDiscount;
	}

	public String getOrderMsg() {
		return orderMsg;
	}

	public void setOrderMsg(String orderMsg) {
		orderMsgProperty.set(orderMsg);
		this.orderMsg = orderMsg;
	}

	public StringProperty getOrderMsgProperty() {
		return orderMsgProperty;
	}

	public void setOrderMsgProperty(StringProperty orderMsgProperty) {
		this.orderMsgProperty = orderMsgProperty;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		amountProperty.set(amount);
		this.amount = amount;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getCgstAmount() {
		return cgstAmount;
	}

	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public Double getAddCessAmount() {
		return addCessAmount;
	}

	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}

	public Double getSgstAmount() {
		return sgstAmount;
	}

	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public Double getIgstAmount() {
		return igstAmount;
	}

	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}

	public Double getCessAmount() {
		return cessAmount;
	}

	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}

	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		 
		this.itemId = itemId;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		rateProperty.set(rate+"");
		this.rate = rate;
	}

	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		
		
		this.cgstTaxRate = cgstTaxRate;
	}

	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		qtyProperty.set(qty+"");
		this.qty = qty;
	}

	public Double getAddCessRate() {
		return addCessRate;
	}

	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}

	public String getItemTaxaxId() {
		return itemTaxaxId;
	}

	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		
		this.itemNameProperty.set(itemName);
		this.itemName = itemName;
	}

	

	public String getBatchCode() {
		return batch;
	}

	public void setBatchCode(String batch) {
		this.batchProperty.set(batch);
		this.batch = batch;
	}

	public String getBarcode() {
		return barode;
	}

	public void setBarcode(String barCode) {
		this.barodeProperty.set(barCode);
		this.barode = barCode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRateProperty.set(taxRate+"");
		this.taxRate = taxRate;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrpProperty.set(mrp+"");
		this.mrp = mrp;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCodeProperty.set(itemCode);
		this.itemCode = itemCode;
	}

	public String getUnitName() {
		
		
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitNameProperty.set(unitName);
		this.unitName = unitName;
	}

	public StringProperty getItemNameProperty() {
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getUnitNameProperty() {
		return unitNameProperty;
	}

	public StringProperty getRateProperty() {
		rateProperty.set(rate+"");
		return rateProperty;
	}

	public void setRateProperty(StringProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}

	public StringProperty getItemCodeProperty() {
		return itemCodeProperty;
	}

	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	public StringProperty getMrpProperty() {
		return mrpProperty;
	}

	public void setMrpProperty(StringProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}

	public DoubleProperty getAmountProperty() {
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	public StringProperty getTaxRateProperty() {
		return taxRateProperty;
	}

	public void setTaxRateProperty(StringProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}

	public StringProperty getBarcodeProperty() {
		return barodeProperty;
	}

	public void setBarcodeProperty(StringProperty barcodeProperty) {
		this.barodeProperty = barodeProperty;
	}

	public StringProperty getBatchCodeProperty() {
		return batchProperty;
	}

	public void setBatchCodeProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public StringProperty getQtyProperty() {
		return qtyProperty;
	}

	public void setQtyProperty(StringProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public StringProperty getCessRateProperty() {
		return cessRateProperty;
	}

	public void setCessRateProperty(StringProperty cessRateProperty) {
		this.cessRateProperty = cessRateProperty;
	}


	
	@JsonIgnore
	public ObjectProperty<LocalDate> getexpiryDateProperty() {
		if(null != expiryDate)
		{
		expiryDateProperty.set(expiryDate.toLocalDate());
		}
		return expiryDateProperty;
	}
	
	public void setexpiryDateProperty(ObjectProperty<LocalDate> expiryDateProperty) {
		
		this.expiryDateProperty = expiryDateProperty;
		this.expiryDate = expiryDate;
	}
	
//	@JsonProperty("expiryDate")
//	
//	public java.sql.Date getexpiryDate() {
//		 
//		return null== expiryDate.get() ? null : Date.valueOf(this.expiryDate.get() );
//	}
	

	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	
	public String getrDate() {
		return rDate;
	}

	public void setrDate(String rDate) {
		this.rDate = rDate;
	}
	
	

	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	
	public Double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Date getexpiryDate() {
		return expiryDate;
	}

	public void setexpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

		 
}
