package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UserGroupMst {
	
	String id;
	String groupId;
	String userId;
	String userName;
	String groupName;
	private String  processInstanceId;
	private String taskId;
	public UserGroupMst() {
		this.userNameProperty = new SimpleStringProperty("");
		this.groupNameProperty = new SimpleStringProperty("");
		this.groupIdProperty = new SimpleStringProperty("");
	}
	
	@JsonIgnore
	private StringProperty groupNameProperty;
	@JsonIgnore
	private StringProperty userNameProperty;
	@JsonIgnore
	private StringProperty groupIdProperty;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupId() {
		
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public StringProperty getGroupNameProperty() {

		groupNameProperty.set(groupName);
		return groupNameProperty;
	}

	public void setGroupNameProperty(StringProperty groupNameProperty) {
		groupNameProperty = groupNameProperty;
	}
	
	public StringProperty getUserNameProperty() {

		userNameProperty.set(userName);
		return userNameProperty;
	}

	public void setUserNameProperty(StringProperty userNameProperty) {
		userNameProperty = userNameProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "UserGroupMst [id=" + id + ", groupId=" + groupId + ", userId=" + userId + ", userName=" + userName
				+ ", groupName=" + groupName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
}
