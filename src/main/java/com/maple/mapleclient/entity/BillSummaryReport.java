package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BillSummaryReport {

	String slNo;
	String description;
	String amount;
	
	private String  processInstanceId;
	private String taskId;

	@JsonIgnore
	private StringProperty slNoProperty;
	@JsonIgnore
	private StringProperty descriptionProperty;
	@JsonIgnore
	private StringProperty amountProperty;
	
	

	public BillSummaryReport() {
		this.slNoProperty = new SimpleStringProperty("");
		this.descriptionProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleStringProperty("");
	}

	public String getSlNo() {
		return slNo;
	}

	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public StringProperty getSlNoPropertyProperty() {

		slNoProperty.set(slNo);
		return slNoProperty;
	}

	public void setSlNoProperty(StringProperty slNoProperty) {
		slNoProperty = slNoProperty;
	}

	public StringProperty getDescriptionProperty() {

		descriptionProperty.set(description);
		return descriptionProperty;
	}

	public void setDescriptionProperty(StringProperty descriptionProperty) {
		descriptionProperty = descriptionProperty;
	}

	public StringProperty getAmountProperty() {

		amountProperty.set(amount);
		return amountProperty;
	}

	public void setAmountProperty(StringProperty descriptionProperty) {
		amountProperty = amountProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "BillSummaryReport [slNo=" + slNo + ", description=" + description + ", amount=" + amount
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	

}
