package com.maple.mapleclient.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockVerificationDtl {

	String id;
	String itemId;
	Double qty;
	Double systemQty;
	private String  processInstanceId;
	private String taskId;
	
	private LocalDateTime updatedTime;
	
	@JsonIgnore
	private String itemName;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	public StockVerificationDtl() {
		
		this.itemNameProperty = new SimpleStringProperty();
		this.qtyProperty =  new SimpleDoubleProperty();
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	@JsonIgnore
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}

	
	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}
	
	public Double getSystemQty() {
		return systemQty;
	}

	public void setSystemQty(Double systemQty) {
		this.systemQty = systemQty;
	}

	@Override
	public String toString() {
		return "StockVerificationDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", systemQty=" + systemQty
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", updatedTime=" + updatedTime
				+ ", itemName=" + itemName + "]";
	}

	
	
}
