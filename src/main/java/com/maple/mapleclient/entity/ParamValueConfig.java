package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ParamValueConfig {

	String param;
	String value;
	String id;

	String branchCode;
	
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;

	
	
	public ParamValueConfig() {
		
		this.paramProperty = new SimpleStringProperty("");
		this.valueProperty = new SimpleStringProperty("");


	}
	@JsonIgnore
	private StringProperty paramProperty;
	
	@JsonIgnore
	private StringProperty valueProperty;
	
	
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	public StringProperty getParamProperty() {
		paramProperty.set(param);
		return paramProperty;
	}
	public void setParamProperty(StringProperty paramProperty) {
		this.paramProperty = paramProperty;
	}
	public StringProperty getValueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}
	public void setValueProperty(StringProperty valueProperty) {
		this.valueProperty = valueProperty;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ParamValueConfig [param=" + param + ", value=" + value + ", id=" + id + ", branchCode=" + branchCode
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	
	
	
	
}
