package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;
import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesReceipts {

	String Id;
	String receiptMode;
	String branchCode;
	String userId;
	Double receiptAmount;
	java.util.Date rereceiptDate;
	SalesTransHdr salesTransHdr;
	SalesOrderTransHdr salesOrderTransHdr;
	String accountId;
	String voucherNumber;
	Double fcAmount;
	String soStatus;
	private String  processInstanceId;
	private String taskId;
	public SalesReceipts() {

		this.receiptAmountProperty = new SimpleDoubleProperty();
		this.receiptModeProperty = new SimpleStringProperty();

	}



	public java.util.Date getRereceiptDate() {
		return rereceiptDate;
	}

	public void setRereceiptDate(java.util.Date rereceiptDate) {
		this.rereceiptDate = rereceiptDate;
	}

	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}

	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}

	public void setReceiptAmountProperty(DoubleProperty receiptAmountProperty) {
		this.receiptAmountProperty = receiptAmountProperty;
	}



	@JsonIgnore
	private StringProperty receiptModeProperty;


	@JsonIgnore
	private DoubleProperty receiptAmountProperty;


	
	
	
	
	public String getReceiptMode() {
		return receiptMode;
	}

	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}





	public String getSoStatus() {
		return soStatus;
	}

	public void setSoStatus(String soStatus) {
		this.soStatus = soStatus;
	}

	public java.util.Date getReceiptDate() {
		return rereceiptDate;
	}

	public void setReceiptDate(java.util.Date rereceiptDate) {
		this.rereceiptDate = rereceiptDate;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public Double getFcAmount() {
		return fcAmount;
	}

	public void setFcAmount(Double fcAmount) {
		this.fcAmount = fcAmount;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public Double getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}



	public DoubleProperty getReceiptAmountProperty() {
		receiptAmountProperty.set(receiptAmount);
		return receiptAmountProperty;
	}

	public void setReceiptAmountProperty(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	
	

	public StringProperty getReceiptModeProperty() {
		receiptModeProperty.set(receiptMode);
		return receiptModeProperty;
	}

	public void setReceiptModeProperty(StringProperty receiptModeProperty) {
		this.receiptModeProperty = receiptModeProperty;
	}
	


	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}

	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	


	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "SalesReceipts [Id=" + Id + ", receiptMode=" + receiptMode + ", branchCode=" + branchCode + ", userId="
				+ userId + ", receiptAmount=" + receiptAmount + ", rereceiptDate=" + rereceiptDate + ", salesTransHdr="
				+ salesTransHdr + ", salesOrderTransHdr=" + salesOrderTransHdr + ", accountId=" + accountId
				+ ", voucherNumber=" + voucherNumber + ", fcAmount=" + fcAmount + ", soStatus=" + soStatus
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", receiptModeProperty="
				+ receiptModeProperty + ", receiptAmountProperty=" + receiptAmountProperty + "]";
	}




}
