package com.maple.mapleclient.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PurchaseSchemeMst {
	 private String id;
		
		String supplierId;
		String itemId;
		String offerDescription;
		Double minimumQty;
		Date fromDate;
		Date toDate;
		String branchCode;
		private String  processInstanceId;
		private String taskId;
		
		@JsonIgnore
		private StringProperty supplierNameProperty;
		
		@JsonIgnore
		private StringProperty itemNameProperty;
		
		@JsonIgnore
		private StringProperty offerDescriptionProperty;
		
		@JsonIgnore
		private DoubleProperty minimumQtyProperty;
		
		@JsonIgnore
		private StringProperty fromDateProperty;
		
		@JsonIgnore
		private StringProperty toDateProperty;
		
		@JsonIgnore
		private String itemName;
		
		@JsonIgnore
		private String supplierName;
		
		public PurchaseSchemeMst() {
			this.supplierNameProperty = new SimpleStringProperty();
			this.itemNameProperty = new SimpleStringProperty();
			this.offerDescriptionProperty = new SimpleStringProperty();
			this.minimumQtyProperty = new SimpleDoubleProperty();
			this.fromDateProperty = new SimpleStringProperty();
			this.toDateProperty = new SimpleStringProperty();
		}
		@JsonIgnore
		public StringProperty getsupplierNameProperty () {
			supplierNameProperty .set(supplierName);
			return supplierNameProperty ;
		}
		public void setsupplierNameProperty (StringProperty supplierNameProperty ) {
			this.supplierNameProperty  = supplierNameProperty ;
		}
		@JsonIgnore
		public StringProperty getitemNameProperty () {
			itemNameProperty .set(itemName);
			return itemNameProperty ;
		}
		public void setitemNameProperty (StringProperty itemNameProperty ) {
			this.itemNameProperty  = itemNameProperty ;
		}
		@JsonIgnore
		public StringProperty getofferDescriptionProperty () {
			offerDescriptionProperty .set(offerDescription);
			return offerDescriptionProperty ;
		}
		public void setofferDescriptionProperty (StringProperty offerDescriptionProperty ) {
			this.offerDescriptionProperty  = offerDescriptionProperty ;
		}
		@JsonIgnore
		public DoubleProperty getminimumQtyProperty () {
			minimumQtyProperty .set(minimumQty);
			return minimumQtyProperty ;
		}
		public void setminimumQtyProperty(DoubleProperty minimumQtyProperty ) {
			this.minimumQtyProperty  = minimumQtyProperty ;
		}
		@JsonIgnore
		public StringProperty getfromDateProperty () {
			if(null != fromDate)
			{
			fromDateProperty .set(SystemSetting.UtilDateToString(fromDate, "dd-MM-yyyy"));
			}
			return fromDateProperty ;
		}
		public void setfromDateProperty (StringProperty fromDateProperty ) {
			this.fromDateProperty  = fromDateProperty ;
		}
		@JsonIgnore
		public StringProperty gettoDateProperty() {
			if(null != toDate)
			{
			toDateProperty .set(SystemSetting.UtilDateToString(toDate, "dd-MM-yyyy"));
			}
			return toDateProperty ;
		}
		public void settoDateProperty (StringProperty toDateProperty ) {
			this.toDateProperty  = toDateProperty ;
		}
		public String getId() {
			return id;
		}
		
		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public String getSupplierName() {
			return supplierName;
		}

		public void setSupplierName(String supplierName) {
			this.supplierName = supplierName;
		}

		public void setId(String id) {
			this.id = id;
		}
		public String getSupplierId() {
			return supplierId;
		}
		public void setSupplierId(String supplierId) {
			this.supplierId = supplierId;
		}
		public String getItemId() {
			return itemId;
		}
		public void setItemId(String itemId) {
			this.itemId = itemId;
		}
		public String getOfferDescription() {
			return offerDescription;
		}
		public void setOfferDescription(String offerDescription) {
			this.offerDescription = offerDescription;
		}
		
		public String getBranchCode() {
			return branchCode;
		}
		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}
		public Double getMinimumQty() {
			return minimumQty;
		}
		public void setMinimumQty(Double minimumQty) {
			this.minimumQty = minimumQty;
		}
		public Date getFromDate() {
			return fromDate;
		}
		public void setFromDate(Date fromDate) {
			this.fromDate = fromDate;
		}
		public Date getToDate() {
			return toDate;
		}
		public void setToDate(Date toDate) {
			this.toDate = toDate;
		}
	
		public String getProcessInstanceId() {
			return processInstanceId;
		}
		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}
		public String getTaskId() {
			return taskId;
		}
		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}
		@Override
		public String toString() {
			return "PurchaseSchemeMst [id=" + id + ", supplierId=" + supplierId + ", itemId=" + itemId
					+ ", offerDescription=" + offerDescription + ", minimumQty=" + minimumQty + ", fromDate=" + fromDate
					+ ", toDate=" + toDate + ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId
					+ ", taskId=" + taskId + ", itemName=" + itemName + ", supplierName=" + supplierName + "]";
		}
		
}
