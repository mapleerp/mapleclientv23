package com.maple.mapleclient.entity;

import java.util.Date;

public class ProductConversionMst {
	
	

	String branchCode;
	  String id;
	 Date conversionDate;
	 String voucherNo;
	 private String  processInstanceId;
		private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getConversionDate() {
		return conversionDate;
	}
	public void setConversionDate(Date conversionDate) {
		this.conversionDate = conversionDate;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ProductConversionMst [branchCode=" + branchCode + ", id=" + id + ", conversionDate=" + conversionDate
				+ ", voucherNo=" + voucherNo + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	

}
