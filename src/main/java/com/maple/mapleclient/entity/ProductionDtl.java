package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductionDtl {

	String id;
	String itemId;
	String batch;
	Double qty;
	String status;

	String kitname;
	private String processInstanceId;
	private String taskId;

	String store;

	@JsonIgnore
	private StringProperty kitnameProperty;

	@JsonIgnore
	private StringProperty idProperty;

	@JsonIgnore
	private StringProperty batchProperty;

	@JsonIgnore
	private DoubleProperty qtyProperty;

	ProductionMst productionMst;

	public ProductionMst getProductionMst() {
		return productionMst;
	}

	public void setProductionMst(ProductionMst productionMst) {
		this.productionMst = productionMst;
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	@JsonProperty
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonProperty
	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public ProductionDtl() {
		this.kitnameProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty();
		this.idProperty = new SimpleStringProperty("");
	}

	@JsonIgnore

	public StringProperty getKitnameProperty() {
		kitnameProperty.set(kitname);
		return kitnameProperty;
	}

	public void setKitnameProperty(String kitname) {
		this.kitname = kitname;
	}

	@JsonIgnore
	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setbatchProperty(String batch) {
		this.batch = batch;
	}

	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}

	@JsonIgnore

	public String getKitname() {
		return kitname;
	}

	public void setKitname(String kitname) {
		this.kitname = kitname;
	}

	public StringProperty getIdProperty() {
		idProperty.set(id);

		return idProperty;
	}

	public void setIdProperty(StringProperty idProperty) {
		this.idProperty = idProperty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	@Override
	public String toString() {
		return "ProductionDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", qty=" + qty + ", status="
				+ status + ", kitname=" + kitname + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", store=" + store + ", kitnameProperty=" + kitnameProperty + ", idProperty=" + idProperty
				+ ", batchProperty=" + batchProperty + ", qtyProperty=" + qtyProperty + ", productionMst="
				+ productionMst + "]";
	}

	
}
