package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PDCPayment implements Serializable {

	private static final long serialVersionUID = 1L;

	String id;
	String account;
	Double amount;
	String bank;
	String remark;
	String instrumentnumber;
	String bankAccountNumber;

	String branchCode;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	String status;

	@JsonIgnore
	String accountName;

	@JsonIgnore
	StringProperty accountNameProperty;

	@JsonIgnore
	StringProperty remarkeProperty;

	@JsonIgnore
	StringProperty bankProperty;

	@JsonIgnore
	StringProperty instNoProperty;

	@JsonIgnore
	DoubleProperty amountProperty;

	@JsonIgnore
	private ObjectProperty<LocalDate> instrumentDate;

	@JsonIgnore
	private ObjectProperty<LocalDate> transDate;

	public PDCPayment() {

		this.accountNameProperty = new SimpleStringProperty("");
		this.remarkeProperty = new SimpleStringProperty("");
		this.bankProperty = new SimpleStringProperty("");
		this.instNoProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty();
		this.instrumentDate = new SimpleObjectProperty<LocalDate>(null);
		this.transDate = new SimpleObjectProperty<LocalDate>(null);

	}
	
	
	
	

	public String getStatus() {
		return status;
	}





	public void setStatus(String status) {
		this.status = status;
	}





	public String getBank() {
		return bank;
	}



	public void setBank(String bank) {
		this.bank = bank;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getInstrumentnumber() {
		return instrumentnumber;
	}

	public void setInstrumentnumber(String instrumentnumber) {
		this.instrumentnumber = instrumentnumber;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

//	public Date getInstrumentDate() {
//		return instrumentDate;
//	}
//
//	public void setInstrumentDate(Date instrumentDate) {
//		this.instrumentDate = instrumentDate;
//	}
//
//	public Date getTransDate() {
//		return transDate;
//	}
//
//	public void setTransDate(Date transDate) {
//		this.transDate = transDate;
//	}

	@JsonProperty("instrumentDate")
	public Date getInstrumentDate() {
		if (null == this.instrumentDate.get()) {
			return null;
		} else {
			return Date.valueOf(this.instrumentDate.get());
		}
	}

	public void setInstrumentDate(Date instrumentDate) {
		if (null != instrumentDate)
			this.instrumentDate.set(instrumentDate.toLocalDate());
	}

	@JsonProperty("transDate")
	public Date getTransDate() {
		if (null == this.transDate.get()) {
			return null;
		} else {
			return Date.valueOf(this.transDate.get());
		}
	}

	public void setTransDate(Date transDate) {
		if (null != transDate)
			this.transDate.set(transDate.toLocalDate());
	}


	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public StringProperty getAccountNameProperty() {
		accountNameProperty.set(accountName);
		return accountNameProperty;
	}

	public void setAccountNameProperty(StringProperty accountNameProperty) {
		this.accountNameProperty = accountNameProperty;
	}

	public StringProperty getRemarkeProperty() {
		remarkeProperty.set(remark);
		return remarkeProperty;
	}

	public void setRemarkeProperty(StringProperty remarkeProperty) {
		this.remarkeProperty = remarkeProperty;
	}

	public StringProperty getBankProperty() {
		bankProperty.set(bank);
		return bankProperty;
	}

	public void setBankProperty(StringProperty bankProperty) {
		this.bankProperty = bankProperty;
	}

	public StringProperty getInstNoProperty() {
		instNoProperty.set(instrumentnumber);
		return instNoProperty;
	}

	public void setInstNoProperty(StringProperty instNoProperty) {
		this.instNoProperty = instNoProperty;
	}


	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	public ObjectProperty<LocalDate> getInstDateProperty() {
		return instrumentDate;
	}

	public void setInstDateProperty(ObjectProperty<LocalDate> instDateProperty) {
		this.instrumentDate = instDateProperty;
	}

	public ObjectProperty<LocalDate> getTransDateProperty() {
		return transDate;
	}

	public void setTransDateProperty(ObjectProperty<LocalDate> transDateProperty) {
		this.transDate = transDateProperty;
	}





	public String getProcessInstanceId() {
		return processInstanceId;
	}





	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}





	public String getTaskId() {
		return taskId;
	}





	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}





	@Override
	public String toString() {
		return "PDCPayment [id=" + id + ", account=" + account + ", amount=" + amount + ", bank=" + bank + ", remark="
				+ remark + ", instrumentnumber=" + instrumentnumber + ", bankAccountNumber=" + bankAccountNumber
				+ ", branchCode=" + branchCode + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", status=" + status + ", accountName=" + accountName
				+ "]";
	}

}
