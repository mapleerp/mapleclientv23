package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesTypeMst {

	
	String id;
	String salesType;
	String salesPrefix;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty salesTypeProperty;
	
	@JsonIgnore
	private StringProperty salesPrefixProperty;
	
	
	public SalesTypeMst() {
		
		this.salesTypeProperty = new SimpleStringProperty();
		this.salesPrefixProperty = new SimpleStringProperty();
	}
	
	@JsonIgnore
	public StringProperty getsalesTypeProperty() {
		salesTypeProperty.set(salesType);
		return salesTypeProperty;
	}

	public void setsalesTypeProperty(String salesType) {
		this.salesType = salesType;
	}

	@JsonIgnore
	public StringProperty getsalesPrefixProperty() {
		salesPrefixProperty.set(salesPrefix);
		return salesPrefixProperty;
	}
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public void setsalesPrefixProperty(String salesPrefix) {
		this.salesPrefix = salesPrefix;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSalesType() {
		return salesType;
	}
	public void setSalesType(String salesType) {
		this.salesType = salesType;
	}
	public String getSalesPrefix() {
		return salesPrefix;
	}
	public void setSalesPrefix(String salesPrefix) {
		this.salesPrefix = salesPrefix;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SalesTypeMst [id=" + id + ", salesType=" + salesType + ", salesPrefix=" + salesPrefix + ", branchCode="
				+ branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
