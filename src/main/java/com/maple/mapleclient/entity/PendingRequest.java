package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

public class PendingRequest {
	String statutoryVoucher;
	private String  processInstanceId;
	private String taskId;

	public String getStatutoryVoucher() {
		return statutoryVoucher;
	}

	public void setStatutoryVoucher(String statutoryVoucher) {
		this.statutoryVoucher = statutoryVoucher;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "PendingRequest [statutoryVoucher=" + statutoryVoucher + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	

}
