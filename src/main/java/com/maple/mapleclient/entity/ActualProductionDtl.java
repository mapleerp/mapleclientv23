package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ActualProductionDtl {

	String id;
	String itemId;
	String batch;
	Double actualQty;
	String itemName;
	Double productionCost;

	private String processInstanceId;
	private String taskId;

	private String store;

	@JsonIgnore
	private StringProperty itemNameProperty;

	@JsonIgnore
	private StringProperty batchProperty;

	@JsonIgnore
	private DoubleProperty actualQtyProperty;

	@JsonIgnore
	private StringProperty idProperty;

	@JsonIgnore
	private DoubleProperty productionCostProperty;

	public ActualProductionDtl() {

		this.itemNameProperty = new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
		this.actualQtyProperty = new SimpleDoubleProperty();
		this.idProperty = new SimpleStringProperty();
		this.productionCostProperty = new SimpleDoubleProperty();
	}

	public Double getProductionCost() {
		return productionCost;
	}

	public void setProductionCost(Double productionCost) {
		this.productionCost = productionCost;
	}

	ProductionDtl productionDtl;

	public ProductionDtl getProductionDtl() {
		return productionDtl;
	}

	public void setProductionDtl(ProductionDtl productionDtl) {
		this.productionDtl = productionDtl;
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getActualQty() {
		return actualQty;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public void setActualQty(Double actualQty) {
		this.actualQty = actualQty;
	}

	ActualProductionHdr actualProductionHdr;

	public ActualProductionHdr getActualProductionHdr() {
		return actualProductionHdr;
	}

	public void setActualProductionHdr(ActualProductionHdr actualProductionHdr) {
		this.actualProductionHdr = actualProductionHdr;
	}

	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}

	@JsonIgnore
	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setbatchProperty(String batch) {
		this.batch = batch;
	}

	@JsonIgnore
	public DoubleProperty getProductionCostProperty() {
		productionCostProperty.set(productionCost);
		return productionCostProperty;
	}

	public void setProductionCostProperty(Double productionCost) {
		this.productionCost = productionCost;
	}

	@JsonIgnore
	public DoubleProperty getactualQtyProperty() {
		actualQtyProperty.set(actualQty);
		return actualQtyProperty;
	}

	public void setactualQtyProperty(Double actualQty) {
		this.actualQty = actualQty;
	}

	@JsonIgnore
	public StringProperty getidProperty() {
		idProperty.set(id);
		return idProperty;
	}

	public void setidProperty(String id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	@Override
	public String toString() {
		return "ActualProductionDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", actualQty=" + actualQty
				+ ", itemName=" + itemName + ", productionCost=" + productionCost + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", store=" + store + ", itemNameProperty="
				+ itemNameProperty + ", batchProperty=" + batchProperty + ", actualQtyProperty=" + actualQtyProperty
				+ ", idProperty=" + idProperty + ", productionCostProperty=" + productionCostProperty
				+ ", productionDtl=" + productionDtl + ", actualProductionHdr=" + actualProductionHdr + "]";
	}


}
