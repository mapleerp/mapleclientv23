package com.maple.mapleclient.entity;

import java.util.Date;

public class OwnAccountSettlementMst {

	String id;
	Date voucherDate;
	String voucherNumber;
	
	AccountHeads accountHeads;
	private String  processInstanceId;
	private String taskId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public AccountHeads getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}
	@Override
	public String toString() {
		return "OwnAccountSettlementMst [id=" + id + ", voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber
				+ ", accountHeads=" + accountHeads + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}



}
