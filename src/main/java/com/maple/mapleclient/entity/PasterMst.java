package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PasterMst {
	
	String pasterMstId;
	String pasterName;
//	Date doj;
//	Date dol;
	String orgId;
	String memberId;
	String id;
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	@JsonIgnore
    private StringProperty memberIdProperty;
	@JsonIgnore
    private StringProperty orgIdProperty;
	@JsonIgnore
    private StringProperty pasterNameProperty;
	@JsonIgnore
    private StringProperty pasterMstIdProperty;
	@JsonIgnore
	private ObjectProperty<LocalDate> doj;
	@JsonIgnore
	private ObjectProperty<LocalDate> dol;
	
	public PasterMst() {
		this.memberIdProperty = new SimpleStringProperty("");
		this.orgIdProperty = new SimpleStringProperty("");
		this.pasterNameProperty = new SimpleStringProperty("");
		this.pasterMstIdProperty = new SimpleStringProperty("");
		this.doj = new SimpleObjectProperty<LocalDate>(null);
		this.dol = new SimpleObjectProperty<LocalDate>(null);
	}


	@JsonIgnore
	public StringProperty getMemberIdProperty() {
		memberIdProperty.set(memberId);
		return memberIdProperty;
	}
	public void setMemberIdProperty(String memberId) {
		this.memberId = memberId;
	}
	@JsonIgnore
	public StringProperty getOrgIdProperty() {
		orgIdProperty.set(orgId);
		return orgIdProperty;
	}
	public void setOrgIdProperty(String orgId) {
		this.orgId = orgId;
	}
	@JsonIgnore
	public StringProperty getPasterNameProperty() {
		pasterNameProperty.set(pasterName);
		return pasterNameProperty;
	}
	public void setPasterNameProperty(String pasterName) {
		this.pasterName = pasterName;
	}
	@JsonIgnore
	public StringProperty getPasterMstIdProperty() {
		pasterMstIdProperty.set(pasterMstId);
		return pasterMstIdProperty;
	}
	public void setPasterMstIdProperty(String pasterMstId) {
		this.pasterMstId = pasterMstId;
	}
	public ObjectProperty<LocalDate> getDolProperty( ) {
		return dol;
	}
	public void setDolProperty(ObjectProperty<LocalDate> dol) {
		this.dol = dol;
	}
	public ObjectProperty<LocalDate> getDojProperty( ) {
		return doj;
	}
 
	public void setDojProperty(ObjectProperty<LocalDate> doj) {
		this.doj = doj;
	}
	@JsonProperty("doj")
	public java.sql.Date getdoj ( ) {
		if(null==this.doj.get() ) {
			return null;
		}else {
			return Date.valueOf(this.doj.get() );
		}
	}	
   public void setdoj (java.sql.Date dojProperty) {
						if(null!=dojProperty)
		this.doj.set(dojProperty.toLocalDate());
	}
   
	
	   @JsonProperty("dol")
		public java.sql.Date getdol ( ) {
			if(null==this.dol.get() ) {
				return null;
			}else {
				return Date.valueOf(this.dol.get() );
			}
		}	
	  public void setdol (java.sql.Date dolProperty) {
							if(null!=dolProperty)
			this.dol.set(dolProperty.toLocalDate());
		}


   @JsonProperty
	public String getPasterMstId() {
		return pasterMstId;
	}
	public void setPasterMstId(String pasterMstId) {
		this.pasterMstId = pasterMstId;
	}
	@JsonProperty
	public String getPasterName() {
		return pasterName;
	}
	public void setPasterName(String pasterName) {
		this.pasterName = pasterName;
	}
	@JsonProperty
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	@JsonProperty
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "PasterMst [pasterMstId=" + pasterMstId + ", pasterName=" + pasterName + ", orgId=" + orgId
				+ ", memberId=" + memberId + ", id=" + id + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
	
	
}
