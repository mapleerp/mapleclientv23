package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;

import org.springframework.stereotype.Component;

public class DayEndReportStore implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	String branchCode;
	private String branchName;
	
	private String posMaxBillNo;
	private String posMinBillNo;
	private String posBillCount;
	
	private String wholeSaleMaxBillNo;
	private String wholeSaleMinBillNo;
	private String wholeSaleBillCount;
	
	private String pettyCash;
	private String closingCash;
	private String totalCash;
	private String physicalCash;
	 Date date;
	 
	private String cashReceipt;
	private String creditReceipt;
	private String ebsReceipt;
	private String gpayReceipt;
	private String swiggyReceipt;
	private String yesBankReceipt;
	private String cardSale;
	private String zomatoReceipt;
	private String paytmReceipt;
	private String totalSales;
	
	private String cashSaleOrder;
	private String cardSaleOrder;
	private String creditSaleOrder;
	private String ebsSaleOrder;
	private String gpaySaleOrder;
	private String yesBankSaleOrder;
	private String paytmSaleOrder;
	
	private String previousCashSaleOrder;
	private String cash;
	private String neftSales;


	
	private String cashVarience;
	
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getNeftSales() {
		return neftSales;
	}

	public void setNeftSales(String neftSales) {
		this.neftSales = neftSales;
	}

	public String getCash() {
		return cash;
	}

	public void setCash(String cash) {
		this.cash = cash;
	}

	public String getPreviousCashSaleOrder() {
		return previousCashSaleOrder;
	}

	public void setPreviousCashSaleOrder(String previousCashSaleOrder) {
		this.previousCashSaleOrder = previousCashSaleOrder;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPosMaxBillNo() {
		return posMaxBillNo;
	}

	public void setPosMaxBillNo(String posMaxBillNo) {
		this.posMaxBillNo = posMaxBillNo;
	}

	public String getPosMinBillNo() {
		return posMinBillNo;
	}

	public void setPosMinBillNo(String posMinBillNo) {
		this.posMinBillNo = posMinBillNo;
	}

	public String getPosBillCount() {
		return posBillCount;
	}

	public void setPosBillCount(String posBillCount) {
		this.posBillCount = posBillCount;
	}

	public String getWholeSaleMaxBillNo() {
		return wholeSaleMaxBillNo;
	}

	public void setWholeSaleMaxBillNo(String wholeSaleMaxBillNo) {
		this.wholeSaleMaxBillNo = wholeSaleMaxBillNo;
	}

	public String getWholeSaleMinBillNo() {
		return wholeSaleMinBillNo;
	}

	public void setWholeSaleMinBillNo(String wholeSaleMinBillNo) {
		this.wholeSaleMinBillNo = wholeSaleMinBillNo;
	}

	public String getWholeSaleBillCount() {
		return wholeSaleBillCount;
	}

	public void setWholeSaleBillCount(String wholeSaleBillCount) {
		this.wholeSaleBillCount = wholeSaleBillCount;
	}

	public String getPettyCash() {
		return pettyCash;
	}

	public void setPettyCash(String pettyCash) {
		this.pettyCash = pettyCash;
	}

	public String getClosingCash() {
		return closingCash;
	}

	public void setClosingCash(String closingCash) {
		this.closingCash = closingCash;
	}

	public String getTotalCash() {
		return totalCash;
	}

	public void setTotalCash(String totalCash) {
		this.totalCash = totalCash;
	}

	public String getPhysicalCash() {
		return physicalCash;
	}

	public void setPhysicalCash(String physicalCash) {
		this.physicalCash = physicalCash;
	}

	

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCashReceipt() {
		return cashReceipt;
	}

	public void setCashReceipt(String cashReceipt) {
		this.cashReceipt = cashReceipt;
	}

	public String getCreditReceipt() {
		return creditReceipt;
	}

	public void setCreditReceipt(String creditReceipt) {
		this.creditReceipt = creditReceipt;
	}

	public String getEbsReceipt() {
		return ebsReceipt;
	}

	public void setEbsReceipt(String ebsReceipt) {
		this.ebsReceipt = ebsReceipt;
	}

	public String getGpayReceipt() {
		return gpayReceipt;
	}

	public void setGpayReceipt(String gpayReceipt) {
		this.gpayReceipt = gpayReceipt;
	}

	public String getSwiggyReceipt() {
		return swiggyReceipt;
	}

	public void setSwiggyReceipt(String swiggyReceipt) {
		this.swiggyReceipt = swiggyReceipt;
	}

	public String getYesBankReceipt() {
		return yesBankReceipt;
	}

	public void setYesBankReceipt(String yesBankReceipt) {
		this.yesBankReceipt = yesBankReceipt;
	}

	public String getCardSale() {
		return cardSale;
	}

	public void setCardSale(String cardSale) {
		this.cardSale = cardSale;
	}

	public String getZomatoReceipt() {
		return zomatoReceipt;
	}

	public void setZomatoReceipt(String zomatoReceipt) {
		this.zomatoReceipt = zomatoReceipt;
	}

	public String getPaytmReceipt() {
		return paytmReceipt;
	}

	public void setPaytmReceipt(String paytmReceipt) {
		this.paytmReceipt = paytmReceipt;
	}

	public String getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}

	public String getCashSaleOrder() {
		return cashSaleOrder;
	}

	public void setCashSaleOrder(String cashSaleOrder) {
		this.cashSaleOrder = cashSaleOrder;
	}

	public String getCardSaleOrder() {
		return cardSaleOrder;
	}

	public void setCardSaleOrder(String cardSaleOrder) {
		this.cardSaleOrder = cardSaleOrder;
	}

	public String getCreditSaleOrder() {
		return creditSaleOrder;
	}

	public void setCreditSaleOrder(String creditSaleOrder) {
		this.creditSaleOrder = creditSaleOrder;
	}

	public String getEbsSaleOrder() {
		return ebsSaleOrder;
	}

	public void setEbsSaleOrder(String ebsSaleOrder) {
		this.ebsSaleOrder = ebsSaleOrder;
	}

	public String getGpaySaleOrder() {
		return gpaySaleOrder;
	}

	public void setGpaySaleOrder(String gpaySaleOrder) {
		this.gpaySaleOrder = gpaySaleOrder;
	}

	public String getYesBankSaleOrder() {
		return yesBankSaleOrder;
	}

	public void setYesBankSaleOrder(String yesBankSaleOrder) {
		this.yesBankSaleOrder = yesBankSaleOrder;
	}

	public String getPaytmSaleOrder() {
		return paytmSaleOrder;
	}

	public void setPaytmSaleOrder(String paytmSaleOrder) {
		this.paytmSaleOrder = paytmSaleOrder;
	}

	public String getCashVarience() {
		return cashVarience;
	}

	public void setCashVarience(String cashVarience) {
		this.cashVarience = cashVarience;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "DayEndReportStore [id=" + id + ", branchCode=" + branchCode + ", branchName=" + branchName
				+ ", posMaxBillNo=" + posMaxBillNo + ", posMinBillNo=" + posMinBillNo + ", posBillCount=" + posBillCount
				+ ", wholeSaleMaxBillNo=" + wholeSaleMaxBillNo + ", wholeSaleMinBillNo=" + wholeSaleMinBillNo
				+ ", wholeSaleBillCount=" + wholeSaleBillCount + ", pettyCash=" + pettyCash + ", closingCash="
				+ closingCash + ", totalCash=" + totalCash + ", physicalCash=" + physicalCash + ", date=" + date
				+ ", cashReceipt=" + cashReceipt + ", creditReceipt=" + creditReceipt + ", ebsReceipt=" + ebsReceipt
				+ ", gpayReceipt=" + gpayReceipt + ", swiggyReceipt=" + swiggyReceipt + ", yesBankReceipt="
				+ yesBankReceipt + ", cardSale=" + cardSale + ", zomatoReceipt=" + zomatoReceipt + ", paytmReceipt="
				+ paytmReceipt + ", totalSales=" + totalSales + ", cashSaleOrder=" + cashSaleOrder + ", cardSaleOrder="
				+ cardSaleOrder + ", creditSaleOrder=" + creditSaleOrder + ", ebsSaleOrder=" + ebsSaleOrder
				+ ", gpaySaleOrder=" + gpaySaleOrder + ", yesBankSaleOrder=" + yesBankSaleOrder + ", paytmSaleOrder="
				+ paytmSaleOrder + ", previousCashSaleOrder=" + previousCashSaleOrder + ", cash=" + cash
				+ ", neftSales=" + neftSales + ", cashVarience=" + cashVarience + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	

	


	

}
