package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class JobCardOutDtl implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String id;
	private String itemId;
	private Double qty;
	private String locationId;

	JobCardOutHdr jobCardOutHdr;
	private String  processInstanceId;
	private String taskId;
	
	
	
	public JobCardOutDtl() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.locationProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty(0.0);

	}
	@JsonIgnore
	private String itemName;
	
	@JsonIgnore
	private String location;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty locationProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	
	public JobCardOutHdr getJobCardOutHdr() {
		return jobCardOutHdr;
	}
	public void setJobCardOutHdr(JobCardOutHdr jobCardOutHdr) {
		this.jobCardOutHdr = jobCardOutHdr;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	
	
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public StringProperty getLocationProperty() {
		locationProperty.set(location);
		return locationProperty;
	}
	public void setLocationProperty(StringProperty locationProperty) {
		this.locationProperty = locationProperty;
	}
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "JobCardOutDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", locationId=" + locationId
				+ ", jobCardOutHdr=" + jobCardOutHdr + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", itemName=" + itemName + ", location=" + location + "]";
	}


	

}
