package com.maple.mapleclient.entity;

import java.io.Serializable;


import org.springframework.stereotype.Component;
public class SchOfferAttrInst implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;
	
	String schemeId ;
	String offerId ;
	String attributeName ; 
	String attributeValue ; 
	String attributeType ;
	
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	
	private CompanyMst companyMst;
	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSchemeId() {
		return schemeId;
	}

	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	public String getAttributeType() {
		return attributeType;
	}

	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SchOfferAttrInst [id=" + id + ", schemeId=" + schemeId + ", offerId=" + offerId + ", attributeName="
				+ attributeName + ", attributeValue=" + attributeValue + ", attributeType=" + attributeType
				+ ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", companyMst=" + companyMst + "]";
	}




	
 
}
