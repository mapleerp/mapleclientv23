package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductionDtlDtl {
	
	String id;
	String rawMaterialItemId;
	String itemName;
	Double qty;
	String unitId;
	String unitName;
	private String  processInstanceId;
	private String taskId;

	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	
	
	public ProductionDtlDtl() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty();
		
	}

	
	@JsonProperty
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRawMaterialItemId() {
		return rawMaterialItemId;
	}


	public void setRawMaterialItemId(String rawMaterialItemId) {
		this.rawMaterialItemId = rawMaterialItemId;
	}
ProductionDtl productionDtl;


	public ProductionDtl getProductionDtl() {
	return productionDtl;
}


public void setProductionDtl(ProductionDtl productionDtl) {
	this.productionDtl = productionDtl;
}



	@JsonProperty
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@JsonProperty
	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}
	@JsonProperty
	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	
	@JsonIgnore
	 public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
			return itemNameProperty;
		}
		

		public void setitemNameProperty(String itemName) {
			this.itemName = itemName;
		}
		

		@JsonIgnore
		 public DoubleProperty getqtyProperty() {
			qtyProperty.set(qty);
				return qtyProperty;
			}
			

			public void setqtyProperty(Double qty) {
				this.qty = qty;
			}
			@JsonIgnore
			 public StringProperty getunitNameProperty() {
				unitNameProperty.set(unitName);
					return unitNameProperty;
				}
				

				public void setunitNameProperty(String unitName) {
					this.unitName = unitName;
				}
			
			
			@JsonIgnore
	public String getUnitName() {
				return unitName;
			}


			public void setUnitName(String unitName) {
				this.unitName = unitName;
			}


			


			public String getProcessInstanceId() {
				return processInstanceId;
			}


			public void setProcessInstanceId(String processInstanceId) {
				this.processInstanceId = processInstanceId;
			}


			public String getTaskId() {
				return taskId;
			}


			public void setTaskId(String taskId) {
				this.taskId = taskId;
			}


			@Override
			public String toString() {
				return "ProductionDtlDtl [id=" + id + ", rawMaterialItemId=" + rawMaterialItemId + ", itemName="
						+ itemName + ", qty=" + qty + ", unitId=" + unitId + ", unitName=" + unitName
						+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", productionDtl="
						+ productionDtl + "]";
			}


	

}
