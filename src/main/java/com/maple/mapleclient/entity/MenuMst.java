package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class MenuMst {

	String id;
	
	String menuId;
	String parentId;
	String branchCode;
	
	
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	String menuName;
	
	
	@JsonIgnore
	String parentName;
	
	
	
	public MenuMst() {
		this.menuNameProperty = new SimpleStringProperty("");
		this.parentNameProperty = new SimpleStringProperty("");

	}

	@JsonIgnore
	StringProperty menuNameProperty;
	
	@JsonIgnore
	StringProperty parentNameProperty;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	



	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public StringProperty getMenuNameProperty() {
		menuNameProperty.set(menuName);
		return menuNameProperty;
	}

	public void setMenuNameProperty(StringProperty menuNameProperty) {
		this.menuNameProperty = menuNameProperty;
	}

	public StringProperty getParentNameProperty() {
		parentNameProperty.set(parentName);
		return parentNameProperty;
	}

	public void setParentNameProperty(StringProperty parentNameProperty) {
		this.parentNameProperty = parentNameProperty;
	}

	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "MenuMst [id=" + id + ", menuId=" + menuId + ", parentId=" + parentId + ", branchCode=" + branchCode
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", menuName=" + menuName + ", parentName=" + parentName + "]";
	}

	
	

	
}
