package com.maple.mapleclient.entity;

import java.io.Serializable;


import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class PurchaseOrderHdr implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;

	
	private String supplierId;
	 
	 
	private String finalSavedStatus;
	private String machineId;
	private String branchCode;
	private String deletedStatus;
 	private String narration;
	private String purchaseType;
	private String pONum;
	private Date poDate;
	private String supplierInvNo;
	private String voucherNumber;
	private Double invoiceTotal;
	private String currency;
	private Date supplierInvDate;
	private Date voucherDate;
	private Date tansactionEntryDate;
 
	private Integer enableBatchStatus;
	private String userId;
	private String voucherType;
	
	private String  processInstanceId;
	private String taskId;
	
	
	CompanyMst companyMst;
	
	@JsonIgnore
	String supplierName;

	@JsonIgnore
	StringProperty supplierNameProperty;

	@JsonIgnore
	StringProperty poNumberProperty;
	
	@JsonIgnore
	StringProperty narrationProperty;
	
	@JsonIgnore
	StringProperty voucherDateProperty;
	
	

	public PurchaseOrderHdr() {
		this.supplierNameProperty = new SimpleStringProperty();
		this.poNumberProperty = new SimpleStringProperty();
		this.narrationProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();


	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getSupplierId() {
		return supplierId;
	}


	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}


	public String getFinalSavedStatus() {
		return finalSavedStatus;
	}


	public void setFinalSavedStatus(String finalSavedStatus) {
		this.finalSavedStatus = finalSavedStatus;
	}


	public String getMachineId() {
		return machineId;
	}


	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getDeletedStatus() {
		return deletedStatus;
	}


	public void setDeletedStatus(String deletedStatus) {
		this.deletedStatus = deletedStatus;
	}


	public String getNarration() {
		return narration;
	}


	public void setNarration(String narration) {
		this.narration = narration;
	}


	public String getPurchaseType() {
		return purchaseType;
	}


	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}


	public String getpONum() {
		return pONum;
	}


	public void setpONum(String pONum) {
		this.pONum = pONum;
	}


	public Date getPoDate() {
		return poDate;
	}


	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}


	public String getSupplierInvNo() {
		return supplierInvNo;
	}


	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public Double getInvoiceTotal() {
		return invoiceTotal;
	}


	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public Date getSupplierInvDate() {
		return supplierInvDate;
	}


	public void setSupplierInvDate(Date supplierInvDate) {
		this.supplierInvDate = supplierInvDate;
	}


	public Date getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}


	public Date getTansactionEntryDate() {
		return tansactionEntryDate;
	}


	public void setTansactionEntryDate(Date tansactionEntryDate) {
		this.tansactionEntryDate = tansactionEntryDate;
	}


	public Integer getEnableBatchStatus() {
		return enableBatchStatus;
	}


	public void setEnableBatchStatus(Integer enableBatchStatus) {
		this.enableBatchStatus = enableBatchStatus;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getVoucherType() {
		return voucherType;
	}


	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getSupplierName() {
		return supplierName;
	}


	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}


	public StringProperty getSupplierNameProperty() {
		supplierNameProperty.set(supplierName);
		return supplierNameProperty;
	}


	public void setSupplierNameProperty(StringProperty supplierNameProperty) {
		
		
		this.supplierNameProperty = supplierNameProperty;
	}


	public StringProperty getPoNumberProperty() {
		poNumberProperty.set(pONum);
		return poNumberProperty;
	}


	public void setPoNumberProperty(StringProperty poNumberProperty) {
		this.poNumberProperty = poNumberProperty;
	}


	public StringProperty getNarrationProperty() {
		narrationProperty.set(narration);
		return narrationProperty;
	}


	public void setNarrationProperty(StringProperty narrationProperty) {
		this.narrationProperty = narrationProperty;
	}


	public StringProperty getVoucherDateProperty() {
		
		if(null != voucherDate)
		{
			voucherDateProperty.set(SystemSetting.SqlDateTostring(voucherDate));
		}
		return voucherDateProperty;
	}


	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}


	@Override
	public String toString() {
		return "PurchaseOrderHdr [id=" + id + ", supplierId=" + supplierId + ", finalSavedStatus=" + finalSavedStatus
				+ ", machineId=" + machineId + ", branchCode=" + branchCode + ", deletedStatus=" + deletedStatus
				+ ", narration=" + narration + ", purchaseType=" + purchaseType + ", pONum=" + pONum + ", poDate="
				+ poDate + ", supplierInvNo=" + supplierInvNo + ", voucherNumber=" + voucherNumber + ", invoiceTotal="
				+ invoiceTotal + ", currency=" + currency + ", supplierInvDate=" + supplierInvDate + ", voucherDate="
				+ voucherDate + ", tansactionEntryDate=" + tansactionEntryDate + ", enableBatchStatus="
				+ enableBatchStatus + ", userId=" + userId + ", voucherType=" + voucherType + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst + ", supplierName="
				+ supplierName + ", supplierNameProperty=" + supplierNameProperty + ", poNumberProperty="
				+ poNumberProperty + ", narrationProperty=" + narrationProperty + ", voucherDateProperty="
				+ voucherDateProperty + "]";
	}
	
	
	
	
}
