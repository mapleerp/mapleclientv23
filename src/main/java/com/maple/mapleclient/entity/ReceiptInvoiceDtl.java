package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ReceiptInvoiceDtl {
	
	String id;
	String receiptNumber;
	String invoiceNumber;
	Double recievedAmount;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty receiptNumberProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> receiptDate;
	
	@JsonIgnore
	private StringProperty invoiceNumberProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> invoiceDate;
	
	@JsonIgnore
	private DoubleProperty receivedAmountProperty;
	
 
	public ReceiptInvoiceDtl() {
		
		this.receiptNumberProperty = new SimpleStringProperty();
		this.receiptDate =new SimpleObjectProperty<LocalDate>(null);
		this.invoiceNumberProperty = new SimpleStringProperty();
		this.invoiceDate = new SimpleObjectProperty<LocalDate>(null);
		this.receivedAmountProperty = new SimpleDoubleProperty();
	}

	ReceiptHdr receiptHdr;
	AccountHeads accountHeads;
	SalesTransHdr salesTransHdr;
	
	@JsonProperty
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public ReceiptHdr getReceiptHdr() {
		return receiptHdr;
	}
	public void setReceiptHdr(ReceiptHdr receiptHdr) {
		this.receiptHdr = receiptHdr;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	public Double getRecievedAmount() {
		return recievedAmount;
	}
	public void setRecievedAmount(Double recievedAmount) {
		this.recievedAmount = recievedAmount;
	}
	

	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	
	@JsonProperty("receiptDate")
	public java.sql.Date getreceiptDate ( ) {
		if(null==this.receiptDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.receiptDate.get() );
		}
	}
	 
		  public void setreceiptDate (java.sql.Date receiptDateProperty) {
				if(null!=receiptDateProperty)
this.receiptDate.set(receiptDateProperty.toLocalDate());
}
	
	@JsonProperty("invoiceDate")
	public java.sql.Date getinvoiceDate ( ) {
		if(null==this.invoiceDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.invoiceDate.get() );
		}
	 
		
	}
	  public void setinvoiceDate (java.sql.Date invoiceDateProperty) {
			if(null!=invoiceDateProperty)
this.invoiceDate.set(invoiceDateProperty.toLocalDate());
}

	@JsonIgnore
	public StringProperty getreceiptNumberProperty() {
		receiptNumberProperty.set(receiptNumber);
		return receiptNumberProperty;
	}
	

	public void setreceiptNumberProperty(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	@JsonIgnore
	public StringProperty getinvoiceNumberProperty() {
		invoiceNumberProperty.set(invoiceNumber);
		return invoiceNumberProperty;
	}
	

	public void setinvoiceNumberProperty(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	@JsonIgnore
	public DoubleProperty getreceivedAmountProperty() {
		receivedAmountProperty.set(recievedAmount);
		return receivedAmountProperty;
	}
	

	public void setreceivedAmountProperty(Double recievedAmount) {
		this.recievedAmount = recievedAmount;
	}

	public ObjectProperty<LocalDate> getreceiptDateProperty( ) {
		return receiptDate;
	}
 
	public void setreceiptDateProperty(ObjectProperty<LocalDate> receiptDate) {
		this.receiptDate = receiptDate;
	}
	public ObjectProperty<LocalDate> getinvoiceDateProperty( ) {
		return invoiceDate;
	}
 
	public void setinvoiceDateProperty(ObjectProperty<LocalDate> invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}
	@Override
	public String toString() {
		return "ReceiptInvoiceDtl [id=" + id + ", receiptNumber=" + receiptNumber + ", invoiceNumber=" + invoiceNumber
				+ ", recievedAmount=" + recievedAmount + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", receiptNumberProperty=" + receiptNumberProperty + ", receiptDate=" + receiptDate
				+ ", invoiceNumberProperty=" + invoiceNumberProperty + ", invoiceDate=" + invoiceDate
				+ ", receivedAmountProperty=" + receivedAmountProperty + ", receiptHdr=" + receiptHdr
				+ ", accountHeads=" + accountHeads + ", salesTransHdr=" + salesTransHdr + "]";
	}
	
}
