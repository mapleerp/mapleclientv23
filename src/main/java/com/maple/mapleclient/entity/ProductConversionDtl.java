package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductConversionDtl {

	 String id;
	 String fromItem;
	 Double fromQty;
	 String toItem;
	 Double toQty;
	 String fromitemName;
	 String toItemName;
	 String batchCode;
	 private String  processInstanceId;
		private String taskId;
	 ProductConversionMst productConversionMst;
	 
	 public ProductConversionMst getProductConversionMst() {
		return productConversionMst;
	}

	public void setProductConversionMst(ProductConversionMst productConversionMst) {
		this.productConversionMst = productConversionMst;
	}

	@JsonIgnore
		private StringProperty fromItemProperty;
	 
	 @JsonIgnore
	 private DoubleProperty fromQtyProperty;
	 
	 @JsonIgnore
	 private StringProperty toItemProperty;
	 
	 @JsonIgnore
	 private DoubleProperty toQtyProperty;
	 
	 @JsonIgnore
	 private StringProperty batchCodeProperty;
	 
	 
	 

	public String getFromitemName() {
		return fromitemName;
	}

	public void setFromitemName(String fromitemName) {
		this.fromitemName = fromitemName;
	}

	public String getToItemName() {
		return toItemName;
	}

	public void setToItemName(String toItemName) {
		this.toItemName = toItemName;
	}

	public ProductConversionDtl() {
		this.fromItemProperty = new SimpleStringProperty();
		this.fromQtyProperty = new SimpleDoubleProperty();
		this.toItemProperty = new SimpleStringProperty();
		this.toQtyProperty = new SimpleDoubleProperty();
		this.batchCodeProperty = new SimpleStringProperty();
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFromItem() {
		return fromItem;
	}

	public void setFromItem(String fromItem) {
		this.fromItem = fromItem;
	}

	public Double getFromQty() {
		return fromQty;
	}

	public void setFromQty(Double fromQty) {
		this.fromQty = fromQty;
	}

	public String getToItem() {
		return toItem;
	}

	public void setToItem(String toItem) {
		this.toItem = toItem;
	}

	public Double getToQty() {
		return toQty;
	}

	public void setToQty(Double toQty) {
		this.toQty = toQty;
	}
	 
	 	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

		@JsonIgnore
	 	public StringProperty getfromItemProperty() {
		 fromItemProperty.set(fromitemName);
			return fromItemProperty;
		}
		public void setfromItemProperty(String fromitemName) {
			this.fromitemName = fromitemName;
		}
		public DoubleProperty getfromQtyProperty() {
			 fromQtyProperty.set(fromQty);
				return fromQtyProperty;
		}
		public void setfromQtyProperty(Double fromQty) {
				this.fromQty = fromQty;
		}
		@JsonIgnore
	 	public StringProperty getbatchCodeProperty() {
			batchCodeProperty.set(batchCode);
			return batchCodeProperty;
		}
		public void setbatchCodeProperty(String batchCode) {
			this.batchCode = batchCode;
		}
		@JsonIgnore
	 	public StringProperty gettoItemProperty() {
			toItemProperty.set(toItemName);
			return toItemProperty;
		}
		public void settoItemProperty(String toItemName) {
			this.toItemName = toItemName;
		}
		public DoubleProperty gettoQtyProperty() {
			toQtyProperty.set(toQty);
				return toQtyProperty;
		}
		public void settoQtyProperty(Double toQty) {
				this.toQty = toQty;
		}

		

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "ProductConversionDtl [id=" + id + ", fromItem=" + fromItem + ", fromQty=" + fromQty + ", toItem="
					+ toItem + ", toQty=" + toQty + ", fromitemName=" + fromitemName + ", toItemName=" + toItemName
					+ ", batchCode=" + batchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
					+ ", productConversionMst=" + productConversionMst + "]";
		}
	
}
