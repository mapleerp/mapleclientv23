package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BarcodeBatchMst implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;

	private String barcode;
	private String batchCode;
	
	private String branchCode;

	CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	@Override
	public String toString() {
		return "BarcodeBatchMst [id=" + id + ", barcode=" + barcode + ", batchCode=" + batchCode + ", branchCode="
				+ branchCode + ", companyMst=" + companyMst + "]";
	}

	
	

}
