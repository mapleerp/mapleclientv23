package com.maple.mapleclient.entity;

 
import java.io.Serializable;
import java.util.Date;

 
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;
 

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LocalCustomerMst implements Serializable  {
	private static final long serialVersionUID = 1L;
	
 
     
	String id;
	
	 String oldId;
	 
	 
	 
	String localcustomerName;
	String address;
	String phoneNo;
	String phoneNO2;
	String customerId;

	String addressLine1;
	String addressLine2;
	String landMark;
	String companyMstId;
	
 
	
	Date dateOfBirth;
	Date weddingDate;
	
	String branchCode;
	private String  processInstanceId;
	private String taskId;

	
	 
	private CompanyMst companyMst;
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private StringProperty addressProperty ;
	
	@JsonIgnore
	private StringProperty phoneNoProperty ;
	

	@JsonIgnore
	private StringProperty addressLine1Property;
	@JsonIgnore
	private StringProperty landMarkProperty;
	
	@JsonIgnore
	private StringProperty dateofBirthProperty;
	
	@JsonIgnore
	private StringProperty weddingDateProperty;
	
	
	public LocalCustomerMst() {
		this.customerNameProperty= new SimpleStringProperty("");
		this.addressProperty= new SimpleStringProperty("");
		this.phoneNoProperty= new SimpleStringProperty("");
		this.addressLine1Property= new SimpleStringProperty("");
		this.landMarkProperty= new SimpleStringProperty("");
		this.dateofBirthProperty =  new SimpleStringProperty("");
		this.weddingDateProperty = new SimpleStringProperty("");
	}



	public String getCompanyMstId() {
		return companyMstId;
	}



	public void setCompanyMstId(String companyMstId) {
		this.companyMstId = companyMstId;
	}



	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getLocalcustomerName() {
		return localcustomerName;
	}


	public void setLocalcustomerName(String localcustomerName) {
		this.localcustomerName = localcustomerName;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getPhoneNo() {
		return phoneNo;
	}


	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}


	public String getPhoneNO2() {
		return phoneNO2;
	}


	public void setPhoneNO2(String phoneNO2) {
		this.phoneNO2 = phoneNO2;
	}


	public String getCustomerId() {
		return customerId;
	}


	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}


	public String getAddressLine1() {
		return addressLine1;
	}


	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	public String getAddressLine2() {
		return addressLine2;
	}


	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	public String getLandMark() {
		return landMark;
	}


	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}


	 

	public StringProperty getDateofBirthProperty() {
		if(null != dateOfBirth)
		{
			dateofBirthProperty.set(SystemSetting.UtilDateToString(dateOfBirth, "yyyy-MM-dd"));
		}
		return dateofBirthProperty;
	}



	public void setDateofBirthProperty(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}



	public StringProperty getWeddingDateProperty() {
		if(null != weddingDate)
		{
			weddingDateProperty.set(SystemSetting.UtilDateToString(weddingDate, "yyyy-MM-dd"));
		}
		return weddingDateProperty;
	}



	public void setWeddingDateProperty(Date weddingDate) {
		this.weddingDate = weddingDate;
	}



	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(localcustomerName);
		return customerNameProperty;
	}


	@JsonIgnore
	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}


	@JsonIgnore
	public StringProperty getAddressProperty() {
		addressProperty.set(address);
		return addressProperty;
	}


	@JsonIgnore
	public void setAddressProperty(StringProperty addressProperty) {
		this.addressProperty = addressProperty;
	}


	@JsonIgnore
	public StringProperty getPhoneNoProperty() {
		phoneNoProperty.set(phoneNo);
		return phoneNoProperty;
	}


	@JsonIgnore
	public void setPhoneNoProperty(StringProperty phoneNoProperty) {
		this.phoneNoProperty = phoneNoProperty;
	}


	@JsonIgnore
	public StringProperty getAddressLine1Property() {
		addressLine1Property.set(addressLine1);
		return addressLine1Property;
	}


	@JsonIgnore
	public void setAddressLine1Property(StringProperty addressLine1Property) {
		this.addressLine1Property = addressLine1Property;
	}


	@JsonIgnore
	public StringProperty getLandMarkProperty() {
		landMarkProperty.set(landMark);
		return landMarkProperty;
	}


	@JsonIgnore
	public void setLandMarkProperty(StringProperty landMarkProperty) {
		this.landMarkProperty = landMarkProperty;
	}



	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}



	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	




	public Date getWeddingDate() {
		return weddingDate;
	}



	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}






	public String getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "LocalCustomerMst [id=" + id + ", localcustomerName=" + localcustomerName + ", address=" + address
				+ ", phoneNo=" + phoneNo + ", phoneNO2=" + phoneNO2 + ", customerId=" + customerId + ", addressLine1="
				+ addressLine1 + ", addressLine2=" + addressLine2 + ", landMark=" + landMark + ", companyMstId="
				+ companyMstId + ", dateOfBirth=" + dateOfBirth + ", weddingDate=" + weddingDate + ", branchCode="
				+ branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	


	
}
