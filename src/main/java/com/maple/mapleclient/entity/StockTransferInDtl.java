package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

 

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockTransferInDtl {

String id;
	
	
	String batch;
	Double qty;
	Double rate;
	String itemCode;
	Double taxRate;
	Double amount;
	String unitId;
	String itemName;
//	String itemId;
	
	ItemMst itemId;
	
	
	String barcode; 
	Double mrp;
	private String  processInstanceId;
	private String taskId;
	
	String unitName;
	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	@JsonIgnore
	private StringProperty batchCodeProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	@JsonIgnore 
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDate;
	
	@JsonIgnore
	private StringProperty unitIdProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;

	@JsonIgnore
	private StringProperty barcodeProperty;
	
	@JsonIgnore
	private DoubleProperty mrpProperty;
	
	StockTransferInHdr stockTransferInHdr;

	public StockTransferInDtl() {
	

		this.batchCodeProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.rateProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.expiryDate =new SimpleObjectProperty<LocalDate>(null);
		this.unitIdProperty = new SimpleStringProperty();
		this.itemNameProperty =  new SimpleStringProperty();
		this.barcodeProperty =  new SimpleStringProperty();
		this.mrpProperty = new SimpleDoubleProperty();
		this.unitNameProperty = new SimpleStringProperty();
	}
	@JsonProperty("expiryDate")
	public java.sql.Date getExpiryDate ( ) {
		if(null==this.expiryDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.expiryDate.get() );
		}
	 
		
	}
	
   public void setExpiryDate (java.sql.Date expiryDateProperty) {
						if(null!=expiryDateProperty)
		this.expiryDate.set(expiryDateProperty.toLocalDate());
	}
 @JsonProperty
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	


	public Double getQty() {
		return qty;
	}


	public void setQty(Double qty) {
		this.qty = qty;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}


	


	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@JsonIgnore
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
@JsonProperty
	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public ObjectProperty<LocalDate> getExpiryDateProperty( ) {
		return expiryDate;
	}
 
	public void setExpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {
		this.expiryDate = expiryDate;
	}
	

	public String getUnitId() {
		return unitId;
	}


	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}


	public StockTransferInHdr getStockTransferInHdr() {
		return stockTransferInHdr;
	}


	public void setStockTransferInHdr(StockTransferInHdr stockTransferInHdr) {
		this.stockTransferInHdr = stockTransferInHdr;
	}
	@JsonIgnore
	public StringProperty getbarcodeProperty() {
		barcodeProperty.set(barcode);
		return barcodeProperty;
	}
	

	public void setbarcodeProperty(String barcode) {
		this.barcode = barcode;
	}
	
	@JsonIgnore
	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	

	public void setunitNameProperty(String unitName) {
		this.unitName = unitName;
	}
	
	@JsonIgnore
	public StringProperty getunitIdProperty() {
		unitIdProperty.set(unitId);
		return unitIdProperty;
	}
	

	public void setunitIdProperty(String unitId) {
		this.unitId = unitId;
	}
	
@JsonIgnore
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}

	@JsonIgnore
	
	public StringProperty getBatchCodeProperty() {
		batchCodeProperty.set(batch);
		return batchCodeProperty;
	}
	public void setBatchCodeProperty(String batch) {
		this.batch = batch;
	}
	@JsonIgnore
	
	public DoubleProperty getmrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}
	public void setmrpProperty(Double mrp) {
		this.mrp = mrp;
	}
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(Double qty) {
		this.qty = qty;
	}
	@JsonIgnore
	public DoubleProperty getRateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}
	public void setRateProperty(Double rate) {
		this.rate = rate;
	}
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(Double amount) {
		this.amount = amount;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public ItemMst getItemId() {
		return itemId;
	}

	public void setItemId(ItemMst itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return "StockTransferInDtl [id=" + id + ", batch=" + batch + ", qty=" + qty + ", rate=" + rate + ", itemCode="
				+ itemCode + ", taxRate=" + taxRate + ", amount=" + amount + ", unitId=" + unitId + ", itemName="
				+ itemName + ", itemId=" + itemId + ", barcode=" + barcode + ", mrp=" + mrp + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", unitName=" + unitName + ", batchCodeProperty="
				+ batchCodeProperty + ", qtyProperty=" + qtyProperty + ", rateProperty=" + rateProperty
				+ ", amountProperty=" + amountProperty + ", expiryDate=" + expiryDate + ", unitIdProperty="
				+ unitIdProperty + ", unitNameProperty=" + unitNameProperty + ", itemNameProperty=" + itemNameProperty
				+ ", barcodeProperty=" + barcodeProperty + ", mrpProperty=" + mrpProperty + ", stockTransferInHdr="
				+ stockTransferInHdr + "]";
	}



	

	
	
	
	
}
