package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InsuranceCompanyDtl {
	String id;
	String policyType;
	String percentageCoverage;
	InsuranceCompanyMst insuranceCompanyMst;
	
	public InsuranceCompanyMst getInsuranceCompanyMst() {
		return insuranceCompanyMst;
	}

	public void setInsuranceCompanyMst(InsuranceCompanyMst insuranceCompanyMst) {
		this.insuranceCompanyMst = insuranceCompanyMst;
	}
	
	@JsonIgnore
	String insuranceCompanyName;
	@JsonIgnore
	String contactPerson;
	
	@JsonIgnore
	String phoneNumber;
	@JsonIgnore
	StringProperty contactPersonProperty;
	@JsonIgnore
	StringProperty phoneNumberProperty;
	@JsonIgnore
	StringProperty insuranceCompanyNameProperty;
	
	@JsonIgnore
	StringProperty policyTypeProperty;
	
	@JsonIgnore
	StringProperty percentageCoverageProperty;
	
	
	
	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public StringProperty getContactPersonProperty() {
		contactPersonProperty.set(contactPerson);
		return contactPersonProperty;
	}

	public void setContactPersonProperty(StringProperty contactPersonProperty) {
		this.contactPersonProperty = contactPersonProperty;
	}

	public StringProperty getPhoneNumberProperty() {
		phoneNumberProperty.set(phoneNumber);
		return phoneNumberProperty;
	}

	public void setPhoneNumberProperty(StringProperty phoneNumberProperty) {
		this.phoneNumberProperty = phoneNumberProperty;
	}

	public InsuranceCompanyDtl() {
	this.contactPersonProperty=new SimpleStringProperty();
	this.policyTypeProperty = new SimpleStringProperty();
	this.percentageCoverageProperty = new SimpleStringProperty();
	this.phoneNumberProperty = new SimpleStringProperty();
	this.insuranceCompanyNameProperty = new SimpleStringProperty();
	
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getPercentageCoverage() {
		return percentageCoverage;
	}

	public void setPercentageCoverage(String percentageCoverage) {
		this.percentageCoverage = percentageCoverage;
	}

	public StringProperty getPolicyTypeProperty() {
		policyTypeProperty.set(policyType);
		return policyTypeProperty;
	}

	public void setPolicyTypeProperty(StringProperty policyTypeProperty) {
		this.policyTypeProperty = policyTypeProperty;
	}

	public StringProperty getPercentageCoverageProperty() {
		percentageCoverageProperty.set(percentageCoverage);
		return percentageCoverageProperty;
	}

	public void setPercentageCoverageProperty(StringProperty percentageCoverageProperty) {
		this.percentageCoverageProperty = percentageCoverageProperty;
	}
	
	

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	public StringProperty getInsuranceCompanyNameProperty() {
		
		insuranceCompanyNameProperty.set(insuranceCompanyName);
		return insuranceCompanyNameProperty;
	}

	public void setInsuranceCompanyNameProperty(StringProperty insuranceCompanyNameProperty) {
		this.insuranceCompanyNameProperty = insuranceCompanyNameProperty;
	}

	@Override
	public String toString() {
		return "InsuranceCompanyDtl [id=" + id + ", policyType=" + policyType + ", percentageCoverage="
				+ percentageCoverage + ", insuranceCompanyMst=" + insuranceCompanyMst + ", insuranceCompanyName="
				+ insuranceCompanyName + ", contactPerson=" + contactPerson + ", phoneNumber=" + phoneNumber
				+ ", contactPersonProperty=" + contactPersonProperty + ", phoneNumberProperty=" + phoneNumberProperty
				+ ", insuranceCompanyNameProperty=" + insuranceCompanyNameProperty + ", policyTypeProperty="
				+ policyTypeProperty + ", percentageCoverageProperty=" + percentageCoverageProperty
				+ ", getInsuranceCompanyMst()=" + getInsuranceCompanyMst() + ", getContactPerson()="
				+ getContactPerson() + ", getPhoneNumber()=" + getPhoneNumber() + ", getContactPersonProperty()="
				+ getContactPersonProperty() + ", getPhoneNumberProperty()=" + getPhoneNumberProperty() + ", getId()="
				+ getId() + ", getPolicyType()=" + getPolicyType() + ", getPercentageCoverage()="
				+ getPercentageCoverage() + ", getPolicyTypeProperty()=" + getPolicyTypeProperty()
				+ ", getPercentageCoverageProperty()=" + getPercentageCoverageProperty()
				+ ", getInsuranceCompanyName()=" + getInsuranceCompanyName() + ", getInsuranceCompanyNameProperty()="
				+ getInsuranceCompanyNameProperty() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	
}
