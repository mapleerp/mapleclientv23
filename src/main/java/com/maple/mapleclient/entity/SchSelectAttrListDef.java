package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;
public class SchSelectAttrListDef implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	String selectionId ;
	String attribName ;
	String attrib_type ;
	String branchCode ;
	private String  processInstanceId;
	private String taskId;

	private CompanyMst companyMst;
	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSelectionId() {
		return selectionId;
	}

	public void setSelectionId(String selectionId) {
		this.selectionId = selectionId;
	}

	public String getAttribName() {
		return attribName;
	}

	public void setAttribName(String attribName) {
		this.attribName = attribName;
	}

	public String getAttrib_type() {
		return attrib_type;
	}

	public void setAttrib_type(String attrib_type) {
		this.attrib_type = attrib_type;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SchSelectAttrListDef [id=" + id + ", selectionId=" + selectionId + ", attribName=" + attribName
				+ ", attrib_type=" + attrib_type + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst + "]";
	}

	

}
