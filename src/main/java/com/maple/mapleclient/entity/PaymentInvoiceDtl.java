package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PaymentInvoiceDtl {

	String id;
	String voucherNumber;
	String invoiceNumber;
	Double paidAmount;

//	ReceiptHdr receiptHdr;
	AccountHeads accountHeads;
	PaymentHdr PaymentHdr;
	PurchaseHdr purchaseHdr;
	private String  processInstanceId;
	private String taskId;
	
	public PaymentInvoiceDtl() {
	
		this.voucherNumberProperty = new SimpleStringProperty();
		this.invoiceNumberProperty = new SimpleStringProperty();
		this.paidAmountProperty = new SimpleDoubleProperty();
	}

	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonProperty("voucherDate")
	private java.util.Date voucherDate;
	
	
	@JsonIgnore
	private StringProperty invoiceNumberProperty;

	
	@JsonIgnore
	private DoubleProperty paidAmountProperty;

	@JsonProperty("voucherDate")
	


	public java.util.Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(java.util.Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}



	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}


	

	public PaymentHdr getPaymentHdr() {
		return PaymentHdr;
	}

	public void setPaymentHdr(PaymentHdr paymentHdr) {
		PaymentHdr = paymentHdr;
	}

	
	public StringProperty getInvoiceNumberProperty() {
		invoiceNumberProperty.set(invoiceNumber);
		return invoiceNumberProperty;
	}

	public void setInvoiceNumberProperty(StringProperty invoiceNumberProperty) {
		this.invoiceNumberProperty = invoiceNumberProperty;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}

	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}

	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}

	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}

	public DoubleProperty getPaidAmountProperty() {
		paidAmountProperty.set(paidAmount);
		return paidAmountProperty;
	}

	public void setPaidAmountProperty(DoubleProperty paidAmountProperty) {
		this.paidAmountProperty = paidAmountProperty;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	@Override
	public String toString() {
		return "PaymentInvoiceDtl [id=" + id + ", voucherNumber=" + voucherNumber + ", invoiceNumber=" + invoiceNumber
				+ ", paidAmount=" + paidAmount + ", accountHeads=" + accountHeads + ", PaymentHdr=" + PaymentHdr
				+ ", purchaseHdr=" + purchaseHdr + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", voucherDate=" + voucherDate
				+ ", invoiceNumberProperty=" + invoiceNumberProperty + ", paidAmountProperty=" + paidAmountProperty
				+ "]";
	}

	

	

	
	

}
