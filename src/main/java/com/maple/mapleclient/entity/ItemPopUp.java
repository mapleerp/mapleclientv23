package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/*
 * This class is used from Item pop-up only. Not persisted in database
 */
public class ItemPopUp {

	StringProperty barcode;
	StringProperty unitname;
	StringProperty  itemName;
	DoubleProperty cess;
	DoubleProperty tax ;
	StringProperty unitId;
	StringProperty itemId;
	StringProperty itemcode;
	DoubleProperty mrp;
	private String  processInstanceId;
	private String taskId;
	KitDefinitionMst kitDefinitionMst;
	
	
	public ItemPopUp() {
		this.barcode = new SimpleStringProperty("");
		this.unitname = new SimpleStringProperty("");
		this.itemName = new SimpleStringProperty("");
		this.cess = new SimpleDoubleProperty(0);
		this.tax = new SimpleDoubleProperty(0.0);
		this.unitId = new SimpleStringProperty("");
		this.itemId = new SimpleStringProperty("");
		this.itemcode = new SimpleStringProperty("");
		this.mrp = new SimpleDoubleProperty(0.0);
		this.kitNameProperty =new SimpleStringProperty("");
		 
		
	}
	@JsonIgnore
	String kitName;
	@JsonIgnore
	StringProperty kitNameProperty;
	
	@JsonIgnore
	public String getKitName() {
		return kitName;
	}


	public void setKitName(String kitName) {
		this.kitName = kitName;
	}

	@JsonIgnore
	public StringProperty getKitNameProperty() {
		if(null!=kitName) {
			kitNameProperty.set(kitName);
		}
		return kitNameProperty;
	}


	public void setKitNameProperty(StringProperty kitNameProperty) {
		this.kitNameProperty = kitNameProperty;
	}


	public KitDefinitionMst getKitDefinitionMst() {
		return kitDefinitionMst;
	}


	public void setKitDefinitionMst(KitDefinitionMst kitDefinitionMst) {
		this.kitDefinitionMst = kitDefinitionMst;
	}


	public StringProperty getBarcodeProperty() {
		return barcode;
	}
	public void setBarcodeProperty(StringProperty barcode) {
		this.barcode = barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode.set(barcode);
	}
	
	
	public String  getBarcode () {
		return barcode.get();
	}
	
	
	public StringProperty getUnitnameProperty() {
		return unitname;
	}
	
	public String  getUnitname () {
		return unitname.get();
	}
	
	
	public void setUnitnameProperty(StringProperty unitname) {
		this.unitname = unitname;
	}
	public void setUnitname (String  unitname) {
		this.unitname.set(unitname);
	}
	
	
	public StringProperty getItemNameProperty() {
		return itemName;
	}
	
	
	public String  getItemName () {
		return itemName.get();
	}
	public void setItemNameProperty(StringProperty itemName) {
		this.itemName = itemName;
	}
	public void setItemName (String  itemName) {
		this.itemName.set(itemName);
	}
	
	public DoubleProperty getCessProperty() {
		return cess;
	}
	public void setCessProperty(DoubleProperty cess) {
		this.cess = cess;
	}
	
	

	public Double  getCess () {
		return cess.get();
	}
	public void setCess (Double  cess) {
		this.cess.set(cess);
	}
	
	
	
	public DoubleProperty getTaxProperty() {
		return tax;
	}
	public void setTaxProperty(DoubleProperty tax) {
		this.tax = tax;
	}
	
	
	
	public Double  getTax () {
		return tax.get();
	}
	public void setTax (Double  tax) {
		this.tax.set(tax);
	}
	
	
	
	public StringProperty getUnitIdProperty() {
		return unitId;
	}
	public void setUnitIdProperty(StringProperty unitId) {
		this.unitId = unitId;
	}
	
	
	public String  getUnitId () {
		return unitId.get();
	}
	public void setUnitId (String unitId) {
		this.unitId.set(unitId);
	}
	
	
	public StringProperty getItemIdProperty() {
		return itemId;
	}
	public void setItemIdProperty(StringProperty itemId) {
		this.itemId = itemId;
	}
	 
	
	
	public String  getItemId () {
		return itemId.get();
	}
	public void setItemId (String  itemId) {
		this.itemId.set(itemId);
	}
	 
	
 
	public StringProperty getItemcodeProperty() {
		return itemcode;
	}
	public void setItemcodeProperty(StringProperty itemcode) {
		this.itemcode = itemcode;
	}
	
	 

	public String  getItemcode () {
		return itemcode.get();
	}
	public void setItemcode (String itemcode) {
		this.itemcode.set( itemcode);
	}
	public DoubleProperty getMrpProperty() {
		return mrp;
	}
	public void setMrpProperty(DoubleProperty mrp) {
		this.mrp = mrp;
	}
 
	public Double  getMrp () {
		return mrp.get();
	}
	public void setMrp (Double  mrp) {
		this.mrp.set( mrp);
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "ItemPopUp [processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


}
