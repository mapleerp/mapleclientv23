package com.maple.mapleclient.entity;

public class CardSalesDtl {


  private String id;
		

	private CompanyMst companyMst;
	String branchCode;

	private SalesTransHdr salesTransHdrId;
	
	
	String receiptMode;
	Double receiptAmount;
	java.util.Date rereceiptDate;
	
	String accountId;
	String salesReceiptId;
	String userId;
	
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public SalesTransHdr getSalesTransHdrId() {
		return salesTransHdrId;
	}
	public void setSalesTransHdrId(SalesTransHdr salesTransHdrId) {
		this.salesTransHdrId = salesTransHdrId;
	}
	
	public Double getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	public java.util.Date getRereceiptDate() {
		return rereceiptDate;
	}
	public void setRereceiptDate(java.util.Date rereceiptDate) {
		this.rereceiptDate = rereceiptDate;
	}
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSalesReceiptId() {
		return salesReceiptId;
	}
	public void setSalesReceiptId(String salesReceiptId) {
		this.salesReceiptId = salesReceiptId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "CardSalesDtl [id=" + id + ", companyMst=" + companyMst + ", branchCode=" + branchCode
				+ ", salesTransHdrId=" + salesTransHdrId + ", receiptMode=" + receiptMode + ", receiptAmount="
				+ receiptAmount + ", rereceiptDate=" + rereceiptDate + ", accountId=" + accountId + ", salesReceiptId="
				+ salesReceiptId + ", userId=" + userId + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
	
	
	
}
