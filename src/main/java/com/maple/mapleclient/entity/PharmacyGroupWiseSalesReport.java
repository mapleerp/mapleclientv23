package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyGroupWiseSalesReport {

	String itemName;
	Double amount;
	Double tax;
	String batchCode;
	Double qty;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
    private	DoubleProperty  amountProperty;
	
	@JsonIgnore
	 private DoubleProperty  taxProperty;
	
	
	
	@JsonIgnore
	 private StringProperty batchCodeProperty;

	@JsonIgnore
	 private DoubleProperty qtyProperty;
	
	public PharmacyGroupWiseSalesReport() {
		
		
		this.itemNameProperty= new SimpleStringProperty("");
		this.amountProperty=new SimpleDoubleProperty();
		this.taxProperty=new SimpleDoubleProperty();
		this.batchCodeProperty= new SimpleStringProperty("");
		this.qtyProperty= new SimpleDoubleProperty();
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	
	@JsonIgnore
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set( amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	@JsonIgnore
	public DoubleProperty getTaxProperty() {
		taxProperty.set(tax);
		return taxProperty;
	}
	public void setTaxProperty(DoubleProperty taxProperty) {
		this.taxProperty = taxProperty;
	}
	@JsonIgnore 
	public StringProperty getBatchCodeProperty() {
		
		batchCodeProperty.set(batchCode);
		return batchCodeProperty;
	}
	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PharmacyGroupWiseSalesReport [itemName=" + itemName + ", amount=" + amount + ", tax=" + tax
				+ ", batchCode=" + batchCode + ", qty=" + qty + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
	
}
