package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class LanguageMst {
	

   private String id;
	
	private String languageName;
	private String languageFontType;
	private String  processInstanceId;
	private String taskId;
	
	CompanyMst companyMst;
	public LanguageMst() {
		this.languageFontTypeProperty = new SimpleStringProperty("");
		this.languageNameProperty = new SimpleStringProperty("");
	}
	@JsonIgnore
	private StringProperty languageNameProperty;
	@JsonIgnore
	private StringProperty languageFontTypeProperty;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getLanguageFontType() {
		return languageFontType;
	}
	public void setLanguageFontType(String languageFontType) {
		this.languageFontType = languageFontType;
	}
	
	
	public StringProperty getLanguageNameProperty() {
		languageNameProperty.set(languageName);
		return languageNameProperty;
	}
	public void setLanguageNameProperty(StringProperty languageNameProperty) {
		this.languageNameProperty = languageNameProperty;
	}
	public StringProperty getLanguageFontTypeProperty() {
		languageFontTypeProperty.set(languageFontType);
		return languageFontTypeProperty;
	}
	public void setLanguageFontTypeProperty(StringProperty languageFontTypeProperty) {
		this.languageFontTypeProperty = languageFontTypeProperty;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "LanguageMst [id=" + id + ", languageName=" + languageName + ", languageFontType=" + languageFontType
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ "]";
	}
	
	
	
	
	

}
