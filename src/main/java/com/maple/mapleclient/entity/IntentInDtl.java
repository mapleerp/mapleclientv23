package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class IntentInDtl {

	 String id;
	 String itemId;
	 String UnitId;
	 Double qty;
	 Double rate;
	 String itemName;
	 String unitName;
	 String status;
	 Double allocatedQty;
	 Double balanceQty;
	 String branchCode;
	 IntentHdr intentInHdr;
	 
	 String voucherDate;
	 
	 private String  processInstanceId;
	 private String taskId;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getAllocatedQty() {
		return allocatedQty;
	}
	public void setAllocatedQty(Double allocatedQty) {
		this.allocatedQty = allocatedQty;
	}
	public IntentHdr getIntentInHdr() {
		return intentInHdr;
	}
	
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	public void setIntentInHdr(IntentHdr intentInHdr) {
		this.intentInHdr = intentInHdr;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitId() {
		return UnitId;
	}
	public void setUnitId(String unitId) {
		UnitId = unitId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	 
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private StringProperty branchCodeProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty balanceProperty;
	
	
	
	public IntentInDtl() {
		this.itemNameProperty = new SimpleStringProperty();
		this.unitNameProperty = new SimpleStringProperty();
		this.rateProperty = new SimpleDoubleProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.branchCodeProperty = new SimpleStringProperty("");
		this.voucherDateProperty = new SimpleStringProperty();
		this.balanceProperty = new SimpleDoubleProperty(0.0);
		this.branchCode= "";
		this.balanceQty = 0.0;
	}
	
	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}

	@JsonIgnore
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(voucherDate);
		return voucherDateProperty;
	}

	public void setvoucherDateProperty(String voucherDate) {
		this.voucherDate = voucherDate;
	}

	
	
	@JsonIgnore
	public StringProperty getunitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}

	
	
	public Double getBalanceQty() {
		return balanceQty;
	}
	public void setBalanceQty(Double balanceQty) {
		this.balanceQty = balanceQty;
	}
	public StringProperty getbranchCodeProperty() {
		branchCodeProperty.set(branchCode);
		return branchCodeProperty;
	}

	public void setbranchCodeProperty(String branchCode) {
		this.branchCode = branchCode;
	}

	
	public void setunitNameProperty(String unitName) {
		this.unitName = unitName;
	}
	@JsonIgnore
	public DoubleProperty getrateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}

	public void setrateProperty(Double rate) {
		this.rate = rate;
	}
	
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setQtyProperty(Double qty) {
		this.qty = qty;
	}
	
	@JsonIgnore
	public DoubleProperty getbalanceProperty() {
		balanceProperty.set(balanceQty);
		return balanceProperty;
	}

	public void setbalanceProperty(Double balanceQty) {
		this.balanceQty = balanceQty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "IntentInDtl [id=" + id + ", itemId=" + itemId + ", UnitId=" + UnitId + ", qty=" + qty + ", rate=" + rate
				+ ", itemName=" + itemName + ", unitName=" + unitName + ", status=" + status + ", allocatedQty="
				+ allocatedQty + ", balanceQty=" + balanceQty + ", branchCode=" + branchCode + ", intentInHdr="
				+ intentInHdr + ", voucherDate=" + voucherDate + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
}
