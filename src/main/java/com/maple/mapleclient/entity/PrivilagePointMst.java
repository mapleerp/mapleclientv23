package com.maple.mapleclient.entity;


import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
public class PrivilagePointMst implements Serializable{
	private static final long serialVersionUID = 1L;

	String id;
	Double pointsEarned;
	Double pointsRedeemed;
	Date earnedDate;
	Date redeemedDate;
	AccountHeads accountHeads;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Double getPointsEarned() {
		return pointsEarned;
	}
	public void setPointsEarned(Double pointsEarned) {
		this.pointsEarned = pointsEarned;
	}
	public Double getPointsRedeemed() {
		return pointsRedeemed;
	}
	public void setPointsRedeemed(Double pointsRedeemed) {
		this.pointsRedeemed = pointsRedeemed;
	}
	public Date getEarnedDate() {
		return earnedDate;
	}
	public void setEarnedDate(Date earnedDate) {
		this.earnedDate = earnedDate;
	}
	public Date getRedeemedDate() {
		return redeemedDate;
	}
	public void setRedeemedDate(Date redeemedDate) {
		this.redeemedDate = redeemedDate;
	}
	
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public AccountHeads getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}
	@Override
	public String toString() {
		return "PrivilagePointMst [id=" + id + ", pointsEarned=" + pointsEarned + ", pointsRedeemed=" + pointsRedeemed
				+ ", earnedDate=" + earnedDate + ", redeemedDate=" + redeemedDate + ", accountHeads=" + accountHeads
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	
	
	
}
