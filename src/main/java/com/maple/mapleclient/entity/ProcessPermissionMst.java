package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProcessPermissionMst {

	String id;
	String userId;
	String processId;
	String userName;
	String processName;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty userNameProperty;
	@JsonIgnore
	private StringProperty processNameProperty;
	
	public ProcessPermissionMst() {
		this.userNameProperty = new SimpleStringProperty("");
		this.processNameProperty = new SimpleStringProperty("");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	@JsonIgnore
	public StringProperty getUserNameProperty() {
		userNameProperty.set(userName);
		return userNameProperty;
	}

	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}
	@JsonIgnore
	public StringProperty getProcessNameProperty() {
		processNameProperty.set(processName);
		return processNameProperty;
	}

	public void setProcessNameProperty(StringProperty processNameProperty) {
		
		this.processNameProperty = processNameProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ProcessPermissionMst [id=" + id + ", userId=" + userId + ", processId=" + processId + ", userName="
				+ userName + ", processName=" + processName + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}


	

	
}
