package com.maple.mapleclient.entity;



import java.util.Date;

import org.springframework.stereotype.Component;

public class VoucherReprint {
	String VoucherType;
	Date VoucherDate;
	String VoucherNo;
	private String  processInstanceId;
	private String taskId;
	public String getVoucherType() {
		return VoucherType;
	}
	public void setVoucherType(String voucherType) {
		VoucherType = voucherType;
	}
	public Date getVoucherDate() {
		return VoucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		VoucherDate = voucherDate;
	}
	public String getVoucherNo() {
		return VoucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		VoucherNo = voucherNo;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "VoucherReprint [VoucherType=" + VoucherType + ", VoucherDate=" + VoucherDate + ", VoucherNo="
				+ VoucherNo + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	

	
	
}
