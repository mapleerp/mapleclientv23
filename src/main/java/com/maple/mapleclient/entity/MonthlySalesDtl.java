package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class MonthlySalesDtl implements Serializable {

	
private static final long serialVersionUID = 1L;
	
	String itemId;
	Double qty;
	
	Double rate;
	
	Double cgstTaxRate;
	
	Double sgstTaxRate;
	
	Double cessRate;
	
	Double addCessRate;
	
	Double igstTaxRate;
	 
	String itemTaxaxId;
	
	String unitId;
	String itemName;
	Date expiryDate;

	String batch;
	String barcode;

	
	Double taxRate;
	 
	Double mrp;

	
	Double amount;
	String unitName;
	 
    Double discount;
  
    Double cgstAmount;
  
    Double sgstAmount;
   
    Double igstAmount;
    
    
    Double cessAmount;
    Double returnedQty;
    String status;
    Double addCessAmount;
    Double costPrice;
    String printKotStatus;
    private String  processInstanceId;
	private String taskId;
    
	 	
	private MonthlySalesTransHdr monthlySalesTransHdr;


	private CompanyMst companyMst;
	
	String offerReferenceId;
	String schemeId;
	 
	Double standardPrice;
	 
	String fbKey;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;

	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private DoubleProperty taxRateProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	@JsonIgnore
	private DoubleProperty mrpProperty;

	@JsonIgnore
	private DoubleProperty cessRateProperty;
	@JsonIgnore
	private StringProperty unitProperty;
	@JsonIgnore
	private StringProperty branchNameProperty;

	MonthlySalesDtl (){

		this.branchNameProperty = new SimpleStringProperty();
		this.customerNameProperty =new SimpleStringProperty();
			this.amountProperty = new SimpleDoubleProperty();
			this.itemNameProperty = new SimpleStringProperty();
			this.taxRateProperty = new SimpleDoubleProperty();
	        this.qtyProperty = new SimpleDoubleProperty();
			this.mrpProperty = new SimpleDoubleProperty();
			 this.cessRateProperty = new SimpleDoubleProperty();
				this.unitProperty = new SimpleStringProperty();
	}
	
	
	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}

	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Double getAddCessRate() {
		return addCessRate;
	}

	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}

	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	public String getItemTaxaxId() {
		return itemTaxaxId;
	}

	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getCgstAmount() {
		return cgstAmount;
	}

	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public Double getSgstAmount() {
		return sgstAmount;
	}

	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public Double getIgstAmount() {
		return igstAmount;
	}

	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}

	public Double getCessAmount() {
		return cessAmount;
	}

	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	public Double getReturnedQty() {
		return returnedQty;
	}

	public void setReturnedQty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getAddCessAmount() {
		return addCessAmount;
	}

	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public String getPrintKotStatus() {
		return printKotStatus;
	}

	public void setPrintKotStatus(String printKotStatus) {
		this.printKotStatus = printKotStatus;
	}

	public MonthlySalesTransHdr getMonthlySalesTransHdr() {
		return monthlySalesTransHdr;
	}

	public void setMonthlySalesTransHdr(MonthlySalesTransHdr monthlySalesTransHdr) {
		this.monthlySalesTransHdr = monthlySalesTransHdr;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getOfferReferenceId() {
		return offerReferenceId;
	}

	public void setOfferReferenceId(String offerReferenceId) {
		this.offerReferenceId = offerReferenceId;
	}

	public String getSchemeId() {
		return schemeId;
	}

	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}

	public Double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public String getFbKey() {
		return fbKey;
	}

	public void setFbKey(String fbKey) {
		this.fbKey = fbKey;
	}
	
	
     @JsonIgnore
	public StringProperty getCustomerNameProperty() {
    	
		return customerNameProperty;
	}


	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}


	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set(qty*mrp);
		return amountProperty;
	}


	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

    @JsonIgnore
	public StringProperty getItemNameProperty() {
	itemNameProperty.set(itemName);
		return itemNameProperty;
	}


	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

  @JsonIgnore
	public DoubleProperty getTaxRateProperty() {
	taxRateProperty.set(taxRate);
		return taxRateProperty;
	}


	public void setTaxRateProperty(DoubleProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}

    @JsonIgnore
	public DoubleProperty getQtyProperty() {
	qtyProperty.set(qty);
		return qtyProperty;
	}


	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

@JsonIgnore 
	public DoubleProperty getMrpProperty() {
	
     	mrpProperty.set(mrp);
		return mrpProperty;
	}


	public void setMrpProperty(DoubleProperty mrpProperty) {
		
		
		this.mrpProperty = mrpProperty;
	}

@JsonIgnore
	public DoubleProperty getCessRateProperty() {
	  cessRateProperty.set(cessRate);
		return cessRateProperty;
	}


	public void setCessRateProperty(DoubleProperty cessRateProperty) {
		this.cessRateProperty = cessRateProperty;
	}

@JsonIgnore 
	public StringProperty getUnitProperty() {
	unitProperty.set(unitName);
	
		return unitProperty;
	}


	public void setUnitProperty(StringProperty unitProperty) {
		this.unitProperty = unitProperty;
	}

@JsonIgnore
	public StringProperty getBranchNameProperty() {
	branchNameProperty.set(monthlySalesTransHdr.getBranchCode());
		return branchNameProperty;
	}


	public void setBranchNameProperty(StringProperty branchNameProperty) {
		this.branchNameProperty = branchNameProperty;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "MonthlySalesDtl [itemId=" + itemId + ", qty=" + qty + ", rate=" + rate + ", cgstTaxRate=" + cgstTaxRate
				+ ", sgstTaxRate=" + sgstTaxRate + ", cessRate=" + cessRate + ", addCessRate=" + addCessRate
				+ ", igstTaxRate=" + igstTaxRate + ", itemTaxaxId=" + itemTaxaxId + ", unitId=" + unitId + ", itemName="
				+ itemName + ", expiryDate=" + expiryDate + ", batch=" + batch + ", barcode=" + barcode + ", taxRate="
				+ taxRate + ", mrp=" + mrp + ", amount=" + amount + ", unitName=" + unitName + ", discount=" + discount
				+ ", cgstAmount=" + cgstAmount + ", sgstAmount=" + sgstAmount + ", igstAmount=" + igstAmount
				+ ", cessAmount=" + cessAmount + ", returnedQty=" + returnedQty + ", status=" + status
				+ ", addCessAmount=" + addCessAmount + ", costPrice=" + costPrice + ", printKotStatus=" + printKotStatus
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", monthlySalesTransHdr="
				+ monthlySalesTransHdr + ", companyMst=" + companyMst + ", offerReferenceId=" + offerReferenceId
				+ ", schemeId=" + schemeId + ", standardPrice=" + standardPrice + ", fbKey=" + fbKey + "]";
	}

	
}
