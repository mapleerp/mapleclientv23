package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class StockTransferInExceptionHdr {

	
private String id;
	

	String intentNumber;
	Date 	voucherDate;
	String voucherNumber;
	String voucherType;
	String branchCode;

	String deleted;
	String fromBranch;
	String inVoucherNumber;
	Date inVoucherDate;
	String statusAcceptStock;
	
	
	String stockTransferOutHdrId;
	String narration;
	

	CompanyMst companyMst;
	 String  processInstanceId;
	String taskId;
	String exceptionReason;
	
	
	
	public StockTransferInExceptionHdr() {


		this.voucherNumberProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();
		this.reasonProperty=new SimpleStringProperty();
		this.intentVoucherNumberProperty=new SimpleStringProperty();
		this.fromBranchProperty=new SimpleStringProperty();
		
	}
	
	

	@JsonIgnore
	StringProperty voucherNumberProperty;
	@JsonIgnore
	StringProperty voucherDateProperty;
	@JsonIgnore
	StringProperty reasonProperty;
	
	@JsonIgnore
	StringProperty intentVoucherNumberProperty;
	@JsonIgnore
	StringProperty fromBranchProperty;
	
	
	
	
	
	
	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(inVoucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	public StringProperty getVoucherDateProperty() {
		
		String vDate=SystemSetting.UtilDateToString(voucherDate);
		voucherDateProperty.set(vDate);
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}
	public StringProperty getReasonProperty() {
		reasonProperty.set(exceptionReason);
		return reasonProperty;
	}
	public void setReasonProperty(StringProperty reasonProperty) {
		this.reasonProperty = reasonProperty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIntentNumber() {
		return intentNumber;
	}
	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getDeleted() {
	
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getInVoucherNumber() {
		return inVoucherNumber;
	}
	public void setInVoucherNumber(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}
	public Date getInVoucherDate() {
		return inVoucherDate;
	}
	public void setInVoucherDate(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}
	public String getStatusAcceptStock() {
		return statusAcceptStock;
	}
	public void setStatusAcceptStock(String statusAcceptStock) {
		this.statusAcceptStock = statusAcceptStock;
	}
	public String getStockTransferOutHdrId() {
		return stockTransferOutHdrId;
	}
	public void setStockTransferOutHdrId(String stockTransferOutHdrId) {
		this.stockTransferOutHdrId = stockTransferOutHdrId;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getExceptionReason() {
		return exceptionReason;
	}
	public void setExceptionReason(String exceptionReason) {
		this.exceptionReason = exceptionReason;
	}
	@JsonIgnore
	public StringProperty getIntentVoucherNumberProperty() {
		intentVoucherNumberProperty.set(inVoucherNumber);
		return intentVoucherNumberProperty;
	}
	public void setIntentVoucherNumberProperty(StringProperty intentVoucherNumberProperty) {
		this.intentVoucherNumberProperty = intentVoucherNumberProperty;
	}
	@JsonIgnore
	public StringProperty getFromBranchProperty() {
		fromBranchProperty.set(fromBranch);
		return fromBranchProperty;
	}
	public void setFromBranchProperty(StringProperty fromBranchProperty) {
		this.fromBranchProperty = fromBranchProperty;
	}
	@Override
	public String toString() {
		return "StockTransferInExceptionHdr [id=" + id + ", intentNumber=" + intentNumber + ", voucherDate="
				+ voucherDate + ", voucherNumber=" + voucherNumber + ", voucherType=" + voucherType + ", branchCode="
				+ branchCode + ", deleted=" + deleted + ", fromBranch=" + fromBranch + ", inVoucherNumber="
				+ inVoucherNumber + ", inVoucherDate=" + inVoucherDate + ", statusAcceptStock=" + statusAcceptStock
				+ ", stockTransferOutHdrId=" + stockTransferOutHdrId + ", narration=" + narration + ", companyMst="
				+ companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", exceptionReason="
				+ exceptionReason + ", voucherNumberProperty=" + voucherNumberProperty + ", voucherDateProperty="
				+ voucherDateProperty + ", reasonProperty=" + reasonProperty + ", intentVoucherNumberProperty="
				+ intentVoucherNumberProperty + ", fromBranchProperty=" + fromBranchProperty
				+ ", getVoucherNumberProperty()=" + getVoucherNumberProperty() + ", getVoucherDateProperty()="
				+ getVoucherDateProperty() + ", getReasonProperty()=" + getReasonProperty() + ", getId()=" + getId()
				+ ", getIntentNumber()=" + getIntentNumber() + ", getVoucherDate()=" + getVoucherDate()
				+ ", getVoucherNumber()=" + getVoucherNumber() + ", getVoucherType()=" + getVoucherType()
				+ ", getBranchCode()=" + getBranchCode() + ", getDeleted()=" + getDeleted() + ", getFromBranch()="
				+ getFromBranch() + ", getInVoucherNumber()=" + getInVoucherNumber() + ", getInVoucherDate()="
				+ getInVoucherDate() + ", getStatusAcceptStock()=" + getStatusAcceptStock()
				+ ", getStockTransferOutHdrId()=" + getStockTransferOutHdrId() + ", getNarration()=" + getNarration()
				+ ", getCompanyMst()=" + getCompanyMst() + ", getProcessInstanceId()=" + getProcessInstanceId()
				+ ", getTaskId()=" + getTaskId() + ", getExceptionReason()=" + getExceptionReason()
				+ ", getIntentVoucherNumberProperty()=" + getIntentVoucherNumberProperty()
				+ ", getFromBranchProperty()=" + getFromBranchProperty() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	

	
	
	
}
