package com.maple.mapleclient.entity;

import java.io.Serializable;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ServiceItemMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
 

	private String id;
	
 	private String itemName;

	private String unitId;

	private Double standardPrice;

	private String branchCode;


	private Double taxRate;

	private String isDeleted;

	private Double cess ;
	
	private String categoryId;

	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	
	public ServiceItemMst() {
		this.itemNameProperty  = new SimpleStringProperty("");
		this.categoryProperty  = new SimpleStringProperty("");
		this.deletedStatusProperty = new SimpleStringProperty("");
		this.cessProperty  = new SimpleDoubleProperty(0.0);
		this.taxProperty  = new SimpleDoubleProperty(0.0);
		this.mrpProperty  = new SimpleDoubleProperty(0.0);
		
	}

	@JsonIgnore
	String categoryName;
	
	@JsonIgnore
	String deletedStatus;
	
	@JsonIgnore
	StringProperty deletedStatusProperty;
	
	@JsonIgnore
	StringProperty itemNameProperty;
	
	@JsonIgnore
	StringProperty categoryProperty;
	
	@JsonIgnore
	DoubleProperty cessProperty;
	
	@JsonIgnore
	DoubleProperty taxProperty;
	
	@JsonIgnore
	DoubleProperty mrpProperty;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public Double getStandardPrice() {
		return standardPrice;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Double getCess() {
		return cess;
	}

	public void setCess(Double cess) {
		this.cess = cess;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	
	
	

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getCategoryProperty() {
		categoryProperty.set(categoryName);
		return categoryProperty;
	}

	public void setCategoryProperty(StringProperty categoryProperty) {
		this.categoryProperty = categoryProperty;
	}

	public DoubleProperty getCessProperty() {
		cessProperty.set(cess);
		return cessProperty;
	}

	public void setCessProperty(DoubleProperty cessProperty) {
		this.cessProperty = cessProperty;
	}

	public DoubleProperty getTaxProperty() {
		taxProperty.set(taxRate);
		return taxProperty;
	}

	public void setTaxProperty(DoubleProperty taxProperty) {
		this.taxProperty = taxProperty;
	}

	public DoubleProperty getMrpProperty() {
		mrpProperty.set(standardPrice);
		return mrpProperty;
	}

	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}
	
	

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	

	public String getDeletedStatus() {
		return deletedStatus;
	}

	public void setDeletedStatus(String deletedStatus) {
		this.deletedStatus = deletedStatus;
	}

	public StringProperty getDeletedStatusProperty() {
		
		deletedStatusProperty.set(deletedStatus);
		return deletedStatusProperty;
	}

	public void setDeletedStatusProperty(StringProperty deletedStatusProperty) {
		this.deletedStatusProperty = deletedStatusProperty;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ServiceItemMst [id=" + id + ", itemName=" + itemName + ", unitId=" + unitId + ", standardPrice="
				+ standardPrice + ", branchCode=" + branchCode + ", taxRate=" + taxRate + ", isDeleted=" + isDeleted
				+ ", cess=" + cess + ", categoryId=" + categoryId + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", categoryName=" + categoryName
				+ ", deletedStatus=" + deletedStatus + "]";
	}

	
	
	
	

}