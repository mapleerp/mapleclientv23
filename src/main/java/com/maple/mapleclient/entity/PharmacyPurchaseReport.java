package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyPurchaseReport {

	String trEntryDate;
	String supplierName;
	String ourVoucherDate;
	String supplierVoucherDate;
	Double netInvoiceTotal;
	String tinNo;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty trEntryDateProperty;
	@JsonIgnore
    private	DoubleProperty  netInvoiceTotalProperty;
	
	@JsonIgnore
	 private StringProperty supplierNameProperty;
	
	@JsonIgnore
	 private StringProperty tinNoProperty;
	
	@JsonIgnore
	 private StringProperty  ourVoucherDateProperty;
	
	@JsonIgnore
	 private StringProperty  supplierVoucherDateProperty;
	
	
	
  public PharmacyPurchaseReport(){
		
	  this.supplierNameProperty=new SimpleStringProperty("");
	  this.trEntryDateProperty=new SimpleStringProperty("");
	  this.ourVoucherDateProperty=new SimpleStringProperty("");
	  this.tinNoProperty=new SimpleStringProperty("");
	  this.supplierVoucherDateProperty=new SimpleStringProperty("");
		this.netInvoiceTotalProperty=new SimpleDoubleProperty();
		
		
  
  }

  
  public String getTinNo() {
	return tinNo;
}


public void setTinNo(String tinNo) {
	this.tinNo = tinNo;
}

@JsonIgnore
public StringProperty getTrEntryDateProperty() {
	trEntryDateProperty.set(trEntryDate);
	return trEntryDateProperty;
}


public void setTrEntryDateProperty(StringProperty trEntryDateProperty) {
	this.trEntryDateProperty = trEntryDateProperty;
}

@JsonIgnore
public DoubleProperty getNetInvoiceTotalProperty() {
	netInvoiceTotalProperty.set(netInvoiceTotal);
	return netInvoiceTotalProperty;
}


public void setNetInvoiceTotalProperty(DoubleProperty netInvoiceTotalProperty) {
	this.netInvoiceTotalProperty = netInvoiceTotalProperty;
}

@JsonIgnore
public StringProperty getSupplierNameProperty() {
	supplierNameProperty.set(supplierName);
	
	return supplierNameProperty;
}


public void setSupplierNameProperty(StringProperty supplierNameProperty) {
	this.supplierNameProperty = supplierNameProperty;
}

@JsonIgnore
public StringProperty getTinNoProperty() {
	tinNoProperty.set(tinNo);
	
	return tinNoProperty;
}


public void setTinNoProperty(StringProperty tinNoProperty) {
	this.tinNoProperty = tinNoProperty;
}

@JsonIgnore
public StringProperty getOurVoucherDateProperty() {
	
	ourVoucherDateProperty.set(ourVoucherDate);
	return ourVoucherDateProperty;
}


public void setOurVoucherDateProperty(StringProperty ourVoucherDateProperty) {
	this.ourVoucherDateProperty = ourVoucherDateProperty;
}

@JsonIgnore
public StringProperty getSupplierVoucherDateProperty() {
	
	supplierVoucherDateProperty.set(supplierVoucherDate);
	
	return supplierVoucherDateProperty;
}


public void setSupplierVoucherDateProperty(StringProperty supplierVoucherDateProperty) {
	this.supplierVoucherDateProperty = supplierVoucherDateProperty;
}


public String getTrEntryDate() {
		return trEntryDate;
	}
	public void setTrEntryDate(String trEntryDate) {
		this.trEntryDate = trEntryDate;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getOurVoucherDate() {
		return ourVoucherDate;
	}
	public void setOurVoucherDate(String ourVoucherDate) {
		this.ourVoucherDate = ourVoucherDate;
	}
	public String getSupplierVoucherDate() {
		return supplierVoucherDate;
	}
	public void setSupplierVoucherDate(String supplierVoucherDate) {
		this.supplierVoucherDate = supplierVoucherDate;
	}
	public Double getNetInvoiceTotal() {
		return netInvoiceTotal;
	}
	public void setNetInvoiceTotal(Double netInvoiceTotal) {
		this.netInvoiceTotal = netInvoiceTotal;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "PharmacyPurchaseReport [trEntryDate=" + trEntryDate + ", supplierName=" + supplierName
				+ ", ourVoucherDate=" + ourVoucherDate + ", supplierVoucherDate=" + supplierVoucherDate
				+ ", netInvoiceTotal=" + netInvoiceTotal + ", tinNo=" + tinNo + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
