package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class NutritionMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	 private String id;
	 String nutrition;
		
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;

	@JsonIgnore
	private StringProperty nutritionProperty;
	

	public NutritionMst() {
		
		this.nutritionProperty = new SimpleStringProperty();
		
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getNutrition() {
		return nutrition;
	}


	public void setNutrition(String nutrition) {
		this.nutrition = nutrition;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public StringProperty getNutritionProperty() {
		nutritionProperty.set(nutrition);
		return nutritionProperty;
	}


	public void setNutritionProperty(StringProperty nutritionProperty) {
		this.nutritionProperty = nutritionProperty;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "NutritionMst [id=" + id + ", nutrition=" + nutrition + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
}
