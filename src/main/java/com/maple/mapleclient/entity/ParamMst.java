package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class ParamMst implements Serializable {

	
	private String id;
	private String paramName;
	private String parentId;
	private String parentName;
	
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;

	
	
	public  ParamMst() {
		
		this.paramNameProperty = new SimpleStringProperty();
		this.parentNameProperty = new SimpleStringProperty();
	}
	
	@JsonIgnore
	private StringProperty paramNameProperty;
	@JsonIgnore
	private StringProperty parentNameProperty;



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public StringProperty getParamNameProperty() {
		paramNameProperty.set(paramName);
		return paramNameProperty;
	}
	public void setParamNameProperty(StringProperty paramNameProperty) {
		this.paramNameProperty = paramNameProperty;
	}
	public StringProperty getParentNameProperty() {
		parentNameProperty.set(parentName);
		return parentNameProperty;
	}
	public void setParentNameProperty(StringProperty parentNameProperty) {
		this.parentNameProperty = parentNameProperty;
	}
	@Override
	public String toString() {
		return "ParamMst [id=" + id + ", paramName=" + paramName + ", parentId=" + parentId + ", parentName="
				+ parentName + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", paramNameProperty=" + paramNameProperty + ", parentNameProperty=" + parentNameProperty
				+ "]";
	}
	
	
	
	






}
