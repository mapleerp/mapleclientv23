package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StoreMst implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String name;
	private String shortCode;
	private String branchCode;
	private String mobile;
	private String  processInstanceId;
	private String taskId;
	
	CompanyMst companyMst;
	@JsonIgnore
	private StringProperty nameProperty;
	@JsonIgnore
	private StringProperty shortCodeProperty;
	@JsonIgnore
	private StringProperty mobileProperty;
	
	

	public StoreMst() {
		this.nameProperty = new SimpleStringProperty("");
		this.shortCodeProperty = new SimpleStringProperty("");
		this.mobileProperty = new SimpleStringProperty("");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	
	
	public StringProperty getNameProperty() {
		nameProperty.set(name);
		return nameProperty;
	}

	public void setNameProperty(StringProperty nameProperty) {
		this.nameProperty = nameProperty;
	}

	public StringProperty getShortCodeProperty() {
		shortCodeProperty.set(shortCode);
		return shortCodeProperty;
	}

	public void setShortCodeProperty(StringProperty shortCodeProperty) {
		this.shortCodeProperty = shortCodeProperty;
	}

	public StringProperty getMobileProperty() {
		mobileProperty.set(mobile);
		return mobileProperty;
	}

	public void setMobileProperty(StringProperty mobileProperty) {
		this.mobileProperty = mobileProperty;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "StoreMst [id=" + id + ", name=" + name + ", shortCode=" + shortCode + ", branchCode=" + branchCode
				+ ", mobile=" + mobile + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", companyMst=" + companyMst + "]";
	}

	

}
