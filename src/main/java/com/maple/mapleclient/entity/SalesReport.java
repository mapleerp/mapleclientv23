package com.maple.mapleclient.entity;

import java.io.DataOutput;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesReport {
	
	private String branch;
	private Double cash;
	private Double card;
	private Double card2;
	private String credit;
	private Double bank;
	private Double total;
	private Double invovicetotal;
	private String  processInstanceId;
	private String taskId;
	
	
	
	public SalesReport() {
		this.branchCodeProperty = new SimpleStringProperty("");
		this.cashProperty = new SimpleDoubleProperty(0.0);
		this.cardProperty = new SimpleDoubleProperty(0.0);
		this.totalProperty = new SimpleDoubleProperty(0.0);
		this.invovicetotalProperty = new SimpleDoubleProperty(0.0);
		this.creditAmountProperty = new SimpleDoubleProperty(0.0);


	}
	@JsonIgnore
	private StringProperty branchCodeProperty;
	
	@JsonIgnore
	private DoubleProperty cashProperty;
	
	@JsonIgnore
	private DoubleProperty cardProperty;
	
	@JsonIgnore
	private DoubleProperty totalProperty;
	
	@JsonIgnore
	private DoubleProperty invovicetotalProperty;
	
	@JsonIgnore
	private Double creditAmount;
	
	@JsonIgnore
	private DoubleProperty creditAmountProperty;
	
	
	public Double getInvovicetotal() {
		return invovicetotal;
	}
	public void setInvovicetotal(Double invovicetotal) {
		this.invovicetotal = invovicetotal;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public Double getCash() {
		return cash;
	}
	public void setCash(Double cash) {
		this.cash = cash;
	}
	public Double getCard() {
		return card;
	}
	public void setCard(Double card) {
		this.card = card;
	}
	public Double getCard2() {
		return card2;
	}
	public void setCard2(Double card2) {
		this.card2 = card2;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	public Double getBank() {
		return bank;
	}
	public void setBank(Double bank) {
		this.bank = bank;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public StringProperty getBranchCodeProperty() {
		branchCodeProperty.set(branch);
		return branchCodeProperty;
	}
	public void setBranchCodeProperty(StringProperty branchCodeProperty) {
		this.branchCodeProperty = branchCodeProperty;
	}
	public DoubleProperty getCashProperty() {
		cashProperty.set(cash);
		return cashProperty;
	}
	public void setCashProperty(DoubleProperty cashProperty) {
		this.cashProperty = cashProperty;
	}
	public DoubleProperty getCardProperty() {
		cardProperty.set(card);
		return cardProperty;
	}
	public void setCardProperty(DoubleProperty cardProperty) {
		this.cardProperty = cardProperty;
	}
	public DoubleProperty getTotalProperty() {
		totalProperty.set(total);
		return totalProperty;
	}
	public void setTotalProperty(DoubleProperty totalProperty) {
		this.totalProperty = totalProperty;
	}
	public DoubleProperty getInvovicetotalProperty() {
		invovicetotalProperty.set(invovicetotal);
		return invovicetotalProperty;
	}
	public void setInvovicetotalProperty(DoubleProperty invovicetotalProperty) {
		this.invovicetotalProperty = invovicetotalProperty;
	}
	
	
	
	
	
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public DoubleProperty getCreditAmountProperty() {
		creditAmountProperty.set(creditAmount);
		return creditAmountProperty;
	}
	public void setCreditAmountProperty(DoubleProperty creditAmountProperty) {
		this.creditAmountProperty = creditAmountProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SalesReport [branch=" + branch + ", cash=" + cash + ", card=" + card + ", card2=" + card2 + ", credit="
				+ credit + ", bank=" + bank + ", total=" + total + ", invovicetotal=" + invovicetotal
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", creditAmount=" + creditAmount
				+ "]";
	}
	
	
	

}
