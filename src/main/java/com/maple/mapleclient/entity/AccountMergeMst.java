package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AccountMergeMst {

	private String id;
	
	String fromAccountId;
	
	String toAccountId;
	
	String fromAccountName;
	
	String toAccountName;
	
	String BranchCode;
	
	Date UpdatedDate;
	
	CompanyMst companyMst;
	
	@JsonIgnore
	private StringProperty fromAccountIdProperty;
	@JsonIgnore
	private StringProperty toAccountIdProperty;
	@JsonIgnore
	private StringProperty fromAccountNameProperty;
	@JsonIgnore
	private StringProperty toAccountNameProperty;
	@JsonIgnore
	private StringProperty UpdatedDateProperty;
	
	public AccountMergeMst()
	{
		
		this.toAccountIdProperty = new SimpleStringProperty("");
		this.fromAccountNameProperty = new SimpleStringProperty("");
		this.fromAccountIdProperty = new SimpleStringProperty("");
		this.toAccountNameProperty = new SimpleStringProperty("");
		this.UpdatedDateProperty = new SimpleStringProperty("");
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFromAccountId() {
		return fromAccountId;
	}

	public void setFromAccountId(String fromAccountId) {
		this.fromAccountId = fromAccountId;
	}

	public String getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(String toAccountId) {
		this.toAccountId = toAccountId;
	}

	public String getFromAccountName() {
		return fromAccountName;
	}

	public void setFromAccountName(String fromAccountName) {
		this.fromAccountName = fromAccountName;
	}

	public String getToAccountName() {
		return toAccountName;
	}

	public void setToAccountName(String toAccountName) {
		this.toAccountName = toAccountName;
	}

	public String getBranchCode() {
		return BranchCode;
	}

	public void setBranchCode(String branchCode) {
		BranchCode = branchCode;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public StringProperty getFromAccountIdProperty() {
		
		if(null!=fromAccountId) {
			fromAccountIdProperty.set(fromAccountId);
		}
		return fromAccountIdProperty;
	}

	public void setFromAccountIdProperty(StringProperty fromAccountIdProperty) {
		this.fromAccountIdProperty = fromAccountIdProperty;
	}

	public StringProperty getToAccountIdProperty() {
		
		if(null!=toAccountId) {
			toAccountIdProperty.set(toAccountId);
		}
		return toAccountIdProperty;
	}

	public void setToAccountIdProperty(StringProperty toAccountIdProperty) {
		this.toAccountIdProperty = toAccountIdProperty;
	}

	public StringProperty getFromAccountNameProperty() {
		
		if(null!=fromAccountName) {
			fromAccountNameProperty.set(fromAccountName);
		}
		return fromAccountNameProperty;
	}

	public void setFromAccountNameProperty(StringProperty fromAccountNameProperty) {
		this.fromAccountNameProperty = fromAccountNameProperty;
	}

	public StringProperty getToAccountNameProperty() {
		
		if(null!=toAccountName) {
			toAccountNameProperty.set(toAccountName);
		}
		return toAccountNameProperty;
	}

	public void setToAccountNameProperty(StringProperty toAccountNameProperty) {
		this.toAccountNameProperty = toAccountNameProperty;
	}

	public StringProperty getUpdatedDateProperty() {
		if(null!=UpdatedDate) {
			
			UpdatedDateProperty.set(SystemSetting.UtilDateToString(UpdatedDate, "yyyy-MM-dd"));
		}
		return UpdatedDateProperty;
	}

	public void setUpdatedDateProperty(StringProperty updatedDateProperty) {
		UpdatedDateProperty = updatedDateProperty;
	}

	public Date getUpdatedDate() {
		return UpdatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		UpdatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "AccountMergeMst [id=" + id + ", fromAccountId=" + fromAccountId + ", toAccountId=" + toAccountId
				+ ", fromAccountName=" + fromAccountName + ", toAccountName=" + toAccountName + ", BranchCode="
				+ BranchCode + ", UpdatedDate=" + UpdatedDate + ", companyMst=" + companyMst
				+ ", fromAccountIdProperty=" + fromAccountIdProperty + ", toAccountIdProperty=" + toAccountIdProperty
				+ ", fromAccountNameProperty=" + fromAccountNameProperty + ", toAccountNameProperty="
				+ toAccountNameProperty + ", UpdatedDateProperty=" + UpdatedDateProperty + "]";
	}

	

}
