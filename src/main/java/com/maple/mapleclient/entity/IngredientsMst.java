package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.StringProperty;

public class IngredientsMst {

	
	String id;
	
   private String IngredientText1;
	
	private String IngredientText2;
	
	private String duration;
	
	private ItemMst itemMst;

	private String printerName;
	private String  processInstanceId;
	private String taskId;

	public String getPrinterName() {
		return printerName;
	}

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	public String getIngredientText1() {
		return IngredientText1;
	}

	public void setIngredientText1(String ingredientText1) {
		IngredientText1 = ingredientText1;
	}

	public String getIngredientText2() {
		return IngredientText2;
	}

	public void setIngredientText2(String ingredientText2) {
		IngredientText2 = ingredientText2;
	}

	public ItemMst getItemMst() {
		return itemMst;
	}

	public void setItemMst(ItemMst itemMst) {
		this.itemMst = itemMst;
	}

	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "IngredientsMst [id=" + id + ", IngredientText1=" + IngredientText1 + ", IngredientText2="
				+ IngredientText2 + ", duration=" + duration + ", itemMst=" + itemMst + ", printerName=" + printerName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	
	
	
}
