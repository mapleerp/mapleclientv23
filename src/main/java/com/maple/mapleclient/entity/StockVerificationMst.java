package com.maple.mapleclient.entity;

import java.time.LocalDateTime;
import java.util.Date;

public class StockVerificationMst {
	

	String id;
	Date voucherDate;
	String userId;
	String branchCode;
	String voucherNumber;
	private String  processInstanceId;
	private String taskId;
	
	private LocalDateTime updatedTime;
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "StockVerificationMst [id=" + id + ", voucherDate=" + voucherDate + ", userId=" + userId
				+ ", branchCode=" + branchCode + ", voucherNumber=" + voucherNumber + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", updatedTime=" + updatedTime + "]";
	}

}
