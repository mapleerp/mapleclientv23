package com.maple.mapleclient.entity;

import java.util.List;


public class MemberWiseReceipt {

	String familyName;
	String headOfFamily;
	private String  processInstanceId;
	private String taskId;
	List<AccountDtls> accountDtls;
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getHeadOfFamily() {
		return headOfFamily;
	}
	public void setHeadOfFamily(String headOfFamily) {
		this.headOfFamily = headOfFamily;
	}
	public List<AccountDtls> getAccountDtls() {
		return accountDtls;
	}
	public void setAccountDtls(List<AccountDtls> accountDtls) {
		this.accountDtls = accountDtls;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MemberWiseReceipt [familyName=" + familyName + ", headOfFamily=" + headOfFamily + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", accountDtls=" + accountDtls + "]";
	}
	
	
}
