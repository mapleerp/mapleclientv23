package com.maple.mapleclient.entity;
import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class ReceiptDtl
{
	String memberId;
	String id;
	String bankAccountNumber;
	String account;
	String modeOfpayment;
	Double amount;
	String depositBank;
	String remark;
	String instnumber;
	String invoiceNumber;
	String branchCode;
	String accountName;
	String debitAccountId;
	java.util.Date invoiceDate;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty bankAccountNumberProperty;
	@JsonIgnore
	private StringProperty accountProperty;
	@JsonIgnore
	private StringProperty memberIdProperty;
	@JsonIgnore
	private StringProperty modeOfPaymentProperty;
	@JsonIgnore
	private DoubleProperty amountPropery;
	@JsonIgnore
	private StringProperty depositBankProperty;
	@JsonIgnore
	private StringProperty remarkProperty;
	@JsonIgnore
	private StringProperty instnumberProperty;
	
	@JsonIgnore
	private StringProperty debitAccountIdProperty;
	@JsonIgnore
	
	private StringProperty invoiceNumberProperty;
	@JsonIgnore
	 private   ObjectProperty<LocalDate> instrumentDate;
	
	@JsonIgnore
	 private   ObjectProperty<LocalDate> transDate;
	
	
	
	
	
	public ReceiptDtl() {
		this.bankAccountNumberProperty = new SimpleStringProperty("");
		this.accountProperty =new SimpleStringProperty("");
		this.modeOfPaymentProperty = new SimpleStringProperty("");
		this.amountPropery = new SimpleDoubleProperty();
		this.depositBankProperty = new SimpleStringProperty("");
		this.remarkProperty = new SimpleStringProperty("");
		this.instnumberProperty = new SimpleStringProperty("");
		this.invoiceNumberProperty = new SimpleStringProperty("");
		this.instrumentDate = new SimpleObjectProperty<LocalDate>(null);
		this.transDate = new SimpleObjectProperty<LocalDate>(null);
		this.memberIdProperty=new SimpleStringProperty("");
		this.debitAccountIdProperty=new SimpleStringProperty("");
	}


	@JsonProperty("instrumentDate")
	public java.sql.Date getInstrumentDate( ) {
		if(null==this.instrumentDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.instrumentDate.get() );
		}
	}
	
   public void setInstrumentDate (java.sql.Date instrumentDate) {
						if(null!=instrumentDate)
		this.instrumentDate.set(instrumentDate.toLocalDate());
	}
	
	@JsonProperty("transDate")
	public java.sql.Date getTransDate( ) {
		if(null==this.transDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.transDate.get() );
		}
	 
	}
	 public void setTransDate (java.sql.Date transDateProperty) {
		 if(null!=transDateProperty)
			this.transDate.set(transDateProperty.toLocalDate());

	 }

	 



	ReceiptHdr receipthdr;

	
	
	public ReceiptHdr getReceipthdr() {
		return receipthdr;
	}
	public void setReceipthdr(ReceiptHdr receipthdr) {
		this.receipthdr = receipthdr;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDebitAccountId() {
		return debitAccountId;
	}


	public void setDebitAccountId(String debitAccountId) {
		this.debitAccountId = debitAccountId;
	}


	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getModeOfpayment() {
		return modeOfpayment;
	}

	public void setModeOfpayment(String modeOfpayment) {
		this.modeOfpayment = modeOfpayment;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getDepositBank() {
		return depositBank;
	}

	public void setDepositBank(String depositBank) {
		this.depositBank = depositBank;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getMemberId() {
		return memberId;
	}


	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getInstnumber() {
		return instnumber;
	}

	public void setInstnumber(String instnumber) {
		this.instnumber = instnumber;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	@JsonIgnore
	public StringProperty getbankAccountNumberProperty() {

		bankAccountNumberProperty.set(bankAccountNumber);
		return bankAccountNumberProperty;
	}

	public void setbankAccountNumberProperty(String bankAccountNumber) {
		bankAccountNumber = bankAccountNumber;
	}
	
	@JsonIgnore
	public StringProperty getaccountProperty() {

		accountProperty.set(account);
		return accountProperty;
	}

	public void setaccountProperty(String accountName) {
		this.accountName = accountName;
	}
	@JsonIgnore
	public StringProperty getmodeOfPaymentProperty() {

		modeOfPaymentProperty.set(modeOfpayment);
		return modeOfPaymentProperty;
	}

	public void setmodeOfPaymentProperty(String modeOfpayment) {
		modeOfpayment = modeOfpayment;
	}
	@JsonIgnore
	public DoubleProperty getamountPropery() {

		amountPropery.set(amount);
		return amountPropery;
	}

	public void setamountPropery(Double amount) {
		amount = amount;
	}
	@JsonIgnore
	public StringProperty getdepositBankProperty() {

		depositBankProperty.set(depositBank);
		return depositBankProperty;
	}

	public void setdepositBankProperty(String depositBank) {
		depositBank = depositBank;
	}
	@JsonIgnore
	public StringProperty getmemberIdProperty() {

		memberIdProperty.set(memberId);
		return memberIdProperty;
	}

	public void setmemberIdProperty(String memberId) {
		memberId = memberId;
	}
	@JsonIgnore
	public StringProperty getremarkProperty() {

		remarkProperty.set(remark);
		return remarkProperty;
	}

	public void setremarkProperty(String remark) {
		remark = remark;
	}
	@JsonIgnore
	public StringProperty getinstnumberProperty() {

		instnumberProperty.set(instnumber);
		return instnumberProperty;
	}

	public void setinstnumberProperty(String instnumber) {
		instnumber = instnumber;
	}
	

	public String getAccountName() {
		return accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	
	
	@JsonIgnore
	public StringProperty getinvoiceNumberProperty() {

		invoiceNumberProperty.set(invoiceNumber);
		return invoiceNumberProperty;
	}

	public void setinvoiceNumberProperty(String invoiceNumber) {
		invoiceNumber = invoiceNumber;
	}


	public ObjectProperty<LocalDate> getInstrumentDateProperty( ) {
		return instrumentDate;
	}
 
	public void setExpiryDateProperty(ObjectProperty<LocalDate> instrumentDate) {
		this.instrumentDate = instrumentDate;
	}
	public ObjectProperty<LocalDate> gettransDateProperty( ) {
		return transDate;
	}
 
	public void settransDateProperty(ObjectProperty<LocalDate> transDate) {
		this.transDate = transDate;
	}

@JsonIgnore
	public StringProperty getDebitAccountIdProperty() {
	debitAccountIdProperty.set( debitAccountId);
		return debitAccountIdProperty;
	}


	public void setDebitAccountIdProperty(StringProperty debitAccountIdProperty) {
		this.debitAccountIdProperty = debitAccountIdProperty;
	}





	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public java.util.Date getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(java.util.Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	@Override
	public String toString() {
		return "ReceiptDtl [memberId=" + memberId + ", id=" + id + ", bankAccountNumber=" + bankAccountNumber
				+ ", account=" + account + ", modeOfpayment=" + modeOfpayment + ", amount=" + amount + ", depositBank="
				+ depositBank + ", remark=" + remark + ", instnumber=" + instnumber + ", invoiceNumber=" + invoiceNumber
				+ ", branchCode=" + branchCode + ", accountName=" + accountName + ", debitAccountId=" + debitAccountId
				+ ", invoiceDate=" + invoiceDate + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", bankAccountNumberProperty=" + bankAccountNumberProperty + ", accountProperty=" + accountProperty
				+ ", memberIdProperty=" + memberIdProperty + ", modeOfPaymentProperty=" + modeOfPaymentProperty
				+ ", amountPropery=" + amountPropery + ", depositBankProperty=" + depositBankProperty
				+ ", remarkProperty=" + remarkProperty + ", instnumberProperty=" + instnumberProperty
				+ ", debitAccountIdProperty=" + debitAccountIdProperty + ", invoiceNumberProperty="
				+ invoiceNumberProperty + ", instrumentDate=" + instrumentDate + ", transDate=" + transDate
				+ ", receipthdr=" + receipthdr + "]";
	}
	

	
}
