package com.maple.mapleclient.entity;

import java.util.Date;

 
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
 

public class PhysicalStockDtl {
	
 
    private String id;
 
	private String itemId;
	private String batch ;
	private String barcode;
	private Double qtyIn;
	private Double qtyOut;
	private Double mrp;
	private String itemName;
	private Double systemQty;
	 
	private Date expiryDate;
	private Date manufactureDate;
	private String  processInstanceId;
	private String taskId;
	
	
	 
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty barcodeProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty mrpProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
 
	private PhysicalStockHdr physicalStockHdrId;
	
	

	private String store;


	public PhysicalStockDtl() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.barcodeProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty();
		this.mrpProperty = new SimpleDoubleProperty();
		
		//this.batch="NOBATCH";
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}

	


	public String getItemName() {
		return itemName;
	}



	public void setItemName(String itemName) {
		this.itemName = itemName;
	}



	public String getItemId() {
		return itemId;
	}



	public void setItemId(String itemId) {
		this.itemId = itemId;
	}



	public String getBatch() {
		return batch;
	}



	public void setBatch(String batch) {
		this.batch = batch;
	}



	public String getBarcode() {
		return barcode;
	}



	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}



	public Double getQtyIn() {
		return qtyIn;
	}



	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}



	public Double getQtyOut() {
		return qtyOut;
	}



	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}



	public Double getMrp() {
		return mrp;
	}



	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}



	public Date getExpiryDate() {
		return expiryDate;
	}



	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}



	public Date getManufactureDate() {
		return manufactureDate;
	}



	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}



	public PhysicalStockHdr getPhysicalStockHdrId() {
		return physicalStockHdrId;
	}



	public void setPhysicalStockHdrId(PhysicalStockHdr physicalStockHdrId) {
		this.physicalStockHdrId = physicalStockHdrId;
	}

	
	public StringProperty getItemNameProperty() {

		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		itemNameProperty = itemNameProperty;
	}
	
	public StringProperty getBarcodeProperty() {

		barcodeProperty.set(barcode);
		return barcodeProperty;
	}

	public void setBarcodeProperty(StringProperty barcodeProperty) {
		itemNameProperty = barcodeProperty;
	}
	
	public DoubleProperty getQtyProperty() {

		qtyProperty.set(qtyIn);
		return qtyProperty;
	}

	public void setQtyProperty(DoubleProperty qtyProperty) {
		qtyProperty = qtyProperty;
	}
	
	
	public DoubleProperty getMrpProperty() {

		mrpProperty.set(mrp);
		return mrpProperty;
	}

	public void setMrpProperty(DoubleProperty mrpProperty) {
		mrpProperty = mrpProperty;
	}
	
	
	public StringProperty getBatchProperty() {

		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		batchProperty = batchProperty;
	}



	



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	public Double getSystemQty() {
		return systemQty;
	}



	public void setSystemQty(Double systemQty) {
		this.systemQty = systemQty;
	}



	public String getStore() {
		return store;
	}



	public void setStore(String store) {
		this.store = store;
	}



	@Override
	public String toString() {
		return "PhysicalStockDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", barcode=" + barcode
				+ ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", mrp=" + mrp + ", itemName=" + itemName + ", systemQty="
				+ systemQty + ", expiryDate=" + expiryDate + ", manufactureDate=" + manufactureDate
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", itemNameProperty="
				+ itemNameProperty + ", barcodeProperty=" + barcodeProperty + ", qtyProperty=" + qtyProperty
				+ ", mrpProperty=" + mrpProperty + ", batchProperty=" + batchProperty + ", store=" + store + "]";
	}








	
	
	
	
}