

package com.maple.mapleclient.entity;




import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GroupMst {

    private String id;
  
	private String groupName;
	private String branchCode;
	@JsonIgnore
	private StringProperty groupNameProperty;
	@JsonIgnore
	private StringProperty branchCodeProperty;
	private String  processInstanceId;
	private String taskId;
	

public GroupMst()
{
	this.groupNameProperty= new SimpleStringProperty("");
	this.branchCodeProperty =new SimpleStringProperty("");
	
}

	
	
	public StringProperty getGroupNameProperty() {
		groupNameProperty.set(groupName);
		return groupNameProperty;
	}
	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}
	public StringProperty getBranchCodeProperty() {
		branchCodeProperty.set(branchCode);
		return branchCodeProperty;
	}
	public void setBranchCodeProperty(StringProperty branchCodeProperty) {
		this.branchCodeProperty = branchCodeProperty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "GroupMst [id=" + id + ", groupName=" + groupName + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
}
