package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MemberWiseReceiptReport {

	
	String memberName;
	String accountName;
	String TransDate;
	Double amount;
	String familyName;
	private String  processInstanceId;
	private String taskId;
	
	


	@JsonIgnore
	private StringProperty memberNameProperty;
	@JsonIgnore
	private StringProperty familyNameProperty;
	
	@JsonIgnore
	private StringProperty accountNameProperty;
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private StringProperty TransDateProperty;

	
	MemberWiseReceiptReport(){
		
		this.memberNameProperty = new SimpleStringProperty();
		this.familyNameProperty = new SimpleStringProperty();
		this.accountNameProperty = new SimpleStringProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.TransDateProperty = new SimpleStringProperty();
	
		
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getTransDate() {
		return TransDate;
	}
	public void setTransDate(String transDate) {
		TransDate = transDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	@JsonIgnore
	public StringProperty getMemberNameProperty() {
		memberNameProperty.set(memberName);
		return memberNameProperty;
	}
	public void setMemberNameProperty(StringProperty memberNameProperty) {
		this.memberNameProperty = memberNameProperty;
	}
	@JsonIgnore
	public StringProperty getFamilyNameProperty() {
		familyNameProperty.set(familyName);
		return familyNameProperty;
	}
	public void setFamilyNameProperty(StringProperty familyNameProperty) {
		this.familyNameProperty = familyNameProperty;
	}

	
	public DoubleProperty getAmountProperty() {
		
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	@JsonIgnore
	public StringProperty getTransDateProperty() {
		TransDateProperty.set(TransDate);
		return TransDateProperty;
	}
	public void setTransDateProperty(StringProperty transDateProperty) {
		TransDateProperty = transDateProperty;
	}
	
	
	@JsonIgnore
	public StringProperty getAccountNameProperty() {
		accountNameProperty.set(accountName);
		return accountNameProperty;
	}
	public void setAccountNameProperty(StringProperty accountNameProperty) {
		this.accountNameProperty = accountNameProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MemberWiseReceiptReport [memberName=" + memberName + ", accountName=" + accountName + ", TransDate="
				+ TransDate + ", amount=" + amount + ", familyName=" + familyName + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
