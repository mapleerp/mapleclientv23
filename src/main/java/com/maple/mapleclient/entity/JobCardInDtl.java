package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class JobCardInDtl {
	String id;
	private String itemId;
	
	private Double qty;
	JobCardInHdr jobCardInHdr;
	String locationId;
	
	String locationName;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty itemNameproperty;
	
	@JsonIgnore
	String itemName;
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private StringProperty locationProperty;
	
	public JobCardInDtl() {
	
		this.itemNameproperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.locationProperty = new SimpleStringProperty();
	}
	
	@JsonIgnore
	public StringProperty getItemNameProperty()
	{
		itemNameproperty.set(itemName);
		return itemNameproperty;
	}
	public void setItemNameProperty(String itemName)
	{
		this.itemName = itemName;
	}
	
	@JsonIgnore
	public StringProperty getlocationProperty()
	{
		locationProperty.set(locationName);
		return locationProperty;
	}
	public void setlocationProperty(String locationName)
	{
		this.locationName = locationName;
	}
	
	@JsonIgnore
	public DoubleProperty getqtyProperty()
	{
		qtyProperty.set(qty);
		return qtyProperty;
	}
	
	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public void setqtyProperty(Double qty)
	{
		this.qty = qty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	
	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public JobCardInHdr getJobCardInHdr() {
		return jobCardInHdr;
	}
	public void setJobCardInHdr(JobCardInHdr jobCardInHdr) {
		this.jobCardInHdr = jobCardInHdr;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "JobCardInDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", jobCardInHdr=" + jobCardInHdr
				+ ", locationId=" + locationId + ", locationName=" + locationName + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", itemName=" + itemName + "]";
	}
	
}
