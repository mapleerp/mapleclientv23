package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TaxReport {

	

	Double salesTaxRate;
	
	Double salesSgstAmount;
	Double salesCgstAmount;
    Double salesIgstAmount;
    Double salesCessAmount;
    Double totalSaleTaxAmount;
    Double salesTaxAssesableAmount;
    Double netSalesAmount;
    
	
	Double purchaseTaxRate; 
	Double purchaseSgstAmount; 
	Double purchaseCgstAmount;
	Double purchaseIgstAmount; 
	Double purchaseCessAmount; 
	Double totalPurchaseTaxAmount;
	Double purchaseTaxAssesableAmount; 
	Double netPurchaseAmount;
	 

	Double finalTaxRate; 
	Double finalSgstAmount; 
	Double finalCgstAmount;
	Double finalIgstAmount; 
	Double finalCessAmount; 
	Double totalFinalTaxAmount;
	Double finalTaxAssesableAmount; 
	Double netFinalAmount;
	
	private String  processInstanceId;
	private String taskId;
	
	
	

public TaxReport(){
	this.salesTaxRateProperty = new SimpleDoubleProperty(0.0);
	this.salesSgstAmountProperty = new SimpleDoubleProperty(0.0);
	this.salesCgstAmountProperty = new SimpleDoubleProperty(0.0);
	this.salesIgstAmountProperty = new SimpleDoubleProperty(0.0);
	this.salesCessAmountProperty = new SimpleDoubleProperty(0.0);
	this.totalSaleTaxAmountProperty = new SimpleDoubleProperty(0.0);
	this.netSalesAmountProperty=new SimpleDoubleProperty(0.0);
	this. salesTaxAssesableAmountProperty=new SimpleDoubleProperty(0.0);
	
	
	this.purchaseTaxRateProperty=new SimpleDoubleProperty(0.0);
	
	this.purchaseSgstAmountProperty=new SimpleDoubleProperty(0.0);
	this.purchaseCgstAmountProperty=new SimpleDoubleProperty(0.0);
	this.purchaseIgstAmountProperty=new SimpleDoubleProperty(0.0);
	this.netPurchaseAmountProperty=new SimpleDoubleProperty(0.0);
	this.purchaseTaxAssesableAmountProperty=new  SimpleDoubleProperty(0.0);
	this.totalPurchaseTaxAmountProperty=new SimpleDoubleProperty(0.0);
	this.purchaseCessAmountProperty=new SimpleDoubleProperty(0.0);
}
	@JsonIgnore
	private DoubleProperty salesTaxRateProperty;

	@JsonIgnore
	private DoubleProperty salesSgstAmountProperty;

	@JsonIgnore
	private DoubleProperty salesCgstAmountProperty;

	@JsonIgnore
	private DoubleProperty salesIgstAmountProperty;

	@JsonIgnore
	private DoubleProperty salesCessAmountProperty;

	@JsonIgnore
	private DoubleProperty totalSaleTaxAmountProperty;
	@JsonIgnore
	private DoubleProperty salesTaxAssesableAmountProperty;

	@JsonIgnore
	private DoubleProperty purchaseSgstAmountProperty;
	
	@JsonIgnore
	private DoubleProperty netSalesAmountProperty;
	
	
	
	
	@JsonIgnore
	private DoubleProperty purchaseTaxRateProperty;
	@JsonIgnore
	private DoubleProperty purchaseCgstAmountProperty;
	@JsonIgnore
	private DoubleProperty purchaseIgstAmountProperty;
	
	
	@JsonIgnore
	private DoubleProperty purchaseCessAmountProperty;
	@JsonIgnore
	private DoubleProperty netPurchaseAmountProperty;
	@JsonIgnore
	private DoubleProperty purchaseTaxAssesableAmountProperty;

	@JsonIgnore
	private DoubleProperty totalPurchaseTaxAmountProperty;
	public Double getSalesTaxRate() {
		return salesTaxRate;
	}
	public void setSalesTaxRate(Double salesTaxRate) {
		this.salesTaxRate = salesTaxRate;
	}
	public Double getSalesSgstAmount() {
		return salesSgstAmount;
	}
	public void setSalesSgstAmount(Double salesSgstAmount) {
		this.salesSgstAmount = salesSgstAmount;
	}
	public Double getSalesCgstAmount() {
		return salesCgstAmount;
	}
	public void setSalesCgstAmount(Double salesCgstAmount) {
		this.salesCgstAmount = salesCgstAmount;
	}
	public Double getSalesIgstAmount() {
		return salesIgstAmount;
	}
	public void setSalesIgstAmount(Double salesIgstAmount) {
		this.salesIgstAmount = salesIgstAmount;
	}
	public Double getSalesCessAmount() {
		return salesCessAmount;
	}
	public void setSalesCessAmount(Double salesCessAmount) {
		this.salesCessAmount = salesCessAmount;
	}
	public Double getTotalSaleTaxAmount() {
		return totalSaleTaxAmount;
	}
	public void setTotalSaleTaxAmount(Double totalSaleTaxAmount) {
		this.totalSaleTaxAmount = totalSaleTaxAmount;
	}
	public Double getSalesTaxAssesableAmount() {
		return salesTaxAssesableAmount;
	}
	public void setSalesTaxAssesableAmount(Double salesTaxAssesableAmount) {
		this.salesTaxAssesableAmount = salesTaxAssesableAmount;
	}
	public Double getNetSalesAmount() {
		return netSalesAmount;
	}
	public void setNetSalesAmount(Double netSalesAmount) {
		this.netSalesAmount = netSalesAmount;
	}
	public Double getPurchaseTaxRate() {
		return purchaseTaxRate;
	}
	public void setPurchaseTaxRate(Double purchaseTaxRate) {
		this.purchaseTaxRate = purchaseTaxRate;
	}
	public Double getPurchaseSgstAmount() {
		return purchaseSgstAmount;
	}
	public void setPurchaseSgstAmount(Double purchaseSgstAmount) {
		this.purchaseSgstAmount = purchaseSgstAmount;
	}
	public Double getPurchaseCgstAmount() {
		return purchaseCgstAmount;
	}
	public void setPurchaseCgstAmount(Double purchaseCgstAmount) {
		this.purchaseCgstAmount = purchaseCgstAmount;
	}
	public Double getPurchaseIgstAmount() {
		return purchaseIgstAmount;
	}
	public void setPurchaseIgstAmount(Double purchaseIgstAmount) {
		this.purchaseIgstAmount = purchaseIgstAmount;
	}
	public Double getPurchaseCessAmount() {
		
		
		return purchaseCessAmount;
	}
	public void setPurchaseCessAmount(Double purchaseCessAmount) {
		this.purchaseCessAmount = purchaseCessAmount;
	}
	public Double getTotalPurchaseTaxAmount() {
		return totalPurchaseTaxAmount;
	}
	public void setTotalPurchaseTaxAmount(Double totalPurchaseTaxAmount) {
		this.totalPurchaseTaxAmount = totalPurchaseTaxAmount;
	}
	public Double getPurchaseTaxAssesableAmount() {
		return purchaseTaxAssesableAmount;
	}
	public void setPurchaseTaxAssesableAmount(Double purchaseTaxAssesableAmount) {
		this.purchaseTaxAssesableAmount = purchaseTaxAssesableAmount;
	}
	public Double getNetPurchaseAmount() {
		return netPurchaseAmount;
	}
	public void setNetPurchaseAmount(Double netPurchaseAmount) {
		this.netPurchaseAmount = netPurchaseAmount;
	}
	public Double getFinalTaxRate() {
		return finalTaxRate;
	}
	public void setFinalTaxRate(Double finalTaxRate) {
		this.finalTaxRate = finalTaxRate;
	}
	public Double getFinalSgstAmount() {
		return finalSgstAmount;
	}
	public void setFinalSgstAmount(Double finalSgstAmount) {
		this.finalSgstAmount = finalSgstAmount;
	}
	public Double getFinalCgstAmount() {
		return finalCgstAmount;
	}
	public void setFinalCgstAmount(Double finalCgstAmount) {
		this.finalCgstAmount = finalCgstAmount;
	}
	public Double getFinalIgstAmount() {
		return finalIgstAmount;
	}
	public void setFinalIgstAmount(Double finalIgstAmount) {
		this.finalIgstAmount = finalIgstAmount;
	}
	public Double getFinalCessAmount() {
		return finalCessAmount;
	}
	public void setFinalCessAmount(Double finalCessAmount) {
		this.finalCessAmount = finalCessAmount;
	}
	public Double getTotalFinalTaxAmount() {
		return totalFinalTaxAmount;
	}
	public void setTotalFinalTaxAmount(Double totalFinalTaxAmount) {
		this.totalFinalTaxAmount = totalFinalTaxAmount;
	}
	public Double getFinalTaxAssesableAmount() {
		return finalTaxAssesableAmount;
	}
	public void setFinalTaxAssesableAmount(Double finalTaxAssesableAmount) {
		this.finalTaxAssesableAmount = finalTaxAssesableAmount;
	}
	public Double getNetFinalAmount() {
		return netFinalAmount;
	}
	public void setNetFinalAmount(Double netFinalAmount) {
		this.netFinalAmount = netFinalAmount;
	}
	public DoubleProperty getSalesTaxRateProperty() {
		
		salesTaxRateProperty.set(Math.round(salesTaxRate));
		return salesTaxRateProperty;
	}
	public void setSalesTaxRateProperty(DoubleProperty salesTaxRateProperty) {
		this.salesTaxRateProperty = salesTaxRateProperty;
	}
	public DoubleProperty getSalesSgstAmountProperty() {
		
		salesSgstAmountProperty.set(Math.round(salesSgstAmount));
		return salesSgstAmountProperty;
	}
	public void setSalesSgstAmountProperty(DoubleProperty salesSgstAmountProperty) {
		this.salesSgstAmountProperty = salesSgstAmountProperty;
	}
	public DoubleProperty getSalesCgstAmountProperty() {
		salesCgstAmountProperty.set(Math.round(salesCgstAmount));
		
		return salesCgstAmountProperty;
	}
	public void setSalesCgstAmountProperty(DoubleProperty salesCgstAmountProperty) {
		this.salesCgstAmountProperty = salesCgstAmountProperty;
	}
	public DoubleProperty getSalesIgstAmountProperty() {
		salesIgstAmountProperty.set(Math.round(salesIgstAmount));
		return salesIgstAmountProperty;
	}
	public void setSalesIgstAmountProperty(DoubleProperty salesIgstAmountProperty) {
		this.salesIgstAmountProperty = salesIgstAmountProperty;
	}
	
	
	
	public DoubleProperty getSalesCessAmountProperty() {
		if(null==salesCessAmount) {		
			salesCessAmountProperty.set(0.0);
		}else {
			salesCessAmountProperty.set(Math.round(salesCessAmount));
		}
		return salesCessAmountProperty;
	}
	public void setSalesCessAmountProperty(DoubleProperty salesCessAmountProperty) {
		
		
		this.salesCessAmountProperty = salesCessAmountProperty;
		
	}
	public DoubleProperty getTotalSaleTaxAmountProperty() {
		totalSaleTaxAmountProperty.set(Math.round(totalSaleTaxAmount));
		return totalSaleTaxAmountProperty;
	}
	public void setTotalSaleTaxAmountProperty(DoubleProperty totalSaleTaxAmountProperty) {
		this.totalSaleTaxAmountProperty = totalSaleTaxAmountProperty;
	}
	public DoubleProperty getSalesTaxAssesableAmountProperty() {
		salesTaxAssesableAmountProperty.set(Math.round(salesTaxAssesableAmount));
		return salesTaxAssesableAmountProperty;
	}
	public void setSalesTaxAssesableAmountProperty(DoubleProperty salesTaxAssesableAmountProperty) {
		this.salesTaxAssesableAmountProperty = salesTaxAssesableAmountProperty;
	}
	public DoubleProperty getNetSalesAmountProperty() {
		if(null==netSalesAmount) {
			netSalesAmountProperty.set(0.0);
		}else {
		netSalesAmountProperty.set(Math.round(netSalesAmount));
		}
		return netSalesAmountProperty;
	}
	public void setNetSalesAmountProperty(DoubleProperty netSalesAmountProperty) {
		this.netSalesAmountProperty = netSalesAmountProperty;
	}
	public DoubleProperty getPurchaseSgstAmountProperty() {
		if(null==purchaseSgstAmount) {
			purchaseSgstAmountProperty.set(0.0);
		}else {
			purchaseSgstAmountProperty.set(Math.round(purchaseSgstAmount));
		}
		
		return purchaseSgstAmountProperty;
	}
	public void setPurchaseSgstAmountProperty(DoubleProperty purchaseSgstAmountProperty) {
		this.purchaseSgstAmountProperty = purchaseSgstAmountProperty;
	}
	public DoubleProperty getPurchaseTaxRateProperty() {
		if(null==purchaseTaxRate) {
			purchaseTaxRateProperty.set(0.0);
		}else {
			purchaseTaxRateProperty.set(Math.round(purchaseTaxRate));
		}
		return purchaseTaxRateProperty;
	}
	public void setPurchaseTaxRateProperty(DoubleProperty purchaseTaxRateProperty) {
		this.purchaseTaxRateProperty = purchaseTaxRateProperty;
	}
	public DoubleProperty getPurchaseCgstAmountProperty() {
		if(null== purchaseCgstAmount) {
			purchaseCgstAmountProperty.set(0.0);
		}else {
			purchaseCgstAmountProperty.set(Math.round(purchaseCgstAmount));
		}
		return purchaseCgstAmountProperty;
	}
	public void setPurchaseCgstAmountProperty(DoubleProperty purchaseCgstAmountProperty) {
		this.purchaseCgstAmountProperty = purchaseCgstAmountProperty;
	}
	public DoubleProperty getPurchaseIgstAmountProperty() {
		if(null==purchaseIgstAmount) {
			purchaseIgstAmountProperty.set(0.0);
		}else {
			purchaseIgstAmountProperty.set(Math.round(purchaseIgstAmount));
		}
		return purchaseIgstAmountProperty;
	}
	public void setPurchaseIgstAmountProperty(DoubleProperty purchaseIgstAmountProperty) {
		this.purchaseIgstAmountProperty = purchaseIgstAmountProperty;
	}
	public DoubleProperty getNetPurchaseAmountProperty() {
		if(null==netPurchaseAmount) {
			netPurchaseAmountProperty.set(0.0);
		}else {
			netPurchaseAmountProperty.set(Math.round(netPurchaseAmount));
		}
	
		return netPurchaseAmountProperty;
	}
	public void setNetPurchaseAmountProperty(DoubleProperty netPurchaseAmountProperty) {
		this.netPurchaseAmountProperty = netPurchaseAmountProperty;
	}
	public DoubleProperty getPurchaseTaxAssesableAmountProperty() {
		if(null==purchaseTaxAssesableAmount) {
			purchaseTaxAssesableAmountProperty.set(0.0);
		}else {
			purchaseTaxAssesableAmountProperty.set(Math.round(purchaseTaxAssesableAmount));
		}
	
		return purchaseTaxAssesableAmountProperty;
	}
	public void setPurchaseTaxAssesableAmountProperty(DoubleProperty purchaseTaxAssesableAmountProperty) {
		this.purchaseTaxAssesableAmountProperty = purchaseTaxAssesableAmountProperty;
	}
	public DoubleProperty getTotalPurchaseTaxAmountProperty() {
		if(null==totalPurchaseTaxAmount) {
			totalPurchaseTaxAmountProperty.set(0.0);
		}else {
			totalPurchaseTaxAmountProperty.set(Math.round(totalPurchaseTaxAmount));
		}
		
		return totalPurchaseTaxAmountProperty;
	}
	public void setTotalPurchaseTaxAmountProperty(DoubleProperty totalPurchaseTaxAmountProperty) {
		this.totalPurchaseTaxAmountProperty = totalPurchaseTaxAmountProperty;
	}
	public DoubleProperty getPurchaseCessAmountProperty() {
		
		if(null==purchaseCessAmount) {
			purchaseCessAmountProperty.set(0.0);
		}else {
			purchaseCessAmountProperty.set(Math.round(purchaseCessAmount));
		}
		return purchaseCessAmountProperty;
	}
	public void setPurchaseCessAmountProperty(DoubleProperty purchaseCessAmountProperty) {
		this.purchaseCessAmountProperty = purchaseCessAmountProperty;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "TaxReport [salesTaxRate=" + salesTaxRate + ", salesSgstAmount=" + salesSgstAmount + ", salesCgstAmount="
				+ salesCgstAmount + ", salesIgstAmount=" + salesIgstAmount + ", salesCessAmount=" + salesCessAmount
				+ ", totalSaleTaxAmount=" + totalSaleTaxAmount + ", salesTaxAssesableAmount=" + salesTaxAssesableAmount
				+ ", netSalesAmount=" + netSalesAmount + ", purchaseTaxRate=" + purchaseTaxRate
				+ ", purchaseSgstAmount=" + purchaseSgstAmount + ", purchaseCgstAmount=" + purchaseCgstAmount
				+ ", purchaseIgstAmount=" + purchaseIgstAmount + ", purchaseCessAmount=" + purchaseCessAmount
				+ ", totalPurchaseTaxAmount=" + totalPurchaseTaxAmount + ", purchaseTaxAssesableAmount="
				+ purchaseTaxAssesableAmount + ", netPurchaseAmount=" + netPurchaseAmount + ", finalTaxRate="
				+ finalTaxRate + ", finalSgstAmount=" + finalSgstAmount + ", finalCgstAmount=" + finalCgstAmount
				+ ", finalIgstAmount=" + finalIgstAmount + ", finalCessAmount=" + finalCessAmount
				+ ", totalFinalTaxAmount=" + totalFinalTaxAmount + ", finalTaxAssesableAmount="
				+ finalTaxAssesableAmount + ", netFinalAmount=" + netFinalAmount + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
}
