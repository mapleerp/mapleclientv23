package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EnquiryPopUp {

	String itemName;
	String voucherType;
	String unitName;
	Double mrp;
	Double qty;
	String itemId;
	String unitId;
	String batch;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty voucherTypeProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private DoubleProperty mrpProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	
	public EnquiryPopUp() {
		
		this.itemNameProperty = new SimpleStringProperty("");
		this.voucherTypeProperty =new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.mrpProperty = new SimpleDoubleProperty(0.0);
		this.qtyProperty = new SimpleDoubleProperty(0.0);
	}
//	@JsonIgnore
//	public StringProperty getitemNameProperty() {
//		itemNameProperty.set(itemName);
//		return itemNameProperty;
//	}
//
//	public void setitemNameProperty(StringProperty itemNameProperty) {
//		this.itemNameProperty = itemNameProperty;
//	}
//	@JsonIgnore
//	public StringProperty getvoucherTypeProperty() {
//		voucherTypeProperty.set(voucherType);
//		return voucherTypeProperty;
//	}
//
//	public void setvoucherTypeProperty(StringProperty voucherTypeProperty) {
//		this.voucherTypeProperty = voucherTypeProperty;
//	}
//	@JsonIgnore
//	public DoubleProperty getmrpProperty() {
//		mrpProperty.set(mrp);
//		return mrpProperty;
//	}
//
//	public void setmrpProperty(DoubleProperty mrpProperty) {
//		this.mrpProperty = mrpProperty;
//	}
//	@JsonIgnore
//	public StringProperty getunitNameProperty() {
//		unitNameProperty.set(unitName);
//		return unitNameProperty;
//	}
//
//	public void setunitNameProperty(StringProperty unitNameProperty) {
//		this.unitNameProperty = unitNameProperty;
//	}
//	@JsonIgnore
//	public DoubleProperty getqtyProperty() {
//		qtyProperty.set(qty);
//		return qtyProperty;
//	}
//
//	public void setqtyProperty(DoubleProperty qtyProperty) {
//		this.qtyProperty = qtyProperty;
//	}
//	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public StringProperty getVoucherTypeProperty() {
		voucherTypeProperty.set(voucherType);
		return voucherTypeProperty;
	}
	public void setVoucherTypeProperty(StringProperty voucherTypeProperty) {
		this.voucherTypeProperty = voucherTypeProperty;
	}
	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}
	public DoubleProperty getMrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}
	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "EnquiryPopUp [itemName=" + itemName + ", voucherType=" + voucherType + ", unitName=" + unitName
				+ ", mrp=" + mrp + ", qty=" + qty + ", itemId=" + itemId + ", unitId=" + unitId + ", batch=" + batch
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
}
