package com.maple.mapleclient.entity;

public class KitValidationReport {


	Double qtyInMachineUnit;
	String errorMessage;
	 String machineUnit;
	 private String  processInstanceId;
		private String taskId;
	public Double getQtyInMachineUnit() {
		return qtyInMachineUnit;
	}
	public void setQtyInMachineUnit(Double qtyInMachineUnit) {
		this.qtyInMachineUnit = qtyInMachineUnit;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getMachineUnit() {
		return machineUnit;
	}
	public void setMachineUnit(String machineUnit) {
		this.machineUnit = machineUnit;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "KitValidationReport [qtyInMachineUnit=" + qtyInMachineUnit + ", errorMessage=" + errorMessage
				+ ", machineUnit=" + machineUnit + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	 
	 
}
