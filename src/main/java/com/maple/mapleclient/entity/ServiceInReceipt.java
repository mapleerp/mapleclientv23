package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ServiceInReceipt implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String Id;
	String receiptMode;
	String branchCode;
	String userId;
	Double receiptAmount;
	java.util.Date rereceiptDate;
	
	String accountId;
	
	String voucherNumber;
	
	ServiceInHdr serviceInHdr;
	private String  processInstanceId;
	private String taskId;
	
	CompanyMst companyMst;

	public ServiceInReceipt() {
		this.receiptAmountProperty = new SimpleDoubleProperty();
		this.receiptModeProperty = new SimpleStringProperty();
	}
	@JsonIgnore
	private StringProperty receiptModeProperty;


	@JsonIgnore
	private DoubleProperty receiptAmountProperty;
	
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Double getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	public java.util.Date getRereceiptDate() {
		return rereceiptDate;
	}
	public void setRereceiptDate(java.util.Date rereceiptDate) {
		this.rereceiptDate = rereceiptDate;
	}

	
	
	public StringProperty getReceiptModeProperty() {
		receiptModeProperty.set(receiptMode);
		return receiptModeProperty;
	}
	public void setReceiptModeProperty(StringProperty receiptModeProperty) {
		this.receiptModeProperty = receiptModeProperty;
	}
	public DoubleProperty getReceiptAmountProperty() {
		receiptAmountProperty.set(receiptAmount);
		return receiptAmountProperty;
	}
	public void setReceiptAmountProperty(DoubleProperty receiptAmountProperty) {
		this.receiptAmountProperty = receiptAmountProperty;
	}
	
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	
	public ServiceInHdr getServiceInHdr() {
		return serviceInHdr;
	}
	public void setServiceInHdr(ServiceInHdr serviceInHdr) {
		this.serviceInHdr = serviceInHdr;
	}
	
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ServiceInReceipt [Id=" + Id + ", receiptMode=" + receiptMode + ", branchCode=" + branchCode
				+ ", userId=" + userId + ", receiptAmount=" + receiptAmount + ", rereceiptDate=" + rereceiptDate
				+ ", accountId=" + accountId + ", voucherNumber=" + voucherNumber + ", serviceInHdr=" + serviceInHdr
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ "]";
	}

	

	
	

}
