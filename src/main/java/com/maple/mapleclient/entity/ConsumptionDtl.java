package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ConsumptionDtl {
	  private String id;
		String itemId;
		String unitId;
		Double mrp;
		Double qty;
		Double amount;
		String batch;
		String reasonId;
		String store;
		
		private String  processInstanceId;
		private String taskId;
		
		public String getReasonId() {
			return reasonId;
		}


		public void setReasonId(String reasonId) {
			this.reasonId = reasonId;
		}


		ConsumptionHdr consumptionHdr;
		
		@JsonIgnore
		StringProperty reasonProperty;
		@JsonIgnore
		StringProperty itemNameProperty;
		
		@JsonIgnore
		StringProperty unitNameProperty;
		
		@JsonIgnore
		DoubleProperty mrpProperty;
		
		@JsonIgnore
		DoubleProperty qtyProperty;
		
		@JsonIgnore
		DoubleProperty amountProperty;
		
		@JsonIgnore
		String itemName;
		
		@JsonIgnore
		String unitName;
		
		@JsonIgnore
		String reason;
		@JsonIgnore
		private StringProperty storeNameProperty;
		public ConsumptionDtl() {
			
			this.itemNameProperty = new SimpleStringProperty();
			this.unitNameProperty = new SimpleStringProperty();
			this.mrpProperty = new SimpleDoubleProperty();
			this.qtyProperty =new SimpleDoubleProperty();
			this.amountProperty = new SimpleDoubleProperty();
			this.reasonProperty=new SimpleStringProperty();
			this.storeNameProperty = new SimpleStringProperty();
		}
		


		public String getStore() {
			return store;
		}


		public void setStore(String store) {
			this.store = store;
		}

		@JsonIgnore
		public StringProperty getStoreNameProperty() {
			if(null!=store) {
			storeNameProperty.set(store);
			}
			return storeNameProperty;
		}


		public void setStoreNameProperty(StringProperty storeNameProperty) {
			this.storeNameProperty = storeNameProperty;
		}


		public String getItemName() {
			return itemName;
		}


		public String getReason() {
			return reason;
		}


		public void setReason(String reason) {
			this.reason = reason;
		}


		public void setItemName(String itemName) {
			this.itemName = itemName;
		}


		public String getUnitName() {
			return unitName;
		}


		public void setUnitName(String unitName) {
			this.unitName = unitName;
		}


		public String getBatch() {
			return batch;
		}


		public void setBatch(String batch) {
			this.batch = batch;
		}

		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getItemId() {
			return itemId;
		}
		public void setItemId(String itemId) {
			this.itemId = itemId;
		}
		public String getUnitId() {
			return unitId;
		}
		public void setUnitId(String unitId) {
			this.unitId = unitId;
		}
		public Double getMrp() {
			return mrp;
		}
		public void setMrp(Double mrp) {
			this.mrp = mrp;
		}
		public Double getQty() {
			return qty;
		}
		public void setQty(Double qty) {
			this.qty = qty;
		}
		public Double getAmount() {
			return amount;
		}
		public void setAmount(Double amount) {
			this.amount = amount;
		}
		public ConsumptionHdr getConsumptionHdr() {
			return consumptionHdr;
		}
		public void setConsumptionHdr(ConsumptionHdr consumptionHdr) {
			this.consumptionHdr = consumptionHdr;
		}
		@JsonIgnore
		  public StringProperty getreasonProperty() {
			reasonProperty.set(reason);
				return reasonProperty;
			}
			public void setreasonProperty(String reason) {
				this.reason = reason;
			}
		@JsonIgnore
		  public StringProperty getitemNameProperty() {
			itemNameProperty.set(itemName);
				return itemNameProperty;
			}
			public void setitemNameProperty(String itemName) {
				this.itemName = itemName;
			}
			@JsonIgnore
			  public StringProperty getunitNameProperty() {
				unitNameProperty.set(unitName);
					return unitNameProperty;
				}
				public void setunitNameProperty(String unitName) {
					this.unitName = unitName;
				}
				@JsonIgnore
				  public DoubleProperty getmrpProperty() {
					mrpProperty.set(mrp);
						return mrpProperty;
				}
				public void setmrpProperty(Double mrp) {
						this.mrp = mrp;
				}
				@JsonIgnore
				 public DoubleProperty getqtyProperty() {
						qtyProperty.set(qty);
							return qtyProperty;
				}
				public void setqtyProperty(Double qty) {
							this.qty = qty;
				}
				@JsonIgnore
			 public DoubleProperty getamountProperty() {
				amountProperty.set(amount);
						return amountProperty;
				}
			public void setamountProperty(Double amount) {
								this.amount = amount;
			}
		
				
		

		public String getProcessInstanceId() {
			return processInstanceId;
		}


		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}


		public String getTaskId() {
			return taskId;
		}


		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}


		@Override
		public String toString() {
			return "ConsumptionDtl [id=" + id + ", itemId=" + itemId + ", unitId=" + unitId + ", mrp=" + mrp + ", qty="
					+ qty + ", amount=" + amount + ", batch=" + batch + ", reasonId=" + reasonId + ", store=" + store
					+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", consumptionHdr="
					+ consumptionHdr + ", reasonProperty=" + reasonProperty + ", itemNameProperty=" + itemNameProperty
					+ ", unitNameProperty=" + unitNameProperty + ", mrpProperty=" + mrpProperty + ", qtyProperty="
					+ qtyProperty + ", amountProperty=" + amountProperty + ", itemName=" + itemName + ", unitName="
					+ unitName + ", reason=" + reason + ", storeNameProperty=" + storeNameProperty + "]";
		}


		


}
