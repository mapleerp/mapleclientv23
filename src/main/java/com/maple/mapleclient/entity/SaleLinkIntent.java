package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
public class SaleLinkIntent {

	private String id;
	
	private String intentDtlId;

	private String salesDtlId;

	private String allocatedQty;
	private String  processInstanceId;
	private String taskId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIntentDtlId() {
		return intentDtlId;
	}

	public void setIntentDtlId(String intentDtlId) {
		this.intentDtlId = intentDtlId;
	}

	public String getSalesDtlId() {
		return salesDtlId;
	}

	public void setSalesDtlId(String salesDtlId) {
		this.salesDtlId = salesDtlId;
	}

	public String getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(String allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SaleLinkIntent [id=" + id + ", intentDtlId=" + intentDtlId + ", salesDtlId=" + salesDtlId
				+ ", allocatedQty=" + allocatedQty + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	

	

}
