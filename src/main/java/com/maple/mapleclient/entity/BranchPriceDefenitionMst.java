package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BranchPriceDefenitionMst {
	String id;
	String branchCode;
	String costPrice;
	String mrp;
	public BranchPriceDefenitionMst() {


		this.branchCodeProperty = new SimpleStringProperty();
		this.costPriceProperty = new SimpleStringProperty();
		this.mrpProperty=new SimpleStringProperty();
		this.idProperty=new SimpleStringProperty();
	}
	
	@JsonIgnore
	StringProperty branchCodeProperty;

	@JsonIgnore
	StringProperty costPriceProperty;
	@JsonIgnore
	StringProperty mrpProperty;

    @JsonIgnore
    StringProperty idProperty;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public String getMrp() {
		return mrp;
	}

	public void setMrp(String mrp) {
		this.mrp = mrp;
	}
	@JsonIgnore
	public StringProperty getBranchCodeProperty() {
		branchCodeProperty.set(branchCode);
		return branchCodeProperty;
	}

	
	public void setBranchCodeProperty(StringProperty branchCodeProperty) {
		this.branchCodeProperty = branchCodeProperty;
	}
@JsonIgnore
	public StringProperty getCostPriceProperty() {
	
	costPriceProperty.set(costPrice);
	
		return costPriceProperty;
	}

	public void setCostPriceProperty(StringProperty costPriceProperty) {
		this.costPriceProperty = costPriceProperty;
	}
	@JsonIgnore
	public StringProperty getMrpProperty() {
		mrpProperty.set(mrp);
		
		return mrpProperty;
		
	}

	public void setMrpProperty(StringProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}
	@JsonIgnore
	public StringProperty getIdProperty() {
		idProperty.set(id);
		return idProperty;
	}

	public void setIdProperty(StringProperty idProperty) {
		this.idProperty = idProperty;
	}

	@Override
	public String toString() {
		return "BranchPriceDefenitionMst [id=" + id + ", branchCode=" + branchCode + ", costPrice=" + costPrice
				+ ", mrp=" + mrp + ", branchCodeProperty=" + branchCodeProperty + ", costPriceProperty="
				+ costPriceProperty + ", mrpProperty=" + mrpProperty + ", idProperty=" + idProperty + "]";
	}
	
	
	
	
	
	
	
}
