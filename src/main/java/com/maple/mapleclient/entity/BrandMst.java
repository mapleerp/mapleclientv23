package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BrandMst {
	String id;
	String brandName;
	String branchCode;
	
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty brandNameProperty;
	
	
	
	public BrandMst() {
	
		this.brandNameProperty = new SimpleStringProperty();
	}
	@JsonIgnore
	 public StringProperty getbrandNameProperty() {
		brandNameProperty.set(brandName);
			return brandNameProperty;
		}
		
		public void setbrandNameProperty(String brandName) {
			this.brandName = brandName;
		}
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "BrandMst [id=" + id + ", brandName=" + brandName + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	
	 

}
