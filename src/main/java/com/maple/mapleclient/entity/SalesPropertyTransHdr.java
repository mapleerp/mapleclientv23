package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesPropertyTransHdr {
	private String id;
	String propertyId;
	String value;
	private String  processInstanceId;
	private String taskId;
	private SalesTransHdr salesTransHdr;
	
	String propertyName;
	@JsonIgnore
	StringProperty propertyNameProperty;
	
	@JsonIgnore
	StringProperty valueProperty;
	

	public SalesPropertyTransHdr() {
		this.propertyNameProperty = new SimpleStringProperty();
		this.valueProperty = new SimpleStringProperty();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	
	public StringProperty getpropertyNameProperty() {
		propertyNameProperty.set(propertyName);
		return propertyNameProperty;
	}

	public void setpropertyNameProperty(String propertyName) {
		this.propertyName = propertyName;
	}

	
	public StringProperty getvalueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}

	public void setvalueProperty(String value) {
		this.value = value;
	}

	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}

	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SalesPropertyTransHdr [id=" + id + ", propertyId=" + propertyId + ", value=" + value
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", salesTransHdr=" + salesTransHdr
				+ ", propertyName=" + propertyName + "]";
	}


	
}
