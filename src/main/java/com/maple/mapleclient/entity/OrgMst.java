package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class OrgMst {
	
      String id;
	  private String orgId;
	  private String orgName;
	  private String orgTypeId;
	  private String orgLocation;
	  private String orgLatitude;
	  private String orgLongtitude;
	  private Date orgCreationDate;
	  private int createdBy;
	  private String  orgParentId;
	  private int  myOrg;
	  private String  orgCheck;
	  private String  processInstanceId;
		private String taskId;
	
	  
	  @JsonIgnore
	    private StringProperty orgNameProperty;
	  @JsonIgnore
	    private StringProperty orgIdProperty;
	  @JsonIgnore
	    private StringProperty orgLocationProperty;
	  @JsonIgnore
	    private StringProperty orgLatitudeProperty;
	  @JsonIgnore
	    private StringProperty orgLongtitudeProperty;
		
		public OrgMst()
		{
			this.orgNameProperty= new SimpleStringProperty("");
			this.orgIdProperty= new SimpleStringProperty("");
			this.orgLocationProperty= new SimpleStringProperty("");
			this.orgLatitudeProperty= new SimpleStringProperty("");
			this.orgLongtitudeProperty= new SimpleStringProperty("");
			
		}
		  public StringProperty getOrgNameProperty() {
			  orgNameProperty.set(orgName);
			return orgNameProperty;
		}
		public void setOrgNameProperty(StringProperty orgNameProperty) {
			
			this.orgNameProperty = orgNameProperty;
		}

	   public StringProperty getOrgIdProperty() {
		   
		   orgIdProperty.set(orgId);
			return orgIdProperty;
		}
		public void setOrgIdProperty(StringProperty orgIdProperty) {
			this.orgIdProperty = orgIdProperty;
		}
		public StringProperty getOrgLocationProperty() {
			
			orgLocationProperty.set(orgLocation);
			return orgLocationProperty;
		}
		public void setOrgLocationProperty(StringProperty orgLocationProperty) {
			this.orgLocationProperty = orgLocationProperty;
		}
		public StringProperty getOrgLatitudeProperty() {
			
			orgLatitudeProperty.set(orgLatitude);
			return orgLatitudeProperty;
		}
		public void setOrgLatitudeProperty(StringProperty orgLatitudeProperty) {
			
			this.orgLatitudeProperty = orgLatitudeProperty;
		}
		public StringProperty getOrgLongtitudeProperty() {
			orgLongtitudeProperty.set(orgLongtitude);
			return orgLongtitudeProperty;
		}
		public void setOrgLongtitudeProperty(StringProperty orgLongtitudeProperty) {
			this.orgLongtitudeProperty = orgLongtitudeProperty;
		}
		
		
		  String branchCode;
			public String getBranchCode() {
				return branchCode;
			}
			public void setBranchCode(String branchCode) {
				this.branchCode = branchCode;
			}
			
	public String getOrgCheck() {
				return orgCheck;
			}
			public void setOrgCheck(String orgCheck) {
				this.orgCheck = orgCheck;
			}
	public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getOrgTypeId() {
		return orgTypeId;
	}
	public void setOrgTypeId(String orgTypeId) {
		this.orgTypeId = orgTypeId;
	}
	public String getOrgLocation() {
		return orgLocation;
	}
	public void setOrgLocation(String orgLocation) {
		this.orgLocation = orgLocation;
	}
	public String getOrgLatitude() {
		return orgLatitude;
	}
	public void setOrgLatitude(String orgLatitude) {
		this.orgLatitude = orgLatitude;
	}
	public String getOrgLongtitude() {
		return orgLongtitude;
	}
	public void setOrgLongtitude(String orgLongtitude) {
		this.orgLongtitude = orgLongtitude;
	}
	public Date getOrgCreationDate() {
		return orgCreationDate;
	}
	public void setOrgCreationDate(Date orgCreationDate) {
		this.orgCreationDate = orgCreationDate;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public String getOrgParentId() {
		return orgParentId;
	}
	public void setOrgParentId(String orgParentId) {
		this.orgParentId = orgParentId;
	}
	public int getMyOrg() {
		return myOrg;
	}
	public void setMyOrg(int myOrg) {
		this.myOrg = myOrg;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "OrgMst [id=" + id + ", orgId=" + orgId + ", orgName=" + orgName + ", orgTypeId=" + orgTypeId
				+ ", orgLocation=" + orgLocation + ", orgLatitude=" + orgLatitude + ", orgLongtitude=" + orgLongtitude
				+ ", orgCreationDate=" + orgCreationDate + ", createdBy=" + createdBy + ", orgParentId=" + orgParentId
				+ ", myOrg=" + myOrg + ", orgCheck=" + orgCheck + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", branchCode=" + branchCode + "]";
	}
	
	
	
}
