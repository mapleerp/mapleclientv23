package com.maple.mapleclient.entity;

import java.util.Date;

public class DamageHdr {
	String id;
	String voucheNumber;
	Date voucherDate;
	String branchCode;
	
	private String  processInstanceId;
	private String taskId;
	public String getBranchCode() {
		return branchCode;
	}

	

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getId() {
		return id;
	}
	
	public String getVoucheNumber() {
		return voucheNumber;
	}
	public void setVoucheNumber(String voucheNumber) {
		this.voucheNumber = voucheNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "DamageHdr [id=" + id + ", voucheNumber=" + voucheNumber + ", voucherDate=" + voucherDate
				+ ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}

}
