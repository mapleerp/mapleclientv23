package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InsuranceCompanyMst {
	String id;
	String insuranceCompanyName;

	String contactPerson;
	String phoneNumber;
	


	private String processInstanceId;
	private String taskId;
	@JsonIgnore
	StringProperty insuranceCompanyProperty;

	@JsonIgnore
	StringProperty contactPersonProperty;

	@JsonIgnore
	StringProperty phoneNumberProperty;
	

	public InsuranceCompanyMst() {
		this.insuranceCompanyProperty = new SimpleStringProperty();

		this.contactPersonProperty = new SimpleStringProperty();
		this.phoneNumberProperty = new SimpleStringProperty();
	}

	@JsonIgnore
	public StringProperty getinsuranceCompanyProperty() {
		insuranceCompanyProperty.set(insuranceCompanyName);
		return insuranceCompanyProperty;
	}

	public void setinsuranceCompanyProperty(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	@JsonIgnore
	public StringProperty getcontactPersonProperty() {
		contactPersonProperty.set(contactPerson);
		return contactPersonProperty;
	}

	public void setcontactPersonProperty(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	@JsonIgnore
	public StringProperty getphoneNumberProperty() {
		phoneNumberProperty.set(phoneNumber);
		return phoneNumberProperty;
	}

	public void setphoneNumberProperty(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}

	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	
	@JsonIgnore
	public StringProperty getInsuranceCompanyProperty() {
		return insuranceCompanyProperty;
	}

	public void setInsuranceCompanyProperty(StringProperty insuranceCompanyProperty) {
		this.insuranceCompanyProperty = insuranceCompanyProperty;
	}

	public StringProperty getContactPersonProperty() {
		return contactPersonProperty;
	}

	public void setContactPersonProperty(StringProperty contactPersonProperty) {
		this.contactPersonProperty = contactPersonProperty;
	}

	public StringProperty getPhoneNumberProperty() {
		return phoneNumberProperty;
	}

	public void setPhoneNumberProperty(StringProperty phoneNumberProperty) {
		this.phoneNumberProperty = phoneNumberProperty;
	}

	@Override
	public String toString() {
		return "InsuranceCompanyMst [id=" + id + ", insuranceCompanyName=" + insuranceCompanyName + ", contactPerson="
				+ contactPerson + ", phoneNumber=" + phoneNumber + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", insuranceCompanyProperty=" + insuranceCompanyProperty
				+ ", contactPersonProperty=" + contactPersonProperty + ", phoneNumberProperty=" + phoneNumberProperty
				+ "]";
	}
	


	

}
