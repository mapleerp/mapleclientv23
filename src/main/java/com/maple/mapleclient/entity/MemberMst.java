package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MemberMst {
	
	String branchCode;
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	
	 @JsonIgnore
	    private StringProperty memberIdProperty;
	  @JsonIgnore
	    private StringProperty memberNameProperty;
	  @JsonIgnore
	    private StringProperty familyIdProperty;
	  @JsonIgnore
	    private StringProperty sexProperty;
	  @JsonIgnore
	    private StringProperty emailProperty;
	  @JsonIgnore
	    private StringProperty mobileNoProperty;
	  @JsonIgnore
	    private StringProperty orgIdProperty;
	    String id;
		String memberId;
		String familyId;
		String memberName;
		String sex;  
		Date dob;
		Date dos;
		Date dowb;
		Date dohsb;
		Date dom;
		String previousCommunity;
		Date dateOfJoining;
		Date dod;
		String reasonOfDeath;
		Date dol;
		String reasonOfLeaving;
		String memberIdActive;
		String proffession;
		String mobileNo;
		Date dataAddedDate;
	  
		String dataAddedBy;
		String email;
		double totalOfferings;
		String orgId;
		String isAPaster;
		String headOfFamily;
		
		private String  processInstanceId;
		private String taskId;
	  public MemberMst()
		{
			this.memberIdProperty= new SimpleStringProperty("");
			this.memberNameProperty= new SimpleStringProperty("");
			this.familyIdProperty= new SimpleStringProperty("");
			this.sexProperty= new SimpleStringProperty("");
			this.emailProperty= new SimpleStringProperty("");
			this.mobileNoProperty= new SimpleStringProperty("");
			this.orgIdProperty= new SimpleStringProperty("");
		
		}
	  

	public StringProperty getFamilyIdProperty() {
		familyIdProperty.set(familyId);
		return familyIdProperty;
	}


	public void setFamilyIdProperty(StringProperty familyIdProperty) {
		this.familyIdProperty = familyIdProperty;
	}


	public StringProperty getSexProperty() {
		sexProperty.set(sex);
		return sexProperty;
	}


	public void setSexProperty(StringProperty sexProperty) {
		this.sexProperty = sexProperty;
	}

@JsonIgnore
	public StringProperty getEmailProperty() {
		emailProperty.set(email);
		return emailProperty;
	}


	public void setEmailProperty(StringProperty emailProperty) {
		this.emailProperty = emailProperty;
	}


	public StringProperty getMobileNoProperty() {
		mobileNoProperty.set(mobileNo);
		return mobileNoProperty;
	}


	public void setMobileNoProperty(StringProperty mobileNoProperty) {
		this.mobileNoProperty = mobileNoProperty;
	}


	public StringProperty getOrgIdProperty() {
		orgIdProperty.set(orgId);
		return orgIdProperty;
	}


	public void setOrgIdProperty(StringProperty orgIdProperty) {
		this.orgIdProperty = orgIdProperty;
	}


	public StringProperty getMemberIdProperty() {
		memberIdProperty.set(memberId);
		return memberIdProperty;
	}
	public void setMemberIdProperty(StringProperty memberIdProperty) {
		this.memberIdProperty = memberIdProperty;
	}
	public StringProperty getMemberNameProperty() {
		memberNameProperty.set(memberName);
		return memberNameProperty;
	}
	public void setMemberNameProperty(StringProperty memberNameProperty) {
		this.memberNameProperty = memberNameProperty;
	}


	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getFamilyId() {
		return familyId;
	}
	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Date getDos() {
		return dos;
	}
	public void setDos(Date dos) {
		this.dos = dos;
	}
	public Date getDowb() {
		return dowb;
	}
	public void setDowb(Date dowb) {
		this.dowb = dowb;
	}
	public Date getDohsb() {
		return dohsb;
	}
	public void setDohsb(Date dohsb) {
		this.dohsb = dohsb;
	}
	public Date getDom() {
		return dom;
	}
	public void setDom(Date dom) {
		this.dom = dom;
	}
	public String getPreviousCommunity() {
		return previousCommunity;
	}
	public void setPreviousCommunity(String previousCommunity) {
		this.previousCommunity = previousCommunity;
	}
	public Date getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	public Date getDod() {
		return dod;
	}
	public void setDod(Date dod) {
		this.dod = dod;
	}
	public String getReasonOfDeath() {
		return reasonOfDeath;
	}
	public void setReasonOfDeath(String reasonOfDeath) {
		this.reasonOfDeath = reasonOfDeath;
	}
	public Date getDol() {
		return dol;
	}
	public void setDol(Date dol) {
		this.dol = dol;
	}
	public String getReasonOfLeaving() {
		return reasonOfLeaving;
	}
	public void setReasonOfLeaving(String reasonOfLeaving) {
		this.reasonOfLeaving = reasonOfLeaving;
	}
	public String getMemberIdActive() {
		return memberIdActive;
	}
	public void setMemberIdActive(String memberIdActive) {
		this.memberIdActive = memberIdActive;
	}
	public String getProffession() {
		return proffession;
	}
	public void setProffession(String proffession) {
		this.proffession = proffession;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public Date getDataAddedDate() {
		return dataAddedDate;
	}
	public void setDataAddedDate(Date dataAddedDate) {
		this.dataAddedDate = dataAddedDate;
	}
	public String getDataAddedBy() {
		return dataAddedBy;
	}
	public void setDataAddedBy(String dataAddedBy) {
		this.dataAddedBy = dataAddedBy;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public double getTotalOfferings() {
		return totalOfferings;
	}
	public void setTotalOfferings(double totalOfferings) {
		this.totalOfferings = totalOfferings;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getIsAPaster() {
		return isAPaster;
	}
	public void setIsAPaster(String isAPaster) {
		this.isAPaster = isAPaster;
	}
	public String getHeadOfFamily() {
		return headOfFamily;
	}
	public void setHeadOfFamily(String headOfFamily) {
		this.headOfFamily = headOfFamily;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MemberMst [branchCode=" + branchCode + ", id=" + id + ", memberId=" + memberId + ", familyId="
				+ familyId + ", memberName=" + memberName + ", sex=" + sex + ", dob=" + dob + ", dos=" + dos + ", dowb="
				+ dowb + ", dohsb=" + dohsb + ", dom=" + dom + ", previousCommunity=" + previousCommunity
				+ ", dateOfJoining=" + dateOfJoining + ", dod=" + dod + ", reasonOfDeath=" + reasonOfDeath + ", dol="
				+ dol + ", reasonOfLeaving=" + reasonOfLeaving + ", memberIdActive=" + memberIdActive + ", proffession="
				+ proffession + ", mobileNo=" + mobileNo + ", dataAddedDate=" + dataAddedDate + ", dataAddedBy="
				+ dataAddedBy + ", email=" + email + ", totalOfferings=" + totalOfferings + ", orgId=" + orgId
				+ ", isAPaster=" + isAPaster + ", headOfFamily=" + headOfFamily + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}


	
	
}
