package com.maple.mapleclient.entity;

public class PaymentReports {

	
	String voucherNumber;
	String voucherDate;;
	String voucherType;
	String creditAccountId;
	String accountId;
	String instrumentNumber;
	String bankAccountNumber;
	String modeOfPayment;
	Double amount;
	private String  processInstanceId;
	private String taskId;
	
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getCreditAccountId() {
		return creditAccountId;
	}
	public void setCreditAccountId(String creditAccountId) {
		this.creditAccountId = creditAccountId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PaymentReports [voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", voucherType="
				+ voucherType + ", creditAccountId=" + creditAccountId + ", accountId=" + accountId
				+ ", instrumentNumber=" + instrumentNumber + ", bankAccountNumber=" + bankAccountNumber
				+ ", modeOfPayment=" + modeOfPayment + ", amount=" + amount + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
}
