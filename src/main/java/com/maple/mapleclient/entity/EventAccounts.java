package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;



public class EventAccounts 
{

   
	String incomeHead;
    double incomeAmount;
	String expenseHead;
	double expenseAmount;
	Date transDate;
	String dataEnteredBy;
    String recordId;
    String eventId;
    String voucherNumber;
    String orgId;
    private String  processInstanceId;
	private String taskId;
	public String getIncomeHead() {
		return incomeHead;
	}
	public void setIncomeHead(String incomeHead) {
		this.incomeHead = incomeHead;
	}
	public double getIncomeAmount() {
		return incomeAmount;
	}
	public void setIncomeAmount(double incomeAmount) {
		this.incomeAmount = incomeAmount;
	}
	public String getExpenseHead() {
		return expenseHead;
	}
	public void setExpenseHead(String expenseHead) {
		this.expenseHead = expenseHead;
	}
	public double getExpenseAmount() {
		return expenseAmount;
	}
	public void setExpenseAmount(double expenseAmount) {
		this.expenseAmount = expenseAmount;
	}
	public Date getTransDate() {
		return transDate;
	}
	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}
	public String getDataEnteredBy() {
		return dataEnteredBy;
	}
	public void setDataEnteredBy(String dataEnteredBy) {
		this.dataEnteredBy = dataEnteredBy;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
    
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "EventAccounts [incomeHead=" + incomeHead + ", incomeAmount=" + incomeAmount + ", expenseHead="
				+ expenseHead + ", expenseAmount=" + expenseAmount + ", transDate=" + transDate + ", dataEnteredBy="
				+ dataEnteredBy + ", recordId=" + recordId + ", eventId=" + eventId + ", voucherNumber=" + voucherNumber
				+ ", orgId=" + orgId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
	
	
}
