package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemWiseDtlReport {

	String companyName;
	String branchName;
	String branchAddress;
	String branchPhoneNo;
	String branchState;
	String itemName;
	Double qty;
	Double value;

	String branchEmail;
	String branchWebsite;
	
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty valueProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;
	
	
	

	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public ItemWiseDtlReport() {
		
		this.itemNameProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.voucherDate= new SimpleObjectProperty<LocalDate>(null);
		this.valueProperty = new SimpleDoubleProperty(0.0);
		
	}
	public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
		return voucherDate;
	}
 
	public void setvoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	
	@JsonProperty("voucherDate")
	public java.sql.Date getvoucherDate( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}
	
   public void setvoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}
	
	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	
	
	
	public DoubleProperty getValueProperty() {
		
		valueProperty.set(value);
		return valueProperty;
	}
	public void setValueProperty(Double value) {
		this.value = value;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public StringProperty getItemNameProperty() {
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public DoubleProperty getQtyProperty() {
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	
	

	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchPhoneNo() {
		return branchPhoneNo;
	}
	public void setBranchPhoneNo(String branchPhoneNo) {
		this.branchPhoneNo = branchPhoneNo;
	}
	public String getBranchState() {
		return branchState;
	}
	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}
	
	public void setValueProperty(DoubleProperty valueProperty) {
		this.valueProperty = valueProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ItemWiseDtlReport [companyName=" + companyName + ", branchName=" + branchName + ", branchAddress="
				+ branchAddress + ", branchPhoneNo=" + branchPhoneNo + ", branchState=" + branchState + ", itemName="
				+ itemName + ", qty=" + qty + ", value=" + value + ", branchEmail=" + branchEmail + ", branchWebsite="
				+ branchWebsite + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
}
