package com.maple.mapleclient.entity;





import java.sql.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.module.SimpleAbstractTypeResolver;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class ItemMergeMst {

	String id;
	String fromItemName;
	String toItemName;
    String fromItemId;
	String toItemId;
	String toItemBarCode;
	String fromItemBarCode;
	String userId;
	Date voucherDate;
	String branchCode;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	public ItemMergeMst() 
	{
		this.fromItemNameProperty = new SimpleStringProperty("");
		this.toItemNameProperty = new SimpleStringProperty("");
		this.toItemBarCodeProperty = new SimpleStringProperty("");
		this.fromItemBarCodeProperty = new SimpleStringProperty("");
		this.userNameProperty = new SimpleStringProperty("");
		
	}
	
	@JsonIgnore
	private StringProperty fromItemNameProperty;
	@JsonIgnore
	private StringProperty toItemNameProperty;
	@JsonIgnore
	private StringProperty toItemBarCodeProperty;
	@JsonIgnore
	private StringProperty fromItemBarCodeProperty;
	@JsonIgnore
	private StringProperty userNameProperty;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFromItemName() {
		return fromItemName;
	}
	public void setFromItemName(String fromItemName) {
		this.fromItemName = fromItemName;
	}
	public String getToItemName() {
		return toItemName;
	}
	public void setToItemName(String toItemName) {
		this.toItemName = toItemName;
	}
	public String getFromItemId() {
		return fromItemId;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public void setFromItemId(String fromItemId) {
		this.fromItemId = fromItemId;
	}
	public String getToItemId() {
		return toItemId;
	}
	public void setToItemId(String toItemId) {
		this.toItemId = toItemId;
	}
	public String getToItemBarCode() {
		return toItemBarCode;
	}
	public void setToItemBarCode(String toItemBarCode) {
		this.toItemBarCode = toItemBarCode;
	}
	public String getFromItemBarCode() {
		return fromItemBarCode;
	}
	public void setFromItemBarCode(String fromItemBarCode) {
		this.fromItemBarCode = fromItemBarCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	
	
	
	public StringProperty getFromItemNameProperty() {
		fromItemNameProperty.set(fromItemName);
		return fromItemNameProperty;
	}
	public void setFromItemNameProperty(StringProperty fromItemNameProperty) {
		this.fromItemNameProperty = fromItemNameProperty;
	}
	public StringProperty getToItemNameProperty() {
		toItemNameProperty.set(toItemName);
		return toItemNameProperty;
	}
	public void setToItemNameProperty(StringProperty toItemNameProperty) {
		this.toItemNameProperty = toItemNameProperty;
	}
	public StringProperty getToItemBarCodeProperty() {
		toItemBarCodeProperty.set(toItemBarCode);
		return toItemBarCodeProperty;
	}
	public void setToItemBarCodeProperty(StringProperty toItemBarCodeProperty) {
		this.toItemBarCodeProperty = toItemBarCodeProperty;
	}
	public StringProperty getFromItemBarCodeProperty() {
		fromItemBarCodeProperty.set(fromItemBarCode);
		return fromItemBarCodeProperty;
	}
	public void setFromItemBarCodeProperty(StringProperty fromItemBarCodeProperty) {
		this.fromItemBarCodeProperty = fromItemBarCodeProperty;
	}
	
	
	public StringProperty getUserNameProperty() {
		userNameProperty.set(userId);
		return userNameProperty;
	}
	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ItemMergeMst [id=" + id + ", fromItemName=" + fromItemName + ", toItemName=" + toItemName
				+ ", fromItemId=" + fromItemId + ", toItemId=" + toItemId + ", toItemBarCode=" + toItemBarCode
				+ ", fromItemBarCode=" + fromItemBarCode + ", userId=" + userId + ", voucherDate=" + voucherDate
				+ ", branchCode=" + branchCode + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
	
}
