package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class NewDoctorMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	 private String id; 
	
	 String name;
	 String designation;
	 String phoneNumber;
	 String place;
		
		public NewDoctorMst() {


			
			this.nameProperty = new SimpleStringProperty();
			this.designationProperty=new SimpleStringProperty();
			this.phoneNumberProperty=new SimpleStringProperty();
			this.placeProperty=new SimpleStringProperty();
		}
		
		@JsonIgnore
		StringProperty nameProperty;
		@JsonIgnore 
		StringProperty designationProperty;
		@JsonIgnore
		StringProperty phoneNumberProperty;
		@JsonIgnore
		StringProperty placeProperty;

		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDesignation() {
			return designation;
		}
		public void setDesignation(String designation) {
			this.designation = designation;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getPlace() {
			return place;
		}
		public void setPlace(String place) {
			this.place = place;
		}
		public StringProperty getNameProperty() {
			nameProperty.set(name);
			return nameProperty;
		}
		public void setNameProperty(StringProperty nameProperty) {
			this.nameProperty = nameProperty;
		}
		public StringProperty getDesignationProperty() {
			designationProperty.set(designation);
			return designationProperty;
		}
		public void setDesignationProperty(StringProperty designationProperty) {
			this.designationProperty = designationProperty;
		}
		public StringProperty getPhoneNumberProperty() {
			phoneNumberProperty.set(phoneNumber);
			return phoneNumberProperty;
		}
		public void setPhoneNumberProperty(StringProperty phoneNumberProperty) {
			this.phoneNumberProperty = phoneNumberProperty;
		}
		public StringProperty getPlaceProperty() {
			placeProperty.set(place);
			return placeProperty;
		}
		public void setPlaceProperty(StringProperty placeProperty) {
			this.placeProperty = placeProperty;
		}
		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		@Override
		public String toString() {
			return "DoctorMst [id=" + id + ", name=" + name + ", designation=" + designation + ", phoneNumber="
					+ phoneNumber + ", place=" + place + ", nameProperty=" + nameProperty + ", designationProperty="
					+ designationProperty + ", phoneNumberProperty=" + phoneNumberProperty + ", placeProperty="
					+ placeProperty + "]";
		}
		
	
}
