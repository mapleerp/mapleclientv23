package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Date;

 

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class LoyalityVouchers  implements Serializable  {
	
	private static final long serialVersionUID = 1L;


	String id;
	
	String voucherNumber;
	String voucherLabel;
	Date expiryDate;
	Double percentage;
	Double amount;
	String cardType;
	Double minAmountForRedemption;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty voucherLabelProperty;
	
	@JsonIgnore
	private StringProperty expiryDateProperty;
	
	@JsonIgnore
	private DoubleProperty percentageProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private StringProperty cardTypeProperty;
	
	@JsonIgnore
	private DoubleProperty minAmountForRedemptionProperty;
	
	public LoyalityVouchers() {
		this.voucherNumberProperty = new SimpleStringProperty();
		this.voucherLabelProperty =new SimpleStringProperty();
		this.expiryDateProperty = new SimpleStringProperty();
		this.percentageProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.cardTypeProperty =new SimpleStringProperty();
		this.minAmountForRedemptionProperty = new SimpleDoubleProperty();
	}
	
	public StringProperty getVoucherNumberProperty() {
		voucherLabelProperty.set(voucherNumber);
		return voucherNumberProperty;
	}

	public void setVoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public StringProperty getVoucherLabelProperty() {
		voucherLabelProperty.set(voucherLabel);
		return voucherLabelProperty;
	}

	public void setVoucherLabelProperty(String voucherLabel) {
		this.voucherLabel = voucherLabel;
	}

	public StringProperty getExpiryDateProperty() {
		if(null != expiryDate)
		{
			expiryDateProperty.set(SystemSetting.UtilDateToString(expiryDate, "yyyy-MM-dd"));
		}
		return expiryDateProperty;
	}

	public void setExpiryDateProperty(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public DoubleProperty getPercentageProperty() {
		if(null != percentage)
		{
			percentageProperty.set(percentage);
		}
		return percentageProperty;
	}

	public void setPercentageProperty(Double percentage) {
		this.percentage = percentage;
	}

	public DoubleProperty getAmountProperty() {
		if(null != amount)
		{
			amountProperty.set(amount);
		}
		return amountProperty;
	}

	public void setAmountProperty(Double amount) {
		this.amount = amount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public StringProperty getcardTypeProperty() {
		cardTypeProperty.set(cardType);
		return cardTypeProperty;
	}

	public void setcardTypeProperty(String cardType) {
		this.cardType = cardType;
	}

	public DoubleProperty getMinAmountForRedemptionProperty() {
		if(null != minAmountForRedemption)
		{
			minAmountForRedemptionProperty.set(minAmountForRedemption);
		}
		return minAmountForRedemptionProperty;
	}

	public void setMinAmountForRedemptionProperty(Double minAmountForRedemption) {
		this.minAmountForRedemption = minAmountForRedemption;
	}

	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherLabel() {
		return voucherLabel;
	}
	public void setVoucherLabel(String voucherLabel) {
		this.voucherLabel = voucherLabel;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getPercentage() {
		return percentage;
	}
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public Double getMinAmountForRedemption() {
		return minAmountForRedemption;
	}
	public void setMinAmountForRedemption(Double minAmountForRedemption) {
		this.minAmountForRedemption = minAmountForRedemption;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "LoyalityVouchers [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherLabel=" + voucherLabel
				+ ", expiryDate=" + expiryDate + ", percentage=" + percentage + ", amount=" + amount + ", cardType="
				+ cardType + ", minAmountForRedemption=" + minAmountForRedemption + ", branchCode=" + branchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	
}
