package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

public class SysDateMst {
	
	private String id;
	private Date aplicationDate;
	private Date systemDate;
	private String dayEndDone;
	private String branchCode;
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getAplicationDate() {
		return aplicationDate;
	}
	public void setAplicationDate(Date aplicationDate) {
		this.aplicationDate = aplicationDate;
	}
	public Date getSystemDate() {
		return systemDate;
	}
	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}
	public String getDayEndDone() {
		return dayEndDone;
	}
	public void setDayEndDone(String dayEndDone) {
		this.dayEndDone = dayEndDone;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SysDateMst [id=" + id + ", aplicationDate=" + aplicationDate + ", systemDate=" + systemDate
				+ ", dayEndDone=" + dayEndDone + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
