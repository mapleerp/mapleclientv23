package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrgReceiptReport {

	Date voucherDate;
	String VoucherNumber;
	Double amount;
	String voucherType;
	String memberNmae;
	private String  processInstanceId;
	private String taskId;
	
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty voucherTypeProperty;
	
	
	@JsonIgnore
	private StringProperty memberNmaeProperty;
	
	
	@JsonIgnore DoubleProperty amountProperty;
	
	public OrgReceiptReport() {
		
		this.voucherNumberProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();
	    this.voucherTypeProperty=new  SimpleStringProperty();
	    this.memberNmaeProperty=new  SimpleStringProperty();
	    this.amountProperty=new SimpleDoubleProperty();
	}
	
	
	
	
	public Date getVoucherDate() {
		return voucherDate;
	}




	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}




	public String getVoucherNumber() {
		return VoucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		VoucherNumber = voucherNumber;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getMemberNmae() {
		return memberNmae;
	}
	public void setMemberNmae(String memberNmae) {
		this.memberNmae = memberNmae;
	}

	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set( VoucherNumber);
		return voucherNumberProperty;
	}

	
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	@JsonIgnore
	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		return voucherDateProperty;
	}
	@JsonIgnore
	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}

	@JsonIgnore
	public StringProperty getVoucherTypeProperty() {
		voucherTypeProperty.set(voucherType);
		return voucherTypeProperty;
	}

	
	public void setVoucherTypeProperty(StringProperty voucherTypeProperty) {
		this.voucherTypeProperty = voucherTypeProperty;
	}
	@JsonIgnore
	public StringProperty getMemberNmaeProperty() {
		memberNmaeProperty.set(memberNmae);
		
		return memberNmaeProperty;
	}

	public void setMemberNmaeProperty(StringProperty memberNmaeProperty) {
		this.memberNmaeProperty = memberNmaeProperty;
	}
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}

	
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}




	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}




	public String getTaskId() {
		return taskId;
	}




	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}




	@Override
	public String toString() {
		return "OrgReceiptReport [voucherDate=" + voucherDate + ", VoucherNumber=" + VoucherNumber + ", amount="
				+ amount + ", voucherType=" + voucherType + ", memberNmae=" + memberNmae + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	}
