package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DamageDtl {

	String id;

	public DamageDtl() {

		this.itemNameProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.batchCodeProperty = new SimpleStringProperty();
		this.narrationProperty = new SimpleStringProperty();
		this.storeNameProperty = new SimpleStringProperty();
		this.reasonProperty = new SimpleStringProperty();

	}

	String itemId;
	String itemName;
	Double qty;
	String batchCode;
	String narration;
	String reason;
	String store;

	private String processInstanceId;
	private String taskId;
	DamageHdr damageHdr;

	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private DoubleProperty qtyProperty;
	@JsonIgnore
	private StringProperty batchCodeProperty;
	@JsonIgnore
	private StringProperty narrationProperty;
	@JsonIgnore
	private StringProperty storeNameProperty;
	@JsonIgnore
	private StringProperty reasonProperty;

	@JsonIgnore

	public StringProperty getReasonProperty() {
		reasonProperty.set(reason);
		return reasonProperty;
	}

	public void setReasonProperty(StringProperty reasonProperty) {
		this.reasonProperty = reasonProperty;
	}

	@JsonIgnore
	public StringProperty getStoreNameProperty() {

		storeNameProperty.set(store);

		return storeNameProperty;
	}

	public void setStoreNameProperty(StringProperty storeNameProperty) {
		this.storeNameProperty = storeNameProperty;
	}

	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}

	@JsonIgnore
	public StringProperty getBatchCodeProperty() {
		batchCodeProperty.set(batchCode);
		return batchCodeProperty;
	}

	public void setBatchCodeProperty(String batchCode) {
		this.batchCode = batchCode;
	}

	@JsonIgnore
	public StringProperty getnarrationProperty() {
		narrationProperty.set(narration);
		return narrationProperty;
	}

	public void setnarrationProperty(String narration) {
		this.narration = narration;
	}

	public DamageHdr getDamageHdr() {
		return damageHdr;
	}

	public void setDamageHdr(DamageHdr damageHdr) {
		this.damageHdr = damageHdr;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "DamageDtl [id=" + id + ", itemId=" + itemId + ", itemName=" + itemName + ", qty=" + qty + ", batchCode="
				+ batchCode + ", narration=" + narration + ", reason=" + reason + ", store=" + store
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", damageHdr=" + damageHdr
				+ ", itemNameProperty=" + itemNameProperty + ", qtyProperty=" + qtyProperty + ", batchCodeProperty="
				+ batchCodeProperty + ", narrationProperty=" + narrationProperty + ", storeNameProperty="
				+ storeNameProperty + ", reasonProperty=" + reasonProperty + "]";
	}

}
