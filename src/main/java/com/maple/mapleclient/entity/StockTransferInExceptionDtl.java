package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class StockTransferInExceptionDtl {
	
private String id;

String itemName;
String qty;
String batch;
String rate;
String itemCode;
String barcode;
String reason1;
String amount;
String unit;

@JsonIgnore
StringProperty itemNameProperty;
@JsonIgnore
StringProperty quantityProperty;
@JsonIgnore
StringProperty batchProperty;
@JsonIgnore
StringProperty unitidProperty;
@JsonIgnore
StringProperty ratesProperty;
@JsonIgnore
StringProperty amountsProperty;

@JsonIgnore
StringProperty barcodeProperty;
@JsonIgnore
StringProperty reasonProperty;

public StockTransferInExceptionDtl() {
	this.itemNameProperty=new SimpleStringProperty();
	this.quantityProperty = new SimpleStringProperty();
	this.batchProperty = new SimpleStringProperty();
	this.ratesProperty = new SimpleStringProperty();
	this.unitidProperty = new SimpleStringProperty();
	this.barcodeProperty = new SimpleStringProperty();
	this.reasonProperty = new SimpleStringProperty();
	
		
	
}

public String getUnit() {
	return unit;
}

public void setUnit(String unit) {
	this.unit = unit;
}

public StringProperty getUnitidProperty() {
	unitidProperty.set(unit);
	return unitidProperty;
}

public void setUnitidProperty(StringProperty unitidProperty) {
	this.unitidProperty = unitidProperty;
}

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public String getItemName() {
	return itemName;
}

public void setItemName(String itemName) {
	this.itemName = itemName;
}



public String getBatch() {
	return batch;
}

public void setBatch(String batch) {
	this.batch = batch;
}



public String getItemCode() {
	return itemCode;
}

public void setItemCode(String itemCode) {
	this.itemCode = itemCode;
}

public String getBarcode() {
	return barcode;
}

public void setBarcode(String barcode) {
	this.barcode = barcode;
}

@JsonIgnore
public StringProperty getAmountsProperty() {
	amountsProperty.set(amount);
	return amountsProperty;
}

public void setAmountsProperty(StringProperty amountsProperty) {
	this.amountsProperty = amountsProperty;
}

@JsonIgnore
public StringProperty getItemNameProperty() {
	itemNameProperty.set(itemName);
	return itemNameProperty;
}

public void setItemNameProperty(StringProperty itemNameProperty) {
	this.itemNameProperty = itemNameProperty;
}


@JsonIgnore
public StringProperty getBatchProperty() {
	batchProperty.set(batch);
	return batchProperty;
}

public void setBatchProperty(StringProperty batchProperty) {
	this.batchProperty = batchProperty;
}



@JsonIgnore

public StringProperty getBarcodeProperty() {
	return barcodeProperty;
}

public void setBarcodeProperty(StringProperty barcodeProperty) {
	this.barcodeProperty = barcodeProperty;
}
@JsonIgnore

public StringProperty getReasonProperty() {
	reasonProperty.set(reason1);
	return reasonProperty;
}

public void setReasonProperty(StringProperty reasonProperty) {
	this.reasonProperty = reasonProperty;
}

public String getReason1() {
	return reason1;
}

public void setReason1(String reason1) {
	this.reason1 = reason1;
}

public String getAmount() {
	return amount;
}

public void setAmount(String amount) {
	this.amount = amount;
}

public String getQty() {
	return qty;
}

public void setQty(String qty) {
	this.qty = qty;
}

public String getRate() {
	return rate;
}

public void setRate(String rate) {
	this.rate = rate;
}
@JsonIgnore
public StringProperty getQuantityProperty() {
	quantityProperty.set(qty);
	return quantityProperty;
}

public void setQuantityProperty(StringProperty quantityProperty) {
	this.quantityProperty = quantityProperty;
}
@JsonIgnore
public StringProperty getRatesProperty() {
	ratesProperty.set(rate);
	return ratesProperty;
}

public void setRatesProperty(StringProperty ratesProperty) {
	this.ratesProperty = ratesProperty;
}




}
