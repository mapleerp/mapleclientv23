package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AddKotTable {

	@JsonProperty
	String id;
	@JsonProperty
	String tableName;
	String status;
	String branchCode;
	
	private String  processInstanceId;
	private String taskId;
 
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	@JsonIgnore
	private StringProperty tableNameProperty;
	
	@JsonIgnore
	private StringProperty statusProperty;
 	
	public AddKotTable () {
		this.tableNameProperty = new SimpleStringProperty("");
		this.statusProperty = new SimpleStringProperty("");
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@JsonIgnore
	public StringProperty gettableNameProperty() {
		tableNameProperty.set(tableName);
		return tableNameProperty;
	}
	

	public void settableNameProperty(String tableName) {
		this.tableName = tableName;
	}
	@JsonIgnore
	public StringProperty getstatusProperty() {
		statusProperty.set(status);
		return statusProperty;
	}
	

	public void setstatusProperty(String status) {
		this.tableName = status;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "AddKotTable [id=" + id + ", tableName=" + tableName + ", status=" + status + ", branchCode="
				+ branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

 
}