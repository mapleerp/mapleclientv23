package com.maple.mapleclient.entity;

import java.io.Serializable;
 
import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WeighBridgeWeights implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
 
   private String id;
   private String  processInstanceId;
	private String taskId;
	
	@JsonProperty("machineweight")
	Integer machineweight;
	
	@JsonProperty("vehicleno")
	private String vehicleno;

	
	
	
	
	@JsonProperty("firstweightdate")
	private String firstweightdate;
	
	@JsonProperty("previousweight")
	private Integer previousweight;
	
	@JsonProperty("previousweightid")
	private String previousweightid;

	
	@JsonProperty("nextweight")
	private Integer nextweight;
	
	@JsonProperty("nextweightid")
	private String nextweightid;
	
	
	@JsonProperty("netweight")
	private String netweight;
	
	
	@JsonProperty("voucherNumber")
	private String voucherNumber;
	
	
	@JsonProperty("vehicletypeid")
	private String vehicletypeid;
	
	@JsonProperty("materialtypeid")
	private String materialtypeid;
	
	@JsonProperty("rate")
	private Integer rate;

	@JsonProperty("voucherDate")
	private LocalDateTime voucherDate;

	@JsonIgnore
	private StringProperty firstwtDateProperty;
 
	@JsonIgnore
	private SimpleIntegerProperty rateProperty;
	
	@JsonIgnore
	private SimpleStringProperty  vehiclenoProperty;
	
	@JsonIgnore
	private SimpleStringProperty netweightProperty;
	
	@JsonIgnore
	private SimpleIntegerProperty nextwtproperty;
	
	@JsonIgnore
	private SimpleIntegerProperty previousWtProperty;
	
	@JsonIgnore
	private SimpleStringProperty voucherDateProperty;
	
	@JsonIgnore
	private SimpleIntegerProperty machineWeightProperty;
	
	public WeighBridgeWeights() {
		this.vehiclenoProperty = new SimpleStringProperty();
		this.netweightProperty =  new SimpleStringProperty();
		this.voucherDateProperty =  new SimpleStringProperty();
		this.previousWtProperty =  new SimpleIntegerProperty();
		this.machineWeightProperty =  new SimpleIntegerProperty();
		this.rateProperty=new SimpleIntegerProperty();
		this.firstwtDateProperty = new SimpleStringProperty();
		this.nextwtproperty = new SimpleIntegerProperty();
	}

	


	public SimpleIntegerProperty getNextwtproperty() {
		if(null != nextweight)
		{
		nextwtproperty.set(nextweight);
		}
		return nextwtproperty;
	}

	public void setNextwtproperty(Integer nextweight) {
		this.nextweight = nextweight;
	}

	public StringProperty getFirstwtDateProperty() {
		firstwtDateProperty.set(firstweightdate);
		return firstwtDateProperty;
	}

	public void setFirstwtDateProperty(String firstweightdate) {
		this.firstweightdate = firstweightdate;
	}

	public StringProperty getvehiclenoProperty() {
		vehiclenoProperty.set(vehicleno);
		return vehiclenoProperty;
	}
	
	
	public void setvehiclenoProperty(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	public StringProperty getnetweightProperty() {
		netweightProperty.set(netweight);
		return netweightProperty;
	}
	
	
	public String getFirstweightdate() {
		return firstweightdate;
	}

	public void setFirstweightdate(String firstweightdate) {
		this.firstweightdate = firstweightdate;
	}

	public void setnetweightProperty(String netweight) {
		this.netweight = netweight;
	}
	
	public IntegerProperty getpreviousWtPropertyProperty() {
		if(null != previousweight)
		{
		previousWtProperty.set(previousweight);
		}
		return previousWtProperty;
	}
	
	
	public void setpreviousWtPropertyProperty(Integer previousweight) {
		this.previousweight = previousweight;
	}
	
	public IntegerProperty getMachineWeightProperty() {
		machineWeightProperty.set(machineweight);
		return machineWeightProperty;
	}
	
	
	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public void setMachineWeightProperty(Integer machineweight) {
		this.machineweight = machineweight;
	}
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(voucherDate.toString());
		return voucherDateProperty;
	}
	
	
	public void setvoucherDateProperty(LocalDateTime voucherDate) {
		this.voucherDate = voucherDate;
	}

	LocalDateTime updatedTime;
	

 
	private BranchMst branchMst;
	
	
 
	private CompanyMst companyMst;


	

	public LocalDateTime getVoucherDate() {
		return voucherDate;
	}



	public void setVoucherDate(LocalDateTime voucherDate) {
		this.voucherDate = voucherDate;
	}



	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}



	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}


 


	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public Integer getMachineweight() {
		return machineweight;
	}



	public void setMachineweight(Integer machineweight) {
		this.machineweight = machineweight;
	}



	 


	public String getNetweight() {
		return netweight;
	}



	public void setNetweight(String netweight) {
		this.netweight = netweight;
	}



	public String getVehicletypeid() {
		return vehicletypeid;
	}



	public void setVehicletypeid(String vehicletypeid) {
		this.vehicletypeid = vehicletypeid;
	}



	public String getMaterialtypeid() {
		return materialtypeid;
	}



	public void setMaterialtypeid(String materialtypeid) {
		this.materialtypeid = materialtypeid;
	}



	public Integer getRate() {
		return rate;
	}



	public void setRate(Integer rate) {
		this.rate = rate;
	}


 


 


	public CompanyMst getCompanyMst() {
		return companyMst;
	}



	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}



	public BranchMst getBranchMst() {
		return branchMst;
	}



	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}


	

	public String getVehicleno() {
		return vehicleno;
	}



	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}



	public Integer getPreviousweight() {
		return previousweight;
	}



	public void setPreviousweight(Integer previousweight) {
		this.previousweight = previousweight;
	}



	public String getPreviousweightid() {
		return previousweightid;
	}



	public void setPreviousweightid(String previousweightid) {
		this.previousweightid = previousweightid;
	}



	public Integer getNextweight() {
		return nextweight;
	}



	public void setNextweight(Integer nextweight) {
		this.nextweight = nextweight;
	}



	public String getNextweightid() {
		return nextweightid;
	}



	public void setNextweightid(String nextweightid) {
		this.nextweightid = nextweightid;
	}


	
@JsonIgnore
	public SimpleIntegerProperty getRateProperty() {
	
	rateProperty.set(rate);
		return rateProperty;
	}

	public void setRateProperty(SimpleIntegerProperty rateProperty) {
		this.rateProperty = rateProperty;
	}





	public String getProcessInstanceId() {
		return processInstanceId;
	}




	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}




	public String getTaskId() {
		return taskId;
	}




	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}




	@Override
	public String toString() {
		return "WeighBridgeWeights [id=" + id + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", machineweight=" + machineweight + ", vehicleno=" + vehicleno + ", firstweightdate="
				+ firstweightdate + ", previousweight=" + previousweight + ", previousweightid=" + previousweightid
				+ ", nextweight=" + nextweight + ", nextweightid=" + nextweightid + ", netweight=" + netweight
				+ ", voucherNumber=" + voucherNumber + ", vehicletypeid=" + vehicletypeid + ", materialtypeid="
				+ materialtypeid + ", rate=" + rate + ", voucherDate=" + voucherDate + ", updatedTime=" + updatedTime
				+ ", branchMst=" + branchMst + ", companyMst=" + companyMst + "]";
	}

 

 	

	
	
	

}
