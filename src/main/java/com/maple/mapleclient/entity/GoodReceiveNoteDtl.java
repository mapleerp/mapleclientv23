package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GoodReceiveNoteDtl {
	
	String unitId;
	String purchaseOrderDtl;
	private Double fcMrp;
	private String itemId;
	private String id;
	private String processInstanceId;
	private String taskId;
	private Integer displaySerial;
	


	GoodReceiveNoteHdr goodReceiveNoteHdr;
	
	



	private Integer itemSerial;


	private String store;

	public GoodReceiveNoteDtl() {
		this.itemName = new SimpleStringProperty("");
		this.storeProperty=new SimpleStringProperty("");
		this.purchseRate = new SimpleDoubleProperty();
		this.batch = new SimpleStringProperty("");
		this.itemSerialNo = new SimpleStringProperty("");
		this.barcode = new SimpleStringProperty("");
		this.previousMRP = new SimpleDoubleProperty();
		this.expiryDate = new SimpleObjectProperty();
		String s = "2080-12-01";
		this.expiryDate.set(Date.valueOf(s).toLocalDate());
		this.freeQty = new SimpleIntegerProperty();
		this.qty = new SimpleDoubleProperty();
		this.changePriceStatus = new SimpleStringProperty("");
		this.unitProperty = new SimpleStringProperty("");
		this.mrp = new SimpleDoubleProperty();
		this.cessRate = new SimpleDoubleProperty();
		this.manufactureDate = new SimpleObjectProperty();
		this.manufactureDate.set(LocalDate.now());
		this.BinNo = new SimpleStringProperty("");
		this.GoodReceiveNoteDate = new SimpleObjectProperty();
		this.GoodReceiveNoteDate.set(LocalDate.now());
		this.idproperty = new SimpleStringProperty();

	}

	

	

	private DoubleProperty fcGoodReceiveNoteRateProperty;
	
	private DoubleProperty fcMrpProperty;
	
	
	@JsonIgnore
	private StringProperty idproperty;
	@JsonIgnore
	private StringProperty toBranchproperty;
	@JsonIgnore
	private StringProperty itemName;
	@JsonIgnore
	private StringProperty storeProperty;
	@JsonIgnore
	String unitName;
	@JsonIgnore
	private DoubleProperty purchseRate;


	@JsonIgnore
	private StringProperty batch;

	@JsonIgnore
	private StringProperty itemSerialNo;

	@JsonIgnore
	private StringProperty barcode;


	@JsonIgnore
	private DoubleProperty previousMRP;

	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDate;

	@JsonIgnore
	private IntegerProperty freeQty;

	@JsonIgnore
	private DoubleProperty qty;


	@JsonIgnore
	private StringProperty changePriceStatus;

	@JsonIgnore
	private StringProperty unitProperty;

	@JsonIgnore
	private DoubleProperty mrp;

	@JsonIgnore
	private DoubleProperty cessRate;

	@JsonIgnore
	private ObjectProperty<LocalDate> manufactureDate;

	// version1.7

	
	@JsonIgnore
	private StringProperty BinNo;

	@JsonIgnore
	private ObjectProperty<LocalDate> GoodReceiveNoteDate;

	
	
	
	

	public void setToBranchproperty(StringProperty toBranchproperty) {
		this.toBranchproperty = toBranchproperty;
	}

	public GoodReceiveNoteHdr getGoodReceiveNoteHdr() {
		return goodReceiveNoteHdr;
	}

	public void setGoodReceiveNoteHdr(GoodReceiveNoteHdr goodReceiveNoteHdr) {
		this.goodReceiveNoteHdr = goodReceiveNoteHdr;
	}

	public Double getFcMrp() {
		return fcMrp;
	}

	public void setFcMrp(Double fcMrp) {
		this.fcMrp = fcMrp;
	}

	@JsonIgnore
	public StringProperty getBinNoProperty() {
		return BinNo;
	}

	public void setBinNoProperty(StringProperty binNo) {
		BinNo = binNo;
	}

	@JsonProperty("BinNo")
	public String getBinNo() {
		return BinNo.get();
	}

	@JsonProperty("BinNo")
	public void setBinNo(String binNo) {
		this.BinNo.set(binNo);
	}

	@JsonIgnore

	public ObjectProperty<LocalDate> getexpiryDateProperty() {
		return expiryDate;
	}

	public void setexpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {

		this.expiryDate = expiryDate;
	}

	
	public StringProperty getStoreProperty() {
		if(null!=store) {
			storeProperty.set(store);
		}
		return storeProperty;
	}

	public void setStoreProperty(StringProperty storeProperty) {
		this.storeProperty = storeProperty;
	}

	

	


	

	@JsonProperty("expiryDate")

	public java.sql.Date getexpiryDate() {

		return Date.valueOf(this.expiryDate.get());
	}

	public void setexpiryDate(java.sql.Date expiryDate) {
		if (null != expiryDate)
			this.expiryDate.set(expiryDate.toLocalDate());

	}

	// version1.7

	public String getPurchaseOrderDtl() {
		return purchaseOrderDtl;
	}

	public void setPurchaseOrderDtl(String purchaseOrderDtl) {
		this.purchaseOrderDtl = purchaseOrderDtl;
	}

	// version1.7 end

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	@JsonIgnore
	public ObjectProperty<LocalDate> getGoodReceiveNoteDateProperty() {
		return GoodReceiveNoteDate;
	}

	public void setGoodReceiveNoteDateProperty(ObjectProperty<LocalDate> GoodReceiveNoteDate) {
		this.GoodReceiveNoteDate = GoodReceiveNoteDate;
	}

	@JsonProperty("GoodReceiveNoteDate")
	public java.sql.Date getGoodReceiveNoteDate() {
		return Date.valueOf(this.GoodReceiveNoteDate.get());
	}

	public void setGoodReceiveNoteDate(java.sql.Date GoodReceiveNoteDate) {
		this.GoodReceiveNoteDate.set(GoodReceiveNoteDate.toLocalDate());
	}

	@JsonIgnore

	public ObjectProperty<LocalDate> getmanufactureDateProperty() {
		return manufactureDate;
	}

	public void setmanufactureDateProperty(ObjectProperty<LocalDate> manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	@JsonProperty("manufactureDate")
	public java.sql.Date getmanufactureDate() {
		return Date.valueOf(this.manufactureDate.get());
	}

	public void setmanufactureDate(java.sql.Date manufactureDate) {
		if (null != manufactureDate)
			this.manufactureDate.set(manufactureDate.toLocalDate());
	}

	

	@JsonIgnore
	public DoubleProperty getfcMrpProperty() {
		if (null != fcMrp) {
			fcMrpProperty.set(fcMrp);
		}
		return fcMrpProperty;
	}

	public void setfcMrpProperty(Double fcMrp) {
		this.fcMrp = fcMrp;
	}

	
	public StringProperty getIdProperty() {
		idproperty.set(id);
		return idproperty;
	}

	public void setItdProperty(String id) {
		this.id = id;
	}

	public StringProperty getItemNameProperty() {
		return itemName;
	}

	public void setItemNameProperty(StringProperty itemName) {
		this.itemName = itemName;
	}

	@JsonProperty("itemName")
	public String getItemName() {
		return itemName.get();
	}

	@JsonProperty("itemName")
	public void setItemName(String itemName) {
		this.itemName.set(itemName);
	}

	@JsonIgnore
	public DoubleProperty getPurchseRateProperty() {
		return purchseRate;
	}

	public void setPurchseRateProperty(DoubleProperty purchseRate) {
		this.purchseRate = purchseRate;
	}

	@JsonProperty("purchseRate")
	public Double getPurchseRate() {
		return purchseRate.get();
	}

	@JsonProperty("purchseRate")
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate.set(purchseRate);
	}

	
	@JsonIgnore

	public StringProperty getBatchProperty() {
		return batch;
	}

	public void setBatchProperty(StringProperty batch) {
		this.batch = batch;
	}

	@JsonProperty("batch")
	public String getBatch() {
		return batch.get();
	}

	@JsonProperty("batch")
	public void setBatch(String batch) {
		this.batch.set(batch);
	}

	@JsonIgnore
	public StringProperty getItemSerialProperty() {
		if(null != displaySerial) {
			itemSerialNo.set(displaySerial+"");
		}
		return itemSerialNo;
	}

	public void setItemSerialProperty(StringProperty itemSerial) {
		this.itemSerialNo = itemSerial;
	}

	

	@JsonIgnore
	public StringProperty getBarcodepProperty() {
		return barcode;
	}

	public void setBarcodeProperty(StringProperty barcode) {
		this.barcode = barcode;
	}

	@JsonProperty("barcode")
	public String getBarcode() {
		return barcode.get();
	}

	@JsonProperty("barcode")
	public void setBarcode(String barcode) {
		this.barcode.set(barcode);
	}




	@JsonIgnore
	public DoubleProperty getPreviousMRProperty() {
		return previousMRP;
	}

	public void setPreviousMRPProperty(DoubleProperty previousMRP) {
		this.previousMRP = previousMRP;
	}

	@JsonProperty("previousMRP")
	public Double getPreviousMRP() {
		return previousMRP.get();
	}

	@JsonProperty("previousMRP")
	public void setPreviousMRP(Double previousMRP) {
		if (null != previousMRP) {
			this.previousMRP.set(previousMRP);
		}
	}

	@JsonIgnore
	public IntegerProperty getFreeQtyProperty() {
		return freeQty;
	}

	public void setFreeQty(IntegerProperty freeQty) {
		this.freeQty = freeQty;
	}

	@JsonProperty("freeQty")
	public Integer getFreeQty() {
		return freeQty.get();
	}

	@JsonProperty("freeQty")
	public void setFreeQty(Integer freeQty) {
		if (null != freeQty) {
			this.freeQty.set(freeQty);
		}
	}

	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		return qty;
	}

	public void setQtyProperty(DoubleProperty qty) {
		this.qty = qty;
	}

	@JsonProperty("qty")
	public Double getQty() {
		return qty.get();
	}

	@JsonProperty("qty")
	public void setQty(Double qty) {
		this.qty.set(qty);
	}


	@JsonIgnore
	public StringProperty getChangePriceStatusProperty() {
		return changePriceStatus;
	}

	public void setChangePriceStatusProperty(StringProperty changePriceStatus) {
		this.changePriceStatus = changePriceStatus;
	}

	@JsonProperty("changePriceStatus")
	public String getChangePriceStatus() {
		return changePriceStatus.get();
	}

	@JsonProperty("changePriceStatus")
	public void setChangePriceStatus(String changePriceStatus) {
		this.changePriceStatus.set(changePriceStatus);
	}

	/*
	 * @JsonIgnore public StringProperty getUnitProperty() { if(null!=unitName) {
	 * unit.set(unitName); }
	 * 
	 * return unit; }
	 * 
	 * public void setUnitProperty(String unitName) { this.unitName = unitName; }
	 */

	@JsonIgnore
	public StringProperty getUnitProperty() {
		if(null!= unitName) {
			unitProperty.set(unitName);
		}
		return unitProperty;
	}

	public void setUnitProperty(StringProperty unitProperty) {
		this.unitProperty = unitProperty;
	}

	@JsonIgnore
	public DoubleProperty getMrpProperty() {
		return mrp;
	}

	public void setMrpProperty(DoubleProperty mrp) {
		this.mrp = mrp;
	}

	@JsonProperty("mrp")
	public Double getMrp() {
		return mrp.get();
	}

	@JsonProperty("mrp")
	public void setMrp(Double mrp) {
		this.mrp.set(mrp);
	}

	@JsonIgnore
	public DoubleProperty getCessRateProperty() {
		return cessRate;
	}

	public void setCessRateProperty(DoubleProperty cessRate) {
		this.cessRate = cessRate;
	}

	@JsonProperty("cessRate")
	public Double getCessRate() {
		return cessRate.get();
	}

	@JsonProperty("cessRate")
	public void setCessRate(Double cessRate) {
		this.cessRate.set(cessRate);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Integer getDisplaySerial() {
		return displaySerial;
	}

	public void setDisplaySerial(Integer displaySerial) {
		this.displaySerial = displaySerial;
	}

	public Integer getItemSerial() {
		return itemSerial;
	}

	public void setItemSerial(Integer itemSerial) {
		this.itemSerial = itemSerial;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	@Override
	public String toString() {
		return "GoodReceiveNoteDtl [unitId=" + unitId + ", purchaseOrderDtl=" + purchaseOrderDtl + ", fcMrp=" + fcMrp
				+ ", itemId=" + itemId + ", id=" + id + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", displaySerial=" + displaySerial + ", goodReceiveNoteHdr=" + goodReceiveNoteHdr
				+ ", itemSerial=" + itemSerial + ", store=" + store + ", fcGoodReceiveNoteRateProperty="
				+ fcGoodReceiveNoteRateProperty + ", fcMrpProperty=" + fcMrpProperty + ", idproperty=" + idproperty
				+ ", toBranchproperty=" + toBranchproperty + ", itemName=" + itemName + ", storeProperty="
				+ storeProperty + ", unitName=" + unitName + ", purchseRate=" + purchseRate + ", batch=" + batch
				+ ", itemSerialNo=" + itemSerialNo + ", barcode=" + barcode + ", previousMRP=" + previousMRP
				+ ", expiryDate=" + expiryDate + ", freeQty=" + freeQty + ", qty=" + qty + ", changePriceStatus="
				+ changePriceStatus + ", unitProperty=" + unitProperty + ", mrp=" + mrp + ", cessRate=" + cessRate
				+ ", manufactureDate=" + manufactureDate + ", BinNo=" + BinNo + ", GoodReceiveNoteDate="
				+ GoodReceiveNoteDate + "]";
	}

	

	




	
}
