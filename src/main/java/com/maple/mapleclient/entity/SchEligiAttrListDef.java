package com.maple.mapleclient.entity;

import java.io.Serializable;
import org.springframework.stereotype.Component;

public class SchEligiAttrListDef implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	String  eligibilityId ;
	String attribName ;
	String attribType ;
	String branchCode ;
	private String  processInstanceId;
	private String taskId;
	
	
	
	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	private CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEligibilityId() {
		return eligibilityId;
	}

	public void setEligibilityId(String eligibilityId) {
		this.eligibilityId = eligibilityId;
	}

	public String getAttribName() {
		return attribName;
	}

	public void setAttribName(String attribName) {
		this.attribName = attribName;
	}

	public String getAttribType() {
		return attribType;
	}

	public void setAttribType(String attribType) {
		this.attribType = attribType;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SchEligiAttrListDef [id=" + id + ", eligibilityId=" + eligibilityId + ", attribName=" + attribName
				+ ", attribType=" + attribType + ", branchCode=" + branchCode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst + "]";
	}

	


	


}
