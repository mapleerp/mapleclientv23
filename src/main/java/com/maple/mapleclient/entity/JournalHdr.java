package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Date;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class JournalHdr implements Serializable{
	
	private static final long serialVersionUID = 1L;
	String id;
	String voucherNumber;
	Date voucherDate;
	Date transDate;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty voucherNoProperty;
	@JsonIgnore
    private	StringProperty  voucherDateProperty;
	
	@JsonIgnore
	 private StringProperty transDateProperty;
	
	public JournalHdr() {
		this.voucherNoProperty= new SimpleStringProperty("");
		
		this.voucherDateProperty=new SimpleStringProperty("");
		
		this.transDateProperty=new SimpleStringProperty("");
	}
	public StringProperty getVoucherNoProperty() {
		voucherNoProperty.set(voucherNumber);
		return voucherNoProperty;
	}
	
	public void setVoucherNoProperty(StringProperty voucherNoProperty) {
		this.voucherNoProperty = voucherNoProperty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	public Date getTransDate() {
		return transDate;
	}
	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	@JsonIgnore
	public StringProperty getVoucherDateProperty() {
		
		voucherDateProperty.set(voucherDate.toString());
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}
	@JsonIgnore
	public StringProperty getTransDateProperty() {
		
		transDateProperty.set(transDate.toString());	
		
		return transDateProperty;
	}
	public void setTransDateProperty(StringProperty transDateProperty) {
		this.transDateProperty = transDateProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "JournalHdr [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", transDate=" + transDate + ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
	
	
	
	
}
