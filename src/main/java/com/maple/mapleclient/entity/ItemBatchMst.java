package com.maple.mapleclient.entity;

import java.util.Date;

 
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemBatchMst {
	
 

	private String id;
	
	private String itemId;
	private String batch ;
	private String barcode;
	private Double qty;
	private Double mrp;
	private String itemName;
	private Date expDate;
	private Double rate;
	
	private String  processInstanceId;
	private String taskId;
	
	public ItemBatchMst(){
		this.batchCodeProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty();
		this.barcodeProperty = new SimpleStringProperty("");
		this.rateProperty = new SimpleDoubleProperty();
	}
	 
	@JsonIgnore
	private StringProperty batchCodeProperty;
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private StringProperty barcodeProperty;
	
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
 
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Date getExpDate() {
		return expDate;
	}

	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public StringProperty getBatchCodeProperty() {
		batchCodeProperty.set(batch);
		return batchCodeProperty;
	}
	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}
	
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ItemBatchMst [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", barcode=" + barcode + ", qty="
				+ qty + ", mrp=" + mrp + ", itemName=" + itemName + ", expDate=" + expDate + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	public StringProperty getBarcodeProperty() {
		barcodeProperty.set(barcode);
		return barcodeProperty;
	}
	public void setBarcodeProperty(StringProperty barcodeProperty) {
		this.barcodeProperty = barcodeProperty;
	}
	public DoubleProperty getRateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}
	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}
	 

}
