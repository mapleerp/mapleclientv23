package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class OpeningBalanceMst implements Serializable{
	private static final long serialVersionUID = 1L;



	private String id;
	
	String accountId;
	Double debitAmount;
	Double creditAmount;
	Date transDate;


	FinancialYearMst financialYearMst;

	String accountName;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty accountNameproperty;

	@JsonIgnore
	private DoubleProperty debitAmountProperty;
	
	@JsonIgnore
	private DoubleProperty creditAmountProperty;
	
	@JsonIgnore
	private StringProperty transDateProperty;
	
	public OpeningBalanceMst() {
		this.accountNameproperty = new SimpleStringProperty();
		this.debitAmountProperty = new SimpleDoubleProperty();
		this.creditAmountProperty = new SimpleDoubleProperty();
		this.transDateProperty = new SimpleStringProperty();
	}

	@JsonIgnore
	public StringProperty getaccountNameproperty() {
		accountNameproperty.set(accountName);
		return accountNameproperty;
	}


	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void setaccountNameproperty(String accountName) {
		this.accountName = accountName;
	}
	
	@JsonIgnore
	public DoubleProperty getdebitAmountProperty() {
		debitAmountProperty.set(debitAmount);
		return debitAmountProperty;
	}


	public void setaccountNameproperty(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	
	@JsonIgnore
	public DoubleProperty getcreditAmountProperty() {
		creditAmountProperty.set(creditAmount);
		return creditAmountProperty;
	}


	public void setcreditAmountProperty(Double creditAmount) {
		this.creditAmount = creditAmount;
	}

	@JsonIgnore
	public StringProperty gettransDateProperty() {
		transDateProperty.set(SystemSetting.UtilDateToString(transDate, "yyyy-MM-dd"));
		return transDateProperty;
	}


	public void settransDateProperty(Date transDate) {
		this.transDate = transDate;
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getAccountId() {
		return accountId;
	}


	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}


	public Double getDebitAmount() {
		return debitAmount;
	}


	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}


	public Double getCreditAmount() {
		return creditAmount;
	}


	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}


	public Date getTransDate() {
		return transDate;
	}


	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}


	public FinancialYearMst getFinancialYearMst() {
		return financialYearMst;
	}


	public void setFinancialYearMst(FinancialYearMst financialYearMst) {
		this.financialYearMst = financialYearMst;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "OpeningBalanceMst [id=" + id + ", accountId=" + accountId + ", debitAmount=" + debitAmount
				+ ", creditAmount=" + creditAmount + ", transDate=" + transDate + ", financialYearMst="
				+ financialYearMst + ", accountName=" + accountName + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	




}
