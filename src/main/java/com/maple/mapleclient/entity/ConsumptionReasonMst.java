package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ConsumptionReasonMst {
	  String id;
	  String reason;
	  private String  processInstanceId;
		private String taskId;
	  
	  @JsonIgnore
	  StringProperty reasonProperty;
	  
	  
	public ConsumptionReasonMst() {
		
		this.reasonProperty = new SimpleStringProperty();
	}
	@JsonIgnore
	  public StringProperty getreasonProperty() {
		reasonProperty.set(reason);
			return reasonProperty;
		}
		public void setreasonProperty(String reason) {
			this.reason = reason;
		}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ConsumptionReasonMst [id=" + id + ", reason=" + reason + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	  
	  
}
