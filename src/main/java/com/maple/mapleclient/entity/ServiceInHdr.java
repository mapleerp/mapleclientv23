package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Date;


public class ServiceInHdr implements Serializable{
	private static final long serialVersionUID = 1L;
	 
	
     String id;

	 String voucherNumber;
	 Date voucherDate;
	 Date estimatedDeliveryDate;
	 Double estimatedCharge;
	 Double advanceAmount;
	 AccountHeads accountHeads;
	 String branchCode;
	 private String  processInstanceId;
		private String taskId;
	 
	 


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public Date getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	
	

	public Date getEstimatedDeliveryDate() {
		return estimatedDeliveryDate;
	}


	public void setEstimatedDeliveryDate(Date estimatedDeliveryDate) {
		this.estimatedDeliveryDate = estimatedDeliveryDate;
	}


	public Double getEstimatedCharge() {
		return estimatedCharge;
	}


	public void setEstimatedCharge(Double estimatedCharge) {
		this.estimatedCharge = estimatedCharge;
	}


	public Double getAdvanceAmount() {
		return advanceAmount;
	}


	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}





	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public AccountHeads getAccountHeads() {
		return accountHeads;
	}


	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}


	@Override
	public String toString() {
		return "ServiceInHdr [id=" + id + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", estimatedDeliveryDate=" + estimatedDeliveryDate + ", estimatedCharge=" + estimatedCharge
				+ ", advanceAmount=" + advanceAmount + ", accountHeads=" + accountHeads + ", branchCode=" + branchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}



	
}
