package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class UserCreatedTask {
	String userId;
	String taskId;
	Date taskDueDate;
	Date createDate;
	String remark;
	String status;
	Date completeDate;
	String id;

	public UserCreatedTask() {

		this.userIdProperty = new SimpleStringProperty();
		this.remarkProperty = new SimpleStringProperty();
		this.taskDueDateProperty = new SimpleStringProperty();
		this.createDateProperty = new SimpleStringProperty();
		this.completeDateProperty = new SimpleStringProperty();
		this.statusProperty = new SimpleStringProperty();
	}

	@JsonIgnore
	StringProperty userIdProperty;

	@JsonIgnore
	StringProperty remarkProperty;

	@JsonIgnore
	StringProperty taskDueDateProperty;

	@JsonIgnore
	StringProperty createDateProperty;
	@JsonIgnore
	StringProperty completeDateProperty;
	@JsonIgnore
	StringProperty statusProperty;

	public StringProperty getUserIdProperty() {
		return userIdProperty;
	}

	public void setUserIdProperty(StringProperty userIdProperty) {
		this.userIdProperty = userIdProperty;
	}

	public StringProperty getRemarkProperty() {
		return remarkProperty;
	}

	public void setRemarkProperty(StringProperty remarkProperty) {
		this.remarkProperty = remarkProperty;
	}

	public StringProperty getTaskDueDateProperty() {
		if (null != taskDueDate) {
			String taskDueDateS = SystemSetting.UtilDateToString(taskDueDate,"yyyy-MM-dd");
			taskDueDateProperty.set(taskDueDateS);

		}
		return taskDueDateProperty;
	}

	public void setTaskDueDateProperty(StringProperty taskDueDateProperty) {
		this.taskDueDateProperty = taskDueDateProperty;
	}

	public StringProperty getCreateDateProperty() {
		
		if(null != createDate)
		{
			String createDateS = SystemSetting.UtilDateToString(createDate);
			createDateProperty.set(createDateS);
		}
		
		return createDateProperty;
	}

	public void setCreateDateProperty(StringProperty createDateProperty) {
		this.createDateProperty = createDateProperty;
	}

	public StringProperty getCompleteDateProperty() {
		if(null != completeDate) {
			String completeDateS = SystemSetting.UtilDateToString(completeDate);
			completeDateProperty.set(completeDateS);
		}
		
		return completeDateProperty;
	}

	public void setCompleteDateProperty(StringProperty completeDateProperty) {
		this.completeDateProperty = completeDateProperty;
	}

	public StringProperty getStatusProperty() {
		return statusProperty;
	}

	public void setStatusProperty(StringProperty statusProperty) {
		this.statusProperty = statusProperty;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Date getTaskDueDate() {
		return taskDueDate;
	}

	public void setTaskDueDate(Date taskDueDate) {
		this.taskDueDate = taskDueDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	@Override
	public String toString() {
		return "UserCreatedTask [userId=" + userId + ", taskId=" + taskId + ", taskDueDate=" + taskDueDate
				+ ", createDate=" + createDate + ", remark=" + remark + ", status=" + status + ", completeDate="
				+ completeDate + ", id=" + id + ", userIdProperty=" + userIdProperty + ", remarkProperty="
				+ remarkProperty + ", taskDueDateProperty=" + taskDueDateProperty + ", createDateProperty="
				+ createDateProperty + ", completeDateProperty=" + completeDateProperty + ", statusProperty="
				+ statusProperty + "]";
	}

}
