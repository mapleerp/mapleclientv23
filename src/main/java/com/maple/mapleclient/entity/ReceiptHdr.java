package com.maple.mapleclient.entity;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class ReceiptHdr {
	String id;
	
	String BranchCode;
	String voucherType;
	Date transdate;
	String voucherNumber;
	Date voucherDate;
    String memberId;
    private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	@JsonIgnore
	private StringProperty transDateProperty;
	@JsonIgnore
	private StringProperty voucherTypeProperty;
	
	
	
	
	public ReceiptHdr() {
		
		this.voucherNumberProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();
	this.voucherTypeProperty=new  SimpleStringProperty();
	this.transDateProperty=new  SimpleStringProperty();
	}
	@JsonIgnore
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	

	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	@JsonIgnore
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate));
		return voucherDateProperty;
	}
	
	

	@JsonIgnore
	public StringProperty getVoucherTypeProperty() {
		voucherTypeProperty.set(voucherType);
		return voucherTypeProperty;
	}
	public void setVoucherTypeProperty(StringProperty voucherTypeProperty) {
		this.voucherTypeProperty = voucherTypeProperty;
	}
	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	
	public Date getVoucherDate() {
		return voucherDate;
	}



	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}



	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBranchCode() {
		return BranchCode;
	}
	public void setBranchCode(String branchCode) {
		this.BranchCode = branchCode;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public Date getTransdate() {
		return transdate;
	}
	public void setTransdate(Date transdate) {
		this.transdate = transdate;
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public StringProperty getVoucherNumberProperty() {
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	public StringProperty getVoucherDateProperty() {
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}
	
	
	
	@JsonIgnore
	public StringProperty getTransDateProperty() {
		if(null!=transdate) {
		transDateProperty.set(SystemSetting.UtilDateToString(transdate));
		}
		return transDateProperty;
	}
	public void setTransDateProperty(StringProperty transDateProperty) {
		this.transDateProperty = transDateProperty;
	}
	
	
	
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ReceiptHdr [id=" + id + ", BranchCode=" + BranchCode + ", voucherType=" + voucherType + ", transdate="
				+ transdate + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", memberId="
				+ memberId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
		
			
				
}
								