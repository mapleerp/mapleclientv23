package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import javax.swing.JScrollBar;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DailySalesReportDtl {
	
	String voucherNumber;
	String customerName;
	Double amount;
	String customerType;
	String receiptMode;
	String itemName;
	Double qty;
	String categoryName;
	String batch;
	Date expiryDate;
	Double rate;
	Double taxRate;
	Double saleValue;
	Double cost;
	Double costValue;
	String userId;
	
	private String  processInstanceId;
	private String taskId;

	
	java.util.Date vDate;

	public java.util.Date getvDate() {
		return vDate;
	}
	public void setvDate(java.util.Date vDate) {
		this.vDate = vDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getSaleValue() {
		return saleValue;
	}
	public void setSaleValue(Double saleValue) {
		this.saleValue = saleValue;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Double getCostValue() {
		return costValue;
	}
	public void setCostValue(Double costValue) {
		this.costValue = costValue;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
//	public ObjectProperty<LocalDate> getVoucherDate() {
//		return voucherDate;
//	}
//	public void setVoucherDate(ObjectProperty<LocalDate> voucherDate) {
//		this.voucherDate = voucherDate;
//	}
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty receiptModeProperty;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;

	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;
	
	
	public DailySalesReportDtl() {
		this.receiptModeProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.customerNameProperty =new SimpleStringProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.voucherDate = new SimpleObjectProperty<LocalDate>();
	}
@JsonProperty

	public String getVoucherNumber() {
	return voucherNumber;
}
public void setVoucherNumber(String voucherNumber) {
	this.voucherNumber = voucherNumber;
}
public String getCustomerName() {
	return customerName;
}
public void setCustomerName(String customerName) {
	this.customerName = customerName;
}
public Double getAmount() {
	return amount;
}
public void setAmount(Double amount) {
	this.amount = amount;
}



@JsonIgnore
public StringProperty getvoucherNumberProperty() {
	voucherNumberProperty.set(voucherNumber);
	return voucherNumberProperty;
}

public void setvoucherNumberProperty(String voucherNumber) {
	this.voucherNumber = voucherNumber;
}
@JsonIgnore
public StringProperty getcustomerNameProperty() {
	customerNameProperty.set(customerName);
	return customerNameProperty;
}

public void setcustomerNameProperty(String customerName) {
	this.customerName = customerName;
}
@JsonIgnore
public StringProperty getreceiptModeProperty() {
	receiptModeProperty.set(receiptMode);
	return receiptModeProperty;
}

public void setreceiptModeProperty(String receiptMode) {
	this.receiptMode = receiptMode;
}

@JsonIgnore
public DoubleProperty getamountProperty() {
	if(null != amount)
	amountProperty.set(amount);
	return amountProperty;
}

public String getCustomerType() {
	return customerType;
}
public void setCustomerType(String customerType) {
	this.customerType = customerType;
}
public void setamountProperty(Double amount) {
	this.amount = amount;
}
@JsonProperty("voucherDate")
public java.sql.Date getvoucherDate( ) {
	if(null==this.voucherDate.get() ) {
		return null;
	}else {
		return Date.valueOf(this.voucherDate.get() );
	}
 
	
}

public void setvoucherDate (java.sql.Date voucherDateProperty) {
					if(null!=voucherDateProperty)
	this.voucherDate.set(voucherDateProperty.toLocalDate());
}
public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
	return voucherDate;
}

public void setsourceVoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
	this.voucherDate = (SimpleObjectProperty<LocalDate>) voucherDate;
}

public String getReceiptMode() {
	return receiptMode;
}
public void setReceiptMode(String receiptMode) {
	this.receiptMode = receiptMode;
}

public String getProcessInstanceId() {
	return processInstanceId;
}
public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}
public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
@Override
public String toString() {
	return "DailySalesReportDtl [voucherNumber=" + voucherNumber + ", customerName=" + customerName + ", amount="
			+ amount + ", customerType=" + customerType + ", receiptMode=" + receiptMode + ", itemName=" + itemName
			+ ", qty=" + qty + ", categoryName=" + categoryName + ", batch=" + batch + ", expiryDate=" + expiryDate
			+ ", rate=" + rate + ", taxRate=" + taxRate + ", saleValue=" + saleValue + ", cost=" + cost + ", costValue="
			+ costValue + ", userId=" + userId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
			+ ", vDate=" + vDate + "]";
}

	
	

}
