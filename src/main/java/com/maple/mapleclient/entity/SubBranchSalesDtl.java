package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;

import org.springframework.stereotype.Component;

public class SubBranchSalesDtl implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	String itemId;
	Double qty;
	Double rate;
	Double cgstTaxRate;
	Double sgstTaxRate;
	Double cessRate;
	Double addCessRate;
	Double igstTaxRate;
	String itemTaxaxId;
	String unitId;
	String itemName;
	Date expiryDate;
	String batch;
	String barcode;
	Double taxRate;
	Double mrp;
	Double amount;
	String unitName;
	Double discount;
	Double cgstAmount;
	Double sgstAmount;
	Double igstAmount;
	Double cessAmount;
	Double returnedQty;
	String status;
	Double addCessAmount;
	Double costPrice;
	String offerReferenceId;
	String schemeId;

	Double standardPrice;

	String fbKey;
	SubBranchSalesTransHdr subBranchSalesTransHdr;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}
	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}
	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}
	public Double getCessRate() {
		return cessRate;
	}
	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}
	public Double getAddCessRate() {
		return addCessRate;
	}
	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}
	public Double getIgstTaxRate() {
		return igstTaxRate;
	}
	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}
	public String getItemTaxaxId() {
		return itemTaxaxId;
	}
	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getCgstAmount() {
		return cgstAmount;
	}
	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}
	public Double getSgstAmount() {
		return sgstAmount;
	}
	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}
	public Double getIgstAmount() {
		return igstAmount;
	}
	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}
	public Double getCessAmount() {
		return cessAmount;
	}
	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}
	public Double getReturnedQty() {
		return returnedQty;
	}
	public void setReturnedQty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getAddCessAmount() {
		return addCessAmount;
	}
	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}
	public Double getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}
	public String getOfferReferenceId() {
		return offerReferenceId;
	}
	public void setOfferReferenceId(String offerReferenceId) {
		this.offerReferenceId = offerReferenceId;
	}
	public String getSchemeId() {
		return schemeId;
	}
	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}
	public Double getStandardPrice() {
		return standardPrice;
	}
	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}
	public String getFbKey() {
		return fbKey;
	}
	public void setFbKey(String fbKey) {
		this.fbKey = fbKey;
	}
	
	public SubBranchSalesTransHdr getSubBranchSalesTransHdr() {
		return subBranchSalesTransHdr;
	}
	public void setSubBranchSalesTransHdr(SubBranchSalesTransHdr subBranchSalesTransHdr) {
		this.subBranchSalesTransHdr = subBranchSalesTransHdr;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SubBranchSalesDtl [id=" + id + ", itemId=" + itemId + ", qty=" + qty + ", rate=" + rate
				+ ", cgstTaxRate=" + cgstTaxRate + ", sgstTaxRate=" + sgstTaxRate + ", cessRate=" + cessRate
				+ ", addCessRate=" + addCessRate + ", igstTaxRate=" + igstTaxRate + ", itemTaxaxId=" + itemTaxaxId
				+ ", unitId=" + unitId + ", itemName=" + itemName + ", expiryDate=" + expiryDate + ", batch=" + batch
				+ ", barcode=" + barcode + ", taxRate=" + taxRate + ", mrp=" + mrp + ", amount=" + amount
				+ ", unitName=" + unitName + ", discount=" + discount + ", cgstAmount=" + cgstAmount + ", sgstAmount="
				+ sgstAmount + ", igstAmount=" + igstAmount + ", cessAmount=" + cessAmount + ", returnedQty="
				+ returnedQty + ", status=" + status + ", addCessAmount=" + addCessAmount + ", costPrice=" + costPrice
				+ ", offerReferenceId=" + offerReferenceId + ", schemeId=" + schemeId + ", standardPrice="
				+ standardPrice + ", fbKey=" + fbKey + ", subBranchSalesTransHdr=" + subBranchSalesTransHdr
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	
}
