package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
 

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CategoryMst implements Serializable{
	
	private String id;
	private String categoryName;
	private String parentId;
	private String parentName;
	
	CompanyMst companyMst;
	
	private String  processInstanceId;
	private String taskId;
	

	public CategoryMst() {
		this.categoryNameProperty = new SimpleStringProperty("");
		this.parentNameProperty = new SimpleStringProperty("");
	}
	
	@JsonIgnore
	private StringProperty categoryNameProperty;
	@JsonIgnore
	private StringProperty parentNameProperty;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public StringProperty getCategoryNameProperty() {

		categoryNameProperty.set(categoryName);
	return categoryNameProperty;
	}

	public void setCategoryNameProperty(StringProperty categoryNameProperty) {
		categoryNameProperty = categoryNameProperty;
	}
	
	public StringProperty getParentNameProperty() {

		parentNameProperty.set(parentName);
	return parentNameProperty;
	}

	public void setParentNameProperty(StringProperty parentNameProperty) {
		parentNameProperty = parentNameProperty;
	}
	
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "CategoryMst [id=" + id + ", categoryName=" + categoryName + ", parentId=" + parentId + ", parentName="
				+ parentName + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
	
	

}
