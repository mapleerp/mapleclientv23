package com.maple.mapleclient.entity;

import java.util.Date;


public class CreditCardReconcile {
    private String id;
	
	
	
	
	
	String branchCode;
	
	Double amount;
	Double systemAmount;
	Date transDate;
	String receiptMode;
	private String  processInstanceId;
	private String taskId;
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "CreditCardReconcile [id=" + id + ", branchCode=" + branchCode + ", amount=" + amount + ", systemAmount="
				+ systemAmount + ", transDate=" + transDate + ", receiptMode=" + receiptMode + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
