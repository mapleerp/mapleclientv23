package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InvoiceTermsAndConditionsMst implements Serializable {
	private static final long serialVersionUID = 1L;
	   private String id;
	String slNo;
	String termsAndConditions;
	String branchCode;
	
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty termsAndConditionsProperty;
	
	@JsonIgnore
	private StringProperty slNoProperty;
	
	
	public InvoiceTermsAndConditionsMst() {
		this.termsAndConditionsProperty = new SimpleStringProperty();
		this.slNoProperty = new SimpleStringProperty();
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSlNo() {
		return slNo;
	}
	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}
	public String getTermsAndConditions() {
		return termsAndConditions;
	}
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	
	
	public StringProperty getslNoProperty() {
		slNoProperty.set(slNo);
		return slNoProperty;
	}
	public void setslNoProperty(String slNo) {
		this.slNo = slNo;
	}
	public StringProperty gettermsAndConditionsProperty() {
		termsAndConditionsProperty.set(termsAndConditions);
		return termsAndConditionsProperty;
	}
	public void settermsAndConditionsProperty(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "InvoiceTermsAndConditionsMst [id=" + id + ", slNo=" + slNo + ", termsAndConditions="
				+ termsAndConditions + ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
		
}
