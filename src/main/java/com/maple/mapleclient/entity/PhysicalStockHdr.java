package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

 

public class PhysicalStockHdr {
	
 
    private String id;
 
	 
	private String voucherNumber;
	 private String branchCode;
	 private int companyId;
	 private String userId;
	 private String  processInstanceId;
		private String taskId;
	 
	 
	 public PhysicalStockHdr() {
		 this.voucherDate = new SimpleObjectProperty<LocalDate>(null);
	}
	@JsonIgnore
		private ObjectProperty<LocalDate> voucherDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public ObjectProperty<LocalDate> getVoucherDateProperty( ) {
		return voucherDate;
	}
 
	public void setVoucherDateProperty(ObjectProperty<LocalDate> voucherDateProperty) {
		this.voucherDate = voucherDate;
	}
	@JsonProperty("voucherDate")
	public java.sql.Date getVoucherDate ( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}
	
  public void setVoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PhysicalStockHdr [id=" + id + ", voucherNumber=" + voucherNumber + ", branchCode=" + branchCode
				+ ", companyId=" + companyId + ", userId=" + userId + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
	
	  
	 
	
}
