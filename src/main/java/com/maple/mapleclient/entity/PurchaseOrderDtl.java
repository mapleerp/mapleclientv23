package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class PurchaseOrderDtl {
String unitId;
	
String status;

Double receivedQty;
private String  processInstanceId;
private String taskId;

	public PurchaseOrderDtl()
	{
		this.itemName = new SimpleStringProperty("");
		 this.purchseRate = new SimpleDoubleProperty();
		this.amount = new SimpleDoubleProperty(0);
		this.batch = new SimpleStringProperty("");
		this.itemSerial = new SimpleStringProperty("");
		 this.barcode = new SimpleStringProperty("");
		 this.taxAmt = new SimpleDoubleProperty();
		this.cessAmt = new SimpleDoubleProperty();
		this.previousMRP = new SimpleDoubleProperty();
		this.expiryDate = new SimpleObjectProperty();
		this.expiryDate.set(LocalDate.now());
		 this.freeQty = new SimpleIntegerProperty();
		this.qty = new SimpleDoubleProperty();
		 this.taxRate = new SimpleDoubleProperty();
		this.discount = new SimpleDoubleProperty();
		this.changePriceStatus = new SimpleStringProperty("");
		this.unit = new SimpleStringProperty();
		 this.mrp = new SimpleDoubleProperty();
		 this.cessRate = new SimpleDoubleProperty();
		 this.manufactureDate = new SimpleObjectProperty();
		 this.manufactureDate.set(LocalDate.now());
			this.BinNo = new SimpleStringProperty("");
			this.PurchaseDate = new SimpleObjectProperty();
			 this.PurchaseDate.set(LocalDate.now());
			this.CessRate2 = new SimpleDoubleProperty();
			this.NetCost = new SimpleStringProperty("");
		 
	}



	
	



	/*
	 * public PurchaseDtl(IntegerProperty id, StringProperty itemName,
	 * DoubleProperty purchseRate, DoubleProperty amount, StringProperty batch,
	 * StringProperty itemSerial, StringProperty barcode, DoubleProperty taxAmt,
	 * DoubleProperty cessAmt, DoubleProperty previousMRP, Object expiryDate,
	 * IntegerProperty freeQty, IntegerProperty qty, DoubleProperty taxRate,
	 * DoubleProperty discount, StringProperty changePriceStatus, IntegerProperty
	 * unit, DoubleProperty mrp, DoubleProperty cessRate, DoubleProperty
	 * manufactureDate, IntegerProperty ItemId, StringProperty binNo, Object
	 * purchaseDate, DoubleProperty cessRate2, StringProperty netCost) { super(); Id
	 * = id; this.itemName = itemName; this.purchseRate = purchseRate; this.amount =
	 * amount; this.batch = batch; this.itemSerial = itemSerial; this.barcode =
	 * barcode; this.taxAmt = taxAmt; this.cessAmt = cessAmt; this.previousMRP =
	 * previousMRP; this.expiryDate = expiryDate; this.freeQty = freeQty; this.qty =
	 * qty; this.taxRate = taxRate; this.discount = discount; this.changePriceStatus
	 * = changePriceStatus; this.unit = unit; this.mrp = mrp; this.cessRate =
	 * cessRate; this.manufactureDate = manufactureDate; this.ItemId = ItemId;
	 * this.BinNo = binNo; this.PurchaseDate = purchaseDate; this.CessRate2 =
	 * cessRate2; this.NetCost = netCost; }
	 */



	private String id;
	
	
	@JsonIgnore
	private StringProperty itemName;
	
	@JsonIgnore
	String unitName;
	@JsonIgnore
	private DoubleProperty purchseRate;
	
	@JsonIgnore
	private DoubleProperty amount;
	
	@JsonIgnore
	private StringProperty batch;
	
	@JsonIgnore
	private StringProperty itemSerial;
	
	@JsonIgnore
	private StringProperty barcode;
	
	@JsonIgnore
	private DoubleProperty taxAmt;
	
	@JsonIgnore
	private DoubleProperty cessAmt;
	
	@JsonIgnore
	private DoubleProperty previousMRP;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDate;
	
	@JsonIgnore
	private IntegerProperty freeQty;
	
	@JsonIgnore
	private DoubleProperty qty;
	
	@JsonIgnore
	private DoubleProperty taxRate;
	
	@JsonIgnore
	private DoubleProperty discount;
	
	@JsonIgnore
	private StringProperty changePriceStatus;
	
	@JsonIgnore
	private StringProperty unit;
	
	@JsonIgnore
	private DoubleProperty mrp;
	
	
	
	@JsonIgnore
	private DoubleProperty cessRate;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> manufactureDate;
	
 
	private String ItemId;
	
	@JsonIgnore
	private StringProperty BinNo;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> PurchaseDate;
	
	@JsonIgnore
	private DoubleProperty CessRate2;
	
	@JsonIgnore
	private StringProperty NetCost;
	
	PurchaseOrderHdr purchaseOrderHdr;
	
	public PurchaseOrderHdr getPurchaseOrderHdr() {
		return purchaseOrderHdr;
	}
	public void setPurchaseOrderHdr(PurchaseOrderHdr purchaseOrderHdr) {
		this.purchaseOrderHdr = purchaseOrderHdr;
	}
	@JsonIgnore
	public StringProperty getBinNoProperty() {
		return BinNo;
	}
	public void setBinNoProperty(StringProperty binNo) {
		BinNo = binNo;
	}
	@JsonProperty("BinNo")
	public String getBinNo() {
		return BinNo.get();
	}
	@JsonProperty("BinNo")
	public void setBinNo(String binNo) {
		this.BinNo.set(binNo);
	}

	
	@JsonIgnore
	
	public ObjectProperty<LocalDate> getexpiryDateProperty() {
		return expiryDate;
	}
	
	public void setexpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {
		
		this.expiryDate = expiryDate;
	}
	
	@JsonProperty("expiryDate")
	
	public java.sql.Date getexpiryDate() {
	
		return Date.valueOf(this.expiryDate.get() );
	}
	
	public void setexpiryDate(java.sql.Date expiryDate) {
		
		this.expiryDate.set(expiryDate.toLocalDate());
	
	}


	


	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	@JsonIgnore
	public ObjectProperty<LocalDate> getPurchaseDateProperty() {
		return PurchaseDate;
	}
	public void setPurchaseDateProperty(ObjectProperty<LocalDate> PurchaseDate) {
		this.PurchaseDate = PurchaseDate;
	}
	@JsonProperty("PurchaseDate")
	public java.sql.Date getPurchaseDate() {
		return Date.valueOf(this.PurchaseDate.get() );
	}
	public void setPurchaseDate(java.sql.Date PurchaseDate) {
		this.PurchaseDate.set(PurchaseDate.toLocalDate());
	}
	
	
	
	
	

@JsonIgnore
	
	public ObjectProperty<LocalDate> getmanufactureDateProperty() {
		return manufactureDate;
	}
	public void setmanufactureDateProperty(ObjectProperty<LocalDate> manufactureDate) {
		this.manufactureDate = manufactureDate;
	}
	@JsonProperty("manufactureDate")
	public java.sql.Date getmanufactureDate() {
		return Date.valueOf(this.manufactureDate.get() );
	}
	public void setmanufactureDate(java.sql.Date manufactureDate) {
		this.manufactureDate.set(manufactureDate.toLocalDate());
	}
	
	
	@JsonIgnore
	public DoubleProperty getCessRate2Property() {
		return CessRate2;
	}
	public void setCessRate2Property(DoubleProperty cessRate2) {
		CessRate2 = cessRate2;
	}
	
	@JsonProperty("CessRate2")
	public Double getCessRate2() {
		return CessRate2.get();
		
	}
	@JsonProperty("CessRate2")
	public void setCessRate2(double CessRate2) {
		this.CessRate2.set(CessRate2);
	}
	
	

	@JsonIgnore
	public StringProperty getNetCostProperty() {
		return NetCost;
	}
	public void setNetCostProperty(StringProperty netCost) {
		NetCost = netCost;
	}
	@JsonProperty("NetCost")
	public String getNetCost() {
		return NetCost.get();
	}
	@JsonProperty("NetCost")
	public void setNetCost(String netCost) {
		this.NetCost.set(netCost);
	}

	
 

	public StringProperty getItemNameProperty() {
		return itemName;
	}

	public void setItemNameProperty(StringProperty itemName) {
		this.itemName = itemName;
	}
	@JsonProperty("itemName")
	public String getItemName() {
		return itemName.get();
	}
	@JsonProperty("itemName")
	public void setItemName(String itemName) {
		this.itemName.set(itemName);
	}
	
	
	
	@JsonIgnore
	public DoubleProperty getPurchseRateProperty() {
		return purchseRate;
	}

	public void setPurchseRateProperty(DoubleProperty purchseRate) {
		this.purchseRate = purchseRate;
	}
	@JsonProperty("purchseRate")
	public Double getPurchseRate() {
		return purchseRate.get();
	}
	@JsonProperty("purchseRate")
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate.set(purchseRate);
	}
	
	
	
	
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		return amount;
	}
	public void setAmountProperty(DoubleProperty amount) {
		this.amount = amount;
	}
	@JsonProperty("amount")
	public Double getAmount() {
		return amount.get();
	}

	public void setAmount(Double amount) {
		this.amount.set(amount);
	}
	
	
	
	
	
	
	@JsonIgnore
	
	public StringProperty getBatchProperty() {
		return batch;
	}

	public void setBatchProperty(StringProperty batch) {
		this.batch = batch;
	}
	@JsonProperty("batch")
	public String getBatch() {
		return batch.get();
	}
	@JsonProperty("batch")
	public void setBatch(String batch) {
		this.batch.set(batch);
	}
	
	
	
	
	@JsonIgnore
	public StringProperty getItemSerialProperty() {
		return itemSerial;
	}

	public void setItemSerialProperty(StringProperty itemSerial) {
		this.itemSerial = itemSerial;
	}
	@JsonProperty("itemSerial")
	public String getItemSerial() {
		return itemSerial.get();
	}
	@JsonProperty("itemSerial")
	public void setItemSerial(String itemSerial) {
		this.itemSerial.set(itemSerial);
	}
	
	
	
	
	
	@JsonIgnore
	public StringProperty getBarcodepProperty() {
		return barcode;
	}

	public void setBarcodeProperty(StringProperty barcode) {
		this.barcode = barcode;
	}
	@JsonProperty("barcode")
	public String getBarcode() {
		return barcode.get();
	}
	@JsonProperty("barcode")
	public void setBarcode(String barcode) {
		this.barcode.set(barcode);
	}
	
	
	
	@JsonIgnore
	public DoubleProperty getTaxAmtProperty() {
		return taxAmt;
	}

	public void setTaxAmtProperty(DoubleProperty taxAmt) {
		this.taxAmt = taxAmt;
	}
	@JsonProperty("taxAmt")
	public Double getTaxAmt() {
		return taxAmt.get();
	}
	@JsonProperty("taxAmt")
	public void setTaxAmt(Double taxAmt) {
		this.taxAmt.set(taxAmt);
	}
	
	
	
	@JsonIgnore
	public DoubleProperty getCessAmtProperty() {
		return cessAmt;
	}

	public void setCessAmtProperty(DoubleProperty cessAmt) {
		this.cessAmt = cessAmt;
	}
	@JsonProperty("cessAmt")
	public Double getCessAmt() {
		return cessAmt.get();
	}
	@JsonProperty("cessAmt")
	public void setCessAmt(Double cessAmt) {
		this.cessAmt.set(cessAmt);
	}
	
	
	@JsonIgnore
	public DoubleProperty getPreviousMRProperty() {
		return previousMRP;
	}

	public void setPreviousMRPProperty(DoubleProperty previousMRP) {
		this.previousMRP = previousMRP;
	}
	@JsonProperty("previousMRP")
	public Double getPreviousMRP() {
		return previousMRP.get();
	}
	@JsonProperty("previousMRP")
	public void setPreviousMRP(Double previousMRP) {
		this.previousMRP.set(previousMRP);
	}

	
	@JsonIgnore
	public IntegerProperty getFreeQtyProperty() {
		return freeQty;
	}

	public void setFreeQty(IntegerProperty freeQty) {
		this.freeQty = freeQty;
	}
	@JsonProperty("freeQty")
	public Integer getFreeQty() {
		return freeQty.get();
	}
	@JsonProperty("freeQty")
	public void setFreeQty(Integer freeQty) {
		this.freeQty.set(freeQty);
	}
	
	
	
	
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		return qty;
	}

	public void setQtyProperty(DoubleProperty qty) {
		this.qty = qty;
	}
	@JsonProperty("qty")
	public Double getQty() {
		return qty.get();
	}
	@JsonProperty("qty")
	public void setQty(Double qty) {
		this.qty.set(qty);
	}
	
	
	

	@JsonIgnore
	public DoubleProperty getTaxRateProperty() {
		return taxRate;
	}

	public void setTaxRate(DoubleProperty taxRate) {
		this.taxRate = taxRate;
	}
	@JsonProperty("taxRate")
	public Double getTaxRate() {
		return taxRate.get();
	}
	@JsonProperty("taxRate")
	public void setTaxRate(Double taxRate) {
		this.taxRate.set(taxRate);
	}
	
	
	
	@JsonIgnore
	public DoubleProperty getDiscountProperty() {
		return discount;
	}

	public void setDiscountProperty(DoubleProperty discount) {
		this.discount = discount;
	}
	@JsonProperty("discount")
	public Double getDiscount() {
		return discount.get();
	}
	@JsonProperty("discount")
	public void setDiscount(Double discount) {
		this.discount.set(discount);
	}
	
	
	
	@JsonIgnore
	public StringProperty getChangePriceStatusProperty() {
		return changePriceStatus;
	}

	public void setChangePriceStatusProperty(StringProperty changePriceStatus) {
		this.changePriceStatus = changePriceStatus;
	}
	@JsonProperty("changePriceStatus")
	public String getChangePriceStatus() {
		return changePriceStatus.get();
	}
	@JsonProperty("changePriceStatus")
	public void setChangePriceStatus(String changePriceStatus) {
		this.changePriceStatus.set(changePriceStatus);
	}
	
	
	@JsonIgnore
	public StringProperty getUnitProperty() {
		unit.set(unitName);
		return unit;
	}

	public void setUnitProperty(String unitName) {
		this.unitName = unitName;
	}
//	@JsonProperty("unit")
//	public Integer getUnit() {
//		return unit.get();
//	}
//	@JsonProperty("unit")
//	public void setUnit(Integer unit) {
//		this.unit.set(unit);
//	}
	
	
	
	
	
	@JsonIgnore
	public DoubleProperty getMrpProperty() {
		return mrp;
	}

	public void setMrpProperty(DoubleProperty mrp) {
		this.mrp = mrp;
	}
	@JsonProperty("mrp")
	public Double getMrp() {
		return mrp.get();
	}
	@JsonProperty("mrp")
	public void setMrp(Double mrp) {
		this.mrp.set(mrp);
	}
	@JsonIgnore
	public DoubleProperty getCessRateProperty() {
		return cessRate;
	}

	public void setCessRateProperty(DoubleProperty cessRate) {
		this.cessRate = cessRate;
	}
	@JsonProperty("cessRate")
	public Double getCessRate() {
		return cessRate.get();
	}
	@JsonProperty("cessRate")
	public void setCessRate(Double cessRate) {
		this.cessRate.set(cessRate);
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return ItemId;
	}
	public void setItemId(String itemId) {
		ItemId = itemId;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	
	public Double getReceivedQty() {
		return receivedQty;
	}
	public void setReceivedQty(Double receivedQty) {
		this.receivedQty = receivedQty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PurchaseOrderDtl [unitId=" + unitId + ", status=" + status + ", receivedQty=" + receivedQty
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", id=" + id + ", unitName="
				+ unitName + ", ItemId=" + ItemId + ", purchaseOrderHdr=" + purchaseOrderHdr + "]";
	}
	
}
