package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OwnAccountSettlementDtl {
	
	String id;
	String invoiceNumber;
	Double dueAmt;
	Double paidAmount;
	Double newPaidAmount;
	OwnAccount ownAccount;
	AccountReceivable accountReceivable;
	AccountPayable accountPayable;
	private String  processInstanceId;
	private String taskId;
//	OwnAccountSettlementMst ownAccountSettlementMst;
	
	
	@JsonIgnore
	private StringProperty invoiceNumberProperty;
	
	@JsonIgnore
	private DoubleProperty dueAmountProperty;
	
	@JsonIgnore
	private DoubleProperty paidAmountProperty;
	
	@JsonIgnore
	private DoubleProperty balanceAmountProperty;
	
	Double balanceAmount;
	

	public OwnAccountSettlementDtl() {
		
		this.invoiceNumberProperty = new SimpleStringProperty();
		this.dueAmountProperty = new SimpleDoubleProperty();
		this.paidAmountProperty =new SimpleDoubleProperty();

		this.balanceAmountProperty =new SimpleDoubleProperty();
	}

	public Double getNewPaidAmount() {
		return newPaidAmount;
	}

	public void setNewPaidAmount(Double newPaidAmount) {
		this.newPaidAmount = newPaidAmount;
	}

	public OwnAccount getOwnAccount() {
		return ownAccount;
	}

	public void setOwnAccount(OwnAccount ownAccount) {
		this.ownAccount = ownAccount;
	}

	public AccountReceivable getAccountReceivable() {
		return accountReceivable;
	}

	public void setAccountReceivable(AccountReceivable accountReceivable) {
		this.accountReceivable = accountReceivable;
	}

	public AccountPayable getAccountPayable() {
		return accountPayable;
	}

	public void setAccountPayable(AccountPayable accountPayable) {
		this.accountPayable = accountPayable;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Double getDueAmt() {
		return dueAmt;
	}

	public void setDueAmt(Double dueAmt) {
		this.dueAmt = dueAmt;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

//	public OwnAccountSettlementMst getOwnAccountSettlementMst() {
//		return ownAccountSettlementMst;
//	}
//
//	public void setOwnAccountSettlementMst(OwnAccountSettlementMst ownAccountSettlementMst) {
//		this.ownAccountSettlementMst = ownAccountSettlementMst;
//	}

	@JsonIgnore
	public StringProperty getinvoiceNumberProperty() {
		invoiceNumberProperty.set(invoiceNumber);
		return invoiceNumberProperty;
	}
	

	public void setinvoiceNumberProperty(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	@JsonIgnore
	public DoubleProperty getdueAmountProperty() {
		dueAmountProperty.set(dueAmt);
		return dueAmountProperty;
	}
	

	public void setdueAmountProperty(Double dueAmt) {
		this.dueAmt = dueAmt;
	}
	@JsonIgnore
	public DoubleProperty getpaidAmountProperty() {
		paidAmountProperty.set(paidAmount);
		return paidAmountProperty;
	}
	

	public void setpaidAmountProperty(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	@JsonIgnore
	public DoubleProperty getbalanceAmountProperty() {
		balanceAmountProperty.set(balanceAmount);
		return balanceAmountProperty;
	}
	

	public void setbalanceAmountProperty(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "OwnAccountSettlementDtl [id=" + id + ", invoiceNumber=" + invoiceNumber + ", dueAmt=" + dueAmt
				+ ", paidAmount=" + paidAmount + ", newPaidAmount=" + newPaidAmount + ", ownAccount=" + ownAccount
				+ ", accountReceivable=" + accountReceivable + ", accountPayable=" + accountPayable
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", balanceAmount=" + balanceAmount
				+ "]";
	}



	
}
