package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ReorderMst 
{
	
	String branchCode;
	
    String id;
    private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty itemId;
	@JsonIgnore
	private StringProperty itemName;
	@JsonIgnore
	private StringProperty supplierName;
	@JsonIgnore
	private StringProperty  supplierId;
	@JsonIgnore
	private   ObjectProperty<LocalDate> startingDate;
	@JsonIgnore
	private   ObjectProperty<LocalDate> endingDate;
	@JsonIgnore
	private DoubleProperty minQty;
	@JsonIgnore
	private DoubleProperty  maxQty;
	
	

	
	
	public ReorderMst(){
	
		this.itemId= new SimpleStringProperty("");
		this.supplierId =new SimpleStringProperty("");
		this.startingDate=new SimpleObjectProperty<LocalDate>( LocalDate.now());
		this.endingDate= new SimpleObjectProperty<LocalDate>( LocalDate.now());
		this.minQty= new SimpleDoubleProperty();
		this.maxQty= new SimpleDoubleProperty();
		this.itemName= new SimpleStringProperty("");
		this.supplierName= new SimpleStringProperty("");
		
}

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	@JsonIgnore
	public StringProperty getItemIdProperty() {
		return itemId;
	}
	public void setItemIdProperty(StringProperty itemId) {
		this.itemId = itemId;
	}
	@JsonProperty("itemId")
	public String getItemId () {
		return itemId.get();
	}
	public void setItemId(String itemId) {
		this.itemId.set(itemId);
	}
	
	@JsonIgnore
	public StringProperty getItemNameProperty() {
		return itemName;
	}
	public void setItemNameProperty(StringProperty itemName) {
		this.itemName = itemName;
	}
	@JsonProperty("itemName")
	public String getItemName () {
		return itemName.get();
	}
	public void setItemName(String itemName) {
		this.itemName.set(itemName);
	}
	
	@JsonIgnore
	public StringProperty getSupplierNameProperty() {
		return supplierName;
	}
	public void setSupplierNameProperty(StringProperty supplierName) {
		this.supplierName = supplierName;
	}
	@JsonProperty("supplierName")
	public String getSupplierName () {
		return supplierName.get();
	}
	public void setSupplierName(String supplierName) {
		this.supplierName.set(supplierName);
	}
	
	@JsonIgnore
	public StringProperty getSupplierIdProperty() {
		return supplierId;
	}
	public void setSupplierIdProperty(StringProperty supplierId) {
		this.supplierId = supplierId;
	}
	@JsonProperty("supplierId")
	public String getSupplierId () {
		return supplierId.get();
	}
	public void setSupplierId(String supplierId) {
		this.supplierId.set(supplierId);
	}
	
	
	
	
	@JsonIgnore
	public ObjectProperty<LocalDate> getStartingDateProperty() {
		return startingDate;
	}
	public void setStartingDateProperty(ObjectProperty<LocalDate> startingDate) {
		this.startingDate = startingDate;
	}
	@JsonProperty("startingDate")
	public java.sql.Date getStartingDate ( ) {
		return Date.valueOf(this.startingDate.get() );
	}
	public void setStartingDate (java.sql.Date startingDate) {
		this.startingDate.set(startingDate.toLocalDate());
	}
	
	
	
	
	@JsonIgnore
	public ObjectProperty<LocalDate> getEndingDateProperty() {
		return endingDate;
	}
	public void setEndingDateProperty(ObjectProperty<LocalDate> endingDate) {
		this.endingDate = endingDate;
	}
	@JsonProperty("endingDate")
	public java.sql.Date getEndingDate() {
		return Date.valueOf(this.endingDate.get() );
	}
	public void setEndingDate (java.sql.Date endingDate) {
		this.endingDate.set(endingDate.toLocalDate());
	}
	
	
	
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	@JsonIgnore
	public DoubleProperty getMinQtyProperty() {
		return minQty;
	}    

	public void setMinQtyProperty(DoubleProperty minQty) {
		this.minQty = minQty;
	}    
	
	@JsonProperty("minQty")
	public Double getMinQty() {
		return minQty.get();
	}
	public void setMinQty(Double  minQty) {
		this.minQty.set(minQty);
	}
	
	@JsonIgnore
	public DoubleProperty getMaxQtyProperty() {
		return maxQty;
	}
	public void setMaxQtyProperty(DoubleProperty maxQty) {
		this.maxQty = maxQty;
	}
	@JsonProperty("maxQty")
	public Double getMaxQty() {
		return maxQty.get();
	}
	public void setMaxQty(Double  maxQty) {
		this.maxQty.set(maxQty);  
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ReorderMst [branchCode=" + branchCode + ", id=" + id + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}


}