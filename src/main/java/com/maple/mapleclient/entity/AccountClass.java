
package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.stereotype.Component;
public class AccountClass  
{

	private String id;

	BigDecimal totalDebit;

	BigDecimal totalCredit;

	Date transDate;
	String machineId;
	String voucherType;
	String sourceVoucherNumber;
	String sourceParentId;
	String brachCode;

	String financialYear;
	private String  processInstanceId;
	private String taskId;


	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public String getSourceVoucherNumber() {
		return sourceVoucherNumber;
	}

	public void setSourceVoucherNumber(String sourceVoucherNumber) {
		this.sourceVoucherNumber = sourceVoucherNumber;
	}

	public String getSourceParentId() {
		return sourceParentId;
	}

	public void setSourceParentId(String sourceParentId) {
		this.sourceParentId = sourceParentId;
	}

	public String getBrachCode() {
		return brachCode;
	}

	public void setBrachCode(String brachCode) {
		this.brachCode = brachCode;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public BigDecimal getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}

	public BigDecimal getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	@Override
	public String toString() {
		return "AccountClass [id=" + id + ", totalDebit=" + totalDebit + ", totalCredit=" + totalCredit + ", transDate="
				+ transDate + ", machineId=" + machineId + ", voucherType=" + voucherType + ", sourceVoucherNumber="
				+ sourceVoucherNumber + ", sourceParentId=" + sourceParentId + ", brachCode=" + brachCode
				+ ", financialYear=" + financialYear + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}





}
