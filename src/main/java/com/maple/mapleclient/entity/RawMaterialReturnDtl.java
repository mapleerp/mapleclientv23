package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class RawMaterialReturnDtl {
	String id;
	String itemId;
	String batch;
	String unitId;
	Double qty;
	private String  processInstanceId;
	private String taskId;
	RawMaterialReturnHdr rawMaterialReturnHdr;
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	@JsonIgnore
	String itemName;
	@JsonIgnore
	String unitName;
	
	public RawMaterialReturnDtl() {
		
		this.itemNameProperty =new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
		this.unitNameProperty =new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.qty = 0.0;
	}
	@JsonIgnore

	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	@JsonIgnore

	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setbatchProperty(String batch) {
		this.batch = batch;
	}
	
	@JsonIgnore

	public StringProperty getunitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}

	public void setunitNameProperty(String unitName) {
		this.unitName = unitName;
	}
	@JsonIgnore

	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public RawMaterialReturnHdr getRawMaterialReturnHdr() {
		return rawMaterialReturnHdr;
	}
	public void setRawMaterialReturnHdr(RawMaterialReturnHdr rawMaterialReturnHdr) {
		this.rawMaterialReturnHdr = rawMaterialReturnHdr;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "RawMaterialReturnDtl [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", unitId=" + unitId
				+ ", qty=" + qty + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", rawMaterialReturnHdr=" + rawMaterialReturnHdr + ", itemName=" + itemName + ", unitName=" + unitName
				+ "]";
	}
	
}
