package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GroupPermissionMst {


	private String id;
	private String groupId;
	private String processId;
	private String groupName;
	private String processName;
	private String  processInstanceId;
	private String taskId;
	
	public GroupPermissionMst() {
		this.groupNameProperty = new SimpleStringProperty("");
		this.processNameProperty = new SimpleStringProperty("");
	}

	@JsonIgnore
	private StringProperty groupNameProperty;
	@JsonIgnore
	private StringProperty processNameProperty;
	
	
	
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getProcessId() {
		return processId;
	}
	public void setProcessId(String processId) {
		this.processId = processId;
	}
	
	public StringProperty getGroupNameProperty() {

		groupNameProperty.set(groupName);
		return groupNameProperty;
	}

	public void setGroupNameProperty(StringProperty groupNameProperty) {
		groupNameProperty = groupNameProperty;
	}
	
	public StringProperty getProcessNameProperty() {

		processNameProperty.set(processName);
		return processNameProperty;
	}

	public void setProcessNameProperty(StringProperty processNameProperty) {
		processNameProperty = processNameProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "GroupPermissionMst [id=" + id + ", groupId=" + groupId + ", processId=" + processId + ", groupName="
				+ groupName + ", processName=" + processName + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + "]";
	}
	
	
	
	
}
