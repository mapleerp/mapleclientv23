package com.maple.mapleclient.entity;

public class WeighingItemMst {
	String id;
	String itemId;
	String barcode;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "WeighingItemMst [id=" + id + ", itemId=" + itemId + ", barcode=" + barcode + ", branchCode="
				+ branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
