package com.maple.mapleclient.entity;


import java.io.Serializable;

 
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
 
 
public class WeighBridgeMaterialMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	 
   private String id;
	
	@JsonProperty("material")
	String material;
 	 
	private CompanyMst companyMst;
	
 
	 
	private BranchMst branchMst;
	private String  processInstanceId;
	private String taskId;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getMaterial() {
		return material;
	}


	public void setMaterial(String material) {
		this.material = material;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public BranchMst getBranchMst() {
		return branchMst;
	}


	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "WeighBridgeMaterialMst [id=" + id + ", material=" + material + ", companyMst=" + companyMst
				+ ", branchMst=" + branchMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


 
	
}
