package com.maple.mapleclient.entity;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
public class WeighBridgeVehicleMst {

	   private String id;
	   private String  vehicletype;
		private Integer rate;

		CompanyMst companyMst;
		
		BranchMst branchMst;
		private String  processInstanceId;
		private String taskId;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getVehicletype() {
			return vehicletype;
		}

		public void setVehicletype(String vehicletype) {
			this.vehicletype = vehicletype;
		}

		public Integer getRate() {
			return rate;
		}

		public void setRate(Integer rate) {
			this.rate = rate;
		}

		public CompanyMst getCompanyMst() {
			return companyMst;
		}

		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}

		public BranchMst getBranchMst() {
			return branchMst;
		}

		public void setBranchMst(BranchMst branchMst) {
			this.branchMst = branchMst;
		}

		

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "WeighBridgeVehicleMst [id=" + id + ", vehicletype=" + vehicletype + ", rate=" + rate
					+ ", companyMst=" + companyMst + ", branchMst=" + branchMst + ", processInstanceId="
					+ processInstanceId + ", taskId=" + taskId + "]";
		}
		
		


}
