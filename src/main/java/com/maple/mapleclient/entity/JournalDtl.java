package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class JournalDtl {
	String id;
	String accountHead;
	String remarks;
	Double debitAmount;
	Double creditAmount;
	String accountHeadName;
	
	private String  processInstanceId;
	private String taskId;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccountHead() {
		return accountHead;
	}
	public void setAccountHead(String accountHead) {
		this.accountHead = accountHead;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	JournalHdr journalHdr;
	
	public JournalHdr getJournalHdr() {
		return journalHdr;
	}
	public void setJournalHdr(JournalHdr journalHdr) {
		this.journalHdr = journalHdr;
	}
	
	@JsonIgnore
	private StringProperty accountHeadProperty;
	
	@JsonIgnore
	private StringProperty remarksProperty;
	
	@JsonIgnore
	private DoubleProperty debitAmountProperty;
	
	@JsonIgnore
	private DoubleProperty creditAmountProperty;
	
	@JsonIgnore
	private StringProperty accountHeadNameProperty;
	

	public JournalDtl() {
		this.accountHeadProperty = new SimpleStringProperty("");
		this.remarksProperty = new SimpleStringProperty("");
		this.debitAmountProperty =new SimpleDoubleProperty();
		this.creditAmountProperty = new SimpleDoubleProperty();
		this.accountHeadNameProperty=new SimpleStringProperty();
	}
	
	
	@JsonIgnore
	public StringProperty getaccountHeadProperty() {
		
		accountHeadProperty.set(accountHeadName);
		return accountHeadProperty;
	}

	public void setaccountHeadProperty(String accountHeadName) {
		this.accountHeadName = accountHeadName;
	}
	
	@JsonIgnore
	public StringProperty getremarksProperty() {
		remarksProperty.set(remarks);
		return remarksProperty;
	}

	public void setremarksProperty(String remarks) {
		this.remarks = remarks;
	}
	
	public String getAccountHeadName() {
		return accountHeadName;
	}
	public void setAccountHeadName(String accountHeadName) {
		this.accountHeadName = accountHeadName;
	}
	@JsonIgnore
	public DoubleProperty getdebitAmountProperty() {
		debitAmountProperty.set(debitAmount);
		return debitAmountProperty;
	}

	public void setdebitAmountProperty(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	@JsonIgnore
	public DoubleProperty getcreditAmountProperty() {
		creditAmountProperty.set(creditAmount);
		return creditAmountProperty;
	}

	public void setcreditAmountProperty(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	
	@JsonIgnore
	public StringProperty getAccountHeadNameProperty() {
		accountHeadNameProperty.set(accountHead);
		return accountHeadNameProperty;
	}

	public void setAccountHeadNameProperty(StringProperty accountHeadNameProperty) {
	
		
		this.accountHeadNameProperty = accountHeadNameProperty;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "JournalDtl [id=" + id + ", accountHead=" + accountHead + ", remarks=" + remarks + ", debitAmount="
				+ debitAmount + ", creditAmount=" + creditAmount + ", accountHeadName=" + accountHeadName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", journalHdr=" + journalHdr
				+ "]";
	}


}
