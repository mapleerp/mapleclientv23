package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MeetingMst {
	String orgId;
//	Date meetingDate;
	String chairPerson;
	String subject;
	String minutesBy;
	String meetingId;
	String subGroup;
	String id;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@JsonIgnore
    private StringProperty meetingIdProperty;
	@JsonIgnore
    private StringProperty subGroupProperty;
	@JsonIgnore
    private StringProperty minutesByProperty;
	@JsonIgnore
    private StringProperty chairPersonProperty;
	@JsonIgnore
    private StringProperty orgIdProperty;
	@JsonIgnore
    private StringProperty subjectProperty;
	@JsonIgnore
	private ObjectProperty<LocalDate> meetingDate;
	
	
	@JsonIgnore
	public StringProperty getMeetingIdProperty() {
		meetingIdProperty.set(meetingId);
		return meetingIdProperty;
	}
	public void setMeetingIdProperty(String meetingId) {
		this.meetingId = meetingId;
	}


	@JsonIgnore
	public StringProperty getSubGroupProperty() {
		subGroupProperty.set(subGroup);
		return subGroupProperty;
	}
	public void setSubGroupProperty(String subGroup) {
		this.subGroup = subGroup;
	}


	@JsonIgnore
	public StringProperty getMinutesByProperty() {
		minutesByProperty.set(minutesBy);
		return minutesByProperty;
	}
	public void setMinutesByProperty(String minutesBy) {
		this.minutesBy = minutesBy;
	}


	@JsonIgnore
	public StringProperty getChairPersonProperty() {
		chairPersonProperty.set(chairPerson);
		return chairPersonProperty;
	}
	public void setChairPersonProperty(String chairPerson) {
		this.chairPerson = chairPerson;
	}



	@JsonIgnore
	public StringProperty getOrgIdProperty() {
		orgIdProperty.set(orgId);
		return orgIdProperty;
	}
	public void setOrgIdProperty(String orgId) {
		this.orgId = orgId;
	}


	@JsonIgnore
	public StringProperty getSubjectProperty() {
		subjectProperty.set(subject);
		return subjectProperty;
	}
	public void setSubjectProperty(String subject) {
		this.subject = subject;
	}


	public ObjectProperty<LocalDate> getmeetingDateProperty( ) {
		return meetingDate;
	}
 
	public void setmeetingDateProperty(ObjectProperty<LocalDate> meetingDate) {
		this.meetingDate = meetingDate;
	}
	@JsonProperty("meetingDate")
	public java.sql.Date getmeetingDate ( ) {
		if(null==this.meetingDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.meetingDate.get() );
		}
	}
   public void setmeetingDate (java.sql.Date meetingDateProperty) {
						if(null!=meetingDateProperty)
		this.meetingDate.set(meetingDateProperty.toLocalDate());
	}
	public MeetingMst() {
	
		this.orgIdProperty= new SimpleStringProperty("");
		this.meetingDate=new SimpleObjectProperty<LocalDate>(null);
		this.subjectProperty= new SimpleStringProperty("");
		this.chairPersonProperty= new SimpleStringProperty("");
		this.minutesByProperty= new SimpleStringProperty("");
		this.meetingIdProperty= new SimpleStringProperty("");
		this.subGroupProperty= new SimpleStringProperty("");
	
	}



	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
//	public Date getMeetingDate() {
//		return meetingDate;
//	}
//	public void setMeetingDate(Date meetingDate) {
//		this.meetingDate = meetingDate;
//	}
	public String getChairPerson() {
		return chairPerson;
	}
	public void setChairPerson(String chairPerson) {
		this.chairPerson = chairPerson;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMinutesBy() {
		return minutesBy;
	}
	public void setMinutesBy(String minutesBy) {
		this.minutesBy = minutesBy;
	}
	public String getMeetingId() {
		return meetingId;
	}
	public void setMeetingId(String meetingId) {
		this.meetingId = meetingId;
	}
	public String getSubGroup() {
		return subGroup;
	}
	public void setSubGroup(String subGroup) {
		this.subGroup = subGroup;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MeetingMst [orgId=" + orgId + ", chairPerson=" + chairPerson + ", subject=" + subject + ", minutesBy="
				+ minutesBy + ", meetingId=" + meetingId + ", subGroup=" + subGroup + ", id=" + id + ", branchCode="
				+ branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
