package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class NutritionValueDtl {
	
	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public void setValue(String value) {
		this.value = value;
	}

	String id;
	String nutrition;
	String value;
	NutritionValueMst nutritionValueMst;
	String percentage;
	String serial;
	private String  processInstanceId;
	private String taskId;

	@JsonIgnore
	private StringProperty nutritionNameProperty;
	
	@JsonIgnore
	private StringProperty valueProperty;
	
	@JsonIgnore
	private StringProperty percentageProperty;
	
	@JsonIgnore
	private StringProperty serialProperty;
	public String getId() {
		return id;
	}
	
	public NutritionValueDtl() {
		
		this.nutritionNameProperty = new SimpleStringProperty();
		this.valueProperty = new SimpleStringProperty();
		this.percentageProperty = new SimpleStringProperty();
		this.serialProperty = new SimpleStringProperty();
	}
	
	public NutritionValueMst getNutritionValueMst() {
		return nutritionValueMst;
	}

	public void setNutritionValueMst(NutritionValueMst nutritionValueMst) {
		this.nutritionValueMst = nutritionValueMst;
	}


	public void setId(String id) {
		this.id = id;
	}
	public String getNutrition() {
		return nutrition;
	}
	public void setNutrition(String nutrition) {
		this.nutrition = nutrition;
	}
	
	
	public String getValue() {
		return value;
	}

	@JsonIgnore
	public StringProperty getnutritionNameProperty() {

		nutritionNameProperty.set(nutrition);
		return nutritionNameProperty;
	}

	public void setnutritionNameProperty(String nutrition) {
		this.nutrition = nutrition;
	}
	@JsonIgnore
	public StringProperty getValueProperty() {

		valueProperty.set(value);
		return valueProperty;
	}

	public void setvalueProperty(String value) {
		this.value = value;
	}
	@JsonIgnore
	public StringProperty getpercentageProperty() {

		valueProperty.set(percentage);
		return valueProperty;
	}

	public void setpercentageProperty(String percentage) {
		this.percentage = percentage;
	}
	@JsonIgnore
	public StringProperty getserialProperty() {

		serialProperty.set(serial);
		return serialProperty;
	}

	public void setserialProperty(String serial) {
		this.serial = serial;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "NutritionValueDtl [id=" + id + ", nutrition=" + nutrition + ", value=" + value + ", nutritionValueMst="
				+ nutritionValueMst + ", percentage=" + percentage + ", serial=" + serial + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	

}
