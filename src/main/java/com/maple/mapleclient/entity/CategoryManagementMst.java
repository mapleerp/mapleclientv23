package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CategoryManagementMst {

	
	String id;
	String categoryId;
	String subCategoryId;
	
	String categoryName;
	String subCategoryName;
	private String  processInstanceId;
	private String taskId;
	
	private StringProperty categoryNameProperty;
	private StringProperty subCategoryNameProperty;
	
	public CategoryManagementMst() {
		this.categoryNameProperty = new SimpleStringProperty();
		this.subCategoryNameProperty = new SimpleStringProperty();
	}
	
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	@JsonIgnore
	 public StringProperty getcategoryNameProperty() {
	categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}
	public void setcategoryNameProperty(String categoryName) {
		this.categoryName = categoryName;
	}
	@JsonIgnore
	public StringProperty getsubCategoryNameProperty() {
		subCategoryNameProperty.set(subCategoryName);
		return subCategoryNameProperty;
	}
	public void setsubCategoryNameProperty(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "CategoryManagementMst [id=" + id + ", categoryId=" + categoryId + ", subCategoryId=" + subCategoryId
				+ ", categoryName=" + categoryName + ", subCategoryName=" + subCategoryName + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
}
