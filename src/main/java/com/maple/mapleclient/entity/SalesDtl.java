package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesDtl {
	
	@JsonProperty
	String id;
	@JsonProperty
	String itemId;
	@JsonProperty
	Double rate;
	@JsonProperty
	Double cgstTaxRate;
	@JsonProperty
	Double sgstTaxRate;
//	@JsonProperty
//	Double cessRate;
	@JsonProperty
	Double qty;
	@JsonProperty
	Double addCessRate;
	@JsonProperty
	Double amount;
	@JsonProperty
	String itemTaxaxId;
	@JsonProperty
	String unitId;
	@JsonProperty
	String itemName;
	
	

	
	@JsonProperty
	String batch;
	@JsonProperty
	String barcode;
	@JsonProperty
	Double taxRate;
	@JsonProperty
	Double mrp;
	@JsonProperty
	String itemCode;
	@JsonProperty
	String unitName;
	 Double returnedQty;
	    String status;
	    
	    String offerReferenceId;
	    String schemeId;
	    
	    Double standardPrice;
	    Double costPrice;

	    String printKotStaus;
	    private LocalDateTime updatedTime;
	    Double fcAmount;
	    Double fcRate;
	    Double fcIgstRate;
	    Double fcCgst;
	    Double fcSgst;
	    Double fcTaxRate;
	    Double fcTaxAmount;
	    Double fcCessRate;
	    Double fcCessAmount;
	    Double fcMrp;
	    Double fcStandardPrice;
	    Double fcDiscount;
	    Double fcIgstAmount;
	    
	    Double listPrice;
	    
	    Double rateBeforeDiscount;
	    private String  processInstanceId;
		private String taskId;
		
		
		String store;
		String kotId;
		
	    
	public String getStore() {
			return store;
		}

		public void setStore(String store) {
			this.store = store;
		}

	public LocalDateTime getUpdatedTime() {
			return updatedTime;
		}

		public void setUpdatedTime(LocalDateTime updatedTime) {
			this.updatedTime = updatedTime;
		}

	public Double getStandardPrice() {
			return standardPrice;
		}

		public void setStandardPrice(Double standardPrice) {
			this.standardPrice = standardPrice;
		}

	public String getSchemeId() {
			return schemeId;
		}

		public void setSchemeId(String schemeId) {
			this.schemeId = schemeId;
		}

	public String getOfferReferenceId() {
			return offerReferenceId;
		}

		public String getPrintKotStaus() {
		return printKotStaus;
	}

	public void setPrintKotStaus(String printKotStaus) {
		this.printKotStaus = printKotStaus;
	}

		public void setOfferReferenceId(String offerReferenceId) {
			this.offerReferenceId = offerReferenceId;
		}

	public Double getReturnedQty() {
			return returnedQty;
		}

		public void setReturnedQty(Double returnedQty) {
			this.returnedQty = returnedQty;
		}
		

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

//	Date expiryDate;
	
	StringProperty ReturnedProperty;
	DoubleProperty returnedQtyProperty;
	StringProperty itemNameProperty;
	StringProperty unitNameProperty;
	StringProperty itemCodeProperty;
	StringProperty mrpProperty;
	StringProperty rateProperty;
	
	StringProperty taxRateProperty;
	StringProperty barcodeProperty;
	StringProperty batchProperty;
	StringProperty qtyProperty;
	DoubleProperty amountProperty;
	
	StringProperty fcRateProperty;
	StringProperty fcAmountProperty;
	StringProperty fcTaxRateProperty;
	StringProperty fcMrpProperty;
	StringProperty kotIdProperty;
	
	
	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDate;
	
	StringProperty cessRateProperty;
	
	   Double discount;
	    Double igstTaxRate;
	    Double cgstAmount;
	    Double sgstAmount;
	    Double igstAmount;
	    Double cessAmount;
	    Double cessRate;
	    Double addCessAmount;
	    
	
	private SalesTransHdr salesTransHdr;
	
	String warrantySerial;
	
//-------------------------------new version 1.11 surya 
	String kotDescription;
	//-------------------------------new version 1.11 surya  end

	public SalesDtl () {
		this.itemNameProperty = new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.mrpProperty = new SimpleStringProperty("");
		this.taxRateProperty = new SimpleStringProperty("");
		this.barcodeProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty();
		this.qtyProperty = new SimpleStringProperty("");
		this.cessRateProperty = new SimpleStringProperty("");
		this.ReturnedProperty = new SimpleStringProperty("");
		this.returnedQtyProperty = new SimpleDoubleProperty(0.0);
		this.rateProperty = new SimpleStringProperty("");
		this.expiryDate = new SimpleObjectProperty<LocalDate>(null);
		this.fcAmountProperty = new SimpleStringProperty("");
		this.fcMrpProperty = new SimpleStringProperty("");
		this.fcTaxRateProperty = new SimpleStringProperty("");
		this.fcRateProperty = new SimpleStringProperty("");
		this.kotIdProperty = new SimpleStringProperty("");
		    this.discount = 0.0;
		    this. igstTaxRate =0.0;
		    this. cgstAmount =0.0;
		    this. sgstAmount =0.0;
		    this. igstAmount =0.0;
		    this. cessAmount=0.0;
		    this.cgstTaxRate =0.0;
		    this.sgstTaxRate =0.0;
		    this.addCessRate=0.0;
		    this.cessRate=0.0;
		    this.mrp=0.0;
		    this.returnedQty = 0.0;
		    this.addCessAmount = 0.0;
		    this.rate = 0.0;
		    
		    
		    
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		
		this.id = id;
	}

	public StringProperty getRateProperty() {
		return rateProperty;
	}

	public void setRateProperty(StringProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	public StringProperty getFcRateProperty() {
		return fcRateProperty;
	}

	public void setFcRateProperty(StringProperty fcRateProperty) {
		this.fcRateProperty = fcRateProperty;
	}

	public StringProperty getFcAmountProperty() {
		return fcAmountProperty;
	}

	public void setFcAmountProperty(StringProperty fcAmountProperty) {
		this.fcAmountProperty = fcAmountProperty;
	}

	public StringProperty getFcTaxRateProperty() {
		return fcTaxRateProperty;
	}

	public void setFcTaxRateProperty(StringProperty fcTaxRateProperty) {
		this.fcTaxRateProperty = fcTaxRateProperty;
	}

	public StringProperty getFcMrpProperty() {
		return fcMrpProperty;
	}

	public void setFcMrpProperty(StringProperty fcMrpProperty) {
		this.fcMrpProperty = fcMrpProperty;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		 
		this.itemId = itemId;
	}

	public Double getRate() {
		return rate;
	}

	public Double getAddCessAmount() {
		return addCessAmount;
	}

	public void setAddCessAmount(Double addCessAmount) {
		this.addCessAmount = addCessAmount;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amountProperty.set(amount);
		this.amount = amount;
	}

	public void setRate(Double rate) {
		this.rateProperty.set(rate+""); 
		this.rate = rate;
	}

	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}

	public void setCgstTaxRate(Double cgstTaxRate) {
		
		
		this.cgstTaxRate = cgstTaxRate;
	}

	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}

	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Double getQty() {
		return qty;
	}

	public Double getFcAmount() {
		
		return fcAmount;
	}

	public void setFcAmount(Double fcAmount) {
		this.fcAmountProperty.set(fcAmount+""); 
		this.fcAmount = fcAmount;
	}

	public Double getFcRate() {
		return fcRate;
	}

	public void setFcRate(Double fcRate) {
		this.fcRateProperty.set(fcRate+""); 
		this.fcRate = fcRate;
	}



	public Double getFcIgstRate() {
		return fcIgstRate;
	}

	public void setFcIgstRate(Double fcIgstRate) {
		this.fcIgstRate = fcIgstRate;
	}

	public Double getFcIgstAmount() {
		return fcIgstAmount;
	}

	public void setFcIgstAmount(Double fcIgstAmount) {
		this.fcIgstAmount = fcIgstAmount;
	}

	public Double getFcCgst() {
		return fcCgst;
	}

	public void setFcCgst(Double fcCgst) {
		this.fcCgst = fcCgst;
	}

	public Double getFcSgst() {
		return fcSgst;
	}

	public void setFcSgst(Double fcSgst) {
		this.fcSgst = fcSgst;
	}

	public Double getFcTaxRate() {
		return fcTaxRate;
	}

	public void setFcTaxRate(Double fcTaxRate) {
		this.fcTaxRateProperty.set(fcTaxRate+""); 
		this.fcTaxRate = fcTaxRate;
	}

	public Double getFcTaxAmount() {
		return fcTaxAmount;
	}

	public void setFcTaxAmount(Double fcTaxAmount) {
		this.fcTaxAmount = fcTaxAmount;
	}

	public Double getFcCessRate() {
		return fcCessRate;
	}

	public void setFcCessRate(Double fcCessRate) {
		this.fcCessRate = fcCessRate;
	}

	public Double getFcCessAmount() {
		return fcCessAmount;
	}

	public void setFcCessAmount(Double fcCessAmount) {
		this.fcCessAmount = fcCessAmount;
	}

	public Double getFcMrp() {
		return fcMrp;
	}

	public void setFcMrp(Double fcMrp) {
		this.fcMrpProperty.set(fcMrp+""); 
		this.fcMrp = fcMrp;
	}

	public Double getFcStandardPrice() {
		return fcStandardPrice;
	}

	public void setFcStandardPrice(Double fcStandardPrice) {
		this.fcStandardPrice = fcStandardPrice;
	}

	public Double getFcDiscount() {
		return fcDiscount;
	}

	public void setFcDiscount(Double fcDiscount) {
		this.fcDiscount = fcDiscount;
	}

	public void setQty(Double qty) {
		qtyProperty.set(qty+"");
		this.qty = qty;
	}

	public Double getAddCessRate() {
		return addCessRate;
	}

	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}

	public String getItemTaxaxId() {
		return itemTaxaxId;
	}

	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		
		this.itemNameProperty.set(itemName);
		this.itemName = itemName;
	}

	

	public String getBatchCode() {
		return batch;
	}

	public void setBatchCode(String batch) {
		this.batchProperty.set(batch);
		this.batch = batch;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barCode) {
		this.barcodeProperty.set(barCode);
		this.barcode = barCode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRateProperty.set(taxRate+"");
		this.taxRate = taxRate;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrpProperty.set(mrp+"");
		this.mrp = mrp;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCodeProperty.set(itemCode);
		this.itemCode = itemCode;
	}

	public String getUnitName() {
			
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitNameProperty.set(unitName);
		this.unitName = unitName;
	}

	public StringProperty getItemNameProperty() {
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getUnitNameProperty() {
		return unitNameProperty;
	}

	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}

	public StringProperty getItemCodeProperty() {
		return itemCodeProperty;
	}

	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	public StringProperty getMrpProperty() {
		return mrpProperty;
	}

	public void setMrpProperty(StringProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}

	public StringProperty getTaxRateProperty() {
		return taxRateProperty;
	}

	public void setTaxRateProperty(StringProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}

	public StringProperty getBarcodeProperty() {
		return barcodeProperty;
	}

	public void setBarcodeProperty(StringProperty barcodeProperty) {
		this.barcodeProperty = barcodeProperty;
	}

	public StringProperty getBatchCodeProperty() {
		batchProperty.set(batch); 
		return batchProperty;
	}
	
	public void setBatchCodeProperty(String batch) {
		this.batch = batch;
	}
	public StringProperty getReturnedProperty() {
		ReturnedProperty.set(status); 
		return ReturnedProperty;
	}
	
	public void setReturnedProperty(String status) {
		this.status = status;
	}
	public DoubleProperty getreturnedQtyProperty() {
		returnedQtyProperty.set(returnedQty); 
		return returnedQtyProperty;
	}
	
	public void setreturnedQtyProperty(Double returnedQty) {
		this.returnedQty = returnedQty;
	}

	public StringProperty getQtyProperty() {
		return qtyProperty;
	}

	public void setQtyProperty(StringProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public StringProperty getCessRateProperty() {
		return cessRateProperty;
	}

	public void setCessRateProperty(StringProperty cessRateProperty) {
		this.cessRateProperty = cessRateProperty;
	}
	

	public DoubleProperty getAmountProperty() {
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}

	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	


	/*@JsonIgnore
	
	public ObjectProperty<LocalDate> getexpiryDateProperty() {
		return expiryDate;
	}*/
	
	
	/*public void setexpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {
		
		this.expiryDate = expiryDate;
	}*/
	
	
	
	/*public java.sql.Date getexpiryDate() {
		 
		return null== expiryDate ? null : Date.valueOf(this.expiryDate.get() );
	}*/
	
	
	
	/*public void setexpiryDate(java.sql.Date expiryDate) {
		if(null==expiryDate)
			return;
		this.expiryDate.set(expiryDate.toLocalDate());
	
	}

	public ObjectProperty<LocalDate> getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(ObjectProperty<LocalDate> expiryDate) {
		this.expiryDate = expiryDate;
	}

	*/
	
	
	
	
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	 

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getIgstTaxRate() {
		return igstTaxRate;
	}

	public void setIgstTaxRate(Double igstTaxRate) {
		this.igstTaxRate = igstTaxRate;
	}

	public Double getCgstAmount() {
		return cgstAmount;
	}

	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public Double getSgstAmount() {
		return sgstAmount;
	}

	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public Double getIgstAmount() {
		return igstAmount;
	}

	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}
	
//	public Date getExpiryDate() {
//		return expiryDate;
//	}
//
//	public void setExpiryDate(Date expiryDate) {
//		
//		if(null!=expiryDate) {
//			this.expiryDateProperty.set( expiryDate.toLocalDate());
//		}
//		 
//		
//		this.expiryDate = expiryDate;
//	}
	
	@JsonProperty("expiryDate")
	public java.sql.Date getExpiryDate( ) {
		if(null==this.expiryDate.get()) {
			return null;
		}else {
			return Date.valueOf(this.expiryDate.get() );
		}
	
	}

	
   public void setExpiryDate (java.sql.Date expiryDateProperty) {
		if(null!=expiryDateProperty)
		this.expiryDate.set(expiryDateProperty.toLocalDate());
	}

	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(String batch) {
		this.batch = batch;
	}

	public ObjectProperty<LocalDate> getExpiryDateProperty() {
		return expiryDate;
	}

	public void setExpiryDateProperty(ObjectProperty<LocalDate> expiryDateProperty) {
		this.expiryDate = expiryDate;
	}

	public Double getCessAmount() {
		return cessAmount;
	}

	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	
	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}
	
	

	public String getWarrantySerial() {
		return warrantySerial;
	}

	public void setWarrantySerial(String warrantySerial) {
		this.warrantySerial = warrantySerial;
	}

	public String getKotDescription() {
		return kotDescription;
	}

	public void setKotDescription(String kotDescription) {
		this.kotDescription = kotDescription;
	}
	
	


	public Double getListPrice() {
		return listPrice;
	}

	public void setListPrice(Double listPrice) {
		this.listPrice = listPrice;
	}

	

	public Double getRateBeforeDiscount() {
		return rateBeforeDiscount;
	}

	public void setRateBeforeDiscount(Double rateBeforeDiscount) {
		this.rateBeforeDiscount = rateBeforeDiscount;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	

	public String getKotId() {
		return kotId;
	}

	public StringProperty getKotIdProperty() {
		return kotIdProperty;
	}

	public void setKotId(String kotId) {
		this.kotId = kotId;
	}

	public void setKotIdProperty(StringProperty kotIdProperty) {
		this.kotIdProperty = kotIdProperty;
	}

	@Override
	public String toString() {
		return "SalesDtl [id=" + id + ", itemId=" + itemId + ", rate=" + rate + ", cgstTaxRate=" + cgstTaxRate
				+ ", sgstTaxRate=" + sgstTaxRate + ", qty=" + qty + ", addCessRate=" + addCessRate + ", amount="
				+ amount + ", itemTaxaxId=" + itemTaxaxId + ", unitId=" + unitId + ", itemName=" + itemName + ", batch="
				+ batch + ", barcode=" + barcode + ", taxRate=" + taxRate + ", mrp=" + mrp + ", itemCode=" + itemCode
				+ ", unitName=" + unitName + ", returnedQty=" + returnedQty + ", status=" + status
				+ ", offerReferenceId=" + offerReferenceId + ", schemeId=" + schemeId + ", standardPrice="
				+ standardPrice + ", costPrice=" + costPrice + ", printKotStaus=" + printKotStaus + ", updatedTime="
				+ updatedTime + ", fcAmount=" + fcAmount + ", fcRate=" + fcRate + ", fcIgstRate=" + fcIgstRate
				+ ", fcCgst=" + fcCgst + ", fcSgst=" + fcSgst + ", fcTaxRate=" + fcTaxRate + ", fcTaxAmount="
				+ fcTaxAmount + ", fcCessRate=" + fcCessRate + ", fcCessAmount=" + fcCessAmount + ", fcMrp=" + fcMrp
				+ ", fcStandardPrice=" + fcStandardPrice + ", fcDiscount=" + fcDiscount + ", fcIgstAmount="
				+ fcIgstAmount + ", listPrice=" + listPrice + ", rateBeforeDiscount=" + rateBeforeDiscount
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", store=" + store + ", kotId="
				+ kotId + ", ReturnedProperty=" + ReturnedProperty + ", returnedQtyProperty=" + returnedQtyProperty
				+ ", itemNameProperty=" + itemNameProperty + ", unitNameProperty=" + unitNameProperty
				+ ", itemCodeProperty=" + itemCodeProperty + ", mrpProperty=" + mrpProperty + ", rateProperty="
				+ rateProperty + ", taxRateProperty=" + taxRateProperty + ", barcodeProperty=" + barcodeProperty
				+ ", batchProperty=" + batchProperty + ", qtyProperty=" + qtyProperty + ", amountProperty="
				+ amountProperty + ", fcRateProperty=" + fcRateProperty + ", fcAmountProperty=" + fcAmountProperty
				+ ", fcTaxRateProperty=" + fcTaxRateProperty + ", fcMrpProperty=" + fcMrpProperty + ", kotIdProperty="
				+ kotIdProperty + ", expiryDate=" + expiryDate + ", cessRateProperty=" + cessRateProperty
				+ ", discount=" + discount + ", igstTaxRate=" + igstTaxRate + ", cgstAmount=" + cgstAmount
				+ ", sgstAmount=" + sgstAmount + ", igstAmount=" + igstAmount + ", cessAmount=" + cessAmount
				+ ", cessRate=" + cessRate + ", addCessAmount=" + addCessAmount + ", salesTransHdr=" + salesTransHdr
				+ ", warrantySerial=" + warrantySerial + ", kotDescription=" + kotDescription + "]";
	}

	

	
	
	 
}
