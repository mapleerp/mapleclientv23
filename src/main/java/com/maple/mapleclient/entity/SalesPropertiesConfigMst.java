package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;



public class SalesPropertiesConfigMst{


    private String id;
	String propertyName;

	String propertyType;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	StringProperty propertyNameproperty;
	@JsonIgnore
	StringProperty propertyTypeproperty;

	public SalesPropertiesConfigMst() {
		

		this.propertyNameproperty = new SimpleStringProperty();
		this.propertyTypeproperty = new SimpleStringProperty();

	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public StringProperty getpropertyNameproperty() {
		propertyNameproperty.set(propertyName);
		return propertyNameproperty;
	}
	public void setpropertyNameproperty(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public String getPropertyType() {
		return propertyType;
	}


	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}


	public StringProperty getpropertyTypeproperty() {
		propertyTypeproperty.set(propertyType);
		return propertyTypeproperty;
	}
	public void setpropertyTypeproperty(String propertyType) {
		this.propertyType = propertyType;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "SalesPropertiesConfigMst [id=" + id + ", propertyName=" + propertyName + ", propertyType="
				+ propertyType + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
