package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class SubGroupMember {
	

	
	@JsonIgnore
    private StringProperty orgIdProperty;
   @JsonIgnore
    private StringProperty subGroupIdProperty;
   
    @JsonIgnore
  	private ObjectProperty<LocalDate> membersinceProperty;
	@JsonIgnore
    private StringProperty recordIdProperty;
    @JsonIgnore
    private StringProperty memberIdProperty;
   
    @JsonIgnore
    private StringProperty subGroupNameProperty;
    String id;
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	String orgId;
	String subGroupId;
	String memberId;
	String recordId;
	Date membersince;
	String subGroupName;
	private String  processInstanceId;
	private String taskId;
	public SubGroupMember()
	{
		
		this.orgIdProperty= new SimpleStringProperty("");
		this.subGroupIdProperty= new SimpleStringProperty("");
		
		this.recordIdProperty=new SimpleStringProperty("");
		this.memberIdProperty= new SimpleStringProperty("");
        this.subGroupNameProperty=new  SimpleStringProperty("");
		
	}
 
	
	public StringProperty getOrgIdProperty() {
		orgIdProperty.set(orgId);
		return orgIdProperty;
	}




	public void setOrgIdProperty(StringProperty orgIdProperty) {
		this.orgIdProperty = orgIdProperty;
	}




	public StringProperty getSubGroupIdProperty() {
		subGroupIdProperty.set(subGroupId);
		return subGroupIdProperty;
	}




	public void setSubGroupIdProperty(StringProperty subGroupIdProperty) {
		this.subGroupIdProperty = subGroupIdProperty;
	}




	public ObjectProperty<LocalDate> getMembersinceProperty() {
	
		return membersinceProperty;
	}




	public void setMembersinceProperty(ObjectProperty<LocalDate> membersinceProperty) {
		this.membersinceProperty = membersinceProperty;
	}




	public StringProperty getRecordIdProperty() {
		recordIdProperty.set(recordId);
		return recordIdProperty;
	}




	public void setRecordIdProperty(StringProperty recordIdProperty) {
		this.recordIdProperty = recordIdProperty;
	}




	public StringProperty getMemberIdProperty() {
		memberIdProperty.set(memberId);
		return memberIdProperty;
	}




	public void setMemberIdProperty(StringProperty memberIdProperty) {
		this.memberIdProperty = memberIdProperty;
	}


public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getSubGroupId() {
		return subGroupId;
	}
	public void setSubGroupId(String subGroupId) {
		this.subGroupId = subGroupId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public Date getMembersince() {
		return membersince;
	}
	public void setMembersince(Date membersince) {
		this.membersince = membersince;
	}

@JsonIgnore
	public StringProperty getSubGroupNameProperty() {
	subGroupNameProperty.set(subGroupName);
		return subGroupNameProperty;
	}


	public void setSubGroupNameProperty(StringProperty subGroupNameProperty) {
		this.subGroupNameProperty = subGroupNameProperty;
	}


	public String getSubGroupName() {
		return subGroupName;
	}


	public void setSubGroupName(String subGroupName) {
		this.subGroupName = subGroupName;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "SubGroupMember [id=" + id + ", orgId=" + orgId + ", subGroupId=" + subGroupId + ", memberId=" + memberId
				+ ", recordId=" + recordId + ", membersince=" + membersince + ", subGroupName=" + subGroupName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}


	
	
}
