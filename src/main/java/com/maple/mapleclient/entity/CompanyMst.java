package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

 
import org.springframework.stereotype.Component;

public class CompanyMst implements Serializable{
	private static final long serialVersionUID = 1L;
	 
	private String id;
	
	private String companyName;
	private String state;
	private String companyGst;
	private String ipAdress;
	private LocalDateTime updatedTime;
	private String country;
	String currencyName;
	private String  processInstanceId;
	private String taskId;
	
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getId() {
		return id;
	}
	 
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		 
		String idString = companyName.replaceAll("[^A-Za-z0-9]", "");
		 this.id=idString;
		
		this.companyName = companyName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	
	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "CompanyMst [id=" + id + ", companyName=" + companyName + ", state=" + state + ", companyGst="
				+ companyGst + ", ipAdress=" + ipAdress + ", updatedTime=" + updatedTime + ", country=" + country
				+ ", currencyName=" + currencyName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}

	
	
	
}
