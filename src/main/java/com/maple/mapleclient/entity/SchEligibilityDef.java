package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;
public class SchEligibilityDef implements Serializable{
	private static final long serialVersionUID = 1L;
	

	private String id;
	String eligibilityName ;
	String branchCode ;
	
	
	private CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;


	
	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getEligibilityName() {
		return eligibilityName;
	}


	public void setEligibilityName(String eligibilityName) {
		this.eligibilityName = eligibilityName;
	}


	public CompanyMst getCompanyMst() {
		return companyMst;
	}


	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "SchEligibilityDef [id=" + id + ", eligibilityName=" + eligibilityName + ", branchCode=" + branchCode
				+ ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}


	

}
