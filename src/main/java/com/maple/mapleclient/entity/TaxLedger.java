package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TaxLedger implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	String taxPercent;
	String taxName;
	String ledger;
	String tallyLedgerName;

	@JsonIgnore
	StringProperty taxPercentProperty;
	@JsonIgnore
	StringProperty taxNameProperty;
	@JsonIgnore
	StringProperty ledgerProperty;
	@JsonIgnore
	StringProperty tallyLedgerNameProperty;

	public TaxLedger() {
		this.taxPercentProperty = new SimpleStringProperty();
		this.taxNameProperty = new SimpleStringProperty();
		this.ledgerProperty = new SimpleStringProperty();
		this.tallyLedgerNameProperty = new SimpleStringProperty();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(String taxPercent) {
		this.taxPercent = taxPercent;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public String getLedger() {
		return ledger;
	}

	public void setLedger(String ledger) {
		this.ledger = ledger;
	}

	public String getTallyLedgerName() {
		return tallyLedgerName;
	}

	public void setTallyLedgerName(String tallyLedgerName) {
		this.tallyLedgerName = tallyLedgerName;
	}

	public StringProperty getTaxPercentProperty() {
		taxPercentProperty.set(taxPercent);
		return taxPercentProperty;
	}

	public void setTaxPercentProperty(StringProperty taxPercentProperty) {
		this.taxPercentProperty = taxPercentProperty;
	}

	public StringProperty getTaxNameProperty() {
		taxNameProperty.set(taxName);
		return taxNameProperty;
	}

	public void setTaxNameProperty(StringProperty taxNameProperty) {
		this.taxNameProperty = taxNameProperty;
	}

	public StringProperty getLedgerProperty() {
		ledgerProperty.set(ledger);
		return ledgerProperty;
	}

	public void setLedgerProperty(StringProperty ledgerProperty) {
		this.ledgerProperty = ledgerProperty;
	}

	public StringProperty getTallyLedgerNameProperty() {
		tallyLedgerNameProperty.set(tallyLedgerName);
		return tallyLedgerNameProperty;
	}

	public void setTallyLedgerNameProperty(StringProperty tallyLedgerNameProperty) {
		this.tallyLedgerNameProperty = tallyLedgerNameProperty;
	}

	@Override
	public String toString() {
		return "TaxLedger [id=" + id + ", taxPercent=" + taxPercent + ", taxName=" + taxName + ", ledger=" + ledger
				+ ", tallyLedgerName=" + tallyLedgerName + ", taxPercentProperty=" + taxPercentProperty
				+ ", taxNameProperty=" + taxNameProperty + ", ledgerProperty=" + ledgerProperty
				+ ", tallyLedgerNameProperty=" + tallyLedgerNameProperty + "]";
	}

}
