package com.maple.mapleclient.entity;

public class CategoryWiseSalesReport {


	String categoryName;
	String categorId;
	String itemName;
	Double qty;
	Double value;
	Double rate;
	
	private String  processInstanceId;
	private String taskId;
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategorId() {
		return categorId;
	}
	public void setCategorId(String categorId) {
		this.categorId = categorId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "CategoryWiseSalesReport [categoryName=" + categoryName + ", categorId=" + categorId + ", itemName="
				+ itemName + ", qty=" + qty + ", value=" + value + ", rate=" + rate + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
