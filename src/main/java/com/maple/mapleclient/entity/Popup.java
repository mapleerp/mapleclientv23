package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Popup {
	
	Integer Id;
	private String  processInstanceId;
	private String taskId;
	
	@JsonProperty("name")
	private String name;

	public Integer getId() {
		return Id;
	}

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	


	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "Popup [Id=" + Id + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", name=" + name
				+ "]";
	}
	
	
	
	
	

}
