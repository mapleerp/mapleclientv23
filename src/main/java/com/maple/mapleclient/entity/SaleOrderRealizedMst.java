package com.maple.mapleclient.entity;


import java.io.Serializable;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SaleOrderRealizedMst  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private CompanyMst companyMst;
	private SalesTransHdr salesTransHdr;
	private SalesOrderTransHdr salesOrderTransHdr;

	
	Double advanceAmount;
	Double realizedAmount;
	Double invoiceTotal;
	private String  processInstanceId;
	private String taskId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	public SalesOrderTransHdr getSalesOrderTransHdr() {
		return salesOrderTransHdr;
	}
	public void setSalesOrderTransHdr(SalesOrderTransHdr salesOrderTransHdr) {
		this.salesOrderTransHdr = salesOrderTransHdr;
	}

	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	public Double getRealizedAmount() {
		return realizedAmount;
	}
	public void setRealizedAmount(Double realizedAmount) {
		this.realizedAmount = realizedAmount;
	}
	public Double getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SaleOrderRealizedMst [id=" + id + ", companyMst=" + companyMst + ", salesTransHdr=" + salesTransHdr
				+ ", salesOrderTransHdr=" + salesOrderTransHdr + ", advanceAmount=" + advanceAmount
				+ ", realizedAmount=" + realizedAmount + ", invoiceTotal=" + invoiceTotal + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
	
}
