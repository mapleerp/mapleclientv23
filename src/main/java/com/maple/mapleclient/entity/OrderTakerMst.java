package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrderTakerMst {
	
	String id;
	String orderTakerName;
	String branchCode;
	String status;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty orderTakerNameProperty;
	
	@JsonIgnore
	private StringProperty statusProperty;
	
	
	
	public OrderTakerMst() {
		
		this.orderTakerNameProperty = new SimpleStringProperty();
		this.statusProperty =new SimpleStringProperty();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getOrderTakerName() {
		return orderTakerName;
	}
	public void setOrderTakerName(String orderTakerName) {
		this.orderTakerName = orderTakerName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonIgnore
	public StringProperty getstatusProperty() {
		statusProperty.set(status);
		return statusProperty;
	}
	

	public void setstatusProperty(String status) {
		this.status = status;
	}
	@JsonIgnore
	public StringProperty getorderTakerNameProperty() {
		orderTakerNameProperty.set(orderTakerName);
		return orderTakerNameProperty;
	}
	

	public void setorderTakerNameProperty(String orderTakerName) {
		this.orderTakerName = orderTakerName;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "OrderTakerMst [id=" + id + ", orderTakerName=" + orderTakerName + ", branchCode=" + branchCode
				+ ", status=" + status + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	

}
