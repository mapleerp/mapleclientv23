package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.sql.Date;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class StockTransferOutDtl {

	Integer slNo;
	Integer displaySerial;

	String id;

	public void setId(String id) {
		this.id = id;
	}

	String batch;
	Double qty;
	Double rate;
	String itemCode;
	Double taxRate;
	Double amount;
	String unitId;
	String itemName;
//	String itemId;
	String barcode;
	Double mrp;

	String unitName;
	String intentDtlId;
	
	String storeName;
	
	
	ItemMst itemId;
	
	
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	private String processInstanceId;
	private String taskId;

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getIntentDtlId() {
		return intentDtlId;
	}

	public void setIntentDtlId(String intentDtlId) {
		this.intentDtlId = intentDtlId;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	@JsonIgnore
	private StringProperty batchCodeProperty;

	@JsonIgnore
	private DoubleProperty qtyProperty;

	@JsonIgnore
	private DoubleProperty rateProperty;

	@JsonIgnore
	private DoubleProperty amountProperty;

	@JsonIgnore
	private StringProperty unitNameProperty;
	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDate;

	@JsonIgnore
	private StringProperty unitIdProperty;

	@JsonIgnore
	private StringProperty itemNameProperty;

	@JsonIgnore
	private StringProperty barcodeProperty;

	@JsonIgnore
	private DoubleProperty mrpProperty;

	@JsonIgnore
	private IntegerProperty slNoProperty;
	
	@JsonIgnore
	private IntegerProperty displaySlNoProperty;
	

	StockTransferOutHdr stockTransferOutHdr;

	public StockTransferOutHdr getStockTransferOutHdr() {
		return stockTransferOutHdr;
	}

	public void setStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr) {
		this.stockTransferOutHdr = stockTransferOutHdr;
	}

	public ObjectProperty<LocalDate> getExpiryDateProperty() {
		return expiryDate;
	}

	public void setExpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {
		this.expiryDate = expiryDate;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemName() {
		return itemName;
	}

	// @JsonIgnore
//	
//	public StringProperty getItemNameProperty() {
//		return itemNameProperty;
//	}
//
//	public void setItemNameProperty(StringProperty itemNameProperty) {
//		this.itemNameProperty = itemNameProperty;
//	}
	@JsonIgnore
	public StringProperty getbarcodeProperty() {
		barcodeProperty.set(barcode);
		return barcodeProperty;
	}

	public void setbarcodeProperty(String barcode) {
		this.barcode = barcode;
	}

	@JsonIgnore
	public StringProperty getunitIdProperty() {
		unitIdProperty.set(unitId);
		return unitIdProperty;
	}

	public void setunitIdProperty(String unitId) {
		this.unitId = unitId;
	}

	@JsonIgnore

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}

	@JsonIgnore

	public StringProperty getBatchCodeProperty() {
		batchCodeProperty.set(batch);
		return batchCodeProperty;
	}

	public void setBatchCodeProperty(String batch) {
		this.batch = batch;
	}

	@JsonIgnore

	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}

	public void setUnitNameProperty(String unitName) {
		this.unitName = unitName;
	}

	@JsonIgnore

	public DoubleProperty getmrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}

	public void setmrpProperty(Double mrp) {
		this.mrp = mrp;
	}

	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setQtyProperty(Double qty) {
		this.qty = qty;
	}

	@JsonIgnore
	public DoubleProperty getRateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}

	public void setRateProperty(Double rate) {
		this.rate = rate;
	}

	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}

	public void setAmountProperty(Double amount) {
		this.amount = amount;
	}

 
	@JsonProperty

	public String getId() {
		return id;
	}

	@JsonProperty
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	@JsonProperty
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@JsonProperty
	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	@JsonProperty
	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	@JsonProperty
	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	@JsonProperty
	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	@JsonProperty
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	@JsonProperty
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	@JsonProperty("expiryDate")
	public java.sql.Date getexpiryDate() {
		if (null == this.expiryDate.get()) {
			return null;
		} else {
			return Date.valueOf(this.expiryDate.get());
		}

	}

	public void setexpiryDate(java.sql.Date expiryDateProperty) {
		if (null != expiryDateProperty)
			this.expiryDate.set(expiryDateProperty.toLocalDate());
	}

	@JsonProperty
	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public StockTransferOutDtl() {

		this.itemNameProperty = new SimpleStringProperty("");
		this.batchCodeProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty();
		this.rateProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.expiryDate = new SimpleObjectProperty<LocalDate>(null);
		this.barcodeProperty = new SimpleStringProperty("");
		this.mrpProperty = new SimpleDoubleProperty();
		this.unitNameProperty = new SimpleStringProperty();
		this.unitIdProperty = new SimpleStringProperty("");
		this.slNoProperty = new SimpleIntegerProperty();
		
		this.displaySlNoProperty = new SimpleIntegerProperty();
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	

	

	

	public IntegerProperty getSlNoProperty() {
		
		slNoProperty.set(slNo);
		return slNoProperty;
	}

	public void setSlNoProperty(IntegerProperty slNoProperty) {
		this.slNoProperty = slNoProperty;
	}

	public IntegerProperty getDisplaySlNoProperty() {
		
		displaySlNoProperty.set(displaySerial);
		return displaySlNoProperty;
	}

	public void setDisplaySlNoProperty(IntegerProperty displaySlNoProperty) {
		this.displaySlNoProperty = displaySlNoProperty;
	}

	public Integer getDisplaySerial() {
		return displaySerial;
	}

	public void setDisplaySerial(Integer displaySerial) {
		this.displaySerial = displaySerial;
	}

	public ItemMst getItemId() {
		return itemId;
	}

	public void setItemId(ItemMst itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return "StockTransferOutDtl [slNo=" + slNo + ", displaySerial=" + displaySerial + ", id=" + id + ", batch="
				+ batch + ", qty=" + qty + ", rate=" + rate + ", itemCode=" + itemCode + ", taxRate=" + taxRate
				+ ", amount=" + amount + ", unitId=" + unitId + ", itemName=" + itemName + ", barcode=" + barcode
				+ ", mrp=" + mrp + ", unitName=" + unitName + ", intentDtlId=" + intentDtlId + ", storeName="
				+ storeName + ", itemId=" + itemId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", batchCodeProperty=" + batchCodeProperty + ", qtyProperty=" + qtyProperty + ", rateProperty="
				+ rateProperty + ", amountProperty=" + amountProperty + ", unitNameProperty=" + unitNameProperty
				+ ", expiryDate=" + expiryDate + ", unitIdProperty=" + unitIdProperty + ", itemNameProperty="
				+ itemNameProperty + ", barcodeProperty=" + barcodeProperty + ", mrpProperty=" + mrpProperty
				+ ", slNoProperty=" + slNoProperty + ", displaySlNoProperty=" + displaySlNoProperty
				+ ", stockTransferOutHdr=" + stockTransferOutHdr + "]";
	}

	 
	

}
