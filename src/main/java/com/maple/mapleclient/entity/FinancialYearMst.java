

package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
 
public class  FinancialYearMst implements Serializable{
	
	private static final long serialVersionUID = 1L;

 
	Integer id;
	@JsonProperty("username")
	String UserName;
	Date startDate;
	Date endDate;
	String financialYear;
	Integer currentFinancialYear;
	String openCloseStatus;
	CompanyMst companyMst;
	
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty startDateProperty;
	
	@JsonIgnore
	private StringProperty endDateProperty;
	
	@JsonIgnore
	private StringProperty financialYrProperty;
	
	@JsonIgnore
	private StringProperty currentFinancialYrProperty;
	
	public FinancialYearMst() {
		this.startDateProperty = new SimpleStringProperty();
		this.endDateProperty = new SimpleStringProperty();
		this.financialYrProperty =new SimpleStringProperty();
		this.currentFinancialYrProperty = new SimpleStringProperty();
	}

	
	public StringProperty getstartDateProperty() {
		startDateProperty.set(SystemSetting.UtilDateToString(startDate, "yyyy-MM-dd"));
		return startDateProperty;
	}

	public void setstartDateProperty(Date startDate) {
		this.startDate = startDate;
	}
	
	
	public StringProperty getendDateProperty() {
		endDateProperty.set(SystemSetting.UtilDateToString(endDate, "yyyy-MM-dd"));
		return endDateProperty;
	}

	public void setendDateProperty(Date endDate) {
		this.endDate = endDate;
	}
	
	public StringProperty getfinancialYrProperty() {
		financialYrProperty.set(financialYear);
		return financialYrProperty;
	}

	public void setfinancialYrProperty(String financialYear) {
		this.financialYear = financialYear;
	}
	
	public StringProperty getcurrentFinancialYrProperty() {
		currentFinancialYrProperty.set(currentFinancialYear+"");
		return currentFinancialYrProperty;
	}

	public void setcurrentFinancialYrProperty(Integer currentFinancialYear) {
		this.currentFinancialYear = currentFinancialYear;
	}
	public Integer getId() {
		return id;
	}
	
	//version2.0
		public Integer getCurrentFinancialYear() {
			return currentFinancialYear;
		}

		public void setCurrentFinancialYear(Integer currentFinancialYear) {
			this.currentFinancialYear = currentFinancialYear;
		}
	//version2.0ends
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	
	
	public Date getStartDate() {
		return startDate;
	}

	public String getOpenCloseStatus() {
		return openCloseStatus;
	}

	public void setOpenCloseStatus(String openCloseStatus) {
		this.openCloseStatus = openCloseStatus;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "FinancialYearMst [id=" + id + ", UserName=" + UserName + ", startDate=" + startDate + ", endDate="
				+ endDate + ", financialYear=" + financialYear + ", currentFinancialYear=" + currentFinancialYear
				+ ", openCloseStatus=" + openCloseStatus + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	

}

