package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductConversionConfigMst {

	String id;
	String fromItemId;
	String toItemId;
	String branchCode;
	String fromItemName;
	String toItemName;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty fromItemNameProperty;
	
	@JsonIgnore
	private StringProperty toItemNameProperty;
	
	
	
	public ProductConversionConfigMst() {
		
		this.fromItemNameProperty = new SimpleStringProperty();
		this.toItemNameProperty =new SimpleStringProperty();
	}
	
	@JsonIgnore
	public StringProperty getfromItemNameProperty() {
		fromItemNameProperty.set(fromItemName);
		return fromItemNameProperty;
	}
	public void setfromItemNameProperty(String fromItemName) {
		this.fromItemName = fromItemName;
	}
	
	@JsonIgnore
	public StringProperty gettoItemNameProperty() {
		toItemNameProperty.set(toItemName);
		return toItemNameProperty;
	}
	public void settoItemNameProperty(String toItemName) {
		this.toItemName = toItemName;
	}
	public String getFromItemName() {
		return fromItemName;
	}
	public void setFromItemName(String fromItemName) {
		this.fromItemName = fromItemName;
	}
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getToItemName() {
		return toItemName;
	}
	public void setToItemName(String toItemName) {
		this.toItemName = toItemName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFromItemId() {
		return fromItemId;
	}
	public void setFromItemId(String fromItemId) {
		this.fromItemId = fromItemId;
	}
	public String getToItemId() {
		return toItemId;
	}
	public void setToItemId(String toItemId) {
		this.toItemId = toItemId;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ProductConversionConfigMst [id=" + id + ", fromItemId=" + fromItemId + ", toItemId=" + toItemId
				+ ", branchCode=" + branchCode + ", fromItemName=" + fromItemName + ", toItemName=" + toItemName
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
