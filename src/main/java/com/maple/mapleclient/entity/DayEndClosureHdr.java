package com.maple.mapleclient.entity;



import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DayEndClosureHdr implements Serializable{
	
	private static final long serialVersionUID = 1L;
	String id;
	private String userId;
	private String branchCode;
	private Double cashSale;
	private Double cardSale;
	private Double cardSale2;
	private Double physicalCash;
	private Double changeInCash;
	private Date processDate;
	
	String dayEndStatus;
	
	private String  processInstanceId;
	private String taskId;

	//version1.9
		@JsonIgnore
		Double pettyCash;
		//version1.9ends
	
		private Double cardSettlementAmount;

	
	public DayEndClosureHdr() {

		this.cashSaleProperty = new SimpleDoubleProperty(0.0);
		this.cardSaleProperty = new SimpleDoubleProperty(0.0);
		this.cardSale2Property = new SimpleDoubleProperty(0.0);
		this.physicalCashProperty = new SimpleDoubleProperty(0.0);
		this.userNameProperty = new SimpleStringProperty("");
		this.branchCodeProperty = new SimpleStringProperty("");
		this.physicalCash = 0.0;

		//version1.9
		this.pettyCashProperty = new SimpleDoubleProperty(0.0);
//version1.9ends
		
	}



	
   
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}


	//version.19
		@JsonIgnore
		private DoubleProperty pettyCashProperty;

	///version1.9ends


	@JsonIgnore
	private DoubleProperty cashSaleProperty;
	
	@JsonIgnore
	private DoubleProperty cardSaleProperty;
	
	@JsonIgnore
	private DoubleProperty cardSale2Property;
	
	@JsonIgnore
	private DoubleProperty physicalCashProperty;
	
	@JsonIgnore
	private String userName;
	
	@JsonIgnore
	private StringProperty userNameProperty;
	
	@JsonIgnore
	private StringProperty branchCodeProperty;
	
	
	
	public StringProperty getBranchCodeProperty() {
		branchCodeProperty.set(branchCode);
		return branchCodeProperty;
	}





	public void setBranchCodeProperty(StringProperty branchCodeProperty) {
		this.branchCodeProperty = branchCodeProperty;
	}


	//version1.9

	public DoubleProperty getpettyCashProperty() {
		if(null==pettyCash) {
			pettyCash = 0.0;
		}
		pettyCashProperty.set(pettyCash);
		return pettyCashProperty;
	}





	public void setpettyCashProperty(Double pettyCash) {
		this.pettyCash =pettyCash;
	}


	public Double getPettyCash() {
		return pettyCash;
	}





	public void setPettyCash(Double pettyCash) {
		this.pettyCash = pettyCash;
	}



//version1.9ends


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}





	public StringProperty getUserNameProperty() {
		userNameProperty.set(userName);
		return userNameProperty;
	}





	public void setUserNameProperty(String userName) {
		this.userName = userName;
	}





	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}



	public Double getCashSale() {
		return cashSale;
	}



	public void setCashSale(Double cashSale) {
		this.cashSale = cashSale;
	}



	public Double getCardSale() {
		return cardSale;
	}



	public void setCardSale(Double cardSale) {
		this.cardSale = cardSale;
	}



	public Double getChangeInCash() {
		return changeInCash;
	}





	public void setChangeInCash(Double changeInCash) {
		this.changeInCash = changeInCash;
	}





	public Double getCardSale2() {
		return cardSale2;
	}



	public void setCardSale2(Double cardSale2) {
		this.cardSale2 = cardSale2;
	}



	public Double getPhysicalCash() {
		return physicalCash;
	}



	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}



	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	
	@JsonIgnore
	public DoubleProperty getCashSaleProperty() {
		cashSaleProperty.set(cashSale);
		return cashSaleProperty;
	}
	public void setCashSaleProperty(DoubleProperty cashSaleProperty) {
		this.cashSaleProperty = cashSaleProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getCardSaleProperty() {
		cardSaleProperty.set(cardSale);
		return cardSaleProperty;
	}
	public void setCardSaleProperty(DoubleProperty cardSaleProperty) {
		this.cardSaleProperty = cardSaleProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getCardSale2Property() {
		cardSale2Property.set(cardSale2);
		return cardSale2Property;
	}
	public void setCardSale2Property(DoubleProperty cardSale2Property) {
		this.cardSale2Property = cardSale2Property;
	}
	
	@JsonIgnore
	public DoubleProperty getPhysicalCashProperty() {
		if(null == physicalCash)
		{
			physicalCash = 0.0;
		} 
		physicalCashProperty.set(physicalCash);
		return physicalCashProperty;
	}
	public void setPhysicalCashProperty(DoubleProperty physicalCashProperty) {
		this.physicalCashProperty = physicalCashProperty;
	}




	public String getDayEndStatus() {
		return dayEndStatus;
	}





	public void setDayEndStatus(String dayEndStatus) {
		this.dayEndStatus = dayEndStatus;
	}





	





	public String getProcessInstanceId() {
		return processInstanceId;
	}





	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}





	public String getTaskId() {
		return taskId;
	}





	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}






	public Double getCardSettlementAmount() {
		return cardSettlementAmount;
	}





	public void setCardSettlementAmount(Double cardSettlementAmount) {
		this.cardSettlementAmount = cardSettlementAmount;
	}





	@Override
	public String toString() {
		return "DayEndClosureHdr [id=" + id + ", userId=" + userId + ", branchCode=" + branchCode + ", cashSale="
				+ cashSale + ", cardSale=" + cardSale + ", cardSale2=" + cardSale2 + ", physicalCash=" + physicalCash
				+ ", changeInCash=" + changeInCash + ", processDate=" + processDate + ", dayEndStatus=" + dayEndStatus
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", pettyCash=" + pettyCash
				+ ", cardSettlementAmount=" + cardSettlementAmount + ", pettyCashProperty=" + pettyCashProperty
				+ ", cashSaleProperty=" + cashSaleProperty + ", cardSaleProperty=" + cardSaleProperty
				+ ", cardSale2Property=" + cardSale2Property + ", physicalCashProperty=" + physicalCashProperty
				+ ", userName=" + userName + ", userNameProperty=" + userNameProperty + ", branchCodeProperty="
				+ branchCodeProperty + "]";
	}





	

}
