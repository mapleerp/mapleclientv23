package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.io.Serializable;
import java.sql.Date;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
 

public class StoreChangeDtl implements Serializable {
	private static final long serialVersionUID = 1L;

	String id;
	String batch;
	Double qty;
	Double rate;
	String itemCode;
	Double taxRate;
	Double amount;
	String unitId;
	String itemName;
	String itemId;
	String barcode; 
	Double mrp;
	private String  processInstanceId;
	private String taskId;
	
	
	StoreChangeMst storeChangeMst;
	
	@JsonIgnore
	String unitName;
	
	@JsonIgnore
	private StringProperty batchProperty;
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty unitProperty;
	@JsonIgnore
	private DoubleProperty mrpProperty;
	@JsonIgnore
	private StringProperty barCodeProperty;
	@JsonIgnore
	private DoubleProperty rateProperty;
	@JsonIgnore
	private DoubleProperty qtyProperty;
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	
	
	
	public StoreChangeDtl() {
		this.batchProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.unitProperty = new SimpleStringProperty("");
		this.barCodeProperty = new SimpleStringProperty("");
		
		this.mrpProperty = new SimpleDoubleProperty(0.0);
		this.rateProperty = new SimpleDoubleProperty(0.0);
		this.qtyProperty = new SimpleDoubleProperty(0.0);
		this.amountProperty = new SimpleDoubleProperty(0.0);

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}


	public StoreChangeMst getStoreChangeMst() {
		return storeChangeMst;
	}

	public void setStoreChangeMst(StoreChangeMst storeChangeMst) {
		this.storeChangeMst = storeChangeMst;
	}
	
	
	

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getUnitProperty() {
		unitProperty.set(unitName);
		return unitProperty;
	}

	public void setUnitProperty(StringProperty unitProperty) {
		this.unitProperty = unitProperty;
	}

	public DoubleProperty getMrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}

	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}

	public StringProperty getBarCodeProperty() {
		barCodeProperty.set(barcode);
		return barCodeProperty;
	}

	public void setBarCodeProperty(StringProperty barCodeProperty) {
		this.barCodeProperty = barCodeProperty;
	}

	public DoubleProperty getRateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}

	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "StoreChangeDtl [id=" + id + ", batch=" + batch + ", qty=" + qty + ", rate=" + rate + ", itemCode="
				+ itemCode + ", taxRate=" + taxRate + ", amount=" + amount + ", unitId=" + unitId + ", itemName="
				+ itemName + ", itemId=" + itemId + ", barcode=" + barcode + ", mrp=" + mrp + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", storeChangeMst=" + storeChangeMst + ", unitName="
				+ unitName + "]";
	}

	
	
	

}
