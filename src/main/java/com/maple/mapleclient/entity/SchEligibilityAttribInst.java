package com.maple.mapleclient.entity;

import java.io.Serializable;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SchEligibilityAttribInst implements Serializable{
	private static final long serialVersionUID = 1L;
	

	private String id;
	
	String schemeId ;
	String attributeName ;
	String attributeValue ;
	String attributeType ;
	String eligibilityId ;
	
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	String instanceName ;
	
	public SchEligibilityAttribInst() {
		this.attributeNameProperty = new SimpleStringProperty("");
		this.attributeValueProperty = new SimpleStringProperty("");
		this.attributeTypeProperty = new SimpleStringProperty("");
		
	}
	
	@JsonIgnore
	private StringProperty attributeNameProperty;
	
	@JsonIgnore
	private StringProperty attributeValueProperty;
	
	@JsonIgnore
	private StringProperty attributeTypeProperty;
	
	@JsonIgnore
	public String getInstanceName() {
		return instanceName;
	}
	@JsonIgnore
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getEligibilityId() {
		return eligibilityId;
	}
	public void setEligibilityId(String eligibilityId) {
		this.eligibilityId = eligibilityId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSchemeId() {
		return schemeId;
	}
	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	public String getAttributeType() {
		return attributeType;
	}
	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}
	
	
	
	public StringProperty getAttributeNameProperty() {
		attributeNameProperty.set(attributeName);
		return attributeNameProperty;
	}
	public void setAttributeNameProperty(StringProperty attributeNameProperty) {
		this.attributeNameProperty = attributeNameProperty;
	}
	public StringProperty getAttributeValueProperty() {
		attributeValueProperty.set(attributeValue);
		return attributeValueProperty;
	}
	public void setAttributeValueProperty(StringProperty attributeValueProperty) {
		this.attributeValueProperty = attributeValueProperty;
	}
	public StringProperty getAttributeTypeProperty() {
		attributeTypeProperty.set(attributeType);
		return attributeTypeProperty;
	}
	public void setAttributeTypeProperty(StringProperty attributeTypeProperty) {
		this.attributeTypeProperty = attributeTypeProperty;
	}
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SchEligibilityAttribInst [id=" + id + ", schemeId=" + schemeId + ", attributeName=" + attributeName
				+ ", attributeValue=" + attributeValue + ", attributeType=" + attributeType + ", eligibilityId="
				+ eligibilityId + ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + ", instanceName=" + instanceName + "]";
	}
	
	
	

	
 
}
