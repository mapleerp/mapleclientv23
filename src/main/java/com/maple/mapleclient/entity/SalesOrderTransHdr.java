
package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
 * R.George
 * Entity Corrected on 5/6
 * 
 */

public class SalesOrderTransHdr implements Serializable {

	String id;
	//private CustomerMst customerMst;
	AccountHeads accountHeads;
	LocalCustomerMst localCustomerId;
	private String salesManId;
	private String deliveryBoyId;
	private String salesMode;
	private String creditOrCash;
	private String customerName;
	private String orderDueTime;
	private String orderTimeMode;
	//private String customerId;
	private String orderStatus;
	private String userId;
	private Double sodexoAmount;
	private String customiseSalesMode;
	private String saleOrderReceiptVoucherNumber;

	private Long companyId;
	String branchCode;
	private String performaInvoicePrinted;
	public String getPerformaInvoicePrinted() {
		return performaInvoicePrinted;
	}

	public void setPerformaInvoicePrinted(String performaInvoicePrinted) {
		this.performaInvoicePrinted = performaInvoicePrinted;
	}

	private String machineId;

	String voucherNumber;

	private Date voucherDate;

	private Double invoiceAmount;

	private Double invoiceDiscount;

	private Double cashPay;

	private Double itemDiscount;

	private Double paidAmount;

	private Double changeAmount;

	private String cardNo;
	private String cardType;

	private Double cardamount;

	private String convertedToInvoice;

	Date statusDate;
	private String paymentMode;

	private String deliveryMode;

	private String orderDueDay;

	private String ordedBy;

	private Date convertedToInvoiceDate;

	private Double advanceAmount;
	private String orderMessageToPrint;
	private String orderAdvice;
	private Date orderDue;
	private String discount;
	String localCustomerName;
	Double cashPaidSale;
	private String targetDepartment;

	private String servingTableName;
	
	private String cancelationReason;
	private String  processInstanceId;
	private String taskId;
	private String isBranchSales;
	String invoicePrintType;
	private LocalCustomerMst localCustomerMst;

//	public CustomerMst getCustomerid() {
//		return customerid;
//	}
//
//	public void setCustomerid(CustomerMst customerid) {
//		this.customerid = customerid;
//	}

	public SalesOrderTransHdr() {
		this.voucherNoProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.paidAmtProperty = new SimpleDoubleProperty(0.0);
		this.orderDueDateProperty = new SimpleStringProperty();
		this.orderDueTimeProperty = new SimpleStringProperty();
		this.invoiceAmountProperty = new SimpleDoubleProperty();
		this.deliveryModeProperty = new SimpleStringProperty();
		this.targetBranchProperty = new SimpleStringProperty();
		this.orderMessageProperty = new SimpleStringProperty();
		this.localCustomerNameProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();

		this.cancelationReasonProperty = new SimpleStringProperty();
		

	}

	public Double getCashPaidSale() {
		return cashPaidSale;
	}

	public void setCashPaidSale(Double cashPaidSale) {
		this.cashPaidSale = cashPaidSale;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getCustomiseSalesMode() {
		return customiseSalesMode;
	}

	public void setCustomiseSalesMode(String customiseSalesMode) {
		this.customiseSalesMode = customiseSalesMode;
	}

	public String getIsBranchSales() {
		return isBranchSales;
	}

	public void setIsBranchSales(String isBranchSales) {
		this.isBranchSales = isBranchSales;
	}

	public LocalCustomerMst getLocalCustomerMst() {
		return localCustomerMst;
	}

	public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}

	public StringProperty getOrderDueDateProperty() {
		return orderDueDateProperty;
	}

	public void setOrderDueDateProperty(StringProperty orderDueDateProperty) {
		this.orderDueDateProperty = orderDueDateProperty;
	}

	public DoubleProperty getInvoiceAmountProperty() {
		return invoiceAmountProperty;
	}

	public void setInvoiceAmountProperty(DoubleProperty invoiceAmountProperty) {
		this.invoiceAmountProperty = invoiceAmountProperty;
	}

	public StringProperty getDeliveryModeProperty() {
		return deliveryModeProperty;
	}

	public void setDeliveryModeProperty(StringProperty deliveryModeProperty) {
		this.deliveryModeProperty = deliveryModeProperty;
	}

	public StringProperty getTargetBranchProperty() {
		return targetBranchProperty;
	}

	public void setTargetBranchProperty(StringProperty targetBranchProperty) {
		this.targetBranchProperty = targetBranchProperty;
	}

	public StringProperty getOrderMessageProperty() {
		return orderMessageProperty;
	}

	public void setOrderMessageProperty(StringProperty orderMessageProperty) {
		this.orderMessageProperty = orderMessageProperty;
	}

	public DoubleProperty getPaidAmtProperty() {
		return paidAmtProperty;
	}

	public void setPaidAmtProperty(DoubleProperty paidAmtProperty) {
		this.paidAmtProperty = paidAmtProperty;
	}

	public void setOrderDueTimeProperty(StringProperty orderDueTimeProperty) {
		this.orderDueTimeProperty = orderDueTimeProperty;
	}

	public void setLocalCustomerNameProperty(StringProperty localCustomerNameProperty) {
		this.localCustomerNameProperty = localCustomerNameProperty;
	}

//	public CustomerMst getCustomerMst() {
//		return customerMst;
//	}
//
//	public void setCustomerMst(CustomerMst customerMst) {
//		this.customerMst = customerMst;
//	}

	@JsonIgnore
	private StringProperty orderDueDateProperty;

	@JsonIgnore
	private StringProperty orderDueTimeProperty;

	@JsonIgnore
	private DoubleProperty invoiceAmountProperty;

	@JsonIgnore
	private StringProperty deliveryModeProperty;

	@JsonIgnore
	private StringProperty targetBranchProperty;

	@JsonIgnore
	private StringProperty orderMessageProperty;

	@JsonIgnore
	private StringProperty customerNameProperty;

	@JsonIgnore
	private StringProperty voucherNoProperty;

	@JsonIgnore
	private StringProperty voucherDateProperty;

	@JsonIgnore
	private DoubleProperty paidAmtProperty;

	@JsonIgnore
	private StringProperty localCustomerNameProperty;
	
	@JsonIgnore
	private StringProperty cancelationReasonProperty;


	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getOrdedBy() {
		return ordedBy;
	}

	public void setOrdedBy(String ordedBy) {
		this.ordedBy = ordedBy;
	}

	public String getOrderDueTime() {
		return orderDueTime;
	}

	public void setOrderDueTime(String orderDueTime) {
		this.orderDueTime = orderDueTime;
	}

	public String getOrderTimeMode() {
		return orderTimeMode;
	}

	public void setOrderTimeMode(String orderTimeMode) {
		this.orderTimeMode = orderTimeMode;
	}

	public String getDeliveryMode() {
		return deliveryMode;
	}

	public String getLocalCustomerName() {
		return localCustomerName;
	}

	public void setLocalCustomerName(String localCustomerName) {
		this.localCustomerName = localCustomerName;
	}

	public void setDeliveryMode(String deliveryMode) {
		this.deliveryMode = deliveryMode;
	}

	public String getConvertedToInvoice() {
		return convertedToInvoice;
	}

	public void setConvertedToInvoice(String convertedToInvoice) {
		this.convertedToInvoice = convertedToInvoice;
	}

	public Date getConvertedToInvoiceDate() {
		return convertedToInvoiceDate;
	}

//	public String getCustomerId() {
//		return customerId;
//	}
//
//	public void setCustomerId(String customerId) {
//		this.customerId = customerId;
//	}

	public void setConvertedToInvoiceDate(Date convertedToInvoiceDate) {
		this.convertedToInvoiceDate = convertedToInvoiceDate;
	}

	
	public Double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	public String getOrderMessageToPrint() {
		return orderMessageToPrint;
	}

	public void setOrderMessageToPrint(String orderMessageToPrint) {
		this.orderMessageToPrint = orderMessageToPrint;
	}

	public String getOrderAdvice() {
		return orderAdvice;
	}

	public void setOrderAdvice(String orderAdvice) {
		this.orderAdvice = orderAdvice;
	}

	public String getTargetDepartment() {
		return targetDepartment;
	}

	public void setTargetDepartment(String targetDepartment) {
		this.targetDepartment = targetDepartment;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public LocalCustomerMst getLocalCustomerId() {
		return localCustomerId;
	}

	public void setLocalCustomerId(LocalCustomerMst localCustomerId) {
		this.localCustomerId = localCustomerId;
	}

//	public void setCustomerId(CustomerMst customerId) {
//		this.customerId = customerId;
//	}
//
//	public CustomerMst getCustomerId() {
//		return customerId;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSalesManId() {
		return salesManId;
	}

	public void setSalesManId(String salesManId) {
		this.salesManId = salesManId;
	}

	public String getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(String deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}

	public String getSalesMode() {
		return salesMode;
	}

	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}

	public String getCreditOrCash() {
		return creditOrCash;
	}

	public void setCreditOrCash(String creditOrCash) {
		this.creditOrCash = creditOrCash;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}

	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}

	public Double getCashPay() {
		return cashPay;
	}

	public void setCashPay(Double cashPay) {
		this.cashPay = cashPay;
	}

	public Double getItemDiscount() {
		return itemDiscount;
	}

	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Double getChangeAmount() {
		return changeAmount;
	}

	public void setChangeAmount(Double changeAmount) {
		this.changeAmount = changeAmount;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public Double getCardamount() {
		return cardamount;
	}

	public void setCardamount(Double cardamount) {
		this.cardamount = cardamount;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getServingTableName() {
		return servingTableName;
	}

	public void setServingTableName(String servingTableName) {
		this.servingTableName = servingTableName;
	}

	public Date getOrderDue() {
		return orderDue;
	}

	public void setOrderDue(Date orderDue) {
		this.orderDue = orderDue;
	}

	@JsonIgnore
	public StringProperty getLocalCustomerNameProperty() {
		localCustomerNameProperty.set(localCustomerName);
		return localCustomerNameProperty;
	}

	public void setLocalCustomerNameProperty(String localCustomerName) {
		this.localCustomerName = localCustomerName;
	}

	@JsonIgnore
	public DoubleProperty getpaidAmtProperty() {
		paidAmtProperty.set(paidAmount);
		return paidAmtProperty;
	}

	public void setpaidAmtProperty(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	@JsonIgnore
	public DoubleProperty getInvoiceAmtProperty() {
		invoiceAmountProperty.set(invoiceAmount);
		return invoiceAmountProperty;
	}

	public void setInvoiceAmtProperty(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	@JsonIgnore
	public StringProperty getorderDueDateProperty() {
		orderDueDateProperty.set(SystemSetting.UtilDateToString(orderDue));
		return orderDueDateProperty;
	}

	public void setorderDueDateProperty(Date orderDue) {
		this.orderDue = orderDue;
	}

	@JsonIgnore
	public StringProperty getOrderDueTimeProperty() {
		orderDueTimeProperty.set(orderDueTime);
		return orderDueTimeProperty;
	}

	public void setOrderDueTimeProperty(String orderDueTime) {
		this.orderDueTime = orderDueTime;
	}

	@JsonIgnore
	public StringProperty getdeliveryModeProperty() {
		deliveryModeProperty.set(deliveryMode);
		return deliveryModeProperty;
	}

	public void setdeliveryModeProperty(String deliveryMode) {
		this.deliveryMode = deliveryMode;
	}

	@JsonIgnore
	public StringProperty gettargetBranchProperty() {
		targetBranchProperty.set(targetDepartment);
		return targetBranchProperty;
	}

	public void settargetBranchProperty(String targetDepartment) {
		this.targetDepartment = targetDepartment;
	}

	@JsonIgnore
	public StringProperty getorderMessageProperty() {
		orderMessageProperty.set(orderMessageToPrint);
		return orderMessageProperty;
	}

	public void setorderMessageProperty(String orderMessageToPrint) {
		this.orderMessageToPrint = orderMessageToPrint;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getOrderDueDay() {
		return orderDueDay;
	}

	public void setOrderDueDay(String orderDueDay) {
		this.orderDueDay = orderDueDay;
	}

	public Double getSodexoAmount() {
		return sodexoAmount;
	}

	public void setSodexoAmount(Double sodexoAmount) {
		this.sodexoAmount = sodexoAmount;
	}

	public String getSaleOrderReceiptVoucherNumber() {
		return saleOrderReceiptVoucherNumber;
	}

	public void setSaleOrderReceiptVoucherNumber(String saleOrderReceiptVoucherNumber) {
		this.saleOrderReceiptVoucherNumber = saleOrderReceiptVoucherNumber;
	}

	public StringProperty getVoucherNoProperty() {
		voucherNoProperty.set(voucherNumber);
		return voucherNoProperty;
	}

	public void setVoucherNoProperty(StringProperty voucherNoProperty) {
		this.voucherNoProperty = voucherNoProperty;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public StringProperty getCustomerNameProperty() {
		if (null != customerName) {
			customerNameProperty.set(customerName);

		}
		return customerNameProperty;
	}

	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}

	public StringProperty getVoucherDateProperty() {
		if(null != voucherDate)
		{
			voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		}
		return voucherDateProperty;
	}

	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}

	public String getCancelationReason() {
		return cancelationReason;
	}

	public void setCancelationReason(String cancelationReason) {
		this.cancelationReason = cancelationReason;
	}

	public StringProperty getCancelationReasonProperty() {
		cancelationReasonProperty.set(cancelationReason);
		return cancelationReasonProperty;
	}

	public void setCancelationReasonProperty(StringProperty cancelationReasonProperty) {
		this.cancelationReasonProperty = cancelationReasonProperty;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	

	public String getInvoicePrintType() {
		return invoicePrintType;
	}

	public void setInvoicePrintType(String invoicePrintType) {
		this.invoicePrintType = invoicePrintType;
	}

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	@Override
	public String toString() {
		return "SalesOrderTransHdr [id=" + id + ", accountHeads=" + accountHeads + ", localCustomerId="
				+ localCustomerId + ", salesManId=" + salesManId + ", deliveryBoyId=" + deliveryBoyId + ", salesMode="
				+ salesMode + ", creditOrCash=" + creditOrCash + ", customerName=" + customerName + ", orderDueTime="
				+ orderDueTime + ", orderTimeMode=" + orderTimeMode + ", orderStatus=" + orderStatus + ", userId="
				+ userId + ", sodexoAmount=" + sodexoAmount + ", customiseSalesMode=" + customiseSalesMode
				+ ", saleOrderReceiptVoucherNumber=" + saleOrderReceiptVoucherNumber + ", companyId=" + companyId
				+ ", branchCode=" + branchCode + ", performaInvoicePrinted=" + performaInvoicePrinted + ", machineId="
				+ machineId + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", invoiceAmount="
				+ invoiceAmount + ", invoiceDiscount=" + invoiceDiscount + ", cashPay=" + cashPay + ", itemDiscount="
				+ itemDiscount + ", paidAmount=" + paidAmount + ", changeAmount=" + changeAmount + ", cardNo=" + cardNo
				+ ", cardType=" + cardType + ", cardamount=" + cardamount + ", convertedToInvoice=" + convertedToInvoice
				+ ", statusDate=" + statusDate + ", paymentMode=" + paymentMode + ", deliveryMode=" + deliveryMode
				+ ", orderDueDay=" + orderDueDay + ", ordedBy=" + ordedBy + ", convertedToInvoiceDate="
				+ convertedToInvoiceDate + ", advanceAmount=" + advanceAmount + ", orderMessageToPrint="
				+ orderMessageToPrint + ", orderAdvice=" + orderAdvice + ", orderDue=" + orderDue + ", discount="
				+ discount + ", localCustomerName=" + localCustomerName + ", cashPaidSale=" + cashPaidSale
				+ ", targetDepartment=" + targetDepartment + ", servingTableName=" + servingTableName
				+ ", cancelationReason=" + cancelationReason + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", isBranchSales=" + isBranchSales + ", invoicePrintType=" + invoicePrintType
				+ ", localCustomerMst=" + localCustomerMst + ", orderDueDateProperty=" + orderDueDateProperty
				+ ", orderDueTimeProperty=" + orderDueTimeProperty + ", invoiceAmountProperty=" + invoiceAmountProperty
				+ ", deliveryModeProperty=" + deliveryModeProperty + ", targetBranchProperty=" + targetBranchProperty
				+ ", orderMessageProperty=" + orderMessageProperty + ", customerNameProperty=" + customerNameProperty
				+ ", voucherNoProperty=" + voucherNoProperty + ", voucherDateProperty=" + voucherDateProperty
				+ ", paidAmtProperty=" + paidAmtProperty + ", localCustomerNameProperty=" + localCustomerNameProperty
				+ ", cancelationReasonProperty=" + cancelationReasonProperty + "]";
	}

	
	
	

}
