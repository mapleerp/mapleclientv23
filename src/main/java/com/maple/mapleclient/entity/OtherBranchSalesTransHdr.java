
package com.maple.mapleclient.entity;

import java.time.LocalDateTime;
import java.util.Date;

 

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
 * R.George
 * Entity Corrected on 5/6
 * 
 */
public class OtherBranchSalesTransHdr {

	String id;

	String takeOrderNumber;
	private String customerId;
	private String salesManId;
	private String deliveryBoyId;
	private String salesMode;
	private String creditOrCash;
	private Long numericVoucherNumber;
	private AccountHeads accountHeads;
	private String salesReceiptsVoucherNumber;
	
	private String userId;
	
	
	private Long companyId;
	String branchCode;
	String customerName;
	private String  processInstanceId;
	private String taskId;
	//version2.9
	private String sourceBranchCode;
	//version2.9ends
	
	
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	private String machineId;

	String voucherNumber;

	private Date voucherDate;

	private Double invoiceAmount;

	private Double invoiceDiscount;

	private Double cashPay;

	private Double itemDiscount;

	private Double sodexoAmount;
	private Double paidAmount;

	private Double changeAmount;

	private String cardNo;
	private String cardType;
	private String voucherType;
	
	private LocalCustomerMst localCustomerMst;
	
	private String invoiceNumberPrefix;
	private String discount;
	

	
	private Double cardamount;

	Double cashPaidSale;
	private String servingTableName;

	private String isBranchSales;
	
	private String saleOrderHrdId;

	private String performaInvoicePrinted;
	
	
	@JsonIgnore
	private StringProperty idproperty;
	@JsonIgnore
	DoubleProperty cashPaidSalesProperty;
	
	@JsonIgnore
	private StringProperty salesModeProperty;
	
	@JsonIgnore
	private DoubleProperty paidAmountProperty;
	
	private LocalDateTime updatedTime;
	
	
	
	 
	private String SourceIP ;
	
 
	private String  SourcePort ;
	
	private String salesTransHdrId;
	
	
	 @JsonIgnore
	 private StringProperty takeOrderNumberProperty;
	@JsonIgnore
	private StringProperty voucherNoProperty;
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private StringProperty userNameProperty;
	
	@JsonIgnore
	DoubleProperty invoiceAmountProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty fromBranchProperty;
	
	
	@JsonIgnore
	String userName;
	
	public OtherBranchSalesTransHdr() {
		
		this.idproperty = new SimpleStringProperty();
		this.cashPaidSalesProperty = new SimpleDoubleProperty(0);
		this.invoiceAmountProperty = new SimpleDoubleProperty(0);
		this.voucherNoProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.userNameProperty = new SimpleStringProperty("");
		this.takeOrderNumberProperty = new SimpleStringProperty();
		
		this.fromBranchProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();

	}

	//---------version 4.19
	private String branchSaleCustomer;
	
	

	 
	public String getBranchSaleCustomer() {
		return branchSaleCustomer;
	}
	public void setBranchSaleCustomer(String branchSaleCustomer) {
		this.branchSaleCustomer = branchSaleCustomer;
	}
	
	//-------------version 4.19 end

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Double getCashPaidSale() {
		return cashPaidSale;
	}

	
	

	

	
	public String getPerformaInvoicePrinted() {
		return performaInvoicePrinted;
	}

	public void setPerformaInvoicePrinted(String performaInvoicePrinted) {
		this.performaInvoicePrinted = performaInvoicePrinted;
	}

	public void setCashPaidSale(Double cashPaidSale) {
		this.cashPaidSale = cashPaidSale;
	}

	public DoubleProperty getCashPaidSalesProperty() {
		cashPaidSalesProperty.set(cashPaidSale);
		return cashPaidSalesProperty;
	}

	public void setCashPaidSalesProperty(DoubleProperty cashPaidSalesProperty) {
		this.cashPaidSalesProperty = cashPaidSalesProperty;
	}

	
	public Long getNumericVoucherNumber() {
		return numericVoucherNumber;
	}

	public void setNumericVoucherNumber(Long numericVoucherNumber) {
		this.numericVoucherNumber = numericVoucherNumber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getSalesManId() {
		return salesManId;
	}

	public void setSalesManId(String salesManId) {
		this.salesManId = salesManId;
	}

	public String getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(String deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}

	public String getSalesMode() {
		return salesMode;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}



	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}

	public String getCreditOrCash() {
		return creditOrCash;
	}

	public void setCreditOrCash(String creditOrCash) {
		this.creditOrCash = creditOrCash;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}

	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}

	public Double getCashPay() {
		return cashPay;
	}

	public void setCashPay(Double cashPay) {
		this.cashPay = cashPay;
	}

	public Double getItemDiscount() {
		return itemDiscount;
	}

	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Double getChangeAmount() {
		return changeAmount;
	}

	public void setChangeAmount(Double changeAmount) {
		this.changeAmount = changeAmount;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public Double getCardamount() {
		return cardamount;
	}

	public void setCardamount(Double cardamount) {
		this.cardamount = cardamount;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getTakeOrderNumber() {
		return takeOrderNumber;
	}

	public void setTakeOrderNumber(String takeOrderNumber) {
		this.takeOrderNumber = takeOrderNumber;
	}

	public String getServingTableName() {
		return servingTableName;
	}

	public void setServingTableName(String servingTableName) {
		this.servingTableName = servingTableName;
	}
	@JsonIgnore

	public StringProperty getSalesModeProperty() {
		salesModeProperty.set(salesMode);
		return salesModeProperty;
	}

	public void setSalesModeProperty(String salesMode) {
		this.salesMode = salesMode;
	}
	
	
	@JsonIgnore

	public DoubleProperty getinvoiceAmountProperty() {
		invoiceAmountProperty.set(invoiceAmount);
		return invoiceAmountProperty;
	}

	public void setinvoiceAmountProperty(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	
	@JsonIgnore

	public StringProperty getUserNameProperty() {
		userNameProperty.set(userName);
		return userNameProperty;
	}

	public void setUserNameProperty(String userName) {
		this.userName = userName;
	}
	@JsonIgnore

	public StringProperty getTakeOrderNumberProperty() {
		takeOrderNumberProperty.set(takeOrderNumber);
		return takeOrderNumberProperty;
	}

	public void setTakeOrderNumberProperty(String takeOrderNumber) {
		this.takeOrderNumber = takeOrderNumber;
	}
	
	
	public StringProperty getIdProperty() {
		idproperty.set(id);
		return idproperty;
	}

	public void setIdProperty(String id) {
		this.id = id;
	}
	@JsonIgnore

	public DoubleProperty getpaidAmountProperty() {
		paidAmountProperty.set(paidAmount);
		return paidAmountProperty;
	}

	public void setpaidAmountProperty(Double paidAmount) {
		this.paidAmount = paidAmount;
	}



	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	

	public String getIsBranchSales() {
		return isBranchSales;
	}

	public void setIsBranchSales(String isBranchSales) {
		this.isBranchSales = isBranchSales;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	
	public Double getSodexoAmount() {
		return sodexoAmount;
	}

	public void setSodexoAmount(Double sodexoAmount) {
		this.sodexoAmount = sodexoAmount;
	}
	
	public LocalCustomerMst getLocalCustomerMst() {
		return localCustomerMst;
	}

	public void setLocalCustomerMst(LocalCustomerMst localCustomerMst) {
		this.localCustomerMst = localCustomerMst;
	}
	
	

	public String getSaleOrderHrdId() {
		return saleOrderHrdId;
	}

	public void setSaleOrderHrdId(String saleOrderHrdId) {
		this.saleOrderHrdId = saleOrderHrdId;
	}


	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getInvoiceNumberPrefix() {
		return invoiceNumberPrefix;
	}

	public void setInvoiceNumberPrefix(String invoiceNumberPrefix) {
		this.invoiceNumberPrefix = invoiceNumberPrefix;
	}

	public String getSalesReceiptsVoucherNumber() {
		return salesReceiptsVoucherNumber;
	}

	public void setSalesReceiptsVoucherNumber(String salesReceiptsVoucherNumber) {
		this.salesReceiptsVoucherNumber = salesReceiptsVoucherNumber;
	}
	
	

	public StringProperty getVoucherNoProperty() {
		voucherNoProperty.set(voucherNumber);
		return voucherNoProperty;
	}

	public void setVoucherNoProperty(StringProperty voucherNoProperty) {
		
		this.voucherNoProperty = voucherNoProperty;
	}

	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}

	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}

	 

	public String getSourceBranchCode() {
		return sourceBranchCode;
	}

	public void setSourceBranchCode(String sourceBranchCode) {
		this.sourceBranchCode = sourceBranchCode;
	}
	
	
	
	
	

	public String getSalesTransHdrId() {
		return salesTransHdrId;
	}

	public void setSalesTransHdrId(String salesTransHdrId) {
		this.salesTransHdrId = salesTransHdrId;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	
	
	
	public StringProperty getVoucherDateProperty() {
		
		if(null != voucherDate)
		{
			voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		}
		return voucherDateProperty;
	}

	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}

	public StringProperty getFromBranchProperty() {
		fromBranchProperty.set(sourceBranchCode);
		return fromBranchProperty;
	}

	public void setFromBranchProperty(StringProperty fromBranchProperty) {
		this.fromBranchProperty = fromBranchProperty;
	}

	public AccountHeads getAccountHeads() {
		return accountHeads;
	}

	public void setAccountHeads(AccountHeads accountHeads) {
		this.accountHeads = accountHeads;
	}

	@Override
	public String toString() {
		return "OtherBranchSalesTransHdr [id=" + id + ", takeOrderNumber=" + takeOrderNumber + ", customerId="
				+ customerId + ", salesManId=" + salesManId + ", deliveryBoyId=" + deliveryBoyId + ", salesMode="
				+ salesMode + ", creditOrCash=" + creditOrCash + ", numericVoucherNumber=" + numericVoucherNumber
				+ ", accountHeads=" + accountHeads + ", salesReceiptsVoucherNumber=" + salesReceiptsVoucherNumber
				+ ", userId=" + userId + ", companyId=" + companyId + ", branchCode=" + branchCode + ", customerName="
				+ customerName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", sourceBranchCode=" + sourceBranchCode + ", machineId=" + machineId + ", voucherNumber="
				+ voucherNumber + ", voucherDate=" + voucherDate + ", invoiceAmount=" + invoiceAmount
				+ ", invoiceDiscount=" + invoiceDiscount + ", cashPay=" + cashPay + ", itemDiscount=" + itemDiscount
				+ ", sodexoAmount=" + sodexoAmount + ", paidAmount=" + paidAmount + ", changeAmount=" + changeAmount
				+ ", cardNo=" + cardNo + ", cardType=" + cardType + ", voucherType=" + voucherType
				+ ", localCustomerMst=" + localCustomerMst + ", invoiceNumberPrefix=" + invoiceNumberPrefix
				+ ", discount=" + discount + ", cardamount=" + cardamount + ", cashPaidSale=" + cashPaidSale
				+ ", servingTableName=" + servingTableName + ", isBranchSales=" + isBranchSales + ", saleOrderHrdId="
				+ saleOrderHrdId + ", performaInvoicePrinted=" + performaInvoicePrinted + ", idproperty=" + idproperty
				+ ", cashPaidSalesProperty=" + cashPaidSalesProperty + ", salesModeProperty=" + salesModeProperty
				+ ", paidAmountProperty=" + paidAmountProperty + ", updatedTime=" + updatedTime + ", SourceIP="
				+ SourceIP + ", SourcePort=" + SourcePort + ", salesTransHdrId=" + salesTransHdrId
				+ ", takeOrderNumberProperty=" + takeOrderNumberProperty + ", voucherNoProperty=" + voucherNoProperty
				+ ", customerNameProperty=" + customerNameProperty + ", userNameProperty=" + userNameProperty
				+ ", invoiceAmountProperty=" + invoiceAmountProperty + ", voucherDateProperty=" + voucherDateProperty
				+ ", fromBranchProperty=" + fromBranchProperty + ", userName=" + userName + ", branchSaleCustomer="
				+ branchSaleCustomer + "]";
	}


	
	
	



}
