package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AdditionalExpense {
	private Double fcAmount;
	private String currencyId;
	private String accountId;
	
	PurchaseHdr purchaseHdr;
	String calculatedStatus;

	private String  processInstanceId;
	private String taskId;

	private String Id;

	 private String expenseHead;
	
	private	String accountHead;
	
	private	Double amount;
	

	private String currencyName;
	

	private	Double conversionRate;



	private StringProperty accountHeadProperty;
	private StringProperty expenseHeadProperty;
	private DoubleProperty amountProperty;
	private DoubleProperty conversionRateProperty;
	private DoubleProperty fcAmountProperty;
	private StringProperty currencyProperty;
	

	public AdditionalExpense() {


		this.accountHeadProperty = new SimpleStringProperty();
		this.expenseHeadProperty = new SimpleStringProperty();
		this.currencyProperty = new SimpleStringProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.conversionRateProperty =  new SimpleDoubleProperty();
		this.fcAmountProperty = new SimpleDoubleProperty();
	}


	@JsonIgnore
	public StringProperty getaccountHeadProperty() {
		
		if(null!=accountHead)
		{
		accountHeadProperty.set(accountHead);
		}
		return accountHeadProperty;
	}
	
	public void setaccountHeadProperty(String accountHead) {
		this.accountHead = accountHead;
	}
	
	
	public String getCalculatedStatus() {
		return calculatedStatus;
	}


	public void setCalculatedStatus(String calculatedStatus) {
		this.calculatedStatus = calculatedStatus;
	}


	@JsonIgnore
	public StringProperty getexpenseHeadProperty() {
		if(null!=expenseHead) {
		expenseHeadProperty.set(expenseHead);
		}
		return expenseHeadProperty;
	}
	
	public void setexpenseHeadProperty(String expenseHead) {
		this.expenseHead = expenseHead;
	}
	
	@JsonIgnore
	public StringProperty getcurrencyProperty() {
		if(null!=currencyName)
		{
		currencyProperty.set(currencyName);
		}
		return currencyProperty;
	}
	
	public void setcurrencyProperty(String currencyName) {
		this.currencyName = currencyName;
	}
	

	@JsonIgnore
	public DoubleProperty getamountProperty() {
		if(null!=amount) {
		amountProperty.set(amount);
		}
		return amountProperty;
	}
	
	public void setamountProperty(Double amount) {
		this.amount = amount;
	}
	
	@JsonIgnore
	public DoubleProperty getfcAmountProperty() {
		if(null!=fcAmount) {
		fcAmountProperty.set(fcAmount);
		}
		return fcAmountProperty;
	}
	
	public void setfcAmountProperty(Double fcAmount) {
		this.fcAmount = fcAmount;
	}
	

	public Double getFcAmount() {
		return fcAmount;
	}




	public void setFcAmount(Double fcAmount) {
		this.fcAmount = fcAmount;
	}




	public String getCurrencyId() {
		return currencyId;
	}




	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}




	public String getAccountId() {
		return accountId;
	}




	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}




	public String getCurrencyName() {
		return currencyName;
	}


	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}


	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}




	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}




	public String getId() {
		return Id;
	}




	public void setId(String id) {
		Id = id;
	}





	public String getExpenseHead() {
		return expenseHead;
	}




	public void setExpenseHead(String expenseHead) {
		this.expenseHead = expenseHead;
	}






	public String getAccountHead() {
		return accountHead;
	}


	public void setAccountHead(String accountHead) {
		this.accountHead = accountHead;
	}


	public Double getAmount() {
		return amount;
	}




	public void setAmount(Double amount) {
		this.amount = amount;
	}




	public Double getConversionRate() {
		return conversionRate;
	}




	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "AdditionalExpense [fcAmount=" + fcAmount + ", currencyId=" + currencyId + ", accountId=" + accountId
				+ ", purchaseHdr=" + purchaseHdr + ", calculatedStatus=" + calculatedStatus + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", Id=" + Id + ", expenseHead=" + expenseHead
				+ ", accountHead=" + accountHead + ", amount=" + amount + ", currencyName=" + currencyName
				+ ", conversionRate=" + conversionRate + "]";
	}

	

	


}
