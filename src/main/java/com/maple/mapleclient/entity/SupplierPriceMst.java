package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class SupplierPriceMst {
	
	String id;
	String supplierId;
	String itemId;

	Date updatedDate;
	private String  processInstanceId;
	private String taskId;

     Double dP;
	public SupplierPriceMst() {


		this.supplierNameProperty = new SimpleStringProperty();
		this.itemNameProperty = new SimpleStringProperty();
		this.dpProperty=new SimpleDoubleProperty();
		this.updatedDateProperty=new SimpleStringProperty();
		this.idProperty=new SimpleStringProperty();
	}
	@JsonIgnore
	String supplierName;

	@JsonIgnore
	DoubleProperty dpProperty;
	

@JsonIgnore
StringProperty idProperty;
	
@JsonIgnore
	public DoubleProperty getDpProperty() {
	if(null!=dP) {
	dpProperty.set(dP);
	}else {
		dpProperty.set(0.0);
	}
		return dpProperty;
	}

	public void setDpProperty(DoubleProperty dpProperty) {
		this.dpProperty = dpProperty;
	}




	@JsonIgnore
	StringProperty updatedDateProperty;
	@JsonIgnore
	String itemName;
	
	@JsonIgnore
	StringProperty supplierNameProperty;
	
	@JsonIgnore
	StringProperty itemNameProperty;

	

	
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@JsonIgnore
	public StringProperty getSupplierNameProperty() {
		if(null!=supplierId) {
		supplierNameProperty.set(supplierId);
		}
		return supplierNameProperty;
	}

	public void setSupplierNameProperty(StringProperty supplierNameProperty) {
		this.supplierNameProperty = supplierNameProperty;
	}
	@JsonIgnore
	public StringProperty getItemNameProperty() {
		
		if(null!=itemId) {
		itemNameProperty.set(itemId);
		}else {
			itemNameProperty.set("");
		}
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public Date getUpdatedDate() {
	
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


@JsonIgnore
	public StringProperty getUpdatedDateProperty() {
	
	String date=SystemSetting.UtilDateToString(updatedDate);
	updatedDateProperty.set(date);
		return updatedDateProperty;
	}

	public void setUpdatedDateProperty(StringProperty updatedDateProperty) {
		this.updatedDateProperty = updatedDateProperty;
	}
@JsonIgnore
	public StringProperty getIdProperty() {
	idProperty.set(id);
		return idProperty;
	}

	public void setIdProperty(StringProperty idProperty) {
		this.idProperty = idProperty;
	}

	public Double getdP() {
		return dP;
	}

	public void setdP(Double dP) {
		this.dP = dP;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "SupplierPriceMst [id=" + id + ", supplierId=" + supplierId + ", itemId=" + itemId + ", updatedDate="
				+ updatedDate + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", dP=" + dP
				+ ", supplierName=" + supplierName + ", itemName=" + itemName + "]";
	}


	

}
