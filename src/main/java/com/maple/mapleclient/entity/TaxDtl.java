package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

public class TaxDtl 
{
	 
	private String id;
	
	private String  itemId;
 
   
    
    private Double sgstTax;
    private Double cgstTax;
    private Double cessRate;
    private Double addCessRate;
    
	 
    private Date startingDate;
	 
	private Date endingDate;
	private String  processInstanceId;
	private String taskId;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	 
	public Double getSgstTax() {
		return sgstTax;
	}

	public void setSgstTax(Double sgstTax) {
		this.sgstTax = sgstTax;
	}

	public Double getCgstTax() {
		return cgstTax;
	}

	public void setCgstTax(Double cgstTax) {
		this.cgstTax = cgstTax;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Double getAddCessRate() {
		return addCessRate;
	}

	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}

	public Date getStartingDate() {
		return startingDate;
	}

	public void setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
	}

	public Date getEndingDate() {
		return endingDate;
	}

	public void setEndingDate(Date endingDate) {
		this.endingDate = endingDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "TaxDtl [id=" + id + ", itemId=" + itemId + ", sgstTax=" + sgstTax + ", cgstTax=" + cgstTax
				+ ", cessRate=" + cessRate + ", addCessRate=" + addCessRate + ", startingDate=" + startingDate
				+ ", endingDate=" + endingDate + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	
}