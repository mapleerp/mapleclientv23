package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

public class SalesAnalysis implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String branchCode;
	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;
	SalesDtl salesDtl;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public SalesDtl getSalesDtl() {
		return salesDtl;
	}
	public void setSalesDtl(SalesDtl salesDtl) {
		this.salesDtl = salesDtl;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SalesAnalysis [id=" + id + ", branchCode=" + branchCode + ", companyMst=" + companyMst
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", salesDtl=" + salesDtl + "]";
	}
	
	

}
