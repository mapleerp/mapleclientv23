package com.maple.mapleclient.entity;

import java.util.Date;


public class InvoiceEditEnableMst {
private String id;
	
	String userName;
	
	String branchCode;
	Date voucherDate;
	String voucherNumber;
	String voucherType;
	String status;
	UserMst userMst;
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public UserMst getUserMst() {
		return userMst;
	}
	public void setUserMst(UserMst userMst) {
		this.userMst = userMst;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "InvoiceEditEnableMst [id=" + id + ", userName=" + userName + ", branchCode=" + branchCode
				+ ", voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber + ", voucherType=" + voucherType
				+ ", status=" + status + ", userMst=" + userMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	
	
}
