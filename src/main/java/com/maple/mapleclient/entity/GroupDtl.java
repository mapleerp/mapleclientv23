package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
public class GroupDtl 
{
	@JsonIgnore
	private IntegerProperty groupId;
	@JsonIgnore
    private StringProperty groupName;
	@JsonIgnore
    private IntegerProperty parentId;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	public IntegerProperty getGroupIdProperty() {
		return groupId;
	}
	public void setGroupId(IntegerProperty groupId) {
		this.groupId = groupId;
	}
	@JsonProperty("groupId")
	public Integer getGroupId() {
		return groupId.get();
	}
	public void setGroupId(Integer groupId) {
		this.groupId.set(groupId);
	}
	@JsonIgnore
	public StringProperty getGroupNameProperty() {
		return groupName;
	}
	public void setGroupName(StringProperty groupName) {
		this.groupName = groupName;
	}
	@JsonProperty("groupName")
	public String getGroupName() {
		return groupName.get();
	}
	public void setGroupName(String groupName) {
		this.groupName.set(groupName);
	}
	@JsonIgnore
	public IntegerProperty getParentIdProperty() {
		return parentId;
	}
	public void setParentId(IntegerProperty parentId) {
		this.parentId = parentId;
	}
	@JsonProperty("parentId")
	public Integer getParentId() {
		return parentId.get();
	}
	public void setParentId(Integer parentId) {
		this.parentId.set(parentId);
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "GroupDtl [processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
