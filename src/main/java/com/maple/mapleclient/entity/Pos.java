package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

public class Pos {
Integer Id;
String itemName;
String genName;
String insuranceCoverage;
String salesType;
String unit;
Date mftDate;
String itemcode;
Double tax;
String currency;
Double invoiceAmount;
Double cashToPay;
Integer cardNumber;
Double itemDiscount;
Double paidAmount;
Double cardAmount;
Double changeAmount;
String recordId;
String parentId;
Integer itemId;
String barcode;
Double taxRate;
Double qty;
Double rate;
Integer unitId;
String batch;
Date expiryDate;
Integer itemServiceNumber;
Double discount;
Double fcRate;
Double fcDiscount;
Double fcTaxRate;
Integer freeQuantityParentItem;
Integer hasReturned;
String godowenCode;
Double purchaseDate;
Integer financerItem;
Double cessRate;
Double cgst;
Double sgst;
String invoiceDiscount;

private String  processInstanceId;
private String taskId;
public String getInvoiceDiscount() {
	return invoiceDiscount;
}
public void setInvoiceDiscount(String invoiceDiscount) {
	this.invoiceDiscount = invoiceDiscount;
}
public Integer getId() {
	return Id;
}
public String getItemName() {
	return itemName;
}
public void setItemName(String itemName) {
	this.itemName = itemName;
}
public String getGenName() {
	return genName;
}
public void setGenName(String genName) {
	this.genName = genName;
}
public String getInsuranceCoverage() {
	return insuranceCoverage;
}
public void setInsuranceCoverage(String insuranceCoverage) {
	this.insuranceCoverage = insuranceCoverage;
}
public String getSalesType() {
	return salesType;
}
public void setSalesType(String salesType) {
	this.salesType = salesType;
}
public String getUnit() {
	return unit;
}
public void setUnit(String unit) {
	this.unit = unit;
}
public Date getMftDate() {
	return mftDate;
}
public void setMftDate(Date mftDate) {
	this.mftDate = mftDate;
}
public String getItemcode() {
	return itemcode;
}
public void setItemcode(String itemcode) {
	this.itemcode = itemcode;
}
public Double getTax() {
	return tax;
}
public void setTax(Double tax) {
	this.tax = tax;
}
public String getCurrency() {
	return currency;
}
public void setCurrency(String currency) {
	this.currency = currency;
}
public Double getInvoiceAmount() {
	return invoiceAmount;
}
public void setInvoiceAmount(Double invoiceAmount) {
	this.invoiceAmount = invoiceAmount;
}
public Double getCashToPay() {
	return cashToPay;
}
public void setCashToPay(Double cashToPay) {
	this.cashToPay = cashToPay;
}
public Integer getCardNumber() {
	return cardNumber;
}
public void setCardNumber(Integer cardNumber) {
	this.cardNumber = cardNumber;
}
public Double getItemDiscount() {
	return itemDiscount;
}
public void setItemDiscount(Double itemDiscount) {
	this.itemDiscount = itemDiscount;
}
public Double getPaidAmount() {
	return paidAmount;
}
public void setPaidAmount(Double paidAmount) {
	this.paidAmount = paidAmount;
}
public Double getCardAmount() {
	return cardAmount;
}
public void setCardAmount(Double cardAmount) {
	this.cardAmount = cardAmount;
}
public Double getChangeAmount() {
	return changeAmount;
}
public void setChangeAmount(Double changeAmount) {
	this.changeAmount = changeAmount;
}
public String getRecordId() {
	return recordId;
}
public void setRecordId(String recordId) {
	this.recordId = recordId;
}
public String getParentId() {
	return parentId;
}
public void setParentId(String parentId) {
	this.parentId = parentId;
}
public Integer getItemId() {
	return itemId;
}
public void setItemId(Integer itemId) {
	this.itemId = itemId;
}
public String getBarcode() {
	return barcode;
}
public void setBarcode(String barcode) {
	this.barcode = barcode;
}
public Double getTaxRate() {
	return taxRate;
}
public void setTaxRate(Double taxRate) {
	this.taxRate = taxRate;
}
public Double getQty() {
	return qty;
}
public void setQty(Double qty) {
	this.qty = qty;
}
public Double getRate() {
	return rate;
}
public void setRate(Double rate) {
	this.rate = rate;
}
public Integer getUnitId() {
	return unitId;
}
public void setUnitId(Integer unitId) {
	this.unitId = unitId;
}
public String getBatch() {
	return batch;
}
public void setBatch(String batch) {
	this.batch = batch;
}
public Date getExpiryDate() {
	return expiryDate;
}
public void setExpiryDate(Date expiryDate) {
	this.expiryDate = expiryDate;
}
public Integer getItemServiceNumber() {
	return itemServiceNumber;
}
public void setItemServiceNumber(Integer itemServiceNumber) {
	this.itemServiceNumber = itemServiceNumber;
}
public Double getDiscount() {
	return discount;
}
public void setDiscount(Double discount) {
	this.discount = discount;
}
public Double getFcRate() {
	return fcRate;
}
public void setFcRate(Double fcRate) {
	this.fcRate = fcRate;
}
public Double getFcDiscount() {
	return fcDiscount;
}
public void setFcDiscount(Double fcDiscount) {
	this.fcDiscount = fcDiscount;
}
public Double getFcTaxRate() {
	return fcTaxRate;
}
public void setFcTaxRate(Double fcTaxRate) {
	this.fcTaxRate = fcTaxRate;
}
public Integer getFreeQuantityParentItem() {
	return freeQuantityParentItem;
}
public void setFreeQuantityParentItem(Integer freeQuantityParentItem) {
	this.freeQuantityParentItem = freeQuantityParentItem;
}
public Integer getHasReturned() {
	return hasReturned;
}
public void setHasReturned(Integer hasReturned) {
	this.hasReturned = hasReturned;
}
public String getGodowenCode() {
	return godowenCode;
}
public void setGodowenCode(String godowenCode) {
	this.godowenCode = godowenCode;
}
public Double getPurchaseDate() {
	return purchaseDate;
}
public void setPurchaseDate(Double purchaseDate) {
	this.purchaseDate = purchaseDate;
}
public Integer getFinancerItem() {
	return financerItem;
}
public void setFinancerItem(Integer financerItem) {
	this.financerItem = financerItem;
}
public Double getCessRate() {
	return cessRate;
}
public void setCessRate(Double cessRate) {
	this.cessRate = cessRate;
}
public Double getCgst() {
	return cgst;
}
public void setCgst(Double cgst) {
	this.cgst = cgst;
}
public Double getSgst() {
	return sgst;
}
public void setSgst(Double sgst) {
	this.sgst = sgst;
}

public String getProcessInstanceId() {
	return processInstanceId;
}
public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}
public String getTaskId() {
	return taskId;
}
public void setTaskId(String taskId) {
	this.taskId = taskId;
}
@Override
public String toString() {
	return "Pos [Id=" + Id + ", itemName=" + itemName + ", genName=" + genName + ", insuranceCoverage="
			+ insuranceCoverage + ", salesType=" + salesType + ", unit=" + unit + ", mftDate=" + mftDate + ", itemcode="
			+ itemcode + ", tax=" + tax + ", currency=" + currency + ", invoiceAmount=" + invoiceAmount + ", cashToPay="
			+ cashToPay + ", cardNumber=" + cardNumber + ", itemDiscount=" + itemDiscount + ", paidAmount=" + paidAmount
			+ ", cardAmount=" + cardAmount + ", changeAmount=" + changeAmount + ", recordId=" + recordId + ", parentId="
			+ parentId + ", itemId=" + itemId + ", barcode=" + barcode + ", taxRate=" + taxRate + ", qty=" + qty
			+ ", rate=" + rate + ", unitId=" + unitId + ", batch=" + batch + ", expiryDate=" + expiryDate
			+ ", itemServiceNumber=" + itemServiceNumber + ", discount=" + discount + ", fcRate=" + fcRate
			+ ", fcDiscount=" + fcDiscount + ", fcTaxRate=" + fcTaxRate + ", freeQuantityParentItem="
			+ freeQuantityParentItem + ", hasReturned=" + hasReturned + ", godowenCode=" + godowenCode
			+ ", purchaseDate=" + purchaseDate + ", financerItem=" + financerItem + ", cessRate=" + cessRate + ", cgst="
			+ cgst + ", sgst=" + sgst + ", invoiceDiscount=" + invoiceDiscount + ", processInstanceId="
			+ processInstanceId + ", taskId=" + taskId + "]";
}




}
