package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;

public class OwnAccount {
	String id;
	Date voucherDate;
	 String voucherNo;
	 String accountId;
	 String accountType;
	 Double debitAmount;
	 Double creditAmount;
	 Double realizedAmount;
	 Double balanceAmount;
	 String ownAccountStatus;
	 
	 PaymentHdr paymentHdr;
	 ReceiptHdr receiptHdr;
	 private String  processInstanceId;
		private String taskId;
	 @JsonIgnore
	 private DoubleProperty debitAmountProperty;
	 
	 @JsonIgnore
	 private DoubleProperty creditAmountProperty;
	 
	 @JsonIgnore
	 private DoubleProperty realizedAmountProperty;
	 
	 @JsonIgnore
	 private DoubleProperty balanceAmountProperty;
	 
	public OwnAccount() {
		
		this.debitAmountProperty = new SimpleDoubleProperty();
		this.creditAmountProperty = new SimpleDoubleProperty();
		this.realizedAmountProperty = new SimpleDoubleProperty();
		this.balanceAmountProperty = new SimpleDoubleProperty();
		this.balanceAmount = 0.0;
		}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public Double getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(Double debitAmount) {
		this.debitAmount = debitAmount;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public Double getRealizedAmount() {
		return realizedAmount;
	}
	public void setRealizedAmount(Double realizedAmount) {
		this.realizedAmount = realizedAmount;
	}
	public String getOwnAccountStatus() {
		return ownAccountStatus;
	}
	public void setOwnAccountStatus(String ownAccountStatus) {
		this.ownAccountStatus = ownAccountStatus;
	}
	public PaymentHdr getPaymenthdr() {
		return paymentHdr;
	}
	public void setPaymenthdr(PaymentHdr paymentHdr) {
		this.paymentHdr = paymentHdr;
	}
	public ReceiptHdr getReceiptHdr() {
		return receiptHdr;
	}
	public void setReceiptHdr(ReceiptHdr receiptHdr) {
		this.receiptHdr = receiptHdr;
	}
	
	@JsonIgnore
	public void setdebitAmountProperty(Double debitAmount)
	{
		this.debitAmount = debitAmount;
	}
	public DoubleProperty getDebitAmountProperty()
	{
		debitAmountProperty.set(debitAmount);
		return debitAmountProperty;
	}
	public void setcreditAmountProperty(Double creditAmount)
	{
		this.creditAmount = creditAmount;
	}
	public DoubleProperty getcreditAmountProperty()
	{
		creditAmountProperty.set(creditAmount);
		return creditAmountProperty;
	}
	public void setrealizedAmountProperty(Double realizedAmount)
	{
		this.realizedAmount = realizedAmount;
	}
	public DoubleProperty getrealizedAmountProperty()
	{
		realizedAmountProperty.set(realizedAmount);
		return realizedAmountProperty;
	}
	public void setbalanceAmountProperty(Double balanceAmount)
	{
		this.balanceAmount = balanceAmount;
	}
	public DoubleProperty getbalanceAmountProperty()
	{
		balanceAmountProperty.set(balanceAmount);
		return balanceAmountProperty;
	}
	
	public Double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "OwnAccount [id=" + id + ", voucherDate=" + voucherDate + ", voucherNo=" + voucherNo + ", accountId="
				+ accountId + ", accountType=" + accountType + ", debitAmount=" + debitAmount + ", creditAmount="
				+ creditAmount + ", realizedAmount=" + realizedAmount + ", balanceAmount=" + balanceAmount
				+ ", ownAccountStatus=" + ownAccountStatus + ", paymentHdr=" + paymentHdr + ", receiptHdr=" + receiptHdr
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	 
}
