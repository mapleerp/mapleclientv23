package com.maple.mapleclient.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CustomInvoicePropertyFormat implements Serializable {
	private static final long serialVersionUID = 1L;

	String id;
	String customSalesMode;
	String propertyName;
	String propertyValue;
	
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	StringProperty customSalesModeProperty;
	
	@JsonIgnore
	StringProperty propertyNameProperty;
	
	@JsonIgnore
	StringProperty propertyValueProperty;
	
	
	public CustomInvoicePropertyFormat() {
		this.customSalesModeProperty = new SimpleStringProperty();
		this.propertyNameProperty =  new SimpleStringProperty();
		this.propertyValueProperty = new SimpleStringProperty();
	}
	
	public StringProperty getCustomSalesModeProperty() {
		customSalesModeProperty.set(customSalesMode);
		return customSalesModeProperty;
	}

	public void setCustomSalesModeProperty(String customSalesMode) {
		this.customSalesMode = customSalesMode;
	}

	public StringProperty getPropertyNameProperty() {
		propertyNameProperty.set(propertyName);
		return propertyNameProperty;
	}

	public void setPropertyNameProperty(String propertyName) {
		this.propertyName = propertyName;
	}

	public StringProperty getPropertyValueProperty() {
		propertyValueProperty.set(propertyValue);
		return propertyValueProperty;
	}

	public void setPropertyValueProperty(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomSalesMode() {
		return customSalesMode;
	}
	public void setCustomSalesMode(String customSalesMode) {
		this.customSalesMode = customSalesMode;
	}
	

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "CustomInvoicePropertyFormat [id=" + id + ", customSalesMode=" + customSalesMode + ", propertyName="
				+ propertyName + ", propertyValue=" + propertyValue + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	

}
