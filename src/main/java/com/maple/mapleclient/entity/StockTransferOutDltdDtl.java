package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class StockTransferOutDltdDtl implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private String id;

		private String itemId;

		private String unitId;

		private Double qty;
		
		Double mrp;

		String branchCode;
		
		CompanyMst companyMst;
		private String  processInstanceId;
		private String taskId;
		
		@JsonIgnore
		private String toBranch;
		
		@JsonIgnore
		private String voucherNumber;
		
		@JsonIgnore
		private String itemName;
		
		@JsonIgnore
		private String unitName;
		
		
		
		public StockTransferOutDltdDtl() {
			this.toBranchProperty = new SimpleStringProperty("");
			this.voucherNumberProperty = new SimpleStringProperty("");
			this.itemNameProperty = new SimpleStringProperty("");
			this.unitNameProperty = new SimpleStringProperty("");
			
			this.qtyProperty =  new SimpleDoubleProperty(0.0);
			this.mrpProperty =  new SimpleDoubleProperty(0.0);


		}

		@JsonIgnore
		private StringProperty toBranchProperty;
		
		@JsonIgnore
		private StringProperty voucherNumberProperty;
		
		@JsonIgnore
		private StringProperty itemNameProperty;
		
		@JsonIgnore
		private StringProperty unitNameProperty;
		
		@JsonIgnore
		private DoubleProperty qtyProperty;
		
		@JsonIgnore
		private DoubleProperty mrpProperty;
		
		StockTransferOutHdr stockTransferOutHdr;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getItemId() {
			return itemId;
		}

		public void setItemId(String itemId) {
			this.itemId = itemId;
		}

		public String getUnitId() {
			return unitId;
		}

		public void setUnitId(String unitId) {
			this.unitId = unitId;
		}

		public Double getQty() {
			return qty;
		}

		public void setQty(Double qty) {
			this.qty = qty;
		}

		public Double getMrp() {
			return mrp;
		}

		public void setMrp(Double mrp) {
			this.mrp = mrp;
		}

		public String getBranchCode() {
			return branchCode;
		}

		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}

		public CompanyMst getCompanyMst() {
			return companyMst;
		}

		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}

		public StockTransferOutHdr getStockTransferOutHdr() {
			return stockTransferOutHdr;
		}

		public void setStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr) {
			this.stockTransferOutHdr = stockTransferOutHdr;
		}

		public StringProperty getToBranchProperty() {
			toBranchProperty.set(toBranch);
			return toBranchProperty;
		}

		public void setToBranchProperty(StringProperty toBranchProperty) {
			this.toBranchProperty = toBranchProperty;
		}

		public StringProperty getVoucherNumberProperty() {
			voucherNumberProperty.set(voucherNumber);
			return voucherNumberProperty;
		}

		public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
			this.voucherNumberProperty = voucherNumberProperty;
		}

		public StringProperty getItemNameProperty() {
			itemNameProperty.set(itemName);
			return itemNameProperty;
		}

		public void setItemNameProperty(StringProperty itemNameProperty) {
			this.itemNameProperty = itemNameProperty;
		}

		public StringProperty getUnitNameProperty() {
			unitNameProperty.set(unitName);
			return unitNameProperty;
		}

		public void setUnitNameProperty(StringProperty unitNameProperty) {
			this.unitNameProperty = unitNameProperty;
		}

		public DoubleProperty getQtyProperty() {
			qtyProperty.set(qty);
			return qtyProperty;
		}

		public void setQtyProperty(DoubleProperty qtyProperty) {
			this.qtyProperty = qtyProperty;
		}

		public DoubleProperty getMrpProperty() {
			mrpProperty.set(mrp);
			return mrpProperty;
		}

		public void setMrpProperty(DoubleProperty mrpProperty) {
			this.mrpProperty = mrpProperty;
		}

		public String getToBranch() {
			return toBranch;
		}

		public void setToBranch(String toBranch) {
			this.toBranch = toBranch;
		}

		public String getVoucherNumber() {
			return voucherNumber;
		}

		public void setVoucherNumber(String voucherNumber) {
			this.voucherNumber = voucherNumber;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public String getUnitName() {
			return unitName;
		}

		public void setUnitName(String unitName) {
			this.unitName = unitName;
		}

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "StockTransferOutDltdDtl [id=" + id + ", itemId=" + itemId + ", unitId=" + unitId + ", qty=" + qty
					+ ", mrp=" + mrp + ", branchCode=" + branchCode + ", companyMst=" + companyMst
					+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", toBranch=" + toBranch
					+ ", voucherNumber=" + voucherNumber + ", itemName=" + itemName + ", unitName=" + unitName
					+ ", stockTransferOutHdr=" + stockTransferOutHdr + "]";
		}
		
		
		
		
}
