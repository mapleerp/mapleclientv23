package com.maple.mapleclient.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

public class Sales {
	Integer Id;
	private String  processInstanceId;
	private String taskId;
	public Integer getId() {
		return Id;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	String customerName;
	Date date;
	String item;
	String quantity;
	String barcode;
	String batch;
	String rate;
	String amount;
	String discount;
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "Sales [Id=" + Id + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", customerName="
				+ customerName + ", date=" + date + ", item=" + item + ", quantity=" + quantity + ", barcode=" + barcode
				+ ", batch=" + batch + ", rate=" + rate + ", amount=" + amount + ", discount=" + discount + "]";
	}
	

}
