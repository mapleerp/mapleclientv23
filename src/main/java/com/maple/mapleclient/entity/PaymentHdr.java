
package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

 

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PaymentHdr {
 
	 String id;
	 private String  processInstanceId;
	
	
	@JsonProperty("voucherNumber")
	private String voucherNumber;
	@JsonProperty("userId")
	private String userId;
	@JsonProperty("companyId")
	private String companyId;
	@JsonProperty("branchCode")
	private String branchCode;
	@JsonProperty("machineId")
	private String machineId;
	@JsonProperty("voucherDate")
	private String voucherType;
	@JsonProperty("voucherDate")
	private java.util.Date voucherDate;


	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	@JsonIgnore
	private StringProperty voucherTypeProperty;
	

	private String taskId;
	
public	PaymentHdr(){
		
		this.voucherNumberProperty=new SimpleStringProperty("");
		this.voucherDateProperty=new SimpleStringProperty("");
		this.voucherTypeProperty=new SimpleStringProperty("");
		
	}
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public java.util.Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(java.util.Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		
		return voucherNumberProperty;
	}



	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		
		
		this.voucherNumberProperty = voucherNumberProperty;
	}



	public StringProperty getVoucherDateProperty() {
		if(null!=voucherDate) {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate));
		}
		return voucherDateProperty;
	}



	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}



	public StringProperty getVoucherTypeProperty() {
		voucherTypeProperty.set(voucherType);
		
		return voucherTypeProperty;
	}



	public void setVoucherTypeProperty(StringProperty voucherTypeProperty) {
		this.voucherTypeProperty = voucherTypeProperty;
	}



	


	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	@Override
	public String toString() {
		return "PaymentHdr [id=" + id + ", processInstanceId=" + processInstanceId + ", voucherNumber=" + voucherNumber
				+ ", userId=" + userId + ", companyId=" + companyId + ", branchCode=" + branchCode + ", machineId="
				+ machineId + ", voucherType=" + voucherType + ", voucherDate=" + voucherDate + ", taskId=" + taskId
				+ "]";
	}



}
