package com.maple.mapleclient.entity;


import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MeetingMinutesDtl  {
	

		
	
	




		@JsonIgnore
	    private StringProperty meetingIdProperty;
	  @JsonIgnore
	    private StringProperty orgIdProperty;
	  @JsonIgnore
	    private StringProperty minutesIdProperty;
	  @JsonIgnore
	    private StringProperty minutesTextProperty;
	  @JsonIgnore
	    private StringProperty minutePointbyMemberProperty;
	  @JsonIgnore
		private ObjectProperty<LocalDate> dateOfAction;

	  
	
	  String id;
	String meetingId;
	String minutesId;
	String minutesText;
	String orgId;
	String minutePointbyMember;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public MeetingMinutesDtl() {
		this.meetingIdProperty= new SimpleStringProperty("");
		this.orgIdProperty= new SimpleStringProperty("");
		this.minutesIdProperty= new SimpleStringProperty("");
		this.minutesTextProperty= new SimpleStringProperty("");
		this.minutePointbyMemberProperty= new SimpleStringProperty("");
		
		this.dateOfAction =new SimpleObjectProperty<LocalDate>(null);
	}
	@JsonIgnore
	public StringProperty getMeetingIdProperty() {
			meetingIdProperty.set(meetingId);
			return meetingIdProperty;
	}
	public void setMeetingIdProperty(String meeting) {
		this.meetingId = meetingId;
	}
	
	@JsonIgnore
	public StringProperty getOrgIdProperty() {
		orgIdProperty.set(orgId);
		return orgIdProperty;
	}
	public void setOrgIdProperty(String orgId) {
		this.orgId = orgId;
	}

	@JsonIgnore
	public StringProperty getMinutesIdProperty() {
		minutesIdProperty.set(minutesId);
		return minutesIdProperty;
	}
	public void setMinutesIdProperty(String minutesId) {
		this.minutesId = minutesId;
	}
	@JsonIgnore
	public StringProperty getMinutesTextProperty() {
		minutesTextProperty.set(minutesText);
		return minutesTextProperty;
	}
	public void setMinutesTextProperty(String minutesText) {
		this.minutesText = minutesText;
	}
	@JsonIgnore
	public StringProperty getMinutePointbyMemberProperty() {
		minutePointbyMemberProperty.set(minutePointbyMember);
		return minutePointbyMemberProperty;
	}
	public void setMinutePointbyMemberProperty(String minutePointbyMember) {
		this.minutePointbyMember = minutePointbyMember;
	}

	public ObjectProperty<LocalDate> getDateOfActionProperty() {
		return dateOfAction;
	}
	public void setDateOfActionProperty(ObjectProperty<LocalDate> dateOfAction) {
		this.dateOfAction = dateOfAction;
	}	
	@JsonProperty("dateOfAction")
	public java.sql.Date getDateOfAction ( ) {
		if(null==this.dateOfAction.get() ) {
			return null;
		}else {
			return Date.valueOf(this.dateOfAction.get() );
		}
	}
   public void setDateOfAction(java.sql.Date dateOfActionProperty) {
						if(null!=dateOfActionProperty)
		this.dateOfAction.set(dateOfActionProperty.toLocalDate());
	}
	
	
//   public ObjectProperty<LocalDate> getdateOfActionProperty( ) {
//		return dateOfAction;
//	}
//	public void setdateOfActionProperty(ObjectProperty<LocalDate> dateOfAction) {
//		this.dateOfAction = dateOfAction;
//	}
//	
//	  @JsonProperty("dateOfAction")
//	  public java.sql.Date getdateOfAction ( ) {
//	  if(null==this.dateOfAction.get() ) 
//	  { return null; }else { return
//	  Date.valueOf(this.dateOfAction.get() ); } }
//	  public void setdateOfAction(java.sql.Date dateOfActionProperty) 
//	  { if(null!=dateOfActionProperty)
//	  this.dateOfAction.set(dateOfActionProperty.toLocalDate()); }
//	 
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMeetingId() {
		return meetingId;
	}
	public void setMeetingId(String meetingId) {
		this.meetingId = meetingId;
	}
	public String getMinutesId() {
		return minutesId;
	}
	public void setMinutesId(String minutesId) {
		this.minutesId = minutesId;
	}
	public String getMinutesText() {
		return minutesText;
	}
	public void setMinutesText(String minutesText) {
		this.minutesText = minutesText;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getMinutePointbyMember() {
		return minutePointbyMember;
	}
	public void setMinutePointbyMember(String minutePointbyMember) {
		this.minutePointbyMember = minutePointbyMember;
	}

	
	
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "MeetingMinutesDtl [id=" + id + ", meetingId=" + meetingId + ", minutesId=" + minutesId
				+ ", minutesText=" + minutesText + ", orgId=" + orgId + ", minutePointbyMember=" + minutePointbyMember
				+ ", branchCode=" + branchCode + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	

}
