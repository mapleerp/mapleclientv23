package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BranchCategoryMst {
	
	String id;
	String categoryId;
	String branchCode;
	
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	String branchName;

	
	@JsonIgnore
	String categoryName;
	
	@JsonIgnore
	StringProperty categoryNameProperty;
	
	@JsonIgnore
	StringProperty branchNameProperty;


	public BranchCategoryMst() {


		this.categoryNameProperty = new SimpleStringProperty();
		this.branchNameProperty = new SimpleStringProperty();
		
	}

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public StringProperty getCategoryNameProperty() {
		categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}


	public void setCategoryNameProperty(StringProperty categoryNameProperty) {
		this.categoryNameProperty = categoryNameProperty;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public StringProperty getBranchNameProperty() {
		branchNameProperty.set(branchName);
		return branchNameProperty;
	}


	public void setBranchNameProperty(StringProperty branchNameProperty) {
		this.branchNameProperty = branchNameProperty;
	}


	


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "BranchCategoryMst [id=" + id + ", categoryId=" + categoryId + ", branchCode=" + branchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", branchName=" + branchName
				+ ", categoryName=" + categoryName + "]";
	}



	
}
