package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class SalesDltdDtl {

	   String id;
		 String branchCode;
		String itemId;
		Double qty;
		Double rate;
		Double cgstTaxRate;
		Double sgstTaxRate;
		Double cessRate;
		Double addCessRate;
		Double igstTaxRate;
		 java.util.Date voucherDate;
		String itemTaxaxId;
		String unitId;
		String itemName;
		Date expiryDate;
		String batch;
		String barcode;
		
		Double taxRate;
		Double mrp;

		Double amount;
		String unitName;
	    Double discount;
	    Double cgstAmount;
	    Double sgstAmount;
	    Double igstAmount;
	    
	    Double cessAmount;
	    Double returnedQty;
	    String status;
	    Double addCessAmount;
	    Double costPrice;
		
		 LocalDateTime updatedTime;
	    
	    String offerReferenceId;
		String schemeId;
		 
		Double standardPrice;
		private String  processInstanceId;
		private String taskId;
		 
		String fbKey;
		StringProperty itemNameProperty;
		StringProperty unitNameProperty;
		StringProperty itemCodeProperty;
		DoubleProperty mrpProperty;
		DoubleProperty rateProperty;
		
		DoubleProperty taxRateProperty;
		StringProperty barcodeProperty;
		StringProperty batchProperty;
		DoubleProperty qtyProperty;
		DoubleProperty amountProperty;
		@JsonIgnore
		StringProperty voucherDateProperty;
		
		DoubleProperty cessRateProperty;
		
		
		public SalesDltdDtl() {
			
			this.itemNameProperty = new SimpleStringProperty();
			this.unitNameProperty = new SimpleStringProperty();
			this.itemCodeProperty = new SimpleStringProperty();
			this.mrpProperty =new SimpleDoubleProperty();
			this.rateProperty = new SimpleDoubleProperty();
			this.taxRateProperty = new SimpleDoubleProperty();
			this.barcodeProperty =new SimpleStringProperty();
			this.batchProperty =new SimpleStringProperty();
			this.qtyProperty =new SimpleDoubleProperty();
			this.amountProperty =new SimpleDoubleProperty();
			this.voucherDateProperty =new SimpleStringProperty();
			this.cessRateProperty = new SimpleDoubleProperty();
		}
		
		@JsonIgnore
		public DoubleProperty getcessRateProperty() {
			cessRateProperty.set(cessRate);
			return cessRateProperty;
		}

		public void setcessRateProperty(Double cessRate) {
			this.cessRate = cessRate;
		}
		@JsonIgnore
		public DoubleProperty getamountProperty() {
			amountProperty.set(amount);
			return amountProperty;
		}

		public void setamountProperty(Double amount) {
			this.amount = amount;
		}
		@JsonIgnore
		public DoubleProperty getqtyProperty() {
			qtyProperty.set(qty);
			return qtyProperty;
		}

		public void setqtyProperty(Double qty) {
			this.qty = qty;
		}
		@JsonIgnore
		public DoubleProperty gettaxRateProperty() {
			taxRateProperty.set(taxRate);
			return taxRateProperty;
		}

		public void settaxRateProperty(Double taxRate) {
			this.taxRate = taxRate;
		}
		@JsonIgnore
		public DoubleProperty getrateProperty() {
			rateProperty.set(rate);
			return rateProperty;
		}

		public void setrateProperty(Double rate) {
			this.rate = rate;
		}
		@JsonIgnore
		public DoubleProperty getmrpProperty() {
			mrpProperty.set(mrp);
			return mrpProperty;
		}

		public void setdebitAmountProperty(Double mrp) {
			this.mrp = mrp;
		}
		public StringProperty getvoucherDateProperty() {
			voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate));
			return voucherDateProperty;
		}
		public void setvoucherDateProperty(Date voucherDate) {
			this.voucherDate = voucherDate;
		}
		public StringProperty getbatchProperty() {
			batchProperty.set(batch);
			return batchProperty;
		}
		public void setbatchProperty(String batch) {
			this.batch = batch;
		}
		public StringProperty getItemNameProperty() {
			itemNameProperty.set(itemName);
			return itemNameProperty;
		}
		public void setItemNameProperty(String itemName) {
			this.itemName = itemName;
		}
		
		public StringProperty getunitNameProperty() {
			unitNameProperty.set(unitName);
			return unitNameProperty;
		}
		public void setunitNameProperty(String unitName) {
			this.unitName = unitName;
		}
		
		public StringProperty getbarcodeProperty() {
			barcodeProperty.set(barcode);
			return barcodeProperty;
		}
		public void setbarcodeProperty(String barcode) {
			this.barcode = barcode;
		}
		
		
		public String getBranchCode() {
			return branchCode;
		}

		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getItemId() {
			return itemId;
		}

		public void setItemId(String itemId) {
			this.itemId = itemId;
		}

		
		public java.util.Date getVoucherDate() {
			return voucherDate;
		}

		public void setVoucherDate(java.util.Date voucherDate) {
			this.voucherDate = voucherDate;
		}

		public Double getQty() {
			return qty;
		}

		public void setQty(Double qty) {
			this.qty = qty;
		}

		public Double getRate() {
			return rate;
		}

		public void setRate(Double rate) {
			this.rate = rate;
		}

		public Double getCgstTaxRate() {
			return cgstTaxRate;
		}

		public void setCgstTaxRate(Double cgstTaxRate) {
			this.cgstTaxRate = cgstTaxRate;
		}

		public Double getSgstTaxRate() {
			return sgstTaxRate;
		}

		public void setSgstTaxRate(Double sgstTaxRate) {
			this.sgstTaxRate = sgstTaxRate;
		}

		public Double getCessRate() {
			return cessRate;
		}

		public void setCessRate(Double cessRate) {
			this.cessRate = cessRate;
		}

		public Double getAddCessRate() {
			return addCessRate;
		}

		public void setAddCessRate(Double addCessRate) {
			this.addCessRate = addCessRate;
		}

		public Double getIgstTaxRate() {
			return igstTaxRate;
		}

		public void setIgstTaxRate(Double igstTaxRate) {
			this.igstTaxRate = igstTaxRate;
		}

		public String getItemTaxaxId() {
			return itemTaxaxId;
		}

		public void setItemTaxaxId(String itemTaxaxId) {
			this.itemTaxaxId = itemTaxaxId;
		}

		public String getUnitId() {
			return unitId;
		}

		public void setUnitId(String unitId) {
			this.unitId = unitId;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public Date getExpiryDate() {
			return expiryDate;
		}

		public void setExpiryDate(Date expiryDate) {
			this.expiryDate = expiryDate;
		}

		public String getBatch() {
			return batch;
		}

		public void setBatch(String batch) {
			this.batch = batch;
		}

		public String getBarcode() {
			return barcode;
		}

		public void setBarcode(String barcode) {
			this.barcode = barcode;
		}

		public Double getTaxRate() {
			return taxRate;
		}

		public void setTaxRate(Double taxRate) {
			this.taxRate = taxRate;
		}

		public Double getMrp() {
			return mrp;
		}

		public void setMrp(Double mrp) {
			this.mrp = mrp;
		}

		public Double getAmount() {
			return amount;
		}

		public void setAmount(Double amount) {
			this.amount = amount;
		}

		public String getUnitName() {
			return unitName;
		}

		public void setUnitName(String unitName) {
			this.unitName = unitName;
		}

		public Double getDiscount() {
			return discount;
		}

		public void setDiscount(Double discount) {
			this.discount = discount;
		}

		public Double getCgstAmount() {
			return cgstAmount;
		}

		public void setCgstAmount(Double cgstAmount) {
			this.cgstAmount = cgstAmount;
		}

		public Double getSgstAmount() {
			return sgstAmount;
		}

		public void setSgstAmount(Double sgstAmount) {
			this.sgstAmount = sgstAmount;
		}

		public Double getIgstAmount() {
			return igstAmount;
		}

		public void setIgstAmount(Double igstAmount) {
			this.igstAmount = igstAmount;
		}

		public Double getCessAmount() {
			return cessAmount;
		}

		public void setCessAmount(Double cessAmount) {
			this.cessAmount = cessAmount;
		}

		public Double getReturnedQty() {
			return returnedQty;
		}

		public void setReturnedQty(Double returnedQty) {
			this.returnedQty = returnedQty;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Double getAddCessAmount() {
			return addCessAmount;
		}

		public void setAddCessAmount(Double addCessAmount) {
			this.addCessAmount = addCessAmount;
		}

		public Double getCostPrice() {
			return costPrice;
		}

		public void setCostPrice(Double costPrice) {
			this.costPrice = costPrice;
		}

		public LocalDateTime getUpdatedTime() {
			return updatedTime;
		}

		public void setUpdatedTime(LocalDateTime updatedTime) {
			this.updatedTime = updatedTime;
		}

		public String getOfferReferenceId() {
			return offerReferenceId;
		}

		public void setOfferReferenceId(String offerReferenceId) {
			this.offerReferenceId = offerReferenceId;
		}

		public String getSchemeId() {
			return schemeId;
		}

		public void setSchemeId(String schemeId) {
			this.schemeId = schemeId;
		}

		public Double getStandardPrice() {
			return standardPrice;
		}

		public void setStandardPrice(Double standardPrice) {
			this.standardPrice = standardPrice;
		}

		public String getFbKey() {
			return fbKey;
		}

		public void setFbKey(String fbKey) {
			this.fbKey = fbKey;
		}

	

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "SalesDltdDtl [id=" + id + ", branchCode=" + branchCode + ", itemId=" + itemId + ", qty=" + qty
					+ ", rate=" + rate + ", cgstTaxRate=" + cgstTaxRate + ", sgstTaxRate=" + sgstTaxRate + ", cessRate="
					+ cessRate + ", addCessRate=" + addCessRate + ", igstTaxRate=" + igstTaxRate + ", voucherDate="
					+ voucherDate + ", itemTaxaxId=" + itemTaxaxId + ", unitId=" + unitId + ", itemName=" + itemName
					+ ", expiryDate=" + expiryDate + ", batch=" + batch + ", barcode=" + barcode + ", taxRate="
					+ taxRate + ", mrp=" + mrp + ", amount=" + amount + ", unitName=" + unitName + ", discount="
					+ discount + ", cgstAmount=" + cgstAmount + ", sgstAmount=" + sgstAmount + ", igstAmount="
					+ igstAmount + ", cessAmount=" + cessAmount + ", returnedQty=" + returnedQty + ", status=" + status
					+ ", addCessAmount=" + addCessAmount + ", costPrice=" + costPrice + ", updatedTime=" + updatedTime
					+ ", offerReferenceId=" + offerReferenceId + ", schemeId=" + schemeId + ", standardPrice="
					+ standardPrice + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", fbKey="
					+ fbKey + "]";
		}
		 
}
