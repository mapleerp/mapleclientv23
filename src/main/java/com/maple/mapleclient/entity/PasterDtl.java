package com.maple.mapleclient.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PasterDtl {
	String id;
	String memberName;
	String pasterId;
	String memberId;
	private String  processInstanceId;
	private String taskId;
	//Date pasterMstDoj;
	
	@JsonIgnore
    private StringProperty memberNameProperty;
	@JsonIgnore
    private StringProperty pasterIdProperty;
	@JsonIgnore
    private StringProperty memberIdProperty;
	@JsonIgnore
	private ObjectProperty<LocalDate> pasterMstDoj;
	
	String branchCode;
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	
	public PasterDtl() {
		
		this.memberNameProperty = new SimpleStringProperty("");
		this.pasterIdProperty = new SimpleStringProperty("");
		this.memberIdProperty = new SimpleStringProperty("");
		this.pasterMstDoj = new SimpleObjectProperty<LocalDate>(null);
	}
	public StringProperty getMemberNameProperty() {
		memberNameProperty.set(memberName);
		return memberNameProperty;
	}
	public void setMemberNameProperty(String memberName) {
		this.memberName = memberName;
	}
	public StringProperty getPasterIdProperty() {
		pasterIdProperty.set(pasterId);
		return pasterIdProperty;
	}
	public void setPasterIdProperty(String pasterId) {
		this.pasterId = pasterId;
	}
	public StringProperty getMemberIdProperty() {
		memberIdProperty.set(memberId);
		return memberIdProperty;
	}
	public void setMemberIdProperty(String memberId) {
		this.memberId = memberId;
	}
	
	
	public ObjectProperty<LocalDate> getPasterMstDojProperty() {
		return pasterMstDoj;
	}
	public void setPasterMstDojProperty(ObjectProperty<LocalDate> pasterMstDoj) {
		this.pasterMstDoj = pasterMstDoj;
	}	
	@JsonProperty("pasterMstDoj")
	public java.sql.Date getpasterMstDoj ( ) {
		if(null==this.pasterMstDoj.get() ) {
			return null;
		}else {
			return Date.valueOf(this.pasterMstDoj.get() );
		}
	}
   public void setpasterMstDoj(java.sql.Date pasterMstDojProperty) {
						if(null!=pasterMstDojProperty)
		this.pasterMstDoj.set(pasterMstDojProperty.toLocalDate());
	}
	
	
   public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	
	
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getPasterId() {
		return pasterId;
	}
	public void setPasterId(String pasterId) {
		this.pasterId = pasterId;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "PasterDtl [id=" + id + ", memberName=" + memberName + ", pasterId=" + pasterId + ", memberId="
				+ memberId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", branchCode="
				+ branchCode + "]";
	}

	
}
