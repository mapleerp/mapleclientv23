package com.maple.mapleclient.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class JobCardInHdr {
	String id;
	String fromBranch;
	String branchCode;
	String voucherNumber;
	String voucherDate;
	private String jobCardVoucherNumber;
	private Date jobCardVoucherDate;
	private String customerId;
	CompanyMst companyMst;
	ServiceInDtl serviceInDtl;
	String status;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty fromBranchProprty;
	public JobCardInHdr() 
	{
		this.voucherNumberProperty = new SimpleStringProperty();
		this.fromBranchProprty = new SimpleStringProperty();
	}
	
	@JsonIgnore
	public StringProperty getvoucherNumberProperty()
	{
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(String voucherNumber)
	{
		this.voucherNumber = voucherNumber;
	}
	@JsonIgnore
	public StringProperty getfromBranchProprty()
	{
		fromBranchProprty.set(fromBranch);
		return fromBranchProprty;
	}
	public void setfromBranchProprty(String fromBranch)
	{
		this.fromBranch = fromBranch;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getJobCardVoucherNumber() {
		return jobCardVoucherNumber;
	}
	public void setJobCardVoucherNumber(String jobCardVoucherNumber) {
		this.jobCardVoucherNumber = jobCardVoucherNumber;
	}
	public Date getJobCardVoucherDate() {
		return jobCardVoucherDate;
	}
	public void setJobCardVoucherDate(Date jobCardVoucherDate) {
		this.jobCardVoucherDate = jobCardVoucherDate;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public ServiceInDtl getServiceInDtl() {
		return serviceInDtl;
	}
	public void setServiceInDtl(ServiceInDtl serviceInDtl) {
		this.serviceInDtl = serviceInDtl;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "JobCardInHdr [id=" + id + ", fromBranch=" + fromBranch + ", branchCode=" + branchCode
				+ ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", jobCardVoucherNumber="
				+ jobCardVoucherNumber + ", jobCardVoucherDate=" + jobCardVoucherDate + ", customerId=" + customerId
				+ ", companyMst=" + companyMst + ", serviceInDtl=" + serviceInDtl + ", status=" + status
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
}
