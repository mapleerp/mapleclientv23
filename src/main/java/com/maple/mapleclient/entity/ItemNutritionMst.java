package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class ItemNutritionMst implements Serializable {
	private static final long serialVersionUID = 1L;
	
	 private String id; 
	 String itemId;
	 String nutritionFact;
		String value;
		
		public ItemNutritionMst() {


			
			this.itemNameProperty = new SimpleStringProperty();
			this.nutritionFactProperty=new SimpleStringProperty();
			this.valueProperty=new SimpleStringProperty();
		}
		
		@JsonIgnore
		StringProperty itemNameProperty;
		@JsonIgnore 
		StringProperty nutritionFactProperty;
		@JsonIgnore
		StringProperty valueProperty;
		
		

		public String getItemId() {
			return itemId;
		}

		public void setItemId(String itemId) {
			this.itemId = itemId;
		}

		public StringProperty getItemNameProperty() {
			if(null!=itemId) {
				itemNameProperty.set(itemId);
				}else {
					itemNameProperty.set("");
				}
				return itemNameProperty;
		}

		public void setItemNameProperty(StringProperty itemNameProperty) {
			this.itemNameProperty = itemNameProperty;
		}

		public StringProperty getNutritionFactProperty() {
			
			nutritionFactProperty.set(nutritionFact);
			return nutritionFactProperty;
		}

		public void setNutritionFactProperty(StringProperty nutritionFactProperty) {
			this.nutritionFactProperty = nutritionFactProperty;
		}

		public StringProperty getValueProperty() {
			valueProperty.set(value);
			return valueProperty;
		}

		public void setValueProperty(StringProperty valueProperty) {
			this.valueProperty = valueProperty;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getNutritionFact() {
			return nutritionFact;
		}

		public void setNutritionFact(String nutritionFact) {
			this.nutritionFact = nutritionFact;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "ItemNutritions [id=" + id + ", itemId=" + itemId + ", nutritionFact=" + nutritionFact + ", value="
					+ value + ", itemNameProperty=" + itemNameProperty + ", nutritionFactProperty="
					+ nutritionFactProperty + ", valueProperty=" + valueProperty + "]";
		}
		
		
		
		
		
		
		

}
