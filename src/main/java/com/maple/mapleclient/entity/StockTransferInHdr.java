package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
 * This Entity is corrected on 5/6/2019
 */
public class StockTransferInHdr  implements Serializable{

	String id;
	String intentNumber;
	Date 	voucherDate;
	String voucherNumber;
	String voucherType;
	String branchCode;
	String deleted;
	String fromBranch;
	String inVoucherNumber;
	Date inVoucherDate;
	String statusAcceptStock;
	private String  processInstanceId;
	private String taskId;
	
	
	private StringProperty intentNumberProperty; 
	private StringProperty voucherNumberProperty; 
	private StringProperty branchProperty; 
	private StringProperty recIdProperty; 
	private StringProperty voucherDateProperty;
	Date ourVoucherDate;
	
	
	String intetNumber;
	Date intetDate;
	
	Date acceptedDate;
	Long acceptedUser;
	String corRelatedMessageId;
	 private   ObjectProperty<LocalDate>intentDates;
	
	public StockTransferInHdr()
	{
		this.intentNumberProperty=new SimpleStringProperty("00");
		this.voucherNumberProperty=new SimpleStringProperty("000");
		this.branchProperty=new SimpleStringProperty("000");
		this.recIdProperty=new SimpleStringProperty("000");
		this.voucherDateProperty = new SimpleStringProperty();
		
	}
	
	

	
	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public String getIntentNumber() {
		return intentNumber;
	}




	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}




	public Date getVoucherDate() {
		return voucherDate;
	}




	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}




	public String getVoucherNumber() {
		return voucherNumber;
	}




	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}




	public String getVoucherType() {
		return voucherType;
	}




	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}




	public String getBranchCode() {
		return branchCode;
	}




	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}




	public String getDeleted() {
		return deleted;
	}




	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}




	public String getFromBranch() {
		return fromBranch;
	}




	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}




	public String getInVoucherNumber() {
		return inVoucherNumber;
	}




	public void setInVoucherNumber(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}




	public Date getInVoucherDate() {
		return inVoucherDate;
	}




	public void setInVoucherDate(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}




	public String getStatusAcceptStock() {
		return statusAcceptStock;
	}




	public void setStatusAcceptStock(String statusAcceptStock) {
		this.statusAcceptStock = statusAcceptStock;
	}




	public Date getOurVoucherDate() {
		return ourVoucherDate;
	}
	public void setOurVoucherDate(Date ourVoucherDate) {
		this.ourVoucherDate = ourVoucherDate;
	}
	
	public String getIntetNumber() {
		return intetNumber;
	}
	public void setIntetNumber(String intetNumber) {
		this.intetNumber = intetNumber;
	}
	public Date getAcceptedDate() {
		return acceptedDate;
	}
	public void setAcceptedDate(Date acceptedDate) {
		this.acceptedDate = acceptedDate;
	}
	public Long getAcceptedUser() {
		return acceptedUser;
	}
	public void setAcceptedUser(Long acceptedUser) {
		this.acceptedUser = acceptedUser;
	}
	public String getCorRelatedMessageId() {
		return corRelatedMessageId;
	}
	public void setCorRelatedMessageId(String corRelatedMessageId) {
		this.corRelatedMessageId = corRelatedMessageId;
	}
	public Date getIntetDate() {
		return intetDate;
	}
	public void setIntetDate(Date intetDate) {
		this.intetDate = intetDate;
	}
	public StringProperty getIntentNumberProperty() 
	{
		intentNumberProperty.set(intetNumber);
		return intentNumberProperty;
	}
	public void setIntentNumberProperty(StringProperty intentNumberProperty) {
		this.intentNumberProperty = intentNumberProperty;
	}
	public StringProperty getInVoucherNumberProperty() 
	{
		voucherNumberProperty.set(inVoucherNumber);
		return voucherNumberProperty;
	}
	public void setInVoucherNumberProperty(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}
	
	
	public StringProperty getVoucherNumberProperty() 
	{
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public StringProperty getVoucherDateProperty() 
	{
		if(null != voucherDate)
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate));
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public StringProperty getRecIdProperty() 
	{
		recIdProperty.set(id);
		return recIdProperty;
	}
	public void setRecIdProperty(String id) {
		this.id = id;
	}
	public StringProperty getBranchProperty() 
	{
		branchProperty.set(fromBranch);
		return branchProperty;
	}
	public void setBranchProperty(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	


	public ObjectProperty<LocalDate> getIntentDates() {
		return intentDates;
	}


	public void setIntentDates(ObjectProperty<LocalDate> intentDates) {
		this.intentDates = intentDates;
	}




	public String getProcessInstanceId() {
		return processInstanceId;
	}




	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}




	public String getTaskId() {
		return taskId;
	}




	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}




	@Override
	public String toString() {
		return "StockTransferInHdr [id=" + id + ", intentNumber=" + intentNumber + ", voucherDate=" + voucherDate
				+ ", voucherNumber=" + voucherNumber + ", voucherType=" + voucherType + ", branchCode=" + branchCode
				+ ", deleted=" + deleted + ", fromBranch=" + fromBranch + ", inVoucherNumber=" + inVoucherNumber
				+ ", inVoucherDate=" + inVoucherDate + ", statusAcceptStock=" + statusAcceptStock
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", ourVoucherDate="
				+ ourVoucherDate + ", intetNumber=" + intetNumber + ", intetDate=" + intetDate + ", acceptedDate="
				+ acceptedDate + ", acceptedUser=" + acceptedUser + ", corRelatedMessageId=" + corRelatedMessageId
				+ "]";
	}
	

	
	
}
