package com.maple.mapleclient.entity;

import java.util.Date;



public class StockTransferInReturnHdr {
	
    String id;
	
	
	String intentNumber;
	Date 	voucherDate;
	String voucherNumber;
	String voucherType;
	String branchCode;
	String deleted;
	String fromBranch;
	String inVoucherNumber;
	Date inVoucherDate;
	String statusAcceptStock;
	StockTransferInHdr stockTransferInHdr;
	
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIntentNumber() {
		return intentNumber;
	}
	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getInVoucherNumber() {
		return inVoucherNumber;
	}
	public void setInVoucherNumber(String inVoucherNumber) {
		this.inVoucherNumber = inVoucherNumber;
	}
	public Date getInVoucherDate() {
		return inVoucherDate;
	}
	public void setInVoucherDate(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}
	public String getStatusAcceptStock() {
		return statusAcceptStock;
	}
	public void setStatusAcceptStock(String statusAcceptStock) {
		this.statusAcceptStock = statusAcceptStock;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	
	
	public StockTransferInHdr getStockTransferInHdr() {
		return stockTransferInHdr;
	}
	public void setStockTransferInHdr(StockTransferInHdr stockTransferInHdr) {
		this.stockTransferInHdr = stockTransferInHdr;
	}
	@Override
	public String toString() {
		return "StockTransferInReturnHdr [id=" + id + ", intentNumber=" + intentNumber + ", voucherDate=" + voucherDate
				+ ", voucherNumber=" + voucherNumber + ", voucherType=" + voucherType + ", branchCode=" + branchCode
				+ ", deleted=" + deleted + ", fromBranch=" + fromBranch + ", inVoucherNumber=" + inVoucherNumber
				+ ", inVoucherDate=" + inVoucherDate + ", statusAcceptStock=" + statusAcceptStock
				+ ", stockTransferInHdr=" + stockTransferInHdr + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	

}
