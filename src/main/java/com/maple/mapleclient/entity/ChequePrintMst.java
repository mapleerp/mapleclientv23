package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ChequePrintMst  {
	private String id;
	String chequeProperty;
	String chequeValue;
	String accountId;
	private String  processInstanceId;
	private String taskId;
	
	@JsonIgnore
	StringProperty chequePropertyProperty;
	
	@JsonIgnore
	StringProperty chequeValueProperty;
	
	@JsonIgnore
	String accountName;
	
	@JsonIgnore
	StringProperty accountNameProperty;
	
	
	public ChequePrintMst() {
		this.chequePropertyProperty = new SimpleStringProperty();
		this.chequeValueProperty = new SimpleStringProperty();
		this.accountNameProperty = new SimpleStringProperty();
	}

	public StringProperty getchequePropertyProperty() {
		chequePropertyProperty.set(chequeProperty);
		return chequePropertyProperty;
	}

	public void setchequePropertyProperty(String chequeProperty) {
		this.chequeProperty = chequeProperty;
	}
	public StringProperty getchequeValueProperty() {
		chequeValueProperty.set(chequeProperty);
		return chequeValueProperty;
	}

	public void setchequeValueProperty(String chequeProperty) {
		this.chequeProperty = chequeProperty;
	}
	
	public StringProperty getaccountNameProperty() {
		accountNameProperty.set(accountName);
		return accountNameProperty;
	}

	public void setaccountNameProperty(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getChequeProperty() {
		return chequeProperty;
	}
	public void setChequeProperty(String chequeProperty) {
		this.chequeProperty = chequeProperty;
	}
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getChequeValue() {
		return chequeValue;
	}
	public void setChequeValue(String chequeValue) {
		this.chequeValue = chequeValue;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ChequePrintMst [id=" + id + ", chequeProperty=" + chequeProperty + ", chequeValue=" + chequeValue
				+ ", accountId=" + accountId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", accountName=" + accountName + "]";
	}
	

}
