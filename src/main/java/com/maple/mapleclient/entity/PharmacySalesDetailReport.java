package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacySalesDetailReport{

	String voucherNumber;
	String invoiceDate;
	Double qty;
	Double amount;
	Double tax;
	Double invoiceAmount;
	String customerName;
	String itemName;
    String groupName;
    String batchCode;
    private String  processInstanceId;
	private String taskId;

	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
    private	DoubleProperty  amountProperty;
	
	@JsonIgnore
	 private DoubleProperty  taxProperty;
	
	@JsonIgnore
	 private StringProperty  voucherNumberProperty;
	

	@JsonIgnore
	 private StringProperty  invoiceDateProperty;
	
	@JsonIgnore
	 private DoubleProperty  InvoiceAmountProperty;
	
	@JsonIgnore
	private StringProperty groupNameProperty;
	@JsonIgnore
	 private StringProperty batchCodeProperty;
	
	@JsonIgnore
	 private StringProperty customerNameProperty;

	@JsonIgnore
	 private DoubleProperty qtyProperty;
    
    
    public PharmacySalesDetailReport() {
    	
    	this.invoiceDateProperty= new SimpleStringProperty("");
		this.customerNameProperty=new SimpleStringProperty("");
		this.voucherNumberProperty=new SimpleStringProperty("");
		this.customerNameProperty= new SimpleStringProperty("");
    	this.batchCodeProperty=new SimpleStringProperty("");
    this.groupNameProperty=new SimpleStringProperty("");
    this.qtyProperty=new SimpleDoubleProperty();
    this.InvoiceAmountProperty=new SimpleDoubleProperty();
    this.amountProperty=new SimpleDoubleProperty();
    this.itemNameProperty=new SimpleStringProperty("");
    this.taxProperty=new SimpleDoubleProperty();
    }
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	
	@JsonIgnore
	public StringProperty getItemNameProperty() {
		if(null!=itemName) {
		itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		if(null!=amount) {
		amountProperty.set(amount);
		}
		return amountProperty;
	}
	
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	@JsonIgnore
	public DoubleProperty getTaxProperty() {
		if(null!=tax) {
		taxProperty.set(tax);
		}
		return taxProperty;
		
	}

	public void setTaxProperty(DoubleProperty taxProperty) {
		this.taxProperty = taxProperty;
	}
	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		if(null!=voucherNumber) {
		voucherNumberProperty.set(voucherNumber);
		}
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}

	
	@JsonIgnore
	public StringProperty getInvoiceDateProperty() {
		if(null!=invoiceDate) {
		invoiceDateProperty.set(invoiceDate);
		}
		return invoiceDateProperty;
	}
	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}
	@JsonIgnore
	public DoubleProperty getInvoiceAmountProperty() {
		if(null!=invoiceAmount) {
		InvoiceAmountProperty.set(invoiceAmount);
		}
		return InvoiceAmountProperty;
	}
	public void setInvoiceAmountProperty(DoubleProperty invoiceAmountProperty) {
		InvoiceAmountProperty = invoiceAmountProperty;
	}
	@JsonIgnore
	public StringProperty getGroupNameProperty() {
		if(null!=groupName) {
		groupNameProperty.set(groupName);
		}
		return groupNameProperty;
	}
	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}
	@JsonIgnore
	public StringProperty getBatchCodeProperty() {
		if(null!=batchCode) {
		batchCodeProperty.set(batchCode);
		}
		return batchCodeProperty;
	}
	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		if(null!=qty) {
		qtyProperty.set(qty);
		}
		return qtyProperty;
	}
	
	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		if(null!=customerName) {
		customerNameProperty.set(customerName);
		}
		return customerNameProperty;
	}
	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	@Override
	public String toString() {
		return "PharmacySalesDetailReport [voucherNumber=" + voucherNumber + ", invoiceDate=" + invoiceDate + ", qty="
				+ qty + ", amount=" + amount + ", tax=" + tax + ", invoiceAmount=" + invoiceAmount + ", customerName="
				+ customerName + ", itemName=" + itemName + ", groupName=" + groupName + ", batchCode=" + batchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", itemNameProperty="
				+ itemNameProperty + ", amountProperty=" + amountProperty + ", taxProperty=" + taxProperty
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", invoiceDateProperty=" + invoiceDateProperty
				+ ", InvoiceAmountProperty=" + InvoiceAmountProperty + ", groupNameProperty=" + groupNameProperty
				+ ", batchCodeProperty=" + batchCodeProperty + ", customerNameProperty=" + customerNameProperty
				+ ", qtyProperty=" + qtyProperty + "]";
	}
	

}
