package com.maple.mapleclient.entity;

import java.sql.Date;
import java.security.PublicKey;
import java.time.LocalDate;
import java.time.ZoneId;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.WritableDoubleValue;

public class PurchaseDtl {

	String unitId;
	private String processInstanceId;
	private String taskId;

	public PurchaseDtl() {
		this.itemName = new SimpleStringProperty("");
		this.purchseRate = new SimpleDoubleProperty();
		this.amount = new SimpleDoubleProperty(0);
		this.batch = new SimpleStringProperty("");
		this.itemSerialNo = new SimpleStringProperty("");
		this.barcode = new SimpleStringProperty("");
		this.taxAmt = new SimpleDoubleProperty();
		this.cessAmt = new SimpleDoubleProperty();
		this.previousMRP = new SimpleDoubleProperty();
		this.expiryDate = new SimpleObjectProperty();
		String s = "2080-12-01";
		this.expiryDate.set(Date.valueOf(s).toLocalDate());
		this.freeQty = new SimpleIntegerProperty();
		this.qty = new SimpleDoubleProperty();
		this.taxRate = new SimpleDoubleProperty();
		this.discount = new SimpleDoubleProperty();
		this.changePriceStatus = new SimpleStringProperty("");
		this.unit = new SimpleStringProperty();
		this.mrp = new SimpleDoubleProperty();
		this.cessRate = new SimpleDoubleProperty();
		this.manufactureDate = new SimpleObjectProperty();
		this.manufactureDate.set(LocalDate.now());
		this.BinNo = new SimpleStringProperty("");
		this.PurchaseDate = new SimpleObjectProperty();
		this.PurchaseDate.set(LocalDate.now());
		this.CessRate2 = new SimpleDoubleProperty();
		this.NetCostProperty = new SimpleDoubleProperty();
		this.idproperty = new SimpleStringProperty();
		this.fcAmountProperty = new SimpleDoubleProperty(0.0);
		this.fcCessAmtProperty = new SimpleDoubleProperty(0.0);
		this.fcCessRate2Property = new SimpleDoubleProperty(0.0);
		this.fcMrpProperty = new SimpleDoubleProperty(0.0);
		this.fcPurchaseRateProperty = new SimpleDoubleProperty(0.0);
		this.fcTaxAmtProperty = new SimpleDoubleProperty(0.0);
		
		
		this.returnQtyProperty = new SimpleDoubleProperty(0.0);
	
		
	

	}

	/*
	 * public PurchaseDtl(IntegerProperty id, StringProperty itemName,
	 * DoubleProperty purchseRate, DoubleProperty amount, StringProperty batch,
	 * StringProperty itemSerial, StringProperty barcode, DoubleProperty taxAmt,
	 * DoubleProperty cessAmt, DoubleProperty previousMRP, Object expiryDate,
	 * IntegerProperty freeQty, IntegerProperty qty, DoubleProperty taxRate,
	 * DoubleProperty discount, StringProperty changePriceStatus, IntegerProperty
	 * unit, DoubleProperty mrp, DoubleProperty cessRate, DoubleProperty
	 * manufactureDate, IntegerProperty ItemId, StringProperty binNo, Object
	 * purchaseDate, DoubleProperty cessRate2, StringProperty netCost) { super(); Id
	 * = id; this.itemName = itemName; this.purchseRate = purchseRate; this.amount =
	 * amount; this.batch = batch; this.itemSerial = itemSerial; this.barcode =
	 * barcode; this.taxAmt = taxAmt; this.cessAmt = cessAmt; this.previousMRP =
	 * previousMRP; this.expiryDate = expiryDate; this.freeQty = freeQty; this.qty =
	 * qty; this.taxRate = taxRate; this.discount = discount; this.changePriceStatus
	 * = changePriceStatus; this.unit = unit; this.mrp = mrp; this.cessRate =
	 * cessRate; this.manufactureDate = manufactureDate; this.ItemId = ItemId;
	 * this.BinNo = binNo; this.PurchaseDate = purchaseDate; this.CessRate2 =
	 * cessRate2; this.NetCost = netCost; }
	 */

	private Double fcPurchaseRate;
	private Double fcAmount;
	private Double fcTaxAmt;
	private Double fcCessAmt;
	private Double fcTaxRate;
	private Double fcDiscount;
	private Double fcCessRate;
	private Double fcCessRate2;
	private Double fcMrp;
	
	

	private DoubleProperty fcPurchaseRateProperty;
	private DoubleProperty fcAmountProperty;
	private DoubleProperty fcTaxAmtProperty;
	private DoubleProperty fcCessAmtProperty;
	private DoubleProperty fcTaxRateProperty;
	private DoubleProperty fcDiscountProperty;
	private DoubleProperty fcCessRateProperty;
	private DoubleProperty fcCessRate2Property;
	private DoubleProperty fcMrpProperty;
	
	
	private String id;

	@JsonIgnore
	private StringProperty idproperty;

	@JsonIgnore
	private StringProperty itemName;

	@JsonIgnore
	String unitName;
	@JsonIgnore
	private DoubleProperty purchseRate;

	@JsonIgnore
	private DoubleProperty amount;

	@JsonIgnore
	private StringProperty batch;

	@JsonIgnore
	private StringProperty itemSerialNo;

	@JsonIgnore
	private StringProperty barcode;

	@JsonIgnore
	private DoubleProperty taxAmt;

	@JsonIgnore
	private DoubleProperty cessAmt;

	@JsonIgnore
	private DoubleProperty previousMRP;

	@JsonIgnore
	private ObjectProperty<LocalDate> expiryDate;

	@JsonIgnore
	private IntegerProperty freeQty;

	@JsonIgnore
	private DoubleProperty qty;

	@JsonIgnore
	private DoubleProperty taxRate;

	@JsonIgnore
	private DoubleProperty discount;

	@JsonIgnore
	private StringProperty changePriceStatus;

	@JsonIgnore
	private StringProperty unit;

	@JsonIgnore
	private DoubleProperty mrp;

	@JsonIgnore
	private DoubleProperty cessRate;

	@JsonIgnore
	private ObjectProperty<LocalDate> manufactureDate;

	// version1.7

	String purchaseOrderDtl;
	// version1.7ends

	private String itemId;

	@JsonIgnore
	private StringProperty BinNo;

	@JsonIgnore
	private ObjectProperty<LocalDate> PurchaseDate;

	@JsonIgnore
	private DoubleProperty CessRate2;
	@JsonIgnore
	private DoubleProperty NetCostProperty;

	private Double NetCost;
	private Double fcNetCost;
	
	private Integer displaySerial;


	PurchaseHdr purchaseHdr;
	
	private Integer itemSerial;
	
	
	@JsonIgnore
	private Double returnQty;
	@JsonIgnore
	private DoubleProperty returnQtyProperty;
	

	

	

	private String store;
	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}

	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}

	public Double getFcMrp() {
		return fcMrp;
	}

	public void setFcMrp(Double fcMrp) {
		this.fcMrp = fcMrp;
	}

	public Double getFcPurchaseRate() {
		return fcPurchaseRate;
	}

	public void setFcPurchaseRate(Double fcPurchaseRate) {
		this.fcPurchaseRate = fcPurchaseRate;
	}

	@JsonIgnore
	public StringProperty getBinNoProperty() {
		return BinNo;
	}

	public void setBinNoProperty(StringProperty binNo) {
		BinNo = binNo;
	}

	@JsonProperty("BinNo")
	public String getBinNo() {
		return BinNo.get();
	}

	@JsonProperty("BinNo")
	public void setBinNo(String binNo) {
		this.BinNo.set(binNo);
	}

	@JsonIgnore

	public ObjectProperty<LocalDate> getexpiryDateProperty() {
		return expiryDate;
	}

	public void setexpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {

		this.expiryDate = expiryDate;
	}

	public Double getFcAmount() {
		return fcAmount;
	}

	public void setFcAmount(Double fcAmount) {
		this.fcAmount = fcAmount;
	}

	public Double getFcTaxAmt() {
		return fcTaxAmt;
	}

	public void setFcTaxAmt(Double fcTaxAmt) {
		this.fcTaxAmt = fcTaxAmt;
	}

	public Double getFcCessAmt() {
		return fcCessAmt;
	}

	public void setFcCessAmt(Double fcCessAmt) {
		this.fcCessAmt = fcCessAmt;
	}

	public Double getFcTaxRate() {
		return fcTaxRate;
	}

	public void setFcTaxRate(Double fcTaxRate) {
		this.fcTaxRate = fcTaxRate;
	}

	public Double getFcDiscount() {
		return fcDiscount;
	}

	public void setFcDiscount(Double fcDiscount) {
		this.fcDiscount = fcDiscount;
	}

	public Double getFcCessRate() {
		return fcCessRate;
	}

	public void setFcCessRate(Double fcCessRate) {
		this.fcCessRate = fcCessRate;
	}

	public Double getFcCessRate2() {
		return fcCessRate2;
	}

	public void setFcCessRate2(Double fcCessRate2) {
		this.fcCessRate2 = fcCessRate2;
	}

	@JsonProperty("expiryDate")

	public java.sql.Date getexpiryDate() {

		return Date.valueOf(this.expiryDate.get());
	}

	public void setexpiryDate(java.sql.Date expiryDate) {
		if (null != expiryDate)
			this.expiryDate.set(expiryDate.toLocalDate());

	}

	// version1.7

	public String getPurchaseOrderDtl() {
		return purchaseOrderDtl;
	}

	public void setPurchaseOrderDtl(String purchaseOrderDtl) {
		this.purchaseOrderDtl = purchaseOrderDtl;
	}

	// version1.7 end

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	@JsonIgnore
	public ObjectProperty<LocalDate> getPurchaseDateProperty() {
		return PurchaseDate;
	}

	public void setPurchaseDateProperty(ObjectProperty<LocalDate> PurchaseDate) {
		this.PurchaseDate = PurchaseDate;
	}

	@JsonProperty("PurchaseDate")
	public java.sql.Date getPurchaseDate() {
		return Date.valueOf(this.PurchaseDate.get());
	}

	public void setPurchaseDate(java.sql.Date PurchaseDate) {
		this.PurchaseDate.set(PurchaseDate.toLocalDate());
	}

	@JsonIgnore

	public ObjectProperty<LocalDate> getmanufactureDateProperty() {
		return manufactureDate;
	}

	public void setmanufactureDateProperty(ObjectProperty<LocalDate> manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	@JsonProperty("manufactureDate")
	public java.sql.Date getmanufactureDate() {
		return Date.valueOf(this.manufactureDate.get());
	}

	public void setmanufactureDate(java.sql.Date manufactureDate) {
		if (null != manufactureDate)
			this.manufactureDate.set(manufactureDate.toLocalDate());
	}

	@JsonIgnore
	public DoubleProperty getCessRate2Property() {
		return CessRate2;
	}

	public void setCessRate2Property(DoubleProperty cessRate2) {
		CessRate2 = cessRate2;
	}

	@JsonProperty("CessRate2")
	public Double getCessRate2() {
		return CessRate2.get();

	}

	@JsonProperty("CessRate2")
	public void setCessRate2(double CessRate2) {
		this.CessRate2.set(CessRate2);
	}

	@JsonIgnore
	public DoubleProperty getNetCostProperty() {
		if(null != NetCost) {
		NetCostProperty.set(NetCost);
		}
		return NetCostProperty;
	}

	public void setNetCostProperty(Double netCost) {
		this.NetCost = netCost;
	}

	@JsonIgnore
	public DoubleProperty getfcAmountProperty() {
		if (null != fcAmount) {
			fcAmountProperty.set(fcAmount);
		}
		return fcAmountProperty;
	}

	public void setfcAmountProperty(Double fcAmount) {
		this.fcAmount = fcAmount;
	}

	@JsonIgnore
	public DoubleProperty getfcpurchaseRateProperty() {
		if (null != fcPurchaseRate) {
			fcPurchaseRateProperty.set(fcPurchaseRate);
		}
		return fcPurchaseRateProperty;
	}

	public void setfcPurchaseRateProperty(Double fcPurchaseRate) {
		this.fcPurchaseRate = fcPurchaseRate;
	}

	@JsonIgnore
	public DoubleProperty getfcMrpProperty() {
		if (null != fcMrp) {
			fcMrpProperty.set(fcMrp);
		}
		return fcMrpProperty;
	}

	public void setfcMrpProperty(Double fcMrp) {
		this.fcMrp = fcMrp;
	}

	@JsonIgnore
	public DoubleProperty getfcTaxAmountProperty() {
		if (null != fcTaxAmt) {
			fcTaxAmtProperty.set(fcTaxAmt);
		}
		return fcTaxAmtProperty;
	}

	public void setfctaxAmountProperty(Double fcTaxAmt) {
		this.fcTaxAmt = fcTaxAmt;
	}

	public Double getNetCost() {
		return NetCost;
	}

	public void setNetCost(Double netCost) {
		NetCost = netCost;
	}

	public StringProperty getIdProperty() {
		idproperty.set(id);
		return idproperty;
	}

	public void setItdProperty(String id) {
		this.id = id;
	}

	public Double getFcNetCost() {
		return fcNetCost;
	}

	public void setFcNetCost(Double fcNetCost) {
		this.fcNetCost = fcNetCost;
	}

	public StringProperty getItemNameProperty() {
		return itemName;
	}

	public void setItemNameProperty(StringProperty itemName) {
		this.itemName = itemName;
	}

	@JsonProperty("itemName")
	public String getItemName() {
		return itemName.get();
	}

	@JsonProperty("itemName")
	public void setItemName(String itemName) {
		this.itemName.set(itemName);
	}

	@JsonIgnore
	public DoubleProperty getPurchseRateProperty() {
		return purchseRate;
	}

	public void setPurchseRateProperty(DoubleProperty purchseRate) {
		this.purchseRate = purchseRate;
	}

	@JsonProperty("purchseRate")
	public Double getPurchseRate() {
		return purchseRate.get();
	}

	@JsonProperty("purchseRate")
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate.set(purchseRate);
	}

	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		return amount;
	}

	public void setAmountProperty(DoubleProperty amount) {
		this.amount = amount;
	}

	@JsonProperty("amount")
	public Double getAmount() {
		return amount.get();
	}

	public void setAmount(Double amount) {
		this.amount.set(amount);
	}

	@JsonIgnore

	public StringProperty getBatchProperty() {
		return batch;
	}

	public void setBatchProperty(StringProperty batch) {
		this.batch = batch;
	}

	@JsonProperty("batch")
	public String getBatch() {
		return batch.get();
	}

	@JsonProperty("batch")
	public void setBatch(String batch) {
		this.batch.set(batch);
	}

	@JsonIgnore
	public StringProperty getItemSerialProperty() {
		if(null != displaySerial) {
			itemSerialNo.set(displaySerial+"");
		}
		return itemSerialNo;
	}

	public void setItemSerialProperty(StringProperty itemSerial) {
		this.itemSerialNo = itemSerial;
	}

	

	@JsonIgnore
	public StringProperty getBarcodepProperty() {
		return barcode;
	}

	public void setBarcodeProperty(StringProperty barcode) {
		this.barcode = barcode;
	}

	@JsonProperty("barcode")
	public String getBarcode() {
		return barcode.get();
	}

	@JsonProperty("barcode")
	public void setBarcode(String barcode) {
		this.barcode.set(barcode);
	}

	@JsonIgnore
	public DoubleProperty getTaxAmtProperty() {
		return taxAmt;
	}

	public void setTaxAmtProperty(DoubleProperty taxAmt) {
		this.taxAmt = taxAmt;
	}

	@JsonProperty("taxAmt")
	public Double getTaxAmt() {
		return taxAmt.get();
	}

	@JsonProperty("taxAmt")
	public void setTaxAmt(Double taxAmt) {
		this.taxAmt.set(taxAmt);
	}

	@JsonIgnore
	public DoubleProperty getCessAmtProperty() {
		return cessAmt;
	}

	public void setCessAmtProperty(DoubleProperty cessAmt) {
		this.cessAmt = cessAmt;
	}

	@JsonProperty("cessAmt")
	public Double getCessAmt() {
		return cessAmt.get();
	}

	@JsonProperty("cessAmt")
	public void setCessAmt(Double cessAmt) {
		this.cessAmt.set(cessAmt);
	}

	@JsonIgnore
	public DoubleProperty getPreviousMRProperty() {
		return previousMRP;
	}

	public void setPreviousMRPProperty(DoubleProperty previousMRP) {
		this.previousMRP = previousMRP;
	}

	@JsonProperty("previousMRP")
	public Double getPreviousMRP() {
		return previousMRP.get();
	}

	@JsonProperty("previousMRP")
	public void setPreviousMRP(Double previousMRP) {
		if (null != previousMRP) {
			this.previousMRP.set(previousMRP);
		}
	}

	@JsonIgnore
	public IntegerProperty getFreeQtyProperty() {
		return freeQty;
	}

	public void setFreeQty(IntegerProperty freeQty) {
		this.freeQty = freeQty;
	}

	@JsonProperty("freeQty")
	public Integer getFreeQty() {
		return freeQty.get();
	}

	@JsonProperty("freeQty")
	public void setFreeQty(Integer freeQty) {
		if (null != freeQty) {
			this.freeQty.set(freeQty);
		}
	}

	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		return qty;
	}

	public void setQtyProperty(DoubleProperty qty) {
		this.qty = qty;
	}

	@JsonProperty("qty")
	public Double getQty() {
		return qty.get();
	}

	@JsonProperty("qty")
	public void setQty(Double qty) {
		this.qty.set(qty);
	}

	@JsonIgnore
	public DoubleProperty getTaxRateProperty() {
		return taxRate;
	}

	public void setTaxRate(DoubleProperty taxRate) {
		this.taxRate = taxRate;
	}

	@JsonProperty("taxRate")
	public Double getTaxRate() {
		return taxRate.get();
	}

	@JsonProperty("taxRate")
	public void setTaxRate(Double taxRate) {
		this.taxRate.set(taxRate);
	}

	@JsonIgnore
	public DoubleProperty getDiscountProperty() {
		return discount;
	}

	public void setDiscountProperty(DoubleProperty discount) {
		this.discount = discount;
	}

	@JsonProperty("discount")
	public Double getDiscount() {
		return discount.get();
	}

	@JsonProperty("discount")
	public void setDiscount(Double discount) {
		if (null != discount) {
			this.discount.set(discount);
		}
	}

	@JsonIgnore
	public StringProperty getChangePriceStatusProperty() {
		return changePriceStatus;
	}

	public void setChangePriceStatusProperty(StringProperty changePriceStatus) {
		this.changePriceStatus = changePriceStatus;
	}

	@JsonProperty("changePriceStatus")
	public String getChangePriceStatus() {
		return changePriceStatus.get();
	}

	@JsonProperty("changePriceStatus")
	public void setChangePriceStatus(String changePriceStatus) {
		this.changePriceStatus.set(changePriceStatus);
	}

	@JsonIgnore
	public StringProperty getUnitProperty() {
		unit.set(unitName);
		return unit;
	}

	public void setUnitProperty(String unitName) {
		this.unitName = unitName;
	}
//	@JsonProperty("unit")
//	public Integer getUnit() {
//		return unit.get();
//	}
//	@JsonProperty("unit")
//	public void setUnit(Integer unit) {
//		this.unit.set(unit);
//	}

	@JsonIgnore
	public DoubleProperty getMrpProperty() {
		return mrp;
	}

	public void setMrpProperty(DoubleProperty mrp) {
		this.mrp = mrp;
	}

	@JsonProperty("mrp")
	public Double getMrp() {
		return mrp.get();
	}

	@JsonProperty("mrp")
	public void setMrp(Double mrp) {
		this.mrp.set(mrp);
	}

	@JsonIgnore
	public DoubleProperty getCessRateProperty() {
		return cessRate;
	}

	public void setCessRateProperty(DoubleProperty cessRate) {
		this.cessRate = cessRate;
	}

	@JsonProperty("cessRate")
	public Double getCessRate() {
		return cessRate.get();
	}

	@JsonProperty("cessRate")
	public void setCessRate(Double cessRate) {
		this.cessRate.set(cessRate);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Integer getDisplaySerial() {
		return displaySerial;
	}

	public void setDisplaySerial(Integer displaySerial) {
		this.displaySerial = displaySerial;
	}

	public Integer getItemSerial() {
		return itemSerial;
	}

	public void setItemSerial(Integer itemSerial) {
		this.itemSerial = itemSerial;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}


	public Double getReturnQty() {
		return returnQty;
	}

	public void setReturnQty(Double returnQty) {
		this.returnQty = returnQty;
	}

	@JsonIgnore
	public DoubleProperty getReturnQtyProperty( ) {
		returnQtyProperty.set(returnQty);
		return returnQtyProperty;
	}

	public void setReturnQtyProperty(DoubleProperty returnQtyProperty) {
		this.returnQtyProperty = returnQtyProperty;
	}

	@Override
	public String toString() {
		return "PurchaseDtl [unitId=" + unitId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", fcPurchaseRate=" + fcPurchaseRate + ", fcAmount=" + fcAmount + ", fcTaxAmt=" + fcTaxAmt
				+ ", fcCessAmt=" + fcCessAmt + ", fcTaxRate=" + fcTaxRate + ", fcDiscount=" + fcDiscount
				+ ", fcCessRate=" + fcCessRate + ", fcCessRate2=" + fcCessRate2 + ", fcMrp=" + fcMrp
				+ ", fcPurchaseRateProperty=" + fcPurchaseRateProperty + ", fcAmountProperty=" + fcAmountProperty
				+ ", fcTaxAmtProperty=" + fcTaxAmtProperty + ", fcCessAmtProperty=" + fcCessAmtProperty
				+ ", fcTaxRateProperty=" + fcTaxRateProperty + ", fcDiscountProperty=" + fcDiscountProperty
				+ ", fcCessRateProperty=" + fcCessRateProperty + ", fcCessRate2Property=" + fcCessRate2Property
				+ ", fcMrpProperty=" + fcMrpProperty + ", id=" + id + ", idproperty=" + idproperty + ", itemName="
				+ itemName + ", unitName=" + unitName + ", purchseRate=" + purchseRate + ", amount=" + amount
				+ ", batch=" + batch + ", itemSerialNo=" + itemSerialNo + ", barcode=" + barcode + ", taxAmt=" + taxAmt
				+ ", cessAmt=" + cessAmt + ", previousMRP=" + previousMRP + ", expiryDate=" + expiryDate + ", freeQty="
				+ freeQty + ", qty=" + qty + ", taxRate=" + taxRate + ", discount=" + discount + ", changePriceStatus="
				+ changePriceStatus + ", unit=" + unit + ", mrp=" + mrp + ", cessRate=" + cessRate
				+ ", manufactureDate=" + manufactureDate + ", purchaseOrderDtl=" + purchaseOrderDtl + ", itemId="
				+ itemId + ", BinNo=" + BinNo + ", PurchaseDate=" + PurchaseDate + ", CessRate2=" + CessRate2
				+ ", NetCostProperty=" + NetCostProperty + ", NetCost=" + NetCost + ", fcNetCost=" + fcNetCost
				+ ", displaySerial=" + displaySerial + ", purchaseHdr=" + purchaseHdr + ", itemSerial=" + itemSerial
				+ ", returnQty=" + returnQty + ", returnQtyProperty=" + returnQtyProperty + ", store=" + store + "]";
	}

	
//	@Override
//	public String toString() {
//		return "PurchaseDtl [unitId=" + unitId + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
//				+ ", fcPurchaseRate=" + fcPurchaseRate + ", fcAmount=" + fcAmount + ", fcTaxAmt=" + fcTaxAmt
//				+ ", fcCessAmt=" + fcCessAmt + ", fcTaxRate=" + fcTaxRate + ", fcDiscount=" + fcDiscount
//				+ ", fcCessRate=" + fcCessRate + ", fcCessRate2=" + fcCessRate2 + ", fcMrp=" + fcMrp
//				+ ", fcPurchaseRateProperty=" + fcPurchaseRateProperty + ", fcAmountProperty=" + fcAmountProperty
//				+ ", fcTaxAmtProperty=" + fcTaxAmtProperty + ", fcCessAmtProperty=" + fcCessAmtProperty
//				+ ", fcTaxRateProperty=" + fcTaxRateProperty + ", fcDiscountProperty=" + fcDiscountProperty
//				+ ", fcCessRateProperty=" + fcCessRateProperty + ", fcCessRate2Property=" + fcCessRate2Property
//				+ ", fcMrpProperty=" + fcMrpProperty + ", id=" + id + ", idproperty=" + idproperty + ", itemName="
//				+ itemName + ", unitName=" + unitName + ", purchseRate=" + purchseRate + ", amount=" + amount
//				+ ", batch=" + batch + ", itemSerialNo=" + itemSerialNo + ", barcode=" + barcode + ", taxAmt=" + taxAmt
//				+ ", cessAmt=" + cessAmt + ", previousMRP=" + previousMRP + ", expiryDate=" + expiryDate + ", freeQty="
//				+ freeQty + ", qty=" + qty + ", taxRate=" + taxRate + ", discount=" + discount + ", changePriceStatus="
//				+ changePriceStatus + ", unit=" + unit + ", mrp=" + mrp + ", cessRate=" + cessRate
//				+ ", manufactureDate=" + manufactureDate + ", purchaseOrderDtl=" + purchaseOrderDtl + ", itemId="
//				+ itemId + ", BinNo=" + BinNo + ", PurchaseDate=" + PurchaseDate + ", CessRate2=" + CessRate2
//				+ ", NetCostProperty=" + NetCostProperty + ", NetCost=" + NetCost + ", fcNetCost=" + fcNetCost
//				+ ", displaySerial=" + displaySerial + ", purchaseHdr=" + purchaseHdr + ", itemSerial=" + itemSerial
//				+ ", store=" + store + "]";
//	}

	

	


	

}
