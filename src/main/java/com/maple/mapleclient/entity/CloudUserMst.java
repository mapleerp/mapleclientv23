package com.maple.mapleclient.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

public class CloudUserMst implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;

	private String userName;

	private String password;

	private CompanyMst companyMst;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	@Override
	public String toString() {
		return "CloudUserMst [id=" + id + ", userName=" + userName + ", password=" + password + ", companyMst="
				+ companyMst + "]";
	}

}
