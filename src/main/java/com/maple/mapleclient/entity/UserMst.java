package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class UserMst {

	private String id;
	private String userName;
	private String branchCode;
	private String fullName;
	private String branchName;
	private String password;
	private String status;
	private String password1;
	private String  processInstanceId;
	private String taskId;

private CompanyMst companyMst;


@JsonIgnore
private StringProperty userNameProperty;
@JsonIgnore
private StringProperty branchCodeProperty;
@JsonIgnore
private StringProperty userFullNameProperty;
@JsonIgnore
private StringProperty branchNameProperty;



public UserMst()
{
	this.userNameProperty= new SimpleStringProperty("");
	this.branchCodeProperty =new SimpleStringProperty("");
	this.userFullNameProperty=new SimpleStringProperty("");
	this.branchNameProperty=new SimpleStringProperty("");
}


public StringProperty getBranchNameProperty() {
	branchNameProperty.set(branchName);
	return branchNameProperty;
}


public void setBranchNameProperty(StringProperty branchNameProperty) {
	this.branchNameProperty = branchNameProperty;
}


public String getId() {
	return id;
}


public void setId(String id) {
	this.id = id;
}


public String getUserName() {
	return userName;
}


public void setUserName(String userName) {
	this.userName = userName;
}


public String getBranchCode() {
	return branchCode;
}


public void setBranchCode(String branchCode) {
	this.branchCode = branchCode;
}


public String getFullName() {
	return fullName;
}


public void setFullName(String fullName) {
	this.fullName = fullName;
}


public String getBranchName() {
	return branchName;
}


public void setBranchName(String branchName) {
	this.branchName = branchName;
}


public String getPassword() {
	return password;
}


public void setPassword(String password) {
	this.password = password;
}


public String getStatus() {
	return status;
}


public void setStatus(String status) {
	this.status = status;
}


public String getPassword1() {
	return password1;
}


public void setPassword1(String password1) {
	this.password1 = password1;
}


public StringProperty getUserNameProperty() {
	userNameProperty.set(userName);
	return userNameProperty;
}


public void setUserNameProperty(StringProperty userNameProperty) {
	this.userNameProperty = userNameProperty;
}


public StringProperty getBranchCodeProperty() {
	branchCodeProperty.set(branchCode);
	return branchCodeProperty;
}


public void setBranchCodeProperty(StringProperty branchCodeProperty) {
	this.branchCodeProperty = branchCodeProperty;
}


public StringProperty getUserFullNameProperty() {
	userFullNameProperty.set(fullName);
	return userFullNameProperty;
}


public void setUserFullNameProperty(StringProperty userFullNameProperty) {
	this.userFullNameProperty = userFullNameProperty;
}





public CompanyMst getCompanyMst() {
	return companyMst;
}


public void setCompanyMst(CompanyMst companyMst) {
	this.companyMst = companyMst;
}


public String getProcessInstanceId() {
	return processInstanceId;
}


public void setProcessInstanceId(String processInstanceId) {
	this.processInstanceId = processInstanceId;
}


public String getTaskId() {
	return taskId;
}


public void setTaskId(String taskId) {
	this.taskId = taskId;
}


@Override
public String toString() {
	return "UserMst [id=" + id + ", userName=" + userName + ", branchCode=" + branchCode + ", fullName=" + fullName
			+ ", branchName=" + branchName + ", password=" + password + ", status=" + status + ", password1="
			+ password1 + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst="
			+ companyMst + "]";
}


}
