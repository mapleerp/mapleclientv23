package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KitchenCategoryDtl {

	String id;
	String categoryId;
	String branchCode;
	String categoryName;
	@JsonIgnore
	StringProperty categoryNameProperty;
	
	private String  processInstanceId;
	private String taskId;
	
	
	public KitchenCategoryDtl() {
		
		this.categoryNameProperty =new SimpleStringProperty();
	}
	
	
	
	@JsonIgnore
	public StringProperty getcategoryNameProperty() {
		categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}

	public void setcategoryNameProperty(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getTaskId() {
		return taskId;
	}



	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}



	@Override
	public String toString() {
		return "KitchenCategoryDtl [id=" + id + ", categoryId=" + categoryId + ", branchCode=" + branchCode
				+ ", categoryName=" + categoryName + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ "]";
	}
	
	
}
