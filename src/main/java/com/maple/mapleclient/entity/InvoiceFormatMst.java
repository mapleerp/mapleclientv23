package com.maple.mapleclient.entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InvoiceFormatMst {

	String id;
	String priceTypeId;
	String jasperName;
	String gst;
	String discount;
	String batch;
	String reportName;

	String status;

	CompanyMst companyMst;
	private String  processInstanceId;
	private String taskId;

	public InvoiceFormatMst() {
		this.priceTypeProperty = new SimpleStringProperty("");
		this.jasperNameProperty = new SimpleStringProperty("");

		this.reportNameProperty = new SimpleStringProperty("");
		this.gstProperty = new SimpleStringProperty("");
		this.mrpProperty = new SimpleStringProperty("");
		this.discountProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.reportTypeProperty = new SimpleStringProperty("");

	}

	@JsonIgnore
	String priceName;

	@JsonIgnore
	private StringProperty priceTypeProperty;

	@JsonIgnore
	private StringProperty jasperNameProperty;

	@JsonIgnore
	private StringProperty reportNameProperty;

	@JsonIgnore
	private StringProperty gstProperty;

	@JsonIgnore
	private StringProperty mrpProperty;

	@JsonIgnore
	private StringProperty discountProperty;

	@JsonIgnore
	private StringProperty batchProperty;

	@JsonIgnore
	private StringProperty reportTypeProperty;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPriceTypeId() {
		return priceTypeId;
	}

	public void setPriceTypeId(String priceTypeId) {
		this.priceTypeId = priceTypeId;
	}

	public String getJasperName() {
		return jasperName;
	}

	public void setJasperName(String jasperName) {
		this.jasperName = jasperName;
	}

	public StringProperty getPriceTypeProperty() {
		priceTypeProperty.set(priceName);
		return priceTypeProperty;
	}

	public void setPriceTypeProperty(StringProperty priceTypeProperty) {
		this.priceTypeProperty = priceTypeProperty;
	}

	public StringProperty getJasperNameProperty() {
		jasperNameProperty.set(jasperName);
		return jasperNameProperty;
	}

	public void setJasperNameProperty(StringProperty jasperNameProperty) {
		this.jasperNameProperty = jasperNameProperty;
	}

	public String getPriceName() {
		return priceName;
	}

	public void setPriceName(String priceName) {
		this.priceName = priceName;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public StringProperty getReportNameProperty() {
		reportNameProperty.set(reportName);
		return reportNameProperty;
	}

	public void setReportNameProperty(StringProperty reportNameProperty) {
		this.reportNameProperty = reportNameProperty;
	}

	public StringProperty getGstProperty() {
		gstProperty.set(gst);
		return gstProperty;
	}

	public void setGstProperty(StringProperty gstProperty) {
		this.gstProperty = gstProperty;
	}

	public StringProperty getDiscountProperty() {
		discountProperty.set(discount);
		return discountProperty;
	}

	public void setDiscountProperty(StringProperty discountProperty) {
		this.discountProperty = discountProperty;
	}

	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "InvoiceFormatMst [id=" + id + ", priceTypeId=" + priceTypeId + ", jasperName=" + jasperName + ", gst="
				+ gst + ", discount=" + discount + ", batch=" + batch + ", reportName=" + reportName + ", status="
				+ status + ", companyMst=" + companyMst + ", processInstanceId=" + processInstanceId + ", taskId="
				+ taskId + ", priceName=" + priceName + "]";
	}

}
