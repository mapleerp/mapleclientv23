package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductionPreplanningDtlReport {

	    private String id;
		private  String itemId;
		private String batch;
		private Double qty;
		private String status;
		private String itemName;
		private String  processInstanceId;
		private String taskId;
		ProductionPreplanningMst productionPreplanningMst;
		
		
		
		
		  public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		@JsonIgnore
		  private StringProperty kitnameProperty;
		  
		  @JsonIgnore
		  private StringProperty batchProperty;
		  
		  @JsonIgnore
		  private DoubleProperty qtyProperty;
		  
		  ProductionPreplanningDtlReport(){
			  
			  
				this.kitnameProperty = new SimpleStringProperty("");
				this.batchProperty = new SimpleStringProperty("");
				this.qtyProperty = new SimpleDoubleProperty();
		  }
		  
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getItemId() {
			return itemId;
		}
		public void setItemId(String itemId) {
			this.itemId = itemId;
		}
		public String getBatch() {
			return batch;
		}
		public void setBatch(String batch) {
			this.batch = batch;
		}
		public Double getQty() {
			return qty;
		}
		public void setQty(Double qty) {
			this.qty = qty;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public ProductionPreplanningMst getProductionPreplanningMst() {
			return productionPreplanningMst;
		}
		public void setProductionPreplanningMst(ProductionPreplanningMst productionPreplanningMst) {
			this.productionPreplanningMst = productionPreplanningMst;
		}
		@JsonProperty
		public StringProperty getKitnameProperty() {
			
			kitnameProperty.set(itemName);
			return kitnameProperty;
		}

		public void setKitnameProperty(StringProperty kitnameProperty) {
			this.kitnameProperty = kitnameProperty;
		}
		@JsonProperty
		public StringProperty getBatchProperty() {
			batchProperty.set(batch);
			return batchProperty;
		}

		public void setBatchProperty(StringProperty batchProperty) {
			this.batchProperty = batchProperty;
		}
		@JsonProperty
		public DoubleProperty getQtyProperty() {
			qtyProperty.set(qty);
			return qtyProperty;
		}

		public void setQtyProperty(DoubleProperty qtyProperty) {
			this.qtyProperty = qtyProperty;
		}

		public String getProcessInstanceId() {
			return processInstanceId;
		}

		public void setProcessInstanceId(String processInstanceId) {
			this.processInstanceId = processInstanceId;
		}

		public String getTaskId() {
			return taskId;
		}

		public void setTaskId(String taskId) {
			this.taskId = taskId;
		}

		@Override
		public String toString() {
			return "ProductionPreplanningDtlReport [id=" + id + ", itemId=" + itemId + ", batch=" + batch + ", qty="
					+ qty + ", status=" + status + ", itemName=" + itemName + ", processInstanceId=" + processInstanceId
					+ ", taskId=" + taskId + ", productionPreplanningMst=" + productionPreplanningMst + "]";
		}

		
		
	
}
