package com.maple.mapleclient.entity;

import java.io.Serializable;
import java.util.Date;

public class JobCardOutHdr implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String id;
	String fromBranch;
	String toBranch;
	String voucherNumber;
	Date voucherDate;
	private String jobCardVoucherNumber;
	private Date jobCardVoucherDate;
	private String customerId;
	CompanyMst companyMst;
	ServiceInDtl serviceInDtl;
	private String  processInstanceId;
	private String taskId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getToBranch() {
		return toBranch;
	}
	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public ServiceInDtl getServiceInDtl() {
		return serviceInDtl;
	}
	public void setServiceInDtl(ServiceInDtl serviceInDtl) {
		this.serviceInDtl = serviceInDtl;
	}
	public String getJobCardVoucherNumber() {
		return jobCardVoucherNumber;
	}
	public void setJobCardVoucherNumber(String jobCardVoucherNumber) {
		this.jobCardVoucherNumber = jobCardVoucherNumber;
	}
	public Date getJobCardVoucherDate() {
		return jobCardVoucherDate;
	}
	public void setJobCardVoucherDate(Date jobCardVoucherDate) {
		this.jobCardVoucherDate = jobCardVoucherDate;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "JobCardOutHdr [id=" + id + ", fromBranch=" + fromBranch + ", toBranch=" + toBranch + ", voucherNumber="
				+ voucherNumber + ", voucherDate=" + voucherDate + ", jobCardVoucherNumber=" + jobCardVoucherNumber
				+ ", jobCardVoucherDate=" + jobCardVoucherDate + ", customerId=" + customerId + ", companyMst="
				+ companyMst + ", serviceInDtl=" + serviceInDtl + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}
	

}
