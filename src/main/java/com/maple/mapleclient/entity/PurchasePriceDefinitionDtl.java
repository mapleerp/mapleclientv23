package com.maple.mapleclient.entity;

import java.util.Date;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PurchasePriceDefinitionDtl {

	private String id;

	String purchaseVoucherNumber;

	String itemId;

	String itemName;

	String batch;


	String unitName;

	String priceStatus;

	String branchCode;

	Date purchaseVoucherDate;
	
	CompanyMst companyMst;
	
	String purchaseHdrId;
	
	private String priceId;
	
	private Double amount;
	
	private String priceType;
	
	private String unitId;
	
	private String  processInstanceId;
	
	private String taskId;
	
	private Date startDate;

	
	@JsonProperty
	StringProperty itemNameProperty;
	
	@JsonProperty
	StringProperty amountProperty;
	
	@JsonProperty
	StringProperty priceTypeProperty;
	
	@JsonProperty
	StringProperty expiryProperty;
	
	
	@JsonProperty
	StringProperty batchProperty;
	
	

	public PurchasePriceDefinitionDtl() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleStringProperty("");
		this.priceTypeProperty = new SimpleStringProperty("");
		this.expiryProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");


	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPurchaseVoucherNumber() {
		return purchaseVoucherNumber;
	}

	public void setPurchaseVoucherNumber(String purchaseVoucherNumber) {
		this.purchaseVoucherNumber = purchaseVoucherNumber;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getPriceStatus() {
		return priceStatus;
	}

	public void setPriceStatus(String priceStatus) {
		this.priceStatus = priceStatus;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Date getPurchaseVoucherDate() {
		return purchaseVoucherDate;
	}

	public void setPurchaseVoucherDate(Date purchaseVoucherDate) {
		this.purchaseVoucherDate = purchaseVoucherDate;
	}



	public String getPurchaseHdrId() {
		return purchaseHdrId;
	}

	public void setPurchaseHdrId(String purchaseHdrId) {
		this.purchaseHdrId = purchaseHdrId;
	}

	public String getPriceId() {
		return priceId;
	}

	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public String getUnitId() {
		return unitId;
	}

	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}


	public StringProperty getItemNameProperty() {
		
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getAmountProperty() {
		amountProperty.set(amount.toString());
		return amountProperty;
	}

	public void setAmountProperty(StringProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	public StringProperty getPriceTypeProperty() {
		priceTypeProperty.set(priceType);
		return priceTypeProperty;
	}

	public void setPriceTypeProperty(StringProperty priceTypeProperty) {
		this.priceTypeProperty = priceTypeProperty;
	}

	public StringProperty getExpiryProperty() {
		
		if(null != startDate)
		{
			expiryProperty.set(SystemSetting.UtilDateToString(startDate, "yyyy-MM-dd"));
		}
		return expiryProperty;
	}

	public void setExpiryProperty(StringProperty expiryProperty) {
		this.expiryProperty = expiryProperty;
	}

	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	@Override
	public String toString() {
		return "PurchasePriceDefinitionDtl [id=" + id + ", purchaseVoucherNumber=" + purchaseVoucherNumber + ", itemId="
				+ itemId + ", itemName=" + itemName + ", batch=" + batch + ", unitName=" + unitName + ", priceStatus="
				+ priceStatus + ", branchCode=" + branchCode + ", purchaseVoucherDate=" + purchaseVoucherDate
				+ ", companyMst=" + companyMst + ", purchaseHdrId=" + purchaseHdrId + ", priceId=" + priceId
				+ ", amount=" + amount + ", priceType=" + priceType + ", unitId=" + unitId + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", startDate=" + startDate + ", itemNameProperty="
				+ itemNameProperty + ", amountProperty=" + amountProperty + ", priceTypeProperty=" + priceTypeProperty
				+ ", expiryProperty=" + expiryProperty + ", batchProperty=" + batchProperty + "]";
	}
	
	
	
}
