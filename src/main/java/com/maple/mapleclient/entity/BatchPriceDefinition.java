package com.maple.mapleclient.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BatchPriceDefinition {
	
	
	
	private String id;
	private String itemId;
	private String priceId;
	private double amount;
	private String itemName;
	private String priceType;
	private String unitId;
	private String branchCode;
	private String batch;
	private String  processInstanceId;
	private String taskId;
	private Double qty;
	
	
	@JsonIgnore
	private ObjectProperty<LocalDate> startDate;
	
	String unitName;
	@JsonIgnore
	private ObjectProperty<LocalDate> endDate;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty priceTypeProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	public BatchPriceDefinition() {

		this.itemNameProperty = new SimpleStringProperty("");
		this.priceTypeProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.startDate = new SimpleObjectProperty<LocalDate>(null);
		this.endDate = new SimpleObjectProperty<LocalDate>(null);
		this.unitNameProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty();
	}
	@JsonProperty("startDate")
	public java.sql.Date getStartDate( ) {
		if(null==this.startDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.startDate.get() );
		}
	 
		
	}
	
   public void setStartDate (java.sql.Date startDateProperty) {
						if(null!=startDateProperty)
		this.startDate.set(startDateProperty.toLocalDate());
	}/////
   @JsonProperty("endDate")
	public java.sql.Date getEndDate ( ) {
		if(null==this.endDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.endDate.get() );
		}
	}
	
  public void setEndDate (java.sql.Date endDateProperty) {
						if(null!=endDateProperty)
		this.endDate.set(endDateProperty.toLocalDate());
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getPriceId() {
		return priceId;
	}
	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getPriceType() {
		return priceType;
	}
	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}

	public ObjectProperty<LocalDate> getStartDateProperty( ) {
		return startDate;
	}
 
	public void setStartDateProperty(ObjectProperty<LocalDate> startDateProperty) {
		this.startDate = startDate;
	}
	
	public ObjectProperty<LocalDate> getEndDateProperty( ) {
		return endDate;
	}
 
	public void setEndDateProperty(ObjectProperty<LocalDate> endDate) {
		this.endDate = endDate;
	}
	
	public StringProperty getPriceTypeProperty() {

		priceTypeProperty.set(priceType);
		return priceTypeProperty;
	}

	public void setPriceTypeProperty(StringProperty priceTypeProperty) {
		priceTypeProperty = priceTypeProperty;
	}
	
	public StringProperty getItemNameProperty() {

		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		itemNameProperty = itemNameProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	public StringProperty getUnitNameProperty() {

		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setUnitNameProperty(String unitName) {
		this.unitName = unitName;
	}
	
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	@JsonIgnore
	public StringProperty getbatchProperty() {
		if(null != batch) {
		batchProperty.set(batch);
		}
		return batchProperty;
	}
	public void setbatchProperty(String batch) {
		this.batch = batch;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	
	
	
	
	
	
	
	public Double getQty() {
		return qty;
	}
	
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		if(null != qty) {
			qtyProperty.set(qty);
			}
		return qtyProperty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	@Override
	public String toString() {
		return "BatchPriceDefinition [id=" + id + ", itemId=" + itemId + ", priceId=" + priceId + ", amount=" + amount
				+ ", itemName=" + itemName + ", priceType=" + priceType + ", unitId=" + unitId + ", branchCode="
				+ branchCode + ", batch=" + batch + ", processInstanceId=" + processInstanceId + ", taskId=" + taskId
				+ ", qty=" + qty + ", startDate=" + startDate + ", unitName=" + unitName + ", endDate=" + endDate
				+ ", itemNameProperty=" + itemNameProperty + ", priceTypeProperty=" + priceTypeProperty
				+ ", amountProperty=" + amountProperty + ", batchProperty=" + batchProperty + ", unitNameProperty="
				+ unitNameProperty + ", qtyProperty=" + qtyProperty + "]";
	}
	
	
}
