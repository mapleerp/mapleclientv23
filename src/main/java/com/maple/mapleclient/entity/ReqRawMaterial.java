package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ReqRawMaterial {

	
	String itemName;
	Double reqQty;
	String unit;
	private String  processInstanceId;
	private String taskId;
	StringProperty itemNameProperty;
	
	StringProperty unitProperty;
	DoubleProperty reqQtyProperty;
	ReqRawMaterial(){
		
		this.itemNameProperty = new SimpleStringProperty("");
		this.unitProperty = new SimpleStringProperty("");
		this.reqQtyProperty=new SimpleDoubleProperty();
		
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getReqQty() {
		return reqQty;
	}
	public void setReqQty(Double reqQty) {
		this.reqQty = reqQty;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	@JsonIgnore
	public StringProperty getItemNameProperty() {
		itemNameProperty.setValue(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	@JsonIgnore
	public StringProperty getUnitProperty() {
		
		unitProperty.setValue(unit);
		return unitProperty;
	}
	@JsonIgnore
	public void setUnitProperty(StringProperty unitProperty) {
		this.unitProperty = unitProperty;
	}
	@JsonIgnore
	public DoubleProperty getReqQtyProperty() {
		reqQtyProperty.setValue(reqQty);
		
		return reqQtyProperty;
	}
	public void setReqQtyProperty(DoubleProperty reqQtyProperty) {
		this.reqQtyProperty = reqQtyProperty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "ReqRawMaterial [itemName=" + itemName + ", reqQty=" + reqQty + ", unit=" + unit + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}
	
	
	
}
