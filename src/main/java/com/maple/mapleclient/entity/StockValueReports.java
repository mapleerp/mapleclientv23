package com.maple.mapleclient.entity;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockValueReports {

	String itemName;
	Double closingQty;
	Double closingValue;
	Double dicount;
	Double mrp;
	Double costprice;
	private String  processInstanceId;
	private String taskId;
	
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	

	@JsonIgnore
	private DoubleProperty closingQtyProperty;

	@JsonIgnore
	private DoubleProperty closingValueProperty;
	
	@JsonIgnore
	private DoubleProperty mrpProperty;
	
	@JsonIgnore
	private DoubleProperty discountProperty;
	
	@JsonIgnore
	private DoubleProperty costPriceProperty;

	public StockValueReports() {
		
		this.itemNameProperty = new SimpleStringProperty("");
		this.closingQtyProperty =  new SimpleDoubleProperty(0.0);
		this.closingValueProperty = new SimpleDoubleProperty(0.0);
		this.mrpProperty = new SimpleDoubleProperty(0.0);
		this.discountProperty = new SimpleDoubleProperty(0.0);

		this.costPriceProperty = new SimpleDoubleProperty(0.0);

	
	}
	
	
	public Double getDicount() {
		return dicount;
	}


	public void setDicount(Double dicount) {
		this.dicount = dicount;
	}


	public Double getMrp() {
		return mrp;
	}


	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}


	public Double getCostprice() {
		return costprice;
	}


	public void setCostprice(Double costprice) {
		this.costprice = costprice;
	}


	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getClosingQty() {
		return closingQty;
	}
	public void setClosingQty(Double closingQty) {
		this.closingQty = closingQty;
	}
	public Double getClosingValue() {
		return closingValue;
	}
	public void setClosingValue(Double closingValue) {
		this.closingValue = closingValue;
	}
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	public DoubleProperty getClosingQtyProperty() {
		closingQtyProperty.set(closingQty);
		return closingQtyProperty;
	}
	public void setClosingQtyProperty(DoubleProperty closingQtyProperty) {
		this.closingQtyProperty = closingQtyProperty;
	}
	public DoubleProperty getClosingValueProperty() {
		if(null == closingValue)
		{
			closingValue = 0.0;
		}
		 closingValueProperty.set(closingValue);
		 return closingValueProperty;
	}
	public void setClosingValueProperty(DoubleProperty closingValueProperty) {
		this.closingValueProperty = closingValueProperty;
	}


	public DoubleProperty getMrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}


	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}


	public DoubleProperty getDiscountProperty() {
		discountProperty.set(dicount);
		return discountProperty;
	}


	public void setDiscountProperty(DoubleProperty discountProperty) {
		this.discountProperty = discountProperty;
	}


	public DoubleProperty getCostPriceProperty() {
		if(null == costprice)
		{
			costprice = 0.0;
		}
		costPriceProperty.set(costprice);
		return costPriceProperty;
	}


	public void setCostPriceProperty(DoubleProperty costPriceProperty) {
		this.costPriceProperty = costPriceProperty;
	}


	public String getProcessInstanceId() {
		return processInstanceId;
	}


	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	@Override
	public String toString() {
		return "StockValueReports [itemName=" + itemName + ", closingQty=" + closingQty + ", closingValue="
				+ closingValue + ", dicount=" + dicount + ", mrp=" + mrp + ", costprice=" + costprice
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}

	
	
	

}
