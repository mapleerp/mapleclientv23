package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class SessionEndDtl {
	private String id;
	private Double denomination;
	private Double count;
	private String  processInstanceId;
	private String taskId;
	
	SessionEndClosureMst sessionEndClosure;
	
	
	public SessionEndClosureMst getSessionEndClosure() {
		return sessionEndClosure;
	}
	public void setSessionEndClosure(SessionEndClosureMst sessionEndClosure) {
		this.sessionEndClosure = sessionEndClosure;
	}
	public SessionEndDtl() {
		this.denominationProperty = new SimpleDoubleProperty();
		this.countProperty = new SimpleDoubleProperty();
	}
	@JsonIgnore
	private DoubleProperty denominationProperty;
	
	@JsonIgnore
	private DoubleProperty countProperty;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public Double getDenomination() {
		return denomination;
	}
	public void setDenomination(Double denomination) {
		this.denomination = denomination;
	}
	public Double getCount() {
		return count;
	}
	public void setCount(Double count) {
		this.count = count;
	}
	
	@JsonIgnore
	public DoubleProperty getDenominationProperty() {
		denominationProperty.set(denomination);
		return denominationProperty;
	}
	public void setDenominationProperty(DoubleProperty denominationProperty) {
		this.denominationProperty = denominationProperty;
	}

	
	@JsonIgnore
	public DoubleProperty getCountProperty() {
		countProperty.set(count);
		return countProperty;
	}
	public void setCountProperty(DoubleProperty countProperty) {
		this.countProperty = countProperty;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "SessionEndDtl [id=" + id + ", denomination=" + denomination + ", count=" + count
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", sessionEndClosure="
				+ sessionEndClosure + "]";
	}


	
}
