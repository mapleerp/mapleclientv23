package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProfitAndLossExpense {
	String id;
	String accountId;
	String serialNo;
	private String processInstanceId;
	private String taskId;
	CompanyMst companyMst;
	public ProfitAndLossExpense() {
		this.serialNoProperty = new SimpleStringProperty();
		this.accountNameProperty = new SimpleStringProperty();
	}
	
	@JsonIgnore
	String accountName;

	@JsonIgnore
	StringProperty serialNoProperty;

	@JsonIgnore
	StringProperty accountNameProperty;

	public String getId() {
		return id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@JsonIgnore
	public StringProperty getSerialNoProperty() {
		
		if(null!=serialNo) {
			serialNoProperty.set(serialNo);
		}
		
		return serialNoProperty;
	}

	public void setSerialNoProperty(StringProperty serialNoProperty) {
		this.serialNoProperty = serialNoProperty;
	}
	@JsonIgnore
	public StringProperty getAccountNameProperty() {
		
		if(null != accountName) {
		accountNameProperty.set(accountName);
		}
		return accountNameProperty;
	}

	public void setAccountNameProperty(StringProperty accountNameProperty) {
		this.accountNameProperty = accountNameProperty;
	}
	
	

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Override
	public String toString() {
		return "ProfitAndLossExpense [id=" + id + ", accountId=" + accountId + ", serialNo=" + serialNo
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + ", companyMst=" + companyMst
				+ ", serialNoProperty=" + serialNoProperty + ", accountNameProperty=" + accountNameProperty + "]";
	}

	

	

}
