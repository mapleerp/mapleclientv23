package com.maple.mapleclient.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemLocationMst {
	
	String id;
	String itemId;
	String floor;
	String shelf;
	String rack;
	String bin;
	
	String itemName;
	String branchCode;
	private String  processInstanceId;
	private String taskId;
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty floorProperty;
	
	@JsonIgnore
	private StringProperty shelfProperty;
	
	@JsonIgnore
	private StringProperty rackProperty;
	
	
	
	public ItemLocationMst() {
		
		this.itemNameProperty =new SimpleStringProperty();
		this.floorProperty = new SimpleStringProperty();
		this.shelfProperty = new SimpleStringProperty();
		this.rackProperty =new SimpleStringProperty();
	}
	
	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setaccountHeadProperty(String itemName) {
		this.itemName = itemName;
	}
	
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	@JsonIgnore
	public StringProperty getfloorProperty() {
		floorProperty.set(floor);
		return floorProperty;
	}

	public void setfloorProperty(String floor) {
		this.floor = floor;
	}
	
	@JsonIgnore
	public StringProperty getshelfProperty() {
		shelfProperty.set(shelf);
		return shelfProperty;
	}

	public void setshelfProperty(String shelf) {
		this.shelf = shelf;
	}
	
	
	@JsonIgnore
	public StringProperty getrackProperty() {
		rackProperty.set(rack);
		return rackProperty;
	}

	public void setrackProperty(String rack) {
		this.rack = rack;
	}
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getShelf() {
		return shelf;
	}
	public void setShelf(String shelf) {
		this.shelf = shelf;
	}
	public String getRack() {
		return rack;
	}
	public void setRack(String rack) {
		this.rack = rack;
	}
	public String getBin() {
		return bin;
	}
	public void setBin(String bin) {
		this.bin = bin;
	}
	

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "ItemLocationMst [id=" + id + ", itemId=" + itemId + ", floor=" + floor + ", shelf=" + shelf + ", rack="
				+ rack + ", bin=" + bin + ", itemName=" + itemName + ", branchCode=" + branchCode
				+ ", processInstanceId=" + processInstanceId + ", taskId=" + taskId + "]";
	}
	
}
