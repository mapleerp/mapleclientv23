package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PharmacyNewCustomerWiseSaleReport;
import com.maple.report.entity.PoliceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PoliceReportCtl {
	
	
//  (0_0)       SharonN  

	private ObservableList<PoliceReport> policeReportList = FXCollections.observableArrayList();
	 EventBus eventBus = EventBusFactory.getEventBus();

	@FXML
	private DatePicker dptoDate;

	@FXML
	private Button btnPrintReport;

	@FXML
	private Button btnGenerate;

	@FXML
	private DatePicker dpfromDate;

	@FXML
	private TableView<PoliceReport> tblpolicereport;

	@FXML
	private TableColumn<PoliceReport, String> clinvoiceDate;

	@FXML
	private TableColumn<PoliceReport, String> clVoucherNumber;

	@FXML
	private TableColumn<PoliceReport, String> clCustomerName;

	@FXML
	private TableColumn<PoliceReport, String> clMPSType;

	@FXML
	private TableColumn<PoliceReport, String> clPatientID;

	@FXML
	private TableColumn<PoliceReport, Number> clCreditAmount;

	@FXML
	private TableColumn<PoliceReport, Number> clcashPaid;

	@FXML
	private TableColumn<PoliceReport, Number> clCardPaid;

	@FXML
	private TableColumn<PoliceReport, Number> clInsuranceAmount;
	


	@FXML
	void GenerateReport(ActionEvent event) {
		if (null == dpfromDate.getValue()) {

			notifyMessage(3, "please select From date", false);
			dpfromDate.requestFocus();
			return ;
		}
	}
		

	// EventBus eventBus = EventBusFactory.getEventBus();
//	   @FXML
//	    private DatePicker dptoDate;
//
//	    @FXML
//	    private DatePicker dpfromDate;

	    @FXML
	    private Button btnAdd;

	    @FXML
	    private Button btnShowReport;

	    @FXML
	    private Button btPrintReport;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private TextField txtCustomer;

//	    @FXML
//	    private TableView<PoliceReport> tblpolicereport;
//
//	    @FXML
//	    private TableColumn<PoliceReport, String> clinvoiceDate;
//
//	    @FXML
//	    private TableColumn<PoliceReport, String> clVoucherNumber;
//
//	    @FXML
//	    private TableColumn<PoliceReport, String> clCustomerName;
//
//	    @FXML
//	    private TableColumn<PoliceReport, String> clMPSType;
//
//	    @FXML
//	    private TableColumn<PoliceReport, String> clPatientID;
//
//	    @FXML
//	    private TableColumn<PoliceReport, Number> clCreditAmount;
//
//	    @FXML
//	    private TableColumn<PoliceReport, Number> clcashPaid;
//
//	    @FXML
//	    private TableColumn<PoliceReport, Number> clCardPaid;
//
//	    @FXML
//	    private TableColumn<PoliceReport, Number> clInsuranceAmount;
//
//	    @FXML
	    private ListView<String> lstCustomerList;
	    @FXML
	    void initialize() {
	    	dptoDate = SystemSetting.datePickerFormat(dptoDate, "dd/MMM/yyyy");
	    	dpfromDate = SystemSetting.datePickerFormat(dpfromDate, "dd/MMM/yyyy");
	    	eventBus.register(this);
	    	
	    }
	    @FXML
	    void AddItem(ActionEvent event) {
	    	lstCustomerList.getItems().add(txtCustomer.getText());
	    	lstCustomerList.getSelectionModel().select(txtCustomer.getText());
	    	txtCustomer.clear();
	    }

	    @FXML
	    void clear(ActionEvent event) {
	    	txtCustomer.clear();
	    	dpfromDate.setValue(null);
	    	dptoDate.setValue(null);
	    	lstCustomerList.getItems().clear();
	    }

	    @FXML
	    void onEnterCustomer(ActionEvent event) {
	    	loadCustomerPopup();
	    }

	    @FXML
	    void printReport(ActionEvent event) {
	    	java.util.Date fdate = SystemSetting.localToUtilDate(dpfromDate.getValue());
			String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
			
			java.util.Date tdate = SystemSetting.localToUtilDate(dptoDate.getValue());
			String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
			
			
			List<String> selectedCustomer = lstCustomerList.getSelectionModel().getSelectedItems();
	        
	    	String cust="";
	    	for(String s:selectedCustomer)
	    	{
	    		s = s.concat(";");
	    		cust= cust.concat(s);
	    	} 

			  try { JasperPdfReportService.PoliceReport(sfdate,
					  stdate,cust); } catch (JRException e) { // TODO Auto-gesnerated catch block
			  e.printStackTrace(); }
	    	
	    		
	    }

	    @FXML
	    void showReport(ActionEvent event) {
	    	if(null == dpfromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
				return;
	    	}
	    	if(null == dptoDate.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	}
	    	
	    	
	    	if(null==lstCustomerList.getSelectionModel().getSelectedItem()) {
	    		notifyMessage(5, "plz add Customer",false);
	    		return;
	    	}
	    	
	        
	    	
	  List<String> selectedCustomer = lstCustomerList.getSelectionModel().getSelectedItems();
	        
	    	String cust="";
	    	for(String s:selectedCustomer)
	    	{
	    		s = s.concat(";");
	    		cust= cust.concat(s);
	    	} 
	    	
	    	
	    	java.util.Date fdate = SystemSetting.localToUtilDate(dpfromDate.getValue());
			String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
			
			java.util.Date tdate = SystemSetting.localToUtilDate(dptoDate.getValue());
			String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
			
			ResponseEntity<List<PoliceReport>> policeReport=RestCaller.getPoliceReport(sfdate,stdate,cust);
			policeReportList = FXCollections.observableArrayList(policeReport.getBody());
			fillTable();
			
	    }
	    private void loadCustomerPopup() {
	    	try {

//			txtLocalCustomer.clear();
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();


		} catch (IOException e) {
			//
			e.printStackTrace();

		}
	    
	    }
	    
	    
	    @Subscribe
		public void popupCustomerlistner(CustomerEvent customerEvent) {

			ResponseEntity<AccountHeads> getCustomer = RestCaller.getAccountHeadsById(customerEvent.getCustId());
			AccountHeads custmrMst = new AccountHeads();
			custmrMst = getCustomer.getBody();

			Stage stage = (Stage) btnAdd.getScene().getWindow();
			if (stage.isShowing()) {
				txtCustomer.setText(customerEvent.getCustomerName());
			}
		}
	    
	    
	    private void fillTable() { 
			  tblpolicereport.setItems(policeReportList);
		  
		  
		  clinvoiceDate.setCellValueFactory(cellData ->
		  cellData.getValue().getInvoiceDateProperty());
		  clVoucherNumber.setCellValueFactory(cellData ->
		  cellData.getValue().getVoucherNumberProperty());
		  clCustomerName.setCellValueFactory(cellData ->
		  cellData.getValue().getCustomerNameProperty());
		  clMPSType.setCellValueFactory(cellData ->
		  cellData.getValue().getMpsTypeProperty());
		  clPatientID.setCellValueFactory(cellData -> cellData.getValue().getPatientIDProperty());
		  clCreditAmount.setCellValueFactory(cellData ->
		  cellData.getValue().getCreditAmountProperty());
		  clcashPaid.setCellValueFactory(cellData ->
		  cellData.getValue().getCashPaidProperty());
		  clCardPaid.setCellValueFactory(cellData ->
		  cellData.getValue().getCardPaidProperty());
		  clInsuranceAmount.setCellValueFactory(cellData ->
		  cellData.getValue().getInsuranceAmountProperty()); 
		  }
	    private void notifyMessage(int i, String string, boolean b) {
			Image img;
			if (b) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

			
		}
	
	
	  
	  
	 
	
	
	
	
	 
	
}
