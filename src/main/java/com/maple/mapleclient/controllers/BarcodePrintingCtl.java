package com.maple.mapleclient.controllers;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportBarCode;
import com.maple.maple.util.ExportBarcodeFromBarcodePrinting;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.TSCBarcode;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BarcodeBatchMst;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.IngredientsMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class BarcodePrintingCtl {
	String taskid;
	String processInstanceId;

	ExportBarCode exportBarCodeToExcel = new ExportBarCode();

	ExportBarcodeFromBarcodePrinting exportBarcodeFromBarcodePrinting = new ExportBarcodeFromBarcodePrinting();
	EventBus eventBus = EventBusFactory.getEventBus();
	IngredientsMst ingredientMst = null;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtPrinterName;
	@FXML
	private TextField txtNetWt;
	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtMrp;

	@FXML
	private TextField txtDuration;

	@FXML
	private TextField txtbestBefore;

	@FXML
	private TextField txtIngredient1;

	@FXML
	private TextField txtIngredient2;

	@FXML
	private TextField txtQty;

	@FXML
	private DatePicker dpProduction;

	@FXML
	private Button btnPrint;

	@FXML
	private Button btnCalibrate;

	@FXML
	private TextField txtbatch;

	@FXML
	private ComboBox<String> cmbBarcodeFormat;

	@FXML
	private TextField txtNutrition1;

	@FXML
	private TextField txtNutrition2;

	@FXML
	private TextField txtNutrition3;

	@FXML
	private TextField txtNutrition4;

	@FXML
	private TextField txtNutrition5;

	@FXML
	private TextField txtNutrition6;

	@FXML
	private TextField txtNutrition7;

	@FXML
	private TextField txtNutrition8;

	@FXML
	private TextField txtNutrition9;

	@FXML
	private TextField txtNutrition10;

	@FXML
	private TextField txtNutrition11;

	@FXML
	private TextField txtNutrition12;

	@FXML
	private TextField txtNutrition13;

	@FXML
	private TextField txtNutrition14;

	@FXML
	private Label lblNutrition1;

	@FXML
	private Label lblNutrition2;

	@FXML
	private Label lblNutrition3;

	@FXML
	private Label lblNutrition4;

	@FXML
	private Label lblNutrition5;

	@FXML
	private Label lblNutrition6;

	@FXML
	private Label lblNutrition7;

	@FXML
	private Label lblNutrition8;

	@FXML
	private Label lblNutrition9;

	@FXML
	private Label lblNutrition10;

	@FXML
	private Label lblNutrition11;

	@FXML
	private Label lblNutrition12;

	@FXML
	private Label lblNutrition13;

	@FXML
	private Label lblNutrition14;

	@FXML
	private void initialize() {
		dpProduction = SystemSetting.datePickerFormat(dpProduction, "dd/MMM/yyyy");
		txtNutrition1.setVisible(false);
		txtNutrition2.setVisible(false);
		txtNutrition3.setVisible(false);
		txtNutrition4.setVisible(false);
		txtNutrition5.setVisible(false);
		txtNutrition6.setVisible(false);
		txtNutrition7.setVisible(false);
		txtNutrition8.setVisible(false);
		txtNutrition9.setVisible(false);
		txtNutrition10.setVisible(false);
		txtNutrition11.setVisible(false);
		txtNutrition12.setVisible(false);
		txtNutrition13.setVisible(false);
		txtNutrition14.setVisible(false);

		lblNutrition1.setVisible(false);
		lblNutrition2.setVisible(false);
		lblNutrition3.setVisible(false);
		lblNutrition4.setVisible(false);
		lblNutrition5.setVisible(false);
		lblNutrition6.setVisible(false);
		lblNutrition7.setVisible(false);
		lblNutrition8.setVisible(false);
		lblNutrition9.setVisible(false);
		lblNutrition10.setVisible(false);
		lblNutrition11.setVisible(false);
		lblNutrition12.setVisible(false);
		lblNutrition13.setVisible(false);
		lblNutrition14.setVisible(false);

		eventBus.register(this);
		dpProduction.setValue(LocalDate.now());

		String PrinterNAme = SystemSetting.printer_name;
		txtPrinterName.setText(PrinterNAme);
		txtDuration.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtDuration.setText(oldValue);
				}
			}
		});

		cmbBarcodeFormat.getItems().add("BISCOTTI");
		cmbBarcodeFormat.getItems().add("CHOCOLATE");
		cmbBarcodeFormat.getItems().add("CHICKEN");
		cmbBarcodeFormat.getItems().add("JAWAHAR FOODS");

		cmbBarcodeFormat.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				txtIngredient2.setVisible(true);

				txtNutrition1.setVisible(false);
				txtNutrition2.setVisible(false);
				txtNutrition3.setVisible(false);
				txtNutrition4.setVisible(false);
				txtNutrition5.setVisible(false);
				txtNutrition6.setVisible(false);
				txtNutrition7.setVisible(false);
				txtNutrition8.setVisible(false);
				txtNutrition9.setVisible(false);
				txtNutrition10.setVisible(false);
				txtNutrition11.setVisible(false);
				txtNutrition12.setVisible(false);
				txtNutrition13.setVisible(false);
				txtNutrition14.setVisible(false);

				lblNutrition1.setVisible(false);
				lblNutrition2.setVisible(false);
				lblNutrition3.setVisible(false);
				lblNutrition4.setVisible(false);
				lblNutrition5.setVisible(false);
				lblNutrition6.setVisible(false);
				lblNutrition7.setVisible(false);
				lblNutrition8.setVisible(false);
				lblNutrition9.setVisible(false);
				lblNutrition10.setVisible(false);
				lblNutrition11.setVisible(false);
				lblNutrition12.setVisible(false);
				lblNutrition13.setVisible(false);
				lblNutrition14.setVisible(false);

				if (newValue.equalsIgnoreCase("FORMAT8") || newValue.equalsIgnoreCase("CHOCOLATE")) {

					txtNutrition1.setVisible(true);
					txtNutrition2.setVisible(true);
					txtNutrition3.setVisible(true);
					txtNutrition4.setVisible(true);
					txtNutrition5.setVisible(true);
					txtNutrition6.setVisible(true);
					txtNutrition7.setVisible(true);
					txtNutrition8.setVisible(true);
					txtNutrition9.setVisible(true);
					txtNutrition10.setVisible(true);
					txtNutrition11.setVisible(true);
					txtNutrition12.setVisible(true);
					txtNutrition13.setVisible(true);
					txtNutrition14.setVisible(true);

					lblNutrition1.setVisible(true);
					lblNutrition2.setVisible(true);
					lblNutrition3.setVisible(true);
					lblNutrition4.setVisible(true);
					lblNutrition5.setVisible(true);
					lblNutrition6.setVisible(true);
					lblNutrition7.setVisible(true);
					lblNutrition8.setVisible(true);
					lblNutrition9.setVisible(true);
					lblNutrition10.setVisible(true);
					lblNutrition11.setVisible(true);
					lblNutrition12.setVisible(true);
					lblNutrition13.setVisible(true);
					lblNutrition14.setVisible(true);

				}

				if (newValue.equalsIgnoreCase("FORMAT3") || newValue.equalsIgnoreCase("JAWAHAR FOODS")) {
					txtNutrition1.setVisible(true);
					txtNutrition2.setVisible(true);
					txtNutrition3.setVisible(true);
					txtNutrition4.setVisible(true);
					txtNutrition5.setVisible(true);
					txtNutrition6.setVisible(true);
					txtNutrition7.setVisible(true);
					txtNutrition8.setVisible(true);
						txtNutrition9.setVisible(true);
//						txtNutrition10.setVisible(true);
//						txtNutrition11.setVisible(true);
//						txtNutrition12.setVisible(true);
//						txtNutrition13.setVisible(true);
//						txtNutrition14.setVisible(true);

					lblNutrition1.setVisible(true);
					lblNutrition2.setVisible(true);
					lblNutrition3.setVisible(true);
					lblNutrition4.setVisible(true);
					lblNutrition5.setVisible(true);
					lblNutrition6.setVisible(true);
					lblNutrition7.setVisible(true);
					lblNutrition8.setVisible(true);
						lblNutrition9.setVisible(true);
//						lblNutrition10.setVisible(true);
//						lblNutrition11.setVisible(true);
//						lblNutrition12.setVisible(true);
//						lblNutrition13.setVisible(true);
//						lblNutrition14.setVisible(true);
				}

				if (newValue.equalsIgnoreCase("FORMAT9") || newValue.equalsIgnoreCase("CHICKEN")) {
					txtIngredient2.setVisible(false);
				}
			}
		});

	}

//	private void showPopup() {
//		try {
//			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
//			// fxmlLoader.setController(itemStockPopupCtl);
//			Parent root1;
//
//			root1 = (Parent) fxmlLoader.load();
//			Stage stage = new Stage();
//
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.initStyle(StageStyle.UNDECORATED);
//			stage.setTitle("Stock Item");
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.setScene(new Scene(root1));
//			stage.show();
//
//			// txtDuration.requestFocus();
//			//
//		} catch (IOException e) {
//
//			e.printStackTrace();
//		}
//
//	}

	private void showPopup() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			ItemStockPopupCtl itemStockPopupCtl = fxmlLoader.getController();
			itemStockPopupCtl.windowName = "WHOLESALE";
			root1 = (Parent) fxmlLoader.load();

			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}

		txtQty.requestFocus();
	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("------@@@-------popupItemlistner-------@@@@------" + itemPopupEvent.getItemName());
		Stage stage = (Stage) btnCalibrate.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {

					txtBarcode.clear();
					txtBarcode.clear();
					txtIngredient1.clear();
					txtDuration.clear();
					txtDuration.clear();
					txtMrp.clear();
					txtIngredient2.clear();
					txtItemName.setText(itemPopupEvent.getItemName());

					String itemId = itemPopupEvent.getItemId();

					ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(itemId);
					if (respentity == null) {

						return;
					}

					ItemMst itemMst = respentity.getBody();
					if (null != itemMst.getBarCode()) {
						txtBarcode.setText(itemPopupEvent.getBarCode());
					}
					txtMrp.setText(String.valueOf(itemMst.getStandardPrice()));

					if (null != itemMst.getBarCodeLine1()) {
						txtIngredient1.setText(itemMst.getBarCodeLine1());
					}

					if (null != itemMst.getBarCodeLine2()) {
						txtIngredient2.setText(itemMst.getBarCodeLine2());
					}

					if (null != itemMst.getBestBefore()) {
						txtDuration.setText(itemMst.getBestBefore() + "");
						LocalDate bestBeforeDays = dpProduction.getValue();

						LocalDate expDate = bestBeforeDays.plusDays(itemMst.getBestBefore());
						Date udate = SystemSetting.localToUtilDate(expDate);
						String sdate = SystemSetting.UtilDateToString(udate, "dd-MM-yyyy");
						txtbestBefore.setText(sdate);
						txtQty.requestFocus();
					} else {
						txtDuration.requestFocus();
					}

//			LocalDate bestBefore = dpProduction.getValue();

					txtNetWt.setText(itemMst.getNetWeight());

					// --------------29-3-2021 surya
					Platform.runLater(() -> {
						txtbatch.setText(itemPopupEvent.getBatch());

					});

					if (null != itemMst.getBarcodeFormat()) {
						cmbBarcodeFormat.setValue(itemMst.getBarcodeFormat());
						if (itemMst.getBarcodeFormat().equalsIgnoreCase("FORMAT9")
								|| itemMst.getBarcodeFormat().equalsIgnoreCase("CHICKEN")) {
							txtIngredient2.setVisible(false);
						}
					}

					setNutritionValues(itemMst);
					// --------------29-3-2021 surya end

					// LocalDate bestBefore = dpProduction.getValue();

//			Integer duration = Integer.valueOf(txtDuration.getText());
//			LocalDate expDate = bestBefore.plusDays(duration);
//			txtbestBefore.setText(String.valueOf(expDate));
				}
			});
		}
	}

	protected void setNutritionValues(ItemMst itemMst) {
		if (null != itemMst.getNutrition1() || !itemMst.getNutrition1().trim().isEmpty()) {
			txtNutrition1.setVisible(true);
			lblNutrition1.setVisible(true);
			txtNutrition1.setText(itemMst.getNutrition1());
		}

		if (null != itemMst.getNutrition2() || !itemMst.getNutrition2().trim().isEmpty()) {
			txtNutrition2.setVisible(true);
			lblNutrition2.setVisible(true);
			txtNutrition2.setText(itemMst.getNutrition2());
		}

		if (null != itemMst.getNutrition3() || !itemMst.getNutrition3().trim().isEmpty()) {
			txtNutrition3.setVisible(true);
			lblNutrition3.setVisible(true);
			txtNutrition3.setText(itemMst.getNutrition3());
		}
		if (null != itemMst.getNutrition4() || !itemMst.getNutrition4().trim().isEmpty()) {
			txtNutrition4.setVisible(true);
			lblNutrition4.setVisible(true);
			txtNutrition4.setText(itemMst.getNutrition4());
		}
		if (null != itemMst.getNutrition5() || !itemMst.getNutrition5().trim().isEmpty()) {
			txtNutrition5.setVisible(true);
			lblNutrition5.setVisible(true);
			txtNutrition5.setText(itemMst.getNutrition5());
		}
		if (null != itemMst.getNutrition6() || !itemMst.getNutrition6().trim().isEmpty()) {
			txtNutrition6.setVisible(true);
			lblNutrition6.setVisible(true);
			txtNutrition6.setText(itemMst.getNutrition6());
		}
		if (null != itemMst.getNutrition7() || !itemMst.getNutrition7().trim().isEmpty()) {
			txtNutrition7.setVisible(true);
			lblNutrition7.setVisible(true);
			txtNutrition7.setText(itemMst.getNutrition7());
		}
		if (null != itemMst.getNutrition8() || !itemMst.getNutrition8().trim().isEmpty()) {
			txtNutrition8.setVisible(true);
			lblNutrition8.setVisible(true);
			txtNutrition8.setText(itemMst.getNutrition8());
		}
		if (null != itemMst.getNutrition9() || !itemMst.getNutrition9().trim().isEmpty()) {
			txtNutrition9.setVisible(true);
			lblNutrition9.setVisible(true);
			txtNutrition9.setText(itemMst.getNutrition9());
		}
		if (null != itemMst.getNutrition10() || !itemMst.getNutrition10().trim().isEmpty()) {
			txtNutrition10.setVisible(true);
			lblNutrition10.setVisible(true);
			txtNutrition10.setText(itemMst.getNutrition10());
		}
		if (null != itemMst.getNutrition11() || !itemMst.getNutrition11().trim().isEmpty()) {
			txtNutrition11.setVisible(true);
			lblNutrition11.setVisible(true);
			txtNutrition11.setText(itemMst.getNutrition11());
		}
		if (null != itemMst.getNutrition12() || !itemMst.getNutrition12().trim().isEmpty()) {
			txtNutrition12.setVisible(true);
			lblNutrition12.setVisible(true);
			txtNutrition12.setText(itemMst.getNutrition12());
		}
		if (null != itemMst.getNutrition13() || !itemMst.getNutrition13().trim().isEmpty()) {
			txtNutrition13.setVisible(true);
			lblNutrition13.setVisible(true);
			txtNutrition13.setText(itemMst.getNutrition13());
		}
		if (null != itemMst.getNutrition14() || !itemMst.getNutrition14().trim().isEmpty()) {
			txtNutrition14.setVisible(true);
			lblNutrition14.setVisible(true);
			txtNutrition14.setText(itemMst.getNutrition14());
		}
	}

	/*
	 * @Subscribe public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
	 * 
	 * Stage stage = (Stage) btnCalibrate.getScene().getWindow();
	 * 
	 * if (stage.isShowing()) {
	 * 
	 * txtItemName.setText(itemPopupEvent.getItemName());
	 * 
	 * String itemId = itemPopupEvent.getItemId();
	 * 
	 * ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(itemId); if
	 * (respentity == null) {
	 * 
	 * return; }
	 * 
	 * ItemMst itemMst = respentity.getBody();
	 * 
	 * if(null!=itemMst.getBarCode()) { txtBarcode.setText(itemMst.getBarCode()); }
	 * txtMrp.setText(String.valueOf(itemMst.getStandardPrice()));
	 * 
	 * if(null!=itemMst.getBarCodeLine1()) {
	 * txtIngredient1.setText(itemMst.getBarCodeLine1()); }
	 * 
	 * if(null!=itemMst.getBarCodeLine2()) {
	 * txtIngredient2.setText(itemMst.getBarCodeLine2()); }
	 * 
	 * 
	 * if(null!=itemMst.getBestBefore()) {
	 * txtDuration.setText(itemMst.getBestBefore() + ""); LocalDate bestBeforeDays =
	 * dpProduction.getValue();
	 * 
	 * LocalDate expDate = bestBeforeDays.plusDays(itemMst.getBestBefore());
	 * txtbestBefore.setText(String.valueOf(expDate)); txtQty.requestFocus(); }else
	 * { txtDuration.requestFocus(); }
	 * 
	 * }
	 * 
	 * }
	 */

	@FXML
	void onActionEnterDuration(ActionEvent event) {
		LocalDate bestBefore = dpProduction.getValue();

		Integer duration = Integer.valueOf(txtDuration.getText());

		LocalDate expDate = bestBefore.plusDays(duration);
		Date udate = SystemSetting.localToUtilDate(expDate);
		String sdate = SystemSetting.UtilDateToString(udate, "dd-MM-yyyy");
		txtbestBefore.setText(sdate);
//		if (null==txtIngredient1.getText()) {
//			txtIngredient1.requestFocus();
//		}
//		if (null==txtIngredient2.getText()) {
//			txtIngredient2.requestFocus();
//		} else {
//			txtQty.requestFocus();
//		}

	}

	@FXML
	void showpopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			clear();
			txtItemName.requestFocus();
			if(SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO"))
			{
				showItemPopup();
			} else {
				showPopup();
			}
			txtQty.requestFocus();
		}

	}
	
	private void showItemPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void onActionBestBefore(ActionEvent event) {

		if (null == txtIngredient1.getText()) {
			txtIngredient1.requestFocus();
		}

	}

	@FXML
	void actionPressedTo(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtIngredient1.getText().trim().isEmpty()) {
				txtIngredient1.requestFocus();

			} else if (txtIngredient2.getText().trim().isEmpty()) {
				txtIngredient2.requestFocus();
			} else {
				txtQty.requestFocus();
			}
		}

	}

	@FXML
	void actionOnIngredients(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtIngredient2.requestFocus();
		}

	}

	@FXML
	void actionOnIngredients2(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtQty.requestFocus();
		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	void calibrateAction(ActionEvent event) {

		if (txtPrinterName.getText().trim().isEmpty()) {
			notifyMessage(5, "please PrinterName", false);
			return;
		}

		String PrinterNAme = SystemSetting.printer_name;
		TSCBarcode.caliberate(PrinterNAme);
	}

	private void print() {

		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "please select item", false);
			return;
		}
		if (txtDuration.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter Duration", false);
			return;
		}
		if (txtbestBefore.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter best before date", false);
			return;
		}
		if (txtIngredient1.getText().trim().isEmpty())

		{
			txtIngredient1.setText("");
			// notifyMessage(5, "please incredient", false);
			// return;
		}
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter Qty", false);
			return;
		}
		if (txtPrinterName.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter PrinterName", false);
			return;
		}

		if (null == cmbBarcodeFormat.getValue()) {
			notifyMessage(5, "please select  Barcode format", false);
			return;
		}
		ResponseEntity<ItemMst> respentity = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		ItemMst itemMst = respentity.getBody();
		// itemMst.setBarCode(txtBarcode.getText());
		itemMst.setBestBefore(Integer.valueOf(txtDuration.getText()));
		itemMst.setBarCodeLine1(txtIngredient1.getText());
		itemMst.setBarCodeLine2(txtIngredient2.getText());
		itemMst.setBarcodeFormat(cmbBarcodeFormat.getSelectionModel().getSelectedItem().toString());

		String batchCode = "";
		if (null != txtbatch.getText() && !txtbatch.getText().trim().isEmpty()) {
			batchCode = txtbatch.getText();
		} else {
			batchCode = "NOBATCH";
		}

		itemMst.setNetWeight(txtNetWt.getText());

		if (null != txtNutrition1.getText()) {
			itemMst.setNutrition1(txtNutrition1.getText());
		}
		if (null != txtNutrition2.getText()) {
			itemMst.setNutrition2(txtNutrition2.getText());
		}
		if (null != txtNutrition3.getText()) {
			itemMst.setNutrition3(txtNutrition3.getText());
		}
		if (null != txtNutrition4.getText()) {
			itemMst.setNutrition4(txtNutrition4.getText());
		}
		if (null != txtNutrition5.getText()) {
			itemMst.setNutrition5(txtNutrition5.getText());
		}
		if (null != txtNutrition6.getText()) {
			itemMst.setNutrition6(txtNutrition6.getText());
		}
		if (null != txtNutrition7.getText()) {
			itemMst.setNutrition7(txtNutrition7.getText());
		}
		if (null != txtNutrition8.getText()) {
			itemMst.setNutrition8(txtNutrition8.getText());
		}
		if (null != txtNutrition9.getText()) {
			itemMst.setNutrition9(txtNutrition9.getText());
		}
		if (null != txtNutrition10.getText()) {
			itemMst.setNutrition10(txtNutrition10.getText());
		}
		if (null != txtNutrition11.getText()) {
			itemMst.setNutrition11(txtNutrition11.getText());
		}
		if (null != txtNutrition12.getText()) {
			itemMst.setNutrition12(txtNutrition12.getText());
		}
		if (null != txtNutrition13.getText()) {
			itemMst.setNutrition13(txtNutrition13.getText());
		}
		if (null != txtNutrition14.getText()) {
			itemMst.setNutrition14(txtNutrition14.getText());
		}
		RestCaller.updateBarcode(itemMst);
		
		String barcodeToPrint = itemMst.getBarCode();

		
		if(!SystemSetting.NEGATIVEBILLING)
		{

		BarcodeBatchMst barcodeBatchMst = new BarcodeBatchMst();

		ResponseEntity<BarcodeBatchMst> barcodeBatchMstResp = RestCaller
				.barcodeBatchMstByBarcodeAndBatch(txtBarcode.getText(), batchCode);

		barcodeBatchMst = barcodeBatchMstResp.getBody();
		if (null == barcodeBatchMst) {

			barcodeBatchMst = new BarcodeBatchMst();

			barcodeBatchMst.setBarcode(txtBarcode.getText());
			barcodeBatchMst.setBatchCode(batchCode);
			barcodeBatchMst.setBranchCode(SystemSetting.getUser().getBranchCode());

			ResponseEntity<BarcodeBatchMst> barcodeBatchMstSaved = RestCaller.saveBarcodeBatchMst(barcodeBatchMst);
			barcodeBatchMst = barcodeBatchMstSaved.getBody();
		}
		
		barcodeToPrint = itemMst.getBarCode() + "-" + batchCode;

		if (null != barcodeBatchMst) {
			barcodeToPrint = barcodeBatchMst.getId();
		}
		
		}

		SystemSetting.BARCODE_FORMAT = cmbBarcodeFormat.getSelectionModel().getSelectedItem();

		String NumberOfCopies = txtQty.getText();
		LocalDate ldate = dpProduction.getValue();
		Date uProdDate = SystemSetting.localToUtilDate(ldate);
		String sProdDate = SystemSetting.UtilDateToString(uProdDate, "dd-MM-yyyy");

		String ManufactureDate = sProdDate;
		String ExpiryDate = txtbestBefore.getText();
		String ItemName = txtItemName.getText();
		String Barcode = txtBarcode.getText();
		String Mrp = txtMrp.getText();
		String IngLine1 = txtIngredient1.getText();
		String IngLine2 = txtIngredient2.getText();
		String PrinterNAme = SystemSetting.printer_name;
		String netWt = txtNetWt.getText().trim();


	

		String batchLine = batchCode;

		

		TSCBarcode.manualPrint(NumberOfCopies, ManufactureDate, ExpiryDate, ItemName, barcodeToPrint, Mrp, IngLine1,
				IngLine2, PrinterNAme, netWt, batchLine);

		// Barcode Without Heading

		/*
		 * BarcodePrinterZeebra
		 */

		clear();
		txtItemName.requestFocus();

//			}
//		}

		// Barcode Without Heading

		/*
		 * BarcodePrinterZeebra
		 */

		clear();
		txtItemName.requestFocus();

	}

	@FXML
	void onActionEnterQty(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if (null == txtDuration.getText() || txtDuration.getText().trim().length() == 0) {
				txtDuration.requestFocus();
			} else {
				btnPrint.requestFocus();
			}
		}

	}

	@FXML
	void actionBestBefore(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtIngredient1.getText().trim().isEmpty()) {
				txtIngredient1.requestFocus();

			}
		}

	}

	@FXML
	void printEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			print();

		}

	}

	@FXML
	void PrintAction(ActionEvent event) {
		print();
	}

	private void clear() {

		txtItemName.clear();
		// txtPrinterName.clear();
		txtBarcode.clear();
		txtMrp.clear();
		txtDuration.clear();
		txtbestBefore.setText("");
		txtIngredient1.clear();
		txtIngredient2.clear();
		txtQty.clear();
		txtNetWt.clear();

		txtNutrition1.clear();
		txtNutrition2.clear();
		txtNutrition3.clear();
		txtNutrition4.clear();
		txtNutrition5.clear();
		txtNutrition6.clear();
		txtNutrition7.clear();
		txtNutrition8.clear();
		txtNutrition9.clear();
		txtNutrition10.clear();
		txtNutrition11.clear();
		txtNutrition12.clear();
		txtNutrition13.clear();
		txtNutrition14.clear();

		txtNutrition1.setVisible(false);
		txtNutrition2.setVisible(false);
		txtNutrition3.setVisible(false);
		txtNutrition4.setVisible(false);
		txtNutrition5.setVisible(false);
		txtNutrition6.setVisible(false);
		txtNutrition7.setVisible(false);
		txtNutrition8.setVisible(false);
		txtNutrition9.setVisible(false);
		txtNutrition10.setVisible(false);
		txtNutrition11.setVisible(false);
		txtNutrition12.setVisible(false);
		txtNutrition13.setVisible(false);
		txtNutrition14.setVisible(false);

		lblNutrition1.setVisible(false);
		lblNutrition2.setVisible(false);
		lblNutrition3.setVisible(false);
		lblNutrition4.setVisible(false);
		lblNutrition5.setVisible(false);
		lblNutrition6.setVisible(false);
		lblNutrition7.setVisible(false);
		lblNutrition8.setVisible(false);
		lblNutrition9.setVisible(false);
		lblNutrition10.setVisible(false);
		lblNutrition11.setVisible(false);
		lblNutrition12.setVisible(false);
		lblNutrition13.setVisible(false);
		lblNutrition14.setVisible(false); // dpProduction.setValue(null);
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

}
