package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.svg.GetSVGDocument;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.PurchasePriceDefinitionDtl;
import com.maple.mapleclient.entity.PurchasePriceDefinitionMst;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class PurchasePriceDefinitionCtl {

	private EventBus eventBus = EventBusFactory.getEventBus();

	String taskid;
	String processInstanceId;
	String hdrId;

	private ObservableList<PurchasePriceDefinitionMst> purchasePriceDefinitionMstList = FXCollections
			.observableArrayList();

	private ObservableList<PriceDefinition> priceDefenitionListTable = FXCollections.observableArrayList();

	private ObservableList<PurchasePriceDefinitionDtl> purchasePriceDefinitionDtlListTable = FXCollections
			.observableArrayList();

	PurchasePriceDefinitionMst purchasePriceDefinitionMst = new PurchasePriceDefinitionMst();
	PurchasePriceDefinitionDtl purchasePriceDefinitionDtl = new PurchasePriceDefinitionDtl();

	@FXML
	private TableView<PurchasePriceDefinitionMst> tblPurchaseDtls;

	@FXML
	private TableColumn<PurchasePriceDefinitionMst, String> clPItemName;

	@FXML
	private TableColumn<PurchasePriceDefinitionMst, String> clPBatch;

	@FXML
	private TableColumn<PurchasePriceDefinitionMst, String> clPUnit;

	@FXML
	private TableView<PriceDefinition> tblPriceDefinition;

	@FXML
	private TableColumn<PriceDefinition, String> clPDPriceType;

	@FXML
	private TableColumn<PriceDefinition, Number> clPDAmount;

	@FXML
	private TextField txtVoucherNumber;

	@FXML
	private DatePicker dpDate;

	@FXML
	private Button btnFetchItem;

	@FXML
	private TableView<PurchasePriceDefinitionDtl> tblPriceDefinitionList;

	@FXML
	private TableColumn<PurchasePriceDefinitionDtl, String> clDPLItemName;

	@FXML
	private TableColumn<PurchasePriceDefinitionDtl, String> clPDLBatch;

	@FXML
	private TableColumn<PurchasePriceDefinitionDtl, String> clPDLPriceType;

	@FXML
	private TableColumn<PurchasePriceDefinitionDtl, String> clPDLAmount;

	@FXML
	private TableColumn<PurchasePriceDefinitionDtl, String> clPDLExpiryDate;

	@FXML
	private Button tblFinalSave;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtAmount;

	@FXML
	private ComboBox<String> cmbPriceType;

	@FXML
	private DatePicker dpStartDate;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnAddItem;

	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		eventBus.register(this);

		tblPurchaseDtls.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getItemId()) {
					LoadPriceDefenitionByItemId(newSelection.getItemId());

				}

				purchasePriceDefinitionMst = new PurchasePriceDefinitionMst();

				if (null != newSelection.getBatch()) {
					purchasePriceDefinitionMst.setBatch(newSelection.getBatch());
				}

				if (null != newSelection.getItemId()) {
					purchasePriceDefinitionMst.setItemId(newSelection.getItemId());
				}
				if (null != newSelection.getItemName()) {
					purchasePriceDefinitionMst.setItemName(newSelection.getItemName());
					txtItemName.setText(newSelection.getItemName());
				}
				if (null != newSelection.getUnitId()) {
					purchasePriceDefinitionMst.setUnitId(newSelection.getUnitId());
				}
				if (null != newSelection.getPurchaseVoucherDate()) {
					purchasePriceDefinitionMst.setPurchaseVoucherDate(newSelection.getPurchaseVoucherDate());
				}
				if (null != newSelection.getPurchaseVoucherNumber()) {
					purchasePriceDefinitionMst.setPurchaseVoucherNumber(newSelection.getPurchaseVoucherNumber());
				}
				if (null != newSelection.getPurchaseHdrId()) {
					purchasePriceDefinitionMst.setPurchaseHdrId(newSelection.getPurchaseHdrId());
				}

			}
		});

		tblPriceDefinitionList.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {

						purchasePriceDefinitionDtl = new PurchasePriceDefinitionDtl();
						purchasePriceDefinitionDtl.setId(newSelection.getId());
						purchasePriceDefinitionDtl.setPriceStatus(newSelection.getPriceStatus());

					}
				});
		cmbDisplay();

	}

	@FXML
	void addItem(ActionEvent event) {

		if (null != purchasePriceDefinitionMst) {
			if (txtItemName.getText().trim().isEmpty()) {
				notifyMessage(2, "Item name can not be null...", false);
				tblPurchaseDtls.requestFocus();
				return;

			}

			if (txtAmount.getText().trim().isEmpty()) {
				notifyMessage(2, "Amount can not be null...", false);
				txtAmount.requestFocus();
				return;

			}

			if (null == dpDate.getValue()) {
				notifyMessage(2, "Start date can not be null...", false);
				dpDate.requestFocus();
				return;

			}

			if (null == cmbPriceType.getValue()) {
				notifyMessage(2, "Start date can not be null...", false);
				cmbPriceType.requestFocus();
				return;

			}

			purchasePriceDefinitionDtl = new PurchasePriceDefinitionDtl();

			purchasePriceDefinitionDtl.setAmount(Double.parseDouble(txtAmount.getText()));
			purchasePriceDefinitionDtl.setBatch(purchasePriceDefinitionMst.getBatch());
			purchasePriceDefinitionDtl.setBranchCode(purchasePriceDefinitionMst.getBranchCode());
			purchasePriceDefinitionDtl.setCompanyMst(purchasePriceDefinitionMst.getCompanyMst());
			purchasePriceDefinitionDtl.setItemId(purchasePriceDefinitionMst.getItemId());
			purchasePriceDefinitionDtl.setItemName(purchasePriceDefinitionMst.getItemName());

			ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
					.getPriceDefenitionByName(cmbPriceType.getSelectionModel().getSelectedItem().toString());

			List<PriceDefenitionMst> priceDefList = priceDefenitionSaved.getBody();
			if (priceDefList.size() > 0) {
				purchasePriceDefinitionDtl.setPriceId(priceDefList.get(0).getId());

			}
			purchasePriceDefinitionDtl.setPriceStatus("N");
			purchasePriceDefinitionDtl.setPriceType(cmbPriceType.getSelectionModel().getSelectedItem().toString());
			purchasePriceDefinitionDtl.setPurchaseHdrId(purchasePriceDefinitionMst.getPurchaseHdrId());
			purchasePriceDefinitionDtl.setPurchaseVoucherDate(purchasePriceDefinitionMst.getPurchaseVoucherDate());
			purchasePriceDefinitionDtl.setPurchaseVoucherNumber(purchasePriceDefinitionMst.getPurchaseVoucherNumber());

			Date date = SystemSetting.localToUtilDate(dpStartDate.getValue());
			purchasePriceDefinitionDtl.setStartDate(date);
			purchasePriceDefinitionDtl.setUnitId(purchasePriceDefinitionMst.getUnitId());
			purchasePriceDefinitionDtl.setUnitName(purchasePriceDefinitionMst.getUnitName());

			ResponseEntity<PurchasePriceDefinitionDtl> purchasePriceDef = RestCaller
					.SavePurchasePriceDefinitionDtl(purchasePriceDefinitionDtl);
			purchasePriceDefinitionDtl = purchasePriceDef.getBody();

			ShowTablePDDtl();

		} else {
			notifyMessage(2, "Please select item", false);
			tblPurchaseDtls.requestFocus();
		}

		txtAmount.clear();
		txtItemName.clear();
		cmbPriceType.getSelectionModel().clearSelection();
		dpStartDate.setValue(null);
		purchasePriceDefinitionDtl = null;

	}

	private void ShowTablePDDtl() {

		ResponseEntity<List<PurchasePriceDefinitionDtl>> purchasePriceDefDtlResp = RestCaller
				.getPurchasePriceDefinitionDtl(hdrId);
		purchasePriceDefinitionDtlListTable = FXCollections.observableArrayList(purchasePriceDefDtlResp.getBody());

		fillTablePDDtl();
	}

	private void fillTablePDDtl() {

		tblPriceDefinitionList.setItems(purchasePriceDefinitionDtlListTable);

		clDPLItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clPDPriceType.setCellValueFactory(cellData -> cellData.getValue().getPriceTypeProperty());
		clPDLBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clPDLAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clPDLExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryProperty());

	}

	private void cmbDisplay() {

		if (null == cmbPriceType) {
			return;
		}

		cmbPriceType.getItems().clear();

		ArrayList priceType = new ArrayList();
		RestTemplate restTemplate = new RestTemplate();
		priceType = RestCaller.getPriceDefinition();
		Iterator itr = priceType.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object priceLevelName = lm.get("priceLevelName");
			Object id = lm.get("id");
			String priceName = (String) priceLevelName;
			if (id != null) {
				cmbPriceType.getItems().add((String) priceLevelName);

			}

		}

	}

	private void LoadPriceDefenitionByItemId(String id) {

		ResponseEntity<List<PriceDefinition>> priceDefinitionSaved = RestCaller.SearchPriceDefinitionByitemid(id);
		if (null != priceDefinitionSaved.getBody()) {
			priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
			fillTablePrevPriceDef();
		}

	}

	private void fillTablePrevPriceDef() {

		tblPriceDefinition.setItems(priceDefenitionListTable);
		for (PriceDefinition p : priceDefenitionListTable) {

			if (null != p.getPriceId()) {
				ResponseEntity<PriceDefenitionMst> respentity = RestCaller.getPriceNameById(p.getPriceId());
				if (null != respentity.getBody())
					p.setPriceType(respentity.getBody().getPriceLevelName());
				clPDPriceType.setCellValueFactory(cellData -> cellData.getValue().getPriceTypeProperty());
			}

		}
		clPDAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

	}

	@FXML
	void AmountOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			cmbPriceType.requestFocus();
		}
	}

	@FXML
	void FetchItemDetails(ActionEvent event) {
		if (null != hdrId || !hdrId.isEmpty()) {
			ResponseEntity<List<PurchasePriceDefinitionMst>> puchasePDMstList = RestCaller
					.getPurchasePriceDefinitionByHdrId(hdrId);

			purchasePriceDefinitionMstList = FXCollections.observableArrayList(puchasePDMstList.getBody());

			fillTablePPD();
			ShowTablePDDtl();
		}
	}

	@FXML
	void FinalSave(ActionEvent event) {

		if (null != hdrId) {
			ResponseEntity<String> priceDefStatusResp = RestCaller.updatePriceDefinitionFromPurchaseBatch(hdrId);
			String priceDefStatus = priceDefStatusResp.getBody();

			if (null != priceDefStatus) {
				notifyMessage(2, priceDefStatus, false);
				CleareFields();
				
				return;
			}

		}

	}

	private void CleareFields() {

		hdrId = null;
		tblPriceDefinition.getItems().clear();
		tblPriceDefinitionList.getItems().clear();
		tblPurchaseDtls.getItems().clear();
		txtAmount.clear();
		txtItemName.clear();
		txtVoucherNumber.clear();

		dpDate.setValue(null);
		dpStartDate.setValue(null);
	}

	@FXML
	void ItemNameOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtAmount.requestFocus();
		}

	}

	@FXML
	void PriceTypeOnENter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			dpStartDate.requestFocus();
		}

	}

	@FXML
	void purchaseDtlListOnEnter(KeyEvent event) {

	}

	@FXML
	void Delete(ActionEvent event) {

		if (null == purchasePriceDefinitionDtl) {

			notifyMessage(2, "Please select item...", false);
			tblPriceDefinitionList.requestFocus();
			return;

		}

		if (null == purchasePriceDefinitionDtl.getId()) {

			notifyMessage(2, "Please select item...", false);
			tblPriceDefinitionList.requestFocus();
			return;

		}
		
		if(null != purchasePriceDefinitionDtl.getPriceStatus())
		{
			if(purchasePriceDefinitionDtl.getPriceStatus().equalsIgnoreCase("N"))
			{
				RestCaller.deletePurchasePriceDefinitionDtlById(purchasePriceDefinitionDtl.getId());
				ShowTablePDDtl();

			}
		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {

		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String voucherNumber = taskWindowDataEvent.getVoucherNumber();
		String voucherDate = taskWindowDataEvent.getVoucherDate();
		hdrId = taskWindowDataEvent.getBusinessProcessId();

		txtVoucherNumber.setText(voucherNumber);
		dpDate.setValue(SystemSetting.StringToSqlDateSlash(voucherDate, "yyyy-MM-dd").toLocalDate());

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

		ResponseEntity<List<PurchasePriceDefinitionMst>> puchasePDMstList = RestCaller
				.getPurchasePriceDefinitionByHdrId(hdrId);

		purchasePriceDefinitionMstList = FXCollections.observableArrayList(puchasePDMstList.getBody());

		fillTablePPD();
		ShowTablePDDtl();

	}

	private void fillTablePPD() {

		tblPurchaseDtls.setItems(purchasePriceDefinitionMstList);

		for (PurchasePriceDefinitionMst purchase : purchasePriceDefinitionMstList) {
			ResponseEntity<UnitMst> unitMStResp = RestCaller.getunitMst(purchase.getUnitId());
			UnitMst unitMst = unitMStResp.getBody();
			if (null != unitMst) {
				purchase.setUnitName(unitMst.getUnitName());
			}
		}

		clPItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clPBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clPUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

	}

}
