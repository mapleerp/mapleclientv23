package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MenuMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.MenuEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class MenuPopupCtl {
	String taskid;
	String processInstanceId;


	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<MenuMst> menuList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

	MenuEvent menuEvent;

    @FXML
    private TextField txtMenu;

    
    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<MenuMst> tablePopupView;

    @FXML
    private TableColumn<MenuMst, String> menuName;

    @FXML
    private Button btnCancel;

    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtMenu.requestFocus();
		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    
    @FXML
	private void initialize() {
    	LoadMenuBySearch("");
		
    	txtMenu.textProperty().bindBidirectional(SearchString);
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		btnCancel.setCache(true);
		
		tablePopupView.setItems(menuList);
		
		menuName.setCellValueFactory(cellData -> cellData.getValue().getMenuNameProperty());
		
		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
				
					menuEvent = new MenuEvent();
					menuEvent.setId(newSelection.getId());
					menuEvent.setMenuName(newSelection.getMenuName());
					eventBus.post(menuEvent);
					
				}
			}
		});


		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadMenuBySearch((String) newValue);
			}
		});

    }
    
    
	private void LoadMenuBySearch(String searchData) {

		menuList.clear();
		ArrayList menuArray = new ArrayList();
		menuArray = RestCaller.searchMenuByName(searchData);
		Iterator itr = menuArray.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object menuName = lm.get("menuName");
			Object id = lm.get("id");
			if (null != id) {
				MenuMst menu = new MenuMst();
				
				menu.setMenuName((String) menuName);
				menu.setId((String) id);
				menuList.add(menu);
			}

		}
		
		

		return;

	}
	 @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }

	
}
