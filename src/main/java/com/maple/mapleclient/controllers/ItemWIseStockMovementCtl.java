package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.bouncycastle.asn1.DLTaggedObject;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.CategoryWiseStockMovement;
import com.maple.report.entity.StockSummaryDtlReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ItemWIseStockMovementCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<CategoryWiseStockMovement> itemList = FXCollections.observableArrayList();

    @FXML
    private TextField txtFromDate;

    @FXML
    private TextField txtToDate;

    @FXML
    private Button btnBack;
    @FXML
    private Button btnShow;

    @FXML
    private TableView<CategoryWiseStockMovement> tblItem;
    
    @FXML
    private TableColumn<CategoryWiseStockMovement,String> clVoucherDate;

    @FXML
    private TableColumn<CategoryWiseStockMovement, String> clItemName;

    @FXML
    private TableColumn<CategoryWiseStockMovement, Number> clOpen;

    @FXML
    private TableColumn<CategoryWiseStockMovement, Number> clIn;

    @FXML
    private TableColumn<CategoryWiseStockMovement, Number> clOut;

    @FXML
    private TableColumn<CategoryWiseStockMovement, Number> clClosing;

    @FXML
	private void initialize() {
		
		tblItem.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				System.out.println("getSelectionModel" + newSelection);
				if (null != newSelection.getCategory()) {
					
					ResponseEntity<ItemMst> getItem =RestCaller.getItemByNameRequestParam(newSelection.getCategory());
					ItemWiseStockMovementByDate(getItem.getBody().getItemName(),getItem.getBody().getId(),newSelection.getVoucherDate());
					}
			}
		});
	}

    
    private void ItemWiseStockMovementByDate(String itemName,String Itemid,Date voucherDate)
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/itemStockMovemntByDate.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			ItemStockMovementByDateCtl itemWIseStockMovementCtl =fxmlLoader.getController();
			itemWIseStockMovementCtl.stockSummaryDtls(itemName,Itemid,voucherDate);
   			Stage stage = new Stage();
   			stage.setScene(new Scene(root1));
   			stage.initModality(Modality.APPLICATION_MODAL);
   			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
		
	}


	@FXML
    void actionShow(ActionEvent event) {

    }
    
    public void itemWiseStkMovement(String catId,String fDate,String tDate)
    {
    	txtFromDate.setText(fDate);
    	txtToDate.setText(tDate);
    	
    	ResponseEntity<List<CategoryWiseStockMovement>> getItems = RestCaller.getItemWiseWiseStockMovement(catId,fDate,tDate);
    	itemList = FXCollections.observableArrayList(getItems.getBody());
    	
    	fillTable();
    }

    @FXML
    void actionBack(ActionEvent event) {
    	Stage stage = (Stage) tblItem.getScene().getWindow();
		stage.close();
    }
    
    private void fillTable()
    {
    	
    	for (CategoryWiseStockMovement cat:itemList)
    	{
    		if(null != cat.getClosingStock())
    		{
    		BigDecimal closing = new BigDecimal(cat.getClosingStock());
    		closing = closing.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        	cat.setClosingStock(closing.doubleValue());
    		}
    		else
    		{
    			cat.setClosingStock(0.0);
    		}
    		if(null != cat.getOpeningStock())
			{
        	BigDecimal opening = new BigDecimal(cat.getOpeningStock());
        	opening = opening.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        	cat.setOpeningStock(opening.doubleValue());
			}
    		else
    		{
    			cat.setOpeningStock(0.0);
    		}
    		if(null != cat.getInWardQty())
			{
        	BigDecimal inward = new BigDecimal(cat.getInWardQty());
        	inward = inward.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        	cat.setInWardQty(inward.doubleValue());
			}
    		else
    		{
    			cat.setInWardQty(0.0);
    		}
    		if(null != cat.getOutWardQty())
			{
        	BigDecimal outward = new BigDecimal(cat.getOutWardQty());
        	outward = outward.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        	cat.setOutWardQty(outward.doubleValue());
			}else
			{
				cat.setOutWardQty(0.0);
			}
    	}
    	tblItem.setItems(itemList);
		clClosing.setCellValueFactory(cellData -> cellData.getValue().getclosingStockProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
		clIn.setCellValueFactory(cellData -> cellData.getValue().getinWardQtyProperty());
		clOut.setCellValueFactory(cellData -> cellData.getValue().getoutWardQtyProperty());
		clOpen.setCellValueFactory(cellData -> cellData.getValue().getopeningStockProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}