package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.POSThermalPrintFactory;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BarcodeBatchMst;
import com.maple.mapleclient.entity.BatchPriceDefinition;

import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.entity.WeighingItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class TakeOrderCtl {
	String taskid;
	String processInstanceId;

	private static final Logger logger = LoggerFactory.getLogger(possWindowCtl.class);

	boolean initializedCalled = false;
	SalesReceipts salesReceipts = new SalesReceipts();
	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
	private ObservableList<SalesReceipts> salesReceiptsList = FXCollections.observableArrayList();
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();
	ReceiptModeMst receiptModeMst = null;
	ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();

	boolean multi = false;
	UnitMst unitMst = null;
	String previous_invamt_pos = SystemSetting.SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS;
	String invoiceNumberPrefix = SystemSetting.POS_SALES_PREFIX;
	POSThermalPrintABS printingSupport;
	String salesReceiptVoucherNo = null;

	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
	private ObservableList<SalesTransHdr> saleTransListTable = FXCollections.observableArrayList();
	// BranchMst branch = null;
	SalesDtl salesDtl = null;
	LocalCustomerMst localCustomerMst = null;
	SalesTransHdr salesTransHdr = null;
	String fixedQty = null;
	double qtyTotal = 0;
	// double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	double cardAmount = 0.0;
	String custId = "";

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty sodexoAmountLis = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");

	StringProperty barcodeProperty = new SimpleStringProperty("");

	StringProperty taxRateProperty = new SimpleStringProperty("");

	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");

	SalesTransHdr salesTransHr = null;
	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtRate;

	@FXML
	private Button btnAdditem;

	@FXML
	private Button btnClearSelection;
	@FXML
	private TextField txtBatch;
	@FXML
	private ComboBox<String> cmbSalesMode;
	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private Button btnUnhold;
	@FXML
	private Button btnPrintKot;
	@FXML
	private Button btnHold;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnMrp;

	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private ComboBox<String> cmbCardType;

	@FXML
	private Label lblCardType;

	@FXML
	private Label lblAmount;

	@FXML
	private Button AddCardAmount;

	@FXML
	private TableView<SalesReceipts> tblCardAmountDetails;

	@FXML
	private TableColumn<SalesReceipts, String> clCardTye;

	@FXML
	private TableColumn<SalesReceipts, Number> clCardAmount;

	@FXML
	private Button btnDeleteCardAmount;

	@FXML
	private TextField txtCrAmount;

	@FXML
	private TableView<SalesTransHdr> tblHold;

	@FXML
	private TableColumn<SalesTransHdr, String> clholdedId;

	@FXML
	private TableColumn<SalesTransHdr, String> clOrdrNo;
	@FXML
	private TableColumn<SalesTransHdr, String> clUser;

	@FXML
	private TableColumn<SalesTransHdr, Number> clInvoiceAmt;

	@FXML
	private Label lblPreviosAmt;

	@FXML
	private Label lblAmt;

	@FXML
	private Button btnCardSale;

	@FXML
	private TextField txtTotalNumberofItems;

	@FXML
	private TextField txtCustomerPhoneNo;

	@FXML
	private TextField txtCustomerName;

	@FXML
	private void initialize() {

		eventBus.register(this);

//		cmbSalesMode.getItems().add("TAKE AWAY");
		cmbSalesMode.getItems().add("HOME DELIVERY");
		cmbSalesMode.getSelectionModel().select("HOME DELIVERY");
		holdedPOs();
		/*
		 * Load logo to printing support once
		 */

		printingSupport = POSThermalPrintFactory.getPOSThermalPrintABS();
		try {
			printingSupport.setupLogo();

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			printingSupport.setupBottomline(SystemSetting.getPosInvoiceBottomLine());

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		setCardType();

		/*
		 * final submit. SalesDtl will be added on AddItem Function
		 * 
		 */
		// salesDtl = new SalesDtl();

		/*
		 * Fieds with Entity property
		 */
		salesDtl = new SalesDtl();

		/*
		 * branch = new BranchMst(); ResponseEntity<BranchMst> respentity =
		 * RestCaller.SearchMyBranch(); branch = respentity.getBody();
		 */

		txtCashtopay.setStyle("-fx-text-inner-color: red;-fx-font-size: 24px;");

		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);

		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});
		txtCustomerPhoneNo.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCustomerPhoneNo.setText(oldValue);
				}
			}
		});
		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPaidamount.setText(oldValue);
				}
			}
		});

		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashtopay.setText(oldValue);
				}
			}
		});

		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});

		// Multi Unit Billing is disabled for Testing - REGY on June 19 2020
		/*
		 * cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
		 * 
		 * @Override public void changed(ObservableValue<? extends String> observable,
		 * String oldValue, String newValue) { ResponseEntity<ItemMst> getItem =
		 * RestCaller.getItemByNameRequestParam(txtItemname.getText());
		 * 
		 * 
		 * ItemMst selecteditem = getItem.getBody();
		 * 
		 * String selectUnit = cmbUnit.getSelectionModel().getSelectedItem();
		 * 
		 * ResponseEntity<UnitMst> getUnit = RestCaller .getUnitByName(selectUnit);
		 * 
		 * 
		 * UnitMst selectedUnitMst = getUnit.getBody();
		 * 
		 * 
		 * ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller
		 * .getMultiUnitbyprimaryunit(selecteditem.getId(), selectedUnitMst.getId());
		 * 
		 * 
		 * if (null != getMultiUnit.getBody()) {
		 * txtRate.setText(Double.toString(getMultiUnit.getBody().getPrice())); } else {
		 * 
		 * txtRate.setText(Double.toString(selecteditem.getStandardPrice())); } } });
		 * 
		 */

		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if ((txtcardAmount.getText().length() >= 0)) {

					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;

					}

					try {
						paidAmount = Double.parseDouble(txtPaidamount.getText());
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					try {
						cardPaid = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
					if (newrate.doubleValue() < 0) {

						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
					} else {

						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
					}

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if ((txtPaidamount.getText().length() > 0)) {

					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;
					}

					try {
						cardPaid = Double.parseDouble(txtcardAmount.getText());
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					try {
						paidAmount = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());

					if (newrate.doubleValue() < 0) {

						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
					} else {

						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
					}

				}

				if (((String) newValue).length() == 0) {
					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;
					}

					try {
						cardPaid = Double.parseDouble(txtcardAmount.getText());
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					try {
						paidAmount = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
				}

			}
		});

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesDtl = new SalesDtl();
					salesDtl.setId(newSelection.getId());
					if (null != newSelection.getOfferReferenceId()) {
						salesDtl.setOfferReferenceId(newSelection.getOfferReferenceId());
					}

					if (null != newSelection.getSchemeId()) {
						salesDtl.setSchemeId(newSelection.getSchemeId());
					}
					salesDtl.setItemId(newSelection.getItemId());
					salesDtl.setSalesTransHdr(newSelection.getSalesTransHdr());

					salesDtl.setQty(newSelection.getQty());
					txtBarcode.setText(newSelection.getBarcode());
					txtBatch.setText(newSelection.getBatchCode());
					txtItemname.setText(newSelection.getItemName());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));

					cmbUnit.setValue(newSelection.getUnitName());

				}
			}
		});

		itemDetailTable.setOnKeyPressed((event) -> {
			if (event.getCode().isDigitKey()) {
				txtQty.requestFocus();
			} else if (event.getCode() == KeyCode.ENTER) {
				txtQty.requestFocus();
			} else if (event.getCode() == KeyCode.DELETE) {
				deleteRow();
			}
		});

		tblHold.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					txtCustomerName.clear();
					txtCustomerPhoneNo.clear();
					salesTransHdr = new SalesTransHdr();
					salesTransHdr.setId(newSelection.getId());
					System.out.println("SALES TRANSHDR ID" + salesTransHdr.getId());
					salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
					txtCustomerName.setText(salesTransHdr.getLocalCustomerMst().getLocalcustomerName());
					LocalCustomerMst lc = salesTransHdr.getLocalCustomerMst();
					txtCustomerPhoneNo.setText(lc.getPhoneNo());
				}
			}
		});

		tblCardAmountDetails.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						if (null != newSelection.getId()) {

							salesReceipts = new SalesReceipts();
							salesReceipts.setId(newSelection.getId());
						}
					}
				});
		// btnSave.setDisable(true);
		itemDetailTable.setItems(saleListTable);

//		holdedPOs();

		cmbSalesMode.requestFocus();
	}

	@FXML
	void salesModeKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtBarcode.requestFocus();
		}
	}

	@FXML
	void actionClearSelection(ActionEvent event) {

		tblHold.getSelectionModel().clearSelection();
		itemDetailTable.getItems().clear();
		saleListItemTable.clear();
		salesTransHdr = null;
		txtPaidamount.clear();
		txtCashtopay.clear();
		txtChangeamount.clear();
		txtPaidamount.clear();
		txtCustomerName.clear();
		txtCustomerPhoneNo.clear();
	}

	@FXML
	void DeleteCardAmount(ActionEvent event) {
		if (null != salesReceipts) {
			if (null != salesReceipts.getId()) {
				RestCaller.deleteSalesReceipts(salesReceipts.getId());
				getAllCardAmount();

			}
		}
	}

	private void holdedPOs() {
		ResponseEntity<List<SalesTransHdr>> holdedpos = RestCaller.getHoldedTakerOrderSales();
		saleTransListTable = FXCollections.observableArrayList(holdedpos.getBody());
		if (saleTransListTable.size() > 0) {
			tblHold.setVisible(true);
			btnUnhold.setVisible(true);
		}

		for (SalesTransHdr salesTrans : saleTransListTable) {
			Double invoiceAmount = 0.0;
			// Get Local Customer Phone Number
			if (null != salesTrans.getLocalCustomerMst()) {
				ResponseEntity<LocalCustomerMst> getuser = RestCaller
						.getLocalCustomerById(salesTrans.getLocalCustomerMst().getId());
				if (null != getuser.getBody()) {
					salesTrans.setUserName(getuser.getBody().getLocalcustomerName());
				}

			}
			ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTrans);
			for (SalesDtl salesdt : saledtlSaved.getBody()) {
				invoiceAmount = invoiceAmount + salesdt.getAmount();
			}
			salesTrans.setInvoiceAmount(invoiceAmount);
		}
		tblHold.setItems(saleTransListTable);
		clholdedId.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
		clUser.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());

		clOrdrNo.setCellValueFactory(cellData -> cellData.getValue().getTakeOrderNumberProperty());
		clInvoiceAmt.setCellValueFactory(cellData -> cellData.getValue().getinvoiceAmountProperty());

		txtBarcode.requestFocus();
	}

	private void FinalSave() throws SQLException {

		if (null == salesTransHdr) {
			return;
		}

		Double cashPaid = 0.0;
		Double cardAmount = 0.0;
		Double cashToPay = 0.0;
		Double sodexoAmount = 0.0;
		Double changeAmount = 0.0;

		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		if (saledtlSaved.getBody().size() == 0) {
			return;
		}

		if (null == txtCashtopay.getText() || txtCashtopay.getText().trim().isEmpty()) {

			return;
		}

		if (!txtcardAmount.getText().trim().isEmpty()) {

			cardAmount = Double.parseDouble(txtcardAmount.getText());
		}

		cashToPay = Double.parseDouble(txtCashtopay.getText());

		Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
		Double AmountTenderd = 0.0;

		cashPaid = Double.parseDouble(txtPaidamount.getText());

		if (!txtPaidamount.getText().isEmpty()) {
			AmountTenderd = Double.parseDouble(txtPaidamount.getText());
		}

		Double totalAmountTenderd = AmountTenderd + cardAmount;
		if (totalAmountTenderd >= invoiceAmount) {
			cashPaid = invoiceAmount - cardAmount;

		} else if (totalAmountTenderd < invoiceAmount) {

			notifyMessage(1, "Please Enter Valid Amount!!!!");
			return;
		}

		// ==========receipts

		if (!txtPaidamount.getText().trim().isEmpty()) {

			saveCashPaidAmountReceipts(cashPaid, salesTransHdr);

		}

		if (!txtChangeamount.getText().trim().isEmpty()) {
			changeAmount = Double.parseDouble(txtChangeamount.getText());

		}

		txtChangeamount.setText("00.0");

		String card = "";
		String cardType = "";

		if (cashToPay <= (cardAmount + cashPaid)) {

			salesTransHdr.setBranchCode(SystemSetting.systemBranch);

			salesTransHdr.setCardType(cardType);
			if (!txtPaidamount.getText().trim().isEmpty()) {
				salesTransHdr.setCashPay(cashPaid);
			}

			salesTransHdr.setCardamount(cardAmount);
			salesTransHdr.setPaidAmount(cashPaid + cardAmount);
			salesTransHdr.setChangeAmount(changeAmount);
			salesTransHdr.setSodexoAmount(sodexoAmount);
			salesTransHdr.setInvoiceAmount(cashToPay);
			salesTransHdr.setSalesReceiptsVoucherNumber(salesReceiptVoucherNo);
			salesTransHdr.setSalesMode("POS");
			salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);

			if (null == salesTransHdr.getVoucherNumber()) {
				RestCaller.updateSalesTranshdr(salesTransHdr);
			}
			salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());

			eventBus.post(salesTransHdr);

			InsertIntoDayBook(salesTransHdr);

			System.out.println("Now Print Invoice");

			PrintRoutine();

			if (previous_invamt_pos.equalsIgnoreCase("YES")) {
				lblAmt.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
				lblPreviosAmt.setText("Amount:");
				lblAmt.setText(txtCashtopay.getText());
			}
			
			
			txtcardAmount.setText("");
			txtCashtopay.setText("");
			txtPaidamount.setText("");
			txtChangeamount.clear();
			saleListTable.clear();
			txtBarcode.requestFocus();
			txtcardAmount.setText("");
			txtCashtopay.setText("");
			txtPaidamount.setText("");
			txtChangeamount.clear();
			saleListTable.clear();
			txtBarcode.requestFocus();
			salesTransHdr = null;
			txtcardAmount.setText("");
			txtCashtopay.setText("");
			txtPaidamount.setText("");
			saleListTable.clear();
			txtBarcode.requestFocus();
			salesDtl = new SalesDtl();
			tblCardAmountDetails.getItems().clear();
			salesReceiptVoucherNo = null;
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtQty.clear();
			salesReceiptsList.clear();

		} else {
			notifyMessage(1, "Please Enter Valid Amount!!!!");
		}

		tblCardAmountDetails.setVisible(false);
		cmbCardType.setVisible(false);
		txtCrAmount.setVisible(false);
		txtCustomerPhoneNo.clear();
		txtCustomerName.clear();
		AddCardAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		txtcardAmount.setVisible(false);
		lblAmount.setVisible(false);
		lblCardType.setVisible(false);
		tblHold.getItems().clear();
		holdedPOs();
		cmbSalesMode.getSelectionModel().clearSelection();

	}

	private void PrintRoutine() {

		try {
			Integer noOfPrintCopies = 1;
			ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("TakeOrderPrintCopies");
			if (null != getParam.getBody()) {
				noOfPrintCopies = Integer.parseInt(getParam.getBody().getValue());
			}
			while (noOfPrintCopies > 0) {

				printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());

				noOfPrintCopies = noOfPrintCopies - 1;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void saveCashPaidAmountReceipts(Double cashPaid, SalesTransHdr salesTransHdr) {

		salesReceipts = new SalesReceipts();
		if (null == salesReceiptVoucherNo) {
			salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
			salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
		} else {
			salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
		}
		ResponseEntity<AccountHeads> accountHeads = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-" + "CASH");
		salesReceipts.setAccountId(accountHeads.getBody().getId());
		salesReceipts.setReceiptMode(salesTransHdr.getBranchCode() + "-" + "CASH");

		salesReceipts.setReceiptAmount(cashPaid);
		salesReceipts.setUserId(SystemSetting.getUser().getId());
		salesReceipts.setBranchCode(SystemSetting.systemBranch);
		salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
		salesReceipts.setSalesTransHdr(salesTransHdr);
		ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
		salesReceipts = respEntity.getBody();
	}

	private void InsertIntoDayBook(SalesTransHdr salesTransHdr) {

		ResponseEntity<List<SalesReceipts>> salesreceipt = RestCaller
				.getSalesReceiptsByTransHdrId(salesTransHdr.getId());
		salesReceiptsList = FXCollections.observableArrayList(salesreceipt.getBody());
		salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
		for (SalesReceipts salesRec : salesReceiptsList) {
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(salesTransHdr.getBranchCode());
			ResponseEntity<AccountHeads> accountHead1 = RestCaller.getAccountById(salesRec.getAccountId());
			AccountHeads accountHeads1 = accountHead1.getBody();
			dayBook.setDrAccountName(accountHeads1.getAccountName());
			dayBook.setDrAmount(salesRec.getReceiptAmount());
			dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
			if (salesRec.getReceiptMode().contains("CASH")) {
				dayBook.setNarration("POS CASH SALE");
				dayBook.setCrAmount(0.0);

			} else {
				dayBook.setNarration("POS CARD SALE");
				dayBook.setCrAmount(salesRec.getReceiptAmount());
				dayBook.setCrAccountName("POS");
			}
			dayBook.setSourceVoucherType("SALES RECEIPTS");

			LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(rdate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}
		DayBook dayBook = new DayBook();
		dayBook.setBranchCode(salesTransHdr.getBranchCode());
		dayBook.setDrAccountName("POS");
		dayBook.setDrAmount(salesTransHdr.getInvoiceAmount());
		dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
		dayBook.setSourceVoucherType("SALES");
		dayBook.setNarration("POS");
		dayBook.setCrAccountName("SALES ACCOUNT");
		dayBook.setCrAmount(salesTransHdr.getInvoiceAmount());

		LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
		dayBook.setsourceVoucherDate(Date.valueOf(rdate));
		ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
	}

	@FXML
	void FocusOnCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCrAmount.requestFocus();
		}

	}

	@FXML
	void KeyPressAddCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			addCardAmount();
		}
		if (event.getCode() == KeyCode.RIGHT) {
			txtPaidamount.requestFocus();
		}

	}

	@FXML
	void KeyPressDeleteCardAmount(KeyEvent event) {

	}

	private void addCardAmount() {

		if (null != salesTransHdr) {

			if (null == cmbCardType.getValue()) {

				notifyMessage(1, "Please select card type!!!!");
				cmbCardType.requestFocus();
				return;

			} else if (txtCrAmount.getText().trim().isEmpty()) {

				notifyMessage(1, "Please enter amount!!!!");
				txtCrAmount.requestFocus();
				return;

			} else {

				// rest call for salesReceipt voucher number
				// generaly voucherNo. generation done by backend, its a special case
				salesReceipts = new SalesReceipts();
				if (null == salesReceiptVoucherNo) {
					salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
				} else {
					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
				}
				ResponseEntity<AccountHeads> accountHeads = RestCaller
						.getAccountHeadByName(cmbCardType.getSelectionModel().getSelectedItem());
				salesReceipts.setAccountId(accountHeads.getBody().getId());
				salesReceipts.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());
				salesReceipts.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
				salesReceipts.setUserId(SystemSetting.getUser().getId());
				salesReceipts.setBranchCode(SystemSetting.systemBranch);

//				LocalDate date = LocalDate.now();
//				java.util.Date udate = SystemSetting.localToUtilDate(date);
				salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
				salesReceipts.setSalesTransHdr(salesTransHdr);
				System.out.println(salesReceipts);
				ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
				salesReceipts = respEntity.getBody();

				if (null != salesReceipts) {
					if (null != salesReceipts.getId()) {
						notifyMessage(1, "Successfully added cardAmount");
//				cardAmount = cardAmount+Double.parseDouble(txtCrAmount.getText());
//				txtcardAmount.setText(Double.toString(cardAmount));
						salesReceiptsList.add(salesReceipts);
						filltableCardAmount();
					}
				}

				txtCrAmount.clear();
				cmbCardType.getSelectionModel().clearSelection();
				cmbCardType.requestFocus();
				salesReceipts = null;

			}
		} else {
			notifyMessage(1, "Please add Item!!!!");
			return;
		}

	}

	@FXML
	void KeyPressFocusAddBtn(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if (txtCrAmount.getText().length() == 0 && null == cmbCardType.getValue()) {
				btnSave.requestFocus();
			} else {

				AddCardAmount.requestFocus();
			}
		}
		if (event.getCode() == KeyCode.RIGHT) {
			txtPaidamount.requestFocus();
		}
	}

	@FXML
	void SaveButton(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			try {
				FinalSave();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@FXML
	void VisibleCardSale(ActionEvent event) {
		visibleCardSale();
	}

	private void visibleCardSale() {

		tblCardAmountDetails.setVisible(true);
		cmbCardType.setVisible(true);
		txtCrAmount.setVisible(true);
		AddCardAmount.setVisible(true);
		btnDeleteCardAmount.setVisible(true);
		txtcardAmount.setVisible(true);
		lblAmount.setVisible(true);
		lblCardType.setVisible(true);

	}

	@FXML
	void actionstart(ActionEvent event) {

	}

	@FXML
	void addCardAmount(ActionEvent event) {

		addCardAmount();
	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();

	}

	@FXML
	void deleteRow(ActionEvent event) {
		deleteRow();
		salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtl(salesTransHdr);
		if (getSalesDtl.getBody().size() == 0) {
			RestCaller.deleteSalesTransHdr(salesTransHdr.getId());
			salesTransHdr = null;
			salesDtl = new SalesDtl();
		}
		holdedPOs();

	}

	private void deleteRow() {
		Alert a = new Alert(AlertType.CONFIRMATION);
		a.setHeaderText("Delete Item");
		a.setContentText("Are you sure you want to Delete Item");
		a.showAndWait().ifPresent((btnType) -> {
			if (btnType == ButtonType.OK) {
				try {

					if (null != salesDtl) {
						if (null != salesDtl.getId()) {

							if (null == salesDtl.getSchemeId()) {

								// Save Data to SalesDeleteDtl

								RestCaller.deleteSalesDtl(salesDtl.getId());

								System.out.println("toDeleteSale.getId()" + salesDtl.getId());
//						RestCaller.deleteSalesDtl(salesDtl.getId());
								txtItemname.clear();
								txtBarcode.clear();
								txtBatch.clear();
								txtRate.clear();
								txtQty.clear();
								ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
								saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
								FillTable();
								salesDtl = new SalesDtl();
								Integer countOfSalesDtl = RestCaller.getCountOfSalesDtl(salesTransHdr.getId());
								if (null != countOfSalesDtl) {
									txtTotalNumberofItems.setText(Integer.toString(countOfSalesDtl));
								}
								notifyMessage(1, " Item Deleted Successfully");
								txtBarcode.requestFocus();

								System.out.println("SALES TRANSHDR ID" + salesTransHdr.getId());
							} else {
								notifyMessage(1, " Offer can not be delete");
							}

						}
					}

				} catch (Exception e) {
					System.out.println(e);
				}
			} else if (btnType == ButtonType.CANCEL) {

				return;

			}
		});

	}

	@FXML
	void hold(ActionEvent event) {

		hold();
	}

	private void hold() {
		checkForLocalCustomer();
		localCustomerExist = false;
		String hdrId = salesTransHdr.getId();
		salesDtl = null;
		salesTransHdr = null;
		clearFields();
		txtBarcode.requestFocus();
		txtCustomerName.clear();
		txtCustomerPhoneNo.clear();
		itemDetailTable.getItems().clear();
		saleListItemTable.clear();
		txtCashtopay.clear();
		holdedPOs();
		notifyMessage(3, "Holded");
		txtBarcode.requestFocus();
	}

	@FXML
	void holdOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			hold();
		}
	}

	@FXML
	void actionPrintKot(ActionEvent event) {

		printKot();
		salesTransHdr = null;
		txtBarcode.requestFocus();
	}

	@FXML
	void holdOnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ESCAPE) {
			txtBarcode.requestFocus();
		}
		if (event.getCode() == KeyCode.ENTER) {
			unHold();

		}
		if (event.getCode() == KeyCode.R && event.isAltDown()) {
			txtPaidamount.requestFocus();
		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	private void addItem() {

		// version2.0
		/// check financial year
		// ----------check day end
		// stock checking
		// stock checking for kit
		// create sales_trans_hdr
		// set sales dtl
		// perform calcuations
		// save sales dtls

		if (null == cmbSalesMode.getSelectionModel() || null == cmbSalesMode.getSelectionModel().getSelectedItem()) {
			notifyFailureMessage(3, "Select Sales Mode");
			return;
		}

		ResponseEntity<List<FinancialYearMst>> getFinancialYear = RestCaller.getAllFinancialYear();
		if (getFinancialYear.getBody().size() == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}
		int count = RestCaller.getFinancialYearCount();
		if (count == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}

		if (SystemSetting.systemBranch.equalsIgnoreCase(null)) {
			notifyMessage(5, "Incorrect Branch");
			return;
		}

		boolean validDay = CheckForValidLoginDate();
		if (!validDay) {
			return;
		}

		if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(1, " Please type Item...!!!");
			txtItemname.requestFocus();
		} else if (txtBatch.getText().trim().isEmpty()) {
			notifyMessage(1, "Item Batch is not present!!!");
			txtQty.requestFocus();
			return;
		}

		ResponseEntity<UnitMst> getUnitBYItem = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

		ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
				getUnitBYItem.getBody().getId());
		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			if (null == getmulti.getBody()) {
				notifyMessage(5, "Please Add the item in Multi Unit");
				return;
			}
		}

		if (null == salesTransHdr) { // || null == salesTransHdr.getId()

			salesTransHdr = crteateSalesTranshdr();
		}

		if (null == salesTransHdr && null != salesTransHdr.getId()) {
			return;
		}

		boolean stockChecking = stockChecking(getItem.getBody(), getUnitBYItem.getBody());

		if (null != salesDtl) {
			// ==========delete sales_dtl for editing
			deleteSalesDtlFroEdit();
		}

		salesDtl = SaveSalesDtl();
		salesDtl = new SalesDtl();


		

		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);
		List<SalesDtl> salesDtlList = respentityList.getBody();

		/*
		 * Call Rest to get the summary and set that to the display fields
		 */

		saleListTable.clear();
		saleListTable.setAll(salesDtlList);
		FillTable();
		clearFields();
		Integer countOfSalesDtl = RestCaller.getCountOfSalesDtl(salesTransHdr.getId());
		if (null != countOfSalesDtl) {
			txtTotalNumberofItems.setText(Integer.toString(countOfSalesDtl));
		}

	}

	private SalesDtl SaveSalesDtl() {

		salesDtl = new SalesDtl();

		if (txtQty.getText().trim().isEmpty() && fixedQty == null) {

			notifyMessage(1, " Item please enter Qty...!!!");
			return null;

		}

		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(txtItemname.getText());
		salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());

		String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();
		salesDtl.setBatchCode(batch);

		Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());
		salesDtl.setQty(qty);
		Double mrpRateIncludingTax = 00.0;
		mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
		salesDtl.setMrp(mrpRateIncludingTax);

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst
																											// =
		ItemMst item = respsentity.getBody();

		salesDtl.setBarcode(item.getBarCode());
		salesDtl.setItemCode(item.getItemCode());
		salesDtl.setStandardPrice(item.getStandardPrice());

		/*
		 * Calculate Rate Before Tax
		 */

		salesDtl.setItemId(item.getId());
		if (null == cmbUnit.getValue()) {
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
			salesDtl.setUnitId(getUnit.getBody().getId());
			salesDtl.setUnitName(getUnit.getBody().getUnitName());
		} else {
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
			salesDtl.setUnitId(getUnit.getBody().getId());
			salesDtl.setUnitName(getUnit.getBody().getUnitName());
		}

		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());

		if (getTaxMst.getBody().size() > 0) {

			salesDtl = TaxCalcutaionUsingTaxMst(getTaxMst.getBody(), mrpRateIncludingTax, salesDtl);

		} else {

			salesDtl = taxCalculationWithoutTaxMst(salesDtl, item, mrpRateIncludingTax);

		}
		salesDtl.setAmount(Double.parseDouble(txtQty.getText()) * Double.parseDouble(txtRate.getText()));
		salesDtl = setCostFromPriceDefenition(salesDtl);
		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		salesDtl = respentity.getBody();

		return salesDtl;
	}

	private void deleteSalesDtlFroEdit() {

		if (null != salesDtl.getId()) {
			RestCaller.deleteSalesDtl(salesDtl.getId());
			salesDtl = null;

		}
	}

	private SalesDtl setCostFromPriceDefenition(SalesDtl salesDtl) {

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if (null != priceDefenitionMst) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller.getBatchPriceDefinition(
					salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), salesDtl.getBatch(), sdate);
			if (null != batchpriceDef.getBody()) {
				salesDtl.setCostPrice(batchpriceDef.getBody().getAmount());
			} else {
				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();
				if (null != priceDefinition) {
					salesDtl.setCostPrice(priceDefinition.getAmount());
				}
			}
		}

		return salesDtl;
	}

	

	private SalesDtl taxCalculationWithoutTaxMst(SalesDtl salesDtl, ItemMst item, Double mrpRateIncludingTax) {

		Double taxRate = 00.0;
		taxRate = item.getTaxRate();
		salesDtl.setTaxRate(taxRate);
		Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
		salesDtl.setRate(rateBeforeTax);

		double cessRate = item.getCess();
		double cessAmount = 0.0;
		if (cessRate > 0) {

			rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
			salesDtl.setRate(rateBeforeTax);
			cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;
		}

		salesDtl.setCessRate(cessRate);

		salesDtl.setCessAmount(cessAmount);

		salesDtl.setSgstTaxRate(taxRate / 2);

		salesDtl.setCgstTaxRate(taxRate / 2);

		salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		return salesDtl;
	}

	private SalesDtl TaxCalcutaionUsingTaxMst(List<TaxMst> taxMstList, Double mrpRateIncludingTax, SalesDtl salesDtl) {

		for (TaxMst taxMst : taxMstList) {

			if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
				salesDtl.setIgstTaxRate(taxMst.getTaxRate());
				salesDtl.setTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
				salesDtl.setCgstTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
				salesDtl.setSgstTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
				salesDtl.setCessRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
				salesDtl.setAddCessRate(taxMst.getTaxRate());
			}

		}

		Double rateBeforeTax = (100 * mrpRateIncludingTax)
				/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate());
		salesDtl.setRate(rateBeforeTax);
		BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
		salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
		BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
		salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
		BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
		salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
		BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
		salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
		BigDecimal adcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
		salesDtl.setAddCessAmount(adcessAmount.doubleValue() * salesDtl.getQty());

		return salesDtl;
	}

	private boolean stockChecking(ItemMst itemMst, UnitMst untiMst) {

		ArrayList items = new ArrayList();
		items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());
		Double chkQty = 0.0;
		String itemId = null;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			List element = (List) itr.next();
			chkQty = (Double) element.get(4);
			itemId = (String) element.get(7);
		}

		Double itemsqty = 0.0;
		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
				itemMst.getId(), txtBatch.getText());
		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
		if (saleListItemTable.size() > 1) {
			Double PrevQty = 0.0;
			for (SalesDtl saleDtl : saleListItemTable) {
				if (!saleDtl.getUnitId().equalsIgnoreCase(itemMst.getUnitId())) {
					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(), itemMst.getUnitId(),
							saleDtl.getQty());
				} else {
					PrevQty = saleDtl.getQty();
				}
				itemsqty = itemsqty + PrevQty;
			}
		} else {
			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemMst.getId(), txtBatch.getText());
		}
		if (!untiMst.getId().equalsIgnoreCase(itemMst.getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(itemMst.getId(), untiMst.getId(), itemMst.getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if ((chkQty < itemsqty + conversionQty) && !SystemSetting.isNEGATIVEBILLING()) {
				notifyMessage(1, "Not in Stock!!!");
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				cmbUnit.getSelectionModel().clearSelection();
				return false;
			}

		} else if ((chkQty < itemsqty + Double.parseDouble(txtQty.getText())) && !SystemSetting.isNEGATIVEBILLING()) {
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();

			cmbUnit.getSelectionModel().clearSelection();
			notifyMessage(1, "No Stock!!");
			return false;
		}

		return true;
	}

	private boolean CheckForValidLoginDate() {

		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.applicationDate);
		if (null != dayEndClosureHdr) {
			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return false;
				}
			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.applicationDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return false;
		}

		return true;
	}

	private SalesTransHdr crteateSalesTranshdr() {

		salesTransHdr = new SalesTransHdr();

		/*
		 * new url for getting account heads instead of customer mst=========05/0/2022
		 */
//		ResponseEntity<CustomerMst> customerMstResponse = RestCaller.getCustomerByNameAndBarchCode("POS",
//				SystemSetting.systemBranch);
		ResponseEntity<AccountHeads> accountHeadsResponse=RestCaller.getAccountHeadsByNameAndBranchCode("POS",SystemSetting.systemBranch);
				AccountHeads accountHeads = accountHeadsResponse.getBody();

		salesTransHdr.setAccountHeads(accountHeads);

		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.setUserId(SystemSetting.getUserId());
		salesTransHdr.setBranchCode(SystemSetting.systemBranch);
		salesTransHdr.getId();
		salesTransHdr.setCustomerId(accountHeads.getId());
		salesTransHdr.setSalesMode("POS");
		salesTransHdr.setVoucherType("SALESTYPE-" + cmbSalesMode.getSelectionModel().getSelectedItem());
		salesTransHdr.setCreditOrCash("CASH");
		salesTransHdr.setIsBranchSales("N");
		String financialYear = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-");
		String takeOrderNumber = RestCaller.getVoucherNumber(financialYear);
		salesTransHdr.setTakeOrderNumber(takeOrderNumber);
		salesTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
		ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
		salesTransHdr = respentity.getBody();

		return salesTransHdr;

	}

	private void clearFields() {

		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");
		txtBatch.setText("");
		txtBarcode.requestFocus();
		cmbUnit.getSelectionModel().clearSelection();
		txtQty.setText("");
		txtRate.setText("");
	}

	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		columnCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());

		if (null != summary.getTotalAmount()) {

			salesTransHdr.setInvoiceAmount(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			txtCashtopay.setText(bdCashToPay.toPlainString());

		} else {
			txtCashtopay.setText("");
		}
	}

	private void showPopup() {
		try {

			if (SystemSetting.isNEGATIVEBILLING()) {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				Parent root1;
				String cst = null;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

				txtQty.requestFocus();
			} else {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));

				Parent root1;

				String cst = null;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

				txtQty.requestFocus();
			}

			// txtBarcode.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
		ItemMst item = new ItemMst();
		item = getItem.getBody();

		ResponseEntity<UnitMst> getItemUnit = RestCaller.getunitMst(item.getUnitId());
		UnitMst itemUnit = getItemUnit.getBody();

		if (null == txtItemname) {
			return;
		}

		if (null == txtItemname.getScene()) {
			return;
		}
		try {

			Stage stage = (Stage) txtItemname.getScene().getWindow();
			if (stage.isShowing()) {

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						// Update UI here.
						if (null == itemPopupEvent.getItemName()) {
							txtBarcode.requestFocus();
							return;
						}

						if (!multi) {

							cmbUnit.getItems().clear();

							itemNameProperty.set(itemPopupEvent.getItemName());

							txtItemname.setText(itemPopupEvent.getItemName());
							txtBarcode.setText(itemPopupEvent.getBarCode());

							if (null == itemPopupEvent.getBatch()) {
								batchProperty.set("NOBATCH");
							} else {
								batchProperty.set(itemPopupEvent.getBatch());
							}

							txtRate.setText(Double.toString(itemPopupEvent.getMrp()));

							cmbUnit.getItems().add(itemUnit.getUnitName());

							cmbUnit.setValue(itemUnit.getUnitName());

							cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());

							ResponseEntity<TaxMst> taxMst = RestCaller
									.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
							if (null != taxMst.getBody()) {
							}

							if (null != itemPopupEvent.getExpiryDate()) {
								// salesDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
							}

							ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller
									.getMultiUnitByItemId(itemPopupEvent.getItemId());

							multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
							if (!multiUnitList.isEmpty()) {

								for (MultiUnitMst multiUniMst : multiUnitList) {

									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								}
								System.out.println(itemPopupEvent.toString());
							}

						}

						else if (multi) {

							itemNameProperty.set(itemPopupEvent.getItemName());

							txtItemname.setText(itemPopupEvent.getItemName());
							txtBarcode.setText(itemPopupEvent.getBarCode());
							if (null == itemPopupEvent.getBatch()) {
								batchProperty.set("NOBATCH");
							} else {
								batchProperty.set(itemPopupEvent.getBatch());
							}

							txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
							cmbUnit.getItems().add(itemPopupEvent.getUnitName());
							cmbUnit.setValue(itemPopupEvent.getUnitName());
							cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());

							ResponseEntity<TaxMst> taxMst = RestCaller
									.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
							if (null != taxMst.getBody()) {
							}
							if (null != itemPopupEvent.getExpiryDate()) {
								// salesDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
							}

							ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller
									.getMultiUnitByItemId(itemPopupEvent.getItemId());

							multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
							if (!multiUnitList.isEmpty()) {

								for (MultiUnitMst multiUniMst : multiUnitList) {

									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								}
								System.out.println(itemPopupEvent.toString());
							}
							txtQty.setText("1");
							// if(!txtItemname.getText().trim().isEmpty())
							addItem();
							multi = false;
							System.out.println(itemPopupEvent.toString());
						}
					}
				});

			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void showPopupByBarcode() {
		try {
			if (SystemSetting.isNEGATIVEBILLING()) {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				Parent root1;
				String cst = null;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				ItemPopupCtl popctl = fxmlLoader.getController();
				popctl.LoadItemPopupBySearch(txtBarcode.getText());

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} else {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
				// fxmlLoader.setController(itemStockPopupCtl);
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				ItemStockPopupCtl popctl = fxmlLoader.getController();
				popctl.LoadItemPopupByBarcode(txtBarcode.getText());

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {
		showPopup();
	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

		try {
			if (event.getCode() == KeyCode.H && event.isControlDown()) {
				btnHold.requestFocus();

			}

			if (event.getCode() == KeyCode.ESCAPE) {
				tblHold.requestFocus();

			}
			if (event.getCode() == KeyCode.BACK_SPACE && txtBarcode.getText().trim().length() == 0) {

				txtItemname.requestFocus();
				showPopup();
			}
			if (event.getCode() == KeyCode.ENTER) {
				barcodeExec();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (event.getCode() == KeyCode.DOWN) {
			itemDetailTable.requestFocus();
			itemDetailTable.getSelectionModel().selectFirst();

		}

	}

	private void barcodeExec() {
		


		// txtQty.setText("1");

		if (null == txtBarcode) {
			return;

		}
		if (null == txtBarcode.getText() || 
				txtBarcode.getText().trim().isEmpty()) {
			return;

		}
		
		String barcodestring = txtBarcode.getText();
		/*
		 * String subBarcode = null; if (txtBarcode.getText().trim().length() >= 12) {
		 * String barcode1 = txtBarcode.getText(); subBarcode = barcode1.substring(0,
		 * 7); }
		 */

		ResponseEntity<BarcodeBatchMst> barcodeBatchMstResp = RestCaller.getBarcodeBatchMstById(barcodestring);

		BarcodeBatchMst barcodeBatchMst = barcodeBatchMstResp.getBody();

		if (null != barcodeBatchMst) {
			barcodestring = barcodeBatchMst.getBarcode() + "-" + barcodeBatchMst.getBatchCode();
		}

		ArrayList items = new ArrayList();

		items = RestCaller.SearchStockItemByBarcode(barcodestring);

		if (SystemSetting.isNEGATIVEBILLING()) {
			items = RestCaller.ItemSearchByBarcode(barcodestring);
		} else {
			items = RestCaller.SearchStockItemByBarcode(barcodestring);
		}

	


		if (barcodestring.trim().contains("-")) {
			System.out.println("Barcode has -");

			// Following will split the barcode into 2, barcode and batchcode

			String[] barcodearr = barcodestring.split("-", 2);

			// String[] barcodearr = txtBarcode.getText().split("-",2);

			String barcode = barcodearr[0];
			String batch = barcodearr[1];

			batch = batch.trim();

			barcodestring = barcode;

			items = RestCaller.SearchStockItemByBarcodeAndBatch(barcode, batch);

			if (items.size() == 0) {
				if (SystemSetting.isNEGATIVEBILLING()) {
					items = RestCaller.ItemSearchByBarcode(barcode);
				} else {
					items = RestCaller.SearchStockItemByBarcode(barcode);
				}
			}

		}

		if (items.size() > 1) {
			multi = true;

			showPopupByBarcode(barcodestring);

		} else {
			multi = false;

			String itemName = "";
			String barcode = "";
			Double mrp = 0.0;
			String unitname = "";
//			Double qty = 0.0;
//			Double cess = 0.0;
			Double tax = 0.0;
			String itemId = "";
			String unitId = "";

			Iterator itr = items.iterator();
			String batch = "";

			while (itr.hasNext()) {

				List element = (List) itr.next();
				itemName = (String) element.get(0);
				barcode = (String) element.get(1);
				unitname = (String) element.get(2);
				mrp = (Double) element.get(3);
				tax = (Double) element.get(6);
				itemId = (String) element.get(7);
				unitId = (String) element.get(8);
				batch = (String) element.get(9);

				if (null != itemName) {
					salesDtl = new SalesDtl();
					salesDtl.setBarcode(barcode);
					salesDtl.setUnitId(unitId);
					salesDtl.setMrp(mrp);
					salesDtl.setTaxRate(tax);
					salesDtl.setItemId(itemId);
					salesDtl.setBatchCode(batch);
					txtItemname.setText(itemName);
					txtBatch.setText(batch);

					txtRate.setText(Double.toString(mrp));

					cmbUnit.setValue(unitname);

					ResponseEntity<ParamValueConfig> paramval = RestCaller.getParamValueConfig("BARCODE_DEFAULT_QTY");
					ParamValueConfig paramValueConfig = paramval.getBody();

					if (null != paramValueConfig) {

						if (paramValueConfig.getValue().equalsIgnoreCase("YES")) {
							txtQty.setText("1");
							addItem();
						} else {
							txtQty.requestFocus();

						}

					} else {
						txtQty.requestFocus();

					}

//					addItem();

				}

			}
		}

	
	}

	
	private void showPopupByBarcode(String barcode) {
		try {
			if (SystemSetting.isNEGATIVEBILLING()) {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				Parent root1;
				String cst = null;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				ItemPopupCtl popctl = fxmlLoader.getController();
				popctl.LoadItemPopupBySearch(barcode);

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} else {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
				// fxmlLoader.setController(itemStockPopupCtl);
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				ItemStockPopupCtl popctl = fxmlLoader.getController();
				popctl.LoadItemPopupByBarcode(barcode);

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	@FXML
	void qtyKeyRelease(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		} else if (event.getCode() == KeyCode.LEFT) {

			txtBarcode.requestFocus();

		} else if (event.getCode() == KeyCode.BACK_SPACE && txtQty.getText().length() == 0) {
			txtBarcode.requestFocus();
		}

	}

	@FXML
	void save(ActionEvent event) {

		try {
			FinalSave();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void saveOnKey(KeyEvent event) {

	}

	@FXML
	void setCardType(MouseEvent event) {
		setCardType();
	}

	private void setCardType() {
		cmbCardType.getItems().clear();

		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());

		for (ReceiptModeMst receiptModeMst : receiptModeList) {
			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
				cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
			}
		}
	}

	@FXML
	void tablekeypress(KeyEvent event) {

	}

	Boolean localCustomerExist = false;

	@FXML
	void phoneNumberOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtCustomerPhoneNo.getText().length() > 0) {
				ResponseEntity<LocalCustomerMst> getLocalCustMst = RestCaller
						.getLocalCustomerbyPhone(txtCustomerPhoneNo.getText().trim());
				if (getLocalCustMst.getBody() != null) {
					localCustomerExist = true;
					localCustomerMst = getLocalCustMst.getBody();
					updateSalesTransHdrWithLocalCustomer();
					txtCustomerName.setText(getLocalCustMst.getBody().getLocalcustomerName());
					txtCustomerName.requestFocus();
				} else {
					txtCustomerName.requestFocus();
					localCustomerExist = false;
				}

			}
		}
	}

	private void updateSalesTransHdrWithLocalCustomer() {

		salesTransHdr.setLocalCustomerMst(localCustomerMst);
		RestCaller.updateSalesTransHdrWithLocalCustMst(salesTransHdr);
	}

	@FXML
	void custNameOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (!localCustomerExist) {
				localCustomerMst = new LocalCustomerMst();
				localCustomerMst.setLocalcustomerName(txtCustomerName.getText() + " " + txtCustomerPhoneNo.getText());
				localCustomerMst.setPhoneNo(txtCustomerPhoneNo.getText());
				ResponseEntity<LocalCustomerMst> respentity = RestCaller.saveLocalCustomer(localCustomerMst);
				localCustomerMst = respentity.getBody();
				updateSalesTransHdrWithLocalCustomer();

			}
			localCustomerExist = false;
			String hdrId = salesTransHdr.getId();
			salesDtl = null;
			clearFields();
			txtBarcode.requestFocus();
			txtCustomerName.clear();
			txtCustomerPhoneNo.clear();
			itemDetailTable.getItems().clear();
			saleListItemTable.clear();
			txtCashtopay.clear();
			holdedPOs();
			notifyMessage(3, "Order Saved");
			printKot();
			// Print kot and Print performa
			// PrintPerforma
//		logger.info("KOTPOS ======== print invoice started");
//		try {
//			printingSupport.PrintInvoiceThermalPrinter(hdrId);
//			logger.info("KOTPOS ======== print invoice completed");
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	
		}

	}

	private void checkForLocalCustomer() {
		SalesTransHdr salesTrans = RestCaller.getSalesTransHdr(salesTransHdr.getId());
		if (null == salesTrans.getLocalCustomerMst()) {
			if (txtCustomerPhoneNo.getText().trim().length() > 0) {
				ResponseEntity<LocalCustomerMst> getLocalCustMst = RestCaller
						.getLocalCustomerbyPhone(txtCustomerPhoneNo.getText().trim());
				if (null == getLocalCustMst.getBody()) {
					localCustomerExist = true;
					localCustomerMst = new LocalCustomerMst();
					localCustomerMst.setPhoneNo(txtCustomerPhoneNo.getText());
					localCustomerMst.setLocalcustomerName(txtCustomerName.getText());
					ResponseEntity<LocalCustomerMst> respentity = RestCaller.saveLocalCustomer(localCustomerMst);
					localCustomerMst = respentity.getBody();
					updateSalesTransHdrWithLocalCustomer();
				} else {
					localCustomerMst = getLocalCustMst.getBody();
					updateSalesTransHdrWithLocalCustomer();
				}
			}
		}
	}

	private void printKot() {

		checkForLocalCustomer();

		String hdrid = salesTransHdr.getId();
		salesTransHdr = null;
		ResponseEntity<List<SalesTransHdr>> kotPrint = RestCaller.PrintKotTakeOrder(hdrid);
		clearFields();
		localCustomerExist = false;
		txtBarcode.requestFocus();
		txtCustomerName.clear();
		txtCustomerPhoneNo.clear();
		itemDetailTable.getItems().clear();
		saleListItemTable.clear();
		txtCashtopay.clear();
		holdedPOs();
	}

	@FXML
	void printKot(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			printKot();

			txtBarcode.requestFocus();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			txtBarcode.requestFocus();
			salesTransHdr = null;
			txtBarcode.requestFocus();
		}

	}

	@FXML
	void toPrintChange(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			btnSave.requestFocus();

		} else if (event.getCode() == KeyCode.LEFT) {
			visibleCardSale();
			cmbCardType.requestFocus();

		} else if (event.getCode() == KeyCode.UP) {
			txtBarcode.requestFocus();
		} else if (event.getCode() == KeyCode.BACK_SPACE) {
			if (txtPaidamount.getText().isEmpty()) {
				txtBarcode.requestFocus();
			}
		} else if (event.getCode() == KeyCode.S && event.isControlDown()) {
//			try {
//				FinalSave();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
//		else if (event.getCode() == KeyCode.U && event.isControlDown()) {
//
//			if (SystemSetting.HOLDPROPERTY) {
//				tblHold.setVisible(true);
//				btnUnhold.setVisible(true);
//				holdedPOs();
//			}
//
//		}

	}

	@FXML
	void unHold(ActionEvent event) {
		unHold();
		txtPaidamount.requestFocus();
	}

	private void unHold() {
		txtCustomerName.clear();
		txtCustomerPhoneNo.clear();
		if (salesTransHdr != null) {
			if (salesTransHdr.getId() != null) {
				salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
				if (salesTransHdr.getVoucherType().contains("TAKE AWAY")) {
					cmbSalesMode.getSelectionModel().select("TAKE AWAY");
				} else {
					cmbSalesMode.getSelectionModel().select("HOME DELIVERY");
				}
				txtCustomerName.setText(salesTransHdr.getLocalCustomerMst().getLocalcustomerName());
				LocalCustomerMst lc = salesTransHdr.getLocalCustomerMst();
				System.out.println(lc.getPhoneNo());
				txtCustomerPhoneNo.setText(lc.getPhoneNo());
				ResponseEntity<List<SalesDtl>> respentity = RestCaller.getSalesDtl(salesTransHdr);
				saleListTable = FXCollections.observableArrayList(respentity.getBody());
				FillTable();

				getAllCardAmount();
			}
		}

		txtBarcode.clear();
		txtItemname.clear();
		txtBatch.clear();
		txtBarcode.clear();
		txtRate.clear();
		txtQty.clear();
		txtPaidamount.requestFocus();
	}

	private void getAllCardAmount() {
		tblCardAmountDetails.getItems().clear();
		ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
				.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
		salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

		try {
			cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
		} catch (Exception e) {

			txtcardAmount.setText("");
		}

		filltableCardAmount();
	}

	private void filltableCardAmount() {

		tblCardAmountDetails.setItems(salesReceiptsList);
		clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
		clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

		cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
		txtcardAmount.setText(Double.toString(cardAmount));

	}

	@FXML
	void unholdKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			unHold();
		}
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	public void notifyFailureMessage(int duration, String msg) {

		Image img = new Image("failed.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

	}
}
