package com.maple.mapleclient.controllers;

import java.time.LocalDate;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ServiceInDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ServiceInReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;
import net.sf.jasperreports.engine.JRException;

public class ServiceInReportCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<ServiceInReport> serviceListTable = FXCollections.observableArrayList();


    @FXML
    private DatePicker fromDate;

    @FXML
    private DatePicker toDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private TableView<ServiceInReport> tblServiceIn;

    @FXML
    private TableColumn<ServiceInReport, String> clSerialId;

    @FXML
    private TableColumn<ServiceInReport, LocalDate> clDate;

    @FXML
    private TableColumn<ServiceInReport, String> clCustomer;

    @FXML
    private TableColumn<ServiceInReport, String> clItemName;

    @FXML
    private TableColumn<ServiceInReport, String> clProduct;

    @FXML
    private TableColumn<ServiceInReport, String> clBrand;

    @FXML
    private TableColumn<ServiceInReport, String> clModel;

    @FXML
    private TableColumn<ServiceInReport, Number> clQty;

    @FXML
    private TableColumn<ServiceInReport, String> clCompliant;

    @FXML
    private TableColumn<ServiceInReport, String> clObservation;

    @FXML
    private Button btnPrint;
    @FXML
   	private void initialize() {
   		fromDate = SystemSetting.datePickerFormat(fromDate, "dd/MMM/yyyy");
   		toDate = SystemSetting.datePickerFormat(toDate, "dd/MMM/yyyy");
       }
    @FXML
    void FromDateOnPress(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) 
    	{
    		toDate.requestFocus();
    	}

    }

    @FXML
    void GenerateReport(ActionEvent event) {
    	genereteReport();
    }

    @FXML
    void PrintAction(ActionEvent event) {
    	

		if(null == fromDate.getValue())
		{
    		notifyMessage(5,"Please Select from Date",false);
    		fromDate.requestFocus();
    		return;
		}
		
		if(null == toDate.getValue())
		{
    		notifyMessage(5,"Please Select to Date",false);
    		toDate.requestFocus();
    		return;
		}
		
		Date sdate = SystemSetting.localToUtilDate(fromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
		
		Date edate = SystemSetting.localToUtilDate(toDate.getValue());
		String endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		
		try {
			JasperPdfReportService.ServiceInReport(startDate, endDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

    @FXML
    void ReportOnPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) 
    	{
    		genereteReport();
    	}
    	
    }

    @FXML
    void ToDateOnPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) 
    	{
    		if(null != toDate.getValue())
    		{
    			genereteReport();
    		}
    	}
    }

	private void genereteReport() {
		
		if(null == fromDate.getValue())
		{
    		notifyMessage(5,"Please Select from Date",false);
    		fromDate.requestFocus();
    		return;
		}
		
		if(null == toDate.getValue())
		{
    		notifyMessage(5,"Please Select to Date",false);
    		toDate.requestFocus();
    		return;
		}
		
		Date sdate = SystemSetting.localToUtilDate(fromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
		
		Date edate = SystemSetting.localToUtilDate(toDate.getValue());
		String endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		
		ResponseEntity<List<ServiceInReport>> serviceInList = RestCaller.getServiceInReportBetweenDate(startDate,endDate);
		serviceListTable = FXCollections.observableArrayList(serviceInList.getBody());
		
		FillTable();

	}
	
	
	private void FillTable() {
		tblServiceIn.setItems(serviceListTable);
		clBrand.setCellValueFactory(cellData -> cellData.getValue().getBrandNameProperty());
		clCompliant.setCellValueFactory(cellData -> cellData.getValue().getComplaintsProperty());
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clModel.setCellValueFactory(cellData -> cellData.getValue().getModelProperty());
		clObservation.setCellValueFactory(cellData -> cellData.getValue().getObservationProperty());
		clProduct.setCellValueFactory(cellData -> cellData.getValue().getProductNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clSerialId.setCellValueFactory(cellData -> cellData.getValue().getSerialIdProperty());
		
	}

	public void notifyMessage(int duration, String msg, boolean success) {
		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload(hdrId);
			}


		private void PageReload(String hdrId) {

		}


}
