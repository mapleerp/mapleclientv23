package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemBatchDtl;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockReport;
 

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class FastMovingItemsReportCtl {
	String taskid;
	String processInstanceId;
	
	static final String  windowName = "FAST MOVING ITEMS REPORT";
	
	private ObservableList<StockReport> stockList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private TableView<StockReport> tbItems;

    @FXML
    private TableColumn<StockReport, String> clItemName;

    @FXML
    private TableColumn<StockReport, Number> clQty;

    @FXML
    void actionPrint(ActionEvent event) {
    	LocalDate lfdate = dpFromDate.getValue();
    	Date ufDate = SystemSetting.localToUtilDate(lfdate);
    	String sfDate = SystemSetting.UtilDateToString(ufDate, "yyyy-MM-dd");
    	LocalDate ltdate = dpToDate.getValue();
    	Date utDate = SystemSetting.localToUtilDate(ltdate);
    	String stDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
    	try {
			JasperPdfReportService.fastMovingItems(sfDate, stDate, windowName);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
    
    
    
    private void fillTable()
    {
    	tbItems.setItems(stockList);
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());

    }

    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
 }
    @FXML
    void actionshow(ActionEvent event) {

    	tbItems.getItems().clear();
    	LocalDate lfdate = dpFromDate.getValue();
    	Date ufDate = SystemSetting.localToUtilDate(lfdate);
    	String sfDate = SystemSetting.UtilDateToString(ufDate, "yyyy-MM-dd");
    	LocalDate ltdate = dpToDate.getValue();
    	Date utDate = SystemSetting.localToUtilDate(ltdate);
    	String stDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
    	ResponseEntity<List<StockReport>> stockReport = RestCaller.getFastMovingItems(sfDate,stDate,windowName);
    	stockList= FXCollections.observableArrayList(stockReport.getBody());
    	fillTable();
    
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }


}
