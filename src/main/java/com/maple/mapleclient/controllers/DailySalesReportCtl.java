package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.POSThermalPrintFactory;
import com.maple.maple.util.ExportCustomerSalesToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ReceiptModeReport;
import com.maple.report.entity.TallyImportReport;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import net.sf.jasperreports.engine.JRException;

public class DailySalesReportCtl {
	String taskid;
	String processInstanceId;

	ExportCustomerSalesToExcel exportCustomerSalesToExcel = new ExportCustomerSalesToExcel();
	SalesTransHdr salesTransHdr = null;
	POSThermalPrintABS printingSupport;
	private ObservableList<ReceiptModeReport> ReceiptModeReportList = FXCollections.observableArrayList();

	private ObservableList<DailySalesReportDtl> dailySalesList = FXCollections.observableArrayList();
	private ObservableList<VoucherReprintDtl> dailySalesLists = FXCollections.observableArrayList();
	double totalamount = 0.0;
	String customer = null;
	String voucher = null;
	String vDate = null;
	@FXML
	private Button ok;
	@FXML
	private DatePicker dpdate;

	@FXML
	private DatePicker dpToDate;
	@FXML
	private TableView<VoucherReprintDtl> tbreports;

	@FXML
	private Button btnPrintSalesDtl;
	@FXML
	private Button btnExportToExcel;
	@FXML
	private ComboBox<String> branchselect;

	@FXML
	private TableView<DailySalesReportDtl> tbReport;

	@FXML
	private TableColumn<DailySalesReportDtl, String> clVoucherNo;

	@FXML
	private TableColumn<DailySalesReportDtl, LocalDate> clDate;
	@FXML
	private TableColumn<DailySalesReportDtl, String> clCustomer;

	@FXML
	private TableColumn<DailySalesReportDtl, String> clreceiptmode;
	@FXML
	private TableColumn<DailySalesReportDtl, Number> clamt;

	@FXML
	private TextField txtGrandTotal;
	@FXML
	private Button btnShow;
	@FXML
	private Button btnPrint;

	@FXML
	private TableColumn<VoucherReprintDtl, String> voucherNo;

	@FXML
	private TableColumn<VoucherReprintDtl, String> Customername;

	@FXML
	private TableColumn<VoucherReprintDtl, String> itemName;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> qty;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> clRate;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> taxRate;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> cessRate;

	@FXML
	private TableColumn<VoucherReprintDtl, String> unit;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> amount;

	@FXML
	private Button btnPrintReport;

	@FXML
	private TableView<ReceiptModeReport> tbSalesReceipts;

	@FXML
	private TableColumn<ReceiptModeReport, String> clReceiptMode;

	@FXML
	private TableColumn<ReceiptModeReport, Number> clTotalAmt;

	@FXML
	private void initialize() {
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");
		printingSupport = POSThermalPrintFactory.getPOSThermalPrintABS();
		try {
			printingSupport.setupLogo();

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			printingSupport.setupBottomline(SystemSetting.getPosInvoiceBottomLine());

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		setBranches();

		tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {

					String voucherNumber = newSelection.getVoucherNumber();
					customer = newSelection.getCustomerName();
					voucher = newSelection.getVoucherNumber();
					java.sql.Date uDate = newSelection.getvoucherDate();
					vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

					String branchName = branchselect.getValue();
					ResponseEntity<List<VoucherReprintDtl>> dailysaless = RestCaller
							.getVoucherReprintDtlWithDate(newSelection.getVoucherNumber(), branchName,vDate);
					dailySalesLists = FXCollections.observableArrayList(dailysaless.getBody());
					tbreports.setItems(dailySalesLists);

					FillTable();
				}
			}
		});

	}

	@FXML
	void show(ActionEvent event) {
		
		/*
		 * This report is disabled for HC. 
		 * Need to find a proper solution for disable the report
		 */
		int i=0;
		if(i==0) {
			return;
		}
		
		
		totalamount = 0.0;
		java.util.Date uDate = Date.valueOf(dpdate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		// uDate = SystemSetting.StringToUtilDate(strDate,);
		ResponseEntity<List<DailySalesReportDtl>> dailysales = RestCaller
				.getDailySaleReportAll(branchselect.getSelectionModel().getSelectedItem(), strDate, strDate1);
		dailySalesList = FXCollections.observableArrayList(dailysales.getBody());
		for (DailySalesReportDtl dtl : dailySalesList) {
			if (null != dtl.getAmount()) {
				BigDecimal bdCAmount = new BigDecimal(dtl.getAmount());

				bdCAmount = bdCAmount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
				dtl.setAmount(bdCAmount.doubleValue());
			}
		}
		tbReport.setItems(dailySalesList);
		clamt.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getcustomerNameProperty());
		clreceiptmode.setCellValueFactory(cellData -> cellData.getValue().getreceiptModeProperty());

		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		
		ResponseEntity<Double> totalSales = RestCaller.TodaysTotalSales(strDate,strDate1);
		
		if(null != totalSales.getBody())
		{
			totalamount = totalSales.getBody();
		}


		BigDecimal settoamount = new BigDecimal(totalamount);
		settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtGrandTotal.setText(settoamount.toString());

		ResponseEntity<List<ReceiptModeReport>> getReceiptModelist = RestCaller
				.getReceiptModeReportBetweenDate(SystemSetting.getSystemBranch(), strDate, strDate1);
		ReceiptModeReportList = FXCollections.observableArrayList(getReceiptModelist.getBody());

		for (ReceiptModeReport rec : ReceiptModeReportList) {
			BigDecimal bdamt = new BigDecimal(rec.getAmount());
			bdamt = bdamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			rec.setAmount(bdamt.doubleValue());
		}
		tbSalesReceipts.setItems(ReceiptModeReportList);
		clReceiptMode.setCellValueFactory(cellData -> cellData.getValue().getreceiptModeProperty());
		clTotalAmt.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());

	}

	private void setBranches() {

		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();

		for (BranchMst branchMst : branchMstList) {
			branchselect.getItems().add(branchMst.getBranchCode());

		}

	}

	@FXML
	void PrintReport(ActionEvent event) {
		
		/*
		 * This report is disabled for HC. 
		 * Need to find a proper solution for disable the report
		 */
		int i=0;
		if(i==0) {
			return;
		}
		
		java.util.Date uDate = Date.valueOf(dpdate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		try {
			JasperPdfReportService.SalesReportBetweenDate(branchselect.getSelectionModel().getSelectedItem(), strDate,
					strDate1);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void actionExcelExport(ActionEvent event) {
		
		/*
		 * This report is disabled for HC. 
		 * Need to find a proper solution for disable the report
		 */
		int i=0;
		if(i==0) {
			return;
		}
		
		java.util.Date uDate = Date.valueOf(dpdate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		ArrayList tallyImport = new ArrayList();
		if (branchselect.getSelectionModel().isEmpty()) {
			return;
		}
		tallyImport = RestCaller.getTallyImport(branchselect.getSelectionModel().getSelectedItem(), strDate, strDate1);
		exportCustomerSalesToExcel.exportToExcel(
				"SalesToTally" + branchselect.getSelectionModel().getSelectedItem() + strDate + strDate1 + ".xls",
				tallyImport);

	}

	@FXML
	void onClickOk(ActionEvent event) {

	}

	@FXML
	void reportPrint(ActionEvent event) {
		/*
		 * This report is disabled for HC. 
		 * Need to find a proper solution for disable the report
		 */
		int i=0;
		if(i==0) {
			return;
		}

		if (customer.equalsIgnoreCase("POS") || customer.equalsIgnoreCase("KOT")) {
			salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucher, vDate);
			// salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
			if (null != salesTransHdr) {
				try {
					System.out.println(salesTransHdr.getId());
					printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			try {
				NewJasperPdfReportService.TaxInvoiceReport(voucher, vDate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}

	private void FillTable() {

		amount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Customername.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		voucherNo.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
		qty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		taxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		cessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
		unit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
		itemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

	}

	@FXML
	void actionPrintSalesDtl(ActionEvent event) {
		
		/*
		 * This report is disabled for HC. 
		 * Need to find a proper solution for disable the report
		 */
		int i=0;
		if(i==0) {
			return;
		}
		
		
		
		
		java.util.Date uDate = Date.valueOf(dpdate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		try {
			JasperPdfReportService.DailySalesDtlBetweenDate(branchselect.getSelectionModel().getSelectedItem(), strDate,
					strDate1);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	   @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
