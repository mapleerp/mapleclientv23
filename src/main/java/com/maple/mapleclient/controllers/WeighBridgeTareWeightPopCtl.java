package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.WeighBridgeTareWeight;
import com.maple.mapleclient.entity.WeighBridgeWeights;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.WeighBridgeTareWeightEvent;
import com.maple.mapleclient.events.WeighingMachineFirstWtEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class WeighBridgeTareWeightPopCtl {
	
	String taskid;
	String processInstanceId;
	
	boolean initializedCalled = false;

	WeighBridgeTareWeightEvent weighBridgeTareWeightEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<WeighBridgeTareWeight> popUpItemList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

    @FXML
    private TableView<WeighBridgeTareWeight> tbTareWeight;

    @FXML
    private TableColumn<WeighBridgeTareWeight, String> clVehicleNumber;

    @FXML
    private TableColumn<WeighBridgeTareWeight, Number> clTareWeight;

    @FXML
    private TextField txtVehicleNo;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
    @FXML
  	private void initialize() {
  		if (initializedCalled)
  			return;

  		initializedCalled = true;
  		System.out.println("initializedCalledinitializedCalled");
  		weighBridgeTareWeightEvent = new WeighBridgeTareWeightEvent();
  		eventBus.register(this);
  		btnOk.setDefaultButton(true);

  		btnCancel.setCache(true);
  		
  		txtVehicleNo.textProperty().bindBidirectional(SearchString);
  		LoadItemPopupBySearch("");
  		tbTareWeight.setItems(popUpItemList);
  		clTareWeight.setCellValueFactory(cellData -> cellData.getValue().gettareweightProperty());
  		clVehicleNumber.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());
  		tbTareWeight.getSelectionModel().selectedItemProperty().addListener((obs, oldSelectionss, newSelection) -> {

  		if (newSelection != null) {
  			weighBridgeTareWeightEvent = new WeighBridgeTareWeightEvent();
  			weighBridgeTareWeightEvent.setTareweight(newSelection.getTareweight());
  			weighBridgeTareWeightEvent.setVehicleno(newSelection.getVehicleno());
  		}
  		});
  		SearchString.addListener(new ChangeListener() {

  			@Override
  			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

  				LoadItemPopupBySearch((String) newValue);
  			}
  		});
      }
    @FXML
    void actionCancel(ActionEvent event) {

    	Stage stage = (Stage) btnCancel.getScene().getWindow();
    	weighBridgeTareWeightEvent.setTareweight(null);
		eventBus.post(weighBridgeTareWeightEvent);
		stage.close();
    
    }

    @FXML
    void actionOk(ActionEvent event) {

    	Stage stage = (Stage) btnCancel.getScene().getWindow();
		eventBus.post(weighBridgeTareWeightEvent);
		stage.close();

    
    }

    @FXML
    void onKeyPress(KeyEvent event) {

    	if (event.getCode() == KeyCode.ENTER) {
			eventBus.post(weighBridgeTareWeightEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		} else {
			txtVehicleNo.requestFocus();
		}

		initializedCalled = false;

    

    }

    @FXML
    void onKeyPressText(KeyEvent event) {

    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tbTareWeight.requestFocus();
			tbTareWeight.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			weighBridgeTareWeightEvent.setTareweight(null);
			eventBus.post(weighBridgeTareWeightEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		}
		if(event.getCode() == KeyCode.BACK_SPACE && txtVehicleNo.getText().length()==0)
		{
			weighBridgeTareWeightEvent.setTareweight(null);
			eventBus.post(weighBridgeTareWeightEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		}
    
    }
    private void LoadItemPopupBySearch(String searchData)
    {
    	ResponseEntity<List<WeighBridgeTareWeight>> getAllWeights = RestCaller.searchTareWeightByVehicleNo(searchData);
    	popUpItemList = FXCollections.observableArrayList(getAllWeights.getBody());
    	tbTareWeight.setItems(popUpItemList);
    }
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}
  
}
