package com.maple.mapleclient.controllers;


import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Date;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.POSThermalPrintFactory;
import com.maple.maple.util.ExportCustomerSalesToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ConsumptionReasonMst;

import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ReceiptModeReport;
import com.maple.report.entity.TallyImportReport;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import net.sf.jasperreports.engine.JRException;
public class ReasonWiseConsumptionReportCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<ConsumptionHdrReport> consumptionHdrReportList = FXCollections.observableArrayList();
	private ObservableList<ConsumptionReportDtl> ConsumptionReportDtlList = FXCollections.observableArrayList();
    
	String voucher = null;
	String vDate = null;
	
	@FXML
    private DatePicker dpdate;

    @FXML
    private TextField txtGrandTotal;

    @FXML
    private TableView<ConsumptionHdrReport> tbReport;


    @FXML
    private TableColumn<ConsumptionHdrReport, String> clReason;

    @FXML
    private Button btnShow;

    @FXML
    private TableView<ConsumptionReportDtl> tbreports;


    @FXML
    private TableColumn<ConsumptionReportDtl, String> itemName;

    @FXML
    private TableColumn<ConsumptionReportDtl, Number> qty;

    @FXML
    private TableColumn<ConsumptionReportDtl, Number> clMrp;



    @FXML
    private TableColumn<ConsumptionReportDtl, String> unit;

    @FXML
    private TableColumn<ConsumptionReportDtl, Number> amount;

    @FXML
    private  TableColumn<ConsumptionReportDtl, String>clmnBatch;
    @FXML
    private ComboBox<String> cmbRason;

    @FXML
    private Button ok;

    @FXML
    private Button btnPrint;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnPrintReport;

    @FXML
    private Button btnExportToExcel;
    @FXML
    private TextField txtTotal;
    
    
    @FXML
    void PrintReport(ActionEvent event) {

    }

    @FXML
    void actionExcelExport(ActionEvent event) {

    }

    @FXML
    void onClickOk(ActionEvent event) {

    }

    @FXML
    void reportPrint(ActionEvent event) {

    }

    @FXML
    void show(ActionEvent event) {

    	
    	java.util.Date uDate = Date.valueOf(dpdate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		//uDate = SystemSetting.StringToUtilDate(strDate,);
		
		 
		String branchCode=SystemSetting.getSystemBranch();
    	ResponseEntity<List<ConsumptionHdrReport>> consumptionHdrReport = RestCaller.getConsumptionHdrReport(branchCode, strDate,strDate1);
    	consumptionHdrReportList = FXCollections.observableArrayList(consumptionHdrReport.getBody());
    	
    	tbReport.setItems(consumptionHdrReportList);

    	clReason.setCellValueFactory(cellData -> cellData.getValue().getReasonProperty());

    	ResponseEntity<Double>amountResp=RestCaller.getConsumptionAmount(branchCode, strDate,strDate1);
    	Double total=amountResp.getBody();
    	txtGrandTotal.setText(String.valueOf(total));
    }
    @FXML
  	private void initialize() {
    	dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	setReason();
    	cmbRason.setVisible(false);
    	btnExportToExcel.setVisible(false);
    	btnPrint.setVisible(false);
		  tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	    		if (newSelection != null) {
	    			System.out.println("getSelectionModel");
	    			if (null != newSelection.getReason()) {
	    			
	    			//	String voucherNumber=newSelection.getVoucherNumber();

	    				//voucher = newSelection.getVoucherNumber();
	    			//	String uDate = newSelection.getVoucherDate();
	    	    	//	 vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
	    		    
	    				 String reason=newSelection.getReason();
	    				 ResponseEntity<ConsumptionReasonMst> ConsumptionReasonMstResp=RestCaller.getConsumptionReasonMstId( reason);
	    					ConsumptionReasonMst consumptionReasonMst=	ConsumptionReasonMstResp.getBody();
	    					String reasonId=consumptionReasonMst.getId();
	    					java.util.Date fDate = Date.valueOf(dpdate.getValue());
	    					String strDate = SystemSetting.UtilDateToString(fDate, "yyy-MM-dd");
	    					java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
	    					String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
	    				ResponseEntity<List<ConsumptionReportDtl>> consumptionReportDtlList = RestCaller.getConsumerReprintDtl(reasonId,strDate,strDate1);
	    				ConsumptionReportDtlList = FXCollections.observableArrayList(consumptionReportDtlList.getBody());
	    		    	tbreports.setItems(ConsumptionReportDtlList);
	    		    	List<ConsumptionReportDtl> consumptionReportDtlLists=consumptionReportDtlList.getBody();
	    		    	 Double totalAmount=0.0;
	    		    	for(int i=0;i<consumptionReportDtlLists.size();i++) {
	    		    		
	    		    		
	    		    		Double  amount=consumptionReportDtlLists.get(i).getAmount();
	    		    		totalAmount+=amount;
	    		    	}
	    		    	txtTotal.setText(String.valueOf(totalAmount));
	    		    	FillTable();
	    			}
	    		}
	    	});
    }
    
private void setReason() {
		
		ResponseEntity<List<ConsumptionReasonMst>> consumptionReasonMstRep = RestCaller.getConsumptionReasonMst();
		List<ConsumptionReasonMst> consumptionReasonMstList = new ArrayList<ConsumptionReasonMst>();
		consumptionReasonMstList = consumptionReasonMstRep.getBody();
		
		for(ConsumptionReasonMst consumptionReasonMst : consumptionReasonMstList)
		{
			cmbRason.getItems().add(consumptionReasonMst.getReason());
		
			
		}
		 
	}


private void FillTable()
{
	
	amount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
	qty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
	itemName.setCellValueFactory(cellData -> cellData.getValue().getItemIdProperty());
	clmnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
	unit.setCellValueFactory(cellData -> cellData.getValue().getUnitIdProperty());

	
	
}
@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


private void PageReload() {
	
}
}
