package com.maple.mapleclient.controllers;



import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.PharmacyPurchaseReport;
import com.maple.mapleclient.entity.PharmacySalesDetailReport;
import com.maple.mapleclient.entity.PharmacySalesReturnReport;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
	
public class PharmacyPurchaseReportCtl {
	String taskid;
	String processInstanceId;

	private EventBus eventBus = EventBusFactory.getEventBus();
	
	private ObservableList<PharmacyPurchaseReport> pharmacyPurchaseReportList = FXCollections.observableArrayList();
	   @FXML
	    private Button btnShowReport;

	    @FXML
	    private Button btPrintReport;

	    @FXML
	    private Button btnAdd;

	    @FXML
	    private ListView<String> lstItem;
        Double totalamount=0.0;
	    @FXML
	    private Button btnClear;

	    @FXML
	    private DatePicker dpFromDate;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private ComboBox<String> cmbBranch;

	    @FXML
	    private TextField txtCategoryName;

	    @FXML
	    private TableView<PharmacyPurchaseReport> tblPharmacyPurchase;

	    @FXML
	    private TableColumn<PharmacyPurchaseReport, String> clTrEntryDate;

	    @FXML
	    private TableColumn<PharmacyPurchaseReport, String> clSupplierName;

	    @FXML
	    private TableColumn<PharmacyPurchaseReport, String> clOurVoucherDate;

	    @FXML
	    private TableColumn<PharmacyPurchaseReport, String> clSupplierVoucherDate;

	    @FXML
	    private TableColumn<PharmacyPurchaseReport, String> clTinNo;

	    @FXML
	    private TableColumn<PharmacyPurchaseReport, Number> clNetInvoiceTotal;

	    @FXML
	    private TextField txtTotalAmount;

	    @FXML
	    void AddItem(ActionEvent event) {


	    	lstItem.getItems().add(txtCategoryName.getText());
	    	lstItem.getSelectionModel().select(txtCategoryName.getText());
	    	txtCategoryName.clear();
	    	
	    
	    }

	    @FXML
	    void clear(ActionEvent event) {
	    	clear();

	    }

	    @FXML
	    void printReport(ActionEvent event) {
	    	

	    	if(null==dpFromDate.getValue()) {
	    		notifyMessage(5, "select from date",false);
	    		return;
	    	}
	    	if(null==dpToDate.getValue()) {
	    		notifyMessage(5, "select to date",false);
	    		return;
	    	}
	    	if(null==cmbBranch.getValue()) {
	    		notifyMessage(5, "select Branch",false);
	    		return;
	    	}
	    	if(null==lstItem.getSelectionModel().getSelectedItem()) {
	    		notifyMessage(5, "plz add Grouph",false);
	    		return;
	    	}
	    	
	    	java.util.Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
			String fromdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
			
	    	
				java.util.Date utodate = SystemSetting.localToUtilDate(dpToDate.getValue());
				String todate = SystemSetting.UtilDateToString(utodate, "yyyy-MM-dd");
				
				  List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
			        
			    	String cat="";
			    	for(String s:selectedItems)
			    	{
			    		s = s.concat(";");
			    		cat= cat.concat(s);
			    	} 
			
		    	try {
					
		    		JasperPdfReportService.PharmacyPurchaseReport (fromdate, todate,cmbBranch.getSelectionModel().getSelectedItem(),cat);

				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	

	    }

	    @FXML
	    void showReport(ActionEvent event) {

	    	 totalamount=0.0;

	    	if(null==dpFromDate.getValue()) {
	    		notifyMessage(5, "select from date",false);
	    		return;
	    	}
	    	if(null==dpToDate.getValue()) {
	    		notifyMessage(5, "select to date",false);
	    		return;
	    	}
	    	if(null==cmbBranch.getValue()) {
	    		notifyMessage(5, "select Branch",false);
	    		return;
	    	}
	    	if(null==lstItem.getSelectionModel().getSelectedItem()) {
	    		notifyMessage(5, "plz add Grouph",false);
	    		return;
	    	}
	    	
	  List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
	        
	    	String cat="";
	    	for(String s:selectedItems)
	    	{
	    		s = s.concat(";");
	    		cat= cat.concat(s);
	    	} 
	    	 java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
				String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
				java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
				String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
				
				
	    ResponseEntity<List<PharmacyPurchaseReport>> resp=RestCaller.
	    		getPharmacyPurchaseReport(startDate,endDate,cmbBranch.getSelectionModel().getSelectedItem(),cat);
	   
	    pharmacyPurchaseReportList = FXCollections.observableArrayList(resp.getBody());
	    tblPharmacyPurchase.setItems(pharmacyPurchaseReportList);
		fillTable();
//	    	clear();
	    	
	    	for(PharmacyPurchaseReport purchaseReportDtl:pharmacyPurchaseReportList)
	    	{
	    		if(null != purchaseReportDtl.getNetInvoiceTotal())
	    		totalamount = totalamount + purchaseReportDtl.getNetInvoiceTotal();
	    	}
	    	BigDecimal settoamount = new BigDecimal(totalamount);
			settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtTotalAmount.setText(settoamount.toString());

	    }
	
	
	    @FXML
	   	private void initialize() {
	    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
	    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
	    	setBranches();
	    	eventBus.register(this);
	    	 lstItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	    }
	    

		 private void loadCategoryPopup() {
				
				try {
					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
					Parent root1;
					root1 = (Parent) fxmlLoader.load();
					Stage stage = new Stage();
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.initStyle(StageStyle.UNDECORATED);
					stage.setTitle("ABC");
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.setScene(new Scene(root1));
					stage.show();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		  @Subscribe
			public void popupOrglistner(CategoryEvent CategoryEvent) {

				Stage stage = (Stage) btPrintReport.getScene().getWindow();
				if (stage.isShowing()) {

					
					txtCategoryName.setText(CategoryEvent.getCategoryName());
			
				

				}
		  }
		  
		  public void clear() {
			  
			  
			  dpFromDate.setValue(null);
			  dpToDate.setValue(null);
			  cmbBranch.setValue(null);
			  lstItem.getItems().clear();
			  txtTotalAmount.clear();
			  totalamount=0.0;
			  tblPharmacyPurchase.getItems().clear();
			  
			  
		  }
	    
		private void setBranches() {
			
			ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
			List<BranchMst> branchMstList = new ArrayList<BranchMst>();
			branchMstList = branchMstRep.getBody();
			
			for(BranchMst branchMst : branchMstList)
			{
				cmbBranch.getItems().add(branchMst.getBranchCode());
			
				
			}
			 
		}
		
		 @FXML
		    void onEnterGrop(ActionEvent event) {

			  loadCategoryPopup();
		    }
		  public void notifyMessage(int duration, String msg, boolean success) {

				Image img;
				if (success) {
					img = new Image("done.png");

				} else {
					img = new Image("failed.png");
				}

				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
						.onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();
				notificationBuilder.show();

			}
	
		  public void fillTable() {
			  
			  
			  clTrEntryDate.setCellValueFactory(cellData -> cellData.getValue().getTrEntryDateProperty());
			  clSupplierName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
			  
			  clTinNo.setCellValueFactory(cellData -> cellData.getValue().getTinNoProperty());
			  
			  clNetInvoiceTotal.setCellValueFactory(cellData -> cellData.getValue().getNetInvoiceTotalProperty());
			  clSupplierVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getSupplierVoucherDateProperty());
			  
			  clOurVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getOurVoucherDateProperty());
		  }
		  @Subscribe
		  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		  		//Stage stage = (Stage) btnClear.getScene().getWindow();
		  		//if (stage.isShowing()) {
		  			taskid = taskWindowDataEvent.getId();
		  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
		  			
		  		 
		  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
		  			System.out.println("Business Process ID = " + hdrId);
		  			
		  			 PageReload();
		  		}


		  private void PageReload() {
		  	
		  }

		    
		  
}
