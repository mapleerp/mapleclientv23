package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.OpeningBalanceConfigMst;
import com.maple.mapleclient.entity.OpeningBalanceMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OpeningBalanceConfigCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<OpeningBalanceConfigMst> openingList = FXCollections.observableArrayList();

	EventBus eventBus = EventBusFactory.getEventBus();

	OpeningBalanceConfigMst openingBalanceConfigMst = null;
    @FXML
    private TextField txtAccount;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private TableView<OpeningBalanceConfigMst> tbAccount;

    @FXML
    private TableColumn<OpeningBalanceConfigMst, String> clAccount;
    @FXML
   	private void initialize() {
    	eventBus.register(this);
    	
    	ResponseEntity<List<OpeningBalanceConfigMst>> openingBalanceConfigresp=RestCaller.getAllOpeningBalanceConfigMst();
    	openingList = FXCollections.observableArrayList(openingBalanceConfigresp.getBody());
    	fillTable();

		tbAccount.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					openingBalanceConfigMst = new OpeningBalanceConfigMst();
					openingBalanceConfigMst.setId(newSelection.getId());
				}
			}
		});
    }
    @FXML
    void actionDelete(ActionEvent event) {
    	if(null == openingBalanceConfigMst)
    	{
    		return;
    	}
    	if(null == openingBalanceConfigMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteOpeningBalanceConfigMst(openingBalanceConfigMst.getId());
    	ResponseEntity<List<OpeningBalanceConfigMst>> openingBalanceConfigresp=RestCaller.getAllOpeningBalanceConfigMst();
    	openingList = FXCollections.observableArrayList(openingBalanceConfigresp.getBody());
    	fillTable();
    	txtAccount.clear();
    }

    @FXML
    void actionSave(ActionEvent event) {
    	openingBalanceConfigMst = new OpeningBalanceConfigMst();
    	ResponseEntity<AccountHeads> getAcc = RestCaller.getAccountHeadByName(txtAccount.getText());
    	openingBalanceConfigMst.setAccountId(getAcc.getBody().getId());
    	ResponseEntity<OpeningBalanceConfigMst> respenity = RestCaller.saveOpeningBalanceConfigMst(openingBalanceConfigMst);
    	openingBalanceConfigMst = respenity.getBody();
    	openingList.add(openingBalanceConfigMst);
    	fillTable();
    	txtAccount.clear();

    }
    private void fillTable()
    {
    	for(OpeningBalanceConfigMst open:openingList)
    	{
    		ResponseEntity<AccountHeads> getAccHeads = RestCaller.getAccountById(open.getAccountId());
    		open.setAccountName(getAccHeads.getBody().getAccountName());
    	}
    	tbAccount.setItems(openingList);
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getaccountNameProperty());

    }
    
    @Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountEvent.getAccountName());
		}
	}
    @FXML
    void accountOnEnter(KeyEvent event) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
    
    }
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }

}
