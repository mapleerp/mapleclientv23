package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.GroupMst;
import com.maple.mapleclient.entity.GroupPermissionMst;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.entity.ProcessPermissionMst;
import com.maple.mapleclient.entity.UserGroupMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class UserGroupPermission {
	
	String taskid;
	String processInstanceId;
	
	GroupMst groupMst = null;
	UserGroupMst userGroupMst = null;
	UserMst userMst =null; 
	private ObservableList<UserGroupMst> UserGroupMstList = FXCollections.observableArrayList();

    @FXML
    private Button btnsave;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btndelete;

    @FXML
    private ComboBox<String> cmbGroup;

    @FXML
    private ComboBox<String> cmbUser;

    @FXML
    private Button btnSearch;

    @FXML
    private TableView<UserGroupMst> processtbl;

    @FXML
    private TableColumn<UserGroupMst, String> PC1;

    @FXML
    private TableColumn<UserGroupMst, String> PC2;
    
	@FXML
	private void initialize() {
		
		getGroup();
		getUserName();
		
		processtbl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					userGroupMst = new UserGroupMst();

					userGroupMst.setId(newSelection.getId());

				}
			}
		});
		
	}

    @FXML
    void SearchPermissions(ActionEvent event) {
    	
    	userGroupMst = new UserGroupMst();

		if (null != cmbGroup.getValue()) {

			groupMst = new GroupMst();
			ResponseEntity<GroupMst> respentityGroup = RestCaller.getGroupDtls(cmbGroup.getValue());
			groupMst = respentityGroup.getBody();
			userGroupMst.setGroupId(groupMst.getId());

		}
		if (null != cmbUser.getValue()) {

			userMst = new UserMst();
			ResponseEntity<UserMst> respentity = RestCaller.getUserByName(cmbUser.getValue());
			userMst = respentity.getBody();
			userGroupMst.setUserId(userMst.getId());

		}
		if (null != userGroupMst) {
			if (null != userGroupMst.getGroupId()) {
				System.out.println("====userGroupMst==getGroupId=="+userGroupMst.getGroupId());
    			SearchUserGroupPermissionByGroupId();
			}
			if (null != userGroupMst.getUserId()) {
				System.out.println("====userGroupMst==getUserId=="+userGroupMst.getUserId());
    			SearchUserGroupPermissionByUserId();
    			
			}
		} else {
			notifyMessage(5, " Please select group or user...!!!");
		}


    }

    @FXML
    void ShowAll(ActionEvent event) {
    	
    	ShowAllUserGroupPermission();

    }

    @FXML
    void delete(ActionEvent event) {
    	
    	if(null != userGroupMst)
    	{
    		if(null != userGroupMst.getId())
    		{
    			RestCaller.userGroupPermissionDelete(userGroupMst.getId());
    			ShowAllUserGroupPermission();
				notifyMessage(5, " Successfully Deleted...!!!");
    		}
    	}

    }

    @FXML
    void getGroupNames(MouseEvent event) {
    	getGroup();
		

    }

    @FXML
    void getUserName(MouseEvent event) {
    	
    	getUserName();

    }

    @FXML
    void save(ActionEvent event) {
    	
    	if(null == cmbGroup.getValue())
    	{
    		notifyMessage(5, " Please select group name...!!!");
    	} else if (null == cmbUser.getValue()) {
    		
    		notifyMessage(5, " Please select user name...!!!");
		} else {
			userGroupMst = new UserGroupMst();
			
			groupMst = new GroupMst();
			ResponseEntity<GroupMst> respentityGroup = RestCaller.getGroupDtls(cmbGroup.getValue());
			groupMst = respentityGroup.getBody();
			System.out.println("====groupMst====="+groupMst.getId());
			userGroupMst.setGroupId(groupMst.getId());
			
			userMst = new UserMst();
			ResponseEntity<UserMst> respentity = RestCaller.getUserByName(cmbUser.getValue());
			userMst = respentity.getBody();
			userGroupMst.setUserId(userMst.getId());
			
			processtbl.getItems().clear();
			ResponseEntity<UserGroupMst> respentityPermission = RestCaller
					.saveUserGroupPermissionMst(userGroupMst);
			userGroupMst = respentityPermission.getBody();
			System.out.println("===usergroupPermissionMst====" + userGroupMst);
			UserGroupMstList.add(userGroupMst);
			FillTable();
			userGroupMst = null;
			notifyMessage(5, " Successfully saved...!!!");
			
		}

    }
	private void getGroup() {

		cmbGroup.getItems().clear();
		ArrayList process = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		process = RestCaller.getAllGroup();
		Iterator itr = process.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object groupName = lm.get("groupName");
			Object id = lm.get("id");
			if (id != null) {

				cmbGroup.getItems().add((String) groupName);

			}
		}

	}
	
	private void getUserName() {

		System.out.println("@======getUserName======@");
		cmbUser.getItems().clear();
		ArrayList user = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		user = RestCaller.getAllUsers();
		Iterator itr = user.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			System.out.println("------processPermition-----" + lm);
			Object userName = lm.get("userName");
			Object id = lm.get("id");
			if (id != null) {

				cmbUser.getItems().add((String)userName);
			
			}
		}

	}
	
	private void FillTable() {

		processtbl.setItems(UserGroupMstList);
		for (UserGroupMst p : UserGroupMstList) {
			if (null != p.getId()) {
				groupMst = new GroupMst();
				ResponseEntity<GroupMst> respentity = RestCaller.getGroupNameById(p.getGroupId());
				groupMst = respentity.getBody();
				System.out.println("=====groupMst====="+groupMst);
				if (null != groupMst) {
					System.out.println("===groupMst.getGroupName()====" + groupMst.getGroupName());
					p.setGroupName(groupMst.getGroupName());
					PC1.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
				}
			}
			if (null != p.getUserId()) {
				UserMst user = new UserMst();
				ResponseEntity<UserMst> respentity = RestCaller.getUserNameById(p.getUserId());
				user = respentity.getBody();
				if (null != user) {
					p.setUserName(user.getUserName());
					PC2.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
				}
			}
		}
		clearField();

	}
	private void clearField() {
		cmbGroup.getSelectionModel().clearSelection();
		cmbUser.getSelectionModel().clearSelection();
		
	}
	private void ShowAllUserGroupPermission() {

		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		permission = RestCaller.getAllUserGroupPermissionDtl();

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();

			Object groupId = lm.get("groupId");
			Object userId = lm.get("userId");
			System.out.println("=====lm=====" + lm);

			Object id = lm.get("id");
			if (id != null) {
				UserGroupMst pp = new UserGroupMst();
				pp.setUserId((String) userId);
				pp.setGroupId((String) groupId);
				pp.setId((String) id);

				UserGroupMstList.add(pp);

			}
			
		}
		FillTable();
	}
	
	private void SearchUserGroupPermissionByGroupId() {
		
		
		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		permission = RestCaller.getUserGroupPermissionDtlByGroupId(userGroupMst.getGroupId());

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			System.out.println("=====lm=====" + lm);
			Object groupId = lm.get("groupId");
			Object userId = lm.get("userId");
			

			Object id = lm.get("id");
			if (id != null) {
				UserGroupMst pp = new UserGroupMst();
				pp.setUserId((String) userId);
				pp.setGroupId((String) groupId);
				pp.setId((String)id);

				UserGroupMstList.add(pp);

			}
			FillTable();
		}

	}
	
	private void SearchUserGroupPermissionByUserId() {
		
		
		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		permission = RestCaller.getUserGroupPermissionDtlByUserId(userGroupMst.getUserId());

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			System.out.println("=====lm=====" + lm);
			Object groupId = lm.get("groupId");
			Object userId = lm.get("userId");
			

			Object id = lm.get("id");
			if (id != null) {
				UserGroupMst pp = new UserGroupMst();
				pp.setUserId((String) userId);
				pp.setGroupId((String) groupId);
				pp.setId((String)id);

				UserGroupMstList.add(pp);

			}
			FillTable();
		}

	}
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	
	   @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
