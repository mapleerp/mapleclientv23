//package com.maple.mapleclient.controllers;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//import org.controlsfx.control.Notifications;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.client.RestTemplate;
//
//import com.google.common.eventbus.EventBus;
//import com.google.common.eventbus.Subscribe;
//import com.maple.maple.util.SystemSetting;
//import com.maple.mapleclient.EventBusFactory;
//import com.maple.mapleclient.entity.AccountHeads;
//import com.maple.mapleclient.entity.CurrencyMst;
//import com.maple.mapleclient.entity.PriceDefenitionMst;
//import com.maple.mapleclient.events.TaskWindowDataEvent;
//import com.maple.mapleclient.restService.RestCaller;
//
//import javafx.beans.property.SimpleStringProperty;
//import javafx.beans.property.StringProperty;
//import javafx.beans.value.ChangeListener;
//import javafx.beans.value.ObservableValue;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.fxml.FXML;
//import javafx.geometry.Pos;
//import javafx.scene.control.Button;
//import javafx.scene.control.ComboBox;
//import javafx.scene.control.Label;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.KeyEvent;
//import javafx.util.Duration;
//
//public class PharmacyCustomerCreationCtl {
//	String taskid;
//	String processInstanceId;
//
//
//	EventBus eventBus = EventBusFactory.getEventBus();
//
//	String pricetype;
//	private ObservableList<AccountHeads> customerRegistrationList = FXCollections.observableArrayList();
//
//	AccountHeads customerregistration = null;
//	StringProperty SearchString = new SimpleStringProperty();
//
//    @FXML
//    private TextField txtCustDiscount;
//	@FXML
//	private TextField custName;
//    @FXML
//    private ComboBox<String> cmbCountry;
//    @FXML
//    private ComboBox<String> cmbDiscountProperty;
//	@FXML
//	private TextField custAddress1;
//	@FXML
//	private ComboBox<String> pricecombo;
//
//	@FXML
//	private TextField custContact;
//
//	@FXML
//	private TextField custEmail;
//    @FXML
//    private TextField txtDrugLicenseNo;
//	@FXML
//	private TextField txtCreditPeriod;
//
//	@FXML
//	private TextField custGst;
//	@FXML
//	private ComboBox<String> cmbGroup;
//	@FXML
//	private Button custbtn;
//
//    @FXML
//    private ComboBox<String> cmbCurrency;
//	
//	@FXML 
//	private TextField txtBankName;
//	@FXML
//	private TextField txtBankAccountName;
//	@FXML
//	private TextField txtBankIfsc;
//	@FXML
//	private Button btnBankDtl;
//	@FXML
//	private Button btnsearch;
//
//    @FXML
//    private ComboBox<String> cmbCustomerCountry;
//
//    @FXML
//    private Label lblCustomerCountry;
//	@FXML
//	private TableView<AccountHeads> tblcust;
//
//	@FXML
//	private TableColumn<AccountHeads, String> CR1Name;
//
//	@FXML
//	private TableColumn<AccountHeads, String> CR2ADDRESS;
//
//	@FXML
//	private TableColumn<AccountHeads, String> CR3Contact;
//
//	@FXML
//	private TableColumn<AccountHeads, String> CR4Email;
//
//	@FXML
//	private TableColumn<AccountHeads, String> CR5GSt;
//
//	@FXML
//	private TableColumn<AccountHeads, String> clState;
//
//	@FXML
//	private ComboBox<String> cmbCustState;
//	@FXML
//	private Button btnClear;
//	
//    @FXML
//    private Label lblBankName;
//    
//    
//    @FXML
//    private Label lblBankAccountName;
//
//    @FXML
//    private Label lblBankIfsc;
//
//	@FXML
//	void Clearfields(ActionEvent event) {
//
//		clear();
//		customerregistration = null;
//	}
//
//	
//	@FXML
//	void bankDtls(ActionEvent event) {
//		
//		txtBankName.setVisible(true);
//		txtBankAccountName.setVisible(true);
//		txtBankIfsc.setVisible(true);
//		lblBankName.setVisible(true);
//		lblBankIfsc.setVisible(true);
//		lblBankAccountName.setVisible(true);
//		lblCustomerCountry.setVisible(true);
//		cmbCustomerCountry.setVisible(true);
//	}
//	@FXML
//	private void initialize() {
//		ResponseEntity<List<String>> countryList = RestCaller.getCountryList();
//    	for(int i=0;i<countryList.getBody().size();i++)
//    	{
//    		cmbCountry.getItems().add(countryList.getBody().get(i));
//    	}
//		
//    	cmbCountry.valueProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				cmbCustState.getItems().clear();
//				if(!newValue.equalsIgnoreCase(null))
//				{
//					ResponseEntity<List<String>> stateList = RestCaller.getStateListByCountry(cmbCountry.getSelectionModel().getSelectedItem());
//			    	for(int i = 0;i<stateList.getBody().size();i++)
//			    	{
//			    		cmbCustState.getItems().add(stateList.getBody().get(i));
//			    	}
//				}
//			}
//
//		});
//		
//		eventBus.register(this);
//		showAccountHeadGroup();
//		
//		
//		try {
//		lblBankName.setVisible(false);
//		lblBankIfsc.setVisible(false);
//		txtBankAccountName.setVisible(false);
//		txtBankName.setVisible(false);
//		lblBankAccountName.setVisible(false);
//		txtBankIfsc.setVisible(false);
//		lblCustomerCountry.setVisible(false);
//		cmbCustomerCountry.setVisible(false);
//		}catch(Exception e) {
//			System.out.println(e.toString());
//		}
//		
//		System.out.println("--LoadCustomerBySearch--initialize");
////		cmbCustState.getItems().add("KERALA");
////		cmbCustState.getItems().add("ANDHRA PRADESH");
////		cmbCustState.getItems().add("ARUNACHAL PRADESH");
////		cmbCustState.getItems().add("ASSAM");
////		cmbCustState.getItems().add("BIHAR");
////		cmbCustState.getItems().add("CHHATTISGARH");
////		cmbCustState.getItems().add("GOA");
////		cmbCustState.getItems().add("GUJARAT");
////		cmbCustState.getItems().add("HARYANA");
////		cmbCustState.getItems().add("HIMACHAL PRADESH");
////		cmbCustState.getItems().add("JAMMU AND  KASHMIR");
////		cmbCustState.getItems().add("JHARKHAND");
////		cmbCustState.getItems().add("KARNATAKA");
////		cmbCustState.getItems().add("MADHYA PRADESH");
////		cmbCustState.getItems().add("MAHARASHTRA");
////		cmbCustState.getItems().add("MANIPUR");
////		cmbCustState.getItems().add("MEGHALAYA");
////		cmbCustState.getItems().add("MIZORAM");
////		cmbCustState.getItems().add("NAGALAND");
////		cmbCustState.getItems().add("ODISHA");
////		cmbCustState.getItems().add("PUNJAB");
////		cmbCustState.getItems().add("RAJASTHAN");
////		cmbCustState.getItems().add("TAMIL NADU");
////		cmbCustState.getItems().add("TELANGANA");
////		cmbCustState.getItems().add("TRIPURA");
////		cmbCustState.getItems().add("UTTAR PRADESH");
////		cmbCustState.getItems().add("UTTARAKHAND");
////		cmbCustState.getItems().add("WEST BENGAL");
//       // customer country
//		cmbCustomerCountry.getItems().add("INDIA");
//		cmbCustomerCountry.getItems().add("PAKISTAN");
//		cmbCustomerCountry.getItems().add("BANGLADESH");
//		cmbCustomerCountry.getItems().add("SREELANKA");
//		cmbCustomerCountry.getItems().add("CHINA");
//		cmbCustomerCountry.getItems().add("NEPAL");
//		cmbCustomerCountry.getItems().add("IRAN");
//		cmbCustomerCountry.getItems().add("AFGANISTHAN");
//		
//		// custName.textProperty().bindBidirectional(SearchString);
//
//		customerregistration = new AccountHeads();
//		custName.textProperty().bindBidirectional(SearchString);
//
//		cmbDiscountProperty.getItems().add("ON BASIS OF BASE PRICE");
//		cmbDiscountProperty.getItems().add("ON BASIS OF MRP");
//		cmbDiscountProperty.getItems().add("ON BASIS OF DISCOUNT INCLUDING TAX");
//		
//		
//	
//		cmbPriceDefenition();
//
//		ResponseEntity<List<CurrencyMst>> currencyMst = RestCaller.getallCurrencyMst();
//		for(int i=0;i<currencyMst.getBody().size();i++)
//		{
//			cmbCurrency.getItems().add(currencyMst.getBody().get(i).getCurrencyName());
//		}
//		/*
//		 * custName.textProperty().bindBidirectional(customerRegistrationList.get(0).
//		 * getCutomerNameProperty());
//		 * 
//		 * custAddress1.textProperty().bindBidirectional(customerRegistrationList.get(0)
//		 * .getCustomerAddressProperty());
//		 * 
//		 * custContact.textProperty().bindBidirectional(customerRegistrationList.get(0).
//		 * getCustomerContactProperty());
//		 * 
//		 * custEmail.textProperty().bindBidirectional(customerRegistrationList.get(0).
//		 * getCustomerMailProperty());
//		 * 
//		 * custGst.textProperty().bindBidirectional(customerRegistrationList.get(0).
//		 * getCustomerGstProperty());
//		 * 
//		 * tblcust.setItems(customerRegistrationList);
//		 */
//
//		// ------------------------table binding
//
//		SearchString.addListener(new ChangeListener() {
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//				System.out.println("item Search changed");
//				LoadCustomerBySearch((String) newValue);
//			}
//		});
//		
//		txtCustDiscount.textProperty().addListener(new ChangeListener<String>() {
//
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
//					txtCustDiscount.setText(oldValue);
//				}
//			}
//		});
//		
//		
//		tblcust.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//			if (newSelection != null) {
//				if (null != newSelection.getId()) {
//					pricecombo.setPromptText("");
//					cmbGroup.setPromptText("");
//					customerregistration = new AccountHeads();
//					ResponseEntity<AccountHeads> getcust = RestCaller.getAccountHeadsById(newSelection.getId());
//					customerregistration = getcust.getBody();
//					ResponseEntity<PriceDefenitionMst> priceDef = RestCaller
//							.getPriceNameById(getcust.getBody().getPriceTypeId());
//					txtCreditPeriod.setText(Integer.toString(newSelection.getCreditPeriod()));
//					custName.setText(newSelection.getAccountName());
//					custAddress1.setText(newSelection.getPartyAddress1());
//					custContact.setText(newSelection.getCustomerContact());
//					custEmail.setText(newSelection.getPartyMail());
//					custGst.setText(newSelection.getPartyGst());
//					txtCustDiscount.setText(Double.toString(newSelection.getCustomerDiscount()));
//					txtDrugLicenseNo.setText(customerregistration.getDrugLicenseNumber());
////					cmbCurrency.setValue(customerregistration.getcu);
//					if(null != customerregistration.getCurrencyId())
//					{
//					ResponseEntity<CurrencyMst> getCurrencyById = RestCaller.getcurrencyMsyById(customerregistration.getCurrencyId());
//					if(null != getCurrencyById.getBody())
//					{
//						cmbCurrency.getSelectionModel().select(getCurrencyById.getBody().getCurrencyName());
//					}
//					}
//					if (null != priceDef.getBody()) {
//						pricecombo.setPromptText(priceDef.getBody().getPriceLevelName());
//						pricecombo.setValue(priceDef.getBody().getPriceLevelName());
//					} else
//						pricecombo.getSelectionModel().clearSelection();
//
//					cmbCustState.setPromptText(newSelection.getCustomerState());
//					cmbGroup.setPromptText(getcust.getBody().getCustomerGroup());
//					cmbGroup.setValue(getcust.getBody().getCustomerGroup());
//					cmbDiscountProperty.setPromptText(getcust.getBody().getDiscountProperty());
//					cmbDiscountProperty.setValue(getcust.getBody().getDiscountProperty());
//					cmbCountry.getSelectionModel().select(newSelection.getCustomerCountry());
//				}
//			}
//		});
//
//	}
//
//	private void cmbPriceDefenition() {
//
//		pricecombo.getItems().clear();
//
//		System.out.println("=====PriceTypeOnKEyPress===");
//		ArrayList priceType = new ArrayList();
//
//		priceType = RestCaller.getPriceDefinition();
//		Iterator itr = priceType.iterator();
//		while (itr.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr.next();
//			Object priceLevelName = lm.get("priceLevelName");
//			Object id = lm.get("id");
//			if (id != null) {
//				pricecombo.getItems().add((String) priceLevelName);
//
//			}
//
//		}
//
//	}
//
//	@FXML
//	void custAddressOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			custContact.requestFocus();
//		}
//	}
//
//	@FXML
//	void nameOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			custAddress1.requestFocus();
//		}
//	}
//
//	@FXML
//	void custContactOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			custEmail.requestFocus();
//		}
//	}
//
//	@FXML
//	void custEmailOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			custGst.requestFocus();
//		}
//	}
//
//	@FXML
//	void custStateOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//		//	txtCreditPeriod.requestFocus();
//		}
//	}
//	
//
//	@FXML
//	void custCountryOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			txtCreditPeriod.requestFocus();
//		}
//	}
//
//	@FXML
//	void creditPeriodOnPress(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			pricecombo.requestFocus();
//		}
//	}
//
//	@FXML
//	void gstOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			cmbCustState.requestFocus();
//		}
//	}
//
//	@FXML
//	void submitOnEnter(KeyEvent event) {
//
//		if (event.getCode() == KeyCode.ENTER) {
//			submit();
//		}
//	}
//
//	@FXML
//	void Submit(ActionEvent event) {
//		submit();
//
//	}
//
//	@FXML
//	void priceTypeOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			cmbGroup.requestFocus();
//		}
//	}
//	@FXML
//    void drugLicenseOnEnter(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			custbtn.requestFocus();
//		}
//    }
//
//    @FXML
//    void discountOnEnter(KeyEvent event) {
//    	if (event.getCode() == KeyCode.ENTER) {
//			txtDrugLicenseNo.requestFocus();
//		}
//    }
//	@FXML
//	void groupOnPress(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			txtCustDiscount.requestFocus();
//		}
//	}
//
//    @FXML
//    void custDiscountKeyPress(KeyEvent event) {
//    	if (event.getCode() == KeyCode.ENTER) {
//			custbtn.requestFocus();
//		}
//    }
//	public void clear() {
//		txtDrugLicenseNo.clear();
//		custAddress1.setText("");
//		custContact.setText("");
//		custEmail.setText("");
//		custGst.setText("");
//		custName.setText("");
//
//		txtCustDiscount.clear();
////		tblcust.getItems().clear();
//		txtBankName.clear();
//		txtBankIfsc.clear();
//		txtBankAccountName.clear();
//		txtCreditPeriod.clear();
//		cmbCustState.getSelectionModel().clearSelection();
//		cmbCustState.setPromptText("");
//		cmbDiscountProperty.getSelectionModel().clearSelection();
//		cmbDiscountProperty.setPromptText("");
//		pricecombo.getSelectionModel().clearSelection();
//		pricecombo.setPromptText("");
//		cmbGroup.getSelectionModel().clearSelection();
//		cmbGroup.setPromptText("");
//		customerregistration = new AccountHeads();
//		cmbCurrency.getSelectionModel().clearSelection();
//		cmbCountry.getSelectionModel().clearSelection();
////	    	tblcust.getItems().clear();
//	}
//
//	public void filltable() {
//
//		CR1Name.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
//		CR2ADDRESS.setCellValueFactory(cellData -> cellData.getValue().getCustomerAddressProperty());
//		CR3Contact.setCellValueFactory(cellData -> cellData.getValue().getCustomerContactProperty());
//		CR4Email.setCellValueFactory(cellData -> cellData.getValue().getCustomerMailProperty());
//		CR5GSt.setCellValueFactory(cellData -> cellData.getValue().getCustomerGstProperty());
//		clState.setCellValueFactory(cellData -> cellData.getValue().getStateProperty());
//	}
//
//	public void notifyMessage(int duration, String msg) {
//		System.out.println("OK Event Receid");
//
//		Image img = new Image("done.png");
//		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
//				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
//				.onAction(new EventHandler<ActionEvent>() {
//					@Override
//					public void handle(ActionEvent event) {
//						System.out.println("clicked on notification");
//					}
//				});
//		notificationBuilder.darkStyle();
//		notificationBuilder.show();
//	}
//
//	@FXML
//	void Search(ActionEvent event) {
//		tblcust.getItems().clear();
//		LoadCustomerBySearch("");
//	}
//
//	private void LoadCustomerBySearch(String searchData) {
//
//		tblcust.getItems().clear();
//		ArrayList cat = new ArrayList();
//		RestTemplate restTemplate = new RestTemplate();
////		searchData = "cust";
//		cat = RestCaller.SearchPartyByName(searchData);
//		Iterator itr = cat.iterator();
//		while (itr.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr.next();
//			Object cutomerName = lm.get("customerName");
//			Object customerCountry = lm.get("customerCountry");
//			Object customerAddress = lm.get("customerAddress");
//			Object customerContact = lm.get("customerContact");
//			Object customerMail = lm.get("customerMail");
//			Object customerGst = lm.get("customerGst");
//			Object id = lm.get("id");
//			Object state = lm.get("customerState");
//			Object customerDiscount =  lm.get("customerDiscount");
//			Object discountProperty = lm.get("discountProperty");
//			if (id != null) {
//				AccountHeads customer = new AccountHeads();
//
//				
//				try {
//				customer.setPartyAddress1((String) customerAddress);
//				customer.setCustomerContact((String) customerContact);
//				customer.setPartyGst((String) customerGst);
//				customer.setCustomerMail((String) customerMail);
//				customer.setCustomerName((String) cutomerName);
//				customer.setId((String) id);
//				customer.setCustomerState((String) state);
//				customer.setCustomerDiscount((Double)customerDiscount);
//				customer.setDiscountProperty((String)discountProperty);
//				customer.setCustomerCountry((String)customerCountry);
//				customerRegistrationList.add(customer);
//				tblcust.setItems(customerRegistrationList);
//				}catch(Exception e) {
//					System.out.println(e.toString());
//				}
//				
//				filltable();
//			}
//		}
//	}
//
//	private void submit() {
//		if (null != customerregistration)
//
//		{
//			if (null != customerregistration.getId()) {
//
//				customerregistration.setCustomerName(custName.getText());
//				customerregistration.setBankAccountName(txtBankAccountName.getText());
//				customerregistration.setBankIfsc(txtBankIfsc.getText());
//				customerregistration.setBankName(txtBankName.getText());
//				customerregistration.setDrugLicenseNumber(txtDrugLicenseNo.getText());
//				if(null == cmbCustomerCountry.getSelectionModel())
//				{
//					customerregistration.setCustomerCountry("INDIA");
//				}
//				else
//				{
//				customerregistration.setCustomerCountry(cmbCustomerCountry.getSelectionModel().getSelectedItem());
//				}
//				customerregistration.setCustomerAddress(custAddress1.getText());
//				customerregistration.setCustomerContact(custContact.getText());
//				customerregistration.setCustomerMail(custEmail.getText());
//				customerregistration.setCustomerGst(custGst.getText());
//				customerregistration.setCustomerGroup(cmbGroup.getSelectionModel().getSelectedItem());
//				customerregistration.setCreditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
//	
//				if(null!=txtBankName.getText()) {
//					customerregistration.setBankName(txtBankName.getText());
//					
//				}
//				if(null!=txtBankAccountName) {
//					customerregistration.setBankAccountName(txtBankAccountName.getText());
//				}
//				if(null!=txtBankIfsc) {
//					customerregistration.setBankIfsc(txtBankIfsc.getText());
//				}
//				if(null!=cmbCustomerCountry.getValue()) {
//					customerregistration.setCustomerCountry(cmbCustomerCountry.getValue());
//				}
//				String pricetype = pricecombo.getValue();
//				ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
//						.getPriceDefenitionByName(pricetype);
//
//				if(priceDefenitionSaved.getBody().size()>0)
//				{
//				customerregistration.setPriceTypeId(priceDefenitionSaved.getBody().get(0).getId());
//				}
//				else
//				{
//					notifyMessage(4,"Select Price Type");
//					return;
//				}
//				if (null == cmbCustState.getValue()) {
//					customerregistration.setCustomerState("KERALA");
//
//				} else {
//
//					customerregistration.setCustomerState(cmbCustState.getValue());
//				}
//
//				if(txtCustDiscount.getText().trim().isEmpty())
//				{
//					customerregistration.setCustomerDiscount(0.0);
//				}
//				else
//				{
//				customerregistration.setCustomerDiscount(Double.parseDouble(txtCustDiscount.getText()));
//				}
//				customerregistration.setDiscountProperty(cmbDiscountProperty.getSelectionModel().getSelectedItem());
//
//				if (custGst.getText().length()>=13) 
//				{
//					customerregistration.setCustomerType("REGULAR");
//				}
//				else
//				{
//					customerregistration.setCustomerType("CONSUMER");
//				}
//				if(null != cmbCurrency && null != cmbCurrency.getSelectionModel().getSelectedItem())
//				{
//				ResponseEntity<CurrencyMst> getcurrency = RestCaller.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem());
//				customerregistration.setCurrencyId(getcurrency.getBody().getId());
//				}
//				customerregistration.setCustomerCountry(cmbCountry.getSelectionModel().getSelectedItem());
//				RestCaller.updateCustomer(customerregistration);
//				notifyMessage(5, "Updated");
//				filltable();
//				clear();
//			}
//		 else {
//			try {
//				if (custName.getText().trim().isEmpty()) {
//					notifyMessage(5, "Please Enter Customer Name...!!!");
//					return;
//				} else if (custAddress1.getText().trim().isEmpty()) {
//					notifyMessage(5, "Please Enter Address...!!!");
//					return;
//				} else if (custContact.getText().trim().isEmpty()) {
//					notifyMessage(5, "Please Enter Contact...!!!");
//					return;
//				} else if (custGst.getText().trim().isEmpty()) {
//					notifyMessage(5, "Please Enter gst...!!!");
//					return;
//				} else if (cmbCustState.getSelectionModel().isEmpty()) {
//					notifyMessage(5, "Please Select State...!!!");
//					cmbCustState.requestFocus();
//					return;
//				} else if (null == pricecombo.getValue()) {
//
//					notifyMessage(5, "Please Select Price type...!!!");
//					pricecombo.requestFocus();
//					return;
//				} else if (txtCreditPeriod.getText().trim().isEmpty()) {
//
//					notifyMessage(5, "Please Select credit period...!!!");
//					return;
//				} else if (!custName.getText().contains(custContact.getText())) {
//
//					notifyMessage(5, "Please add customer 1st phone number to the end of customer name");
//					return;
//				} else {
//					tblcust.getItems().clear();
//					customerregistration = new CustomerMst();
//					// System.out.println("custName.getText()custName.getText()"+custName.getText());
//					String custName1 = custName.getText().trim();
//
//					custName1 = custName1.replaceAll("'", "");
//					custName1 = custName1.replaceAll("&", "");
//
//					custName1 = custName1.replaceAll("/", "");
//					custName1 = custName1.replaceAll("%", "");
//					custName1 = custName1.replaceAll("\"", "");
//					
//					customerregistration.setCustomerName(custName1);
//					String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "CU");
//					customerregistration.setId(vNo + SystemSetting.getUser().getCompanyMst().getId()
//							+ SystemSetting.systemBranch);
//					customerregistration.setRank(0);
//					customerregistration.setCustomerAddress(custAddress1.getText());
//					if(txtDrugLicenseNo.getText().trim().isEmpty())					    {
//							customerregistration.setDrugLicenseNumber(txtDrugLicenseNo.getText());
//
//				    }
//					customerregistration.setCustomerContact(custContact.getText());
//					customerregistration.setCustomerMail(custEmail.getText());
//					customerregistration.setCustomerGst(custGst.getText());
//					customerregistration.setCustomerGroup(cmbGroup.getSelectionModel().getSelectedItem());
//					customerregistration.setCreditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
//					String pricetype = pricecombo.getValue();
//					ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
//							.getPriceDefenitionByName(pricetype);
//
//					customerregistration.setPriceTypeId(priceDefenitionSaved.getBody().get(0).getId());
//					if (null == cmbCustState.getValue()) {
//						customerregistration.setCustomerState("KERALA");
//
//					} else {
//
//						customerregistration.setCustomerState(cmbCustState.getValue());
//					}
//
//					if (custGst.getText().length()>=13) 
//					{
//						customerregistration.setCustomerType("REGULAR");
//					}
//					else
//					{
//						customerregistration.setCustomerType("CONSUMER");
//					}
//					ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadByName(custName.getText());
//					if (null != getAccountHeads.getBody()) {
//						customerregistration = new CustomerMst();
//						notifyMessage(5, "Customer Already Registered");
//						clear();
//						return;
//					}
//					if(txtCustDiscount.getText().trim().isEmpty())
//					{
//						customerregistration.setCustomerDiscount(0.0);
//					}
//					else
//					{
//					customerregistration.setCustomerDiscount(Double.parseDouble(txtCustDiscount.getText()));
//					}
//					customerregistration.setDiscountProperty(cmbDiscountProperty.getSelectionModel().getSelectedItem());
//					if(null!=txtBankName.getText()) {
//						customerregistration.setBankName(txtBankName.getText());
//						
//					}
//					if(null!=txtBankAccountName) {
//						customerregistration.setBankAccountName(txtBankAccountName.getText());
//					}
//					if(null!=txtBankIfsc) {
//						customerregistration.setBankIfsc(txtBankIfsc.getText());
//					}
//					if(null!=cmbCustomerCountry.getValue()) {
//						customerregistration.setCustomerCountry(cmbCustomerCountry.getValue());
//					}
//					customerregistration.setDrugLicenseNumber(txtDrugLicenseNo.getText());
//					if(null != cmbCurrency && null != cmbCurrency.getSelectionModel().getSelectedItem())
//					{
//					ResponseEntity<CurrencyMst> getcurrency = RestCaller.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem());
//					customerregistration.setCurrencyId(getcurrency.getBody().getId());
//					}
//					customerregistration.setCustomerCountry(cmbCountry.getSelectionModel().getSelectedItem());
//
//					ResponseEntity<CustomerMst> respentity = RestCaller.customerRegistration(customerregistration);
//					customerregistration = respentity.getBody();
//					if (null != customerregistration) {
//						customerRegistrationList.add(customerregistration);
//						tblcust.setItems(customerRegistrationList);
//						notifyMessage(5, "Registration Successfully Completed");
//
//						filltable();
//						clear();
//
//						eventBus.post(customerregistration);
//					} else {
//
//						notifyMessage(5, "Please Fill Your Fields");
//					}
//				}
//			} catch (Exception e) {
//				customerregistration = new CustomerMst();
//				return;
//
//			}
//		}
//		}
//		customerregistration = new CustomerMst();
//	}
//
//	private void showAccountHeadGroup() {
//		ArrayList cat = new ArrayList();
//		RestTemplate restTemplate1 = new RestTemplate();
//		cat = RestCaller.SearchAccountsByGroup();
//		Iterator itr1 = cat.iterator();
//		while (itr1.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr1.next();
//			Object acccuntName = lm.get("accountName");
//			Object id = lm.get("id");
//			Object parentId = lm.get("parentId");
//			Object group = lm.get("groupOnly");
//			if (id != null) {
//				cmbGroup.getItems().add((String) acccuntName);
//			}
//
//		}
//
//	}
//	  @Subscribe
//	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
//	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
//	   		//if (stage.isShowing()) {
//	   			taskid = taskWindowDataEvent.getId();
//	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
//	   			
//	   		 
//	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
//	   			System.out.println("Business Process ID = " + hdrId);
//	   			
//	   			 PageReload();
//	   		}
//
//
//	   private void PageReload() {
//	   	
//	   }
//
//
//
//
//}
