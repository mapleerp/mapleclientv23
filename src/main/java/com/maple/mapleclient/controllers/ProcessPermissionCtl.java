package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.entity.ProcessPermissionMst;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.ProcessPermissionEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;

public class ProcessPermissionCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<ProcessMst> processMstList = FXCollections.observableArrayList();
	private ObservableList<UserMst> userMstList = FXCollections.observableArrayList();
	private ObservableList<ProcessPermissionMst> processPermissionMstList = FXCollections.observableArrayList();
	//ProcessPermissionMst processPermissionMst = new ProcessPermissionMst();
	String processId = "";
	String processName = "";
	ProcessMst processMst = null;
	UserMst userMst = null;
	
	ProcessPermissionMst selectedProcessPermission = new ProcessPermissionMst();
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	@FXML
	private TextField	textProcessName;

    @FXML
    private Button btnRefresh;

	@FXML
	private TextField processdesc;

	@FXML
	private Button btnsave;

	@FXML
	private Button btnShowAll;

	@FXML
	private Button btndelete;

	@FXML
	private ComboBox<String> usernamecombo;



	@FXML
	private TableView<ProcessPermissionMst> processtbl;

	@FXML
	private TableColumn<ProcessPermissionMst, String> PC1;

	@FXML
	private TableColumn<ProcessPermissionMst, String> PC2;

	@FXML
	private Button btnSearch;

	@FXML
	void GetUserName(MouseEvent event) {
		getUserName();

	}

	@FXML
	void getAllProcess(MouseEvent event) {
		getProcess();
	}


    @FXML
    void Refresh(ActionEvent event) {
    	getProcess();
    }
	@FXML
	private void initialize() {

		getUserName();
		eventBus.register(this);
	//	getProcess();

		/*
		 * processnamecombo.valueProperty().addListener(new ChangeListener<String>() {
		 * 
		 * @Override public void changed(ObservableValue<? extends String> observable,
		 * String oldValue, String newValue) { if (null != newValue) { processMst = new
		 * ProcessMst(); ResponseEntity<ProcessMst> respentity =
		 * RestCaller.getProcessDtls(newValue); processMst = respentity.getBody();
		 * processdesc.setText(processMst.getProcessDescription()); processId =
		 * processMst.getId(); processName = processMst.getProcessName(); } } });
		 */

		processtbl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					 

					selectedProcessPermission.setId(newSelection.getId());

				}
			}
		});
	}

	@FXML
	void delete(ActionEvent event) {

		if (null != selectedProcessPermission) {
			if (null != selectedProcessPermission.getId()) {
				RestCaller.processPermissionMstDelete(selectedProcessPermission.getId());
				
				notifyMessage(5, " Successfully Deleted...!!!");
				//ShowAllPermissions();
			}
		}

	}

	@FXML
	void ShowAll(ActionEvent event) {

		ShowAllPermissions();

	}

	@FXML
	void save(ActionEvent event) {

		if (null == usernamecombo.getValue()) {
			notifyMessage(5, " Please Select User...!!!");
		} else if (textProcessName.getText().isEmpty()) {

			notifyMessage(5, " Please Select Process...!!!");

		} else {
			 
			ProcessPermissionMst 	processPermissionMst = new ProcessPermissionMst();
			String user = usernamecombo.getValue();
			userMst = new UserMst();
			ResponseEntity<UserMst> respentity = RestCaller.getUserByName(user);
			userMst = respentity.getBody();
			
			processPermissionMst.setUserId(userMst.getId());
			
			
			String processName = textProcessName.getText();
			ResponseEntity<ProcessMst> respentityProcess = RestCaller.getProcessDtls(processName);
			processMst = respentityProcess.getBody();
			processPermissionMst.setProcessId(processMst.getId());
			
			
		//	processPermissionMst.setProcessId(processId);
           
			System.out.print(processPermissionMst.getProcessId()+"process id inside  procces permission");
			String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
            
            processPermissionMst.setId(vNo);
			processtbl.getItems().clear();
			ResponseEntity<ProcessPermissionMst> respentity1 = RestCaller
					.saveProcessPermissionMst(processPermissionMst);
			processPermissionMst = respentity1.getBody();
			processPermissionMstList.add(processPermissionMst);
			FillTable();

			//usernamecombo.getSelectionModel().clearSelection();
			textProcessName.clear();
			processdesc.setText("");
			processPermissionMst = null;
			notifyMessage(5, " Successfully saved...!!!");

		}

	}

	@FXML
	void SearchPermissions(ActionEvent event) {

		ProcessPermissionMst 	  processPermissionMst = new ProcessPermissionMst();

		if (null != usernamecombo.getValue()) {

			String user = usernamecombo.getValue();
			userMst = new UserMst();
			ResponseEntity<UserMst> respentity = RestCaller.getUserByName(user);
			userMst = respentity.getBody();
			processPermissionMst.setUserId(userMst.getId());

		}
		if ( !textProcessName.getText().isEmpty()) {
			String process = textProcessName.getText();
			processMst = new ProcessMst();
			ResponseEntity<ProcessMst> respentity = RestCaller.getProcessDtls(process);
			processMst = respentity.getBody();
			processPermissionMst.setProcessId(processMst.getId());
		}

		if (null != processPermissionMst) {
			if (null != processPermissionMst.getUserId()) {
				SearchPermissionByUserId(processPermissionMst);
			}
			if (null != processPermissionMst.getProcessId()) {
				SearchPermissionByProcessId(processPermissionMst);
			}
		}

	}

	private void getUserName() {

		usernamecombo.getItems().clear();
		ArrayList user = new ArrayList();
		 
		user = RestCaller.getAllUsers();
		Iterator itr = user.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			System.out.println("------processPermition-----" + lm);
			Object userName = lm.get("userName");
			Object id = lm.get("id");
			if (id != null) {

				usernamecombo.getItems().add((String) userName);
				UserMst userMst = new UserMst();
				userMst.setUserName((String) userName);
				userMst.setId((String) id);
				userMstList.add(userMst);
			}
		}

	}

	private void getProcess() {
		/*
		 * processnamecombo.getItems().clear(); ArrayList process = new ArrayList();
		 * 
		 * process = RestCaller.getAllProcess(); Iterator itr = process.iterator();
		 * while (itr.hasNext()) { LinkedHashMap lm = (LinkedHashMap) itr.next(); Object
		 * processName = lm.get("processName"); Object id = lm.get("id"); if (id !=
		 * null) {
		 * 
		 * processnamecombo.getItems().add((String) processName); ProcessMst processMst
		 * = new ProcessMst(); processMst.setProcessName((String) processName);
		 * processMst.setId((String) id); processMstList.add(processMst); } }
		 */

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void FillTable() {

		processtbl.setItems(processPermissionMstList);
		for (ProcessPermissionMst p : processPermissionMstList) {
			
			System.out.print(p.getUserId() +"user id issssssssssssssssssssssssssssssssssssssss ");
			if (null != p.getUserId()) {
				UserMst user = new UserMst();
				ResponseEntity<UserMst> respentity = RestCaller.getUserNameById(p.getUserId());
				user = respentity.getBody();
				if (null != user) {
					p.setUserName(user.getUserName());
					System.out.print(p.getUserName() +"user NAME  issssssssssssssssssssssssssssssssssssssss ");
					PC1.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
				}
			}
			if (null != p.getProcessId()) {
				ProcessMst processMst = new ProcessMst();
				System.out.print(p.getProcessId()+"process id isssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
				ResponseEntity<ProcessMst> respentity = RestCaller.getProcessById(p.getProcessId());
				processMst = respentity.getBody();
				if (null != processMst) {
					p.setProcessName(processMst.getProcessName());
					PC2.setCellValueFactory(cellData -> cellData.getValue().getProcessNameProperty());
				}
			}
		}
		clearField();

	}

	private void ShowAllPermissions() {

		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		 
		permission = RestCaller.getPermissionDtl();

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();

			Object userId = lm.get("userId");
			Object processId = lm.get("processId");

			Object id = lm.get("id");
			if (id != null) {
				ProcessPermissionMst pp = new ProcessPermissionMst();
				pp.setProcessId((String) processId);
				pp.setUserId((String) userId);
				pp.setId((String) id);

				processPermissionMstList.add(pp);

			}
			
		}
		FillTable();

	}

	private void SearchPermissionByUserId(ProcessPermissionMst processPermissionMst) {

		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		 
		permission = RestCaller.getPermissionDtlByUserId(processPermissionMst.getUserId());

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();

			Object userId = lm.get("userId");
			Object processId = lm.get("processId");

			Object id = lm.get("id");
			if (id != null) {
				ProcessPermissionMst pp = new ProcessPermissionMst();
				pp.setProcessId((String) processId);
				pp.setUserId((String) userId);
				pp.setId((String)id);
				processPermissionMstList.add(pp);

			}
			FillTable();
		}

	}

	private void SearchPermissionByProcessId(ProcessPermissionMst processPermissionMst) {
		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		 
		permission = RestCaller.getPermissionDtlByProcessId(processPermissionMst.getProcessId());

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();

			Object userId = lm.get("userId");
			Object processId = lm.get("processId");

			Object id = lm.get("id");
			if (id != null) {
				ProcessPermissionMst pp = new ProcessPermissionMst();
				pp.setProcessId((String) processId);
				pp.setUserId((String) userId);
				pp.setId((String)id);
				processPermissionMstList.add(pp);

			}
			FillTable();
		}

	}

	private void clearField() {
		processdesc.setText("");
		//usernamecombo.getSelectionModel().clearSelection();
	//	processnamecombo.getSelectionModel().clearSelection();
		textProcessName.clear();
	}
	
	 @FXML
	    void onEnterProcessName(ActionEvent event) {

		 loadPermissionPopup();
		 
	    }

	  

	 private void loadPermissionPopup() {
			
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/permissionPopup.fxml"));
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	  @Subscribe
			public void popupOrglistner(ProcessPermissionEvent processPermissionEvent) {

				Stage stage = (Stage) btnRefresh.getScene().getWindow();
				if (stage.isShowing()) {

					
					textProcessName.setText(processPermissionEvent.getProcessName());
			
				

				}
	  }
	  @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }
}
