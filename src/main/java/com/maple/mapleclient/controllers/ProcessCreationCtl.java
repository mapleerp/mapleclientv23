package com.maple.mapleclient.controllers;

import java.util.List;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class ProcessCreationCtl {
	String taskid;
	String processInstanceId;

	ProcessMst processMst = null;
	ProcessMst processMstDelete;
	private ObservableList<ProcessMst> processList = FXCollections.observableArrayList();
	private ObservableList<ProcessMst> processList1 = FXCollections.observableArrayList();
    @FXML
    private TextField txtprocessName;

    @FXML
    private TextField txtProcessDescription;

    @FXML
    private Button btnsave;

    @FXML
    private Button btndelete;

    @FXML
    private TableView<ProcessMst> tblProcess;

    @FXML
    private TableColumn<ProcessMst, String> clProcessName;
    
    @FXML
    private TableColumn<ProcessMst, String> clProcessDescription;

    @FXML
    private TableColumn<ProcessMst, String> clProcessType;
    
    @FXML
    private Button btnShowAll;
    
    @FXML
    private ComboBox<String> cmbProcessType;
    
    @FXML
	private void initialize() {
    	tblProcess.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					processMstDelete = new ProcessMst();
					processMstDelete.setId(newSelection.getId());
					}
			}
		});
    	
    	cmbProcessType.getItems().add("REPORT");
    	cmbProcessType.getItems().add("WINDOW");
    	cmbProcessType.getItems().add("CONFIGURATION");


    	
    }
    @FXML
    void ShowAll(ActionEvent event) {
    	processList.clear();
    	ResponseEntity<List<ProcessMst>> processMstSaved = RestCaller.getprocessMst();
		processList1 = FXCollections.observableArrayList(processMstSaved.getBody());
		if(null != processList1)
		{
			for(ProcessMst processmst : processList1 )
			{
				processList.add(processmst);
			}
			tblProcess.setItems(processList);
			filltable();
		}
    }

    @FXML
    void delete(ActionEvent event) {
    	
    	if(null!=processMstDelete)
    	{
 
    	if(null!= processMstDelete.getId())
    	{
    		RestCaller.deleteProcessMst(processMstDelete.getId());
    		notifyMessage(5,"Deleted!!!");
    		ResponseEntity<List<ProcessMst>> processMstSaved = RestCaller.getprocessMst();
    		processList = FXCollections.observableArrayList(processMstSaved.getBody());
    		if(null != processList)
    		{
    			tblProcess.setItems(processList);
    		}
    		txtProcessDescription.clear();
        	txtProcessDescription.clear();
    	}
    	}
    	else
    	{
    		notifyMessage(5,"Select Process From Table!!!");
    	}
    
    }
    
    @FXML
    void processOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtProcessDescription.requestFocus();
		}

    }
    @FXML
    void processDescOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			btnsave.requestFocus();
		}
    }
    @FXML
    void saveOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		 SaveProcess();
		}
    }
    @FXML
    void save(ActionEvent event) {
 
    	
    	 SaveProcess();
    	
    }
    private void filltable()
    {
    	
    	clProcessName.setCellValueFactory(cellData -> cellData.getValue().getprocessNameProperty());
    	clProcessDescription.setCellValueFactory(cellData -> cellData.getValue().getprocessDescriptionProperty());
    	clProcessType.setCellValueFactory(cellData -> cellData.getValue().getProcessTypeProperty());
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    private void SaveProcess()
    {
    	if(txtprocessName.getText().trim().isEmpty())
    	{

   		 notifyMessage(5,"Please Type Process Name!!");
   		txtprocessName.requestFocus();
   		return;
    	}
    	if(null == cmbProcessType.getValue())
    	{
    		 notifyMessage(5,"Please select Process Type!!");
    		 cmbProcessType.requestFocus();
    		 return;
    	}
    	
    	processMst = new ProcessMst();
    	processMst.setProcessDescription(txtProcessDescription.getText());
    	processMst.setProcessName(txtprocessName.getText());
    	processMst.setBranchCode(SystemSetting.systemBranch);
    	processMst.setProcessType(cmbProcessType.getValue());
    	String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PRC");
    	processMst.setId(vNo);
    	ResponseEntity<ProcessMst> respentity = RestCaller.saveProcessMst(processMst);
    	processMst = respentity.getBody();
    	processList.add(processMst);
    	tblProcess.setItems(processList);
    	filltable();
    	ResponseEntity<List<ProcessMst>> processMstSaved = RestCaller.getprocessMst();
		processList = FXCollections.observableArrayList(processMstSaved.getBody());
		if(null != processList)
		{
			tblProcess.setItems(processList);
		}
    	txtProcessDescription.clear();
    	txtprocessName.clear();
    	txtprocessName.requestFocus();
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }

}
