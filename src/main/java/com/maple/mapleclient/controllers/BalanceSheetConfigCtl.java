package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BalanceSheetConfigAsset;
import com.maple.mapleclient.entity.BalanceSheetConfigLiability;

import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountPopUpLiabilityEvent;
import com.maple.mapleclient.events.AcountPopUpAssetEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

@RestController
public class BalanceSheetConfigCtl {

	String accountId;
	String liabilityAccountId;
	String assetAccountId;
	private EventBus eventBus = EventBusFactory.getEventBus();

	AccountHeads accountHeads;
	
	BalanceSheetConfigAsset balanceSheetConfigAssetDtl=null;
	BalanceSheetConfigLiability balanceSheetConfigLiabilityDtl=null;
	
	
	private ObservableList<BalanceSheetConfigAsset> balanceSheetConfigAssetTable = FXCollections.observableArrayList();
	private ObservableList<BalanceSheetConfigLiability> balanceSheetConfigLiabiliityTable = FXCollections.observableArrayList();
	
	

  //========================================================================
 //==========================================================================
    
    
    @FXML
    private TextField txtLiabilitySerialNo;

    @FXML
    private TextField txtLiabilityAccountName;

    @FXML
    private Button btnLiabilityAdd;

    @FXML
    private Button btnLiabilityClear;

    @FXML
    private Button btnLiabilityShowAll;

    @FXML
    private Button btnLiabilityDelete;

    @FXML
    private TextField txtAssetSerialNo;

    @FXML
    private TextField txtAssetAccount;

    @FXML
    private Button btnAssetAdd;

    @FXML
    private Button btnAssetClear;

    @FXML
    private Button btnAssetShowAll;

    @FXML
    private Button btnAssetDelete;

    @FXML
    private TableView<BalanceSheetConfigLiability> tblLiability;

    @FXML
    private TableColumn<BalanceSheetConfigLiability, String> clmnLiabilitySerialNo;

    @FXML
    private TableColumn<BalanceSheetConfigLiability, String> clmnLiabilityAccount;

    @FXML
    private TableView<BalanceSheetConfigAsset> tblAsset;

    @FXML
    private TableColumn<BalanceSheetConfigAsset, String> clmnAssetSerialNo;

    @FXML
    private TableColumn<BalanceSheetConfigAsset, String> clmnAssetAccount;

    @FXML
   	private void initialize() {
    	eventBus.register(this);
    	
    	tblLiability.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					balanceSheetConfigLiabilityDtl = new BalanceSheetConfigLiability();
					balanceSheetConfigLiabilityDtl.setId(newSelection.getId());
					
				}
			}
    	});
    	
    	tblAsset.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					balanceSheetConfigAssetDtl = new BalanceSheetConfigAsset();
					balanceSheetConfigAssetDtl.setId(newSelection.getId());
					
				}
			}
    	});
    }
    
    @FXML
    void assetAccountKey(KeyEvent event) {
    	loadAssetPopup();
    }

    @FXML
    void assetAccountMouse(MouseEvent event) {
    	loadAssetPopup();
    }

    @FXML
    void assetAdd(ActionEvent event) {
    	saveAsset();
    }

    private void saveAsset() {
    	
    	if (txtAssetSerialNo.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter a serial number", false);
			txtAssetSerialNo.requestFocus();
			return;
		}
    	if (txtAssetAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select Account", false);
			txtAssetAccount.requestFocus();
			return;
		}
    	
    	 balanceSheetConfigAssetDtl = new BalanceSheetConfigAsset();

    	 ResponseEntity<AccountHeads> accountIdddd=RestCaller.getAccountHeadByName(txtAssetAccount.getText());
     	AccountHeads accountHeadssss=accountIdddd.getBody();
     	if(null != accountHeadssss) {
     		balanceSheetConfigAssetDtl.setAccountId(accountHeadssss.getId());
     	}
    	balanceSheetConfigAssetDtl.setSerialNumber(txtAssetSerialNo.getText());

		ResponseEntity<BalanceSheetConfigAsset> balanceSheetConfigAssetSaved = RestCaller
				.saveBalanceSheetConfigAsset(balanceSheetConfigAssetDtl);

		notifyMessage(5, "Saved ", true);
		txtAssetAccount.clear();
		txtAssetSerialNo.clear();
		showAsset();
		return;
		
	}

	@FXML
    void assetClear(ActionEvent event) {
    	txtAssetSerialNo.clear();
    	txtAssetAccount.clear();
		tblAsset.getItems().clear();
    }

    @FXML
    void assetDelete(ActionEvent event) {
    	
      	if(null != balanceSheetConfigAssetDtl) {
    		if(null != balanceSheetConfigAssetDtl.getId()) {
    			System.out.print(balanceSheetConfigAssetDtl.getId());
    			RestCaller.deleteBalanceSheetConfigAsset(balanceSheetConfigAssetDtl.getId());
  			
    			ResponseEntity<List<BalanceSheetConfigAsset>> balanceSheetConfigAssetList = RestCaller.getAllBalanceSheetConfigAsset();
    			balanceSheetConfigAssetTable=FXCollections.observableArrayList(balanceSheetConfigAssetList.getBody());
    	    	fillAssetTable();
    			notifyMessage(5, "Deleted...",false);
    		}
    	}

    }

    private void fillAssetTable() {
    	tblAsset.setItems(balanceSheetConfigAssetTable);
    	for(BalanceSheetConfigAsset asset:balanceSheetConfigAssetTable) {
    		ResponseEntity<AccountHeads> accountAsset=RestCaller.getAccountHeadsById(asset.getAccountId());
    		if(null != accountAsset.getBody()) {
    			asset.setAccountName(accountAsset.getBody().getAccountName());
    		}
    	}
		clmnAssetSerialNo.setCellValueFactory(cellData -> cellData.getValue().getSerialNumberProperty());
		clmnAssetAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		
	}

	@FXML
    void assetShowAll(ActionEvent event) {
		showAsset();
    }

    private void showAsset() {
    	ResponseEntity<List<BalanceSheetConfigAsset>> balanceSheetConfigAssetList = RestCaller.getAllBalanceSheetConfigAsset();
		balanceSheetConfigAssetTable=FXCollections.observableArrayList(balanceSheetConfigAssetList.getBody());
		fillAssetTable();
		
	}
    
    
    private void loadAssetPopup() {
		System.out.println("-------------showPopup-------------");
		try {

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/AccountPopUpByAsset.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
    
    @Subscribe
	public void popupOrglistnerForAsset(AcountPopUpAssetEvent acountPopUpAssetEvent) {
		Stage stage1 = (Stage) btnAssetAdd.getScene().getWindow();
		if (stage1.isShowing()) {

			Platform.runLater(() -> {
				txtAssetAccount.setText(acountPopUpAssetEvent.getAccountName());
				assetAccountId = acountPopUpAssetEvent.getAccountId();
			});

		}
	}
    

	@FXML
    void liabiliityAdd(ActionEvent event) {
		saveLiability();
    }

    private void saveLiability() {
    	if (txtLiabilitySerialNo.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter a serial number", false);
			txtLiabilitySerialNo.requestFocus();
			return;
		}
    	if (txtLiabilityAccountName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select Account", false);
			txtLiabilityAccountName.requestFocus();
			return;
		}
    	
    	balanceSheetConfigLiabilityDtl = new BalanceSheetConfigLiability();
    	ResponseEntity<AccountHeads> accountId=RestCaller.getAccountHeadByName(txtLiabilityAccountName.getText());
    	AccountHeads accountHeads=accountId.getBody();
    	if(null != accountHeads) {
    	balanceSheetConfigLiabilityDtl.setAccountId(accountHeads.getId());
    	}
    	balanceSheetConfigLiabilityDtl.setSerialNumber(txtLiabilitySerialNo.getText());

		ResponseEntity<BalanceSheetConfigLiability> balanceSheetConfigAssetSaved = RestCaller
				.saveBalanceSheetConfigLiability(balanceSheetConfigLiabilityDtl);

		notifyMessage(5, "Saved ", true);
		txtLiabilityAccountName.clear();
		txtLiabilitySerialNo.clear();
		showLiability();
		return;
		
	}

	@FXML
    void liabiliityShowAll(ActionEvent event) {
    	showLiability();
    }

    private void showLiability() {
    	ResponseEntity<List<BalanceSheetConfigLiability>> balanceSheetConfigLiabilityList = RestCaller.getAllBalanceSheetConfigLiability();
		balanceSheetConfigLiabiliityTable=FXCollections.observableArrayList(balanceSheetConfigLiabilityList.getBody());
    	fillLiabilityTable();
		
	}

	@FXML
    void liabilityAccountNameKey(KeyEvent event) {
    	loadLiabilityPopup();
    }

    @FXML
    void liabilityAccountNameMouse(MouseEvent event) {
    	loadLiabilityPopup();

    }

    @FXML
    void liabilityClear(ActionEvent event) {
    	txtLiabilitySerialNo.clear();
    	txtLiabilityAccountName.clear();
		tblLiability.getItems().clear();
    }

    @FXML
    void liabilityDelete(ActionEvent event) {

    	if(null != balanceSheetConfigLiabilityDtl) {
    		if(null != balanceSheetConfigLiabilityDtl.getId()) {
    			RestCaller.deleteBalanceSheetConfigLiability(balanceSheetConfigLiabilityDtl.getId());
  			
    			ResponseEntity<List<BalanceSheetConfigLiability>> balanceSheetConfigLiabilityList = RestCaller.getAllBalanceSheetConfigLiability();
    			balanceSheetConfigLiabiliityTable=FXCollections.observableArrayList(balanceSheetConfigLiabilityList.getBody());
    	    	fillLiabilityTable();
    			notifyMessage(5, "Deleted...",false);
    		}
    	}
    }
    
    
    private void fillLiabilityTable() {
    	tblLiability.setItems(balanceSheetConfigLiabiliityTable);
    	for(BalanceSheetConfigLiability liability:balanceSheetConfigLiabiliityTable) {
    		ResponseEntity<AccountHeads> accountLiability=RestCaller.getAccountHeadsById(liability.getAccountId());
    		if(null != accountLiability.getBody()) {
    			liability.setAccountName(accountLiability.getBody().getAccountName());
    		}
    	}
		clmnLiabilitySerialNo.setCellValueFactory(cellData -> cellData.getValue().getSerialNumberProperty());
		clmnLiabilityAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
			
	}

	private void loadLiabilityPopup() {
		System.out.println("-------------showPopup-------------");
		try {

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/AccountPopUpByLiability.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
    
    
    @Subscribe
	public void popupOrglistner(AccountPopUpLiabilityEvent accountPopUpLiabilityEvent) {
		Stage stage1 = (Stage) btnLiabilityAdd.getScene().getWindow();
		if (stage1.isShowing()) {

			Platform.runLater(() -> {
				txtLiabilityAccountName.setText(accountPopUpLiabilityEvent.getAccountName());
				liabilityAccountId = accountPopUpLiabilityEvent.getAccountId();
			});

		}
	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
    
    
    
	}
    

