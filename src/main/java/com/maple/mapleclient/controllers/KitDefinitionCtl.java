
package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.KitValidationReport;
import com.maple.mapleclient.entity.MixCatItemLinkMst;
import com.maple.mapleclient.entity.MixCategoryMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

@Controller
public class KitDefinitionCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	KitDefinitionMst kitDefenitionmst = null;
	ItemMst itemmst = new ItemMst();
	ItemMst itemkit = new ItemMst();
	UnitMst unitmst = new UnitMst();
	//version1.8
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();
//version1.8ends
	
	private ObservableList<KitDefinitionDtl> kitDefenitionDtlList = FXCollections.observableArrayList();
	private ObservableList<KitDefinitionMst> kitDefenitionMstList = FXCollections.observableArrayList();
	KitDefinitionDtl kitDefenitionDtl = new KitDefinitionDtl();
	double itemMrp=0.0;
	double productionCost =0.0;
	@FXML
	private TextField txtKitName;

    @FXML
    private TextField txtUnit;
	//version1.8
    @FXML
    private ComboBox<String> cmbUnit;
    //version1.8ends.. coninues...

	@FXML
	private TextField txtProductionCost;

	@FXML
	private TextField txtTaxRate;

	@FXML
	private TextField txtMinimumQty;

	@FXML
	private TextField txtItemCode;

	@FXML
	private ComboBox<String >cmbMixCategory;
	@FXML
	private Button btnFetchDetails;

	@FXML
	private TextField txtkitBarcode;

	@FXML
	private TextField txtMachineUnit;
	 
	@FXML
	private Button btnSubmit;

	@FXML
	private TextField txtkitUnitName;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtItemBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private Button btnValidate;
	
	@FXML
    private TextField txtItemQtyInMachineUnit;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnAdd;

	//version 1.8
    @FXML
    private TextField txtPrimaryQty;
//version1.8 ends... txtUnitCode is not needed
    
    
    @FXML
    private CheckBox chkOnline;
    
	//@FXML
	//private TextField txtUnitCode;

	@FXML
	private TableView<KitDefinitionDtl> tblKitDefinition;

	@FXML
	private TableColumn<KitDefinitionDtl, String> clKitItemName;

	@FXML
	private TableColumn<KitDefinitionDtl, String> clKitBarcode;

	@FXML
	private TableColumn<KitDefinitionDtl, Number> clKitQty;

	@FXML
	private TableColumn<KitDefinitionDtl, String> clKitUnitCode;

	@FXML
	private void initialize() {
		// btnAdd.setVisible(false);
		btnDelete.setVisible(false);
		eventBus.register(this);
		btnFetchDetails.setVisible(false);
		// kitDefenitionmst = new KitDefinitionMst();
		kitDefenitionDtl = new KitDefinitionDtl();
		// kitDefenitionmst = new KitDefinitionMst();
		
		
		//check box for deciding the process is done on either offline or online=======01-07-2021
		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("OFFLINE_KIT_CREATION");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("YES")) {
				chkOnline.setDisable(false);
			} else {
				chkOnline.setDisable(true);
			}
		}else {
			chkOnline.setDisable(true);

		}
		chkOnline.selectedProperty().addListener(
				(obs, oldSelection, newSelection) ->	{
					if(chkOnline.isSelected()) {
						chkOnline.setText("Offline");
					}else {
						chkOnline.setText("Online");
					}
				});
		//============================end=====================01-07-2021==============
		
		
		UnitMst unitMst = new UnitMst();
		// set unit
		ArrayList unit = new ArrayList();

		unit = RestCaller.SearcResourceCat();
		
		System.out.print(unit.size()+"unit list size isssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		Iterator itr = unit.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object unitName = lm.get("mixName");
			Object resCatId = lm.get("id");
			if (resCatId != null) {
				cmbMixCategory.getItems().add((String) unitName);
				UnitMst newUnitMst = new UnitMst();
				newUnitMst.setUnitName((String) unitName);
				
			}
		
		//version1.8
		
				cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
						if(null != cmbUnit.getSelectionModel())
						{
						ResponseEntity<UnitMst> getUnit = RestCaller
								.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
						txtUnit.setText(cmbUnit.getSelectionModel().getSelectedItem());
//						cmbUnit.setValue(cmbUnit.getSelectionModel().getSelectedItem());
						if(null != getUnit.getBody())
						{
						if(getItem.getBody().getId().equalsIgnoreCase(getUnit.getBody().getId()))
						{
							txtPrimaryQty.setText(txtQty.getText());
						}
						else
						{
						ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller
								.getMultiUnitbyprimaryunit(getItem.getBody().getId(), getUnit.getBody().getId());
						if (null != getMultiUnit.getBody()) {
							Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
									getUnit.getBody().getId(), getItem.getBody().getUnitId(),
									Double.parseDouble(txtQty.getText()));
							txtPrimaryQty.setText(conversionQty.toString());
						}
						else
						{
							txtPrimaryQty.setText(txtQty.getText());
						}
						}
						}
						else
						{
							txtPrimaryQty.setText(txtQty.getText());
						}
						}
					}
				});
				
				txtQty.textProperty().addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
							txtQty.setText(oldValue);
							txtPrimaryQty.setText(oldValue);
						}
							ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
								if(null != cmbUnit.getSelectionModel())
								{
								ResponseEntity<UnitMst> getUnit = RestCaller
										.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
								
//								cmbUnit.setValue(cmbUnit.getSelectionModel().getSelectedItem());
								if(null != getUnit.getBody())
								{
								if(getItem.getBody().getId().equalsIgnoreCase(getUnit.getBody().getId()))
								{
									txtPrimaryQty.setText(txtQty.getText());
								}
								else
								{
								ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller
										.getMultiUnitbyprimaryunit(getItem.getBody().getId(), getUnit.getBody().getId());
								if (null != getMultiUnit.getBody()) {
									Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
											getUnit.getBody().getId(), getItem.getBody().getUnitId(),
											Double.parseDouble(txtQty.getText()));
									txtPrimaryQty.setText(conversionQty.toString());
								}
								else
								{
									txtPrimaryQty.setText(txtQty.getText());
								}
								}
								}
								else
								{
									txtPrimaryQty.setText(txtQty.getText());
								}
								}
								else
								{
									txtPrimaryQty.setText(txtQty.getText());
								}
							}
					
				});
				//version1.8ends
		txtMinimumQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtMinimumQty.setText(oldValue);
				}
			}
		});
		txtTaxRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtTaxRate.setText(oldValue);
				}
			}
		});
		txtProductionCost.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtProductionCost.setText(oldValue);
				}
			}
		});}
	}

	@FXML
	void btnAdd(ActionEvent event) {

		/*
		 * Verify Fields before proceeding
		 */
		 addItems();
	}
	private void addItems()
	{
		 
		if (!verifyFields()) {
			return;
		}

		btnSubmit.setVisible(true);

		if (null == kitDefenitionmst) {
			kitDefenitionmst = new KitDefinitionMst();
			String id = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
			kitDefenitionmst.setId(id);
			kitDefenitionmst.setBarcode(txtkitBarcode.getText());
			kitDefenitionmst.setKitName(txtKitName.getText());
			kitDefenitionmst.setBranchCode(SystemSetting.getUser().getBranchCode());

//			try {
//				ResponseEntity<ItemMst> repitem = RestCaller.getItemName(txtKitName.getText());
//
//				itemmst = repitem.getBody();
//			} catch (Exception e) {
//
//				kitDefenitionmst = null;
//				return;
//			}

			kitDefenitionmst.setItemId(itemkit.getId());
			kitDefenitionmst.setMinimumQty(Double.parseDouble(txtMinimumQty.getText()));
			kitDefenitionmst.setUnitId(unitmst.getId());
			kitDefenitionmst.setUnitName(txtkitUnitName.getText());
			kitDefenitionmst.setProductionCost(0.0);
			kitDefenitionmst.setTaxRate(Double.parseDouble(txtTaxRate.getText()));
			kitDefenitionmst.setItemCode(txtItemCode.getText());
			kitDefenitionmst.setFinalSave("N");

//			try {
//				ResponseEntity<KitDefinitionMst> respentitykit = RestCaller.getKitId(kitDefenitionmst.getKitName());
//				if(null != respentitykit.getBody())
//				{
//					kitDefenitionmst = respentitykit.getBody();
//					RestCaller.UpdateKitDefenitionMst(kitDefenitionmst);
//					ResponseEntity<List<KitDefinitionDtl>> kitdefinitiondtlSaved = RestCaller
//							.getKidefinitionDtl(kitDefenitionmst);
//					kitDefenitionDtlList = FXCollections.observableArrayList(kitdefinitiondtlSaved.getBody());
//					tblKitDefinition.setItems(kitDefenitionDtlList);
//					txtProductionCost.setText(Double.toString(kitDefenitionmst.getProductionCost()));
//					txtMinimumQty.setText(Double.toString(kitDefenitionmst.getMinimumQty()));
//					txtItemCode.setText(kitDefenitionmst.getItemCode());
//					clKitBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
//					clKitItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
//					clKitQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
//					clKitUnitCode.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
//				}
//				else
//				{
			
			
	 		//===========code for identify whether the kit definition mst is saved on offline or online mode=======01-07-2021====

			String status = null;
			ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("OFFLINE_KIT_CREATION");
			ParamValueConfig paramValueConfig = getParamValue.getBody();
			if(chkOnline.isSelected() || null == paramValueConfig || paramValueConfig.getValue().equalsIgnoreCase("NO")) {
				
				ResponseEntity<KitDefinitionMst> respentity = RestCaller.saveKitDefenitionMst(kitDefenitionmst);
				kitDefenitionmst = respentity.getBody();
			}else {
				status = "ONLINE";
					ResponseEntity<KitDefinitionMst> respentity = RestCaller.saveKitDefenitionMst(kitDefenitionmst,status);
					kitDefenitionmst = respentity.getBody();
			}		
					
			//=================code end for saving kit definition mst==============================		
//				}
//			} catch (Exception e) {
//
//				return;
//			}
		}
	

		if (null != kitDefenitionmst.getId()) {
			if(null != kitDefenitionDtl)
			{
				if(null != kitDefenitionDtl.getId())
				{
					RestCaller.deleteKitDefintionDtl(kitDefenitionDtl.getId());
					tblKitDefinition.getItems().clear();
					ResponseEntity<List<KitDefinitionDtl>> getKit =RestCaller.getKidefinitionDtl(kitDefenitionmst);
					kitDefenitionDtlList = FXCollections.observableArrayList(getKit.getBody());
					Filltable();
				}
			}

			kitDefenitionDtl = new KitDefinitionDtl();

			kitDefenitionDtl.setKitDefenitionmst(kitDefenitionmst);
			kitDefenitionDtl.setItemName(txtItemName.getText());
	
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
			itemmst = getItem.getBody();
			
			
			//version1.8
			ResponseEntity<UnitMst> getUnit = RestCaller
					.getUnitByName(txtUnit.getText());
//			cmbUnit.setValue(cmbUnit.getSelectionModel().getSelectedItem());
			ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller
					.getMultiUnitbyprimaryunit(getItem.getBody().getId(), getUnit.getBody().getId());
			if(null == getMultiUnit.getBody())
			{
			if(txtProductionCost.getText().trim().isEmpty())
			{
			productionCost= productionCost + (Double.parseDouble(txtQty.getText())/Double.parseDouble(txtMinimumQty.getText()))*itemMrp;
			}
			else
			{
				productionCost= productionCost +(Double.parseDouble(txtProductionCost.getText()))+ (Double.parseDouble(txtQty.getText())/Double.parseDouble(txtMinimumQty.getText()))*itemMrp;

			}
			}
			else
			{
				if(txtProductionCost.getText().trim().isEmpty())
				{
				productionCost= productionCost + (Double.parseDouble(txtQty.getText())/Double.parseDouble(txtMinimumQty.getText()))*getMultiUnit.getBody().getPrice();
				}
				else
				{
					productionCost= productionCost +(Double.parseDouble(txtProductionCost.getText()))+ (Double.parseDouble(txtQty.getText())/Double.parseDouble(txtMinimumQty.getText()))*getMultiUnit.getBody().getPrice();

				}
			}
			//version1.8ends
			BigDecimal prodCost = new BigDecimal(productionCost);
			prodCost = prodCost.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtProductionCost.setText(prodCost.toPlainString());
			//kitDefenitionDtl.setQty(Double.parseDouble(txtQty.getText()));
			
			
				kitDefenitionDtl.setItemId(itemmst.getId());
				
				//version1.8
				if(txtPrimaryQty.getText().trim().isEmpty())
				{
					kitDefenitionDtl.setQty(Double.parseDouble(txtQty.getText()));
					kitDefenitionDtl.setMltiUnitQty(Double.parseDouble(txtQty.getText()));
				}
				else
				{
				kitDefenitionDtl.setQty(Double.parseDouble(txtPrimaryQty.getText()));
				kitDefenitionDtl.setMltiUnitQty(Double.parseDouble(txtQty.getText()));
				}
				kitDefenitionDtl.setCustomisedUnitName(cmbUnit.getValue());
				ResponseEntity<UnitMst> unit1= RestCaller.getUnitByName(cmbUnit.getValue());
				kitDefenitionDtl.setCustomisedUnitId(unit1.getBody().getId());
				ResponseEntity<UnitMst> unit= RestCaller.getunitMst(itemmst.getUnitId());
				kitDefenitionDtl.setUnitId(itemmst.getUnitId());
				kitDefenitionDtl.setUnitName(unit.getBody().getUnitName());
				//version1.8 ends
				
			//kitDefenitionDtl.setUnitId(itemmst.getUnitId());
			//kitDefenitionDtl.setUnitName(txtUnitCode.getText());
				
				
			kitDefenitionDtl.setBarcode(txtItemBarcode.getText());

			
			
	 		//===========code for identify whether the kit definition dtl is saved on offline or online mode=======01-07-2021====

			String status = null;
			ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("OFFLINE_KIT_CREATION");
			ParamValueConfig paramValueConfig = getParamValue.getBody();
			if(chkOnline.isSelected() || null == paramValueConfig || paramValueConfig.getValue().equalsIgnoreCase("NO")) {
				try {
					ResponseEntity<KitDefinitionDtl> respentitydtl = RestCaller.saveKitDefenitionDtl(kitDefenitionDtl);
					kitDefenitionDtl = respentitydtl.getBody();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				status = "ONLINE";
				try {
					ResponseEntity<KitDefinitionDtl> respentitydtl = RestCaller.saveKitDefenitionDtl(kitDefenitionDtl,status);
					kitDefenitionDtl = respentitydtl.getBody();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
			
			//=================code end for saving kit definition dtl==============================
			
			kitDefenitionDtlList.add(kitDefenitionDtl);
			Filltable();
			clearfield();

			kitDefenitionDtl = null;
		}
		
		//version1.8
		cmbUnit.getSelectionModel().clearSelection();
		cmbUnit.getItems().clear();
		//version1.8ends
		txtItemName.requestFocus();

		}
		 
		
 
    @FXML
    void minimumQtyOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		if(txtMinimumQty.getText().trim().isEmpty())
    		{
    			notifyMessage(5, "Please Enter Minimum Qty",false);
    			return;
    		}
    		showStockitemPopup() ;
		}

    }
    @FXML
    void qtyKeyPress(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		 addItems();
		}
    }
    @FXML
    void itemOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		showStockitemPopup() ;
		}
    }

	private boolean verifyFields() {
		if (txtKitName.getText().trim().isEmpty()) {
			notifyMessage(5, "Type KitName",false);
			txtKitName.requestFocus();
			return false;
		}
		if (txtkitUnitName.getText().trim().isEmpty()) {
			notifyMessage(5, " Type Unit",false);
			txtkitUnitName.requestFocus();
			return false;
		}
		if (txtMinimumQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Type Minimum Qty",false);
			txtMinimumQty.requestFocus();
			return false;
		}

		if (txtTaxRate.getText().trim().isEmpty()) {
			notifyMessage(5, " Please fill Tax Rate",false);
			txtTaxRate.requestFocus();
			return false;
		}
		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Select Item Name",false);
			txtItemName.requestFocus();
			return false;
		}
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Fill Qty",false);
			txtQty.requestFocus();
			return false;
		}
		return true;
	}

	@FXML
	void btnDelete(ActionEvent event) {
		
		RestCaller.deleteKitDefintionDtl(kitDefenitionDtl.getId());
		notifyMessage(5, "Deleted",true);
		clearfield();
		tblKitDefinition.getItems().clear();
		ResponseEntity<List<KitDefinitionDtl>> getKit =RestCaller.getKidefinitionDtl(kitDefenitionmst);
		kitDefenitionDtlList = FXCollections.observableArrayList(getKit.getBody());
		Filltable();
		btnDelete.setVisible(false);

		kitDefenitionDtl = null;
	}

	@FXML
	void tableclick(MouseEvent event) {
		btnDelete.setVisible(true);
		if (tblKitDefinition.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tblKitDefinition.getSelectionModel();
			kitDefenitionDtlList = selectionModel.getSelectedItems();
			for (KitDefinitionDtl kit : kitDefenitionDtlList) {
				kitDefenitionDtl = new KitDefinitionDtl();
				kitDefenitionDtl.setId(kit.getId());
				txtItemName.setText(kit.getItemName());
				
				//version1.8
				cmbUnit.getItems().clear();
				if(null != kit.getMltiUnitQty())
				{
				txtQty.setText(Double.toString(kit.getMltiUnitQty()));
				}
				else
				{
					txtQty.setText(Double.toString(kit.getQty()));
				}
				txtPrimaryQty.setText(Double.toString(kit.getQty()));
				cmbUnit.getItems().add(kit.getUnitName());
				ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller.getMultiUnitByItemId(kit.getItemId());

				multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());

				if (!multiUnitList.isEmpty()) {

					for (MultiUnitMst multiUniMst : multiUnitList) {

						ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
						cmbUnit.getItems().add(getUnit.getBody().getUnitName());
					}

				} 
				if(null != kit.getCustomisedUnitId())
				{
					if(!kit.getCustomisedUnitId().equalsIgnoreCase(null))
					{
						ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(kit.getCustomisedUnitId());
						//cmbUnit.getItems().add( getUnit.getBody().getUnitName());
						cmbUnit.setValue( getUnit.getBody().getUnitName());
						txtUnit.setText(getUnit.getBody().getUnitName());
					}
					else
					{
						cmbUnit.setValue(kit.getUnitName());
						txtUnit.setText(kit.getUnitName());
					}
				}
				else
				{
					cmbUnit.setValue(kit.getUnitName());
					txtUnit.setText(kit.getUnitName());
					
				}
				
				//txtUnitCode.setText(kit.getUnitName()); not needed
				//version1.8ends
				
				//txtQty.setText(Double.toString(kit.getQty()));
				//txtUnitCode.setText(kit.getUnitName());
			

				txtItemBarcode.setText(kit.getBarcode());
			}
		}
	}


	@FXML
	void ClearFilelds(ActionEvent event) {
		clearfield();
		clearkit();
		tblKitDefinition.getItems().clear();
	}

	@FXML
	void fetchDetails(ActionEvent event) {
		if (txtKitName.getText().trim().isEmpty()) {
			notifyMessage(5, "Select KitName...!",false);
			txtKitName.requestFocus();
			
			return;
		} else {
			tblKitDefinition.getItems().clear();
			// btnAdd.setVisible(false);
			btnDelete.setVisible(false);
			btnSubmit.setVisible(false);
			kitDefenitionmst = new KitDefinitionMst();
			kitDefenitionmst.setKitName(txtKitName.getText());
			ResponseEntity<KitDefinitionMst> respentitykit = RestCaller.getKitId(kitDefenitionmst.getKitName());
			kitDefenitionmst = respentitykit.getBody();
			if (null != kitDefenitionmst) {
				ResponseEntity<List<KitDefinitionDtl>> getKit =RestCaller.getKidefinitionDtl(kitDefenitionmst);
				kitDefenitionDtlList = FXCollections.observableArrayList(getKit.getBody());
				Filltable();
				tblKitDefinition.setItems(kitDefenitionDtlList);
				txtProductionCost.setText(Double.toString(kitDefenitionmst.getProductionCost()));
				txtMinimumQty.setText(Double.toString(kitDefenitionmst.getMinimumQty()));
				txtItemCode.setText(kitDefenitionmst.getItemCode());
				clKitBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
				clKitItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
				clKitQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
				clKitUnitCode.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
			} else {
				notifyMessage(5, " Kit Name Not Found!!!!",false);
				txtkitBarcode.clear();
				txtKitName.clear();
				txtProductionCost.clear();
				txtMinimumQty.clear();
				txtKitName.requestFocus();

			}
		}
	}

	@FXML
	void submit(ActionEvent event) {
		try
		{
		if (txtKitName.getText().trim().isEmpty()) {
			notifyMessage(5, "Type KitName",false);
			txtKitName.requestFocus();

		} else if (txtkitUnitName.getText().trim().isEmpty()) {
			notifyMessage(5, " Type Unit",false);
			txtkitUnitName.requestFocus();
		} else if (txtMinimumQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Type Minimum Qty",false);
			txtMinimumQty.requestFocus();
		} else if (txtProductionCost.getText().trim().isEmpty()) {
			notifyMessage(5, " Type Production Cost",false);
			txtProductionCost.requestFocus();
		} else if (txtTaxRate.getText().trim().isEmpty()) {
			notifyMessage(5, " Type Tax Rate",false);
			txtTaxRate.requestFocus();
		}

		else {
			if (null != kitDefenitionmst.getId()) {
				kitDefenitionmst.setProductionCost(Double.valueOf(txtProductionCost.getText()));
				kitDefenitionmst.setFinalSave("Y");
				// RestCaller.updatekitDefinitionMst(kitDefenitionmst);
				
		//===========code for identify whether the kit definition dtl is saved on offline or online mode=======01-07-2021====
				String status = null;
				ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("OFFLINE_KIT_CREATION");
				ParamValueConfig paramValueConfig = getParamValue.getBody();
				if(chkOnline.isSelected() || null == paramValueConfig || paramValueConfig.getValue().equalsIgnoreCase("NO")) {
					RestCaller.UpdateKitDefenitionMst(kitDefenitionmst);
				}
				else {
					status = "ONLINE";
					RestCaller.UpdateKitDefenitionMst(kitDefenitionmst,status);
				}
				
				
				
//--------------------------------------------Mix Category Starts-------------------------------------------------
//				if(null != cmbMixCategory.getSelectionModel().getSelectedItem()) {
//				MixCatItemLinkMst resourceCatItemLinkMst =new MixCatItemLinkMst();
//				ResponseEntity<ItemMst> basicResponse=RestCaller.getItemByNameRequestParam(txtKitName.getText());
//				ItemMst itemMst=basicResponse.getBody();
//				resourceCatItemLinkMst.setItemId(itemMst.getId());
//			    ResponseEntity<MixCategoryMst> resp=RestCaller.findByResoureCatName(cmbMixCategory.getValue()); 
//			    MixCategoryMst resourceCategoryMst=resp.getBody();
//			    resourceCatItemLinkMst.setResourceCatId(String.valueOf(resourceCategoryMst.getId()));
//			    ResponseEntity<List<MixCategoryMst>> response=RestCaller.findByResoureCatNameAndItem(itemMst.getId(),resourceCategoryMst.getId());
//			    ResponseEntity<MixCatItemLinkMst> mixCatItemLinkMst=RestCaller.findByItemId(itemMst.getId());
//			    MixCatItemLinkMst mixCatItemLinkMsts=mixCatItemLinkMst.getBody();
//
//		    	
//
//		    	
//			 if(null!=mixCatItemLinkMsts) {
//			    if(mixCatItemLinkMsts.getId()!=null) {
//			    	
//			    	System.out.print(cmbMixCategory.getValue()+"mix category name isssssssssssssssssss");
//			        ResponseEntity<MixCategoryMst> responseEntity=RestCaller.findByResoureCatName(cmbMixCategory.getValue()); 
//			        MixCategoryMst mixCategoryMst=responseEntity.getBody();
//			    	
//			    	 RestCaller.updateMixCategory(itemMst.getId(),mixCategoryMst.getId());
//			    	
//			    	 System.out.print(itemMst.getId()+"item id isssssssssssssssssssssssssss");
//			    	 System.out.print(cmbMixCategory.getValue()+"category name isssssssssssssssssssss");
//			    	 
//			    	 
//			    	System.out.print("inside update isssssssssssssssssssssssss");
//			    	
//			    }}
//			    else {
//			    	ResponseEntity<MixCatItemLinkMst> respentity = RestCaller.saveResourceCatItemLinkMst(resourceCatItemLinkMst);
//			    	System.out.print("inside save  isssssssssssssssssssssssss");
//			    }
//				}
//-----------------------------------------------Mix Category Ends-----------------------------------------------
			    txtKitName.clear();
				txtkitUnitName.clear();
				txtMinimumQty.clear();
				txtkitBarcode.clear();
				txtProductionCost.clear();
				txtTaxRate.clear();
				txtItemCode.clear();
				txtItemName.clear();
				txtItemBarcode.clear();
				txtQty.clear();
				txtItemQtyInMachineUnit.clear();
				txtMachineUnit.clear();
				txtPrimaryQty.clear();
				txtUnit.clear();
				cmbUnit.getItems().clear();
				tblKitDefinition.getItems().clear();
				kitDefenitionDtlList.clear();
				clearfield();
				clearkit();
				txtMinimumQty.clear();
				kitDefenitionDtl = new KitDefinitionDtl();
				// btnSubmit.setDisable(true);
			} else {
				notifyMessage(5, " Please add the Items!!!",false);
			}
			// btnAdd.setVisible(false);
			tblKitDefinition.getItems().clear();
			kitDefenitionmst = null;
			// kitDefenitionmst = new KitDefinitionMst();
			notifyMessage(5, "Saved",true);
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
	}

	@FXML
	void showItemPopUp(MouseEvent event) {
		// btnFetchDetails.setVisible(false);
		// btnAdd.setVisible(true);
		showStockitemPopup();

	}

	@FXML
	void showItemmstpopup(MouseEvent event) {

		// btnFetchDetails.setVisible(true);
		showItemMst();
	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtItemName.setText(itemPopupEvent.getItemName());
			txtItemBarcode.setText(itemPopupEvent.getBarCode());
			txtUnit.setText(itemPopupEvent.getUnitName());
			//version1.8
			ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller.getMultiUnitByItemId(itemPopupEvent.getItemId());
			cmbUnit.setValue(itemPopupEvent.getUnitName());
			multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());

			cmbUnit.getItems().add(itemPopupEvent.getUnitName());
			if (!multiUnitList.isEmpty()) {

				for (MultiUnitMst multiUniMst : multiUnitList) {

					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
					cmbUnit.getItems().add(getUnit.getBody().getUnitName());
				}

			} else {
//				cmbUnit.getItems().clear();
//				cmbUnit.getItems().add(itemPopupEvent.getUnitName());
//				cmbUnit.setPromptText(itemPopupEvent.getUnitName());
				cmbUnit.setValue(itemPopupEvent.getUnitName());
				txtUnit.setText(itemPopupEvent.getUnitName());
			}
			cmbUnit.setPromptText(itemPopupEvent.getUnitName());
			cmbUnit.setValue(itemPopupEvent.getUnitName());
			//txtUnitCode.setText(itemPopupEvent.getUnitName());not needed from previous version
			//version1.8ends
			
			//txtUnitCode.setText(itemPopupEvent.getUnitName());
			itemmst.setId(itemPopupEvent.getItemId());
			unitmst.setId(itemPopupEvent.getUnitId());
			itemMrp =  itemPopupEvent.getMrp();
			// txtQty.setText(Integer.toString(itemPopupEvent.getQty()));
				}
				
			});
		}

	}

	@Subscribe
	public void popupItemlistner(KitPopupEvent kitPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			// kitDefenitionmst = new KitDefinitionMst();
			txtKitName.setText(kitPopupEvent.getItemName());
			txtTaxRate.setText(Double.toString(kitPopupEvent.getTaxRate()));
			txtkitUnitName.setText(kitPopupEvent.getUnitName());
			txtkitBarcode.setText(kitPopupEvent.getBarCode());
			txtItemCode.setText(kitPopupEvent.getItemCode());
			itemkit.setId(kitPopupEvent.getItemId());
			unitmst.setId(kitPopupEvent.getUnitId());
			tblKitDefinition.getItems().clear();
			// kitDefenitionmst = null;
			// btnSubmit.setVisible(false);
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtKitName.getText());
			ResponseEntity<KitDefinitionMst> respentitykit = RestCaller.getKitByItemId(getItem.getBody().getId());
			if(null != respentitykit)
			{
			if(null != respentitykit.getBody())
			{
				kitDefenitionmst = respentitykit.getBody();
				ResponseEntity<List<KitDefinitionDtl>> getKit =RestCaller.getKidefinitionDtl(kitDefenitionmst);
				kitDefenitionDtlList = FXCollections.observableArrayList(getKit.getBody());
				Filltable();
				txtProductionCost.setText(Double.toString(kitDefenitionmst.getProductionCost()));
				txtMinimumQty.setText(Double.toString(kitDefenitionmst.getMinimumQty()));
				txtItemCode.setText(kitDefenitionmst.getItemCode());
//				clKitBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
//				clKitItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
//				clKitQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
//				clKitUnitCode.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
			}
			else
			{
//				txtkitBarcode.clear();
//				txtItemCode.clear();
//				txtkitUnitName.clear();
//				txtkitUnitName.clear();
	
				
				
				txtProductionCost.clear();
				txtMinimumQty.clear();
				kitDefenitionmst = null;
				
			}
			}
			else
			{
//				txtkitBarcode.clear();
//				txtItemCode.clear();
//				txtkitUnitName.clear();
//				txtkitUnitName.clear();
				
				
				
				
				txtProductionCost.clear();
				txtMinimumQty.clear();
				kitDefenitionmst = null;
			}
				}
			});
		}
	}

	private void showStockitemPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void Filltable() {
		tblKitDefinition.setItems(kitDefenitionDtlList);
		clKitBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
		clKitItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clKitQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		clKitUnitCode.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
	}

	private void clearkit() {
		txtKitName.setText("");
		txtkitBarcode.setText("");
		txtkitUnitName.setText("");
		txtProductionCost.setText("");
		txtTaxRate.setText("");
		txtkitBarcode.setText("");
		txtItemCode.setText("");

		txtMinimumQty.clear();
	}

	private void clearfield() {
		
		
		txtItemName.setText("");
		txtItemCode.setText("");
		txtQty.setText("");
		txtItemBarcode.setText("");
		cmbMixCategory.setValue("");
        productionCost = 0.0;
        //version1.8
		cmbUnit.setPromptText(null);
		cmbUnit.getSelectionModel().clearSelection();
		cmbUnit.getItems().clear();
		//cmbUnit.setValue(null);
		txtPrimaryQty.clear();
		txtUnit.clear();
		//txtUnitCode.setText(""); delete from previous ver
        //version1.8ends

	}
	
	private void showItemMst() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtMinimumQty.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

    
    
    @FXML
    void btnValidate(ActionEvent event) {
    	txtItemQtyInMachineUnit.clear();
    	txtMachineUnit.clear();
    	KitValidationReport kitValidationReport;
   ResponseEntity<ItemMst> responseEntity=RestCaller.getItemByNameRequestParam(txtKitName.getText());
   ResponseEntity<KitValidationReport> resp;
   if (txtKitName.getText().trim().isEmpty()) {
		notifyMessage(5, "Type KitName",false);
		txtKitName.requestFocus();
	
	
   }else {
	   
	   System.out.print(txtKitName.getText()+"kit name issssssssssssssssssssssssssssssssss");
       ItemMst itemMstOpt=responseEntity.getBody();
   	txtKitName.getText();

   	 resp=RestCaller.vailDateKit(txtKitName.getText());
   	kitValidationReport=resp.getBody();
   	if(null!=kitValidationReport.getQtyInMachineUnit()) {
   	txtItemQtyInMachineUnit.setText(String.valueOf(kitValidationReport.getQtyInMachineUnit()));
   	}
   	txtMachineUnit.setText(kitValidationReport.getMachineUnit());

   	if(kitValidationReport.getErrorMessage()!=null) {
   		notifyMessage(5,  kitValidationReport.getErrorMessage(),false);
   	}
   }
    }
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


  private void PageReload() {
  	
}
}
