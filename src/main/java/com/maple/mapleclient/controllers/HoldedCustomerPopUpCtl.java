package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.HoldedCustomerEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class HoldedCustomerPopUpCtl {
	
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	HoldedCustomerEvent holdedCustomerEvent;
	
	private ObservableList<SalesTransHdr> salesList = FXCollections.observableArrayList();
	SalesTransHdr salesTransHdr = null;
	StringProperty SearchString = new SimpleStringProperty();
    @FXML
    private TableView<SalesTransHdr> tblCustomer;
    @FXML
    private TableColumn<SalesTransHdr, String> clCustomerName;

    @FXML
    private TextField txtcustName;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;

	@FXML
	private void initialize() {
		LoadCustomerBySearch("");

		txtcustName.textProperty().bindBidirectional(SearchString);
		holdedCustomerEvent = new HoldedCustomerEvent();
		eventBus.register(this);
		btnOk.setDefaultButton(true);
		tblCustomer.setItems(salesList);

		btnCancel.setCache(true);
		clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());

		tblCustomer.setItems(salesList);
		tblCustomer.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getCustomerName() && newSelection.getCustomerName().length() > 0) {
					holdedCustomerEvent.setAddress(newSelection.getAccountHeads().getPartyAddress1());
					holdedCustomerEvent.setCustomerName(newSelection.getAccountHeads().getAccountName());
					holdedCustomerEvent.setCustomerId(newSelection.getCustomerId());
					holdedCustomerEvent.setHdrId(newSelection.getId());
					holdedCustomerEvent.setCustGst(newSelection.getAccountHeads().getPartyGst());
					holdedCustomerEvent.setCustPriceTYpe(newSelection.getAccountHeads().getPriceTypeId());

					eventBus.post(holdedCustomerEvent);
				}
			}
		});

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadCustomerBySearch((String) newValue);
			}
		});
	}
		@FXML
	    void cancelKeyPress(KeyEvent event) {

	    }

	    @FXML
	    void onCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
	    }

	    @FXML
	    void submit(ActionEvent event) {
	    	Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
	    }

	    @FXML
	    void submitOnKeypress(KeyEvent event) {

	    }
	    @FXML
	    void onKeypress(KeyEvent event) {

			if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnOk.getScene().getWindow();
				stage.close();
			}  else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

			} else {
				txtcustName.requestFocus();
			}
	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tblCustomer.requestFocus();
				tblCustomer.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnOk.getScene().getWindow();
				stage.close();
			}
	    }
	    private void LoadCustomerBySearch(String searchData) {

			/*
			 * This method populate the instance variable popUpItemList , which the source
			 * of data for the Table.
			 */
	    	salesList.clear();
			ResponseEntity<List<SalesTransHdr>>salessaves = RestCaller.getHoldedSales();
			salesList = FXCollections.observableArrayList(salessaves.getBody());
			for (SalesTransHdr sales: salesList)
			{
//				salesTransHdr = new SalesTransHdr();
				sales.setCustomerName(sales.getAccountHeads().getAccountName());
			}

	    }
	    @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
