package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ConsumptionReport;
import com.maple.report.entity.StockTransferOutReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;

public class StockTransferOutDetailReportCtl {
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<StockTransferOutReport> reportList = FXCollections.observableArrayList();

    @FXML
    private ComboBox<String> cmbToBranch;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtItemGroup;

    @FXML
    private ListView<String> lsGroup;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnAdd;

    @FXML
    private TableView<StockTransferOutReport> tbStockDtls;

    @FXML
    private TableColumn<StockTransferOutReport, String> clVoucherDate;

    @FXML
    private TableColumn<StockTransferOutReport, String> clVoucherNumber;

    @FXML
    private TableColumn<StockTransferOutReport, String> clToBranch;

    @FXML
    private TableColumn<StockTransferOutReport, String> clItemName;

    @FXML
    private TableColumn<StockTransferOutReport, String> clGroupName;

    @FXML
    private TableColumn<StockTransferOutReport, String> clBatch;

    @FXML
    private TableColumn<StockTransferOutReport, String> clItemCode;

    @FXML
    private TableColumn<StockTransferOutReport, String> clExpiryDate;

    @FXML
    private TableColumn<StockTransferOutReport, Number> clQty;

    @FXML
    private TableColumn<StockTransferOutReport, String> clUnit;

    @FXML
    private TableColumn<StockTransferOutReport, Number> clRate;

    @FXML
    private TableColumn<StockTransferOutReport,Number> clAmount;

    @FXML
 	private void initialize() 
    {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	ResponseEntity<List<BranchMst>> branchRestListResp = RestCaller.getBranchDtl();
		List branchRestList =branchRestListResp.getBody();
		Iterator itr1  = branchRestList.iterator();
		
		while (itr1.hasNext() ) {
			
			BranchMst lm = (BranchMst) itr1.next();
			if(!lm.getBranchCode().equalsIgnoreCase(SystemSetting.systemBranch))
			cmbToBranch.getItems().add(lm.getBranchCode());
		}
    }
    @FXML
    void actionAdd(ActionEvent event) {
    	lsGroup.getItems().add(txtItemGroup.getText());
    	lsGroup.getSelectionModel().select(txtItemGroup.getText());
    	txtItemGroup.clear();
    }

    @FXML
    void actionClear(ActionEvent event) {

    	lsGroup.getItems().clear();
    	dpFromDate.setValue(null);
    	dpToDate.setValue(null);
    	cmbToBranch.getSelectionModel().clearSelection();
    	tbStockDtls.getItems().clear();
    	reportList.clear();
    
    }

    @FXML
    void actionPrint(ActionEvent event) {
    	String tobranch="";
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		if(null != cmbToBranch.getSelectionModel().getSelectedItem())
		{
			tobranch = cmbToBranch.getSelectionModel().getSelectedItem();
		}
		List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	try {
    		JasperPdfReportService.StockTransferOutDetailReport(startDate,endDate,tobranch,cat);
    	} catch (JRException e) {
    		e.printStackTrace();
    	}
    }

    @FXML
    void actionShow(ActionEvent event) {
    	String tobranch="";
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		if(null != cmbToBranch.getSelectionModel().getSelectedItem())
		{
			tobranch = cmbToBranch.getSelectionModel().getSelectedItem();
		}
		List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
		ResponseEntity<List<StockTransferOutReport>> stkreport = RestCaller.getStockTransferOutReportBetweenDate(startDate,endDate,tobranch,cat);
		reportList = FXCollections.observableArrayList(stkreport.getBody());
		fillTable();
    }
    private void fillTable()
    {
    	tbStockDtls.setItems(reportList);
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getexpiryDateProperty());
		clGroupName.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
		clItemCode.setCellValueFactory(cellData -> cellData.getValue().getitemCodeProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameproperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getrateProperty());
		clToBranch.setCellValueFactory(cellData -> cellData.getValue().gettoBranchProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
    }
    @Subscribe
  		public void popupOrglistner(CategoryEvent CategoryEvent) {

  			Stage stage = (Stage) btnAdd.getScene().getWindow();
  			if (stage.isShowing()) {
  				txtItemGroup.setText(CategoryEvent.getCategoryName());
  			}
  		}
    @FXML
    void loadCategoryPopUp(MouseEvent event) {

    	try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			btnAdd.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
    
    

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}

}
