package com.maple.mapleclient.controllers;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.LinkedAccounts;
import com.maple.mapleclient.entity.PatientMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class LinkedAccountsCtl {
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<LinkedAccounts> accountHeadsList = FXCollections.observableArrayList();
	
	LinkedAccounts linkedAccount =  new LinkedAccounts();
	
	StringProperty SearchString = new SimpleStringProperty();
	
	 	@FXML
	    private TextField txtParentAccount;

	    @FXML
	    private TextField txtChildAccount;

	    @FXML
	    private Button btnJoin;

	    @FXML
	    private Label lblResponse;
	    
	    @FXML
	    private Button btnClear;
	    
	    @FXML
	    private Button btnShowAll;

	    @FXML
	    private TableView<LinkedAccounts> tblLinkedAccounts;

	    @FXML
	    private TableColumn<LinkedAccounts, String> clmnParent;

	    @FXML
	    private TableColumn<LinkedAccounts, String> clmnChild;
	    
	    @FXML
		private void initialize() {
	    	
	    	eventBus.register(this);
	    	txtParentAccount.textProperty().bindBidirectional(SearchString);
	    	SearchString.addListener(new ChangeListener() {

				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

					loadAccountHeads((String) newValue,"parent");
				}

			});
	    	
//	    	txtChildAccount.textProperty().bindBidirectional(SearchString);
//	    	SearchString.addListener(new ChangeListener() {
//
//				@Override
//				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
//					loadAccountHeads((String) newValue,"child");
//				}
//
//			});
	    	
	    }
	    
		private void loadAccountHeads(String newValue, String account) {
			tblLinkedAccounts.getItems().clear();
			ArrayList lnkAccount = new ArrayList();
			RestTemplate restTemplate = new RestTemplate();
			if(account == "parent") {
				lnkAccount = RestCaller.searchByParentAccount(newValue);
			}
			Iterator itr = lnkAccount.iterator();
			while (itr.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				Object parentAccount = lm.get("parentAccount");
				Object childAccount = lm.get("childAccount");
				Object id = lm.get("id");
				
				if (id != null) {
					LinkedAccounts lAccounts = new LinkedAccounts();

					
					try {
						lAccounts.setParentAccount((String) parentAccount);
						lAccounts.setChildAccount((String) childAccount);
						lAccounts.setId((String) id);
					
						accountHeadsList.add(lAccounts);
					tblLinkedAccounts.setItems(accountHeadsList);
					}catch(Exception e) {
						System.out.println(e.toString());
					}
					
					FillTable();
				}
			}
		}


	    @FXML
	    void Join(ActionEvent event) {
	    	if(null == txtParentAccount.getText())
	    	{
	    		notifyMessage(3, "Please select from account", false);
	    		return;
	    	}
	    	
	    	if(null == txtChildAccount.getText())
	    	{
	    		notifyMessage(3, "Please select to account", false);
	    		return;
	    	}

	    	ResponseEntity<AccountHeads> fromAccountHeadsResp = RestCaller.getAccountHeadByName(txtParentAccount.getText());
	    	AccountHeads fromAccountHeads = fromAccountHeadsResp.getBody();
	    	
	    	
	    	ResponseEntity<AccountHeads> toAccountHeadsResp = RestCaller.getAccountHeadByName(txtChildAccount.getText());
	    	AccountHeads toAccountHeads = toAccountHeadsResp.getBody();
	    	
	    	if(fromAccountHeads.getId().equalsIgnoreCase(toAccountHeads.getId())) {
	    		notifyMessage(3, "Please select another child account", false);
	    		return;
	    	}
	    	
	    	ResponseEntity<LinkedAccounts> linkedReply=RestCaller.getLinkedByParentAndChild(fromAccountHeads.getId(),toAccountHeads.getId());
	    	if(null != linkedReply.getBody()) {
	    		notifyMessage(3, "Duplicate Entry", false);
	    		return;
	    	}
	    	
	    	
	    	linkedAccount.setParentAccount(fromAccountHeads.getId());
	    	linkedAccount.setChildAccount(toAccountHeads.getId());
	    	linkedAccount.setBranch(SystemSetting.systemBranch);
	    	
	    	ResponseEntity<LinkedAccounts> linkedAccounts = RestCaller.saveLinkedAccounts(linkedAccount);
	    	if(null != linkedAccounts.getBody()) {
	    	accountHeadsList.add(linkedAccounts.getBody());
	    	}
	    	
	    	
	    	txtParentAccount.clear();
	    	txtChildAccount.clear();
	    	FillTable();
	    	showAll();
	    }

	    private void FillTable() {
	    	
	    	for (LinkedAccounts linkedAccounts : accountHeadsList) {
				if (null != linkedAccounts.getParentAccount()) {
					ResponseEntity<AccountHeads> parentNameAccount=RestCaller.getAccountHeadsById(linkedAccounts.getParentAccount());
					linkedAccounts.setParentAccountName(parentNameAccount.getBody().getAccountName());
				}
				if (null != linkedAccounts.getChildAccount()) {
					ResponseEntity<AccountHeads> childNameAccount=RestCaller.getAccountHeadsById(linkedAccounts.getChildAccount());
					linkedAccounts.setChildAccountName(childNameAccount.getBody().getAccountName());
				}
			}
	    	
	    	tblLinkedAccounts.setItems(accountHeadsList);
			clmnParent.setCellValueFactory(cellData -> cellData.getValue().getParentAccountProperty());
			clmnChild.setCellValueFactory(cellData -> cellData.getValue().getChildAccountProperty());
		}

		private void notifyMessage(int duration, String msg, boolean success) {
	    	Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

			
		}

		@FXML
	    void fromAccountPopup(KeyEvent event) {
			if (event.getCode() == KeyCode.ENTER) {
	    		fromAccountshowPopup();

			}
	    }

	    @FXML
	    void fromAccountPopupClicked(MouseEvent event) {
	    	fromAccountshowPopup();
	    }

	    @FXML
	    void toAccountPupup(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		toAccountshowPopup();

			}
	    }

	    @FXML
	    void toAccountPupupClicked(MouseEvent event) {
	    	toAccountshowPopup();
	    }
	    
	    @FXML
	    void showAll(ActionEvent event) {
	    	showAll();
	    }
	    
	    
	    private void showAll() {
	    	ResponseEntity<List<LinkedAccounts>> allLinkedAccountsList = RestCaller.finAllLinkedAccounts();
	    	if(null != allLinkedAccountsList.getBody()) {
		    	accountHeadsList = FXCollections.observableArrayList(allLinkedAccountsList.getBody());
		    	}
	    	FillTable();
			
		}

		private void fromAccountshowPopup() {

			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
				Parent root = loader.load();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				txtParentAccount.requestFocus();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		
		private void toAccountshowPopup() {

			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
				Parent root = loader.load();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				txtParentAccount.requestFocus();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		
		
		@Subscribe
		public void popuplistner(AccountEvent accountEvent) {

			System.out.println("------AccountEvent-------ParentAccount-------------");
			Stage stage = (Stage) txtParentAccount.getScene().getWindow();
			if (stage.isShowing()) {

				txtParentAccount.setText(accountEvent.getAccountName());
		
			}

		}
		
		
		@Subscribe
		public void popuplistner(AccountPopupEvent accountPopupEvent) {

			System.out.println("------AccountEvent-------ChildAccount-------------");
			Stage stage = (Stage) txtChildAccount.getScene().getWindow();
			if (stage.isShowing()) {

				txtChildAccount.setText(accountPopupEvent.getAccountName());

			}

		}
		
	    
	    
	    
	    @FXML
	    void clear(ActionEvent event) {
	    	txtParentAccount.clear();
	    	txtChildAccount.clear();
	    	accountHeadsList.clear();
	    	tblLinkedAccounts.getItems().clear();
	    }
}
