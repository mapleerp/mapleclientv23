package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ItemWiseTaxCreation {
	String taskid;
	String processInstanceId;
	private ObservableList<TaxMst> itemsListTable1= FXCollections.observableArrayList();

	TaxMst taxMst = null;
	private EventBus eventBus = EventBusFactory.getEventBus();
    @FXML
    private DatePicker dpStartDate;

    @FXML
    private TextField txtTaxRate;

    @FXML
    private Button btnSubmit;

    @FXML
    private Button btnShowAll;

    @FXML
    private ComboBox<String> cmbTaxType;

    @FXML
    private TextField txtCurrentTax;

    @FXML
    private Button btnFetchItems;

    @FXML
    private Button btnClear;

    @FXML
    private TextField txtItemName;

    @FXML
    private TableView<TaxMst> tblTax;

    @FXML
    private TableColumn<TaxMst, String> clItemName;

    @FXML
    private TableColumn<TaxMst, Number> clItemTax;

    @FXML
    private TableColumn<TaxMst, String> clTaxType;

    @FXML
    private TableColumn<TaxMst, String> clItemUnit;

    @FXML
    private TableColumn<TaxMst, String> clStartDate;

    @FXML
    private TableColumn<TaxMst, String> clEndDate;

	@FXML
	private void initialize() {
		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		eventBus.register(this);
		searchTaxType();
	}
    @FXML
    void FetchItems(ActionEvent event) {
    	tblTax.getItems().clear();
    	itemsListTable1.clear();
    	if(!txtItemName.getText().trim().isEmpty() && txtCurrentTax.getText().trim().isEmpty())
    	{
    		
    		getItemsByCategory();
    		
    	} else if (!txtCurrentTax.getText().trim().isEmpty()) {
    		
    		getItemsByCategoryAndTax();
			
		}
    	
    }
    private void searchTaxType()
    {
    	ArrayList allTaxType = new ArrayList();
    	allTaxType = RestCaller.SearchTaxTypes();
    	Iterator itr1 = allTaxType.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object taxName = lm.get("taxName");
			Object taxId = lm.get("taxId");
			if (taxId != null) {
				
				cmbTaxType.getItems().add((String)taxId);
	
			}
		}
    }
    
 private void getItemsByCategory() {
    	
    	if(!txtItemName.getText().trim().isEmpty() )
    	{
    		ResponseEntity<ItemMst> itemSaved = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    		
    		if(!cmbTaxType.getSelectionModel().isEmpty())
    		{
    			
    			ResponseEntity<List<TaxMst>> getTaxByTaxId = RestCaller.getAllTaxsByItemIdAndTaxId(itemSaved.getBody().getId(),cmbTaxType.getSelectionModel().getSelectedItem());
    			if(getTaxByTaxId.getBody().size()>0)
        		{
        			itemsListTable1 = FXCollections.observableArrayList(getTaxByTaxId.getBody());
        			for(TaxMst tax:itemsListTable1)
        			{
        				
        				ResponseEntity<ItemMst> getItem= RestCaller.getitemMst(tax.getItemId());
        				ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
        				tax.setItemName(getItem.getBody().getItemName());
        				tax.setUnitName(getUnit.getBody().getUnitName());
        				
        			}
        		}
    		}
    		else
    		{
    		
    		ResponseEntity<List<TaxMst>> getTax = RestCaller.getAllTaxsByItemId(itemSaved.getBody().getId());
    		if(getTax.getBody().size()>0)
    		{
    			itemsListTable1 = FXCollections.observableArrayList(getTax.getBody());
    			for(TaxMst tax:itemsListTable1)
    			{
    				
    				ResponseEntity<ItemMst> getItem= RestCaller.getitemMst(tax.getItemId());
    				ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
    				tax.setItemName(getItem.getBody().getItemName());
    				tax.setUnitName(getUnit.getBody().getUnitName());
    				
    			}
    		}
    		else if(null != itemSaved.getBody())
    		{
    			taxMst = new TaxMst();
    			taxMst.setItemName( itemSaved.getBody().getItemName());
    			ResponseEntity<UnitMst> unit = RestCaller.getunitMst( itemSaved.getBody().getUnitId());
    			taxMst.setUnitName(unit.getBody().getUnitName());
    			taxMst.setTaxId("");
    			taxMst.setTaxRate(itemSaved.getBody().getTaxRate());
    			taxMst.setItemId( itemSaved.getBody().getId());
    			taxMst.setEndDate(null);
    			taxMst.setEndDate(null);
    			itemsListTable1.add(taxMst);
    		
    		
    		}
    		}
    		fillTable();
    		}
    	
		
	}
    @FXML
    void ShowAll(ActionEvent event) {

    	ResponseEntity<List<TaxMst>> getTaxs= RestCaller.getAllTaxs();
    	itemsListTable1   = FXCollections.observableArrayList(getTaxs.getBody());
    	for(TaxMst tax:itemsListTable1)
		{
			
			ResponseEntity<ItemMst> getItem= RestCaller.getitemMst(tax.getItemId());
			ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
			tax.setItemName(getItem.getBody().getItemName());
			tax.setUnitName(getUnit.getBody().getUnitName());
			
		}
    	fillTable();
    	
    	
    }
    private void fillTable() {
    	
    	tblTax.setItems(itemsListTable1);
    	
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clItemTax.setCellValueFactory(cellData -> cellData.getValue().gettaxRateProperty());
    	clTaxType.setCellValueFactory(cellData -> cellData.getValue().gettaxIdProperty());
    	clItemUnit.setCellValueFactory(cellData -> cellData.getValue().getunitProperty());
    	clStartDate.setCellValueFactory(cellData -> cellData.getValue().getstartDateProperty());
    	clEndDate.setCellValueFactory(cellData -> cellData.getValue().getendDateProperty());
	}

    @FXML
    void Submit(ActionEvent event) 
    {
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(5,"Select Start Date",true);
    		return;
    	}
    	if(txtTaxRate.getText().trim().isEmpty())
    	{
    		notifyMessage(5, "please enter tax rate",false);
    	} else if (null == itemsListTable1) {
    		
    		notifyMessage(5, "please select items",false);
			
		}
    	else if(cmbTaxType.getSelectionModel().isEmpty())
{
    		
    		notifyMessage(5, "please select Tax Type",false);
			
		}
    	else {
			
				
				
								
					taxMst = new TaxMst();
					ResponseEntity<ItemMst>item = RestCaller.getItemByNameRequestParam(txtItemName.getText());
					ResponseEntity<UnitMst> unit = RestCaller.getunitMst(item.getBody().getUnitId());
				taxMst.setItemId(item.getBody().getId());
				taxMst.setTaxId(cmbTaxType.getSelectionModel().getSelectedItem());
				taxMst.setEndDate(null);
				String vNo= RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"TX");
				taxMst.setId(vNo);
				taxMst.setTaxRate(Double.parseDouble(txtTaxRate.getText()));
				java.util.Date startDate = Date.valueOf(dpStartDate.getValue());
				taxMst.setStartDate(startDate);
				
				ResponseEntity<TaxMst> respentity = RestCaller.saveTaxMst(taxMst);
				taxMst = respentity.getBody();
//				taxMst.setItemName(txtItemName.getText());
//				taxMst.setUnitName(unit.getBody().getUnitName());
//				itemsListTable1.add(taxMst);
//				
//				fillTable();
				cmbTaxType.getSelectionModel().clearSelection();
				getItemsByCategory();
				notifyMessage(5,"Saved",true);
				}
				taxMst = null;			
		
    }
    
private void getItemsByCategoryAndTax() {
	ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		if(!txtItemName.getText().trim().isEmpty()&& !txtCurrentTax.getText().trim().isEmpty() && null != cmbTaxType.getValue() && null != dpStartDate.getValue())
    	{
    		
    			
    			LocalDate dpDate1 = dpStartDate.getValue();
    			java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
    			 String  strDate = SystemSetting.UtilDateToString(uDate, "dd-MM-yyyy");
    		
     		ResponseEntity<List<TaxMst>> tacSaved = RestCaller.getItemsByCategoryAndTaxAndTaxId(getItem.getBody().getId(),cmbTaxType.getSelectionModel().getSelectedItem(),Double.parseDouble(txtCurrentTax.getText()),strDate);
    		
    		if(tacSaved.getBody().size()>0)
    		{
    		itemsListTable1 = FXCollections.observableArrayList(tacSaved.getBody());
    		for(TaxMst tax:itemsListTable1)
    		{
    			
    			
    			ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
    			tax.setItemName(getItem.getBody().getItemName());
    			tax.setUnitName(getUnit.getBody().getUnitName());
//    			itemsListTable1.add(tax);
    		}
    		fillTable();
    		}
    		
    	
    	}
		if(!txtCurrentTax.getText().trim().isEmpty() && !cmbTaxType.getSelectionModel().isEmpty())
		{
		
		
		ResponseEntity<List<TaxMst>> getTaxs = RestCaller.getTaxByRateTaxId(getItem.getBody().getId(),cmbTaxType.getSelectionModel().getSelectedItem(),Double.parseDouble(txtCurrentTax.getText()));
		if(getTaxs.getBody().size()>0)
		{
			itemsListTable1 = FXCollections.observableArrayList(getTaxs.getBody());
			for(TaxMst tax:itemsListTable1)
    		{
    			
    			
    			ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
    			tax.setItemName(getItem.getBody().getItemName());
    			tax.setUnitName(getUnit.getBody().getUnitName());
//    			itemsListTable1.add(tax);
    		}
    		fillTable();
    		}
		}
		else if(!txtCurrentTax.getText().trim().isEmpty() )
		{
			ResponseEntity<List<TaxMst>> getTaxs = RestCaller.getTaxByRate(getItem.getBody().getId(),Double.parseDouble(txtCurrentTax.getText()));
			if(getTaxs.getBody().size()>0)
			{
				itemsListTable1 = FXCollections.observableArrayList(getTaxs.getBody());
				for(TaxMst tax:itemsListTable1)
	    		{
	    			
	    			
	    			ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
	    			tax.setItemName(getItem.getBody().getItemName());
	    			tax.setUnitName(getUnit.getBody().getUnitName());
//	    			itemsListTable1.add(tax);
	    		}
	    		fillTable();
	    		}
		}
		
		
		
	}
    @FXML
    void actionClear(ActionEvent event) {
    	
    	txtCurrentTax.clear();
    	txtTaxRate.clear();
    	cmbTaxType.getSelectionModel().clearSelection();
    	dpStartDate.setValue(null);
    	tblTax.getItems().clear();
    	txtItemName.clear();

    }
    @Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnFetchItems.getScene().getWindow();
		if (stage.isShowing()) {
			
			txtItemName.setText(itemPopupEvent.getItemName());
			

		}
	}
    @FXML
    void loadItemPopUp(MouseEvent event) {
    	try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
