package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;

import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReturnDtl;
import com.maple.mapleclient.entity.SalesReturnHdr;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class SalesReturnCtl {
	
	String taskid;
	String processInstanceId;
	
	SalesTransHdr salesTransHdr = null;
	SalesReturnDtl salesReturnDtl = null;
	SalesDtl salesDtl =null;
	String custId = "";
	String sdate = null;
	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();
	private ObservableList<SalesReturnDtl> saleReturnListTable = FXCollections.observableArrayList();
	private ObservableList<SalesTransHdr> salesTransHdrList = FXCollections.observableArrayList();
	SalesReturnHdr salesReturnHdr = null;
	boolean customerIsBranch = false;
	ItemMst itemMst = null;

    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnFetchInvoice;

    @FXML
    private TableView<SalesTransHdr> tblInvoice;

    @FXML
    private TableColumn<SalesTransHdr, String> clVoucherNo;

    @FXML
    private TableColumn<SalesTransHdr, String> clCustomerName;

    @FXML
    private Button btnAdditem;

    @FXML
    private TextField txtQty;

    @FXML
    private TableView<SalesDtl> itemDetailTable;

    @FXML
    private TableColumn<SalesDtl, String> columnItemName;

    @FXML
    private TableColumn<SalesDtl, String> columnBarCode;

    @FXML
    private TableColumn<SalesDtl, String> columnQty;

    @FXML
    private TableColumn<SalesDtl, String> columnTaxRate;

    @FXML
    private TableColumn<SalesDtl, String> columnMrp;

    @FXML
    private TableColumn<SalesDtl, Number> clAmount;

    @FXML
    private TableColumn<SalesDtl, String> columnBatch;

    @FXML
    private TableColumn<SalesDtl, String> columnUnitName;

    @FXML
    private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

    @FXML
    private TableColumn<SalesDtl,String> clReturned;

    @FXML
    private TableColumn<SalesDtl, Number> clQty;
    @FXML
    private Button btnFinalSave;

    @FXML
    private TableView<SalesReturnDtl> tblSalesReturn;

    @FXML
    private TableColumn<SalesReturnDtl, String> clItemReturn;

    @FXML
    private TableColumn<SalesReturnDtl, String> clBarCodeReturn;

    @FXML
    private TableColumn<SalesReturnDtl, Number> clQtyReturn;

    @FXML
    private TableColumn<SalesReturnDtl, Number> clTaxRateReturn;

    @FXML
    private TableColumn<SalesReturnDtl, Number> clMrpReturn;

    @FXML
    private TableColumn<SalesReturnDtl, Number> clAmountReturn;

    @FXML
    private TableColumn<SalesReturnDtl, String> clBatchReturn;

    @FXML
    private TableColumn<SalesReturnDtl,String> clUnitReturn;

    @FXML
    private TableColumn<SalesReturnDtl, LocalDate> clExpiryReturn;

  
    @FXML
    private TextField txtSalesReturmBillAmount;

    @FXML
    private Button btnDelete;

    @FXML
    private Button BtnClear;

    @FXML
    private TextField gstNo;

    @FXML
    private TextField custname;

    @FXML
    private TextField custAdress;

    @FXML
    private TextField txtCashtopay;
    
	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		tblInvoice.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getVoucherNumber()) {
					itemDetailTable.getItems().clear();
					salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(newSelection.getVoucherNumber(), sdate);
					custname.setText(salesTransHdr.getAccountHeads().getAccountName());
					custAdress.setText(salesTransHdr.getAccountHeads().getPartyAddress1());
					gstNo.setText(salesTransHdr.getAccountHeads().getPartyGst());
					custId = salesTransHdr.getAccountHeads().getId();
					getSalesDtls();
				}
			}
		});
		
		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					txtQty.setText(String.valueOf(newSelection.getQty()));
				salesDtl = new SalesDtl();
				ResponseEntity<SalesDtl> salesDtlResp = RestCaller.getSalesDtlBydtlId(newSelection.getId());
				salesDtl = salesDtlResp.getBody();
				}
			}
		});
		tblSalesReturn.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
				salesReturnDtl = new SalesReturnDtl();
				salesReturnDtl.setId(newSelection.getId());
				}
			}
			
		});

		
	}
	private void getSalesDtls() {
		tblSalesReturn.getItems().clear();
		itemDetailTable.getItems().clear();
		saleListTable = null;
		ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
		System.out.println(SalesDtlResponse.getBody());
		saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
		
		FillTable();
	}
	
	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		clReturned.setCellValueFactory(cellData -> cellData.getValue().getReturnedProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getreturnedQtyProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
		if (null != summary.getTotalAmount()) {
			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
		}

	}

    @FXML
    void CustomerPopUp(MouseEvent event) {

    }

    @FXML
    void FetchInvoice(ActionEvent event) {
    	fetchinvoice();
    }

    @FXML
    void FetchInvoiceOnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			fetchinvoice();
		}
    }
    
    private void fetchinvoice() {
		tblInvoice.getItems().clear();
		tblSalesReturn.getItems().clear();
		itemDetailTable.getItems().clear();
		saleListTable = null;
		custname.clear();
		custAdress.clear();
		gstNo.clear();
		if (null == dpDate.getValue()) {
			
			notifyMessage(5, "please select voucher number", false);
			return;

		} else {

			java.util.Date date = SystemSetting.localToUtilDate(dpDate.getValue());
			sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

			ArrayList invoice = new ArrayList();
			invoice = RestCaller.getWholeSaleInvoiceBill(sdate);

			String voucherNo = "";
			String customerName = "";
			String salesmanId = "";
			Iterator itr = invoice.iterator();

			while (itr.hasNext()) {
				List element = (List) itr.next();
				voucherNo = (String) element.get(0);
				customerName = (String) element.get(1);
				salesmanId = (String) element.get(2);
				
				if (null != voucherNo) {

					SalesTransHdr salesTransHdr = new SalesTransHdr();
					salesTransHdr.setVoucherNumber(voucherNo);
					salesTransHdr.setCustomerName(customerName);
					salesTransHdr.setSalesManId(salesmanId);
					salesTransHdrList.add(salesTransHdr);

				}
			}
			
			FillInvoiceTable();

		}

	}
	private void FillInvoiceTable() {

		tblInvoice.setItems(salesTransHdrList);
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
		clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
	}

    @FXML
    void addItemButtonClick(ActionEvent event) {
    	returnItem();
    }
    private void returnItem() {
    	
    	Double qty = 0.0;
		if(txtQty.getText().trim().isEmpty())
		{
			notifyMessage(5, "please enter Qty", false);
			return;
		} else if (null == salesTransHdr) {
			notifyMessage(5, "please select Item", false);
			return;
		} else {
			
			if(null == salesDtl)
			{
				return;
			}
			
			if(null != salesDtl.getReturnedQty())
			{
			
			if(salesDtl.getQty() <= salesDtl.getReturnedQty())
			{
				notifyMessage(5, "Item already Returned", false);
				return;

			}
			
			qty = salesDtl.getReturnedQty();
			}
			

		
			if(null == salesReturnHdr)
			{
				salesReturnHdr = new SalesReturnHdr();
				salesReturnHdr.setInvoiceAmount(0.0);

				salesReturnHdr.setCustomerId(custId);
				salesReturnHdr.setIsBranchSales(salesTransHdr.getIsBranchSales());

				ResponseEntity<AccountHeads> customerResponce = RestCaller.getAccountHeadsById(custId);

				AccountHeads customerMst = customerResponce.getBody();
				salesReturnHdr.setAccountHeads(customerMst);

				/*
				 * If Customer Has a valid GST , then B2B Invoice , otherwise its B2C
				 */
				if (null==customerMst.getPartyGst() || customerMst.getPartyGst().length() < 14) {
					salesReturnHdr.setSalesMode("B2C");
				} else {
					salesReturnHdr.setSalesMode("B2B");
				}

				salesReturnHdr.setCreditOrCash("CREDIT");
				salesReturnHdr.setUserId(SystemSetting.getUser().getId());
				salesReturnHdr.setBranchCode(SystemSetting.systemBranch);
				salesReturnHdr.setSalesManId(salesTransHdr.getSalesManId());
				salesReturnHdr.setSalesVoucherNumber(salesTransHdr.getVoucherNumber());
				salesReturnHdr.setSalesTransHdr(salesTransHdr);
				
				ResponseEntity<SalesReturnHdr> respentity = RestCaller.saveSalesReturnHdr(salesReturnHdr);
				salesReturnHdr = respentity.getBody();
			}
			
			qty = qty + Double.parseDouble(txtQty.getText());
			ResponseEntity<SalesReturnDtl> salesDtlResp = RestCaller.findBySalesDtl(salesReturnHdr.getId(),salesDtl);
			SalesReturnDtl salesReturnDtlQty = salesDtlResp.getBody();
			if(null != salesReturnDtlQty)
			{
				qty = qty + salesReturnDtlQty.getQty();
			}
			
			if(salesDtl.getQty() < qty)
			{
				notifyMessage(5, "Please check qty", false);
				txtQty.requestFocus();
				return;
			}
			salesReturnDtl = new SalesReturnDtl();
			salesReturnDtl.setItemId(salesDtl.getItemId());
			salesReturnDtl.setBarcode(salesDtl.getBarcode());
			salesReturnDtl.setBatch(salesDtl.getBatch());
			
			salesReturnDtl.setQty(Double.parseDouble(txtQty.getText()));
			salesReturnDtl.setMrp(salesDtl.getMrp());
			Double mrpRateIncludingTax = 00.0;
			mrpRateIncludingTax = salesDtl.getMrp();
			
			
			Double taxRate = 00.0;
			ResponseEntity<ItemMst> respsentity = RestCaller.getitemMst(salesDtl.getItemId()); // itemmst =
			ItemMst item = respsentity.getBody();
			salesReturnDtl.setTaxRate(item.getTaxRate());
			taxRate = item.getTaxRate();
			salesReturnDtl.setUnitId(item.getUnitId());
			
			ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());
			salesReturnDtl.setUnitName(unitMst.getBody().getUnitName());
			ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesReturnDtl.getItemId());
			if (getTaxMst.getBody().size() > 0) 
			{
				for (TaxMst taxMst : getTaxMst.getBody()) {

					String companyState = SystemSetting.getUser().getCompanyMst().getState();
					String customerState = "KERALA";
					try {
						customerState = salesTransHdr.getAccountHeads().getCustomerState();
					} catch (Exception e) {

					}

					if (null == customerState) {
						customerState = "KERALA";
					}

					if (null == companyState) {
						companyState = "KERALA";
					}

					if (customerState.equalsIgnoreCase(companyState)) {
						if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
							salesReturnDtl.setCgstTaxRate(taxMst.getTaxRate());
//							BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setCgstAmount(CgstAmount.doubleValue());
						}
						if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
							salesReturnDtl.setSgstTaxRate(taxMst.getTaxRate());
//							BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setSgstAmount(SgstAmount.doubleValue());
						}
						salesReturnDtl.setIgstTaxRate(0.0);
						salesReturnDtl.setIgstAmount(0.0);
					} else {
						if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
							salesReturnDtl.setCgstTaxRate(0.0);
							salesReturnDtl.setCgstAmount(0.0);
							salesReturnDtl.setSgstTaxRate(0.0);
							salesReturnDtl.setSgstAmount(0.0);

							salesReturnDtl.setIgstTaxRate(taxMst.getTaxRate());
//								BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//								salesDtl.setIgstAmount(igstAmount.doubleValue());
						}
					}
					if (salesReturnHdr.getSalesMode().equalsIgnoreCase("B2C")) {
						if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
							salesReturnDtl.setCessRate(taxMst.getTaxRate());
//							BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setCessAmount(cessAmount.doubleValue());
						}
					}
					if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
						salesReturnDtl.setAddCessRate(taxMst.getTaxRate());
//						BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//								Double.valueOf(txtRate.getText()));
//						salesReturnDtl.setAddCessAmount(cessAmount.doubleValue());
					}

				}
				Double rateBeforeTax = (100 * mrpRateIncludingTax)
						/ (100 + salesReturnDtl.getIgstTaxRate() + salesReturnDtl.getCessRate() + salesReturnDtl.getAddCessRate()
								+ salesReturnDtl.getSgstTaxRate() + salesReturnDtl.getCgstTaxRate());
				salesReturnDtl.setRate(rateBeforeTax);
				BigDecimal igstAmount = RestCaller.TaxCalculator(salesReturnDtl.getIgstTaxRate(), rateBeforeTax);
				salesReturnDtl.setIgstAmount(igstAmount.doubleValue() * salesReturnDtl.getQty());
				BigDecimal SgstAmount = RestCaller.TaxCalculator(salesReturnDtl.getSgstTaxRate(), rateBeforeTax);
				salesReturnDtl.setSgstAmount(SgstAmount.doubleValue() * salesReturnDtl.getQty());
				BigDecimal CgstAmount = RestCaller.TaxCalculator(salesReturnDtl.getCgstTaxRate(), rateBeforeTax);
				salesReturnDtl.setCgstAmount(CgstAmount.doubleValue() * salesReturnDtl.getQty());
				BigDecimal cessAmount = RestCaller.TaxCalculator(salesReturnDtl.getCessRate(), rateBeforeTax);
				salesReturnDtl.setCessAmount(cessAmount.doubleValue() * salesReturnDtl.getQty());
				BigDecimal addcessAmount = RestCaller.TaxCalculator(salesReturnDtl.getAddCessRate(), rateBeforeTax);
				salesReturnDtl.setAddCessAmount(addcessAmount.doubleValue() * salesReturnDtl.getQty());
			}

			else
			{
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
			salesReturnDtl.setRate(rateBeforeTax);
			
			
			double sgstTaxRate = taxRate/2;
			double cgstTaxRate = taxRate/2;
			salesReturnDtl.setCgstTaxRate(cgstTaxRate );
			
			salesReturnDtl.setSgstTaxRate(sgstTaxRate);
			double cessAmount  =0.0;
			double cessRate = 0.0; 
			if(salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if(item.getCess()>0) {
					cessRate  =item.getCess() ;
					
					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate+item.getCess());
					
					System.out.println("rateBeforeTax---------"+rateBeforeTax);
					salesReturnDtl.setRate(rateBeforeTax);
					
					cessAmount =  salesReturnDtl.getQty() * salesReturnDtl.getRate() * item.getCess() /100;
					
					
					/*
					 * Recalculate RateBefore Tax if Cess is applied
					 */
					

					
				}
			}else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}
			
			salesReturnDtl.setCessRate(cessRate);
			salesReturnDtl.setCessAmount(cessAmount);
			
			String companyState = SystemSetting.getUser().getCompanyMst().getState();

			String customerState = "KERALA";
			try {
				customerState = salesTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {

			}
			
			if(null==customerState) {
				customerState = "KERALA";
			}
			
			if(null==companyState) {
				companyState = "KERALA";
			}

			if (  customerState.equalsIgnoreCase(companyState)) {
				salesReturnDtl.setSgstTaxRate(taxRate / 2);

				salesReturnDtl.setCgstTaxRate(taxRate / 2);

				salesReturnDtl.setCgstAmount(salesReturnDtl.getCgstTaxRate() * salesReturnDtl.getQty() * salesReturnDtl.getRate() / 100);

				salesReturnDtl.setSgstAmount(salesReturnDtl.getSgstTaxRate() * salesReturnDtl.getQty() * salesReturnDtl.getRate() / 100);

				salesReturnDtl.setIgstTaxRate(0.0);
				salesReturnDtl.setIgstAmount(0.0);

			} else {
				salesReturnDtl.setSgstTaxRate(0.0);

				salesReturnDtl.setCgstTaxRate(0.0);

				salesReturnDtl.setCgstAmount(0.0);

				salesReturnDtl.setSgstAmount(0.0);

				salesReturnDtl.setIgstTaxRate(taxRate);
				salesReturnDtl.setIgstAmount(salesReturnDtl.getIgstTaxRate() *salesReturnDtl.getQty() * salesReturnDtl.getRate() / 100);

				
			}
			}
			BigDecimal settoamount = new BigDecimal(Double.parseDouble(txtQty.getText())* salesDtl.getMrp());
			settoamount = settoamount.setScale(2, BigDecimal.ROUND_CEILING);
			salesReturnDtl.setAmount(settoamount.doubleValue());
			salesReturnDtl.setSalesReturnHdr(salesReturnHdr);
			salesReturnDtl.setSalesTransHdr(salesTransHdr);
			salesReturnDtl.setSales_dtl_id(salesDtl.getId());
			ResponseEntity<SalesReturnDtl> respentity = RestCaller.saveSalesReturnDtl(salesReturnDtl);
			salesReturnDtl = respentity.getBody();
			ResponseEntity<List<SalesReturnDtl>> salesReturnSaved = RestCaller.getSalesReturnDtl(salesReturnHdr.getId());
	    	saleReturnListTable = FXCollections.observableArrayList(salesReturnSaved.getBody());
	    	for (SalesReturnDtl salesRetnDtl: saleReturnListTable)
	    	{
	    		ResponseEntity<ItemMst> items = RestCaller.getitemMst(salesRetnDtl.getItemId());
	    		salesRetnDtl.setItemName(items.getBody().getItemName());
	    	}
	    	fillsalesReturnTable();
		}
	}

    private void fillsalesReturnTable()
    {
    	
    	
    	tblSalesReturn.setItems(saleReturnListTable);
    	clAmountReturn.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clBarCodeReturn.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
    	clBatchReturn.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
    	clExpiryReturn.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
    	clItemReturn.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clMrpReturn.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
    	clQtyReturn.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clTaxRateReturn.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
    	clUnitReturn.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
    	Summary summary = RestCaller.getSalesReturnWindowSummary(salesReturnHdr.getId());
		if (null != summary.getTotalAmount())
		{
			salesReturnHdr.setCashPay(summary.getTotalAmount());
		BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);

		txtSalesReturmBillAmount.setText(bdCashToPay.toPlainString());
		}
		else {
			txtSalesReturmBillAmount.setText("");
		}
    }
    @FXML
    void clearFields(ActionEvent event) {
    	
    	saleListTable = null; 
    	itemDetailTable.getItems().clear();
    	tblInvoice.getItems().clear();
    	tblSalesReturn.getItems().clear();

    }

    @FXML
    void deleteItem(ActionEvent event) {
    	

    	if(null !=salesReturnDtl )
    	{
    		if(null != salesReturnDtl.getId())
    		{
    			RestCaller.DeleteSalesReturnDtl( salesReturnDtl.getId());
    			ResponseEntity<List<SalesReturnDtl>> salesReturnSaved = RestCaller.getSalesReturnDtl(salesReturnHdr.getId());
    	    	saleReturnListTable = FXCollections.observableArrayList(salesReturnSaved.getBody());
    	    	for (SalesReturnDtl salesRetnDtl: saleReturnListTable)
    	    	{
    	    		ResponseEntity<ItemMst> items = RestCaller.getitemMst(salesRetnDtl.getItemId());
    	    		salesRetnDtl.setItemName(items.getBody().getItemName());
    	    	}
    	    	fillsalesReturnTable();
    	    	notifyMessage(5,"Deleted");
    		}
    	}
    }

    @FXML
    void finalSave(ActionEvent event) {
    	
    
    	finalSave();
    }

    @FXML
    void saveOnKey(KeyEvent event) {
    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
    		finalSave();
    	}
    }
private void finalSave()
{
	ResponseEntity<List<SalesReturnDtl>> salesReturnSaved = RestCaller.getSalesReturnDtl(salesReturnHdr.getId());
	saleReturnListTable = FXCollections.observableArrayList(salesReturnSaved.getBody());
	for (SalesReturnDtl salesRetnDtl: saleReturnListTable)
	{
		salesDtl = new SalesDtl();
		ResponseEntity<SalesDtl> resp = RestCaller.getSalesDtlBydtlId(salesRetnDtl.getSales_dtl_id());
		salesDtl = resp.getBody();
		Double returnedQty = salesRetnDtl.getQty();
		if(null != salesDtl.getReturnedQty())
		{
			returnedQty = returnedQty + salesDtl.getReturnedQty();
		}
		salesDtl.setReturnedQty(returnedQty);
		RestCaller.updateSalesDtlByReturn(salesDtl);
	}
	String vno=RestCaller.getVoucherNumber("SALESRETRN");

	
	salesReturnHdr.setVoucherDate(SystemSetting.systemDate);
	//salesReturnHdr.setVoucherDate(SystemSetting. SystemSetting.systemDate);
	salesReturnHdr.setVoucherNumber(vno);
	salesReturnHdr.setInvoiceAmount(Double.parseDouble(txtSalesReturmBillAmount.getText()));
	RestCaller.updateSalesDtlByReturn(salesReturnHdr);
	 AccountReceivable accountReceivable = new AccountReceivable();
	  accountReceivable.setAccountId(custId);
	  ResponseEntity<AccountHeads> custentity = RestCaller.getAccountHeadsById(custId);
	  accountReceivable.setAccountHeads(custentity.getBody());
	  accountReceivable.setDueAmount(-Double.parseDouble(txtSalesReturmBillAmount.getText()));
	  accountReceivable.setBalanceAmount(0.0);
	  LocalDate dueDate = LocalDate.now().plusDays(custentity.getBody().getCreditPeriod());
	  accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
	  accountReceivable.setVoucherNumber(salesReturnHdr.getVoucherNumber());
	  accountReceivable.setSalesTransHdr(salesTransHdr);
	  accountReceivable.setRemark("SalesReturn");
	  accountReceivable.setVoucherDate(salesTransHdr.getVoucherDate());
	  accountReceivable.setPaidAmount(0.0);
	  
	  ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
	  DayBook dayBook = new DayBook();
		dayBook.setBranchCode(salesReturnHdr.getBranchCode());
		dayBook.setDrAccountName(custname.getText());
		dayBook.setDrAmount(salesReturnHdr.getInvoiceAmount());
		dayBook.setNarration(custname.getText() + salesReturnHdr.getVoucherNumber());
		dayBook.setSourceVoucheNumber(salesReturnHdr.getVoucherNumber());
		dayBook.setSourceVoucherType("SALES RETURN");
		dayBook.setCrAccountName("SALES RETURN ACCOUNT");
		dayBook.setCrAmount(salesReturnHdr.getInvoiceAmount());

		LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
		dayBook.setsourceVoucherDate(java.sql.Date.valueOf(rdate));
		ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
   	notifyMessage(5, "Returned!!!");
   	
   	Format formatter;
	formatter = new SimpleDateFormat("yyyy-MM-dd");
	String strDate = formatter.format(salesReturnHdr.getVoucherDate());

	try {
		JasperPdfReportService.SalesReturnTaxInvoiceReport(salesReturnHdr.getVoucherNumber(), strDate);
	} catch (JRException e) {
		e.printStackTrace();
	}
   	
   	
   	salesTransHdrList.clear();
   	saleListTable.clear();
   	txtCashtopay.clear();
   	txtQty.clear();
   	txtSalesReturmBillAmount.clear();
   	custentity = null;
   	custAdress.clear();
   	custname.clear();
   	gstNo.clear();
   	itemDetailTable.getItems().clear();
   	tblSalesReturn.getItems().clear();
   	salesDtl = null;
   	salesReturnDtl = null;
   	salesReturnHdr = null;
   	salesTransHdr = null;
}
    @FXML
    void onEnterCustPopup(KeyEvent event) {

    }

    @FXML
    void qtyKeyRelease(KeyEvent event) {

    }

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload(hdrId);
			}


	private void PageReload(String hdrId) {
		
	}

}
