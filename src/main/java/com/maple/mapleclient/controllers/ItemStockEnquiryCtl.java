package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import java.math.BigDecimal;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ItemStockEnquiryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;


public class ItemStockEnquiryCtl {
	String taskid;
	String processInstanceId;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	
	private ObservableList<ItemStockEnquiryReport> stockEnquiryReport = FXCollections.observableArrayList();



    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnPurchase;

    @FXML
    private Button btnSales;

    @FXML
    private TableView<ItemStockEnquiryReport> tblStockReport;

    @FXML
    private TableColumn<ItemStockEnquiryReport, String> clItemName;

    @FXML
    private TableColumn<ItemStockEnquiryReport, String> clVoucherNumber;

    @FXML
    private TableColumn<ItemStockEnquiryReport, String> clVoucherDate;

    @FXML
    private TableColumn<ItemStockEnquiryReport, Number> clQty;

    @FXML
    private TableColumn<ItemStockEnquiryReport, Number> clAmount;

    @FXML
    private TextField txtTotal;
    
    @FXML
	private void initialize() {
    	
		eventBus.register(this);

    	
    }

    @FXML
    void ItemPopup(MouseEvent event) {
    	
    	showPopup();

    }



	@FXML
    void PurchaseStock(ActionEvent event) {
		
		if(txtItemName.getText().trim().isEmpty())
		{
			notifyMessage(3, "Please select item name...!", false);
			return;
		}
		
		ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		ItemMst itemMst = itemResp.getBody();
		
		
		ResponseEntity<List<ItemStockEnquiryReport>> stockEnquiryReportResp = RestCaller.getItemStockEnquiryReport(itemMst.getId(),
				"PURCHASE");
		stockEnquiryReport = FXCollections.observableArrayList(stockEnquiryReportResp.getBody());
		
		FillTable();
    }

    private void FillTable() {

    	
    	tblStockReport.setItems(stockEnquiryReport);
    	
    	Double amount = 0.0;
    	
    	for(ItemStockEnquiryReport stock : stockEnquiryReport)
    	{
    		amount = amount+stock.getAmount();
    	}
    	
    	
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		
		BigDecimal bdCashToPay = new BigDecimal(amount);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		txtTotal.setText(bdCashToPay.toPlainString());


	}

	@FXML
    void SalesStock(ActionEvent event) {

		if(txtItemName.getText().trim().isEmpty())
		{
			notifyMessage(3, "Please select item name...!", false);
			return;
		}
		
		ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		ItemMst itemMst = itemResp.getBody();
		
		
		ResponseEntity<List<ItemStockEnquiryReport>> stockEnquiryReportResp = RestCaller.
				getItemStockEnquiryReport(itemMst.getId(),"SALES");
		stockEnquiryReport = FXCollections.observableArrayList(stockEnquiryReportResp.getBody());
		
		FillTable();
    }
    
    private void showPopup() {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
    
	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {
		
		txtItemName.setText(itemPopupEvent.getItemName());
	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
