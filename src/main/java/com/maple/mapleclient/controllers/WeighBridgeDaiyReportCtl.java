package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import javafx.scene.control.TextField;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.WeighBridgeWeights;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
public class WeighBridgeDaiyReportCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<WeighBridgeWeights> weighBridgeReportList = FXCollections.observableArrayList();


    @FXML
    private DatePicker dpStartDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtGrandTotal;
    
 
    @FXML
    private Button btnGenerateReport;

    @FXML
    private TableView<WeighBridgeWeights> tblReport;

    @FXML
    private TableColumn<WeighBridgeWeights, String> clVehicleNumber;

    @FXML
    private TableColumn<WeighBridgeWeights, String> clDate;

    @FXML
    private TableColumn<WeighBridgeWeights, String> clNetWeight;

    @FXML
    private TableColumn<WeighBridgeWeights, Number> clRate;
    @FXML
   	private void initialize() {
   		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
   		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
       }
    @FXML
    void FocusOnBtn(KeyEvent event) {

    }

    @FXML
    void FocusOnToDate(KeyEvent event) {

    }

    @FXML
    void GenerateReport(ActionEvent event) {

    	
    	java.util.Date uDate = Date.valueOf(dpStartDate.getValue());
 		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
 		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
 		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
 		//uDate = SystemSetting.StringToUtilDate(strDate,);
 		
    	ResponseEntity<List<WeighBridgeWeights>> weighBridgeReport = RestCaller.getWeighBridgeReport( strDate,strDate1);
    	weighBridgeReportList = FXCollections.observableArrayList(weighBridgeReport.getBody());
    	System.out.print(weighBridgeReportList.size()+"weigh bridge report size isssssssssssssssssssssssssssssss");
    	FillTable();
    	tblReport.setItems(weighBridgeReportList);
    	   
    	
    	weighBridgeReportList = FXCollections.observableArrayList(weighBridgeReport.getBody());
    	
    	FillTable();
    	tblReport.setItems(weighBridgeReportList);
    	System.out.print(weighBridgeReportList.size()+"weigh bridge report size isssssssssssssssssssssssssssssss");

    	   List<WeighBridgeWeights> weighBridgeReportLists=weighBridgeReport.getBody();
 
   System.out.print(weighBridgeReportList.size()+"grand total list size issssssssssss");
  
   
   Integer grandTotal=0;
   for(int i=0;i<weighBridgeReportList.size();i++) {
    		
    		
    		Integer total=weighBridgeReportList.get(i).getRate();
    		
    		grandTotal=total+grandTotal;
    		  
    	}
   System.out.print(grandTotal+"grand total isssssssssssssss");

   txtGrandTotal.setText(String.valueOf(grandTotal));
    }

    @FXML
    void KeyPressGenerateReport(KeyEvent event) {

    	java.util.Date uDate = Date.valueOf(dpStartDate.getValue());
 		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
 		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
 		String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
 		//uDate = SystemSetting.StringToUtilDate(strDate,);
 		
    	ResponseEntity<List<WeighBridgeWeights>> weighBridgeReport = RestCaller.getWeighBridgeReport( strDate,endDate);

    	
    	
    	weighBridgeReportList = FXCollections.observableArrayList(weighBridgeReport.getBody());
    	
    	FillTable();
    	tblReport.setItems(weighBridgeReportList);
    	System.out.print(weighBridgeReportList.size()+"weigh bridge report size isssssssssssssssssssssssssssssss");
 
    	  List<WeighBridgeWeights> weighBridgeReportLists=weighBridgeReport.getBody();
   
   System.out.print(weighBridgeReportList.size()+"grand total list size issssssssssss");
   Integer grandTotal=0;
   for(int i=0;i<weighBridgeReportList.size();i++) {
    		
    		
    		Integer total=weighBridgeReportList.get(i).getRate();
    		
    		grandTotal=total+grandTotal;
    	}
  
   Double grndTotal=Double.valueOf(grandTotal);
   txtGrandTotal.setText(String.valueOf(grndTotal));
    }

    @FXML
    void showItems(MouseEvent event) {

    }

    


	private void FillTable()
	{
		
		clVehicleNumber.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clNetWeight.setCellValueFactory(cellData -> cellData.getValue().getnetweightProperty());
    	clRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
    	
    	
	}
	
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}
	  
	

}
