package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.RetailSalesSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class RetailSalesSummaryReportCtl {

	
	
		private ObservableList<RetailSalesSummaryReport> retailSalesSummaryReportList = FXCollections.observableArrayList();
	 	
		@FXML
	    private DatePicker dpFrom;

	    @FXML
	    private DatePicker dpTo;

	    @FXML
	    private Button btnGenerate;

	    @FXML
	    private Button btnPrint;

	    @FXML
	    private TableView<RetailSalesSummaryReport> tblRetailSalesSummary;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, String> clmnInvoice;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, String> clmnVoucher;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, String> clmnCustomer;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, String> clmnPatient;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, String> clmnDoctor;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, Number> clmnBeforeGST;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, Number> clmnGST;
	    
	    @FXML
	    private TableColumn<RetailSalesSummaryReport, Number> clmnInvoiceAmount;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, Number> clmnTotalPaid;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, Number> clmnInsurance;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, Number> clmnCredit;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, Number> clmnCardReceipt;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, Number> clmnCashReceipt;

	    @FXML
	    private TableColumn<RetailSalesSummaryReport, String> clmnUser;

	    
	    EventBus eventBus = EventBusFactory.getEventBus();
	    @FXML
		private void initialize() {
//	    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
//	    	dpFromDate = SystemSetting.datePickerFormat(dpFrom, "dd/MMM/yyyy");
	    	eventBus.register(this);
	    	
	    	
	    	
	    	
	    }
	    
	    
	    @FXML
	    void generateReport(ActionEvent event) {

	    	Date fdate = SystemSetting.localToUtilDate(dpFrom.getValue());
	    	String fromDate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
	    	
	    	Date tdate = SystemSetting.localToUtilDate(dpTo.getValue());
	    	String toDate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");

	    	ResponseEntity<List<RetailSalesSummaryReport>> retailSalesSummaryReport1 = RestCaller.getRetailSalesSummaryReport(fromDate,toDate);
	    	retailSalesSummaryReportList = FXCollections.observableArrayList(retailSalesSummaryReport1.getBody());
			
			
			
			if (retailSalesSummaryReportList.size() > 0) {
				
				
				fillTable();
				dpFrom.getEditor().clear();;
				dpTo.getEditor().clear();
				
			}
	    
	    }

	    private void fillTable() {
	    	tblRetailSalesSummary.setItems(retailSalesSummaryReportList);
			clmnInvoice.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
			clmnVoucher.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumProperty());
			clmnCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
			clmnPatient.setCellValueFactory(cellData -> cellData.getValue().getPatientNameProperty());
			clmnDoctor.setCellValueFactory(cellData -> cellData.getValue().getDoctorNameProperty());
			clmnBeforeGST.setCellValueFactory(cellData -> cellData.getValue().getBeforeGSTProperty());
			clmnGST.setCellValueFactory(cellData -> cellData.getValue().getGstProperty());
			clmnInvoiceAmount.setCellValueFactory(cellData -> cellData.getValue().getInvoiceAmountProperty());
			clmnTotalPaid.setCellValueFactory(cellData -> cellData.getValue().getTotalPaidProperty());
			clmnInsurance.setCellValueFactory(cellData -> cellData.getValue().getInsuranceProperty());
			clmnCredit.setCellValueFactory(cellData -> cellData.getValue().getCreditProperty());
			clmnCardReceipt.setCellValueFactory(cellData -> cellData.getValue().getCardReceiptProperty());
			clmnCashReceipt.setCellValueFactory(cellData -> cellData.getValue().getCashReceiptProperty());
			clmnUser.setCellValueFactory(cellData -> cellData.getValue().getUserProperty());
			
		}


		@FXML
	    void printReport(ActionEvent event) {

			Date fdate = SystemSetting.localToUtilDate(dpFrom.getValue());
			String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
			
			Date tdate = SystemSetting.localToUtilDate(dpTo.getValue());
			String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
			

			  try { JasperPdfReportService.retailSalesSummaryREport(sfdate,
					  stdate); } catch (JRException e) { // TODO Auto-gesnerated catch block
			  e.printStackTrace(); }
	    }
}
