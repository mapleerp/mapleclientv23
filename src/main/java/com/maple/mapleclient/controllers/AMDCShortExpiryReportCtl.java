package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AMDCShortExpiryReport;
import com.maple.report.entity.BranchStockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class AMDCShortExpiryReportCtl {

	private ObservableList<AMDCShortExpiryReport> aMDCShortExpiryReportList = FXCollections.observableArrayList();
    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnPrintReport;

    @FXML
    private Button btnGenerate;

    @FXML
    private TableView<AMDCShortExpiryReport> tblshortexpiryreport;

    @FXML
    private TableColumn<AMDCShortExpiryReport, String> clgroupName;

    @FXML
    private TableColumn<AMDCShortExpiryReport, String> clitemName;

    @FXML
    private TableColumn<AMDCShortExpiryReport, String> clbatchCode;

    @FXML
    private TableColumn<AMDCShortExpiryReport, String> clitemCode;

    @FXML
    private TableColumn<AMDCShortExpiryReport, String> clexpiryDate;

    @FXML
    private TableColumn<AMDCShortExpiryReport, Number> clqty;

    @FXML
    private TableColumn<AMDCShortExpiryReport, Number> clrate;

    @FXML
    private TableColumn<AMDCShortExpiryReport, Number> clamount;

    @FXML
    private TableColumn<AMDCShortExpiryReport, String> cldaystoexpiry;

    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null == dpDate.getValue()) {

			notifyMessage(3, "please select date", false);
			dpDate.requestFocus();
			return;
		}

		

		java.util.Date uDate = Date.valueOf(dpDate.getValue());
		String date = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		
		ResponseEntity<List<AMDCShortExpiryReport>> aMDCShortExpiryReportResp = RestCaller
				.getAMDCShortExpiryReport(date);

		aMDCShortExpiryReportList = FXCollections.observableArrayList(aMDCShortExpiryReportResp.getBody());

		fillTable();
		
		
		try {
			JasperPdfReportService.AMDCShortExpiryReport(date);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    private void fillTable() {
    	tblshortexpiryreport.setItems(aMDCShortExpiryReportList);

		
		clgroupName.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
		clitemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clbatchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clitemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
		clexpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clqty.setCellValueFactory(cellData -> cellData.getValue().getQuantityProperty());
		clrate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clamount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());	
		cldaystoexpiry.setCellValueFactory(cellData -> cellData.getValue().getDaysToExpiryProperty());
	}
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	private void initialize() {

		tblshortexpiryreport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getItemName()) {

					
					fillTable();


				}
			}
		});

	}

	@FXML
    void PrintReport(ActionEvent event) {

    }

}