package com.maple.mapleclient.controllers;

import java.time.LocalDate;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;
import com.maple.report.entity.ProductConversionReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import net.sf.jasperreports.engine.JRException;

public class ProductConversionReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<ProductConversionReport> ProductConversionReportList = FXCollections.observableArrayList();

	 @FXML
	    private DatePicker dpToDate;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private TableView<ProductConversionReport> tbDetails;

    @FXML
    private TableColumn<ProductConversionReport, LocalDate> clDate;

    @FXML
    private TableColumn<ProductConversionReport, String> clFromItem;

    @FXML
    private TableColumn<ProductConversionReport,Number> clFromQty;

    @FXML
    private TableColumn<ProductConversionReport, String> clToItem;

    @FXML
    private TableColumn<ProductConversionReport, Number> clToQty;

    @FXML
    private TableColumn<ProductConversionReport, Number> clFrAmt;

    @FXML
    private TableColumn<ProductConversionReport, Number> clToAmt;

    @FXML
    private TextField txtDiffAmt;
    @FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    }
    @FXML
    void printReport(ActionEvent event) {
    	LocalDate dpfDate = dpFromDate.getValue();
		java.util.Date fDate  = SystemSetting.localToUtilDate(dpfDate);
		String  strFDate = SystemSetting.UtilDateToString(fDate, "yyyy-MM-dd");
    	
		LocalDate dpTDate =dpToDate.getValue();
		java.util.Date TDate  = SystemSetting.localToUtilDate(dpTDate);
		String  strTDate = SystemSetting.UtilDateToString(TDate, "yyyy-MM-dd");
		try {
			JasperPdfReportService.ProductConversionReport(SystemSetting.systemBranch, strFDate,strTDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }

    @FXML
    void showReport(ActionEvent event) {
    	LocalDate dpfDate = dpFromDate.getValue();
		java.util.Date fDate  = SystemSetting.localToUtilDate(dpfDate);
		String  strFDate = SystemSetting.UtilDateToString(fDate, "yyyy-MM-dd");
    	
		LocalDate dpTDate =dpToDate.getValue();
		java.util.Date TDate  = SystemSetting.localToUtilDate(dpTDate);
		String  strTDate = SystemSetting.UtilDateToString(TDate, "yyyy-MM-dd");
	   
		ResponseEntity<List<ProductConversionReport>> respentity = RestCaller.getProductConversionReport(strFDate,strTDate);
		ProductConversionReportList = FXCollections.observableArrayList(respentity.getBody());
    	fillTable();
    	
    }
    private void fillTable()
    {
    	tbDetails.setItems(ProductConversionReportList);
		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clFrAmt.setCellValueFactory(cellData -> cellData.getValue().getfromAmountProperty());
		clFromItem.setCellValueFactory(cellData -> cellData.getValue().getfromItemNameProperty());
		clFromQty.setCellValueFactory(cellData -> cellData.getValue().getfromQtyProperty());
		clToAmt.setCellValueFactory(cellData -> cellData.getValue().gettoAmountProperty());
		clToItem.setCellValueFactory(cellData -> cellData.getValue().gettoItemNameProperty());
		clToQty.setCellValueFactory(cellData -> cellData.getValue().gettoQtyProperty());

    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }

}
