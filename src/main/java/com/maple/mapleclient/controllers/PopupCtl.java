package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.keyvalue.core.KeyValueTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.Popup;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class PopupCtl {
	String taskid;
	String processInstanceId;

	SupplierPopupEvent supplierPopupEvent;

	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<AccountHeads> accountHeadsList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();
	

	@FXML
	private TextField txtname;

	@FXML
	private Button btnSubmit;

	@FXML
	private TableView<AccountHeads> tablePopupView;

	@FXML
	private TableColumn<AccountHeads, String> ColumnC1;

	@FXML
	private TableColumn<AccountHeads, String> ColumnC2;

	@FXML
	private Button btnCancel;

	@FXML
	void onCancel(ActionEvent event) {
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void initialize() {

		txtname.textProperty().bindBidirectional(SearchString);

		supplierPopupEvent = new SupplierPopupEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);

		btnCancel.setCache(true);

		// call supplier rest
		ColumnC1.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		ColumnC2.setCellValueFactory(cellData -> cellData.getValue().getPartyAddressProperty());

		ArrayList suppliers = new ArrayList();

		RestTemplate restTemplate = new RestTemplate();

		suppliers = RestCaller.SearchSupplierByName();

		Iterator itr = suppliers.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			// Iterator itr2 = lm.keySet().iterator();
			// while(itr2.hasNext()) {

			Object supName = lm.get("supplierName");
			Object supAdd = lm.get("address");
			Object supPh = lm.get("phoneNo");
			Object SupGST = lm.get("supGST");
			Object SupId = lm.get("id");

			if (null != SupId) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setAccountName((String) supName);
				accountHeads.setPartyAddress1((String) supAdd);
				accountHeads.setCustomerContact((String) supPh);
				accountHeads.setPartyGst((String) SupGST);
				accountHeads.setId((String) SupId);

				System.out.println(lm);

				accountHeadsList.add(accountHeads);
			}

		}
		tablePopupView.setItems(accountHeadsList);
		for (AccountHeads s : accountHeadsList) {
			System.out.println("@@ SupplierName @@" + s.getAccountName());
		}

		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getAccountName() && newSelection.getAccountName().length() > 0) {
					supplierPopupEvent.setSupplierName(newSelection.getAccountName());
					supplierPopupEvent.setSupplierAddress(newSelection.getPartyAddress1());
					supplierPopupEvent.setSupplierPhone(newSelection.getCustomerContact());
					supplierPopupEvent.setSupplierGST(newSelection.getPartyGst());
					supplierPopupEvent.setSupplierId(newSelection.getId());
//					eventBus.post(supplierPopupEvent);
				}
			}
		});

		/*
		 * Below code will do the searching based on typed string
		 */

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadSupplierBySearch((String) newValue);
			}
		});

	}

	@FXML
	void submit(ActionEvent event) {
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		eventBus.post(supplierPopupEvent);
		stage.close();

	}

	@FXML
	void OnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			eventBus.post(supplierPopupEvent);
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		} else {
			txtname.requestFocus();
		}
	}

	@FXML
	void OnKeyPressTxt(KeyEvent event) {
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
		if(event.getCode() == KeyCode.BACK_SPACE && txtname.getText().length()==0)
		{
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
	}

	private void LoadSupplierBySearch(String searchData) {

		accountHeadsList.clear();
		ArrayList suppliers = new ArrayList();
		suppliers = RestCaller.SearchSupplierByName(searchData);

		Iterator itr = suppliers.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			// Iterator itr2 = lm.keySet().iterator();
			// while(itr2.hasNext()) {

			Object supName = lm.get("supplierName");
			Object supAdd = lm.get("address");
			Object supPh = lm.get("phoneNo");
			Object SupGST = lm.get("supGST");

			if (null != supName) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setAccountName((String) supName);
				accountHeads.setPartyAddress1((String) supAdd);
				accountHeads.setCustomerContact((String) supPh);
				accountHeads.setPartyGst((String) SupGST);

				System.out.println(lm);

				accountHeadsList.add(accountHeads);
			}

		}

	}
	 @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }
}
