package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchCategoryMst;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ReceiptInvoice;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class BranchCategoryCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<BranchCategoryMst> branchCategoryTable = FXCollections.observableArrayList();
	
	 BranchCategoryMst branchCategoryMst; 
	private EventBus eventBus = EventBusFactory.getEventBus();
	@FXML
	private TableView<BranchCategoryMst> tblBranchCategory;

	@FXML
	private TableColumn<BranchCategoryMst, String> clBranch;

	@FXML
	private TableColumn<BranchCategoryMst, String> clCategory;

	@FXML
	private TextField txtCategory;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnShowAll;

	@FXML
	private Button btnDelete;

	@FXML
	private ComboBox<String> cmbBranch;

	@FXML
	void AddItem(ActionEvent event) {

		save();

	}

	@FXML
	void DeleteItem(ActionEvent event) {
		
		
           if(null != branchCategoryMst)
           {
    	
    		if(null != branchCategoryMst.getId())
    		{
    			RestCaller.deleteBranchCategoryMst(branchCategoryMst.getId());
    			showAll();
    		}
           }


	}

	@FXML
	void ShowAllItems(ActionEvent event) {

		showAll();
	}

	@FXML
	void addOnEnter(KeyEvent event) {
		save();
	}

	@FXML
	void saveOnKey(KeyEvent event) {

	}

	private void setBranches() {

		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();

		for (BranchMst branchMst : branchMstList) {
			cmbBranch.getItems().add(branchMst.getBranchCode());

		}

	}

	@FXML
	void categorypopup(MouseEvent event) {
		loadCategoryPopup();
	}

	private void loadCategoryPopup() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void categoryOnEnter(KeyEvent event) {
		loadCategoryPopup();
	}

	@FXML
	private void initialize() {

		eventBus.register(this);
		setBranches();
		
		
		tblBranchCategory.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					branchCategoryMst = new BranchCategoryMst();
					branchCategoryMst.setId(newSelection.getId());
				}
			}
		});

	}

	@Subscribe
	public void popupOrglistner(CategoryEvent CategoryEvent) {

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtCategory.setText(CategoryEvent.getCategoryName());

		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	public void save() {

		if (null == cmbBranch.getValue()) {
			notifyMessage(5, "Please select branch code", false);
			return;
		}
		
		if(txtCategory.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please select category", false);
			return;
		}

		BranchCategoryMst branchCategoryMst = new BranchCategoryMst();

		branchCategoryMst.setBranchCode(cmbBranch.getSelectionModel().getSelectedItem());

		ResponseEntity<CategoryMst> category = RestCaller.getCategoryByName(txtCategory.getText());
		CategoryMst categoryMst = category.getBody();
		if (null == categoryMst) {
			notifyMessage(5, "Category not found", false);
			return;
		}
		branchCategoryMst.setCategoryId(category.getBody().getId());

		ResponseEntity<BranchCategoryMst> respEntity = RestCaller.saveBranchCategory(branchCategoryMst);
		txtCategory.clear();
		cmbBranch.getSelectionModel().clearSelection();
		{
			notifyMessage(5, "Saved ", true);

			showAll();
			return;
		}

	}

	public void showAll() {

		ResponseEntity<List<BranchCategoryMst>> respentity = RestCaller.getBranchCategory();
		branchCategoryTable = FXCollections.observableArrayList(respentity.getBody());
		fillTable();
	}

	private void fillTable() {
		tblBranchCategory.setItems(branchCategoryTable);
		
		for(BranchCategoryMst branch : branchCategoryTable)
		{
			if(null != branch.getCategoryId())
			{
				ResponseEntity<CategoryMst> categoryResp = RestCaller.getCategoryById(branch.getCategoryId());
				CategoryMst categoryMst = categoryResp.getBody();
				
				if(null != categoryMst)
				{
					branch.setCategoryName(categoryMst.getCategoryName());
				}
			}
			
			if(null!=branch.getBranchCode()) {
				BranchMst branchMst = RestCaller.getBranchDtls(branch.getBranchCode());
				if(null != branchMst) {
					branch.setBranchName(branchMst.getBranchName());
				}
			}
		}
		clBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
		clCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	       private void PageReload() {
	       	
	 }


}
