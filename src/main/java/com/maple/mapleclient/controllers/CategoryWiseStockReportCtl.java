package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import javafx.scene.control.TextField;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportCategoryWiseStockReportToExcel;
import com.maple.maple.util.ItemWiseStockReportToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockReport;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class CategoryWiseStockReportCtl {
	String taskid;
	String processInstanceId;

	
	private EventBus eventBus = EventBusFactory.getEventBus();
	List<StockReport>stockReportList= new ArrayList<StockReport>();
	ItemWiseStockReportToExcel itemWiseStockReportToExcel=new ItemWiseStockReportToExcel();
	ExportCategoryWiseStockReportToExcel exportCategoryWiseStockReportToExcel=new ExportCategoryWiseStockReportToExcel();
	List<StockReport> stockreportlist=new ArrayList<StockReport>();
    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnGenerate;
    
    
    @FXML
    private Button btnItemWiseStockReport;
   
    @FXML
    private TextField  txtCategory;
    

    @FXML
    private Button btnCategorywWiseToExcel;
    @FXML
	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	//setCategpry();
    }

    @FXML
    void getCategory(MouseEvent event) {
    	
  //  setCategpry();
    
    	
    }
	/*
	 * private void setCategpry() { cmbCategory.getItems().clear(); ArrayList cat =
	 * new ArrayList(); cat = RestCaller.SearchCategory(); Iterator itr1 =
	 * cat.iterator(); while (itr1.hasNext()) { LinkedHashMap lm = (LinkedHashMap)
	 * itr1.next(); Object categoryName = lm.get("categoryName"); Object id =
	 * lm.get("id"); if (id != null) { cmbCategory.getItems().add((String)
	 * categoryName); } } }
	 */

    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpDate.getValue())
    	{
    		notifyMessage(5, "Please select date", false);
			return;
    	}
    	if(txtCategory.getText().isEmpty())
    	{
    		notifyMessage(5, "Please select category", false);
    		return;
    	}
    	
		ResponseEntity<CategoryMst> category = RestCaller.getCategoryByName(txtCategory.getText());
		CategoryMst categoryMst = category.getBody();
		if(null == categoryMst)
		{
			notifyMessage(5, "Category not found", false);
    		return;
		}
    	Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
    	
    	String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
    	
    	try {
			JasperPdfReportService.DailyStockReport(date,categoryMst.getId());
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	
    	
    	
    }
    


	 private void loadCategoryPopup() {
			
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
    @FXML
    void itemWiseStockReportToExcel(ActionEvent event) {

    	if(null == dpDate.getValue())
    	{
    		notifyMessage(5, "Please select date", false);
			return;
    	}
    	Date udate = SystemSetting.localToUtilDate((dpDate.getValue()));
    	String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
    	
    	
    	String userBranch=SystemSetting.systemBranch;
		
    	ArrayList stockreportlist = new ArrayList();
    	stockreportlist=RestCaller.getItemWiseStock(userBranch,date);
    
    	
		if(null == stockreportlist)
		{
			notifyMessage(5, " not data found", false);
    		return;
		}

		Iterator itr = stockreportlist.iterator();


		while (itr.hasNext()) {
		
			 LinkedHashMap element = (LinkedHashMap) itr.next();
			 StockReport report =new StockReport();
			 System.out.println("accaccaccacc33"+element.get("id"));
			 report.setItemName((String) element.get("itemName"));
		
		
			report.setQty((Double) element.get("qty"));
			report.setStandardPrice((Double) element.get("standardPrice"));
			report.setBranchName((String) element.get("branchName"));
			stockReportList.add(report);
		
		}
			if(stockReportList.size()>0) {
		itemWiseStockReportToExcel.exportToExcel("ItemWiseStockToExcel"+date+".xls", stockReportList,date);
			}
			else {
				notifyMessage(5, " not data found", false);
	    		return;
			}
    }
    
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	  @FXML
	    void categoryWiseStockToExecl(ActionEvent event) {
		  stockReportList.clear();
		  if(null == dpDate.getValue())
	    	{
	    		notifyMessage(5, "Please select date", false);
				return;
	    	}
	    	if(txtCategory.getText().isEmpty())
	    	{
	    		notifyMessage(5, "Please select category", false);
	    		return;
	    	}
	    	
			ResponseEntity<CategoryMst> category = RestCaller.getCategoryByName(txtCategory.getText());
			CategoryMst categoryMst = category.getBody();
			if(null == categoryMst)
			{
				notifyMessage(5, "Category not found", false);
	    		return;
			}
	    	Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
	    	
	    	String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

			ResponseEntity<List<StockReport>> DailyStockReportResponse = RestCaller
					.getDailystockReport(SystemSetting.getSystemBranch(), date, categoryMst.getId());
			
			
			stockreportlist=DailyStockReportResponse.getBody();
			

      if(stockreportlist.size()>0) {
	
			exportCategoryWiseStockReportToExcel.exportToExcel("CategoryWiseStockToExcel"+date+".xls", stockreportlist,date);
	    }else {
	    	notifyMessage(5, " no data found", false);
    		return;
	    }
	    	
	    	

	  }
	  
	  @Subscribe
		public void popupOrglistner(CategoryEvent CategoryEvent) {

			Stage stage = (Stage) btnGenerate.getScene().getWindow();
			if (stage.isShowing()) {

				
				txtCategory.setText(CategoryEvent.getCategoryName());
		
			

			}
}

	    @FXML
	    void categoryPopUp(MouseEvent event) {

	    	loadCategoryPopup();
	    }
	    @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	     private void PageReload() {
	     	
	}
}
