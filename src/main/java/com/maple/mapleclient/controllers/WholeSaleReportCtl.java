package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SalesInvoiceReport;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class WholeSaleReportCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<SalesInvoiceReport> salesInvoiceReportList = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> salesDtlList = FXCollections.observableArrayList();


	@FXML
	private DatePicker dpStartDate;

	@FXML
	private DatePicker dpEndDate;

	@FXML
	private Button btnGenerateReport;

	@FXML
	private TableView<SalesInvoiceReport> tblHdrReport;

	@FXML
	private TableColumn<SalesInvoiceReport, String> clVoucherDate;

	@FXML
	private TableColumn<SalesInvoiceReport, String> clVoucherNumber;

	@FXML
	private TableColumn<SalesInvoiceReport, String> clCustomer;

	@FXML
	private TableColumn<SalesInvoiceReport, Number> clAmount;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnRate;
	@FXML
	private TableColumn<SalesDtl, String> columnMrp;
	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;
	@FXML
	private TableColumn<SalesDtl, Number> amount;
	
	@FXML
    private TextField txtTotal;


	@FXML
	private void initialize() {
		dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		tblHdrReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {

					String hdrId = newSelection.getId();
					
					SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(hdrId);
					
					ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

					salesDtlList = FXCollections.observableArrayList(respentityList.getBody());
					
					FillTable();


				}
			}
		});

	}
	
	private void FillTable() {

		itemDetailTable.setItems(salesDtlList);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		// columnCessRate.setCellValueFactory(cellData ->
		// cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

	}

	@FXML
	void GenerateReport(ActionEvent event) {

		if (null == dpStartDate.getValue()) {

			notifyMessage(3, "please select start date", false);
			dpStartDate.requestFocus();
			return;
		}

		if (null == dpEndDate.getValue()) {

			notifyMessage(3, "please select start date", false);
			dpEndDate.requestFocus();

			return;
		}

		java.util.Date uDate = Date.valueOf(dpStartDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		java.util.Date eDate = Date.valueOf(dpEndDate.getValue());
		String endDate = SystemSetting.UtilDateToString(eDate, "yyy-MM-dd");

		ResponseEntity<List<SalesInvoiceReport>> salesInvoiceReportResp = RestCaller
				.getWholeSaleReportBetweenDate(strDate, endDate);

		salesInvoiceReportList = FXCollections.observableArrayList(salesInvoiceReportResp.getBody());
		
		

		fillHdrReportTable();

	}

	private void fillHdrReportTable() {

		tblHdrReport.setItems(salesInvoiceReportList);
		
		Double total = 0.0;
		
		for(SalesInvoiceReport sales : salesInvoiceReportList)
		{
			total = total + sales.getAmount();
		}

		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherProperty());
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		
		
		BigDecimal bdCashToPay = new BigDecimal(total);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		
		System.out.println(bdCashToPay);

		txtTotal.setText(bdCashToPay.toPlainString());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
