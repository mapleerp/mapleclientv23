package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.OtherBranchSalesDtl;
import com.maple.mapleclient.entity.OtherBranchSalesTransHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.OtherBranchSalesEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class PurchaseAcknowledgeCtl {

	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<OtherBranchSalesDtl> otherBranchSalesDtlTable = FXCollections.observableArrayList();

	OtherBranchSalesTransHdr otherBranchSalesTransHdr = null;

	 @FXML
	    private Button btnClear;

	    @FXML
	    private TextField txtVoucherIn;

	    @FXML
	    private TextField txtFromBranch;

	    @FXML
	    private DatePicker dpIntentDate;

	    @FXML
	    private Button btnFetchData;

	    @FXML
	    private Button btnFinalSave;

	@FXML
	private TableView<OtherBranchSalesDtl> tblDtlReport;

	@FXML
	private TableColumn<OtherBranchSalesDtl, String> clItemName;

	@FXML
	private TableColumn<OtherBranchSalesDtl, String> clUnit;

	@FXML
	private TableColumn<OtherBranchSalesDtl, String> clQty;

	@FXML
	private TableColumn<OtherBranchSalesDtl, String> clBatch;

	@FXML
	private TableColumn<OtherBranchSalesDtl, Number> clAmount;

	@FXML
	void FromBranchKeyPress(KeyEvent event) {

	}

	@FXML
	void VoucherInKeyPress(KeyEvent event) {

	}

	@FXML
	private void initialize() {
		dpIntentDate = SystemSetting.datePickerFormat(dpIntentDate, "dd/MMM/yyyy");
		eventBus.register(this);

	}

	@FXML
	void clear(ActionEvent event) {
		txtFromBranch.clear();
		txtVoucherIn.clear();
		dpIntentDate.setValue(null);
		tblDtlReport.getItems().clear();
		otherBranchSalesDtlTable.clear();

		otherBranchSalesTransHdr = null;

	}

	@FXML
	void fetchData(ActionEvent event) {
		fetchDataResult();
	}

	private void fetchDataResult() {

		otherBranchSalesDtlTable.clear();
		tblDtlReport.getItems().clear();
		if (null == otherBranchSalesTransHdr) {
			notifyMessage(3, "Please select other branch sales...!", false);
			return;
		}

		if (null != otherBranchSalesTransHdr.getId()) {

			ResponseEntity<List<OtherBranchSalesDtl>> otherBranchdtlSaved = RestCaller
					.getOtherBranchSalesDtl(otherBranchSalesTransHdr.getId());

			otherBranchSalesDtlTable = FXCollections.observableArrayList(otherBranchdtlSaved.getBody());

			for (OtherBranchSalesDtl otherbranch : otherBranchSalesDtlTable) {
				try {

					OtherBranchSalesDtl otherBranchSalesDtl = new OtherBranchSalesDtl();

					otherBranchSalesDtl.setAmount(otherbranch.getAmount());
					otherBranchSalesDtl.setBatch(otherbranch.getBatch());
					ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(otherbranch.getItemId());
					ItemMst itemMst = respentity.getBody();
					if (null != itemMst) {
						otherBranchSalesDtl.setItemName(itemMst.getItemName());
					}
					otherBranchSalesDtl.setQty(otherbranch.getQty());

					ResponseEntity<UnitMst> unitsaved = RestCaller.getunitMst(otherbranch.getUnitId());
					UnitMst unitMst = unitsaved.getBody();

					if (null != unitMst) {
						otherBranchSalesDtl.setUnitName(unitMst.getUnitName());
					}

				} catch (Exception e) {

				}

			}
			FillTable();

		}

	}

	private void FillTable() {

		// logger.info("=============started fill table!!=============");
		tblDtlReport.setItems(otherBranchSalesDtlTable);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	}

	@FXML
	void finalSave(ActionEvent event) {
		// if (event.getCode() == KeyCode.S && event.isControlDown()) {
		// finalSave();
		// }
	}

	@FXML
	void VoucherInMouseClick(MouseEvent event) {

		loadPopup();

	}

	private void loadPopup() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/OtherBranchPopUp.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(OtherBranchSalesEvent otherBranchSalesEvent) {

		System.out.println("------OtherBranchSalesEvent-------popuplistner-------------" + otherBranchSalesEvent);
		Stage stage = (Stage) btnFetchData.getScene().getWindow();
		if (stage.isShowing()) {

			txtVoucherIn.setText(otherBranchSalesEvent.getVoucherNumber());
			txtFromBranch.setText(otherBranchSalesEvent.getFromBranchCode());

			dpIntentDate.setValue(SystemSetting.utilToLocaDate(otherBranchSalesEvent.getVoucherDate()));

			ResponseEntity<OtherBranchSalesTransHdr> otherBranchSalesTransHdrResp = RestCaller
					.OtherBranchSalesTransHdrById(otherBranchSalesEvent.getOtherBranchSalesHdrId());
			otherBranchSalesTransHdr = otherBranchSalesTransHdrResp.getBody();

		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

}
