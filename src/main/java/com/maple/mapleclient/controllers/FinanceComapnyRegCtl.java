package com.maple.mapleclient.controllers;

import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.FinanceMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class FinanceComapnyRegCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<FinanceMst> financeCompanyMstList = FXCollections.observableArrayList();

	FinanceMst financeCompanyMst = null;
	

    @FXML
    private TableView<FinanceMst> tblcust;

    @FXML
    private TableColumn<FinanceMst, String> CR1Name;

    @FXML
    private TableColumn<FinanceMst, String> CR3Contact;


    @FXML
    private Button custbtn;



 

    @FXML
    private TextField custContact;

 

    @FXML
    private TextField custName;

 

    @FXML
    private Button btnClear;

    @FXML
    private Button btnsearch;
    
	@FXML
	private void initialize() {
		
	}

    @FXML
    void Clearfields(ActionEvent event) {
    	
    	tblcust.getItems().clear();
    	financeCompanyMstList = null;
    	financeCompanyMst = null;
    	custName.clear();
    	custContact.clear();
    	custContact.clear();

    }

    @FXML
    void Search(ActionEvent event) {
    	
    	ResponseEntity<List<FinanceMst>> financeList = RestCaller.findAllFinanceMst();
    	financeCompanyMstList = FXCollections.observableArrayList(financeList.getBody());
    	FillTable();
    }

    @FXML
    void Submit(ActionEvent event) {
    	
    	addItem();
    	
    }

    private void FillTable() {
    	
		tblcust.setItems(financeCompanyMstList);

		CR1Name.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		CR3Contact.setCellValueFactory(cellData -> cellData.getValue().getPhoneNoProperty());

		
	}

	

 
    @FXML
    void custContactOnEnter(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		custbtn.requestFocus();
		}

    }

    @FXML
    void custEmailOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
		}
    }

  


    @FXML
    void gstOnEnter(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
		}

    }

    @FXML
    void nameOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		custContact.requestFocus();
		}
    }

   

    @FXML
    void submitOnEnter(KeyEvent event) {
    	
    	addItem();

    }
    
    private void addItem() {
    	if(custName.getText().trim().isEmpty())
    	{
    		notifyMessage(5, "Please Enter Name", false);
    		custName.requestFocus();
    		return;
    	}
    	
   
    	
    	if(custContact.getText().trim().isEmpty())
    	{
    		notifyMessage(5, "Please Enter Contanct", false);
    		custContact.requestFocus();
    		return;
    	}
    	
    	
    	
    	
    	

    	
    	
    	financeCompanyMst = new FinanceMst();
    	financeCompanyMst.setName(custName.getText());
    	financeCompanyMst.setPhoneNo(custContact.getText());
    	financeCompanyMst.setBranchCode(SystemSetting.systemBranch);
		String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "FC");

    	financeCompanyMst.setId(vNo+SystemSetting.getUser().getCompanyMst().getId());
    	
    	ResponseEntity<FinanceMst> financeSaved = RestCaller.saveFinanceComapny(financeCompanyMst);
    	financeCompanyMst = financeSaved.getBody();
    	
    	if(null != financeCompanyMst)
    	{
    		notifyMessage(5, "Success", false);
    	}
    	
    	financeCompanyMstList.add(financeCompanyMst);
    	
    	FillTable();
    	financeCompanyMst = null;
    	custName.clear();
    	custContact.clear();
    	custContact.clear();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
