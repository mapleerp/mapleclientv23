
package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.javapos.print.PosTaxLayoutPrint;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.CloudUserMst;
import com.maple.mapleclient.entity.CompanyMst;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.ItemBatchDtl;
import com.maple.mapleclient.entity.LoginPopup;
import com.maple.mapleclient.entity.StockVerificationMst;
import com.maple.mapleclient.entity.SysDateMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.events.LoginPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

@Component
public class LoginController {
	String taskid;
	String processInstanceId;
	private static final Logger logger = LoggerFactory.getLogger(PosTaxLayoutPrint.class);
	BranchMst branchMst = new BranchMst();

	@Value("${STOCKTRANSFER_FORMAT}")
	private String STOCKTRANSFER_FORMAT;

	@Value("${STORE}")
	private String STORE;

	@Value("${DISCOUNT_ENABLE}")
	private String DISCOUNT_ENABLE;

	@Value("${SMSENABLE}")
	private String SMSENABLE;

	@Value("${PHONENUMBER}")
	private String PHONENUMBER;

	@Value("${INVOICELINE_Y}")
	private int INVOICELINE_Y;

	@Value("${SALE_ORDER_PRINT_FORMAT}")
	private String SALE_ORDER_PRINT_FORMAT;

	@Value("${STOCK_TRANSFER_PREFIX}")
	private String STOCK_TRANSFER_PREFIX;

	@Value("${mybranch}")
	private String mybranch;
	
	@Value("${mycompany}")
	private String mycompany;
	
	
	

	@Value("${timeverificationbeforeorderedit}")
	private String timeverification;

	@Value("${CUSTOMER_SITE}")
	private String customer_site;

	@Value("${ONLINEDAYENDREPORT}")
	private String online_dayend_report;

	@Value("${SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS}")
	private String previous_invoice_amt_in_pos;

	@Value("${HOLDPROPERTY}")
	private boolean holdproperty;

	@Value("${reportpath}")
	private String reportPath;

	@Value("${WHOLE_SALE_INVOICE_FORMAT}")
	private String wholesale_invoice_format;

	@Value("${PRINTER_NAME}")
	private String printer_name;

	// ---------version 4.11
	@Value("${DAY_THERMAL_PRINTER}")
	private String DAY_END_FORMAT;

	// -------------version 4.11 end

	@Value("${NUTRITION_FACTS_PRINTER}")
	private String nutrition_facts_printer;

	@Value("${ENABLENEGATIVEBILLING}")
	private boolean NEGATIVEBILLING;

	@Value("${print.posformat}")
	private String posformat;

	@Value("${NUTRITIONTITLE}")
	private String NUTRITIONTITLE;

	@Value("${BARCODETITLE}")
	private String BARCODETITLE;

	@Value("${NUTRITION_FACT}")
	private String NUTRITION_FACT;

	@Value("${WEIGHBRIDGEPORT}")
	private int WEIGHBRIDGEPORT;

	@Value("${WEIGHBRIDGEPAPERSIZE}")
	private int WEIGHBRIDGEPAPERSIZE;

	@Value("${WEIGHCOMPANYTITLE}")
	private String WEIGHCOMPANYTITLE;

	@Value("${WEIGHCOMPANYTITLEFONTSIZE}")
	private int WEIGHCOMPANYTITLEFONTSIZE;

	@Value("${WEIGHCOMPANYTITLEX}")
	private int WEIGHCOMPANYTITLEX;

	@Value("${WEIGHCOMPANYTITLEY}")
	private int WEIGHCOMPANYTITLEY;

	@Value("${WEIGHCOMPANYADDRESS1}")
	private String WEIGHCOMPANYADDRESS1;

	@Value("${WEIGHCOMPANYADDRESS1FONTSIZE}")
	private int WEIGHCOMPANYADDRESS1FONTSIZE;

	@Value("${WEIGHCOMPANYADDRESS1X}")
	private int WEIGHCOMPANYADDRESS1X;

	@Value("${WEIGHCOMPANYADDRESS1Y}")
	private int WEIGHCOMPANYADDRESS1Y;

	@Value("${WEIGHCOMPANYADDRESS2}")
	private String WEIGHCOMPANYADDRESS2;

	@Value("${WEIGHCOMPANYADDRESS2FONTSIZE}")
	private int WEIGHCOMPANYADDRESS2FONTSIZE;

	@Value("${WEIGHCOMPANYADDRESS2X}")
	private int WEIGHCOMPANYADDRESS2X;

	@Value("${WEIGHCOMPANYADDRESS2Y}")
	private int WEIGHCOMPANYADDRESS2Y;

	@Value("${WEIGHCOMPANYADDRESS3}")
	private String WEIGHCOMPANYADDRESS3;

	@Value("${WEIGHCOMPANYADDRESS3FONTSIZE}")
	private int WEIGHCOMPANYADDRESS3FONTSIZE;

	@Value("${WEIGHCOMPANYADDRESS3X}")
	private int WEIGHCOMPANYADDRESS3X;

	@Value("${WEIGHCOMPANYADDRESS3Y}")
	private int WEIGHCOMPANYADDRESS3Y;

	@Value("${WEIGHCOMPANYVEHICKENOX}")
	private int WEIGHCOMPANYVEHICKENOX;

	@Value("${WEIGHCOMPANYVEHICKENOY}")
	private int WEIGHCOMPANYVEHICKENOY;

	@Value("${WEIGHCOMPANYVEHICKENOFONTSIZE}")
	private int WEIGHCOMPANYVEHICKENOFONTSIZE;

	@Value("${WEIGHCOMPANYRATEX}")
	private int WEIGHCOMPANYRATEX;

	@Value("${WEIGHCOMPANYRATEY}")
	private int WEIGHCOMPANYRATEY;

	@Value("${WEIGHCOMPANYRATEFONTSIZE}")
	private int WEIGHCOMPANYRATEFONTSIZE;

	@Value("${WEIGHCOMPANYDATEX}")
	private int WEIGHCOMPANYDATEX;

	@Value("${WEIGHCOMPANYDATEY}")
	private int WEIGHCOMPANYDATEY;

	@Value("${WEIGHCOMPANYDATEFONTSIZE}")
	private int WEIGHCOMPANYDATEFONTSIZE;

	@Value("${WEIGHCOMPANYFIRSTWEIGHTX}")
	private int WEIGHCOMPANYFIRSTWEIGHTX;

	@Value("${WEIGHCOMPANYFIRSTWEIGHTY}")
	private int WEIGHCOMPANYFIRSTWEIGHTY;

	@Value("${WEIGHCOMPANYFIRSTWEIGHTFONTSIZE}")
	private int WEIGHCOMPANYFIRSTWEIGHTFONTSIZE;

	@Value("${WEIGHCOMPANYSECONDWEIGHTX}")
	private int WEIGHCOMPANYSECONDWEIGHTX;

	@Value("${WEIGHCOMPANYSECONDWEIGHTY}")
	private int WEIGHCOMPANYSECONDWEIGHTY;

	@Value("${WEIGHCOMPANYSECONDWEIGHTFONTSIZE}")
	private int WEIGHCOMPANYSECONDWEIGHTFONTSIZE;

	@Value("${WEIGHCOMPANYNETWEIGHTX}")
	private int WEIGHCOMPANYNETWEIGHTX;

	@Value("${WEIGHCOMPANYNETWEIGHTY}")
	private int WEIGHCOMPANYNETWEIGHTY;

	@Value("${WEIGHCOMPANYNETWEIGHTFONTSIZE}")
	private int WEIGHCOMPANYNETWEIGHTFONTSIZE;

	@Value("${CHEQUECROSSLINE1X1}")
	private int CHEQUECROSSLINE1X1;

	@Value("${CHEQUECROSSLINE1Y1}")
	private int CHEQUECROSSLINE1Y1;

	@Value("${CHEQUECROSSLINE1X2}")
	private int CHEQUECROSSLINE1X2;

	@Value("${CHEQUECROSSLINE1Y2}")
	private int CHEQUECROSSLINE1Y2;

	@Value("${CHEQUECROSSLINE2X1}")
	private int CHEQUECROSSLINE2X1;

	@Value("${CHEQUECROSSLINE2Y1}")
	private int CHEQUECROSSLINE2Y1;

	@Value("${CHEQUECROSSLINE2X2}")
	private int CHEQUECROSSLINE2X2;

	@Value("${CHEQUECROSSLINE2Y2}")
	private int CHEQUECROSSLINE2Y2;

	@Value("${CHEQUEDATEX}")
	private int CHEQUEDATEX;

	@Value("${CHEQUEDATEY}")
	private int CHEQUEDATEY;

	@Value("${CHEQUEACCOUNTNAMEX}")
	private int CHEQUEACCOUNTNAMEX;

	@Value("${CHEQUEACCOUNTNAMEY}")
	private int CHEQUEACCOUNTNAMEY;

	@Value("${CHEQUEAMOUNTINWORDSX}")
	private int CHEQUEAMOUNTINWORDSX;

	@Value("${CHEQUEAMOUNTINWORDSY}")
	private int CHEQUEAMOUNTINWORDSY;

	@Value("${CHEQUEAMOUNTX}")
	private int CHEQUEAMOUNTX;

	@Value("${CHEQUEAMOUNTY}")
	private int CHEQUEAMOUNTY;

	@Value("${WEIGHCOMPANYVOUCHERNUMBERY}")
	private int WEIGHCOMPANYVOUCHERNUMBERY;

	@Value("${WEIGHCOMPANYVOUCHERNUMBERX}")
	private int WEIGHCOMPANYVOUCHERNUMBERX;

	@Value("${WEIGHCOMPANYVOUCHERNUMBERFONTSIZE}")
	private int WEIGHCOMPANYVOUCHERNUMBERFONTSIZE;

	@Value("${CHEQUEPRINTFONTSIZE}")
	private int CHEQUEPRINTFONTSIZE;

	@Value("${IMPORT_PURCHASE_COSTING_METHOD}")
	private String IMPORT_PURCHASE_COSTING_METHOD;

	// camunda.host=http://localhost:9091

	@Value("${camunda.host}")
	private String CAMUNDAHOST;

	/*
	 * SystemSetting.WEIGHBRIDGEPORT, SystemSetting.WEIGHBRIDGEBAUDRATE,
	 * SystemSetting.WEIGHBRIDGEDATABITS, SystemSetting.WEIGHBRIDGESTOPBIT,
	 * SystemSetting.WEIGHBRIDGEPARITY)
	 */

	@Value("${WEIGHBRIDGEBAUDRATE}")
	private int WEIGHBRIDGEBAUDRATE;

	@Value("${WEIGHBRIDGEDATABITS}")
	private int WEIGHBRIDGEDATABITS;

	@Value("${WEIGHBRIDGESTOPBIT}")
	private int WEIGHBRIDGESTOPBIT;

	@Value("${WEIGHBRIDGEPARITY}")
	private int WEIGHBRIDGEPARITY;

	@Value("${CUSTOMER_CREDIT_PERIOD}")
	private String CREDIT_PERIOD;
	
	

	

	/* TalltRetry */

	@Value("${tallyServer}")
	private String tallyServer;

	

	@Value("${STOCK_VERIFICATION_COUNT}")
	private int stcok_verification_count;

	@Value("${SHOW_STOCK_POPUP}")
	private String show_stock_popup;

	@Value("${POS_SALES_PREFIX}")
	private String POSSALESPREFIX;

	@Value("${KOT_SALES_PREFIX}")
	private String KOTSALESPREFIX;
	
	@Value("${PKGLIC}")
	private String packagingLicence;
	
	
	@Value("${FSSAI}")
	private String FSSAI;
	
	

	String user = "maple";
	String password = "maple";
	String checkuser, checkpassword;

	Boolean emptyDatabase = false;

	@FXML
	private TextField loguser;

	@FXML
	private PasswordField logpass;

	@FXML
	private DatePicker logDate;

	@FXML
	private Button logButton;

	@FXML
	private Button exit;

	@FXML
	private ComboBox<String> cmbBranchCode;

	String serverVersion = null;

	@FXML
	void exitbtn(ActionEvent event) {

		Stage stage = (Stage) exit.getScene().getWindow();
		stage.close();
	}

	@FXML
	void loginWindow(ActionEvent event) throws IOException {

		

		logger.info("====login action started=====");

		Stage primaryStage = (Stage) logButton.getScene().getWindow();
		primaryStage.hide();
		
		checkuser = loguser.getText().toString();
		checkpassword = logpass.getText().toString();
		

		CloudUserMst	userMstcloud = new CloudUserMst();

		
		if (mybranch.equalsIgnoreCase("CLOUD") || mybranch.equalsIgnoreCase("PHARMACY_CLOUD") ) {
			SystemSetting.systemBranch = mybranch;
			SystemSetting.myCompany = mycompany;
			
			ResponseEntity<CloudUserMst> cloudUserMstResp = RestCaller.CloudLogin(checkuser,checkpassword);
			CloudUserMst cloudUserMst = cloudUserMstResp.getBody();
			
			//---------------------sharon----------july 7-2022----------------------
			if(null != cloudUserMst)
			{
				if (checkpassword.equalsIgnoreCase(cloudUserMst.getPassword())) {
					
					
					            SystemSetting.setCloudUser(cloudUserMst);
								SystemSetting.systemBranch = mybranch;
					
							
								ShowMainframe();
								
							   }
							
			}
			else {
				Alert a = new Alert(AlertType.ERROR);
				a.setContentText("Incorrect UserName / Password");
				a.showAndWait().ifPresent((btnType) -> {
					if (btnType == ButtonType.OK) {

						return;
					}
				});
			}
			return;
		}
		
		if (null == logDate.getValue()) {
			notifyMessage(5, " Please select Date...!!!", false);
			return;
		}

		logger.info("==============checkuser==============");

		
		UserMst userMst = new UserMst();
		String ldate = logDate.getValue().toString();
		Date logDatet = SystemSetting.StringToUtilDate(ldate, "yyyy-MM-dd");
		SystemSetting.systemDate = logDatet;
		
		 
		
	
		
		
		
		// version1.10

		LocalDate lodate = logDate.getValue();
		Date tdate = SystemSetting.localToUtilDate(lodate);
		String sdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");

		ResponseEntity<FinancialYearMst> getfiancialYearMst = null;

		try {
			getfiancialYearMst = RestCaller.getFinancialYear(sdate);
		} catch (Exception e) {
			System.out.println("Error " + e.toString());
		}


		SystemSetting.STOCK_VERIFICATION_COUNT = stcok_verification_count;
		ResponseEntity<List<CompanyMst>> companysavedResp = RestCaller.returnValueOfCompanymst();


//		if (mybranch.equals("HO")) {
		mybranch = cmbBranchCode.getValue();
//		}

		CompanyMst companyMst = null;
		SystemSetting.reportPath = reportPath;
		SystemSetting.systemBranch = mybranch;
		SystemSetting.myCompany = mycompany;

		SystemSetting.STOCK_TRANSFER_PREFIX = STOCK_TRANSFER_PREFIX;
		SystemSetting.HOLDPROPERTY = holdproperty;
		SystemSetting.customer_site_selection = customer_site;
		SystemSetting.wholesale_invoice_format = wholesale_invoice_format;
		SystemSetting.ONLINEDAYENDREPORT = online_dayend_report;
		SystemSetting.SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS = previous_invoice_amt_in_pos;
		SystemSetting.printer_name = printer_name;
		SystemSetting.nutrition_facts_printer = nutrition_facts_printer;
		SystemSetting.DISCOUNT_ENABLE = DISCOUNT_ENABLE;
		SystemSetting.WEIGHBRIDGEPORT = WEIGHBRIDGEPORT;
		SystemSetting.SALE_ORDER_PRINT_FORMAT = SALE_ORDER_PRINT_FORMAT;
		SystemSetting.setNEGATIVEBILLING(NEGATIVEBILLING);
		SystemSetting.setPosFormat(posformat);
		SystemSetting.setNUTRITIONTITLE(NUTRITIONTITLE);
		SystemSetting.setBARCODETITLE(BARCODETITLE);
		SystemSetting.setINVOICELINE_Y(INVOICELINE_Y);
		SystemSetting.setNutritionfact(NUTRITION_FACT);
		SystemSetting.setSMSENABLE(SMSENABLE);
		SystemSetting.setPHONENUMBER(PHONENUMBER);

		SystemSetting.SHOWSTOCKPOPUP = show_stock_popup;

		SystemSetting.STORE = STORE;
		

		SystemSetting.STOCKTRANSFER_FORMAT = STOCKTRANSFER_FORMAT;

		SystemSetting.WEIGHBRIDGEPAPERSIZE = WEIGHBRIDGEPAPERSIZE;

		SystemSetting.WEIGHCOMPANYTITLE = WEIGHCOMPANYTITLE;

		SystemSetting.WEIGHCOMPANYTITLEFONTSIZE = WEIGHCOMPANYTITLEFONTSIZE;

		SystemSetting.WEIGHCOMPANYTITLEX = WEIGHCOMPANYTITLEX;

		SystemSetting.WEIGHCOMPANYTITLEY = WEIGHCOMPANYTITLEY;

		SystemSetting.IMPORT_PURCHASE_COSTING_METHOD = IMPORT_PURCHASE_COSTING_METHOD;
		SystemSetting.WEIGHCOMPANYADDRESS1 = WEIGHCOMPANYADDRESS1;

		SystemSetting.WEIGHCOMPANYADDRESS1FONTSIZE = WEIGHCOMPANYADDRESS1FONTSIZE;

		SystemSetting.WEIGHCOMPANYADDRESS1X = WEIGHCOMPANYADDRESS1X;

		SystemSetting.WEIGHCOMPANYADDRESS1Y = WEIGHCOMPANYADDRESS1Y;

		SystemSetting.WEIGHCOMPANYADDRESS2 = WEIGHCOMPANYADDRESS2;

		SystemSetting.WEIGHCOMPANYADDRESS2FONTSIZE = WEIGHCOMPANYADDRESS2FONTSIZE;

		SystemSetting.WEIGHCOMPANYADDRESS2X = WEIGHCOMPANYADDRESS2X;

		SystemSetting.WEIGHCOMPANYADDRESS2Y = WEIGHCOMPANYADDRESS2Y;

		SystemSetting.WEIGHCOMPANYADDRESS3 = WEIGHCOMPANYADDRESS3;

		SystemSetting.WEIGHCOMPANYADDRESS3FONTSIZE = WEIGHCOMPANYADDRESS3FONTSIZE;

		SystemSetting.WEIGHCOMPANYADDRESS3X = WEIGHCOMPANYADDRESS3X;

		SystemSetting.WEIGHCOMPANYADDRESS3Y = WEIGHCOMPANYADDRESS3Y;

		SystemSetting.WEIGHCOMPANYVEHICKENOX = WEIGHCOMPANYVEHICKENOX;

		SystemSetting.WEIGHCOMPANYVEHICKENOY = WEIGHCOMPANYVEHICKENOY;

		SystemSetting.WEIGHCOMPANYVEHICKENOFONTSIZE = WEIGHCOMPANYVEHICKENOFONTSIZE;

		SystemSetting.WEIGHCOMPANYRATEX = WEIGHCOMPANYRATEX;

		SystemSetting.WEIGHCOMPANYRATEY = WEIGHCOMPANYRATEY;

		SystemSetting.WEIGHCOMPANYRATEFONTSIZE = WEIGHCOMPANYRATEFONTSIZE;

		SystemSetting.WEIGHCOMPANYDATEX = WEIGHCOMPANYDATEX;

		SystemSetting.WEIGHCOMPANYDATEY = WEIGHCOMPANYDATEY;

		SystemSetting.WEIGHCOMPANYDATEFONTSIZE = WEIGHCOMPANYDATEFONTSIZE;

		SystemSetting.WEIGHCOMPANYFIRSTWEIGHTX = WEIGHCOMPANYFIRSTWEIGHTX;

		SystemSetting.WEIGHCOMPANYFIRSTWEIGHTY = WEIGHCOMPANYFIRSTWEIGHTY;

		SystemSetting.WEIGHCOMPANYFIRSTWEIGHTFONTSIZE = WEIGHCOMPANYFIRSTWEIGHTFONTSIZE;

		SystemSetting.WEIGHCOMPANYSECONDWEIGHTX = WEIGHCOMPANYSECONDWEIGHTX;

		SystemSetting.WEIGHCOMPANYSECONDWEIGHTY = WEIGHCOMPANYSECONDWEIGHTY;

		SystemSetting.WEIGHCOMPANYSECONDWEIGHTFONTSIZE = WEIGHCOMPANYSECONDWEIGHTFONTSIZE;

		SystemSetting.WEIGHCOMPANYNETWEIGHTX = WEIGHCOMPANYNETWEIGHTX;

		SystemSetting.WEIGHCOMPANYNETWEIGHTY = WEIGHCOMPANYNETWEIGHTY;

		SystemSetting.WEIGHCOMPANYNETWEIGHTFONTSIZE = WEIGHCOMPANYNETWEIGHTFONTSIZE;

		SystemSetting.WEIGHCOMPANYVOUCHERNUMBERY = WEIGHCOMPANYVOUCHERNUMBERY;
		SystemSetting.WEIGHCOMPANYVOUCHERNUMBERX = WEIGHCOMPANYVOUCHERNUMBERX;
		SystemSetting.WEIGHCOMPANYVOUCHERNUMBERFONTSIZE = WEIGHCOMPANYVOUCHERNUMBERFONTSIZE;

		SystemSetting.CHEQUEACCOUNTNAMEX = CHEQUEACCOUNTNAMEX;
		SystemSetting.CHEQUEACCOUNTNAMEY = CHEQUEACCOUNTNAMEY;
		SystemSetting.CHEQUEAMOUNTINWORDSX = CHEQUEAMOUNTINWORDSX;
		SystemSetting.CHEQUEAMOUNTINWORDSY = CHEQUEAMOUNTINWORDSY;
		SystemSetting.CHEQUEAMOUNTX = CHEQUEAMOUNTX;
		SystemSetting.CHEQUEAMOUNTY = CHEQUEAMOUNTY;
		SystemSetting.CHEQUECROSSLINE1X1 = CHEQUECROSSLINE1X1;
		SystemSetting.CHEQUECROSSLINE1X2 = CHEQUECROSSLINE1X2;
		SystemSetting.CHEQUECROSSLINE1Y1 = CHEQUECROSSLINE1Y1;
		SystemSetting.CHEQUECROSSLINE1Y2 = CHEQUECROSSLINE1Y2;
		SystemSetting.CHEQUECROSSLINE2X1 = CHEQUECROSSLINE2X1;
		SystemSetting.CHEQUECROSSLINE2X2 = CHEQUECROSSLINE2X2;
		SystemSetting.CHEQUECROSSLINE2Y1 = CHEQUECROSSLINE2Y1;
		SystemSetting.CHEQUECROSSLINE2Y2 = CHEQUECROSSLINE2Y2;
		SystemSetting.CHEQUEDATEX = CHEQUEDATEX;
		SystemSetting.CHEQUEDATEY = CHEQUEDATEY;

		SystemSetting.CHEQUEPRINTFONTSIZE = CHEQUEPRINTFONTSIZE;
		SystemSetting.CAMUNDAHOST = CAMUNDAHOST;

		SystemSetting.WEIGHBRIDGEBAUDRATE = WEIGHBRIDGEBAUDRATE;

		SystemSetting.WEIGHBRIDGEDATABITS = WEIGHBRIDGEDATABITS;

		SystemSetting.WEIGHBRIDGESTOPBIT = WEIGHBRIDGESTOPBIT;

		SystemSetting.WEIGHBRIDGEPARITY = WEIGHBRIDGEPARITY;

		SystemSetting.CUSTOMER_CDREDIT_PERIOD = CREDIT_PERIOD;

		/* TallyRetry */

		SystemSetting.tallyServer = tallyServer;

		SystemSetting.POSPREFIX = POSSALESPREFIX;

		SystemSetting.KOTPREFIX = KOTSALESPREFIX;

		// ------------version 4.11
		SystemSetting.day_end_format = DAY_END_FORMAT;
		
		SystemSetting.PACKAGINGLICENCE = packagingLicence;
		
		SystemSetting.FSSAI = FSSAI;
		
		
		
		
		
		
	    ResponseEntity<SysDateMst> systemdatemstrepos = RestCaller.getSysDate(SystemSetting.systemBranch);
		
		SysDateMst	sysDateMstObject = systemdatemstrepos.getBody();
		
		String Application_date = SystemSetting.UtilDateToString(sysDateMstObject.getAplicationDate(), "yyyy-MM-dd");
		
		Date applicationDate = SystemSetting.StringToUtilDate(Application_date, "yyyy-MM-dd");
		
		ResponseEntity<DayEndClosureHdr> maxofDayrespo = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdrObject = maxofDayrespo.getBody();
		
		String process_date = SystemSetting.UtilDateToString(dayEndClosureHdrObject.getProcessDate(), "yyyy-MM-dd");
		
		Date lastDayEndDate = SystemSetting.StringToUtilDate(process_date, "yyyy-MM-dd");
		
		
		
		
		if(!allowLoginForTheDate(logDatet,applicationDate,lastDayEndDate)) {
			
			Alert a = new Alert(AlertType.ERROR);
			
			 // set alert type
            a.setAlertType(AlertType.ERROR);
            // show the dialog
            a.show();
			return;
			
			
		}
	 
         //Update application = login date
		
		

		
		try {
			serverVersion = RestCaller.getServerVersion();
		} catch (Exception e) {

		}


		try {
			ResponseEntity<List<CompanyMst>> companysaved = RestCaller.returnValueOfCompanymst();
			companyMst = companysaved.getBody().get(0);
			System.out.println(companyMst);
		} catch (Exception e2) {

		}
		try {
			ResponseEntity<UserMst> respentity = RestCaller.verifyUser(checkuser, checkpassword, mybranch);
			userMst = respentity.getBody();
		} catch (Exception e) {

		}
		// Firstr time blank db
		if (checkpassword.equalsIgnoreCase("maplesystem")) {

			userMst = new UserMst();
			userMst.setUserName("maplesystem");

			userMst.setUserName("qwerty123");
			userMst.setPassword("qwerty123");
			SystemSetting.userId = "qwerty123";
			userMst.setBranchCode(mybranch);

			SystemSetting.systemBranch = mybranch;
			SystemSetting.reportPath = reportPath;
			SystemSetting.customer_site_selection = customer_site;

			SystemSetting.timeverificationbeforeorderedit = timeverification;
			SystemSetting.wholesale_invoice_format = wholesale_invoice_format;

			SystemSetting.user_roles.add("CONFIGURATIONS");
			SystemSetting.user_roles.add("COMPANY CREATION");
			SystemSetting.user_roles.add("BRANCH CREATION");
			SystemSetting.user_roles.add("USER REGISTRATION");
			SystemSetting.user_roles.add("MENU CREATION");
			SystemSetting.user_roles.add("ADD ACCOUNT HEADS");
			SystemSetting.user_roles.add("MENU CONFIGURATION");
			SystemSetting.user_roles.add("SUBSCRIPTION VALIDITY");
			SystemSetting.user_roles.add("DOMAIN INITIALIZE");

			try {
				companyMst = RestCaller.getCompanyMst(mycompany).getBody();
				userMst.setCompanyMst(companyMst);
			} catch (Exception e) {

			}

			SystemSetting.setUser(userMst);
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.setTitle(branchMst.getBranchName() + "-" + userMst.getUserName() + " "
					+ SystemSetting.UtilDateToString(SystemSetting.systemDate) + "-" + SystemSetting.version + "/"
					+ serverVersion);
			stage.show();
			return;
		}
		if (checkpassword.equalsIgnoreCase("maplesystem")) {

			userMst = new UserMst();
			userMst.setUserName("maplesystem");

			userMst.setUserName("qwerty123");
			userMst.setPassword("qwerty123");
			SystemSetting.userId = "qwerty123";

			SystemSetting.reportPath = reportPath;
			SystemSetting.customer_site_selection = customer_site;

			SystemSetting.timeverificationbeforeorderedit = timeverification;
			SystemSetting.wholesale_invoice_format = wholesale_invoice_format;

			SystemSetting.user_roles.add("CONFIGURATIONS");
			SystemSetting.user_roles.add("COMPANY CREATION");
			SystemSetting.user_roles.add("BRANCH CREATION");
			SystemSetting.user_roles.add("USER REGISTRATION");
			SystemSetting.user_roles.add("MENU CREATION");
			SystemSetting.user_roles.add("ADD ACCOUNT HEADS");
			SystemSetting.user_roles.add("MENU CONFIGURATION");
			SystemSetting.user_roles.add("SUBSCRIPTION VALIDITY");
			SystemSetting.user_roles.add("DOMAIN INITIALIZE");

			try {
				companyMst = RestCaller.getCompanyMst(mycompany).getBody();
				userMst.setCompanyMst(companyMst);
			} catch (Exception e) {

			}

			SystemSetting.setUser(userMst);
			Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.setTitle(branchMst.getBranchName() + "-" + userMst.getUserName() + " "
					+ SystemSetting.UtilDateToString(SystemSetting.systemDate) + "-" + SystemSetting.version + "/"
					+ serverVersion);
			stage.show();
			return;
		}
		//---------------------------------------------
		if (checkpassword.equalsIgnoreCase("maplesystem")) {
			logger.info("==============equalsIgnoreCase(\"maplesystem\"==============");

			SystemSetting.debugUser = true;

			if (null == userMst) {

				try {
					ResponseEntity<UserMst> respentity2 = RestCaller.verifyUserNoPassword(checkuser, mybranch);
					userMst = respentity2.getBody();
					logger.info("==============userMst==============" + userMst);
				} catch (Exception exc) {

				}

				userMst = new UserMst();
				userMst.setUserName("maplesystem");

				userMst.setUserName("qwerty123");
				userMst.setPassword("qwerty123");
				SystemSetting.userId = "qwerty123";
				userMst.setBranchCode(mybranch);

				SystemSetting.systemBranch = mybranch;
				SystemSetting.reportPath = reportPath;
				SystemSetting.customer_site_selection = customer_site;

				SystemSetting.timeverificationbeforeorderedit = timeverification;
				SystemSetting.wholesale_invoice_format = wholesale_invoice_format;

				companyMst = RestCaller.getCompanyMst(mycompany).getBody();
				userMst.setCompanyMst(companyMst);
				SystemSetting.setUser(userMst);
				try {

//					ResponseEntity<SysDateMst> respetnity = RestCaller
//							.getSysDate(SystemSetting.systemBranch);
//					
//					SysDateMst sysDateMst = null;
//					sysDateMst = respetnity.getBody();
//					if (sysDateMst != null) {
//						System.out.println("SysDate Mst"+sysDateMst);
//						sysDateMst.setAplicationDate(java.sql.Date.valueOf(LocalDate.now()));
//						sysDateMst.setSystemDate(java.sql.Date.valueOf(LocalDate.now()));
//						sysDateMst.setBranchCode(SystemSetting.systemBranch);
//						//RestCaller.updateSysDate(sysDateMst);
//						if (sysDateMst.getSystemDate() != sysDateMst.getAplicationDate()) {
//							notifyMessage(5, " System Date and Application Dates are different");
//						}
//
//					}
//					else {
//						sysDateMst = new SysDateMst();
//						sysDateMst.setAplicationDate(SystemSetting.localToUtilDate(logDate.getValue()));
//						sysDateMst.setBranchCode(SystemSetting.systemBranch);
//						sysDateMst.setSystemDate(java.sql.Date.valueOf(LocalDate.now()));
//						ResponseEntity<SysDateMst> respentity = RestCaller.saveSysDateMst(sysDateMst);
//						System.out.println("New ---SysDate Mst"+sysDateMst);
//						System.out.println();
//					}
					
			       
					saveSysDateMst(logDatet,mybranch);
					// RestCaller.updateHostWithComapnyId();
					// if (emptyDatabase == false) {
					SystemSetting.user_roles.add("COMPANY CREATION");
					SystemSetting.user_roles.add("BRANCH CREATION");
					SystemSetting.user_roles.add("USER REGISTRATION");
					logger.info("==============SystemSetting.user_roles==============" + SystemSetting.user_roles);
					// }

					Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));
					Scene scene = new Scene(root);
					Stage stage = new Stage();
					stage.setScene(scene);
					stage.setTitle(branchMst.getBranchName() + "-" + userMst.getUserName() + " "
							+ SystemSetting.UtilDateToString(SystemSetting.systemDate) + "-" + SystemSetting.version
							+ "/" + serverVersion);
					stage.show();
					return;

//				 else {
//					sysDateMst = new SysDateMst();
//					sysDateMst.setAplicationDate(java.sql.Date.valueOf(LocalDate.now()));
//					sysDateMst.setBranchCode(SystemSetting.getUser().getBranchCode());
//					sysDateMst.setSystemDate(java.sql.Date.valueOf(LocalDate.now()));
//					ResponseEntity<SysDateMst> respentity = RestCaller.saveSysDateMst(sysDateMst);
//
//				}

				} catch (Exception e) {
					// TODO: handle exception
				}
				// RestCaller.updateHostWithComapnyId();
				// if (emptyDatabase == false) {
				SystemSetting.user_roles.add("COMPANY CREATION");
				SystemSetting.user_roles.add("BRANCH CREATION");
				SystemSetting.user_roles.add("USER REGISTRATION");

				logger.info("==============SystemSetting.user_roles==============" + SystemSetting.user_roles);
				// }

				Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));
				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.setScene(scene);
				stage.setTitle(branchMst.getBranchName() + "-" + userMst.getUserName() + " "
						+ SystemSetting.UtilDateToString(SystemSetting.systemDate) + "-" + SystemSetting.version + "/"
						+ serverVersion);
				stage.show();

				return;

			} else {

				SystemSetting.setUser(userMst);
				SystemSetting.getUser_roles(SystemSetting.userId);
				logger.info("==============uset Mst not null==============");
				userMst.setCompanyMst(companyMst);

				SystemSetting.setUser(userMst);
				try {
					 String  branch=SystemSetting.systemBranch;
 
					saveSysDateMst(logDatet,branch);

				} catch (Exception e) {
					// TODO: handle exception
				}

				try {

					BranchMst branch = RestCaller.getBranchDtls(mybranch);
					userMst.setBranchName(branch.getBranchName());

					SystemSetting.setSystemBranch(branch.getBranchCode());
					ResponseEntity<SysDateMst> respetnity = RestCaller.getSysDate(SystemSetting.systemBranch);
					SysDateMst sysDateMst = new SysDateMst();
					if (null != respetnity.getBody()) {
						sysDateMst = respetnity.getBody();
						if (null != sysDateMst)
							SystemSetting.setApplicationDate(sysDateMst.getAplicationDate());
					}
				} catch (Exception e1) {
					notifyMessage(5, " Incorrect User Branch ...!!!", false);
					userMst.setBranchName(mybranch);

					SystemSetting.setSystemBranch(mybranch);
				}

				SystemSetting.reportPath = reportPath;

				Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));
				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.setScene(scene);
				stage.setTitle(branchMst.getBranchName() + "-" + userMst.getUserName() + ""
						+ SystemSetting.UtilDateToString(SystemSetting.systemDate) + "-" + SystemSetting.version + "/"
						+ serverVersion);
				stage.show();
				return;

			}

		}

		else {
			try {
				ResponseEntity<UserMst> respentity = RestCaller.verifyUser(checkuser, checkpassword, mybranch);
				userMst = respentity.getBody();
			} catch (Exception e) {

			}

			if (null != userMst) {
				if ((checkuser.contentEquals(userMst.getUserName())
						&& checkpassword.contentEquals(userMst.getPassword()))) {

					if (userMst.getUserName().toUpperCase().contains("QWERTY123")
							|| userMst.getUserName().equalsIgnoreCase("maple")) {
						SystemSetting.debugUser = true;
					}

					branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

					userMst.setBranchName(branchMst.getBranchName());

					if (branchMst.getBranchCode().equals("HO")) {
						RestCaller.HOST = userMst.getCompanyMst().getIpAdress();
					}
					
					
					ResponseEntity<SysDateMst> respetnity = RestCaller.getSysDate(SystemSetting.systemBranch);
					if (null != respetnity.getBody()) {
						SysDateMst sysDateMst = new SysDateMst();
						sysDateMst = respetnity.getBody();
						if (null != sysDateMst) {
							SystemSetting.setApplicationDate(sysDateMst.getAplicationDate());
						}
					}
					SystemSetting.systemBranch = mybranch;
					SystemSetting.reportPath = reportPath;
					SystemSetting.userId = userMst.getId();
					SystemSetting.setUser(userMst);
					SystemSetting.timeverificationbeforeorderedit = timeverification;
					SystemSetting.customer_site_selection = customer_site;
					SystemSetting.wholesale_invoice_format = wholesale_invoice_format;

					if (null != logDate.getValue()) {

						SystemSetting.systemDate = SystemSetting.localToUtilDate(logDate.getValue());
//					 ldate =logDate.getValue();
//					 sdate = SystemSetting.localToUtilDate(ldate);
//					 tdate = SystemSetting.UtilDateToString(sdate);
//					 logDatet = 	SystemSetting.StringToUtilDate(tdate, "yyyy-MM-dd");
//					SystemSetting.systemDate = logDatet;
					}
					if (!mybranch.equalsIgnoreCase(SystemSetting.systemBranch)) {

						Alert a = new Alert(AlertType.ERROR);
						// a.setHeaderText("");
						a.setContentText("Incorrect User Branch");
						a.showAndWait().ifPresent((btnType) -> {
							if (btnType == ButtonType.OK) {

								return;
							}
						});
					}
					// RestCaller.updateHostWithComapnyId();
					// if (emptyDatabase == false) {
					SystemSetting.getUser_roles(SystemSetting.userId);

					// }

					// Stage primaryStage = (Stage)logButton.getScene().getWindow();
					// primaryStage.hide();
					
					 String  branch=SystemSetting.systemBranch;
					saveSysDateMst(logDatet,branch);
					Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));
					Scene scene = new Scene(root);
					Stage stage = new Stage();
					stage.setScene(scene);
					stage.setTitle(branchMst.getBranchName() + "-" + userMst.getUserName() + " "
							+ SystemSetting.UtilDateToString(SystemSetting.systemDate) + "-" + SystemSetting.version
							+ "/" + serverVersion);
					stage.show();
					return;

				}

				Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));
				Scene scene = new Scene(root);
				Stage stage = new Stage();
				stage.setScene(scene);
				stage.setTitle(branchMst.getBranchName() + "-" + userMst.getUserName() + " "
						+ SystemSetting.UtilDateToString(SystemSetting.systemDate) + "-" + SystemSetting.version + "/"
						+ serverVersion);
				stage.show();
				return;
//					
//			    	Stage stage2 = (Stage) exit.getScene().getWindow();
//					stage2.close();

			} else {
				Alert a = new Alert(AlertType.ERROR);
				// a.setHeaderText("");
				a.setContentText("Incorrect UserName / Password");
				a.showAndWait().ifPresent((btnType) -> {
					if (btnType == ButtonType.OK) {

//				notifyMessage(5, " check your username or password...!!!");
						return;
					}
				});
			}
		}
	}

	private void ShowMainframe() {
		
		

		try {
			
			SystemSetting.user_roles.add("REPORT SQL COMMAND");
			SystemSetting.myCompany = mycompany;



			Parent root;

			root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));

			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.setTitle(checkuser + "-" + "CLOUD" + " "
					+ SystemSetting.UtilDateToString(SystemSetting.systemDate) + "-" + SystemSetting.version + "/"
					+ serverVersion);
			stage.show();
			return;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	private void initialize() {
		if (mybranch.equals("HO")) {
			setBranches();
		} else {

			cmbBranchCode.getSelectionModel().select(mybranch);
		}
		setApplicationDate();
		// loginPopupEvent = new LoginPopupEvent();
		// eventBus.register(this);

	}

	private void setApplicationDate() {

		// ( ResponseEntity<SysDateMst> sysDateMst =
		// RestCaller.getSysDate(cmbBranchCode.getValue());

		try {
			ResponseEntity<SysDateMst> sysDateMst = RestCaller.getSysDate(cmbBranchCode.getValue());

			if (null != sysDateMst.getBody()) {
				LocalDate logdate = SystemSetting.utilToLocaDate(sysDateMst.getBody().getAplicationDate());
				logDate.setValue(logdate);
				System.out.println("Log Date" + logdate);
			}

		} catch (Exception e) {
			emptyDatabase = true;
			System.out.println(e);
		}

	}

	@FXML
	void userNameOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			logpass.requestFocus();
		}

	}

	@FXML
	void passwordOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			logButton.requestFocus();
		}
	}

	private void setBranches() {

		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();

		for (BranchMst branchMst : branchMstList) {
			cmbBranchCode.getItems().add(branchMst.getBranchCode());
		}

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	public void saveSysDateMst(Date logDatet, String branchCode ) {
		SysDateMst sysDateMst  = null;
		ResponseEntity<SysDateMst> respetnity = RestCaller.getSysDate(SystemSetting.systemBranch);
		if(null== respetnity.getBody()) {
			  sysDateMst = new SysDateMst();
			  sysDateMst = respetnity.getBody();
			 
			    sysDateMst.setAplicationDate(java.sql.Date.valueOf(SystemSetting.UtilDateToString(logDatet)));
				sysDateMst.setBranchCode(branchCode);
				sysDateMst.setSystemDate(java.sql.Date.valueOf(LocalDate.now()));
					ResponseEntity<SysDateMst> respentity = RestCaller.saveSysDateMst(sysDateMst);  
		}

		sysDateMst = respetnity.getBody();
		sysDateMst.setAplicationDate(logDatet);
		sysDateMst.setBranchCode(branchCode);
		sysDateMst.setSystemDate(java.sql.Date.valueOf(LocalDate.now()));
			ResponseEntity<SysDateMst> respentity = RestCaller.saveSysDateMst(sysDateMst);
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {
	}
	
	public boolean allowLoginForTheDate(Date login,Date applicationDate,Date lastDayEndDate) {
		
		return login.after(lastDayEndDate) && 
				(login.after(applicationDate)|| login.compareTo(applicationDate)==0	);
		
		
	}
}