package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.TSCBarcode;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.IngredientsMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.entity.NutritionValueDtl;
import com.maple.mapleclient.entity.NutritionValueMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class BarcodeNutritionvalueCombinedCtl {
	String taskid;
	String processInstanceId;


	private ObservableList<NutritionMst> nutritionList = FXCollections.observableArrayList();
	private ObservableList<NutritionValueDtl> nutrintionValueList = FXCollections.observableArrayList();

	NutritionValueDtl nutritionValueDtl = null;
	NutritionValueMst nutritionValueMst = null;
	 EventBus eventBus = EventBusFactory.getEventBus();
		EventBus eventBuz = EventBusFactory.getEventBus();
	ItemMst itemmst;

    @FXML
    private TextField txtservingSize;

    @FXML
    private TextField txtcalories;

    @FXML
    private Button btnPrint;
    @FXML
    private TextField txtFat;
    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnFinalSave;
    @FXML
    private TextField txtprintername;

    @FXML
    private ComboBox<String> cmbNutrition;

    @FXML
    private TextField txtNutritionValue;

    @FXML
    private TableView<NutritionValueDtl> tblNutrition;
    @FXML
    private TableColumn<NutritionValueDtl, String> clNutrition;

    @FXML
    private TableColumn<NutritionValueDtl, String> clValue;
    

    @FXML
    private TableColumn<NutritionValueDtl, String> clSerial;
    @FXML
    private TableColumn<NutritionValueDtl, String> clPercentage;
    @FXML
    private TextField txtpercentage;
    @FXML
    private Button btnAdd;
    @FXML
    private TextField txtSerial;
    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFetch;

    @FXML
    private Button btnClear;

    
    @FXML
    private TextField txtNoOfcopies;
    
    

	IngredientsMst ingredientMst = null;

	@FXML
	private TextField txtBarItemName;

	@FXML
	private TextField txtPrinterName;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtMrp;

	@FXML
	private TextField txtDuration;

    @FXML
    private TextField txtBatch;

	
	@FXML
	private TextField txtbestBefore;

	@FXML
	private TextField txtIngredient1;

	@FXML
	private TextField txtIngredient2;

	@FXML
	private TextField txtQty;

	@FXML
	private DatePicker dpProduction;

	@FXML
	private Button btnBarPrint;

	@FXML
	private Button btnCalibrate;

    @FXML
	private void initialize()
	{
    	dpProduction = SystemSetting.datePickerFormat(dpProduction, "dd/MMM/yyyy");
    	System.out.print("inside the barcodenutrition controller");
    	txtItemName.setVisible(false);
  
    	cmbNutrition.setVisible(false);
    	txtNutritionValue.setVisible(false);
    	txtpercentage.setVisible(false);
    	txtSerial.setVisible(false);
    	txtSerial.setVisible(false);
    	btnFetch.setVisible(false);
    	btnClear.setVisible(false);
    	btnBarPrint.setVisible(false);
    	txtNoOfcopies.setVisible(false);
//    	eventBus.register(this);
		dpProduction.setValue(LocalDate.now());
		txtIngredient1.setEditable(false);
//		txtIngredient2.setEditable(false);
	//	txtDuration.setEditable(false);
		txtMrp.setEditable(false);
		txtbestBefore.setEditable(false);
		txtBarcode.setEditable(false);
//		String PrinterNAme = SystemSetting.printer_name;
//		txtPrinterName.setText(PrinterNAme);
		txtDuration.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtDuration.setText(oldValue);
				}
			}
		});
	
		
	
	
    	txtservingSize.setEditable(false);
    	txtcalories.setEditable(false);
    	txtFat.setEditable(false);
    	txtpercentage.setEditable(false);
    	txtSerial.setEditable(false);
    //	txtprintername.setEditable(false);
    	txtNutritionValue.setEditable(false);
    	btnDelete.setVisible(false);
    	btnAdd.setVisible(false);
    	btnFinalSave.setVisible(false);
    	txtprintername.setText(SystemSetting.getNutrition_facts_printer());
    	
    	
    	ResponseEntity<List<NutritionMst>> savedNutritions = RestCaller.getAllNutritions();
    	nutritionList = FXCollections.observableArrayList(savedNutritions.getBody());
    	for (NutritionMst nutrtionMst:nutritionList)
    	{
    	cmbNutrition.getItems().add(nutrtionMst.getNutrition());
    	}
    	
    	
    	tblNutrition.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
					
					if (null != newSelection.getId()) {
						nutritionValueDtl = new NutritionValueDtl();
						nutritionValueDtl.setId(newSelection.getId());

					}

				}
			
		});
	}
    
    

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {
	
		txtBarItemName.clear();
		txtBarcode.clear();
		txtBarcode.clear();
		txtIngredient1.clear();
		txtDuration.clear();
		txtDuration.clear();
		txtMrp.clear();
		txtIngredient2.clear();
		System.out.println("------@@@-------popupItemlistner-------@@@@------" + itemPopupEvent.getItemName());
		Stage stage = (Stage) btnCalibrate.getScene().getWindow();
		if (stage.isShowing()) {


			txtBarItemName.setText(itemPopupEvent.getItemName());

			String itemId = itemPopupEvent.getItemId();

			
			  ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(itemId);
			  if
			  (respentity == null) {
			  
			  return;
			  }
			 

			ItemMst itemMst = respentity.getBody();
			if(null!=itemMst.getBarCode()) {
				txtBarcode.setText(itemPopupEvent.getBarCode());
			}
			txtMrp.setText(String.valueOf(itemMst.getStandardPrice()));

			if(null!=itemMst.getBarCodeLine1()) {
				txtIngredient1.setText(itemMst.getBarCodeLine1());
				
			}
			
			if(null!=itemMst.getBarCodeLine2()) {
				txtIngredient2.setText(itemMst.getBarCodeLine2());
				
			}
			
			if(null!=itemMst.getBestBefore()) {
				txtDuration.setText(itemMst.getBestBefore() + "");
				LocalDate bestBeforeDays = dpProduction.getValue();
				LocalDate expDate = bestBeforeDays.plusDays(itemMst.getBestBefore());
				txtbestBefore.setText(String.valueOf(expDate));
				txtQty.requestFocus();
			}else {
				txtDuration.requestFocus();
			}

		
		}
		addItems();
	}
    
	/*
	 * @Subscribe public void popupItemlistner(ItemPopupEvent itemPopupEvent) {
	 * 
	 * System.out.println("------@@@-------popupItemlistner-------@@@@------" +
	 * itemPopupEvent.getItemName()); Stage stage = (Stage)
	 * btnCalibrate.getScene().getWindow(); if (stage.isShowing()) {
	 * 
	 * 
	 * txtBarItemName.setText(itemPopupEvent.getItemName()); String itemId =
	 * itemPopupEvent.getItemId(); ResponseEntity<ItemMst> respentity =
	 * RestCaller.getitemMst(itemId); if (respentity == null) {
	 * 
	 * return; }
	 * 
	 * ItemMst itemMst = respentity.getBody(); if(null!=itemMst.getBarCode()) {
	 * txtBarcode.setText(itemMst.getBarCode()); }
	 * txtMrp.setText(String.valueOf(itemMst.getStandardPrice()));
	 * 
	 * if(null!=itemMst.getBarCodeLine1()) {
	 * txtIngredient1.setText(itemMst.getBarCodeLine1()); }
	 * 
	 * if(null!=itemMst.getBarCodeLine2()) {
	 * txtIngredient2.setText(itemMst.getBarCodeLine2()); }
	 * 
	 * txtBatch.setText(itemPopupEvent.getBatch());
	 * if(null!=itemMst.getBestBefore()) {
	 * txtDuration.setText(itemMst.getBestBefore() + ""); LocalDate bestBeforeDays =
	 * dpProduction.getValue();
	 * 
	 * LocalDate expDate = bestBeforeDays.plusDays(itemMst.getBestBefore());
	 * txtbestBefore.setText(String.valueOf(expDate)); txtQty.requestFocus(); }else
	 * { txtDuration.requestFocus(); } addItems();
	 * 
	 * } addItems(); }
	 */
	
	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@FXML
	void showpopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			eventBus.register(this);
			showPopup();
			txtQty.requestFocus();

			/*
			 * if(txtDuration.getText().equalsIgnoreCase(null)) {
			 * 
			 * txtDuration.requestFocus(); }
			 * 
			 * if(txtIngredient1.getText().equalsIgnoreCase(null)) {
			 * txtIngredient1.requestFocus();
			 * }if(txtIngredient2.getText().equalsIgnoreCase(null)) {
			 * txtIngredient2.requestFocus();
			 * 
			 * } else { txtQty.requestFocus(); }
			 */
		}

	}

	 @FXML
	    void onActionBestBefore(ActionEvent event) {

		 if (null==txtIngredient1.getText()) {
			 txtIngredient1.requestFocus();
		 }
		 
	    }

		@FXML
		void actionOnIngredients(KeyEvent event) {
			if (event.getCode() == KeyCode.ENTER) {
				txtIngredient2.requestFocus();
			}

		}
		@FXML
		void actionOnIngredients2(KeyEvent event) {

			if (event.getCode() == KeyCode.ENTER) {
				txtQty.requestFocus();
			}

		}
		@FXML
		void actionPressedTo(KeyEvent event) {
			if (event.getCode() == KeyCode.ENTER) {
				 if (txtIngredient1.getText().trim().isEmpty()) {
					txtIngredient1.requestFocus();

				} else if (txtIngredient2.getText().trim().isEmpty()) {
					txtIngredient2.requestFocus();
				} else {
					txtQty.requestFocus();
				}
			}

		}

		@FXML
		void calibrateAction(ActionEvent event) {

			if (txtPrinterName.getText().trim().isEmpty()) {
				notifyMessage(5, "please PrinterName", false);
				return;
			}
			
			String PrinterNAme = SystemSetting.printer_name;
			TSCBarcode.caliberate(PrinterNAme);
		}

		@FXML
		void onActionEnterQty(KeyEvent event) {
			if (event.getCode() == KeyCode.ENTER) {
				
				if(null==txtDuration.getText()||txtDuration.getText().trim().length()==0) {
					txtDuration.requestFocus();
				}
				 else {
					btnPrint.requestFocus();
				}
			}

		}

		private void print() {
			if (txtBarItemName.getText().trim().isEmpty()) {
				notifyMessage(5, "please select item", false);
				return;
			}
			if (txtDuration.getText().trim().isEmpty()) {
				notifyMessage(5, "please enter Duration", false);
				return;
			}
			if (txtbestBefore.getText().trim().isEmpty()) {
				notifyMessage(5, "please enter best before date", false);
				return;
			}
			if (txtIngredient1.getText().trim().isEmpty())

			{
				txtIngredient1.setText("");
				// notifyMessage(5, "please incredient", false);
				// return;
			}
			if (txtQty.getText().trim().isEmpty()) {
				notifyMessage(5, "please Qty", false);
				return;
			}
			if (txtPrinterName.getText().trim().isEmpty()) {
				notifyMessage(5, "please PrinterName", false);
				return;
			}
			ResponseEntity<ItemMst> respentity = RestCaller.getItemByNameRequestParam(txtItemName.getText());
			ItemMst itemMst = respentity.getBody();
			 itemMst.setBarCode(txtBarcode.getText());
			itemMst.setBestBefore(Integer.valueOf(txtDuration.getText()));
			itemMst.setBarCodeLine1(txtIngredient1.getText());
			itemMst.setBarCodeLine2(txtIngredient2.getText());
			RestCaller.updateBarcode(itemMst);

			String NumberOfCopies = txtQty.getText();
			String ManufactureDate = "Mf:" + String.valueOf(dpProduction.getValue());
			String ExpiryDate = "Exp:" + txtbestBefore.getText();
			String ItemName = txtItemName.getText();
			String Barcode = txtBarcode.getText();
			String Mrp = "Mrp:" + txtMrp.getText();
			String IngLine1 = txtIngredient1.getText();
			String IngLine2 = txtIngredient2.getText();
			String PrinterNAme = SystemSetting.printer_name;
			
			 // TSCBarcode.manualPrint(NumberOfCopies, ManufactureDate, ExpiryDate, ItemName,
			 // Barcode, Mrp, IngLine1, IngLine2, PrinterNAme);
			 
		   clear();
		   txtItemName.requestFocus();
			
		}
		@FXML
		void PrintAction(ActionEvent event) {
			print();
		}
		 @FXML
		    void actionBestBefore(KeyEvent event) {
				if (event.getCode() == KeyCode.ENTER) {
					if(txtIngredient1.getText().trim().isEmpty()) {
						txtIngredient1.requestFocus();	
						
					
				}
		   }
		   
		    }
		@FXML
		void printEnter(KeyEvent event) {
			if (event.getCode() == KeyCode.ENTER) {

				print();

			}

		}
		private void clear() {

		
			txtBarItemName.clear();
			txtBarcode.clear();
			txtMrp.clear();
			txtDuration.clear();
			txtbestBefore.setText("");
			txtIngredient1.clear();
			txtIngredient2.clear();
			txtQty.clear();
			txtservingSize.clear();
			txtcalories.clear();
			txtFat.clear();
			txtQty.clear();
			dpProduction.setValue(null);
			nutritionList.clear();
			txtBatch.clear();
			txtBarItemName.requestFocus();
		}
	@FXML
	void onActionEnterDuration(ActionEvent event) {
		LocalDate bestBefore = dpProduction.getValue();

		Integer duration = Integer.valueOf(txtDuration.getText());
		LocalDate expDate = bestBefore.plusDays(duration);
		txtbestBefore.setText(String.valueOf(expDate));
//		if (null==txtIngredient1.getText()) {
//			txtIngredient1.requestFocus();
//		}
//		if (null==txtIngredient2.getText()) {
//			txtIngredient2.requestFocus();
//		} else {
//			txtQty.requestFocus();
//		}

	}
	
    @FXML
    void finalSave(ActionEvent event) {
    	if(null != nutritionValueMst)
    	{
    		
    		nutritionValueMst.setItemMst(itemmst);
    		nutritionValueMst.setCalories(txtcalories.getText());
    		nutritionValueMst.setFat(txtFat.getText());
    		nutritionValueMst.setServingSize(txtservingSize.getText());
    		nutritionValueMst.setPrinterName(txtprintername.getText());
    		RestCaller.updateNutritionValueMst(nutritionValueMst);
    		txtcalories.clear();
        	txtFat.clear();
        	txtItemName.clear();
        	txtservingSize.clear();
        	tblNutrition.getItems().clear();
        	txtpercentage.clear();
        	nutritionValueDtl = null;
        	nutritionValueMst = null;
        	nutritionList.clear();
    	}
    }

    @FXML
    void clearItems(ActionEvent event) {

    	txtcalories.clear();
    	txtFat.clear();
    	txtItemName.clear();
    	txtservingSize.clear();
    	tblNutrition.getItems().clear();
    	txtpercentage.clear();
    	nutritionValueDtl = null;
    	nutritionValueMst = null;
    	nutritionList.clear();
    	
    }

    @FXML
    void loadItem(MouseEvent event) {
    	
    	showItem();
    }
  
    
    @FXML
    void deleteItems(ActionEvent event) {
    	if(null != nutritionValueDtl.getId())
    	{
    		RestCaller.deleteNutritionValueDtl(nutritionValueDtl.getId());
    		ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller.getNutritionValueDtlByMstId(nutritionValueMst);
        	nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
        	fillTable();
    	}

    }
  
    @FXML
    void AddItems(ActionEvent event) {

  
    	addItems();
    }
    private void addItems()
    {

    	ResponseEntity<NutritionValueMst> savedNutritionMst = RestCaller.getNutritionValueMstByItemMst(itemmst);

    	if(null != savedNutritionMst.getBody())
    	{
    		nutritionValueMst = new NutritionValueMst();

    		fetchData();
    	}
    	
    	if(null == nutritionValueMst)
    	{
    		nutritionValueMst = new NutritionValueMst();
    		nutritionValueMst.setItemMst(itemmst);
    		nutritionValueMst.setCalories(txtcalories.getText());
    		nutritionValueMst.setFat(txtFat.getText());
    		nutritionValueMst.setServingSize(txtservingSize.getText());
    		nutritionValueMst.setPrinterName(txtprintername.getText());

    		ResponseEntity<NutritionValueMst> respentity = RestCaller.saveNutritionValueMst(nutritionValueMst);
    		nutritionValueMst = respentity.getBody();
    	}
    		
    	if(txtSerial.getText().isEmpty())
    	{
    		notifyMessage(5,"Please Type Serial Number");
    		txtSerial.requestFocus();
    		return;
    	}
    	nutritionValueDtl = new NutritionValueDtl();
    	nutritionValueDtl.setNutritionValueMst(nutritionValueMst);
    	nutritionValueDtl.setNutrition(cmbNutrition.getSelectionModel().getSelectedItem());
    	nutritionValueDtl.setValue(txtNutritionValue.getText());
    	
    	nutritionValueDtl.setPercentage(txtpercentage.getText());
    	
    	nutritionValueDtl.setSerial(txtSerial.getText());
    	
    	ResponseEntity<NutritionValueDtl> getNutrition = RestCaller.getNutritionValueMstByItemMstAndNutrition(nutritionValueMst.getId(),cmbNutrition.getSelectionModel().getSelectedItem());
    	if(null != getNutrition.getBody())
    	{
    		nutritionValueDtl = getNutrition.getBody();
    		
    		nutritionValueDtl.setNutritionValueMst(nutritionValueMst);
        	nutritionValueDtl.setNutrition(cmbNutrition.getSelectionModel().getSelectedItem());
        	nutritionValueDtl.setValue(txtNutritionValue.getText());
        	
        	nutritionValueDtl.setPercentage(txtpercentage.getText());
        	
        	nutritionValueDtl.setSerial(txtSerial.getText());
        	
    		RestCaller.updateNUtritionValueDtl(nutritionValueDtl);
    		ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller.getNutritionValueDtlByMstId(nutritionValueMst);
        	nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
    	}
    	else
    	{
    	ResponseEntity<NutritionValueDtl> respentity = RestCaller.saveNutritionValueDtl(nutritionValueDtl);
    	nutritionValueDtl = respentity.getBody();
    	nutrintionValueList.add(nutritionValueDtl);
    	}
    	fillTable();
    	cmbNutrition.getSelectionModel().clearSelection();
    	txtNutritionValue.clear();
    	txtpercentage.clear();
    	txtSerial.clear();
    	cmbNutrition.requestFocus();
    }

    @FXML
    void fetchData(ActionEvent event) {

    	 fetchData();
    }
    private void fetchData()
    {
    	ResponseEntity<NutritionValueMst> savedNutritionMst = RestCaller.getNutritionValueMstByItemMst(itemmst);
    
    	nutritionValueMst = savedNutritionMst.getBody();
    	if(null !=nutritionValueMst )
    	{
    	txtcalories.setText(savedNutritionMst.getBody().getCalories());
    	txtFat.setText(savedNutritionMst.getBody().getFat());
    	 txtservingSize.setText(savedNutritionMst.getBody().getServingSize());  	
    	 //txtprintername.setText(SystemSetting.getNutrition_facts_printer());
    	ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller.getNutritionValueDtlByMstId(nutritionValueMst);
    	nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
    	fillTable();
    	}
    	else
    	{
    		txtcalories.clear();
    		txtFat.clear();
    		txtItemName.clear();
    		txtNutritionValue.clear();
    		txtpercentage.clear();
    		txtSerial.clear();
    		txtservingSize.clear();
    		tblNutrition.getItems().clear();
    	}
    }
    private void fillTable()
    {
    	tblNutrition.setItems(nutrintionValueList);
		clNutrition.setCellValueFactory(cellData -> cellData.getValue().getnutritionNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());
		clPercentage.setCellValueFactory(cellData -> cellData.getValue().getpercentageProperty());
		clSerial.setCellValueFactory(cellData -> cellData.getValue().getserialProperty());


    }
    @Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			itemmst = new ItemMst();
			txtItemName.setText(itemPopupEvent.getItemName());
			itemmst.setId(itemPopupEvent.getItemId());
			ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(itemmst.getId());
			itemmst = respentity.getBody();
			
			// txtQty.setText(Integer.toString(itemPopupEvent.getQty()));

		}

	}

    @FXML
    void servingSizeOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtcalories.requestFocus();
    	}

    }
    @FXML
    void fatOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		cmbNutrition.requestFocus();
    	}
    }
    @FXML
    void valueOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtpercentage.requestFocus();
    	}
    }
    @FXML
    void percentageOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtSerial.requestFocus();
    	}
    }
    @FXML
    void serialOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		btnAdd.requestFocus();
    	}
    }

    @FXML
    void addOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		addItems();
    	}
    }

    @FXML
    void caloriesOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtFat.requestFocus();
    	}
    }
    @FXML
    void nutritionOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtNutritionValue.requestFocus();
    	}
    }
    private void showItem()
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			txtservingSize.requestFocus();
			stage.show();

		} catch (IOException e) {
			
			e.printStackTrace();
		}
    }
    public void notifyMessage(int duration, String msg) {
  		System.out.println("OK Event Receid");
  		Image img = new Image("done.png");
  		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
  				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
  				.onAction(new EventHandler<ActionEvent>() {
  					@Override
  					public void handle(ActionEvent event) {
  						System.out.println("clicked on notification");
  					}
  				});
  		notificationBuilder.darkStyle();
  		notificationBuilder.show();
  	}
    
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    
    @FXML
    void actionPrint(ActionEvent event) {
    	if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter item  name", false);
			return;
		}
    	if (txtservingSize.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter item name ", false);
			return;
		}
    	if (txtcalories.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter calories", false);
			return;
		}
    	if (txtFat.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter fat", false);
			return;
		}
    	if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter no of copies", false);
			return;
		}
    	String itemName=txtBarItemName.getText();
    	String servingSize=txtservingSize.getText();
    	String calories=txtcalories.getText();
    	String fat=txtFat.getText();
    	String batch = txtBatch.getText();
    	Integer nofCopies=Integer.valueOf(txtQty.getText());
    
		String ManufactureDate = "Mf:" + String.valueOf(dpProduction.getValue());
		String ExpiryDate = "Exp:" + txtbestBefore.getText();
		String Barcode = txtBarcode.getText();
		String Mrp = "Mrp:" + txtMrp.getText();
		String IngLine1 = txtIngredient1.getText();
		String IngLine2 = txtIngredient2.getText();
    	if (txtprintername.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter printer name", false);
			return;
		}
    	String printerName=SystemSetting.getNutrition_facts_printer();
  	     List<NutritionValueDtl> dtlList=new ArrayList<NutritionValueDtl>();
		  for(int i=0;i<nutrintionValueList.size();i++) {
			  
			  System.out.println("Looping for Nutri value ");
			  
			  NutritionValueDtl nutritionDtl =new NutritionValueDtl();
			  nutritionDtl.setNutrition(nutrintionValueList.get(i).getNutrition());
			  nutritionDtl.setValue(nutrintionValueList.get(i).getValue());
			  nutritionDtl.setPercentage(nutrintionValueList.get(i).getPercentage());
			  nutritionDtl.setSerial(nutrintionValueList.get(i).getSerial());
			  dtlList.add(nutritionDtl);
		  
		  }


		System.out.println("Starting pring fn");
    	TSCBarcode.barcodeNutritionValeuCombained(printerName,itemName, servingSize, calories, fat,dtlList ,nofCopies,ManufactureDate,ExpiryDate,Barcode,Mrp,IngLine1,IngLine2,batch);
    	clear();
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


      private void PageReload() {
      	
}
    
}
