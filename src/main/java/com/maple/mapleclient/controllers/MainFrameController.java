package com.maple.mapleclient.controllers;

import java.awt.event.FocusEvent;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.MenuMst;
import com.maple.mapleclient.entity.MenuWindowMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

@Component
public class MainFrameController implements Initializable {

	String taskid;
	String processInstanceId;

	public HashMap menuWindowQueue = new HashMap();



	EventBus eventBus = EventBusFactory.getEventBus();



	// version4.4
	Node supplierwisePurchaseReport;

	Node fastMovingItem;

//
	Node openingBalanceConfig;
	Node openingBalance;
	Node customerwiseSalesReport;
	Node itemLocationReport;
	Node itemLocation;
	Node urs;
	Node purchaseSchemeMst;
	Node kitchenCategoryDtl;
	// version4.4 end
//
//	// -------------version 4.19
	Node otherBranchSale;
//	// ------------version 4.19 end
//
//	// -------------version 4.8
	Node dailyCakeSetting;
//
	Node groupwisesalesreport;
//
//	// ------------version 4.8 end
//	// -------------version 4.9
//
	Node saleOrderHomeDeliveryReport;
//
	// ------------version 4.9 end
	// version3.6sari
	Node kitReport;
	// version3.6end
	Node consumptionReason;
	Node stockCorrection;
	// version2.5
	// Node OtherBranchsale;
	// version2.5end
	// version2.0
	Node financialYear;
	// version2.0ends
	// -------------version 4.12
	Node subBranchSale;
	// ------------version 4.12 end

	Node insuranceCompanySales;
	// version1.7
	Node PurchaseOrderMgmant;
	Node pharmacyPurchaseReport;
	// version1.7 end-- bt contiues

	Node pharmacyCorporateSales;
	Node productionQtyGraph;
	Node KotCateogry;
	Node Rcproot; // Receipt Window
	// Node Sales; // Payment Window.
	Node finance;
	Node productionGraph;
	Node salesPropertyConfig;
	Node deliveryBoyPendingReport;
	Node puchaseOrder;
	Node serviceIn;
	Node jobCardIn;
	Node ServiceTree;
	Node ServiceReportTree;
	Node ServiceInReport;
	Node JobcardReport;
	Node JobcardOut;
	Node ActiveJobCardReport;
	Node serviceOrItemProfit;
	Node dbInitialization;
	Node SalesAnalysis;

	Node salesDeletDtl;
	Node printer;
	Node productConversionConfig;
	Node StockTransferOutReport;
	Node StoreChange;
	Node StoreCreation;
	Node TallyIntegration;
	Node TakeAwayReport;
	Node postToServerReTry;

	Node AllCustomerBalance;
	Node groupwisesalesdtlreport;
	Node salesReturnReport;
	Node receiptEdit;
	Node itemWiseShortExpiry;
	Node stockVerification;
	Node intentStockTransfer;
	Node intentReport;
	Node categoryWiseShortExpiry;
	Node receiptInvReport;
	Node supPayments; // q Window for testing.
	Node kitde;
	Node SalesType;
	Node home;
	Node AcceptStock;
	Node customerwiseReport;
	Node custReciepts;
	Node SalesOrderReport;
	Node AddSupplier;
	Node Purchaseroot;
	Node customerBillBalance;
	Node pos;
	Node salesModeWiseSummary;
	Node wholesale;
	Node kotPos;
	Node onlinesale;
	Node ItemMstr;
	Node cmd;
	Node StockTransfer;
	Node CustomerReg;
	Node DayEnd;
	Node Intent;
	Node Tax;
	Node ReOrder;
	Node Sample;
	Node PriceDefenition;
	Node kotManagement;
	Node physicalStock;
	Node Journal;
	Node Production;
	Node changepassword;
	Node groupcreation;
	Node SessionEnd;
	Node SaleOrder;
	Node ProductConversion;
	Node AccountHeads;
	Node Scheme;
	Node KotPOS;
	Node ActualProduction;
	Node ReportTree;
	Node AdvanceFeatures;
	Node userreg;
	Node BranchCreation;
	Node Process;
	Node CompanyCreations;
	Node menuCreations;
	Node subscriptionValidity;
	Node domainInitialize;

	Node reportSqlCommand;

	Node ExecuteSql;
	Node ProcessPermission;
	Node GroupPermission;
	Node UserGroupPermission;
	Node Multiunit;
	Node VoucherReprint;
	Node publishMessages;

	Node productionAndPlaningReport;

	Node tableOccupied;

	Node consumptionReport;

	Node saleOrderPendingReport;
	Node MemberMst;
	Node MeetingMinutes;
	Node MeetingMst;
	Node PasterMst;
	Node PasterDtl;
	Node subGroupMember;
	Node OrgTree;
	Node Organisation;
	Node StockTransferInReport;
	Node ItemStockReport;
//	// version 1.4
//	Node purchaseOrderEdit;
//	// node for version1.4 end--
//
	Node MeetingMemberDetail;
	Node OrgReceipts;
	Node OrgPayments;
	Node orgAccBalance;
	Node OrgAccPayReport;
	Node MemberWiseReceiptReport;
	Node AllMemWiseReceiptReport;
	Node StockTransferOutDtlReport;
	Node categoryManagement;
// new node 
	Node consumption;
	Node itemWiseTaxCreation;
	Node PaymentInvReport;
	Node dailySalesreport;
	Node ownAccSettlement;
	Node dayBookReport;
	Node ItemBranch;
	Node DayEndClosure;
	Node SalesFromOrder;
	Node DamageEntry;
	Node SchemeDetails;
	Node ItemOrCategorySaleReport;
	Node SaleOrderStatusChecking;
	Node MonthlyReports;
	Node SaleOrderReports;
	Node AccountBalance;
	Node ReceiptMode;
	Node StockReport;
	Node InventoryReport;
	Node SupplierLedger;
	Node SupplierBillwise;
	Node SaleOrderBalAdvReport;
	Node DailyOrderDetails;
	Node DeliveryboyReport;
	Node HomedeliveryReport;
	Node Familycreation;
	Node Eventcreation;
	Node sundryDebitReport;
	Node CardReconcileReport;
	Node bargraph;
	Node shortExpiryReport;
	Node BatchPriceDefinition;
	Node NutritionValue;
	Node RawMaterialReturn;
	Node rawMaterialIssue;
	Node RawMaterialIssueReport;
	Node RawMaterialReport;
	Node ConfigTree;
	Node accountsTree;
	Node SaleSummaryReport;
	Node OtherReciepts;
	Node OtherPayments;
	Node PettyCashPayments;
	Node b2bSaleReport;
	Node SalesReport;
	Node PettyReciepts;
	Node SaleOrderEdit;
	Node SiteCreation;
	Node damageEntry;
	Node CustomerBillwise;
	Node customerLedger;
	Node InvoiceEdit;
	Node SalesReturn;
	Node actualProductionReport;
	Node PurchaseReport;
	Node barcodePrinting;
	Node CategoryWiseStockReport;
	Node ItemMerge;
	Node LanguageMst;
	Node ItemNameInLanguage;
	Node RawMaterialRequiredReport;
	Node productionPlanningReport;
	Node NewWholeSaleWindow;
	Node CustomerSalesReport;
	Node PDCReceipts;
	Node PDCPayment;
	Node PDCReconsile;
	Node productConversionreport;
	Node serviceItemCreation;
	Node locationMst;
	Node JobCardPreparation;
	Node trialBalance;
	Node balanceSheet;
	Node SupplierDueReport;
	Node branchProfit;
	Node StockValueReport;

	Node categoryWiseStockMovmnt;
	Node serviceItemMst;

	Node NutritionFacts;
	Node barcodenutritioncombined;
	// -------------version 4.10
	Node dailyOrderReport;
	Node journalReports;
//
//	// ------------version 4.10 end
//
	Node HsnCodeSaleReport;
	Node hsnCodePurchaseReport;
	Node productionTree;

	Node itemImageMst;
	Node receiptmodewisereport;
	Node voucherdateedit;
	Node barcodeConfiguration;
	Node taxReport;

	Node machineResource;
	Node resourceCatItemLink;
	Node subCategory;
	Node ProductionPrePlaning;
	Node paymentEdit;
	Node OrgPendingReport;
	Node checkPrint;
	Node receiptReports;
	Node paymentReports;
//
//	// ------------------version 6.9 surya
//
	Node stockTransferDeletedReport;
//	// ------------------version 6.9 surya end
////--------------------versio 6.11 surya
	Node exceptionList;
	Node weighingMachine;
	Node weighingMachineReport;

	Node journalEdit;
	Node reasonWiseconsumptionReport;
	Node monthlySalesreport;

	Node itemStockEnquiry;

	Node categoryUpdation;
	Node closingBalanceReport;
	Node inHouseConsumptionReport;
	Node accountMerge;
	Node otherBranchSaleReport;
	Node physicalStockReport;
	Node categoryExceptionList;

	Node salesReportByPriceType;
	Node taxInvoiceByPriceType;

	Node fastMovingItemReportBySalesMode;
	Node itemWiseIntentSummaryReport;

	Node pharmacyitemorcategorywisestockmovement;
	Node salesManCreation;
//
//	// ----------------------------------------PHARMACY----------------
	Node pharmacySalesReturnReport;
//  
//	// --------------------------------------PHARMACY END-----------------
	Node stockTransferInDetailReport;
	Node incomeAndExpenceReport;
	Node accoutnSlNoUpdation;
	Node TallyUrl;
	Node MenuConfig;

	// ----------------sharon pharmacy reports---------
	
	Node pharmacyReports;
	Node bincardreport;
	Node branchstockmarginreport;
	Node branchstockreport;
	Node consumptiondetailreport;
	Node consumptionsummaryreport;
	
	//**
	Node dailysalesreport;
	Node dayclosingreport;
    Node gstinputdetailreport;
     Node gstinputsummaryreport; 
      Node gstoutputdetailreport;
      Node gstoutputsummaryreport;
      Node importpurchasesummaryreport;
      Node insurancereportfromserver;
      Node inverntorystockreport;
      Node localpurchasedetail;
      Node localpurchasesummaryreport;
      Node physicalstockvariance;
      Node policereport;
      Node purchasereturndetail;
      Node purchasereturnsummary;
      Node retailsalesdetail;
      Node retailsalessummary;
      Node salesreportcustomerwisesalesreport;
      Node salesreportdailysalestransaction;
      Node salesreportgroupwisesales;
      Node salesreportgroupwisesalesdetails;
      Node salesreportitemwisesalesreport;
      Node salesreportsalesreturnreport;
      Node salesreportwriteoffdetailsclientreport;
      Node salesreportwriteoffsummaryreport;
      Node salesreturndetail;
      Node salesreturnsummary;
      Node shortexpiryreport;
      Node stocktransferindetail;
      Node stocktransferinsummary;
      Node stocktransferoutdetails;
      Node stocktransferoutsummary;
      Node wholesalesdetail;
      Node wholesalessummary;
      Node writeoffdetailsreport;
      Node writeoffsummaryreport;
      Node dayendreport;
      
	// --------------------versio 6.11 surya end

// CODE AREA - 1 JobCardPreparationCtl
	// Define other Windows here.

	@FXML
	Button HomeBtn;

	@FXML
	private Pane functionSideBar;

	@FXML
	VBox sideVBOX;

	@FXML
	private AnchorPane mainWorkArea; // Main Pane where the sub window is loaded

	@FXML
	private void handleHomeAction(ActionEvent event) {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/tasklist-1000x600.fxml"));
			Parent root = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void anchorarea1(MouseEvent event) {

	}

	@FXML
	void anchorarea2(MouseEvent event) {

	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		eventBus.register(this);
		MapleclientApplication.mainWorkArea = mainWorkArea;

		MapleclientApplication.mainFrameController = this;

		sideVBOX.getChildren().clear();

		try {

			ConfigTree = FXMLLoader.load(getClass().getResource("/fxml/ConfigTree.fxml"));

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		if (!SystemSetting.systemBranch.equalsIgnoreCase("CLOUD")) {

			ResponseEntity<List<MenuConfigMst>> menuTaskListResp = RestCaller.MenuConfigMstByMenuName("TASK LIST");
			List<MenuConfigMst> menuTaskList = menuTaskListResp.getBody();

			if (null != menuTaskList && menuTaskList.size() > 0) {
				ResponseEntity<MenuMst> menuMstTaskListResp = RestCaller
						.getMenuMstByMenuId(menuTaskList.get(0).getId());
				MenuMst menuTaskListMst = menuMstTaskListResp.getBody();

				if (null != menuTaskListMst) {

					final Button actionBtnPos = new Button(menuTaskList.get(0).getMenuName());
					actionBtnPos.setMinWidth(HomeBtn.getMinWidth());
					actionBtnPos.setMaxWidth(HomeBtn.getMaxWidth());
					actionBtnPos.setPrefWidth(HomeBtn.getPrefWidth());
					actionBtnPos.setMinHeight(HomeBtn.getMinHeight());
					actionBtnPos.setMaxHeight(HomeBtn.getMaxHeight());
					actionBtnPos.setPrefHeight(HomeBtn.getPrefHeight());

					actionBtnPos.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							// removeUnderLine();
							// actionBtnPos.setUnderline(true);

							// Clear screen before loading the Page.

							Node dynamicWindow = (Node) menuWindowQueue.get(menuTaskList.get(0).getMenuName());
							if (null == dynamicWindow) {
								if (null != menuTaskList.get(0).getMenuFxml()) {
									try {
										dynamicWindow = FXMLLoader.load(
												getClass().getResource("/fxml/" + menuTaskList.get(0).getMenuFxml()));
										menuWindowQueue.put(menuTaskList.get(0).getMenuName(), dynamicWindow);

									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}

								// task newSelection.getId()
							}

							try {

								// Clear screen before loading the Page.
								if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
									MapleclientApplication.mainWorkArea.getChildren().clear();

								MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

								MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
								MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
								MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
								MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
								dynamicWindow.requestFocus();

							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					});
					if (SystemSetting.UserHasRole(menuTaskList.get(0).getMenuName())) {

						System.out.println(SystemSetting.getUser_roles());
						sideVBOX.getChildren().add(actionBtnPos);
					}

				}
			}

			ResponseEntity<List<MenuMst>> menuMstResp = RestCaller.getAllMenuMst();
			List<MenuMst> menuMstList = menuMstResp.getBody();
			for (MenuMst menuMst : menuMstList) {
				if (null == menuMst.getParentId()) {
					if (null != menuMst.getMenuId()) {
						ResponseEntity<MenuConfigMst> menuConficMst = RestCaller.MenuConfigMstById(menuMst.getMenuId());
						MenuConfigMst menuConfigMst = menuConficMst.getBody();

						if (null != menuConfigMst) {
							if (!menuConfigMst.getMenuName().equalsIgnoreCase("TASK LIST")) {

								final Button actionBtnPos = new Button(menuConfigMst.getMenuName());
								actionBtnPos.setMinWidth(HomeBtn.getMinWidth());
								actionBtnPos.setMaxWidth(HomeBtn.getMaxWidth());
								actionBtnPos.setPrefWidth(HomeBtn.getPrefWidth());
								actionBtnPos.setMinHeight(HomeBtn.getMinHeight());
								actionBtnPos.setMaxHeight(HomeBtn.getMaxHeight());
								actionBtnPos.setPrefHeight(HomeBtn.getPrefHeight());

								actionBtnPos.setOnMouseClicked(event -> {

									// removeUnderLine();
									// actionBtnPos.setUnderline(true);

									// Clear screen before loading the Page.

									Node dynamicWindow = (Node) menuWindowQueue.get(menuConfigMst.getMenuName());

									if (null == dynamicWindow) {
										if (null != menuConfigMst.getMenuFxml()) {
											try {
												// FXMLLoader loader = (FXMLLoader)
												// menuWindowQueue.get(menuConfigMst.getMenuName());

												System.out.println(menuConfigMst.getMenuFxml());
												FXMLLoader loader = new FXMLLoader(
														getClass().getResource("/fxml/" + menuConfigMst.getMenuFxml()));

												dynamicWindow = loader.load();

												System.out.println(loader.getController().getClass());

//												if (loader.getController().getClass().isAssignableFrom(
//														com.maple.mapleclient.controllers.SupplierCtl.class)) {
//
//													SupplierCtl sup = (SupplierCtl) loader.getController();
//													System.out.print(sup.setCustomerVoucherProperty("HELLO"));
//												}
												if (loader.getController().getClass().isAssignableFrom(
														com.maple.mapleclient.controllers.KOTPosWindowCtl.class)) {

													KOTPosWindowCtl kot = (KOTPosWindowCtl) loader.getController();
													if (null == menuConfigMst.getCustomiseSalesMode()) {
														kot.setCustomerVoucherProperty("NONE");
														kot.addTableWaiter();
													} else {
														kot.setCustomerVoucherProperty(
																menuConfigMst.getCustomiseSalesMode());
														kot.loadInpProgressKot();
													}

												}

												dynamicWindow.setOnKeyPressed(evt -> {

													if (evt.getCode().equals(KeyCode.F2)) {
														System.out.println("F2 Pressed");
													}
												});

												/*
												 * dynamicWindow = FXMLLoader.load( getClass().getResource("/fxml/" +
												 * menuConfigMst.getMenuFxml()));
												 */

												menuWindowQueue.put(menuConfigMst.getMenuName(), dynamicWindow);

											} catch (IOException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}

										}

										// task newSelection.getId()
									}

									try {

										// Clear screen before loading the Page.
										if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
											MapleclientApplication.mainWorkArea.getChildren().clear();

										MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

										MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
										MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
										MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
										MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
										dynamicWindow.requestFocus();

									} catch (Exception e) {
										e.printStackTrace();
									}

								});

								if (SystemSetting.UserHasRole(menuConfigMst.getMenuName())) {
									sideVBOX.getChildren().add(actionBtnPos);
								}

							}
						}
					}
				}

			}

		}
		try {
			BranchCreation = FXMLLoader.load(getClass().getResource("/fxml/branchmst.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			CompanyCreations = FXMLLoader.load(getClass().getResource("/fxml/CompanyCreation.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			menuCreations = FXMLLoader.load(getClass().getResource("/fxml/MenuCreation.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			subscriptionValidity = FXMLLoader.load(getClass().getResource("/fxml/SubscriptionValidity.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		try {
			domainInitialize = FXMLLoader.load(getClass().getResource("/fxml/DomainInitialize.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		try {
			reportSqlCommand = FXMLLoader.load(getClass().getResource("/fxml/ReportSqlCommand.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			userreg = FXMLLoader.load(getClass().getResource("/fxml/userregistration.fxml"));

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		// sharon -----------------------------------pharmacy reports--------------------------------------
		try {
			pharmacyReports = FXMLLoader.load(getClass().getResource("/fxml/PharmacyReportTree.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			branchstockreport = FXMLLoader.load(getClass().getResource("/fxml/BranchStockReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		try {
			bincardreport = FXMLLoader.load(getClass().getResource("/fxml/PharmacyItemStockMovementReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			branchstockmarginreport = FXMLLoader.load(getClass().getResource("/fxml/PharmacyBrachStockMarginReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			dailysalesreport = FXMLLoader.load(getClass().getResource("/fxml/PharmacyDailySalesTransactionReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			dayclosingreport= FXMLLoader.load(getClass().getResource("/fxml/PharmacyDayClosing.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			gstinputdetailreport = FXMLLoader.load(getClass().getResource("/fxml/GSTInputDetailReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			gstinputsummaryreport= FXMLLoader.load(getClass().getResource("/fxml/GSTInputSummaryReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			gstoutputdetailreport = FXMLLoader.load(getClass().getResource("/fxml/GSTOutputDetailReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			gstoutputsummaryreport= FXMLLoader.load(getClass().getResource("/fxml/GSTOutputSummaryReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			importpurchasesummaryreport = FXMLLoader.load(getClass().getResource("/fxml/ImportPurchaseSummaryReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			insurancereportfromserver = FXMLLoader.load(getClass().getResource("/fxml/.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			inverntorystockreport = FXMLLoader.load(getClass().getResource("/fxml/.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			localpurchasedetail= FXMLLoader.load(getClass().getResource("/fxml/PharmacyLocalPurchaseDetailsReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			localpurchasesummaryreport = FXMLLoader.load(getClass().getResource("/fxml/LocalPurchaseSummaryReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			physicalstockvariance= FXMLLoader.load(getClass().getResource("/fxml/PharmacyPhysicalStockVarianceReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			policereport = FXMLLoader.load(getClass().getResource("/fxml/PoliceReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			purchasereturndetail = FXMLLoader.load(getClass().getResource("/fxml/PharmacyPurchaseReturnDetailReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			purchasereturnsummary= FXMLLoader.load(getClass().getResource("/fxml/PurchaseReturnSummaryReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			retailsalesdetail = FXMLLoader.load(getClass().getResource("/fxml/RetailSalesDetailReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			retailsalessummary = FXMLLoader.load(getClass().getResource("/fxml/RetailSalesSummaryReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreportcustomerwisesalesreport = FXMLLoader.load(getClass().getResource("/fxml/PharmacyCustomerwiseSalesReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreportdailysalestransaction = FXMLLoader.load(getClass().getResource("/fxml/PharmacyDailySalesTransactionReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreportgroupwisesales = FXMLLoader.load(getClass().getResource("/fxml/PharmacyGroupWiseSales.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreportgroupwisesalesdetails= FXMLLoader.load(getClass().getResource("/fxml/PharmacyGropWiseSalesDetailReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreportitemwisesalesreport = FXMLLoader.load(getClass().getResource("/fxml/PharmacyItemWiseSalesReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreportsalesreturnreport= FXMLLoader.load(getClass().getResource("/fxml/PharmacySalesReturnReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreportwriteoffdetailsclientreport = FXMLLoader.load(getClass().getResource("/fxml/WriteOffReportDetail.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreportwriteoffsummaryreport= FXMLLoader.load(getClass().getResource("/fxml/WriteOffReportSummary.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreturndetail = FXMLLoader.load(getClass().getResource("/fxml/PharmacySalesReturnDetailReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			salesreturnsummary= FXMLLoader.load(getClass().getResource("/fxml/PharmacySalesReturnReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		try {
			shortexpiryreport= FXMLLoader.load(getClass().getResource("/fxml/ShortExpiryReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			stocktransferindetail= FXMLLoader.load(getClass().getResource("/fxml/StockTransferInDetailReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			stocktransferinsummary = FXMLLoader.load(getClass().getResource("/fxml/.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			stocktransferoutdetails= FXMLLoader.load(getClass().getResource("/fxml/StockTransferOutDetailReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			stocktransferoutsummary= FXMLLoader.load(getClass().getResource("/fxml/.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			wholesalesdetail = FXMLLoader.load(getClass().getResource("/fxml/WholeSalesDetailReportOrginal.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			wholesalessummary = FXMLLoader.load(getClass().getResource("/fxml/WholeSaleSummaryReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			writeoffdetailsreport = FXMLLoader.load(getClass().getResource("/fxml/WriteOffReportDetail.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			writeoffsummaryreport = FXMLLoader.load(getClass().getResource("/fxml/WriteOffReportSummary.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		try {
			dayendreport= FXMLLoader.load(getClass().getResource("/fxml/DayEndReport.fxml"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		
		// sharon --------------------END---------------pharmacy reports--------------------------------------
		

		try {

			MenuConfig = FXMLLoader.load(getClass().getResource("/fxml/MenuConfigMst.fxml"));

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			TallyUrl = FXMLLoader.load(getClass().getResource("/fxml/TallyIntegrationMst.fxml"));

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			AccountHeads = FXMLLoader.load(getClass().getResource("/fxml/AccountHead.fxml"));

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			AccountHeads = FXMLLoader.load(getClass().getResource("/fxml/AccountHead.fxml"));

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			AllMemWiseReceiptReport = FXMLLoader.load(getClass().getResource("/fxml/AllMemberWiseReceiptRerport.fxml"));

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		// -----------------version 4.10
		try {

			PDCPayment = FXMLLoader.load(getClass().getResource("/fxml/PDCPayment.fxml"));

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			userreg = FXMLLoader.load(getClass().getResource("/fxml/userregistration.fxml"));

		} catch (Exception e) {
			System.out.println(e.toString());
		}
	
		final Button actionBtnserviceTree = new Button("MASTER REPORTS");
		actionBtnserviceTree.setMinWidth(HomeBtn.getMinWidth());
		actionBtnserviceTree.setMaxWidth(HomeBtn.getMaxWidth());
		actionBtnserviceTree.setPrefWidth(HomeBtn.getPrefWidth());
		actionBtnserviceTree.setMinHeight(HomeBtn.getMinHeight());
		actionBtnserviceTree.setMaxHeight(HomeBtn.getMaxHeight());
		actionBtnserviceTree.setPrefHeight(HomeBtn.getPrefHeight());
		actionBtnserviceTree.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				try {

					if (!mainWorkArea.getChildren().isEmpty())
						mainWorkArea.getChildren().clear();

					mainWorkArea.getChildren().add(reportSqlCommand);

					mainWorkArea.setTopAnchor(reportSqlCommand, 0.0);
					mainWorkArea.setRightAnchor(reportSqlCommand, 0.0);
					mainWorkArea.setLeftAnchor(reportSqlCommand, 0.0);
					mainWorkArea.setBottomAnchor(reportSqlCommand, 0.0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	
		if (SystemSetting.systemBranch.equalsIgnoreCase("CLOUD")) {
			sideVBOX.getChildren().add(actionBtnserviceTree);
		}
	
		////// -----------------------sharon 30/06/21--------------------
		// sharon
		final Button actionBtnpharmacyreportTree = new Button("PHARMACY REPORTS");
		actionBtnpharmacyreportTree.setMinWidth(HomeBtn.getMinWidth());
		actionBtnpharmacyreportTree.setMaxWidth(HomeBtn.getMaxWidth());
		actionBtnpharmacyreportTree.setPrefWidth(HomeBtn.getPrefWidth());
		actionBtnpharmacyreportTree.setMinHeight(HomeBtn.getMinHeight());
		actionBtnpharmacyreportTree.setMaxHeight(HomeBtn.getMaxHeight());
		actionBtnpharmacyreportTree.setPrefHeight(HomeBtn.getPrefHeight());
		actionBtnpharmacyreportTree.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				try {

					if (!mainWorkArea.getChildren().isEmpty())
						mainWorkArea.getChildren().clear();

					mainWorkArea.getChildren().add(pharmacyReports);

					mainWorkArea.setTopAnchor(pharmacyReports, 0.0);
					mainWorkArea.setRightAnchor(pharmacyReports, 0.0);
					mainWorkArea.setLeftAnchor(pharmacyReports, 0.0);
					mainWorkArea.setBottomAnchor(pharmacyReports, 0.0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {
			sideVBOX.getChildren().add(actionBtnpharmacyreportTree);
		}

		final Button actionBtnConfigTree = new Button("CONFIGURATIONS");
		actionBtnConfigTree.setMinWidth(HomeBtn.getMinWidth());
		actionBtnConfigTree.setMaxWidth(HomeBtn.getMaxWidth());
		actionBtnConfigTree.setPrefWidth(HomeBtn.getPrefWidth());
		actionBtnConfigTree.setMinHeight(HomeBtn.getMinHeight());
		actionBtnConfigTree.setMaxHeight(HomeBtn.getMaxHeight());
		actionBtnConfigTree.setPrefHeight(HomeBtn.getPrefHeight());
		actionBtnConfigTree.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				try {

					if (!mainWorkArea.getChildren().isEmpty())
						mainWorkArea.getChildren().clear();

					mainWorkArea.getChildren().add(ConfigTree);

					mainWorkArea.setTopAnchor(ConfigTree, 0.0);
					mainWorkArea.setRightAnchor(ConfigTree, 0.0);
					mainWorkArea.setLeftAnchor(ConfigTree, 0.0);
					mainWorkArea.setBottomAnchor(ConfigTree, 0.0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		if (SystemSetting.UserHasRole("CONFIGURATIONS")) {
			sideVBOX.getChildren().add(actionBtnConfigTree);
		}

	}

	public void removeUnderLine() {
		// Remove underline for all Menu Action buttons

		Iterator itr = sideVBOX.getChildren().iterator();
		while (itr.hasNext()) {

			try {
				Button btn = (Button) itr.next();
				btn.setUnderline(false);

			} catch (Exception e) {

			}
		}

	}

	public HashMap getMenuWindowQueue() {
		return menuWindowQueue;
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

}
