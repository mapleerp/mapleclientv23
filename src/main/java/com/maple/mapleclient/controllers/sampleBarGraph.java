package com.maple.mapleclient.controllers;

import java.math.BigDecimal;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;

import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PurchaseSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class sampleBarGraph {
	
	String taskid;
	String processInstanceId;

    private ObservableList<String> xaxis = FXCollections.observableArrayList();
    private ObservableList<Double> yaxis = FXCollections.observableArrayList();
    XYChart.Series<String, Double> series;
    List<BranchMst> branchList = new ArrayList<BranchMst>();
    private ObservableList<PurchaseSummaryReport> purchaseList = FXCollections.observableArrayList();
    @FXML
    private Button btnClear;
    @FXML
    private ListView<String> lsBranches;

    @FXML
    private ListView<String> lsSelection;

    @FXML
    private BarChart<String ,Double> barSample;
    
    @FXML
    private CategoryAxis xAxis;

    @FXML
    private NumberAxis yAxis;
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;
    @FXML
    private Button btnShow;
    @FXML
    private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	lsBranches.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	lsSelection.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	lsBranches.getItems().add("ALL BRANCHES");
    	ResponseEntity<List<BranchMst>> getAllBranches = RestCaller.getBranchDtl();
    	for(BranchMst branchMst:getAllBranches.getBody())
    	{
    		lsBranches.getItems().add(branchMst.getBranchCode());
    	}
    	lsSelection.getItems().add("ALL REPORTS");
    	lsSelection.getItems().add("SALES SUMMARY");
    	lsSelection.getItems().add("PURCHASE SUMMARY");
    	lsSelection.getItems().add("EXPENSES SUMMARY");
    	lsSelection.getItems().add("PROFIT SUMMARY");
    	lsSelection.getItems().add("STOCK SUMMARY");
    	lsSelection.getItems().add("BANK SUMMARY");
    	lsSelection.getItems().add("CASH SUMMARY");
    	lsSelection.getItems().add("CREDITORS SUMMARY");
    	lsSelection.getItems().add("DEBTORS SUMMARY");
    	lsSelection.getItems().add("SALES SUMMARY MONTHLY");
    	lsSelection.getItems().add("PURCHASE SUMMARY MONTHLY");
    	lsSelection.getItems().add("EXPENSES SUMMARY MONTHLY");
    	lsSelection.getItems().add("PROFIT SUMMARY MONTHLY");
    	lsSelection.getItems().add("STOCK SUMMARY MONTHLY");
    	lsSelection.getItems().add("BANK SUMMARY MONTHLY");
    	lsSelection.getItems().add("CASH SUMMARY MONTHLY");
    	lsSelection.getItems().add("CREDITORS SUMMARY MONTHLY");
    	lsSelection.getItems().add("DEBTORS SUMMARY MONTHLY");
    	lsSelection.getItems().add("SALES SUMMARY YEARLY");
    	lsSelection.getItems().add("PURCHASE SUMMARY YEARLY");
    	lsSelection.getItems().add("EXPENSES SUMMARY YEARLY");
    	lsSelection.getItems().add("PROFIT SUMMARY YEARLY");
    	lsSelection.getItems().add("STOCK SUMMARY YEARLY");
    	lsSelection.getItems().add("BANK SUMMARY YEARLY");
    	lsSelection.getItems().add("CASH SUMMARY YEARLY");
    	lsSelection.getItems().add("CREDITORS SUMMARY YEARLY");
    	lsSelection.getItems().add("DEBTORS SUMMARY YEARLY");
          
    }
    @FXML
    void clearAction(ActionEvent event) {

    	 barSample.getData().clear();
    	 lsBranches.getSelectionModel().clearSelection();
    	 lsSelection.getSelectionModel().clearSelection();
    	 series.getData().clear();
    	 xaxis.clear();
    	 yaxis.clear();
    }
    public void notifyMessage(int duration, String msg, boolean success) {
		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @FXML
    void actionShow(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5,"Select From Date",false);
    		dpFromDate.requestFocus();
    		return;
    	}
    	if(null == dpToDate.getValue())
    	{

    		notifyMessage(5,"Select To Date",false);
    		dpToDate.requestFocus();
    		return;
    	}
    			
    	 series= new XYChart.Series<String, Double>();
    	LocalDate dpDate1 = dpFromDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strtDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
    	
		 LocalDate dpDate2= dpToDate.getValue();
			java.util.Date tDate  = SystemSetting.localToUtilDate(dpDate2);
			 String  strfDate = SystemSetting.UtilDateToString(tDate, "yyyy-MM-dd");
			 String saveData;
			// saveData = RestCaller.saveAnalyticsTest();
			 List<String> selectedBranches = lsBranches.getSelectionModel().getSelectedItems();
		        
		    	String branches="";
		    	
		    	for(String s:selectedBranches)
		    	{
		    		if(!s.equalsIgnoreCase("ALL BRANCHES"))
		    		{
		    	    	s = s.concat(";");
		    	    	branches= branches.concat(s);
		    		}
		    		else
		    		{
		    			String sbanch="";
		    			ResponseEntity<List<BranchMst>> getBranch = RestCaller.getBranchDtl();
		    			for(BranchMst branchList:getBranch.getBody())
		    			{
		    				sbanch = branchList.getBranchCode().concat(";");
			    	    	branches= branches.concat(sbanch);
		    			}
		    		}
		    	} 
		    
		   List<String> selectedSummary = lsSelection.getSelectionModel().getSelectedItems();
			        
			   String summary="";
			   for(String s:selectedSummary)
			    {
			    	if(s.equalsIgnoreCase("ALL REPORTS"))
			    	{
			    		summary="allreport";
			    	} 
			    	else
			    	{
			    		summary=s;
			    	}
			    }
				Map<String, Object> map = new HashMap<String, Object>();
				map = RestCaller.generateGraph(branches,summary,strtDate,strfDate);
	
				ArrayList xaxisArray = new ArrayList();
				xaxisArray=(ArrayList) map.get("X");
				String branchCode;
				Iterator itr = xaxisArray.iterator();
				while (itr.hasNext()) {
					branchCode = (String) itr.next();
					xaxis.add(branchCode);
				}
				ArrayList yaxisArrayPuchase = new ArrayList();
				yaxisArrayPuchase =(ArrayList) map.get("Yp");
				Double amt;
				if(null != yaxisArrayPuchase)
				{
				Iterator itr2 = yaxisArrayPuchase.iterator();
				while (itr2.hasNext()) {
					 LinkedHashMap element = (LinkedHashMap) itr2.next();
					amt = (Double) element.get("branchPurchaseAmount");
					branchCode = (String) element.get(0);
					yaxis.add(amt);
				}
				series.setName("PURCHASE");
				}
				ArrayList yaxisArraySales = new ArrayList();
				yaxisArraySales =(ArrayList) map.get("Ys");
				if(null != yaxisArraySales)
				{
				Iterator itr1 = yaxisArraySales.iterator();
				while (itr1.hasNext()) {
					 LinkedHashMap element = (LinkedHashMap) itr1.next();
					amt = (Double) element.get("branchSales");
					yaxis.add(amt);
				}
				series.setName("SALES");
				}
				
				
				ArrayList yaxisArrayProfit = new ArrayList();
				yaxisArrayProfit =(ArrayList) map.get("Ypro");
				if(null != yaxisArrayProfit)
				{
				Iterator itr1 = yaxisArrayProfit.iterator();
				while (itr1.hasNext()) {
					 LinkedHashMap element = (LinkedHashMap) itr1.next();
					amt = (Double) element.get("profitSummaryBranch");
					yaxis.add(amt);
				}
				series.setName("PROFIT");
				}
				
				
				ArrayList yaxisArrayExpense = new ArrayList();
				yaxisArrayExpense =(ArrayList) map.get("Ye");
				if(null != yaxisArrayExpense)
				{
				Iterator itr1 = yaxisArrayExpense.iterator();
				while (itr1.hasNext()) {
					 LinkedHashMap element = (LinkedHashMap) itr1.next();
					amt = (Double) element.get("sumOfExpance");
					yaxis.add(amt);
				}
				series.setName("EXPENSE");
				}
			
    	createBarGraph(xaxis,yaxis);
    	
    }
    public void createBarGraph(ObservableList<String> xaxis,ObservableList<Double> yaxis)
    {
    	
    	
    	for(int i = 0;i<xaxis.size();i++)
    	{
    	
    			 series.getData().add(new XYChart.Data<>(xaxis.get(i),yaxis.get(i)));
    		
    		
    	}
    	//series = new XYChart.Series<String, Double>();
    	 barSample.getData().add(series);
//    	}
    	
    }
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


	private void PageReload(String hdrId) {

	}


}
