package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LmsQueueMst;
import com.maple.mapleclient.entity.ProductionDtl;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class StockTransferOutReportCtl {

	String taskid;
	String processInstanceId;

	private ObservableList<StockTransferOutHdr> stockTransferOutHdrList = FXCollections.observableArrayList();
	private ObservableList<StockTransferOutDtl> stockTransferOutDtlList = FXCollections.observableArrayList();

	StockTransferOutHdr stockTransferOutHdr = null;
	@FXML
	private DatePicker dpFromDate;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private Button btnShow;

	@FXML
	private Button btnResend;
	@FXML
	private Button btnPrint;
	@FXML
	private Button btnPrintSummary;
	@FXML
	private TableView<StockTransferOutDtl> tblStockDtl;

	@FXML
	private TableColumn<StockTransferOutDtl, String> clItemName;

	@FXML
	private TableColumn<StockTransferOutDtl, String> clBatch;

	@FXML
	private TableColumn<StockTransferOutDtl, Number> clQty;

	@FXML
	private TableColumn<StockTransferOutDtl, String> clUnit;

	@FXML
	private TableColumn<StockTransferOutDtl, Number> clRate;

	@FXML
	private TableColumn<StockTransferOutDtl, Number> clAmount;

	@FXML
	private TableView<StockTransferOutHdr> tblStockHdr;

	@FXML
	private TableColumn<StockTransferOutHdr, String> clVoucherDate;

	@FXML
	private TableColumn<StockTransferOutHdr, String> clVoucherNo;

	@FXML
	private TableColumn<StockTransferOutHdr, String> clToBranch;

	@FXML
	private TableColumn<StockTransferOutHdr, String> clIntent;
	@FXML
	private TableColumn<StockTransferOutHdr, String> clStatus;

	@FXML
	void actionPrint(ActionEvent event) {

		String strfDate = SystemSetting.UtilDateToString(stockTransferOutHdr.getVoucherDate(), "yyyy-MM-dd");
//		try {
//
//			JasperPdfReportService.StockTransferInvoiceReport(stockTransferOutHdr.getVoucherNumber(), strfDate);
//		} catch (JRException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		if (SystemSetting.STOCKTRANSFER_FORMAT.equalsIgnoreCase("POS")) {
			JasperPdfReportService.stockTransferOutPrintPOSformat(stockTransferOutHdr.getId());
		} else {

			try {

				JasperPdfReportService.StockTransferInvoiceReport(stockTransferOutHdr.getVoucherNumber(), strfDate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		tblStockHdr.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					stockTransferOutHdr = new StockTransferOutHdr();
					stockTransferOutHdr.setId(newSelection.getId());

					ResponseEntity<List<StockTransferOutDtl>> getStockDtl = RestCaller
							.getStockTransferOutDtlByVoucherNo(newSelection.getVoucherNumber(),
									newSelection.getFromBranch());

					stockTransferOutHdr.setVoucherNumber(newSelection.getVoucherNumber());
					stockTransferOutHdr.setVoucherDate(newSelection.getVoucherDate());
					stockTransferOutDtlList = FXCollections.observableArrayList(getStockDtl.getBody());
					fillDtlTable();
				}
			}
		});
	}

	@FXML
	void actionPrintSummary(ActionEvent event) {
		LocalDate dpDate1 = dpFromDate.getValue();
		java.util.Date uDate = SystemSetting.localToUtilDate(dpDate1);
		String strfDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		LocalDate dpDate2 = dpToDate.getValue();
		java.util.Date utDate = SystemSetting.localToUtilDate(dpDate2);
		String strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");

		try {
			JasperPdfReportService.StockTransferOutSummaryReport(strfDate, strtDate);
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	@FXML
	void actionShow(ActionEvent event) {
		
		tblStockDtl.getItems().clear();
		tblStockHdr.getItems().clear();
		LocalDate dpDate1 = dpFromDate.getValue();
		java.util.Date uDate = SystemSetting.localToUtilDate(dpDate1);
		String strfDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		LocalDate dpDate2 = dpToDate.getValue();
		java.util.Date utDate = SystemSetting.localToUtilDate(dpDate2);
		String strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
		ResponseEntity<List<StockTransferOutHdr>> getStockOutHdr = RestCaller.getStockTransferBetweenDate(strfDate,
				strtDate);
		stockTransferOutHdrList = FXCollections.observableArrayList(getStockOutHdr.getBody());
		fillTable();

	}

	private void fillTable() {
		tblStockHdr.setItems(stockTransferOutHdrList);
		/*
		 * for(StockTransferOutHdr stk:stockTransferOutHdrList) { String sdate =
		 * stk.getVoucherDate().toString(); System.out.println("Date STRING"+sdate);
		 * ResponseEntity<LmsQueueMst> getLms =
		 * RestCaller.getlmsQueueByDateAndVoucherNo(stk.getVoucherNumber(),
		 * "forwardStockTranOut",sdate); if(null != getLms.getBody()) {
		 * if(getLms.getBody().getPostedToServer().equalsIgnoreCase("YES")) { if(null !=
		 * stk.getStatus()) { if(stk.getStatus().equalsIgnoreCase("DONE")) {
		 * stk.setStatus("DONE"); } else { stk.setStatus("SERVER"); } } else {
		 * stk.setStatus("SERVER"); }
		 * 
		 * } else { stk.setStatus("PENDING"); } } else { stk.setStatus("SERVER"); }
		 * 
		 * 
		 * if(null != stk.getToBranch()) { BranchMst branchMst =
		 * RestCaller.getBranchDtls(stk.getToBranch()); if(null != branchMst) {
		 * stk.setToBranchName(branchMst.getBranchName()); } } }
		 */
		clToBranch.setCellValueFactory(cellData -> cellData.getValue().getToBranchProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
		clIntent.setCellValueFactory(cellData -> cellData.getValue().getintentNumberProperty());

		clStatus.setCellValueFactory(cellData -> cellData.getValue().getstatusProperty());
	}

	private void fillDtlTable() {
		tblStockDtl.setItems(stockTransferOutDtlList);
		for (StockTransferOutDtl stk : stockTransferOutDtlList) {
//			ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(stk.getItemMst().getId());
			stk.setItemName(stk.getItemId().getItemName());
			ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(stk.getUnitId());
			stk.setUnitName(getUnit.getBody().getUnitName());

			BigDecimal amount = new BigDecimal(stk.getAmount());
			amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			stk.setAmount(amount.doubleValue());
		}
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

	}

	@FXML
	void resendStock(ActionEvent event) {

		String msg = RestCaller.resendStockTransfer(stockTransferOutHdr.getId());
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

	}

}
