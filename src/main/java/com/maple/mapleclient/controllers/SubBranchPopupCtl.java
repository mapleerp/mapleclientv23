
package com.maple.mapleclient.controllers;

import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;

import com.maple.mapleclient.entity.ItemPopUp;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.SubBranchSaleEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class SubBranchPopupCtl {
	String taskid;
	String processInstanceId;
	
	

	SubBranchSaleEvent customerEvent;

	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<BranchMst> custList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();

	@FXML
	private TextField txtname;

	@FXML
	private Button btnSubmit;

	@FXML
	private TableView<BranchMst> tablePopupView;

	@FXML
	private TableColumn<BranchMst, String> custName;

	@FXML
	private TableColumn<BranchMst, String> custAddress;

	@FXML
	private Button btnCancel;

	@FXML
	private void initialize() {
		LoadCustomerBySearch();

		txtname.textProperty().bindBidirectional(SearchString);

		customerEvent = new SubBranchSaleEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		tablePopupView.setItems(custList);

		btnCancel.setCache(true);

		custName.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
		custAddress.setCellValueFactory(cellData -> cellData.getValue().getBranchAddressProperty());

		tablePopupView.setItems(custList);
		

		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getBranchName() && newSelection.getBranchName().length() > 0) {
					customerEvent.setCustomerAddress(newSelection.getBranchAddress1());
					customerEvent.setCustomerContact(newSelection.getBranchTelNo());
					customerEvent.setCustomerGst(newSelection.getBranchGst());
					customerEvent.setCustomerMail(newSelection.getBranchEmail());
					customerEvent.setCustomerName(newSelection.getBranchName());
					customerEvent.setCustId(newSelection.getId());
					eventBus.post(customerEvent);
				}
			}
		});

		/*
		 * Below code will do the searching based on typed string
		 */

//		SearchString.addListener(new ChangeListener() {
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
//				LoadCustomerBySearch((String) newValue);
//			}
//		});

	}

	@FXML
	void onCancel(ActionEvent event) {
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
	}

	@FXML
	void submit(ActionEvent event) {
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();

	}

	@FXML
	void OnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}  else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		} else {
			txtname.requestFocus();
		}
	}

	@FXML
	void OnKeyPressTxt(KeyEvent event) {
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
	}

	private void LoadCustomerBySearch() {
		
		ResponseEntity<List<BranchMst>> subBranchMsts = RestCaller.getBranchMst();
		custList = FXCollections.observableArrayList(subBranchMsts.getBody());
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}
}

