package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.KitchenCategoryDtl;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class KitchenCategoryDtlCtl {
	String taskid;
	String processInstanceId;
	KitchenCategoryDtl kitchenCategoryDtl = null;
	private ObservableList<KitchenCategoryDtl> KitchenCategoryDtlList = FXCollections.observableArrayList();

    @FXML
    private ComboBox<String> cmbCategory;

    @FXML
    private TableView<KitchenCategoryDtl> tblCategory;

    @FXML
    private TableColumn<KitchenCategoryDtl,String> clCategory;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;
    @FXML
	private void initialize() {
    	
    	tblCategory.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					kitchenCategoryDtl = new KitchenCategoryDtl();
					kitchenCategoryDtl.setId(newSelection.getId());
				}
			}
		});
    	ArrayList cat = new ArrayList();

		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
			
				cmbCategory.getItems().add((String)categoryName);
			
			}

		}

    }
    @FXML
    void actionDelete(ActionEvent event) {
    	if(null == kitchenCategoryDtl)
    	{
    		return;
    	}
    	if(null == kitchenCategoryDtl.getId())
    	{
    		return;
    	}
    	RestCaller.deleteKitchenCategoryDtl(kitchenCategoryDtl.getId());

    	notifyMessage(3,"Deleted");
    	showAll();
    }

    @FXML
    void actionSave(ActionEvent event) {
    	kitchenCategoryDtl = new KitchenCategoryDtl();
    	kitchenCategoryDtl.setBranchCode(SystemSetting.getSystemBranch());
    	ResponseEntity<CategoryMst> getCategory = RestCaller.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem());
    	kitchenCategoryDtl.setCategoryId(getCategory.getBody().getId());
    	ResponseEntity<KitchenCategoryDtl> respentity = RestCaller.saveKitchenCategoryDtl(kitchenCategoryDtl);
    	kitchenCategoryDtl = respentity.getBody();
    	KitchenCategoryDtlList.add(kitchenCategoryDtl);
    	fillTable();
    }
    private void fillTable()
    {
    	for(KitchenCategoryDtl kitCategory:KitchenCategoryDtlList )
    	{
    		ResponseEntity<CategoryMst> getCategory = RestCaller.getCategoryById(kitCategory.getCategoryId());
    		kitCategory.setCategoryName(getCategory.getBody().getCategoryName());
    	}
    	tblCategory.setItems(KitchenCategoryDtlList);
    	clCategory.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());

    }

    @FXML
    void actionShowAll(ActionEvent event) {
    	showAll();
    }
    private void showAll()
    {
    	ResponseEntity<List<KitchenCategoryDtl>> getAllKitchenCategoryDtl = RestCaller.getAllKitchenCategoryDtl();
    	KitchenCategoryDtlList = FXCollections.observableArrayList(getAllKitchenCategoryDtl.getBody());
    	fillTable();
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }

}
