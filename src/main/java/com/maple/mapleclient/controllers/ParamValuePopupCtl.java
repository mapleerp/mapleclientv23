package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ParamMst;
import com.maple.mapleclient.events.ParamValueEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ParamValuePopupCtl {
	
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<ParamMst> paramList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	ParamValueEvent paramValueEvent;
	  @FXML
	    private TextField txtname;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TableView<ParamMst> tblPopupView;

	    @FXML
	    private TableColumn<ParamMst, String> paramName;

	    @FXML
	    private Button btnCancel;

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
				
				eventBus.post(paramValueEvent);
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB) {

			} else {
				txtname.requestFocus();

			}
	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
	    		tblPopupView.requestFocus();
	    		tblPopupView.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
				eventBus.post(paramValueEvent);

			}
	    }

	    @FXML
	    void onCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
	    }

	    @FXML
	    void submit(ActionEvent event) {
			eventBus.post(paramValueEvent);

	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
	    }
	    @FXML
		private void initialize() {
			txtname.textProperty().bindBidirectional(SearchString);

			paramValueEvent= new ParamValueEvent();
			eventBus.register(this);

 
			tblPopupView.setItems(paramList);
			paramName.setCellValueFactory(cellData -> cellData.getValue().getParamNameProperty());


 

			LoadParamBySearch("");
			

			tblPopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getParamName() && newSelection.getParamName().length() > 0) {
						paramValueEvent.setParamName(newSelection.getParamName());
						paramValueEvent.setParamId(newSelection.getId());
						eventBus.post(paramValueEvent);

					}
				}
			});

			 

			SearchString.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

					LoadParamBySearch((String) newValue);
				}
			});

		}
//--------------------------------------------------------------------------
	
		private void LoadParamBySearch(String searchData) {

			paramList.clear();
			ArrayList paramArray = new ArrayList();
			paramArray = RestCaller.SearchParamByName(searchData);
			Iterator itr = paramArray.iterator();
			while (itr.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				Object paramName = lm.get("paramName");
				Object id = lm.get("id");
				if (null != id) {
					ParamMst param = new ParamMst();
					param.setParamName((String) paramName);
					param.setId((String) id);
					paramList.add(param);
				}

			}
			
			

			return;

		}
		
}
