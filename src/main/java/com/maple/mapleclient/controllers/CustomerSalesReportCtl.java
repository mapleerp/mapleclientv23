package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.AccountHeadsPopupEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.CustomerNewPopUpEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class CustomerSalesReportCtl {
	
	String taskid;
	String processInstanceId;
	POSThermalPrintABS printingSupport ;

	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<DailySalesReportDtl> dailySalesList = FXCollections.observableArrayList();
	private ObservableList<VoucherReprintDtl> dailySalesLists = FXCollections.observableArrayList();


	String customerId = null;
	Double totalamount = 0.0;
	String customer = null;
	String voucher = null;
	String vDate = null;
	@FXML
	private DatePicker dpFromDate;

	@FXML
	private TableView<DailySalesReportDtl> tbReport;

	@FXML
	private TableColumn<DailySalesReportDtl, String> clVoucherNo;

	@FXML
	private TableColumn<DailySalesReportDtl, LocalDate> clDate;

	@FXML
	private TableColumn<DailySalesReportDtl, Number> clamt;

	@FXML
	private TextField txtGrandTotal;

	@FXML
	private Button btnShow;

	@FXML
	private TableView<VoucherReprintDtl> tbreports;

	@FXML
	private TableColumn<VoucherReprintDtl, String> clItemName;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> clQty;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> clRate;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> clTaxRate;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> clCessRate;

	@FXML
	private TableColumn<VoucherReprintDtl, String> clUnit;

	@FXML
	private TableColumn<VoucherReprintDtl, Number> clAmount;

	@FXML
	private Button ok;

	@FXML
	private Button btnPrint;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private Button btnPrintReport;

	@FXML
	private TextField txtCustomer;

	@FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		eventBus.register(this);
		
		tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
    		if (newSelection != null) {
    			System.out.println("getSelectionModel");
    			if (null != newSelection.getVoucherNumber()) {
    			
    				String voucherNumber=newSelection.getVoucherNumber();
    				
    				customer = newSelection.getCustomerName();
    				voucher = newSelection.getVoucherNumber();
    				java.sql.Date uDate = newSelection.getvoucherDate();
    	    		 vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
    		
    		    
    				 String branchName=SystemSetting.getSystemBranch();
    				ResponseEntity<List<VoucherReprintDtl>> dailysaless = RestCaller.getVoucherReprintDtl(newSelection.getVoucherNumber(),branchName);
    		    	dailySalesLists = FXCollections.observableArrayList(dailysaless.getBody());
    		    	tbreports.setItems(dailySalesLists);
    		    	
    		    	FillTable();
    			}
    		}
    	});
	}

	private void FillTable() {
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clRate.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
    	clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
    	clCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
    	clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	}

	@FXML
	void CustomerPopup(MouseEvent event) {

		loadCustomerPopup();
	}

	@FXML
	void CustomerPopupOnKeyPress(KeyEvent event) {

	}

	@FXML
	void PrintReport(ActionEvent event) {
		
		if(null == customerId || customerId.isEmpty())
		{
			notifyMessage(5, "Please select  customer", false);
    		return;
		}
		
		if(null == dpFromDate.getValue())
		{
			notifyMessage(5, "Please select  from Date", false);
    		return;
		}
		
		if(null == dpToDate.getValue())
		{
			notifyMessage(5, "Please select  to Date", false);
    		return;
		}
		
			java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			
			java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
			String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
			
		  try {
				JasperPdfReportService.CustomerSalesReportBetweenDate(SystemSetting.systemBranch,strDate, strDate1,customerId);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


	}

	@FXML
	void onClickOk(ActionEvent event) {

	}

	private void loadCustomerPopup() {
		try {

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CusstomerNewPopUp.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			dpFromDate.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupCustomerlistner(CustomerNewPopUpEvent customerNewPopUpEvent) {

		Stage stage = (Stage) btnPrint.getScene().getWindow();
		if (stage.isShowing()) {

			txtCustomer.setText(customerNewPopUpEvent.getCustomerName());
			customerId = customerNewPopUpEvent.getCustomerId();

		}

	}

	@FXML
	void invoicePrint(ActionEvent event) {
		
		if(customer.equalsIgnoreCase("POS") || customer.equalsIgnoreCase("KOT"))
    	{
    		SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucher,vDate) ;
    		if(null!=salesTransHdr) {
    		try {
				printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId()) ;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	}
    	else
    	{
    		   
	    		try {
	    			NewJasperPdfReportService.TaxInvoiceReport(voucher, vDate);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    	}

	}

	@FXML
	void show(ActionEvent event) {
		
		if(null == customerId || customerId.isEmpty())
		{
			notifyMessage(5, "Please select  customer", false);
    		return;
		}
		
		if(null == dpFromDate.getValue())
		{
			notifyMessage(5, "Please select  from Date", false);
    		return;
		}
		
		if(null == dpToDate.getValue())
		{
			notifyMessage(5, "Please select  to Date", false);
    		return;
		}
		
			totalamount = 0.0;
	    	java.util.Date fromDate = Date.valueOf(dpFromDate.getValue());
			String strDate = SystemSetting.UtilDateToString(fromDate, "yyy-MM-dd");
			
			
			java.util.Date toDate = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
			
			
	    	ResponseEntity<List<DailySalesReportDtl>> dailysales = RestCaller.getCustomerDailySaleReport(SystemSetting.systemBranch, strDate,endDate,customerId);
	    	dailySalesList = FXCollections.observableArrayList(dailysales.getBody());
	    	tbReport.setItems(dailySalesList);
	    	
	    	
	    	clamt.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
	    	clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
	    	clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
	    	
	    	
	    	for(DailySalesReportDtl dailySaleReportDtl:dailySalesList)
	    	{
	    		totalamount = totalamount + dailySaleReportDtl.getAmount();
	    	}
	    	BigDecimal settoamount = new BigDecimal(totalamount);
			settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
	    	txtGrandTotal.setText(settoamount.toString());

	}
	
	  public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
