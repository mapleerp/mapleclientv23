package com.maple.mapleclient.controllers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.HostServices;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class OrgTreeCtl {
	String taskid;
	String processInstanceId;
	HashMap menuWindowQueue;

	String selectedText = null;

    @FXML
    private AnchorPane OrgTree;

    @FXML
    private TreeView<?> treeview;

    @FXML
    void orgOnClick(MouseEvent event) {

    }
    
	@FXML
	void initialize() {
		TreeItem rootItem = new TreeItem("ORGANIZATION");
		//TreeItem masterItem = new TreeItem("MASTERS");
		
		menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
		
		
		
		ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("ORGANIZATION");
		
		List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
		
		MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
		
		String parentId =aMenuConfigMst.getId();
		
		ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
		List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

		
		for(MenuConfigMst aMenuConfig : menuConfigMstList)
		{
			
			//masterItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
			
			if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
				rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
				}
			
			
		}
		
		//rootItem.getChildren().add(masterItem);

		treeview.setRoot(rootItem);
		

		
		EventHandler<MouseEvent> mouseEventHandle = (MouseEvent event) -> {
		    handleMouseClicked(event);
		};

		treeview.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseEventHandle); 
		
		
		
		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text 12345 : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();
				
				loadMenu(  selectedText);
				
				
				
				/*ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller.MenuConfigMstByMenuName(selectedText);
				
				List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();
		
				MenuConfigMst aMenuConfig = menuConfigMstList.get(0);
				
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != selectedText)
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(selectedText, dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}
				  */
				
			/*	  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
			
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  */
				  
				
			/*	if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.kotManagement);

				}
				
				*/
				
				

			}

		});
				 
 
				 
			
		//	if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
				// MapleclientApplication.mainFrameController.sideVBOX.getChildren().add(actionBtnPos); 
			//}
			
			
			
			
			
			/*
			
			Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
			if(null==dynamicWindow) {
				if(null != aMenuConfig.getMenuFxml())
				{
					  try {
						dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
						 menuWindowQueue.put(aMenuConfig.getMenuName(), dynamicWindow);
					  
					  } catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
 
			}
			  
			  try {
		
					// Clear screen before loading the Page.
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();
		
					MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
		
					MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
					MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
					MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
					MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
				 	dynamicWindow.requestFocus();
					 
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			  */
			
			  
		}
	
	private void handleMouseClicked(MouseEvent event) {
	    Node node = event.getPickResult().getIntersectedNode();
	    // Accept clicks only on node cells, and not on empty spaces of the TreeView
	    if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
	        String name = (String) ((TreeItem)treeview.getSelectionModel().getSelectedItem()).getValue();
	        
	        
	        
	        loadMenu(  name);
	        
	        System.out.println("Node click: " + name);
	        
	        
	    }
	}
	
	
	private void loadMenu(String menuName) {
		ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller.MenuConfigMstByMenuName(menuName);
		
		List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();

		MenuConfigMst aMenuConfig = menuConfigMstList.get(0);
		
		
		Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
		if(null==dynamicWindow) {
			if(null != menuName)
			{
				  try {
					dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
					 menuWindowQueue.put(menuName, dynamicWindow);
				  
				  } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
		  
		  try {
	
				// Clear screen before loading the Page.
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();
	
				MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
	
				MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
			 	dynamicWindow.requestFocus();
	
			} catch (Exception e) {
				e.printStackTrace();
			}
		  
		  
	}
	
	private void showStockTransferOutDtl() {

		String reportLocation = SystemSetting.reportPath + "/stocktransferdtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfStockTransferOutDtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showUnitMst() {

		String reportLocation = SystemSetting.reportPath + "/unitmst.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfUnitMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showSuppliers() {

		String reportLocation = SystemSetting.reportPath + "/suppliers.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfSuppliers());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showStockTransferOutHdr() {

		String reportLocation = SystemSetting.reportPath + "/stocktransferhdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfStockTransferOutHdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showReorderMst() {

		String reportLocation = SystemSetting.reportPath + "/reordermsts.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfReorderMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showStockReport() {

		String reportLocation = SystemSetting.reportPath + "/dailystockreport.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfStockReport());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showPaymentDtl() {

		String reportLocation = SystemSetting.reportPath + "/paymentdtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfPaymentDtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showPurchaseHdr() {

		String reportLocation = SystemSetting.reportPath + "/purchasehdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfPurchaseHdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showPaymentHdr() {

		String reportLocation = SystemSetting.reportPath + "/paymenthdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfPaymentHdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showProductionDtl() {

		String reportLocation = SystemSetting.reportPath + "/productiondtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfProductionDtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showProductionMst() {

		String reportLocation = SystemSetting.reportPath + "/productionmst.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfProductionMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showIntentDtl() {

		String reportLocation = SystemSetting.reportPath + "/intentdtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfIntentdtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showKitDefinitionDtl() {

		String reportLocation = SystemSetting.reportPath + "/kitdefinitiondtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfKitDefinitionDtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showKitDefinitionMst() {

		String reportLocation = SystemSetting.reportPath + "/kitdefinitionmst.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfKitDefinitionMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showIntentHdr() {

		String reportLocation = SystemSetting.reportPath + "/intenthdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfIntenthdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void notifyMessageFailure(int duration, String msg) {

		Image img = new Image("failed.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	  @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }

}
