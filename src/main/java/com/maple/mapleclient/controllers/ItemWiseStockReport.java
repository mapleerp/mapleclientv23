package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ItemWiseStockReport {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<StockReport> SalesReportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnShowReport;

    @FXML
    private TableView<StockReport> tblStockReport;
    
  @FXML
  private TableColumn<StockReport, String> clumItemName;

  @FXML
  private TableColumn<StockReport, Number> clumnQty;

  @FXML
  private TableColumn<StockReport, String> clumnUnit;
    
    @FXML
	private void initialize() {
		eventBus.register(this);
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
	}
    private void showItem()
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    @Subscribe
  	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

  		System.out.println("-------------popupItemlistner-------------");

  	
  		Stage stage = (Stage) btnShowReport.getScene().getWindow();
  		if (stage.isShowing()) {
  	
  			txtItemName.setText(itemPopupEvent.getItemName());
  			
  		}

  	}
    
  
		
    @FXML
    void ShowReport(ActionEvent event) {

    	SalesReportList.clear();

    	if(null==dpFromDate.getValue()) {
    		if(txtItemName.getText().isEmpty()) {
    			notifyMessage(5, "please select date", false);
    	return;
    		}
		/*
		 * try { JasperPdfReportService.ItemStockReport(vFDate
		 * ,itemsaved.getBody().getId(),SystemSetting.systemBranch); } catch
		 * (JRException e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */
    	}else {
    		if(txtItemName.getText().isEmpty()) {
    			notifyMessage(5, "please select date", false);
    			return;
    		}
    		java.util.Date fromDate =Date.valueOf(dpFromDate.getValue());
        	ResponseEntity<ItemMst> itemsaved = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    		
    		String vFDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
    	//	String vToDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
    		
    		ResponseEntity<List<StockReport>> itemStockReport=RestCaller.ItemStockReport(vFDate,itemsaved.getBody().getId(),SystemSetting.systemBranch);
    		
    		SalesReportList = FXCollections.observableArrayList(itemStockReport.getBody());
    		System.out.println(SalesReportList.size()+"sales report list size issssssssssssssssssssssssssssssssss");
    		tblStockReport.setItems(SalesReportList);
    		
    		FillTable();
    	}
	    	
		

    }
    @FXML
    void loadPopUp(MouseEvent event) {
    	showItem();
    }
	
    

	private void FillTable()
	{
		System.out.print("inside fill table");
		
		
		clumItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clumnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clumnUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());

		
    	
	}
	

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
	}
