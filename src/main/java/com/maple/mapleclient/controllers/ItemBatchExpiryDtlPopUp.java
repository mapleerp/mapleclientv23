package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.events.ItemBatchEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ItemBatchExpiryDtlPopUp {
	String taskid;
	String processInstanceId;
	private ObservableList<ItemBatchExpiryDtl> itemBatchList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();
	
	ItemBatchExpiryDtl itemBatchExpiryDtl=null;
	StringProperty SearchString = new SimpleStringProperty();

    @FXML
    private TableView<ItemBatchExpiryDtl> tbItemBatchExpDtl;

    @FXML
    private TableColumn<ItemBatchExpiryDtl,String> clItemName;

    @FXML
    private TableColumn<ItemBatchExpiryDtl, String> clBatch;

    @FXML
    private TextField txtName;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
    @FXML
   	private void initialize() {

    	
    	txtName.textProperty().bindBidirectional(SearchString);
    	eventBus.register(this);
    	tbItemBatchExpDtl.setItems(itemBatchList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());

		LoadItemPopupBySearch(""); 
		tbItemBatchExpDtl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getItemName()) {
					itemBatchExpiryDtl = new ItemBatchExpiryDtl();
					itemBatchExpiryDtl.setItemName(newSelection.getItemName());
					itemBatchExpiryDtl.setBatch(newSelection.getBatch());

					eventBus.post(itemBatchExpiryDtl);
				}
			}
		});
		
		
		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadItemPopupBySearch((String) newValue);
			}
		});
    
     
    }
    @FXML
    void actionCancel(ActionEvent event) {


		Stage stage = (Stage) btnCancel.getScene().getWindow();
		eventBus.post(itemBatchExpiryDtl);
		stage.close();

	
    }

    private void LoadItemPopupBySearch(String data) {
    	itemBatchList.clear();
    	List<Object> items = new ArrayList();
    	items = RestCaller.searchItemBatchExpDtlPopup(data);
    	String itemName ="";
    	String batch ="";
    	Iterator itr = items.iterator();
    	while (itr.hasNext()) {
    		List element = (List) itr.next();
    		itemName = (String) element.get(0);
    		batch = (String) element.get(1);
    		if(null != itemName) 
    		{
    			ItemBatchExpiryDtl itemPopup = new ItemBatchExpiryDtl();
    			itemPopup.setItemName(itemName);
    			itemPopup.setBatch(batch);
    			itemBatchList.add(itemPopup);
    		}
    	}
		
	}
	@FXML
    void actionOk(ActionEvent event) {

		Stage stage = (Stage) btnCancel.getScene().getWindow();
		eventBus.post(itemBatchExpiryDtl);
		stage.close();
	
    }
	
	 @FXML
	    void OnKeyPressTxt(KeyEvent event) {


			if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tbItemBatchExpDtl.requestFocus();
				tbItemBatchExpDtl.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {

				itemBatchExpiryDtl.setItemName(null);
				eventBus.post(itemBatchExpiryDtl);
				Stage stage = (Stage) btnCancel.getScene().getWindow();
				stage.close();

			}
			if (event.getCode() == KeyCode.BACK_SPACE && txtName.getText().length() == 0) {
				itemBatchExpiryDtl.setItemName(null);
				eventBus.post(itemBatchExpiryDtl);
				Stage stage = (Stage) btnCancel.getScene().getWindow();
				stage.close();

			}
		

	    }
	@FXML
    void keyPressCancel(KeyEvent event) {

    }
	 @FXML
	    void onKeyPress(KeyEvent event) {



			if (event.getCode() == KeyCode.ENTER) {
				eventBus.post(itemBatchExpiryDtl);
				Stage stage = (Stage) btnCancel.getScene().getWindow();
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP
					|| event.getCode() == KeyCode.KP_UP) {

			} else {
				txtName.requestFocus();
			}

	    }
    @FXML
    void keypressOk(KeyEvent event) {
    	
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
