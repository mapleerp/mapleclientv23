package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.ExportTrialBalanceToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ExpenseReport;
import com.maple.report.entity.IncomeReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ProfitAndLossConfigReport {
	
	
	
	String taskid;
	String processInstanceId;
	String accountId = null;
	private ObservableList<IncomeReport> incomeReportList = FXCollections.observableArrayList();
	private ObservableList<ExpenseReport> expenseReportList = FXCollections.observableArrayList();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");
	ExportTrialBalanceToExcel exportTrialBalanceToExcell = new ExportTrialBalanceToExcel();


    @FXML
    private TableView<ExpenseReport> tblExpense;

    @FXML
    private TableColumn<ExpenseReport, String> clAccount;

    @FXML
    private TableColumn<ExpenseReport, Number> clExpenseAmount;

    @FXML
    private TextField txtexpenseTotal;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnExportToExcel;

    @FXML
    private TextField txtincomeTotal;

    @FXML
    private TableView<IncomeReport> tblIncome;

    @FXML
    private TableColumn<IncomeReport, String> clIncomeAccount;

    @FXML
    private TableColumn<IncomeReport, Number> clIncomeAmount;
    
    
    
    
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	tblExpense.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getAccountName()) {
					
					ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadByName(newSelection.getAccountName());
					AccountHeads accountHeads = accountHeadsResp.getBody();
					
					if(null != accountHeads)
					{
						accountId = accountHeads.getId();
					}
				}
			}
		});
    	tblIncome.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getAccountName()) {
					
					ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadByName(newSelection.getAccountName());
					AccountHeads accountHeads = accountHeadsResp.getBody();
					
					if(null != accountHeads)
					{
						accountId = accountHeads.getId();
					}
				}
			}
		});
    }

    @FXML
    void GenerateReport(ActionEvent event) {
    	if(null == dpFromDate.getValue())
		{
			notifyMessage(2, "pleas select date", false);
			dpFromDate.requestFocus();
			return;
		}
		
		Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		incomeReportList.clear();
		tblIncome.getItems().clear();
		expenseReportList.clear();
		tblExpense.getItems().clear();
    	
    	ArrayList items = new ArrayList();
		items = RestCaller.getTrialBalance(sdate);

		String account = "";
		Double expenseAmount = 0.0;
		Double incomeAmount = 0.0;
		Double amount = 0.0;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			account = (String) element.get(0);
			expenseAmount = (Double) element.get(2);
			incomeAmount = (Double) element.get(1);
			amount = (Double) element.get(3);
			if (null != account) {
				
				IncomeReport incomeReport = new IncomeReport();
				ExpenseReport expenseReport = new ExpenseReport();
				expenseReport.setAccountName(account);
				incomeReport.setAccountName(account);
				if(amount >=0)
				{
					expenseReport.setAmount(amount);
					incomeReport.setAmount(0.0);
				} else {
					incomeReport.setAmount(amount*-1);
					expenseReport.setAmount(0.0);

				}
				incomeReportList.add(incomeReport);
				expenseReportList.add(expenseReport);
			}


    }
		
		IncomefillTable();
		ExpensefillTable();
    }
    private void IncomefillTable() {
		tblIncome.setItems(incomeReportList);
		
		BigDecimal incomeTotal = new BigDecimal("0.0");
		
		for(IncomeReport account : incomeReportList)
		{
			incomeTotal = incomeTotal.add(new BigDecimal( account.getAmount()));
			
		}
		
		clIncomeAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		clIncomeAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		
		clIncomeAmount.setCellFactory(tc-> new TableCell<IncomeReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		
		
		
		incomeTotal=incomeTotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		
		
		txtincomeTotal.setText(incomeTotal.toPlainString());
		
	}
    
    private void ExpensefillTable() {
		tblExpense.setItems(expenseReportList);
		
		
		BigDecimal  expenseTotal = new BigDecimal("0.0");
		for(ExpenseReport account : expenseReportList)
		{
			
			expenseTotal = expenseTotal.add( new BigDecimal(account.getAmount()));
		}
		
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		clExpenseAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		clExpenseAmount.setCellFactory(tc-> new TableCell<ExpenseReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		
		expenseTotal=expenseTotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		
		
		
		
		txtexpenseTotal.setText(expenseTotal.toPlainString());
	}
    @FXML
    void exportToExcel(ActionEvent event) {
    	
    }

    @FXML
    void incometableOnEnter(KeyEvent event) {
    	if(event.getCode()==KeyCode.ENTER)
    	{
    		showStateMentOfAccount(accountId);
    	}
    }

    @FXML
    void tableOnEnter(KeyEvent event) {
    	if(event.getCode()==KeyCode.ENTER)
    	{
    		showStateMentOfAccount(accountId);
    	}
    }
    private void showStateMentOfAccount(String account) {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/AccountBalanceByAccountHeads.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			AccountBalanceByAccountIdCtl accountCtl =fxmlLoader.getController();
			accountCtl.setAccountId(account);
   			Stage stage = new Stage();
   			stage.setScene(new Scene(root1));
   			stage.initModality(Modality.APPLICATION_MODAL);
   			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	} @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}
}
