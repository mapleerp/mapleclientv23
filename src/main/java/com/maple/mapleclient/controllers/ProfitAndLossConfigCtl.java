package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ProfitAndLossExpense;
import com.maple.mapleclient.entity.ProfitAndLossIncome;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.events.AccountEventCr;
import com.maple.mapleclient.events.AccountEventDr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ProfitAndLossConfigCtl {
	
	
	private ObservableList<ProfitAndLossExpense> profitAndLossExpenseTable = FXCollections.observableArrayList();
	private ObservableList<ProfitAndLossIncome> profitAndLossIncomeTable = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();
	AccountHeads accountHeads;
	ProfitAndLossExpense profitAndLossExpense=null;
	ProfitAndLossIncome profitAndLossIncome=null;
	String accountId ;
	@FXML
	private TableView<ProfitAndLossIncome> tblincomedetails;

	@FXML
	private TableColumn<ProfitAndLossIncome, String> clserialIncome;

	@FXML
	private TableColumn<ProfitAndLossIncome, String> claccountnameincome;

	@FXML
	private TextField txtaccountNameIncome;

	@FXML
	private Button btnsaveIncome;

	@FXML
	private Button btndeleteIncome;

	@FXML
	private Button btnclearIncome;

	@FXML
	private Button btnshowallIncome;

	@FXML
	private TextField txtserialNoIncome;

	@FXML
	private TableView<ProfitAndLossExpense> tblexpensedetails;

	@FXML
	private TableColumn<ProfitAndLossExpense, String> clserialnoexpense;

	@FXML
	private TableColumn<ProfitAndLossExpense, String> claccountnameexpense;

	@FXML
	private TextField txtaccountNameExpense;

	@FXML
	private Button btnsaveExpense;

	@FXML
	private Button btndeleteExpense;

	@FXML
	private Button btnclearExpense;

	@FXML
	private Button btnshowallExpense;

	@FXML
	private TextField txtserialNoExpense;

	@FXML
	void AddExpense(ActionEvent event) {
		
		if (txtserialNoExpense.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select serial number", false);
			txtserialNoExpense.requestFocus();
			return;
		}

		if (txtaccountNameExpense.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select account name", false);
			txtaccountNameExpense.requestFocus();
			return;
		}
		ProfitAndLossExpense profitAndLossExpense = new ProfitAndLossExpense();
		profitAndLossExpense.setSerialNo(txtserialNoExpense.getText());
		
		ResponseEntity<AccountHeads> accountResp = RestCaller.getAccountHeadByName(txtaccountNameExpense.getText());
		AccountHeads accountHeads = accountResp.getBody();

		if (null == accountHeads) {
			notifyMessage(5, " Invalid Account name...!", false);
			return;
		}
		
		profitAndLossExpense.setAccountId(accountHeads.getId());
		ResponseEntity<ProfitAndLossExpense> expenserespEntity = RestCaller.createProfitAndLossExpense(profitAndLossExpense);

		

		notifyMessage(5, "Saved ", true);
		txtserialNoExpense.clear();
		txtaccountNameExpense.clear();

		showAllExpense();
		return;

	}

	private void showAllExpense() {
		ResponseEntity<List<ProfitAndLossExpense>> expenserespentity = RestCaller.getProfitAndLossExpense();
		profitAndLossExpenseTable = FXCollections.observableArrayList(expenserespentity.getBody());

		System.out.print(profitAndLossExpenseTable.size() + "observable list size issssssssssssssssssssss");
		fillExpenseTable();

	}

	private void fillExpenseTable() {
		tblexpensedetails.setItems(profitAndLossExpenseTable);
		for(ProfitAndLossExpense profit : profitAndLossExpenseTable)
		{
			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountById(profit.getAccountId());
			if(null != accountHeads.getBody())
			{
				profit.setAccountName(accountHeads.getBody().getAccountName());
			}
		}
		clserialnoexpense.setCellValueFactory(cellData -> cellData.getValue().getSerialNoProperty());
		claccountnameexpense.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

	}
	@FXML
	void DeleteExpense(ActionEvent event) {
		if (null != profitAndLossExpense) {

			if (null != profitAndLossExpense.getId()) {
				RestCaller.deleteProfitAndLossExpense(profitAndLossExpense.getId());

				showAllExpense();
				notifyMessage(5, "Deleted......", false);
				return;
			}
		}
	}
	@FXML
	void ShowAllExpense(ActionEvent event) {
		showAllExpense();
	}
	@FXML
	void clearDetailsExpense(ActionEvent event) {
		txtaccountNameExpense.clear();
		txtserialNoExpense.clear();
		tblexpensedetails.getItems().clear();
	}
	
	
	@FXML
	void expensepopUpKeyOnPressed(KeyEvent event) {
		loadExpensePopup();
	}

	@FXML
	void expensepopUpMouseOnClicked(MouseEvent event) {
		loadExpensePopup();
	}
	
	private void loadExpensePopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			 AccountPopup popupctl = loader.getController();
			popupctl.resultEventName="CreditAccount";
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			txtaccountNameExpense.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Subscribe
	public void popuplistner(AccountEventCr accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnsaveExpense.getScene().getWindow();
		if (stage.isShowing()) {

			txtaccountNameExpense.setText(accountEvent.getAccountName());
		}
	}
	
	
//---------------------------------------------Expense End-----------------------------------------------------------------//	
	
	@FXML
	private void initialize() {

		eventBus.register(this);

		tblexpensedetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

			        profitAndLossExpense = new ProfitAndLossExpense();
					profitAndLossExpense.setId(newSelection.getId());
				}
			}
		});

		tblincomedetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					 profitAndLossIncome = new ProfitAndLossIncome();
					profitAndLossIncome.setId(newSelection.getId());
				}
			}
		});
	}
	
	
	@FXML
	void AddIncome(ActionEvent event) {
		
		if (txtserialNoIncome.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select serial number", false);
			txtserialNoIncome.requestFocus();
			return;
		}

		if (txtaccountNameIncome.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select account name", false);
			txtaccountNameIncome.requestFocus();
			return;
		}
		ProfitAndLossIncome profitAndLossIncome = new ProfitAndLossIncome();
		profitAndLossIncome.setSerialNo(txtserialNoIncome.getText());
		
		
		
		ResponseEntity<AccountHeads> accountResp = RestCaller.getAccountHeadByName(txtaccountNameExpense.getText());
		AccountHeads accountHeads = accountResp.getBody();

		if (null == accountHeads) {
			notifyMessage(5, " Invalid Account name...!", false);
			return;
		}
		
		profitAndLossIncome.setAccountId(accountHeads.getId());
		
		ResponseEntity<ProfitAndLossIncome> incomerespEntity = RestCaller
				.createProfitAndLossIncome(profitAndLossIncome);
		
		
		notifyMessage(5, "Saved ", true);
		txtserialNoExpense.clear();
		txtaccountNameExpense.clear();

		showAllIncome();
		return;

	}

	private void showAllIncome() {
		ResponseEntity<List<ProfitAndLossIncome>> respentity = RestCaller.getProfitAndLossIncome();
		profitAndLossIncomeTable = FXCollections.observableArrayList(respentity.getBody());

		System.out.print(profitAndLossIncomeTable.size() + "observable list size issssssssssssssssssssss");
		fillIncomeTable();

	}

	private void fillIncomeTable() {
		tblincomedetails.setItems(profitAndLossIncomeTable);
		for(ProfitAndLossIncome income : profitAndLossIncomeTable)
		{
			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountById(income.getAccountId());
			if(null != accountHeads.getBody())
			{
				income.setAccountName(accountHeads.getBody().getAccountName());
			}
		}
		clserialIncome.setCellValueFactory(cellData -> cellData.getValue().getSerialNoProperty());
		claccountnameincome.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

	}

	

	@FXML
	void DeleteIncome(ActionEvent event) {
		if (null != profitAndLossIncome) {

			if (null != profitAndLossIncome.getId()) {
				RestCaller.deleteProfitAndLossIncome(profitAndLossIncome.getId());

				showAllIncome();
				notifyMessage(5, "Deleted......", false);
				return;
			}
		}
	}

	

	@FXML
	void ShowAllIncome(ActionEvent event) {
		showAllIncome();
	}

	

	@FXML
	void clearDetailsIncome(ActionEvent event) {
		txtserialNoIncome.clear();
		txtaccountNameIncome.clear();
		tblincomedetails.getItems().clear();
	}

	

	@FXML
	void incomepopUpKeyOnPressed(KeyEvent event) {
		loadPopupExpense();
	}

	@FXML
	void incomepopUpMouseOnClicked(MouseEvent event) {
		loadPopupExpense();
	}

	

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	
	private void loadPopupExpense() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			 AccountPopup popupctl = loader.getController();
			popupctl.resultEventName="DebitAccount";
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			txtaccountNameIncome.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Subscribe
	public void popuplistner(AccountEventDr accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnsaveIncome.getScene().getWindow();
		if (stage.isShowing()) {

			txtaccountNameIncome.setText(accountEvent.getAccountName());
		}
	}



}

