package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PharmacyPurchaseReturnDetailReport;
import com.maple.report.entity.PoliceReport;
import com.maple.report.entity.PurchaseReturnDetailAndSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyPurchaseReturnDetailReportCtl {

	
	//  (0_0)       SharonN  

	private ObservableList<PurchaseReturnDetailAndSummaryReport> purchaseReturnDetailReportList = FXCollections.observableArrayList();

	 @FXML
	    private DatePicker dptoDate;

	    @FXML
	    private Button btnPrintReport;

	    @FXML
	    private Button btnGenerate;

	    @FXML
	    private DatePicker dpfromDate;

	    @FXML
	    private TableView<PurchaseReturnDetailAndSummaryReport> tblpurchaseReturnDetailreport;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clreturnvoucherDate;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clpurchaseVoucherNumber;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clsupplierName;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clreturnvoucherno;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clitemName;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clgroupname;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clbatch;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, Number> clqty;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, Number> clpurchaseRate;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> cluserName;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, Number> clamount;
	    @FXML
		private void initialize() {
			dpfromDate = SystemSetting.datePickerFormat(dpfromDate, "dd/MMM/yyyy");
			dptoDate = SystemSetting.datePickerFormat(dptoDate, "dd/MMM/yyyy");
			tblpurchaseReturnDetailreport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					System.out.println("getSelectionModel");
					if (null != newSelection.getGroupName()) {

						
						fillTable();


					}
				}
			});

		}
    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null == dpfromDate.getValue()) {

			notifyMessage(3, "please select From date", false);
			dpfromDate.requestFocus();
			return;
		}
		if (null == dptoDate.getValue()) {

			notifyMessage(3, "please select Todate", false);
			dptoDate.requestFocus();
			return;
		}

		java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dptoDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> purchaseReturndtlReportResp = RestCaller.getPurchaseReturnDetailAndSummary(fromdate, todate);
//		ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> purchaseReturndtlReportResp = RestCaller.getPurchaseReturnDetailReport(fromdate, todate);

		purchaseReturnDetailReportList = FXCollections.observableArrayList(purchaseReturndtlReportResp.getBody());

		fillTable();

	}
	 public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
	private void fillTable() {
		tblpurchaseReturnDetailreport.setItems(purchaseReturnDetailReportList);

		clreturnvoucherDate.setCellValueFactory(cellData -> cellData.getValue().getReturnVoucherDateProperty());
		clpurchaseVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getPurchaseReturnVoucherProperty());
		//clpurchaseVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getPurchaseReturnVoucherProperty());
		clsupplierName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
		clreturnvoucherno.setCellValueFactory(cellData -> cellData.getValue().getReturnVoucherNumProperty());
		clitemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clgroupname.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
		clbatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clqty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clpurchaseRate.setCellValueFactory(cellData -> cellData.getValue().getPurchaseRateProperty());
		cluserName.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
		clamount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	
	}

	

    @FXML
    void PrintReport(ActionEvent event) {

		java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dptoDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		try {
			JasperPdfReportService.PurchaseReturnDetailReport(fromdate, todate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    }



