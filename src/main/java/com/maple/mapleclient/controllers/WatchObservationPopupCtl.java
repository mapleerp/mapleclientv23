package com.maple.mapleclient.controllers;

import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.WatchComplaintMst;
import com.maple.mapleclient.entity.WatchObservationMst;
import com.maple.mapleclient.events.WatchCompliantEvent;
import com.maple.mapleclient.events.WatchObservationEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class WatchObservationPopupCtl {
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
    private ObservableList<WatchObservationMst> observationList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

	WatchObservationEvent watchObservationEvent = null;

    @FXML
    private TextField txtProductName;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;

    @FXML
    private TableView<WatchObservationMst> tbProducts;

    @FXML
    private TableColumn<WatchObservationMst, String> clProductName;
    
    @FXML
	private void initialize() {
    	LoadAccountPopupBySearch("");
    	txtProductName.textProperty().bindBidirectional(SearchString);

    	eventBus.register(this);
    	watchObservationEvent = new WatchObservationEvent();
    	tbProducts.setItems(observationList);
    	clProductName.setCellValueFactory(cellData -> cellData.getValue().getObservationProperty());
    	tbProducts.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			    if (newSelection != null) {
			    	if(null!=newSelection.getId()&& newSelection.getObservation().length()>0) {
			    		watchObservationEvent.setObservationId(newSelection.getId());
			    		watchObservationEvent.setObservation(newSelection.getObservation());
			    		eventBus.post(watchObservationEvent);
			    	}
			    }
			});
    	
    	 
   	 SearchString.addListener(new ChangeListener(){
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				 					
					LoadAccountPopupBySearch((String)newValue);
					
				}
	        });
    }

    @FXML
    void actionCancel(ActionEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
    void actionOk(ActionEvent event) {
    	if(null != watchObservationEvent.getObservationId())
    	{
    	eventBus.post(watchObservationEvent); 
    	}
    	else
    	{
    		watchObservationEvent.setObservation(txtProductName.getText());
    		eventBus.post(watchObservationEvent);
    	}
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();

    }

    @FXML
    void onKeyPresstxt(KeyEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	if (event.getCode() == KeyCode.ENTER)
    
    	{
    	if(null != watchObservationEvent.getObservationId())
    	{
    	eventBus.post(watchObservationEvent); 
    	stage.close();
    	}
    	else
    	{
    		watchObservationEvent.setObservation(txtProductName.getText());
    		eventBus.post(watchObservationEvent);
    		stage.close();
    	}
    	}
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tbProducts.requestFocus();
    		tbProducts.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			stage.close();
		}
    }

    @FXML
    void tbOnEnter(KeyEvent event) {
if (event.getCode() == KeyCode.ENTER) {
			
    		Stage stage = (Stage) btnOk.getScene().getWindow();
      
        	if(null != watchObservationEvent.getObservationId())
        	{
        	eventBus.post(watchObservationEvent); 
        	stage.close();
        	}
        	else
        	{
        		watchObservationEvent.setObservation(txtProductName.getText());
        		eventBus.post(watchObservationEvent);
        		stage.close();
        	}
		}
    }
    
    private void LoadAccountPopupBySearch(String searchData) {

  		/*
  		 * This method populate the instance variable popUpItemList , which the source
  		 * of data for the Table.
  		 */
  		ArrayList strapArray = new ArrayList();
  		/*
  		 * Clear the Table before calling the Rest
  		 */

  		observationList.clear();

  		strapArray = RestCaller.SearchWatchObservationByName(searchData);
  		String id;
  		String compliant;
  		Iterator itr = strapArray.iterator();
  		System.out.println("accaccaccacc22");
  		while (itr.hasNext()) {
  			
  			List element = (List) itr.next();
  			compliant = (String) element.get(1);
  			  id = (String) element.get(0);
  			 if (null != id) {
  				 WatchObservationMst watchObservationMst = new WatchObservationMst();
  				watchObservationMst.setId(id);
  				watchObservationMst.setObservation(compliant);
  				observationList.add(watchObservationMst);
  		}
      }
  		return;
  }
    
    @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
