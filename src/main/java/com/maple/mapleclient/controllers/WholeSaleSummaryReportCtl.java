package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.WholeSaleDetailReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class WholeSaleSummaryReportCtl {

	private ObservableList<WholeSaleDetailReport> wholeSaleDetailReportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnPrintButton;

    @FXML
    private TableView<WholeSaleDetailReport> tblWholeSaleSummary;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnInvoiceDate;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnVoucher;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnCustomer;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnCustomerPO;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnTin;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnBeforeGST;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnGST;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnTotal;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnSalesMan;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnUserName;

    @FXML
    void generateReport(ActionEvent event) {
    	
    	if (null == dpFromDate.getValue()) {

			notifyMessage(3, "please select start date", false);
			dpFromDate.requestFocus();
			return;
		}

		if (null == dpToDate.getValue()) {

			notifyMessage(3, "please select start date", false);
			dpToDate.requestFocus();

			return;
		}

		Date uDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String fromDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		Date eDate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String toDate = SystemSetting.UtilDateToString(eDate, "yyy-MM-dd");
		
		ResponseEntity<List<WholeSaleDetailReport>> wholeSaleDetailReportResp = RestCaller
				.getWholeSaleDetailReport(fromDate, toDate);

		wholeSaleDetailReportList = FXCollections.observableArrayList(wholeSaleDetailReportResp.getBody());

		fillReportTable();
		
		

    }

    private void fillReportTable() {
		
    	tblWholeSaleSummary.setItems(wholeSaleDetailReportList);

		clmnInvoiceDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
		clmnVoucher.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clmnCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clmnCustomerPO.setCellValueFactory(cellData -> cellData.getValue().getCustomerPOProperty());
		clmnTin.setCellValueFactory(cellData -> cellData.getValue().getTinNumberProperty());
		clmnBeforeGST.setCellValueFactory(cellData -> cellData.getValue().getBeforeGSTProperty());
		clmnGST.setCellValueFactory(cellData -> cellData.getValue().getGstProperty());
		clmnTotal.setCellValueFactory(cellData -> cellData.getValue().getTotalInvoiceAmountProperty());
		clmnSalesMan.setCellValueFactory(cellData -> cellData.getValue().getSalesManProperty());
		clmnUserName.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
		
		
	}
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		tblWholeSaleSummary.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {

					
					fillReportTable();


				}
			}
		});

	}

	@FXML
    void printButton(ActionEvent event) {
		
		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		


		try {
			JasperPdfReportService.WholeSaleSummaryReport(sfdate,stdate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
	
	
}
