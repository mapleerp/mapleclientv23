package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.PhysicalStockDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PhysicalStockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class PhysicalStockReportCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<PhysicalStockReport> physicalStockDtlListTable = FXCollections.observableArrayList();


    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnReport;

    @FXML
    private TableView<PhysicalStockReport> tblReport;

    @FXML
    private TableColumn<PhysicalStockReport, String> clDate;

    @FXML
    private TableColumn<PhysicalStockReport, String> clVoucherNumber;

    @FXML
    private TableColumn<PhysicalStockReport, String> clItemName;

    @FXML
    private TableColumn<PhysicalStockReport, Number> clQty;

    @FXML
    private TableColumn<PhysicalStockReport, String> clBatch;

    @FXML
    private TableColumn<PhysicalStockReport, Number> clAmount;

    @FXML
    private TableColumn<PhysicalStockReport, String> clManufactureDAte;

    @FXML
    private TableColumn<PhysicalStockReport, String> clExpiryDate;
    @FXML
	private void initialize() {

		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    }
    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(3, "Please Select From Date", false);
    		return;
    	}
    	
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(3, "Please Select To Date", false);
    		return;
    	}
    	
    	java.util.Date uFromDate = Date.valueOf(dpFromDate.getValue());
    	String strDate = SystemSetting.UtilDateToString(uFromDate, "yyyy-MM-dd");
    	
    	java.util.Date uToDate = Date.valueOf(dpToDate.getValue());
    	String toDate = SystemSetting.UtilDateToString(uToDate, "yyyy-MM-dd");

    	ResponseEntity<List<PhysicalStockReport>> physicalStockReport = RestCaller.getPhysicalStockReport(strDate,toDate);
    	physicalStockDtlListTable = FXCollections.observableArrayList(physicalStockReport.getBody());
    	
    	FillTable();

    }
    
    
	private void FillTable() {

		tblReport.setItems(physicalStockDtlListTable);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clManufactureDAte.setCellValueFactory(cellData -> cellData.getValue().getManufactureDateProperty());
		clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());


	}


	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }


}
