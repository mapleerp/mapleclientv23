package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTypeMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class SaleTypeCtl {

	String taskid;
	String processInstanceId;
	
	private ObservableList<SalesTypeMst> saleTypeTable = FXCollections.observableArrayList();

	SalesTypeMst salesTypeMst = null;
	@FXML
	private TextField txtSaleType;

	@FXML
	private TextField txtPrefix;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShow;

	@FXML
	private TableView<SalesTypeMst> tblSalesType;

	@FXML
	private TableColumn<SalesTypeMst, String> clSaleType;

	@FXML
	private TableColumn<SalesTypeMst, String> clPrefix;

	@FXML
	private void initialize() {
		tblSalesType.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					salesTypeMst = new SalesTypeMst();
					salesTypeMst.setId(newSelection.getId());
				}
			}
		});
	}

	@FXML
	void deleteItem(ActionEvent event) {

		if (null != salesTypeMst) {
			if (null != salesTypeMst.getId()) {
				RestCaller.deleteSalesType(salesTypeMst.getId());
				showAll();
				notifyMessage(5, "deleted!!!");
			}

		}
	}

	@FXML
	void saveItem(ActionEvent event) {
		if (txtSaleType.getText().isEmpty())

		{

			notifyMessage(5, "Type Sales Type");
			txtSaleType.requestFocus();
			return;
		}
		if (txtPrefix.getText().isEmpty())

		{
			notifyMessage(5, "Type Invocie Prefix");
			txtPrefix.requestFocus();
			return;
		}

		salesTypeMst = new SalesTypeMst();
		ResponseEntity<SalesTypeMst> getsalesType = RestCaller.getSaleTypeByname(txtSaleType.getText());
		if (null == getsalesType.getBody()) {
			salesTypeMst.setSalesPrefix(txtPrefix.getText());
			salesTypeMst.setSalesType(txtSaleType.getText());
			String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "VTYPE");
			salesTypeMst.setId(vNo);
			salesTypeMst.setBranchCode(SystemSetting.getUser().getBranchCode());
			ResponseEntity<SalesTypeMst> respentity = RestCaller.saveSalesType(salesTypeMst);
			salesTypeMst = respentity.getBody();
			saleTypeTable.add(salesTypeMst);
			fillTable();
			txtPrefix.clear();
			txtSaleType.clear();
			salesTypeMst = null;
			notifyMessage(5, "Saved!!!");
		} else {
			notifyMessage(5, "Item already Exist!!!");
			return;
		}

	}

	private void fillTable() {
		tblSalesType.setItems(saleTypeTable);
		clPrefix.setCellValueFactory(cellData -> cellData.getValue().getsalesPrefixProperty());
		clSaleType.setCellValueFactory(cellData -> cellData.getValue().getsalesTypeProperty());
	}

	@FXML
	void showAll(ActionEvent event) {

		showAll();
	}

	private void showAll() {
		ResponseEntity<List<SalesTypeMst>> salesTypeSaved = RestCaller.getSalesTypeMst();
		if (null != salesTypeSaved.getBody()) {
			saleTypeTable = FXCollections.observableArrayList(salesTypeSaved.getBody());
			fillTable();
		}
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


	private void PageReload(String hdrId) {

	}

}