package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.GroupMst;
import com.maple.mapleclient.entity.GroupPermissionMst;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.entity.ProcessPermissionMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class GrroupPremissionCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<GroupPermissionMst> groupPermissionList = FXCollections.observableArrayList();
	GroupPermissionMst groupPermissionMst = null;
	ProcessMst processMst = null;
	GroupMst groupMst = null;

	@FXML
	private Button btnsave;

	@FXML
	private Button btnShowAll;

	@FXML
	private Button btndelete;

	@FXML
	private ComboBox<String> cmbGroup;

	@FXML
	private ComboBox<String> cmbProcess;

	@FXML
	private Button btnSearch;

	@FXML
	private TableView<GroupPermissionMst> processtbl;

	@FXML
	private TableColumn<GroupPermissionMst, String> PC1;

	@FXML
	private TableColumn<GroupPermissionMst, String> PC2;

	@FXML
	private void initialize() {

		getGroup();
		getProcess();

		processtbl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					groupPermissionMst = new GroupPermissionMst();

					groupPermissionMst.setId(newSelection.getId());

				}
			}
		});

	}

	@FXML
	void SearchPermissions(ActionEvent event) {

		groupPermissionMst = new GroupPermissionMst();

		if (null != cmbGroup.getValue()) {

			groupMst = new GroupMst();
			ResponseEntity<GroupMst> respentityGroup = RestCaller.getGroupDtls(cmbGroup.getValue());
			groupMst = respentityGroup.getBody();
			groupPermissionMst.setGroupId(groupMst.getId());

		}
		if (null != cmbProcess.getValue()) {
			processMst = new ProcessMst();
			ResponseEntity<ProcessMst> respentity = RestCaller.getProcessDtls(cmbProcess.getValue());
			processMst = respentity.getBody();
			groupPermissionMst.setProcessId(processMst.getId());
		}
		if (null != groupPermissionMst) {
			if (null != groupPermissionMst.getGroupId()) {
    			SearchPermissionByGroupId();
			}
			if (null != groupPermissionMst.getProcessId()) {
    			SearchPermissionByProcessId();
			}
		}

	}

	@FXML
	void ShowAll(ActionEvent event) {
		ShowAllPermissions();
	}

	@FXML
	void delete(ActionEvent event) {

		if (null != groupPermissionMst) {
			if (null != groupPermissionMst.getId()) {
				RestCaller.groupPermissionDelete(groupPermissionMst.getId());
				ShowAllPermissions();
				notifyMessage(5, " Successfully Deleted...!!!");
			}
		}

	}

	@FXML
	void getGroupNames(MouseEvent event) {

		getGroup();

	}

	@FXML
	void getProcessName(MouseEvent event) {

		getProcess();

	}

	@FXML
	void save(ActionEvent event) {

		if (null == cmbGroup.getValue()) {
			notifyMessage(5, " Please select group...!!!");

		} else if (null == cmbProcess.getValue()) {

			notifyMessage(5, " Please select process...!!!");
		} else {
			groupPermissionMst = new GroupPermissionMst();
			processMst = new ProcessMst();
			ResponseEntity<ProcessMst> respentity = RestCaller.getProcessDtls(cmbProcess.getValue());
			processMst = respentity.getBody();
			
			groupPermissionMst.setProcessId(processMst.getId());

			groupMst = new GroupMst();
			ResponseEntity<GroupMst> respentityGroup = RestCaller.getGroupDtls(cmbGroup.getValue());
			groupMst = respentityGroup.getBody();
			groupPermissionMst.setGroupId(groupMst.getId());
			String vNo= RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"GP");
			groupMst.setId(vNo);
			processtbl.getItems().clear();
			ResponseEntity<GroupPermissionMst> respentityPermission = RestCaller
					.saveGroupPermissionMst(groupPermissionMst);
			groupPermissionMst = respentityPermission.getBody();
			System.out.println("===groupPermissionMst====" + groupPermissionMst);
			groupPermissionList.add(groupPermissionMst);
			FillTable();
			groupPermissionMst = null;

		}

	}

	private void getProcess() {

		cmbProcess.getItems().clear();
		ArrayList process = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		process = RestCaller.getAllProcess();
		Iterator itr = process.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object processName = lm.get("processName");
			Object id = lm.get("id");
			if (id != null) {

				cmbProcess.getItems().add((String) processName);

			}
		}

	}

	private void getGroup() {

		cmbGroup.getItems().clear();
		ArrayList process = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		process = RestCaller.getAllGroup();
		Iterator itr = process.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object groupName = lm.get("groupName");
			Object id = lm.get("id");
			if (id != null) {

				cmbGroup.getItems().add((String) groupName);

			}
		}

	}

	private void FillTable() {

		processtbl.setItems(groupPermissionList);
		for (GroupPermissionMst p : groupPermissionList) {
			if (null != p.getId()) {
				groupMst = new GroupMst();
				ResponseEntity<GroupMst> respentity = RestCaller.getGroupNameById(p.getGroupId());
				groupMst = respentity.getBody();
				System.out.println("=====groupMst====="+groupMst);
				if (null != groupMst) {
					System.out.println("===groupMst.getGroupName()====" + groupMst.getGroupName());
					p.setGroupName(groupMst.getGroupName());
					PC1.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
				}
			}
			if (null != p.getProcessId()) {
				ProcessMst processMst = new ProcessMst();
				ResponseEntity<ProcessMst> respentity = RestCaller.getProcessById(p.getProcessId());
				processMst = respentity.getBody();
				if (null != processMst) {
					System.out.println("===processMst.getProcessName()====" + processMst.getProcessName());
					System.out.println("===processMst====" + processMst);
					p.setProcessName(processMst.getProcessName());
					PC2.setCellValueFactory(cellData -> cellData.getValue().getProcessNameProperty());
				}
			}
		}
		clearField();

	}

	private void clearField() {
		cmbGroup.getSelectionModel().clearSelection();
		cmbProcess.getSelectionModel().clearSelection();

	}

	private void ShowAllPermissions() {

		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		permission = RestCaller.getGroupPermissionDtl();

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();

			Object groupId = lm.get("groupId");
			Object processId = lm.get("processId");
			System.out.println("=====lm=====" + lm);

			Object id = lm.get("id");
			if (id != null) {
				GroupPermissionMst pp = new GroupPermissionMst();
				pp.setProcessId((String) processId);
				pp.setGroupId((String) groupId);
				pp.setId((String) id);

				groupPermissionList.add(pp);

			}
			FillTable();
		}

	}
	
	private void SearchPermissionByProcessId() {
		System.out.println("=======Suuuuuuuuuu======SearchPermissionByProcessId=======");
		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		permission = RestCaller.getGroupPermissionDtlByProcessId(groupPermissionMst.getProcessId());

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();

			Object groupId = lm.get("groupId");
			Object processId = lm.get("processId");
			System.out.println("=====lm=====" + lm);

			Object id = lm.get("id");
			if (id != null) {
				GroupPermissionMst pp = new GroupPermissionMst();
				pp.setProcessId((String) processId);
				pp.setGroupId((String) groupId);
				pp.setId((String)id);

				groupPermissionList.add(pp);

			}
			FillTable();
		}

	}

	private void SearchPermissionByGroupId() {
		
		
		System.out.println("=======Suuuuuuuuuu======SearchPermissionByGroupId======="+groupPermissionMst.getGroupId());
		processtbl.getItems().clear();
		ArrayList permission = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		permission = RestCaller.getGroupPermissionDtlByGroupId(groupPermissionMst.getGroupId());

		Iterator itr = permission.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			System.out.println("=====lm=====" + lm);
			Object groupId = lm.get("groupId");
			Object processId = lm.get("processId");
			

			Object id = lm.get("id");
			if (id != null) {
				GroupPermissionMst pp = new GroupPermissionMst();
				pp.setProcessId((String) processId);
				pp.setGroupId((String) groupId);
				pp.setId((String)id);

				groupPermissionList.add(pp);

			}
			FillTable();
		}

	}
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
