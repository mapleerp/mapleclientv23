package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class CreditCardSalesReport {
	String taskid;
	String processInstanceId;
	private ObservableList<SalesTransHdr> onlineSalesList = FXCollections.observableArrayList();

    @FXML
    private TableView<SalesTransHdr> tbOnlineSales;

    @FXML
    private TableColumn<SalesTransHdr, String> clSalesType;

    @FXML
    private TableColumn<SalesTransHdr, Number> clAmount;

    @FXML
    private TextField txtTotal;
    
	@FXML
	private void initialize() {
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getpaidAmountProperty());
		clSalesType.setCellValueFactory(cellData -> cellData.getValue().getSalesModeProperty());
		
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


  private void PageReload() {
  	
}  
    

}
