package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocationMst;
import com.maple.mapleclient.entity.ServiceItemMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.LocationEvent;
import com.maple.mapleclient.events.ServiceItemEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ServiceItemPopupCtl {
	
	String taskid;
	String processInstanceId;
	
	private ObservableList<ItemMst> serviceItemList = FXCollections.observableArrayList();
	boolean initializedCalled = false;
	ServiceItemEvent serviceItemEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	StringProperty SearchString = new SimpleStringProperty();


    @FXML
    private TextField txtItemSearch;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<ItemMst> tblServiceItem;

    @FXML
    private TableColumn<ItemMst, String> clServiceItem;

    @FXML
    private TableColumn<ItemMst, Number> clMrp;

    @FXML
    private TableColumn<ItemMst, String> clCategory;

    @FXML
    private Button btnCancel;
    
    @FXML
	private void initialize() {
		if (initializedCalled)
			return;

		initializedCalled = true;

		serviceItemEvent = new ServiceItemEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		txtItemSearch.textProperty().bindBidirectional(SearchString);

		btnCancel.setCache(true);
		
		LoadItemPopupBySearch("");
		
		tblServiceItem.setItems(serviceItemList);
		
		clServiceItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getStandardPriceProperty());
		clCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());




		tblServiceItem.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					
					serviceItemEvent = new ServiceItemEvent();
					serviceItemEvent.setCategory(newSelection.getCategoryName());
					serviceItemEvent.setId(newSelection.getId());
					serviceItemEvent.setServiceItemName(newSelection.getItemName());
					serviceItemEvent.setMrp(newSelection.getStandardPrice());
					serviceItemEvent.setUnitId(newSelection.getUnitId());

					eventBus.post(serviceItemEvent);
					
				}
			}

		});
		
		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadItemPopupBySearch((String) newValue);
			}
		});

	}

    private void LoadItemPopupBySearch(String searchData) {
    	ArrayList items = new ArrayList();

    	serviceItemList.clear();

		items = RestCaller.SearchServiceItem(searchData);
		
		String itemName = null;
		String id = null;
		Double mrp = null;
		String unit = null;
		String category = null;
		
		Iterator itr = items.iterator();

		while (itr.hasNext()) {
			
			List element = (List) itr.next();
			itemName = (String) element.get(0);
			id = (String) element.get(1);
			mrp = (Double) element.get(2);
			unit = (String) element.get(4);
			category = (String) element.get(3);
			
			if (null != id) {
				ItemMst serviceItemMst = new ItemMst();
				
				serviceItemMst.setId(id);
				serviceItemMst.setItemName(itemName);
				serviceItemMst.setCategoryId(category);
				ResponseEntity<CategoryMst> categoryResp = RestCaller.getCategoryById(category);
				CategoryMst categoryMst = categoryResp.getBody();
				if(null != categoryMst)
				{
					serviceItemMst.setCategoryName(categoryMst.getCategoryName());

				}
				
				ResponseEntity<UnitMst> unitResp = RestCaller.getunitMst(unit);
				UnitMst UnitMst = unitResp.getBody();
				
				if(null != UnitMst)
				{
					serviceItemMst.setUnitId(UnitMst.getUnitName());

				}
				serviceItemMst.setStandardPrice(mrp);
				serviceItemList.add(serviceItemMst);
			}
			
		}
	}

	@FXML
    void OnKeyPress(KeyEvent event) {
    	
    	
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}  else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		}

    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblServiceItem.requestFocus();
			tblServiceItem.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


	private void PageReload(String hdrId) {

	}

}
