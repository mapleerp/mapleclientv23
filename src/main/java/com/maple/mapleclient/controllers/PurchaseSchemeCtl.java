package com.maple.mapleclient.controllers;

import java.util.List;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;


import org.apache.poi.ss.formula.ptg.TblPtg;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PurchaseSchemeMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PurchaseSchemeCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<PurchaseSchemeMst> purchaseSchemeMstList = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();

	PurchaseSchemeMst purchaseSchemeMst = null;
    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtOfferDescription;

    @FXML
    private TextField txtMinimumQty;

    @FXML
    private TextField txtSupplier;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;
    @FXML
    private TableView<PurchaseSchemeMst> tbPurchaseScheme;
    @FXML
    private TableColumn<PurchaseSchemeMst,String> clFromDate;

    @FXML
    private TableColumn<PurchaseSchemeMst,String> clToDate;

    @FXML
    private TableColumn<PurchaseSchemeMst,String> clSupplier;

    @FXML
    private TableColumn<PurchaseSchemeMst, String> clItemName;

    @FXML
    private TableColumn<PurchaseSchemeMst,Number> clMinQty;

    @FXML
    private TableColumn<PurchaseSchemeMst,String> clOfferDescription;
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		eventBus.register(this);
		tbPurchaseScheme.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					purchaseSchemeMst = new PurchaseSchemeMst();
					purchaseSchemeMst.setId(newSelection.getId());
				}
			}
		});
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == purchaseSchemeMst)
    	{
    		return;
    	}
    	if(null == purchaseSchemeMst.getId())
    	{
    		return;
    	}
    	RestCaller.deletepurchaseSchemeMst(purchaseSchemeMst.getId());
    	purchaseSchemeMst= null;
    	showAll();
    }
    @FXML
    void itemPopUp(KeyEvent event) {
    	if(event.getCode()== KeyCode.ENTER)
    	{
    		loadItemPopUp();
    	}
    }
    @FXML
    void minimumQtyOnEnter(KeyEvent event) {

    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtOfferDescription.requestFocus();
    	}
    }
    @FXML
    void offerDescriptionOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		btnSave.requestFocus();
    	}
    }
    @FXML
    void SaveKeyPress(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		addItem();
    	}
    }

    @FXML
    void loadSupplier(KeyEvent event) {
    	if(event.getCode()== KeyCode.ENTER)
    	{
    	loadSupplierPopUp();
    	}
    }
    private void loadItemPopUp() {

		System.out.println("-------------ShowItemPopup-------------");

		try {
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtMinimumQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	
    }
    @Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {
			
			txtItemName.setText(itemPopupEvent.getItemName());
		
		}
    }
    private void loadSupplierPopUp()
    {


		System.out.println("-------------showPopup-------------");
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/supplierPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtItemName.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	
    }
    @Subscribe
	public void popuplistner(SupplierPopupEvent supplierEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtSupplier.setText(supplierEvent.getSupplierName());
	
				}
			});
		}
	}
    @FXML
    void actionSave(ActionEvent event) 
    {
    	addItem();
    }
    private void addItem()
    {

    	purchaseSchemeMst = new PurchaseSchemeMst();
    	if(null != dpFromDate.getValue())
    	{
    	purchaseSchemeMst.setFromDate(SystemSetting.localToUtilDate(dpFromDate.getValue()));
    	}
    	if(null != dpToDate.getValue())
    	{
    	purchaseSchemeMst.setToDate(SystemSetting.localToUtilDate(dpToDate.getValue()));
    	}
    	purchaseSchemeMst.setBranchCode(SystemSetting.systemBranch);
    	purchaseSchemeMst.setMinimumQty(Double.parseDouble(txtMinimumQty.getText()));
    	purchaseSchemeMst.setOfferDescription(txtOfferDescription.getText());
    	ResponseEntity<ItemMst> itemMst = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	purchaseSchemeMst.setItemId(itemMst.getBody().getId());
    	ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(txtSupplier.getText());
    	purchaseSchemeMst.setSupplierId(accountHeads.getBody().getId());
    	ResponseEntity<PurchaseSchemeMst> respentity = RestCaller.savePurchaseSchemeMst(purchaseSchemeMst);
    	purchaseSchemeMst = respentity.getBody();
    	purchaseSchemeMstList.add(purchaseSchemeMst);
    	clearFields();
    	fillTable();
    	purchaseSchemeMst = null;
    	dpFromDate.requestFocus();
    
    }
    @FXML
    void fromDateOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		dpToDate.requestFocus();
    	}
    }
    @FXML
    void toDateOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtSupplier.requestFocus();
    	}
    }
    private void fillTable()
    {
    	for(PurchaseSchemeMst puchaseScheme:purchaseSchemeMstList)
    	{
    		ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadsById(puchaseScheme.getSupplierId());
    		puchaseScheme.setSupplierName(getAccountHeads.getBody().getAccountName());
    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(puchaseScheme.getItemId());
    		puchaseScheme.setItemName(getItem.getBody().getItemName());
    				
    	}
    	tbPurchaseScheme.setItems(purchaseSchemeMstList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clFromDate.setCellValueFactory(cellData -> cellData.getValue().getfromDateProperty());
		clToDate.setCellValueFactory(cellData -> cellData.getValue().gettoDateProperty());
		clMinQty.setCellValueFactory(cellData -> cellData.getValue().getminimumQtyProperty());
		clOfferDescription.setCellValueFactory(cellData -> cellData.getValue().getofferDescriptionProperty());
		clSupplier.setCellValueFactory(cellData -> cellData.getValue().getsupplierNameProperty());
		
    }
    private void clearFields()
    {
    	txtItemName.clear();
    	txtMinimumQty.clear();
    	txtOfferDescription.clear();
    	txtSupplier.clear();
    	dpFromDate.setValue(null);
    	dpToDate.setValue(null);
    	
    }

    @FXML
    void actionShowAll(ActionEvent event) {

    	showAll();
    }

    private void showAll()
    {
    	ResponseEntity<List<PurchaseSchemeMst>> getAll = RestCaller.getPurchaseSchemeMst();
    	purchaseSchemeMstList = FXCollections.observableArrayList(getAll.getBody());
    	fillTable();
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
