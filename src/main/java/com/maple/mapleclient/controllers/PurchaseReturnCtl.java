 
package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.apache.commons.math3.analysis.function.Rint;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemBatchDtl;
import com.maple.mapleclient.entity.ItemMst;

import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.PurchaseHdrReport;
import com.maple.mapleclient.entity.PurchaseReturnDtl;
import com.maple.mapleclient.entity.PurchaseReturnHdr;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class PurchaseReturnCtl {

	private ObservableList<PurchaseHdrReport> purchaseHdrReportList = FXCollections.observableArrayList();

	private ObservableList<PurchaseDtl> purchaseDtlReportList = FXCollections.observableArrayList();

	private ObservableList<PurchaseReturnDtl> purchaseReturnDtlList = FXCollections.observableArrayList();
	PurchaseReturnHdr purchaseReturnHdr;
	PurchaseHdr purchaseHdr = null;
	ItemBatchDtl itembatchDtl=null;
	
	Double OldReturnedQty=0.0;

	Double allowedReturnQty;
	@FXML
	
	private DatePicker dpDate;

	@FXML
	private Button btnFetchInvoice;

	@FXML
	private TableView<PurchaseHdrReport> tblInvoice;

	@FXML
	private TableColumn<PurchaseHdrReport, String> clVoucherNo;

	@FXML
	private TableColumn<PurchaseHdrReport, String> clSupplierName;

	@FXML
	private TableView<PurchaseDtl> itemDetailTable;

	@FXML
	private TableColumn<PurchaseDtl, String> clItemName;

	@FXML
	private TableColumn<PurchaseDtl, String> clBarCode;

	@FXML
	private TableColumn<PurchaseDtl, Number> clQty;

	@FXML
	private TableColumn<PurchaseDtl, Number> clTaxRate;

	@FXML
	private TableColumn<PurchaseDtl, Number> clMrp;

	@FXML
	private TableColumn<PurchaseDtl, Number> clAmount;

	@FXML
	private TableColumn<PurchaseDtl, String> columnBatch;

	@FXML
	private TableColumn<PurchaseDtl, String> columnUnitName;

	@FXML
	private TableColumn<PurchaseDtl, LocalDate> columnExpiryDate;

	@FXML
	private TableColumn<PurchaseDtl, Number> clReturned;

	@FXML
	private Button btnFinalSave;

	@FXML
	private TableView<PurchaseReturnDtl> tblPurchaseReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, String> clItemReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, String> clBarCodeReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, Number> clQtyReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, Number> clTaxRateReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, Number> clMrpReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, Number> clAmountReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, String> clBatchReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, String> clUnitReturn;

	@FXML
	private TableColumn<PurchaseReturnDtl, String> clExpiryReturn;
	
	@FXML
    private TableColumn<PurchaseReturnDtl, Number> clPurchaseRate;
	

	 
	 @FXML
	 private ComboBox<String> cmbStoreFrom;

	@FXML
	private TextField txtSalesReturmBillAmount;
	String voucherNumber;
	Double qty;
	String purchaseDtlId;

	@FXML
	private Button btnDelete;

	@FXML
	private Button BtnClear;

	@FXML
	private TextField gstNo;

	@FXML
	private TextField txtCustomername;

	@FXML
	private TextField txtAdress;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtQty;
	
	   @FXML
	    private Label lblRetunQty;
	
	

	@FXML
	private Button btnAdditem;
	PurchaseReturnDtl purchaseReturnDtl;

	@FXML
	void CustomerPopUp(MouseEvent event) {

	}

	@FXML
	void purchaseReturn(ActionEvent event) {
		
		String returnQty = txtQty.getText();
		double rtnQty = Double.parseDouble(returnQty);
		
	      if(qty<rtnQty)
	      {
	      
	    	  notifyMessage(3, "Return Qty is Greater Than Actual QTY", false);
				return;
	      }
	      
	      if(allowedReturnQty<rtnQty)
	      {
	    	  notifyMessage(3, "Return Qty is Greater Than Billing QTY", false);
				return;
	      }
       		
		
		

		if (null != purchaseHdr) {
			if (null != purchaseDtlId) {
				ResponseEntity<PurchaseDtl> purchaseDtlRep = RestCaller.getPurchaseDtlById(purchaseDtlId);
				PurchaseDtl purchaseDtl = purchaseDtlRep.getBody();

				if (null == purchaseDtl) {
					return;
				}

				if (null == purchaseReturnHdr) {
					
					purchaseReturnHdr = new PurchaseReturnHdr();
					purchaseReturnHdr.setPurchaseHdr(purchaseHdr);

					if (null != purchaseHdr.getPurchaseType() || !purchaseHdr.getPurchaseType().trim().isEmpty()) {
						purchaseReturnHdr.setPurchaseType(purchaseHdr.getPurchaseType());
					}

					
					if (null != purchaseHdr.getSupplierId()) {
						purchaseReturnHdr.setSupplierId(purchaseHdr.getSupplierId());
					}
					if (null != purchaseHdr.getPurchaseType()) {
						purchaseReturnHdr.setFinalSavedStatus("NO");
					}
					if (null != purchaseHdr.getMachineId()) {
						purchaseReturnHdr.setMachineId(purchaseHdr.getMachineId());
					}
					if (null != purchaseHdr.getBranchCode()) {
						purchaseReturnHdr.setBranchCode(purchaseHdr.getBranchCode());
					}
					if (null != purchaseHdr.getDeletedStatus()) {
						purchaseReturnHdr.setDeletedStatus(purchaseHdr.getDeletedStatus());
					}
					if (null != purchaseHdr.getNarration()) {
						purchaseReturnHdr.setNarration(purchaseHdr.getNarration());
					}
					if (null != purchaseHdr.getPurchaseType()) {
						purchaseReturnHdr.setPurchaseType(purchaseHdr.getPurchaseType());
					}
					if (null != purchaseHdr.getpONum()) {
						purchaseReturnHdr.setpONum(purchaseHdr.getpONum());
					}
					if (null != purchaseHdr.getPoDate()) {
						purchaseReturnHdr.setPoDate(purchaseHdr.getPoDate());
					}
					if (null != purchaseHdr.getSupplierInvNo()) {
						purchaseReturnHdr.setSupplierInvNo(purchaseHdr.getSupplierInvNo());
					}
					/*
					 * if (null != purchaseHdr.getVoucherNumber()) {
					 * purchaseReturnHdr.setVoucherNumber(purchaseHdr.getVoucherNumber()); }
					 */
					if (null != purchaseHdr.getInvoiceTotal()) {
						purchaseReturnHdr.setInvoiceTotal(purchaseHdr.getInvoiceTotal());
					}
					if (null != purchaseHdr.getCurrency()) {
						purchaseReturnHdr.setCurrency(purchaseHdr.getCurrency());
					}
					if (null != purchaseHdr.getVoucherNumber()) {
						purchaseReturnHdr.setReturnVoucherNumber(purchaseHdr.getVoucherNumber());
					}
					

				//	purchaseReturnHdr.setVoucherDate(SystemSetting.applicationDate);

					purchaseReturnHdr.setUserId(SystemSetting.getUserId());
					purchaseReturnHdr.setStore(cmbStoreFrom.getSelectionModel().getSelectedItem());
					
					
					purchaseReturnHdr.setReturnVoucherDate(SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyyy-MM-dd"));
					/*
					 * if (null != purchaseHdr.getInvoiceTotal()) {
					 * purchaseReturnHdr.setInvoiceTotal(purchaseHdr.getInvoiceTotal()); }
					 */

					ResponseEntity<PurchaseReturnHdr> purchaseHdrRep = RestCaller
							.savePurchaseReturnHdr(purchaseReturnHdr);
					purchaseReturnHdr = purchaseHdrRep.getBody();


					
				}
				
				

				if (null != purchaseReturnHdr) {

					PurchaseReturnDtl purchaseReturnDtl = new PurchaseReturnDtl();

					ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(purchaseDtl.getItemId());
					ItemMst itemMst = itemMstResp.getBody();
					if (null == itemMst) {
						return;
					}
					purchaseReturnDtl.setItemId(itemMst.getId());
					purchaseReturnDtl.setBarcode(itemMst.getBarCode());
					purchaseReturnDtl.setUnitId(purchaseDtl.getUnitId());

					purchaseReturnDtl.setItemName(purchaseDtl.getItemName());
					purchaseReturnDtl.setPurchseRate(purchaseDtl.getPurchseRate());
					purchaseReturnDtl.setBarcode(purchaseDtl.getBarcode());
					purchaseReturnDtl.setQty(Double.parseDouble(txtQty.getText()));
					purchaseReturnDtl.setAmount(purchaseDtl.getAmount());
					purchaseReturnDtl.setBatch(purchaseDtl.getBatch());
					purchaseReturnDtl.setExpiryDate(purchaseDtl.getexpiryDate());
					purchaseReturnDtl.setAmount(Double.parseDouble(txtQty.getText())*purchaseDtl.getPurchseRate());
					purchaseReturnDtl.setBinNo(purchaseDtl.getBinNo());
					purchaseReturnDtl.setCessAmt(purchaseDtl.getCessAmt());
					purchaseReturnDtl.setFreeQty(purchaseDtl.getFreeQty());
					purchaseReturnDtl.setMrp(purchaseDtl.getMrp());
					purchaseReturnDtl.setNetCost(purchaseDtl.getNetCost());
					purchaseReturnDtl.setPreviousMRP(purchaseDtl.getPreviousMRP());
					purchaseReturnDtl.setPurchaseRate(purchaseDtl.getPurchseRate());
					purchaseReturnDtl.setTaxAmt(purchaseDtl.getTaxAmt());
					purchaseReturnDtl.setTaxRate(purchaseDtl.getTaxRate());
					purchaseReturnDtl.setPurchaseDtlId(purchaseDtl.getId());
					
					
					System.out.print(purchaseReturnHdr+"purchase return hdr isssssssssssssssss");
		
					purchaseReturnDtl.setPurchaseReturnHdr(purchaseReturnHdr);

					ResponseEntity<PurchaseReturnDtl> purchaseReturnDtlRep = RestCaller
							.savePurchaseReturnDtl(purchaseReturnDtl,purchaseReturnHdr.getId());
					purchaseReturnDtl = purchaseReturnDtlRep.getBody();

					ResponseEntity<List<PurchaseReturnDtl>> purchaseReturnDtlListResp = RestCaller
							.getPurchaseReturnDtlByHdrId(purchaseReturnHdr.getId());

					purchaseReturnDtlList = FXCollections.observableArrayList(purchaseReturnDtlListResp.getBody());
					Double totalPurchaseReturnAmount=0.0;
					for(int i=0;i<purchaseReturnDtlList.size();i++) {
						
					
					totalPurchaseReturnAmount+=	purchaseReturnDtlList.get(0).getPurchaseRate()*purchaseReturnDtlList.get(0).getQty();
					
					}
					txtSalesReturmBillAmount.setText(String.valueOf(totalPurchaseReturnAmount));
					FillTablePurchaseReturn();
					
					System.out.print(purchaseReturnDtlList.size()+"purchase return sizeeeeeeeeeeee");
					
				}
				


				purchaseDtl = null;

			}
			
			purchaseDtlId = null;
			purchaseReturnDtl = null;
		}
	}

	public void clear() {
		itemDetailTable.getItems().clear();
		tblPurchaseReturn.getItems().clear();
		PurchaseHdr purchaseHdr = new PurchaseHdr() ;
		PurchaseReturnDtl purchaseReturnDtl=new PurchaseReturnDtl();
		PurchaseHdrReport purchaseHdrReport=new PurchaseHdrReport();
		purchaseHdrReportList.clear();
		purchaseReturnDtlList.clear();
		
		txtSalesReturmBillAmount.clear();
		
		txtCustomername.clear();
		txtAdress.clear();
		gstNo.clear();
		txtCashtopay.clear();
		txtQty.clear();
		txtSalesReturmBillAmount.clear();
		txtCashtopay.clear();
		
		
		
	}
	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		tblInvoice.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {
					voucherNumber = newSelection.getVoucherNumber();

					 fetchPurchasehdr(voucherNumber);
					 
					 

//					if (null == dpDate.getValue()) {
//
//						notifyMessage(3, "", false);
//						return;

//package com.maple.mapleclient.controllers;
//
//import java.time.LocalDate;
//import java.util.Date;
//import java.util.List;
//
//import org.apache.commons.math3.analysis.function.Rint;
//import org.controlsfx.control.Notifications;
//import org.springframework.http.ResponseEntity;
//
//import com.maple.maple.util.SystemSetting;
//import com.maple.mapleclient.entity.ItemMst;
//import com.maple.mapleclient.entity.PurchaseDtl;
//import com.maple.mapleclient.entity.PurchaseHdr;
//import com.maple.mapleclient.entity.PurchaseHdrReport;
//import com.maple.mapleclient.entity.PurchaseReturnDtl;
//import com.maple.mapleclient.entity.PurchaseReturnHdr;
//import com.maple.mapleclient.entity.UnitMst;
//import com.maple.mapleclient.restService.RestCaller;
//
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.fxml.FXML;
//import javafx.geometry.Pos;
//import javafx.scene.control.Button;
//import javafx.scene.control.DatePicker;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.input.KeyEvent;
//import javafx.scene.input.MouseEvent;
//import javafx.util.Duration;
//
//public class PurchaseReturnCtl {
//
//	private ObservableList<PurchaseHdrReport> purchaseHdrReportList = FXCollections.observableArrayList();
//
//	private ObservableList<PurchaseDtl> purchaseDtlReportList = FXCollections.observableArrayList();
//
//	private ObservableList<PurchaseReturnDtl> purchaseReturnDtlList = FXCollections.observableArrayList();
//	PurchaseReturnHdr purchaseReturnHdr;
//	PurchaseHdr purchaseHdr = null;
//
//	Double allowedReturnQty;
//	@FXML
//	
//	private DatePicker dpDate;
//
//	@FXML
//	private Button btnFetchInvoice;
//
//	@FXML
//	private TableView<PurchaseHdrReport> tblInvoice;
//
//	@FXML
//	private TableColumn<PurchaseHdrReport, String> clVoucherNo;
//
//	@FXML
//	private TableColumn<PurchaseHdrReport, String> clSupplierName;
//
//	@FXML
//	private TableView<PurchaseDtl> itemDetailTable;
//
//	@FXML
//	private TableColumn<PurchaseDtl, String> clItemName;
//
//	@FXML
//	private TableColumn<PurchaseDtl, String> clBarCode;
//
//	@FXML
//	private TableColumn<PurchaseDtl, Number> clQty;
//
//	@FXML
//	private TableColumn<PurchaseDtl, Number> clTaxRate;
//
//	@FXML
//	private TableColumn<PurchaseDtl, Number> clMrp;
//
//	@FXML
//	private TableColumn<PurchaseDtl, Number> clAmount;
//
//	@FXML
//	private TableColumn<PurchaseDtl, String> columnBatch;
//
//	@FXML
//	private TableColumn<PurchaseDtl, String> columnUnitName;
//
//	@FXML
//	private TableColumn<PurchaseDtl, LocalDate> columnExpiryDate;
//
//	@FXML
//	private TableColumn<PurchaseDtl, String> clReturned;
//
//	@FXML
//	private Button btnFinalSave;
//
//	@FXML
//	private TableView<PurchaseReturnDtl> tblPurchaseReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, String> clItemReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, String> clBarCodeReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, Number> clQtyReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, Number> clTaxRateReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, Number> clMrpReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, Number> clAmountReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, String> clBatchReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, String> clUnitReturn;
//
//	@FXML
//	private TableColumn<PurchaseReturnDtl, String> clExpiryReturn;
//	
//	@FXML
//    private TableColumn<PurchaseReturnDtl, Number> clPurchaseRate;
 
 
//	@FXML
//	private TextField txtSalesReturmBillAmount;
//	String voucherNumber;
//	Double qty;
//	String purchaseDtlId;
//
//	@FXML
//	private Button btnDelete;
//
//	@FXML
//	private Button BtnClear;
//
//	@FXML
//	private TextField gstNo;
//
//	@FXML
//	private TextField txtCustomername;
//
//	@FXML
//	private TextField txtAdress;
//
//	@FXML
//	private TextField txtCashtopay;
//
//	@FXML
//	private TextField txtQty;
//
//	@FXML
//	private Button btnAdditem;
//	PurchaseReturnDtl purchaseReturnDtl;
//
//	@FXML
//	void CustomerPopUp(MouseEvent event) {
//
//	}
//
//	@FXML
//	void purchaseReturn(ActionEvent event) {
//
//		if (null != purchaseHdr) {
//			if (null != purchaseDtlId) {
//				ResponseEntity<PurchaseDtl> purchaseDtlRep = RestCaller.getPurchaseDtlById(purchaseDtlId);
//				PurchaseDtl purchaseDtl = purchaseDtlRep.getBody();
//
//				if (null == purchaseDtl) {
//					return;
//				}
//
//				if (null == purchaseReturnHdr) {
//					
//					purchaseReturnHdr = new PurchaseReturnHdr();
//					purchaseReturnHdr.setPurchaseHdr(purchaseHdr);
//
//					if (null != purchaseHdr.getPurchaseType() || !purchaseHdr.getPurchaseType().trim().isEmpty()) {
//						purchaseReturnHdr.setPurchaseType(purchaseHdr.getPurchaseType());
//					}
//
//					
//					if (null != purchaseHdr.getSupplierId()) {
//						purchaseReturnHdr.setSupplierId(purchaseHdr.getSupplierId());
//					}
//					if (null != purchaseHdr.getPurchaseType()) {
//						purchaseReturnHdr.setFinalSavedStatus("NO");
//					}
//					if (null != purchaseHdr.getMachineId()) {
//						purchaseReturnHdr.setMachineId(purchaseHdr.getMachineId());
//					}
//					if (null != purchaseHdr.getBranchCode()) {
//						purchaseReturnHdr.setBranchCode(purchaseHdr.getBranchCode());
//					}
//					if (null != purchaseHdr.getDeletedStatus()) {
//						purchaseReturnHdr.setDeletedStatus(purchaseHdr.getDeletedStatus());
//					}
//					if (null != purchaseHdr.getNarration()) {
//						purchaseReturnHdr.setNarration(purchaseHdr.getNarration());
//					}
//					if (null != purchaseHdr.getPurchaseType()) {
//						purchaseReturnHdr.setPurchaseType(purchaseHdr.getPurchaseType());
//					}
//					if (null != purchaseHdr.getpONum()) {
//						purchaseReturnHdr.setpONum(purchaseHdr.getpONum());
//					}
//					if (null != purchaseHdr.getPoDate()) {
//						purchaseReturnHdr.setPoDate(purchaseHdr.getPoDate());
//					}
//					if (null != purchaseHdr.getSupplierInvNo()) {
//						purchaseReturnHdr.setSupplierInvNo(purchaseHdr.getSupplierInvNo());
//					}
//					if (null != purchaseHdr.getVoucherNumber()) {
//						purchaseReturnHdr.setVoucherNumber(purchaseHdr.getVoucherNumber());
//					}
//					if (null != purchaseHdr.getInvoiceTotal()) {
//						purchaseReturnHdr.setInvoiceTotal(purchaseHdr.getInvoiceTotal());
//					}
//					if (null != purchaseHdr.getCurrency()) {
//						purchaseReturnHdr.setCurrency(purchaseHdr.getCurrency());
//					}
//
//					purchaseReturnHdr.setVoucherDate(SystemSetting.applicationDate);
//
//					purchaseReturnHdr.setUserId(SystemSetting.getUserId());
//					if (null != purchaseHdr.getInvoiceTotal()) {
//						purchaseReturnHdr.setInvoiceTotal(purchaseHdr.getInvoiceTotal());
//					}
//
//					ResponseEntity<PurchaseReturnHdr> purchaseHdrRep = RestCaller
//							.savePurchaseReturnHdr(purchaseReturnHdr);
//					purchaseReturnHdr = purchaseHdrRep.getBody();
//
//				}
//				
//				
//
//				if (null != purchaseReturnHdr) {
//
//					PurchaseReturnDtl purchaseReturnDtl = new PurchaseReturnDtl();
//
//					ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(purchaseDtl.getItemId());
//					ItemMst itemMst = itemMstResp.getBody();
//					if (null == itemMst) {
//						return;
//					}


				}

			}
		});

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getQty()) {

					qty = newSelection.getQty();
					allowedReturnQty = newSelection.getQty();
					purchaseDtlId = newSelection.getId();

					String itemId=newSelection.getItemId();
					String batch=newSelection.getBatch();
					
					Double storeQty= fetchStockQty(itemId,batch,cmbStoreFrom.getSelectionModel().getSelectedItem());
					txtQty.setText(String.valueOf(storeQty));
					
					qty=storeQty.doubleValue();
					//txtQty.setText(String.valueOf(qty));
					
					
				

				}

			}
		});
		
		cmbStoreFrom.valueProperty().addListener(new ChangeListener<String>()
				{
			
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null != newValue) {
					
					lblRetunQty.setText("Possible Return Qty at " + newValue);
					
					itemDetailTable.getItems().clear();
					tblPurchaseReturn.getItems().clear();
					 purchaseReturnDtl= null;
				        purchaseReturnHdr = null;
				        purchaseDtlReportList.clear();
				        purchaseReturnDtlList.clear();
				        txtQty.clear();
				        txtCashtopay.clear();
				        txtSalesReturmBillAmount.clear();
				
					
				
				}
			
				}

			
				});
		
		
		tblPurchaseReturn.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				
				if(null != newSelection.getId())
				{
					purchaseReturnDtl = new PurchaseReturnDtl();
					purchaseReturnDtl.setId(newSelection.getId());
				}
				
			}
		});
		
		ResponseEntity<List<StoreMst>> storeMstListResp = RestCaller.getAllStoreMst();
		List<StoreMst> storeMstList = storeMstListResp.getBody();

		
		
		cmbStoreFrom.getSelectionModel().select("Please Select");
		for (StoreMst storeMst : storeMstList) {
			cmbStoreFrom.getItems().add(storeMst.getShortCode());
		}

	}

	private Double fetchStockQty(String itemId, String batch, String storeFrom) {
		
		Double storeQty=RestCaller.getItemStockByStore(itemId,batch,storeFrom);
		
		
		
		return storeQty;
	}

	private void fetchPurchasehdr(String voucherNumber) {

		if (null == dpDate.getValue()) {

			notifyMessage(3, "", false);
			return;

		}

		Date date = SystemSetting.localToUtilDate(dpDate.getValue());
		String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

		ResponseEntity<List<PurchaseHdr>> puchaseHdrResp = RestCaller.getPurchaseHdrByVoucherNumber(voucherNumber,
				sdate);

		System.out.print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		System.out.print(puchaseHdrResp.getBody() + "purchase hdr body issssssssssssssssssssssss");

		if (null != puchaseHdrResp) {

			if (puchaseHdrResp.getBody().size() > 0) {

				purchaseHdr = puchaseHdrResp.getBody().get(0);
				if(null!=purchaseHdr.getInvoiceTotal()) {
					txtCashtopay.setText(String.valueOf(purchaseHdr.getInvoiceTotal()));
					}
				ResponseEntity<List<PurchaseDtl>> purchaseDtlListResp = RestCaller.getPurchaseDtl(purchaseHdr);
				
				// OldReturnedQty=RestCaller.getPurchaseReturnDtl(purchaseDtlListResp.getBody().get(0).getId());
				
				purchaseDtlReportList = FXCollections.observableArrayList(purchaseDtlListResp.getBody());
			    itemDetailTable.setItems(purchaseDtlReportList);
				FillTablePurchase();

			}

		}

	}

	private void FillTablePurchaseReturn() {

		tblPurchaseReturn.setItems(purchaseReturnDtlList);

		for (PurchaseReturnDtl purchase : purchaseReturnDtlList) {
			if (null != purchase.getItemId()) {
				ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(purchase.getItemId());
				ItemMst itemMst = itemMstResp.getBody();
				if (null != itemMst) {
					purchase.setItemName(itemMst.getItemName());
				}
				
			}

			
			if (null != purchase.getUnitId()) {
				ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(purchase.getUnitId());
				UnitMst unitMst = unitMstResp.getBody();
				if (null != unitMst) {
					purchase.setUnitName(unitMst.getUnitName());
				}
			}

		}

		clItemReturn.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clBarCodeReturn.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		clQtyReturn.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clTaxRateReturn.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clMrpReturn.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clAmountReturn.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clBatchReturn.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clUnitReturn.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clExpiryReturn.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		// clReturned.setCellValueFactory(cellData -> cellData.getValue().);

	}
	

	private void FillTablePurchase() {
		
		
		for (PurchaseDtl purchaseDtl:purchaseDtlReportList)
		{
			
			 OldReturnedQty=RestCaller.getPurchaseReturnDtl(purchaseDtl.getId());
			purchaseDtl.setQty(purchaseDtl.getQty()-OldReturnedQty);
		    purchaseDtl.setReturnQty(OldReturnedQty);
			
	
		}
		
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodepProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getexpiryDateProperty());
		clPurchaseRate.setCellValueFactory(cellData -> cellData.getValue().getPurchaseRateProperty());
		clReturned.setCellValueFactory(cellData -> cellData.getValue().getReturnQtyProperty());
		
		

	}
	
	

	@FXML
	void FetchInvoice(ActionEvent event) {

		if (null == dpDate.getValue()) {

			notifyMessage(3, "", false);
			return;

		} else {

			Date date = SystemSetting.localToUtilDate(dpDate.getValue());
			String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

			ResponseEntity<List<PurchaseHdrReport>> PurchaseHdrReportBody = RestCaller
					.getPurchaseHdrReportByDate(sdate);
			purchaseHdrReportList = FXCollections.observableArrayList(PurchaseHdrReportBody.getBody());

			tblInvoice.setItems(purchaseHdrReportList);
			clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
			clSupplierName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());

			
		}

	}

	@FXML
	void FetchInvoiceOnKeyPress(KeyEvent event) {

	}

	@FXML
	void addItemButtonClick(ActionEvent event) {

	}

	@FXML
	void clearFields(ActionEvent event) {
		clear();

	}

	@FXML
	void deleteItem(ActionEvent event) {
		
		if(null != purchaseReturnDtl)
		{
			
			if(null != purchaseReturnDtl.getId())
			{
			RestCaller.deletePurchaseReturnDtlById(purchaseReturnDtl.getId());
				
				
			ResponseEntity<List<PurchaseReturnDtl>> purchaseReturnDtlListResp = RestCaller
				.getPurchaseReturnDtlByHdrId(purchaseReturnHdr.getId());

			purchaseReturnDtlList = FXCollections.observableArrayList(purchaseReturnDtlListResp.getBody());
				FillTablePurchaseReturn();
				itemDetailTable.getSelectionModel().clearSelection();
			
			}}
		
		

	}

	@FXML
	void finalSave(ActionEvent event) {

		
		String financialYear = SystemSetting.getFinancialYear();
		String pNo = RestCaller.getVoucherNumber(financialYear + "PVN");

		pNo =SystemSetting.systemBranch+pNo;
		purchaseReturnHdr.setVoucherNumber(pNo);

		java.util.Date date = SystemSetting.localToUtilDate(dpDate.getValue());
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());

		purchaseReturnHdr.setVoucherDate(sqlDate);
		purchaseReturnHdr.setInvoiceTotal(Double.valueOf(txtSalesReturmBillAmount.getText()));
	


	//String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		System.out.print(purchaseReturnHdr.getId()+"purchaseHdr id isssssssssssssssssssssssssssssssssssssssssssssssssssss");

	        RestCaller.updatePurchaseReturnHdr(purchaseReturnHdr);
	    	notifyMessage(3, "Purchase Return Updated...!!", false);
	    	
	   
	    	
	    	
	    	try {
				JasperPdfReportService.purchaseReturnInvoice(purchaseReturnHdr.getId());
			} catch (Exception e) {
				System.out.println(e);
			}
	    
	        
	      
	        
		
	        clear();
	        
	        itembatchDtl=null;
	        purchaseReturnDtl= null;
	        purchaseReturnHdr = null;
	        purchaseDtlReportList.clear();
	        purchaseReturnDtlList.clear();
	}

	@FXML
	void onEnterCustPopup(KeyEvent event) {

	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {

	}

	@FXML
	void saveOnKey(KeyEvent event) {

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}


}
//					purchaseReturnDtl.setItemId(itemMst.getId());
//					purchaseReturnDtl.setBarcode(itemMst.getBarCode());
//					purchaseReturnDtl.setUnitId(purchaseDtl.getUnitId());
//
//					purchaseReturnDtl.setItemName(purchaseDtl.getItemName());
//					purchaseReturnDtl.setPurchseRate(purchaseDtl.getPurchseRate());
//					purchaseReturnDtl.setBarcode(purchaseDtl.getBarcode());
//					purchaseReturnDtl.setQty(Double.parseDouble(txtQty.getText()));
//					purchaseReturnDtl.setAmount(purchaseDtl.getAmount());
//					purchaseReturnDtl.setBatch(purchaseDtl.getBatch());
//					purchaseReturnDtl.setExpiryDate(purchaseDtl.getexpiryDate());
//					purchaseReturnDtl.setAmount(purchaseDtl.getAmount());
//					purchaseReturnDtl.setBinNo(purchaseDtl.getBinNo());
//					purchaseReturnDtl.setCessAmt(purchaseDtl.getCessAmt());
//					purchaseReturnDtl.setFreeQty(purchaseDtl.getFreeQty());
//					purchaseReturnDtl.setMrp(purchaseDtl.getMrp());
//					purchaseReturnDtl.setNetCost(purchaseDtl.getNetCost());
//					purchaseReturnDtl.setPreviousMRP(purchaseDtl.getPreviousMRP());
//					purchaseReturnDtl.setPurchaseRate(purchaseDtl.getPurchseRate());
//					purchaseReturnDtl.setTaxAmt(purchaseDtl.getTaxAmt());
//					purchaseReturnDtl.setTaxRate(purchaseDtl.getTaxRate());
//					
//					
//		
//					
//
//					purchaseReturnDtl.setPurchaseReturnHdr(purchaseReturnHdr);
//
//					ResponseEntity<PurchaseReturnDtl> purchaseReturnDtlRep = RestCaller
//							.savePurchaseReturnDtl(purchaseReturnDtl);
//					purchaseReturnDtl = purchaseReturnDtlRep.getBody();
//
//					ResponseEntity<List<PurchaseReturnDtl>> purchaseReturnDtlListResp = RestCaller
//							.getPurchaseReturnDtlByHdrId(purchaseReturnHdr.getId());
//
//					purchaseReturnDtlList = FXCollections.observableArrayList(purchaseReturnDtlListResp.getBody());
//					FillTablePurchaseReturn();
//				}
//				
//
//				purchaseDtl = null;
//
//			}
//			
//			purchaseDtlId = null;
//			purchaseReturnDtl = null;
//		}
//	}
//
//	@FXML
//	private void initialize() {
//
//		tblInvoice.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//			if (newSelection != null) {
//				System.out.println("getSelectionModel");
//				if (null != newSelection.getVoucherNumber()) {
//					voucherNumber = newSelection.getVoucherNumber();
//
//					 fetchPurchasehdr(voucherNumber);
//
////					if (null == dpDate.getValue()) {
////
////						notifyMessage(3, "", false);
////						return;
////
////					}
//
////					Date date = SystemSetting.localToUtilDate(dpDate.getValue());
////					String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
////
////					ResponseEntity<List<PurchaseHdr>> puchaseHdrResp = RestCaller
////							.getPurchaseHdrByVoucherNumber(voucherNumber, sdate);
////
////					System.out.print(puchaseHdrResp.getBody() + "purchase hdr body issssssssssssssssssssssss");
////					purchaseHdr = puchaseHdrResp.getBody().get(0);
////
////					System.out.print(purchaseHdr + "purchase hdr issssssssssssssssssssssssssssssssssssss");
////					if (null != puchaseHdrResp) {
////
////						if (puchaseHdrResp.getBody().size() > 0) {
////
////
////							ResponseEntity<List<PurchaseDtl>> purchaseDtlListResp = RestCaller
////									.getPurchaseDtl(purchaseHdr);
////							purchaseDtlReportList = FXCollections.observableArrayList(purchaseDtlListResp.getBody());
////							itemDetailTable.setItems(purchaseDtlReportList);
////							FillTablePurchase();
////
////						}
////
////					}
//
//				}
//
//			}
//		});
//
//		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//			if (newSelection != null) {
//				System.out.println("getSelectionModel");
//				if (null != newSelection.getQty()) {
//
//					qty = newSelection.getQty();
//					allowedReturnQty = newSelection.getQty();
//					purchaseDtlId = newSelection.getId();
//
//					txtQty.setText(String.valueOf(qty));
//
//				}
//
//			}
//		});
//		
//		
//		tblPurchaseReturn.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//			if (newSelection != null) {
//				
//				if(null != newSelection.getId())
//				{
//					purchaseReturnDtl = new PurchaseReturnDtl();
//					purchaseReturnDtl.setId(newSelection.getId());
//				}
//				
//			}
//		});
//
//	}
//
//	private void fetchPurchasehdr(String voucherNumber) {
//
//		if (null == dpDate.getValue()) {
//
//			notifyMessage(3, "", false);
//			return;
//
//		}
//
//		Date date = SystemSetting.localToUtilDate(dpDate.getValue());
//		String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
//
//		ResponseEntity<List<PurchaseHdr>> puchaseHdrResp = RestCaller.getPurchaseHdrByVoucherNumber(voucherNumber,
//				sdate);
//
//		System.out.print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
//		System.out.print(puchaseHdrResp.getBody() + "purchase hdr body issssssssssssssssssssssss");
//
//		if (null != puchaseHdrResp) {
//
//			if (puchaseHdrResp.getBody().size() > 0) {
//
//				purchaseHdr = puchaseHdrResp.getBody().get(0);
//
//				ResponseEntity<List<PurchaseDtl>> purchaseDtlListResp = RestCaller.getPurchaseDtl(purchaseHdr);
//				purchaseDtlReportList = FXCollections.observableArrayList(purchaseDtlListResp.getBody());
//				itemDetailTable.setItems(purchaseDtlReportList);
//				FillTablePurchase();
//
//			}
//
//		}
//
//	}
//
//	private void FillTablePurchaseReturn() {
//
//		tblPurchaseReturn.setItems(purchaseReturnDtlList);
//
//		for (PurchaseReturnDtl purchase : purchaseReturnDtlList) {
//			if (null != purchase.getItemId()) {
//				ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(purchase.getItemId());
//				ItemMst itemMst = itemMstResp.getBody();
//				if (null != itemMst) {
//					purchase.setItemName(itemMst.getItemName());
//				}
//				
//			}
//
//			
//			if (null != purchase.getUnitId()) {
//				ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(purchase.getUnitId());
//				UnitMst unitMst = unitMstResp.getBody();
//				if (null != unitMst) {
//					purchase.setUnitName(unitMst.getUnitName());
//				}
//			}
//
//		}
//
//		clItemReturn.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
//		clBarCodeReturn.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
//		clQtyReturn.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
//		clTaxRateReturn.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
//		clMrpReturn.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
//		clAmountReturn.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
//		clBatchReturn.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
//		clUnitReturn.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
//		clExpiryReturn.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
//		// clReturned.setCellValueFactory(cellData -> cellData.getValue().);
//
//	}
//	
//
//	private void FillTablePurchase() {
//		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
//		clBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodepProperty());
//		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
//		clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
//		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
//		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
//		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
//		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
//		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getexpiryDateProperty());
//		clPurchaseRate.setCellValueFactory(cellData -> cellData.getValue().getPurchaseRateProperty());
//		// clReturned.setCellValueFactory(cellData -> cellData.getValue().);
//
//	}
//
//	@FXML
//	void FetchInvoice(ActionEvent event) {
//
//		if (null == dpDate.getValue()) {
//
//			notifyMessage(3, "", false);
//			return;
//
//		} else {
//
//			Date date = SystemSetting.localToUtilDate(dpDate.getValue());
//			String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
//
//			ResponseEntity<List<PurchaseHdrReport>> PurchaseHdrReportBody = RestCaller
//					.getPurchaseHdrReportByDate(sdate);
//			purchaseHdrReportList = FXCollections.observableArrayList(PurchaseHdrReportBody.getBody());
//
//			tblInvoice.setItems(purchaseHdrReportList);
//			clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
//			clSupplierName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
//
//		}
//
//	}
//
//	@FXML
//	void FetchInvoiceOnKeyPress(KeyEvent event) {
//
//	}
//
//	@FXML
//	void addItemButtonClick(ActionEvent event) {
//
//	}
//
//	@FXML
//	void clearFields(ActionEvent event) {
//
//	}
//
//	@FXML
//	void deleteItem(ActionEvent event) {
//		
//		if(null != purchaseReturnDtl)
//		{
//			
//			if(null != purchaseReturnDtl.getId())
//			{
//				RestCaller.deletePurchaseReturnDtlById(purchaseReturnDtl.getId());
//				
//				
//				ResponseEntity<List<PurchaseReturnDtl>> purchaseReturnDtlListResp = RestCaller
//						.getPurchaseReturnDtlByHdrId(purchaseReturnHdr.getId());
//
//				purchaseReturnDtlList = FXCollections.observableArrayList(purchaseReturnDtlListResp.getBody());
//				FillTablePurchaseReturn();
//			}
//			
//
//		}
//		
//		
//
//	}
//
//	@FXML
//	void finalSave(ActionEvent event) {
//
//	}
//
//	@FXML
//	void onEnterCustPopup(KeyEvent event) {
//
//	}
//
//	@FXML
//	void qtyKeyRelease(KeyEvent event) {
//
//	}
//
//	@FXML
//	void saveOnKey(KeyEvent event) {
//
//	}
//
//	public void notifyMessage(int duration, String msg, boolean success) {
//
//		Image img;
//		if (success) {
//			img = new Image("done.png");
//
//		} else {
//			img = new Image("failed.png");
//		}
//
//		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
//				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
//				.onAction(new EventHandler<ActionEvent>() {
//					@Override
//					public void handle(ActionEvent event) {
//						System.out.println("clicked on notification");
//					}
//				});
//		notificationBuilder.darkStyle();
//		notificationBuilder.show();
//
//	}

//
//}
