package com.maple.mapleclient.controllers;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.poi.ss.formula.ptg.TblPtg;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.church.controllers.OrganisationMstCtl;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountHeadsProperties;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemNutritionMst;
import com.maple.mapleclient.entity.ItemPropertyConfig;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SchEligiAttrListDef;
import com.maple.mapleclient.entity.SchEligibilityDef;
import com.maple.mapleclient.entity.SchOfferAttrListDef;
import com.maple.mapleclient.entity.SchOfferDef;
import com.maple.mapleclient.entity.SchSelectAttrListDef;
import com.maple.mapleclient.entity.SchSelectDef;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class AccountHeadsCtl {
	String taskid;
	String processInstanceId;
	String accountId = "";

	private ObservableList<AccountHeads> accountHeadList = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();
	// private ObservableList<ReceiptModeMst> receiptModeList =
	// FXCollections.observableArrayList();

	private ObservableList<AccountHeads> accountHeadList1 = FXCollections.observableArrayList();
	private ObservableList<AccountHeads> parentList = FXCollections.observableArrayList();
	private ObservableList<AccountHeadsProperties> accountHeadsPropertiesList = FXCollections.observableArrayList();
	AccountHeads accountHeads;
	SchOfferDef schofferdef = null;
	SchEligibilityDef schEligibilityDef = null;
	SchSelectDef schSelectDef = null;
	
	AccountHeadsProperties accountHeadsProperties = null;
	
	StringProperty SearchString = new SimpleStringProperty();
	@FXML
	private Button btnPrintException;
	@FXML
	private ComboBox<String> cmbGroup;
	 
	@FXML
	private TextField txtAccountName;

	@FXML
	private Button btnInitialize;
	@FXML
	private Button btnRefresh;
	@FXML
	private ComboBox<String> cmbParentId;

	@FXML
	private TableView<AccountHeads> tbAccountHeads;

	@FXML
	private TableColumn<AccountHeads, String> clAcccountName;
	@FXML
	private TableColumn<AccountHeads, String> clParent;

	@FXML
	private TableColumn<AccountHeads, String> clGroup;
	
	
	
/*
 * 	account heads properties
 */
	@FXML
	private TextField txtPropertyAccount;

	@FXML
	private TextField txtPropertyName;

	@FXML
	private TableView<AccountHeadsProperties> tbAccountProperty;

	@FXML
	private TableColumn<AccountHeadsProperties, String> clPropertyAccountName;

	@FXML
	private TableColumn<AccountHeadsProperties, String> clAccountProperty;

	@FXML
	private TableColumn<AccountHeadsProperties, String> clPropertyType;

	@FXML
	private Button btnPropertySave;

	@FXML
	private Button btnPropertyEdit;

	@FXML
	private Button btnPropertyDelete;

	@FXML
	private ComboBox<String> cmbPropertyType;
	
	@FXML
	void actionPropertyDelete(ActionEvent event) {
			
		if (null != accountHeadsProperties) {

			if (null != accountHeadsProperties.getId()) {
				RestCaller.deleteAccountHeadsProperties(accountHeadsProperties.getId());

				showAllAccountHeadsProperties();
				notifyMessage(5, " Account Heads  deleted", false);
				return;
			}
		}
		
	}

	@FXML
	void actionPropertySave(ActionEvent event) {
		if (txtPropertyName.getText().contains("%") || (txtPropertyName.getText().contains("/"))
				|| (txtPropertyName.getText().contains("?")) || txtPropertyName.getText().contains("&")
				|| txtPropertyName.getText().contains("*") || txtPropertyName.getText().contains("$")) {
			notifyMessage(5, "AccountName Invalid");
			return;
		}
		String PropertyName = txtPropertyName.getText().trim();

		PropertyName = PropertyName.replaceAll("'", "");
		PropertyName = PropertyName.replaceAll("&", "");

		PropertyName = PropertyName.replaceAll("/", "");
		PropertyName = PropertyName.replaceAll("%", "");
		PropertyName = PropertyName.replaceAll("\"", "");
		accountHeadsProperties = new AccountHeadsProperties();
		accountHeadsProperties.setAccountNameProperty(txtPropertyAccount.getText());
		ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByNameRequestParam(txtPropertyAccount.getText());
		accountHeadsProperties.setAccountId(accountHeads.getBody().getId());
		accountHeadsProperties.setPropertyName(PropertyName);
		accountHeadsProperties.setBranchCode(SystemSetting.getUser().getBranchCode());
		accountHeadsProperties.setPropertyType(cmbPropertyType.getSelectionModel().getSelectedItem());
		ResponseEntity<AccountHeadsProperties> accountpro = RestCaller
				.getAccountHeadsPropertiesByAccountNameAndProperty(accountHeads.getBody().getId(), PropertyName);
		if (null != accountpro.getBody()) {
			notifyMessage(3, "Already Saved");
			return;
		} else {
			ResponseEntity<AccountHeadsProperties> respenity = RestCaller.saveAccountHeadsProperties(accountHeadsProperties);
			accountHeadsProperties = respenity.getBody();
			accountHeadsPropertiesList.add(accountHeadsProperties);
		}
		fillPropertyTable();
		txtPropertyName.clear();
		txtPropertyName.requestFocus();
	}

	@FXML
	void actionPropertyShowAll(ActionEvent event) {
		
		
		showAllAccountHeadsProperties();
		
	}
	    
	private void showAllAccountHeadsProperties() {
		ResponseEntity<List<AccountHeadsProperties>> accountHeadsProperties = RestCaller.getAllAccountProperty();
		accountHeadsPropertiesList = FXCollections.observableArrayList(accountHeadsProperties.getBody());
		fillPropertyTable();
		
	}

	private void fillPropertyTable() {
		for (AccountHeadsProperties accountHeadsPro : accountHeadsPropertiesList) {
			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsById(accountHeadsPro.getAccountId());
			accountHeadsPro.setAccountName(accountHeads.getBody().getAccountName());
		}
		tbAccountProperty.setItems(accountHeadsPropertiesList);
		clPropertyAccountName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		clPropertyType.setCellValueFactory(cellData -> cellData.getValue().getPropertyTypeProperty());
		clAccountProperty.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
	}
	
	@FXML
	void onAccountNameEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			loadAccountHeadsPopUp();
		}
	}
	
	
	

	private void loadAccountHeadsPopUp() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtPropertyName.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) txtPropertyAccount.getScene().getWindow();
		if (stage.isShowing()) {

			txtPropertyAccount.setText(accountEvent.getAccountName());
			accountId = accountEvent.getAccountId();

		}

	}

	@FXML
	void TxtOnkeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbParentId.requestFocus();
		}

	}

	@FXML
	void AccountCmbKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbGroup.requestFocus();
		}

	}

	@FXML
	void InitializeAccounts(ActionEvent event) {

		if (null == SystemSetting.getUser()) {
			notifyMessage(5, "invalid user...!!!");
			return;
		}
		if (null == SystemSetting.systemBranch) {
			notifyMessage(5, "Branch Code is Invalid!!!!");
			return;
		}

		ResponseEntity<String> accountHeadcreation = RestCaller.initializeAccountHeadsMain();
		ResponseEntity<String> unitCreation = RestCaller.initializeUnitMst();
		
		ResponseEntity<String> storeCreation = RestCaller.initializeStoreMst();


		ResponseEntity<CurrencyMst> currencyMst = RestCaller.getCurrencyMstByName("MVR");
		if (null == currencyMst.getBody() || currencyMst.getBody().getCurrencyName().equalsIgnoreCase("MVR")) {
			CurrencyMst currencymst = new CurrencyMst();
			currencymst.setBranchCode(SystemSetting.getUser().getBranchCode());
			currencymst.setCurrencyName("MVR");
			currencymst.setId("SYC0001" + SystemSetting.getUser().getCompanyMst().getId());
			ResponseEntity<CurrencyMst> respentity = RestCaller.saveCurrencyMst(currencymst);
		}
		currencyMst = RestCaller.getCurrencyMstByName("INR");
		if (null == currencyMst.getBody() || currencyMst.getBody().getCurrencyName().equalsIgnoreCase("INR")) {
			CurrencyMst currencymst = new CurrencyMst();
			currencymst.setBranchCode(SystemSetting.getUser().getBranchCode());
			currencymst.setCurrencyName("INR");
			currencymst.setId("SYC0002" + SystemSetting.getUser().getCompanyMst().getId());
			ResponseEntity<CurrencyMst> respentity = RestCaller.saveCurrencyMst(currencymst);
		}

		currencyMst = RestCaller.getCurrencyMstByName("DOLLAR");
		if (null == currencyMst.getBody() || currencyMst.getBody().getCurrencyName().equalsIgnoreCase("DOLLAR")) {
			CurrencyMst currencymst = new CurrencyMst();
			currencymst.setBranchCode(SystemSetting.getUser().getBranchCode());
			currencymst.setCurrencyName("DOLLAR");
			currencymst.setId("SYC0003" + SystemSetting.getUser().getCompanyMst().getId());
			ResponseEntity<CurrencyMst> respentity = RestCaller.saveCurrencyMst(currencymst);
		}

		currencyMst = RestCaller.getCurrencyMstByName("EURO");
		if (null == currencyMst.getBody() || currencyMst.getBody().getCurrencyName().equalsIgnoreCase("EURO")) {
			CurrencyMst currencymst = new CurrencyMst();
			currencymst.setBranchCode(SystemSetting.getUser().getBranchCode());
			currencymst.setCurrencyName("EURO");
			currencymst.setId("SYC0004" + SystemSetting.getUser().getCompanyMst().getId());
			ResponseEntity<CurrencyMst> respentity = RestCaller.saveCurrencyMst(currencymst);
		}

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if (null == priceDefenitionMst) {
			priceDefenitionMst = new PriceDefenitionMst();
			priceDefenitionMst.setPriceCalculator("FIXEDAMOUNT");
			priceDefenitionMst.setPriceLevelName("COST PRICE");
			String vNo = RestCaller.getVoucherNumber("PD");
			priceDefenitionMst.setId(vNo);
			ResponseEntity<PriceDefenitionMst> respentity = RestCaller.savePriceDefenitionMst(priceDefenitionMst);
			priceDefenitionMst = respentity.getBody();
		}

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp2 = RestCaller.getPriceDefenitionMstByName("MRP");
		PriceDefenitionMst priceDefenitionMst2 = priceDefenitionMstResp2.getBody();
		if (null == priceDefenitionMst2) {
			priceDefenitionMst = new PriceDefenitionMst();
			priceDefenitionMst.setPriceCalculator("FIXEDAMOUNT");
			priceDefenitionMst.setPriceLevelName("MRP");
			String vNo = RestCaller.getVoucherNumber("PD");
			priceDefenitionMst.setId(vNo);
			ResponseEntity<PriceDefenitionMst> respentity = RestCaller.savePriceDefenitionMst(priceDefenitionMst);
			priceDefenitionMst = respentity.getBody();
		}

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp1 = RestCaller
				.getPriceDefenitionMstByName("STOCK TRANSFER");
		priceDefenitionMst = priceDefenitionMstResp1.getBody();
		if (null == priceDefenitionMst) {
			priceDefenitionMst = new PriceDefenitionMst();
			priceDefenitionMst.setPriceCalculator("FIXEDAMOUNT");
			priceDefenitionMst.setPriceLevelName("STOCK TRANSFER");
			String vNo = RestCaller.getVoucherNumber("PD");
			priceDefenitionMst.setId(vNo);
			ResponseEntity<PriceDefenitionMst> respentity = RestCaller.savePriceDefenitionMst(priceDefenitionMst);
			priceDefenitionMst = respentity.getBody();
		}

		/*
		 * creating Process
		 * 
		 */

		ResponseEntity<ProcessMst> processSaved = RestCaller.getProcessDtls("REPORTS");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("REPORTS")) {
			ProcessMst processMst = new ProcessMst();
			processMst.setId("PRC001" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst.setProcessDescription("Reports and Configuration");
			processMst.setProcessName("REPORTS");
			processMst.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst);
		}
		processSaved = RestCaller.getProcessDtls("KOT POS");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("KOT POS")) {
			ProcessMst processMst2 = new ProcessMst();
			processMst2.setId("PRC002" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst2.setProcessDescription("Kitchen Order POS");
			processMst2.setProcessName("KOT POS");
			processMst2.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst2);
		}

		processSaved = RestCaller.getProcessDtls("DAMAGE ENTRY");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DAMAGE ENTRY")) {

			ProcessMst processMst3 = new ProcessMst();
			processMst3.setId("PRC003" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst3.setProcessDescription("Damage Entry");
			processMst3.setProcessName("DAMAGE ENTRY");
			processMst3.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst3);
		}
		processSaved = RestCaller.getProcessDtls("WHOLESALE");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("WHOLESALE")) {

			ProcessMst processMst4 = new ProcessMst();
			processMst4.setId("PRC004" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst4.setProcessDescription("Whole Sale Entry");
			processMst4.setProcessName("WHOLESALE");
			processMst4.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst4);
		}
		processSaved = RestCaller.getProcessDtls("POS WINDOW");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("POS WINDOW")) {

			ProcessMst processMst5 = new ProcessMst();
			processMst5.setId("PRC005" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst5.setProcessDescription("POS Window entry");
			processMst5.setProcessName("POS WINDOW");
			processMst5.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst5);
		}
		processSaved = RestCaller.getProcessDtls("ONLINE SALES");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ONLINE SALES")) {

			ProcessMst processMst6 = new ProcessMst();
			processMst6.setId("PRC006" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst6.setProcessDescription("Online Sales entry");
			processMst6.setProcessName("ONLINE SALES");
			processMst6.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst6);
		}
		processSaved = RestCaller.getProcessDtls("ReORDER");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("ReORDER")) {

			ProcessMst processMst7 = new ProcessMst();
			processMst7.setId("PRC007" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst7.setProcessDescription("Re Order Entry");
			processMst7.setProcessName("ReORDER");
			processMst7.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst7);
		}
		processSaved = RestCaller.getProcessDtls("SALEORDER");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("SALEORDER")) {

			ProcessMst processMst8 = new ProcessMst();
			processMst8.setId("PRC008" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst8.setProcessDescription("Sale Order Entry");
			processMst8.setProcessName("SALEORDER");
			processMst8.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst8);
		}
		processSaved = RestCaller.getProcessDtls("ITEM MASTER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM MASTER")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC009" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("Item Creation Window");
			processMst9.setProcessName("ITEM MASTER");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ACCOUNT HEADS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCOUNT HEADS")) {

			ProcessMst processMst10 = new ProcessMst();
			processMst10.setId("PRC0010" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst10.setProcessDescription("Account Heds Creation and initialization");
			processMst10.setProcessName("ACCOUNT HEADS");
			processMst10.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst10);
		}
		processSaved = RestCaller.getProcessDtls("PRODUCT CONVERSION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCT CONVERSION")) {

			ProcessMst processMst11 = new ProcessMst();
			processMst11.setId("PRC0011" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst11.setProcessDescription("Convert Product into another");
			processMst11.setProcessName("PRODUCT CONVERSION");
			processMst11.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst11);
		}
		processSaved = RestCaller.getProcessDtls("RECEIPT WINDOW");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RECEIPT WINDOW")) {

			ProcessMst processMst12 = new ProcessMst();
			processMst12.setId("PRC0012" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst12.setProcessDescription("Reciept Entry");
			processMst12.setProcessName("RECEIPT WINDOW");
			processMst12.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst12);
		}

		processSaved = RestCaller.getProcessDtls("DAY END CLOSURE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DAY END CLOSURE")) {

			ProcessMst processMst14 = new ProcessMst();
			processMst14.setId("PRC0013" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst14.setProcessDescription("Day End Entry");
			processMst14.setProcessName("DAY END CLOSURE");
			processMst14.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst14);
		}
		processSaved = RestCaller.getProcessDtls("PURCHASE");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("PURCHASE")) {

			ProcessMst processMst15 = new ProcessMst();
			processMst15.setId("PRC0014" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst15.setProcessDescription("Purchase window Entry");
			processMst15.setProcessName("PURCHASE");
			processMst15.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst15);
		}
		processSaved = RestCaller.getProcessDtls("CUSTOMER");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("CUSTOMER")) {

			ProcessMst processMst16 = new ProcessMst();
			processMst16.setId("PRC0015" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst16.setProcessDescription("Customer Entry");
			processMst16.setProcessName("CUSTOMER");
			processMst16.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst16);
		}
		processSaved = RestCaller.getProcessDtls("ACCEPT STOCK");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCEPT STOCK")) {

			ProcessMst processMst17 = new ProcessMst();
			processMst17.setId("PRC0016" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst17.setProcessDescription("Accept Stock Entry");
			processMst17.setProcessName("ACCEPT STOCK");
			processMst17.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst17);
		}
		processSaved = RestCaller.getProcessDtls("ADD SUPPLIER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ADD SUPPLIER")) {

			ProcessMst processMst18 = new ProcessMst();
			processMst18.setId("PRC0017" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst18.setProcessDescription("New Supplier Entry");
			processMst18.setProcessName("ADD SUPPLIER");
			processMst18.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst18);
		}
		processSaved = RestCaller.getProcessDtls("PAYMENT");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("PAYMENT")) {

			ProcessMst processMst19 = new ProcessMst();
			processMst19.setId("PRC0018" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst19.setProcessDescription("Payment Entry");
			processMst19.setProcessName("PAYMENT");
			processMst19.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst19);
		}
		processSaved = RestCaller.getProcessDtls("TASK LIST");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("TASK LIST")) {

			ProcessMst processMst21 = new ProcessMst();
			processMst21.setId("PRC0019" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst21.setProcessDescription("Task List");
			processMst21.setProcessName("TASK LIST");
			processMst21.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst21);
			processSaved = RestCaller.getProcessDtls("MULTI UNIT");
		}
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("MULTI UNIT")) {

			ProcessMst processMst22 = new ProcessMst();
			processMst22.setId("PRC0020" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst22.setProcessDescription("Multi Unit Entry");
			processMst22.setProcessName("MULTI UNIT");
			processMst22.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst22);
		}
		processSaved = RestCaller.getProcessDtls("STOCK TRANSFER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK TRANSFER")) {

			ProcessMst processMst23 = new ProcessMst();
			processMst23.setId("PRC0021" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst23.setProcessDescription("Stock Transfer Out");
			processMst23.setProcessName("STOCK TRANSFER");
			processMst23.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst23);
		}
		processSaved = RestCaller.getProcessDtls("INTENT");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("INTENT")) {

			ProcessMst processMst24 = new ProcessMst();
			processMst24.setId("PRC0022" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst24.setProcessDescription("Request Intent");
			processMst24.setProcessName("INTENT");
			processMst24.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst24);
		}
		processSaved = RestCaller.getProcessDtls("PRODUCTION PLANNING");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCTION PLANNING")) {

			ProcessMst processMst25 = new ProcessMst();
			processMst25.setId("PRC0023" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst25.setProcessDescription("Production Planning");
			processMst25.setProcessName("PRODUCTION PLANNING");
			processMst25.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst25);
		}
		processSaved = RestCaller.getProcessDtls("JOURNAL");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("JOURNAL")) {

			ProcessMst processMst26 = new ProcessMst();
			processMst26.setId("PRC0024" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst26.setProcessDescription("Journal Entry Window");
			processMst26.setProcessName("JOURNAL");
			processMst26.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst26);
		}
		processSaved = RestCaller.getProcessDtls("SESSION END");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SESSION END")) {

			ProcessMst processMst27 = new ProcessMst();
			processMst27.setId("PRC0025" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst27.setProcessDescription("Session End Entry Window");
			processMst27.setProcessName("SESSION END");
			processMst27.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst27);

		}
		processSaved = RestCaller.getProcessDtls("PHYSICAL STOCK");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PHYSICAL STOCK")) {

			ProcessMst processMst28 = new ProcessMst();
			processMst28.setId("PRC0026" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst28.setProcessDescription("Physical Stock Entry Window");
			processMst28.setProcessName("PHYSICAL STOCK");
			processMst28.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst28);
		}
		processSaved = RestCaller.getProcessDtls("TAX CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("TAX CREATION")) {

			ProcessMst processMst29 = new ProcessMst();
			processMst29.setId("PRC0027" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst29.setProcessDescription("Tax Creation Entry Window");
			processMst29.setProcessName("TAX CREATION");
			processMst29.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst29);
		}
		processSaved = RestCaller.getProcessDtls("KIT DEFINITION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("KIT DEFINITION")) {

			ProcessMst processMst29 = new ProcessMst();
			processMst29.setId("PRC0028" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst29.setProcessDescription("Kit details Entry Window");
			processMst29.setProcessName("KIT DEFINITION");
			processMst29.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst29);
		}
		processSaved = RestCaller.getProcessDtls("PRICE DEFINITION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRICE DEFINITION")) {

			ProcessMst processMst29 = new ProcessMst();
			processMst29.setId("PRC0029" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst29.setProcessDescription("Price Definition details Entry Window");
			processMst29.setProcessName("PRICE DEFINITION");
			processMst29.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst29);
		}
		processSaved = RestCaller.getProcessDtls("KOT MANAGER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("KOT MANAGER")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0030" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Kitchen Order Table Entry");
			processMst30.setProcessName("KOT MANAGER");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("USER REGISTRATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("USER REGISTRATION")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0031" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("New User Creation");
			processMst30.setProcessName("USER REGISTRATION");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("PROCESS CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PROCESS CREATION")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0032" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("New Process Creation");
			processMst30.setProcessName("PROCESS CREATION");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("CHANGE PASSWORD");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CHANGE PASSWORD")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0033" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("New Process Creation");
			processMst30.setProcessName("CHANGE PASSWORD");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("BRANCH CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("BRANCH CREATION")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0034" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("New Branch Entry");
			processMst30.setProcessName("BRANCH CREATION");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("COMPANY CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("COMPANY CREATION")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0035" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("New Company Creation");
			processMst30.setProcessName("COMPANY CREATION");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("VOUCHER REPRINT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("VOUCHER REPRINT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0036" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Print with Voucher Number and Date");
			processMst30.setProcessName("VOUCHER REPRINT");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("SCHEME");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("SCHEME")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0037" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("New Scheme Entry");
			processMst30.setProcessName("SCHEME");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("SCHEME DETAILS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SCHEME DETAILS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0038" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("New Scheme Details Entry");
			processMst30.setProcessName("SCHEME DETAILS");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("PROCESS PERMISSION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PROCESS PERMISSION")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0039" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Process Permission Entry");
			processMst30.setProcessName("PROCESS PERMISSION");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("GROUP PERMISSION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("GROUP PERMISSION")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0040" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Process Permission Entry");
			processMst30.setProcessName("GROUP PERMISSION");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("GROUP CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("GROUP CREATION")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0041" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Group Creation Entry Window");
			processMst30.setProcessName("GROUP CREATION");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}

		// -------------------------customer report
		processSaved = RestCaller.getProcessDtls("Customer");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("Customer")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0042" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Customer Reports");
			processMst30.setProcessName("Customer");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("Sales Report");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("Sales Report")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0043" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Sales Report Window");
			processMst30.setProcessName("Sales Report");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("MONTHLY SUMMARY");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("MONTHLY SUMMARY")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0044" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Monthly Summary Report Window");
			processMst30.setProcessName("MONTHLY SUMMARY");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("SALEORDER REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALEORDER REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0045" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Sale Order Report Window");
			processMst30.setProcessName("SALEORDER REPORT");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("ACCOUNT BALANCE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCOUNT BALANCE")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0046" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Account Balance Report Window");
			processMst30.setProcessName("ACCOUNT BALANCE");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("MONTHLY STOCK REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("MONTHLY STOCK REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0047" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Monthly Stock Report Window");
			processMst30.setProcessName("MONTHLY STOCK REPORT");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}
		processSaved = RestCaller.getProcessDtls("SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0048" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Sales Report Window");
			processMst30.setProcessName("SALES REPORT");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);
		}

		processSaved = RestCaller.getProcessDtls("ACCOUNTS");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCOUNTS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0049" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Receipts and Payments Windows");
			processMst30.setProcessName("ACCOUNTS");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SUPPLIER PAYMENTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SUPPLIER PAYMENTS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0050" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("Supplier Payments Window");
			processMst30.setProcessName("SUPPLIER PAYMENTS");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("OWN ACCOUNT SETTLEMENT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("OWN ACCOUNT SETTLEMENT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0051" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("For Settlement of Receipts and Payments against invoice");
			processMst30.setProcessName("OWN ACCOUNT SETTLEMENT");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ACTUAL PRODUCTION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACTUAL PRODUCTION")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0052" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("For the Actual production Entry");
			processMst30.setProcessName("ACTUAL PRODUCTION");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALES RETURN");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES RETURN")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0053" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("For Whole Sale Return");
			processMst30.setProcessName("SALES RETURN");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALES FROM ORDER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES FROM ORDER")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0054" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("For converting sale order to sales");
			processMst30.setProcessName("SALES FROM ORDER");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALES ORDER EDIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES ORDER EDIT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0055" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("For editing sale orders");
			processMst30.setProcessName("SALES ORDER EDIT");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}
		processSaved = RestCaller.getProcessDtls("CONFIGURATIONS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CONFIGURATIONS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0056" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("For Configure Software");
			processMst30.setProcessName("CONFIGURATIONS");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("RECEIPT MODE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RECEIPT MODE")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0057" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("For Adding Payment Mode");
			processMst30.setProcessName("RECEIPT MODE");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("VOUCHER TYPE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("VOUCHER TYPE")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0058" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("For Adding Voucher Type");
			processMst30.setProcessName("VOUCHER TYPE");
			processMst30.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst30);

		}

//--------------------------------------------------version 2.0.1 surya----------------------------------------------------------------

		processSaved = RestCaller.getProcessDtls("CUSTOMER REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CUSTOMER REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0059" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CUSTOMER REPORT");
			processMst30.setProcessName("CUSTOMER REPORT REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEM MASTER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM MASTER")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0060" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEM MASTER REPORT");
			processMst30.setProcessName("ITEM MASTER");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ACCOUNT HEADS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCOUNT HEADS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0061" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ACCOUNT HEADS REPORT");
			processMst30.setProcessName("ACCOUNT HEADS");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SUPPLIERS");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("SUPPLIERS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0062" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SUPPLIERS REPORT");
			processMst30.setProcessName("SUPPLIERS");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("KIT REPORT");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("KIT REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0063" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("KIT REPORT");
			processMst30.setProcessName("KIT REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("STOCK REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0064" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("STOCK REPORT");
			processMst30.setProcessName("STOCK REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEM STOCK REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM STOCK REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0065" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEM STOCK REPORT");
			processMst30.setProcessName("ITEM STOCK REPORT");
			processMst30.setProcessType("REPORT");
			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEMWISE STOCK MOVEMENT REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEMWISE STOCK MOVEMENT REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0066" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEMWISE STOCK MOVEMENT REPORT");
			processMst30.setProcessName("ITEMWISE STOCK MOVEMENT REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("CATEGORYWISE STOCK MOVEMENT REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CATEGORYWISE STOCK MOVEMENT REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0067" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CATEGORYWISE STOCK MOVEMENT REPORT");
			processMst30.setProcessName("CATEGORYWISE STOCK MOVEMENT REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("INVENTORY REORDER STATUS REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("INVENTORY REORDER STATUS REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0068" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("INVENTORY REORDER STATUS REPORT");
			processMst30.setProcessName("INVENTORY REORDER STATUS REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("STOCK VALUE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK VALUE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0069" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("STOCK VALUE REPORT");
			processMst30.setProcessName("STOCK VALUE REPORT");
			processMst30.setProcessType("REPORT");
			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEM LOCATION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM LOCATION REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0070" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEM LOCATION REPORT");
			processMst30.setProcessName("ITEM LOCATION REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("FAST MOVING ITEMS REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("FAST MOVING ITEMS REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0071" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("FAST MOVING ITEMS REPORT");
			processMst30.setProcessName("FAST MOVING ITEMS REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("STATEMENTS OF ACCOUNT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STATEMENTS OF ACCOUNT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0072" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("STATEMENTS OF ACCOUNT REPORT");
			processMst30.setProcessName("STATEMENTS OF ACCOUNT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ALL CUSTOMER BILL WISE BALANCE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ALL CUSTOMER BILL WISE BALANCE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0073" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ALL CUSTOMER BILL WISE BALANCE REPORT");
			processMst30.setProcessName("ALL CUSTOMER BILL WISE BALANCE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("CUSTOMER BILL WISE BALANCE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CUSTOMER BILL WISE BALANCE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0074" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CUSTOMER BILL WISE BALANCE REPORT");
			processMst30.setProcessName("CUSTOMER BILL WISE BALANCE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SUPPLIER LEDGER REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SUPPLIER LEDGER REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0075" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SUPPLIER LEDGER REPORT");
			processMst30.setProcessName("SUPPLIER LEDGER REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("RECEIPT INVOICE DETAIL REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RECEIPT INVOICE DETAIL REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0076" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("RECEIPT INVOICE DETAIL REPORT");
			processMst30.setProcessName("RECEIPT INVOICE DETAIL REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("PAYMENT INVOICE DETAIL REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PAYMENT INVOICE DETAIL REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0077" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("PAYMENT INVOICE DETAIL REPORT");
			processMst30.setProcessName("PAYMENT INVOICE DETAIL REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("TRIAL BALANCE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("TRIAL BALANCE")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0077" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("TRIAL BALANCE REPORT");
			processMst30.setProcessName("TRIAL BALANCE");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("BALANCE SHEET");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("BALANCE SHEET")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0078" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("BALANCE SHEET REPORT");
			processMst30.setProcessName("BALANCE SHEET");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SUPPLIER DUE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SUPPLIER DUE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0079" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SUPPLIER DUE REPORT");
			processMst30.setProcessName("SUPPLIER DUE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("CARD RECONCILE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CARD RECONCILE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0080" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CARD RECONCILE REPORT");
			processMst30.setProcessName("CARD RECONCILE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SUNDRY DEBTOR BALANCE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SUNDRY DEBTOR BALANCE")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0081" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SUNDRY DEBTOR BALANCE REPORT");
			processMst30.setProcessName("SUNDRY DEBTOR BALANCE");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("TAX REPORT");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("TAX REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0082" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("TAX REPORT");
			processMst30.setProcessName("TAX REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ACCOUNT CLOSING BALANCE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCOUNT CLOSING BALANCE")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0083" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ACCOUNT CLOSING BALANCE REPORT");
			processMst30.setProcessName("ACCOUNT CLOSING BALANCE");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("DAILY SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DAILY SALES REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0084" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("DAILY SALES REPORT");
			processMst30.setProcessName("DAILY SALES REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("MONTHLY SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("MONTHLY SALES REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0085" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("MONTHLY SALES REPORT");
			processMst30.setProcessName("MONTHLY SALES REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("DAY BOOK REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DAY BOOK REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0086" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("DAY BOOK REPORT");
			processMst30.setProcessName("DAY BOOK REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("HSN CODE SALE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("HSN CODE SALE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0087" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("HSN CODE SALE REPORT");
			processMst30.setProcessName("HSN CODE SALE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("RECEIPT MODE WISE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RECEIPT MODE WISE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0088" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("RECEIPT MODE WISE REPORT");
			processMst30.setProcessName("RECEIPT MODE WISE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEM OR CATEGORY SALE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM OR CATEGORY SALE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0089" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEM OR CATEGORY SALE REPORT");
			processMst30.setProcessName("ITEM OR CATEGORY SALE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALE SUMMARY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALE SUMMARY REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0090" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SALE SUMMARY REPORT");
			processMst30.setProcessName("SALE SUMMARY REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALES MODEWISE SUMMARY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES MODEWISE SUMMARY REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0091" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SALES MODEWISE SUMMARY REPORT");
			processMst30.setProcessName("SALES MODEWISE SUMMARY REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("B2C SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("B2C SALES REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0092" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("B2C SALES REPORT");
			processMst30.setProcessName("B2C SALES REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("B2B SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("B2B SALES REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0093" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("B2B SALES REPORT");
			processMst30.setProcessName("B2B SALES REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("DAMAGE ENTRY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DAMAGE ENTRY REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0094" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("DAMAGE ENTRY REPORT");
			processMst30.setProcessName("DAMAGE ENTRY REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("BRANCH WISE PROFIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("BRANCH WISE PROFIT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0095" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("BRANCH WISE PROFIT REPORT");
			processMst30.setProcessName("BRANCH WISE PROFIT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("PENDING INTENT REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PENDING INTENT REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0096" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("PENDING INTENT REPORT");
			processMst30.setProcessName("PENDING INTENT REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("OTHER BRANCH SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("OTHER BRANCH SALES REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0097" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("OTHER BRANCH SALES REPORT");
			processMst30.setProcessName("OTHER BRANCH SALES REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("PRODUCT CONVERSION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCT CONVERSION REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0098" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("PRODUCT CONVERSION REPORT");
			processMst30.setProcessName("PRODUCT CONVERSION REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("CUSTOMER BILLWISE BY DUE DAYS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CUSTOMER BILLWISE BY DUE DAYS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0099" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CUSTOMER BILLWISE BY DUE DAYS REPORT");
			processMst30.setProcessName("CUSTOMER BILLWISE BY DUE DAYS");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("CUSTOMER SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CUSTOMER SALES REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0100" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CUSTOMER SALES REPORT");
			processMst30.setProcessName("CUSTOMER SALES REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALES RETURN REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES RETURN REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0101" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SALES RETURN REPORT");
			processMst30.setProcessName("SALES RETURN REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEM OR SERVICE WISE PROFIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM OR SERVICE WISE PROFIT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0102" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEM OR SERVICE WISE PROFIT");
			processMst30.setProcessName("ITEM OR SERVICE WISE PROFIT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALEORDER REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALEORDER REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0103" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SALEORDER REPORT");
			processMst30.setProcessName("SALEORDER REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALEORDER BALANCE ADVANCE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALEORDER BALANCE ADVANCE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0104" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SALEORDER BALANCE ADVANCE REPORT");
			processMst30.setProcessName("SALEORDER BALANCE ADVANCE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("DELIVERYBOY SALEORDER REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DELIVERYBOY SALEORDER REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0105" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("DELIVERYBOY SALEORDER REPORT");
			processMst30.setProcessName("DELIVERYBOY SALEORDER REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("HOMEDELIVERY OR TAKE AWAY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("HOMEDELIVERY OR TAKE AWAY REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0106" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("HOMEDELIVERY/TAKE AWAY REPORT");
			processMst30.setProcessName("HOMEDELIVERY/TAKE AWAY REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SALEORDER PENDING REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALEORDER PENDING REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0107" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SALEORDER PENDING REPORT");
			processMst30.setProcessName("SALEORDER PENDING REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("DELIVERY BOY PENDING REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DELIVERY BOY PENDING REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0108" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("DELIVERY BOY PENDING REPORT");
			processMst30.setProcessName("DELIVERY BOY PENDING REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("DAILY CAKE SETTING");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DAILY CAKE SETTING")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0109" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("DAILY CAKE SETTING");
			processMst30.setProcessName("DAILY CAKE SETTING");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("HOMEDELIVERY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("HOMEDELIVERY REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0110" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("HOMEDELIVERY REPORT");
			processMst30.setProcessName("HOMEDELIVERY REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("DAILY ORDER REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DAILY ORDER REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0111" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("DAILY ORDER REPORT");
			processMst30.setProcessName("DAILY ORDER REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("STOCK TRANSFER OUT REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK TRANSFER OUT REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0112" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("STOCK TRANSFER OUT REPORT");
			processMst30.setProcessName("STOCK TRANSFER OUT REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("STOCK TRANSFER IN REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK TRANSFER IN REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0113" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("STOCK TRANSFER IN REPORT");
			processMst30.setProcessName("STOCK TRANSFER IN REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("STOCK TRANSFER DETAIL DELETED REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK TRANSFER DETAIL DELETED REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0114" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("STOCK TRANSFER DETAIL DELETED REPORT");
			processMst30.setProcessName("STOCK TRANSFER DETAIL DELETED REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("VOUCHER REPRINTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("VOUCHER REPRINTS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0115" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("VOUCHER REPRINTS");
			processMst30.setProcessName("VOUCHER REPRINTS");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("PAYMENT REPORTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PAYMENT REPORTS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0116" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("PAYMENT REPORTS");
			processMst30.setProcessName("PAYMENT REPORTS");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("JOURNAL REPORTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("JOURNAL REPORTS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0117" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("JOURNAL REPORTS");
			processMst30.setProcessName("JOURNAL REPORTS");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("PURCHASE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PURCHASE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0118" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("PURCHASE REPORT");
			processMst30.setProcessName("PURCHASE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SUPPLIER MONTHLY SUMMARY");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SUPPLIER MONTHLY SUMMARY")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0120" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SUPPLIER MONTHLY SUMMARY REPORT");
			processMst30.setProcessName("SUPPLIER MONTHLY SUMMARY");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SUPPLIER BILLWISE BY DUE DAYS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SUPPLIER BILLWISE BY DUE DAYS")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0121" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SUPPLIER BILLWISE BY DUE DAYS REPORT");
			processMst30.setProcessName("SUPPLIER BILLWISE BY DUE DAYS");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("PRODUCTION PLANNING REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCTION PLANNING REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0122" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("PRODUCTION PLANNING REPORT");
			processMst30.setProcessName("PRODUCTION PLANNING REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("RAW MATERIAL REQUIRED REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RAW MATERIAL REQUIRED REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0123" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("RAW MATERIAL REQUIRED REPORT");
			processMst30.setProcessName("RAW MATERIAL REQUIRED REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("RAW MATERIAL ISSUE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RAW MATERIAL ISSUE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0124" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("RAW MATERIAL ISSUE REPORT");
			processMst30.setProcessName("RAW MATERIAL ISSUE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ACTUAL PRODUCTION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACTUAL PRODUCTION REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0125" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ACTUAL PRODUCTION REPORT");
			processMst30.setProcessName("ACTUAL PRODUCTION REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ACTUAL PRODUCTION AND PLANING SUMMARY");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACTUAL PRODUCTION AND PLANING SUMMARY")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0126" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ACTUAL PRODUCTION AND PLANING SUMMARY");
			processMst30.setProcessName("ACTUAL PRODUCTION AND PLANING SUMMARY");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("CONSUMPTION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CONSUMPTION REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0127" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CONSUMPTION REPORT");
			processMst30.setProcessName("CONSUMPTION REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("REASON WISE CONSUMPTION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("REASON WISE CONSUMPTION REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0128" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("REASON WISE CONSUMPTION REPORT");
			processMst30.setProcessName("REASON WISE CONSUMPTION REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("SHORT EXPIRY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SHORT EXPIRY REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0129" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("SHORT EXPIRY REPORT");
			processMst30.setProcessName("SHORT EXPIRY REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("CATEGORYWISE SHORT EXPIRY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CATEGORYWISE SHORT EXPIRY REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0130" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CATEGORYWISE SHORT EXPIRY REPORT");
			processMst30.setProcessName("CATEGORYWISE SHORT EXPIRY REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEMWISE SHORT EXPIRY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEMWISE SHORT EXPIRY REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0131" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEMWISE SHORT EXPIRY REPORT");
			processMst30.setProcessName("ITEMWISE SHORT EXPIRY REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("CONSUMPTION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CONSUMPTION REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0132" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("CONSUMPTION REPORT");
			processMst30.setProcessName("CONSUMPTION REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("REASON WISE CONSUMPTION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("REASON WISE CONSUMPTION REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0133" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("REASON WISE CONSUMPTION REPORT");
			processMst30.setProcessName("REASON WISE CONSUMPTION REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEM EXCEPTION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM EXCEPTION REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0134" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEM EXCEPTION REPORT");
			processMst30.setProcessName("ITEM EXCEPTION REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("WEIGHING MACHINE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("WEIGHING MACHINE REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0135" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("WEIGHING MACHINE REPORT");
			processMst30.setProcessName("WEIGHING MACHINE REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ITEM STOCK ENQUIRY");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM STOCK ENQUIRY")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0136" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("ITEM STOCK ENQUIRY REPORT");
			processMst30.setProcessName("ITEM STOCK ENQUIRY");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("PHYSICAL STOCK REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PHYSICAL STOCK REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0137" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("PHYSICAL STOCK REPORT");
			processMst30.setProcessName("PHYSICAL STOCK REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("PHYSICAL STOCK REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PHYSICAL STOCK REPORT")) {

			ProcessMst processMst30 = new ProcessMst();
			processMst30.setId("PRC0138" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst30.setProcessDescription("PHYSICAL STOCK REPORT");
			processMst30.setProcessName("PHYSICAL STOCK REPORT");
			processMst30.setProcessType("REPORT");

			RestCaller.saveProcessMst(processMst30);

		}

		processSaved = RestCaller.getProcessDtls("ORGANIZATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ORGANIZATION")) {
			ProcessMst processMst = new ProcessMst();
			processMst.setId("PRC0139" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst.setProcessDescription("Organization");
			processMst.setProcessName("ORGANIZATION");
			processMst.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst);
		}

		// -----------------------------------------------------------------------------------------------------------------

		processSaved = RestCaller.getProcessDtls("PETTY-CASH PAYMENTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PETTY-CASH PAYMENTS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0120" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PETTY-CASH PAYMENTS");
			processMst9.setProcessName("PETTY-CASH PAYMENTS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PETTY-CASH RECIEPTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PETTY-CASH RECIEPTS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0121" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PETTY-CASH RECIEPTS");
			processMst9.setProcessName("PETTY-CASH RECIEPTS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PETTY-CASH RECIEPTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PETTY-CASH RECIEPTS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0121" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PETTY-CASH RECIEPTS");
			processMst9.setProcessName("PETTY-CASH RECIEPTS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PDC RECEIPT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PDC RECEIPT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0122" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PDC RECEIPT");
			processMst9.setProcessName("PDC RECEIPT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PDC PAYMENT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PDC PAYMENT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0123" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PDC PAYMENT");
			processMst9.setProcessName("PDC PAYMENT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PDC RECONCILE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PDC RECONCILE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0124" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PDC RECONCILE");
			processMst9.setProcessName("PDC RECONCILE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RECEIPT EDIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RECEIPT EDIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0125" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RECEIPT EDIT");
			processMst9.setProcessName("RECEIPT EDIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PAYMENT EDIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PAYMENT EDIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0126" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PAYMENT EDIT");
			processMst9.setProcessName("PAYMENT EDIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("JOURNAL EDIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("JOURNAL EDIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0127" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("JOURNAL EDIT");
			processMst9.setProcessName("JOURNAL EDIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("OPENING BALANCE ENTRY");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("OPENING BALANCE ENTRY")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0128" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("OPENING BALANCE ENTRY");
			processMst9.setProcessName("OPENING BALANCE ENTRY");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ACCOUNT SL.NO UPDATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCOUNT SL.NO UPDATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0129" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ACCOUNT SL.NO UPDATION");
			processMst9.setProcessName("ACCOUNT SL.NO UPDATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("GROUP WISE SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("GROUP WISE SALES REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0130" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("GROUP WISE SALES REPORT");
			processMst9.setProcessName("GROUP WISE SALES REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("GROUP WISE SALES DETAIL REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("GROUP WISE SALES DETAIL REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0131" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("GROUP WISE SALES DETAIL REPORT");
			processMst9.setProcessName("GROUP WISE SALES DETAIL REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PRINTER");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("PRINTER")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0132" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PRINTER");
			processMst9.setProcessName("PRINTER");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("USER GROUP PERMISSION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("USER GROUP PERMISSION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0133" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("USER GROUP PERMISSION");
			processMst9.setProcessName("USER GROUP PERMISSION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("MULTI CATEGORY UPDATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("MULTI CATEGORY UPDATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0134" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("MULTI CATEGORY UPDATION");
			processMst9.setProcessName("MULTI CATEGORY UPDATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("MACHINE RESOUCE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("MACHINE RESOUCE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0135" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("MACHINE RESOUCE");
			processMst9.setProcessName("MACHINE RESOUCE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RESOUCE CATEGORY");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RESOUCE CATEGORY")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0136" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RESOUCE CATEGORY");
			processMst9.setProcessName("RESOUCE CATEGORY");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RESOUCE CATEGORY LINK");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RESOUCE CATEGORY LINK")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0137" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RESOUCE CATEGORY LINK");
			processMst9.setProcessName("RESOUCE CATEGORY LINK");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ADD ACCOUNT HEADS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ADD ACCOUNT HEADS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0138" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ADD ACCOUNT HEADS");
			processMst9.setProcessName("ADD ACCOUNT HEADS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("FINANCIAL YEAR");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("FINANCIAL YEAR")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0139" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("FINANCIAL YEAR");
			processMst9.setProcessName("FINANCIAL YEAR");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("DB INITIALIZE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DB INITIALIZE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0140" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("DB INITIALIZE");
			processMst9.setProcessName("DB INITIALIZE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("EXECUTE SQL");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("EXECUTE SQL")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0141" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("EXECUTE SQL");
			processMst9.setProcessName("EXECUTE SQL");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ITEM EDIT");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM EDIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0142" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ITEM EDIT");
			processMst9.setProcessName("ITEM EDIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("CATEGORY MANAGEMENT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CATEGORY MANAGEMENT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0143" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("CATEGORY MANAGEMENT");
			processMst9.setProcessName("CATEGORY MANAGEMENT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("CHECK PRINT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CHECK PRINT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0144" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("CHECK PRINT");
			processMst9.setProcessName("CHECK PRINT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ITEMWISE TAX CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEMWISE TAX CREATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0145" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ITEMWISE TAX CREATION");
			processMst9.setProcessName("ITEMWISE TAX CREATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PUBLISH MESSAGE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PUBLISH MESSAGE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0146" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PUBLISH MESSAGE");
			processMst9.setProcessName("PUBLISH MESSAGE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("STORE CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STORE CREATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0147" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("STORE CREATION");
			processMst9.setProcessName("STORE CREATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("DAY END");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("DAY END")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0148" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("DAY END");
			processMst9.setProcessName("DAY END");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SITE CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SITE CREATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0149" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SITE CREATION");
			processMst9.setProcessName("SITE CREATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("FINANCE CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("FINANCE CREATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0150" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("FINANCE CREATION");
			processMst9.setProcessName("FINANCE CREATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("BARCODE PRINTING");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("BARCODE PRINTING")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0151" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("BARCODE PRINTING");
			processMst9.setProcessName("BARCODE PRINTING");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("BARCODE CONFIGURATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("BARCODE CONFIGURATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0152" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("BARCODE CONFIGURATION");
			processMst9.setProcessName("BARCODE CONFIGURATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ITEM MERGE");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM MERGE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0153" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ITEM MERGE");
			processMst9.setProcessName("ITEM MERGE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SALE ORDER STATUS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALE ORDER STATUS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0154" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SALE ORDER STATUS");
			processMst9.setProcessName("SALE ORDER STATUS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RE-ORDER");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("RE-ORDER")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0155" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RE-ORDER");
			processMst9.setProcessName("RE-ORDER");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ITEM BRANCH");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM BRANCH")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0156" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ITEM BRANCH");
			processMst9.setProcessName("ITEM BRANCH");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("MULTY UNIT");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("MULTY UNIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0157" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("MULTY UNIT");
			processMst9.setProcessName("MULTY UNIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PRODUCT CONVERSION CONFIGURATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCT CONVERSION CONFIGURATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0158" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PRODUCT CONVERSION CONFIGURATION");
			processMst9.setProcessName("PRODUCT CONVERSION CONFIGURATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("CATEGORY EXCEPTION LIST");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CATEGORY EXCEPTION LIST")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0159" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("CATEGORY EXCEPTION LIST");
			processMst9.setProcessName("CATEGORY EXCEPTION LIST");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("NUTRITION FACTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("NUTRITION FACTS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0160" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("NUTRITION FACTS");
			processMst9.setProcessName("NUTRITION FACTS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("NUTRITION PRINT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("NUTRITION PRINT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0161" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("NUTRITION PRINT");
			processMst9.setProcessName("NUTRITION PRINT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("BARCODE NUTRRITION COMBINED");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("BARCODE NUTRRITION COMBINED")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0162" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("BARCODE NUTRRITION COMBINED");
			processMst9.setProcessName("BARCODE NUTRRITION COMBINED");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SALEORDER EDIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALEORDER EDIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0163" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SALEORDER EDIT");
			processMst9.setProcessName("SALEORDER EDIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("TAX INVOICE BY PRICE TYPE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("TAX INVOICE BY PRICE TYPE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0164" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("TAX INVOICE BY PRICE TYPE");
			processMst9.setProcessName("TAX INVOICE BY PRICE TYPE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("BATCH PRICE DEFINITION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("BATCH PRICE DEFINITION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0165" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("BATCH PRICE DEFINITION");
			processMst9.setProcessName("BATCH PRICE DEFINITION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("KITCHEN CATEGORY DTL");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("KITCHEN CATEGORY DTL")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0166" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("KITCHEN CATEGORY DTL");
			processMst9.setProcessName("KITCHEN CATEGORY DTL");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("KOT CATEGORY PRINTER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("KOT CATEGORY PRINTER")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0167" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("KOT CATEGORY PRINTER");
			processMst9.setProcessName("KOT CATEGORY PRINTER");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("CUSTOMERWISE SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CUSTOMERWISE SALES REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0168" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("CUSTOMERWISE SALES REPORT");
			processMst9.setProcessName("CUSTOMERWISE SALES REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("INSURANCE COMPANY SALES REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("INSURANCE COMPANY SALES REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0169" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("INSURANCE COMPANY SALES REPORT");
			processMst9.setProcessName("INSURANCE COMPANY SALES REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("DAILY ORDER DETAILS REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("DAILY ORDER DETAILS REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0170" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("DAILY ORDER DETAILS REPORT");
			processMst9.setProcessName("DAILY ORDER DETAILS REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("STOCK TRANSFER OUT DETAIL REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK TRANSFER OUT DETAIL REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0171" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("STOCK TRANSFER OUT DETAIL REPORT");
			processMst9.setProcessName("STOCK TRANSFER OUT DETAIL REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("STOCK TRANSFER IN DETAIL REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK TRANSFER IN DETAIL REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0172" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("STOCK TRANSFER IN DETAIL REPORT");
			processMst9.setProcessName("STOCK TRANSFER IN DETAIL REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PURCHASE SUMMARY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PURCHASE SUMMARY REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0173" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PURCHASE SUMMARY REPORT");
			processMst9.setProcessName("PURCHASE SUMMARY REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("HSNCODE WISE PURCHASE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("HSNCODE WISE PURCHASE REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0174" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("HSNCODE WISE PURCHASE REPORT");
			processMst9.setProcessName("HSNCODE WISE PURCHASE REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PHARMACY PURCHASE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PHARMACY PURCHASE REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0175" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PHARMACY PURCHASE REPORT");
			processMst9.setProcessName("PHARMACY PURCHASE REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SUPPLIERWISE PURCHASE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SUPPLIERWISE PURCHASE REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0176" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SUPPLIERWISE PURCHASE REPORT");
			processMst9.setProcessName("SUPPLIERWISE PURCHASE REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RAW MATERIAL ITEMWISE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RAW MATERIAL ITEMWISE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0177" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RAW MATERIAL ITEMWISE");
			processMst9.setProcessName("RAW MATERIAL ITEMWISE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ITEM WISE INTENT REPORT SUMMARY");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM WISE INTENT REPORT SUMMARY")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0178" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ITEM WISE INTENT REPORT SUMMARY");
			processMst9.setProcessName("ITEM WISE INTENT REPORT SUMMARY");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("INCOME AND EXPENCE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("INCOME AND EXPENCE REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0179" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("INCOME AND EXPENCE REPORT");
			processMst9.setProcessName("INCOME AND EXPENCE REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PRODUCTION PRE PLANNING");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCTION PRE PLANNING")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0180" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PRODUCTION PRE PLANNING");
			processMst9.setProcessName("PRODUCTION PRE PLANNING");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RAW MATERIAL ISSUE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RAW MATERIAL ISSUE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0181" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RAW MATERIAL ISSUE");
			processMst9.setProcessName("RAW MATERIAL ISSUE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RAW MATERIAL RETURN");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RAW MATERIAL RETURN")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0182" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RAW MATERIAL RETURN");
			processMst9.setProcessName("RAW MATERIAL RETURN");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("CUSTOMER BILLWISE ADJUSTMENT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CUSTOMER BILLWISE ADJUSTMENT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0183" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("CUSTOMER BILLWISE ADJUSTMENT");
			processMst9.setProcessName("CUSTOMER BILLWISE ADJUSTMENT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("OPENING BALANCE CONFIGURATIONS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("OPENING BALANCE CONFIGURATIONS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0184" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("OPENING BALANCE CONFIGURATIONS");
			processMst9.setProcessName("OPENING BALANCE CONFIGURATIONS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SALES MAN CREATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES MAN CREATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0185" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SALES MAN CREATION");
			processMst9.setProcessName("SALES MAN CREATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SUB BRANCH SALE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SUB BRANCH SALE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0186" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SUB BRANCH SALE");
			processMst9.setProcessName("SUB BRANCH SALE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("VOUCHER DATE EDIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("VOUCHER DATE EDIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0187" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("VOUCHER DATE EDIT");
			processMst9.setProcessName("VOUCHER DATE EDIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RETRY");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("RETRY")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0188" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RETRY");
			processMst9.setProcessName("RETRY");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("STOCK CORRECTION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK CORRECTION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0189" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("STOCK CORRECTION");
			processMst9.setProcessName("STOCK CORRECTION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("TALLY INTEGRATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("TALLY INTEGRATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0190" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("TALLY INTEGRATION");
			processMst9.setProcessName("TALLY INTEGRATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SET AN IMAGE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SET AN IMAGE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0191" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SET AN IMAGE");
			processMst9.setProcessName("SET AN IMAGE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PRODUCTION QTY GRAPH");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCTION QTY GRAPH")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0192" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PRODUCTION QTY GRAPH");
			processMst9.setProcessName("PRODUCTION QTY GRAPH");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PRODUCTION VALUE GRAPH");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCTION VALUE GRAPH")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0193" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PRODUCTION VALUE GRAPH");
			processMst9.setProcessName("PRODUCTION VALUE GRAPH");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("BAR GRAPH");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("BAR GRAPH")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0194" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("BAR GRAPH");
			processMst9.setProcessName("BAR GRAPH");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ITEM IN LANGUAGE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM IN LANGUAGE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0195" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ITEM IN LANGUAGE");
			processMst9.setProcessName("ITEM IN LANGUAGE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("LANGUAGE MST");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("LANGUAGE MST")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0196" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("LANGUAGE MST");
			processMst9.setProcessName("LANGUAGE MST");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("LMS_QUEUE PENDING TASK");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("LMS_QUEUE PENDING TASK")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0197" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("LMS_QUEUE PENDING TASK");
			processMst9.setProcessName("LMS_QUEUE PENDING TASK");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("INTENT TO STOCK TRANSFER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("INTENT TO STOCK TRANSFER")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0198" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("INTENT TO STOCK TRANSFER");
			processMst9.setProcessName("INTENT TO STOCK TRANSFER");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SALES PROPERTIES CONFIGURATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES PROPERTIES CONFIGURATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0199" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SALES PROPERTIES CONFIGURATION");
			processMst9.setProcessName("SALES PROPERTIES CONFIGURATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PURCHASE SCHEME");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PURCHASE SCHEME")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0200" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PURCHASE SCHEME");
			processMst9.setProcessName("PURCHASE SCHEME");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("STOCK VERIFICATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK VERIFICATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0201" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("STOCK VERIFICATION");
			processMst9.setProcessName("STOCK VERIFICATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ITEM LOCATION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ITEM LOCATION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0202" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ITEM LOCATION");
			processMst9.setProcessName("ITEM LOCATION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("STORE CHANGE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STORE CHANGE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0203" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("STORE CHANGE");
			processMst9.setProcessName("STORE CHANGE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("USER REGISTERED STOCK");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("USER REGISTERED STOCK")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0204" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("USER REGISTERED STOCK");
			processMst9.setProcessName("USER REGISTERED STOCK");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("OTHER BRANCH SALE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("OTHER BRANCH SALE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0205" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("OTHER BRANCH SALE");
			processMst9.setProcessName("OTHER BRANCH SALE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PURCHASE ORDER MANAGEMENT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PURCHASE ORDER MANAGEMENT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0206" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PURCHASE ORDER MANAGEMENT");
			processMst9.setProcessName("PURCHASE ORDER MANAGEMENT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PURCHASE ORDER");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PURCHASE ORDER")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0207" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PURCHASE ORDER");
			processMst9.setProcessName("PURCHASE ORDER");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PHYSICAL STOCK");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PHYSICAL STOCK")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0208" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PHYSICAL STOCK");
			processMst9.setProcessName("PHYSICAL STOCK");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("CONSUMPTION REASON");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CONSUMPTION REASON")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0209" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("CONSUMPTION REASON");
			processMst9.setProcessName("CONSUMPTION REASON");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ACCOUNT MERGE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCOUNT MERGE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0210" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ACCOUNT MERGE");
			processMst9.setProcessName("ACCOUNT MERGE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("INHOUSE CONSUMPTION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("INHOUSE CONSUMPTION REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0211" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("INHOUSE CONSUMPTION REPORT");
			processMst9.setProcessName("INHOUSE CONSUMPTION REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PHARMACY ITEM OR CATEGORY MOVEMENT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PHARMACY ITEM OR CATEGORY MOVEMENT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0212" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PHARMACY ITEM OR CATEGORY MOVEMENT");
			processMst9.setProcessName("PHARMACY ITEM OR CATEGORY MOVEMENT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("FAST MOVING ITEMS REPORT BY SALES MODE");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName()
				.equalsIgnoreCase("FAST MOVING ITEMS REPORT BY SALES MODE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0213" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("FAST MOVING ITEMS REPORT BY SALES MODE");
			processMst9.setProcessName("FAST MOVING ITEMS REPORT BY SALES MODE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("MOBILE STOCK REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("MOBILE STOCK REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0214" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("MOBILE STOCK REPORT");
			processMst9.setProcessName("MOBILE STOCK REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("INTENT REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("INTENT REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0215" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("INTENT REPORT");
			processMst9.setProcessName("INTENT REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("BATCHWISE INVENTORY REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("BATCHWISE INVENTORY REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0216" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("BATCHWISE INVENTORY REPORT");
			processMst9.setProcessName("BATCHWISE INVENTORY REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PRODUCTION REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCTION REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0216" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PRODUCTION REPORT");
			processMst9.setProcessName("PRODUCTION REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RECEIPT REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("RECEIPT REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0217" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RECEIPT REPORT");
			processMst9.setProcessName("RECEIPT REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("REPRINT");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("REPRINT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0218" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("REPRINT");
			processMst9.setProcessName("REPRINT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("STOCK TRANSFER REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("STOCK TRANSFER REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0219" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("STOCK TRANSFER REPORT");
			processMst9.setProcessName("STOCK TRANSFER REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PHARMACY SALES RETURN REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("PHARMACY SALES RETURN REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0220" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PHARMACY SALES RETURN REPORT");
			processMst9.setProcessName("PHARMACY SALES RETURN REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ACCOUNTING REPORTS");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ACCOUNTING REPORTS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0221" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ACCOUNTING REPORTS");
			processMst9.setProcessName("ACCOUNTING REPORTS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("MASTERS");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("MASTERS")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0222" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("MASTERS");
			processMst9.setProcessName("MASTERS");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("WEIGHING MACHINE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("WEIGHING MACHINE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0223" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("WEIGHING MACHINE");
			processMst9.setProcessName("WEIGHING MACHINE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SALES ORDER EDIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SALES ORDER EDIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0224" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SALES ORDER EDIT");
			processMst9.setProcessName("SALES ORDER EDIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("INVOICE EDIT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("INVOICE EDIT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0225" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("INVOICE EDIT");
			processMst9.setProcessName("INVOICE EDIT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PRODUCTION");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("PRODUCTION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0226" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PRODUCTION");
			processMst9.setProcessName("PRODUCTION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("TAKE ORDER");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("TAKE ORDER")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0227" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("TAKE ORDER");
			processMst9.setProcessName("TAKE ORDER");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("RETAIL");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("RETAIL")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0228" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("RETAIL");
			processMst9.setProcessName("RETAIL");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SET ITEM IMAGE");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SET ITEM IMAGE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0229" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SET ITEM IMAGE");
			processMst9.setProcessName("SET ITEM IMAGE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("OCCUPIED TABLES");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("OCCUPIED TABLES")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0230" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("OCCUPIED TABLES");
			processMst9.setProcessName("OCCUPIED TABLES");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("CONSUMPTION");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("CONSUMPTION")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0231" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("CONSUMPTION");
			processMst9.setProcessName("CONSUMPTION");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("PHARMACY");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("PHARMACY")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0232" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("PHARMACY");
			processMst9.setProcessName("PHARMACY");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("ADVANCED FEATURES");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("ADVANCED FEATURES")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0233" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("ADVANCED FEATURES");
			processMst9.setProcessName("ADVANCED FEATURES");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SERVICE REPORT");
		if (null == processSaved.getBody()
				|| !processSaved.getBody().getProcessName().equalsIgnoreCase("SERVICE REPORT")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0234" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SERVICE REPORT");
			processMst9.setProcessName("SERVICE REPORT");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("SERVICE");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("SERVICE")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0235" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("SERVICE");
			processMst9.setProcessName("SERVICE");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("INVENTORY");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("INVENTORY")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0236" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("INVENTORY");
			processMst9.setProcessName("INVENTORY");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}

		processSaved = RestCaller.getProcessDtls("INVENTORY");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("INVENTORY")) {

			ProcessMst processMst9 = new ProcessMst();
			processMst9.setId("PRC0236" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst9.setProcessDescription("INVENTORY");
			processMst9.setProcessName("INVENTORY");
			processMst9.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst9);
		}
		
		processSaved = RestCaller.getProcessDtls("PARTY CREATION");
		if (null == processSaved.getBody() || !processSaved.getBody().getProcessName().equalsIgnoreCase("PARTY CREATION")) {

			ProcessMst processMst10 = new ProcessMst();
			processMst10.setId("PRC0237" + SystemSetting.getUser().getCompanyMst().getCompanyName());
			processMst10.setProcessDescription("PARTY CREATION");
			processMst10.setProcessName("PARTY CREATION");
			processMst10.setProcessType("WINDOW");

			RestCaller.saveProcessMst(processMst10);
		}

//-----------------------------------------------------version 2.0.1-----------------------------------------------------------

		// -------------------- Save new Category

		ResponseEntity<CategoryMst> categormstresp = RestCaller.getCategoryByName("PACKING MATERIALS");
		if (null == categormstresp.getBody()) {
			CategoryMst categoryMst = new CategoryMst();

			categoryMst.setCategoryName("PACKING MATERIALS");
			String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "CAT");
			categoryMst.setId(vNo + SystemSetting.getUser().getCompanyMst().getId());
			ResponseEntity<CategoryMst> respentity = RestCaller.saveCategory(categoryMst);
			categoryMst = respentity.getBody();
		}

//----------------------------------------------------------------------------------------------------------------------

//----------------------------------------Sceme offer Def--------------------------------------------------------	
		ResponseEntity<SchOfferDef> schOfferDefResp = RestCaller.getSchemeOfferDef("AMOUNT DISCOUNT");
		SchOfferDef schOfferDef = schOfferDefResp.getBody();
		if (null == schOfferDef) {
			schOfferDef = new SchOfferDef();
			schOfferDef.setId("1");
			schOfferDef.setBranchCode(SystemSetting.systemBranch);
			schOfferDef.setOfferType("AMOUNT DISCOUNT");
			ResponseEntity<SchOfferDef> schOfferDefSaved = RestCaller.saveSchOfferDef(schOfferDef);
		}
		ResponseEntity<SchOfferDef> schOfferDefResp2 = RestCaller.getSchemeOfferDef("FREE QUANTITY");
		SchOfferDef schOfferDef2 = schOfferDefResp2.getBody();
		if (null == schOfferDef2) {
			schOfferDef = new SchOfferDef();
			schOfferDef.setId("2");
			schOfferDef.setBranchCode(SystemSetting.systemBranch);
			schOfferDef.setOfferType("FREE QUANTITY");
			ResponseEntity<SchOfferDef> schOfferDefSaved = RestCaller.saveSchOfferDef(schOfferDef);
		}

		ResponseEntity<SchOfferDef> schOfferDefResp4 = RestCaller.getSchemeOfferDef("PERCENTAGE DISCOUNT");
		SchOfferDef schOfferDef4 = schOfferDefResp4.getBody();
		if (null == schOfferDef4) {
			schOfferDef = new SchOfferDef();
			schOfferDef.setId("4");
			schOfferDef.setBranchCode(SystemSetting.systemBranch);
			schOfferDef.setOfferType("PERCENTAGE DISCOUNT");
			ResponseEntity<SchOfferDef> schOfferDefSaved = RestCaller.saveSchOfferDef(schOfferDef);
		}
//----------------------------------Scheme offer Attribute ----------------------------------------	

		ResponseEntity<SchOfferAttrListDef> schOfferAttrListDefResp = RestCaller
				.getSchemeOfferAttributeListDefByName("DISCOUNT");
		SchOfferAttrListDef schOfferAttrListDef = schOfferAttrListDefResp.getBody();
		if (null == schOfferAttrListDef) {
			schOfferAttrListDef = new SchOfferAttrListDef();
			schOfferAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "1");
			schOfferAttrListDef.setOfferId("1");
			schOfferAttrListDef.setBranchCode(SystemSetting.systemBranch);
			schOfferAttrListDef.setAttribName("DISCOUNT");
			schOfferAttrListDef.setAttribType("DOUBLE");
			ResponseEntity<SchOfferAttrListDef> schOfferDefSaved = RestCaller
					.saveSchOfferAttrListDef(schOfferAttrListDef);
		}
		ResponseEntity<SchOfferAttrListDef> schOfferAttrListDefResp1 = RestCaller
				.getSchemeOfferAttributeListDefByName("ITEMNAME");
		SchOfferAttrListDef schOfferAttrListDef1 = schOfferAttrListDefResp1.getBody();
		if (null == schOfferAttrListDef1) {
			schOfferAttrListDef = new SchOfferAttrListDef();
			schOfferAttrListDef.setOfferId("2");
			schOfferAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "2");

			schOfferAttrListDef.setBranchCode(SystemSetting.systemBranch);

			schOfferAttrListDef.setAttribName("ITEMNAME");
			schOfferAttrListDef.setAttribType("STRING");
			ResponseEntity<SchOfferAttrListDef> schOfferDefSaved = RestCaller
					.saveSchOfferAttrListDef(schOfferAttrListDef);
		}
		ResponseEntity<SchOfferAttrListDef> schOfferAttrListDefResp2 = RestCaller
				.getSchemeOfferAttributeListDefByName("QTY");
		SchOfferAttrListDef schOfferAttrListDef2 = schOfferAttrListDefResp2.getBody();
		if (null == schOfferAttrListDef2) {
			schOfferAttrListDef = new SchOfferAttrListDef();
			schOfferAttrListDef.setOfferId("2");
			schOfferAttrListDef.setBranchCode(SystemSetting.systemBranch);
			schOfferAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "3");

			schOfferAttrListDef.setAttribName("QTY");
			schOfferAttrListDef.setAttribType("DOUBLE");
			ResponseEntity<SchOfferAttrListDef> schOfferDefSaved = RestCaller
					.saveSchOfferAttrListDef(schOfferAttrListDef);
		}

		ResponseEntity<SchOfferAttrListDef> schOfferAttrListDefResp3 = RestCaller
				.getSchemeOfferAttributeListDefByName("BATCH_CODE");
		SchOfferAttrListDef schOfferAttrListDef3 = schOfferAttrListDefResp3.getBody();
		if (null == schOfferAttrListDef3) {
			schOfferAttrListDef = new SchOfferAttrListDef();
			schOfferAttrListDef.setOfferId("2");
			schOfferAttrListDef.setBranchCode(SystemSetting.systemBranch);
			schOfferAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "4");

			schOfferAttrListDef.setAttribName("BATCH_CODE");
			schOfferAttrListDef.setAttribType("STRING");
			ResponseEntity<SchOfferAttrListDef> schOfferDefSaved = RestCaller
					.saveSchOfferAttrListDef(schOfferAttrListDef);
		}

		ResponseEntity<SchOfferAttrListDef> schOfferAttrListDefResp4 = RestCaller
				.getSchemeOfferAttributeListDefByName("MULTIPLE ALLOWED");
		SchOfferAttrListDef schOfferAttrListDef4 = schOfferAttrListDefResp4.getBody();
		if (null == schOfferAttrListDef4) {
			schOfferAttrListDef = new SchOfferAttrListDef();
			schOfferAttrListDef.setOfferId("2");
			schOfferAttrListDef.setBranchCode(SystemSetting.systemBranch);
			schOfferAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "5");

			schOfferAttrListDef.setAttribName("MULTIPLE ALLOWED");
			schOfferAttrListDef.setAttribType("BOOLEAN");
			ResponseEntity<SchOfferAttrListDef> schOfferDefSaved = RestCaller
					.saveSchOfferAttrListDef(schOfferAttrListDef);
		}

		ResponseEntity<SchOfferAttrListDef> schOfferAttrListDefResp6 = RestCaller
				.getSchemeOfferAttributeListDefByName("PERCENTAGE DISCOUNT");
		SchOfferAttrListDef schOfferAttrListDef6 = schOfferAttrListDefResp6.getBody();
		if (null == schOfferAttrListDef6) {
			schOfferAttrListDef = new SchOfferAttrListDef();
			schOfferAttrListDef.setOfferId("4");
			schOfferAttrListDef.setBranchCode(SystemSetting.systemBranch);
			schOfferAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "6");

			schOfferAttrListDef.setAttribName("PERCENTAGE DISCOUNT");
			schOfferAttrListDef.setAttribType("DOUBLE");
			ResponseEntity<SchOfferAttrListDef> schOfferDefSaved = RestCaller
					.saveSchOfferAttrListDef(schOfferAttrListDef);
		}
//--------------------------------Scheme Selection Def--------------------------------------------
		ResponseEntity<SchSelectDef> SchSelectDefResp = RestCaller.getSchSelectDefbyName("FOR A PERIOD");
		SchSelectDef schSelectDef = SchSelectDefResp.getBody();
		if (null == schSelectDef) {
			schSelectDef = new SchSelectDef();
			schSelectDef.setId("1");
			schSelectDef.setBranchCode(SystemSetting.systemBranch);
			schOfferAttrListDef.setBranchCode(SystemSetting.systemBranch);

			schSelectDef.setSelectionName("FOR A PERIOD");
			ResponseEntity<SchSelectDef> schSelectDefSaved = RestCaller.saveSchSelectDef(schSelectDef);
		}

		ResponseEntity<SchSelectDef> SchSelectDefResp1 = RestCaller.getSchSelectDefbyName("FOR A SPECIFIC DAY OF WEEK");
		SchSelectDef schSelectDef1 = SchSelectDefResp1.getBody();
		if (null == schSelectDef1) {
			schSelectDef = new SchSelectDef();
			schSelectDef.setId("2");
			schSelectDef.setBranchCode(SystemSetting.systemBranch);
			schSelectDef.setSelectionName("FOR A SPECIFIC DAY OF WEEK");
			ResponseEntity<SchSelectDef> schSelectDefSaved = RestCaller.saveSchSelectDef(schSelectDef);
		}
		ResponseEntity<SchSelectDef> SchSelectDefResp2 = RestCaller.getSchSelectDefbyName("FOR A SPECIFIC MONTH");
		SchSelectDef schSelectDef2 = SchSelectDefResp2.getBody();
		if (null == schSelectDef2) {
			schSelectDef = new SchSelectDef();
			schSelectDef.setId("3");
			schSelectDef.setBranchCode(SystemSetting.systemBranch);
			schSelectDef.setSelectionName("FOR A SPECIFIC MONTH");
			ResponseEntity<SchSelectDef> schSelectDefSaved = RestCaller.saveSchSelectDef(schSelectDef);
		}
//----------------------------------Scheme Selection Attribute Def----------------------------------

		ResponseEntity<SchSelectAttrListDef> SchSelectAttrListDefResp = RestCaller
				.getSchSelectionAttributeNameByName("START DATE");
		SchSelectAttrListDef schSelectAttrListDef = SchSelectAttrListDefResp.getBody();
		if (null == schSelectAttrListDef) {
			schSelectAttrListDef = new SchSelectAttrListDef();
			schSelectAttrListDef.setSelectionId("1");
			schSelectAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "1");
			schSelectAttrListDef.setBranchCode(SystemSetting.systemBranch);
			schSelectAttrListDef.setAttrib_type("DATE");
			schSelectAttrListDef.setAttribName("START DATE");

			ResponseEntity<SchSelectAttrListDef> schSelectDefSaved = RestCaller
					.saveSchSelectionAtrribute(schSelectAttrListDef);
		}

		ResponseEntity<SchSelectAttrListDef> SchSelectAttrListDefResp1 = RestCaller
				.getSchSelectionAttributeNameByName("END DATE");
		SchSelectAttrListDef schSelectAttrListDef1 = SchSelectAttrListDefResp1.getBody();
		if (null == schSelectAttrListDef1) {
			schSelectAttrListDef = new SchSelectAttrListDef();
			schSelectAttrListDef.setSelectionId("1");
			schSelectAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "2");

			schSelectAttrListDef.setBranchCode(SystemSetting.systemBranch);

			schSelectAttrListDef.setAttrib_type("DATE");
			schSelectAttrListDef.setAttribName("END DATE");

			ResponseEntity<SchSelectAttrListDef> schSelectDefSaved = RestCaller
					.saveSchSelectionAtrribute(schSelectAttrListDef);
		}

		ResponseEntity<SchSelectAttrListDef> SchSelectAttrListDefResp2 = RestCaller
				.getSchSelectionAttributeNameByName("DAY OF WEEK");
		SchSelectAttrListDef schSelectAttrListDef2 = SchSelectAttrListDefResp2.getBody();
		if (null == schSelectAttrListDef2) {
			schSelectAttrListDef = new SchSelectAttrListDef();
			schSelectAttrListDef.setSelectionId("2");
			schSelectAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "3");

			schSelectAttrListDef.setBranchCode(SystemSetting.systemBranch);

			schSelectAttrListDef.setAttrib_type("DAY");
			schSelectAttrListDef.setAttribName("DAY OF WEEK");

			ResponseEntity<SchSelectAttrListDef> schSelectDefSaved = RestCaller
					.saveSchSelectionAtrribute(schSelectAttrListDef);
		}

		ResponseEntity<SchSelectAttrListDef> SchSelectAttrListDefResp3 = RestCaller
				.getSchSelectionAttributeNameByName("MONTH NAME");
		SchSelectAttrListDef schSelectAttrListDef3 = SchSelectAttrListDefResp3.getBody();
		if (null == schSelectAttrListDef3) {
			schSelectAttrListDef = new SchSelectAttrListDef();
			schSelectAttrListDef.setSelectionId("3");
			schSelectAttrListDef.setBranchCode(SystemSetting.systemBranch);
			schSelectAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "4");

			schSelectAttrListDef.setAttrib_type("MONTH");
			schSelectAttrListDef.setAttribName("MONTH NAME");

			ResponseEntity<SchSelectAttrListDef> schSelectDefSaved = RestCaller
					.saveSchSelectionAtrribute(schSelectAttrListDef);
		}
//------------------------------------Scheme eligibility def---------------------------------------------	

		ResponseEntity<SchEligibilityDef> SchEligibilityDefResp = RestCaller
				.getSchemeEligibilityDef("FOR A SPECIFIC QTY OF ITEM NAME");
		SchEligibilityDef schEligibilityDef = SchEligibilityDefResp.getBody();
		if (null == schEligibilityDef) {
			schEligibilityDef = new SchEligibilityDef();
			schEligibilityDef.setId("1");
			schEligibilityDef.setBranchCode(SystemSetting.systemBranch);
			schEligibilityDef.setEligibilityName("FOR A SPECIFIC QTY OF ITEM NAME");

			ResponseEntity<SchEligibilityDef> schSelectDefSaved = RestCaller.saveSchEligibilityDef(schEligibilityDef);
		}

		ResponseEntity<SchEligibilityDef> SchEligibilityDefResp1 = RestCaller
				.getSchemeEligibilityDef("FOR A SPECIFIC QTY OF CATEGORY");
		SchEligibilityDef schEligibilityDef1 = SchEligibilityDefResp1.getBody();
		if (null == schEligibilityDef1) {
			schEligibilityDef = new SchEligibilityDef();
			schEligibilityDef.setId("2");

			schEligibilityDef.setBranchCode(SystemSetting.systemBranch);
			schEligibilityDef.setEligibilityName("FOR A SPECIFIC QTY OF CATEGORY");

			ResponseEntity<SchEligibilityDef> schSelectDefSaved = RestCaller.saveSchEligibilityDef(schEligibilityDef);
		}

		ResponseEntity<SchEligibilityDef> SchEligibilityDefResp3 = RestCaller
				.getSchemeEligibilityDef("FOR A SPECIFIC AMOUNT OF ITEM NAME");
		SchEligibilityDef schEligibilityDef3 = SchEligibilityDefResp3.getBody();
		if (null == schEligibilityDef3) {
			schEligibilityDef = new SchEligibilityDef();
			schEligibilityDef.setId("4");
			schEligibilityDef.setBranchCode(SystemSetting.systemBranch);
			schEligibilityDef.setEligibilityName("FOR A SPECIFIC AMOUNT OF ITEM NAME");

			ResponseEntity<SchEligibilityDef> schSelectDefSaved = RestCaller.saveSchEligibilityDef(schEligibilityDef);
		}

		ResponseEntity<SchEligibilityDef> SchEligibilityDefResp4 = RestCaller
				.getSchemeEligibilityDef("FOR A SPECIFIC AMOUNT OF CATEGORY");
		SchEligibilityDef schEligibilityDef4 = SchEligibilityDefResp4.getBody();
		if (null == schEligibilityDef4) {
			schEligibilityDef = new SchEligibilityDef();
			schEligibilityDef.setId("5");

			schEligibilityDef.setBranchCode(SystemSetting.systemBranch);
			schEligibilityDef.setEligibilityName("FOR A SPECIFIC AMOUNT OF CATEGORY");

			ResponseEntity<SchEligibilityDef> schSelectDefSaved = RestCaller.saveSchEligibilityDef(schEligibilityDef);
		}

		ResponseEntity<SchEligibilityDef> SchEligibilityDefResp6 = RestCaller
				.getSchemeEligibilityDef("FOR A SPECIFIC AMOUNT OF INVOICE");
		SchEligibilityDef schEligibilityDef6 = SchEligibilityDefResp6.getBody();
		if (null == schEligibilityDef6) {
			schEligibilityDef = new SchEligibilityDef();
			schEligibilityDef.setId("7");

			schEligibilityDef.setBranchCode(SystemSetting.systemBranch);
			schEligibilityDef.setEligibilityName("FOR A SPECIFIC AMOUNT OF INVOICE");

			ResponseEntity<SchEligibilityDef> schSelectDefSaved = RestCaller.saveSchEligibilityDef(schEligibilityDef);
		}

		ResponseEntity<SchEligibilityDef> SchEligibilityDefResp7 = RestCaller
				.getSchemeEligibilityDef("FOR A SPECIFIC QTY OF ITEM AND BATCH");
		SchEligibilityDef schEligibilityDef7 = SchEligibilityDefResp7.getBody();
		if (null == schEligibilityDef7) {
			schEligibilityDef = new SchEligibilityDef();
			schEligibilityDef.setId("8");
			schEligibilityDef.setBranchCode(SystemSetting.systemBranch);
			schEligibilityDef.setEligibilityName("FOR A SPECIFIC QTY OF ITEM AND BATCH");

			ResponseEntity<SchEligibilityDef> schSelectDefSaved = RestCaller.saveSchEligibilityDef(schEligibilityDef);
		}
//----------------------------------------------Scheme eligibility attribute----------------------------

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp = RestCaller
				.getSchEligibilityAttrListDefByName("ITEMNAME", "1");
		SchEligiAttrListDef SchEligiAttrListDef = SchEligiAttrListDefResp.getBody();
		if (null == SchEligiAttrListDef) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("ITEMNAME");
			SchEligiAttrListDef.setAttribType("STRING");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "1");

			SchEligiAttrListDef.setEligibilityId("1");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp1 = RestCaller
				.getSchEligibilityAttrListDefByName("QTY", "1");
		SchEligiAttrListDef SchEligiAttrListDef1 = SchEligiAttrListDefResp1.getBody();
		if (null == SchEligiAttrListDef1) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("QTY");
			SchEligiAttrListDef.setAttribType("DOUBLE");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "2");

			SchEligiAttrListDef.setEligibilityId("1");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp3 = RestCaller
				.getSchEligibilityAttrListDefByName("CATEGORY", "2");
		SchEligiAttrListDef SchEligiAttrListDef3 = SchEligiAttrListDefResp3.getBody();
		if (null == SchEligiAttrListDef3) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("CATEGORY");
			SchEligiAttrListDef.setAttribType("STRING");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "3");

			SchEligiAttrListDef.setEligibilityId("2");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp2 = RestCaller
				.getSchEligibilityAttrListDefByName("QTY", "2");
		SchEligiAttrListDef SchEligiAttrListDef2 = SchEligiAttrListDefResp2.getBody();
		if (null == SchEligiAttrListDef2) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("QTY");
			SchEligiAttrListDef.setAttribType("DOUBLE");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "4");

			SchEligiAttrListDef.setEligibilityId("2");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp6 = RestCaller
				.getSchEligibilityAttrListDefByName("ITEMNAME", "4");
		SchEligiAttrListDef SchEligiAttrListDef6 = SchEligiAttrListDefResp6.getBody();
		if (null == SchEligiAttrListDef6) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("ITEMNAME");
			SchEligiAttrListDef.setAttribType("STRING");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "5");

			SchEligiAttrListDef.setEligibilityId("4");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp7 = RestCaller
				.getSchEligibilityAttrListDefByName("AMOUNT", "4");
		SchEligiAttrListDef SchEligiAttrListDef7 = SchEligiAttrListDefResp7.getBody();
		if (null == SchEligiAttrListDef7) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("AMOUNT");
			SchEligiAttrListDef.setAttribType("DOUBLE");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "6");

			SchEligiAttrListDef.setEligibilityId("4");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp8 = RestCaller
				.getSchEligibilityAttrListDefByName("CATEGORY", "5");
		SchEligiAttrListDef SchEligiAttrListDef8 = SchEligiAttrListDefResp8.getBody();
		if (null == SchEligiAttrListDef8) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("CATEGORY");
			SchEligiAttrListDef.setAttribType("STRING");
			SchEligiAttrListDef.setEligibilityId("5");
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "7");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp9 = RestCaller
				.getSchEligibilityAttrListDefByName("AMOUNT", "5");
		SchEligiAttrListDef SchEligiAttrListDef9 = SchEligiAttrListDefResp9.getBody();
		if (null == SchEligiAttrListDef9) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("AMOUNT");
			SchEligiAttrListDef.setAttribType("DOUBLE");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "8");

			SchEligiAttrListDef.setEligibilityId("5");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp12 = RestCaller
				.getSchEligibilityAttrListDefByName("AMOUNT", "7");
		SchEligiAttrListDef SchEligiAttrListDef12 = SchEligiAttrListDefResp12.getBody();
		if (null == SchEligiAttrListDef12) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("AMOUNT");
			SchEligiAttrListDef.setAttribType("DOUBLE");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "9");

			SchEligiAttrListDef.setEligibilityId("7");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp13 = RestCaller
				.getSchEligibilityAttrListDefByName("ITEMNAME", "8");
		SchEligiAttrListDef SchEligiAttrListDef13 = SchEligiAttrListDefResp13.getBody();
		if (null == SchEligiAttrListDef13) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("ITEMNAME");
			SchEligiAttrListDef.setAttribType("STRING");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "10");

			SchEligiAttrListDef.setEligibilityId("8");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp15 = RestCaller
				.getSchEligibilityAttrListDefByName("QTY", "8");
		SchEligiAttrListDef SchEligiAttrListDef15 = SchEligiAttrListDefResp15.getBody();
		if (null == SchEligiAttrListDef15) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("QTY");
			SchEligiAttrListDef.setAttribType("DOUBLE");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "1");

			SchEligiAttrListDef.setEligibilityId("8");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp14 = RestCaller
				.getSchEligibilityAttrListDefByName("BATCH_CODE", "8");
		SchEligiAttrListDef SchEligiAttrListDef14 = SchEligiAttrListDefResp14.getBody();
		if (null == SchEligiAttrListDef14) {
			SchEligiAttrListDef = new SchEligiAttrListDef();
			SchEligiAttrListDef.setAttribName("BATCH_CODE");
			SchEligiAttrListDef.setBranchCode(SystemSetting.systemBranch);
			SchEligiAttrListDef.setId(SystemSetting.getUser().getCompanyMst().getId() + "12");

			SchEligiAttrListDef.setAttribType("STRING");
			SchEligiAttrListDef.setEligibilityId("8");

			ResponseEntity<SchEligiAttrListDef> schSelectDefSaved = RestCaller
					.saveSchEligibilityAttrDef(SchEligiAttrListDef);
		}

		// ------------------------new version 1.9 surya

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstCashResp = RestCaller.getReceiptModeMstByName("CASH");

		List<ReceiptModeMst> receiptModeMstCashrList = receiptModeMstCashResp.getBody();
		if (receiptModeMstCashrList.size() == 0) {
			ReceiptModeMst receiptModeMst = new ReceiptModeMst();

			receiptModeMst.setReceiptMode("CASH");
			receiptModeMst.setCreditCardStatus("NO");
			receiptModeMst.setStatus("ACTIVE");

			ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

		}

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstTransferResp = RestCaller
				.getReceiptModeMstByName("TRASNFER");

		List<ReceiptModeMst> receiptModeMstTransferList = receiptModeMstTransferResp.getBody();
		if (receiptModeMstTransferList.size() == 0) {
			ReceiptModeMst receiptModeMst = new ReceiptModeMst();

			receiptModeMst.setReceiptMode("TRASNFER");
			receiptModeMst.setCreditCardStatus("NO");
			receiptModeMst.setStatus("ACTIVE");

			ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

		}

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstChequeResp = RestCaller.getReceiptModeMstByName("CHEQUE");

		List<ReceiptModeMst> receiptModeMstChequeList = receiptModeMstChequeResp.getBody();
		if (receiptModeMstChequeList.size() == 0) {
			ReceiptModeMst receiptModeMst = new ReceiptModeMst();

			receiptModeMst.setReceiptMode("CHEQUE");
			receiptModeMst.setCreditCardStatus("NO");
			receiptModeMst.setStatus("ACTIVE");

			ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

		}

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstDDResp = RestCaller.getReceiptModeMstByName("DD");

		List<ReceiptModeMst> receiptModeMstDDList = receiptModeMstDDResp.getBody();
		if (receiptModeMstDDList.size() == 0) {
			ReceiptModeMst receiptModeMst = new ReceiptModeMst();

			receiptModeMst.setReceiptMode("DD");
			receiptModeMst.setCreditCardStatus("NO");
			receiptModeMst.setStatus("ACTIVE");

			ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

		}

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstMobilePayResp = RestCaller
				.getReceiptModeMstByName("MOBILE PAY");

		List<ReceiptModeMst> receiptModeMstMobilePayList = receiptModeMstMobilePayResp.getBody();
		if (receiptModeMstMobilePayList.size() == 0) {
			ReceiptModeMst receiptModeMst = new ReceiptModeMst();

			receiptModeMst.setReceiptMode("MOBILE PAY");
			receiptModeMst.setCreditCardStatus("NO");
			receiptModeMst.setStatus("ACTIVE");

			ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

		}

		ResponseEntity<AccountHeads> accountHeadBranchCash = RestCaller
				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
		if (null != accountHeadBranchCash.getBody()) {

			ResponseEntity<List<ReceiptModeMst>> receiptModeMstBranchCashResp = RestCaller
					.getReceiptModeMstByName(accountHeadBranchCash.getBody().getAccountName());

			List<ReceiptModeMst> receiptModeMstBranchCashList = receiptModeMstBranchCashResp.getBody();
			if (receiptModeMstBranchCashList.size() == 0) {
				ReceiptModeMst receiptModeMst = new ReceiptModeMst();

				receiptModeMst.setReceiptMode(accountHeadBranchCash.getBody().getAccountName());
				receiptModeMst.setCreditCardStatus("NO");
				receiptModeMst.setStatus("ACTIVE");

				ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

			}
		}

		ResponseEntity<AccountHeads> accountHeadBranchPettyCash = RestCaller
				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-PETTY CASH");
		if (null != accountHeadBranchPettyCash.getBody()) {

			ResponseEntity<List<ReceiptModeMst>> receiptModeMstBranchPettyCashResp = RestCaller
					.getReceiptModeMstByName(accountHeadBranchPettyCash.getBody().getAccountName());

			List<ReceiptModeMst> receiptModeMstBranchPettyCashList = receiptModeMstBranchPettyCashResp.getBody();
			if (receiptModeMstBranchPettyCashList.size() == 0) {
				ReceiptModeMst receiptModeMst = new ReceiptModeMst();

				receiptModeMst.setReceiptMode(accountHeadBranchPettyCash.getBody().getAccountName());
				receiptModeMst.setCreditCardStatus("NO");
				receiptModeMst.setStatus("ACTIVE");

				ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

			}
		}

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstNeftResp = RestCaller.getReceiptModeMstByName("NEFT");

		List<ReceiptModeMst> receiptModeMstNeftList = receiptModeMstNeftResp.getBody();
		if (receiptModeMstNeftList.size() == 0) {
			ReceiptModeMst receiptModeMst = new ReceiptModeMst();

			receiptModeMst.setReceiptMode("NEFT");
			receiptModeMst.setCreditCardStatus("NO");
			receiptModeMst.setStatus("ACTIVE");

			ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

		}

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstRtgsResp = RestCaller.getReceiptModeMstByName("RTGS");

		List<ReceiptModeMst> receiptModeMstRtgsList = receiptModeMstRtgsResp.getBody();
		if (receiptModeMstRtgsList.size() == 0) {
			ReceiptModeMst receiptModeMst = new ReceiptModeMst();

			receiptModeMst.setReceiptMode("RTGS");
			receiptModeMst.setCreditCardStatus("NO");
			receiptModeMst.setStatus("ACTIVE");

			ResponseEntity<ReceiptModeMst> savedReceiptMode = RestCaller.saveReceiptMode(receiptModeMst);

		}

		// ...........................InitializeInsurance.............//

		// --------------new version 1.9 surya end
		notifyMessage(5, "Initailized!!");

	}

	@FXML
	void SaveAction(ActionEvent event) {
		if (txtAccountName.getText().trim().isEmpty()) {
			notifyMessage(5, "Type Account Name");
			txtAccountName.requestFocus();
		} else {
			// try
			// {

			// --------------version 4.4
			if (null != accountHeads) {
				if (null != accountHeads.getId()) {
					if (null != cmbParentId.getSelectionModel().getSelectedItem()) {
						String parentName = cmbParentId.getSelectionModel().getSelectedItem().toString();
						ResponseEntity<AccountHeads> accountSaved = RestCaller.getAccountHeadByName(parentName);
						accountHeads.setParentId(accountSaved.getBody().getId());
					}

					RestCaller.updateAccountHeads(accountHeads);
					txtAccountName.clear();
					cmbGroup.getSelectionModel().clearSelection();
					cmbParentId.getSelectionModel().clearSelection();
					showTable();
					filltable();
				}
			} else {

				accountHeads = new AccountHeads();

				ResponseEntity<AccountHeads> accountS = RestCaller.getAccountHeadByName(txtAccountName.getText());
				if (null != accountS.getBody()) {
					notifyMessage(5, "Item Already Saved!!!");
					txtAccountName.clear();
					cmbGroup.getSelectionModel().clearSelection();
					cmbParentId.getSelectionModel().clearSelection();
					return;
				}
				accountHeads.setAccountName(txtAccountName.getText());
				accountHeads.setGroupOnly(cmbGroup.getSelectionModel().getSelectedItem());
				if (null != cmbParentId.getSelectionModel().getSelectedItem()) {
					String parentName = cmbParentId.getSelectionModel().getSelectedItem().toString();
					ResponseEntity<List<AccountHeads>> accountSaved = RestCaller.getAccountHeads();
					accountHeadList = FXCollections.observableArrayList(accountSaved.getBody());

					for (AccountHeads acc : accountHeadList) {
						System.out.println(acc.getAccountName());

						if (acc.getAccountName().equals(parentName)) {
							accountHeads.setParentId(acc.getId());
						}
					}
				}
				String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "AH");
				accountHeads.setId(vNo);

				ResponseEntity<AccountHeads> respentity = RestCaller.saveAccountHeads(accountHeads);
//				ResponseEntity<List<AccountHeads>> accountSaved = RestCaller.getAccountHeads();
//				accountHeadList = FXCollections.observableArrayList(accountSaved.getBody());
				// tbAccountHeads.setItems(accountHeadList);
				notifyMessage(5, " AccountHeads Saved");
				showTable();
//				tbAccountHeads.getItems().clear();
//				tbAccountHeads.setItems(accountHeadList);
				txtAccountName.clear();
				cmbParentId.getSelectionModel().clearSelection();
				cmbGroup.getSelectionModel().clearSelection();
				filltable();
				txtAccountName.clear();
				cmbGroup.getSelectionModel().clearSelection();
				showTable();
//				showAccountHeadGroup();
				// --------------version 4.4

			}
			// --------------version 4.4 end

			/*
			 * }
			 * 
			 * catch(Exception e) { return; }
			 */
		}
	}

	private void showTable() {
		showAccountHeadGroup();
		ResponseEntity<List<AccountHeads>> accountSaved = RestCaller.getAccountHeads();
		accountHeadList = FXCollections.observableArrayList(accountSaved.getBody());
//		System.out.println("accountHeadList"+accountHeadList);
		filltable();
		tbAccountHeads.setItems(accountHeadList);
		for (AccountHeads a : accountHeadList) {
			clAcccountName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

			if (null != a.getParentId()) {
				for (AccountHeads ac : accountHeadList1) {
					if (a.getParentId().equals(ac.getId())) {
						a.setParentName(ac.getAccountName());
						clParent.setCellValueFactory(cellData -> cellData.getValue().getparentNameProperty());
					}
				}
			}
			clGroup.setCellValueFactory(cellData -> cellData.getValue().getgroupProperty());
		}

	}

	private void filltable() {

		accountHeads = null;
	}
	/*
	 * @FXML private void initialize() { cmbGroup.getItems().add("Y");
	 * cmbGroup.getItems().add("N"); showAccountHeadGroup();
	 * ResponseEntity<List<AccountHeads>> accountSaved =
	 * RestCaller.getAccountHeads(); accountHeadList =
	 * FXCollections.observableArrayList(accountSaved.getBody());
	 * tbAccountHeads.setItems(accountHeadList); filltable();
	 * 
	 * tbAccountHeads.getSelectionModel().selectedItemProperty().addListener((obs,
	 * oldSelection, newSelection) -> { if (newSelection != null) { if (null !=
	 * newSelection.getId()) { accountHeads = new AccountHeads();
	 * txtAccountName.setText(newSelection.getAccountName());
	 * accountHeads.setId(newSelection.getId()); } } });
	 * 
	 * }
	 */

	@FXML
	private void initialize() {
		btnRefresh.setVisible(false);
		txtAccountName.textProperty().bindBidirectional(SearchString);
		cmbGroup.getItems().add("Y");
		cmbGroup.getItems().add("N");
		cmbPropertyType.getItems().add("STRING");
		cmbPropertyType.getItems().add("NUMBER");
		cmbPropertyType.getItems().add("BOOLEAN");
		showAccountHeadGroup();
		eventBus.register(this);
		ResponseEntity<List<AccountHeads>> accountSaved = RestCaller.getAccountHeads();
		
		tbAccountProperty.getSelectionModel().selectedItemProperty()
		.addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					accountHeadsProperties = new AccountHeadsProperties();
					accountHeadsProperties.setId(newSelection.getId());
				}
			}
		});
		
		
		
		
		

		// ResponseEntity<List<ReceiptModeMst>> receiptModeMst =
		// RestCaller.getInitializeReceiptModeMstByName();

		accountHeadList = FXCollections.observableArrayList(accountSaved.getBody());

		// ResponseEntity<List<ReceiptModeMst>> receiptModeMst =
		// RestCaller.getInitializeReceiptModeMstByName();

		// ResponseEntity<String> receiptModeMst =
		// RestCaller.getInitializeReceiptModeMstByName();

		// ResponseEntity<String> localCustomer=RestCaller.initializeLocalCustomer();

		tbAccountHeads.setItems(accountHeadList);
		filltable();

		tbAccountHeads.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					accountHeads = new AccountHeads();
					txtAccountName.setText(newSelection.getAccountName());
					accountHeads.setId(newSelection.getId());
				}
			}
		});
		for (AccountHeads a : accountHeadList) {
			clAcccountName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

			if (null != a.getParentId()) {
				for (AccountHeads ac : accountHeadList1) {
					if (a.getParentId().equals(ac.getId())) {
						a.setParentName(ac.getAccountName());
						clParent.setCellValueFactory(cellData -> cellData.getValue().getparentNameProperty());
					}
				}
			}
			clGroup.setCellValueFactory(cellData -> cellData.getValue().getgroupProperty());
		}
		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				loadAccountHeads((String) newValue);
			}

		});
	}

	private void loadAccountHeads(String newValue) {
		showAccountHeadGroup();
		ArrayList account = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		accountHeadList.clear();

		account = RestCaller.SearchAccountByName(newValue);
		Iterator itr = account.iterator();
		while (itr.hasNext()) {

			LinkedHashMap element = (LinkedHashMap) itr.next();
			Object accountName = (String) element.get("accountName");
			Object id = (String) element.get("id");
			Object parentId = (String) element.get("parentId");
			Object groupOnly = (String) element.get("groupOnly");
			Object machineId = (String) element.get("machineId");
			Object taxId = (String) element.get("taxId");

			if (null != id) {
				AccountHeads acc = new AccountHeads();
				acc.setAccountName((String) accountName);
				acc.setId((String) id);
				acc.setGroupOnly((String) groupOnly);
				acc.setMachineId((String) machineId);
				acc.setParentId((String) parentId);
				acc.setTaxId((String) taxId);
				accountHeadList.add(acc);

			}
		}

		tbAccountHeads.setItems(accountHeadList);
		for (AccountHeads a : accountHeadList) {
			clAcccountName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

			if (null != a.getParentId()) {
				for (AccountHeads ac : accountHeadList1) {
					if (a.getParentId().equals(ac.getId())) {
						a.setParentName(ac.getAccountName());
						clParent.setCellValueFactory(cellData -> cellData.getValue().getparentNameProperty());
					}
				}
			}
			clGroup.setCellValueFactory(cellData -> cellData.getValue().getgroupProperty());
		}
		filltable();
	}

	private void showAccountHeadGroup() {
		ArrayList cat = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		cat = RestCaller.SearchAccountsByGroup();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object acccuntName = lm.get("accountName");
			Object id = lm.get("id");
			Object parentId = lm.get("parentId");
			Object group = lm.get("groupOnly");
			if (id != null) {
				cmbParentId.getItems().add((String) acccuntName);

				AccountHeads accountHeads1 = new AccountHeads();
				accountHeads1.setId((String) id);
				accountHeads1.setAccountName((String) acccuntName);
				accountHeads1.setParentId((String) parentId);
				accountHeads1.setGroupOnly((String) group);
				accountHeadList1.add(accountHeads1);
				// accountHeads.getAccountName);
			}

		}

	}

	public void notifyMessage(int duration, String msg) {
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}

	@FXML
	void actionprintException(ActionEvent event) {

		ResponseEntity<List<AccountHeads>> getAcc = RestCaller.getAccountHeadsiWithParentNull();
		accountHeadList1 = FXCollections.observableArrayList(getAcc.getBody());
		try {
			JasperPdfReportService.ExceptionReport(accountHeadList1);
		} catch (JRException e) {
			e.printStackTrace();
		}

	}

	@FXML
	void actionRefresh(ActionEvent event) {
		showTable();
	}
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
