package com.maple.mapleclient.controllers;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.SchEligibilityDef;
import com.maple.mapleclient.entity.SchOfferDef;
import com.maple.mapleclient.entity.SchSelectDef;
import com.maple.mapleclient.entity.SchemeInstance;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
public class CreditCardReconcileCtl {
	String taskid;
	String processInstanceId;

	
	@FXML
	 private ComboBox<String>cmbReceiptMode;
	    @FXML
	    private Button btnAdd;

	    @FXML
	    private Button btnRefresh;

	    @FXML
	    private Button btnDelete;

	    @FXML
	    private Button btnEdit;

	    @FXML
	    private TableView<?> tblCreditCardReconcile;

	    @FXML
	    private TableColumn<?, ?> clReceiptMode;

	    @FXML
	    private TableColumn<?, ?> clDate;

	    @FXML
	    private TableColumn<?, ?> clAmount;

	    @FXML
	    private TableColumn<?, ?> clSystemAmount;

	    @FXML
	    void EditScheme(ActionEvent event) {

	    }

	    @FXML
	    void Refresh(ActionEvent event) {

	    }

	    @FXML
	    void addItem(ActionEvent event) {

	    }

	    @FXML
	    void deleteAction(ActionEvent event) {

	    }
	
	    
		@FXML
		private void initialize() {
			
			
			
			
		}
		@Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload();
			}


	  private void PageReload() {
	  	
	}
}
