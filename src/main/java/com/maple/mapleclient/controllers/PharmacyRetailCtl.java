package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.hamcrest.core.IsEqual;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.CompanyMst;
import com.maple.mapleclient.entity.CurrencyConversionMst;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.entity.FinanceMst;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.NewDoctorMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PatientMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.SalesTypeMst;
import com.maple.mapleclient.entity.SchEligibilityAttribInst;
import com.maple.mapleclient.entity.SchOfferAttrInst;
import com.maple.mapleclient.entity.SchSelectionAttribInst;
import com.maple.mapleclient.entity.SchemeInstance;
import com.maple.mapleclient.entity.SiteMst;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CategorywiseItemSearchEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.HoldedCustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.events.PatientEvent;
import com.maple.mapleclient.events.ReceiptModeEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.mapleclient.service.SalesDtlServiceImpl;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyRetailCtl {

	String taskid;
	String processInstanceId;
	SalesDtlServiceImpl salesDtlService = new SalesDtlServiceImpl();
	String localCustId = null;
	SalesReceipts salesReceipts = new SalesReceipts();
	private ObservableList<SalesTypeMst> saleTypeTable = FXCollections.observableArrayList();
	private ObservableList<PriceDefinition> priceDefenitionList = FXCollections.observableArrayList();
	private ObservableList<BatchPriceDefinition> BatchpriceDefenitionList = FXCollections.observableArrayList();
	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
	private ObservableList<SalesReceipts> salesReceiptsList = FXCollections.observableArrayList();
	private ObservableList<SiteMst> siteMstList = FXCollections.observableArrayList();
	String salesReceiptVoucherNo = null;
	private static final Logger logger = LoggerFactory.getLogger(PurchaseCtl.class);
	String invoiceNumberPrefix = SystemSetting.WHOLE_SALES_PREFIX;
	String gstInvoicePrefix = SystemSetting.GST_INVOCE_PREFIX;
	String vanSalesPrefix = SystemSetting.GST_INVOCE_PREFIX;
	String discount_Enable = SystemSetting.DISCOUNT_ENABLE;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();

	SalesDtl salesDtl = null;
	SalesTransHdr salesTransHdr = null;
	UnitMst unitMst = null;
	double cardAmount = 0.0;
	double qtyTotal = 0;
	double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	String custId = "";
	boolean customerIsBranch = false;
	Double AmountTenderd = 0.0;
	Double CashPaid = 0.0;
	Double totalAmountTenderd = 0.0;
	Double CardAmount = 0.0;
	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty sodexoAmountLis = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");

	StringProperty barcodeProperty = new SimpleStringProperty("");

	StringProperty taxRateProperty = new SimpleStringProperty("");

	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");
	@FXML
	private TextField custAdress;

	@FXML
	private TextField custname;

//	 @FXML
//	 private TextField txtPatient;
	@FXML
	private Label lblDisc;
	@FXML
	private ComboBox<String> cmbFinance;
	@FXML
	private TextField txtDiscount;

	@FXML
	private Label lbldisper;
	@FXML
	private TextField txtTotalInvoice;
	@FXML
	private TextField txtInsuranceCompany;

	@FXML
	private TextField txtPolicyType;

	@FXML
	private TextField txtPercentageCoverage;

	@FXML
	private ComboBox<String> cmbDoctor;

	@FXML
	private ComboBox<String> cmbCurrency;

	@FXML
	private TextField txtFCCashToPay;

	@FXML
	private Button btnAddCustomer;

	@FXML
	private TextField txtPreviousBalance;

	@FXML
	private Label lblamtdis;

	@FXML
	private TextField txtDiscountPercent;
	@FXML
	private TextField txtPriceType;

	@FXML
	private ComboBox<String> cmbSaleType;

	@FXML
	private TextField txtLoginDate;

	@FXML
	private TextField txtLocalCustomer;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtItemcode;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnDeleterow;

	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;
	
	 @FXML
	    private Button btnClear;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnMrp;

	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;
	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TextField txtSBICard;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private TextField txtSodexoCard;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtYesCard;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField txtcardAmount1;

	@FXML
	private TextField txtAmtAftrDiscount;

	@FXML
	private TextField txtPaidamount1;

	@FXML
	private TextField txtChangeamount1;

	@FXML
	private Button btnCardSale;

	@FXML
	private Label lblCardType;

	@FXML
	private Label lblAmount;

	@FXML
	private TextField txtCustomerMst;

	@FXML
	void VisibleCardSale(ActionEvent event) {

		visileCardSale();

	}

	private void visileCardSale() {

		lblCardType.setVisible(true);
		lblAmount.setVisible(true);
		txtCardType.setVisible(true);
		txtCrAmount.setVisible(true);
		AddCardAmount.setVisible(true);
		btnDeleteCardAmount.setVisible(true);
		tblCardAmountDetails.setVisible(true);
		txtcardAmount.setVisible(true);

	}

	@FXML
	void CustomerPopUp(MouseEvent event) {
		loadCustomerPopup();

	}

	private void showLocalCustPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LocalCustPopup.fxml"));

			// fxmlLoader.setController(itemStockPopupCtl);

			Parent root = loader.load();
			LocalCustPopupCtl popupctl = loader.getController();
			popupctl.localCustomerPopupByCustomerId(custId);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupLocalCustomerlistner(LocalCustomerEvent localCustomerEvent) {
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtLocalCustomer.setText(localCustomerEvent.getLocalCustAddress());
					localCustId = localCustomerEvent.getLocalCustid();
					System.out.println("custIdcustIdcustId" + custId);
				}
			});
		}

	}

	@FXML
	void discountPerOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if (!txtDiscountPercent.getText().isEmpty()) {
				txtDiscount.clear();
				Double amtaftradiscount;
				Double discountamt = (Double.parseDouble(txtCashtopay.getText())
						* Double.parseDouble(txtDiscountPercent.getText())) / 100;
				BigDecimal disamt = new BigDecimal(discountamt);
				disamt = disamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				txtDiscount.setText(disamt.toPlainString());
				// txtDiscountAmt.setEditable(false);
				amtaftradiscount = Double.parseDouble(txtCashtopay.getText())
						- Double.parseDouble(txtDiscount.getText());
				BigDecimal amtaftdiscount = new BigDecimal(amtaftradiscount);
				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
				txtAmtAftrDiscount.setEditable(false);
			}

		}
	}

	@FXML
	void discountAmtOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtDiscount.getText().isEmpty()) {
//				txtDiscount.clear();
				Double discountAftrAmt = 0.0, discount = 0.0;
				discount = (Double.parseDouble(txtDiscount.getText()) / Double.parseDouble(txtCashtopay.getText()))
						* 100;
				BigDecimal disamt = new BigDecimal(discount);
				disamt = disamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtDiscountPercent.setText(disamt.toPlainString());
				discountAftrAmt = Double.parseDouble(txtCashtopay.getText())
						- Double.parseDouble(txtDiscount.getText());
				BigDecimal amtaftdiscount = new BigDecimal(discountAftrAmt);
				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
				txtAmtAftrDiscount.setEditable(false);
			}

		}
	}

	@FXML
	private void initialize() {

//		if (discount_Enable.equalsIgnoreCase("YES")) {
//			txtDiscount.setVisible(true);
//			txtDiscountPercent.setVisible(true);
//			txtAmtAftrDiscount.setVisible(true);
//			lblDisc.setVisible(true);
//			lbldisper.setVisible(true);
//			lblamtdis.setVisible(true);
//		} else {
//			txtDiscount.setVisible(false);
//			txtDiscountPercent.setVisible(false);
//			txtAmtAftrDiscount.setVisible(false);
//			lblDisc.setVisible(false);
//			lbldisper.setVisible(false);
//			lblamtdis.setVisible(false);
//		}
//		
		
		//===========no need of currency combo============
		ResponseEntity<List<CurrencyMst>> currencyMst = RestCaller.getallCurrencyMst();
		for (int i = 0; i < currencyMst.getBody().size(); i++) {
			cmbCurrency.getItems().add(currencyMst.getBody().get(i).getCurrencyName());
			cmbCurrency.getSelectionModel().select(SystemSetting.getUser().getCompanyMst().getCurrencyName());
		}
		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("RETAIL_RATE_EDIT");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("NO")) {
				txtRate.setEditable(false);
			} else {
				txtRate.setEditable(true);
			}
		}
		txtCashtopay.setEditable(false);
		txtFCCashToPay.setEditable(false);
		cmbFinance.getItems().add("NO FINANCE");
		cmbFinance.setPromptText("NO FINANCE");
		cmbFinance.setValue("NO FINANCE");
		ResponseEntity<List<FinanceMst>> getallFinance = RestCaller.findAllFinanceMst();
		for (FinanceMst finance : getallFinance.getBody())
		{
			cmbFinance.getItems().add(finance.getName());
		}

		txtLoginDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate));
//		cmbSaleType.getItems().add("VAN SALE");
//		cmbSaleType.getItems().add("COUNTER SALE");
		ResponseEntity<List<SalesTypeMst>> salesTypeSaved = RestCaller.getSalesTypeMst();
		if (null == salesTypeSaved.getBody()) {
			notifyMessage(5, "Please Add Voucher Type",false);
			return;
		}
		saleTypeTable = FXCollections.observableArrayList(salesTypeSaved.getBody());
//		setCardType();

		for (SalesTypeMst salesType : saleTypeTable) {
			cmbSaleType.getItems().add(salesType.getSalesType());

		}

		logger.info("========INITIALIZATION STARTED IN WHOLE SALE WINDOW ===============");
		/*
		 * Create an instance of SalesDtl. SalesTransHdr entity will be refreshed after
		 * final submit. SalesDtl will be added on AddItem Function
		 * 
		 */

		// salesDtl = new SalesDtl();
		/*
		 * Fieds with Entity property
		 */
		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);
		// txtItemcode.textProperty().bindBidirectional(salesDtl.getItemCodeProperty());
		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPaidamount.setText(oldValue);
				}
			}
		});
		txtChangeamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtChangeamount.setText(oldValue);
				}
			}
		});
		txtFCCashToPay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtFCCashToPay.setText(oldValue);
				}
			}
		});
		txtSBICard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSBICard.setText(oldValue);
				}
			}
		});

		txtYesCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtYesCard.setText(oldValue);
				}
			}
		});

		txtSodexoCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSodexoCard.setText(oldValue);
				}
			}
		});
		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});
		
		//==================no need of currency combo===============
		cmbCurrency.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null == cmbCurrency.getSelectionModel()
						|| null == cmbCurrency.getSelectionModel().getSelectedItem()) {
					txtFCCashToPay.setText("0.0");
				} else if (SystemSetting.getUser().getCompanyMst().getCurrencyName()
						.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
					txtFCCashToPay.setText("0.0");
				} else {
//					Double fcRate = RestCaller.getFCRate(Double.parseDouble(txtCashtopay.getText()),
//							cmbCurrency.getSelectionModel().getSelectedItem());
					Double fcRate = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), Double.parseDouble(txtCashtopay.getText()));

					if (fcRate > 0) {
						txtFCCashToPay.setText(Double.toString(fcRate));
					} else {
						notifyMessage(3, "Currency Rate not Set",false);
						return;
					}
				}
			}

		});
		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if ((txtcardAmount.getText().length() >= 0)) {

					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtTotalInvoice.getText());
					} catch (Exception e) {
						cashToPay = 0.0;

					}

					try {
						paidAmount = Double.parseDouble(txtPaidamount.getText());
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					try {
						cardPaid = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					BigDecimal newrate = new BigDecimal(cashToPay-(paidAmount + cardPaid + sodexoAmt));
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
					if (newrate.doubleValue() < 0) {

						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
					} else {

						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
					}

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if ((txtPaidamount.getText().length() > 0)) {

					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						if (discount_Enable.equalsIgnoreCase("YES")) {

							if (txtAmtAftrDiscount.getText().trim().length() > 0) {
								cashToPay = Double.parseDouble(txtAmtAftrDiscount.getText());
							} else {
								cashToPay = Double.parseDouble(txtFCCashToPay.getText());
							}
						} else {
							cashToPay = Double.parseDouble(txtTotalInvoice.getText());
						}
					} catch (Exception e) {
						cashToPay = 0.0;
					}

					try {
						cardPaid = Double.parseDouble(txtcardAmount.getText());
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					try {
						paidAmount = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					BigDecimal newrate = new BigDecimal(cashToPay-(paidAmount + cardPaid + sodexoAmt));
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());

					if (newrate.doubleValue() < 0) {

						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
					} else {

						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
					}

				}

				if (((String) newValue).length() == 0) {
					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtTotalInvoice.getText());
					} catch (Exception e) {
						cashToPay = 0.0;
					}

					try {
						cardPaid = Double.parseDouble(txtcardAmount.getText());
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					try {
						paidAmount = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					BigDecimal newrate = new BigDecimal(cashToPay-(paidAmount + cardPaid + sodexoAmt));
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
				}

			}
		});

//		discount.addListener(new ChangeListener() {
//
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//				if (((String) newValue).length() == 0)
//					return;
//				if ((txtDiscount.getText().length() > 0)) 
//				{
//					double discountper =Double.parseDouble(txtDiscount.getText());
//				
//				Double amtaftradiscount;
//				Double discountamt = (Double.parseDouble(txtCashtopay.getText()) * Double.parseDouble(txtDiscount.getText()))/100;
//				amtaftradiscount = Double.parseDouble(txtCashtopay.getText())-discountamt;
//				BigDecimal amtaftdiscount = new BigDecimal(amtaftradiscount);
//				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_CEILING);
//				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
//				}
//				
//		}
//		});
//
//		discountAmt.addListener(new ChangeListener() {
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//				if (((String) newValue).length() == 0)
//					return;
//				if ((txtDiscountAmt.getText().length() > 0)) 
//				{
//			Double discountAftrAmt = 0.0,discount = 0.0;
//			discount = (Double.parseDouble(txtDiscountAmt.getText())/Double.parseDouble(txtCashtopay.getText()))*100;
//		
//		//	txtDiscount.setText(Double.toString(discount));
//			discountAftrAmt = Double.parseDouble(txtCashtopay.getText()) -discount;
//			BigDecimal amtaftdiscount = new BigDecimal(discountAftrAmt);
//			amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_CEILING);
//			txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
//				}
//				
//			}
//		});
		eventBus.register(this);

		// btnSave.setDisable(true);
		itemDetailTable.setItems(saleListTable);

		txtBarcode.requestFocus();

		/////////////

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesDtl = new SalesDtl();
					txtBatch.setText(newSelection.getBatchCode());
					salesDtl.setId(newSelection.getId());
					txtBarcode.setText(newSelection.getBarcode());
					txtItemname.setText(newSelection.getItemName());
					txtItemcode.setText(newSelection.getItemCode());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));
					cmbUnit.getSelectionModel().select(newSelection.getUnitName());
				}
			}
		});
		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				ResponseEntity<AccountHeads> getCust = RestCaller.getAccountHeadsByName(txtCustomerMst.getText());

				AccountHeads customerMst = getCust.getBody();

				// ResponseEntity<PriceDefinition> getpriceDef = RestCaller.get
				ResponseEntity<UnitMst> getUnit = RestCaller
						.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

				UnitMst unitMst = getUnit.getBody();

				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

				ItemMst item = getItem.getBody();

				String unitId = "";

				if (null == unitMst) {
					unitId = item.getUnitId();

				} else {
					unitId = unitMst.getId();
				}
				java.util.Date udate = SystemSetting.getApplicationDate();
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
						getItem.getBody().getId(), getCust.getBody().getPriceTypeId(), unitId,
						txtBatch.getText(), sdate);
				if (null != batchPriceDef.getBody()) {
					txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
				} else {
					txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));

					ResponseEntity<PriceDefinition> priceDef = RestCaller.getPriceDefenitionByItemIdAndUnit(
							item.getId(), customerMst.getPriceTypeId(), unitMst.getId(), sdate);
					if (null != priceDef.getBody()) {
						txtRate.setText(Double.toString(priceDef.getBody().getAmount()));

					}

					else

					{
						ResponseEntity<List<PriceDefinition>> pricebyItem = RestCaller
								.getPriceByItemId(getItem.getBody().getId(), getCust.getBody().getPriceTypeId());
						priceDefenitionList = FXCollections.observableArrayList(pricebyItem.getBody());

						if (null != pricebyItem.getBody())

						{
							for (PriceDefinition price : priceDefenitionList) {
								if (null == price.getUnitId()) {
									txtRate.setText(Double.toString(price.getAmount()));

								}
							}
						}
					}

				}

			}
		});

		ResponseEntity<List<NewDoctorMst>> doctorMstListResp = RestCaller.getNewDoctorMst();
		List<NewDoctorMst> doctorMstList = doctorMstListResp.getBody();

		for (NewDoctorMst doctor : doctorMstList) {
			cmbDoctor.getItems().add(doctor.getName());
		}

		tblCardAmountDetails.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						if (null != newSelection.getId()) {

							salesReceipts = new SalesReceipts();
							salesReceipts.setId(newSelection.getId());
							salesReceipts.setReceiptMode(newSelection.getReceiptMode());
						}
					}
				});
		logger.info("======== WHOLE SALE WINDOW INITIALIZATION COMPLETED");

	}

	@FXML
	void FinalSaveOnPress(KeyEvent event) {
		logger.info("===== WHOLE SALE FINAL SAVE STARTED ============");
		if (event.getCode() == KeyCode.ENTER) {
			try {
				FinalSave();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

//	private void debitShortageAmount(double shortageAmount) {
//		// Store shortage amount
//		SalesReceipts salesReceipts = new SalesReceipts();
//		if (null == salesReceiptVoucherNo) {
//			salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
//		}
//
//		salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
//
//		if (cmbFinance.getSelectionModel().getSelectedItem().equalsIgnoreCase("NO FINANCE")) {
//			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(custname.getText());
//			salesReceipts.setAccountId(accountHeads.getBody().getId());
//		} else {
//			ResponseEntity<AccountHeads> accountHeads = RestCaller
//					.getAccountHeadByName(cmbFinance.getSelectionModel().getSelectedItem());
//			salesReceipts.setAccountId(accountHeads.getBody().getId());
//		}
//		salesReceipts.setReceiptMode("CREDIT");
//
//		salesReceipts.setReceiptAmount(shortageAmount);
//		salesReceipts.setUserId(SystemSetting.getUser().getId());
//		salesReceipts.setBranchCode(SystemSetting.systemBranch);
//
//		LocalDate date = LocalDate.now();
//		java.util.Date udate = SystemSetting.localToUtilDate(date);
//		salesReceipts.setReceiptDate(udate);
//		salesReceipts.setSalesTransHdr(salesTransHdr);
//		System.out.println(salesReceipts);
//		ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
//		salesReceipts = respEntity.getBody();
//
//	}

	private void accountCashAmount(double cashAmount) {
		// Store shortage amount
		SalesReceipts salesReceipts = new SalesReceipts();
		if (null == salesReceiptVoucherNo) {
			salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
		}

		salesReceipts.setVoucherNumber(salesReceiptVoucherNo);

		ResponseEntity<AccountHeads> accountHeads1 = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-" + "CASH");
		salesReceipts.setAccountId(accountHeads1.getBody().getId());
		salesReceipts.setReceiptMode(salesTransHdr.getBranchCode() + "-" + "CASH");

		salesReceipts.setReceiptAmount(cashAmount);

		salesReceipts.setUserId(SystemSetting.getUser().getId());
		salesReceipts.setBranchCode(SystemSetting.systemBranch);

		LocalDate date1 = LocalDate.now();
		java.util.Date udate1 = SystemSetting.localToUtilDate(date1);
		salesReceipts.setReceiptDate(udate1);
		salesReceipts.setSalesTransHdr(salesTransHdr);
		System.out.println(salesReceipts);

		ResponseEntity<SalesReceipts> respEntity1 = RestCaller.saveSalesReceipts(salesReceipts);
		salesReceipts = respEntity1.getBody();

	}

	private void FinalSave() throws SQLException {

		// -------------------verification 5.0 Surya
		String stockVerification = RestCaller.StockVerificationBySalesTransHdr(salesTransHdr.getId());
		if (null != stockVerification) {
			notifyMessage(5, stockVerification,false);

//			return;

		} else {

			// -------------------verification 5.0 Surya end

			/*
			 * Update sales_dtl table for fc fields.. if currency not selected before
			 * additem then we should update salesdtl for fc fields
			 * 
			 */
			
			
			if (txtCustomerMst.getText().trim().isEmpty()) {
				
				
				double TotalCashPAidForTheVoucher = 0;

				String strCashPaid = txtPaidamount.getText();

				try {
					TotalCashPAidForTheVoucher = Double.parseDouble(strCashPaid);
				} catch (Exception e) {

				}
				
				double TotalCardPaidForTheVoucher = 0;

				String strCardPaid = txtcardAmount.getText();

				try {
					TotalCardPaidForTheVoucher = Double.parseDouble(strCardPaid);
				} catch (Exception e) {

				}
				
				double totalAmount=TotalCashPAidForTheVoucher + TotalCardPaidForTheVoucher;
				String totalInvoiceAmount = txtTotalInvoice.getText();
				double totalInvoice=Double. parseDouble(totalInvoiceAmount); 
				if(totalAmount < totalInvoice) {
					notifyMessage(3, "Amount mismatch :-( ", false);
					txtPaidamount.requestFocus();
					return;
				}
				
				
			}
			ResponseEntity<List<SalesDtl>> salesDtlList = RestCaller.getSalesDtl(salesTransHdr);

			if (null != salesTransHdr.getPatientMst().getAccountHeads()) {

				addCardAmount("CREDIT", txtTotalInvoice.getText());

			}

			Double cashPaid = 0.0;
			Double cardAmount = 0.0;
			Double cashToPay = 0.0;
			Double sodexoAmount = 0.0;
			Double changeAmount = 0.0;
			Double discountAmount = 0.0;

			Double cashToPayAfterDiscount = 0.0;

			Double totalPaidyCutomer = 0.0;

			cashPaid = getCashPaid();
			cardAmount = getTotalCardsPaid();
			cashToPay = getBillToPay();
			discountAmount = 0.0;

			cashToPayAfterDiscount = cashToPay - discountAmount;

			if (cashPaid > cashToPayAfterDiscount) {
				cashPaid = cashToPayAfterDiscount;
			}

			changeAmount = (cashPaid + cardAmount) - cashToPayAfterDiscount;
			if (changeAmount > 0) {
				if (changeAmount < cashPaid) {
					cashPaid = cashPaid - changeAmount;
				}
			}

			totalPaidyCutomer = cashPaid + cardAmount;

			BigDecimal bdCashToPayAfterDiscount = new BigDecimal(cashToPayAfterDiscount);
			bdCashToPayAfterDiscount = bdCashToPayAfterDiscount.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal bdTotalPaid = new BigDecimal(cashPaid + cardAmount);
			bdTotalPaid = bdTotalPaid.setScale(0, BigDecimal.ROUND_HALF_EVEN);

//			if (bdCashToPayAfterDiscount.compareTo(bdTotalPaid) < 0) {
//				// bdCashToPayAfterDiscount < bdTotalPaid
//
//				// Notify and return
//				notifyMessage(5, " Amount mismatch...!!!",false);
//
//				// Delete Mode Of Pay Cash.
//
//				return;
//
//			} else if (bdCashToPayAfterDiscount.compareTo(bdTotalPaid) == 0) {
//				// bdCashToPayAfterDiscount == bdTotalPaid
//
//				// Account Cash Paid
//				if (cashPaid > 0) {
//
//					accountCashAmount(cashPaid);
//				}
//				// accountCashAmount(cardAmount);
//
//			}
//			else if (bdCashToPayAfterDiscount.compareTo(bdTotalPaid) > 0) {
//				// bdCashToPayAfterDiscount > bdTotalPaid
//
//				double deltaShortage = cashToPayAfterDiscount - (cashPaid + cardAmount);
//				debitShortageAmount(deltaShortage);
//				if (cashPaid > 0) {
//					accountCashAmount(cashPaid);
//				}
//
//				notifyMessage(5, " Balance amount will be debited in customer account !!!");
//
//			}
			
			
			Double cashPaid1 = 0.0;
			Double cardAmount1 = 0.0;
			Double cashToPay1 = 0.0;
			//Double sodexoAmount = 0.0;
			//Double changeAmount = 0.0;

			SalesReceipts salesReceipts = new SalesReceipts();
			
			if (!txtcardAmount.getText().trim().isEmpty()) {

				cardAmount1 = Double.parseDouble(txtcardAmount.getText());

			}

			try {
				cashToPay1 = Double.parseDouble(txtCashtopay.getText());

			} catch (Exception e) {

			}

			try {
				if (!txtPaidamount.getText().trim().isEmpty()) {
					cashPaid1 = Double.parseDouble(txtPaidamount.getText());

					salesReceipts = new SalesReceipts();
					if (null == salesReceiptVoucherNo) {
						salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
						salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
					} else {
						salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
					}
					ResponseEntity<AccountHeads> accountHeads = RestCaller
							.getAccountHeadByName(SystemSetting.systemBranch + "-" + "CASH");
					salesReceipts.setAccountId(accountHeads.getBody().getId());
					salesReceipts.setReceiptMode(salesTransHdr.getBranchCode() + "-" + "CASH");

					Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
					Double AmountTenderd = 0.0;
					Double CashPaid = 0.0;

					if (!txtPaidamount.getText().isEmpty()) {
						AmountTenderd = Double.parseDouble(txtPaidamount.getText());
					}

					Double CardAmount = 0.0;

					if (!txtcardAmount.getText().isEmpty()) {
						CardAmount = Double.parseDouble(txtcardAmount.getText());

					}
					Double totalAmountTenderd = AmountTenderd + CardAmount;
					if (totalAmountTenderd >= invoiceAmount) {
						CashPaid = invoiceAmount - CardAmount;

					} else if (totalAmountTenderd < invoiceAmount) {

						notifyMessage(1, "Please Enter Valid Amount!!!!",false);

						return;
					}

					salesReceipts.setReceiptAmount(CashPaid);

				

					salesReceipts.setUserId(SystemSetting.getUser().getId());
					salesReceipts.setBranchCode(SystemSetting.systemBranch);
//
					salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
					salesReceipts.setSalesTransHdr(salesTransHdr);
					System.out.println(salesReceipts);
					ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
					salesReceipts = respEntity.getBody();

				}
			} catch (Exception e) {

			}
			
			
			
			

			if (!txtChangeamount.getText().trim().isEmpty()) {
				changeAmount = Double.parseDouble(txtChangeamount.getText());

			}

			txtChangeamount.setText("00.0");

			String card = "";
			String cardType = "";

			if (!card.trim().isEmpty()) {
				salesTransHdr.setCardNo(card);
			}

			salesTransHdr.setBranchCode(SystemSetting.systemBranch);

			salesTransHdr.setCardType(cardType);

			salesTransHdr.setCardamount(cardAmount);

			salesTransHdr.setCashPay(cashPaid);

			salesTransHdr.setInvoiceDiscount(discountAmount);

			salesTransHdr.setPaidAmount(cashPaid + cardAmount);

			salesTransHdr.setChangeAmount(changeAmount);
			salesTransHdr.setSodexoAmount(sodexoAmount);

			LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
			java.util.Date date1 = SystemSetting.systemDate;
			salesTransHdr.setVoucherDate(date1);
			salesTransHdr.setInvoiceAmount(Double.parseDouble(txtTotalInvoice.getText()));
			
			ResponseEntity<SalesTypeMst> getsalesType = RestCaller
					.getSaleTypeByname(cmbSaleType.getSelectionModel().getSelectedItem());
			salesTransHdr.setInvoiceNumberPrefix(getsalesType.getBody().getSalesPrefix());
			//===============no need of currency combo==========
			if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {
				if (!SystemSetting.getUser().getCompanyMst().getCurrencyName()
						.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
//					Double fcInvoiceAmount = RestCaller.getFCRate(salesTransHdr.getInvoiceAmount(),
//							cmbCurrency.getSelectionModel().getSelectedItem());
					Double fcInvoiceAmount = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), salesTransHdr.getInvoiceAmount());

					if (fcInvoiceAmount > 0) {
						salesTransHdr.setFcInvoiceAmount(fcInvoiceAmount);
					}
				}
			}
			eventBus.post(salesTransHdr);

			ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
			if (saledtlSaved.getBody().size() == 0) {
				return;
			}
			salesTransHdr.setSalesReceiptsVoucherNumber(salesReceiptVoucherNo);

			/*
			 * Boolean offer = CheckOfferWhileFinalSave(salesTransHdr); if (offer) {
			 * 
			 * }
			 */

			ResponseEntity<AccountHeads> accountHeadsentity = RestCaller.getAccountHeadsById(salesTransHdr.getAccountHeads().getId());
			ResponseEntity<AccountReceivable> accountReceivableResp = RestCaller
					.getAccountReceivableBySalesTransHdrId(salesTransHdr.getId());
			AccountReceivable accountReceivable = null;
			if (null != accountReceivableResp.getBody()) {
				accountReceivable = accountReceivableResp.getBody();
			} else {
				accountReceivable = new AccountReceivable();
			}

			accountReceivable.setAccountId(salesTransHdr.getAccountHeads().getId());
			accountReceivable.setAccountHeads(accountHeadsentity.getBody());

			accountReceivable.setPaidAmount(totalPaidyCutomer);
			accountReceivable.setDueAmount(bdCashToPayAfterDiscount.doubleValue());

			accountReceivable.setBalanceAmount(bdCashToPayAfterDiscount.doubleValue() - totalPaidyCutomer);

			LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
			LocalDate dueDate = due.plusDays(accountHeadsentity.getBody().getCreditPeriod());
			accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
			accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
			accountReceivable.setSalesTransHdr(salesTransHdr);
			accountReceivable.setRemark("Wholesale");
			LocalDate due1 = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
			accountReceivable.setVoucherDate(java.sql.Date.valueOf(due1));
//			accountReceivable.setPaidAmount(0.0);
			ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
			accountReceivable = respentity.getBody();
//-------------------------------------surya 2021-05-10----------------------------------
			salesTransHdr.setFcInvoiceAmount(0.0);
			if (!txtFCCashToPay.getText().trim().isEmpty()) {
				salesTransHdr.setFcInvoiceAmount(Double.parseDouble(txtFCCashToPay.getText()));
			}
			//===============no need of currency combo============
			if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {
				salesTransHdr.setCurrencyType(cmbCurrency.getSelectionModel().getSelectedItem());

				ResponseEntity<CurrencyMst> currencyMstResp = RestCaller
						.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem());
				CurrencyMst currencyMst = currencyMstResp.getBody();

				if (null != currencyMst) {
					ResponseEntity<CurrencyConversionMst> currencyConversionResp = RestCaller
							.getCurrencyConversionMstByCurrencyId(currencyMst.getId());
					CurrencyConversionMst currencyConversionMst = currencyConversionResp.getBody();

					if (null != currencyConversionMst) {
						salesTransHdr.setCurrencyConversionRate(currencyConversionMst.getConversionRate());
					}
				}
			}

//-------------------------------------surya 2021-05-10- end---------------------------------

			if (null == salesTransHdr.getVoucherNumber()) {
				RestCaller.updateSalesTranshdr(salesTransHdr);
			}
			ResponseEntity<List<SalesReceipts>> salesreceipt = RestCaller
					.getSalesReceiptsByTransHdrId(salesTransHdr.getId());

			salesReceiptsList = FXCollections.observableArrayList(salesreceipt.getBody());
			salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
			
			for (SalesReceipts salesRec : salesReceiptsList) {
				DayBook dayBook = new DayBook();
				dayBook.setBranchCode(salesTransHdr.getBranchCode());
				ResponseEntity<AccountHeads> accountHead1 = RestCaller.getAccountById(salesRec.getAccountId());
				AccountHeads accountHeads1 = accountHead1.getBody();
				
				if(null != accountHeads1) {
				dayBook.setDrAccountName(accountHeads1.getAccountName());
				dayBook.setDrAmount(salesRec.getReceiptAmount());
				dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
				}
				if (salesRec.getReceiptMode().contains("CASH")) {
					if (cmbFinance.getSelectionModel().getSelectedItem().equalsIgnoreCase("NO FINANCE")) {
						dayBook.setNarration(custname.getText());
					} else {
						dayBook.setNarration(cmbFinance.getSelectionModel().getSelectedItem());
					}
					dayBook.setCrAmount(0.0);

				} else {
					dayBook.setNarration("WHOLE SALE");
					dayBook.setCrAmount(salesRec.getReceiptAmount());
					dayBook.setCrAccountName("SALES");
				}
				dayBook.setSourceVoucherType("SALES RECEIPTS");

				LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
				dayBook.setsourceVoucherDate(Date.valueOf(rdate));
				ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			}

			Format formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			String strDate = formatter.format(salesTransHdr.getVoucherDate());
			String voucherNumber = salesTransHdr.getVoucherNumber();
			salesTransHdr = null;
			salesDtl = null;
			txtcardAmount.setText("");
			txtCashtopay.setText("");
			txtPaidamount.setText("");
			txtSBICard.setText("");
			txtSodexoCard.setText("");
			txtYesCard.setText("");
			custname.setText("");
			custAdress.setText("");
			saleListTable.clear();
			txtItemname.setText("");
			txtBarcode.setText("");
			txtQty.setText("");
			txtRate.setText("");
			totalAmountTenderd = 0.0;
			CashPaid = 0.0;
			AmountTenderd = 0.0;
			custId = null;
			cmbSaleType.getSelectionModel().clearSelection();
			txtPriceType.clear();
			txtItemcode.setText("");
			txtBatch.setText("");
			txtBarcode.requestFocus();
			CardAmount = 0.0;
			txtLocalCustomer.clear();
			txtLocalCustomer.setDisable(true);

			salesTransHdr = null;

			txtcardAmount.setText("");
			txtCashtopay.setText("");
			txtPaidamount.setText("");

			txtChangeamount.clear();
			saleListTable.clear();
			txtBarcode.requestFocus();
			salesDtl = new SalesDtl();

			txtDiscount.clear();
			txtDiscountPercent.clear();
			txtAmtAftrDiscount.clear();
			salesDtl = new SalesDtl();
			tblCardAmountDetails.getItems().clear();
			salesReceiptVoucherNo = null;
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtItemcode.clear();
			txtRate.clear();

			txtPreviousBalance.clear();
			txtQty.clear();
			salesReceiptsList.clear();
			lblCardType.setVisible(false);
			lblAmount.setVisible(false);
			txtCardType.setVisible(false);
			txtCrAmount.setVisible(false);
			AddCardAmount.setVisible(false);
			btnDeleteCardAmount.setVisible(false);
			tblCardAmountDetails.setVisible(false);
			txtcardAmount.setVisible(false);
			txtInsuranceCompany.clear();
			txtFCCashToPay.clear();
			txtPolicyType.clear();
			txtPercentageCoverage.clear();
			cmbCurrency.getSelectionModel().clearSelection();
			cmbDoctor.getSelectionModel().clearSelection();
			txtTotalInvoice.clear();

//			try {
//				NewJasperPdfReportService.TaxInvoiceReport(voucherNumber, strDate);
//			} catch (JRException e) {
//				e.printStackTrace();
//				logger.info("Whole Sale " + e);
//			}

			try {
				JasperPdfReportService.PharmacyTaxInvoiceReport(voucherNumber, strDate);
			} catch (JRException e) {
				e.printStackTrace();
				logger.info("Whole Sale " + e);
			}
			logger.info("===========Whole Sale jasper print completed!!====================");

			System.out.println("=======salesTransHdr=======");
			System.out.println("Now Print Invoice completed");

			System.out.println("=======salesTransHdr=======");

			txtPaidamount.setDisable(false);
			txtCustomerMst.clear();

//		} else {
//			notifyMessage(1, "Please Enter Valid Amount!!!!");
//		}

			// -----------------version 5.0 surya
		}
		// ----------------- version 5.0 end

	}

	private double getTotalCardsPaid() {

		double TotalSavedReceiptsForTheVoucher = 0;

		ResponseEntity<List<SalesReceipts>> srList = RestCaller.getSalesReceiptsByTransHdrId(salesTransHdr.getId());
		List<SalesReceipts> salesReceiptList = srList.getBody();

		if (null != salesReceiptList && salesReceiptList.size() > 0) {

			for (int i = 0; i < salesReceiptList.size(); i++) {
				SalesReceipts salesReceipts = salesReceiptList.get(i);

				TotalSavedReceiptsForTheVoucher = TotalSavedReceiptsForTheVoucher + salesReceipts.getReceiptAmount();

			}

		}

		return TotalSavedReceiptsForTheVoucher;

	}

	private double getCashPaid() {

		double TotalCashPAidForTheVoucher = 0;

		String strCashPaid = txtPaidamount.getText();

		try {
			TotalCashPAidForTheVoucher = Double.parseDouble(strCashPaid);
		} catch (Exception e) {

		}
		//============no need of currency combo===========
		if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {
			if (!SystemSetting.getUser().getCompanyMst().getCurrencyName()
					.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
				Double fcTotalCashPAidForTheVoucher = RestCaller.getCompanyCurrencyAmountOfFC(
						TotalCashPAidForTheVoucher, cmbCurrency.getSelectionModel().getSelectedItem());
				if (fcTotalCashPAidForTheVoucher > 0) {
					TotalCashPAidForTheVoucher = fcTotalCashPAidForTheVoucher;
				}
			}
		}
		return TotalCashPAidForTheVoucher;

	}

	private double getBillToPay() {

		double TotalBillToPay = 0;

		String strBillToPay = txtCashtopay.getText();
		try {
			TotalBillToPay = Double.parseDouble(strBillToPay);
		} catch (Exception e) {

		}
		return TotalBillToPay;

	}

	private double getDiscount() {

		double totalDiscount = 0;

		if (discount_Enable.equalsIgnoreCase("YES")) {

			String strTotalDiscount = txtDiscount.getText();
			try {
				totalDiscount = Double.parseDouble(strTotalDiscount);
			} catch (Exception e) {

			}
		}

		return totalDiscount;
	}

	private void addItem() {

		AccountHeads customerMst = null;
		logger.info("Inside Whole Sale add item");

		if (txtQty.getText().trim().isEmpty()) {

			notifyMessage(5, " Please Enter Quantity...!!!",false);
			return;

		}
		if (custname.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Enter Customer Name...!!!",false);
			custname.requestFocus();
			return;
		}
		if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Select Item Name...!!!",false);
			txtItemname.requestFocus();
			return;
		}
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Type Quantity...!!!",false);
			txtQty.requestFocus();
			return;
		}

		if (txtBatch.getText().trim().isEmpty()) {
			notifyMessage(1, "Item Batch is not present!!!",false);
			txtQty.requestFocus();
			return;
		}

		if (null == cmbSaleType.getValue()) {
			notifyMessage(5, " Please Select Sale Type...!!!",false);
			cmbSaleType.requestFocus();
			return;
		}

		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

		ResponseEntity<ItemMst> respsentityItem = RestCaller.getItemByNameRequestParam(txtItemname.getText()); // itemmst
																												// =
		ItemMst itemMst = respsentityItem.getBody();

		ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(itemMst.getUnitId());

		cmbUnit.getItems().add(unitMst.getBody().getUnitName());

		ResponseEntity<UnitMst> getUnitBYItem = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

		ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
				getUnitBYItem.getBody().getId());
		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			if (null == getmulti.getBody()) {
				notifyMessage(5, "Please Add the item in Multi Unit",false);
				return;
			}
		}
		ArrayList items = new ArrayList();

		items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());

		Double chkQty = 0.0;
		String itemId = null;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			List element = (List) itr.next();
			chkQty = (Double) element.get(4);
			itemId = (String) element.get(7);
		}

		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if (chkQty < conversionQty) {
				notifyMessage(1, "Not in Stock!!!",false);
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
		} else if (chkQty < Double.parseDouble(txtQty.getText())) {
			notifyMessage(5, "Not in Stock!!!",false);
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();
			return;
		}
		if (null == salesTransHdr) {
			createSalesTransHdr();
		}

		if (null == salesTransHdr) {

			notifyMessage(2, "Error",false);
			return;
		}

		// ----------------------service

//		String batch = txtBatch.getText();
//		Double qty = Double.parseDouble(txtQty.getText());
//		Double rate = Double.parseDouble(txtRate.getText());
//		
//		
//		String salesResp = 	salesDtlService.AddSalesDtl(salesTransHdr.getId(),itemMst.getId(),
//				qty,rate);
//		
//		notifyMessage(5, salesResp);

		// --------------------service end

		Double itemsqty = 0.0;
		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
				getItem.getBody().getId(), txtBatch.getText());
		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
		if (saleListItemTable.size() > 1) {
			Double PrevQty = 0.0;
			for (SalesDtl saleDtl : saleListItemTable) {
				if (!saleDtl.getUnitId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
							getItem.getBody().getUnitId(), saleDtl.getQty());
				} else {
					PrevQty = saleDtl.getQty();
				}
				itemsqty = itemsqty + PrevQty;
			}
		} else {
			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId, txtBatch.getText());
		}
		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if (chkQty < itemsqty + conversionQty) {
				notifyMessage(1, "Not in Stock!!!",false);
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
		}

		else if (chkQty < itemsqty + Double.parseDouble(txtQty.getText())) {
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();
			notifyMessage(5, "No Stock!!",false);
			return;
		}
		if (null == salesDtl) {
			salesDtl = new SalesDtl();
		}
		if (null != salesDtl.getId()) {
			logger.info("Whole Sale delete sales Dtl started!!");
			System.out.println("toDeleteSale.getId()" + salesDtl.getId());
			RestCaller.deleteSalesDtl(salesDtl.getId());
			logger.info("Whole Sale delete sales Dtl completed!!");

		}

		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(txtItemname.getText());
		salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());

		String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();
		System.out.println(batch);
		salesDtl.setBatchCode(batch);
		logger.info("Whole Sale amount Calculation Started!!");
		Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());

		salesDtl.setQty(qty);

		salesDtl.setItemCode(txtItemcode.getText());
		Double mrpRateIncludingTax = 00.0;
		mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());

		salesDtl.setMrp(mrpRateIncludingTax);
		
		
		if(null != cmbCurrency.getValue() || !cmbCurrency.getSelectionModel().getSelectedItem().trim().isEmpty())
		{
			if(!SystemSetting.getUser().getCompanyMst().getCurrencyName().equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem()))
			{
				
				Double fcmrpRateIncludingTax = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), mrpRateIncludingTax);
				if(fcmrpRateIncludingTax>0)
				{
					salesDtl.setFcMrp(fcmrpRateIncludingTax);
				}
			}
		}

		Double taxRate = 00.0;

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
		salesDtl.setBarcode(item.getBarCode());
		salesDtl.setStandardPrice(item.getStandardPrice());

		if(null != cmbCurrency.getSelectionModel() || null != cmbCurrency.getSelectionModel().getSelectedItem())
		{
			if(!SystemSetting.getUser().getCompanyMst().getCurrencyName().equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem()))
			{
				
				//Double fcstprice = RestCaller.getFCRate(item.getStandardPrice(), cmbCurrency.getSelectionModel().getSelectedItem());
				Double fcstprice = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), item.getStandardPrice());
				if(fcstprice>0)
				{
					salesDtl.setFcStandardPrice(fcstprice);
				}
			}
		}
//		salesDtl.setUnitName(unitMst.getUnitName());

		salesDtl.setItemId(item.getId());
		if (null == cmbUnit.getValue()) {
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
			salesDtl.setUnitId(getUnit.getBody().getId());
			salesDtl.setUnitName(getUnit.getBody().getUnitName());
		} else {
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
			salesDtl.setUnitId(getUnit.getBody().getId());
			salesDtl.setUnitName(getUnit.getBody().getUnitName());
		}
		// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());
		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.getBody().size() > 0) {
			for (TaxMst taxMst : getTaxMst.getBody()) {

				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				String companyCountry = SystemSetting.getUser().getCompanyMst().getCountry();
				if(null == companyCountry)
				{
					companyCountry = "INDIA";
				}
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {
					logger.info(e.toString());

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}
				
				String igstType  = RestCaller.getIgstType(companyState,customerState,companyCountry);

				if (igstType.equalsIgnoreCase("CGSTSGST")) {
					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesDtl.setCgstTaxRate(taxMst.getTaxRate());
						salesDtl.setFcCgst(0.0);
//					BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesDtl.setSgstTaxRate(taxMst.getTaxRate());
						salesDtl.setFcSgst(0.0);
//					BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
//					Double Igst = ta
					ResponseEntity<TaxMst> taxMst1 = RestCaller.getTaxMstByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
					if (null != taxMst1.getBody()) {

						salesDtl.setTaxRate(taxMst1.getBody().getTaxRate());
					}
				} else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesDtl.setCgstTaxRate(0.0);
						salesDtl.setCgstAmount(0.0);
						salesDtl.setSgstTaxRate(0.0);
						salesDtl.setSgstAmount(0.0);
						salesDtl.setTaxRate(taxMst.getTaxRate());
						salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//						BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setIgstAmount(igstAmount.doubleValue());
					}
				}
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesDtl.setCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setCessAmount(cessAmount.doubleValue());
					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtl.setAddCessRate(taxMst.getTaxRate());
					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
							Double.valueOf(txtRate.getText()));
					salesDtl.setAddCessAmount(cessAmount.doubleValue());
				}

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax)
					/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate()
							+ salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
			BigDecimal addcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
		}

		else {
			if (null != item.getTaxRate()) {
				taxRate = item.getTaxRate();
			} else {
				taxRate = 0.0;
			}

			salesDtl.setTaxRate(taxRate);

			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
			salesDtl.setRate(rateBeforeTax);

			double sgstTaxRate = taxRate / 2;
			double cgstTaxRate = taxRate / 2;
			salesDtl.setCgstTaxRate(cgstTaxRate);

			salesDtl.setSgstTaxRate(sgstTaxRate);
			double cessAmount = 0.0;
			double cessRate = 0.0;

			if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();

					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

					System.out.println("rateBeforeTax---------" + rateBeforeTax);
					salesDtl.setRate(rateBeforeTax);

					cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

					/*
					 * Recalculate RateBefore Tax if Cess is applied
					 */

				}
			} else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}

			salesDtl.setCessRate(cessRate);
			salesDtl.setCessAmount(cessAmount);

			String companyState = SystemSetting.getUser().getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {
				logger.info(e.toString());

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}
			String gstType = RestCaller.getGSTTypeByCustomerState(customerState);

			if (gstType.equalsIgnoreCase("GST")) {
				salesDtl.setSgstTaxRate(taxRate / 2);

				salesDtl.setCgstTaxRate(taxRate / 2);

				salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setIgstAmount(0.0);

			} else {
				salesDtl.setSgstTaxRate(0.0);

				salesDtl.setCgstTaxRate(0.0);

				salesDtl.setCgstAmount(0.0);

				salesDtl.setSgstAmount(0.0);

				salesDtl.setIgstTaxRate(taxRate);
				salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

			}
		}

		BigDecimal settoamount = new BigDecimal(
				Double.parseDouble(txtQty.getText()) * Double.parseDouble(txtRate.getText()));
		settoamount = settoamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(settoamount.doubleValue());
		logger.info("Whole Sale AMOUNT CALCULATION COMPLETED!!");
		logger.info("Whole Sale Save sals Dtl!!");

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if (null != priceDefenitionMst) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
					salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if (null != priceDefinition) {
				salesDtl.setCostPrice(priceDefinition.getAmount());
			}
		}
		/*
		 * Save to FCFields
		 */
		//=================no need of currency combo==========
		if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {
			if (!SystemSetting.getUser().getCompanyMst().getCurrencyName()
					.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
//				Double fcAmount = RestCaller.getFCRate(salesDtl.getAmount(),
//						cmbCurrency.getSelectionModel().getSelectedItem());
				Double fcAmount = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), salesDtl.getAmount());

				if (fcAmount > 0) {
					salesDtl.setFcAmount(fcAmount);
				}
				salesDtl.setFcCgst(0.0);
				if (null != salesDtl.getDiscount()) {
					if (salesDtl.getDiscount() > 0) {
//						Double fcDiscount = RestCaller.getFCRate(salesDtl.getFcDiscount(),
//								cmbCurrency.getSelectionModel().getSelectedItem());
						Double fcDiscount = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), salesDtl.getFcDiscount());

						if (fcDiscount > 0) {
							salesDtl.setFcDiscount(fcDiscount);
						}
					}
				}
				//Double fcTaxRate = RestCaller.getFCRate(taxRate, cmbCurrency.getSelectionModel().getSelectedItem());
				Double fcTaxRate = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), taxRate);

				if (fcTaxRate > 0) {
					salesDtl.setFcTaxRate(fcTaxRate);
					salesDtl.setFcIgstRate(fcTaxRate);
					Double igstAmt = salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
//					Double fcigstAmt = RestCaller.getFCRate(igstAmt, cmbCurrency.getSelectionModel().getSelectedItem());
					Double fcigstAmt = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), igstAmt);

					if (fcigstAmt > 0) {
						salesDtl.setFcIgstAmount(fcigstAmt);
						salesDtl.setFcTaxAmount(fcigstAmt);
					}
				}
//				Double fcMrp = RestCaller.getFCRate(salesDtl.getMrp(),
//						cmbCurrency.getSelectionModel().getSelectedItem());
				Double fcMrp = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), salesDtl.getMrp());

				if (fcMrp > 0) {
					salesDtl.setFcMrp(fcMrp);

				}
//				Double fcRate = RestCaller.getFCRate(salesDtl.getRate(),
//						cmbCurrency.getSelectionModel().getSelectedItem());
				Double fcRate = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), salesDtl.getRate());

				if (fcRate > 0) {
					salesDtl.setFcRate(fcRate);
				}
//				Double fcStdPrice = RestCaller.getFCRate(salesDtl.getStandardPrice(),
//						cmbCurrency.getSelectionModel().getSelectedItem());
				Double fcStdPrice = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), salesDtl.getStandardPrice());

				if (fcStdPrice > 0) {
					salesDtl.setFcStandardPrice(fcStdPrice);
				}

			}
		}
		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		Boolean offer = CheckOfferWhileFinalSave(salesTransHdr);
		if (offer) {

		}
		salesDtl = respentity.getBody();

//		Boolean offerCheck = CheckOfferWhileItemAdding(salesTransHdr.getId(), salesDtl);

		logger.info("Whole Sale Save sals Dtl COMPLETED!!");
		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

		List<SalesDtl> salesDtlList = respentityList.getBody();
		System.out.print(salesDtlList.size()
				+ "sales dtl list size izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");

		/*
		 * Call Rest to get the summary and set that to the display fields
		 */

		// salesDtl.setTempAmount(amount);
		
		saleListTable.clear();
		saleListTable.setAll(salesDtlList);

		// ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		// salesDtl = respentity.getBody();

		// saleListTable.add(salesDtl);

		FillTable();

		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");

		// cmbUnit.getSelectionModel().clearSelection();
		txtItemcode.setText("");
		txtBatch.setText("");
		txtBarcode.requestFocus();
		salesDtl = new SalesDtl();
		logger.info("====================Whole Sale ADD ITEM FINISHED!!================");

	}

	@FXML
	void ShowLocalCustomerPopup(ActionEvent event) {
		showLocalCustPopup();
	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();

	}

	@FXML
	void deleteRow(ActionEvent event) {
		logger.info("===== WHOLE SALE DELETE ITEM STARTED ============");
		if (null != salesDtl) {
			if (null != salesDtl.getId()) {
				System.out.println("toDeleteSale.getId()" + salesDtl.getId());
				RestCaller.deleteSalesDtl(salesDtl.getId());
				txtItemname.clear();
				txtItemcode.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtQty.clear();
				ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
				saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
				FillTable();

				salesDtl = null;
				notifyMessage(5, " Item Deleted Successfully",true);
				logger.info("===== WHOLE SALE DELETE ITEM COMPLETED ============");
			}
		}

	}

	@FXML
	void hold(ActionEvent event) {
		txtBarcode.clear();
		txtBatch.clear();
		txtcardAmount.clear();
		txtCashtopay.clear();
		txtChangeamount.clear();
		txtYesCard.clear();
		txtSodexoCard.clear();
		txtSBICard.clear();
		txtRate.clear();
		txtQty.clear();
		txtPriceType.clear();
		txtItemcode.clear();
		txtItemname.clear();
		txtPaidamount.clear();
		custAdress.clear();
		custname.clear();
		custId = null;
		localCustId = null;
		saleListTable.clear();
		saleTypeTable.clear();
		cmbSaleType.getSelectionModel().clearSelection();
		salesDtl = null;
		salesTransHdr = null;
		tblCardAmountDetails.getItems().clear();
		receiptModeList.clear();
		txtcardAmount.clear();
		txtCrAmount.clear();
		notifyMessage(5, "Sales Holded",true);
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	private void showPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			String cst = null;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

//			ItemStockPopupCtl itemStockPopupCtl =fxmlLoader.getController();
//			itemStockPopupCtl.getCustomer(cst);

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();
			// txtBarcode.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {

	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {
		if (event.getCode() == KeyCode.BACK_SPACE) {

			//txtItemname.requestFocus();
			showPopup();
		}
		if (event.getCode() == KeyCode.ENTER) {
			if (txtBarcode.getText().trim().isEmpty())
				txtPaidamount.requestFocus();
		}
		if (event.getCode() == KeyCode.DOWN) {
			itemDetailTable.requestFocus();
		}
	}

	@FXML
	void onEnterCustPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();

		}
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		}
	}

	@FXML
	void save(ActionEvent event) {

		try {
			FinalSave();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void saveOnCtl(KeyEvent event) {

	}

	@FXML
	void saveOnkey(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()) {
			try {
				FinalSave();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnSave.setDisable(false);

			Double change = Double.parseDouble(txtPaidamount.getText()) - Double.parseDouble(txtFCCashToPay.getText());
			txtChangeamount.setText(Double.toString(change));
			btnSave.requestFocus();
		} else if (event.getCode() == KeyCode.LEFT) {
			visileCardSale();
			txtCardType.requestFocus();

		} else if (event.getCode() == KeyCode.UP) {
			txtBarcode.requestFocus();
		} else if (event.getCode() == KeyCode.BACK_SPACE) {
			if (txtPaidamount.getText().isEmpty()) {
				txtBarcode.requestFocus();
			}
		} else if (event.getCode() == KeyCode.S && event.isControlDown()) {
			try {
				FinalSave();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@FXML
	void unHold(ActionEvent event) {
		loadHoldedSales();

	}

	@Subscribe
	public void popupCustomerlistner(HoldedCustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			custname.setText(customerEvent.getCustomerName());
			custAdress.setText(customerEvent.getAddress());
			custId = customerEvent.getCustomerId();
			ResponseEntity<PriceDefenitionMst> priceDef = RestCaller.getPriceNameById(customerEvent.getCustPriceTYpe());
			if (null != priceDef.getBody()) {
				txtPriceType.setText(priceDef.getBody().getPriceLevelName());
			}
			salesTransHdr = RestCaller.getSalesTransHdr(customerEvent.getHdrId());
			cmbSaleType.setValue(salesTransHdr.getVoucherType());
			cmbSaleType.getSelectionModel().select(salesTransHdr.getVoucherType());
			saleListTable.clear();
			ResponseEntity<List<SalesDtl>> respentity = RestCaller.getSalesDtl(salesTransHdr);
			saleListTable = FXCollections.observableArrayList(respentity.getBody());
			FillTable();
			getAllCardAmount();
			cmbSaleType.getSelectionModel().select(salesTransHdr.getVoucherType().toString());

		}

	}

	private void getAllCardAmount() {
		visileCardSale();
		tblCardAmountDetails.getItems().clear();
		ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
				.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
		salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

		try {
			cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
		} catch (Exception e) {

			txtcardAmount.setText("");
		}

		filltableCardAmount();
	}

	private void loadHoldedSales() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/HoldedCustomer.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtBarcode.requestFocus();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		if (salesTransHdr != null) {

			Alert a = new Alert(AlertType.CONFIRMATION);
			a.setHeaderText("Changing Customer...");
			a.setContentText("The details will be deleted");
			a.showAndWait().ifPresent((btnType) -> {
				if (btnType == ButtonType.OK) {

					deleteAllSalesDtl();

					try {

						FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/PatientPopUp.fxml"));

						Parent root1;

						root1 = (Parent) fxmlLoader.load();
						Stage stage = new Stage();

						stage.initModality(Modality.APPLICATION_MODAL);
						stage.initStyle(StageStyle.UNDECORATED);
						stage.setTitle("ABC");
						stage.initModality(Modality.APPLICATION_MODAL);
						stage.setScene(new Scene(root1));
						stage.show();
						
						txtItemname.requestFocus();
					} catch (IOException e) {
						//
						e.printStackTrace();
					}
				} else if (btnType == ButtonType.CANCEL) {

					return;

				}
			});
		} else {
			try {

				txtLocalCustomer.clear();
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/PatientPopUp.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				
				txtItemname.requestFocus();
			} catch (IOException e) {
				//
				e.printStackTrace();
			}
		}
	}

	private void deleteAllSalesDtl() {

		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		List<SalesDtl> saledtlList = saledtlSaved.getBody();
		for (SalesDtl sales : saledtlList) {
			RestCaller.deleteSalesDtl(sales.getId());
		}

		ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
		saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
		if (saleListTable.size() == 0) {
			RestCaller.deleteSalesTransHdr(salesTransHdr.getId());
			salesTransHdr = null;
			saleListTable.clear();
			itemDetailTable.getItems().clear();
		}

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();

		if (stage.isShowing()) {

			cmbUnit.getItems().clear();
			itemNameProperty.set(itemPopupEvent.getItemName());
			// salesDtl.setItemName(itemNameProperty.get());
			txtBarcode.setText(itemPopupEvent.getBarCode());
			txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
			batchProperty.set(itemPopupEvent.getBatch());
			cmbUnit.getItems().add(itemPopupEvent.getUnitName());
			cmbUnit.setValue(itemPopupEvent.getUnitName());
			ResponseEntity<AccountHeads> custMst = RestCaller.getAccountHeadsById(custId);
			java.util.Date udate = SystemSetting.getApplicationDate();
			String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

			ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
					itemPopupEvent.getItemId(), custMst.getBody().getPriceTypeId(), itemPopupEvent.getUnitId(),
					itemPopupEvent.getBatch(), sdate);
			if (null != batchPriceDef.getBody()) {
				txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
			} else {
				ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
						itemPopupEvent.getItemId(), custMst.getBody().getPriceTypeId(), itemPopupEvent.getUnitId(),
						sdate);

				if (null != pricebyItem.getBody()) {
					txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
				}
			}
			ResponseEntity<List<BatchPriceDefinition>> batchPrice = RestCaller.getBatchPriceDefinitionByItemId(
					itemPopupEvent.getItemId(), custMst.getBody().getPriceTypeId(), txtBatch.getText(), sdate);
			BatchpriceDefenitionList = FXCollections.observableArrayList(batchPrice.getBody());
			if (BatchpriceDefenitionList.size() > 0) {
				for (BatchPriceDefinition priceDef : BatchpriceDefenitionList) {
					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
					if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
						cmbUnit.getItems().add(getUnit.getBody().getUnitName());
						cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
					}
				}
			}

//				cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
//			 
//				txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
//				txtTax.setText(Double.toString(itemPopupEvent.getTaxRate()));
//			 
//			
//			
// 

			else {

				ResponseEntity<List<PriceDefinition>> price = RestCaller.getPriceByItemId(itemPopupEvent.getItemId(),
						custMst.getBody().getPriceTypeId());
				priceDefenitionList = FXCollections.observableArrayList(price.getBody());

				if (priceDefenitionList.size() > 0) {
					// cmbUnit.getItems().clear();
					for (PriceDefinition priceDef : priceDefenitionList) {
						ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
						if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
							cmbUnit.getItems().add(getUnit.getBody().getUnitName());
							cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
						}
					}
				}
			}

			ResponseEntity<TaxMst> taxMst = RestCaller.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
			if (null != taxMst.getBody()) {
			}
			if (null != itemPopupEvent.getExpiryDate()) {
				// salesDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
			}
			if (null != itemPopupEvent.getItemPriceLock()) {
				if (itemPopupEvent.getItemPriceLock().equalsIgnoreCase("YES")) {
					txtRate.setEditable(false);
				} else {
					txtRate.setEditable(true);
				}
			}
			System.out.println(itemPopupEvent.toString());
		}

	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			String customerSite = SystemSetting.customer_site_selection;
			if (customerSite.equalsIgnoreCase("TRUE")) {
				txtLocalCustomer.setDisable(false);
				txtLocalCustomer.clear();
			}

			custname.setText(customerEvent.getCustomerName());
			custAdress.setText(customerEvent.getCustomerAddress());
			custId = customerEvent.getCustId();
			ResponseEntity<AccountHeads> custMst = RestCaller.getAccountHeadsById(custId);
			ResponseEntity<PriceDefenitionMst> priceDef = RestCaller
					.getPriceNameById(custMst.getBody().getPriceTypeId());
			if (null != priceDef.getBody()) {
				txtPriceType.setText(priceDef.getBody().getPriceLevelName());

			} else {
				txtPriceType.clear();

			}
			ResponseEntity<BranchMst> branchMstResp = RestCaller.getBranchMstByName(customerEvent.getCustomerName());

			if (null != branchMstResp.getBody()) {
				customerIsBranch = true;
			}

		}
		ResponseEntity<PatientMst> patientMst = RestCaller.getPatientMstById(customerEvent.getCustId());
		if (null != patientMst.getBody()) {
			ResponseEntity<InsuranceCompanyMst> insuranceComp = RestCaller
					.getinsuraCompanyMstById(patientMst.getBody().getInsuranceCompanyId());
			txtInsuranceCompany.setText(insuranceComp.getBody().getInsuranceCompanyName());
			txtPolicyType.setText(patientMst.getBody().getPolicyType());
			txtPercentageCoverage.setText(patientMst.getBody().getPercentageCoverage());
//			txt
		}
//		String sdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");

//		Double custBalance = RestCaller.getCustomerBalance(custId, sdate);
//		BigDecimal bCustBalance = new BigDecimal(custBalance);
//		bCustBalance = bCustBalance.setScale(2,BigDecimal.ROUND_HALF_EVEN);
//		txtPreviousBalance.setText(bCustBalance.toString());

	}

	@Subscribe
	public void popupPatientEventListner(PatientEvent patientEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			
			txtPaidamount.setDisable(false);
			AddCardAmount.setDisable(false);
			txtInsuranceCompany.clear();
			txtPercentageCoverage.clear();
			txtPolicyType.clear();
			txtCustomerMst.clear();
			
			ResponseEntity<PatientMst> patientMstResp = RestCaller.getPatientMstById(patientEvent.getId());
			PatientMst patientMst = patientMstResp.getBody();

			if (null != patientMst) {

				custname.setText(patientMst.getPatientName());
				custAdress.setText(patientMst.getAddress1());
				txtNationality.setText(patientMst.getNationality());
				if (null != patientMst.getInsuranceCompanyId()) {
					ResponseEntity<InsuranceCompanyMst> insuranceMstResp = RestCaller
							.getinsuraCompanyMstById(patientMst.getInsuranceCompanyId());
					InsuranceCompanyMst insuranceCompanyMst = insuranceMstResp.getBody();

					if (null != insuranceCompanyMst) {
						txtInsuranceCompany.setText(insuranceCompanyMst.getInsuranceCompanyName());
						txtPaidamount.setDisable(true);
						// ===========commented by anandu due to the intereption for running the
						// client========
//						txtPercentageCoverage.setText(insuranceCompanyMst.getPercentageCoverage());
//						txtPolicyType.setText(insuranceCompanyMst.getPolicyType());
						if (null != patientMst.getPolicyType()) {
							txtPercentageCoverage.setText(patientMst.getPercentageCoverage());
							txtPolicyType.setText(patientMst.getPolicyType());
						}

					}
				}

				if (null != patientMst.getAccountHeads()) {
					String currencyName="";
					//txtCustomerMst.setText(patientMst.getCustomerMst().getCustomerName());
					if(null != patientMst.getAccountHeads().getCurrencyId()) {
						ResponseEntity<CurrencyMst> currencyDetails=RestCaller.getcurrencyMsyById(patientMst.getAccountHeads().getCurrencyId());
						currencyName=currencyDetails.getBody().getCurrencyName();
					
					String companyCurrencyName=SystemSetting.getUser().getCompanyMst().getCurrencyName();
					
					if(!currencyName.equalsIgnoreCase(companyCurrencyName)) {
						//notifyMessage(3,"Invalid Customer!!!",false);	
						
						custname.clear();
						
						txtNationality.clear();
						custAdress.clear();
						txtCustomerMst.clear();
						custname.requestFocus();
						//notifyMessage(3, "invalid customer ",false);
						
						return;
					}
					
					}
					
						txtCustomerMst.setText(patientMst.getAccountHeads().getAccountName());
						txtPaidamount.setDisable(true);
						AddCardAmount.setDisable(true);
						//txtItemname.requestFocus();
				
					//txtPaidamount.setDisable(true);
				}
				
			}

		}
		
		
		
	}



	private Boolean CheckOfferWhileFinalSave(SalesTransHdr SalesTransHdr) {

		ResponseEntity<List<SchemeInstance>> schemeResp = RestCaller.getActiveSchemeDeteils("Retail");
		List<SchemeInstance> schemeInstanceList = schemeResp.getBody();
		if (schemeInstanceList.size() > 0) {

			for (SchemeInstance scheme : schemeInstanceList) {
				Boolean validityPeriod = CheckValidityPeriod(scheme);
				if (validityPeriod) {

					if (!scheme.getOfferId().equalsIgnoreCase("2")) {

						ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(SalesTransHdr);
						List<SalesDtl> SalesDtlList = respentityList.getBody();

						if (SalesDtlList.size() > 0) {

							for (SalesDtl salesDtl : SalesDtlList) {
								Boolean eligible = eligibilityCheck(scheme, SalesTransHdr.getId(), salesDtl);
								if (eligible) {
									return true;
								} else {
									return false;
								}

							}

						}
					} else if (scheme.getEligibilityId().equalsIgnoreCase("7")) {
						ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(SalesTransHdr);
						List<SalesDtl> SalesDtlList = respentityList.getBody();

						if (SalesDtlList.size() > 0) {

							for (SalesDtl salesDtl : SalesDtlList) {
								Boolean eligible = eligibilityCheck(scheme, SalesTransHdr.getId(), salesDtl);
								if (eligible) {
									return true;
								} else {
									return false;
								}

							}

						}
					}

				}
			}

		}

		return false;

	}

	private Boolean eligibilityCheck(SchemeInstance scheme, String salesTransHdrId, SalesDtl salesDtl) {

		ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(salesDtl.getItemId());
		ItemMst itemMst = itemResp.getBody();

		ResponseEntity<List<SchEligibilityAttribInst>> schEligibilityAttribInstResp = RestCaller
				.getSchEligibilityAttrInstBySchemeId(scheme.getId());
		List<SchEligibilityAttribInst> schEligibilityAttribInstList = schEligibilityAttribInstResp.getBody();
		if (schEligibilityAttribInstList.size() <= 0) {
			return false;
		}

		for (SchEligibilityAttribInst inst : schEligibilityAttribInstList) {

			if (inst.getEligibilityId().equals("1")) {

				ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller
						.SchEligibilityAttribInstByAttribName("ITEMNAME", inst.getSchemeId());
				SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();

				if (schEligibilityAttribInst.getAttributeValue().equals(itemMst.getItemName())) {
					Boolean validQty = checkItemAndQty(inst, itemMst, salesTransHdrId, scheme, salesDtl);
					if (!validQty) {

						return false;

					} else {
						return true;
					}
				}

			}

			if (inst.getEligibilityId().equals("2")) {

				ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller
						.SchEligibilityAttribInstByAttribName("CATEGORY", inst.getSchemeId());
				SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();

				ResponseEntity<CategoryMst> categoryMstResp = RestCaller.getCategoryById(itemMst.getCategoryId());
				CategoryMst categoryMst = categoryMstResp.getBody();

				if (schEligibilityAttribInst.getAttributeValue().equals(categoryMst.getCategoryName())) {
					Boolean validCat = checkCategoryQty(categoryMst, inst, salesTransHdrId, scheme, salesDtl);
					if (!validCat) {
						return false;
					} else {
						return false;
					}
				}

			}

			if (inst.getEligibilityId().equals("4")) {

				ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller
						.SchEligibilityAttribInstByAttribName("ITEMNAME", inst.getSchemeId());
				SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();

				if (schEligibilityAttribInst.getAttributeValue().equals(itemMst.getItemName())) {
					Boolean validAmount = checkItemAndAmount(inst, itemMst, salesTransHdrId, scheme, salesDtl);
					if (!validAmount) {
						return false;
					} else {
						return false;
					}
				}

			}

			if (inst.getEligibilityId().equals("5")) {

				ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller
						.SchEligibilityAttribInstByAttribName("CATEGORY", inst.getSchemeId());
				SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();

				ResponseEntity<CategoryMst> categoryMstResp = RestCaller.getCategoryById(itemMst.getCategoryId());
				CategoryMst categoryMst = categoryMstResp.getBody();
				if (schEligibilityAttribInst.getAttributeValue().equals(categoryMst.getCategoryName())) {
					Boolean validCatAmount = checkCategoryAmount(inst, categoryMst, salesTransHdrId, scheme);
					if (!validCatAmount) {
						return false;
					} else {
						return true;
					}
				}

			}

			if (inst.getEligibilityId().equals("8")) {

				Boolean validItemBtach = CheckItemBatchValidity(inst, salesTransHdrId, salesDtl, itemMst, scheme);
				if (!validItemBtach) {
					return false;
				}

			}

			if (inst.getEligibilityId().equals("7") && scheme.getOfferId().equals("2")) {

				Boolean validInvoice = ChecknvoiceAmount(inst, salesTransHdrId, scheme);

				return false;

			}

		}

		return true;
	}

	private Boolean ChecknvoiceAmount(SchEligibilityAttribInst inst, String salesTransHdrId, SchemeInstance scheme) {

		Double eligibAmount = 0.0;

		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdrId);

		ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller
				.SchEligibilityAttribInstByAttribName("AMOUNT", scheme.getId());

		SchEligibilityAttribInst eligib = eligibleResp.getBody();
		eligibAmount = Double.parseDouble(eligib.getAttributeValue());

		SalesDtl salesDtl = new SalesDtl();
		salesDtl.setAmount(summary.getTotalAmount());

		if (eligibAmount <= summary.getTotalAmount()) {
			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, eligibAmount, "ITEM_AND_QTY");
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, eligibAmount, "ITEM_AND_QTY");
				return true;
			}

			return true;
		}
		return false;

	}

	private Boolean CheckItemBatchValidity(SchEligibilityAttribInst inst, String salesTransHdrId, SalesDtl salesDtl,
			ItemMst itemMst, SchemeInstance scheme) {

		ResponseEntity<List<SchEligibilityAttribInst>> eligibleInstResp = RestCaller
				.getSchEligibAttrInstByEligibIdAndSchemeId(inst.getEligibilityId(), inst.getSchemeId());

		List<SchEligibilityAttribInst> eligiblrList = eligibleInstResp.getBody();

		if (eligiblrList.size() <= 0) {
			return false;
		}
		String itemName = "";
		String batchCode = "";
		Double qty = 0.0;

		for (SchEligibilityAttribInst eligible : eligiblrList) {
			if (eligible.getAttributeName().equalsIgnoreCase("ITEMNAME")) {
				if (null == eligible.getAttributeValue()) {
					return false;
				}
				itemName = eligible.getAttributeValue();
			}

			if (eligible.getAttributeName().equalsIgnoreCase("QTY")) {
				if (null == eligible.getAttributeValue()) {
					return false;
				}
				qty = Double.parseDouble(eligible.getAttributeValue());
			}

			if (eligible.getAttributeName().equalsIgnoreCase("BATCH_CODE")) {

				if (null == eligible.getAttributeValue()) {
					return false;
				}
				batchCode = eligible.getAttributeValue();
			}

		}

		if (itemName.equals(itemMst.getItemName()) && batchCode.equals(salesDtl.getBatch())) {

			ResponseEntity<List<SalesDtl>> salesDtlResp = RestCaller.getSalesDtlItemAndBatch(itemMst.getId(),
					salesDtl.getBatch(), salesTransHdrId);
			List<SalesDtl> salesDtlList = salesDtlResp.getBody();

			if (salesDtlList.size() <= 0) {
				return false;
			}
			Double salesQty = 0.0;
			for (SalesDtl sales : salesDtlList) {
				ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(sales.getItemId());
				itemMst = itemMstResp.getBody();

				if (null != itemMst) {
					if (!itemMst.getUnitId().equals(sales.getUnitId())) {
						salesQty = salesQty + RestCaller.getConversionQty(itemMst.getId(), sales.getUnitId(),
								itemMst.getUnitId(), sales.getQty());
					} else {
						salesQty = salesQty + sales.getQty();
					}

				}
			}

			if (qty <= salesQty) {

				if (scheme.getOfferId().equals("2")) {

					AddOfferQtyCheck(scheme, salesDtl, qty, "ITEM_AND_QTY");
					return true;
				}

				if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

					AddAmountDiscount(scheme, salesDtl, qty, "ITEM_AND_QTY");
					return true;
				}

				return true;
			}

		}
		return false;

	}

	private Boolean checkCategoryAmount(SchEligibilityAttribInst inst, CategoryMst categoryMst, String salesTransHdrId,
			SchemeInstance scheme) {
		Double amount = 0.0;
		ResponseEntity<List<SalesDtl>> salesDtlListResp = RestCaller
				.getSalesDtlsByCategoryIdAndSalesTransHdr(categoryMst.getId(), salesTransHdrId);

		List<SalesDtl> salesDtlList = salesDtlListResp.getBody();

		if (salesDtlList.size() > 0) {
			for (SalesDtl sales : salesDtlList) {
				amount = amount + sales.getAmount();
			}
		}

		String offerRefId = salesDtlList.get(0).getOfferReferenceId();
		salesDtl.setOfferReferenceId(offerRefId);

		salesDtl.setAmount(amount);

		ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller
				.SchEligibilityAttribInstByAttribName("AMOUNT", scheme.getId());
		SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();

		Double offerAmount = Double.parseDouble(schEligibilityAttribInst.getAttributeValue());
		if (amount >= offerAmount) {

			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, offerAmount, "CATEGORY_AND_QTY");
				return true;
			}
			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, offerAmount, "ITEM_AND_QTY");
				return true;
			}
		}

		return false;
	}

	private Boolean checkItemAndAmount(SchEligibilityAttribInst inst, ItemMst itemMst, String salesTransHdrId,
			SchemeInstance scheme, SalesDtl salesDtl) {

		Double amount = 0.0;

		ResponseEntity<List<SalesDtl>> salesDtlsListResp = RestCaller.getSalesDtlsByItemId(salesTransHdrId,
				itemMst.getId());
		List<SalesDtl> salesDtlList = salesDtlsListResp.getBody();

		if (salesDtlList.size() <= 0) {

			return false;

		}

		String offerRefId = salesDtlList.get(0).getOfferReferenceId();
		salesDtl.setOfferReferenceId(offerRefId);

		for (SalesDtl salesdtl : salesDtlList) {

			amount = amount + salesdtl.getAmount();

		}
		salesDtl.setAmount(amount);
		ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller
				.SchEligibilityAttribInstByAttribName("AMOUNT", scheme.getId());
		SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();

		Double offerAmount = Double.parseDouble(schEligibilityAttribInst.getAttributeValue());
		if (amount >= offerAmount) {

			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, offerAmount, "ITEM_AND_QTY");
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, offerAmount, "ITEM_AND_QTY");
				return true;
			}

		}

		return false;
	}

	private Boolean checkCategoryQty(CategoryMst categoryMst, SchEligibilityAttribInst inst, String salesTransHdrId,
			SchemeInstance scheme, SalesDtl salesDtl) {

		Double qty = 0.0;
		String offerRefId = null;
		ResponseEntity<List<SalesDtl>> salesDtlListResp = RestCaller
				.getSalesDtlsByCategoryIdAndSalesTransHdr(categoryMst.getId(), salesTransHdrId);

		List<SalesDtl> salesDtlList = salesDtlListResp.getBody();

		if (salesDtlList.size() > 0) {
			offerRefId = salesDtlList.get(0).getOfferReferenceId();
			for (SalesDtl sales : salesDtlList) {
				ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(sales.getItemId());
				ItemMst itemMst = itemMstResp.getBody();

				if (null != itemMst) {
					if (!itemMst.getUnitId().equals(sales.getUnitId())) {
						qty = qty + RestCaller.getConversionQty(itemMst.getId(), sales.getUnitId(), itemMst.getUnitId(),
								sales.getQty());
					} else {
						qty = qty + sales.getQty();
					}

				}

			}
		}

		ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller.SchEligibilityAttribInstByAttribName("QTY",
				inst.getSchemeId());
		SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();

		Double instQty = Double.parseDouble(schEligibilityAttribInst.getAttributeValue());
		salesDtl.setOfferReferenceId(offerRefId);

		salesDtl.setQty(qty);
		if (qty >= instQty) {

			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, instQty, "CATEGORY_AND_QTY");
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, instQty, "ITEM_AND_QTY");
				return true;
			}

		}

		return false;

	}

	private Boolean checkItemAndQty(SchEligibilityAttribInst inst, ItemMst itemMst, String salesTransHdrId,
			SchemeInstance scheme, SalesDtl salesDtl) {

		ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller.SchEligibilityAttribInstByAttribName("QTY",
				inst.getSchemeId());
		SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();

		Double eligQty = Double.parseDouble(schEligibilityAttribInst.getAttributeValue());

		if (eligQty <= salesDtl.getQty()) {

			if (scheme.getOfferId().equals("2")) {

				AddOfferQtyCheck(scheme, salesDtl, eligQty, "ITEM_AND_QTY");
				return true;
			}

			if (scheme.getOfferId().equals("1") || scheme.getOfferId().equals("4")) {

				AddAmountDiscount(scheme, salesDtl, eligQty, "ITEM_AND_QTY");
				return true;
			}
			// privilage points remains---- done by client requirement customer

		}

		return false;
	}

	private void AddAmountDiscount(SchemeInstance scheme, SalesDtl salesDtl2, Double eligQty, String string) {

		Double discount = 0.0;
		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());

		ResponseEntity<List<SchOfferAttrInst>> offerInstResp = RestCaller.getSchOfferAttrInstBySchemeId(scheme.getId());
		List<SchOfferAttrInst> offerInstList = offerInstResp.getBody();

		for (SchOfferAttrInst offer : offerInstList) {
			if (offer.getAttributeName().equalsIgnoreCase("DISCOUNT")) {
				discount = Double.parseDouble(offer.getAttributeValue());
			}
			if (offer.getAttributeName().equalsIgnoreCase("PERCENTAGE DISCOUNT")) {
				discount = Double.parseDouble(offer.getAttributeValue());
			}
		}

		if (scheme.getOfferId().equals("1")) {

			salesTransHdr.setInvoiceDiscount(discount);

			discount = (discount / summary.getTotalAmount()) * 100;
			String discountper = discount + "%";

			salesTransHdr.setDiscount(discountper);
		}
		if (scheme.getOfferId().equals("4")) {

			salesTransHdr.setDiscount(discount + "%");

			discount = summary.getTotalAmount() * discount / 100;
			salesTransHdr.setInvoiceDiscount(discount);

		}

		return;

	}

	private void AddOfferQtyCheck(SchemeInstance scheme, SalesDtl salesDtl, Double eligQty, String eligibilityType) {

		String itemName = "";
		String batch = "";
		String mutipleAllowed = "";
		Double offerQty = 0.0;

		ResponseEntity<List<SchOfferAttrInst>> offerInstResp = RestCaller.getSchOfferAttrInstBySchemeId(scheme.getId());
		List<SchOfferAttrInst> offerInstList = offerInstResp.getBody();

		if (offerInstList.size() > 0) {
			for (SchOfferAttrInst offer : offerInstList) {
				if (offer.getAttributeName().equalsIgnoreCase("ITEMNAME")) {
					itemName = offer.getAttributeValue();
				}

				if (offer.getAttributeName().equalsIgnoreCase("QTY")) {
					offerQty = Double.parseDouble(offer.getAttributeValue());
				}

				if (offer.getAttributeName().equalsIgnoreCase("BATCH_CODE")) {
					batch = offer.getAttributeValue();
				}

				if (offer.getAttributeName().equalsIgnoreCase("MULTIPLE ALLOWED")) {
					mutipleAllowed = offer.getAttributeValue();
				}
			}
		}

		addOfferItem(itemName, offerQty, batch, mutipleAllowed, salesDtl, eligQty, eligibilityType, scheme);

	}

	private void addOfferItem(String itemName, Double offerQty, String batch, String mutipleAllowed,
			SalesDtl salesDtlref, Double eligQty, String eligibilityType, SchemeInstance scheme) {

		ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(itemName);
		ItemMst itemMst = itemResp.getBody();
		int itemqty = 0;

		if (mutipleAllowed.equalsIgnoreCase("YES")) {

			ResponseEntity<SchEligibilityAttribInst> eligibleResp = RestCaller
					.SchEligibilityAttribInstByAttribName("QTY", scheme.getId());
			SchEligibilityAttribInst schEligibilityAttribInst = eligibleResp.getBody();
			if (null != schEligibilityAttribInst) {
				itemqty = (int) (salesDtlref.getQty() / eligQty);
				itemqty = (int) (itemqty * offerQty);
			}

			ResponseEntity<SchEligibilityAttribInst> eligibleAmtResp = RestCaller
					.SchEligibilityAttribInstByAttribName("AMOUNT", scheme.getId());
			SchEligibilityAttribInst schEligibilityAttribInstAmt = eligibleAmtResp.getBody();

			if (null != schEligibilityAttribInstAmt) {
				itemqty = (int) (salesDtlref.getAmount() / eligQty);

				itemqty = (int) (itemqty * offerQty);
			}

			System.out.println("--mutipleAllowed---Qty-------" + itemqty);
		}

		if (null != salesDtlref.getOfferReferenceId()) {

			ResponseEntity<List<SalesDtl>> salesDtlOfferResp = RestCaller
					.getSalesDtlByOfferReferenceId(salesDtlref.getOfferReferenceId(), salesTransHdr.getId());
			List<SalesDtl> salesDtlOfferList = salesDtlOfferResp.getBody();

			for (SalesDtl sales : salesDtlOfferList) {
				RestCaller.deleteSalesDtl(sales.getId());
			}
		}

		salesDtl = new SalesDtl();

		salesDtl.setSalesTransHdr(salesTransHdr);
//		salesDtl.setOfferReferenceId(salesDtlref.getId());
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setSchemeId(scheme.getId());

		salesDtl.setBatchCode(batch);

		if (itemqty > 0) {
			salesDtl.setQty((double) itemqty);
			System.out.println("-----Qty-------" + salesDtl.getQty());
		} else {
			salesDtl.setQty((double) offerQty);
			System.out.println("-----Qty-------" + salesDtl.getQty());
		}

		ResponseEntity<UnitMst> unitResp = RestCaller.getunitMst(itemMst.getUnitId());
		UnitMst unitMst = unitResp.getBody();
		if (null != unitMst) {
			salesDtl.setUnitName(unitMst.getUnitName());
		}
		salesDtl.setUnitId(itemMst.getUnitId());

		salesDtl.setItemCode(itemMst.getItemCode());
		Double mrpRateIncludingTax = 00.0;
		mrpRateIncludingTax = itemMst.getStandardPrice();
		salesDtl.setMrp(0.0);
		salesDtl.setTaxRate(itemMst.getTaxRate());
		salesDtl.setItemId(itemMst.getId());

		salesDtl.setRate(0.0);

		double cessRate = itemMst.getCess();
		double cessAmount = 0.0;
		if (cessRate > 0) {
			cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;
		}

		salesDtl.setCessRate(cessRate);

		salesDtl.setCessAmount(cessAmount);

		salesDtl.setSgstTaxRate(itemMst.getTaxRate() / 2);

		salesDtl.setCgstTaxRate(itemMst.getTaxRate() / 2);

		salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);
		salesDtl.setAmount(0.0);
		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);

		salesDtl = respentity.getBody();

		if (eligibilityType.equalsIgnoreCase("CATEGORY_AND_QTY")) {

			updateSalesDtlsByCategory(salesDtlref, salesDtl);
		} else if (eligibilityType.equalsIgnoreCase("ITEM_AND_QTY")) {
			udateSalesDtlsbyItem(salesDtlref, salesDtl, scheme);
		}

//		
//		salesDtlref.setOfferReferenceId(salesDtl.getId());
//		ResponseEntity<SalesDtl> respentity1 = RestCaller.saveSalesDtl(salesDtlref);

		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());

		List<SalesDtl> salesDtlList = respentityList.getBody();

		saleListTable.clear();
		saleListTable.setAll(salesDtlList);
		FillTable();

	}

	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());

		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());

//		salesTransHdr.setCashPaidSale(amount);
//			salespos.setTempAmount(amount);
		// saleListTable.add(salespos);
		if (null != summary.getTotalAmount()) {

			salesTransHdr.setInvoiceAmount(summary.getTotalAmount());
			txtTotalInvoice.setText(summary.getTotalAmount() + "");
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtCashtopay.setText(bdCashToPay.toPlainString());
			
			//=============no need of currency combo==========
			if (null == cmbCurrency.getSelectionModel() || null == cmbCurrency.getSelectionModel().getSelectedItem()) {
				txtFCCashToPay.setText("0.0");
			} else if (SystemSetting.getUser().getCompanyMst().getCurrencyName()
					.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
				txtFCCashToPay.setText("0.0");
			} else {
//				Double fcRate = RestCaller.getFCRate(bdCashToPay.doubleValue(),
//						cmbCurrency.getSelectionModel().getSelectedItem());
				Double fcRate = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),cmbCurrency.getSelectionModel().getSelectedItem(), bdCashToPay.doubleValue());

				if (fcRate > 0) {
					txtFCCashToPay.setText(Double.toString(fcRate));
				} else {
					notifyMessage(3, "Currency Rate not Set",false);
					return;
				}
			}
		} else {
			txtCashtopay.setText("");
		}

		getAllCardAmount();
		
		if(!txtInsuranceCompany.getText().isEmpty()) {
			double cardAmount = 0; 
			String strCardPaid1 = txtcardAmount.getText();
			try {
				cardAmount = Double.parseDouble(strCardPaid1);
			} catch (Exception e) {

			}
			
			double totalInvoice = 0; 
			String strtotalInvoice = txtTotalInvoice.getText();
			try {
				totalInvoice = Double.parseDouble(strtotalInvoice);
			} catch (Exception e) {

			}
			
			if(cardAmount == totalInvoice) {
				txtPaidamount.setDisable(true);
 			}
			else {
				txtPaidamount.setDisable(false);
			}
			
		}

	}

	private void updateSalesDtlsByCategory(SalesDtl salesDtlref, SalesDtl salesDtl2) {

		ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(salesDtlref.getItemId());
		ItemMst itemMst = itemResp.getBody();

		ResponseEntity<List<SalesDtl>> salesDtlListResp = RestCaller.getSalesDtlsByCategoryIdAndSalesTransHdr(
				itemMst.getCategoryId(), salesDtlref.getSalesTransHdr().getId());

		List<SalesDtl> salesDtlList = salesDtlListResp.getBody();

		for (SalesDtl sales : salesDtlList) {
			sales.setOfferReferenceId(salesDtl2.getId());
			RestCaller.updateSalesDlt(sales);
		}
	}

	private void udateSalesDtlsbyItem(SalesDtl salesDtlref, SalesDtl salesDtl2, SchemeInstance scheme) {

		if (scheme.getEligibilityId().equalsIgnoreCase("7")) {
			ResponseEntity<List<SalesDtl>> salesDtlResp = RestCaller.getSalesDtl(salesTransHdr);
			List<SalesDtl> salesDtlList = salesDtlResp.getBody();

			for (SalesDtl sales : salesDtlList) {
				salesDtlref.setOfferReferenceId(salesDtl2.getId());
				RestCaller.updateSalesDlt(sales);
			}
		} else {
			salesDtlref.setOfferReferenceId(salesDtl2.getId());
			RestCaller.updateSalesDlt(salesDtlref);
		}

	}

	private Boolean CheckValidityPeriod(SchemeInstance scheme) {

		ResponseEntity<List<SchSelectionAttribInst>> selectAttrInstResp = RestCaller
				.getSchSelectionAttribInstBySchemeId(scheme.getId());
		List<SchSelectionAttribInst> selectionAttrInstList = selectAttrInstResp.getBody();
		if (selectionAttrInstList.size() <= 0) {
			return false;
		}

		for (SchSelectionAttribInst select : selectionAttrInstList) {

			if (select.getAttributeType().equalsIgnoreCase("DATE")) {

				if (select.getAttributeName().equalsIgnoreCase("START DATE")) {
					Boolean validStartDate = validStartDate(select);
					if (!validStartDate) {
						return false;
					}
				}

				if (select.getAttributeName().equalsIgnoreCase("END DATE")) {
					Boolean validEndDate = validEndDate(select);
					if (!validEndDate) {
						return false;
					}
				}

			}

			if (select.getAttributeType().equalsIgnoreCase("DAY OF WEEK")) {

				java.util.Date udate = SystemSetting.systemDate;
				java.util.Calendar calender = java.util.Calendar.getInstance();
				calender.setTime(udate);

				int day = calender.get(java.util.Calendar.DAY_OF_WEEK);
				String dayofweek = checkValidDay(day);

				if (!dayofweek.equalsIgnoreCase(select.getAttributeValue())) {
					return false;
				}

			}

			if (select.getAttributeType().equalsIgnoreCase("MONTH")) {

				java.util.Date udate = SystemSetting.systemDate;
				java.util.Calendar calender = java.util.Calendar.getInstance();
				calender.setTime(udate);

				int month = udate.getMonth();
				String monthname = MonthName(month);

				if (!monthname.equalsIgnoreCase(select.getAttributeValue())) {
					return false;
				}

			}

		}
		return true;

	}

	private String MonthName(int month) {
		if (month == 0) {
			return "JANUARY";
		}
		if (month == 1) {
			return "FEBRUARY";
		}
		if (month == 2) {
			return "MARCH";
		}
		if (month == 3) {
			return "APRIL";
		}
		if (month == 4) {

			return "MAY";
		}
		if (month == 5) {
			return "JUNE";
		}
		if (month == 6) {
			return "JULY";
		}
		if (month == 7) {
			return "AUGUST";
		}
		if (month == 8) {
			return "SEPTEMBER";
		} else if (month == 9) {
			return "OCTOBER";
		}
		if (month == 10) {
			return "NOVEMBER";
		}
		if (month == 11) {
			return "DECEMBER";
		}

		return null;

	}

	private String checkValidDay(int day) {
		if (day == 1) {
			return "SUNDAY";
		}
		if (day == 2) {
			return "MONDAY";
		}
		if (day == 3) {
			return "TUESDAY";
		}
		if (day == 4) {
			return "WEDNESDAY";
		}
		if (day == 5) {
			return "THURSDAY";
		}
		if (day == 6) {
			return "FRIDAY";
		}
		if (day == 7) {
			return "SATURDAY";
		}

		return null;

	}

	private Boolean validEndDate(SchSelectionAttribInst select) {

		java.util.Date nowDate = SystemSetting.systemDate;
		System.out.println("Noooooooooooooooo" + nowDate);
		java.util.Date endDate = SystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
//		String schemeDate = select.getAttributeValue();
		Integer intDate = endDate.compareTo(nowDate);

		if (intDate >= 0) {
			return true;
		}
		return false;

	}

	private Boolean validStartDate(SchSelectionAttribInst select) {

		java.util.Date nowDate = SystemSetting.systemDate;
		System.out.println("Noooooooooooooooo" + nowDate);
		java.util.Date startDate = SystemSetting.StringToUtilDate(select.getAttributeValue(), "dd-MM-yyyy");
//		String schemeDate = select.getAttributeValue();
		Integer intDate = nowDate.compareTo(startDate);

		if (intDate >= 0) {
			return true;
		}

		return false;
	}

	// ---------------------------------------------------------------------------------------------

	/// -----------------------------surya 7-5-2121

	@FXML
	private TextField txtNationality;

	@FXML
	void showCardTypePopup(MouseEvent event) {

		showCardPopup();

	}

	@FXML
	private TextField txtCardType;

	/// -----------------------------surya 7-5-2121 end

	@FXML
	void FocusOnCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCrAmount.requestFocus();
		}
	}

//	@FXML
//	void setCardType(MouseEvent event) {
//
////		setCardType();
//	}

	@FXML
	private TextField txtCrAmount;

	@FXML
	void KeyPressFocusAddBtn(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (txtCrAmount.getText().length() == 0 && txtCardType.getText().trim().isEmpty()) {
				btnSave.requestFocus();
			} else {

				AddCardAmount.requestFocus();
			}
		}

	}

//	private void setCardType() {
//		txtCardType.getItems().clear();
//
//		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
//		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());
//
//		for (ReceiptModeMst receiptModeMst : receiptModeList) {
//			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
//				cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
//			}
//		}
//	}

	@FXML
	private Button AddCardAmount;

	@FXML
	void KeyPressAddCardAmount(KeyEvent event) {

		ResponseEntity<PatientMst> patientMstResp = RestCaller.getPatientMstByName(custname.getText());
		PatientMst patientMst = patientMstResp.getBody();

		if (null != patientMst) {
			if (null != patientMst.getAccountHeads()) {

				return;
			}
		}

		if (event.getCode() == KeyCode.ENTER) {

			if (txtCardType.getText().trim().isEmpty()) {

				notifyMessage(5, "Please select card type!!!!",false);
				txtCardType.requestFocus();
				return;

			}
			if (txtCrAmount.getText().trim().isEmpty()) {

				notifyMessage(1, "Please enter amount!!!!",false);
				txtCrAmount.requestFocus();
				return;
			}
			addCardAmount(txtCardType.getText(), txtCrAmount.getText());
		}

	}

	@FXML
	void addCardAmount(ActionEvent event) {

		ResponseEntity<PatientMst> patientMstResp = RestCaller.getPatientMstByName(custname.getText());
		PatientMst patientMst = patientMstResp.getBody();

		if (null != patientMst) {
			if (null != patientMst.getAccountHeads()) {

				return;
			}
		}

		if (txtCardType.getText().trim().isEmpty()) {

			notifyMessage(5, "Please select card type!!!!",false);
			txtCardType.requestFocus();
			return;

		}
		if (txtCrAmount.getText().trim().isEmpty()) {

			notifyMessage(1, "Please enter amount!!!!",false);
			txtCrAmount.requestFocus();
			return;

		}

		addCardAmount(txtCardType.getText(), txtCrAmount.getText());

	}

	private void addCardAmount(String cardType, String cardAmount) {
		logger.info("KOTPOS====== add Card Amount started===========");

		if (null != salesTransHdr) {

			salesReceipts = new SalesReceipts();

			if (cardType.equalsIgnoreCase("CREDIT")) {

				ResponseEntity<PatientMst> patientMstResp = RestCaller.getPatientMstByName(custname.getText());
				PatientMst patientMst = patientMstResp.getBody();

				if (null != patientMst) {
					if (null == patientMst.getAccountHeads()) {
						notifyMessage(3, "Invalid customer",false);
					}

					ResponseEntity<AccountHeads> accountHeads = RestCaller
							.getAccountById(patientMst.getAccountHeads().getId());
					if (null != accountHeads.getBody()) {
						salesReceipts.setAccountId(accountHeads.getBody().getId());
					}
				}
			} else {
				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(cardType);
				if (null != accountHeads.getBody()) {
					salesReceipts.setAccountId(accountHeads.getBody().getId());
				}
			}
			salesReceipts.setReceiptMode(cardType);
			salesReceipts.setReceiptAmount(Double.parseDouble(cardAmount));
			salesReceipts.setUserId(SystemSetting.getUser().getId());
			salesReceipts.setBranchCode(SystemSetting.systemBranch);
			if (null == salesReceiptVoucherNo) {
				salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
				salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
			} else {
				salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
			}
			LocalDate date = LocalDate.now();
			java.util.Date udate = SystemSetting.localToUtilDate(date);
			salesReceipts.setReceiptDate(udate);
			salesReceipts.setSalesTransHdr(salesTransHdr);
			System.out.println(salesReceipts);
			ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
			salesReceipts = respEntity.getBody();
			if (null != salesReceipts) {
				if (null != salesReceipts.getId()) {
//					notifyMessage(1, "Successfully added cardAmount",true);
//	    				cardAmount = cardAmount+Double.parseDouble(txtCrAmount.getText());
					// txtcardAmount.setText(Double.toString(txtPaidamount));
					salesReceiptsList.add(salesReceipts);
					filltableCardAmount();
				}
			}

			txtCrAmount.clear();
			txtCardType.clear();
			txtCardType.requestFocus();
			salesReceipts = null;

			logger.info("KOTPOS======= ADD ITEM FINISHED");
		} else {
			notifyMessage(1, "Please add Item!!!!",false);
			return;
		}
	}

	@FXML
	private Button btnDeleteCardAmount;

	@FXML
	void DeleteCardAmount(ActionEvent event) {

		if (null != salesReceipts) {

			if (null != salesReceipts.getId()) {
				if (salesReceipts.getReceiptMode().equalsIgnoreCase("INSURANCE")) {
					notifyMessage(2, "INSURANCE CANT BE DELETED",false);
					return;
				}
				RestCaller.deleteSalesReceipts(salesReceipts.getId());
				tblCardAmountDetails.getItems().clear();
				ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
						.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
				salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

				try {
					cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
				} catch (Exception e) {

					txtcardAmount.setText("");
				}

				filltableCardAmount();

			}
		} else {

		}

	}

	@FXML
	void KeyPressDeleteCardAmount(KeyEvent event) {

	}

	@FXML
	private TableView<SalesReceipts> tblCardAmountDetails;

	@FXML
	private TableColumn<SalesReceipts, String> clCardTye;

	@FXML
	private TableColumn<SalesReceipts, Number> clCardAmount;

	private void filltableCardAmount() {

		tblCardAmountDetails.setItems(salesReceiptsList);
		clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
		clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());
		cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
		txtcardAmount.setText(Double.toString(cardAmount));

	}
	// ---------------------------------------------------------------------------------------------

	private Boolean CheckOfferWhileItemAdding(String salesTransHdrId, SalesDtl salesDtl) {

		ResponseEntity<List<SchemeInstance>> schemeResp = RestCaller.getActiveSchemeDeteils("Retail");
		List<SchemeInstance> schemeInstanceList = schemeResp.getBody();
		if (schemeInstanceList.size() > 0) {

			for (SchemeInstance scheme : schemeInstanceList) {
				Boolean validityPeriod = CheckValidityPeriod(scheme);
				if (validityPeriod) {

					if (scheme.getOfferId().equalsIgnoreCase("2") && !scheme.getEligibilityId().equalsIgnoreCase("7")) {

						Boolean eligible = eligibilityCheck(scheme, salesTransHdrId, salesDtl);
					}

				}
			}

		}

		return false;

	}

	@FXML
	void itemNameOnClick(MouseEvent event) {

		if (null == cmbSaleType.getSelectionModel() || null == cmbSaleType.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Select Voucher Type",false);
			cmbSaleType.requestFocus();
			return;
		}
		if (custname.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Customer",false);
			custname.requestFocus();
			return;
		}

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategorywiseItemSearch.fxml"));
			Parent root1;
			CategorywiseItemSearchCtl categorywiseItemSearchCtl = loader.getController();
			categorywiseItemSearchCtl.customerName = custname.getText();
			if (null != cmbSaleType.getSelectionModel()) {
				categorywiseItemSearchCtl.voucherType = cmbSaleType.getSelectionModel().getSelectedItem();
			}
			categorywiseItemSearchCtl.customerIsBranch = customerIsBranch;
			categorywiseItemSearchCtl.txtLocalCustomer = txtLocalCustomer.getText();
			categorywiseItemSearchCtl.localCustId = localCustId;
			if (null == salesTransHdr) {
				createSalesTransHdr();
				categorywiseItemSearchCtl.salesTransHdr = salesTransHdr;
			} else {
				categorywiseItemSearchCtl.salesTransHdr = salesTransHdr;
			}
			root1 = (Parent) loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root1));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

	}

	@Subscribe
	public void categoryItemSearchListener(CategorywiseItemSearchEvent categorywiseItemSearchEvent) {

		{
			salesTransHdr = categorywiseItemSearchEvent.getSalesTransHdrId();
			ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

			List<SalesDtl> salesDtlList = respentityList.getBody();

			/*
			 * Call Rest to get the summary and set that to the display fields
			 */

			// salesDtl.setTempAmount(amount);
			saleListTable.clear();
			saleListTable.setAll(salesDtlList);
			FillTable();
		}

	}

	private void createSalesTransHdr() {
		
		AccountHeads accountHeads = null;
		salesTransHdr = new SalesTransHdr();
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.getId();

		ResponseEntity<PatientMst> patientMstResp = RestCaller.getPatientMstByName(custname.getText());
		PatientMst patientMst = patientMstResp.getBody();

		if (null != patientMst) {
			salesTransHdr.setPatientMst(patientMst);
		} else {

			notifyMessage(2, "Invalid Patient",false);
			return;
		}

		if (null != patientMst.getAccountHeads()) {
			
			/*
			 * new url for getting account heads instead of customer mst=========05/0/2022
			 */
//			ResponseEntity<CustomerMst> customerMstResp = RestCaller
//					.getCustomerById(patientMst.getCustomerMst().getId());
			ResponseEntity<AccountHeads> accountHeadsResponse=RestCaller.getAccountHeadsById(patientMst.getAccountHeads().getId());
			accountHeads = accountHeadsResponse.getBody();

			if (null != accountHeads) {
				salesTransHdr.setAccountHeads(accountHeads);
				salesTransHdr.setCustomerId(accountHeads.getId());
				custId = accountHeads.getId();
			} else {
				notifyMessage(2, "Invalid customer",false);
				return;
			}

			
		} else {
			/*
			 * new url for getting account heads instead of customer mst=========05/0/2022
			 */
//			ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerByNameAndBarchCode("PHARMACYRETAIL",
//					SystemSetting.getUser().getBranchCode());
			ResponseEntity<AccountHeads> accountHeadsResponse=RestCaller.getAccountHeadsByNameAndBranchCode("PHARMACYRETAIL",SystemSetting.getUser().getBranchCode());

			accountHeads = accountHeadsResponse.getBody();

			if (null == accountHeads) {
				notifyMessage(2, "Invalid customer",false);
				return;
			}
			salesTransHdr.setAccountHeads(accountHeads);
			salesTransHdr.setCustomerId(accountHeads.getId());
			custId = accountHeads.getId();

		}

		salesTransHdr.setVoucherType(cmbSaleType.getSelectionModel().getSelectedItem());

		logger.info("===========Whole Sale get customer by Id in Add item!!");

		// if finance option selected///

		if (!txtLocalCustomer.getText().trim().isEmpty()) {
			ResponseEntity<LocalCustomerMst> LocalCustomerMstResp = RestCaller.getLocalCustomerById(localCustId);
			LocalCustomerMst localCustomerMst = LocalCustomerMstResp.getBody();
			if (null != localCustomerMst) {
				salesTransHdr.setLocalCustomerMst(localCustomerMst);
			}

		}
		/*
		 * If Customer Has a valid GST , then B2B Invoice , otherwise its B2C
		 */
		if (null == accountHeads.getPartyGst() || accountHeads.getPartyGst().length() < 14) {
			salesTransHdr.setSalesMode("B2C");
		} else {
			salesTransHdr.setSalesMode("B2B");
		}

		salesTransHdr.setCreditOrCash("CREDIT");
		salesTransHdr.setUserId(SystemSetting.getUser().getId());
		salesTransHdr.setBranchCode(SystemSetting.systemBranch);

		if (customerIsBranch) {
			salesTransHdr.setIsBranchSales("Y");
		} else {
			salesTransHdr.setIsBranchSales("N");
		}

		LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		java.util.Date date = SystemSetting.systemDate;
		salesTransHdr.setVoucherDate(date);

		// ------------------------------------

		if (null != cmbDoctor.getSelectionModel().getSelectedItem()) {
			ResponseEntity<List<NewDoctorMst>> doctorMstResp = RestCaller
					.getNewDoctorMstByName(cmbDoctor.getSelectionModel().getSelectedItem().toString());
			List<NewDoctorMst> doctorList = doctorMstResp.getBody();
			if (doctorList.size() > 0) {
				NewDoctorMst newDoctorMst = doctorList.get(0);
				salesTransHdr.setDoctorId(newDoctorMst.getId());
			}

		}
		// ------------------------------------

		logger.info("==============Whole Sale save Sales trans Hdr started!!");
		String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

		ResponseEntity<SalesTransHdr> getsales = RestCaller
				.getNullSalesTransHdrByCustomer(salesTransHdr.getAccountHeads().getId(), sdate, "PHARMACYRETAIL");
		if (null != getsales.getBody()) {
			salesTransHdr = getsales.getBody();
		} else {
			ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
			logger.info("Whole Sale save Sales trans Hdr completed!!");
			salesTransHdr = respentity.getBody();

		}
	}

	@FXML
	void actionAddCustomer(ActionEvent event) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/PatientCreation.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			custname.requestFocus();
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showCardPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ReceiptModePopUp.fxml"));

			// fxmlLoader.setController(itemStockPopupCtl);

			Parent root = loader.load();
			ReceiptModePopUpCtl popupctl = loader.getController();
			popupctl.ReceiptModeWithOutInsurance();

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void ReceiptModeListener(ReceiptModeEvent receiptModeEvent) {

		{
			txtCardType.setText(receiptModeEvent.getReceiptMode());
		}

	}
	
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}
	
	
	@FXML
    void clearAllDetails(ActionEvent event) {

		
		salesTransHdr = null;
		salesDtl = null;
		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtPaidamount.setText("");
		txtSBICard.setText("");
		txtSodexoCard.setText("");
		txtYesCard.setText("");
		custname.setText("");
		custAdress.setText("");
		saleListTable.clear();
		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");
		totalAmountTenderd = 0.0;
		CashPaid = 0.0;
		AmountTenderd = 0.0;
		custId = null;
		cmbSaleType.getSelectionModel().clearSelection();
		txtPriceType.clear();
		txtItemcode.setText("");
		txtBatch.setText("");
		txtBarcode.requestFocus();
		CardAmount = 0.0;
		txtLocalCustomer.clear();
		txtLocalCustomer.setDisable(true);

		salesTransHdr = null;

		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtPaidamount.setText("");

		txtChangeamount.clear();
		saleListTable.clear();
		txtBarcode.requestFocus();
		salesDtl = new SalesDtl();

		txtDiscount.clear();
		txtDiscountPercent.clear();
		txtAmtAftrDiscount.clear();
		salesDtl = new SalesDtl();
		tblCardAmountDetails.getItems().clear();
		salesReceiptVoucherNo = null;
		txtItemname.clear();
		txtBarcode.clear();
		txtBatch.clear();
		txtItemcode.clear();
		txtRate.clear();
		txtNationality.clear();
		txtCustomerMst.clear();
		txtPreviousBalance.clear();
		txtQty.clear();
		salesReceiptsList.clear();
		lblCardType.setVisible(false);
		lblAmount.setVisible(false);
		txtCardType.setVisible(false);
		txtCrAmount.setVisible(false);
		AddCardAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		tblCardAmountDetails.setVisible(false);
		txtcardAmount.setVisible(false);
		txtInsuranceCompany.clear();
		txtFCCashToPay.clear();
		txtPolicyType.clear();
		txtPercentageCoverage.clear();
		cmbDoctor.getSelectionModel().clearSelection();
		txtTotalInvoice.clear();
		
    }

	private void PageReload() {

	}
	
	
	
	
	

}
