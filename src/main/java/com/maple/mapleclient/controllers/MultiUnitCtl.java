package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CompanyMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class MultiUnitCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<UnitMst> unitList = FXCollections.observableArrayList();
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();
	private ObservableList<MultiUnitMst> multiUnitList1 = FXCollections.observableArrayList();
	MultiUnitMst multiUnit = null;
	MultiUnitMst multiUnitDelete= new MultiUnitMst();
	ItemMst itemmst = null;
	CompanyMst companyMst = new CompanyMst();
    @FXML
    private TextField txtItemName;

    @FXML
    private ComboBox<String> cmbUnit1;

    @FXML
    private ComboBox<String> cmbUnit2;
    @FXML
    private TextField txtQty1;

    @FXML
    private TextField txtBarcode;

    @FXML
    private Button btnShowAll;
    @FXML
    private TextField txtQty2;

    @FXML
    private TableView<MultiUnitMst> tbMultiUnit;

    @FXML
    private TableColumn<MultiUnitMst, String> clItemName;

    @FXML
    private TableColumn<MultiUnitMst, String> clUnit1;

    @FXML
    private TextField txtPrice;
    @FXML
    private TableColumn<MultiUnitMst, Number> clQty1;

    @FXML
    private TableColumn<MultiUnitMst, String> clUnit2;

    @FXML
    private TableColumn<MultiUnitMst, Number> clQty2;
    @FXML
    private TableColumn<MultiUnitMst, String> clBarcode;
    @FXML
    private TableColumn<MultiUnitMst, Number> clPrice;
    @FXML
    private Button btnSaveUnit;

    @FXML
    private Button btnDeleteUnit;
	@FXML
	private void initialize() {
		
		eventBus.register(this);
		
		ItemMst itemmst = new ItemMst();
		UnitMst unitMst = new UnitMst();
		// set unit
		ArrayList unit = new ArrayList();
		RestTemplate restTemplate = new RestTemplate();
		unit = RestCaller.SearchUnit();
		Iterator itr = unit.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object unitName = lm.get("unitName");
			Object unitId = lm.get("id");
			if (unitId != null) {
				cmbUnit2.getItems().add((String) unitName);
				UnitMst newUnitMst = new UnitMst();
				newUnitMst.setUnitName((String) unitName);
				newUnitMst.setId((String) unitId);
				unitList.add(newUnitMst);
			}
		}
		tbMultiUnit.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
					
					if (null != newSelection.getId()) {
						multiUnitDelete = new MultiUnitMst();
						multiUnitDelete.setId(newSelection.getId());

					}

				}
			
		});
		
	}

    @FXML
    void DeleteUnit(ActionEvent event) {
    	
    	if(null!= multiUnitDelete.getId())
    	{
    		RestCaller.multiUnitDelete(multiUnitDelete.getId());
    		notifyMessage(5, " Deleted Successfully");
    		tbMultiUnit.getItems().clear();
    		//multiUnitList.clear();
    		ResponseEntity<List<MultiUnitMst>> multiunitSaved = RestCaller.getAllMultiUnitMst();
    		multiUnitList1 = FXCollections.observableArrayList(multiunitSaved.getBody());
    	
    		if(multiUnitList1 != null)
    		{
    			
    		
    		for(MultiUnitMst multmst : multiUnitList1)
    		{
    			ResponseEntity<ItemMst> rest = RestCaller.getitemMst(multmst.getItemId());
    			multmst.setItemName(rest.getBody().getItemName());
    
    		}
    		}
    		tbMultiUnit.setItems(multiUnitList1);
    		//notifyMessage(5, "Deleted!!!");
    		
    	}else
    	{
    		notifyMessage(5, "Please select the Item!!!");
    	}
    	

    }
    @FXML
    void loadItem(KeyEvent event) {
    	showItem();
    }

    @FXML
    void loadItemPopup(MouseEvent event) {
    	showItem();
    }


    @FXML
    void saveUnit(ActionEvent event) {

    	multiUnit = new MultiUnitMst();
    	String vNo= RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"MLTUN");
    	//tbMultiUnit.getItems().clear();
    	multiUnit.setId(vNo);
    	multiUnit.setBarCode(SystemSetting.systemBranch);
    	multiUnit.setItemId(itemmst.getId());
    	multiUnit.setQty1(Double.parseDouble(txtQty1.getText()));
    	ResponseEntity<UnitMst> multiunit = RestCaller.getUnitByName(cmbUnit2.getSelectionModel().getSelectedItem());
    	multiUnit.setUnit1(multiunit.getBody().getId());
    	ResponseEntity<UnitMst> multiunit1 = RestCaller.getUnitByName(cmbUnit1.getSelectionModel().getSelectedItem());

    	multiUnit.setUnit2(multiunit1.getBody().getId());
    	multiUnit.setQty2(Double.parseDouble(txtQty2.getText()));
    	multiUnit.setBarCode(txtBarcode.getText());
    	multiUnit.setPrice(Double.parseDouble(txtPrice.getText()));
    	ResponseEntity<MultiUnitMst> respentity = RestCaller.saveMultiUnit(multiUnit);
    	multiUnit= respentity.getBody();
    	multiUnit.setItemName(txtItemName.getText());
    	multiUnitList.add(multiUnit);
    	filltable();
    	txtBarcode.clear();
     	txtItemName.setText("");
     	txtQty1.setText("");
     	txtQty2.setText("");
     	cmbUnit1.setValue("");
     	cmbUnit2.setValue("");
    }
    @FXML
    void bacodeOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			if (txtBarcode.getText().trim().isEmpty()) {
				String barcode = RestCaller.getVoucherNumber("BCD");
				txtBarcode.setText(barcode);
				btnSaveUnit.requestFocus();
			}
    	}
    }
    @FXML
    void qty1OnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		cmbUnit2.requestFocus();
    	}
    }

    @FXML
    void unit1OnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		txtQty1.requestFocus();
    	}
    }

    @FXML
    void unit2OnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		txtQty2.requestFocus();
    	}
    }
    @FXML
    void qty2OnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		txtPrice.requestFocus();
    	}
    }
    @FXML
    void priceOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		txtBarcode.requestFocus();
    	}
    }
    @FXML
    void ShowAll(ActionEvent event) {
    	ResponseEntity<List<MultiUnitMst>> multiunitSaved = RestCaller.getAllMultiUnitMst();
		multiUnitList1 = FXCollections.observableArrayList(multiunitSaved.getBody());
	
		if(multiUnitList1 != null)
		{
			
		
		for(MultiUnitMst multmst : multiUnitList1)
		{
			ResponseEntity<ItemMst> rest = RestCaller.getitemMst(multmst.getItemId());
			multmst.setItemName(rest.getBody().getItemName());

		}
		
		}

		for(MultiUnitMst multi : multiUnitList1)
		{
			ResponseEntity<UnitMst> getunit1 = RestCaller.getunitMst(multi.getUnit1());
			multi.setUnit1Name(getunit1.getBody().getUnitName());
			ResponseEntity<UnitMst> getunit2 = RestCaller.getunitMst(multi.getUnit2());
			multi.setUnit2Name(getunit2.getBody().getUnitName());
		}
		tbMultiUnit.setItems(multiUnitList1);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clUnit1.setCellValueFactory(cellData -> cellData.getValue().getunit1Property());
		clUnit2.setCellValueFactory(cellData -> cellData.getValue().getunit2Property());
		clQty1.setCellValueFactory(cellData -> cellData.getValue().getqty1Property());
		clQty2.setCellValueFactory(cellData -> cellData.getValue().getqty2Property());
		clBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
		clPrice.setCellValueFactory(cellData -> cellData.getValue().getpriceProperty());
		
    }

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

	
		
		Stage stage = (Stage) btnSaveUnit.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			itemmst = new ItemMst();
			txtItemName.setText(itemPopupEvent.getItemName());
			
			itemmst.setId(itemPopupEvent.getItemId());
			cmbUnit1.getItems().clear();
			cmbUnit1.getItems().add(itemPopupEvent.getUnitName());
			ResponseEntity<List<MultiUnitMst>> getmultiUnit = RestCaller.getMultiUnitByItemId(itemPopupEvent.getItemId());
		  for (MultiUnitMst mult : getmultiUnit.getBody())
		  {
			  ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(mult.getUnit1());
			  
			cmbUnit1.getItems().add(getUnit.getBody().getUnitName());
		  }	
			// txtQty.setText(Integer.toString(itemPopupEvent.getQty()));
				}
			});
		}

	}
	private void filltable()
	{
		for(MultiUnitMst multi : multiUnitList)
		{
			ResponseEntity<UnitMst> getunit1 = RestCaller.getunitMst(multi.getUnit1());
			multi.setUnit1Name(getunit1.getBody().getUnitName());
			ResponseEntity<UnitMst> getunit2 = RestCaller.getunitMst(multi.getUnit2());
			multi.setUnit2Name(getunit2.getBody().getUnitName());
		
		}
		tbMultiUnit.setItems(multiUnitList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clUnit1.setCellValueFactory(cellData -> cellData.getValue().getunit1Property());
		clUnit2.setCellValueFactory(cellData -> cellData.getValue().getunit2Property());
		clQty1.setCellValueFactory(cellData -> cellData.getValue().getqty1Property());
		clQty2.setCellValueFactory(cellData -> cellData.getValue().getqty2Property());
		clBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
		clPrice.setCellValueFactory(cellData -> cellData.getValue().getpriceProperty());
	}
    private void showItem()
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			cmbUnit1.requestFocus();
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
