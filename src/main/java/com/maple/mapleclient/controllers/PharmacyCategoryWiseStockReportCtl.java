package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.CategoryWiseStockMovement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyCategoryWiseStockReportCtl {
	
	String taskid;
	String processInstanceId;
	
	String strfDate, strtDate;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<CategoryWiseStockMovement> categoryList = FXCollections.observableArrayList();
	
	   @FXML
	    private AnchorPane clin;

	    @FXML
	    private DatePicker dpFromDate;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private ComboBox<?> cmbCatName;

	    @FXML
	    private Button btnShow;

	    @FXML
	    private Button btnPrint;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private Button btnAdd;

	    @FXML
	    private ListView<String> lsCategory;
	    @FXML
	    private TextField txtCategoryName;


	    @FXML
	    private TableView<CategoryWiseStockMovement> tbICategoryWiseStock;

	    @FXML
	    private TableColumn<CategoryWiseStockMovement, String> clVoucherDate;

	    @FXML
	    private TableColumn<CategoryWiseStockMovement, String> clCatg;

	    @FXML
	    private TableColumn<CategoryWiseStockMovement, Number> clOpen;

	    @FXML
	    private TableColumn<CategoryWiseStockMovement, Number> clinc;

	    @FXML
	    private TableColumn<CategoryWiseStockMovement, Number> clOut;

	    @FXML
	    private TableColumn<CategoryWiseStockMovement, Number> clClosing;

	    @FXML
	    void actionClear(ActionEvent event) {
	    	
	    	  dpFromDate.setValue(null);
			  dpToDate.setValue(null);
			
			  lsCategory.getItems().clear();
			  txtCategoryName.clear();
			  tbICategoryWiseStock.getItems().clear();
			

	    }
	    
	  

	    @FXML
	    void actionPrint(ActionEvent event) {
	    	
	    	if(null==dpFromDate.getValue()) {
	    		notifyMessage(5, "select from date",false);
	    		return;
	    	}
	    	if(null==dpToDate.getValue()) {
	    		notifyMessage(5, "select to date",false);
	    		return;
	    	}
	    	
	    	if(null==lsCategory.getSelectionModel().getSelectedItem()) {
	    		notifyMessage(5, "plz add Category",false);
	    		return;
	    	}
	    	
	       	
	  	  List<String> selectedItems = lsCategory.getSelectionModel().getSelectedItems();
	  	        
	  	    	String cat="";
	  	    	for(String s:selectedItems)
	  	    	{
	  	    		s = s.concat(";");
	  	    		cat= cat.concat(s);
	  	    	} 
	    	
	    	 LocalDate dpDate1 = dpFromDate.getValue();
				java.util.Date ufDate = SystemSetting.localToUtilDate(dpDate1);
				strfDate = SystemSetting.UtilDateToString(ufDate, "yyyy-MM-dd");

				LocalDate dpDate2 = dpToDate.getValue();
				java.util.Date utDate = SystemSetting.localToUtilDate(dpDate2);
				strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
				
				try {
					JasperPdfReportService.PrintStockMovementbyCategoryWiseSReport(strfDate,strtDate,cat);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 


	    }

	    @FXML
	    void actionShow(ActionEvent event) {
	    	
	    	if(null==dpFromDate.getValue()) {
	    		notifyMessage(5, "select from date",false);
	    		return;
	    	}
	    	if(null==dpToDate.getValue()) {
	    		notifyMessage(5, "select to date",false);
	    		return;
	    	}
	    	
	    	if(null==lsCategory.getSelectionModel().getSelectedItem()) {
	    		notifyMessage(5, "plz add Category",false);
	    		return;
	    	}
	    	
	        
	    	
	  List<String> selectedItems = lsCategory.getSelectionModel().getSelectedItems();
	        
	    	String cat="";
	    	for(String s:selectedItems)
	    	{
	    		s = s.concat(";");
	    		cat= cat.concat(s);
	    	} 
	    	 java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
				String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
				java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
				String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
				
				ResponseEntity<List<CategoryWiseStockMovement>> getCategoryStkMvmnt = RestCaller
						.getPharmacyStockMovementByCategorywise(startDate, endDate,cat);
				categoryList = FXCollections.observableArrayList(getCategoryStkMvmnt.getBody());
				fillTable();

	    }
	    
	    @FXML
	    void onEnterGrop(ActionEvent event) {
	    	
	    	 loadCategoryPopup();

	    }


	    private void loadCategoryPopup() {
		
	    	try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@FXML
	    void actionadd(ActionEvent event) {
	    	lsCategory.getItems().add(txtCategoryName.getText());
	    	lsCategory.getSelectionModel().select(txtCategoryName.getText());
	    	txtCategoryName.clear();
	    

	    }
	    
	    @FXML
		private void initialize() {
	    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
	    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
	    	
	    	eventBus.register(this);
	    	lsCategory.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		}
	 
	    
	    private void fillTable() {

			for (CategoryWiseStockMovement cat : categoryList) {
				if (null != cat.getClosingStock()) {
					BigDecimal closing = new BigDecimal(cat.getClosingStock());
					closing = closing.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					cat.setClosingStock(closing.doubleValue());
				} else {
					cat.setClosingStock(0.0);
				}
				if (null != cat.getOpeningStock()) {
					BigDecimal opening = new BigDecimal(cat.getOpeningStock());
					opening = opening.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					cat.setOpeningStock(opening.doubleValue());
				} else {
					cat.setOpeningStock(0.0);
				}
				if (null != cat.getInWardQty()) {
					BigDecimal inward = new BigDecimal(cat.getInWardQty());
					inward = inward.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					cat.setInWardQty(inward.doubleValue());
				} else {
					cat.setInWardQty(0.0);
				}
				if (null != cat.getOutWardQty()) {
					BigDecimal outward = new BigDecimal(cat.getOutWardQty());
					outward = outward.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					cat.setOutWardQty(outward.doubleValue());
				} else {
					cat.setOutWardQty(0.0);
				}
			}
			tbICategoryWiseStock.setItems(categoryList);
			clClosing.setCellValueFactory(cellData -> cellData.getValue().getclosingStockProperty());
			clCatg.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
			clinc.setCellValueFactory(cellData -> cellData.getValue().getinWardQtyProperty());
			clOut.setCellValueFactory(cellData -> cellData.getValue().getoutWardQtyProperty());
			clOpen.setCellValueFactory(cellData -> cellData.getValue().getopeningStockProperty());
			clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());

		}

		public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
		
		 @Subscribe
			public void popupOrglistner(CategoryEvent CategoryEvent) {

				Stage stage = (Stage) btnShow.getScene().getWindow();
				if (stage.isShowing()) {

					
					txtCategoryName.setText(CategoryEvent.getCategoryName());
			
				

				}

}
}
