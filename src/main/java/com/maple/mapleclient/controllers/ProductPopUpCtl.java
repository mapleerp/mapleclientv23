package com.maple.mapleclient.controllers;

import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.collections.map.HashedMap;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ProductMst;
import com.maple.mapleclient.events.ProductEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ProductPopUpCtl {
	
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
    private ObservableList<ProductMst> productList = FXCollections.observableArrayList();
    ProductMst productMst = null;
	ProductEvent productEvent;
    @FXML
    private TextField txtProductName;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;

    @FXML
    private TableView<ProductMst> tbProducts;

    @FXML
    private TableColumn<ProductMst, String> clProductName;
	StringProperty SearchString = new SimpleStringProperty();

    @FXML
	private void initialize() {
    	LoadAccountPopupBySearch("");
    	eventBus.register(this);
    	productEvent = new ProductEvent();
    	tbProducts.setItems(productList);
    	clProductName.setCellValueFactory(cellData -> cellData.getValue().getproductNameProperty());
    	tbProducts.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			    if (newSelection != null) {
			    	if(null!=newSelection.getId()&& newSelection.getProductName().length()>0) {
			    		productEvent.setProductId(newSelection.getId());
			    		productEvent.setProductName(newSelection.getProductName());
			    		eventBus.post(productEvent); 
			    	}
			    }
			});
    	
    	 
   	 SearchString.addListener(new ChangeListener(){
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				 					
					LoadAccountPopupBySearch((String)newValue);
					
				}
	        });
    }
    @FXML
    void actionCancel(ActionEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
    void actionOk(ActionEvent event) {
    	if(null != productEvent.getProductId())
    	{
    	eventBus.post(productEvent); 
    	}
    	else
    	{
    		productEvent.setProductName(txtProductName.getText());
    		eventBus.post(productEvent);
    	}
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
    void onKeyPresstxt(KeyEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	if (event.getCode() == KeyCode.ENTER)
    
    	{
    	if(null != productEvent.getProductId())
    	{
    	eventBus.post(productEvent); 
    	stage.close();
    	}
    	else
    	{
    		productEvent.setProductName(txtProductName.getText());
    		eventBus.post(productEvent);
    		stage.close();
    	}
    	}
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tbProducts.requestFocus();
    		tbProducts.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			stage.close();
		}
    }
    @FXML
    void tbOnEnter(KeyEvent event) {
if (event.getCode() == KeyCode.ENTER) {
			
    		Stage stage = (Stage) btnOk.getScene().getWindow();
      
        	if(null != productEvent.getProductId())
        	{
        	eventBus.post(productEvent); 
        	stage.close();
        	}
        	else
        	{
        		productEvent.setProductName(txtProductName.getText());
        		eventBus.post(productEvent);
        		stage.close();
        	}
		}
    
    }
    private void LoadAccountPopupBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ArrayList product = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		productList.clear();

		product = RestCaller.SearchProductByName(searchData);
		String id;
		String productName;
		Iterator itr = product.iterator();
		System.out.println("accaccaccacc22");
		while (itr.hasNext()) {
			
			List element = (List) itr.next();
			  productName = (String) element.get(1);
			  id = (String) element.get(0);
			 if (null != id) {
					System.out.println("accaccaccacc44");
					 productMst = new ProductMst();
					productMst.setId(id);
					productMst.setProductName(productName);
					productList.add(productMst);
		}
    }
		return;
}
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
