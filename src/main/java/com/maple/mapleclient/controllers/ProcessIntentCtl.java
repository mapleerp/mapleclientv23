package com.maple.mapleclient.controllers;





import java.util.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.IntentDtl;
import com.maple.mapleclient.entity.IntentInDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ProcessIntentMst;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.SupplierPriceMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ProcessIntentCtl {
	
	
	
	IntentDtl intentDtl = null;
	ItemMst itemMst = null;
	SupplierPriceMst supplierPriceMst=null;
	
	private ObservableList<ProcessIntentMst> processIntentTable = FXCollections.observableArrayList();
	private ObservableList<IntentInDtl> intentInDtlList = FXCollections.observableArrayList();
	private ObservableList<SupplierPriceMst> supplierPriceTable = FXCollections.observableArrayList();
	

	String taskid;
	String processInstanceId;
	
	
	@FXML
    private TableView<IntentDtl> tblIntent;

    @FXML
    private TableColumn<IntentDtl, String> clItem;

    @FXML
    private TableColumn<IntentDtl, Number> clQty;

    @FXML
    private TableColumn<IntentDtl, String> clUnitName;

    @FXML
    private TableColumn<IntentDtl, String> clRemark;

    @FXML
    private TableView<SupplierPriceMst> tblSupplier;

    @FXML
    private TableColumn<SupplierPriceMst, String> clSupplier;

    @FXML
    private TableColumn<SupplierPriceMst, Number> clDP;


    @FXML
    private TableView<ProcessIntentMst> tblProcessIntent;

    @FXML
    private TableColumn<ProcessIntentMst, String> clitem;

    @FXML
    private TableColumn<ProcessIntentMst, Number> clqty;

    @FXML
    private TableColumn<ProcessIntentMst, String> clUnit;

    @FXML
    private TableColumn<ProcessIntentMst, String> clremark;

    @FXML
    private TableColumn<ProcessIntentMst, String> clsupplier;

    @FXML
    private TableColumn<ProcessIntentMst, Number> clDp;

    @FXML
    private TableColumn<ProcessIntentMst, Date> clDate;

    @FXML
    private TextField txtSupplier;

    @FXML
    private TextField txtDP;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    void AddItem(ActionEvent event) {

    }

    @FXML
    void DeleteItem(ActionEvent event) {

    }

    @FXML
    void Save(ActionEvent event) {

    }

    @FXML
    void addOnEnter(KeyEvent event) {

    }

    @FXML
    void saveOnEnter(KeyEvent event) {

    }
    
   	@FXML
   	private void initialize() {
   		
   		
   		
   		
   		
   		
   		tblSupplier.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
   			if (newSelection != null) {
   				if (null != newSelection.getId()) {
   					supplierPriceMst = new SupplierPriceMst();
   					supplierPriceMst.setId(newSelection.getId());
   					
   				}
   			}
   			});
	
   	}
   	
   	
   	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}
}
