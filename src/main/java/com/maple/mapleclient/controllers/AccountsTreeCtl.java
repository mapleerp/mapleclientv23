package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class AccountsTreeCtl {
	String taskid;
	String processInstanceId;

	String selectedText = null;
    @FXML
    private AnchorPane AccountsTree;

    @FXML
    private TreeView<String> treeview;

    
    @FXML
    void initialize() {
    	

		TreeItem rootItem = new TreeItem("ACCOUNTS");
		//TreeItem masterItem = new TreeItem("MASTERS");
		
	HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
		
		
		
		ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("ACCOUNTS");
		
		List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
		
		MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
		
		String parentId =aMenuConfigMst.getId();
		
		ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
		List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

		
		for(MenuConfigMst aMenuConfig : menuConfigMstList)
		{
			
			//masterItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
			
			if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
				rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
				}
			
			
		}
		
		//rootItem.getChildren().add(masterItem);

		treeview.setRoot(rootItem);
		
		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text 12345 : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();
				
				
				ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller.MenuConfigMstByMenuName(selectedText);
				
				List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();
		
				MenuConfigMst aMenuConfig = menuConfigMstList.get(0);
				
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != selectedText)
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(selectedText, dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
			
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
				  
				
			/*	if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.kotManagement);

				}
				
				*/
				
				

			}

		});
//    	TreeItem rootItem = new TreeItem("Accounts");
//    	
//    	
//    	
//    	
//    	HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
//    		
//    		
//    		
//    		ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("Accounts");
//    		
//    		List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
//    		
//    		MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
//    		
//    		String parentId =aMenuConfigMst.getId();
//    		
//    		ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
//    		List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();
//
//    		
//    		for(MenuConfigMst aMenuConfig : menuConfigMstList)
//    		{
//    			if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
//    			rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
//    			}
//    			
//    		}
//    		
//    		//rootItem.getChildren().add(masterItem);
//
//    		treeview.setRoot(rootItem);
//    		
//    		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//
//    			@Override
//    			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//    //
//    				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
//    				System.out.println("Selected Text 12345 : " + selectedItem.getValue());
//    				selectedText = selectedItem.getValue();
//    				
//    				
//    				ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller.MenuConfigMstByMenuName(selectedText);
//    				
//    				List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();
//    		
//    				MenuConfigMst aMenuConfig = menuConfigMstList.get(0);
//    				
//    				
//    				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
//    				if(null==dynamicWindow) {
//    					if(null != selectedText)
//    					{
//    						  try {
//    							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
//    							 menuWindowQueue.put(selectedText, dynamicWindow);
//    						  
//    						  } catch (IOException e) {
//    							// TODO Auto-generated catch block
//    							e.printStackTrace();
//    						}
//
//    					}
//
//    				}
//    				  
//    				  try {
//    			
//    						// Clear screen before loading the Page.
//    						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    							MapleclientApplication.mainWorkArea.getChildren().clear();
//    			
//    						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
//    			
//    						MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
//    						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
//    						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
//    						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
//    					 	dynamicWindow.requestFocus();
//    			
//    					} catch (Exception e) {
//    						e.printStackTrace();
//    					}
//    				  
//    				  
//    				  
//    				
//    			/*	if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) {
//    					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//    					MapleclientApplication.mainWorkArea.getChildren()
//    							.add(MapleclientApplication.mainFrameController.kotManagement);
//
//    				}
//    				
//    				*/
//    				
//    				
//
//    			}
//
//    		});
//    				 
//    		
//    		
//    		
//    	
//    	
//    	
//    	
//    	TreeItem openingConfig = new TreeItem("OPENING BALANCE CONFIGURATIONS");
//    	if (SystemSetting.UserHasRole("OPENING BALANCE CONFIGURATIONS")) {
//		rootItem.getChildren().add(openingConfig);
//    	}
//    	TreeItem custReciepts = new TreeItem("CUSTOMER BILLWISE ADJUSTMENT");
//    	
//    		rootItem.getChildren().add(custReciepts);
//    		
//    		TreeItem pettyReciepts = new TreeItem("PETTY-CASH RECIEPTS");
//        	
//    		rootItem.getChildren().add(pettyReciepts);
//    		TreeItem otherReciepts = new TreeItem("RECIEPTS");
//    		rootItem.getChildren().add(otherReciepts);
//    		
//    		TreeItem journal = new TreeItem("JOURNAL");
//    		rootItem.getChildren().add(journal);
//    		
//    		TreeItem supPayments = new TreeItem("SUPPLIER BILLWISE ADJUSTMENT");
////        	if (SystemSetting.UserHasRole("SUPPLIER PAYMENTS")) {
//        		rootItem.getChildren().add(supPayments);
////        	}
//        	TreeItem PettyCashPayments = new TreeItem("PETTY-CASH PAYMENTS");
//        		rootItem.getChildren().add(PettyCashPayments);
//        		
//        	TreeItem OtherPayments = new TreeItem("PAYMENTS");
//        	
//        		rootItem.getChildren().add(OtherPayments);
//        	
//        	TreeItem OwnAccSettlement = new TreeItem("OWN ACCOUNT SETTLEMENT");
//        	if (SystemSetting.UserHasRole("OWN ACCOUNT SETTLEMENT")) {
//        		rootItem.getChildren().add(OwnAccSettlement);
//        	}
//        	
//        	
//        	TreeItem pdcReceipt = new TreeItem("PDC RECEIPT");
//        	
//        	TreeItem pdcPayment = new TreeItem("PDC PAYMENT");
//        	
//        	TreeItem pdcReconsile = new TreeItem("PDC RECONCILE");
//        	
//    		rootItem.getChildren().add(pdcReceipt);
//    		rootItem.getChildren().add(pdcPayment);
//    		rootItem.getChildren().add(pdcReconsile);
//        	treeview.setRoot(rootItem);
//        	
//        	TreeItem receiptEdit = new TreeItem("RECEIPT EDIT");
//        	if (SystemSetting.UserHasRole("RECEIPT EDIT")) {
//        		rootItem.getChildren().add(receiptEdit);
//        	}
//        	
//
//        	TreeItem paymentEdit = new TreeItem("PAYMENT EDIT");
//        	if (SystemSetting.UserHasRole("PAYMENT EDIT")) {
//        		rootItem.getChildren().add(paymentEdit);
//        	}
//
//        	TreeItem jpurnalEdit = new TreeItem("JOURNAL EDIT");
//        	if (SystemSetting.UserHasRole("JOURNAL EDIT")) {
//        		rootItem.getChildren().add(jpurnalEdit);
//        	}
//        	
//        	
//        	TreeItem openigBalance = new TreeItem("OPENING BALANCE ENTRY");
//        	if (SystemSetting.UserHasRole("OPENING BALANCE ENTRY")) {
//        		rootItem.getChildren().add(openigBalance);
//        	}
//        	
//        	TreeItem accountSlNoUpdation = new TreeItem("ACCOUNT SL.NO UPDATION");
//        	if (SystemSetting.UserHasRole("ACCOUNT SL.NO UPDATION")) {
//        		rootItem.getChildren().add(accountSlNoUpdation);
//        	}
//        	
//        	
//        	
//    	treeview.getSelectionModel().selectedItemProperty().addListener( new ChangeListener() {
//    		   @Override
//               public void changed(ObservableValue observable, Object oldValue,
//                       Object newValue) {
//
//                   TreeItem<String> selectedItem = (TreeItem<String>) newValue;
//                   System.out.println("Selected Text : " + selectedItem.getValue());
//                   selectedText = selectedItem.getValue();
//    	
//    if(selectedItem.getValue().equalsIgnoreCase("CUSTOMER BILLWISE ADJUSTMENT")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.custReciepts);
//    
//    }
//    
//    if(selectedItem.getValue().equalsIgnoreCase( "PDC RECEIPT")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.PDCReceipts);
//    
//    }
//    
//    
//    if(selectedItem.getValue().equalsIgnoreCase("OPENING BALANCE CONFIGURATIONS")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.openingBalanceConfig);
//    
//    }
//    if(selectedItem.getValue().equalsIgnoreCase("PDC PAYMENT")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.PDCPayment);
//    
//    }
//    
//    if(selectedItem.getValue().equalsIgnoreCase( "PDC RECONCILE")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.PDCReconsile);
//    
//    }
//    
//    
//    if(selectedItem.getValue().equalsIgnoreCase( "OPENING BALANCE ENTRY")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.openingBalance);
//    
//    }
//    
//    if(selectedItem.getValue().equalsIgnoreCase("ACCOUNT SL.NO UPDATION")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.accoutnSlNoUpdation);
//    
//    }
//    
//   
//    if(selectedItem.getValue().equalsIgnoreCase( "PETTY-CASH RECIEPTS")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.PettyReciepts);
//    
//    }
//    if(selectedItem.getValue().equalsIgnoreCase( "OWN ACCOUNT SETTLEMENT")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ownAccSettlement);
//    
//    }
//    if(selectedItem.getValue().equalsIgnoreCase("RECIEPTS")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.OtherReciepts);
//    
//    }
//    
//    if(selectedItem.getValue().equalsIgnoreCase( "SUPPLIER BILLWISE ADJUSTMENT")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.supPayments);
//    
//    	
//    }
//    if(selectedItem.getValue().equalsIgnoreCase( "PETTY-CASH PAYMENTS")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.PettyCashPayments);
//    
//    	
//    }
//    if(selectedItem.getValue().equalsIgnoreCase( "PAYMENTS")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.OtherPayments);
//    
//    	
//    }
//         
//         
//    		   }
//    
//    	  });
    }

    @FXML
    void accountsOnClick(MouseEvent event) {
    	
    	if (null != selectedText) {
			//1. Fetch command and fx from table based on the clicked item text.
			//2. load the fx similar way we did in the main frame
			
		 
			HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
			
			
			
			ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName(selectedText);
			
			List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
			
			MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
			
			
			ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(aMenuConfigMst.getId());
			List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();
	
			
			for(MenuConfigMst aMenuConfig : menuConfigMstList)
			{
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != aMenuConfig.getMenuFxml())
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(aMenuConfig.getMenuName(), dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
	 
				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						/*MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
						 
						*/
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
			}
			
    	}
    }
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}
}
    


