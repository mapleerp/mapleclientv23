package com.maple.mapleclient.controllers;



import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.PharmacyGroupWiseSalesReport;
import com.maple.mapleclient.entity.PharmacySalesDetailReport;
import com.maple.mapleclient.entity.PharmacySalesReturnReport;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
public class PharmacySalesDetailReportCtl {
	String taskid;
	String processInstanceId;

	private EventBus eventBus = EventBusFactory.getEventBus();
	
	private ObservableList<PharmacySalesDetailReport> pharmacySalesDtlReport = FXCollections.observableArrayList();
	
	   @FXML
	    private Button btnShowReport;

	    @FXML
	    private Button btPrintReport;

	    @FXML
	    private Button btnAdd;

	    @FXML
	    private ListView<String> lstItem;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private DatePicker dpFromDate;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private ComboBox<String> cmbBranch;

	    @FXML
	    private TextField txtCategoryName;

	    @FXML
	    private TableView<PharmacySalesDetailReport> tblPharmacyGroupWiserSales;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, String> clVoucherNumber;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, String> clInvoiceDate;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, String> clCustomerName;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, Number> clInvoiceAmount;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, String> clItemName;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, String> clGroupName;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, Number> clAmount;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, String> clBatchCode;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, Number> clQty;

	    @FXML
	    private TableColumn<PharmacySalesDetailReport, Number> clTax;

	    Double totalamount=0.0;
	    @FXML
	    private TextField txtTotalAmount;

	 

	  





	    @FXML
	    void clear(ActionEvent event) {
	    	clear();
	    }

	    @FXML
	    void onEnterGrop(ActionEvent event) {
	  	  loadCategoryPopup();
	    }

	    @FXML
	    void printReport(ActionEvent event) {
	    	if (null == cmbBranch.getValue()) {
				notifyMessage(5, "Please select branch", false);
				return;
			}

			if (null == dpFromDate.getValue()) {
				notifyMessage(5, "Please select from date", false);
				return;
			}

			if (null == dpToDate.getValue()) {
				notifyMessage(5, "Please select to date", false);
				return;
			}
			if (null == txtCategoryName.getText()) {
				notifyMessage(5, "Please select Category", false);
				return;
			}

			java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String fdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date t1Date = Date.valueOf(dpToDate.getValue());
			String tDate = SystemSetting.UtilDateToString(t1Date, "yyy-MM-dd");

			List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();

			String cat = "";
			for (String s : selectedItems) {
				s = s.concat(";");
				cat = cat.concat(s);
			}
			String branchCode =SystemSetting.systemBranch;
				try {
				JasperPdfReportService.printPharmacyGroupWiseSalesDtlReport(fdate, tDate, cmbBranch.getValue(), cat);
				} catch (Exception e) {
				
				e.printStackTrace();
				}
			
	    	
	    }

		 private void loadCategoryPopup() {
				
				try {
					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
					Parent root1;
					root1 = (Parent) fxmlLoader.load();
					Stage stage = new Stage();
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.initStyle(StageStyle.UNDECORATED);
					stage.setTitle("ABC");
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.setScene(new Scene(root1));
					stage.show();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
	    

		  public void clear() {
			  
			  
			  dpFromDate.setValue(null);
			  dpToDate.setValue(null);
			  cmbBranch.setValue(null);
			  lstItem.getItems().clear();
			  tblPharmacyGroupWiserSales.getItems().clear();
			  txtTotalAmount.clear();
			  
			  
		  }
		  
		 	private void setBranches() {
		   		
		   		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		   		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		   		branchMstList = branchMstRep.getBody();
		   		
		   		for(BranchMst branchMst : branchMstList)
		   		{
		   			cmbBranch.getItems().add(branchMst.getBranchCode());
		   		
		   			
		   		}
		   		 
		   	}

		    @FXML
		    void AddItem(ActionEvent event) {
		    	lstItem.getItems().add(txtCategoryName.getText());
		    	lstItem.getSelectionModel().select(txtCategoryName.getText());
		    	txtCategoryName.clear();
		    
		    }

		  @Subscribe
				public void popupOrglistner(CategoryEvent CategoryEvent) {

					Stage stage = (Stage) btPrintReport.getScene().getWindow();
					if (stage.isShowing()) {

						
						txtCategoryName.setText(CategoryEvent.getCategoryName());
				
					

					}
		  }

	    @FXML
	   	private void initialize() {
	    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
	    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		  setBranches(); eventBus.register(this);
		  lstItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		 
	    }
	    
	    
	    @FXML
	    void showReport(ActionEvent event) {
          
	    	totalamount=0.0;

	    	if(null==dpFromDate.getValue()) {
	    		notifyMessage(5, "select from date",false);
	    		return;
	    	}
	    	if(null==dpToDate.getValue()) {
	    		notifyMessage(5, "select to date",false);
	    		return;
	    	}
	    	if(null==cmbBranch.getValue()) {
	    		notifyMessage(5, "select Branch",false);
	    		return;
	    	}
	    	if(null==lstItem.getSelectionModel().getSelectedItem()) {
	    		notifyMessage(5, "please add Grouph",false);
	    		return;
	    	}
	    	
			
	    	
	  List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
	        
	    	String cat="";
	    	for(String s:selectedItems)
	    	{
	    		s = s.concat(";");
	    		cat= cat.concat(s);
	    	} 
	    	 	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
				String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
				java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
				String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
				
		
			ResponseEntity<List<PharmacySalesDetailReport>> response=RestCaller.getPharmacSalesDtlReport(startDate,endDate,cmbBranch.getSelectionModel().getSelectedItem(),cat);
		    pharmacySalesDtlReport = FXCollections.observableArrayList(response.getBody());
		
	    
	    filltable();
	    	
	    	
	    	
	       	for(PharmacySalesDetailReport groupWiseSaleReportDtl:pharmacySalesDtlReport)
	    	{
	    		if(null != groupWiseSaleReportDtl.getAmount())
	    		totalamount = totalamount + groupWiseSaleReportDtl.getAmount();
	    	}
	    	BigDecimal settoamount = new BigDecimal(totalamount);
			settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtTotalAmount.setText(settoamount.toString());

	    }
	    
	    public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
	    
		public void filltable(){
		    tblPharmacyGroupWiserSales.setItems(pharmacySalesDtlReport);

			clInvoiceDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
			clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
			//clInvoiceDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
			clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
			clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			clGroupName.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
			clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
			clBatchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
			clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
			//clTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());
			
		}
		@Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }
}
