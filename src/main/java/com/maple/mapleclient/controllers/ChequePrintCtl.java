package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ChequePrintMst;
import com.maple.mapleclient.entity.ConsumptionDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class ChequePrintCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<ChequePrintMst> chequeprintList = FXCollections.observableArrayList();

	ChequePrintMst chequePrintMst = null;
    @FXML
    private ComboBox<String> cmbchequeProperty;

    @FXML
    private TextField txtValue;

    @FXML
    private Button btnSave;

    @FXML
    private Button btndelete;

    @FXML
    private Button btnShow;
    @FXML
    private Button btnClear;
    @FXML
    private ComboBox<String> cmbBank;

    @FXML
    private TableView<ChequePrintMst> tbChequePrint;

    @FXML
    private TableColumn<ChequePrintMst, String> clBankName;

    @FXML
    private TableColumn<ChequePrintMst, String> clChequeProperty;

    @FXML
    private TableColumn<ChequePrintMst, String> clValue;
    @FXML
    void initialize()
    {
    	cmbchequeProperty.getItems().add("CHEQUECROSSLINE1X1");
    	cmbchequeProperty.getItems().add("CHEQUECROSSLINE1Y1");
    	cmbchequeProperty.getItems().add("CHEQUECROSSLINE1X2");
    	cmbchequeProperty.getItems().add("CHEQUECROSSLINE1Y2");
    	cmbchequeProperty.getItems().add("CHEQUECROSSLINE2X1");
    	cmbchequeProperty.getItems().add("CHEQUECROSSLINE2Y1");
    	cmbchequeProperty.getItems().add("CHEQUECROSSLINE2X2");
    	cmbchequeProperty.getItems().add("CHEQUECROSSLINE2Y2");
    	cmbchequeProperty.getItems().add("CHEQUEDATEX");
    	cmbchequeProperty.getItems().add("CHEQUEDATEY");
    	cmbchequeProperty.getItems().add("CHEQUEACCOUNTNAMEX");
    	cmbchequeProperty.getItems().add("CHEQUEACCOUNTNAMEY");
    	cmbchequeProperty.getItems().add("CHEQUEAMOUNTINWORDSX");
    	cmbchequeProperty.getItems().add("CHEQUEAMOUNTINWORDSY");
    	cmbchequeProperty.getItems().add("CHEQUEAMOUNTX");
    	cmbchequeProperty.getItems().add("CHEQUEAMOUNTY");
    	cmbchequeProperty.getItems().add("CHEQUEPRINTFONTSIZE");

    	ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid= lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbBank.getItems().add((String) bankName);
				
						    //accountHeads.getAccountName);
			}
		}
		
		tbChequePrint.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					chequePrintMst = new ChequePrintMst();
					chequePrintMst.setId(newSelection.getId());
					chequePrintMst.getAccountId();
					ResponseEntity<AccountHeads> accHeadById = RestCaller.getAccountById(newSelection.getAccountId());
					cmbBank.getSelectionModel().select(accHeadById.getBody().getAccountName());
//					txtBatch.setText(newSelection.getBatch());
//					txtMrp.setText(Double.toString(newSelection.getMrp()));
//					txtQty.setText(Double.toString(newSelection.getQty()));
				}
			}
		});
    }
    @FXML
    void actionClear(ActionEvent event) {

    	cmbBank.getSelectionModel().clearSelection();
    	cmbchequeProperty.getSelectionModel().clearSelection();
    	txtValue.clear();
    	tbChequePrint.getItems().clear();
    	chequeprintList.clear();
    	
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == chequePrintMst)
    	{
    		return;
    	}
    	if(null == chequePrintMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteChequePrintMstById(chequePrintMst.getId());
    	ResponseEntity<AccountHeads> getAccHeadByName = RestCaller.getAccountHeadByName(cmbBank.getSelectionModel().getSelectedItem());
    	ResponseEntity<List<ChequePrintMst>> chequePrintMst = RestCaller.getallChequePrintByAccountId(getAccHeadByName.getBody().getId());
    	chequeprintList = FXCollections.observableArrayList(chequePrintMst.getBody());
    	fillTable();
    }

    @FXML
    void actionSave(ActionEvent event) {

    	chequePrintMst = new ChequePrintMst();
    	ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(cmbBank.getSelectionModel().getSelectedItem());
    	chequePrintMst.setAccountId(accountHeads.getBody().getId());
    	chequePrintMst.setChequeProperty(cmbchequeProperty.getSelectionModel().getSelectedItem());
    	chequePrintMst.setChequeValue(txtValue.getText());
    	ResponseEntity<ChequePrintMst> respentity = RestCaller.saveChequePrintMst(chequePrintMst);
    	chequePrintMst = respentity.getBody();
    	chequeprintList.add(chequePrintMst);
    	fillTable();
    	cmbchequeProperty.getSelectionModel().clearSelection();
    	txtValue.clear();
    	
    }

    private void fillTable()
    {
    	for(ChequePrintMst chque : chequeprintList)
    	{
    		ResponseEntity<AccountHeads> accHeads = RestCaller.getAccountById(chque.getAccountId());
    		chque.setAccountName(accHeads.getBody().getAccountName());
    	}
    	tbChequePrint.setItems(chequeprintList);
    	clBankName.setCellValueFactory(cellData -> cellData.getValue().getaccountNameProperty());
    	clChequeProperty.setCellValueFactory(cellData -> cellData.getValue().getchequePropertyProperty());
    	clValue.setCellValueFactory(cellData -> cellData.getValue().getchequeValueProperty());
    
    }
    @FXML
    void actionShow(ActionEvent event) {
    	if(null == cmbBank.getSelectionModel().getSelectedItem())
    	{
    		notifyMessage(3,"Please Select Bank");
    		cmbBank.requestFocus();
    		return;
    		
    	}
    	ResponseEntity<AccountHeads> getAccHeadByName = RestCaller.getAccountHeadByName(cmbBank.getSelectionModel().getSelectedItem());

    	ResponseEntity<List<ChequePrintMst>> chequePrintMst = RestCaller.getallChequePrintByAccountId(getAccHeadByName.getBody().getId());
    	chequeprintList = FXCollections.observableArrayList(chequePrintMst.getBody());
    	fillTable();
    }
    public void notifyMessage(int duration, String msg) {
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}
    @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	     private void PageReload() {
	     	
	}
}
