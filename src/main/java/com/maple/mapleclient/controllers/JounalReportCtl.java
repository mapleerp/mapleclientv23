package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportJournalReport;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.JournalHdr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
public class JounalReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<JournalHdr> journalHdrList = FXCollections.observableArrayList();
	private ObservableList<JournalDtl> journalDtlList = FXCollections.observableArrayList();
	ExportJournalReport exportJournalReport=new ExportJournalReport();
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private TableView<JournalHdr> tbReport;

    @FXML
    private TableColumn<JournalHdr,String > clVoucherDate;

    @FXML
    private TableColumn<JournalHdr, String> clVoucherNumber;

    @FXML
    private TableColumn<JournalHdr, String> clumnTransDate;

    @FXML
    private Button btnShow;

    @FXML
    private TableView<JournalDtl> tbreports;

    @FXML
    private TableColumn<JournalDtl, String> clAccountId;

    @FXML
    private TableColumn<JournalDtl, Number> clDebitAmount;

    @FXML
    private TableColumn<JournalDtl, Number> clAmount;

    @FXML
    private TableColumn<JournalDtl, String> clInstrumentNumber;

    @FXML
    private TableColumn<JournalDtl, String> clRemark;

    @FXML
    private TableColumn<JournalDtl, Number> clCrAmount;

    @FXML
    private ComboBox<String> branchselect;

    @FXML
    private Button ok;

    @FXML
    private Button btnPrint;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnExcelExport;

    @FXML
    void exportToExcel(ActionEvent event) {
    	if(null == branchselect.getValue())
    	{
    		notifyMessage(5, "Please select branch", false);
    		return;
    	}
    	if(null==dpFromDate.getValue()) {
    		notifyMessage(5, "Please select From date", false);
   		
   		 
    	}if(null==dpToDate.getValue()) {
    		notifyMessage(5, "Please select From date", false);
    	}else {
    
    	LocalDate dpFDate = dpFromDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpFDate);
		 String  startDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 
		 
		 LocalDate dpTDate = dpToDate.getValue();
			java.util.Date utDate  = SystemSetting.localToUtilDate(dpTDate);
			 String  endDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
    	

				ResponseEntity<List<JournalDtl>> journalResponse = RestCaller.getJournalReportPrint(startDate,endDate,branchselect.getValue()
					);
				List<JournalDtl> journalDtlReportList = journalResponse.getBody();

				exportJournalReport.exportToExcel("JournalReportToExcel"+startDate+".xls", journalDtlReportList, startDate,endDate);
    	
    }}

    @FXML
    void onClickOk(ActionEvent event) {

    }

    @FXML
    void reportPrint(ActionEvent event) {

    	if(null == branchselect.getValue())
    	{
    		notifyMessage(5, "Please select branch", false);
    		return;
    	} 
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
    		return;
    	} 
    	
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, "Please select to date", false);
    		return;
    	} 
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	
 	   
    	String branchCode=  SystemSetting.systemBranch;
    	   try {

    		   JasperPdfReportService.printJournalReportDtl( startDate,endDate,branchselect.getValue());
    	   }catch (Exception e) {
    		// TODO: handle exception
    	}
    	
    	
    	
    }

    @FXML
    void show(ActionEvent event) {
    	tbReport.getItems().clear();
    	tbreports.getItems().clear();
    	if(null == branchselect.getValue())
    	{
    		notifyMessage(5, "Please select branch", false);
    		return;
    	} 
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
    		return;
    	} 
    	
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, "Please select to date", false);
    		return;
    	} 
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	ResponseEntity<List<JournalHdr>> resp=RestCaller.getJournalHdrReport(startDate,endDate,branchselect.getValue());
    	journalHdrList = FXCollections.observableArrayList(resp.getBody());

    	FillTable();
    }
    
    private void FillTable()
	{
		tbReport.setItems(journalHdrList);
	
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
		clumnTransDate.setCellValueFactory(cellData -> cellData.getValue().getTransDateProperty());
		
	}
	

    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}
    }
    
    
    
    
    @FXML
	 	private void initialize() {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
	    	setBranches();
	    	
	    	  tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
		    		if (newSelection != null) {
		    			System.out.println("getSelectionModel");
		    			if (null != newSelection.getId()) {
		    				System.out.println("getSelectionModel--getVoucherNumber");
		    				 String voucher=newSelection.getVoucherNumber();
		    				System.out.println(voucher);
		    				String voucherNumber=newSelection.getVoucherNumber();
		    				String journalid=newSelection.getId();
		    				
		    				System.out.println("........" + voucherNumber);
		    				String branchName=branchselect.getValue();
		    				ResponseEntity<List<JournalDtl>> receiptDtlList = RestCaller.getJurnalDtl(journalid);
		    				journalDtlList = FXCollections.observableArrayList(receiptDtlList.getBody());
		    				tbreports.setItems(journalDtlList);
		    				System.out.print(journalDtlList.size()+"receipt detail list size issssssss");
		    				
		    				System.out.print("after filltable");
		    				
		    				FillTables();
		    			}
		    			
		    		}
		    	});
	    }
    
    
    private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			branchselect.getItems().add(branchMst.getBranchCode());
		
			
		}
		 
	}
	    
    
    
	
 	  private void FillTables() {
 	  
	  System.out.print("inside fill table isssssssssssssssssssssss"); 	 
	  
	    clAccountId.setCellValueFactory(cellData -> cellData.getValue().getAccountHeadNameProperty());
 		clDebitAmount.setCellValueFactory(cellData -> cellData.getValue().getdebitAmountProperty());
 		clCrAmount.setCellValueFactory(cellData -> cellData.getValue().getcreditAmountProperty());
		clRemark.setCellValueFactory(cellData -> cellData.getValue().getremarksProperty());
 		
 	  
 	  
 	  
 	  }
 	 @Subscribe
    	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
    		//Stage stage = (Stage) btnClear.getScene().getWindow();
    		//if (stage.isShowing()) {
    			taskid = taskWindowDataEvent.getId();
    			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
    			
    		 
    			String hdrId = taskWindowDataEvent.getBusinessProcessId();
    			System.out.println("Business Process ID = " + hdrId);
    			
    			 PageReload();
    		}


      private void PageReload() {
      	
    }

}
