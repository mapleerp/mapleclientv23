package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportCustomerSalesToExcel;
import com.maple.maple.util.ExportStatementOfAccountToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.AccountClass;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;
import com.maple.report.entity.TallyImportReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class AccountBalanceCtl {
	String taskid;
	String processInstanceId;

	ExportStatementOfAccountToExcel exportStatementOfAccountToExcel = new ExportStatementOfAccountToExcel();
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<AccountBalanceReport> reportList = FXCollections.observableArrayList();
	VoucherNoEvent vno = null;
	String accountId = "";
	Double openingBalance = 0.0;
	String accountName;
	Integer slNo;
	HashMap taskListWindows = new HashMap();
	
	@FXML
	private TextField txtAccount;

	@FXML
	private DatePicker dpStartDate;

	@FXML
	private ComboBox<String> cmbMode;

	@FXML
	private DatePicker dpEndDate;

	@FXML
	private Button btnGenerateReport;

	@FXML
	private Button btnShowReport;
	@FXML
	private Button btnExportToExcel;

	@FXML
	private TableView<AccountBalanceReport> tbAccountBalance;

	@FXML
	private TableColumn<AccountBalanceReport, String> clSlNo;

	@FXML
	private TableColumn<AccountBalanceReport, LocalDate> clDate;

	@FXML
	private TableColumn<AccountBalanceReport, String> clInvoiceNumber;

	@FXML
	private TableColumn<AccountBalanceReport, Number> clDebit;

	@FXML
	private TableColumn<AccountBalanceReport, Number> clCredit;

	@FXML
	void keyPressedExportToExcel(KeyEvent event) {

	}

	@FXML
	void FocusOnBotton(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			showPopup();

		}

	}

	@FXML
	void FocusOnEndDate(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			dpEndDate.requestFocus();

		}
	}

	@FXML
	void actionShowReport(ActionEvent event) {

		tbAccountBalance.getItems().clear();
		reportList.clear();
		if (txtAccount.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Account", false);
			txtAccount.requestFocus();
			return;
		}
		Date sDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sDate, "yyyy-MM-dd");

		Date eDate = SystemSetting.localToUtilDate(dpEndDate.getValue());
		String endDate = SystemSetting.UtilDateToString(eDate, "yyyy-MM-dd");
		if (null == cmbMode.getSelectionModel().getSelectedItem()
				|| cmbMode.getSelectionModel().getSelectedItem().equalsIgnoreCase("MULTI MODE")) {
			
			ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller
					.getAccountBalanceReport(startDate, endDate, accountId);
			reportList = FXCollections.observableArrayList(accountBalanceReport.getBody());
			
		} else if (cmbMode.getSelectionModel().getSelectedItem().equalsIgnoreCase("SINGLE MODE")) {
			
			ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller
					.getAccountBalanceReportMSingleMode(startDate, endDate, accountId);
			reportList = FXCollections.observableArrayList(accountBalanceReport.getBody());

		}
		fillTable();
	}

	private void fillTable() {
		tbAccountBalance.setItems(reportList);
		clCredit.setCellValueFactory(cellData -> cellData.getValue().getCreditProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getdateProperty());
		clDebit.setCellValueFactory(cellData -> cellData.getValue().getDebitProperty());
		clInvoiceNumber.setCellValueFactory(cellData -> cellData.getValue().getRemarkProperty());
		clSlNo.setCellValueFactory(cellData -> cellData.getValue().getslnoProperty());

	}

	@FXML
	void FocusOnTxtAccount(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtAccount.requestFocus();

		}

	}

	@FXML
	void GenerateReport(ActionEvent event) {

		generateReport();

	}

	@FXML
	void KeyPressGenerateReport(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			generateReport();

		}

	}

	@FXML
	private void initialize() {
		
		
		
		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
		cmbMode.getItems().add("SINGLE MODE");
		cmbMode.getItems().add("MULTI MODE");
		eventBus.register(this);
		tbAccountBalance.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getAccountHeads()) {
					AccountClass accountClass = RestCaller.getaccountClassById(newSelection.getAccountClassId());
					if (null != accountClass) {
						if (accountClass.getVoucherType().equalsIgnoreCase("JOURNAL")) {
							if (SystemSetting.UserHasRole("JOURNAL EDIT")) {
								vno = new VoucherNoEvent();
								vno.setVoucherNumber(accountClass.getSourceVoucherNumber());
								vno.setId(accountClass.getSourceParentId());
								loadJournalWindow("JournalEdit.fxml");
							}
						} else if (accountClass.getVoucherType().equalsIgnoreCase("RECEIPT")) {
							if (SystemSetting.UserHasRole("RECEIPT EDIT")) {
								vno = new VoucherNoEvent();
								vno.setVoucherNumber(accountClass.getSourceVoucherNumber());
								vno.setId(accountClass.getSourceParentId());
								loadReceiptWindow("ReceiptEdit.fxml");
							}
						} else if (accountClass.getVoucherType().equalsIgnoreCase("PAYMENT")) {
							if (SystemSetting.UserHasRole("PAYMENT EDIT")) {
								vno = new VoucherNoEvent();
								vno.setVoucherNumber(accountClass.getSourceVoucherNumber());
								vno.setId(accountClass.getSourceParentId());
								loadPaymentWindow("PaymentEdit.fxml");
							}
						}

					}
				}
			}
		});
	}

	private void loadJournalWindow(String windowName) {

		try {

			Node dynamicWindow = (Node) taskListWindows.get(windowName);
			if (null == dynamicWindow) {

				dynamicWindow = FXMLLoader.load(getClass().getResource("/fxml/" + windowName));
				taskListWindows.put(windowName, dynamicWindow);

				// task newSelection.getId()
			}

			try {

				// Clear screen before loading the Page.
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

				MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		eventBus.post(vno);
	}

	private void loadReceiptWindow(String windowName) {
		try {

			Node dynamicWindow = (Node) taskListWindows.get(windowName);
			if (null == dynamicWindow) {

				dynamicWindow = FXMLLoader.load(getClass().getResource("/fxml/" + windowName));
				taskListWindows.put(windowName, dynamicWindow);

				// task newSelection.getId()
			}

			try {

				// Clear screen before loading the Page.
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

				MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		eventBus.post(vno);
	}

	private void loadPaymentWindow(String windowName) {
		try {

			Node dynamicWindow = (Node) taskListWindows.get(windowName);
			if (null == dynamicWindow) {

				dynamicWindow = FXMLLoader.load(getClass().getResource("/fxml/" + windowName));
				taskListWindows.put(windowName, dynamicWindow);

				// task newSelection.getId()
			}

			try {

				// Clear screen before loading the Page.
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

				MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
				MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		eventBus.post(vno);
	}

	private void generateReport() {

		if (null == dpStartDate.getValue()) {
			notifyMessage(5, "Please select From date", false);

		} else if (null == dpEndDate.getValue()) {

			notifyMessage(5, "Please select End date", false);

		} else if (txtAccount.getText().trim().isEmpty()) {

			notifyMessage(5, "Please select Account", false);

		} else {

			Date sDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
			String startDate = SystemSetting.UtilDateToString(sDate, "yyyy-MM-dd");

			Date eDate = SystemSetting.localToUtilDate(dpEndDate.getValue());
			String endDate = SystemSetting.UtilDateToString(eDate, "yyyy-MM-dd");

			if (!accountId.equals("") && !accountId.equals(null)) {
				if (null == cmbMode.getSelectionModel().getSelectedItem()
						|| cmbMode.getSelectionModel().getSelectedItem().equalsIgnoreCase("MULTI MODE")) {
					// call jasperPdf
					try {
						JasperPdfReportService.AccountBalanceReport(startDate, endDate, accountId);
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					try {
						//AccountBalanceReportSingleMode
						//
						JasperPdfReportService.AccountBalanceReport(startDate, endDate, accountId);
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}

	}

	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			btnGenerateReport.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(AccountPopupEvent accountPopupEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) txtAccount.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountPopupEvent.getAccountName());
			accountId = accountPopupEvent.getAccountId();

		}

	}

	@FXML
	void AccounOnClick(MouseEvent event) {

		showPopup();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	void exportToExcel(ActionEvent event) {

		if (null == dpStartDate.getValue()) {

			notifyMessage(5, "Please select date", false);

		} else {

			java.util.Date uDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
			String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date uDate1 = SystemSetting.localToUtilDate(dpEndDate.getValue());
			String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");

			ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller
					.getAccountBalanceReport(strDate, endDate, accountId);
			List<AccountBalanceReport> statementOfAccountToTally = new ArrayList();

			statementOfAccountToTally = accountBalanceReport.getBody();
			if (null != statementOfAccountToTally.get(0).getOpeningBalance()) {
				openingBalance = statementOfAccountToTally.get(0).getOpeningBalance();
			} else {
				openingBalance = 0.0;
			}
			if (null != statementOfAccountToTally.get(0).getAccountHeads()) {
				accountName = statementOfAccountToTally.get(0).getAccountHeads();

			}
			slNo = statementOfAccountToTally.get(0).getSlNo();
			Date date = statementOfAccountToTally.get(0).getDate();
			exportStatementOfAccountToExcel.exportToExcel("StatementOfAccountToExcel" + strDate + ".xls",
					statementOfAccountToTally, strDate, endDate, accountName, openingBalance, slNo, date);

		}
	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload();
			}


	    private void PageReload() {
	    	
	    	
			
		}

}
