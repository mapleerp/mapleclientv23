package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class FinancialYearCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<FinancialYearMst> financialYrList = FXCollections.observableArrayList();

	//version2.0
	FinancialYearMst financialYearMst = null;
    @FXML
    private DatePicker dpStartDate;

    @FXML
    private DatePicker dpEndDate;
    @FXML
    private Button btnChangeFinancialYr;

    @FXML
    private ComboBox<String> cmbOpenCloseStatus;
    @FXML
    private Button btnSave;
    @FXML
    private TableView<FinancialYearMst> tbFinancialYr;

    @FXML
    private TableColumn<FinancialYearMst, String> clStartDate;

    @FXML
    private TableColumn<FinancialYearMst, String> clEndDate;

    @FXML
    private TableColumn<FinancialYearMst,String> clFinancialYr;
    
    @FXML
    private TableColumn<FinancialYearMst, String> clCurrentFinancYr;
    @FXML
	private void initialize() {
    	
    	dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
    	dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
    	cmbOpenCloseStatus.getItems().add("OPEN");
    	cmbOpenCloseStatus.getItems().add("CLOSE");
    	ResponseEntity<List<FinancialYearMst>> getAllFinancialYrMst = RestCaller.getAllFinancialYear();
    	financialYrList = FXCollections.observableArrayList(getAllFinancialYrMst.getBody());
    	fillTable();
    	
    	tbFinancialYr.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					financialYearMst = new FinancialYearMst();
					financialYearMst.setId(newSelection.getId());
					financialYearMst.setStartDate(newSelection.getStartDate());
					financialYearMst.setEndDate(newSelection.getEndDate());
					financialYearMst.setCompanyMst(newSelection.getCompanyMst());
					financialYearMst.setCurrentFinancialYear(newSelection.getCurrentFinancialYear());
					financialYearMst.setFinancialYear(newSelection.getFinancialYear());
					financialYearMst.setOpenCloseStatus(newSelection.getOpenCloseStatus());
					financialYearMst.setUserName(newSelection.getUserName());
				}
			}
		});
    }
    
    private void fillTable()
    {
    	tbFinancialYr.setItems(financialYrList);
		clCurrentFinancYr.setCellValueFactory(cellData -> cellData.getValue().getcurrentFinancialYrProperty());
		clEndDate.setCellValueFactory(cellData -> cellData.getValue().getendDateProperty());
		clFinancialYr.setCellValueFactory(cellData -> cellData.getValue().getfinancialYrProperty());
		clStartDate.setCellValueFactory(cellData -> cellData.getValue().getstartDateProperty());


    }
 
    @FXML
    void actionSave(ActionEvent event) {
    	financialYearMst = new FinancialYearMst();
    	financialYearMst.setCurrentFinancialYear(1);
    	financialYearMst.setEndDate(Date.valueOf(dpEndDate.getValue()));
    	financialYearMst.setStartDate(Date.valueOf(dpStartDate.getValue()));
    	financialYearMst.setUserName("System");
    	financialYearMst.setOpenCloseStatus("OPEN");
    	java.util.Date usdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
    	String ssdate = SystemSetting.UtilDateToString(usdate , "yyyy-MM-dd");
    	java.util.Date uedate = SystemSetting.localToUtilDate(dpEndDate.getValue());
    	String sedate = SystemSetting.UtilDateToString(uedate, "yyyy-MM-dd");
    	String subSdate = ssdate.substring(0, 4);
    	String subedate =sedate.substring(0, 4);
    	financialYearMst.setFinancialYear(subSdate+"-"+subedate);
    	
    	ResponseEntity<List<FinancialYearMst>>resp=RestCaller.cheackFinancialYearDuplication(ssdate, sedate);
    	List<FinancialYearMst> financialYearMstList=resp.getBody();
    	if(financialYearMstList.size()==0) {
    	ResponseEntity<FinancialYearMst> respentity = RestCaller.saveFinancialYearMst(financialYearMst);
    	
    	financialYearMst = respentity .getBody();
    	financialYrList.add(financialYearMst);
    	fillTable();
    	SystemSetting.setFinancialYear(financialYearMst.getFinancialYear());
    	
    	notifyMessage(3,"Financial Year Saved", true);
    	}else {
    		notifyMessage(3,"Financial Year already added!!!", true);
    	}
    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @FXML
    void actionChangeFinancialYr(ActionEvent event) {

    	String status=null;
    	if(null == financialYearMst)
    	{
    		notifyMessage(3,"Select Any Financial Year", true);
    		return;
    	}
    	
    	if(null == financialYearMst.getId())
    	{
    		notifyMessage(3,"Select Any Financial Year", true);
    		return;
    	}
    	if(null == cmbOpenCloseStatus.getSelectionModel().getSelectedItem())
    	{
    		status = "OPEN";
    	}
    	else
    	{
    		status = cmbOpenCloseStatus.getSelectionModel().getSelectedItem();
    	}
    	financialYearMst.setOpenCloseStatus(status);
    	RestCaller.updateFinancialYrOpeninClosingStatus(financialYearMst);
		String enddate = SystemSetting.UtilDateToString(financialYearMst.getEndDate(), "yyy-MM-dd");
    	String msg = RestCaller.changeFinancialYear(enddate,financialYearMst.getId());
    	notifyMessage(3,"Financial Year Changed", true);
    	ResponseEntity<List<FinancialYearMst>> getAllFinancialYrMst = RestCaller.getAllFinancialYear();
    	financialYrList = FXCollections.observableArrayList(getAllFinancialYrMst.getBody());
    	fillTable();
    }
  //version2.0ends
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}