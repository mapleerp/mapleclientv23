//package com.maple.mapleclient.controllers;
//
//import java.io.IOException;
//
//import com.google.common.eventbus.Subscribe;
//import com.maple.mapleclient.events.TaskWindowDataEvent;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//
//import org.controlsfx.control.Notifications;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//
//import com.google.common.eventbus.EventBus;
//import com.google.common.eventbus.Subscribe;
//import com.maple.maple.util.SystemSetting;
//import com.maple.mapleclient.EventBusFactory;
//import com.maple.mapleclient.MapleclientApplication;
//import com.maple.mapleclient.entity.AccountHeads;
//import com.maple.mapleclient.entity.CurrencyMst;
//import com.maple.mapleclient.entity.ParamValueConfig;
//import com.maple.mapleclient.entity.ProductionDtl;
//import com.maple.mapleclient.entity.StockTransferOutDtl;
//import com.maple.mapleclient.entity.Supplier;
//import com.maple.mapleclient.events.SupplierPopupEvent;
//import com.maple.mapleclient.restService.RestCaller;
//
//import javafx.beans.property.SimpleStringProperty;
//import javafx.beans.property.StringProperty;
//import javafx.beans.value.ChangeListener;
//import javafx.beans.value.ObservableValue;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.fxml.FXML;
//import javafx.fxml.FXMLLoader;
//import javafx.geometry.Pos;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.scene.control.Alert;
//import javafx.scene.control.Button;
//import javafx.scene.control.CheckBox;
//import javafx.scene.control.ComboBox;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.scene.control.Alert.AlertType;
//import javafx.scene.control.TableView.TableViewSelectionModel;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.KeyEvent;
//import javafx.scene.input.MouseEvent;
//import javafx.stage.Modality;
//import javafx.stage.Stage;
//import javafx.stage.StageStyle;
//import javafx.util.Duration;
//import javafx.util.StringConverter;
//import javafx.util.converter.NumberStringConverter;
//
//public class SupplierCtl {
//	
//	String taskid;
//	String processInstanceId;
//
//	String CUSTOM_VOUCHER_PROPERTY;
//	
//
//	EventBus eventBus = EventBusFactory.getEventBus();
//	Supplier supplier;
//
//	private ObservableList<Supplier> supplierList = FXCollections.observableArrayList();
//	private ObservableList<Supplier> supplierList1 = FXCollections.observableArrayList();
//	StringProperty SearchString = new SimpleStringProperty();
//
//    @FXML
//    private ComboBox<String> cmbCountry;
//	@FXML
//	private TextField txtSupplierName;
//
//	@FXML
//	private TextField txtCompanyName;
//
//	@FXML
//	private TextField txtAddress;
//
//	@FXML
//	private TextField txtPhoneNo;
//
//	@FXML
//	private TextField txtEmailid;
//
//    @FXML
//    private TextField txtCreditPeriod;
//    
//	@FXML
//	private Button btnSave;
//
//	@FXML
//	private Button btnDelete;
//
//	@FXML
//	private Button btnEdit;
//
//    @FXML
//    private ComboBox<String> cmbCurrency;
//	@FXML
//	private TableView<Supplier> tblSupplier;
//
//    @FXML
//    private TextField txtGst;
//    @FXML
//    private TableColumn<Supplier, String> clSupName;
//
//    @FXML
//    private TableColumn<Supplier, String> clSupCompany;
//
//	@FXML
//    private Button btnShowAll;
//	
//    @FXML
//    private TableColumn<Supplier, String> clSupAddress;
//
//    @FXML
//    private TableColumn<Supplier, String> clSupPhno;
//
//    @FXML
//    private TableColumn<Supplier, String> clEmail;
//
//    @FXML
//    private TableColumn<Supplier, String> clGST;
//    @FXML
//    private TableColumn<Supplier, String> clState;
//    
//    @FXML
//    private ComboBox<String> cmbSupplierState;
//
//    
//    @FXML
//    private CheckBox chkOnline;
//    
//   	@FXML
//	void btnDeleteAction(ActionEvent event) {
//
//	}
//
//	@FXML
//	void btnEditAction(ActionEvent event) {
//	
//		supplier.setAddress(txtAddress.getText());
//		supplier.setPhoneNo(txtPhoneNo.getText());
//		supplier.setSupGST(txtGst.getText());
//		String supName = txtSupplierName.getText().trim();
//		
// 		supName = supName.replaceAll("'", "");
// 		supName = supName.replaceAll("&", "");
//		
// 		supName = supName.replaceAll("/", "");
// 		supName = supName.replaceAll("%", "");
// 		supName = supName.replaceAll("\"", "");
// 		supName = supName.replaceAll(" ", "-");
// 		supplier.setSupplierName(supName);
//		 supplier.setCompany(txtCompanyName.getText());
//		 supplier.setEmailid(txtEmailid.getText());
//		 supplier.setCerditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
//		 supplier.setCountry(cmbCountry.getSelectionModel().getSelectedItem());
//		 if(null != cmbSupplierState.getSelectionModel()) {
//		 supplier.setState(cmbSupplierState.getSelectionModel().getSelectedItem());
//			if(null != cmbCurrency.getSelectionModel().getSelectedItem())
//	 		{
//	 			ResponseEntity<CurrencyMst> getcurrency = RestCaller.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem());
//	 			if(null != getcurrency.getBody())
//	 			{
//	 				supplier.setCurrencyId(getcurrency.getBody().getId());
//	 			}
//	 		}
//		 }
////		 else
////		 {
////			 supplier.setState("KERALA");
////		 }
//          RestCaller.updateSupplier(supplier);
//      	notifyMessage(5, "Updated");
//			clearfileds();
//			tblSupplier.getItems().clear();
//			supplierList.clear();
//			btnEdit.setDisable(true);
//			btnSave.setDisable(false);
//	}
//
//	@FXML
//	void onMouseClick(MouseEvent event) {
//
//		loadSupplirePopup();
//
//	}
//
//	@FXML
//	void btnSaveAction(ActionEvent event) {
//		SaveDate();
//	}
//	@FXML
//	private void initialize() {
//		
//		ResponseEntity<List<String>> countryList = RestCaller.getCountryList();
//    	for(int i=0;i<countryList.getBody().size();i++)
//    	{
//    		cmbCountry.getItems().add(countryList.getBody().get(i));
//    	}
//		
//    	cmbCountry.valueProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				cmbSupplierState.getItems().clear();
//				if(!newValue.equalsIgnoreCase(null))
//				{
//					ResponseEntity<List<String>> stateList = RestCaller.getStateListByCountry(cmbCountry.getSelectionModel().getSelectedItem());
//			    	for(int i = 0;i<stateList.getBody().size();i++)
//			    	{
//			    		cmbSupplierState.getItems().add(stateList.getBody().get(i));
//			    	}
//				}
//			}
//
//		});
//    	
//
//    	//check box for deciding the process is done on either offline or online=======01-07-2021
//    			ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("OFFLINE_SUPPLIER_CREATION");
//    			if (null != getParamValue.getBody()) {
//    				if (getParamValue.getBody().getValue().equalsIgnoreCase("YES")) {
//    					chkOnline.setDisable(false);
//    				} else {
//    					chkOnline.setDisable(true);
//    				}
//    			}else {
//    				chkOnline.setDisable(true);
//
//    			}
//    			chkOnline.selectedProperty().addListener(
//    					(obs, oldSelection, newSelection) ->	{
//    						if(chkOnline.isSelected()) {
//    							chkOnline.setText("Offline");
//    						}else {
//    							chkOnline.setText("Online");
//    						}
//    					});
//    			//============================end=====================01-07-2021==============
//    			
//    			
//    	
//		supplier = new Supplier();
//		btnEdit.setDisable(true);
//		txtSupplierName.textProperty().bindBidirectional(SearchString);
//		eventBus.register(this);
//		tblSupplier.setItems(supplierList);
//		
//		ResponseEntity<List<CurrencyMst>> currencyMst = RestCaller.getallCurrencyMst();
//		for(int i=0;i<currencyMst.getBody().size();i++)
//		{
//			cmbCurrency.getItems().add(currencyMst.getBody().get(i).getCurrencyName());
//		}
//		 //account.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());
//		//	remark.setCellValueFactory(cellData -> cellData.getValue().getRemarkProperty());
//		SearchString.addListener(new ChangeListener() {
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//				System.out.println("Supplier Search changed");
//				LoadSupplierBySearch((String) newValue);
//			}
//		});
//		txtPhoneNo.textProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
//					txtPhoneNo.setText(oldValue);
//				}
//			}
//		});
//		txtCreditPeriod.textProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
//					txtCreditPeriod.setText(oldValue);
//				}
//			}
//		});
//	}
//	
//
//	@Subscribe
//	public void popuplistner(SupplierPopupEvent supplierEvent) {
//		
//		
//		Stage stage = (Stage)btnSave.getScene().getWindow();
//		  if(stage.isShowing()) {
// 
//		
//		  }
//
//	}
//
//
//
//    @FXML
//    void SaveOnEnter(KeyEvent event) {
//      SaveDate();
//    }
//	@FXML
//	void OnEnterSupID(KeyEvent event) {
//
//		/*
//		 * On pressing Enter key while focus on Supplier ID, show Supplier list
//		 */
//    	if(event.getCode()==KeyCode.ENTER ) {
//    		loadSupplirePopup();
//    	}
//		
//	}
//
//	private void loadSupplirePopup() {
//		/*
//		 * Function to display popup window and show list of suppliers to select.
//		 */
//		try {
//			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/popup.fxml"));
//			Parent root1;
//
//			root1 = (Parent) fxmlLoader.load();
//			Stage stage = new Stage();
//
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.initStyle(StageStyle.UNDECORATED);
//			stage.setTitle("ABC");
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.setScene(new Scene(root1));
//			stage.show();
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//	private void filltable()
//	{
//		clEmail.setCellValueFactory(cellData -> cellData.getValue().getemailIdProperty());
//		clGST.setCellValueFactory(cellData -> cellData.getValue().getgstProperty());
//		clSupAddress.setCellValueFactory(cellData -> cellData.getValue().getAddressProperty());
//		clSupCompany.setCellValueFactory(cellData -> cellData.getValue().getcompanyProperty());
//		clSupName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
//		clSupPhno.setCellValueFactory(cellData -> cellData.getValue().getphnoProperty());
//		clState.setCellValueFactory(cellData -> cellData.getValue().getstateProperty());
//						
//	}
//	private void LoadSupplierBySearch(String searchData) {
//		tblSupplier.getItems().clear();
//		supplierList.clear();
//		ArrayList sup = new ArrayList();
//
//		sup = RestCaller.SearchSupplierByName(searchData);
//		Iterator itr = sup.iterator();
//		while (itr.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr.next();
//			
//			Object supName = lm.get("supplierName");
//			Object company = lm.get("company");
//			Object addresss =lm.get("address");
//			Object phno = lm.get("phoneNo");
//			Object email = lm.get("emailid");
//			Object gst = lm.get("supGST");
//			Object state = lm.get("state");
//			Object currencyId = lm.get("currencyId");
//			Object country = lm.get("country");
//			if (null != supName)
//			{
//			   Supplier supplier = new Supplier();
//			   supplier.setSupplierName((String)supName);
//			   supplier.setAddress((String)addresss);
//			   supplier.setCompany((String)company);
//			   supplier.setPhoneNo((String)phno);
//			   supplier.setEmailid((String)email);
//			   supplier.setgstProperty((String)gst);
//			   supplier.setState((String)state);
//			   supplier.setCurrencyId((String)currencyId);
//			   supplier.setCountry((String)country);
//			   supplierList.add(supplier);
//			}
//			
//			tblSupplier.setItems(supplierList);
//			filltable();
//	}
//	}
//
//    @FXML
//    void showAll(ActionEvent event) {
//    	
//		supplierList.clear();
//		ArrayList sup = new ArrayList();
//
//		sup = RestCaller.SearchSupplierByName();
//		Iterator itr = sup.iterator();
//		while (itr.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr.next();
//			
//			Object supName = lm.get("supplierName");
//			Object company = lm.get("company");
//			Object addresss =lm.get("address");
//			Object phno = lm.get("phoneNo");
//			Object email = lm.get("emailid");
//			Object gst = lm.get("supGST");
//			Object id = lm.get("id");
//			Object state = lm.get("state");
//			Object creditPeriod = lm.get("cerditPeriod");
//			Object currencyId = lm.get("currencyId");
//			Object country = lm.get("country");
//			
//			if (null != supName)
//			{
//				Supplier supplier = new Supplier();
//			   supplier.setSupplierName((String)supName);
//			   supplier.setAddress((String)addresss);
//			   supplier.setCompany((String)company);
//			   supplier.setPhoneNo((String)phno);
//			   supplier.setEmailid((String)email);
//			   supplier.setgstProperty((String)gst);
//			   supplier.setId((String)id);
//			   supplier.setState((String)state);
//			   supplier.setCurrencyId((String)currencyId);
//			   supplier.setCerditPeriod((Integer)creditPeriod);
//			   supplier.setCountry((String)country);
//			   supplierList.add(supplier);
//			}
//			tblSupplier.setItems(supplierList);
//			filltable();
//    }
//}
//    @FXML
//    void tableToText(MouseEvent event) {
//    	btnEdit.setDisable(false);
//    	if (tblSupplier.getSelectionModel().getSelectedItem() != null) {
//			TableViewSelectionModel selectionModel = tblSupplier.getSelectionModel();
//			supplierList1 = selectionModel.getSelectedItems();
//			for (Supplier sup : supplierList1) {
//				txtSupplierName.setText(sup.getSupplierName());
//				txtAddress.setText(sup.getAddress());
//				txtEmailid.setText(sup.getEmailid());
//				txtGst.setText(sup.getSupGST());
//				txtPhoneNo.setText(sup.getPhoneNo());
//				txtCompanyName.setText(sup.getCompany());
//				txtCreditPeriod.setText(Integer.toString(sup.getCerditPeriod()));
//				btnSave.setDisable(true);
//				cmbSupplierState.getSelectionModel().select(sup.getState());
//				cmbCountry.getSelectionModel().select(sup.getCountry());
//				if(null != sup.getCurrencyId())
//				{
//				ResponseEntity<CurrencyMst> getCurrencyById = RestCaller.getcurrencyMsyById(sup.getCurrencyId());
//				if(null != getCurrencyById.getBody())
//				{
//					cmbCurrency.getSelectionModel().select(getCurrencyById.getBody().getCurrencyName());
//				}
//				}
//				ArrayList sup1 = new ArrayList();
//
//				sup1 = RestCaller.SearchSupplierByName(txtSupplierName.getText());
//				Iterator itr = sup1.iterator();
//				while (itr.hasNext()) {
//					LinkedHashMap lm = (LinkedHashMap) itr.next();
//					Object id = lm.get("id");
//					Object acc_id = lm.get("account_id");
//					supplier.setAccount_id((String)acc_id);
//					supplier.setId((String)id);
//			}
//			
//    }
//}
//}
//    @FXML
//    void supNameOnEnter(KeyEvent event) {
//    	if (event.getCode() == KeyCode.ENTER)
//    	{
//    		txtCompanyName.requestFocus();
//    	}
//    }
//    @FXML
//    void companyNameOnEnter(KeyEvent event) {
//    	if (event.getCode() == KeyCode.ENTER)
//    	{
//    		txtAddress.requestFocus();
//    	}
//
//    }
//    @FXML
//    void addressOnEnter(KeyEvent event) {
//       	if (event.getCode() == KeyCode.ENTER)
//    	{
//    		txtCreditPeriod.requestFocus();
//    	}
//    }
//
//    @FXML
//    void creditPeriodOnEnter(KeyEvent event) {
//    	if (event.getCode() == KeyCode.ENTER)
//    	{
//    		txtPhoneNo.requestFocus();
//    	}
//    }
//  
//    @FXML
//    void phnoOnEnter(KeyEvent event) {
//    	if (event.getCode() == KeyCode.ENTER)
//    	{
//    		txtEmailid.requestFocus();
//    	}
//    }
//    @FXML
//    void emailIdOnEnter(KeyEvent event) {
//    	if (event.getCode() == KeyCode.ENTER)
//    	{
//    		txtGst.requestFocus();
//    	}
//
//    }
//      @FXML
//    void supGstOnEnter(KeyEvent event) {
//    		if (event.getCode() == KeyCode.ENTER)
//        	{
//        		cmbSupplierState.requestFocus();
//        	}
//    }
//      @FXML
//      void stateOnEnter(KeyEvent event) {
//    	  if (event.getCode() == KeyCode.ENTER)
//      	{
//      		btnSave.requestFocus();
//      	}
//      }
//
//   
//    private void clearfileds()
//    {
//    	txtAddress.setText("");
//    	txtCompanyName.setText("");
//    	txtEmailid.setText("");
//    	txtGst.setText("");
//    	txtPhoneNo.setText("");
//    	txtSupplierName.setText("");
//    	cmbSupplierState.getSelectionModel().clearSelection();
//    	txtCreditPeriod.setText("");
//    	cmbCurrency.getSelectionModel().clearSelection();
//    	cmbCountry.getSelectionModel().clearSelection();
//    }
//    private void SaveDate()
//    {
//    	try
//    	{
//    	 if(txtSupplierName.getText().trim().isEmpty())
// 		{
// 			
// 			notifyMessage(5,"Please fill Supplier Name!!!");
// 			txtSupplierName.requestFocus();
// 			
// 		}
// 		 else if(txtCompanyName.getText().trim().isEmpty())
// 		{
//
// 		
// 			notifyMessage(5,"Please fill Company Name!!!");
// 			txtCompanyName.requestFocus();
// 			
// 		} else if(txtAddress.getText().trim().isEmpty())
// 		{
// 		
// 			notifyMessage(5,"Please fill Supplier Address!!!");
//
// 			txtCompanyName.requestFocus();
// 			
// 		}
// 		else if(cmbSupplierState.getSelectionModel().isEmpty())
// 		{
// 			notifyMessage(5,"Please Select State!!!");
//
// 			cmbSupplierState.requestFocus();
// 		}
//
//    	 
// 		else
// 			try
//    	 {
// 		{
// 			supplier = new Supplier();
// 			 	
// 			supplier.setBranchCode(SystemSetting.systemBranch);
// 		supplier.setAddress(txtAddress.getText());
// 		supplier.setPhoneNo(txtPhoneNo.getText());
// 		supplier.setSupGST(txtGst.getText());
// 		String supName = txtSupplierName.getText().trim();
//		
// 		supName = supName.replaceAll("'", "");
// 		supName = supName.replaceAll("&", "");
//		
// 		supName = supName.replaceAll("/", "");
// 		supName = supName.replaceAll("%", "");
// 		supName = supName.replaceAll("\"", "");
// 		supName = supName.replaceAll(" ", "-");
// 		supplier.setSupplierName(supName);
// 		 supplier.setCompany(txtCompanyName.getText());
// 		 supplier.setEmailid(txtEmailid.getText());
//  //supplier.setCerditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
//
// 		 String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"SU");
// 		 supplier.setId(vNo+SystemSetting.getUser().getCompanyMst().getCompanyName());
// 		 supplier.setCountry(cmbCountry.getSelectionModel().getSelectedItem());
// 	 ;	 	
// 		 if(null == cmbSupplierState.getValue())
// 		 {
// 			supplier.setState("KERALA");
// 		 } else {
// 		supplier.setState(cmbSupplierState.getValue());
// 		}
// 		
//    	ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadByName(txtSupplierName.getText());
//		if(null  != getAccountHeads.getBody())
//		{
//			notifyMessage(5,"Supplier Already Registered");
//			clearfileds();
//			return;
//		}
// 		if(txtCreditPeriod.getText().trim().isEmpty())
// 		{
// 			supplier.setCerditPeriod(0);
// 		}
// 		else
// 		{
// 			supplier.setCerditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
// 		}
// 		System.out.println("supplier credit amountttttt222333333333545555555555");
// 		if(null != cmbCurrency.getSelectionModel().getSelectedItem())
// 		{
// 			ResponseEntity<CurrencyMst> getcurrency = RestCaller.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem());
// 			if(null != getcurrency.getBody())
// 			{
// 				supplier.setCurrencyId(getcurrency.getBody().getId());
// 			}
// 		}
// 		
// 		//===========code for identify whether the supplier is saved on offline or online mode=======01-07-2021====
// 		String status = null;
//		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("OFFLINE_SUPPLIER_CREATION");
//		ParamValueConfig paramValueConfig = getParamValue.getBody();
//		if(chkOnline.isSelected() || null == paramValueConfig || paramValueConfig.getValue().equalsIgnoreCase("NO")) {
//			ResponseEntity<Supplier> respentity = RestCaller.saveSupplier(supplier);
//			supplier= respentity.getBody();
//		}else {
//			status = "ONLINE";
//			ResponseEntity<Supplier> respentity = RestCaller.saveSupplier(supplier,status);
//			supplier= respentity.getBody();
//		}
// 		
// 		
// 		
// 		//=================code end for saving supplier==============================
// 		
// 		
// 		
// 		supplierList.add(supplier);
// 		tblSupplier.setItems(supplierList);
// 		filltable();
//
// 		clearfileds();
// 		eventBus.post(supplier);
// 		notifyMessage(5,"Supplier Saved");
// 		txtSupplierName.requestFocus();
//		 
// 		 
// 		
// 	}
//    	 }
//    	 catch(Exception e)
//    	 {
//    		 return;
//    	 }
//    	}
//    	catch(Exception e)
//    	{
//    		return;
//    	}
//    }
//
//    public void notifyMessage(int duration, String msg) {
//		System.out.println("OK Event Receid");
//
//				Image img = new Image("done.png");
//				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
//						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
//							@Override
//							public void handle(ActionEvent event) {
//								System.out.println("clicked on notification");
//							}
//						});
//				notificationBuilder.darkStyle();
//				notificationBuilder.show();
//			}
//    
//    public String setCustomerVoucherProperty(String voucher) {
//    	this.CUSTOM_VOUCHER_PROPERTY = voucher;
//    	
//    	
//    	return this.CUSTOM_VOUCHER_PROPERTY ;
//    	
//    }
//    @Subscribe
//   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
//   		//Stage stage = (Stage) btnClear.getScene().getWindow();
//   		//if (stage.isShowing()) {
//   			taskid = taskWindowDataEvent.getId();
//   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
//   			
//   		 
//   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
//   			System.out.println("Business Process ID = " + hdrId);
//   			
//   			 PageReload(hdrId);
//   		}
//
//
//   	private void PageReload(String hdrId) {
//
//   	}
//
//}
