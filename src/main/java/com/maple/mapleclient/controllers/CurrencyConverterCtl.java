package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CurrencyConversionMst;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
public class CurrencyConverterCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<CurrencyConversionMst> currencyConversionMstList = FXCollections.observableArrayList();

	CurrencyConversionMst currencyConversionMst = null;


    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;

    @FXML
    private ComboBox<String> cmbCurrency;
    
    @FXML
    private ComboBox<String> cmbToCurrency;

    @FXML
    private TextField txtConversionRate;

    @FXML
    private TableView<CurrencyConversionMst> tblCurrency;

    @FXML
    private TableColumn<CurrencyConversionMst, String> clCurrency;

    @FXML
    private TableColumn<CurrencyConversionMst, String> clmnDestinationCurrency;

    
    @FXML
    private TableColumn<CurrencyConversionMst, Number> clConversionRate;
    
    @FXML
	private void initialize() {
    	
    	ResponseEntity<List<CurrencyMst>> currencyMstResp = RestCaller.getallCurrencyMst();
    	List<CurrencyMst> currenstMstList = currencyMstResp.getBody();
    	
    	currencyConversionMst = new CurrencyConversionMst();

    	
    	for(CurrencyMst currency : currenstMstList)
    	{
    		cmbCurrency.getItems().add(currency.getCurrencyName());
    	}
    	for(CurrencyMst currencyTo : currenstMstList)
    	{
    		cmbToCurrency.getItems().add(currencyTo.getCurrencyName());
    	}
    	
    	tblCurrency.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					
			    	currencyConversionMst = new CurrencyConversionMst();
			    	currencyConversionMst.setId(newSelection.getId());

				}
			}
		});

    }

    @FXML
    void DeleteReceiptMode(ActionEvent event) {
    	
    	if(null != currencyConversionMst)
    	{
    		if(null != currencyConversionMst.getId())
    		{
    			RestCaller.deleteCurrencyConversionMst(currencyConversionMst.getId());
    			ResponseEntity<List<CurrencyConversionMst>> currencyConversionMstListResp = RestCaller.getAllCurrencyConversionMst();
    	    	currencyConversionMstList = FXCollections.observableArrayList(currencyConversionMstListResp.getBody());
    	    	
    	    	FillTable();
    	    	currencyConversionMst = null;

    		}
    	}
    	

    }

    @FXML
    void ShowAll(ActionEvent event) {

    	ResponseEntity<List<CurrencyConversionMst>> currencyConversionMstListResp = RestCaller.getAllCurrencyConversionMst();
    	currencyConversionMstList = FXCollections.observableArrayList(currencyConversionMstListResp.getBody());
    	
    	FillTable();
    }

    @FXML
    void save(ActionEvent event) {

    	if(null == cmbCurrency.getValue())
    	{
    		notifyMessage(3, "Please select FromCurrency type", false);
    		cmbCurrency.requestFocus();
    		return;
    	}
    	
    	if(null == cmbToCurrency.getValue())
    	{
    		notifyMessage(3, "Please select ToCurrency type", false);
    		cmbToCurrency.requestFocus();
    		return;
    	}
    	
    	if(txtConversionRate.getText().trim().isEmpty())
    	{
    		notifyMessage(3, "Please enter currency rate", false);
    		txtConversionRate.requestFocus();
    		return;
    	}
    	
    	currencyConversionMst = new CurrencyConversionMst();
    	
    	currencyConversionMst.setBranchCode(SystemSetting.getUser().getBranchCode());
    	currencyConversionMst.setConversionRate(Double.parseDouble(txtConversionRate.getText()));
    	
    	ResponseEntity<CurrencyMst> currencyMstResp = RestCaller.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem().toString());
    	CurrencyMst currencyMst = currencyMstResp.getBody();
    	
    	if(null == currencyMst)
    	{
    		notifyMessage(3, "FromCurrency Type is not present", false);
    		cmbCurrency.requestFocus();
    		return;
    	}
    	
    	ResponseEntity<CurrencyMst> currencyMstRespTo = RestCaller.getCurrencyMstByName(cmbToCurrency.getSelectionModel().getSelectedItem().toString());
    	CurrencyMst currencyMstTo = currencyMstRespTo.getBody();
    	
    	if(null == currencyMstTo)
    	{
    		notifyMessage(3, "ToCurrency Type is not present", false);
    		cmbToCurrency.requestFocus();
    		return;
    	}
    	else if(cmbToCurrency.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem().toString())) {
    		notifyMessage(3, "FromCurrency and ToCurrency must be different", false);
    		cmbToCurrency.requestFocus();
    		return;
    	}
    	if(currencyMst.getCurrencyName().equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName()) || 
    			currencyMstTo.getCurrencyName().equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName())) {
    		currencyConversionMst.setFromCurrencyId(currencyMst.getId());
    		currencyConversionMst.setToCurrencyId(currencyMstTo.getId());
    		ResponseEntity<CurrencyConversionMst> currencyConversionMstResp = RestCaller.saveCurrencyConversionMst(currencyConversionMst);
    		currencyConversionMst = currencyConversionMstResp.getBody();
    	}
    	else {
    		notifyMessage(3, "FromCurrency or ToCurrency must be CompanyCurrency", false);
    		return;
    	}
//    	currencyConversionMst.setFromCurrencyId(currencyMst.getId());
//    	currencyConversionMst.setToCurrencyId(currencyMstTo.getId());
//    	ResponseEntity<CurrencyConversionMst> currencyConversionMstResp = RestCaller.saveCurrencyConversionMst(currencyConversionMst);
    	
//    	currencyConversionMst = currencyConversionMstResp.getBody();
    	if(null == currencyConversionMst)
    	{
    		notifyMessage(3, "Not saved, Something went wrong...!", false);
    		return;
    	} 
    	
		notifyMessage(3, "Successfully saved...!", false);

    	
    	ResponseEntity<List<CurrencyConversionMst>> currencyConversionMstListResp = RestCaller.getAllCurrencyConversionMst();
    	currencyConversionMstList = FXCollections.observableArrayList(currencyConversionMstListResp.getBody());
    	
    	FillTable();
    	
    	txtConversionRate.clear();
    	cmbCurrency.getSelectionModel().clearSelection();
    	cmbToCurrency.getSelectionModel().clearSelection();
    	currencyConversionMst = null;
    	
    	
    }
    
	private void FillTable() {

		tblCurrency.setItems(currencyConversionMstList);
		
		for(CurrencyConversionMst currency : currencyConversionMstList)
		{
			ResponseEntity<CurrencyMst> currencyMstResp = RestCaller.getcurrencyMsyById(currency.getFromCurrencyId());
			CurrencyMst currencyMst = currencyMstResp.getBody();
			
			if(null != currencyMst)
			{
				currency.setFromCurrencyName(currencyMst.getCurrencyName());
			}
		}
		
		for(CurrencyConversionMst currencyTo : currencyConversionMstList)
		{
			ResponseEntity<CurrencyMst> currencyMstRespTo = RestCaller.getcurrencyMsyById(currencyTo.getToCurrencyId());
			CurrencyMst currencyMstTo = currencyMstRespTo.getBody();
			
			if(null != currencyMstTo)
			{
				currencyTo.setToCurrencyName(currencyMstTo.getCurrencyName());
			}
		}
		
		clConversionRate.setCellValueFactory(cellData -> cellData.getValue().getCurrencyRateProperty());
		clCurrency.setCellValueFactory(cellData -> cellData.getValue().getFromCurrencyNameProperty());
		clmnDestinationCurrency.setCellValueFactory(cellData -> cellData.getValue().getToCurrencyNameProperty());


	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


  private void PageReload() {
  	
}
}
