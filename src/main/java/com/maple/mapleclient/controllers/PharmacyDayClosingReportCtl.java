package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AMDCDailySalesSummaryReport;
import com.maple.report.entity.PharmacyDayClosingReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyDayClosingReportCtl {
	
	//----------------------sharon created  this  report  --------jun 11-2021--------------- 
	private EventBus eventBus = EventBusFactory.getEventBus();
	PharmacyDayClosingReport pharmacyDayClosingReport=null;
	private ObservableList<PharmacyDayClosingReport> pharmacyDayClosingReportList = FXCollections.observableArrayList();
	@FXML
    private DatePicker dpdate;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnGenerate;

    @FXML
    private Button btnPrint;
    
    @FXML
    private TableView<PharmacyDayClosingReport> tblDayClosing;
    @FXML
    private TableColumn<PharmacyDayClosingReport, String> clBranch;

    @FXML
    private TableColumn<PharmacyDayClosingReport, Number> clTotalCard;

    @FXML
    private TableColumn<PharmacyDayClosingReport, Number> clTotalCredit;

    @FXML
    private TableColumn<PharmacyDayClosingReport, Number> clTotalCash;

    @FXML
    private TableColumn<PharmacyDayClosingReport, Number> clTotalBank;

    @FXML
    private TableColumn<PharmacyDayClosingReport, Number> clTotalInsurance;

    @FXML
    private TableColumn<PharmacyDayClosingReport, Number> clGrantTotal;

    @FXML
    private TableColumn<PharmacyDayClosingReport, Number> clphysicalCash;

    @FXML
    private TableColumn<PharmacyDayClosingReport, Number> cldiffenceAmount;

    @FXML
   	private void initialize() {
    	eventBus.register(this);
    	dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");
    	tblDayClosing.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					PharmacyDayClosingReport	pharmacyDayClosingReport = new PharmacyDayClosingReport();
					pharmacyDayClosingReport.setId(newSelection.getId());
					
				}
			}
    	});
    }
    @FXML
    void clear(ActionEvent event) {
    	clearField();
    }
    private void clearField() {
    	dpdate.setValue(null);
    	
	}
    @FXML
    void generateReport(ActionEvent event) {
    	if(null == dpdate.getValue())
    	{
    		notifyMessage(5, "Please select From date", false);
    		return;
    		
    	}  else {
					    	
    		Date uDate = SystemSetting.localToUtilDate(dpdate.getValue());
    		String fdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

    		
    		
    		ResponseEntity<List<PharmacyDayClosingReport>> pharmacyDayClosingReportResp = RestCaller
    				.getPharmacyDayClosingReport(fdate);

    		pharmacyDayClosingReportList = FXCollections.observableArrayList(pharmacyDayClosingReportResp.getBody());
       if (pharmacyDayClosingReportList.size() > 0) {
    			
    			
    			fillTable();
    			dpdate.getEditor().clear();;
    			
    		}
                  }
    }
    private void fillTable() {
    	tblDayClosing.setItems(pharmacyDayClosingReportList);
		clBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchProperty());
		clTotalCard.setCellValueFactory(cellData -> cellData.getValue().getTotalCardSalesProperty());
		clTotalCredit.setCellValueFactory(cellData -> cellData.getValue().getTotalCreditSalesProperty());
		clTotalCash.setCellValueFactory(cellData -> cellData.getValue().getTotalCashSalesProperty());
		clTotalBank.setCellValueFactory(cellData -> cellData.getValue().getTotalBankSalesProperty());
		clTotalInsurance.setCellValueFactory(cellData -> cellData.getValue().getTotalInsuranceSalesProperty());
		clGrantTotal.setCellValueFactory(cellData -> cellData.getValue().getGrandTotalProperty());
		cldiffenceAmount.setCellValueFactory(cellData -> cellData.getValue().getPhysicalCashProperty());
		clphysicalCash.setCellValueFactory(cellData -> cellData.getValue().getDiffenceamountProperty());
		
	}
    @FXML
    void printReport(ActionEvent event) {
    	Date fdate = SystemSetting.localToUtilDate(dpdate.getValue());
		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		


		try {
			JasperPdfReportService.PharmacyDayClosingReport(sfdate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
	
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
}