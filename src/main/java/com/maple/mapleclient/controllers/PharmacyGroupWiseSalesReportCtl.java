package com.maple.mapleclient.controllers;


import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.PharmacyGroupWiseSalesReport;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
public class PharmacyGroupWiseSalesReportCtl {
	String taskid;
	String processInstanceId;

	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PharmacyGroupWiseSalesReport> pharmacyGroupWiseSalesReport = FXCollections.observableArrayList();

    @FXML
    private Button btnShowReport;

    @FXML
    private Button btPrintReport;

    @FXML
    private Button btnAdd;

    @FXML
    private ListView<String> lstItem;

    @FXML
    private Button btnClear;

    
    @FXML
    private TextField txtTotalAmount;
    
    Double totalamount=0.0;
    
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    private TextField txtCategoryName;
    @FXML
    private TableView<PharmacyGroupWiseSalesReport> tblPharmacyGroupWiserSales;

    @FXML
    private TableColumn<PharmacyGroupWiseSalesReport, String> clItemName;

    @FXML
    private TableColumn<PharmacyGroupWiseSalesReport, String> clBatchCode;

    @FXML
    private TableColumn<PharmacyGroupWiseSalesReport, Number> clQty;

    @FXML
    private TableColumn<PharmacyGroupWiseSalesReport, Number> clTax;

    @FXML
    private TableColumn<PharmacyGroupWiseSalesReport, Number> clAmount;


    @FXML
    void AddItem(ActionEvent event) {
    	lstItem.getItems().add(txtCategoryName.getText());
    	lstItem.getSelectionModel().select(txtCategoryName.getText());
    	txtCategoryName.clear();
    
    }

    @FXML
    void clear(ActionEvent event) {
    	clear();
    }

    @FXML
    void onEnterGrop(ActionEvent event) {
  	  loadCategoryPopup();
    }

    @FXML
    void printReport(ActionEvent event) {
    	

    	if(null==dpFromDate.getValue()) {
    		notifyMessage(5, "select from date",false);
    		return;
    	}
    	if(null==dpToDate.getValue()) {
    		notifyMessage(5, "select to date",false);
    		return;
    	}
    	if(null==cmbBranch.getValue()) {
    		notifyMessage(5, "select Branch",false);
    		return;
    	}
    	if(null==lstItem.getSelectionModel().getSelectedItem()) {
    		notifyMessage(5, "plz add Grouph",false);
    		return;
    	}
    	
    	java.util.Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
    	
			java.util.Date utodate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String todate = SystemSetting.UtilDateToString(utodate, "yyyy-MM-dd");
			
			  List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
		        
		    	String cat="";
		    	for(String s:selectedItems)
		    	{
		    		s = s.concat(";");
		    		cat= cat.concat(s);
		    	} 
		
	    	try {
				
	    		JasperPdfReportService.PharmacyGroupWiseSalesReport(fromdate,todate,cmbBranch.getSelectionModel().getSelectedItem(),cat);

			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
    	


    }


	 private void loadCategoryPopup() {
			
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
    @FXML
    void showReport(ActionEvent event) {

    	totalamount=0.0;

    	if(null==dpFromDate.getValue()) {
    		notifyMessage(5, "select from date",false);
    		return;
    	}
    	if(null==dpToDate.getValue()) {
    		notifyMessage(5, "select to date",false);
    		return;
    	}
    	if(null==cmbBranch.getValue()) {
    		notifyMessage(5, "select Branch",false);
    		return;
    	}
    	if(null==lstItem.getSelectionModel().getSelectedItem()) {
    		notifyMessage(5, "plz add Grouph",false);
    		return;
    	}
    	
        
    	
  List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	    java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
    ResponseEntity<List<PharmacyGroupWiseSalesReport>> resp=RestCaller.getPharmacyGroupWiseSales(startDate,endDate,cmbBranch.getSelectionModel().getSelectedItem(),cat);
    pharmacyGroupWiseSalesReport = FXCollections.observableArrayList(resp.getBody());
    tblPharmacyGroupWiserSales.setItems(pharmacyGroupWiseSalesReport);
	filltable();
    	//clear();
    	
    	
       	for(PharmacyGroupWiseSalesReport groupWiseSaleReportDtl:pharmacyGroupWiseSalesReport)
    	{
    		if(null != groupWiseSaleReportDtl.getAmount())
    		totalamount = totalamount + groupWiseSaleReportDtl.getAmount();
    	}
    	BigDecimal settoamount = new BigDecimal(totalamount);
		settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalAmount.setText(settoamount.toString());

    }
    
   	private void setBranches() {
   		
   		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
   		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
   		branchMstList = branchMstRep.getBody();
   		
   		for(BranchMst branchMst : branchMstList)
   		{
   			cmbBranch.getItems().add(branchMst.getBranchCode());
   		
   			
   		}
   		 
   	}
	
    @FXML
   	private void initialize() {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	setBranches();
    	eventBus.register(this);
    	 lstItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	  public void clear() {
		  
		  
		  dpFromDate.setValue(null);
		  dpToDate.setValue(null);
		  cmbBranch.setValue(null);
		  lstItem.getItems().clear();
		  txtTotalAmount.clear();
		  tblPharmacyGroupWiserSales.getItems().clear();
		
		  
		  
	  }
	  
	  @Subscribe
			public void popupOrglistner(CategoryEvent CategoryEvent) {

				Stage stage = (Stage) btPrintReport.getScene().getWindow();
				if (stage.isShowing()) {

					
					txtCategoryName.setText(CategoryEvent.getCategoryName());
			
				

				}
	  }

private void filltable()
{
	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	clBatchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
	clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
	clTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());
	clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	
} 
@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


private void PageReload() {
	
}

}
