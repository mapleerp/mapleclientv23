package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.LmsQueueTallyMst;
import com.maple.mapleclient.entity.ReorderMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class TallyIntegrationCtl {
	String taskid;
	String processInstanceId;
	
	
	private ObservableList<LmsQueueTallyMst> tallyList = FXCollections.observableArrayList();


    @FXML
    private ComboBox<String> cmbVoucherType;

    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnFetchData;

    @FXML
    private TableView<LmsQueueTallyMst> tblTally;

    @FXML
    private TableColumn<LmsQueueTallyMst, String> clBranch;

    @FXML
    private TableColumn<LmsQueueTallyMst, String> clVoucherType;

    @FXML
    private TableColumn<LmsQueueTallyMst, String> clVoucherDate;

    @FXML
    private TableColumn<LmsQueueTallyMst, String> clVoucherNo;

    @FXML
    private Button btnPostToTally;
    
    @FXML
	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    	cmbVoucherType.getItems().add("forwardSales");
    	cmbVoucherType.getItems().add("forwardPurchase");

    	tblTally.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    @FXML
    void FetchData(ActionEvent event) {
    	
    	if(null == dpDate.getValue())
    	{
    		dpDate.requestFocus();
    		notifyMessage(2, "Please select date", false);
    	}
    	if(null == cmbVoucherType.getSelectionModel().getSelectedItem())
    	{
    		cmbVoucherType.requestFocus();
    		notifyMessage(2, "Please select voucher type", false);
    	}
    	
    	Date date =SystemSetting.localToUtilDate(dpDate.getValue());
    	String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
    	
    	tblTally.getItems().clear();
    	ResponseEntity<List<LmsQueueTallyMst>> tallyListResp = RestCaller.TallyByVoucherTypeAndDate(sdate,cmbVoucherType.getSelectionModel().getSelectedItem().toString());
    	tallyList = FXCollections.observableArrayList(tallyListResp.getBody());
    	fillTable();
    }

    private void fillTable() {
		tblTally.setItems(tallyList);
		
		clBranch.setCellValueFactory(cellData -> cellData.getValue().getBrabchProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
		clVoucherType.setCellValueFactory(cellData -> cellData.getValue().getVoucherTypeProperty());

		
	}

	@FXML
    void PostToTally(ActionEvent event) {
		
		List<String> voucherNos = new ArrayList<String>();
		
    	int selesctedItemsNo = tblTally.getSelectionModel().getSelectedItems().size();

		for(int i=0;i<selesctedItemsNo;i++)
		{

			String voucherNo = tblTally.getSelectionModel().getSelectedItems().get(i).getVoucherNumber();
			voucherNos.add(voucherNo);
		}
		
		for(String str : voucherNos)
		{

	    	Date date =SystemSetting.localToUtilDate(dpDate.getValue());
	    	String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
	    	
	    	if(cmbVoucherType.getSelectionModel().getSelectedItem().equalsIgnoreCase("forwardSales"))
	    	{
	    		String status = RestCaller.SalesToTallyIntegration(str,sdate);
	    	}
	    	
	    	if(cmbVoucherType.getSelectionModel().getSelectedItem().equalsIgnoreCase("forwardPurchase"))
	    	{
	    		String status = RestCaller.PurchaseToTallyIntegration(str,sdate);
	    	}
		}
		
		tblTally.getItems().clear();
		dpDate.setValue(null);
		cmbVoucherType.getSelectionModel().clearSelection();
		
    }
    
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
