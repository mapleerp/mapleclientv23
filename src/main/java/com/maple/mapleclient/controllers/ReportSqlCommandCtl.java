package com.maple.mapleclient.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ExecuteSql;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ReportSqlDtl;
import com.maple.mapleclient.entity.ReportSqlHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.HostServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ReportSqlCommandCtl {

	// ------------------sharon----[JUN 1 ]-----------------------

	private ObservableList<ReportSqlHdr> reportSqlHdrList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();
	@FXML
	private ComboBox<String> cmbReportType;

	@FXML
	private DatePicker dpFromDate;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private Button btnGenarateExcelReport;

	@FXML
	private ComboBox<String> cmbBranch;

	@FXML
	private TextField txtItemName;

	@FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		eventBus.register(this);
		ResponseEntity<List<BranchMst>> branchMstResp = RestCaller.getBranchMst();
		List<BranchMst> branchList = branchMstResp.getBody();

		for (BranchMst branch : branchList) {
			cmbBranch.getItems().add(branch.getBranchName());
		}

		// ------------------sharon---------------------------

		if (null != SystemSetting.getCloudUser()) {

			ResponseEntity<List<ReportSqlHdr>> savedReportNameByuser = RestCaller
					.getAllReportSqlHdrByUserId(SystemSetting.getCloudUser().getId());
			reportSqlHdrList = FXCollections.observableArrayList(savedReportNameByuser.getBody());
			if (reportSqlHdrList.size() > 0) {
				for (ReportSqlHdr reportSqlHdr : reportSqlHdrList) {
					cmbReportType.getItems().add(reportSqlHdr.getReportName());
				}
			} else {
				ResponseEntity<List<ReportSqlHdr>> savedReportName = RestCaller.getAllReportSqlHdr();
				reportSqlHdrList = FXCollections.observableArrayList(savedReportName.getBody());
				for (ReportSqlHdr reportSqlHdr : reportSqlHdrList) {
					cmbReportType.getItems().add(reportSqlHdr.getReportName());
				}
			}

		} else {

			ResponseEntity<List<ReportSqlHdr>> savedReportName = RestCaller.getAllReportSqlHdr();
			reportSqlHdrList = FXCollections.observableArrayList(savedReportName.getBody());
			for (ReportSqlHdr reportSqlHdr : reportSqlHdrList) {
				cmbReportType.getItems().add(reportSqlHdr.getReportName());
			}

		}

	}

	@FXML
	void GenarateExcelReport(ActionEvent event) {

		if (null == cmbReportType.getValue()) {
			notifyMessage(5, "Please select  Report Type", false);
			cmbReportType.requestFocus();
			return;
		}

		ResponseEntity<List<ReportSqlHdr>> reportSqlHdrResp = RestCaller
				.getReportSqlHdrByReportName(cmbReportType.getSelectionModel().getSelectedItem().toString());

		List<ReportSqlHdr> reportSqlHdrList = reportSqlHdrResp.getBody();
		if (reportSqlHdrList.size() == 0) {
			return;
		}

		ReportSqlHdr reportSqlHdr = reportSqlHdrList.get(0);
		if (null != reportSqlHdr) {
			String sqlString = reportSqlHdr.getSqlString();
			ResponseEntity<List<ReportSqlDtl>> reportSqlDtlListResp = RestCaller
					.getReportSqlDtlByHdrId(reportSqlHdr.getId());

			List<ReportSqlDtl> reportSqlDtlList = reportSqlDtlListResp.getBody();

			for (ReportSqlDtl reportDtl : reportSqlDtlList) {
				if (null != reportDtl.getParamSequence()) {
					if (null != reportDtl.getParamName()) {
						if (reportDtl.getParamName().equalsIgnoreCase("COMPANYID")) {
							String comapnyid = SystemSetting.myCompany;
							sqlString = sqlString.replaceAll("#" + reportDtl.getParamSequence(), "'" + comapnyid + "'");
						}
						if (reportDtl.getParamName().equalsIgnoreCase("BRANCHCODE")) {
							String branchcode = setParameterBranchCodetoSqlString();
							if (branchcode.equalsIgnoreCase("Failed")) {
								return;
							}
							sqlString = sqlString.replaceAll("#" + reportDtl.getParamSequence(),
									"'" + branchcode + "'");
						}

						if (reportDtl.getParamName().equalsIgnoreCase("FROMDATE")) {
							String fromDate = getFromDateToSqlString();
							if (fromDate.equalsIgnoreCase("Failed")) {
								return;
							}
							sqlString = sqlString.replaceAll("#" + reportDtl.getParamSequence(), "'" + fromDate + "'");
						}
						if (reportDtl.getParamName().equalsIgnoreCase("TODATE")) {
							String toDate = getToDateToSqlString();
							if (toDate.equalsIgnoreCase("Failed")) {
								return;
							}
							sqlString = sqlString.replaceAll("#" + reportDtl.getParamSequence(), "'" + toDate + "'");
						}

						if (reportDtl.getParamName().equalsIgnoreCase("ITEMID")) {
							String itemId = getItemIdToSqlString();
							if (itemId.equalsIgnoreCase("Failed")) {
								return;
							}
							sqlString = sqlString.replaceAll("#" + reportDtl.getParamSequence(), "'" + itemId + "'");
						}
					}
				}
			}

			System.out.println(sqlString);

			ExecuteSql executeSql = new ExecuteSql();

			executeSql.setSqlToExecute(sqlString);
			List<Map<String, Object>> sqlResult = RestCaller.executeSql(executeSql);

			int rownum = 0;
			int cellnum = 0;

			if (sqlResult.size() > 0) {
				ExportToExcel(sqlResult);
			}
		}

	}

	private String getItemIdToSqlString() {

		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(2, "Please select item", false);
			return "Failed";
		}

		ResponseEntity<ItemMst> itemMstResp = RestCaller
				.getItemByNameRequestParamForCloud(txtItemName.getText().toString());
		ItemMst itemMst = itemMstResp.getBody();

		if (null == itemMst) {
			return "Failed";
		}

		return itemMst.getId();

	}

	@FXML
	void ItemPopup(MouseEvent event) {

		showPopup();

	}

	private void ExportToExcel(List<Map<String, Object>> sqlResult) {

		Set<String> keySet = sqlResult.get(0).keySet();

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Ssql Result");

		int rownum = 0;
		int cellnum = -1;

		Iterator itr = keySet.iterator();
		while (itr.hasNext()) {
			rownum = 0;
			cellnum++;
			String element = (String) itr.next();

			Row row = sheet.getRow(rownum);
			if (null == row) {
				row = sheet.createRow(rownum);
			}
			Cell cell = row.createCell(cellnum);
			cell.setCellValue(element);

			for (int i = 0; i < sqlResult.size(); i++) {
				Object valueObject = sqlResult.get(i).get(element);
				System.out.println(valueObject);
				if (null != valueObject) {
					String values = valueObject.toString();
					rownum++;
					Row row2 = sheet.getRow(rownum);

					if (null == row2) {
						row2 = sheet.createRow(rownum);
					}

					Cell cell2 = row2.createCell(cellnum);

					cell2.setCellValue(valueObject.toString());

				} else {
					rownum++;
				}

			}

		}

		try {
			Random rand = new Random();
			int rndFile = rand.nextInt(10000);

			String FileName = rndFile + ".xls";
			FileOutputStream out = new FileOutputStream(new File(FileName));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(FileName);

			// Desktop desktop = Desktop.getDesktop();
			// File file = new File(FileName);
			// desktop.open(file);

		} catch (FileNotFoundException e1) {
			System.out.println(e1.toString());
		} catch (IOException e2) {
			System.out.println(e2.toString());
		}
		try {
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String getToDateToSqlString() {

		if (null == dpToDate.getValue()) {
			notifyMessage(2, "Please select from date", false);
			return "Failed";
		}

		java.util.Date udate = SystemSetting.localToUtilDate(dpToDate.getValue());

		String dpToDate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		return dpToDate;
	}

	private String getFromDateToSqlString() {

		if (null == dpFromDate.getValue()) {
			notifyMessage(2, "Please select from date", false);
			return "Failed";
		}

		java.util.Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());

		String fromDate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		return fromDate;

	}

	private String setParameterBranchCodetoSqlString() {

		if (null == cmbBranch.getSelectionModel().getSelectedItem()) {
			notifyMessage(2, "Please select branch ", false);
			return "Failed";
		}

		ResponseEntity<BranchMst> branchMstResp = RestCaller
				.getBranchMstByName(cmbBranch.getSelectionModel().getSelectedItem().toString());
		BranchMst branchMst = branchMstResp.getBody();

		if (null == branchMst) {
			notifyMessage(2, "Invalid branch", false);
			return "Failed";
		}

		return branchMst.getBranchCode();

	}

	private void showPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			cmbBranch.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) cmbReportType.getScene().getWindow();
		if (stage.isShowing()) {

			txtItemName.setText(itemPopupEvent.getItemName());
		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
}
