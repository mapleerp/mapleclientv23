package com.maple.mapleclient.controllers;
import java.util.HashMap;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;

import javafx.scene.control.ComboBox;



	
public class PublishMessageCtl {
	
	String taskid;
	String processInstanceId;
 
	  @FXML
	    private CheckBox itemmst;

	    @FXML
	    private CheckBox unitmst;

	    @FXML
	    private CheckBox category;

	    @FXML
	    private CheckBox branch;

	    @FXML
	    private CheckBox users;

	    @FXML
	    private CheckBox permissions;

	    @FXML
	    private CheckBox group;

	    @FXML
	    private CheckBox process;

	    @FXML
	    private CheckBox TaxCreation;

	    @FXML
	    private CheckBox KitDefinition;

	    @FXML
	    private CheckBox PriceDefinition;

	    @FXML
	    private CheckBox KotManager;

	    @FXML
	    private CheckBox UserRegistration;

	    @FXML
	    private CheckBox ProcessCreation;

	    @FXML
	    private CheckBox ChangePassword;

	    @FXML
	    private CheckBox BranchCreation;

	    @FXML
	    private CheckBox CompanyCreation;

	    @FXML
	    private CheckBox VoucherReprint;

	    @FXML
	    private CheckBox Scheme;

	    @FXML
	    private CheckBox SchemeDetails;

	    @FXML
	    private CheckBox ItemOrCategory;

	    @FXML
	    private CheckBox ProcessPermission;

	    @FXML
	    private CheckBox GroupPermission;

	    @FXML
	    private CheckBox GroupCreation;

	    @FXML
	    private CheckBox Reorder;

	    @FXML
	    private CheckBox MultiUnit;

	    @FXML
	    private CheckBox ItemMaster;

	    @FXML
	    private CheckBox DayEnd;

	    @FXML
	    private CheckBox AccountHeads;

	    @FXML
	    private CheckBox AddSupplier;

	    @FXML
	    private CheckBox Voucherreprint;

	    @FXML
	    private Button ok;

	    @FXML
	    private ComboBox<String> combobox;

	    @FXML
	    private ComboBox<String> combobox1;

	    @FXML
		private void initialize() {
	    	
		
		  combobox.getItems().add("AllBranch"); 
		  combobox1.getItems().add("Branch");
		  combobox1.getItems().add("Server"); 
		  HashMap<String,String> hm= new HashMap<String,String> (); 
		  hm.put("ItemMst","NO");
		  hm.put("UnitMst","NO");
		  hm.put("Category","NO");
		  hm.put("Branches","NO");
		  hm.put("Permissions","NO");
		  hm.put("Users","NO"); 
		  hm.put("Groups","NO");
		  hm.put("Process","NO");
		  hm.put("Reorder","NO");
		  hm.put("DayEnd","NO"); 
		  hm.put("TaxCreation","NO");
		  hm.put("KitDefinition","NO"); 
		  hm.put("PriceDefinition","NO");
		  hm.put("KotManager","NO");
		  hm.put("UserRegistration","NO");
		  hm.put("ProcessCreation","NO");
		  hm.put("ChangePassword","NO");
		  hm.put("BranchCreation","NO");
		  hm.put("MultiUnit","NO");
		  hm.put("AccountHeads","NO");
		  hm.put("CompanyCreation","NO");
		  hm.put("VoucherReprint","NO");
		  hm.put("Scheme","NO");
		  hm.put("SchemeDetails","NO");
		  hm.put("ItemOrCategory","NO");
		  hm.put("ProcessPermission","NO");
		  hm.put("GroupPermission","NO");
		  hm.put("GroupCreation","NO"); 
		  hm.put("ItemMaster","NO");
		  hm.put("AddSupplier","NO"); 
		  hm.put("VoucherReprint","NO");
		 
	    }

	    @FXML
	    void addOk(ActionEvent event) 
	    {
	    	
	    	HashMap<String,String> hm= new HashMap<String,String> ();
	    	hm.put("ItemMst","yes");
	    	hm.put("UnitMst","yes");
	    	hm.put("Category","yes");
	    	hm.put("Branches","yes");
	    	hm.put("Permissions","yes");
	    	hm.put("Users","yes");
	    	hm.put("Groups","yes");
	    	hm.put("Process","yes");
	    	hm.put("Reorder","yes");
	    	hm.put("DayEnd","yes");
	    	hm.put("TaxCreation","yes");
	    	hm.put("KitDefinition","yes");
	    	hm.put("PriceDefinition","yes");
	    	hm.put("KotManager","yes");
	    	hm.put("UserRegistration","yes");
	    	hm.put("ProcessCreation","yes");
	    	hm.put("ChangePassword","yes");
	    	hm.put("BranchCreation","yes");
	    	hm.put("MultiUnit","yes");
	    	hm.put("AccountHeads","yes");
	    	hm.put("CompanyCreation","yes");
	    	hm.put("VoucherReprint","yes");
	    	hm.put("Scheme","yes");
	    	hm.put("SchemeDetails","yes");
	    	hm.put("ItemOrCategory","yes");
	    	hm.put("ProcessPermission","yes");
	    	hm.put("GroupPermission","yes");
	    	hm.put("GroupCreation","yes");
	    	hm.put("ItemMaster","yes");
	    	hm.put("AddSupplier","yes");
	    	hm.put("VoucherReprint","yes");
	    	
	    

	    }
	    @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }

	}

