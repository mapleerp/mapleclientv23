package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ActualProductionDtl;
import com.maple.mapleclient.entity.JobCardDtl;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ActiveJobCardReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ActiveJobCardReportsCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<ActiveJobCardReport> jobCardList = FXCollections.observableArrayList();


    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private TableView<ActiveJobCardReport> tblJobCard;

    @FXML
    private TableColumn<ActiveJobCardReport, String> clCustomer;

    @FXML
    private TableColumn<ActiveJobCardReport, String> clItemName;

    @FXML
    private TableColumn<ActiveJobCardReport, String> clServiceInNo;

    @FXML
    private TableColumn<ActiveJobCardReport, LocalDate> clServiceInDate;

    @FXML
    private TableColumn<ActiveJobCardReport, String> clComplaint;

    @FXML
    private TableColumn<ActiveJobCardReport, String> clObservation;
    
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		tblJobCard.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getJobCardHdrId()) {
					jobCardDtlReportByHdrId(newSelection.getJobCardHdrId());
				}
			}
		});

	}



    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select form date", false);
    		return;
    	}
    	
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, "Please select to date", false);
    		return;
    	}
    	
    	Date fDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    	String fromDate = SystemSetting.UtilDateToString(fDate, "yyyy-MM-dd");
    	
    	Date tDate = SystemSetting.localToUtilDate(dpToDate.getValue());
    	String toDate = SystemSetting.UtilDateToString(tDate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<ActiveJobCardReport>> jobCardResp = RestCaller.getActiveJobCardResport(fromDate,toDate);
    	jobCardList = FXCollections.observableArrayList(jobCardResp.getBody());
    	
    	tblJobCard.setItems(jobCardList);
    	
		clComplaint.setCellValueFactory(cellData -> cellData.getValue().getComplaintsProperty());
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clObservation.setCellValueFactory(cellData -> cellData.getValue().getObservationProperty());
		clServiceInDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clServiceInNo.setCellValueFactory(cellData -> cellData.getValue().getServiceInNoProperty());

		
    }
    
    
    private void jobCardDtlReportByHdrId(String jobCardHdrId) {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/JobCardDtlReport.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			JobCardDtlReportCtl jobCardDtl =fxmlLoader.getController();
			jobCardDtl.JobCardDtlByHdrId(jobCardHdrId);
   			Stage stage = new Stage();
   			stage.setScene(new Scene(root1));
   			stage.initModality(Modality.APPLICATION_MODAL);
   			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


      private void PageReload() {
      	
      	
  		
  	}
}
