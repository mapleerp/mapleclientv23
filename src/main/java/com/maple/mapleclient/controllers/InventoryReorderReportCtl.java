package com.maple.mapleclient.controllers;
import java.io.IOException;
import java.sql.Date;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
 
public class InventoryReorderReportCtl {
	String taskid;
	String processInstanceId;

	   private EventBus eventBus = EventBusFactory.getEventBus();
	    @FXML
	    private DatePicker dpFromDate;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private Button btnShowReport;

	    @FXML
	    void ShowReport(ActionEvent event) {
	    	
	    	if(null == dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    	} else if (null == dpToDate.getValue()) {
	    		notifyMessage(5, "Please select to date", false);
	    		return;
			} else {
				
		
	    	
	    	java.util.Date fromDate =Date.valueOf(dpFromDate.getValue());
	    	java.util.Date toDate =Date.valueOf(dpToDate.getValue());
	    	
	   // 	ResponseEntity<ItemMst> itemsaved = RestCaller.getItemByName(txtItemName.getText());
			
			String vFDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
			String vToDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
			try {
				JasperPdfReportService.InventoryReorderstatusReport(vFDate,vToDate,SystemSetting.systemBranch);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}

	    }

	    @FXML
	    void loadPopUp(MouseEvent event) {

	    	showItem();
	    }
	    private void showItem()
	    {
	    	try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				// fxmlLoader.setController(itemStockPopupCtl);
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }

		@FXML
		private void initialize() {
			dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
			dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
			System.out.println("ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo");
			eventBus.register(this);
			
			
		}
		
		 
	    @Subscribe
		public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

			System.out.println("-------------popupItemlistner-------------");

		
			Stage stage = (Stage) btnShowReport.getScene().getWindow();
			if (stage.isShowing()) {
		
	//			txtItemName.setText(itemPopupEvent.getItemName());
				
			}

		}
	    
		public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
		  @Subscribe
		   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		   		//Stage stage = (Stage) btnClear.getScene().getWindow();
		   		//if (stage.isShowing()) {
		   			taskid = taskWindowDataEvent.getId();
		   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
		   			
		   		 
		   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
		   			System.out.println("Business Process ID = " + hdrId);
		   			
		   			 PageReload();
		   		}


		     private void PageReload() {
		     	
		   }


	}


