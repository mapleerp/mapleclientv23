package com.maple.mapleclient.controllers;

import java.util.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class SupplierDueReportCtl {
	
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpEndDate;

    @FXML
    private DatePicker dpStartDate;

    @FXML
    private Button btnGenerate;
    @FXML
   	private void initialize() {
   		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
   		dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
       }
    @FXML
    void GenerateReports(ActionEvent event) {
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(2, "Please select start date", false);
    		dpStartDate.requestFocus();
    		return;
    	}
    	
    	if(null == dpEndDate.getValue())
    	{
    		notifyMessage(2, "Please select end date", false);
    		dpEndDate.requestFocus();
    		return;
    	}
    	
    	Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
    	
    	
    	Date edate = SystemSetting.localToUtilDate(dpEndDate.getValue());
    	String endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
    	
    	try {
			JasperPdfReportService.SupplierDueReport(startDate,endDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


    }
    

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
