package com.maple.mapleclient.controllers;

import java.util.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ProductionDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.FinishedProduct;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class ProductionPlaningAndActualReportCtl {
	
	String taskid;
	String processInstanceId;
    
	private ObservableList<FinishedProduct> productionReportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpEndDate;

    @FXML
    private DatePicker dpStartDate;

    @FXML
    private Button btnReport;

    @FXML
    private TableView<FinishedProduct> tblReport;

    @FXML
    private TableColumn<FinishedProduct, String> clDate;

    @FXML
    private TableColumn<FinishedProduct, String> clProductName;

    @FXML
    private TableColumn<FinishedProduct, Number> clPlaningQty;

    @FXML
    private TableColumn<FinishedProduct, Number> clActualQty;

    @FXML
    private TableColumn<FinishedProduct, Number> clCostPrice;
    
    @FXML
    private TableColumn<FinishedProduct, Number> clValue;

    @FXML
    private TableColumn<FinishedProduct, String> clBatch;
    
    @FXML
    private Button btnPrintReport;
    
    @FXML
   	private void initialize() {
   		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
   		dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
       }
    @FXML
    void PrintReport(ActionEvent event) {
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(3, "Please Select Start Date", false);
    		dpStartDate.requestFocus();
    		return;
    	}
    	
    	if(null == dpEndDate.getValue())
    	{
    		notifyMessage(3, "Please Select End Date", false);
    		dpEndDate.requestFocus();
    		return;
    	}
    	
    	Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
    	
    	Date edate = SystemSetting.localToUtilDate(dpEndDate.getValue());
    	String endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
    	
    	try {
			JasperPdfReportService.PlaningAndActualProductionReport(startDate,endDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void ShowReport(ActionEvent event) {
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(3, "Please Select Start Date", false);
    		dpStartDate.requestFocus();
    		return;
    	}
    	
    	if(null == dpEndDate.getValue())
    	{
    		notifyMessage(3, "Please Select End Date", false);
    		dpEndDate.requestFocus();
    		return;
    	}
    	
    	Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
    	
    	Date edate = SystemSetting.localToUtilDate(dpEndDate.getValue());
    	String endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
    	
    	
    	ResponseEntity<List<FinishedProduct>> actualProductionResp = RestCaller.findPlaningAndActualProductionReport(startDate,endDate);
    	productionReportList = FXCollections.observableArrayList(actualProductionResp.getBody());
    	
    	tblReport.setItems(productionReportList);
    	
		clActualQty.setCellValueFactory(cellData -> cellData.getValue().getActualQtyProperty());
		clCostPrice.setCellValueFactory(cellData -> cellData.getValue().getCostPriceProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getDateProperty());
		clPlaningQty.setCellValueFactory(cellData -> cellData.getValue().getPlaningQtyProperty());
		clProductName.setCellValueFactory(cellData -> cellData.getValue().getProductProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());

    }
    
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }

}
