package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.RawMaterialIssueDtl;
import com.maple.mapleclient.entity.RawMaterialIssueHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class RawMaterialIssueCtl {
	
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	RawMaterialIssueDtl rawMaterialIssueDtl = null;
	RawMaterialIssueHdr rawMaterialIssueMst = null;
	private ObservableList<RawMaterialIssueDtl> rawMaterilaListTable = FXCollections.observableArrayList();
    @FXML
    private DatePicker dpDate;

    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtBatch;

    @FXML
    private TextField txtQty;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFinalSave;

    @FXML
    private TextField txtUnit;

    @FXML
    private TableView<RawMaterialIssueDtl> tblItems;

    @FXML
    private TableColumn<RawMaterialIssueDtl, String> clItemName;

    @FXML
    private TableColumn<RawMaterialIssueDtl, String> clBatch;

    @FXML
    private TableColumn<RawMaterialIssueDtl, Number> clQty;

    @FXML
    private TableColumn<RawMaterialIssueDtl, String> clUnit;
    @FXML
	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		eventBus.register(this);
		tblItems.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					
					rawMaterialIssueDtl = new RawMaterialIssueDtl();
					rawMaterialIssueDtl.setId(newSelection.getId());
				}

				}
				});
		

    }
    @FXML
    void itemOnClick(MouseEvent event) {
    	
    	 loadItemPopup();
    }
    @FXML
    void addItems(ActionEvent event) {
    	addItems();
    }
    @FXML
    void addOnEnter(KeyEvent event) {
    
    }

    @FXML
    void qtyOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		addItems();
    	}
    }


    @FXML
    void itemOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		loadItemPopup();
    	}
    }
    private void addItems()
    {
    	if(txtItemName.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Type ItemName");
    		txtItemName.requestFocus();
    		return;
    	}
    	else if(txtQty.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Type Qty");
    		txtQty.requestFocus();
    		return;
    	}

    	else if(null == rawMaterialIssueMst)
    	{
    		rawMaterialIssueMst = new RawMaterialIssueHdr();
    		rawMaterialIssueMst.setBranchCode(SystemSetting.systemBranch);
    	
    		rawMaterialIssueMst.setVoucherDate(Date.valueOf(dpDate.getValue()));
    		ResponseEntity<RawMaterialIssueHdr> respentity = RestCaller.saveRawMaterialIssueMst(rawMaterialIssueMst);
    		rawMaterialIssueMst = respentity.getBody();
    	}
    	
    	rawMaterialIssueDtl = new RawMaterialIssueDtl();
    	rawMaterialIssueDtl.setBatch(txtBatch.getText());
    	rawMaterialIssueDtl.setItemName(txtItemName.getText());
		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(rawMaterialIssueDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
    	rawMaterialIssueDtl.setItemId(item.getId());
    	rawMaterialIssueDtl.setQty(Double.parseDouble(txtQty.getText()));
    	ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());
    	rawMaterialIssueDtl.setUnitId(unitMst.getBody().getId());
    	rawMaterialIssueDtl.setRawMaterialIssueHdr(rawMaterialIssueMst);
    	ResponseEntity<RawMaterialIssueDtl> respentity = RestCaller.saveRawMaterialIssueDtl(rawMaterialIssueDtl);
    	rawMaterialIssueDtl = respentity.getBody();
    	
    	rawMaterilaListTable.add(rawMaterialIssueDtl);
    	fillTable();
    	txtBatch.clear();
    	txtItemName.clear();
    	txtQty.clear();
    	txtUnit.clear();
    	txtItemName.requestFocus();
    	
    }
    @FXML
    void deleteItems(ActionEvent event) {
    	if(null != rawMaterialIssueDtl)
    	{
    		if(null != rawMaterialIssueDtl.getId())
    		{
    			RestCaller.deleteRawmaterialIssueDtl(rawMaterialIssueDtl.getId());
    			ResponseEntity<List<RawMaterialIssueDtl>> rawMaterialIssueSaved = RestCaller.getRawMaterialIssueDtl(rawMaterialIssueMst.getId());
    			rawMaterilaListTable = FXCollections.observableArrayList(rawMaterialIssueSaved.getBody());
    			fillTable();
    			notifyMessage(5, "Deleted!!!");
    		}
    	}
    	

    }
    @FXML
    void saveOnKey(KeyEvent event) {
    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
    		finalSave();
    		}
    }

    @FXML
    void finalSave(ActionEvent event) {
    	finalSave();
    	
    }
    private void finalSave()
    {
    	ResponseEntity<List<RawMaterialIssueDtl>> rawMaterialIssueSaved = RestCaller.getRawMaterialIssueDtl(rawMaterialIssueMst.getId());
    	if(null ==rawMaterialIssueSaved.getBody())
    	{
    		return;
    	}
    	
    		String vNo = RestCaller.getVoucherNumber("RAWMATISSUE");
    		rawMaterialIssueMst.setVoucherNumber(vNo);
    		RestCaller.updateRawMaterialIssueHdr(rawMaterialIssueMst);
    		notifyMessage(5, "Saved!!!");
    		String rdate =SystemSetting.UtilDateToString(rawMaterialIssueMst.getVoucherDate(), "yyyy-MM-dd") ;
    		
    		
    		try {
    			
				JasperPdfReportService.RawMaterialIssue(rawMaterialIssueMst.getVoucherNumber(),rdate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
    
    		rawMaterialIssueDtl = null;
    		rawMaterialIssueMst = null;
    		tblItems.getItems().clear();
    		rawMaterilaListTable.clear();
    }
    @Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtItemName.setText(itemPopupEvent.getItemName());
		//	itemmst.setId(itemPopupEvent.getItemId());
			txtBatch.setText(itemPopupEvent.getBatch());
			txtUnit.setText(itemPopupEvent.getUnitName());
				}
			});
		}
		txtQty.requestFocus(); // ===to automaticaly focus on quantity after popup dtls entered//
	}
    private void loadItemPopup() {
		/*
		 * Function to display popup window and show list of items to select.
		 */
		System.out.println("ssssssss=====loadItemPopup=====sssssssssss");

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    private void fillTable()
    {
    	for(RawMaterialIssueDtl rawmatDtl :rawMaterilaListTable)
    	{
    		ResponseEntity<ItemMst> items = RestCaller.getitemMst(rawmatDtl.getItemId());
    		rawmatDtl.setItemName(items.getBody().getItemName());
    		ResponseEntity<UnitMst> unit = RestCaller.getunitMst(rawmatDtl.getUnitId());
    		rawmatDtl.setUnitName(unit.getBody().getUnitName());
    	}
    	tblItems.setItems(rawMaterilaListTable);
    	clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
    	clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }

}
