package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AcceptStockCtl {
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<StockTransferInDtl> stockTransferDtlTable = FXCollections.observableArrayList();
	private ObservableList<StockTransferInDtl> stockTransferDtlTable1 = FXCollections.observableArrayList();

	StockTransferInHdr stockTransferInHdr = null;
	StockTransferInDtl stockTransferInDtl = null;
	
	@FXML
	private TextField txtVoucherIn;
	
    @FXML
    private Button btnRefresh;
    
	@FXML
	private TextField txtintent;
	
	@FXML
	private TextField txtFromBranch;
	
	@FXML
	private DatePicker dpIntentDate;
	
	@FXML
	private TextField txtRecId;

	@FXML
	private TextField txtVoucherNumber;

	@FXML
	private TextField txtIntentDate;

	@FXML
	private ComboBox<String> cmbPending;

	@FXML
	private TableView<StockTransferInDtl> tblStockTrasfer;

	@FXML
	private TableColumn<StockTransferInDtl, String> clStktransferItemName;

	@FXML
	private TableColumn<StockTransferInDtl, String> clStktransferBarcode;

	@FXML
	private TableColumn<StockTransferInDtl, String> clStktransferBatch;

	@FXML
	private TableColumn<StockTransferInDtl, Number> clStkTransferRate;

	@FXML
	private TableColumn<StockTransferInDtl, Number> clStkTransferQty;

	@FXML
	private TableColumn<StockTransferInDtl, Number> clStkMRP;

	@FXML
	private TableColumn<StockTransferInDtl, LocalDate> clStkTransferExpiryDate;

	@FXML
	private TableColumn<StockTransferInDtl, Number> clStkTransferAmount;
	@FXML
	private Button refreshTable;

	@FXML
	private Button acceptStockID;

	@FXML
	private Button reset;

	@FXML
	void VoucherInKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtFromBranch.requestFocus();
		}

	}

	@FXML
	void FromBranchKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtintent.requestFocus();
		}

	}

	@FXML
	void ItentKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			dpIntentDate.requestFocus();
		}

	}

	@FXML
	private void initialize() {
		
		eventBus.register(this);
		cmbPending.getItems().add("COMPLETED");
		dpIntentDate = SystemSetting.datePickerFormat(dpIntentDate, "dd/MMM/yyyy");
		cmbPending.setValue("COMPLETED");
		stockTransferInHdr = new StockTransferInHdr();
		dpIntentDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		dpIntentDate.setEditable(false);

	}

	@FXML
	void acceptStock(ActionEvent event) {
		
		if(txtVoucherIn.getText().trim().isEmpty())
		{
			txtVoucherIn.requestFocus();
			notifyMessage(5,"Select Voucher Number");
			return;
		}
		
		if (null != stockTransferInHdr) {
			
			 
			
			
			String financialYear = SystemSetting.getFinancialYear();
			
			String sNo = RestCaller.getVoucherNumber(financialYear + "STIN");
			stockTransferInHdr.setVoucherNumber(sNo);
			stockTransferInHdr.setAcceptedDate(Date.valueOf(dpIntentDate.getValue()));
			stockTransferInHdr.setStatusAcceptStock("COMPLETED");
			stockTransferInHdr.setBranchCode(SystemSetting.systemBranch);
			stockTransferInHdr.setVoucherNumber(sNo);
			/*
			 * Update accept Stock
			 */
			//{companymstid}/stocktransferinhdr/acceptstock
			 RestCaller.saveAcceptStock(stockTransferInHdr);
			notifyMessage(5, "Detials Saved Successfully!!!");
			
			if(null != taskid && taskid.length() > 0)
			{
				RestCaller.completeTask(taskid);
			}
			txtFromBranch.clear();
			txtintent.clear();
			txtVoucherIn.clear();
		
			stockTransferDtlTable.clear();
			stockTransferDtlTable1.clear();
			stockTransferInHdr = null;
			stockTransferInDtl = null;
			tblStockTrasfer.getItems().clear();
			
			
		//	txtVoucherNumber.clear();
		}

	}

	@FXML
	void refreshTable(ActionEvent event) {
		
		RefreshTableData();
		
	}

	private void RefreshTableData() {



		stockTransferDtlTable1.clear();
		tblStockTrasfer.getItems().clear();
		if (null != stockTransferInHdr.getId()) {
			ResponseEntity<List<StockTransferInDtl>> stkdtlSaved = RestCaller
					.getStockTransferInDtl(stockTransferInHdr.getId());
			stockTransferDtlTable = FXCollections.observableArrayList(stkdtlSaved.getBody());
			for (StockTransferInDtl stk : stockTransferDtlTable) {
				try
				{
				stockTransferInDtl = new StockTransferInDtl();
				stockTransferInDtl.setAmount(stk.getAmount());
				stockTransferInDtl.setBarcode(stk.getBarcode());
				stockTransferInDtl.setBatch(stk.getBatch());
				stockTransferInDtl.setExpiryDate(stk.getExpiryDate());
				stockTransferInDtl.setItemId(stk.getItemId());
//				ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(stk.getItemId());
				stockTransferInDtl.setItemName(stk.getItemId().getItemName());
				stockTransferInDtl.setMrp(stk.getMrp());
				stockTransferInDtl.setQty(stk.getQty());
				stockTransferInDtl.setRate(stk.getRate());
				stockTransferInDtl.setTaxRate(stk.getTaxRate());
				ResponseEntity<UnitMst> unitsaved = RestCaller.getunitMst(stk.getUnitId());
				stockTransferInDtl.setUnitId(unitsaved.getBody().getUnitName());
				stockTransferDtlTable1.add(stockTransferInDtl);
				}
				catch (Exception e) {
					
					e.printStackTrace();
				}
			}
			FillTable();

		}

	
	}

	@FXML
	void loadVoucherPopup(MouseEvent event) {

		loadPopup();
	}

	@FXML
	void reset(ActionEvent event) {
		txtFromBranch.clear();
		txtintent.clear();
		txtVoucherIn.clear();
//		dpIntentDate.setValue(null);
		tblStockTrasfer.getItems().clear();

	}

	private void FillTable() {
		tblStockTrasfer.setItems(stockTransferDtlTable1);
		clStkTransferAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clStktransferBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clStkTransferExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clStktransferItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clStkTransferQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clStkTransferRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clStktransferBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
		clStkMRP.setCellValueFactory(cellData -> cellData.getValue().getmrpProperty());

	}

	private void loadPopup() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/VoucherNoPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(VoucherNoEvent voucherNoEvent) {

		System.out.println("------Voucher-------popuplistner-------------" + voucherNoEvent);
		Stage stage = (Stage) refreshTable.getScene().getWindow();
		if (stage.isShowing()) {
			stockTransferInHdr = new StockTransferInHdr();
			stockTransferInHdr.setIntetNumber(voucherNoEvent.getIntentNumber());
//			txtAccount.setText(accountEvent.getAccountName());
			// String accountId = accountEvent.getAccountId();
			txtVoucherIn.setText(voucherNoEvent.getVoucherNumber());
			txtintent.setText(voucherNoEvent.getIntentNumber());
			txtFromBranch.setText(voucherNoEvent.getFromBranchCode());
			stockTransferInHdr.setId(voucherNoEvent.getId());
			stockTransferInHdr.setVoucherDate(voucherNoEvent.getVoucherDate());

		}

	}

    @FXML
    void RefreshVoucher(ActionEvent event) {
    	String msg = RestCaller.getallMastrs("StockTransfer");
    }
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


    private void PageReload(String hdrId) {
    	
    	ResponseEntity<StockTransferInHdr> stockTransferInHdrResp = RestCaller.getStockTransferInHdrByHdrId(hdrId);
    	 stockTransferInHdr = stockTransferInHdrResp.getBody();
    	
    	if(null != stockTransferInHdr)
    	{
        	
//    		stockTransferInHdr.setIntetNumber(stockTransferInHdr.getIntentNumber());
//    		stockTransferInHdr.setId(stockTransferInHdr.getId());
//    		stockTransferInHdr.setVoucherDate(stockTransferInHdr.getVoucherDate());
    		
    		txtVoucherIn.setText(stockTransferInHdr.getInVoucherNumber());
    		txtintent.setText(stockTransferInHdr.getIntentNumber());
    		txtFromBranch.setText(stockTransferInHdr.getFromBranch());
    		
//    		stockTransferInHdr = new StockTransferInHdr();
//			stockTransferInHdr.setIntetNumber(stockTransferInHdr.getIntentNumber());
//			stockTransferInHdr.setId(stockTransferInHdr.getId());
//			stockTransferInHdr.setVoucherDate(stockTransferInHdr.getVoucherDate());
  	
    		
    		RefreshTableData();
    	}
    	
    	

	}

}