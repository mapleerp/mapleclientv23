package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.ConsumptionDtl;
import com.maple.mapleclient.entity.ConsumptionReasonMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class ConsumptionReasonCtl {
	String taskid;
	String processInstanceId;
	ConsumptionReasonMst consumptionReasonMst = null;
	private ObservableList<ConsumptionReasonMst> consumptionReasonList = FXCollections.observableArrayList();

    @FXML
    private Button btnShowAll;

    @FXML
    private TextField txtReason;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private TableView<ConsumptionReasonMst> tbReason;

    @FXML
    private TableColumn<ConsumptionReasonMst,String> clReason;

    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == consumptionReasonMst)
    	{
    		return;
    	}
    	if(null == consumptionReasonMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteConsumptionReason(consumptionReasonMst.getId());
    	txtReason.clear();
    	ResponseEntity<List<ConsumptionReasonMst>> getAll = RestCaller.getAllConsumptionReason();
    	consumptionReasonList = FXCollections.observableArrayList(getAll.getBody());
    	tbReason.setItems(consumptionReasonList);
    	clReason.setCellValueFactory(cellData -> cellData.getValue().getreasonProperty());
    		
    }
    @FXML
    void actionShowAll(ActionEvent event) {
    	ResponseEntity<List<ConsumptionReasonMst>> getAll = RestCaller.getAllConsumptionReason();
    	consumptionReasonList = FXCollections.observableArrayList(getAll.getBody());
    	tbReason.setItems(consumptionReasonList);
    	clReason.setCellValueFactory(cellData -> cellData.getValue().getreasonProperty());
    }
    @FXML
    void initialize()
    {
    	tbReason.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					consumptionReasonMst = new ConsumptionReasonMst();
					consumptionReasonMst.setId(newSelection.getId());
					txtReason.setText(consumptionReasonMst.getReason());
					
				}
			}
				
			});
    }
    @FXML
    void actionSave(ActionEvent event) {
    	ResponseEntity<ConsumptionReasonMst > getReason = RestCaller.getConsumptionReasonMstByReason(txtReason.getText());

    	if(null == getReason.getBody())
    	{
    	consumptionReasonMst = new ConsumptionReasonMst();
    	consumptionReasonMst.setReason(txtReason.getText());
    	ResponseEntity<ConsumptionReasonMst> respentity = RestCaller.saveConsumptionReasonMst(consumptionReasonMst);
    	consumptionReasonMst = respentity.getBody();
    	txtReason.clear();
    	consumptionReasonList.add(consumptionReasonMst);
    	tbReason.setItems(consumptionReasonList);
    	clReason.setCellValueFactory(cellData -> cellData.getValue().getreasonProperty());
    	consumptionReasonMst=null;

    }
    	else
    	{
    		return;
    	}
    }
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


     private void PageReload() {
     	
}

}
