//package com.maple.mapleclient.controllers;
//
//import java.sql.Date;
//
//
//import org.controlsfx.control.Notifications;
//
//import com.maple.jasper.JasperPdfReportService;
//import com.maple.maple.util.SystemSetting;
//
//
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.fxml.FXML;
//import javafx.geometry.Pos;
//import javafx.scene.control.Button;
//import javafx.scene.control.DatePicker;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//
//import javafx.util.Duration;
//import net.sf.jasperreports.engine.JRException;
//
//public class BinCardReportCtl {
//	
//	    @FXML
//	    private DatePicker dpToDate;
//
//	    @FXML
//	    private DatePicker dpFromDate;
//
//	    @FXML
//	    private Button btnReport;
//
//	    @FXML
//	    void generateReport(ActionEvent event) {
//	    	
//	    	if (null == dpFromDate.getValue()) {
//
//				notifyMessage(3, "please select From date", false);
//				dpFromDate.requestFocus();
//				return;
//			}
//			if (null == dpToDate.getValue()) {
//
//				notifyMessage(3, "please select Todate", false);
//				dpToDate.requestFocus();
//				return;
//			}
//
//			java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
//			String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
//			java.util.Date u1Date = Date.valueOf(dpToDate.getValue());
//			String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");
//
//			try {
//				JasperPdfReportService.BinCardReport(fromdate, todate);
//			} catch (JRException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//
//	    }
//	    
//	    public void notifyMessage(int duration, String msg, boolean success) {
//
//			Image img;
//			if (success) {
//				img = new Image("done.png");
//
//			} else {
//				img = new Image("failed.png");
//			}
//
//			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
//					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
//					.onAction(new EventHandler<ActionEvent>() {
//						@Override
//						public void handle(ActionEvent event) {
//							System.out.println("clicked on notification");
//						}
//					});
//			notificationBuilder.darkStyle();
//			notificationBuilder.show();
//
//		}
//
//}
