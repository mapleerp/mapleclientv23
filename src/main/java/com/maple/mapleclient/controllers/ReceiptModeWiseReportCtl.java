package com.maple.mapleclient.controllers;

import java.sql.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ReceiptModeWiseDtlExport;
import com.maple.maple.util.ReceiptSummaryExport;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SettledAmountDtlReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import net.sf.jasperreports.engine.JRException;

public class ReceiptModeWiseReportCtl {
	
	String taskid;
	String processInstanceId;
	

    private ObservableList<SettledAmountDtlReport> SettledAmountDtlReportList = FXCollections.observableArrayList();
    ReceiptSummaryExport receiptSummaryExport =new ReceiptSummaryExport();
    
    ReceiptModeWiseDtlExport receiptModeWiseDtlExport =new ReceiptModeWiseDtlExport();
	
    @FXML
    private ComboBox<String> branchselect;
	
    
    @FXML
    private Button btnReceiptSummary; 
    
    @FXML
   private DatePicker dpDate;
   

    @FXML
    private Button btnReceiptSummaryExport;
    
    @FXML
    private Button btnShow;
    @FXML
    private Button btnPrint;

    @FXML
    private TextField txtGrandTotal;
    

    

    
    @FXML
    private Button btnPrintReport;
    

    @FXML
    private Button btnReceiptModewiseDtl;

   
    @FXML
    private TableView<ReceiptModeWiseReport> tblReceiptSummary;

    @FXML
    private TableColumn<ReceiptModeWiseReport, String> clmnReceiptMode;


    @FXML
    private TableColumn<ReceiptModeWiseReport, Number >clmnAmount;
    
    
    
    
    @FXML
    private TableView<ReceiptModeWiseReport> tbReport;

    @FXML
    private TableColumn<ReceiptModeWiseReport, String> tblvoucherNumber;

    @FXML
    private TableColumn<ReceiptModeWiseReport, String> tblvoucherDate;

    @FXML
    private TableColumn<ReceiptModeWiseReport, String> tblreceiptMode;

    @FXML
    private TableColumn<ReceiptModeWiseReport, Number> tblreceiptAmount;

    
    
  


    
    private ObservableList<ReceiptModeWiseReport> receiptModeReportList = FXCollections.observableArrayList();
    
    
    private ObservableList<ReceiptModeWiseReport> receiptModeReportSummaryList = FXCollections.observableArrayList();
    
    
    
    double totalamount = 0.0;
    @FXML
 	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
     	setBranches();
     	
    	FillTable();
     	}

    

@FXML
void show(ActionEvent event) {
	
	receiptModeReportList.clear();
	txtGrandTotal.clear();

	SettledAmountDtlReportList.clear();
	 totalamount = 0.0;
	java.util.Date uDate = Date.valueOf(dpDate.getValue());
	String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

	//uDate = SystemSetting.StringToUtilDate(strDate,);

	ResponseEntity<List<ReceiptModeWiseReport>> receiptmodeList = RestCaller.getReceiptModeWiseReport(branchselect.getSelectionModel().getSelectedItem(), strDate);
	receiptModeReportList = FXCollections.observableArrayList(receiptmodeList.getBody());
	
	
	tbReport.setItems(receiptModeReportList);
	tblvoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
	
	tblvoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
	tblreceiptAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
	
	tblreceiptMode.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

	
	for(ReceiptModeWiseReport reportDtl:receiptModeReportList)
	{
		if(null != reportDtl.getReceiptAmount());
		totalamount = totalamount + reportDtl.getReceiptAmount();
	}
	BigDecimal settoamount = new BigDecimal(totalamount);
	settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
	txtGrandTotal.setText(settoamount.toString());
	
//		ResponseEntity<Double> settlementAmount=
//			    RestCaller.getSettlementAmount(branchselect.getSelectionModel().getSelectedItem(), strDate);

///	Double SaleOrderSettledAmount=settlementAmount.getBody();
//	if(null!=SaleOrderSettledAmount) {
//	txtPreviousAdvance.setText(SaleOrderSettledAmount.toString());
	
///	Double netSettleAmount=totalamount-SaleOrderSettledAmount;
//	txtNetSettlementAmount.setText(netSettleAmount.toString());
	
	
	
	ResponseEntity<List<ReceiptModeWiseReport>> receiptmodeSummaryList = RestCaller.getReceiptModeWiseSummaryReport(branchselect.getSelectionModel().getSelectedItem(), strDate);
	receiptModeReportSummaryList = FXCollections.observableArrayList(receiptmodeSummaryList.getBody());
	
	System.out.print(receiptModeReportSummaryList.size()+"receipt mode report size isssssssssssssssssssssssssssssssssssssssssssss");
	tblReceiptSummary.setItems(receiptModeReportSummaryList);
	clmnReceiptMode.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());
	
	clmnAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
	
	
	//call Url

//	}
		
		 /*
			 * else {
			 * 
			 * ResponseEntity<Double> settlementCashAmount=
			 * RestCaller.getSettlementCashAmount(branchselect.getSelectionModel().
			 * getSelectedItem(), strDate);
			 * 
			 * Double SaleOrderSettledAmount=settlementCashAmount.getBody();
			 * if(null!=SaleOrderSettledAmount) {
			 * txtPreviousAdvance.setText(SaleOrderSettledAmount.toString());
			 * 
			 * Double netSettleAmount=totalamount-SaleOrderSettledAmount;
			 * txtNetSettlementAmount.setText(netSettleAmount.toString());
			 * 
			 * 
			 * //call Url ResponseEntity<List<SettledAmountDtlReport>> settledAmtResp =
			 * RestCaller.getSettledAmtCash(SystemSetting.systemBranch, strDate);
			 * SettledAmountDtlReportList =
			 * FXCollections.observableArrayList(settledAmtResp.getBody());
			 * tbSettlement.setItems(SettledAmountDtlReportList);
			 * clsetledVNo.setCellValueFactory(cellData ->
			 * cellData.getValue().getvoucherNumberProperty());
			 * clSettledAmnt.setCellValueFactory(cellData ->
			 * cellData.getValue().getamountProperty());
			 * clSettledVDate.setCellValueFactory(cellData ->
			 * cellData.getValue().getvoucherDateProperty()); }
			 * 
			 * 
			 * }
			 */
}
    
private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			branchselect.getItems().add(branchMst.getBranchCode());
		
			
		}
}

private void FillTable()
{
	
	tblvoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
	tblvoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
	tblreceiptAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
	tblreceiptMode.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());
	
}


@FXML
void receiptSummaryExport(ActionEvent event) {

	
	 java.util.Date uDate = Date.valueOf(dpDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		
		ArrayList excelExport = new ArrayList();
		if( branchselect.getSelectionModel().isEmpty())
		{
			return;
		}
		//ResponseEntity<List<SalesProfitReportHdr>> salesProfitReport = RestCaller.getSalesProfit(branchselect.getSelectionModel().getSelectedItem(), strDate,strDate1);
		excelExport = RestCaller.exportReceiptReport(branchselect.getSelectionModel().getSelectedItem(),strDate);	
		receiptSummaryExport.exportToExcel("ReceiptSummary"+branchselect.getSelectionModel().getSelectedItem()+strDate+".xls", excelExport);

}

@FXML
void onClickOk(ActionEvent event) {

}
@FXML
void reportPrint(ActionEvent event) {


	 java.util.Date uDate = Date.valueOf(dpDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		
		ArrayList excelExport = new ArrayList();
		if( branchselect.getSelectionModel().isEmpty())
		{
			return;
		}
	
		excelExport = RestCaller.exportReceiptReportDtl(branchselect.getSelectionModel().getSelectedItem(),strDate);	
		receiptModeWiseDtlExport.exportToExcel("ReceiptDtl"+branchselect.getSelectionModel().getSelectedItem()+strDate+".xls", excelExport);

}

	
@FXML
void receiptModeWiseDtl(ActionEvent event) {


	 java.util.Date uDate = Date.valueOf(dpDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		
		ArrayList excelExport = new ArrayList();
		if( branchselect.getSelectionModel().isEmpty())
		{
			return;
		}
	  try {
			JasperPdfReportService.receiptModeWiseDtlReport( branchselect.getSelectionModel().getSelectedItem(),strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}

	
@FXML
void receiptModeWiseSummaryPrint(ActionEvent event) {
	

	 java.util.Date uDate = Date.valueOf(dpDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		
		ArrayList excelExport = new ArrayList();
		if( branchselect.getSelectionModel().isEmpty())
		{
			return;
		}
	  try {
			JasperPdfReportService.receiptModeWiseSummaryPrint( branchselect.getSelectionModel().getSelectedItem(),strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

}
@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


private void PageReload() {
	
}

	
}
