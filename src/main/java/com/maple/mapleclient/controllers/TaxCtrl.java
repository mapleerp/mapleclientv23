package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class  TaxCtrl {
	
	String taskid;
	String processInstanceId;

    @FXML
    private TextField TxtTaxName;

    @FXML
    private TextField TxtTaxRate;

    @FXML
    private TextField TxtTaxOutAcId;

    @FXML
    private TextField txtInputAccountId;

    @FXML
    private TextField TxtTaxappliedon;

    @FXML
    private TextField txtIGST;

    @FXML
    private TextField txtSGST;

    @FXML
    private TextField txtCGST;

    @FXML
    private Button btnTaxSave;

    @FXML
    private Button btnTaxEdit;

    @FXML
    private TableView<?> tblTax;

    @FXML
    private TextField txtCessName;

    @FXML
    private TextField txtCessRate;

    @FXML
    private Button btnCesSave;

    @FXML
    private Button btnCessEdit;

    @FXML
    private TableView<?> tblCess;
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}


}
