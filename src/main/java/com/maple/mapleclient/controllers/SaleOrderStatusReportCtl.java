package com.maple.mapleclient.controllers;

import java.sql.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import org.controlsfx.control.Notifications;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class SaleOrderStatusReportCtl {
	
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpSaleDate;

    @FXML
    private ComboBox<String> cmbStatus;

    @FXML
    private Button btnGenerateReport;
    
    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpSaleDate.getValue())
    	{
    		notifyMessage(5,"Please select date",false);
    	} else if (null == cmbStatus.getValue()) {
    		notifyMessage(5,"Please select status",false);
		} else {
			
			Date date = Date.valueOf(dpSaleDate.getValue());
			
			Format formatter;
			
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			String strDate = formatter.format(date);
			
			if(cmbStatus.getValue().equals("PENDING"))
			{
				try {
					JasperPdfReportService.SaleOrderPendingDetails(strDate);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				
				
			}else if (cmbStatus.getValue().equals("REALIZED")) {
				
				try {
					JasperPdfReportService.SaleOrderRealizedDetails(strDate);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

    }
    @FXML
	private void initialize() {
    	dpSaleDate = SystemSetting.datePickerFormat(dpSaleDate, "dd/MMM/yyyy");
    	cmbStatus.getItems().add("PENDING");
    	cmbStatus.getItems().add("REALIZED");
    }
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }
    

}
