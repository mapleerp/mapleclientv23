
package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemPopUp;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class custPopupCtl {
	String taskid;
	String processInstanceId;

	
	CustomerEvent customerEvent;

	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<AccountHeads> custList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();

	@FXML
	private TextField txtname;

	@FXML
	private Button btnSubmit;

	@FXML
	private TableView<AccountHeads> tablePopupView;

	@FXML
	private TableColumn<AccountHeads, String> custName;

	@FXML
	private TableColumn<AccountHeads, String> custAddress;

	@FXML
	private Button btnCancel;

	@FXML
	private void initialize() {
		LoadCustomerBySearch("");

		txtname.textProperty().bindBidirectional(SearchString);

		customerEvent = new CustomerEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		tablePopupView.setItems(custList);

		btnCancel.setCache(true);

		custName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		custAddress.setCellValueFactory(cellData -> cellData.getValue().getPartyAddressProperty());

		tablePopupView.setItems(custList);
		for (AccountHeads s : custList) {
			// System.out.println("@@ SupplierName @@"+s.getSupplierName());
		}

		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getAccountName() && newSelection.getAccountName().length() > 0) {
					customerEvent.setCustomerAddress(newSelection.getPartyAddress1());
					customerEvent.setCustomerContact(newSelection.getCustomerContact());
					customerEvent.setCustomerGst(newSelection.getPartyGst());
					customerEvent.setCustomerMail(newSelection.getPartyMail());
					customerEvent.setCustomerName(newSelection.getAccountName());
					customerEvent.setCustId(newSelection.getId());
					customerEvent.setCreditPeriod(newSelection.getCreditPeriod());
//					eventBus.post(customerEvent);
				}
			}
		});

		/*
		 * Below code will do the searching based on typed string
		 */

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadCustomerBySearch((String) newValue);
			}
		});

	}

	@FXML
	void onCancel(ActionEvent event) {
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
	}

	@FXML
	void submit(ActionEvent event) {
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		eventBus.post(customerEvent);
		stage.close();

	}

	@FXML
	void OnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			eventBus.post(customerEvent);
			stage.close();
		}  else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		} else {
			txtname.requestFocus();
		}
	}

	@FXML
	void OnKeyPressTxt(KeyEvent event) {
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
	}

	private void LoadCustomerBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		custList.clear();
		ArrayList customer = new ArrayList();
		AccountHeads cust = new AccountHeads();
		RestTemplate restTemplate = new RestTemplate();

		customer = RestCaller.SearchCustomerByName(searchData);
//		customer = RestCaller.SearchCustomerByName();
		Iterator itr = customer.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			// Iterator itr2 = lm.keySet().iterator();
			// while(itr2.hasNext()) {

			Object customerName = lm.get("accountName");
			Object customerAddress = lm.get("partyAddress");
			Object customerContact = lm.get("customerContact");
			Object customerMail = lm.get("customerMail");
			Object customerGst = lm.get("customerGst");
			Object creditPeriod = lm.get("creditPeriod");
			Object id = lm.get("id");
			if (null != id) {
				cust = new AccountHeads();
				cust.setPartyAddress1((String) customerAddress);
				cust.setCustomerContact((String) customerContact);
				cust.setAccountName((String) customerName);
				cust.setPartyGst((String) customerGst);
				cust.setPartyMail((String) customerMail);
				cust.setCreditPeriod((Integer) creditPeriod);

				cust.setId((String) id);
				
				//-------ret branch
				

				System.out.println(lm);

				custList.add(cust);
			}

		}
		
		

		return;

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
