package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BrandMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ProductMst;
import com.maple.mapleclient.entity.ServiceItemMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.BrandEvent;
import com.maple.mapleclient.events.ProductEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ServiceItemCreationCtl {
	String taskid;
	String processInstanceId;

	ProductMst productMst = null;
	BrandMst brandMst = null;
	ItemMst serviceItemMst = null;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<CategoryMst> categoryList = FXCollections.observableArrayList();
	private ObservableList<ItemMst> ItemTableList2 = FXCollections.observableArrayList();
	private ObservableList<CategoryMst> categoryList2 = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	
	StringProperty productProperty = new SimpleStringProperty("");
	StringProperty brandProperty = new SimpleStringProperty("");
	StringProperty modelProperty = new SimpleStringProperty("");
	
	


	@FXML
	private TextField txtitemName;

	@FXML
	private TextField txtbarCode;

	@FXML
	private Button btnbarCode;

	@FXML
	private TextField txtTaxRate;

	@FXML
	private Button btnitemCode;

	@FXML
	private TextField txtitemCode;

	@FXML
	private TextField txtHsnCode;

	@FXML
	private TextField txtSellingPrice;

	@FXML
	private TextField TxtStateCess;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnclr;

	@FXML
	private Button btnshowAll;

	@FXML
	private Button btnSubmit;

	@FXML
	private ComboBox<String> choiceUnit;

	@FXML
	private ComboBox<String> choiceCategory;

	@FXML
	private Button btnRefresh;

	@FXML
	private TextField txtBrand;

	@FXML
	private TextField TxtModel;

	@FXML
	private TextField txtProduct;

	@FXML
	private TableView<ItemMst> tblItemList;

	@FXML
	private TableColumn<ItemMst, String> clItemName;

	@FXML
	private TableColumn<ItemMst, String> clItemCategory;

	@FXML
	private TableColumn<ItemMst, String> clItemUnit;

	@FXML
	private TableColumn<ItemMst, Number> clItemSellingPrice;

	@FXML
	private TableColumn<ItemMst, Number> clItemTax;

	@FXML
	private TableColumn<ItemMst, String> clProduct;

	@FXML
	private TableColumn<ItemMst, String> clBrand;

	@FXML
	private TableColumn<ItemMst, String> clModel;

	@FXML
	private TableColumn<ItemMst, Number> clCess;

	@FXML
	private TableColumn<ItemMst, String> clItemCode;

	@FXML
	private TableColumn<ItemMst, String> clBarcode;

	@FXML
	private TableColumn<ItemMst, String> clStatus;

	@FXML
	private ComboBox<String> cmbInventoryParentId;

	@FXML
	private Button btnInventoryAdd;

	@FXML
	private Button btnInventoryDelete;

	@FXML
	private TextField txtIventoryCategory;

	@FXML
	private Button btnInventoryShowAll;

	@FXML
	private TableView<CategoryMst> tblCategoryList;

	@FXML
	private TableColumn<CategoryMst, String> clCategoryName;

	@FXML
	private TableColumn<CategoryMst, String> clParentCategory;
	

    @FXML
    void BrandPopup(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) 
    	{
    		loadBrandPopUp();
    	}

    }
    
    @FXML
    void ProductPopup(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) 
    	{
    		loadProdcutPopUp();
    	}


    }

    @FXML
    void FocusOnModel(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) 
    	{
    		txtbarCode.requestFocus();
    	}

    }
	@FXML
	private void initialize() {
		
		serviceItemMst = new ItemMst();
				
		txtProduct.textProperty().bindBidirectional(productProperty);
		txtBrand.textProperty().bindBidirectional(brandProperty);
		TxtModel.textProperty().bindBidirectional(modelProperty);

		eventBus.register(this);
		choiceUnit.getSelectionModel().select("NOS");
		txtitemName.textProperty().bindBidirectional(SearchString);
		// set category
		ArrayList cat = new ArrayList();
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				choiceCategory.getItems().add((String) categoryName);
				cmbInventoryParentId.getItems().add((String) categoryName);
				CategoryMst categoryMst = new CategoryMst();
				categoryMst.setCategoryName((String) categoryName);
				categoryMst.setId((String) id);
				categoryList.add(categoryMst);
			}
		}

		TxtStateCess.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					TxtStateCess.setText(oldValue);
				}
			}
		});

		txtTaxRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtTaxRate.setText(oldValue);
				}
			}
		});

		txtSellingPrice.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSellingPrice.setText(oldValue);
				}
			}
		});
		
		productProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtBrand.getText().length() > 0) && (TxtModel.getText().length() > 0)) {

					String brand = txtBrand.getText();
					String model = TxtModel.getText();
					String product =(String) newValue;
					
					String itemName = brand+"-"+product+"-"+model;
					txtitemName.setText(itemName);
				} 
				
			}
		});
		
		brandProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtProduct.getText().length() > 0) && (TxtModel.getText().length() > 0)) {

					String brand = (String) newValue;
					String model = TxtModel.getText();
					String product = txtProduct.getText();
					
					String itemName = brand+"-"+product+"-"+model;
					txtitemName.setText(itemName);
				} 
				
			}
		});

		modelProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtProduct.getText().length() > 0) && (txtBrand.getText().length() > 0)) {

					String model = (String) newValue;
					String brand = txtBrand.getText();
					String product = txtProduct.getText();
					
					String itemName = brand+"-"+product+"-"+model;
					txtitemName.setText(itemName);
				} 
				
			}
		});



		tblItemList.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getItemName() && newSelection.getItemName().length() > 0) {
					if (null != newSelection.getId()) {

						ResponseEntity<ItemMst> itemDetailsSaved = RestCaller.getitemMst(newSelection.getId());
						serviceItemMst = itemDetailsSaved.getBody();

						if (null != serviceItemMst) {
							if (null != serviceItemMst.getItemName()) {
								txtitemName.setText(serviceItemMst.getItemName());
							}
							if (null != serviceItemMst.getBarCode()) {
								txtbarCode.setText(serviceItemMst.getBarCode());
							}
							if (null != serviceItemMst.getTaxRate()) {

								txtTaxRate.setText(Double.toString(serviceItemMst.getTaxRate()));
							}

							if (null != serviceItemMst.getHsnCode()) {
								txtHsnCode.setText(serviceItemMst.getHsnCode());
							}

							if (null != serviceItemMst.getItemCode()) {
								txtitemCode.setText(serviceItemMst.getItemCode());
							}
							if (null != serviceItemMst.getCess()) {
								TxtStateCess.setText(Double.toString(serviceItemMst.getCess()));
							}
							if (null != serviceItemMst.getStandardPrice()) {
								txtSellingPrice.setText(Double.toString(serviceItemMst.getStandardPrice()));
							}

							if (null != serviceItemMst.getModel()) {
								TxtModel.setText(serviceItemMst.getModel());
							}
							if (null != serviceItemMst.getProductId()) {
								ResponseEntity<ProductMst> productMstResp = RestCaller
										.productById(serviceItemMst.getProductId());

								ProductMst productMst = new ProductMst();
								productMst = productMstResp.getBody();
								if (null != productMst) {
									txtProduct.setText(productMst.getProductName());
								}
							}
							if (null != serviceItemMst.getBrandId()) {

								ResponseEntity<BrandMst> brandMstResp = RestCaller
										.brandById(serviceItemMst.getBrandId());

								BrandMst brandMst = new BrandMst();
								brandMst = brandMstResp.getBody();
								if (null != brandMst) {
									txtBrand.setText(brandMst.getBrandName());
								}
							}

							if (null != serviceItemMst.getCategoryId()) {
								ResponseEntity<CategoryMst> categoryResp = RestCaller
										.getCategoryById(serviceItemMst.getCategoryId());
								CategoryMst categoryMst = categoryResp.getBody();
								if (null != categoryMst) {
									choiceCategory.getSelectionModel().select(categoryMst.getCategoryName());
								}
							}
						}

						if (null != serviceItemMst.getUnitId()) {

							ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(serviceItemMst.getUnitId());
							UnitMst unitMst = unitMstResp.getBody();
							if (null != unitMst) {
								choiceUnit.getSelectionModel().select(unitMst.getUnitName());
							}
						}
					}

				}

			}

		});
		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				System.out.println("Supplier Search changed");
				LoadItemBySearch((String) newValue);
			}
		});
	}
	private void LoadItemBySearch(String searchData) {
		tblItemList.getItems().clear();
		ItemTableList2.clear();
		ArrayList item = new ArrayList();

		item =   RestCaller.SearchItemByNameLimited(searchData);
		Iterator itr = item.iterator();
 
		 
		Object id = null ;
		 
		while (itr.hasNext()) {
			java.util.LinkedHashMap element = (LinkedHashMap) itr.next();
			 

			Object itemName = element.get("itemName");
		    Object standardPrice = element.get("standardPrice");
			Object taxRate = element.get("taxRate");
			Object unitId = element.get("unitId");
			Object categoryId = element.get("categoryId");
			Object isDeleted = element.get("isDeleted");
			Object cessRate = element.get("cess");
			Object product = element.get("productId");
			Object brand = element.get("brandId");
			Object model = element.get("model");
			Object itemcode = element.get("itemCode");
			Object barcode = element.get("barCode");

			 id = element.get("id");
 
			
			if (null != itemName) {
				ItemMst itemMst = new ItemMst();
				itemMst.setItemName((String) itemName);
				itemMst.setStandardPrice((Double) standardPrice);
				itemMst.setTaxRate((Double) taxRate);
				itemMst.setUnitId((String) unitId);
				itemMst.setCategoryId((String) categoryId);
				itemMst.setId((String) id);
				itemMst.setCess((Double) cessRate);
				itemMst.setIsDeleted((String) isDeleted);
				itemMst.setProductId((String) product);
				itemMst.setBrandId((String) brand);
				itemMst.setModel((String) model);
				itemMst.setItemCode((String) itemcode);
				itemMst.setBarCode((String) barcode);
				ItemTableList2.add(itemMst);
			
			}
			

		}
		tblItemList.setItems(ItemTableList2);
		
		 ShowAllFillTable();
	}

	@FXML
	void ActionAddCategory(ActionEvent event) {
		
		CategoryMst categoryMst = new CategoryMst();
		categoryList2.clear();

		if (null != cmbInventoryParentId.getSelectionModel().getSelectedItem()) {
			String catName = cmbInventoryParentId.getSelectionModel().getSelectedItem().toString();
			for (CategoryMst cc : categoryList) {
				if (cc.getCategoryName() == catName) {
					categoryMst.setParentId(cc.getId());
				}
			}
		}
		String vNo= RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"CAT");
		
		categoryMst.setId(vNo+SystemSetting.getUser().getCompanyMst().getId());
		categoryMst.setCategoryName(txtIventoryCategory.getText());
		ResponseEntity<CategoryMst> respentity = RestCaller.saveCategory(categoryMst);
		categoryMst = respentity.getBody();

		eventBus.post(categoryMst);
		categoryList2.add(categoryMst);
		// get all category list
		choiceCategory.getItems().clear();
		cmbInventoryParentId.getItems().clear();
		ArrayList cat = new ArrayList();
		 
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				choiceCategory.getItems().add((String) categoryName);
				cmbInventoryParentId.getItems().add((String) categoryName);
				CategoryMst category = new CategoryMst();
				category.setCategoryName((String) categoryName);
				category.setId((String) id);
				categoryList.add(category);
			}

		}

		FillTableCategoryList();
		ClearCategory();


	}
	private void ClearCategory() {

		txtIventoryCategory.setText("");
		cmbInventoryParentId.getSelectionModel().clearSelection();
	}

	@FXML
	void ActionDeleteCategory(ActionEvent event) {

	}

	@FXML
	void ActionShowAllCategory(ActionEvent event) {
		
		categoryList2.clear();

		ArrayList cat = new ArrayList();
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object parentId = lm.get("parentId");
			Object id = lm.get("id");
			if (id != null) {
				CategoryMst category = new CategoryMst();
				category.setCategoryName((String) categoryName);
				category.setParentId((String) parentId);
				category.setId((String) id);
				categoryList2.add(category);

			}

		}
		FillTableCategoryList();

	}

	@FXML
	void BarcodeEnterActionSS(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (txtbarCode.getText().trim().isEmpty()) {
				String barcode = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
				txtbarCode.setText(barcode);
			}
			txtitemCode.requestFocus();
		}

	}

	@FXML
	void CategoryNameEnterAction(KeyEvent event) {

	}

	@FXML
	void ComboCategoryEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtTaxRate.requestFocus();
			}
	}

	@FXML
	void ComboUniteFocusAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			TxtModel.requestFocus();
		}

	}

	@FXML
	void FocusReodrWaitingTime(KeyEvent event) {

	}

	@FXML
	void HsnCodeEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			TxtStateCess.requestFocus();
		}

	}

	@FXML
	void ItemCodeEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (txtitemCode.getText().trim().isEmpty()) {
				String itemcode = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
				txtitemCode.setText(itemcode);
			}
			txtSellingPrice.requestFocus();
		}

	}

	@FXML
	void ItemNameEnterAction(KeyEvent event) {

	}

	@FXML
	void Refresh(ActionEvent event) {

	}

	@FXML
	void SellingPriceEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			choiceCategory.requestFocus();
		}
	}

	@FXML
	void StateCessEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if(TxtStateCess.getText().trim().isEmpty())
			{
				TxtStateCess.requestFocus();
			} else {
				txtCostPrice.requestFocus();
			}
	
		}

	}
	
    @FXML
    void SubmitOnPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
		
				addItem() ;

		}
    }
    
    @FXML
    private TextField txtCostPrice;
    

    @FXML
    void CostPriceOnPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			
				btnSubmit.requestFocus();

		}
    }

	@FXML
	void TaxRateEnterAction(KeyEvent event) {
		
		if (event.getCode() == KeyCode.ENTER) {
			txtHsnCode.requestFocus();
		}

	}

	@FXML
	void barcode(ActionEvent event) {

		String barcode = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
		txtbarCode.setText(barcode);

	}

	@FXML
	void clear(ActionEvent event) {

		txtbarCode.clear();
		txtitemName.clear();
		txtTaxRate.clear();
		txtitemCode.clear();
		txtHsnCode.clear();
		txtSellingPrice.clear();
		TxtStateCess.clear();
		choiceCategory.getSelectionModel().clearSelection();
		choiceUnit.getSelectionModel().clearSelection();
		txtProduct.clear();
		txtBrand.clear();
		TxtModel.clear();
		serviceItemMst = null;
		ItemTableList2 = null;

	}

	@FXML
	void delete(ActionEvent event) {

		if (null != serviceItemMst) {

			if (null != serviceItemMst.getId()) {
				serviceItemMst.setIsDeleted("Y");
				RestCaller.updateItemMst(serviceItemMst);
				notifyMessage(5, "Successfully deleted!!!", true);
				serviceItemMst = null;
				choiceUnit.setDisable(false);
				ResponseEntity<List<ItemMst>> itemList = RestCaller.getAllItemMst();
				ItemTableList2 = FXCollections.observableArrayList(itemList.getBody());

				ShowAllFillTable();
				txtbarCode.clear();
				txtBrand.clear();
				txtHsnCode.clear();
				txtitemCode.clear();
				txtitemName.clear();
				txtIventoryCategory.clear();
				txtProduct.clear();
				txtSellingPrice.clear();
				txtTaxRate.clear();
				TxtModel.clear();
				TxtStateCess.clear();
				choiceCategory.getSelectionModel().clearSelection();
				choiceUnit.getSelectionModel().clearSelection();
				serviceItemMst = null;
			}

		}

	}

	@FXML
	void itemcode(ActionEvent event) {

		String itemcode = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
		txtitemCode.setText(itemcode);

	}

	@FXML
	void showall(ActionEvent event) {

		ResponseEntity<List<ItemMst>> itemList = RestCaller.getAllItemMst();
		ItemTableList2 = FXCollections.observableArrayList(itemList.getBody());

		ShowAllFillTable();

	}

	@FXML
	void submit(ActionEvent event) {
		addItem() ;
		
	}
	
	private void addItem() {
		


		if (txtProduct.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select/enter Product!!!", false);
			txtProduct.requestFocus();
			return;
		}

		if (txtBrand.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select/enter Brand!!!", false);
			txtBrand.requestFocus();
			return;
		}

		if (TxtModel.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select/enter Model!!!", false);
			TxtModel.requestFocus();
			return;
		}

		if (txtbarCode.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select/enter BarCode!!!", false);
			txtbarCode.requestFocus();
			return;
		}

		if (txtitemCode.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select/enter Item Code!!!", false);
			txtitemCode.requestFocus();
			return;
		}

		if (txtTaxRate.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select/enter Tax Rate!!!", false);
			txtTaxRate.requestFocus();
			return;
		}

		if (null == choiceCategory.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please select Category!!!", false);
			choiceCategory.requestFocus();
			return;
		}

		if (txtHsnCode.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Hsn Code!!!", false);
			txtHsnCode.requestFocus();
			return;
		}

		if (txtSellingPrice.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Price!!!", false);
			txtSellingPrice.requestFocus();
			return;
		}

		if (TxtStateCess.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Cess Rate!!!", false);
			TxtStateCess.requestFocus();
			return;
		}

		productMst = getProductMstByName(txtProduct.getText());

		if (null == productMst) {
			notifyMessage(5, "Product Name Not available", false);
			return;
		}

		brandMst = getBrandMstByName(txtBrand.getText());

		if (null == brandMst) {
			notifyMessage(5, "Brand Not available", false);
			return;
		}
		String itemId = null;
		if (null != serviceItemMst) {
			itemId = serviceItemMst.getId();
		} 

		serviceItemMst = new ItemMst();
		if(null != itemId)
		{
			serviceItemMst.setId(itemId);
		}
		

		ResponseEntity<CategoryMst> response = RestCaller
				.getCategoryByName(choiceCategory.getSelectionModel().getSelectedItem().toString());
		CategoryMst categoryMst = response.getBody();

		if (null == categoryMst) {
			notifyMessage(5, "Category not available!!!", false);
			choiceCategory.requestFocus();
			return;
		}
		serviceItemMst.setCategoryId(categoryMst.getId());

		ResponseEntity<UnitMst> unitMstResp = RestCaller
				.getUnitByName(choiceUnit.getSelectionModel().getSelectedItem().toString());
		UnitMst unitMst = unitMstResp.getBody();
		if (null == unitMst) {
			notifyMessage(5, "Unit not available!!!", false);
			choiceCategory.requestFocus();
			return;
		}

		serviceItemMst.setUnitId(unitMst.getId());

		serviceItemMst.setProductId(productMst.getId());
		serviceItemMst.setBrandId(brandMst.getId());
		serviceItemMst.setBarCode(txtbarCode.getText());
		serviceItemMst.setBranchCode(SystemSetting.systemBranch);
		serviceItemMst.setCess(Double.parseDouble(TxtStateCess.getText()));
		serviceItemMst.setHsnCode(txtHsnCode.getText());
		serviceItemMst.setIsDeleted("N");
		serviceItemMst.setBestBefore(0);
		serviceItemMst.setItemCode(txtitemCode.getText());

//		String itemName = txtProduct.getText() + "-" + txtBrand.getText() + "-" + TxtModel.getText();
		serviceItemMst.setItemName(txtitemName.getText());
		serviceItemMst.setModel(TxtModel.getText());
		serviceItemMst.setStandardPrice(Double.parseDouble(txtSellingPrice.getText()));
		serviceItemMst.setTaxRate(Double.parseDouble(txtTaxRate.getText()));
		serviceItemMst.setRank(0);
		serviceItemMst.setServiceOrGoods("SERVICE ITEM");
		
		if (null != serviceItemMst.getId()) {

			RestCaller.updateItemMst(serviceItemMst);
			notifyMessage(5, "Successfully updated", true);
		} else {

			String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "I");
			serviceItemMst.setId(vNo + SystemSetting.getUser().getCompanyMst().getCompanyName());
			ResponseEntity<ItemMst> respentity = RestCaller.saveItemmst(serviceItemMst);
			serviceItemMst = respentity.getBody();
			if(null != serviceItemMst)
			{
				notifyMessage(5, "Successfully added", true);
				
				if(!txtCostPrice.getText().trim().isEmpty())
				{

					ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
							.getPriceDefenitionMstByName("COST PRICE");
					PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
					if (null != priceDefenitionMst) {
						
							PriceDefinition price = new PriceDefinition();
							price.setItemId(serviceItemMst.getId());
							price.setUnitId(serviceItemMst.getUnitId());
							price.setPriceId(priceDefenitionMst.getId());
							price.setAmount(Double.parseDouble(txtCostPrice.getText()));
							LocalDate lDate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
							price.setStartDate(Date.valueOf(lDate));
							price.setBranchCode(SystemSetting.systemBranch);
							
							ResponseEntity<PriceDefinition> respentityPrice = RestCaller.savePriceDefenition(price);
	
					}
				
				}
			} 
			

		}
		tblItemList.getItems().clear();
		ItemTableList2.clear();
		//		ResponseEntity<List<ItemMst>> itemList = RestCaller.getAllItemMst();
		ItemTableList2.add(serviceItemMst);

//		ShowAllFillTable();
		txtbarCode.clear();
//		txtBrand.clear();
//		txtHsnCode.clear();
		txtitemCode.clear();
		txtitemName.clear();
		txtIventoryCategory.clear();
//		txtProduct.clear();
		txtSellingPrice.clear();
//		txtTaxRate.clear();
		TxtModel.clear();
//		TxtStateCess.clear();
//		choiceCategory.getSelectionModel().clearSelection();
//		choiceUnit.getSelectionModel().clearSelection();
		serviceItemMst = null;
		txtCostPrice.clear();
		TxtModel.requestFocus();
		
	
		
	}

	private BrandMst getBrandMstByName(String brand) {
		ResponseEntity<BrandMst> brandMstResp = RestCaller.findBrandMstByName(brand);

		BrandMst brandMst = new BrandMst();

		brandMst = brandMstResp.getBody();

		if (null == brandMst) {
			brandMst = new BrandMst();
			brandMst.setBrandName(txtBrand.getText());
			brandMst.setBranchCode(SystemSetting.systemBranch);

			ResponseEntity<BrandMst> savedBrandMst = RestCaller.saveServiceBrandMst(brandMst);
			brandMst = savedBrandMst.getBody();
		}

		return brandMst;
	}

	private ProductMst getProductMstByName(String productName) {

		ResponseEntity<ProductMst> productMstResp = RestCaller.findProductMstByName(productName);

		ProductMst productMst = new ProductMst();
		productMst = productMstResp.getBody();

		if (null == productMst) {
			productMst = new ProductMst();
			productMst.setProductName(txtProduct.getText());
			productMst.setBranchCode(SystemSetting.systemBranch);

			ResponseEntity<ProductMst> savedProduct = RestCaller.saveServiceProductMst(productMst);
			productMst = savedProduct.getBody();
		}

		return productMst;
	}

	private void ShowAllFillTable() {

		//tblItemList.getItems().clear();
		tblItemList.setItems(ItemTableList2);

		for (ItemMst i : ItemTableList2) {

			if (null != i.getProductId()) {
				ResponseEntity<ProductMst> productResp = RestCaller.productById(i.getProductId());
				ProductMst productMst = productResp.getBody();

				if (null != productMst) {
					i.setProductName(productMst.getProductName());
					clProduct.setCellValueFactory(cellData -> cellData.getValue().getProductNameProperty());

				}
			}

			if (null != i.getBrandId()) {
				ResponseEntity<BrandMst> brandResp = RestCaller.brandById(i.getBrandId());
				BrandMst brandMst = brandResp.getBody();

				if (null != brandMst) {
					i.setBrandName(brandMst.getBrandName());
					clBrand.setCellValueFactory(cellData -> cellData.getValue().getBrandNameProperty());

				}
			}
			if (null != i.getUnitId()) {
				ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(i.getUnitId());
				UnitMst unitMst = unitMstResp.getBody();

				if (null != unitMst) {
					i.setUnitName(unitMst.getUnitName());
					clItemUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
				}
			}

			if (null != i.getCategoryId()) {
				ResponseEntity<CategoryMst> categoryResp = RestCaller.getCategoryById(i.getCategoryId());
				CategoryMst categoryMst = categoryResp.getBody();
				if (null != categoryMst) {
					i.setCategoryName(categoryMst.getCategoryName());
					clItemCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
				}
			}
			if (null != i.getItemName()) {
				clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

			}
			if (null != i.getTaxRate()) {
				clItemTax.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
			}
			if (null != i.getStandardPrice()) {
				clItemSellingPrice.setCellValueFactory(cellData -> cellData.getValue().getStandardPriceProperty());
			}
			if (null != i.getIsDeleted()) {

				if (i.getIsDeleted().equals("Y")) {
					i.setDeletedStatus("DELETED");
					clStatus.setCellValueFactory(cellData -> cellData.getValue().getDeletedStatusProperty());
				} else {
					i.setDeletedStatus("ACTIVE");
					clStatus.setCellValueFactory(cellData -> cellData.getValue().getDeletedStatusProperty());
				}
			}

			if (null != i.getCess()) {
				clCess.setCellValueFactory(cellData -> cellData.getValue().getCessProperty());
			}

			if (null != i.getItemCode()) {
				clItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
			}

			if (null != i.getBarCode()) {
				clBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarCodeProperty());
			}

		}

		clModel.setCellValueFactory(cellData -> cellData.getValue().getModelProperty());

	}
	
	private void FillTableCategoryList() {
		tblCategoryList.setItems(categoryList2);
		for (CategoryMst i : categoryList2) {
			for (CategoryMst u : categoryList) {
				if (null != i.getParentId()) {
					if (i.getParentId().equals(u.getId())) {
						i.setParentName(u.getCategoryName());
						clParentCategory.setCellValueFactory(cellData -> cellData.getValue().getParentNameProperty());
					}
				}
			}

			clCategoryName.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());

		}

	}
	
	  private void loadProdcutPopUp()
	    {
	    	try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ProductPopUp.fxml"));
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				txtBrand.requestFocus();
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	  
	    private void loadBrandPopUp()
	    {
	    	try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/BrandPopUp.fxml"));
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				choiceUnit.requestFocus();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }

	    @Subscribe
	   	public void popupItemlistner(ProductEvent productEvent) {

	   		System.out.println("-------------popupItemlistner-------------");

	   		Stage stage = (Stage) btnDelete.getScene().getWindow();
	   		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
	   		if (stage.isShowing()) {
	   			txtProduct.setText(productEvent.getProductName());
	   	
	   		}
	   	}
	    @Subscribe
	   	public void popupItemlistner(BrandEvent brandEvent) {

	   		System.out.println("-------------popupItemlistner-------------");

	   		Stage stage = (Stage) btnDelete.getScene().getWindow();
	   		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
	   		if (stage.isShowing()) {
	   			txtBrand.setText(brandEvent.getBrandName());
	   	
	   		}
	   	}
	public void notifyMessage(int duration, String msg, boolean success) {
		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload(hdrId);
			}


		private void PageReload(String hdrId) {

		}


}
