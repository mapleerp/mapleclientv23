package com.maple.mapleclient.controllers;



import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ConsumptionReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class ConsumptionReportsCtl {
	
	  @FXML
	    private AnchorPane btnClear;

	    @FXML
	    private DatePicker txtFromDate;

	    @FXML
	    private DatePicker txtToDate;

	    @FXML
	    private Button btnShowReport;

	    @FXML
	    private Button btnPrint;

	    @FXML
	    private TableView<ConsumptionReport> tblTable;

	    @FXML
	    private TableColumn<ConsumptionReport, String> clVocherNo;

	    @FXML
	    private TableColumn<ConsumptionReport, String> clDate;

	    @FXML
	    private TableColumn<ConsumptionReport, Number> clAmount;

	    @FXML
	    private TableColumn<ConsumptionReport, String> clDepart;

	    @FXML
	    private TableColumn<ConsumptionReport, String> clUsername;
	    
	    private ObservableList<ConsumptionReport> consumptioReportList = FXCollections.observableArrayList();
	    
	    
	    @FXML
	    void clear(ActionEvent event) {
	    	

	    	tblTable.getItems().clear();
	    	txtFromDate.getEditor().clear();;
	    	txtToDate.getEditor().clear();
	    	

	    }

	    @FXML
	    void print(ActionEvent event) {
	    	
	    	if(null==txtFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    		
	    	}
	    	if (null==txtToDate.getValue()) {
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    		
	    	}
	    	java.util.Date uDate = Date.valueOf(txtFromDate.getValue());
	    	String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
	    	java.util.Date uDate1 = Date.valueOf(txtToDate.getValue());
	    	String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
	    	
	    	String Branch = SystemSetting.getSystemBranch();
	    	
			
			try {
				JasperPdfReportService.consumptionreportsummary(Branch,strDate,strDate1) ;
			} catch (Exception e) {
				// TODO: handle exception
			}
			

	    }
	    @FXML
		private void initialize() {
	    	txtToDate = SystemSetting.datePickerFormat(txtToDate, "dd/MMM/yyyy");
	    	txtFromDate = SystemSetting.datePickerFormat(txtFromDate, "dd/MMM/yyyy");
	 }
	    @FXML
	    void showReport(ActionEvent event) {
	    	
	    	if(null == txtFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
				return;
	    	}
	    	if(null == txtToDate.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	}
	    	
	    	java.util.Date uDate = Date.valueOf(txtFromDate.getValue());
	    	String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
	    	java.util.Date uDate1 = Date.valueOf(txtToDate.getValue());
	    	String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
	    	
	    	String Branch = SystemSetting.getSystemBranch();
	    	
	    	ResponseEntity<List<ConsumptionReport>> consumptionReportResp = RestCaller.getConsumptionReportSummary(Branch, strDate,strDate1);
	    	consumptioReportList = FXCollections.observableArrayList(consumptionReportResp.getBody());
	    	
                 if (consumptioReportList.size() > 0) {
    			
    			
    			filltable();
    			
    			
    		}

	    }
	    
	    
	    
	    
        private void filltable() {
        	
        	tblTable.setItems(consumptioReportList);
        	
        	clVocherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
        	clDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
        	clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
        	clDepart.setCellValueFactory(cellData -> cellData.getValue().getDepartmentProperty());
        	clUsername.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
			
		}

		private void notifyMessage(int i, String string, boolean b) {
  			
  	    	Image img;
  			if (b) {
  				img = new Image("done.png");

  			} else {
  				img = new Image("failed.png");
  			}

  			Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
  					.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
  					.onAction(new EventHandler<ActionEvent>() {
  						@Override
  						public void handle(ActionEvent event) {
  							System.out.println("clicked on notification");
  						}
  					});
  			notificationBuilder.darkStyle();
  			notificationBuilder.show();

  	    	
  		}

}
