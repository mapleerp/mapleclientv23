package com.maple.mapleclient.controllers;

import java.util.List;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;


import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.ReorderMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import javafx.scene.control.ComboBox;

public class ReceiptModeCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
	
	ReceiptModeMst receiptMode = null;

    @FXML
    private TextField txtReceiptMode;

    @FXML
    private Button btnSave;

    
    @FXML
    private ComboBox<String> cmbCreditCardStatus;
    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;

    @FXML
    private TableView<ReceiptModeMst> tblReceiptMode;

    @FXML
    private TableColumn<ReceiptModeMst, String> clReceiptMode;

    @FXML
    private TableColumn<ReceiptModeMst, String> clStatus;
    @FXML
    private TableColumn<ReceiptModeMst, String>  clCreditCardStatus;
    
   
    @FXML
	private void initialize() {
    	
    	tblReceiptMode.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					receiptMode = new ReceiptModeMst();
					
					txtReceiptMode.setText(newSelection.getReceiptMode());
					cmbCreditCardStatus.getSelectionModel().select(newSelection.getCreditCardStatus());;
				
					cmbCreditCardStatus.setValue(newSelection.getCreditCardStatus());
				//	txtReceiptMode.setText(newSelection.getReceiptMode());
					receiptMode.setId(newSelection.getId());

				}
			}
		});
    	
    	
    	
    	cmbCreditCardStatus.getItems().add("YES");
    	cmbCreditCardStatus.getItems().add("NO");
    }

    @FXML
    void DeleteReceiptMode(ActionEvent event) {
    	
    	if(null != receiptMode)
    	{
    		if(null != receiptMode.getId())
    		{
    			receiptMode.setStatus("DELETED");
    			
    			RestCaller.deleteReceiptMode(receiptMode.getId());
    			tblReceiptMode.getItems().clear();
    			showAll();
    			receiptMode = null;
    			
    		}
    	}

    }

    @FXML
    void FocusOnbtnSave(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		save();
		}

    }

    @FXML
    void ShowAll(ActionEvent event) {
    	
    	showAll();
    }
    
    private void showAll() {
    	
    	ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
    	receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());
    	tblReceiptMode.getItems().clear();
		FillTable();

		
	}

    @FXML
    void save(ActionEvent event) {
    	
    	save();
    }
    
    private void save() {
    	if(null!=receiptMode) {
    		
    		receiptMode.setCreditCardStatus(cmbCreditCardStatus.getValue());
    		receiptMode.setStatus("ACTIVE");
    		
    		
    		RestCaller.updateReceiptmode(receiptMode);
    		
    		receiptMode = null;
    		
    		notifyMessage(5, "Updated",false);
    		FillTable();
    	}else {
    	
    	if(txtReceiptMode.getText().trim().isEmpty())
    	{
    		notifyMessage(3, "Please enter receipt mode name..!", false);
    		return;
    	} 
    	if (null == cmbCreditCardStatus.getValue()) {
    		
    		notifyMessage(3, "Please select card status..!", false);
    		return;
			
		} else {
			
    		ReceiptModeMst receiptMode = RestCaller.getReceiptModeByName(txtReceiptMode.getText()).getBody();
    		if(null == receiptMode)
    		{
    		receiptMode = new ReceiptModeMst();
    		receiptMode.setReceiptMode(txtReceiptMode.getText());
    		receiptMode.setStatus("ACTIVE");
    		receiptMode.setCreditCardStatus(cmbCreditCardStatus.getValue());
    		ResponseEntity<ReceiptModeMst> receiptModeRespEntity = RestCaller.saveReceiptMode(receiptMode);
    		receiptMode = receiptModeRespEntity.getBody();
    		
    
    		
    		if(null != receiptMode)
    		{
    			if(null != receiptMode.getId())
    			{
    				notifyMessage(5, "Successfully saved", true);
    			}
    			
    		}
    		
    		receiptMode = null;
    		
    		} else {
    			notifyMessage(5, "Duplicate entry", false);
    			return;
			}
		}
    	}
    	
    	ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
    	receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());
    	tblReceiptMode.getItems().clear();
		FillTable();
		
		
    	txtReceiptMode.clear();
		cmbCreditCardStatus.getSelectionModel().clearSelection();
    	
		
	}
    private void FillTable() {
    	
    	tblReceiptMode.setItems(receiptModeList);
    	
 
    	clReceiptMode.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

    	clStatus.setCellValueFactory(cellData -> cellData.getValue().getStatusModeProperty());
    	clCreditCardStatus.setCellValueFactory(cellData -> cellData.getValue().getCreditCardStatusProperty());
	}
    
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }

}
