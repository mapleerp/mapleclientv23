package com.maple.mapleclient.controllers;


import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.MonthlySalesDtl;
import com.maple.mapleclient.entity.MonthlySalesTransHdr;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ReceiptModeReport;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
public class MonthlySalesReportCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<MonthlySalesTransHdr> dailySalesList = FXCollections.observableArrayList();
	private ObservableList<MonthlySalesDtl> dailySalesLists = FXCollections.observableArrayList();
	
	double totalamount = 0.0;
	String voucher = null;
	    @FXML
	    private DatePicker dpdate;

	    @FXML
	    private TableView<MonthlySalesTransHdr> tbReport;

	    @FXML
	    private TableColumn<MonthlySalesTransHdr, String> clVoucherNo;

	    @FXML
	    private TableColumn<MonthlySalesTransHdr, String> clDate;

	    @FXML
	    private TableColumn<MonthlySalesTransHdr, String> clCustomer;

	 

	    @FXML
	    private TableColumn<MonthlySalesTransHdr, Number> clamt;

	    @FXML
	    private TextField txtGrandTotal;

	    @FXML
	    private Button btnShow;
	    @FXML
	    private Button btnSalesToMonthlySales;

	    @FXML
	    private TableView<MonthlySalesDtl> tbreports;

	    @FXML
	    private TableColumn<MonthlySalesDtl,String>voucherNo;

	    @FXML
	    private TableColumn<MonthlySalesDtl, String> Customername;

	    @FXML
	    private TableColumn<MonthlySalesDtl, String> itemName;

	    @FXML
	    private TableColumn<MonthlySalesDtl, Number> qty;

	    @FXML
	    private TableColumn<MonthlySalesDtl, Number> clRate;

	    @FXML
	    private TableColumn<MonthlySalesDtl, Number> taxRate;

	    @FXML
	    private TableColumn<MonthlySalesDtl, Number> cessRate;

	    @FXML
	    private TableColumn<MonthlySalesDtl, String> unit;

	    @FXML
	    private TableColumn<MonthlySalesDtl, Number> amount;

	    @FXML
	    private ComboBox<String> branchselect;

	    @FXML
	    private Button ok;

	    @FXML
	    private Button btnPrint;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private Button btnPrintReport;

	    @FXML
	    private Button btnExportToExcel;

	  
	    @FXML
	    void PrintReport(ActionEvent event) {

	    }

	    @FXML
	    void actionExcelExport(ActionEvent event) {

	    }

	    @FXML
	    void onClickOk(ActionEvent event) {

	    }

	    @FXML
	    void reportPrint(ActionEvent event) {

	    }

	    @FXML
	    void show(ActionEvent event) {

	    	if(null==dpdate.getValue()||null==dpToDate.getValue()) {
	    		notifyMessage(5,"select proper date");	
	    	}else if (null==branchselect.getSelectionModel().getSelectedItem()) {
	    		notifyMessage(5,"select Branch Code");	
			}
	    		 
	    	
	    	 totalamount = 0.0;
	     	java.util.Date uDate = Date.valueOf(dpdate.getValue());
	 		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
	 		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
	 		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
	 		//uDate = SystemSetting.StringToUtilDate(strDate,);
	     	ResponseEntity<List<MonthlySalesTransHdr>> monthlySales = RestCaller.getMonthlySalesReport(branchselect.getSelectionModel().getSelectedItem(), strDate,strDate1);
	     	if(null!=monthlySales.getBody()) {
	     	
	     	dailySalesList = FXCollections.observableArrayList(monthlySales.getBody());
	     	for(MonthlySalesTransHdr dtl :dailySalesList)
	     	{
	     		BigDecimal bdCAmount= new BigDecimal(dtl.getInvoiceAmount());
	     		bdCAmount = bdCAmount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
	     		dtl.setInvoiceAmount(bdCAmount.doubleValue());
	     	}
	     	tbReport.setItems(dailySalesList);
	     	
	     	FillReportHdr();
	     	}else {
	     		notifyMessage(5, "No Data !!!!!!!!!!");
	     	}
	     	for(MonthlySalesTransHdr monthlySaleReportDtl:dailySalesList)
	     	{
	     		if(null != monthlySaleReportDtl.getInvoiceAmount())
	     		totalamount = totalamount + monthlySaleReportDtl.getInvoiceAmount();
	     	}
	     	BigDecimal settoamount = new BigDecimal(totalamount);
	 		settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
	     	txtGrandTotal.setText(settoamount.toString());

	     	
	    	
	    }
	
	
	    @FXML
		private void initialize() {
	    	setBranches();
	    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
	    	dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");

			  tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
		    		if (newSelection != null) {
		    			System.out.println("getSelectionModel");
		    			if (null != newSelection.getVoucherNumber()) {
		    			
		    				String voucherNumber=newSelection.getVoucherNumber();
		    			//	customer = newSelection.getCustomerName();
		    				voucher = newSelection.getVoucherNumber();
		    		//		java.sql.Date uDate = newSelection.getVoucherDate();
		    	    //		 vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		    		    
		    				 String branchName=branchselect.getValue();
		    				ResponseEntity<List<MonthlySalesDtl>> dailysaless = RestCaller.getMonthlySalesReportDtl(newSelection.getVoucherNumber(),branchName);
		    		    	dailySalesLists = FXCollections.observableArrayList(dailysaless.getBody());
		    		    	tbreports.setItems(dailySalesLists);
		    		    	
		    		    	FillTable();
		    			}
		    		}
		    	});
	    	
	    }
	    
	    
	    
	    
		private void setBranches() {
			
			ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
			List<BranchMst> branchMstList = new ArrayList<BranchMst>();
			branchMstList = branchMstRep.getBody();
			
			for(BranchMst branchMst : branchMstList)
			{
				branchselect.getItems().add(branchMst.getBranchCode());
			
				
			}
			 
		}
	
		
		


		private void FillTable()
		{
			
			amount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
			Customername.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
	    	voucherNo.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
	    	qty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
	    	clRate.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
	    	taxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
	    	cessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
	    	unit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
	    	itemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	    	
	    	
		}
		
		private void FillReportHdr() {
			clamt.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	    	clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());

	    	clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
	    	clDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
	    	
		}
		
		
		public void notifyMessage(int duration, String msg) {

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();

			notificationBuilder.show();
		}
		
		
		
		

	    @FXML
	    void salesToMonthlySales(ActionEvent event) {

	    	java.util.Date uDate = Date.valueOf(dpdate.getValue());
	 		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
	 		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
	 		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
	 		ResponseEntity<String> response = 	RestCaller.salesToMonthlySales(branchselect.getSelectionModel().getSelectedItem(),strDate,strDate1);
	 	   	
    		notifyMessage(5,response.getBody());
	    }
	    @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }
}
