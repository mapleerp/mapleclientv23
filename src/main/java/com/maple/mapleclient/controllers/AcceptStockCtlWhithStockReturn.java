package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.StockTransferInReturnDtl;
import com.maple.mapleclient.entity.StockTransferInReturnHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class AcceptStockCtlWhithStockReturn {
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<StockTransferInDtl> stockTransferDtlTable = FXCollections.observableArrayList();
	private ObservableList<StockTransferInDtl> stockTransferDtlTable1 = FXCollections.observableArrayList();
	private ObservableList<StockTransferInReturnDtl> stockTransferInReturnDtlList = FXCollections.observableArrayList();
	String stock_tarnsfer_prefix = SystemSetting.STOCK_TRANSFER_PREFIX;
	private ObservableList<StockTransferOutHdr> stockOutHdrList = FXCollections.observableArrayList();

	private ObservableList<StockTransferOutDtl> StockTransferList = FXCollections.observableArrayList();
	StockTransferInHdr stockTransferInHdr = null;
	StockTransferInDtl stockTransferInDtl = null;

	StockTransferOutHdr stkOutHdr = null;
	StockTransferOutDtl stockTransferOutDtl = new StockTransferOutDtl();
	
	Integer count=0;
	
	@FXML
	private TextField txtVoucherIn;
	
    @FXML
    private Button btnRefresh;
    
	@FXML
	private TextField txtintent;
	
	@FXML
	private TextField txtFromBranch;
	
	@FXML
	private DatePicker dpIntentDate;
	
	@FXML
	private TextField txtRecId;

	@FXML
	private TextField txtVoucherNumber;

	@FXML
	private TextField txtIntentDate;

	@FXML
	private ComboBox<String> cmbPending;

	@FXML
	private TableView<StockTransferInDtl> tblStockTrasfer;

	@FXML
	private TableColumn<StockTransferInDtl, String> clStktransferItemName;

	@FXML
	private TableColumn<StockTransferInDtl, String> clStktransferBarcode;

	@FXML
	private TableColumn<StockTransferInDtl, String> clStktransferBatch;

	@FXML
	private TableColumn<StockTransferInDtl, Number> clStkTransferRate;

	@FXML
	private TableColumn<StockTransferInDtl, Number> clStkTransferQty;

	@FXML
	private TableColumn<StockTransferInDtl, Number> clStkMRP;

	@FXML
	private TableColumn<StockTransferInDtl, LocalDate> clStkTransferExpiryDate;

	@FXML
	private TableColumn<StockTransferInDtl, Number> clStkTransferAmount;
	
	
	
	
    @FXML
    private TableView<StockTransferOutDtl> tblStockTRansferReturn;

    @FXML
    private TableColumn<StockTransferOutDtl, String> returnBatchCode;

    @FXML
    private TableColumn<StockTransferOutDtl, Number> returnRate;

    @FXML
    private TableColumn<StockTransferOutDtl, Number> returnQty;

    @FXML
    private TableColumn<StockTransferOutDtl, Number> returnMrp;

    @FXML
    private TableColumn<StockTransferOutDtl, LocalDate> returnExpDate;

    @FXML
    private TableColumn<StockTransferOutDtl, Number> returnAmount;

    @FXML
    private TableColumn<StockTransferOutDtl, String> returnItemName;
    @FXML
    private TableColumn<StockTransferOutDtl, String> returnBarcode;
	@FXML
	private Button refreshTable;

	@FXML
	private Button acceptStockID;

	@FXML
	private Button reset;
	@FXML
    private TextField txtQty;
	
	private Button btnReturnFinalSave;

	StockTransferInReturnHdr stockTransferInReturnHdr;
	
	StockTransferInReturnDtl stockTransferInReturnDtl;
	String barcode;
	String batchcode;
	Double  rate;
	Double qty;
	Double mrp;
	Date expDate;
	String itemName;
	String itemId;
	
	
	@FXML
	void voucherInKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtFromBranch.requestFocus();
		}

	}
	
	
	@FXML
    void onActionReturnFinalSave(ActionEvent event) {
		stockTransferReturnFinalSave();
    }


	@FXML
	void FromBranchKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtintent.requestFocus();
		}

	}

	@FXML
	void intentKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			dpIntentDate.requestFocus();
		}

	}

	@FXML
	private void initialize() {
		dpIntentDate = SystemSetting.datePickerFormat(dpIntentDate, "dd/MMM/yyyy");
		eventBus.register(this);
		cmbPending.getItems().add("COMPLETED");
		
		cmbPending.setValue("COMPLETED");
		stockTransferInHdr = new StockTransferInHdr();
		dpIntentDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		dpIntentDate.setEditable(false);

		
		
		
		tblStockTrasfer.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getBarcode()) {

					barcode=newSelection.getBarcode();
					batchcode=newSelection.getBatch();
					rate=newSelection.getRate();
					qty=newSelection.getQty();
					mrp=newSelection.getMrp();
					expDate=newSelection.getExpiryDate();
					itemName=newSelection.getItemName();
					itemId=newSelection.getItemId().getId();
					
							
					
				
				}
			}
		});

		
		
	}

	@FXML
	void acceptStock(ActionEvent event) {
		
		if(txtVoucherIn.getText().trim().isEmpty())
		{
			txtVoucherIn.requestFocus();
			notifyMessage(5,"Select Voucher Number");
			return;
		}
		
		if (null != stockTransferInHdr) {
			
			 
			
			
			String financialYear = SystemSetting.getFinancialYear();
			
			String sNo = RestCaller.getVoucherNumber(financialYear + "STIN");
			stockTransferInHdr.setVoucherNumber(sNo);
			stockTransferInHdr.setAcceptedDate(Date.valueOf(dpIntentDate.getValue()));
			stockTransferInHdr.setStatusAcceptStock("COMPLETED");
			stockTransferInHdr.setBranchCode(SystemSetting.systemBranch);
			stockTransferInHdr.setVoucherNumber(sNo);
			/*
			 * Update accept Stock
			 */
			//{companymstid}/stocktransferinhdr/acceptstock
			 RestCaller.saveAcceptStock(stockTransferInHdr);
			notifyMessage(5, "Detials Saved Successfully!!!");
			txtFromBranch.clear();
			txtintent.clear();
			txtVoucherIn.clear();
		
			stockTransferDtlTable.clear();
			stockTransferDtlTable1.clear();
			stockTransferInHdr = null;
			stockTransferInDtl = null;
			tblStockTrasfer.getItems().clear();
		//	txtVoucherNumber.clear();
		}

	}

	@FXML
	void refreshTable(ActionEvent event) {
		
		RefreshTableData();
		
	}

	private void RefreshTableData() {



		stockTransferDtlTable1.clear();
		tblStockTrasfer.getItems().clear();
		if (null != stockTransferInHdr.getId()) {
			ResponseEntity<List<StockTransferInDtl>> stkdtlSaved = RestCaller
					.getStockTransferInDtl(stockTransferInHdr.getId());
			stockTransferDtlTable = FXCollections.observableArrayList(stkdtlSaved.getBody());
			for (StockTransferInDtl stk : stockTransferDtlTable) {
				try
				{
				stockTransferInDtl = new StockTransferInDtl();
				stockTransferInDtl.setAmount(stk.getAmount());
				stockTransferInDtl.setBarcode(stk.getBarcode());
				stockTransferInDtl.setBatch(stk.getBatch());
				stockTransferInDtl.setExpiryDate(stk.getExpiryDate());
				stockTransferInDtl.setItemId(stk.getItemId());
//				ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(stk.getItemId());
				stockTransferInDtl.setItemName(stk.getItemId().getItemName());
				stockTransferInDtl.setMrp(stk.getMrp());
				stockTransferInDtl.setQty(stk.getQty());
				stockTransferInDtl.setRate(stk.getRate());
				stockTransferInDtl.setTaxRate(stk.getTaxRate());
				ResponseEntity<UnitMst> unitsaved = RestCaller.getunitMst(stk.getUnitId());
				stockTransferInDtl.setUnitId(unitsaved.getBody().getUnitName());
				stockTransferDtlTable1.add(stockTransferInDtl);
				}
				catch (Exception e) {
					
					e.printStackTrace();
				}
			}
			FillTable();

		}

	
	}

	@FXML
	void loadVoucherPopup(MouseEvent event) {

		loadPopup();
	}

	@FXML
	void reset(ActionEvent event) {
		txtFromBranch.clear();
		txtintent.clear();
		txtVoucherIn.clear();
//		dpIntentDate.setValue(null);
		tblStockTrasfer.getItems().clear();

	}

	private void FillTable() {
		tblStockTrasfer.setItems(stockTransferDtlTable1);
		clStkTransferAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clStktransferBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clStkTransferExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clStktransferItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clStkTransferQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clStkTransferRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clStktransferBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
		clStkMRP.setCellValueFactory(cellData -> cellData.getValue().getmrpProperty());

	}

	private void loadPopup() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/VoucherNoPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(VoucherNoEvent voucherNoEvent) {

		System.out.println("------Voucher-------popuplistner-------------" + voucherNoEvent);
		Stage stage = (Stage) refreshTable.getScene().getWindow();
		if (stage.isShowing()) {
			stockTransferInHdr = new StockTransferInHdr();
			stockTransferInHdr.setIntetNumber(voucherNoEvent.getIntentNumber());
//			txtAccount.setText(accountEvent.getAccountName());
			// String accountId = accountEvent.getAccountId();
			txtVoucherIn.setText(voucherNoEvent.getVoucherNumber());
			txtintent.setText(voucherNoEvent.getIntentNumber());
			txtFromBranch.setText(voucherNoEvent.getFromBranchCode());
			stockTransferInHdr.setId(voucherNoEvent.getId());
			stockTransferInHdr.setVoucherDate(voucherNoEvent.getVoucherDate());

		}

	}

    @FXML
    void RefreshVoucher(ActionEvent event) {
    	String msg = RestCaller.getallMastrs("StockTransfer");
    }
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


    private void PageReload(String hdrId) {
    	
    	ResponseEntity<StockTransferInHdr> stockTransferInHdrResp = RestCaller.getStockTransferInHdrByHdrId(hdrId);
    	StockTransferInHdr stockTransferInHdr = stockTransferInHdrResp.getBody();
    	
    	if(null != stockTransferInHdr)
    	{
        	
//    		stockTransferInHdr.setIntetNumber(stockTransferInHdr.getIntentNumber());
//    		stockTransferInHdr.setId(stockTransferInHdr.getId());
//    		stockTransferInHdr.setVoucherDate(stockTransferInHdr.getVoucherDate());
    		
    		txtVoucherIn.setText(stockTransferInHdr.getVoucherNumber());
    		txtintent.setText(stockTransferInHdr.getIntentNumber());
    		txtFromBranch.setText(stockTransferInHdr.getFromBranch());
  	
    		
    		RefreshTableData();
    	}
    	
    	

	}

    public void filltables() {
    	  returnBarcode.setCellValueFactory(cellData ->cellData.getValue().getbarcodeProperty());
    	  returnBatchCode.setCellValueFactory(cellData->cellData.getValue().getBatchCodeProperty());
    	  returnRate.setCellValueFactory(cellData->cellData.getValue().getRateProperty());
    	  returnQty.setCellValueFactory(cellData->cellData.getValue().getQtyProperty());
    	  returnMrp.setCellValueFactory(cellData->cellData.getValue().getmrpProperty());
    	  returnExpDate.setCellValueFactory(cellData->cellData.getValue().getExpiryDateProperty());
    	  returnAmount.setCellValueFactory(cellData->cellData.getValue().getAmountProperty());
    	  returnItemName.setCellValueFactory(cellData->cellData.getValue().getItemNameProperty());
    }
    
    
    
    @FXML
    void addReturnQty(MouseEvent event) {

    	
    	addReturnItem();
    	
    }
    
    public void addReturnItem() {
    	
    	System.out.print("inside add return item");
    	
    	if(txtVoucherIn.getText().isEmpty()) {
    		notifyMessage(5,"Select Voucher Number");
			return;
    	}else {
    		String voucherNumberIn=txtVoucherIn.getText();
    	
    		ResponseEntity<StockTransferInHdr> stockTransferInHdrResp = RestCaller.getStockTransferInHdrByVoucherNumberIn(voucherNumberIn);
    		stockTransferInHdr=stockTransferInHdrResp.getBody();
    		
    		
    	}
    
    	
    	
    	
    	
   if(null==stkOutHdr) {
	   stkOutHdr=new StockTransferOutHdr();
	   stkOutHdr.setFromBranch(SystemSetting.getSystemBranch());
	   stkOutHdr.setToBranch(stockTransferInHdr.getFromBranch());
	   stkOutHdr.setVoucherType("STOUT");
	   stkOutHdr.setStatus("");
	   stkOutHdr.setProcessInstanceId(processInstanceId);
	   stkOutHdr.setIntentNumber(stockTransferInHdr.getIntentNumber());
	   ResponseEntity<StockTransferOutHdr> respentity = RestCaller.saveStockTransferOutHdr(stkOutHdr);
		stkOutHdr = respentity.getBody();
			 
   }if(null!=stkOutHdr) {
	   count = count + 1;
	   stockTransferOutDtl=new StockTransferOutDtl();
	   stockTransferOutDtl.setItemName(itemName);
	   stockTransferOutDtl.setexpiryDate(expDate);;
	   stockTransferOutDtl.setBatch(batchcode);
	   stockTransferOutDtl.setMrp(mrp);
	   stockTransferOutDtl.setAmount(stockTransferOutDtl.getMrp()*qty);
	   stockTransferOutDtl.setRate(rate);
	   stockTransferOutDtl.setQty(Double.valueOf(txtQty.getText()));
	   stockTransferOutDtl.setSlNo(count);
	   
	   ResponseEntity<StockTransferOutDtl> respentity = RestCaller
				.saveStockTransferOutDtl(stockTransferOutDtl);
	   stockTransferOutDtl=   respentity.getBody();
	   StockTransferList.add(stockTransferOutDtl);
	   tblStockTRansferReturn.setItems(StockTransferList);
		RestCaller.updateStockTransferSlNo(stkOutHdr.getId());
	   filltables();
	   RefreshTableData();
	   }
	   
	   
	   
	   
	   
   }
    	
  
    	 
    	
        
    // meathod is used for messaging  stocktranferindtl upadate and update stocktransferinhdr   
    public void stockTransferReturnFinalSave() {
 	   

if(null!=stkOutHdr) {
		String financialYear = SystemSetting.getFinancialYear();
		String sNo = RestCaller.getVoucherNumber(financialYear + stock_tarnsfer_prefix);
		stkOutHdr.setVoucherNumber(sNo);
		// RestCaller.saveStockTransferOutHdr(stkOutHdr);

		Date date = Date.valueOf(dpIntentDate.getValue());

		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");

		stkOutHdr.setVoucherDate(date);

		

		try {

			RestCaller.updateStockTransferOutHdr(stkOutHdr);
		} catch (Exception e) {

			System.out.println(e.toString());
			throw e;

		}

		/*
		 * Print out Stock transfer
		 */

		String strDate = formatter.format(stkOutHdr.getVoucherDate());
		StockTransferList.clear();
		

		String vNo = stkOutHdr.getVoucherNumber();
		stkOutHdr = null;
		stockTransferOutDtl = new StockTransferOutDtl();
	
		try {

			JasperPdfReportService.StockTransferInvoiceReport(vNo, strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		if(txtVoucherIn.getText().trim().isEmpty())
		{
			txtVoucherIn.requestFocus();
			notifyMessage(5,"Select Voucher Number");
			return;
		}
		
		if (null != stockTransferInHdr) {
			
			 
			
			
			String financialYears = SystemSetting.getFinancialYear();
			
			String sNos = RestCaller.getVoucherNumber(financialYears + "STIN");
			stockTransferInHdr.setVoucherNumber(sNos);
			stockTransferInHdr.setAcceptedDate(Date.valueOf(dpIntentDate.getValue()));
			stockTransferInHdr.setStatusAcceptStock("COMPLETED");
			stockTransferInHdr.setBranchCode(SystemSetting.systemBranch);
			stockTransferInHdr.setVoucherNumber(sNo);
			/*
			 * Update accept Stock
			 */
			//{companymstid}/stocktransferinhdr/acceptstock
			 RestCaller.saveAcceptStock(stockTransferInHdr);
			notifyMessage(5, "Detials Saved Successfully!!!");
			txtFromBranch.clear();
			txtintent.clear();
			txtVoucherIn.clear();
		
			stockTransferDtlTable.clear();
			stockTransferDtlTable1.clear();
			stockTransferInHdr = null;
			stockTransferInDtl = null;
			tblStockTrasfer.getItems().clear();
		//	txtVoucherNumber.clear();
		}


		

}
else {
	notifyMessage(5, "fetch data!!!");
}

	}


	
    
    
    
}