package com.maple.mapleclient.controllers;

import java.math.BigDecimal;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.SalesOrderDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class SaleOrderPendingReportCtl {
	
	String taskid;
	String processInstanceId;
	
	private ObservableList<SalesOrderTransHdr> SalesOrderList = FXCollections.observableArrayList();
	private ObservableList<SalesOrderDtl> SalesOrderDtlList = FXCollections.observableArrayList();
 
	SalesOrderTransHdr salesOrderTransHdr = null;
    @FXML
    private Button btnPrintInvoice;
    @FXML
    private DatePicker dpDeliveryDate;
    @FXML
    private Button btnGenerate;
    @FXML
    private DatePicker dpToDate;
    @FXML
    private TableView<SalesOrderTransHdr> tblOrderHdr;

    @FXML
    private TableColumn<SalesOrderTransHdr, String> clVoucherNumber;

    @FXML
    private TableColumn<SalesOrderTransHdr, String> clCustomerName;

    @FXML
    private TableColumn<SalesOrderTransHdr, String> clDueDate;

    @FXML
    private TableColumn<SalesOrderTransHdr, String> clDueTime;

    @FXML
    private TableColumn<SalesOrderTransHdr, Number> clInvoiceAmt;

    @FXML
    private TableColumn<SalesOrderTransHdr, String> clDeliveryMode;

    @FXML
    private TableColumn<SalesOrderTransHdr, String> clTargetBranch;

    @FXML
    private TableColumn<SalesOrderTransHdr, String> clMessage;

    @FXML
    private TableView<SalesOrderDtl> tbOrderDtl;

    @FXML
    private TableColumn<SalesOrderDtl, String> clItemName;

    @FXML
    private TableColumn<SalesOrderDtl, String> clQty;

    @FXML
    private TableColumn<SalesOrderDtl, String> clUnit;

    @FXML
    private TableColumn<SalesOrderDtl, String> clRate;
    
    @FXML
    private TableColumn<SalesOrderDtl, String> clMrp;
    @FXML
    private TableColumn<SalesOrderDtl, Number> clAmount;


    @FXML
    private TableColumn<SalesOrderDtl, String> clItemMessage;

    
    @FXML
    private Button btnPrint;

    @FXML
    void actionPrint(ActionEvent event) {
    	LocalDate dpDate1 = dpDeliveryDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 
		 LocalDate dpDate2 = dpToDate.getValue();
			java.util.Date uDate1  = SystemSetting.localToUtilDate(dpDate2);
			 String  strDate1 = SystemSetting.UtilDateToString(uDate1, "yyyy-MM-dd");
		 try {
				JasperPdfReportService.SaleOrderPendingReport(SystemSetting.systemBranch, strDate,strDate1);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }

    @FXML
	private void initialize() {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpDeliveryDate = SystemSetting.datePickerFormat(dpDeliveryDate, "dd/MMM/yyyy");
    tblOrderHdr.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
		if (newSelection != null) {
			System.out.println("getSelectionModel");
			if (null != newSelection.getId()) {

				salesOrderTransHdr = new SalesOrderTransHdr();
				salesOrderTransHdr.setVoucherNumber(newSelection.getVoucherNumber());
				salesOrderTransHdr.setVoucherDate(newSelection.getVoucherDate());
				ResponseEntity<List<SalesOrderDtl>> getSaleOrderDtl = RestCaller.getsaleOrderDtlByHdrId(newSelection.getId());
				SalesOrderDtlList = FXCollections.observableArrayList(getSaleOrderDtl.getBody());
				fillDtlTable();
			}
		}
	});
    }
    @FXML
    void actionGenerate(ActionEvent event) {

    	LocalDate dpDate1 = dpDeliveryDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 LocalDate dpDate2 = dpToDate.getValue();
			java.util.Date uDate1  = SystemSetting.localToUtilDate(dpDate2);
			 String  strDate1 = SystemSetting.UtilDateToString(uDate1, "yyyy-MM-dd"); 
	
		 ResponseEntity<List<SalesOrderTransHdr>> getPendingSaleOrder = RestCaller.getPendingSaleOrderTransHdr(strDate,strDate1,SystemSetting.systemBranch);
		 SalesOrderList = FXCollections.observableArrayList(getPendingSaleOrder.getBody());
		 fillTable();
    }
    
    private void fillDtlTable()
    {
    	tbOrderDtl.setItems(SalesOrderDtlList);
    	for(SalesOrderDtl saleOrderDtl :SalesOrderDtlList)
    	{
    		BigDecimal BIgRate = new BigDecimal(saleOrderDtl.getRate());
    		BIgRate = BIgRate.setScale(2, BIgRate.ROUND_HALF_EVEN);
    		saleOrderDtl.setRate(BIgRate.doubleValue());
    		
    		BigDecimal BIgMrp = new BigDecimal(saleOrderDtl.getMrp());
    		BIgMrp = BIgMrp.setScale(2, BIgRate.ROUND_HALF_EVEN);
    		saleOrderDtl.setMrp(BIgMrp.doubleValue());
    		
    		BigDecimal BIgAmount = new BigDecimal(saleOrderDtl.getAmount());
    		BIgAmount = BIgAmount.setScale(2, BIgAmount.ROUND_HALF_EVEN);
    		saleOrderDtl.setAmount(BIgAmount.doubleValue());
    	}
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());

		clItemMessage.setCellValueFactory(cellData -> cellData.getValue().getOrderMsgProperty());



    }
    private void fillTable()
    {
    	tblOrderHdr.setItems(SalesOrderList);
    	for(SalesOrderTransHdr saleOrder:SalesOrderList)
    	{
    		saleOrder.setLocalCustomerName(saleOrder.getLocalCustomerId().getLocalcustomerName());
    		
    	}
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
		clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getLocalCustomerNameProperty());
		clDeliveryMode.setCellValueFactory(cellData -> cellData.getValue().getdeliveryModeProperty());
		clDueDate.setCellValueFactory(cellData -> cellData.getValue().getorderDueDateProperty());
		clDueTime.setCellValueFactory(cellData -> cellData.getValue().getOrderDueTimeProperty());
		clInvoiceAmt.setCellValueFactory(cellData -> cellData.getValue().getInvoiceAmtProperty());
		clMessage.setCellValueFactory(cellData -> cellData.getValue().getorderMessageProperty());
		clTargetBranch.setCellValueFactory(cellData -> cellData.getValue().gettargetBranchProperty());


    }
    @FXML
    void actionPrintInvoice(ActionEvent event) {
    	Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String vdate = formatter.format(salesOrderTransHdr.getVoucherDate());

		
    	
    	try {
			JasperPdfReportService.SaleOrderReport(salesOrderTransHdr.getVoucherNumber(), vdate);
		} catch (JRException e) {
			e.printStackTrace();
		}
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }

}
