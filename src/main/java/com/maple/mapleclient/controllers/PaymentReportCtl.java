package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.functors.CatchAndRethrowClosure;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportPaymentReportToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import net.sf.jasperreports.engine.JRException;

public class PaymentReportCtl {
	String taskid;
	String processInstanceId;
	ExportPaymentReportToExcel exportPaymentReportToExcel=new ExportPaymentReportToExcel();
	
	private ObservableList<PaymentHdr> paymentList = FXCollections.observableArrayList();
	private ObservableList<PaymentDtl> paymenteDtlList = FXCollections.observableArrayList();
	
	@FXML
	    private DatePicker dpFromDate;

	    @FXML
	    private TableView<PaymentHdr> tbReport;

	    @FXML
	    private TableColumn<PaymentHdr, String> clVoucherDate;

	    @FXML
	    private TableColumn<PaymentHdr, String> clVoucherNumber;


	    @FXML
	    private TableColumn<PaymentHdr, String> clumnVoucherType;

	    @FXML
	    private Button btnShow;

	    @FXML
	    private TableView<PaymentDtl> tbreports;
	

	    @FXML
	    private TableColumn<PaymentDtl, String> clAccountId;

	    @FXML
	    private TableColumn<PaymentDtl, String> clModeOfPayment;

	    @FXML
	    private TableColumn<PaymentDtl, Number> clAmount;

	    @FXML
	    private TableColumn<PaymentDtl, String> clInstrumentNumber;

	    @FXML
	    private TableColumn<PaymentDtl, String> clRemark;

	    @FXML
	    private TableColumn<PaymentDtl, String> clBankAccountNumber;
	
	    
	    @FXML
	    private ComboBox<String> branchselect;

	    @FXML
	    private Button ok;

	    @FXML
	    private Button btnPrint;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private Button btnExcelExport;
	
	    
	    
	    @FXML
	    void exportToExcel(ActionEvent event) {

	    	
	     	if(null == branchselect.getValue())
	    	{
	    		notifyMessage(5, "Please select branch", false);
	    		return;
	    	} 
	    	
	    	if(null == dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    	} 
	    	
	    	if(null == dpToDate.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	} 
	    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date toDate = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
	    	
			ArrayList paymentImport = new ArrayList();
  			if( branchselect.getSelectionModel().isEmpty())
  			{
  				return;
  			}
  			
  			System.out.println(paymentImport.size()+"receiptReportListSize issssssssssss");
  			paymentImport = RestCaller.exportPaymentReport(branchselect.getSelectionModel().getSelectedItem(),startDate, endDate);	
  			exportPaymentReportToExcel.exportToExcel("PaymentReport"+branchselect.getSelectionModel().getSelectedItem()+startDate+endDate+".xls", paymentImport);
			
			
	    	
	    }

	    @FXML
	    void onClickOk(ActionEvent event) {

	    }

	    @FXML
	    void reportPrint(ActionEvent event) {

	    	
	     	
	     	if(null == branchselect.getValue())
	    	{
	    		notifyMessage(5, "Please select branch", false);
	    		return;
	    	} 
	    	
	    	if(null == dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    	} 
	    	
	    	if(null == dpToDate.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	} 
	    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date toDate = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
	    	
			ArrayList paymentImport = new ArrayList();
  			if( branchselect.getSelectionModel().isEmpty())
  			{
  				return;
  			}
  			
  		  try {
			  JasperPdfReportService.paymentReportPrintinge(
		  branchselect.getSelectionModel().getSelectedItem(),startDate, endDate);
			  }
		  catch (JRException e) { // TODO Auto-generated catch block
		  e.printStackTrace(); }
	    
	    }

	    @FXML
	    void show(ActionEvent event) {
	    	tbReport.getItems().clear();
	    	tbreports.getItems().clear();
	    	
	    	if(null == branchselect.getValue())
	    	{
	    		notifyMessage(5, "Please select branch", false);
	    		return;
	    	} 
	    	
	    	if(null == dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    	} 
	    	
	    	if(null == dpToDate.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	} 
	    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date toDate = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
	    	ResponseEntity<List<PaymentHdr>> paymentHdrRep = RestCaller.getAllPaymentHdrByDate( startDate, endDate,branchselect.getValue());
	    	paymentList = FXCollections.observableArrayList(paymentHdrRep.getBody());
	    	

	    	FillTable();
	    	
	    }
	    
	    private void setBranches() {
			
	 			ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
	 			List<BranchMst> branchMstList = new ArrayList<BranchMst>();
	 			branchMstList = branchMstRep.getBody();
	 			
	 			for(BranchMst branchMst : branchMstList)
	 			{
	 				branchselect.getItems().add(branchMst.getBranchCode());
	 			
	 				
	 			}
	 			 
	 		}
	    
	    
	    private void FillTable()
		{
			tbReport.setItems(paymentList);
		
		
		
			clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
			clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		//	clumnTransDate.setCellValueFactory(cellData -> cellData.getValue().get);
			clumnVoucherType.setCellValueFactory(cellData -> cellData.getValue().getVoucherTypeProperty());
			
		}
	@FXML
 	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		setBranches();

  	  tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	    		if (newSelection != null) {
	    			System.out.println("getSelectionModel");
	    			if (null != newSelection.getId()) {
	    				System.out.println("getSelectionModel--getVoucherNumber");
	    				 String voucher=newSelection.getVoucherNumber();
	    				System.out.println(voucher);
	    				String voucherNumber=newSelection.getVoucherNumber();
	    				String paymentid=newSelection.getId();
	    				
	    				System.out.println("........" + voucherNumber);
	    				String branchName=branchselect.getValue();
	    				ResponseEntity<List<PaymentDtl>> paymentDtlList = RestCaller.getPaymentDtlReportDtl(paymentid);
	    				paymenteDtlList = FXCollections.observableArrayList(paymentDtlList.getBody());
	    				tbreports.setItems(paymenteDtlList);
	    				System.out.print(paymenteDtlList.size()+"receipt detail list size issssssss");
	    				
	    				System.out.print("after filltable");
	    				
	    				FillTables();
	    			}
	    			
	    		}
	    	});
		
	}
	
	

	  private void FillTables() {
	  
	  System.out.print("inside fill table isssssssssssssssssssssss");
	  clAccountId.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());
		clModeOfPayment.setCellValueFactory(cellData -> cellData.getValue().getModeOfPaymentProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clInstrumentNumber.setCellValueFactory(cellData -> cellData.getValue().getInstrumentNumberProperty());
		clBankAccountNumber.setCellValueFactory(cellData -> cellData.getValue().getBankAccountNumberProperty());
		clRemark.setCellValueFactory(cellData -> cellData.getValue().getCreditAccountIdProperty());
		
	  
	  
	  
	  }
	

    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }
	
}
