package com.maple.mapleclient.controllers;
import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

	public class DeliveryBoySaleOrderReportCtl {
		
		String taskid;
		String processInstanceId;

	    @FXML
	    private DatePicker dpDate;

	    @FXML
	    private Button btnGenerateReport;

	    @FXML
	    private ComboBox<String> cmbDeliveryBoy;

	    @FXML
	    void FocusOnCategory(KeyEvent event) {

	    }

	    @FXML
	    void GenerateReport(ActionEvent event) {
	    	if(null == dpDate.getValue())
	    	{
	    		notifyMessage(5, "Please select  date", false);
	    		return;
	    		
	    	}
	    	
	    	
	    	Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
	    	String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
	    	ResponseEntity<DeliveryBoyMst> deliveryBoyMst = RestCaller.getDelivaryBoyByName(cmbDeliveryBoy.getValue());
			
			try {
				JasperPdfReportService.DeliveryBoyReport(date, deliveryBoyMst.getBody().getId());
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    }

	    @FXML
	    void focusEndDate(KeyEvent event) {

	    }

	    @FXML
	    void setDeliveryBoy(MouseEvent event) {
	    	System.out.println("---------------setDeliveryBoy--------------");
			DeliveryBoy();

	    }

	    private void DeliveryBoy() {
			
			cmbDeliveryBoy.getItems().clear();

			ResponseEntity<List<DeliveryBoyMst>> respdelivery = RestCaller.getDeliveryBoyMstByStatus();
			for (DeliveryBoyMst deliverymst : respdelivery.getBody()) {
				cmbDeliveryBoy.getItems().add(deliverymst.getDeliveryBoyName());
			}

		}

		@FXML
		private void initialize() {
			dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
			DeliveryBoy();
			
		}

	
    public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

	}	
	


