package com.maple.mapleclient.controllers;

import com.maple.maple.util.SystemSetting;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.BalanceSheetAssetReport;
import com.maple.report.entity.BalanceSheetLiabilityReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class BalanceSheetAssetAndLiabilityReportCtl {
	
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	BalanceSheetAssetReport balanceSheetConfigAssetDtl=null;
	BalanceSheetLiabilityReport balanceSheetConfigLiabilityDtl=null;
	private ObservableList<BalanceSheetAssetReport> balanceSheetConfigAssetList = FXCollections.observableArrayList(); 	
	private ObservableList<BalanceSheetLiabilityReport> balanceSheetConfigLiabilityList = FXCollections.observableArrayList(); 	

		@FXML
	    private DatePicker dpDate;

	    @FXML
	    private Button btnGenerateReport;

	    @FXML
	    private Button btnPrintReport;

	    @FXML
	    private TableView<BalanceSheetAssetReport> tblAssetReport;

	    @FXML
	    private TableColumn<BalanceSheetAssetReport, String> clmnAssetAccountName;

	    @FXML
	    private TableColumn<BalanceSheetAssetReport, Number> clmnAsset;

	    @FXML
	    private TextField txtAssetTotal;

	    @FXML
	    private TextField txtLiabilityTotal;

	    @FXML
	    private TableView<BalanceSheetLiabilityReport> tblLiabilityReport;

	    @FXML
	    private TableColumn<BalanceSheetLiabilityReport, String> clmnLiabilityAccountName;

	    @FXML
	    private TableColumn<BalanceSheetLiabilityReport, Number> clmnLiability;

	    
	    @FXML
	   	private void initialize() {
	    	
	    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");

	    	eventBus.register(this);
	    	
	    	tblLiabilityReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {

						balanceSheetConfigLiabilityDtl = new BalanceSheetLiabilityReport();
						balanceSheetConfigLiabilityDtl.setId(newSelection.getId());
						
					}
				}
	    	});
	    	
	    	tblAssetReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {

						balanceSheetConfigAssetDtl = new BalanceSheetAssetReport();
						balanceSheetConfigAssetDtl.setId(newSelection.getId());
						
					}
				}
	    	});
	    }
	    
	    @FXML
	    void generateReport(ActionEvent event) {

	    	
	    	if(null == dpDate.getValue())
	    	{
	    		notifyMessage(5, "Please select date", false);
				return;
	    	}
	    	
	    	
    		Date fdate = SystemSetting.localToUtilDate(dpDate.getValue());
    		String currentDate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
    		
    		ResponseEntity<List<BalanceSheetAssetReport>> balanceSheetConfigAsset=RestCaller.getBalanceSheetConfigAsset(currentDate);
    		balanceSheetConfigAssetList = FXCollections.observableArrayList(balanceSheetConfigAsset.getBody());
    		
    		
    		
    		if (balanceSheetConfigAssetList.size() > 0) {
    			
    			
    			fillAssetTable();
    			dpDate.getEditor().clear();;
    			
    		}
    		
    		ResponseEntity<List<BalanceSheetLiabilityReport>> balanceSheetConfigLiability=RestCaller.getBalanceSheetConfigLiability(currentDate);
    		balanceSheetConfigLiabilityList = FXCollections.observableArrayList(balanceSheetConfigLiability.getBody());
    		
    		
    		
    		if (balanceSheetConfigLiabilityList.size() > 0) {
    			
    			
    			fillLiabilityTable();
    			dpDate.getEditor().clear();;
    			
    		}
			
	    }

	    @FXML
	    void printReport(ActionEvent event) {
	    	
	    	Date fdate = SystemSetting.localToUtilDate(dpDate.getValue());
			String currentDate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
			  try { 
				  JasperPdfReportService.BalanceSheetConfigReport(currentDate); 
			  } catch (JRException e) { // TODO Auto-gesnerated catch block
			  e.printStackTrace(); }

	    }

	    
	    private void notifyMessage(int i, String string, boolean b) {
			
	    	Image img;
			if (b) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

	    	
		}
	    
	    
	    private void fillAssetTable() {
	    	tblAssetReport.setItems(balanceSheetConfigAssetList);
			clmnAsset.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
			clmnAssetAccountName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
			
		}
	    
	    private void fillLiabilityTable() {
	    	tblLiabilityReport.setItems(balanceSheetConfigLiabilityList);
			clmnLiability.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
			clmnLiabilityAccountName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
				
		}

}
