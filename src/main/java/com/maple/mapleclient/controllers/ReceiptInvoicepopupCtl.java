package com.maple.mapleclient.controllers;

import java.awt.event.KeyEvent;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.events.ReceiptInvoicePopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ReceiptInvoicepopupCtl {
	
	String taskid;
	String processInstanceId;

	private EventBus eventBus = EventBusFactory.getEventBus();
	ReceiptInvoicePopupEvent receiptInvoicePopupEvent;

	private ObservableList<AccountReceivable> InvoiceList = FXCollections.observableArrayList();

	@FXML
	private TextField txtOwnAcc;
	@FXML
	private TableView<AccountReceivable> tbInvPopUp;

	@FXML
	private TableColumn<AccountReceivable, String> clInvNo;

	@FXML
	private TableColumn<AccountReceivable, Number> clDueAmt;

	@FXML
	private TableColumn<AccountReceivable, Number> clPaidAmt;

	@FXML
	private TableColumn<AccountReceivable, Number> clBalanceAmt;
	@FXML
	private TableColumn<AccountReceivable, String> clBalanceAmt1;

	@FXML
	private Button btnOk;

	@FXML
	private Button btnCancel;

	@FXML
	private void initialize() {

		receiptInvoicePopupEvent = new ReceiptInvoicePopupEvent();
		eventBus.register(this);
		// LoadAccountReceivable();
		tbInvPopUp.setItems(InvoiceList);

		clBalanceAmt.setCellValueFactory(cellData -> cellData.getValue().getbalanceAmountProperty());
		clDueAmt.setCellValueFactory(cellData -> cellData.getValue().getdueAmountProperty());
		clInvNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
		clPaidAmt.setCellValueFactory(cellData -> cellData.getValue().getpaidAmountProperty());
		clBalanceAmt1.setCellValueFactory(cellData -> cellData.getValue().getvoucher_DateProperty());
		tbInvPopUp.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId() && newSelection.getVoucherNumber().length() > 0) {
					receiptInvoicePopupEvent.setId(newSelection.getId());
					receiptInvoicePopupEvent.setBalanceAmount(newSelection.getBalanceAmount());
					receiptInvoicePopupEvent.setDueAmount(newSelection.getDueAmount());
					receiptInvoicePopupEvent.setInvoiceNumber(newSelection.getVoucherNumber());
					receiptInvoicePopupEvent.setPaidAmount(newSelection.getPaidAmount());
					receiptInvoicePopupEvent.setInvoiceDate(newSelection.getVoucherDate());
					// eventBus.post(receiptInvoicePopupEvent);

				}
			}
		});
	}

	@FXML
	void Cancel(ActionEvent event) {
		Stage stage = (Stage) btnCancel.getScene().getWindow();
		stage.close();
	}

	@FXML
	void Ok(ActionEvent event) {
		Stage stage = (Stage) btnOk.getScene().getWindow();
		eventBus.post(receiptInvoicePopupEvent);
		stage.close();
	}

	@FXML
	void onEnterOk(javafx.scene.input.KeyEvent event) {
		Stage stage = (Stage) btnOk.getScene().getWindow();
		eventBus.post(receiptInvoicePopupEvent);
		stage.close();
	}

	public void LoadAccountReceivable(String custid) {
		
		double ownAccAmt = 0.0;
		ownAccAmt = RestCaller.getOwnAccountByCustId(custid);

		txtOwnAcc.setText(Double.toString(ownAccAmt));

		ArrayList stockin = new ArrayList();
		InvoiceList.clear();
		stockin = RestCaller.getAccountreceivable(custid);
		Iterator itr = stockin.iterator();
		while (itr.hasNext()) {
			LinkedHashMap element = (LinkedHashMap) itr.next();
			Object id = (String) element.get("id");
			Object voucherNumber = (String) element.get("voucherNumber");
			Object accountId = (String) element.get("accountId");
			Object dueAmount = (Double) element.get("dueAmount");
			if (null == dueAmount) {
				dueAmount = 0.0;
			}
			Object paidAmount = (Double) element.get("paidAmount");

			if (null == paidAmount) {
				paidAmount = 0.0;
			}
			Object invDate = (String) (element.get("voucherDate"));
			if (null != id) {
				AccountReceivable ack = new AccountReceivable();
				ack.setId((String) id);
				ack.setVoucherNumber((String) voucherNumber);
				ack.setPaidAmount((Double) paidAmount);
				ack.setDueAmount((Double) dueAmount);
				ack.setBalanceAmount((Double) dueAmount - (Double) paidAmount);
				// LocalDate ldate = SystemSetting.str(Date)invDate);
				Date date1 = SystemSetting.StringToUtilDate((String) invDate, "yyyy-MM-dd");
				ack.setVoucherDate(date1);
				InvoiceList.add(ack);
//    			 tbInvPopUp.setItems(InvoiceList);
			}
		}
		return;

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }
}