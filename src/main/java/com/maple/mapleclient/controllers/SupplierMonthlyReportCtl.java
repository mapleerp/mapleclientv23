package com.maple.mapleclient.controllers;
import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.events.AccountHeadsPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;


	public class SupplierMonthlyReportCtl {
		
		String taskid;
		String processInstanceId;

		
		String supplierId = null;

		EventBus eventBus = EventBusFactory.getEventBus();

	    @FXML
	    private DatePicker dpStartDate;

	    @FXML
	    private DatePicker dpEndDate;

	    @FXML
	    private Button btnGenerateReport;


	    @FXML
	    private TextField txtSupplier;
	    
	    @FXML
	    void SupplierPopup(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
		    	supplierpopup();
		    	}
	    }
	    private void supplierpopup() {
	    	try {
				System.out.println("inside the popup");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPartyPopUpCtl.fxml"));
				Parent root = loader.load();
				// PopupCtl popupctl = loader.getController();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				btnGenerateReport.requestFocus();
			} catch (Exception e) {
				e.printStackTrace();
			}

			
		}
	    
	    @FXML
		private void initialize() {
	    	dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
	    	dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
	    	eventBus.register(this);
	    }

	    @FXML
	    void GenerateReport(ActionEvent event) {

	    	 generateReport();

	    }

	    @FXML
	    void focusButton(KeyEvent event) {

	    	if (event.getCode() == KeyCode.ENTER) {
	    		if(null != dpEndDate.getValue())
	    		{
	    			generateReport();
	    		}
			}
	    }

	    @FXML
	    void focusEndDate(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		dpEndDate.requestFocus();
			}

	    
	    }

	    

	  
			
	    private void generateReport() {
	    	
	    	if(null == dpStartDate.getValue())
	    	{
	    		notifyMessage(5, "please select start date", false);
	    		return;
	    		
	    	} else if (null == dpEndDate) {
	    		
	    		notifyMessage(5, "please select end date", false);
	    		return;
			} else if (txtSupplier.getText().trim().isEmpty()) {
				
				notifyMessage(5, "please select supplier/customer", false);
				return;
			}else {
				
				String endDate = "";
				Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
				String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
				
				if(null != dpEndDate.getValue())
				{
				Date edate = SystemSetting.localToUtilDate(dpEndDate.getValue());
				endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
				}
				
				
			
					ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(txtSupplier.getText());
					
					try {
						JasperPdfReportService.MonthlySummaryReport(strtDate,endDate,accountHeads.getBody().getId());
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
	    }
	    public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
	    
		@Subscribe
		public void popuplistner(AccountHeadsPopupEvent accountHeadsPopupEvent) {

			System.out.println("-------------popuplistner-------------");
			Stage stage = (Stage) btnGenerateReport.getScene().getWindow();
			if (stage.isShowing()) {

				txtSupplier.setText(accountHeadsPopupEvent.getPartyName());
				supplierId = accountHeadsPopupEvent.getId();
			}

		}
		 @Subscribe
		   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		   		//Stage stage = (Stage) btnClear.getScene().getWindow();
		   		//if (stage.isShowing()) {
		   			taskid = taskWindowDataEvent.getId();
		   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
		   			
		   		 
		   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
		   			System.out.println("Business Process ID = " + hdrId);
		   			
		   			 PageReload(hdrId);
		   		}


		   	private void PageReload(String hdrId) {

		   	}
	

	}

	
	

