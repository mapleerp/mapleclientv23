package com.maple.mapleclient.controllers;

import java.math.BigDecimal;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.javapos.print.ChequePrint;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountPayable;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BankMst;

import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.PaymentInvoiceDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptInvoiceDtl;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.Summary;

import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.PaymentInvPopupEvent;
import com.maple.mapleclient.events.PaymentPopUpEvent;
import com.maple.mapleclient.events.ReceiptInvoicePopupEvent;
import com.maple.mapleclient.events.SupplierPaymentEvent;

import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.PaymentPopUpEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;

import javafx.scene.shape.Circle;
import javafx.stage.Modality;

import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

@Controller
public class SupplierPaymentController {
	

	String processInstanceId;

	Integer rowCounter = 1;
	String taskid = null;

	double totalAmt1 = 0.0;
	String taskId = "";
	EventBus eventBus = EventBusFactory.getEventBus();
	PaymentInvoiceDtl paymentInvDtl = null;
	double totalamount = 0.0;
	private ObservableList<PaymentInvoiceDtl> paymentInvDtlListTable = FXCollections.observableArrayList();

	private ObservableList<PaymentDtl> paymentList = FXCollections.observableArrayList();
	private ObservableList<OwnAccount> ownAccList = FXCollections.observableArrayList();
	OwnAccount ownAccount = null;
	PaymentHdr paymenthdr = null;
	PaymentDtl payment = null;
//	Supplier supplier = null;
	AccountHeads accountHeads = null;
	PaymentDtl paymentDelete = null;
	StringProperty acountProperty = new SimpleStringProperty("");
	// private ObservableList<PaymentDtl> paymentList =
	// FXCollections.observableArrayList();

	AccountPayable accountPayable = null;
	@FXML
	private TextField txtinvoiceNumber;

	@FXML
	private TextField txtBalance;
	@FXML
	private Button paymentadd;

	@FXML
	private Button paymentdelete;

	@FXML
	private Button paymentsubmit;

	@FXML
	private TextField paymentaccount;

	@FXML
	private TextField paymentremark;

	@FXML
	private TextField paymentamount;

	@FXML
	private TextField paymentinstrumentnumber;

	@FXML
	private TextField paymentvouchernumber;

	@FXML
	private ComboBox<String> paymentbankaccount;

	@FXML
	private ComboBox<String> paymentmode;

	@FXML
	private DatePicker paymentinstrumentdate;

	@FXML
	private TableView<PaymentDtl> paymentTable;

	@FXML
	private TableView<OwnAccount> tbOwnAccount;

	@FXML
	private TableColumn<OwnAccount, Number> clOwnAccount;
	@FXML
	private TextField txtPaidAmount;

	@FXML
	private TextField txtTotal;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnDelete;

	@FXML
	private TableColumn<PaymentDtl, String> clAccount;

	@FXML
	private TableColumn<PaymentDtl, String> modeOfPay;

	@FXML
	private TableColumn<PaymentDtl, String> remark;

	@FXML
	private TableColumn<PaymentDtl, LocalDate> transdate;

	@FXML
	private TableColumn<PaymentDtl, Number> amount;

	@FXML
	private TableColumn<PaymentDtl, String> instrument;

	@FXML
	private TableColumn<PaymentDtl, String> voucher;

	@FXML
	private TableColumn<PaymentDtl, String> bankaccount;

	@FXML
	private TableColumn<PaymentDtl, LocalDate> instrumentdate;

	@FXML
	private DatePicker paymenttransdate;

	@FXML
	private TableView<PaymentInvoiceDtl> tbPayableInvoiceDtl;

	@FXML
	private TableColumn<PaymentInvoiceDtl, LocalDate> clVoucherDate;

	@FXML
	private TableColumn<PaymentInvoiceDtl, String> clInvoiceNumber;

	@FXML
	private TableColumn<PaymentInvoiceDtl, Number> clPaidAmount;

	@FXML
	private TextField txtGrandTotal;

	@FXML
	private TextField txtDate;

	@FXML
	void PaymentModeKeyPressesd(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			paymentremark.requestFocus();
		}

	}

	@FXML
	void RemarkKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtinvoiceNumber.requestFocus();
		}

	}

	@FXML
	void InvoiceKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			btnAdd.requestFocus();
		}

	}

	@FXML
	void TranDateKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			paymentamount.requestFocus();
		}

	}

	@FXML
	void AmountKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			paymentinstrumentnumber.requestFocus();
		}

	}

	@FXML
	void InstrumentNoOnKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			paymentvouchernumber.requestFocus();
		}

	}

	@FXML
	void VoNOnKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			paymentbankaccount.requestFocus();
		}

	}

	@FXML
	void BankKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			// paymentinstrumentdate.requestFocus();
		}

	}

	@FXML

	private void initialize() {

		
		paymentinstrumentdate = SystemSetting.datePickerFormat(paymentinstrumentdate, "dd/MMM/yyyy");
		paymenttransdate = SystemSetting.datePickerFormat(paymenttransdate, "dd/MMM/yyyy");
		// paymentInvDtl = new PaymentInvoiceDtl();

		/*
		 * Make amount field to accept only numbers
		 */

		txtDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd"));

		paymentamount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					paymentamount.setText(oldValue);
				}
			}
		});
		paymentamount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					paymentamount.setText(oldValue);
				}
			}
		});
		paymentinstrumentnumber.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					paymentinstrumentnumber.setText(oldValue);
				}
			}
		});

		// -----------------------version 1.9 surya

//		ResponseEntity<AccountHeads> accountHead = RestCaller
//				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
//
//		paymentmode.getItems().add(accountHead.getBody().getAccountName());
//		paymentmode.getItems().add("TRASNFER");
//		paymentmode.getItems().add("CHEQUE");
//		paymentmode.getItems().add("DD");
//		paymentmode.getItems().add("NEFT");
//		paymentmode.getItems().add("RTGS");
		
		ResponseEntity<List<ReceiptModeMst>> receiptModeMstListResp = RestCaller.getAllReceiptMode();
		List<ReceiptModeMst> receiptModeMstList = receiptModeMstListResp.getBody();
		
		for(ReceiptModeMst receiptModeMst : receiptModeMstList)
		{
			if(receiptModeMst.getCreditCardStatus().equalsIgnoreCase("NO"))
			{
				paymentmode.getItems().add(receiptModeMst.getReceiptMode());
			}
		}

		// -----------------------version 1.9 surya end

		eventBus.register(this);
		paymentTable.setItems(paymentList);

		EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				System.out.println("---MouseEvent---");
				showPopup();
			}
		};
		paymentaccount.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
//		ArrayList bankList = new ArrayList();
//		bankList = RestCaller.getAllBankmsts();
//		Iterator itr1 = bankList.iterator();
//		while (itr1.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr1.next();
//			Object bankName = lm.get("bankName");
//			if (bankName != null) {
//				paymentbankaccount.getItems().add((String) bankName);
//						    //accountHeads.getAccountName);
//			}
//		}

		ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid = lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				paymentbankaccount.getItems().add((String) bankName);

				// accountHeads.getAccountName);
			}
		}
		paymentmode.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.matches(SystemSetting.systemBranch + "-CASH")) {
					paymentbankaccount.setDisable(true);
					paymentinstrumentnumber.setDisable(true);
					paymentinstrumentdate.setDisable(true);
				} else {
					paymentbankaccount.setDisable(false);
					paymentinstrumentnumber.setDisable(false);
					paymentinstrumentdate.setDisable(false);
				}
			}
		});

		paymentmode.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.equals(SystemSetting.systemBranch + "-CASH")) {

//					ArrayList bankList = new ArrayList();
//					bankList = RestCaller.getAllBankAccounts();
//					Iterator itr1 = bankList.iterator();
//					while (itr1.hasNext()) {
//						LinkedHashMap lm = (LinkedHashMap) itr1.next();
//						Object bankName = lm.get("accountName");
//						Object bankid= lm.get("id");
//						String bankId = (String) bankid;
//						if (bankName != null) {
//							paymentbankaccount.getItems().add((String) bankName);
//							
//									    //accountHeads.getAccountName);
//						}
//					}			
				}
			}
		});
		paymentTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					paymentDelete = new PaymentDtl();
					paymentDelete.setId(newSelection.getId());
				}
			}
		});
		tbPayableInvoiceDtl.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						System.out.println("getSelectionModel");
						if (null != newSelection.getId()) {

							paymentInvDtl = new PaymentInvoiceDtl();
							paymentInvDtl.setId(newSelection.getId());
						}
					}
				});

		tbOwnAccount.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					ownAccount = new OwnAccount();
					ownAccount.setId(newSelection.getId());
				}
			}
		});
	}

	@FXML
	void modeOfPaymentOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			paymentremark.requestFocus();
		}
	}

	@FXML
	void remarkOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtinvoiceNumber.requestFocus();
		}
	}

	@FXML
	void InvoiceOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnAdd.requestFocus();
		}
	}

	@FXML
	void transDateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			paymentamount.requestFocus();
		}
	}

	@FXML
	void amountOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (paymentinstrumentnumber.isDisable())
				paymentadd.requestFocus();

			else
				paymentinstrumentnumber.requestFocus();
		}
	}

	@FXML
	void instrumentNoOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			paymentbankaccount.requestFocus();
		}
	}

	@FXML
	void bankOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			// paymentinstrumentdate.requestFocus();
		}
	}

	@FXML
	void insDateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			paymentadd.requestFocus();
		}
	}

	@FXML
	void addOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			AddItem();
		}
	}

	@FXML
	void add(ActionEvent event) {

		AddItem();

	}

	private void AddItem() {
		if (paymentmode.getSelectionModel().isEmpty()) {
			notifyMessage(3, "Select Payment Mode");
			paymentmode.requestFocus();
			return;
		}
		if (paymentamount.getText().trim().isEmpty()) {
			notifyMessage(3, "Type Amount");
			paymentamount.requestFocus();
			return;
		}
		if (paymentaccount.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Supplier");
			paymentaccount.requestFocus();
			return;
		}
//	if(null == paymenttransdate.getValue())
//	{
//		notifyMessage(3,"Select Trans Date");
//		paymenttransdate.requestFocus();
//		return;
//	}
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}

			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}
		Date voucherDate = null;

		if (null == paymenthdr) {
			paymenthdr = new PaymentHdr();
			paymenthdr.setBranchCode(SystemSetting.getSystemBranch());
			paymenthdr.setTaskId(taskId);
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
//		
//		LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
//			Date date = Date.valueOf(ldate);
			paymenthdr.setVoucherDate(SystemSetting.StringToUtilDate(sdate, "yyyy-MM-dd"));

			ResponseEntity<PaymentHdr> respentity = RestCaller.savePaymenthdr(paymenthdr);
			paymenthdr = respentity.getBody();
			if (null == paymenthdr) {
				return;
			}
		}
		payment = new PaymentDtl();

		payment.setRemark(paymentremark.getText());

		ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountHeadByName(paymentaccount.getText());
		StringProperty modeOfPayProperty = new SimpleStringProperty(
				paymentmode.getSelectionModel().getSelectedItem().toString());

		String bankAccount = "";
		payment.setModeOfPaymentProperty(modeOfPayProperty);

		payment.setAccountId(accSaved.getBody().getId());
		if (!modeOfPayProperty.get().equalsIgnoreCase(SystemSetting.getSystemBranch() + "-CASH")) {
			if (null == paymentinstrumentdate.getValue()) {
				notifyMessage(3, "Select Instrument Date");
				paymentinstrumentdate.requestFocus();
				return;
			}
			if (paymentbankaccount.getSelectionModel().isEmpty()) {
				notifyMessage(3, "Select Bank");
				paymentbankaccount.requestFocus();
				return;
			}
			if (paymentinstrumentnumber.getText().trim().isEmpty()) {
				notifyMessage(3, "Type Instrument Noumber");
				paymentinstrumentnumber.requestFocus();
				return;
			}

			Date date = Date.valueOf(paymentinstrumentdate.getValue());

			if (null != paymentinstrumentdate.getValue())
				payment.setInstrumentDate(date);
			else
				payment.setInstrumentDate(null);
		}

//		if (bankAccount.trim().length() == 0) {
//			// Set Error and return
//			paymentbankaccount.requestFocus();
//		//	paymenthdr = null;
//			return;
//		}

		// credit account id

		if (paymentmode.getValue().equals(SystemSetting.systemBranch + "-CASH")) {
			ResponseEntity<AccountHeads> creditAccount = RestCaller.getAccountHeadByName(paymentmode.getValue());
			payment.setCreditAccountId(creditAccount.getBody().getId());
		} else {

			ResponseEntity<AccountHeads> creditAccount = RestCaller.getAccountHeadByName(paymentbankaccount.getValue());
			payment.setCreditAccountId(creditAccount.getBody().getId());
		}
		payment.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
		Double amount = Double.parseDouble(paymentamount.getText());
		payment.setAmount(amount);
		payment.setInstrumentNumber(paymentinstrumentnumber.getText());

		if (null != paymentbankaccount.getSelectionModel().getSelectedItem()) {
			bankAccount = paymentbankaccount.getSelectionModel().getSelectedItem().toString();

			paymentbankaccount.setStyle("-fx-background: red;");
		}

		payment.setBankAccountNumber(bankAccount);

		payment.setPaymenthdr(paymenthdr);

		ResponseEntity<PaymentDtl> respentity = RestCaller.savePaymentdtls(payment);
		payment = respentity.getBody();
		payment.setAccount(paymentaccount.getText());

		paymentList.add(payment);
		setTotal();
		FillTable();
		clearfields();

	}

	@FXML
	void actionAdd(ActionEvent event) {

		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}

			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}
		Date voucherDate = null;

		if (paymentaccount.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Account");
			paymentaccount.requestFocus();
			return;
		}
		if (txtinvoiceNumber.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Invoice Number");
			txtinvoiceNumber.requestFocus();
			return;
		}
		if (txtPaidAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "Type Paid Amount");
			txtPaidAmount.requestFocus();
			return;
		}
//	if(null == paymenttransdate.getValue())
//	{
//		notifyMessage(3,"Select Date");
//		paymenttransdate.requestFocus();
//		return;
//	}
		if (null == paymenthdr) {
			paymenthdr = new PaymentHdr();
			paymenthdr.setBranchCode(SystemSetting.getSystemBranch());
			paymenthdr.setTaskId(taskId);
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
//		
//		LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
//			Date date = Date.valueOf(ldate);
			paymenthdr.setVoucherDate(SystemSetting.StringToUtilDate(sdate, "yyyy-MM-dd"));

			ResponseEntity<PaymentHdr> respentity = RestCaller.savePaymenthdr(paymenthdr);
			paymenthdr = respentity.getBody();
			if (null == paymenthdr) {
				return;
			}
		}

		paymentInvDtl = new PaymentInvoiceDtl();
		paymentInvDtl.setPaymentHdr(paymenthdr);
		paymentInvDtl.setAccountHeads(accountHeads);
		paymentInvDtl.setVoucherDate(paymenthdr.getVoucherDate());
		String inVNo = txtinvoiceNumber.getText() == null ? "" : txtinvoiceNumber.getText();
		if (inVNo.trim().length() > 0) {
			paymentInvDtl.setInvoiceNumber(txtinvoiceNumber.getText());

			paymentInvDtl.setPaidAmount(Double.parseDouble(txtPaidAmount.getText()));
			ResponseEntity<PaymentInvoiceDtl> respentityPaymentInvDtl = RestCaller.savePaymentInvDtl(paymentInvDtl);
			paymentInvDtl = respentityPaymentInvDtl.getBody();
			paymentInvDtlListTable.add(paymentInvDtl);

			FillPaymentInvDtls();

		} else {
			ResponseEntity<AccountHeads> supSaved = RestCaller.getAccountHeadsById(payment.getAccountId());

			if (null != supSaved.getBody()) {
				ownAccount = new OwnAccount();
				ownAccount.setAccountId(payment.getAccountId());
				ownAccount.setPaymenthdr(paymenthdr);
				ownAccount.setAccountType("SUPPLIER");
				ownAccount.setCreditAmount(0.0);
				ownAccount.setDebitAmount(Double.valueOf(txtPaidAmount.getText()));
				ownAccount.setOwnAccountStatus("PENDING");
				ownAccount.setRealizedAmount(0.0);
				ownAccount.setVoucherDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
				ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
				ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
				ownAccount = respentity1.getBody();
				ownAccList.add(ownAccount);
				if (!txtTotal.getText().trim().isEmpty()) {
					double total = 0.0;
					total = Double.valueOf(txtTotal.getText()) + respentity1.getBody().getDebitAmount();
					txtTotal.setText(Double.toString(total));
				} else {
					txtTotal.setText(Double.toString(respentity1.getBody().getDebitAmount()));
				}
				fillOwnAcc();
				notifyMessage(6, "Amount will set to Own Account");
			}
		}

		txtinvoiceNumber.clear();
		txtPaidAmount.clear();

	}

	@FXML
	void actionDelete(ActionEvent event) {
		if (null != paymentInvDtl) {
			if (null != paymentInvDtl.getId()) {
				RestCaller.deletePaymentInvoiceDtl(paymentInvDtl.getId());
				tbPayableInvoiceDtl.getItems().clear();
				paymentInvDtlListTable.clear();
				ResponseEntity<List<PaymentInvoiceDtl>> paymentSaved = RestCaller.getPaymentInvDtlById(paymenthdr);
				paymentInvDtlListTable = FXCollections.observableArrayList(paymentSaved.getBody());
				FillPaymentInvDtls();

			}

			else if (null != ownAccount.getId()) {
				RestCaller.deleteOwnAcc(ownAccount.getId());
				notifyMessage(4, "Deleted!!!");
				ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller
						.getOwnAccountDtlsByReceiptHdrId(paymenthdr.getId());
				ownAccList = FXCollections.observableArrayList(ownAccSaved.getBody());

				fillOwnAcc();
				double total = 0.0, totalBalance = 0.0;
				total = RestCaller.getSummaryOwnAccByPaymentHdr(paymenthdr.getId());
				if (null != txtTotal) {
					double invTotal = RestCaller.getSummaryPaymentInvTotal(paymenthdr.getId());
					txtTotal.setText(Double.toString(invTotal));

					totalBalance = Double.parseDouble(txtTotal.getText());
					txtTotal.setText(Double.toString(total + totalBalance));

				}
				txtBalance.setText(Double
						.toString((Double.valueOf(txtGrandTotal.getText()) - Double.valueOf(txtTotal.getText()))));

			}
		}

	}

	private void fillOwnAcc() {
		tbOwnAccount.setItems(ownAccList);
		clOwnAccount.setCellValueFactory(cellData -> cellData.getValue().getDebitAmountProperty());

	}

	public java.util.Date convertToDateViaInstant(LocalDate dateToConvert) {
		return java.util.Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	@FXML
	void delete(ActionEvent event) {
		if (null != paymentDelete.getId()) {
			RestCaller.deletePaymentDtl(paymentDelete.getId());
			notifyMessage(5, "Deleted!!!");
			paymentList.clear();
			ResponseEntity<List<PaymentDtl>> paymentdtlSaved = RestCaller.getPaymentDtl(paymenthdr);
			paymentList = FXCollections.observableArrayList(paymentdtlSaved.getBody());
			if (paymentList != null) {
				for (PaymentDtl payment : paymentList) {
					ResponseEntity<AccountHeads> respentity = RestCaller.getAccountById(payment.getAccountId());
					payment.setAccount(respentity.getBody().getAccountName());
				}

			}
			FillTable();
		}
		setTotal();
	}

	@FXML
	void accountOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!paymentaccount.getText().trim().isEmpty())
				paymentmode.requestFocus();
			else
				showPopup();
		}
	}

	@FXML
	void submit(ActionEvent event) {
		if (!txtTotal.getText().trim().isEmpty()) {
			if (Double.parseDouble(txtGrandTotal.getText()) < Double.parseDouble(txtTotal.getText())) {
				notifyMessage(1, "Please Check Amount");
				return;
			}
			if (Double.parseDouble(txtGrandTotal.getText()) > Double.parseDouble(txtTotal.getText())) {
				Double ownAccontAmount = Double.parseDouble(txtGrandTotal.getText())
						- Double.parseDouble(txtTotal.getText());
				ownAccountSetelment(ownAccontAmount);
			}
		} else {
			Double ownAccontAmount = Double.parseDouble(txtGrandTotal.getText());
			ownAccountSetelment(ownAccontAmount);
		}
		ResponseEntity<List<PaymentDtl>> paymentdtlSaved = RestCaller.getPaymentDtl(paymenthdr);
		paymentList = FXCollections.observableArrayList(paymentdtlSaved.getBody());
		if (paymentdtlSaved.getBody().size() == 0) {

			paymenthdr = null;
			paymentList.clear();
			txtGrandTotal.clear();
			tbPayableInvoiceDtl.getItems().clear();
			txtinvoiceNumber.clear();
			txtPaidAmount.clear();
			txtTotal.clear();
			paymentTable.getItems().clear();
			tbOwnAccount.getItems().clear();
			return;
		}

		String financialYear = SystemSetting.getFinancialYear();
		String vNo = RestCaller.getVoucherNumber(financialYear + "PYN");

		paymenthdr.setVoucherNumber(vNo);
		RestCaller.updatePaymenthdr(paymenthdr);
		ResponseEntity<List<PaymentInvoiceDtl>> paymnetInvDtlSaved = RestCaller.getPaymentInvDtlById(paymenthdr);
		paymentInvDtlListTable = FXCollections.observableArrayList(paymnetInvDtlSaved.getBody());
		for (PaymentInvoiceDtl paymenttDtl : paymentInvDtlListTable) {
			accountPayable = new AccountPayable();
			ResponseEntity<AccountPayable> savedAccounts = RestCaller
					.getAccountPayableByVoucherId(paymenttDtl.getInvoiceNumber());
			accountPayable = savedAccounts.getBody();
			if (null != accountPayable) {
				accountPayable.setPaidAmount(accountPayable.getPaidAmount() + paymenttDtl.getPaidAmount());
				RestCaller.updateAccountPayable(accountPayable);
			}

		}

		for (PaymentDtl paymnt : paymentList) {
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(paymenthdr.getBranchCode());
			ResponseEntity<AccountHeads> accountHead = RestCaller
					.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
			AccountHeads accountHeads = accountHead.getBody();
			if (paymnt.getModeOfPayment().equalsIgnoreCase(accountHeads.getAccountName())) {
				dayBook.setCrAccountName(SystemSetting.systemBranch + "-CASH");
				dayBook.setDrAmount(0.0);
			}

			else {
				dayBook.setCrAccountName(paymnt.getBankAccountNumber());
				dayBook.setDrAmount(paymnt.getAmount());
			}

			ResponseEntity<AccountHeads> accountHead1 = RestCaller.getAccountById(paymnt.getAccountId());
			AccountHeads accountHeads1 = accountHead1.getBody();
			dayBook.setCrAmount(paymnt.getAmount());
			dayBook.setNarration(accountHeads1.getAccountName() + paymenthdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(paymenthdr.getVoucherNumber());
			dayBook.setSourceVoucherType("PAYMENTS");
			dayBook.setDrAccountName(accountHeads1.getAccountName());

			LocalDate ldate = SystemSetting.utilToLocaDate(paymenthdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(ldate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}

		java.util.Date uDate = paymenthdr.getVoucherDate();
		String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		// Version 1.6 Starts

		ChequePrint chequePrint = new ChequePrint();
		if (paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("CHEQUE")
				|| paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("NEFT")
				|| paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("RTGS")) {
			Alert a = new Alert(AlertType.CONFIRMATION);
			a.setHeaderText("Cheque Print");
			a.setContentText("Do you Want To Print Cheque");
			a.showAndWait().ifPresent((btnType) -> {

				if (btnType == ButtonType.OK) {
					String accountName;
					BigDecimal bamout = new BigDecimal(paymentdtlSaved.getBody().get(0).getAmount());
					bamout = bamout.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					String amount = bamout + "";
					String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");
					ResponseEntity<AccountHeads> getAccname = RestCaller
							.getAccountById(paymentdtlSaved.getBody().get(0).getAccountId());
					String bankName = paymentdtlSaved.getBody().get(0).getBankAccountNumber();
					if (paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("NEFT")) {
						accountName = "NEFT-" + getAccname.getBody().getAccountName();
					} else if (paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("RTGS")) {
						accountName = "RTGS-" + getAccname.getBody().getAccountName();
					} else {
						accountName = getAccname.getBody().getAccountName();
					}
					System.out.println("ACCOUNT NAME-" + accountName);
					System.out.println("AMOUNT" + paymentdtlSaved.getBody().get(0).getAmount() + "");
					System.out.println("AMOUNT IN WORDS" + amountinwords);
					System.out.println("DATE" + vDate);
					String sdate = SystemSetting.UtilDateToString(uDate, "dd MM yyyy");
					System.out.println("DATE" + sdate);
					try {
						chequePrint.PrintInvoiceThermalPrinter(bankName, accountName, amount, amountinwords, sdate);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		// Version 1.6 Ends
		paymenthdr = null;
		paymentList.clear();
		txtGrandTotal.clear();
		tbPayableInvoiceDtl.getItems().clear();
		txtinvoiceNumber.clear();
		txtPaidAmount.clear();
		txtTotal.clear();
		paymentTable.getItems().clear();
		tbOwnAccount.getItems().clear();
		paymentInvDtlListTable.clear();
		paymentList.clear();
		ownAccList.clear();
		paymentinstrumentdate.setValue(null);
		paymentaccount.clear();
		paymentmode.getSelectionModel().clearSelection();
		txtBalance.clear();
		if (null != taskid) {
			RestCaller.completeTask(taskid);
		}
		taskid = null;
		try {
			JasperPdfReportService.PaymentReport(vNo, vDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Version 1.6 Comment Start
		/*
		 * paymenthdr = null; paymentList.clear(); txtGrandTotal.clear();
		 * tbPayableInvoiceDtl.getItems().clear(); txtinvoiceNumber.clear();
		 * txtPaidAmount.clear(); txtTotal.clear(); paymentTable.getItems().clear();
		 * tbOwnAccount.getItems().clear(); paymentInvDtlListTable.clear();
		 * paymentList.clear(); ownAccList.clear();
		 * paymentinstrumentdate.setValue(null); paymentaccount.clear();
		 * paymentmode.getSelectionModel().clearSelection(); txtBalance.clear();
		 */

		// Version 1.6 Comment ends

//		paymenttransdate.setValue(null);
	}

	private void ownAccountSetelment(Double ownAccontAmount) {
		ResponseEntity<AccountHeads> supSaved = RestCaller.getAccountHeadsByName(paymentaccount.getText());
		AccountHeads supplier = supSaved.getBody();
		if (null != supplier) {
			ownAccount = new OwnAccount();
			ownAccount.setAccountId(payment.getAccountId());
			ownAccount.setPaymenthdr(paymenthdr);
			ownAccount.setAccountType("SUPPLIER");
			ownAccount.setCreditAmount(0.0);
			ownAccount.setDebitAmount(ownAccontAmount);
			ownAccount.setOwnAccountStatus("PENDING");
			ownAccount.setRealizedAmount(0.0);
			ownAccount.setVoucherDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
			ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
			ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
			ownAccount = respentity1.getBody();
			ownAccList.add(ownAccount);

			notifyMessage(6, "Amount will set to Own Account");
		}
	}

	private void setTotalAmt() {

		totalAmt1 = RestCaller.getSummaryPaymentInvTotal(paymenthdr.getId());
		txtTotal.setText(Double.toString(totalAmt1));
		double balance = 0.0;
		balance = totalamount - totalAmt1;
		txtBalance.setText(Double.toString(balance));

	}

	private void FillTable() {

		paymentTable.setItems(paymentList);
		for (PaymentDtl payment : paymentList) {
			ResponseEntity<AccountHeads> respentity = RestCaller.getAccountById(payment.getAccountId());
			payment.setAccount(respentity.getBody().getAccountName());
		}
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());
		remark.setCellValueFactory(cellData -> cellData.getValue().getRemarkProperty());

		modeOfPay.setCellValueFactory(cellData -> cellData.getValue().getModeOfPaymentProperty());
		instrument.setCellValueFactory(cellData -> cellData.getValue().getInstrumentNumberProperty());
		bankaccount.setCellValueFactory(cellData -> cellData.getValue().getBankAccountNumberProperty());
		amount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		instrumentdate.setCellValueFactory(cellData -> cellData.getValue().getInstrumentDateProperty());

	}

	private void FillPaymentInvDtls() {

		tbPayableInvoiceDtl.setItems(paymentInvDtlListTable);
//	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clInvoiceNumber.setCellValueFactory(cellData -> cellData.getValue().getInvoiceNumberProperty());
		clPaidAmount.setCellValueFactory(cellData -> cellData.getValue().getPaidAmountProperty());
		setTotalAmt();
	}

	@FXML
	void ShowAccountPopup(MouseEvent event) {
//		showPopup();
	}

	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/supplierPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			paymentmode.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}

	@Subscribe
	public void popuplistner(SupplierPopupEvent supplierEvent) {
		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) paymentaccount.getScene().getWindow();
		if (stage.isShowing()) {
			accountHeads = new AccountHeads();
			paymentaccount.setText(supplierEvent.getSupplierName());
			String accountId = supplierEvent.getSupplierId();

			ResponseEntity<AccountHeads> respentity = RestCaller.getAccountHeadsByName(supplierEvent.getSupplierName());
			if (null != respentity.getBody()) {
				accountHeads = respentity.getBody();
			}
		}

	}

	@Subscribe
	public void ListentoPAymentTask(SupplierPaymentEvent paymentTask) {

		String sipplierId = paymentTask.getSupplierId();
		String purchaseId = paymentTask.getPurchaseHdrId();

		Summary purcasheSummary = RestCaller.getSummary(purchaseId);

		ResponseEntity<AccountHeads> supplierResponce = RestCaller.getAccountHeadsById(sipplierId);
		AccountHeads supplier = supplierResponce.getBody();
		paymentaccount.setText(supplier.getAccountName());
		paymentamount.setText(purcasheSummary.getTotalAmount() + "");
		taskId = paymentTask.getTakId();

	}

	private void clearfields() {
//		paymentaccount.setText("");
		paymentremark.setText("");
		paymentamount.setText("");
		paymentinstrumentnumber.setText("");
		paymentvouchernumber.setText("");
		paymentbankaccount.getSelectionModel().clearSelection();
		// txtGrandTotal.clear();

	}

	@FXML
	void ShowAccountPayablePopup(MouseEvent event) {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/paymentInvcDtlPopup.fxml"));
			Parent root = loader.load();
			PaymentInvcDetailCtl popupctl = loader.getController();
			popupctl.LoadAccountPayable(accountHeads.getId());

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//					dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void AccountPayablePopup(PaymentInvPopupEvent paymentInvPopupEvent) {

		System.out.println("------AccountPayablePopup-------popuplistner-------------");
		Stage stage = (Stage) paymentadd.getScene().getWindow();
		if (stage.isShowing()) {

			txtinvoiceNumber.setText(paymentInvPopupEvent.getInvoiceNumber());
			txtPaidAmount.setText(String.valueOf(paymentInvPopupEvent.getBalanceAmount()));

		}

	}

	private void setTotal() {

		totalamount = RestCaller.getPaymentTotal(paymenthdr.getId());
		txtGrandTotal.setText(Double.toString(totalamount));
		if (!txtTotal.getText().trim().isEmpty() && !txtGrandTotal.getText().trim().isEmpty()) {
			txtBalance.setText(
					Double.toString((Double.valueOf(txtGrandTotal.getText()) - Double.valueOf(txtTotal.getText()))));
		}
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		{
			taskid = taskWindowDataEvent.getId();
//				customerRegistration.setCustomerName(txtAccount.getText());
			ResponseEntity<AccountHeads> sup = RestCaller.getAccountHeadsById(taskWindowDataEvent.getAccountId());
			paymentaccount.setText(sup.getBody().getAccountName());
			accountHeads = sup.getBody();
			ResponseEntity<List<PurchaseHdr>> prhdr = RestCaller.getPurchaseHdrByVoucherNumber(
					taskWindowDataEvent.getVoucherNumber(), taskWindowDataEvent.getVoucherDate());
			if (prhdr.getBody().size() > 0) {
				txtPaidAmount.setText(prhdr.getBody().get(0).getInvoiceTotal() + "");
			}
		}
	}


}
