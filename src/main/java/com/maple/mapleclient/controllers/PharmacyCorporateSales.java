package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.AddKotWaiter;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CurrencyConversionMst;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.FCSummarySalesDtl;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.InsuranceCompanyDtl;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PatientMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.SalesTypeMst;
import com.maple.mapleclient.entity.SiteMst;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CategorywiseItemSearchEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.HoldedCustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.events.PatientEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyCorporateSales {
	String taskid;
	String processInstanceId;

	CurrencyConversionMst currencyConversionMst = null;

	String localCustId = null;
	private ObservableList<PriceDefinition> priceDefenitionList = FXCollections.observableArrayList();
	private ObservableList<BatchPriceDefinition> BatchpriceDefenitionList = FXCollections.observableArrayList();

	private ObservableList<SiteMst> siteMstList = FXCollections.observableArrayList();

	private static final Logger logger = LoggerFactory.getLogger(PurchaseCtl.class);
	String invoiceNumberPrefix = SystemSetting.WHOLE_SALES_PREFIX;
	String gstInvoicePrefix = SystemSetting.GST_INVOCE_PREFIX;
	String vanSalesPrefix = SystemSetting.GST_INVOCE_PREFIX;
	EventBus eventBus = EventBusFactory.getEventBus();
	// ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();

	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();

	private ObservableList<CurrencyMst> currencyList = FXCollections.observableArrayList();
	@FXML
	private TextField txtDiscount;

	@FXML
	private TextField txtFCCashToPay;
	@FXML
	private TextField txtfcdiscount;

	@FXML
	private TextField txtGrandTotal;

	@FXML
	private TextField txtFCRate;
//	@FXML
//	private TextField txtForeignCurrency;
	@FXML
	private Label lblAmount;
	@FXML
	private TextField txtDiscountAmt;

	@FXML
	private TextField txtPatientName;

	@FXML
	private TextField txtPatientNationalId;
	@FXML
	private TextField txtPriceType;
	@FXML
	private TextField txtPreviousBalance;
	@FXML
	private Button btnSearch;
	@FXML
	private ComboBox<String> cmbCurrency;
	@FXML
	private ComboBox<String> cmbSalesMan;
	@FXML
	private TextField txtSalesType;

	@FXML
	void ShowLocalCustomerPopup(ActionEvent event) {

		showLocalCustPopup();
	}

	private void showLocalCustPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LocalCustPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);

			Parent root = loader.load();
			LocalCustPopupCtl popupctl = loader.getController();
			popupctl.localCustomerPopupByCustomerId(custId);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			gstNo.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupLocalCustomerlistner(LocalCustomerEvent localCustomerEvent) {
//		Stage stage = (Stage) btnAdditem.getScene().getWindow();
//		if (stage.isShowing()) {
//
//			txtLocalCustomer.setText(localCustomerEvent.getLocalCustAddress());
//			localCustId = localCustomerEvent.getLocalCustid();
//			System.out.println("custIdcustIdcustId" + custId);

//		}

	}

	@FXML
	private TextField txtAmtAftrDiscount;
	SalesDtl salesDtl = null;
	SalesTransHdr salesTransHdr = null;
	UnitMst unitMst = null;

	double qtyTotal = 0;
	double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	String custId = "";
	boolean customerIsBranch = false;

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty discount = new SimpleStringProperty("");
	StringProperty discountAmt = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");

	StringProperty barcodeProperty = new SimpleStringProperty("");

	StringProperty taxRateProperty = new SimpleStringProperty("");

	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");
	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;
	@FXML
	private TableColumn<SalesDtl, String> columnFCTaxRate;
	@FXML
	private TableColumn<SalesDtl, String> columnFCMrp;

	@FXML
	private TableColumn<SalesDtl, String> clFCAmount;
	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnFCRate;
	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnRate;
	@FXML
	private TableColumn<SalesDtl, String> columnMrp;
	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;
	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtItemcode;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private ComboBox<String> cmbSaleType;
	@FXML
	private Button btnSave;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField txtLoginDate;
	@FXML
	private TextField txtSBICard;

	@FXML
	private TextField txtSodexoCard;

	@FXML
	private TextField txtYesCard;
	@FXML
	private TextField custname;
	@FXML
	private TextField custAdress;
	@FXML
	private TextField gstNo;

	@FXML
	private TextField txtDiscountAmount;

	@FXML
	void onEnterCustPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();
		}
	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

		loadCustomerPopup();
	}

	@FXML
	void discountOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (!txtDiscount.getText().isEmpty()) {
				txtDiscountAmt.clear();
				Double amtaftradiscount;
				Double discountamt = (Double.parseDouble(txtCashtopay.getText())
						* Double.parseDouble(txtDiscount.getText())) / 100;
				BigDecimal disamt = new BigDecimal(discountamt);
				disamt = disamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				txtDiscountAmt.setText(disamt.toPlainString());
				// txtDiscountAmt.setEditable(false);
				amtaftradiscount = Double.parseDouble(txtCashtopay.getText())
						- Double.parseDouble(txtDiscountAmt.getText());
				BigDecimal amtaftdiscount = new BigDecimal(amtaftradiscount);
				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
				txtAmtAftrDiscount.setEditable(false);
			}

		}
	}

	@FXML
	void discountAmntOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtDiscountAmt.getText().isEmpty()) {
				txtDiscount.clear();
				Double discountAftrAmt = 0.0, discount = 0.0;
				discount = (Double.parseDouble(txtDiscountAmt.getText()) / Double.parseDouble(txtCashtopay.getText()))
						* 100;
				BigDecimal disamt = new BigDecimal(discount);
				disamt = disamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtDiscount.setText(disamt.toPlainString());
				discountAftrAmt = Double.parseDouble(txtCashtopay.getText())
						- Double.parseDouble(txtDiscountAmt.getText());
				BigDecimal amtaftdiscount = new BigDecimal(discountAftrAmt);
				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
				txtAmtAftrDiscount.setEditable(false);
			}

		}
	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {
		// showPopup();
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	@FXML
	private void initialize() {
		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("WHOLESALE_RATE_EDIT");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("NO")) {
				txtRate.setEditable(false);
			} else {
				txtRate.setEditable(true);
			}
		}

		eventBus.register(this);
		
		/// *******************currency cmb****************

		ResponseEntity<List<CurrencyMst>> currencyMstResp = RestCaller.getallCurrencyMst();
		currencyList = FXCollections.observableArrayList(currencyMstResp.getBody());
		for (CurrencyMst currency : currencyList) {
			cmbCurrency.getItems().add(currency.getCurrencyName());
		}

		cmbCurrency.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					ResponseEntity<CurrencyMst> currency = RestCaller.getCurrencyMstByName(newValue);
					CurrencyMst currencyMst = currency.getBody();
					if (SystemSetting.getUser().getCompanyMst().getCurrencyName()
							.equalsIgnoreCase(currencyMst.getCurrencyName())) {
						txtFCCashToPay.setText("0.0");
					}
					if (null != currencyMst && null != txtCashtopay.getText()
							&& !txtCashtopay.getText().trim().isEmpty()) {
						
//						Double amount = RestCaller.getFCRate(Double.parseDouble(txtCashtopay.getText()),
//								currencyMst.getCurrencyName());
						Double amount = RestCaller.getCurrencyConvertedAmount(SystemSetting.getUser().getCompanyMst().getCurrencyName(),currencyMst.getCurrencyName(),Double.parseDouble(txtCashtopay.getText()));
						txtFCCashToPay.setText(amount.toString());

					}

				}
			}
		});

		// ********************************** end currency
		// cmb********************************************

		// *********************************************************************************
		txtLoginDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate));
		txtDiscount.setVisible(false);
		txtAmtAftrDiscount.setVisible(false);
		txtDiscountAmt.setVisible(false);
//		cmbSaleType.getItems().add("VAN SALE");
//		cmbSaleType.getItems().add("COUNTER SALE");
		ResponseEntity<List<SalesTypeMst>> salesTypeSaved = RestCaller.getSalesTypeMst();
		if (null == salesTypeSaved.getBody()) {
			notifyMessage(5, "Please Add Voucher Type", false);
			return;
		}

		cmbSaleType.getItems().add("CORPORATESALE");
		cmbSaleType.getSelectionModel().select("CORPORATESALE");

		ResponseEntity<List<AddKotWaiter>> salesManList = RestCaller.getWaiterbyStatus();
		for (int i = 0; i < salesManList.getBody().size(); i++) {
			cmbSalesMan.getItems().add(salesManList.getBody().get(i).getWaiterName());
		}
		logger.info("========INITIALIZATION STARTED IN WHOLE SALE WINDOW ===============");
		/*
		 * Create an instance of SalesDtl. SalesTransHdr entity will be refreshed after
		 * final submit. SalesDtl will be added on AddItem Function
		 * 
		 */

		// salesDtl = new SalesDtl();
		/*
		 * Fieds with Entity property
		 */

		txtDiscountAmt.textProperty().bindBidirectional(discountAmt);
		txtDiscount.textProperty().bindBidirectional(discount);
		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);
		// txtItemcode.textProperty().bindBidirectional(salesDtl.getItemCodeProperty());
		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPaidamount.setText(oldValue);
				}
			}
		});
		txtChangeamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtChangeamount.setText(oldValue);
				}
			}
		});
		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashtopay.setText(oldValue);
				}
			}
		});
		txtSBICard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSBICard.setText(oldValue);
				}
			}
		});

		txtYesCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtYesCard.setText(oldValue);
				}
			}
		});

		txtSodexoCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSodexoCard.setText(oldValue);
				}
			}
		});
		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});

		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtPaidamount.getText().length() > 0)) {

					double paidAmount = Double.parseDouble(txtPaidamount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmt = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmount = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((cardAmount) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtcardAmount.getText().length() > 0)) {

					double cardAmt = Double.parseDouble(txtcardAmount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmount = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmt = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((paidAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
				}
			}
		});

		// btnSave.setDisable(true);
		itemDetailTable.setItems(saleListTable);

		txtBarcode.requestFocus();

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesDtl = new SalesDtl();
					txtBatch.setText(newSelection.getBatchCode());
					salesDtl = new SalesDtl();
					salesDtl.setId(newSelection.getId());
					if (null != newSelection.getOfferReferenceId()) {
						salesDtl.setOfferReferenceId(newSelection.getOfferReferenceId());
					}

					if (null != newSelection.getSchemeId()) {
						salesDtl.setSchemeId(newSelection.getSchemeId());
					}
					salesDtl.setItemId(newSelection.getItemId());
					salesDtl.setSalesTransHdr(newSelection.getSalesTransHdr());
					salesDtl.setQty(newSelection.getQty());
					salesDtl.setUnitId(newSelection.getUnitId());
					salesDtl.setUnitName(newSelection.getUnitName());
					txtBarcode.setText(newSelection.getBarcode());
					txtItemname.setText(newSelection.getItemName());
					txtItemcode.setText(newSelection.getItemCode());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));
					cmbUnit.setValue(newSelection.getUnitName());
				}
			}
		});
		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadsByName(custname.getText());

				AccountHeads accountHeads = accountHeadsResp.getBody();

				// ResponseEntity<PriceDefinition> getpriceDef = RestCaller.get
				ResponseEntity<UnitMst> getUnit = RestCaller
						.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

				UnitMst unitMst = getUnit.getBody();

				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

				ItemMst item = getItem.getBody();

				if (null == item.getId()) {
					return;
				}

				String unitId = "";

				if (null == unitMst) {
					unitId = item.getUnitId();

				} else {
					unitId = unitMst.getId();
				}
				Date udate = SystemSetting.getApplicationDate();
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
						getItem.getBody().getId(), accountHeadsResp.getBody().getPriceTypeId(), getUnit.getBody().getId(),
						txtBatch.getText(), sdate);
				if (null != batchPriceDef.getBody()) {
					txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
				} else {
					txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));

					ResponseEntity<PriceDefinition> priceDef = RestCaller.getPriceDefenitionByItemIdAndUnit(
							item.getId(), accountHeads.getPriceTypeId(), unitMst.getId(), sdate);
					if (null != priceDef.getBody()) {
						txtRate.setText(Double.toString(priceDef.getBody().getAmount()));

					}

					else

					{
						ResponseEntity<List<PriceDefinition>> pricebyItem = RestCaller
								.getPriceByItemId(getItem.getBody().getId(), accountHeadsResp.getBody().getPriceTypeId());
						priceDefenitionList = FXCollections.observableArrayList(pricebyItem.getBody());

						if (null != pricebyItem.getBody())

						{
							for (PriceDefinition price : priceDefenitionList) {
								if (null == price.getUnitId()) {
									txtRate.setText(Double.toString(price.getAmount()));

								}
							}
						}
					}

				}

			}
		});
		logger.info("======== WHOLE SALE WINDOW INITIALIZATION COMPLETED");

	}

	@FXML
	void deleteRow(ActionEvent event) {

		try {

			if (null != salesDtl) {
				if (null != salesDtl.getId()) {

					if (null == salesDtl.getSchemeId()) {

						RestCaller.deleteSalesDtl(salesDtl.getId());

						System.out.println("toDeleteSale.getId()" + salesDtl.getId());
//						RestCaller.deleteSalesDtl(salesDtl.getId());
						txtItemname.clear();
						txtItemcode.clear();
						txtBarcode.clear();
						txtBatch.clear();
						txtRate.clear();
						txtQty.clear();
						ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
						saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
						FillTable();
						salesDtl = new SalesDtl();

						notifyMessage(1, " Item Deleted Successfully", true);

					} else {
						notifyMessage(1, " Offer can not be delete", false);
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void hold(ActionEvent event) {
		txtAmtAftrDiscount.clear();
		txtBarcode.clear();
		txtBatch.clear();
		txtcardAmount.clear();
		txtCashtopay.clear();
		txtChangeamount.clear();
		txtDiscount.clear();
		txtYesCard.clear();
		txtSodexoCard.clear();
		txtSBICard.clear();
		txtRate.clear();
		txtQty.clear();
		txtPriceType.clear();
		txtItemcode.clear();
		txtItemname.clear();
		txtPaidamount.clear();
		custAdress.clear();
		custname.clear();
		custId = null;
		saleListTable.clear();
		salesDtl = null;
		salesTransHdr = null;

		txtFCCashToPay.clear();
		txtFCRate.clear();
		cmbCurrency.getSelectionModel().clearSelection();
		currencyConversionMst = null;
		notifyMessage(5, "Sales Holded", true);

	}

	@FXML
	void saveOnkey(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}
	}

	@FXML
	void saveOnCtl(KeyEvent event) {
//    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
//    		finalSave();
//    	}
	}

	@FXML
	void FinalSaveOnPress(KeyEvent event) {
		logger.info("===== WHOLE SALE FINAL SAVE STARTED ============");
		if (event.getCode() == KeyCode.ENTER) {
			finalSave();
		}
	}

	private void finalSave() {

		/*
		 * FinalSave Final
		 */
		// -------------------verification 5.0 Surya
		String stockVerification = RestCaller.StockVerificationBySalesTransHdr(salesTransHdr.getId());
		if (null != stockVerification) {
			notifyMessage(5, stockVerification, false);

			// return;

		} else {

			// -------------------verification 5.0 Surya end

			salesTransHdr.setCustomerId(custId);

			logger.info("===========Whole Sale get customer by Id in Add item!!");
			
			/*
			 * new url for getting account heads instead of customer mst=========05/0/2022
			 */
//			ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerById(custId);
			ResponseEntity<AccountHeads> accountHeadsResponse=RestCaller.getAccountHeadsById(custId);
			if (null != accountHeadsResponse.getBody()) {
				salesTransHdr.setAccountHeads(accountHeadsResponse.getBody());

				if (null == accountHeadsResponse.getBody().getPartyGst()
						|| accountHeadsResponse.getBody().getPartyGst().length() < 13) {
					salesTransHdr.setSalesMode("B2C");
				} else {
					salesTransHdr.setSalesMode("B2B");
				}
			}

			txtChangeamount.setText("00.0");
			Double paidAmount = 0.0;
			Double cardAmount = 0.0;
			String card = "";
			if (!txtSBICard.getText().trim().isEmpty()) {
				card = txtSBICard.getText();
			} else if (!txtYesCard.getText().trim().isEmpty()) {
				card = txtYesCard.getText();
			} else if (!txtSodexoCard.getText().trim().isEmpty()) {
				card = txtSodexoCard.getText();
			}

			salesTransHdr.setCardNo(card);
			if (null != cmbSalesMan.getSelectionModel()) {
				if (null != cmbSalesMan.getSelectionModel().getSelectedItem()) {
					ResponseEntity<AddKotWaiter> salesMan = RestCaller
							.getWaiterbyName(cmbSalesMan.getSelectionModel().getSelectedItem());
					salesTransHdr.setSalesManId(salesMan.getBody().getId());
				}
			}

			// ------------------------------cmbCurrency--------------------------------

			if (null != cmbCurrency.getSelectionModel()) {
				if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {
					ResponseEntity<CurrencyMst> currency = RestCaller
							.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem());
					salesTransHdr.setCurrencyId(currency.getBody().getId());
				}
			}

			// ------------------------------cmbCurrency--------------------------------

			logger.info("=====salesTransHdr.getCurrencyId()======" + salesTransHdr.getCurrencyId());

			// ------------------------------cmbCurrency--------------------------------

			Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
			salesTransHdr.setInvoiceAmount(invoiceAmount);
			if (!txtFCCashToPay.getText().trim().isEmpty()) {
				Double fcInvoiceAmount = Double.parseDouble(txtFCCashToPay.getText());
				salesTransHdr.setFcInvoiceAmount(fcInvoiceAmount);
			}
			if (txtDiscountAmt.getText().length() > 0)
				salesTransHdr.setInvoiceDiscount(Double.parseDouble(txtDiscountAmt.getText()));
			else
				salesTransHdr.setInvoiceDiscount(0.0);
			if (!txtPaidamount.getText().trim().isEmpty()) {

				paidAmount = Double.parseDouble(txtPaidamount.getText());
				salesTransHdr.setCashPay(paidAmount);
			} else {
				salesTransHdr.setCashPay(0.0);
			}

			if (!txtcardAmount.getText().trim().isEmpty()) {

				cardAmount = Double.parseDouble(txtcardAmount.getText());
				salesTransHdr.setCardamount(cardAmount);
			} else {
				salesTransHdr.setCardamount(0.0);
			}

			salesTransHdr.setPaidAmount(salesTransHdr.getCashPay() + salesTransHdr.getCardamount());

			if (!txtPatientName.getText().trim().isEmpty()) {
				ResponseEntity<PatientMst> patientMst = RestCaller.getPatientMstByName(txtPatientName.getText());
				if (null != patientMst.getBody()) {
					salesTransHdr.setPatientMst(patientMst.getBody());
				}
			}
			if (!txtChangeamount.getText().trim().isEmpty()) {
				Double changeAmount = Double.parseDouble(txtChangeamount.getText());
				salesTransHdr.setChangeAmount(changeAmount);
			}

			eventBus.post(salesTransHdr);

			ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
			if (saledtlSaved.getBody().size() == 0) {
				return;
			}
			logger.info("=====salesTransHdr.getCustomerId()======" + salesTransHdr.getCustomerId());

			ResponseEntity<BranchMst> branchMst = RestCaller.getBranchMstById(salesTransHdr.getCustomerId());

			BranchMst branch = new BranchMst();
			branch = branchMst.getBody();

			if (null != branch) {
				salesTransHdr.setSalesMode("BRANCH_SALES");
			}

			logger.info("=======Whole Sale set invoice prefix started==========");

			ResponseEntity<SalesTypeMst> getsalesType = RestCaller
					.getSaleTypeByname(cmbSaleType.getSelectionModel().getSelectedItem());
			
			if(null != getsalesType.getBody().getSalesPrefix()) {
			salesTransHdr.setInvoiceNumberPrefix(getsalesType.getBody().getSalesPrefix());
			}
			logger.info("Whole Sale invoice prefix COMPLETED===============");

//				Date date = Date.valueOf(LocalDate.now());
			LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
			Date date = SystemSetting.systemDate;
			salesTransHdr.setVoucherDate(date);
			// Call Rest to find if customer is Branch

			if (customerIsBranch) {
				salesTransHdr.setIsBranchSales("Y");
			} else {
				salesTransHdr.setIsBranchSales("N");
			}

			logger.info("Whole Sale STARTED TO SET ACCOUNT RECEIVABLE");

			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsById(custId);
			ResponseEntity<AccountReceivable> accountReceivableResp = RestCaller
					.getAccountReceivableBySalesTransHdrId(salesTransHdr.getId());
			AccountReceivable accountReceivable = null;
			if (null != accountReceivableResp.getBody()) {
				accountReceivable = accountReceivableResp.getBody();
			} else {
				accountReceivable = new AccountReceivable();
			}

			accountReceivable.setAccountId(custId);
			accountReceivable.setAccountHeads(accountHeads.getBody());

			if (txtAmtAftrDiscount.getText().length() > 0) {
				accountReceivable.setDueAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
				accountReceivable.setDueAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
				accountReceivable.setBalanceAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
			} else {
				accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
				accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
				accountReceivable.setBalanceAmount(Double.parseDouble(txtCashtopay.getText()));
			}
			LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
			LocalDate dueDate = due.plusDays(accountHeads.getBody().getCreditPeriod());
			accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
			accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
			accountReceivable.setSalesTransHdr(salesTransHdr);
			accountReceivable.setRemark("Wholesale");
			LocalDate due1 = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
			accountReceivable.setVoucherDate(java.sql.Date.valueOf(due1));
			accountReceivable.setPaidAmount(0.0);
			ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
			accountReceivable = respentity.getBody();

			logger.info("Whole Sale ACCOUNT RECEIVABLE SAVED !!");
			logger.info("Whole Sale STARTED TO SAVE SALE RECEIPTS!!");

			// Delete any sales receiots previously saved by mistake

			ResponseEntity<List<SalesReceipts>> srList = RestCaller.getSalesReceiptsByTransHdrId(salesTransHdr.getId());
			if (srList.getBody().size() > 00) {

				RestCaller.deleteSalesReceiptsByTransHdr(salesTransHdr);
			}

			if (!txtDiscountAmount.getText().trim().isEmpty()) {
				String currencyType = cmbCurrency.getSelectionModel().getSelectedItem().toString();
				String companyCurrencyType = SystemSetting.getUser().getCompanyMst().getCurrencyName();
				Double discountAmount = Double.parseDouble(txtDiscountAmount.getText());

				if (!txtFCCashToPay.getText().trim().isEmpty()) {
					Double fcDiscountAmount = RestCaller.getCurrencyConvertedAmount(currencyType, companyCurrencyType,
							discountAmount);

					if (null != discountAmount) {
						invoiceAmount = invoiceAmount - fcDiscountAmount;
					}

				} else {
					invoiceAmount = invoiceAmount - discountAmount;

				}
			}

			SalesReceipts salesReceipts = new SalesReceipts();
			salesReceipts.setReceiptMode("CREDIT");
			salesReceipts.setReceiptAmount(invoiceAmount);
			salesReceipts.setSalesTransHdr(salesTransHdr);
			salesReceipts.setAccountId(accountHeads.getBody().getId());
			salesReceipts.setBranchCode(salesTransHdr.getBranchCode());
				if (!txtFCCashToPay.getText().trim().isEmpty()) {
					salesReceipts.setFcAmount(Double.parseDouble(txtFCCashToPay.getText()));
				}

			RestCaller.saveSalesReceipts(salesReceipts);
			if (!txtfcdiscount.getText().trim().isEmpty()) {
				saveSalesReceipt();
			}
			logger.info("Whole Sale SALE RECEIPTS SAVED!!");

			if (null == salesTransHdr.getVoucherNumber()) {
				RestCaller.updateSalesTranshdr(salesTransHdr);
			}
			salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());

			notifyMessage(5, "Sales Saved", true);
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(salesTransHdr.getBranchCode());
			dayBook.setDrAccountName(custname.getText());
			dayBook.setDrAmount(salesTransHdr.getInvoiceAmount());
			dayBook.setNarration(custname.getText() + salesTransHdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
			dayBook.setSourceVoucherType("SALES");
			dayBook.setCrAccountName("SALES ACCOUNT");
			dayBook.setCrAmount(salesTransHdr.getInvoiceAmount());

			LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
			dayBook.setsourceVoucherDate(java.sql.Date.valueOf(rdate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			logger.info("====Whole Sale FINAL SAVE COMPLETED STARTED TO PRINT JASPER!!======");
			Format formatter;
			logger.info("Whole Sale started jasper print!!");
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			String strDate = formatter.format(salesTransHdr.getVoucherDate());

			// Version 1.6

			txtcardAmount.setText("");
			txtCashtopay.setText("");
			txtPaidamount.setText("");
			txtSBICard.setText("");
			txtSodexoCard.setText("");
			txtYesCard.setText("");
			custname.setText("");
			custAdress.setText("");
			gstNo.setText("");
			saleListTable.clear();
			txtItemname.setText("");
			txtBarcode.setText("");
			txtQty.setText("");
			txtRate.setText("");
			cmbCurrency.getSelectionModel().clearSelection();
			custId = null;

			txtPriceType.clear();
			txtItemcode.setText("");
			txtBatch.setText("");
			txtBarcode.requestFocus();
			txtAmtAftrDiscount.clear();
			txtDiscount.clear();
			txtDiscountAmt.clear();
//		txtLocalCustomer.clear();
//		txtLocalCustomer.setDisable(true);
			localCustId = null;
			txtFCCashToPay.clear();
			currencyConversionMst = null;
			// Version 1.6 ends
			String vno = salesTransHdr.getVoucherNumber();
			salesDtl = null;
			txtPatientName.clear();
			txtPatientNationalId.clear();
			txtfcdiscount.clear();
			txtGrandTotal.clear();
			cmbSalesMan.getSelectionModel().clearSelection();
//			try {
//				JasperPdfReportService.TaxInvoiceReport(vno, strDate);
//			} catch (JRException e) {
//				e.printStackTrace();
//				logger.info("Whole Sale " + e);
//			}
			String voucherNumber = salesTransHdr.getVoucherNumber();

			try {
				JasperPdfReportService.PharmacyTaxInvoiceReport(voucherNumber, strDate);
			} catch (JRException e) {
				e.printStackTrace();
				logger.info("Whole Sale " + e);
			}

			salesTransHdr = null;

			logger.info("===========Whole Sale jasper print completed!!====================");
			// ---------------version 5.0 surya
			txtPreviousBalance.clear();

			// ---------------version 5.0 surya end

			// Version 1.6 Comment states
			/*
			 * txtcardAmount.setText(""); txtCashtopay.setText("");
			 * txtPaidamount.setText(""); txtSBICard.setText(""); txtSodexoCard.setText("");
			 * txtYesCard.setText(""); custname.setText(""); custAdress.setText("");
			 * gstNo.setText(""); saleListTable.clear(); txtItemname.setText("");
			 * txtBarcode.setText(""); txtQty.setText(""); txtRate.setText("");
			 * 
			 * custId = null; cmbSaleType.getSelectionModel().clearSelection();
			 * txtPriceType.clear(); txtItemcode.setText(""); txtBatch.setText("");
			 * txtBarcode.requestFocus(); txtAmtAftrDiscount.clear(); txtDiscount.clear();
			 * txtDiscountAmt.clear();
			 * 
			 * txtLocalCustomer.clear(); txtLocalCustomer.setDisable(true); localCustId =
			 * null;
			 */
			// Version 1.6 Comment ends

			logger.info("==========Whole Sale jasper print completed!!=============");
			logger.info("Whole Sale EXIT FROM FINAL SAVE!!");

		}
	}

	@FXML
	void save(ActionEvent event) {

		finalSave();
		// salesTransHdr = null;
		// salesDtl = null;
	}

	private void saveSalesReceipt() {
		SalesReceipts salesReceipts = new SalesReceipts();
		ResponseEntity<AccountHeads> accByName = RestCaller.getAccountHeadByName("DISCOUNT ALLOWED");
		salesReceipts.setAccountId(accByName.getBody().getId());
		salesReceipts.setBranchCode(SystemSetting.getUser().getBranchCode());
		salesReceipts.setReceiptDate(SystemSetting.applicationDate);
		salesReceipts.setUserId(SystemSetting.getUserId());
		salesReceipts.setSalesTransHdr(salesTransHdr);
		salesReceipts.setReceiptMode("DISCOUNT ALLOWED");
		if (!txtFCCashToPay.getText().trim().isEmpty()) {
			Double fcAmount = Double.parseDouble(txtFCCashToPay.getText())
					- Double.parseDouble(txtGrandTotal.getText());
			BigDecimal bdFCAmount = new BigDecimal(fcAmount);
			bdFCAmount = bdFCAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			salesReceipts.setFcAmount(bdFCAmount.doubleValue());
//			Double amount = RestCaller.getCompanyCurrencyAmountOfFC(salesReceipts.getFcAmount(),
//					cmbCurrency.getSelectionModel().getSelectedItem().toString());
			Double amount = RestCaller.getCurrencyConvertedAmount(
					cmbCurrency.getSelectionModel().getSelectedItem().toString(),
					SystemSetting.getUser().getCompanyMst().getCurrencyName(), salesReceipts.getFcAmount());
			if (amount > 0)
				salesReceipts.setReceiptAmount(amount);

		} else {
			salesReceipts.setReceiptAmount(
					Double.parseDouble(txtCashtopay.getText()) - Double.parseDouble(txtGrandTotal.getText()));

		}
		ResponseEntity<SalesReceipts> save = RestCaller.saveSalesReceipts(salesReceipts);
//		salesReceipts.setVoucherNumber(voucherNumber);
	}

	@FXML
	void EnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			// if(event.getSource());

			txtQty.requestFocus();
		}

	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtItemname.requestFocus();
			showPopup();
		}
		if (event.getCode() == KeyCode.ENTER) {
			if (txtBarcode.getText().trim().isEmpty())
				txtfcdiscount.requestFocus();
		}
		if (event.getCode() == KeyCode.DOWN) {
			itemDetailTable.requestFocus();
		}
	}

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnSave.setDisable(false);

			Double change = Double.parseDouble(txtPaidamount.getText()) - Double.parseDouble(txtCashtopay.getText());
			txtChangeamount.setText(Double.toString(change));

		}

	}

	@FXML
	void calcPaid(KeyEvent event) {

	}

	@FXML
	void unHold(ActionEvent event) {
		loadHoldedSales();

	}

	@Subscribe
	public void popupCustomerlistner(HoldedCustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			custname.setText(customerEvent.getCustomerName());
			custAdress.setText(customerEvent.getAddress());
			gstNo.setText(customerEvent.getCustGst());
			custId = customerEvent.getCustomerId();

//			String customerSite = SystemSetting.customer_site_selection;
//			if (customerSite.equalsIgnoreCase("TRUE")) {
//				txtLocalCustomer.setDisable(false);
//				txtLocalCustomer.clear();
//			}

			ResponseEntity<PriceDefenitionMst> priceDef = RestCaller.getPriceNameById(customerEvent.getCustPriceTYpe());
			if (null != priceDef.getBody()) {
				txtPriceType.setText(priceDef.getBody().getPriceLevelName());
			}
			salesTransHdr = RestCaller.getSalesTransHdr(customerEvent.getHdrId());
			cmbSaleType.setValue(salesTransHdr.getVoucherType());
			if (null != salesTransHdr.getAccountHeads().getCurrencyId()) {
				ResponseEntity<CurrencyMst> currencyCon = RestCaller
						.getcurrencyMsyById(salesTransHdr.getAccountHeads().getCurrencyId());
				cmbCurrency.getSelectionModel().select(currencyCon.getBody().getCurrencyName());
				ResponseEntity<CurrencyConversionMst> getcurrencyConMst = RestCaller
						.getCurrencyConversionMstByCurrencyId(salesTransHdr.getAccountHeads().getCurrencyId());
				currencyConversionMst = getcurrencyConMst.getBody();
			}
			cmbSaleType.getSelectionModel().select(salesTransHdr.getVoucherType());
			saleListTable.clear();
			ResponseEntity<List<SalesDtl>> respentity = RestCaller.getSalesDtl(salesTransHdr);
			saleListTable = FXCollections.observableArrayList(respentity.getBody());
			FillTable();
			cmbSaleType.getSelectionModel().select(salesTransHdr.getVoucherType().toString());

		}

	}

	private Boolean confirmMessage() {

		return null;
	}

	private void deleteAllSalesDtl() {

		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		List<SalesDtl> saledtlList = saledtlSaved.getBody();
		for (SalesDtl sales : saledtlList) {
			RestCaller.deleteSalesDtl(sales.getId());
		}

		ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
		saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
		if (saleListTable.size() == 0) {
			RestCaller.deleteSalesTransHdr(salesTransHdr.getId());
			salesTransHdr = null;
			saleListTable.clear();
			itemDetailTable.getItems().clear();
		}

	}

	private void loadHoldedSales() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/HoldedCustomer.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtBarcode.requestFocus();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	private void FillTable() {

		logger.info("=============Whole Sale started fill table!!=============");
		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		// columnCessRate.setCellValueFactory(cellData ->
		// cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clFCAmount.setCellValueFactory(cellData -> cellData.getValue().getFcAmountProperty());
		columnFCMrp.setCellValueFactory(cellData -> cellData.getValue().getFcMrpProperty());
		columnFCRate.setCellValueFactory(cellData -> cellData.getValue().getFcRateProperty());
		columnFCTaxRate.setCellValueFactory(cellData -> cellData.getValue().getFcTaxRateProperty());

		Summary summary = null;
		FCSummarySalesDtl fcSummary = null;

		if (null == salesTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			summary = RestCaller.getSalesWindowSummaryDiscount(salesTransHdr.getId());
			if (null != currencyConversionMst) {
				fcSummary = RestCaller.getFCSalesWindowSummaryDiscount(salesTransHdr.getId());
			}
		} else {
			summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
			if (null != currencyConversionMst) {
				fcSummary = RestCaller.getFCSalesWindowSummary(salesTransHdr.getId());
			}
		}
		if (null != summary.getTotalAmount()) {
			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
		}
		if (null != fcSummary) {

//			salesTransHdr.setCashPaidSale(fcSummary.getFctotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(fcSummary.getFctotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtFCCashToPay.setText(bdCashToPay.toPlainString());

		} else {
			txtFCCashToPay.setText("");
		}
		logger.info("Whole Sale jasper fill table completed!!");
	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();
	}

	private void addItem() {

		// version2.0
		/// check financial year

		ResponseEntity<List<FinancialYearMst>> getFinancialYear = RestCaller.getAllFinancialYear();
		if (getFinancialYear.getBody().size() == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu", false);
			return;
		}
		int count = RestCaller.getFinancialYearCount();
		if (count == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu", false);
			return;
		}

		// version2.0ends
		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("DAY_END_LOCKED_IN_WHOLESALE");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("YES")) {
				ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
				DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
				System.out.println("Sys Date before add" + SystemSetting.systemDate);
				if (null != dayEndClosureHdr) {
					if (null != dayEndClosureHdr.getDayEndStatus()) {
						String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(),
								"yyyy-MM-dd");
						String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
						java.sql.Date prDate = java.sql.Date.valueOf(process_date);
						Date sDate = java.sql.Date.valueOf(sysdate);
						int i = prDate.compareTo(sDate);
						if (i > 0 || i == 0) {
							notifyMessage(1, " Day End Already Done for this Date !!!!", false);
							return;
						}
					}
				}

			}
			Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

			if (!dayendtodone) {
				notifyMessage(1, "Day End should be done before changing the date !!!!", false);
				return;
			}
			System.out.println("Sys Date before add" + SystemSetting.systemDate);
		}

		logger.info("Inside Whole Sale add item");

		if (txtQty.getText().trim().isEmpty()) {

			notifyMessage(5, " Please Enter Quantity...!!!", false);
			return;

		}
		if (custname.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Enter Customer Name...!!!", false);
			custname.requestFocus();
			return;
		}
		if (txtPatientName.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Enter Patient Name...!!!", false);
			txtPatientName.requestFocus();
			return;
		}
		if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Select Item Name...!!!", false);
			txtItemname.requestFocus();
			return;
		}
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Type Quantity...!!!", false);
			txtQty.requestFocus();
			return;
		}

		if (txtBatch.getText().trim().isEmpty()) {
			notifyMessage(1, "Item Batch is not present!!!", false);
			txtQty.requestFocus();
			return;
		}

		if (null == cmbCurrency.getValue()) {
			notifyMessage(3, "Please select currency type", false);
			cmbCurrency.requestFocus();
			return;
		}

		if (null == cmbSaleType.getValue()) {
			notifyMessage(5, " Please Select Sale Type...!!!", false);
			cmbSaleType.requestFocus();
			return;
		}
		ResponseEntity<CurrencyMst> currencyMstResp = RestCaller
				.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem().toString());
		CurrencyMst currencyMst = currencyMstResp.getBody();

		if (!currencyMst.getCurrencyName()
				.equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName())) {
			ResponseEntity<CurrencyConversionMst> currencyConversionMstResp = RestCaller
					.getCurrencyConversionMstByCurrencyId(currencyMst.getId());
			 currencyConversionMst = currencyConversionMstResp.getBody();

			if (null == currencyConversionMst) {
				notifyMessage(2, "currency conversion rate is not set", false);
				return;
			}
		}

		ResponseEntity<UnitMst> getUnitBYItem = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

		ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
				getUnitBYItem.getBody().getId());
		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			if (null == getmulti.getBody()) {
				notifyMessage(5, "Please Add the item in Multi Unit", false);
				return;
			}
		}
		ArrayList items = new ArrayList();
		items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());

		Double chkQty = 0.0;
		String itemId = null;

		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			List element = (List) itr.next();
			chkQty = (Double) element.get(4);
			itemId = (String) element.get(7);

		}

		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if (chkQty < conversionQty) {
				notifyMessage(1, "Not in Stock!!!", false);
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
		} else if (chkQty < Double.parseDouble(txtQty.getText())) {
			notifyMessage(5, "Not in Stock!!!", false);
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();
			return;
		}
		if (null == salesTransHdr) {
			createSalesTransHdr();
		}
		Double itemsqty = 0.0;
		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
				getItem.getBody().getId(), txtBatch.getText());
		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
		if (saleListItemTable.size() > 1) {
			Double PrevQty = 0.0;
			for (SalesDtl saleDtl : saleListItemTable) {
				if (!saleDtl.getUnitId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
							getItem.getBody().getUnitId(), saleDtl.getQty());
				} else {
					PrevQty = saleDtl.getQty();
				}
				itemsqty = itemsqty + PrevQty;
			}
		} else {
			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId, txtBatch.getText());
		}
		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if (chkQty < itemsqty + conversionQty) {
				notifyMessage(1, "Not in Stock!!!", false);
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
		}

		else if (chkQty < itemsqty + Double.parseDouble(txtQty.getText())) {
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();
			notifyMessage(5, "No Stock!!", false);
			return;
		}
		if (null == salesDtl) {
			salesDtl = new SalesDtl();
		}
		if (null != salesDtl.getId()) {

			RestCaller.deleteSalesDtl(salesDtl.getId());

			logger.info("Whole Sale delete sales Dtl completed!!");

			if (null != salesDtl.getSchemeId()) {
				return;
			}

		}

		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(txtItemname.getText());
		salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());

		String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();
		System.out.println(batch);
		salesDtl.setBatchCode(batch);

		logger.info("Whole Sale amount Calculation Started!!");
		Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());

		salesDtl.setQty(qty);

		salesDtl.setItemCode(txtItemcode.getText());
		Double mrpRateIncludingTax = 00.0;
		logger.info("Fetching tax");
		mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
		logger.info("Fetching tax finished");
		salesDtl.setMrp(mrpRateIncludingTax);
		if (null != currencyConversionMst)

		{
			// Double fcmrpIncludingTax = findFcRate(mrpRateIncludingTax,
			// currencyConversionMst);
			Double fcmrpIncludingTax = RestCaller.getCurrencyConvertedAmount(
					SystemSetting.getUser().getCompanyMst().getCurrencyName(),
					cmbCurrency.getSelectionModel().getSelectedItem(), mrpRateIncludingTax);
			salesDtl.setFcMrp(fcmrpIncludingTax);
		}
		Double taxRate = 00.0;

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
		salesDtl.setBarcode(item.getBarCode());
//		salesDtl.setUnitName(unitMst.getUnitName());
		salesDtl.setStandardPrice(item.getStandardPrice());
		if (null != currencyConversionMst) {
			// Double fcstdprice = findFcRate(item.getStandardPrice(),
			// currencyConversionMst);
			Double fcstdprice = RestCaller.getCurrencyConvertedAmount(
					SystemSetting.getUser().getCompanyMst().getCurrencyName(),
					cmbCurrency.getSelectionModel().getSelectedItem(), item.getStandardPrice());
			salesDtl.setFcStandardPrice(fcstdprice);
		}
//		if(null != item.getTaxRate())
//		{
//			salesDtl.setTaxRate(item.getTaxRate());
//			taxRate = item.getTaxRate();
//
//		} else {
//			salesDtl.setTaxRate(0.0);
//		}
		salesDtl.setItemId(item.getId());

		if (!txtBatch.getText().trim().isEmpty()) {
			ResponseEntity<List<ItemBatchExpiryDtl>> batchExpiryDtlResp = RestCaller
					.getItemBatchExpByItemAndBatch(salesDtl.getItemId(), salesDtl.getBatch());

			List<ItemBatchExpiryDtl> itemBatchExpiryDtlList = batchExpiryDtlResp.getBody();
			if (itemBatchExpiryDtlList.size() > 0 && null != itemBatchExpiryDtlList) {
				ItemBatchExpiryDtl itemBatchExpiryDtl = itemBatchExpiryDtlList.get(0);

				if (null != itemBatchExpiryDtl) {
					String expirydate = SystemSetting.UtilDateToString(itemBatchExpiryDtl.getExpiryDate(),
							"yyyy-MM-dd");
					salesDtl.setExpiryDate(java.sql.Date.valueOf(expirydate));
				}
			}
		}

		ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
		salesDtl.setUnitId(getUnit.getBody().getId());
		salesDtl.setUnitName(getUnit.getBody().getUnitName());

		// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());

		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.getBody().size() > 0) {
			for (TaxMst taxMst : getTaxMst.getBody()) {

				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";

				String companyCountry = SystemSetting.getUser().getCompanyMst().getCountry();
				if (null == companyCountry) {
					companyCountry = "INDIA";
				}
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {
					logger.info(e.toString());

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				String igstType = RestCaller.getIgstType(companyState, customerState, companyCountry);

				if (igstType.equalsIgnoreCase("CGSTSGST")) {

					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesDtl.setCgstTaxRate(taxMst.getTaxRate());

//						BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//						BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
					ResponseEntity<TaxMst> taxMst1 = RestCaller.getTaxMstByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
					if (null != taxMst1.getBody()) {

						salesDtl.setTaxRate(taxMst1.getBody().getTaxRate());
					}
				}

				else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesDtl.setCgstTaxRate(0.0);
						salesDtl.setCgstAmount(0.0);
						salesDtl.setSgstTaxRate(0.0);
						salesDtl.setSgstAmount(0.0);

						salesDtl.setTaxRate(taxMst.getTaxRate());
						salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//							BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setIgstAmount(igstAmount.doubleValue());
					}
				}
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesDtl.setCessRate(taxMst.getTaxRate());
//						BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCessAmount(cessAmount.doubleValue());
					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtl.setAddCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//							Double.valueOf(txtRate.getText()));
//					salesDtl.setAddCessAmount(cessAmount.doubleValue());
				}

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax)
					/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate()
							+ salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
			BigDecimal addcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
		}

		else {

// verificed

			if (getUnit.getBody().getId().equalsIgnoreCase(item.getUnitId())) {
				salesDtl.setStandardPrice(item.getStandardPrice());
			} else {

				ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),
						getUnit.getBody().getId());
				MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
				salesDtl.setStandardPrice(multiUnitMst.getPrice());
				if (null != currencyConversionMst) {
					// Double fcStandardPrice = findFcRate(multiUnitMst.getPrice(),
					// currencyConversionMst);
					Double fcStandardPrice = RestCaller.getCurrencyConvertedAmount(
							SystemSetting.getUser().getCompanyMst().getCurrencyName(),
							cmbCurrency.getSelectionModel().getSelectedItem(), multiUnitMst.getPrice());
					salesDtl.setFcStandardPrice(fcStandardPrice);
				}
			}
			if (null != item.getTaxRate()) {
				salesDtl.setTaxRate(item.getTaxRate());
				if (null != currencyConversionMst) {
					// Double fcTaxRate = findFcRate(item.getTaxRate(), currencyConversionMst);
					Double fcTaxRate = RestCaller.getCurrencyConvertedAmount(
							SystemSetting.getUser().getCompanyMst().getCurrencyName(),
							cmbCurrency.getSelectionModel().getSelectedItem(), item.getTaxRate());

					salesDtl.setFcTaxRate(fcTaxRate);
				}
				taxRate = item.getTaxRate();

			} else {
				salesDtl.setTaxRate(0.0);
				salesDtl.setFcTaxRate(0.0);
			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
			// if Discount

			// Calculate discount on base price
			if (null != salesTransHdr.getAccountHeads().getDiscountProperty()) {

				if (salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE")) {
					calcDiscountOnBasePrice(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);

				}
				if (salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP")) {
//				salesDtl.setTaxRate(0.0);
					calcDiscountOnMRP(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);
				}
				if (salesTransHdr.getAccountHeads().getDiscountProperty()
						.equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX")) {
					ambrossiaDiscount(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);
				}
			} else {

				double cessAmount = 0.0;
				double cessRate = 0.0;
				salesDtl.setRate(rateBeforeTax);
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (item.getCess() > 0) {
						cessRate = item.getCess();
						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

						salesDtl.setRate(rateBeforeTax);
						cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;
					} else {
						cessAmount = 0.0;
						cessRate = 0.0;
					}
					salesDtl.setRate(rateBeforeTax);
					if (null != currencyConversionMst) {
						// Double fcrateBeforeTax = findFcRate(rateBeforeTax, currencyConversionMst);
						Double fcrateBeforeTax = RestCaller.getCurrencyConvertedAmount(
								SystemSetting.getUser().getCompanyMst().getCurrencyName(),
								cmbCurrency.getSelectionModel().getSelectedItem(), rateBeforeTax);

						salesDtl.setFcRate(fcrateBeforeTax);
					}
					salesDtl.setCessRate(cessRate);
					salesDtl.setFcCessRate(0.0);
					salesDtl.setCessAmount(cessAmount);
					salesDtl.setFcCessAmount(0.0);
				}

				// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesDtl.setCgstTaxRate(cgstTaxRate);
				salesDtl.setFcCgst(0.0);
				salesDtl.setFcSgst(0.0);

				if (null != currencyConversionMst) {
					// Double fctaxRate = findFcRate(taxRate, currencyConversionMst);
					Double fctaxRate = RestCaller.getCurrencyConvertedAmount(
							SystemSetting.getUser().getCompanyMst().getCurrencyName(),
							cmbCurrency.getSelectionModel().getSelectedItem(), taxRate);

					salesDtl.setFcIgstRate(fctaxRate);
					Double igstamt = taxRate * salesDtl.getQty() * salesDtl.getRate() / 100;
					// Double fcIgstAmount = findFcRate(igstamt, currencyConversionMst);
					Double fcIgstAmount = RestCaller.getCurrencyConvertedAmount(
							SystemSetting.getUser().getCompanyMst().getCurrencyName(),
							cmbCurrency.getSelectionModel().getSelectedItem(), igstamt);

					salesDtl.setFcIgstAmount(fcIgstAmount);
				}
				salesDtl.setSgstTaxRate(sgstTaxRate);
				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {
					logger.info(e.toString());

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}
				String gstType = RestCaller.getGSTTypeByCustomerState(customerState);

				if (gstType.equalsIgnoreCase("GST")) {
					salesDtl.setSgstTaxRate(taxRate / 2);

					salesDtl.setCgstTaxRate(taxRate / 2);

					Double cgstAmt = 0.0, sgstAmt = 0.0;
					cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
					bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
					sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
					bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);

				} else {
					salesDtl.setSgstTaxRate(0.0);

					salesDtl.setCgstTaxRate(0.0);

					salesDtl.setCgstAmount(0.0);

					salesDtl.setSgstAmount(0.0);

					salesDtl.setIgstTaxRate(taxRate);
					salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				}
			}
		}
//			

		String companyState = SystemSetting.getUser().getCompanyMst().getState();
		String customerState = "KERALA";
		try {
			customerState = salesTransHdr.getAccountHeads().getCustomerState();
		} catch (Exception e) {
			logger.info(e.toString());

		}

		if (null == customerState) {
			customerState = "KERALA";
		}

		if (null == companyState) {
			companyState = "KERALA";
		}

		if (customerState.equalsIgnoreCase(companyState)) {
			salesDtl.setSgstTaxRate(taxRate / 2);

			salesDtl.setCgstTaxRate(taxRate / 2);

			Double cgstAmt = 0.0, sgstAmt = 0.0;
			cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
			bdcgstAmt = bdcgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setCgstAmount(bdcgstAmt.doubleValue());
			sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
			bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

			salesDtl.setIgstTaxRate(0.0);
			salesDtl.setIgstAmount(0.0);

		} else {
			salesDtl.setSgstTaxRate(0.0);

			salesDtl.setCgstTaxRate(0.0);

			salesDtl.setCgstAmount(0.0);

			salesDtl.setSgstAmount(0.0);

			salesDtl.setIgstTaxRate(taxRate);
			salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		}

		BigDecimal settoamount = new BigDecimal((Double.parseDouble(txtQty.getText()) * salesDtl.getRate()));

		double includingTax = (settoamount.doubleValue() * salesDtl.getTaxRate()) / 100;
		double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(setamount.doubleValue());
		if (null != currencyConversionMst) {
			// Double fcAmount = findFcRate(setamount.doubleValue(), currencyConversionMst);
			Double fcAmount = RestCaller.getCurrencyConvertedAmount(
					SystemSetting.getUser().getCompanyMst().getCurrencyName(),
					cmbCurrency.getSelectionModel().getSelectedItem(), setamount.doubleValue());

			salesDtl.setFcAmount(fcAmount);
		}
//		}
//		else
//		{
//			BigDecimal settoamount = new BigDecimal(
//					Double.parseDouble(txtQty.getText()) *salesDtl.getMrp() );
//			settoamount = settoamount.setScale(2, BigDecimal.ROUND_CEILING);
//			salesDtl.setAmount(settoamount.doubleValue());
//		}
//		if (getUnit.getBody().getId().equalsIgnoreCase(item.getUnitId())) {
//			salesDtl.setStandardPrice(item.getStandardPrice());
//		} else {
//
//			ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),
//					getUnit.getBody().getId());
//			MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
//			salesDtl.setStandardPrice(multiUnitMst.getPrice());
//
//		}
		salesDtl.setAddCessRate(0.0);
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp1 = RestCaller.getPriceDefenitionMstByName("MRP");
		PriceDefenitionMst priceDefenitionMst1 = priceDefenitionMstResp1.getBody();
		if (null != priceDefenitionMst1) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
					salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(), sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if (null != priceDefinition) {
				salesDtl.setMrp(priceDefinition.getAmount());
				if (null != currencyConversionMst) {
					// Double fcMrp = findFcRate(priceDefinition.getAmount(),
					// currencyConversionMst);
					Double fcMrp = RestCaller.getCurrencyConvertedAmount(
							SystemSetting.getUser().getCompanyMst().getCurrencyName(),
							cmbCurrency.getSelectionModel().getSelectedItem(), priceDefinition.getAmount());

					salesDtl.setFcMrp(fcMrp);
				}
			}
		}

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if (null != priceDefenitionMst) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
					salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if (null != priceDefinition) {
				salesDtl.setCostPrice(priceDefinition.getAmount());
			}
		}
		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		salesDtl = respentity.getBody();

//		//---------------offer---------------

		logger.info("Whole Sale Save sals Dtl COMPLETED!!");
		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());

		List<SalesDtl> salesDtlList = respentityList.getBody();

		/*
		 * Call Rest to get the summary and set that to the display fields
		 */

		// salesDtl.setTempAmount(amount);
		saleListTable.clear();
		saleListTable.setAll(salesDtlList);

		// ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		// salesDtl = respentity.getBody();

		// saleListTable.add(salesDtl);

		FillTable();

		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");

		cmbUnit.getSelectionModel().clearSelection();
		txtItemcode.setText("");
		txtBatch.setText("");
		txtBarcode.requestFocus();
		txtFCRate.clear();
		salesDtl = new SalesDtl();
		logger.info("====================Whole Sale ADD ITEM FINISHED!!================");

	}

	private void ambrossiaDiscount(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discoutAmount = (rateBeforeTax * salesTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(discoutAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			double newRate = rateBeforeTax - discoutAmount;
			salesDtl.setDiscount(discoutAmount);
			if (null != currencyConversionMst) {
				// Double fcDiscount = findFcRate(discoutAmount, currencyConversionMst);
				Double fcDiscount = RestCaller.getCurrencyConvertedAmount(
						SystemSetting.getUser().getCompanyMst().getCurrencyName(),
						cmbCurrency.getSelectionModel().getSelectedItem(), discoutAmount);

				salesDtl.setFcDiscount(fcDiscount);
			}
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BnewRate.doubleValue());
			if (null != currencyConversionMst) {
				// Double fcBnewRate = findFcRate(BnewRate.doubleValue(),
				// currencyConversionMst);
				Double fcBnewRate = RestCaller.getCurrencyConvertedAmount(
						SystemSetting.getUser().getCompanyMst().getCurrencyName(),
						cmbCurrency.getSelectionModel().getSelectedItem(), BnewRate.doubleValue());

				salesDtl.setFcRate(fcBnewRate);
			}
			BigDecimal BrateBeforeTax = new BigDecimal(rateBeforeTax);
			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
			// salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());
		} else {
			salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					if (null != currencyConversionMst) {
						// Double fcBrateAfterDiscount = findFcRate(BrateAfterDiscount.doubleValue(),
						// currencyConversionMst);
						Double fcBrateAfterDiscount = RestCaller.getCurrencyConvertedAmount(
								SystemSetting.getUser().getCompanyMst().getCurrencyName(),
								cmbCurrency.getSelectionModel().getSelectedItem(), BrateAfterDiscount.doubleValue());

						salesDtl.setFcRate(fcBrateAfterDiscount);
					}
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
					if (null != currencyConversionMst) {
						// Double fcrateBeforeTax = findFcRate(rateBeforeTax, currencyConversionMst);
						Double fcrateBeforeTax = RestCaller.getCurrencyConvertedAmount(
								SystemSetting.getUser().getCompanyMst().getCurrencyName(),
								cmbCurrency.getSelectionModel().getSelectedItem(), rateBeforeTax);

						salesDtl.setFcRate(fcrateBeforeTax);
					}
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setFcCessRate(0.0);
		salesDtl.setFcCessAmount(0.0);

	}

	private void calcDiscountOnBasePrice(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BrateAfterDiscount.doubleValue());
			if (null != currencyConversionMst) {
				// Double fcBrateAfterDiscount = findFcRate(BrateAfterDiscount.doubleValue(),
				// currencyConversionMst);
				Double fcBrateAfterDiscount = RestCaller.getCurrencyConvertedAmount(
						SystemSetting.getUser().getCompanyMst().getCurrencyName(),
						cmbCurrency.getSelectionModel().getSelectedItem(), BrateAfterDiscount.doubleValue());

				salesDtl.setFcRate(fcBrateAfterDiscount);
			}
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(rateBefrTax.doubleValue());
			// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
		} else {
			salesDtl.setRate(rateBeforeTax);
			if (null != currencyConversionMst) {
				// Double fcrateBeforeTax = findFcRate(rateBeforeTax, currencyConversionMst);
				Double fcrateBeforeTax = RestCaller.getCurrencyConvertedAmount(
						SystemSetting.getUser().getCompanyMst().getCurrencyName(),
						cmbCurrency.getSelectionModel().getSelectedItem(), rateBeforeTax);

				salesDtl.setFcRate(fcrateBeforeTax);
			}
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					if (null != currencyConversionMst) {
//						Double fcBrateAfterDiscount = findFcRate(BrateAfterDiscount.doubleValue(),
//								currencyConversionMst);
						Double fcBrateAfterDiscount = RestCaller.getCurrencyConvertedAmount(
								SystemSetting.getUser().getCompanyMst().getCurrencyName(),
								cmbCurrency.getSelectionModel().getSelectedItem(), BrateAfterDiscount.doubleValue());

						salesDtl.setFcRate(fcBrateAfterDiscount);
					}
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
					// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
					if (null != currencyConversionMst) {
						// Double fcrateBeforeTax = findFcRate(rateBeforeTax, currencyConversionMst);
						Double fcrateBeforeTax = RestCaller.getCurrencyConvertedAmount(
								SystemSetting.getUser().getCompanyMst().getCurrencyName(),
								cmbCurrency.getSelectionModel().getSelectedItem(), rateBeforeTax);

						salesDtl.setFcRate(fcrateBeforeTax);
					}
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setFcCessAmount(0.0);
		salesDtl.setFcCessRate(0.0);
	}

	private void calcDiscountOnMRP(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * mrpRateIncludingTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BnewrateBeforeTax.doubleValue());
			if (null != currencyConversionMst) {
				// Double fcBrateAfterDiscount = findFcRate(BnewrateBeforeTax.doubleValue(),
				// currencyConversionMst);
				Double fcBrateAfterDiscount = RestCaller.getCurrencyConvertedAmount(
						SystemSetting.getUser().getCompanyMst().getCurrencyName(),
						cmbCurrency.getSelectionModel().getSelectedItem(), BnewrateBeforeTax.doubleValue());

				salesDtl.setFcRate(fcBrateAfterDiscount);
			}
			// BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			// rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
			// salesDtl.setMrp(BrateAfterDiscount.doubleValue());
			// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
		} else {
			salesDtl.setRate(rateBeforeTax);
			if (null != currencyConversionMst) {
				// Double fcrateBeforeTax = findFcRate(rateBeforeTax, currencyConversionMst);
				Double fcrateBeforeTax = RestCaller.getCurrencyConvertedAmount(
						SystemSetting.getUser().getCompanyMst().getCurrencyName(),
						cmbCurrency.getSelectionModel().getSelectedItem(), rateBeforeTax);

				salesDtl.setFcRate(fcrateBeforeTax);
			}
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * mrpRateIncludingTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					if (null != currencyConversionMst) {
//						Double fcBrateAfterDiscount = findFcRate(BrateAfterDiscount.doubleValue(),
//								currencyConversionMst);
						Double fcBrateAfterDiscount = RestCaller.getCurrencyConvertedAmount(
								SystemSetting.getUser().getCompanyMst().getCurrencyName(),
								cmbCurrency.getSelectionModel().getSelectedItem(), BrateAfterDiscount.doubleValue());

						salesDtl.setFcRate(fcBrateAfterDiscount);
					}
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(mrpRateIncludingTax);
					// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
					if (null != currencyConversionMst) {
						// Double fcrateBeforeTax = findFcRate(rateBeforeTax, currencyConversionMst);
						Double fcrateBeforeTax = RestCaller.getCurrencyConvertedAmount(
								SystemSetting.getUser().getCompanyMst().getCurrencyName(),
								cmbCurrency.getSelectionModel().getSelectedItem(), rateBeforeTax);

						salesDtl.setFcRate(fcrateBeforeTax);
					}
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setFcCessAmount(0.0);
		salesDtl.setFcCessRate(0.0);

	}

	private void showPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			ItemStockPopupCtl itemStockPopupCtl = fxmlLoader.getController();
			itemStockPopupCtl.windowName = "WHOLESALE";
			root1 = (Parent) fxmlLoader.load();
			if (!custname.getText().trim().isEmpty()) {
				ItemStockPopupCtl itemStockPopupCtl1 = fxmlLoader.getController();
				itemStockPopupCtl1.getCustomer(custname.getText());
			}

			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
		txtQty.requestFocus();
	}

	private void loadCustomerPopup() {

		if (salesTransHdr != null) {

			Alert a = new Alert(AlertType.CONFIRMATION);
			a.setHeaderText("Changing Customer...");
			a.setContentText("The details will be deleted");
			a.showAndWait().ifPresent((btnType) -> {
				if (btnType == ButtonType.OK) {

					deleteAllSalesDtl();

					try {
//						txtLocalCustomer.clear();
						FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
						Parent root1;

						root1 = (Parent) fxmlLoader.load();
						Stage stage = new Stage();

						stage.initModality(Modality.APPLICATION_MODAL);
						stage.initStyle(StageStyle.UNDECORATED);
						stage.setTitle("ABC");
						stage.initModality(Modality.APPLICATION_MODAL);
						stage.setScene(new Scene(root1));
						stage.show();

						txtPatientName.requestFocus();

					} catch (IOException e) {
						//
						e.printStackTrace();
					}

				} else if (btnType == ButtonType.CANCEL) {

					return;

				}
			});
//			Boolean confirm = confirmMessage();

		} else {
			try {

//				txtLocalCustomer.clear();
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

				txtPatientName.requestFocus();

			} catch (IOException e) {
				//
				e.printStackTrace();
			}
		}
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
		double applicableDiscount = 0.0;
		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
		ItemMst item = new ItemMst();
		item = getItem.getBody();

//		item.setRank(item.getRank()+1);
//		RestCaller.updateRankItemMst(item);
		Stage stage = (Stage) btnAdditem.getScene().getWindow();

		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					cmbUnit.getItems().clear();
					itemNameProperty.set(itemPopupEvent.getItemName());
					// salesDtl.setItemName(itemNameProperty.get());
					txtBarcode.setText(itemPopupEvent.getBarCode());
					txtRate.setText(Double.toString(itemPopupEvent.getMrp()));

					cmbUnit.getItems().add(itemPopupEvent.getUnitName());
					cmbUnit.setValue(itemPopupEvent.getUnitName());
					batchProperty.set(itemPopupEvent.getBatch());
					ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsById(custId);

					if (null != cmbCurrency.getValue()) {

						ResponseEntity<CurrencyMst> currencyMstResp = RestCaller
								.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem().toString());
						CurrencyMst currencyMst = currencyMstResp.getBody();

						ResponseEntity<CurrencyConversionMst> getcurrencyConMst = RestCaller
								.getCurrencyConversionMstByCurrencyId(currencyMst.getId());
						currencyConversionMst = getcurrencyConMst.getBody();
					}
					if (null != currencyConversionMst) {
						// Double dfcRate = findFcRate(itemPopupEvent.getMrp(), currencyConversionMst);
						Double dfcRate = RestCaller.getCurrencyConvertedAmount(
								SystemSetting.getUser().getCompanyMst().getCurrencyName(),
								cmbCurrency.getSelectionModel().getSelectedItem(), itemPopupEvent.getMrp());

						txtFCRate.setText(Double.toString(dfcRate));
					}
					Date udate = SystemSetting.getApplicationDate();
					String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

					ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
							itemPopupEvent.getItemId(), accountHeads.getBody().getPriceTypeId(), itemPopupEvent.getUnitId(),
							itemPopupEvent.getBatch(), sdate);
					if (null != batchPriceDef.getBody()) {
						txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
						if (null != currencyConversionMst) {
							// Double dfcRate = findFcRate(batchPriceDef.getBody().getAmount(),
							// currencyConversionMst);
							Double dfcRate = RestCaller.getCurrencyConvertedAmount(
									SystemSetting.getUser().getCompanyMst().getCurrencyName(),
									cmbCurrency.getSelectionModel().getSelectedItem(),
									batchPriceDef.getBody().getAmount());

							txtFCRate.setText(Double.toString(dfcRate));
						}
					} else {
						ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
								itemPopupEvent.getItemId(), accountHeads.getBody().getPriceTypeId(),
								itemPopupEvent.getUnitId(), sdate);

						if (null != pricebyItem.getBody()) {

							// version 2.3
							txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
							// version2.3ends
							if (null != currencyConversionMst) {
								// Double dfcRate = findFcRate(pricebyItem.getBody().getAmount(),
								// currencyConversionMst);
								Double dfcRate = RestCaller.getCurrencyConvertedAmount(
										SystemSetting.getUser().getCompanyMst().getCurrencyName(),
										cmbCurrency.getSelectionModel().getSelectedItem(),
										pricebyItem.getBody().getAmount());

								txtFCRate.setText(Double.toString(dfcRate));
							}

							ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp2 = RestCaller
									.getPriceDefenitionMstByName("MRP");
							if (null != priceDefenitionMstResp2.getBody()) {
								if (!pricebyItem.getBody().getPriceId()
										.equalsIgnoreCase(priceDefenitionMstResp2.getBody().getId()))
									txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
								if (null != currencyConversionMst) {
									// Double dfcRate = findFcRate(pricebyItem.getBody().getAmount(),
									// currencyConversionMst);
									Double dfcRate = RestCaller.getCurrencyConvertedAmount(
											SystemSetting.getUser().getCompanyMst().getCurrencyName(),
											cmbCurrency.getSelectionModel().getSelectedItem(),
											pricebyItem.getBody().getAmount());

									txtFCRate.setText(Double.toString(dfcRate));
								}
//					else
//					{
//						txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
//					}
							}
						}
					}
					ResponseEntity<List<BatchPriceDefinition>> batchPrice = RestCaller.getBatchPriceDefinitionByItemId(
							itemPopupEvent.getItemId(), accountHeads.getBody().getPriceTypeId(), txtBatch.getText(), sdate);
					BatchpriceDefenitionList = FXCollections.observableArrayList(batchPrice.getBody());
					if (BatchpriceDefenitionList.size() > 0) {
						for (BatchPriceDefinition priceDef : BatchpriceDefenitionList) {
							ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
							if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
								cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
							}
						}
					}

					else {

						ResponseEntity<List<PriceDefinition>> price = RestCaller
								.getPriceByItemId(itemPopupEvent.getItemId(), accountHeads.getBody().getPriceTypeId());
						priceDefenitionList = FXCollections.observableArrayList(price.getBody());

						if (priceDefenitionList.size() > 0) {
							// cmbUnit.getItems().clear();
							for (PriceDefinition priceDef : priceDefenitionList) {
								ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
								if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
									cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
								}
							}
						}
					}

					ResponseEntity<TaxMst> taxMst = RestCaller.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(),
							"IGST");
					if (null != taxMst.getBody()) {
					}

					if (null != itemPopupEvent.getExpiryDate()) {
						// salesDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
					}

//			if(custMst.getBody().getCustomerDiscount()>0)
//			{
//				double discount;
//				discount =Double.parseDouble(txtRate.getText())*(custMst.getBody().getCustomerDiscount()/100);
//				applicableDiscount = Double.parseDouble(txtRate.getText())-discount;
//				txtRate.setText(Double.toString(applicableDiscount));
//			}
//			
					if (null != itemPopupEvent.getItemPriceLock()) {
						if (itemPopupEvent.getItemPriceLock().equalsIgnoreCase("YES")) {
							txtRate.setEditable(false);
						} else {
							txtRate.setEditable(true);
						}
					}

					System.out.println(itemPopupEvent.toString());
				}
			});
		}

	}

//	private Double findFcRate(Double rate, CurrencyConversionMst currencyConversionMst) {
//		Double fcRate = rate / currencyConversionMst.getConversionRate();
//		BigDecimal bdfcRate = new BigDecimal(fcRate);
//		bdfcRate = bdfcRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//		return bdfcRate.doubleValue();
//	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadsById(customerEvent.getCustId());
		AccountHeads account = new AccountHeads();
		account = getAccountHeads.getBody();
//		custmrMst.setRank(custmrMst.getRank()+1);
//		RestCaller.updateCustomerRank(custmrMst);

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

//			String customerSite = SystemSetting.customer_site_selection;
//			if (customerSite.equalsIgnoreCase("TRUE")) {
//				txtLocalCustomer.setDisable(false);
//				txtLocalCustomer.clear();
//			}

			String sdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
			custname.setText(customerEvent.getCustomerName());
			ResponseEntity<BranchMst> branchMst = RestCaller.getBranchMstByName(customerEvent.getCustomerName());

			if (null != branchMst.getBody()) {
				txtSalesType.setText("Branch Sales");
			} else {
				txtSalesType.setText("Customer Sales");
			}
			custAdress.setText(customerEvent.getCustomerAddress());
			gstNo.setText(customerEvent.getCustomerGst());
			custId = customerEvent.getCustId();

			ResponseEntity<AccountHeads> custMst = RestCaller.getAccountHeadsById(custId);
			if (null != custMst.getBody().getCurrencyId()) {
				ResponseEntity<CurrencyMst> getCurrency = RestCaller
						.getcurrencyMsyById(custMst.getBody().getCurrencyId());
				Platform.runLater(() -> {
					cmbCurrency.getSelectionModel().select(getCurrency.getBody().getCurrencyName());

				});

			}

			/*
			 * if customer is having discount then discount textfield will disabled
			 */
			if (account.getCustomerDiscount() > 0) {
				txtfcdiscount.setEditable(false);
				txtGrandTotal.setEditable(false);
			} else {
				txtfcdiscount.setEditable(true);
				txtGrandTotal.setEditable(true);
			}
			ResponseEntity<PriceDefenitionMst> priceDef = RestCaller
					.getPriceNameById(custMst.getBody().getPriceTypeId());
			if (null != priceDef.getBody()) {
				txtPriceType.setText(priceDef.getBody().getPriceLevelName());

			} else {
				txtPriceType.clear();

			}
			ResponseEntity<BranchMst> branchMstResp = RestCaller.getBranchMstByName(customerEvent.getCustomerName());

			if (null != branchMstResp.getBody()) {
				customerIsBranch = true;
			}

			// ResponseEntity<T>
			Double custBalance = RestCaller.getCustomerBalance(custId, sdate);
			BigDecimal bCustBalance = new BigDecimal(custBalance);
			bCustBalance = bCustBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			txtPreviousBalance.setText(bCustBalance.toString());
		}

	}

	// --------------------scheme------------------------------

	// -----------------------------------------------------Scheme

	// Check-------------------------------------------------------------------------------

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	private boolean isCustomerABranch(String customerName) {
		ArrayList branchRest = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		branchRest = RestCaller.SearchBranchLocalhost();
		Iterator itr1 = branchRest.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			System.out.println("---branchRest---" + lm);
			String branchName = (String) lm.get("branchName");
			if (customerName.equalsIgnoreCase(branchName)) {
				return true;
			}

		}

		return false;
	}

	@FXML
	void searchAction(ActionEvent event) {
	}

	@FXML
	void itemNameClick(MouseEvent event) {
		if (null == cmbSaleType.getSelectionModel() || null == cmbSaleType.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Select Voucher Type", false);
			cmbSaleType.requestFocus();
			return;
		}
		if (custname.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Customer", false);
			custname.requestFocus();
			return;
		}

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategorywiseItemSearch.fxml"));
			Parent root1;
			CategorywiseItemSearchCtl categorywiseItemSearchCtl = loader.getController();
			categorywiseItemSearchCtl.customerName = custname.getText();
			if (null != cmbSaleType.getSelectionModel()) {
				categorywiseItemSearchCtl.voucherType = cmbSaleType.getSelectionModel().getSelectedItem();
			}
			categorywiseItemSearchCtl.customerIsBranch = customerIsBranch;
//			categorywiseItemSearchCtl.txtLocalCustomer=txtLocalCustomer.getText();
			categorywiseItemSearchCtl.localCustId = localCustId;
			categorywiseItemSearchCtl.windowType = "WHOLESALE";
			if (null == salesTransHdr) {
				createSalesTransHdr();
				categorywiseItemSearchCtl.salesTransHdr = salesTransHdr;
			} else {
				categorywiseItemSearchCtl.salesTransHdr = salesTransHdr;
			}
			root1 = (Parent) loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root1));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

	}

	@Subscribe
	public void categoryItemSearchListener(CategorywiseItemSearchEvent categorywiseItemSearchEvent) {

		{
			salesTransHdr = categorywiseItemSearchEvent.getSalesTransHdrId();
			ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

			List<SalesDtl> salesDtlList = respentityList.getBody();

			/*
			 * Call Rest to get the summary and set that to the display fields
			 */

			// salesDtl.setTempAmount(amount);
			saleListTable.clear();
			saleListTable.setAll(salesDtlList);
			FillTable();
		}

	}

	private void createSalesTransHdr() {

		salesTransHdr = new SalesTransHdr();
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.getId();

		salesTransHdr.setVoucherType(cmbSaleType.getSelectionModel().getSelectedItem());
		salesTransHdr.setCustomerId(custId);

		logger.info("===========Whole Sale get customer by Id in Add item!!");
		
		/*
		 * new url for getting account heads instead of customer mst=========05/0/2022
		 */
//		ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerById(custId);
		ResponseEntity<AccountHeads> accountHeadsResponse=RestCaller.getAccountHeadsById(custId);
		AccountHeads accountHeads = accountHeadsResponse.getBody();
		if (null == accountHeads) {
			return;
		}
		salesTransHdr.setAccountHeads(accountHeads);
		if (null != cmbSalesMan.getSelectionModel()) {
			if (null != cmbSalesMan.getSelectionModel().getSelectedItem()) {
				ResponseEntity<AddKotWaiter> salesMan = RestCaller
						.getWaiterbyName(cmbSalesMan.getSelectionModel().getSelectedItem());
				salesTransHdr.setSalesManId(salesMan.getBody().getId());
			}
		}

		if (null != accountHeads.getCustomerDiscount()) {
			salesTransHdr.setDiscount((accountHeads.getCustomerDiscount().toString()));
		} else {
			salesTransHdr.setDiscount("0");
		}

		if (!txtPatientName.getText().trim().isEmpty()) {
			ResponseEntity<PatientMst> patientMstResp = RestCaller.getPatientMstByName(txtPatientName.getText());
			PatientMst patientMst = patientMstResp.getBody();
			if (null != patientMst) {
				salesTransHdr.setPatientMst(patientMst);
			}

		}

		/*
		 * If Customer Has a valid GST , then B2B Invoice , otherwise its B2C
		 */
		if (null == accountHeads.getPartyGst() || accountHeads.getPartyGst().length() < 13) {
			salesTransHdr.setSalesMode("B2C");
		} else {
			salesTransHdr.setSalesMode("B2B");
		}

		salesTransHdr.setCreditOrCash("CREDIT");
		salesTransHdr.setUserId(SystemSetting.getUser().getId());
		salesTransHdr.setBranchCode(SystemSetting.systemBranch);
		LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		Date date = SystemSetting.systemDate;
		salesTransHdr.setVoucherDate(date);
		if (customerIsBranch) {
			salesTransHdr.setIsBranchSales("Y");
		} else {
			salesTransHdr.setIsBranchSales("N");
		}
		logger.info("==============Whole Sale save Sales trans Hdr started!!");
		String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
		ResponseEntity<SalesTransHdr> getsales = RestCaller
				.getNullSalesTransHdrByCustomer(salesTransHdr.getCustomerId(), sdate, "PHARMACYCORPORATESALES");
		if (null != getsales.getBody()) {
			salesTransHdr = getsales.getBody();
		} else {
			ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
			logger.info("Whole Sale save Sales trans Hdr completed!!");
			salesTransHdr = respentity.getBody();
		}

	}

	@FXML
	void loadPatientPopUp(KeyEvent event) {
		try {

//			txtLocalCustomer.clear();
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/PatientPopUp.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			PatientPopUpCtl popupctl = fxmlLoader.getController();
			popupctl.PatientPopUpWithCustomer(custId);

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtItemname.requestFocus();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popPatientPopUpListener(PatientEvent patientEvent) {
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			txtPatientName.setText(patientEvent.getPatientName());
			txtPatientNationalId.setText(patientEvent.getNationalId());

		}
	}

	@FXML
	void fcDiscountOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (!txtfcdiscount.getText().trim().isEmpty()) {
				Double discountAmount = 0.0;
				if (txtfcdiscount.getText().trim().contains("%")) {
					if (!txtFCCashToPay.getText().trim().isEmpty()) {
						String[] discountPercent = txtfcdiscount.getText().split("%");

						Double dbDiscount = Double.parseDouble(discountPercent[0].toString());
						discountAmount = (Double.parseDouble(txtFCCashToPay.getText()) * dbDiscount) / 100;

						BigDecimal dbgtotal1 = new BigDecimal(discountAmount);
						dbgtotal1 = dbgtotal1.setScale(2, BigDecimal.ROUND_HALF_EVEN);

						txtDiscountAmount.setText(dbgtotal1.toString());
						salesTransHdr.setFcInvoiceDiscount(discountAmount);
						Double gtotal = Double.parseDouble(txtFCCashToPay.getText()) - discountAmount;
						BigDecimal dbgtotal = new BigDecimal(gtotal);
						dbgtotal = dbgtotal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
						txtGrandTotal.setText(dbgtotal.toPlainString());
					} else {
						String[] discountPercent = txtfcdiscount.getText().split("%");

						Double dbDiscount = Double.parseDouble(discountPercent[0].toString());
						discountAmount = (Double.parseDouble(txtCashtopay.getText()) * dbDiscount) / 100;

						BigDecimal dbgtotal1 = new BigDecimal(discountAmount);
						dbgtotal1 = dbgtotal1.setScale(2, BigDecimal.ROUND_HALF_EVEN);

						txtDiscountAmount.setText(dbgtotal1.toString());

						salesTransHdr.setFcInvoiceDiscount(discountAmount);
						Double gtotal = Double.parseDouble(txtCashtopay.getText()) - discountAmount;
						BigDecimal dbgtotal = new BigDecimal(gtotal);
						dbgtotal = dbgtotal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
						txtGrandTotal.setText(dbgtotal.toPlainString());
					}

				} else {
					if (!txtFCCashToPay.getText().trim().isEmpty()) {
						Double dbDiscount = Double.parseDouble(txtfcdiscount.getText());

						BigDecimal dbgtotal1 = new BigDecimal(dbDiscount);
						dbgtotal1 = dbgtotal1.setScale(2, BigDecimal.ROUND_HALF_EVEN);

						txtDiscountAmount.setText(dbgtotal1.toString());

						salesTransHdr.setFcInvoiceDiscount(dbDiscount);
						Double gtotal = Double.parseDouble(txtFCCashToPay.getText()) - dbDiscount;
						BigDecimal dbgtotal = new BigDecimal(gtotal);
						dbgtotal = dbgtotal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
						txtGrandTotal.setText(dbgtotal.toPlainString());
					} else {
						Double dbDiscount = Double.parseDouble(txtfcdiscount.getText());

						BigDecimal dbgtotal1 = new BigDecimal(dbDiscount);
						dbgtotal1 = dbgtotal1.setScale(2, BigDecimal.ROUND_HALF_EVEN);

						txtDiscountAmount.setText(dbgtotal1.toString());

						salesTransHdr.setFcInvoiceDiscount(dbDiscount);
						Double gtotal = Double.parseDouble(txtCashtopay.getText()) - dbDiscount;
						BigDecimal dbgtotal = new BigDecimal(gtotal);
						dbgtotal = dbgtotal.setScale(2, BigDecimal.ROUND_HALF_EVEN);
						txtGrandTotal.setText(dbgtotal.toPlainString());
					}
				}
			}
			btnSave.requestFocus();
		}

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

}
