package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;



public class AcceptStockReportCtl {
	String taskid;
	String processInstanceId;
	StockTransferInHdr stockTransferInHdr=null;
	private ObservableList<StockTransferInHdr> stockTransferInHdrList = FXCollections.observableArrayList();

	private ObservableList<StockTransferInDtl> stockTransferInDtlList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnDelete;
    


    @FXML
    private TableView<StockTransferInDtl> tblStockDtl;

    @FXML
    private TableColumn<StockTransferInDtl, String> clItemName;

    @FXML
    private TableColumn<StockTransferInDtl, String> clBatch;

    @FXML
    private TableColumn<StockTransferInDtl,Number> clQty;

    @FXML
    private TableColumn<StockTransferInDtl, String> clUnit;

    @FXML
    private TableColumn<StockTransferInDtl, Number> clRate;

    @FXML
    private TableColumn<StockTransferInDtl, Number> clAmount;

    @FXML
    private TableView<StockTransferInHdr> tblStockHdr;

    @FXML
    private TableColumn<StockTransferInHdr, String> clVoucherDate;
    @FXML
    private TableColumn<StockTransferInHdr,String> clInVoucherNo;

    @FXML
    private TableColumn<StockTransferInHdr, String> clVoucherNo;

    @FXML
    private TableColumn<StockTransferInHdr, String> clToBranch;

    @FXML
    private TableColumn<StockTransferInHdr, String> clIntent;
    @FXML
    private void initialize() 
    {
    	
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblStockHdr.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					stockTransferInHdr = new StockTransferInHdr();
					stockTransferInHdr.setId(newSelection.getId());
					ResponseEntity<List<StockTransferInDtl>> getStockDtl = RestCaller.getStockTransferInDtlByVoucherNo(newSelection.getVoucherNumber(),newSelection.getBranchCode());
				
					stockTransferInDtlList = FXCollections.observableArrayList(getStockDtl.getBody());
					fillDtlTable();
				}
			}
		});
    }
    private void fillDtlTable()
    {
    	tblStockDtl.setItems(stockTransferInDtlList);
    	for(StockTransferInDtl stk:stockTransferInDtlList)
    	{
//    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(stk.getItemId());
    		stk.setItemName(stk.getItemId().getItemName());
    		ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(stk.getUnitId());
    		stk.setUnitName(getUnit.getBody().getUnitName());
    	
    	BigDecimal amount = new BigDecimal(stk.getAmount());
    	amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    	stk.setAmount(amount.doubleValue());
    	}
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());




    }
    @FXML
    void actionShow(ActionEvent event) {
    	show();
    	
    }
    private void fillTable()
    {
    	tblStockHdr.setItems(stockTransferInHdrList);
		clToBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clIntent.setCellValueFactory(cellData -> cellData.getValue().getIntentNumberProperty());

		clInVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getInVoucherNumberProperty());
    }
    @FXML
    void deleteAction(ActionEvent event) {

    	if(null == stockTransferInHdr)
    	{
    		return;
    	}
    	if(null == stockTransferInHdr.getId())
    	{
    		return;
    	}
    	RestCaller.deleteStockTransferInHdr(stockTransferInHdr.getId());
    	notifyMessage(3, "Deleted");
    	
    	stockTransferInHdr = null;
    	show();
    }
    private void show()
    {
    	tblStockDtl.getItems().clear();
    	tblStockHdr.getItems().clear();
    	LocalDate dpDate1 = dpFromDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strfDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 
			LocalDate dpDate2 = dpToDate.getValue();
			java.util.Date utDate  = SystemSetting.localToUtilDate(dpDate2);
			 String  strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
    	ResponseEntity<List<StockTransferInHdr>> getStockOutHdr = RestCaller.getStockTransferInHdrBetweenDate(strfDate,strtDate);
    	stockTransferInHdrList = FXCollections.observableArrayList(getStockOutHdr.getBody());
    	fillTable();
    }
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}

}
