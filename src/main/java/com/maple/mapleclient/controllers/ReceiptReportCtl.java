package com.maple.mapleclient.controllers;

import java.sql.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportReceiptReport;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import net.sf.jasperreports.engine.JRException;

public class ReceiptReportCtl {
	
	String taskid;
	String processInstanceId;
	

	ExportReceiptReport  exportReceiptToExcel =new ExportReceiptReport();
	
	
	private ObservableList<ReceiptHdr> receiptList = FXCollections.observableArrayList();
	private ObservableList<ReceiptDtl> receipteDtlLists = FXCollections.observableArrayList();
	
	    @FXML
	    private DatePicker dpFromDate;
	    @FXML
	    private TableView<ReceiptHdr> tbReport;

	    @FXML
	    private TableColumn<ReceiptHdr, String> clVoucherDate;

	    @FXML
	    private TableColumn<ReceiptHdr, String> clVoucherNumber;

	    @FXML
	    private TableColumn<ReceiptHdr, String> clumnTransDate;

	    @FXML
	    private TableColumn<ReceiptHdr, String> clumnVoucherType;

	    
	    

	    @FXML
	    private Button btnShow;

	    @FXML
	    private TableView<ReceiptDtl> tbreports;

	    @FXML
	    private TableColumn<ReceiptDtl, String> clAccountName;

	    @FXML
	    private TableColumn<ReceiptDtl, String> clModeOfPayment;

	    @FXML
	   private TableColumn<ReceiptDtl, Number> clAmount;
	    
	    @FXML
	    private TableColumn<ReceiptDtl, String> clBankAccountNumber;

	    @FXML
	    private TableColumn<ReceiptDtl, String> clDepositBank;
	    @FXML
	    private ComboBox<String> branchselect;
	    
	    
	    @FXML
	    private TableColumn<ReceiptDtl, String> clRemark;

	    
	    String voucherNumber;
	    String voucherDate;
	    @FXML
	    private Button btnPrint;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private Button btnExcelExport;

	    @FXML
	    void exportToExcel(ActionEvent event) {
	    	

	    	if(null == branchselect.getValue())
	    	{
	    		notifyMessage(5, "Please select branch", false);
	    		return;
	    	} 
	    	
	    	if(null == dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    	} 
	    	
	    	if(null == dpToDate.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	} 
	    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date toDate = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
	    	
			ArrayList receiptImport = new ArrayList();
  			if( branchselect.getSelectionModel().isEmpty())
  			{
  				return;
  			}
  			
  			System.out.println(receiptImport.size()+"receiptReportListSize issssssssssss");
  			receiptImport = RestCaller.exportReceiptReport(branchselect.getSelectionModel().getSelectedItem(),startDate);	
  			exportReceiptToExcel.exportToExcel("ReceiptReport"+branchselect.getSelectionModel().getSelectedItem()+startDate+endDate+".xls", receiptImport);
			
			
	    }

	    @FXML
	    private Button ok;
	    

	    @FXML
	    void reportPrint(ActionEvent event) {

	    	if(null == branchselect.getValue())
	    	{
	    		notifyMessage(5, "Please select branch", false);
	    		return;
	    	} 
	    	
	    	if(null == dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    	} 
	    	
	    	if(null == dpToDate.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	} 
	    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date toDate = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
	    	
			ArrayList receiptImport = new ArrayList();
  			if( branchselect.getSelectionModel().isEmpty())
  			{
  				return;
  			}
  			
		
		  try { 
			  
			  JasperPdfReportService.ReceiptInvoice(voucherNumber, voucherDate);
			/*
			 * JasperPdfReportService.ReceiptReportPrintinge(
			 * branchselect.getSelectionModel().getSelectedItem(),startDate, endDate);
			 */
			  }
		  catch (JRException e) { // TODO Auto-generated catch block
		  e.printStackTrace(); }
		 
  			
  			
  			
	    	
	    }


	    @FXML
	    void onClickOk(ActionEvent event) {

	    }

	    
	    @FXML
	    void show(ActionEvent event) {
	    	tbReport.getItems().clear();
	    	tbreports.getItems().clear();
	  //  	 totalamount = 0.0;
	  //  	 grandTotal = 0.0;
	    	
	    	if(null == branchselect.getValue())
	    	{
	    		notifyMessage(5, "Please select branch", false);
	    		return;
	    	} 
	    	
	    	if(null == dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    	} 
	    	
	    	if(null == dpToDate.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	} 
	    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date toDate = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
	    	ResponseEntity<List<ReceiptHdr>> receiptHdrRep = RestCaller.getAllReceiptHdrByDate( startDate, endDate,branchselect.getValue());
	    	receiptList = FXCollections.observableArrayList(receiptHdrRep.getBody());
	    	

	    	FillTable();
	    	
	    
	    }

	    @FXML
	 	private void initialize() {
	    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
	    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
	    	setBranches();
	    	
	    	  tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
		    		if (newSelection != null) {
		    			System.out.println("getSelectionModel");
		    			if (null != newSelection.getId()) {
		    				System.out.println("getSelectionModel--getVoucherNumber");
		    				 String voucher=newSelection.getVoucherNumber();
		    				System.out.println(voucher);
		    				 voucherNumber=newSelection.getVoucherNumber();
		    				 
		    				 java.util.Date vDate=newSelection.getVoucherDate();
		    				  voucherDate=SystemSetting.UtilDateToString(vDate, "yyy-MM-dd");
		    				 
		    				 
		    				String receiptid=newSelection.getId();
		    				
		    				System.out.println("........" + voucherNumber);
		    				String branchName=branchselect.getValue();
		    				ResponseEntity<List<ReceiptDtl>> receiptDtlList = RestCaller.getReceiptReportDtl(receiptid);
		    				receipteDtlLists = FXCollections.observableArrayList(receiptDtlList.getBody());
		    				tbreports.setItems(receipteDtlLists);
		    				System.out.print(receipteDtlLists.size()+"receipt detail list size issssssss");
		    				
		    				System.out.print("after filltable");
		    				
		    				FillTables();
		    			}
		    			
		    		}
		    	});
	    }
	    
	    private void setBranches() {
			
			ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
			List<BranchMst> branchMstList = new ArrayList<BranchMst>();
			branchMstList = branchMstRep.getBody();
			
			for(BranchMst branchMst : branchMstList)
			{
				branchselect.getItems().add(branchMst.getBranchCode());
			
				
			}
			 
		}

		private void FillTable()
		{
			tbReport.setItems(receiptList);
		
		
		
			clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
			clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
			clumnTransDate.setCellValueFactory(cellData -> cellData.getValue().getTransDateProperty());
			clumnVoucherType.setCellValueFactory(cellData -> cellData.getValue().getVoucherTypeProperty());
			
		}
	    
	
	  private void FillTables() {
	  
	  System.out.print("inside fill table isssssssssssssssssssssss");
	    clAccountName.setCellValueFactory(cellData -> cellData.getValue().getaccountProperty());
		clModeOfPayment.setCellValueFactory(cellData -> cellData.getValue().getmodeOfPaymentProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountPropery());
		clDepositBank.setCellValueFactory(cellData -> cellData.getValue().getdepositBankProperty());
		clBankAccountNumber.setCellValueFactory(cellData -> cellData.getValue().getbankAccountNumberProperty());
		clRemark.setCellValueFactory(cellData -> cellData.getValue().getremarkProperty());
		
	  
	  
	  
	  }
	  
	    public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}
	    }
	    @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }

}
