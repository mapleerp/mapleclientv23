package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AccountBalanceByAccountIdCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<AccountBalanceReport> accountBalanceList = FXCollections.observableArrayList();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");

	
	String accountId = null;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerate;

    @FXML
    private TableView<AccountBalanceReport> tblAccountBalance;

    @FXML
    private TableColumn<AccountBalanceReport, LocalDate> clDate;

    @FXML
    private TableColumn<AccountBalanceReport, String> clInvoiceNo;

    @FXML
    private TableColumn<AccountBalanceReport, Number> clDebitAmount;

    @FXML
    private TableColumn<AccountBalanceReport, Number> clCreditAmount;

    @FXML
    private Button btnBack;
    
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblAccountBalance.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getAccountHeads() && 
						!newSelection.getAccountHeads().equalsIgnoreCase("SALES ACCOUNT")) {
					
					String[] invoiceNo = newSelection.getRemark().split("Sales");
					String voucherNo = invoiceNo[1];
					System.out.println(voucherNo);
					System.out.println(newSelection.getDate());
					findByVoucherNoAndDate(voucherNo,newSelection.getDate());
				
					
				}
			}
		});
    	
    }

    private void findByVoucherNoAndDate(String voucherNo, java.sql.Date date) {
		
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/WholeSaleInvoiceEdit.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			WholeSaleInvoiceEdit invoiceEdit =fxmlLoader.getController();
			invoiceEdit.getVoucherNoAndDate(voucherNo,date,accountId);
   			Stage stage = new Stage();
   			stage.setScene(new Scene(root1));
   			stage.initModality(Modality.APPLICATION_MODAL);
   			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
		
	}

	@FXML
    void Back(ActionEvent event) {
    	
    	Stage stage = (Stage) btnBack.getScene().getWindow();
		stage.close();
		

    }
	
	  @FXML
	    void EscOnKeyPress(KeyEvent event) {
		  if (event.getCode() == KeyCode.ESCAPE) {
			  Stage stage = (Stage) btnBack.getScene().getWindow();
				stage.close();

			}
	    }

	    @FXML
	    void EscOnKeyPress1(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ESCAPE) {
	    		 Stage stage = (Stage) btnBack.getScene().getWindow();
	    			stage.close();

				}
	    }

	    @FXML
	    void EscOnKeyPress2(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ESCAPE) {
	    		 Stage stage = (Stage) btnBack.getScene().getWindow();
	    			stage.close();

				}
	    }


    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, " Please select from date...!!!",false);
			return;
    	}
    	
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, " Please select to date...!!!",false);
			return;
    	}

    	Date sDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sDate, "yyyy-MM-dd");
		
		Date eDate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(eDate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller.getAccountBalanceReport(startDate,
				endDate, accountId);
    	
    	accountBalanceList = FXCollections.observableArrayList(accountBalanceReport.getBody());
    	
    	fillTable();
    	
    	
    }
    
    private void fillTable() {
		
    	tblAccountBalance.setItems(accountBalanceList);
    	
    	clCreditAmount.setCellValueFactory(cellData -> cellData.getValue().getCreditProperty());

		clDebitAmount.setCellValueFactory(cellData -> cellData.getValue().getDebitProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getdateProperty());

		clInvoiceNo.setCellValueFactory(cellData -> cellData.getValue().getRemarkProperty());
		
		clDebitAmount.setCellFactory(tc-> new TableCell<AccountBalanceReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		clCreditAmount.setCellFactory(tc-> new TableCell<AccountBalanceReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
	}

	public void setAccountId(String account) {
    	
    	accountId = account;
    	
  
    		
        	LocalDate date = LocalDate.now();
        	Date nDate = SystemSetting.localToUtilDate(date);
        	String endDate = SystemSetting.UtilDateToString(nDate, "yyyy-MM-dd");
        	
        	LocalDate lastMonth = date.minusMonths(1);
        	Date lDate = SystemSetting.localToUtilDate(lastMonth);
        	String startDate = SystemSetting.UtilDateToString(lDate, "yyyy-MM-dd");
        	
        
        	
        	ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller.getAccountBalanceReport(startDate,
    				endDate, accountId);
        	
        	accountBalanceList = FXCollections.observableArrayList(accountBalanceReport.getBody());
        	
        	fillTable();
		
	}
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}

}
