package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.TSCBarcode;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PhysicalStockDtl;
import com.maple.mapleclient.entity.ProductConversionConfigMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ProductConversionCofigCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<ProductConversionConfigMst> productConversionConfigList = FXCollections.observableArrayList();

	 
private static final Logger logger = LoggerFactory.getLogger(TSCBarcode.class);


	private EventBus eventBus = EventBusFactory.getEventBus();
	ProductConversionConfigMst productConversionConfigMst = null;

    @FXML
    private TextField txtFromItem;

    @FXML
    private TextField txtToItem;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;
    @FXML
    private TableView<ProductConversionConfigMst> tblConfigMst;

    @FXML
    private TableColumn<ProductConversionConfigMst, String> clFromItem;

    @FXML
    private TableColumn<ProductConversionConfigMst, String> clToItem;
    @FXML
    private Button btnShowAll;
	@FXML
	private void initialize() {
		eventBus.register(this);
		tblConfigMst.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getId()) {
					
					productConversionConfigMst = new ProductConversionConfigMst();

					productConversionConfigMst.setId(newSelection.getId());
					
				}
			}
		});
	}
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == productConversionConfigMst)
    	{
    		return;
    	}
    	if(null == productConversionConfigMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteProductConversionConfig( productConversionConfigMst.getId());
    	showAll();
    }
    
    private void showAll()
    {
    	ResponseEntity<List<ProductConversionConfigMst>> getProductConfig = RestCaller.getAllProductConversionConfigMst();
    	productConversionConfigList = FXCollections.observableArrayList(getProductConfig.getBody());
    	fillTable();
    }
    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    private void addItem()
    {
	if(txtFromItem.getText().trim().isEmpty())
	{
		notifyMessage(3,"Type From ItemName");
		txtFromItem.requestFocus();
		return;
		
	}
	if(txtToItem.getText().trim().isEmpty())
	{
		notifyMessage(3,"Type To ItemName");
		txtToItem.requestFocus();
		return;
		
	}
	productConversionConfigMst = new ProductConversionConfigMst();
	ResponseEntity<ItemMst> getfromItem = RestCaller.getItemByNameRequestParam(txtFromItem.getText());
	productConversionConfigMst.setFromItemId(getfromItem.getBody().getId());
	ResponseEntity<ItemMst> gettoItem = RestCaller.getItemByNameRequestParam(txtToItem.getText());
	productConversionConfigMst.setToItemId(gettoItem.getBody().getId());
	String vId = RestCaller.getVoucherNumber("PRDCONFIG");
	productConversionConfigMst.setId(vId);
	productConversionConfigMst.setBranchCode(SystemSetting.systemBranch);
	ResponseEntity<ProductConversionConfigMst> getSavedProduct = RestCaller.getProductConversionConfigMstWithItemIds(productConversionConfigMst.getFromItemId(),productConversionConfigMst.getToItemId());
	if(null != getSavedProduct.getBody())
	{
		notifyMessage(5,"Already Saved");
		txtFromItem.clear();
		txtToItem.clear();
		return;
	}
	ResponseEntity<ProductConversionConfigMst> respentity = RestCaller.saveProductConversionConfig(productConversionConfigMst);
	productConversionConfigMst = respentity.getBody();
	productConversionConfigList.add(productConversionConfigMst);
	fillTable();
	txtFromItem.clear();
	txtToItem.clear();
	txtFromItem.requestFocus();
    }
    @FXML
    void actionSave(ActionEvent event) {
    	addItem();
    }
    @FXML
    void saveOnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		addItem();
    	}
    }
    private void fillTable()
    {
    	tblConfigMst.setItems(productConversionConfigList);
    	for (ProductConversionConfigMst prodct:productConversionConfigList)
    	{
    		ResponseEntity<ItemMst> getFItem = RestCaller.getitemMst(prodct.getFromItemId());
    		ResponseEntity<ItemMst> getToItem = RestCaller.getitemMst(prodct.getToItemId());
    		if(null != getFItem.getBody())
    		{
    		prodct.setFromItemName(getFItem.getBody().getItemName());
    		}
    		if(null != getToItem.getBody())
    		{
    		prodct.setToItemName(getToItem.getBody().getItemName());
    		}
    	}
    	clFromItem.setCellValueFactory(cellData -> cellData.getValue().getfromItemNameProperty());
    	clToItem.setCellValueFactory(cellData -> cellData.getValue().gettoItemNameProperty());
    }
    @FXML
    void actionShowAll(ActionEvent event) {
    	showAll();
    }
    @FXML
    void loadFromItemKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		showItemMst();
    	}
    }
    @FXML
    void loadtoItemKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		 ShowItemPopup();
    	}
    }

    @FXML
    void loadFromItemPopUp(MouseEvent event) {
    	showItemMst();
    }
    @FXML
    void loadToItemPopUp(MouseEvent event) {
    	 ShowItemPopup();
    }
    private void showItemMst() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtToItem.requestFocus();
	}

	private void ShowItemPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		btnSave.requestFocus();
	}
	
	 @Subscribe
	 @AllowConcurrentEvents
		public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		 try {
			System.out.println("-------------popupItemlistner-------------");
			Stage stage = (Stage) btnDelete.getScene().getWindow();
			if (stage.isShowing()) {
				
				
				txtToItem.setText(itemPopupEvent.getItemName());

			}
			
		 } catch(Exception e){
             logger.error(e.getMessage());
         }

		 
		}
	 
	 @AllowConcurrentEvents
    @Subscribe
	public void popupItemlistner(KitPopupEvent kitPopupEvent) {

		 try {
		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnDelete.getScene().getWindow();
		if (stage.isShowing()) {
			
			
			txtFromItem.setText(kitPopupEvent.getItemName());

		}
		 } catch(Exception e){
             logger.error(e.getMessage());
         }
	}
	 @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }
}
