package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.LmsQueueMst;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class LmsQueuPendingPopUpCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<LmsQueueMst> LmsQueueMstList = FXCollections.observableArrayList();

    @FXML
    private TableView<LmsQueueMst> tbLmsQueue;

    @FXML
    private TableColumn<LmsQueueMst, String> clVoucherDate;

    @FXML
    private TableColumn<LmsQueueMst, String> clVoucherNumber;

    @FXML
    private TableColumn<LmsQueueMst,String> clParticulars;
    @FXML
	private void initialize() {
    	
    	cmbStatus.getItems().add("NO");
    	cmbStatus.getItems().add("YES");

    	ResponseEntity<List<LmsQueueMst>> getPedingItems = RestCaller.getpendindLmsQueueMst();
    	LmsQueueMstList = FXCollections.observableArrayList(getPedingItems.getBody());
    	tbLmsQueue.setItems(LmsQueueMstList);
		clParticulars.setCellValueFactory(cellData -> cellData.getValue().getVoucherTypeProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());


		ResponseEntity<List<String>> lmsQueueMstListResp = RestCaller.findLmsQueueMstDistinctVoucherTypes();
		
		List<String> voucherTypeLists = lmsQueueMstListResp.getBody();
		for(String voucherType : voucherTypeLists)
		{
			cmbVoucherType.getItems().add(voucherType);
		}
    }
    
    
    @FXML
    private ComboBox<String> cmbVoucherType;

    @FXML
    private ComboBox<String> cmbStatus;

    @FXML
    private DatePicker dpStartDate;

    @FXML
    private DatePicker dpEndDate;

    @FXML
    private Button btnFetchDetails;

    @FXML
    private Button btnReTry;

    @FXML
    void FetchDetails(ActionEvent event) {
    	
    	if(null == cmbStatus.getValue())
    	{
    		notifyMessage(3, "Please select Satus", false);
    		return;
    	}

    	if(null == cmbVoucherType.getValue())
    	{
    		notifyMessage(3, "Please select Voucher type", false);
    		return;

    	}
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(3, "Please select start date", false);
    		return;

    	}
    	
    	if(null == dpEndDate.getValue())
    	{
    		notifyMessage(3, "Please select end date", false);
    		return;

    	}
    	
    	
    	String status = cmbStatus.getSelectionModel().getSelectedItem().toString();
    	String voucherType = cmbVoucherType.getSelectionModel().getSelectedItem().toString();

    	java.util.Date sDate = Date.valueOf(dpStartDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sDate, "yyy-MM-dd");
		
		java.util.Date eDate = Date.valueOf(dpEndDate.getValue());
		String endtDate = SystemSetting.UtilDateToString(eDate, "yyy-MM-dd");
    	
		tbLmsQueue.getItems().clear();
    	ResponseEntity<List<LmsQueueMst>> getPedingItems = RestCaller.
    			getLmsQueueMstByVoucherTypeAndStatus(voucherType,status,startDate,endtDate);
    	
    	LmsQueueMstList = FXCollections.observableArrayList(getPedingItems.getBody());
    	tbLmsQueue.setItems(LmsQueueMstList);


    }

    @FXML
    void RetryAction(ActionEvent event) {
    	
    	if(null == cmbStatus.getValue())
    	{
    		notifyMessage(3, "Please select Satus", false);
    		return;
    	}

    	if(null == cmbVoucherType.getValue())
    	{
    		notifyMessage(3, "Please select Voucher type", false);
    		return;

    	}
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(3, "Please select start date", false);
    		return;

    	}
    	
    	if(null == dpEndDate.getValue())
    	{
    		notifyMessage(3, "Please select end date", false);
    		return;

    	}
    	
    	
    	String status = cmbStatus.getSelectionModel().getSelectedItem().toString();
    	String voucherType = cmbVoucherType.getSelectionModel().getSelectedItem().toString();

    	java.util.Date sDate = Date.valueOf(dpStartDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sDate, "yyy-MM-dd");
		
		java.util.Date eDate = Date.valueOf(dpEndDate.getValue());
		String endtDate = SystemSetting.UtilDateToString(eDate, "yyy-MM-dd");
    	
		
    	ResponseEntity<String> getPedingItems = RestCaller.
    			getLmsQueueMstRetry(voucherType,status,startDate,endtDate);
    	
    	tbLmsQueue.getItems().clear();

    }
    
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
