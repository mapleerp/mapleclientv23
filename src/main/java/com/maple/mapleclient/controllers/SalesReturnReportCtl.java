package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DailySalesReturnReportDtl;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import net.sf.jasperreports.engine.JRException;

public class SalesReturnReportCtl {
	
	String taskid;
	String processInstanceId;
	
	double totalamount = 0.0;
	String customer = null;
	String voucher = null;
	String vDate = null;
	private ObservableList<DailySalesReturnReportDtl> dailySalesList = FXCollections.observableArrayList();
	private ObservableList<VoucherReprintDtl> dailySalesLists = FXCollections.observableArrayList();
    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrintInvoice;

    @FXML
    private TableView<DailySalesReturnReportDtl> tbReport;

    @FXML
    private TableColumn<DailySalesReturnReportDtl, String> clVoucherNo;

    @FXML
    private TableColumn<DailySalesReturnReportDtl, LocalDate> clDate;

    @FXML
    private TableColumn<DailySalesReturnReportDtl,String> clCustomer;

    @FXML
    private TableColumn<DailySalesReturnReportDtl,Number> clamt;

    @FXML
    private TextField txtGrandTotal;

    @FXML
    private TableView<VoucherReprintDtl> tbreports;

    @FXML
    private TableColumn<VoucherReprintDtl, String> voucherNo;

    @FXML
    private TableColumn<VoucherReprintDtl, String> Customername;

    @FXML
    private TableColumn<VoucherReprintDtl, String> itemName;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> qty;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> clRate;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> taxRate;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> cessRate;

    @FXML
    private TableColumn<VoucherReprintDtl, String> unit;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> amount;
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	setBranches();
    	 tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	    		if (newSelection != null) {
	    			System.out.println("getSelectionModel");
	    			if (null != newSelection.getVoucherNumber()) {
	    			
	    				String voucherNumber=newSelection.getVoucherNumber();
	    				customer = newSelection.getCustomerName();
	    				voucher = newSelection.getVoucherNumber();
	    				java.sql.Date uDate = newSelection.getvoucherDate();
	    	    		 vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
	    		    
	    				 String branchName=cmbBranch.getValue();
	    				ResponseEntity<List<VoucherReprintDtl>> dailysaless = RestCaller.getSalesReturnVoucherReprintDtl(newSelection.getVoucherNumber(),branchName);
	    		    	dailySalesLists = FXCollections.observableArrayList(dailysaless.getBody());
	    		    	tbreports.setItems(dailySalesLists);
	    		    	
	    		    	FillTable();
	    			}
	    		}
	    	});
    }
    private void FillTable()
	{
		
		amount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Customername.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
    	voucherNo.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
    	qty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clRate.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
    	taxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
    	cessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
    	unit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
    	itemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	
    	
	}
    @FXML
    void printInvoice(ActionEvent event) {
    	try {
			JasperPdfReportService.SalesReturnTaxInvoiceReport(voucher, vDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    } 
	
    @FXML
    void actionShow(ActionEvent event) {
    	tbreports.getItems().clear();
   	 totalamount = 0.0;
 	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		//uDate = SystemSetting.StringToUtilDate(strDate,);
 	ResponseEntity<List<DailySalesReturnReportDtl>> dailysales = RestCaller.getDailySaleReturnReportAll(cmbBranch.getSelectionModel().getSelectedItem(), strDate,strDate1);
 	dailySalesList = FXCollections.observableArrayList(dailysales.getBody());
 	tbReport.setItems(dailySalesList);
 	clamt.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
 	clCustomer.setCellValueFactory(cellData -> cellData.getValue().getcustomerNameProperty());
 	clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
 	clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
 	
 	for(DailySalesReturnReportDtl dailySaleReportDtl:dailySalesList)
 	{
 		totalamount = totalamount + dailySaleReportDtl.getAmount();
 	}
 	BigDecimal settoamount = new BigDecimal(totalamount);
		settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
 	txtGrandTotal.setText(settoamount.toString());
    }
private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			cmbBranch.getItems().add(branchMst.getBranchCode());
		
			
		}
		 
	}

@Subscribe
public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	//Stage stage = (Stage) btnClear.getScene().getWindow();
	//if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();
		
	 
		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);
		
		 PageReload(hdrId);
	}


private void PageReload(String hdrId) {

}
}
