package com.maple.mapleclient.controllers;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ConsumptionReportDtl {

	   private String id;
		String itemId;
		String unitId;
		Double mrp;
		Double qty;
		Double amount;
		String batch;
		String reasonId;
		@JsonIgnore
		StringProperty itemIdProperty;
		@JsonIgnore
		StringProperty unitIdProperty;
		@JsonIgnore
		DoubleProperty mrpProperty;
		@JsonIgnore
		DoubleProperty qtyProperty;
		@JsonIgnore
		DoubleProperty amountProperty;
		@JsonIgnore
		StringProperty batchProperty;
		@JsonIgnore
		StringProperty reasonIdProperty;
		
		ConsumptionReportDtl(){
			this.itemIdProperty = new SimpleStringProperty();
			this.unitIdProperty = new SimpleStringProperty();
			this.batchProperty =new SimpleStringProperty();
			this.reasonIdProperty=new SimpleStringProperty();
			this.amountProperty=new SimpleDoubleProperty();
			this.qtyProperty=new SimpleDoubleProperty();
			this.mrpProperty=new SimpleDoubleProperty();
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getItemId() {
			return itemId;
		}
		public void setItemId(String itemId) {
			this.itemId = itemId;
		}
		public String getUnitId() {
			return unitId;
		}
		public void setUnitId(String unitId) {
			this.unitId = unitId;
		}
		public Double getMrp() {
			return mrp;
		}
		public void setMrp(Double mrp) {
			this.mrp = mrp;
		}
		public Double getQty() {
			return qty;
		}
		public void setQty(Double qty) {
			this.qty = qty;
		}
		public Double getAmount() {
			return amount;
		}
		public void setAmount(Double amount) {
			this.amount = amount;
		}
		public String getBatch() {
			return batch;
		}
		public void setBatch(String batch) {
			this.batch = batch;
		}
		public String getReasonId() {
			return reasonId;
		}
		public void setReasonId(String reasonId) {
			this.reasonId = reasonId;
		}
		@JsonIgnore
		public StringProperty getItemIdProperty() {
			itemIdProperty.set(itemId);
			return itemIdProperty;
		}

		public void setItemIdProperty(StringProperty itemIdProperty) {
			this.itemIdProperty = itemIdProperty;
		}
		@JsonIgnore
		public StringProperty getUnitIdProperty() {
			unitIdProperty.set(unitId);
			return unitIdProperty;
		}

		public void setUnitIdProperty(StringProperty unitIdProperty) {
			this.unitIdProperty = unitIdProperty;
		}
		@JsonIgnore
		public DoubleProperty getMrpProperty() {
			
			mrpProperty.set(mrp);
			return mrpProperty;
			
		}

		public void setMrpProperty(DoubleProperty mrpProperty) {
			this.mrpProperty = mrpProperty;
		}
		@JsonIgnore
		public DoubleProperty getQtyProperty() {
			qtyProperty.set(qty);
			return qtyProperty;
		}

		public void setQtyProperty(DoubleProperty qtyProperty) {
			this.qtyProperty = qtyProperty;
		}
		@JsonIgnore
		public DoubleProperty getAmountProperty() {
			amountProperty.set(amount);
			return amountProperty;
		}

		public void setAmountProperty(DoubleProperty amountProperty) {
			this.amountProperty = amountProperty;
		}
@JsonIgnore
		public StringProperty getBatchProperty() {
	batchProperty.set(batch);
			return batchProperty;
		}

		public void setBatchProperty(StringProperty batchProperty) {
			this.batchProperty = batchProperty;
		}
		@JsonIgnore
		public StringProperty getReasonIdProperty() {
			reasonIdProperty.set(reasonId);
			return reasonIdProperty;
		}

		public void setReasonIdProperty(StringProperty reasonIdProperty) {
			this.reasonIdProperty = reasonIdProperty;
		}

		@Override
		public String toString() {
			return "ConsumptionReportDtl [id=" + id + ", itemId=" + itemId + ", unitId=" + unitId + ", mrp=" + mrp
					+ ", qty=" + qty + ", amount=" + amount + ", batch=" + batch + ", reasonId=" + reasonId
					+ ", itemIdProperty=" + itemIdProperty + ", unitIdProperty=" + unitIdProperty + ", mrpProperty="
					+ mrpProperty + ", qtyProperty=" + qtyProperty + ", amountProperty=" + amountProperty
					+ ", batchProperty=" + batchProperty + ", reasonIdProperty=" + reasonIdProperty + "]";
		}
		
		
	
}
