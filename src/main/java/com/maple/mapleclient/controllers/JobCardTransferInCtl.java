package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JobCardDtl;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.entity.JobCardInDtl;
import com.maple.mapleclient.entity.JobCardInHdr;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class JobCardTransferInCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<JobCardInDtl> jobCardInDtlList = FXCollections.observableArrayList();

	JobCardHdr jobCardHdr = null;
	JobCardInHdr jobCardInHdr = null;
	JobCardDtl jobCardDtl = null;
	String location = null;
	ItemMst itemMst = null;
    @FXML
    private TextField txtVoucherNumber;
    @FXML
    private TextField txtitemName;
    @FXML
    private TextField txtfromBranch;

    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnSave;

    @FXML
    private TextField txtCustomerName;

    @FXML
    private TableView<JobCardInDtl> tblItems;

    @FXML
    private TableColumn<JobCardInDtl,String> clItemName;

    @FXML
    private TableColumn<JobCardInDtl, Number> clQty;

    @FXML
    private TableColumn<JobCardInDtl, String> clLocation;
    @FXML
   	private void initialize() {
       	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
       	
    }
    @FXML
    void actionSave(ActionEvent event) {
    	// set branch code
    	if(null == jobCardHdr)
    	{
    		jobCardHdr = new JobCardHdr();
    		jobCardHdr.setCustomerId(jobCardInHdr.getCustomerId());
    		jobCardHdr.setItemName(jobCardInHdr.getServiceInDtl().getItemName());
    		jobCardHdr.setServiceInDtl(jobCardInHdr.getServiceInDtl());
    		jobCardHdr.setStatus("ACTIVE");
    		
    		ResponseEntity<JobCardHdr> savedJobCard = RestCaller.saveJobCard(jobCardHdr);
    		jobCardHdr = savedJobCard.getBody();
    	}
    	for(JobCardInDtl jobCardInDtl : jobCardInDtlList)
    	{
    	jobCardDtl = new JobCardDtl();
    	jobCardDtl.setJobCardHdr(jobCardHdr);
    	jobCardDtl.setLocationId(jobCardInDtl.getLocationId());
    	jobCardDtl.setItemId(jobCardInDtl.getItemId());
    	jobCardDtl.setQty(jobCardInDtl.getQty());
    	ResponseEntity<JobCardDtl> jobCardDtlResp = RestCaller.saveJobCardDtl(jobCardDtl);
    	}

    	tblItems.getItems().clear();
    }
    
    @FXML
    void loadVoucherPOpup(MouseEvent event) {
    	
    	loadpopup();
    	

    }
	@Subscribe
	public void popupStockItemlistner(VoucherNoEvent voucherNoEvent) {
		try {
			
			txtVoucherNumber.setText(voucherNoEvent.getVoucherNumber());
			txtfromBranch.setText(voucherNoEvent.getFromBranchCode());
			
			ResponseEntity<JobCardInHdr> getJobCardInHdr = RestCaller.getJobCardByVoucherNo(txtVoucherNumber.getText());
			jobCardInHdr = getJobCardInHdr.getBody();
			if(null != getJobCardInHdr.getBody())
			{
				ResponseEntity<AccountHeads> getCust = RestCaller.getAccountHeadsById(getJobCardInHdr.getBody().getCustomerId());
				if(null !=getCust.getBody() )
				{
					txtitemName.setText(getJobCardInHdr.getBody().getServiceInDtl().getItemName());
					txtCustomerName.setText(getCust.getBody().getAccountName());
				}
				
				ResponseEntity<List<JobCardInDtl>> getHobCardDtl = RestCaller.getJobCardInDtlByHdrId(getJobCardInHdr.getBody().getId());
				jobCardInDtlList = FXCollections.observableArrayList(getHobCardDtl.getBody());
				fillTable();
			}
			
			
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	private void fillTable()
	{
		tblItems.setItems(jobCardInDtlList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clLocation.setCellValueFactory(cellData -> cellData.getValue().getlocationProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
	}
    private void loadpopup()
    {
    	try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/jobcardVoucherPopUpCtl.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

//			txtDebitAccount.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }



}
