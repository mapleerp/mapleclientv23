package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.bouncycastle.crypto.tls.NewSessionTicket;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import javafx.application.Platform;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ItemEditCtl {

	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ItemMst> ItemTableList2 = FXCollections.observableArrayList();
	private ObservableList<UnitMst> unitList = FXCollections.observableArrayList();
	private ObservableList<CategoryMst> categoryList = FXCollections.observableArrayList();
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();

	ItemMst itemMst = null;

	@FXML
	private TextField txtCategory;
	@FXML
	private TextField txtitemName;

	@FXML
	private TextField txtbarCode;

	@FXML
	private TextField txttaxRate;
	@FXML
	private ComboBox<String> cmbPriceLock;

	@FXML
	private TextField txtitemCode;

	@FXML
	private TextField txthsnCode;

	@FXML
	private TextField txtsellingPrice;

	@FXML
	private TextField TxtStateCess;

	@FXML
	private Button btnclr;

	@FXML
	private Button btnshowAll;

	@FXML
	private Button btnSubmit;

//	@FXML
//	private TextField txtUnit;

	 @FXML
	 private ComboBox<String> cmbUnit;


	 
	@FXML
	private ComboBox<String> choiceCategory;

	@FXML
	private TextField txtReoder;

	@FXML
	private Button btnRefresh;

	@FXML
	private TextField txtItemId;

	@FXML
	private TextField txtBestBefore;

	@FXML
	private TableView<ItemMst> tblItemList;

	@FXML
	private TableColumn<ItemMst, String> clItemName;

	@FXML
	private TableColumn<ItemMst, String> clItemCategory;

	@FXML
	private TableColumn<ItemMst, String> clItemUnit;

	@FXML
	private TableColumn<ItemMst, Number> clItemSellingPrice;

	@FXML
	private TableColumn<ItemMst, Number> clItemTax;

	@FXML
	private TableColumn<ItemMst, String> clStatus;

	@FXML
	private RadioButton RdNegativeBillLock;

	@FXML
	void AddItemAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSubmit.requestFocus();
		}
	}

	@FXML
	void submitKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			submit();
		}
	}

	@FXML
	void BarcodeEnterActionSS(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtbarCode.getText().trim().isEmpty()) {
				ArrayList items = new ArrayList();
				items = RestCaller.ItemSearchByBarcode(txtbarCode.getText());
				String itemName;
//    			String unitName;
//    			String unitId;
//    			String itemId;
//    			Double taxRate;
//    			String itemCode;
//    			String categoryName;
//    			String categoryId;
//    			String hsnCode;
//    			Double sellingPrice;
//    			Double cess;
//    			Integer bestBefore;
//    			Integer reorderWaitingPeriod;
				Iterator itr = items.iterator();
				while (itr.hasNext()) {

					List element = (List) itr.next();
					itemName = (String) element.get(0);
//					unitName = (String) element.get(2);
//					sellingPrice = (Double) element.get(3);
////					qty = (Double) element.get(4);
////					cess = (Double) element.get(5);
//					taxRate = (Double) element.get(6);
//					itemId = (String) element.get(7);
//					unitId = (String) element.get(8);
//					categoryId =   (String) element.get(10);
					if (null != itemName) {

						txtitemName.setText(itemName);
						ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemName);
						txtbarCode.setText(getItem.getBody().getBarCode());
						if (null != getItem.getBody().getBestBefore()) {
							txtBestBefore.setText(Integer.toString(getItem.getBody().getBestBefore()));
						} else {
							txtBestBefore.setText("0");
						}
						txthsnCode.setText(getItem.getBody().getHsnCode());
						txtitemCode.setText(getItem.getBody().getItemCode());
						txtItemId.setText(getItem.getBody().getId());
						if (null != getItem.getBody().getReorderWaitingPeriod()) {
							txtReoder.setText(Integer.toString(getItem.getBody().getReorderWaitingPeriod()));
						} else {
							txtReoder.setText("0");
						}
						if (null != getItem.getBody().getStandardPrice()) {
							txtsellingPrice.setText(Double.toString(getItem.getBody().getStandardPrice()));
						} else {
							txtsellingPrice.setText("0.0");
						}
						if (null != getItem.getBody().getTaxRate()) {
							txttaxRate.setText(Double.toString(getItem.getBody().getTaxRate()));
						} else {
							txttaxRate.setText("0.0");
						}
						if (null != getItem.getBody().getCess()) {
							TxtStateCess.setText(Double.toString(getItem.getBody().getCess()));
						} else {
							TxtStateCess.setText("0.0");
						}
						ResponseEntity<CategoryMst> categoryMst = RestCaller
								.getCategoryById(getItem.getBody().getCategoryId());
						txtCategory.setText(categoryMst.getBody().getCategoryName());
						choiceCategory.getSelectionModel().select(categoryMst.getBody().getCategoryName());
						choiceCategory.setValue(categoryMst.getBody().getCategoryName());
						ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
						//txtUnit.setText(getUnit.getBody().getUnitName());
						cmbUnit.getSelectionModel().select(getUnit.getBody().getUnitName());
						cmbUnit.setValue(getUnit.getBody().getUnitName());
						
						itemMst = new ItemMst();

					}
				}
				cmbUnit.requestFocus();
			}
		}
	}

	@FXML
	void unitOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			
			//txttaxRate.requestFocus();
		}
	}

	@FXML
	void ComboCategoryEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txthsnCode.requestFocus();
		}
	}

	@FXML
	private void initialize() {
		// btnAdd.setVisible(false);
		eventBus.register(this);

		cmbPriceLock.getItems().add("YES");
		cmbPriceLock.getItems().add("NO");

		
		UnitMst unitMst = new UnitMst();
		// set unit
		ArrayList unit = new ArrayList();

		unit = RestCaller.SearchUnit();
		Iterator itr = unit.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object unitName = lm.get("unitName");
			Object unitId = lm.get("id");
			if (unitId != null) {
				cmbUnit.getItems().add((String) unitName);
				UnitMst newUnitMst = new UnitMst();
				newUnitMst.setUnitName((String) unitName);
				newUnitMst.setId((String) unitId);
				unitList.add(newUnitMst);
			}

		}
		
		
		// set category
		ArrayList cat = new ArrayList();

		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				choiceCategory.getItems().add((String) categoryName);

				CategoryMst categoryMst = new CategoryMst();
				categoryMst.setCategoryName((String) categoryName);
				categoryMst.setId((String) id);
				categoryList.add(categoryMst);
			}

		}
		choiceCategory.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (!newValue.equalsIgnoreCase(null))
					txtCategory.setText(newValue.toString());
			}
		});

	}

	@FXML
	void ComboUniteFocusAction(KeyEvent event) {

	}

	@FXML
	void FocusReodrWaitingTime(KeyEvent event) {

	}

	@FXML
	void HsnCodeEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtsellingPrice.requestFocus();
		}
	}

	@FXML
	void ItemCodeEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			choiceCategory.requestFocus();
		}

	}

	@FXML
	void ItemNameEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			loadItemPOpUp();
		}

	}

	@FXML
	void Refresh(ActionEvent event) {

	}

	@FXML
	void SellingPriceEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			TxtStateCess.requestFocus();
		}
	}

	@FXML
	void StateCessEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtBestBefore.requestFocus();
		}
	}

	@FXML
	void TaxRateEnterAction(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtitemCode.requestFocus();
		}
	}

	@FXML
	void bestBeforeEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtReoder.requestFocus();
		}
	}

	@FXML
	void clear(ActionEvent event) {
		clearField();

		tblItemList.getItems().clear();
		itemMst = null;
	}

	public void clearField() {
		txtReoder.clear();
		txtItemId.clear();

		txtbarCode.clear();
		txtitemName.clear();
		txttaxRate.clear();
		txtitemCode.clear();
		txthsnCode.clear();
		txtsellingPrice.clear();
		TxtStateCess.clear();
		txtCategory.clear();
//		tblItemList.getItems().clear();
		choiceCategory.setPromptText(null);

		cmbUnit.setPromptText(null);
		// choiceCategory.getSelectionModel().clearSelection();

		txtBestBefore.clear();
		cmbPriceLock.getSelectionModel().clearSelection();
	}

	@FXML
	void showall(ActionEvent event) {
		showItemDetials();
	}

	@FXML
	void submit(ActionEvent event) {
		submit();
	}

	private void submit()

	{

		if (null == choiceCategory.getValue()) {
			notifyMessage(3, "Select Category", false);
			choiceCategory.requestFocus();
			return;
		}

		if (txtitemName.getText().contains("%") || (txtitemName.getText().contains("/"))
				|| (txtitemName.getText().contains("?")) || txtitemName.getText().contains("&")
				|| txtitemName.getText().contains("*") || txtitemName.getText().contains("$")) {
			notifyMessage(5, "ItemName Invalid", false);
			return;
		}
		String itemName = txtitemName.getText().trim();

		itemName = itemName.replaceAll("'", "");
		itemName = itemName.replaceAll("&", "");

		itemName = itemName.replaceAll("/", "");
		itemName = itemName.replaceAll("%", "");
		itemName = itemName.replaceAll("\"", "");
		itemMst.setItemName(itemName);
		itemMst.setItemCode(txtitemCode.getText().trim());
		if (txttaxRate.getText().trim().isEmpty()) {
			itemMst.setTaxRate(0.0);
		} else {
			itemMst.setTaxRate(Double.parseDouble(txttaxRate.getText()));
		}
		if (txtsellingPrice.getText().trim().isEmpty()) {
			itemMst.setStandardPrice(0.0);
		} else {
			itemMst.setStandardPrice(Double.parseDouble(txtsellingPrice.getText()));
		}
		itemMst.setId(txtItemId.getText());
		itemMst.setServiceOrGoods("ITEM MST");
		itemMst.setRank(0);
		itemMst.setHsnCode(txthsnCode.getText().trim());
		if (!txtBestBefore.getText().trim().isEmpty()) {
			itemMst.setBestBefore(Integer.parseInt(txtBestBefore.getText()));
		} else {
			itemMst.setBestBefore(0);
		}
		// -----------------barcode edit not allowed
//		itemMst.setBarCode(txtbarCode.getText().trim());
		if (!TxtStateCess.getText().trim().isEmpty()) {

			itemMst.setCess(Double.parseDouble(TxtStateCess.getText()));
		} else {
			itemMst.setCess(0.0);
		}

		if (!txtReoder.getText().trim().isEmpty()) {

			itemMst.setReorderWaitingPeriod(Integer.parseInt(txtReoder.getText()));
		} else {
			itemMst.setReorderWaitingPeriod(0);
		}

		String categoryName = txtCategory.getText();

		if (categoryName.trim().isEmpty()) {
			notifyMessage(3, "Please Select Category", false);
			return;
		}

		ResponseEntity<CategoryMst> response = RestCaller.getCategoryByName(categoryName);
		CategoryMst categoryMst = response.getBody();
		itemMst.setCategoryId(categoryMst.getId());

		/*
		 * for (CategoryMst u : categoryList) { if (u.getCategoryName() == categoryName)
		 * { itemMst.setCategoryId(u.getId()); } }
		 */

		// ---------------------version new 1.2 surya
		if (RdNegativeBillLock.isSelected()) {
			itemMst.setNegativeBillLock("YES");
		}
		// ---------------------version new 1.2 surya end

		itemMst.setIsDeleted("N");
		itemMst.setBranchCode(SystemSetting.getUser().getBranchCode());

		ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
		if(null != getUnit.getBody()) {
			itemMst.setUnitId(getUnit.getBody().getId());
		}
		//itemMst.setUnitId(getUnit.getBody().getId());

		if (null != cmbPriceLock.getSelectionModel() || null != cmbPriceLock.getSelectionModel().getSelectedItem()) {
			itemMst.setItemPriceLock(cmbPriceLock.getSelectionModel().getSelectedItem());
		}
		ResponseEntity<ItemMst> getItemByid = RestCaller.getitemMst(itemMst.getId());
		if (!getItemByid.getBody().getItemName().equalsIgnoreCase(txtitemName.getText())) {
			ResponseEntity<ItemMst> getItemByName = RestCaller.getItemByNameRequestParam(txtitemName.getText());
			if (null != getItemByName.getBody()) {
				notifyMessage(3, "Item Already Exist", false);
				return;
			}
		}

		itemMst.setBarCode(getItemByid.getBody().getBarCode());

		RestCaller.updateItemMst(itemMst);
		notifyMessage(5, "Item Edited", true);
		savePriceDefenition(itemMst);
		saveBatchPriceDefenition(itemMst);
		itemMst = null;
		clearField();
		RdNegativeBillLock.setSelected(false);
		cmbUnit.getSelectionModel().clearSelection();
		
		txtitemName.requestFocus();

	}

	private void savePriceDefenition(ItemMst itemMst) {

		PriceDefinition priceDefenition = new PriceDefinition();
		priceDefenition.setItemId(itemMst.getId());
		priceDefenition.setBranchCode(SystemSetting.getUser().getBranchCode());
		priceDefenition.setEndDate(null);
		String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
		priceDefenition.setStartDate(Date.valueOf(sdate));
		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller.getPriceDefenitionByName("MRP");
		PriceDefenitionMst price = new PriceDefenitionMst();
		price = priceDefenitionSaved.getBody().get(0);
		if (null != price) {
			priceDefenition.setPriceId(price.getId());
		}
		priceDefenition.setAmount(itemMst.getStandardPrice());
		priceDefenition.setUnitId(itemMst.getUnitId());
		ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(priceDefenition);
		priceDefenition = respentity.getBody();

	}

	private void saveBatchPriceDefenition(ItemMst itemMst) {
		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller.getPriceDefenitionByName("MRP");
		PriceDefenitionMst price = new PriceDefenitionMst();
		if (priceDefenitionSaved.getBody().size() == 0) {
			return;
		}
		price = priceDefenitionSaved.getBody().get(0);
		ResponseEntity<List<BatchPriceDefinition>> batchPrice = RestCaller
				.getBatchPriceDefinitionByItemIdAndPriceId(itemMst.getId(), price.getId());
		if (batchPrice.getBody().size() > 0) {
			for (int i = 0; i < batchPrice.getBody().size(); i++) {
				BatchPriceDefinition batchpriceDefenition = new BatchPriceDefinition();
				batchpriceDefenition.setItemId(itemMst.getId());
				batchpriceDefenition.setBranchCode(SystemSetting.getUser().getBranchCode());
				batchpriceDefenition.setEndDate(null);
				String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
				batchpriceDefenition.setStartDate(Date.valueOf(sdate));

				if (null != price) {
					batchpriceDefenition.setPriceId(price.getId());
				}
				batchpriceDefenition.setAmount(itemMst.getStandardPrice());
				batchpriceDefenition.setUnitId(itemMst.getUnitId());
				batchpriceDefenition.setBatch(batchPrice.getBody().get(i).getBatch());
				ResponseEntity<BatchPriceDefinition> respentity = RestCaller
						.saveBatchPriceDefinition(batchpriceDefenition);
				batchpriceDefenition = respentity.getBody();
			}
		}

	}

	private void showItemDetials() {

		tblItemList.getItems().clear();
		ArrayList cat = new ArrayList();

		cat = RestCaller.SearchItemMsts();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object itemName = lm.get("itemName");
			Object standardPrice = lm.get("standardPrice");
			Object taxRate = lm.get("taxRate");
			Object unitId = lm.get("unitId");
			Object categoryId = lm.get("categoryId");
			Object isDeleted = lm.get("isDeleted");
			Object sgst = lm.get("sgst");
			Object id = lm.get("id");
			if (id != null) {
				ItemMst itemMst = new ItemMst();
				itemMst.setItemName((String) itemName);
				itemMst.setStandardPrice((Double) standardPrice);
				itemMst.setTaxRate((Double) taxRate);
				itemMst.setUnitId((String) unitId);
				itemMst.setCategoryId((String) categoryId);
				itemMst.setId((String) id);
				itemMst.setIsDeleted((String) isDeleted);
				itemMst.setSgst((Double) sgst);
				ItemTableList2.add(itemMst);

			}

		}
		ShowAllFillTable();

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		if (stage.isShowing()) {

			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					cmbUnit.setValue(itemPopupEvent.getUnitName());
					cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
				}
			});
			
			itemMst = new ItemMst();
			txtitemName.setText(itemPopupEvent.getItemName());
			txtbarCode.setText(itemPopupEvent.getBarCode());
			// version1.8

			// txtUnitCode.setText(itemPopupEvent.getUnitName());not needed from previous
			// version
			// version1.8ends
			ResponseEntity<ItemMst> itemMstResp = RestCaller.getItemByNameRequestParam(txtitemName.getText());
			itemMst = itemMstResp.getBody();
			txtBestBefore.setText(Integer.toString(itemMstResp.getBody().getBestBefore()));
			txthsnCode.setText(itemMstResp.getBody().getHsnCode());
			txtitemCode.setText(itemMstResp.getBody().getItemCode());
			txtItemId.setText(itemPopupEvent.getItemId());
			
			
			if (null == itemMstResp.getBody().getReorderWaitingPeriod()) {
				txtReoder.setText("0");
			} else {
				txtReoder.setText(Integer.toString(itemMstResp.getBody().getReorderWaitingPeriod()));
			}
			if (null == itemMstResp.getBody().getStandardPrice()) {
				txtsellingPrice.setText("0.0");
			} else {
				txtsellingPrice.setText(Double.toString(itemMstResp.getBody().getStandardPrice()));

			}
			if (null == itemMstResp.getBody().getTaxRate()) {
				txttaxRate.setText("0.0");
			} else {
				txttaxRate.setText(Double.toString(itemMstResp.getBody().getTaxRate()));
			}
			if (null == itemMstResp.getBody().getCess()) {
				TxtStateCess.setText("0.0");
			} else {
				TxtStateCess.setText(Double.toString(itemMstResp.getBody().getCess()));
			}
			if (null != itemMstResp.getBody().getCategoryId()) {
				ResponseEntity<CategoryMst> getCat = RestCaller.getCategoryById(itemMstResp.getBody().getCategoryId());
				txtCategory.setText(getCat.getBody().getCategoryName());
				// choiceCategory.getSelectionModel().selec
				choiceCategory.setValue(getCat.getBody().getCategoryName());
			}
//			unitmst.setId(itemPopupEvent.getUnitId());
//			itemMrp =  itemPopupEvent.getMrp();
			// txtQty.setText(Integer.toString(itemPopupEvent.getQty()));

		}

	}

	private void loadItemPOpUp() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtbarCode.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void ShowAllFillTable() {

		tblItemList.setItems(ItemTableList2);

		for (ItemMst i : ItemTableList2) {
			if (null != i.getUnitId()) {
				for (UnitMst u : unitList) {
					if (i.getUnitId().equals(u.getId())) {
						i.setUnitName(u.getUnitName());
						clItemUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
					}
				}
			}

			if (null != i.getCategoryId()) {
				for (CategoryMst u : categoryList) {
					if (i.getCategoryId().equals(u.getId())) {
						i.setCategoryName(u.getCategoryName());
						clItemCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
					}
				}
			}

			if (null != i.getItemName()) {
				clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

			}
			if (null != i.getTaxRate()) {
				clItemTax.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
			}
			if (null != i.getStandardPrice()) {
				clItemSellingPrice.setCellValueFactory(cellData -> cellData.getValue().getStandardPriceProperty());
			}
			if (null != i.getIsDeleted()) {

				if (i.getIsDeleted().equals("Y")) {
					i.setDeletedStatus("DELETED");
					clStatus.setCellValueFactory(cellData -> cellData.getValue().getDeletedStatusProperty());
				} else {
					i.setDeletedStatus("ACTIVE");
					clStatus.setCellValueFactory(cellData -> cellData.getValue().getDeletedStatusProperty());
				}
			}
			if (null != i.getId()) {
				txtItemId.setText(i.getId());
			}

		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
