package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DayEndClosureDtl;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.SessionEndClosureMst;
import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class SessionEndCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<SessionEndDtl> sessionDtlList = FXCollections.observableArrayList();

	SessionEndDtl sessionEndDtl = null;
	SessionEndDtl sessionEndDtlDelete = null;
	SessionEndClosureMst sessionEndClosure = null;
	DayEndClosureHdr dayEndClosure = null;

	@FXML
	private DatePicker dpProcessDate;

	@FXML
	private Button btnFetch;

	@FXML
	private TextField txtCashSale;

	@FXML
	private TextField txtCardSale;

	@FXML
	private TextField txtCardSale2;

	@FXML
	private TextField txtPhysicalCash;

	@FXML
	private Button btnSave;
	
	 @FXML
	    private Button btnDelete;

	@FXML
	private TextField txtDenomination;

	@FXML
	private TextField txtCount;

	@FXML
	private Button btnAdd;

	@FXML
	private TableView<SessionEndDtl> tbSessionEnd;

	@FXML
	private TableColumn<SessionEndDtl, Number> clDenomination;

	@FXML
	private TableColumn<SessionEndDtl, Number> clCount;

	@FXML
	void AddSessionEnd(ActionEvent event) {
		addCount();

	}
	
	private void addCount() {
		

		if (null != dpProcessDate.getValue()) {
			java.util.Date uDate = Date.valueOf(dpProcessDate.getValue());

			dayEndClosure = new DayEndClosureHdr();
			dayEndClosure.setProcessDate(uDate);
    		
			Format formatter;
					
					formatter = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = formatter.format(dayEndClosure.getProcessDate());
					
					sessionEndClosure = new SessionEndClosureMst();
					ResponseEntity<SessionEndClosureMst> SessionEndSaved = RestCaller.getSessionEndClosureByDate(strDate,SystemSetting.getUserId());
					
					
					sessionEndClosure = SessionEndSaved.getBody();
					
					System.out.println("======sessionEndClosure======"+sessionEndClosure);
					
					if(null == sessionEndClosure)
					{
				
							
			ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
			dayEndClosure = new DayEndClosureHdr();
			dayEndClosure = dayEndClosuredtlSaved.getBody();

			if (dayEndClosure == null) {


				if (null == sessionEndClosure) {
					sessionEndClosure = new SessionEndClosureMst();

					sessionEndClosure.setBranchCode(SystemSetting.getSystemBranch());

					sessionEndClosure.setSessionDate(Date.valueOf(dpProcessDate.getValue()));
					sessionEndClosure.setUserId(SystemSetting.getUserId());
					System.out.println("===sessionEndClosure==="+sessionEndClosure);
					ResponseEntity<SessionEndClosureMst> respentity = RestCaller.saveSessionEndClosureHdr(sessionEndClosure);
					sessionEndClosure = respentity.getBody();
					System.out.println("===sessionEndClosure=respentity=="+sessionEndClosure);
					// sessionEndClosure.setEmpId(empId);

					// sessionEndClosure.setUserId(userId);

				}
				if (null != sessionEndClosure) {
					if (null != sessionEndClosure.getId()) {
					
					if(txtDenomination.getText().trim().isEmpty())
					{
						notifyMessage(5, "Please enter Denomination!!!");
						
					} else if (txtCount.getText().trim().isEmpty()) {
						
						notifyMessage(5, "Please enter Count!!!");
						
					} else {
						
						sessionEndDtl = new SessionEndDtl();
						sessionEndDtl.setSessionEndClosure(sessionEndClosure);
						sessionEndDtl.setDenomination(Double.parseDouble(txtDenomination.getText()));
						sessionEndDtl.setCount(Double.parseDouble(txtCount.getText()));
						ResponseEntity<SessionEndDtl> respentity = RestCaller.saveSessionEndDtl(sessionEndDtl);
						sessionEndDtl = respentity.getBody();
						sessionDtlList.add(sessionEndDtl);
						filltable();
						txtDenomination.clear();
						txtCount.clear();
						sessionEndDtl = null;
						
					}
					
					}
				
				}
			} else {
				notifyMessage(5, "Sorry Day End already done!!!");
			}
					} else {
						notifyMessage(5, "Already done!!!",false);
					}
					
		} else {
			notifyMessage(5, "Please select date!!!",false);
		}
					
			
		
	}

	@FXML
	void DeleteSession(ActionEvent event) {
		if (null != sessionEndDtlDelete.getId()) {
			RestCaller.deleteSessionDtl(sessionEndDtlDelete.getId());
			tbSessionEnd.getItems().clear();
			notifyMessage(5, "Successfully Deleted!!!");
			ResponseEntity<List<SessionEndDtl>> sessiondtls = RestCaller.getSessionEndDtl(sessionEndClosure);
			sessionDtlList = FXCollections.observableArrayList(sessiondtls.getBody());
			filltable();
			if (null != sessionDtlList) {
				tbSessionEnd.setItems(sessionDtlList);
			}
		}
	}

	@FXML
	void FetchData(ActionEvent event) {
		
if(null != dpProcessDate.getValue()) {
    		
    		
    		
    		java.util.Date uDate = Date.valueOf(dpProcessDate.getValue());
    		
    		dayEndClosure = new DayEndClosureHdr();
			dayEndClosure.setProcessDate(uDate);
    		
			Format formatter;
					
					formatter = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = formatter.format(dayEndClosure.getProcessDate());
			
    		ResponseEntity<SessionEndClosureMst> sessionEndClosuredtlSaved = RestCaller.getSessionEndClosureByDate(strDate,SystemSetting.getUserId());
    		sessionEndClosure = new SessionEndClosureMst();
    		sessionEndClosure = sessionEndClosuredtlSaved.getBody();
    		
    		System.out.println("=========sessionEndClosure========"+sessionEndClosure);
    		if(null != sessionEndClosure)
    		{
    			btnAdd.setDisable(true);
        		btnDelete.setDisable(true);
        		btnSave.setDisable(true);
        		
    			txtCardSale.setText(Double.toString(sessionEndClosure.getCardSale()));
    			txtCardSale2.setText(Double.toString(sessionEndClosure.getCardSale2()));
    			txtCashSale.setText(Double.toString(sessionEndClosure.getCashSale()));
    			txtPhysicalCash.setText(Double.toString(sessionEndClosure.getPhysicalCash()));
    			ResponseEntity<List<SessionEndDtl>> sessiondtls = RestCaller.getSessionEndDtl(sessionEndClosure);
    			sessionDtlList = FXCollections.observableArrayList(sessiondtls.getBody());
    			filltable();
    			
    			
    		} else {
    			
    			tbSessionEnd.getItems().clear();
    			notifyMessage(5, " No data to show...!!!",false);
			}
    		
    	}


	}

	@FXML
	private void initialize() {
		
				dpProcessDate = SystemSetting.datePickerFormat(dpProcessDate, "dd/MMM/yyyy");
				
		   
		tbSessionEnd.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					sessionEndDtlDelete = new SessionEndDtl();
					sessionEndDtlDelete.setId(newSelection.getId());
				}
			}
		});
	}

	@FXML
	void FinalSave(ActionEvent event) {
		
		if(null != sessionEndClosure)
		{
			ResponseEntity<List<SessionEndDtl>> sessiondtls = RestCaller.getSessionEndDtl(sessionEndClosure);
			if (sessiondtls.getBody().size() == 0) {
				return;
			}
			sessionEndClosure.setPhysicalCash(Double.parseDouble(txtPhysicalCash.getText()));
			RestCaller.updateSessionEndClosure(sessionEndClosure);
			sessionEndClosure = null;
			dpProcessDate.setValue(null);
			tbSessionEnd.getItems().clear();
			txtPhysicalCash.clear();
			
		}

		

	}


    @FXML
    void FocusOnAddButton(KeyEvent event) {
    	
if (event.getCode() == KeyCode.ENTER) {
			
			if(txtCount.getLength()>0)
			{
				addCount();
			}
		}

    }

	@FXML
	void FocusOnCount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			
			txtCount.requestFocus();
		}
	}
	
	

	@FXML
	void FocusOnDenomination(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			
			txtDenomination.requestFocus();
		}
	}

	private void filltable() {
		tbSessionEnd.setItems(sessionDtlList);
		clDenomination.setCellValueFactory(cellData -> cellData.getValue().getDenominationProperty());
		clCount.setCellValueFactory(cellData -> cellData.getValue().getCountProperty());
		
		Double sum = RestCaller.getSummarySessionEndClosure(sessionEndClosure.getId());
		if(null != sum)
		{
		txtPhysicalCash.setText(Double.toString(sum));
		} else {
			txtPhysicalCash.setText("0.0");
		}

	}

	public void notifyMessage(int duration, String msg) {
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
