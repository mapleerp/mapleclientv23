package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ConsumptionReport;
import com.maple.report.entity.SalesInvoiceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRException;

public class PharmacyCustomerwiseSalesReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<SalesInvoiceReport> reportList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtItemGroup;

    @FXML
    private ListView<String> lsGroup;

    @FXML
    private Button btnAdd;

    @FXML
    private TextField txtCustomer;


    @FXML
    private TableView<SalesInvoiceReport> tbConsumption;

    @FXML
    private TableColumn<SalesInvoiceReport, String> clVoucherDate;


    @FXML
    private TableColumn<SalesInvoiceReport, String> clVoucherNumber;

    @FXML
    private TableColumn<SalesInvoiceReport, String> clItemName;

    @FXML
    private TableColumn<SalesInvoiceReport, String> clBatch;

    @FXML
    private TableColumn<SalesInvoiceReport,Number> clQty;

    @FXML
    private TableColumn<SalesInvoiceReport,Number> clMrp;

    @FXML
    private TableColumn<SalesInvoiceReport,Number> clAmount;

    @FXML
    private TableColumn<SalesInvoiceReport,Number> clTax;
    @FXML
 	private void initialize() 
    {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	
    	eventBus.register(this);
    }
    @FXML
    void actionAdd(ActionEvent event) {

    	lsGroup.getItems().add(txtItemGroup.getText());
    	lsGroup.getSelectionModel().select(txtItemGroup.getText());
    	txtItemGroup.clear();
    
    }
    @FXML
    void actionClear(ActionEvent event) {
    	lsGroup.getItems().clear();
    	dpFromDate.setValue(null);
    	dpToDate.setValue(null);
    	txtCustomer.getText();
    	tbConsumption.getItems().clear();
    	txtCustomer.clear();
//    	reportList.clear();
    }

    @FXML
    void actionPrint(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
   
	try {
		JasperPdfReportService.PharmacyCustomerwiseSales(startDate, endDate, cat, txtCustomer.getText());
	} catch (JRException e) {
		e.printStackTrace();
	}
    

    }
    @Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {
    	Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			
			txtCustomer.setText(customerEvent.getCustomerName());
		}
		
    }
    @FXML
    void loadCustomerPopup(MouseEvent event) {
    	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
		Parent root1 = null;

		try {
			root1 = (Parent) fxmlLoader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Stage stage = new Stage();

		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setTitle("ABC");
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setScene(new Scene(root1));
		stage.show();

		txtItemGroup.requestFocus();
    }
    @FXML
    void actionShow(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	if(selectedItems.size()>0)
    	{
    	ResponseEntity<List<SalesInvoiceReport>> department = RestCaller.getcustomerWiseSalesbyCategoryList(startDate,endDate,txtCustomer.getText(),cat);
    	reportList = FXCollections.observableArrayList(department.getBody());
    	}
    	else
    	{
    		ResponseEntity<List<SalesInvoiceReport>> department = RestCaller.getcustomerWiseSalesByDate(startDate,endDate,txtCustomer.getText());
        	reportList = FXCollections.observableArrayList(department.getBody());
    	}
    	fillTable();
    }

    private void fillTable()
    {
    	tbConsumption.setItems(reportList);
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
    	clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clMrp.setCellValueFactory(cellData -> cellData.getValue().getrateProperty());
    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
    	clTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());


    }
    @Subscribe
  		public void popupOrglistner(CategoryEvent CategoryEvent) {

  			Stage stage = (Stage) btnAdd.getScene().getWindow();
  			if (stage.isShowing()) {
  				txtItemGroup.setText(CategoryEvent.getCategoryName());
  			}
  		}
    @FXML
    void loadCategoryPopUp(MouseEvent event) {

    	try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			btnAdd.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
    
    

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }



   


}
