package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ShortExpiryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;
public class CategoryWiseShortExpiry {
	String taskid;
	String processInstanceId;
	private ObservableList<ShortExpiryReport> shortExpiryList = FXCollections.observableArrayList();

    @FXML
    private ListView<String> lstCategory;

    @FXML
    private Button btnShowReport;

    @FXML
    private Button btnPrintReport;

    @FXML
    private TableView<ShortExpiryReport> tblShortExpiry;

    @FXML
    private TableColumn<ShortExpiryReport,String> clItemName;

    @FXML
    private TableColumn<ShortExpiryReport, String> clCategory;

    @FXML
    private TableColumn<ShortExpiryReport, String> clBatch;

    @FXML
    private TableColumn<ShortExpiryReport, LocalDate> clExpiryDate;

    @FXML
    private TableColumn<ShortExpiryReport, LocalDate> clUpdatedDate;

    @FXML
    private TableColumn<ShortExpiryReport, Number> clQty;
	@FXML
	private void initialize() {

		ArrayList cat = new ArrayList();
		 lstCategory.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				CategoryMst category = new CategoryMst();
				category.setCategoryName((String) categoryName);
				lstCategory.getItems().add(category.getCategoryName());
			}
		}
	}
	private void fillTable()
	{
		tblShortExpiry.setItems(shortExpiryList);
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
    	clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getexpiryDateProperty());
    	clUpdatedDate.setCellValueFactory(cellData -> cellData.getValue().getupdatedDateProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
    	clCategory.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
	}
    @FXML
    void printReport(ActionEvent event) {

    	List<String> selectedItems = lstCategory.getSelectionModel().getSelectedItems();
    
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	}    	try {
			JasperPdfReportService.CategoryWiseShortExpiryReport(cat,SystemSetting.systemBranch,SystemSetting.UtilDateToString(SystemSetting.getSystemDate()));
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

    @FXML
    void showReport(ActionEvent event) {
    	
    	tblShortExpiry.getItems().clear();
    	
    	List<String> selectedItems = lstCategory.getSelectionModel().getSelectedItems();
    
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	}
    	ResponseEntity<List<ShortExpiryReport>> getSavedItems = RestCaller.getCategoryWiseExpiry(cat);
    	shortExpiryList = FXCollections.observableArrayList(getSavedItems.getBody());
    	fillTable();
    }
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


     private void PageReload() {
     	
}
    


}
