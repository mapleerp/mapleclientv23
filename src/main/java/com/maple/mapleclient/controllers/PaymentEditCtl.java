package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class PaymentEditCtl {
	String taskid;
	String processInstanceId;

	
	private ObservableList<PaymentHdr> paymentLsitTable = FXCollections.observableArrayList();
	private ObservableList<PaymentDtl> paymentDtlLsitTable = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();

	OwnAccount ownAccount = null;
	PaymentHdr paymentHdr = null;
	PaymentDtl paymentDtl = null;
	
    @FXML
    private DatePicker dpVoucherDate;
    @FXML
    private DatePicker dpDate;
    @FXML
    private TableView<PaymentHdr> tblPaymentHdr;

    @FXML
    private TableColumn<PaymentHdr, String> clVoucherDate;

    @FXML
    private TableColumn<PaymentHdr, String> clVoucherNumber;

    @FXML
    private Button btnFetchData;

    @FXML
    private TableView<PaymentDtl> tblPaymentDtl;

    @FXML
    private TableColumn<PaymentDtl, String> clAccount;

    @FXML
    private TableColumn<PaymentDtl, LocalDate> clDate;

    @FXML
    private TableColumn<PaymentDtl, String> clModeOfPayment;

    @FXML
    private TableColumn<PaymentDtl, String> clDepositBank;

    @FXML
    private TableColumn<PaymentDtl, String> clInstNo;

    @FXML
    private TableColumn<PaymentDtl,LocalDate> clInstDate;


    @FXML
    private TableColumn<PaymentDtl, String> clRemarks;

    @FXML
    private TableColumn<PaymentDtl, Number> clAmount;

    @FXML
    private TextField txtAccount;

    @FXML
    private ComboBox<String> cmbModeOfPayment;

    @FXML
    private TextField txtRemarks;

    @FXML
    private TextField txtAmount;

    @FXML
    private ComboBox<String> cmbDepositBank;

    @FXML
    private DatePicker dpInsDate;

    @FXML
    private TextField txtInsNumber;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFinalSave;
    
    @FXML
   	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    	dpInsDate = SystemSetting.datePickerFormat(dpInsDate, "dd/MMM/yyyy");
    	dpVoucherDate = SystemSetting.datePickerFormat(dpVoucherDate, "dd/MMM/yyyy");
		eventBus.register(this);
		ResponseEntity<AccountHeads>accountHeadPetty = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-PETTY CASH");

    	ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
		AccountHeads accountHeads = accountHead.getBody();
		accountHeads.setId(accountHead.getBody().getId());
		cmbModeOfPayment.getItems().add("TRASNFER");
		cmbModeOfPayment.getItems().add("CHEQUE");
		cmbModeOfPayment.getItems().add("DD");
		cmbModeOfPayment.getItems().add("MOBILE PAY");
		cmbModeOfPayment.getItems().add(accountHead.getBody().getAccountName());
		cmbModeOfPayment.getItems().add(accountHeadPetty.getBody().getAccountName());
		
		

    	tblPaymentHdr.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				System.out.println(newSelection);
					Date fromdate = Date.valueOf(dpVoucherDate.getValue());
			    	
			    	Format formatter;
					
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					String strDate = formatter.format(fromdate);
					
					paymentHdr = new PaymentHdr();

					
					if(null == newSelection.getVoucherNumber())
					{
						ResponseEntity<PaymentHdr> respentity = RestCaller.getPaymentHdrById(newSelection.getId());
						paymentHdr = respentity.getBody();

					} else {
						ResponseEntity<PaymentHdr> respentity = RestCaller.getPaymentHdrByVoucherNumber(newSelection.getVoucherNumber());
						paymentHdr = respentity.getBody();

					}
			
				System.out.println("PAYMENT  HDR "+paymentHdr);

			}
		});
		/*
		 * tblPaymentHdr.getSelectionModel().selectedItemProperty().addListener((obs,
		 * oldSelection, newSelection) -> { if (newSelection != null) {
		 * System.out.println("getSelectionModel"); if (null !=
		 * newSelection.getVoucherNumber()) {
		 * 
		 * ResponseEntity<PaymentHdr> respentity =
		 * RestCaller.getPaymentHdrByVoucherNumber(newSelection.getVoucherNumber());
		 * 
		 * System.out.print(newSelection.getId()
		 * +"payment hdr id issssssssssssssssssssssssssssssssssssssssssssss");
		 * paymentHdr = new PaymentHdr(); paymentHdr = respentity.getBody(); }
		 * 
		 * } });
		 */
    	ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid= lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbDepositBank.getItems().add((String) bankName);
				
						    //accountHeads.getAccountName);
			}
		}
    	tblPaymentDtl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				System.out.print(newSelection.getId()+"id issssssssssssssssssssssssssssssssssssssssss");
				if (null != newSelection.getAccountId()) {
					try {
					paymentDtl = new PaymentDtl();
					paymentDtl.setId(newSelection.getId());
				//	ResponseEntity<AccountHeads> getAc = RestCaller.getAccountById(newSelection.getAccount());
					txtAccount.setText(newSelection.getAccountId());
				//	System.out.print(getAc+"accout id issssssssssssssssssssssssssssssssssssssssssssssss");
					txtAmount.setText(Double.toString(newSelection.getAmount()));
					txtInsNumber.setText(newSelection.getInstrumentNumber());
					txtRemarks.setText(newSelection.getRemark());
					cmbDepositBank.setPromptText(newSelection.getBankAccountNumber());
					cmbDepositBank.setValue(newSelection.getBankAccountNumber());
					cmbModeOfPayment.setPromptText(newSelection.getModeOfPayment());
					cmbModeOfPayment.setValue(newSelection.getModeOfPayment());
					
					dpDate.setValue(newSelection.getTransDate().toLocalDate());
					dpInsDate.setValue(newSelection.getInstrumentDate().toLocalDate());
					}catch (Exception e) {
						System.out.print(e.getMessage()+"exception issssssssssssssssssssssssssssssssssssssss");
					}
					
				}

			}
		});
    	cmbModeOfPayment.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.matches(SystemSetting.getSystemBranch()+"-CASH") || newValue.matches(SystemSetting.getSystemBranch()+"-PETTY CASH") )  {
					cmbDepositBank.setDisable(true);
					txtInsNumber.setDisable(true);
					dpInsDate.setDisable(true);
					dpInsDate.setValue(null);
				} else {
					cmbDepositBank.setDisable(false);
					txtInsNumber.setDisable(false);
					dpInsDate.setDisable(false);
				}
			}
		});
    }

    private void fillPaymentHdr()
    {
    	tblPaymentHdr.setItems(paymentLsitTable);
    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
    	clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());


    }

    @FXML
    void deleteReceipt(ActionEvent event) {

    }
    @FXML
    void fetchData(ActionEvent event) {
    	
    	if(null != paymentHdr)
    	{
    		if(null != paymentHdr.getId())
    		{
    			ResponseEntity<List<PaymentDtl>> getPaymentDtl = RestCaller.getPaymentDtls(paymentHdr);
    			paymentDtlLsitTable = FXCollections.observableArrayList(getPaymentDtl.getBody());
    		System.out.print(getPaymentDtl.getBody().get(0).getId()+"payment dtl id izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    		
    			fillPaymentDtl();
    		}
    	  	
    	}
    	showVoucherNumberAndDate();
    }
    private void showVoucherNumberAndDate() {
    	tblPaymentHdr.getItems().clear();
    	if(null != dpVoucherDate.getValue())
    	{
     	Date fromdate = Date.valueOf(dpVoucherDate.getValue());
    	
    	Format formatter;
		
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(fromdate);
//    	ResponseEntity<List<ReceiptHdr>> getReceipts = RestCaller.getAllreceiptsByVoucherDate(strDate);
//    	List<ReceiptHdr> ReceiptList = getReceipts.getBody();
    	
    	ArrayList ReceiptList = new ArrayList();
    	ReceiptList = RestCaller.getAllpaymentsByVoucherDate(strDate);
		
    	Iterator itr = ReceiptList.iterator();

    	String voucherNo = null;;
    	String voucherDate = null;
    	String hdrId = null;

		while (itr.hasNext()) {
			List element = (List) itr.next();
			voucherNo = (String) element.get(0);
			voucherDate = (String) element.get(1);
			hdrId = (String) element.get(2);


				PaymentHdr paymentHdr = new PaymentHdr();
				paymentHdr.setVoucherNumber(voucherNo);
				paymentHdr.setId(hdrId);

				paymentHdr.setVoucherDate(SystemSetting.StringToUtilDate(voucherDate, "yyyy-MM-dd"));
				paymentLsitTable.add(paymentHdr);
				
			
		}
    	
    	fillPaymentHdr();
    	}
    }
    private void fillPaymentDtl()
    {
    	for(PaymentDtl pmt:paymentDtlLsitTable )
    	{
   
    	System.out.print(pmt+"payment dtl izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    		
    		ResponseEntity<AccountHeads> getAcc = RestCaller.getAccountById(pmt.getAccountId());
    	
    		pmt.setAccountId(getAcc.getBody().getAccountName());
    	}
    	tblPaymentDtl.setItems(paymentDtlLsitTable);
    	clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clDate.setCellValueFactory(cellData -> cellData.getValue().gettransDateProperty());
    	clDepositBank.setCellValueFactory(cellData -> cellData.getValue().getBankAccountNumberProperty());
    	clInstDate.setCellValueFactory(cellData -> cellData.getValue().getInstrumentDateProperty());
    	clModeOfPayment.setCellValueFactory(cellData -> cellData.getValue().getModeOfPaymentProperty());
    	
    	clRemarks.setCellValueFactory(cellData -> cellData.getValue().getRemarkProperty());
    }
    @FXML
    void loadAccounts(MouseEvent event) {
    	showPopup();
    }
    private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Accounts");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbModeOfPayment.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnFetchData.getScene().getWindow();
		if (stage.isShowing()) {

					txtAccount.setText(accountEvent.getAccountName());
	
		}

	}
	@Subscribe
	public void popuplistnerVoucher(VoucherNoEvent vno) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnFetchData.getScene().getWindow();
		if (stage.isShowing()) {

					String id = vno.getId();
					ResponseEntity<PaymentHdr > paymmnet = RestCaller.getPaymentHdrById(id);
					paymentHdr = paymmnet.getBody();
					ResponseEntity<List<PaymentDtl>> getpaymentDtl = RestCaller.getPaymentDtl(paymentHdr);
					paymentDtlLsitTable = FXCollections.observableArrayList(getpaymentDtl.getBody());
		    		fillPaymentDtl();
	
		}

	}

    @FXML
    @Transactional
    void save(ActionEvent event) {
    	RestCaller.deletePaymentDtl(paymentDtl.getId());
    	paymentDtl = new PaymentDtl();
    	ResponseEntity<AccountHeads> getaccount = RestCaller.getAccountHeadByName(txtAccount.getText());
    	paymentDtl.setAmount(Double.parseDouble(txtAmount.getText()));
    //	paymentDtl.setBranchCode(SystemSetting.systemBranch);
    	if(cmbModeOfPayment.getSelectionModel().getSelectedItem().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-CASH"))
    	{
			ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
			paymentDtl.setCreditAccountId(accountHead.getBody().getId());
    	}
    	else if(cmbModeOfPayment.getSelectionModel().getSelectedItem().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-PETTY CASH")) {
    		ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-PETTY CASH");
    		paymentDtl.setCreditAccountId(accountHead.getBody().getId());
    	}
    	else
    	{
    		paymentDtl.setCreditAccountId(getaccount.getBody().getId());
    	}
    	paymentDtl.setAccountId(getaccount.getBody().getId());
    	System.out.print(getaccount.getBody().getId()+"account id izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    	
  
    	paymentDtl.setRemark(txtRemarks.getText());
    	paymentDtl.setTransDate(Date.valueOf(dpDate.getValue()));
    	paymentDtl.setPaymenthdr(paymentHdr);
       	paymentDtl.setModeOfPayment(cmbModeOfPayment.getSelectionModel().getSelectedItem());

    	if (cmbModeOfPayment.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-CASH"))
		{
    		paymentDtl.setInstrumentDate(null);
    		
			ResponseEntity<AccountHeads > accountHeadsResp = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
			
			if(null==accountHeadsResp.getBody()) {
				
				notifyMessage(5, "Branch Cash account not set");
				return;
			}
			
		}else if (cmbModeOfPayment.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-PETTY CASH"))
		{
			paymentDtl.setInstrumentDate(null);
    		
			ResponseEntity<AccountHeads > accountHeadsResp = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-PETTY CASH");
			
			if(null==accountHeadsResp.getBody()) {
				
				notifyMessage(5, "Branch Cash account not set");
				return;
			}
			
		}
    	else {
			if (txtInsNumber.getText().trim().isEmpty()) {
				notifyMessage(5, "Type Instrument Number!!!!");
				txtInsNumber.setDisable(false);
				txtInsNumber.requestFocus();
			} 
			else if (cmbDepositBank.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase(null)) {
				notifyMessage(5, "Type Deposit Bank!!!!");
				cmbDepositBank.setDisable(false);
				cmbDepositBank.requestFocus();
			} else if (null == dpDate.getValue()) {
				notifyMessage(5, "Select Instrument Date!!!!");
				dpDate.setDisable(false);
				dpDate.requestFocus();
			} else {
				paymentDtl.setInstrumentDate(Date.valueOf(dpDate.getValue()));
				paymentDtl.setInstrumentNumber(txtInsNumber.getText());
				paymentDtl.setBankAccountNumber(cmbDepositBank.getSelectionModel().getSelectedItem().toString());
				ResponseEntity<AccountHeads > accountHeadsResp = RestCaller.getAccountHeadByName(paymentDtl.getBankAccountNumber());
				if(null==accountHeadsResp.getBody()) {
					notifyMessage(5, " Bank account not set");
					return;
				}
			}
		}
    	ResponseEntity<PaymentDtl> respentity = RestCaller.savePaymentDtl(paymentDtl);
    	paymentDtl = respentity.getBody();
    	System.out.print(paymentDtl.getId()+"payment dtk id izzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    	RestCaller.updatePaymentDtl(paymentDtl);
    	paymentDtlLsitTable.clear();
    	paymentDtlLsitTable.add(paymentDtl);
    //	fillPaymentDtl();
		
		notifyMessage(5,"Edited Successfully");
    	 clearData();
    }

    @FXML
    void finalSave(ActionEvent event) {
    	paymentHdr.setVoucherDate(Date.valueOf(dpDate.getValue()));
    	
    	if(null == paymentHdr.getVoucherNumber())
    	{
    		String financialYear = SystemSetting.getFinancialYear();
    		String vNo = RestCaller.getVoucherNumber(financialYear + "PYN");

    		paymentHdr.setVoucherNumber(vNo);
    	}
    	
		RestCaller.updatePaymenthdr(paymentHdr);
		ResponseEntity<PaymentHdr> paymentresp = RestCaller.getPaymentHdrById(paymentHdr.getId());
		paymentHdr = paymentresp.getBody();
		RestCaller.deleteDayBookBySourceVoucher(paymentHdr.getVoucherNumber());
	    
    	ResponseEntity<List<PaymentDtl>> getPaymentDtl = RestCaller.getPaymentDtl(paymentHdr);
    	paymentDtlLsitTable = FXCollections.observableArrayList(getPaymentDtl.getBody());
		fillPaymentDtl();
		for (PaymentDtl pmt: paymentDtlLsitTable)
		{
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(paymentHdr.getBranchCode());
			ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
			AccountHeads accountHeads = accountHead.getBody();
			if(pmt.getModeOfPayment().equalsIgnoreCase(accountHeads.getAccountName()))
			{
				dayBook.setDrAccountName(SystemSetting.systemBranch+"-CASH");
				dayBook.setCrAmount(0.0);
			}
		
			else
			{
				dayBook.setDrAccountName(pmt.getBankAccountNumber());
				dayBook.setCrAmount(pmt.getAmount());
			}
			dayBook.setDrAmount(pmt.getAmount());
			System.out.print(pmt.getAccountId()+"payment account id issssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
			ResponseEntity<AccountHeads>accountHead1 = RestCaller.getAccountById(pmt.getAccountId());
			AccountHeads accountHeads1 = accountHead1.getBody();
			dayBook.setNarration(pmt.getAccountId()+paymentHdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(paymentHdr.getVoucherNumber());
			dayBook.setSourceVoucherType("PAYMENT");
			dayBook.setCrAccountName(accountHeads.getAccountName());
			
			LocalDate ldate = SystemSetting.utilToLocaDate(paymentHdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(ldate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}
		if(null != dpVoucherDate.getValue())
		{
      Date fromdate = Date.valueOf(dpVoucherDate.getValue());
    	
    	Format formatter;
		
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(fromdate);
		RestCaller.updateInvoiceEditEnableMst(strDate, paymentHdr.getVoucherNumber());
		}
    	paymentDtl = null;
    	paymentHdr = null;
    	paymentDtlLsitTable.clear();
    	tblPaymentDtl.getItems().clear();
    	showVoucherNumberAndDate();
    	paymentDtl = null;
    	paymentHdr = null;
    	paymentDtlLsitTable.clear();
    	tblPaymentDtl.getItems().clear();
    	dpDate.setValue(null);
    }

    private void clearData()
    {
    	txtAccount.clear();
    	txtAmount.clear();
    	txtInsNumber.clear();
    	txtRemarks.clear();
    	cmbDepositBank.getSelectionModel().clearSelection();
    	cmbModeOfPayment.getSelectionModel().clearSelection();
    	//dpDate.setValue(null);
    	dpInsDate.setValue(null);
    	cmbDepositBank.setPromptText(null);
    	cmbModeOfPayment.setPromptText(null);
    }
	public void notifyMessage(int duration, String msg) {

				Image img = new Image("done.png");
				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
						.onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();

				notificationBuilder.show();
			}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }
	
}
