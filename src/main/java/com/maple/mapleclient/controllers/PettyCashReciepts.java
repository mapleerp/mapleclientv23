package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PettyCashReciepts {
	String taskid;
	String processInstanceId;
	EventBus eventBus = EventBusFactory.getEventBus();
	ReceiptDtl receipt;
	Double totalamount;
	ReceiptHdr receipthdr = null;
	ReceiptDtl recieptDelete;
	String modeofpay = "";
	private ObservableList<ReceiptDtl> receiptList1 = FXCollections.observableArrayList();
	@FXML
	private TextField txtAccount;

	@FXML
	private ComboBox<String> cmbModeofReceipt;

	@FXML
	private TextField txtRemark;
	@FXML
	private Button btnClear;

	@FXML
	private TextField txtAmount;
	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnFinalSubmit;

	@FXML
	private Button btnShowAll;

	@FXML
	private DatePicker dpTransDate;
	@FXML
	private TextField txtTotal;

	@FXML
	private TableView<ReceiptDtl> tblReceiptWindow;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC1;

	@FXML
	private TableColumn<ReceiptDtl, LocalDate> RWC2;

	@FXML

	private TableColumn<ReceiptDtl, String> RWC4;

	@FXML
	private TableColumn<ReceiptDtl, Number> RWC5;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC11;

	@FXML
	private TextField txtDate;

	@FXML
	private void initialize() {
		dpTransDate = SystemSetting.datePickerFormat(dpTransDate, "dd/MMM/yyyy");
		txtDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd"));
		eventBus.register(this);
		
		//-----------------new version 1.9 surya 
//		ResponseEntity<AccountHeads> accountHead = RestCaller
//				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-PETTY CASH");
//		cmbModeofReceipt.getItems().add(accountHead.getBody().getAccountName());
//		cmbModeofReceipt.setPromptText(accountHead.getBody().getAccountName());
		
		ResponseEntity<List<ReceiptModeMst>> receiptModeResp  = RestCaller.getReceiptModeMstByName(SystemSetting.getSystemBranch()+"-PETTY CASH");
		List<ReceiptModeMst> receiptMode = receiptModeResp.getBody();
		if(receiptMode.size() > 0)
		{
			cmbModeofReceipt.getItems().add(receiptMode.get(0).getReceiptMode());
			cmbModeofReceipt.setPromptText(receiptMode.get(0).getReceiptMode());
			modeofpay = receiptMode.get(0).getReceiptMode();
			cmbModeofReceipt.setValue(receiptMode.get(0).getReceiptMode());

		}
		//-----------------new version 1.9 surya end

		
		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
		tblReceiptWindow.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					recieptDelete = new ReceiptDtl();
					recieptDelete.setId(newSelection.getId());
				}
			}
		});
	}

	private void filltable() {
		tblReceiptWindow.setItems(receiptList1);
		RWC1.setCellValueFactory(cellData -> cellData.getValue().getaccountProperty());
		RWC11.setCellValueFactory(cellData -> cellData.getValue().getremarkProperty());
		RWC5.setCellValueFactory(cellData -> cellData.getValue().getamountPropery());
		RWC2.setCellValueFactory(cellData -> cellData.getValue().gettransDateProperty());
		RWC4.setCellValueFactory(cellData -> cellData.getValue().getmodeOfPaymentProperty());
	}

	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Accounts");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtRemark.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnClear.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountEvent.getAccountName());

		}

	}

	private void clearfileds() {
		txtAccount.clear();
		txtAmount.clear();
		txtRemark.clear();

	}

	private void addItem() {
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}
			}
		}
		String account;
		String mode;
		account = txtAccount.getText().trim();
		mode = cmbModeofReceipt.getSelectionModel().getSelectedItem();
		if (mode.equalsIgnoreCase(account)) {
			notifyMessage(4, "Mode of Receipt and Account Heads should not be same");
			return;
		}
		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}

		if (txtAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "Select Account!!!!");
			txtAccount.requestFocus();

			return;
		}
		if (txtAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "Type Amount!!!!");
			txtAmount.requestFocus();
			return;
		}
//		if (null == dpTransDate.getValue()) {
//			notifyMessage(5, "Please Select Date!!!!");
//			dpTransDate.requestFocus();
//			return;
//		}
		if (null == cmbModeofReceipt.getSelectionModel()) {
			if (txtAccount.getText().trim().equalsIgnoreCase(cmbModeofReceipt.getSelectionModel().getSelectedItem())) {
				notifyMessage(5, "Debit And Credit Accounts should not be same");
				return;
			}

		}
		if (null == receipthdr) {
			receipthdr = new ReceiptHdr();

			receipthdr.setTransdate(SystemSetting.systemDate);
			receipthdr.setVoucherDate(SystemSetting.systemDate);
			receipthdr.setBranchCode(SystemSetting.systemBranch);

			ResponseEntity<ReceiptHdr> respentity = RestCaller.saveReceipthdr(receipthdr);
			receipthdr = respentity.getBody();
		}
		receipt = new ReceiptDtl();
		ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountHeadByName(txtAccount.getText());
		ResponseEntity<AccountHeads> accountHead = RestCaller
				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-PETTY CASH");
		receipt.setDebitAccountId(accountHead.getBody().getId());
		receipt.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
		receipt.setAccount(accSaved.getBody().getId());
		receipt.setRemark(txtRemark.getText());
		receipt.setAmount(Double.parseDouble(txtAmount.getText()));
		receipt.setInstrumentDate(null);
		receipt.setModeOfpayment(modeofpay);
		receipt.setReceipthdr(receipthdr);
		ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);
		receipt = respentity.getBody();
		setTotal();
		receipt.setAccountName(txtAccount.getText());
		receiptList1.add(receipt);
		filltable();
		clearfileds();
		txtAccount.requestFocus();
		btnSave.setDisable(true);
	}

	@FXML
	void clear(ActionEvent event) {
		clearfileds();
		txtTotal.clear();
	}

	@FXML
	void save(ActionEvent event) {
		addItem();
	}

	@FXML
	void loadpopup(MouseEvent event) {
		showPopup();
	}

	@FXML
	void OnEnterClick(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtAccount.getText().trim().isEmpty())
				cmbModeofReceipt.requestFocus();
			else
				showPopup();

		}
	}

	@FXML
	void voucherDateKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();

		}
	}

	@FXML
	void amountKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}
	}

	@FXML
	void SaveOnKeyPrss(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void RemarkKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbModeofReceipt.requestFocus();

		}
	}

	@FXML
	void modeOfReceiptKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtRemark.requestFocus();

		}
	}

	@FXML
	void delete(ActionEvent event) {
		btnSave.setDisable(false);
		RestCaller.deleteReceiptDtl(recieptDelete.getId());
		notifyMessage(5, "Deleted");

		tblReceiptWindow.getItems().clear();
		ResponseEntity<List<ReceiptDtl>> receiptdtlSaved = RestCaller.getReceiptDtl(receipthdr);
		receiptList1 = FXCollections.observableArrayList(receiptdtlSaved.getBody());
		for (ReceiptDtl recptdtl : receiptList1) {
			ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountById(recptdtl.getAccount());
			recptdtl.setAccountName(accSaved.getBody().getAccountName());
		}
		tblReceiptWindow.setItems(receiptList1);
		txtTotal.clear();
		setTotal();
		
	}

	@FXML
	void finalSubmit(ActionEvent event) {
		ResponseEntity<List<ReceiptDtl>> receiptdtlSaved = RestCaller.getReceiptDtl(receipthdr);
		receiptList1 = FXCollections.observableArrayList(receiptdtlSaved.getBody());
		if (receiptdtlSaved.getBody().size() == 0) {
			return;
		}
		String financialYear = SystemSetting.getFinancialYear();

		String vNo = RestCaller.getVoucherNumber(financialYear + "RECEIPT");
		receipthdr.setVoucherNumber(vNo);
//		  Date date = Date.valueOf(LocalDate.now());
//			
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
//			
//			
//			receipthdr.setVoucherDate(date);
//			
		RestCaller.updateReceipthdr(receipthdr);
		for (ReceiptDtl rcpt : receiptList1) {
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(receipthdr.getBranchCode());
			dayBook.setDrAccountName(SystemSetting.systemBranch + "-PETTY CASH");
			dayBook.setDrAmount(rcpt.getAmount());
			ResponseEntity<AccountHeads> accountHead = RestCaller.getAccountById(rcpt.getAccount());
			AccountHeads accountHeads = accountHead.getBody();
			dayBook.setNarration(accountHeads.getAccountName() + receipthdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(receipthdr.getVoucherNumber());
			dayBook.setSourceVoucherType("RECEIPT");
			dayBook.setCrAccountName(accountHeads.getAccountName());
			dayBook.setCrAmount(rcpt.getAmount());
			LocalDate ldate = SystemSetting.utilToLocaDate(receipthdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(ldate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}
		String strDate = formatter.format(receipthdr.getVoucherDate());
		RestCaller.saveReceipthdr(receipthdr);
		txtTotal.clear();
		notifyMessage(5, "Saved Successfully!!!!");
		tblReceiptWindow.getItems().clear();
		
		receiptList1.clear();
		String voucherNo =receipthdr.getVoucherNumber();
		receipthdr = null;
		receipt = null;
		try {

			JasperPdfReportService.ReceiptInvoice(voucherNo, strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btnSave.setDisable(false);
		
	}

	private void setTotal() {

		totalamount = RestCaller.getSummaryReceiptTotal(receipthdr.getId());
		txtTotal.setText(Double.toString(totalamount));
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }

}
