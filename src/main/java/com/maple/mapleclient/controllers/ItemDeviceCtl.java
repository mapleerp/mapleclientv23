package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.lowagie.text.pdf.AcroFields.Item;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemDeviceMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ItemDeviceCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<ItemDeviceMst> ItemTableList2 = FXCollections.observableArrayList();

	private EventBus eventBus = EventBusFactory.getEventBus();

	ItemDeviceMst itemDeviceMst = null;
    @FXML
    private TextField txtCategory;

    @FXML
    private TextField txtItemName;

    @FXML
    private ComboBox<String> cmbdeviceType;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowall;

    @FXML
    private TableView<ItemDeviceMst> tbItemDevice;

    @FXML
    private TableColumn<ItemDeviceMst,String> clItemName;

    @FXML
    private TableColumn<ItemDeviceMst,String> clDeviceType;
    @FXML
   	private void initialize() {
   		// btnAdd.setVisible(false);
   		eventBus.register(this);
   		cmbdeviceType.getItems().add("ROUTE");
   		cmbdeviceType.getItems().add("ONLINE");
   		cmbdeviceType.getItems().add("KOT");
   		tbItemDevice.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					itemDeviceMst = new ItemDeviceMst();
					itemDeviceMst.setId(newSelection.getId());
				}
			}
		});
    }
    private void showPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			cmbdeviceType.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    private void loadCategoryPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    @Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {
			txtItemName.setText(itemPopupEvent.getItemName());
		}
	}
    
	  @Subscribe
		public void popupOrglistner(CategoryEvent CategoryEvent) {

			Stage stage = (Stage) btnDelete.getScene().getWindow();
			if (stage.isShowing()) {

				
				txtCategory.setText(CategoryEvent.getCategoryName());
		
			

			}
	  }
    @FXML
    void actionDelete(ActionEvent event) {
    	if(null == itemDeviceMst)
    	{
    		return;
    	}
    	if(null == itemDeviceMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteItemDeviceMstById(itemDeviceMst.getId());
    	ResponseEntity<List<ItemDeviceMst>> itemdeciveResp = RestCaller.getAllItemDeviceMst();
    	ItemTableList2 = FXCollections.observableArrayList(itemdeciveResp.getBody());	
    	fillTable();
    	itemDeviceMst = null;
    }

    @FXML
    void loadCategory(KeyEvent event) {

    	if(event.getCode()==KeyCode.ENTER)
    	{
    		loadCategoryPopup();
    	}
    }

    @FXML
    void loadItem(KeyEvent event) {

    	if(event.getCode() == KeyCode.ENTER)
    	{
    		showPopup();
    	}
    }
    @FXML
    void actionSave(ActionEvent event) {

    	if(!txtCategory.getText().trim().isEmpty()&&!txtItemName.getText().trim().isEmpty())
    	{
    		notifyMessage(3,"Choose Either Item or Category", true);
    		txtCategory.clear();
    		txtItemName.clear();
    		return;
    	}
    	if(!txtCategory.getText().trim().isEmpty())
    	{
    		ResponseEntity<CategoryMst> catmst = RestCaller.getCategoryByName(txtCategory.getText());
    		ResponseEntity<List<ItemMst>> itemlist = RestCaller.getItemsByCategory(catmst.getBody());
    		for(int i=0;i<itemlist.getBody().size();i++)
    		{
    			itemDeviceMst = new ItemDeviceMst();
    	    	ResponseEntity<ItemMst> itemmst =  RestCaller.getItemByNameRequestParam(itemlist.getBody().get(i).getItemName());
    	    	if(null == itemmst.getBody())
    	    	{
    	    		continue;
    	    	}
    	    	itemDeviceMst.setItemId(itemmst.getBody().getId());
    	    	itemDeviceMst.setBranchCode(SystemSetting.systemBranch);
    	    	itemDeviceMst.setDeviceType(cmbdeviceType.getSelectionModel().getSelectedItem());
    	    	itemDeviceMst.setItemName(txtItemName.getText());
    	    	ResponseEntity<ItemDeviceMst> respentity = RestCaller.saveItemDeviceMst(itemDeviceMst);
    	    	itemDeviceMst = respentity.getBody();
    	    	ItemTableList2.add(itemDeviceMst);
    		}
    	}
    	else
    	{
    	itemDeviceMst = new ItemDeviceMst();
    	ResponseEntity<ItemMst> itemmst =  RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	itemDeviceMst.setItemId(itemmst.getBody().getId());
    	itemDeviceMst.setBranchCode(SystemSetting.systemBranch);
    	itemDeviceMst.setDeviceType(cmbdeviceType.getSelectionModel().getSelectedItem());
    	itemDeviceMst.setItemName(txtItemName.getText());
    	ResponseEntity<ItemDeviceMst> respentity = RestCaller.saveItemDeviceMst(itemDeviceMst);
    	itemDeviceMst = respentity.getBody();
    	ItemTableList2.add(itemDeviceMst);
    	}
    	fillTable();
    	txtCategory.clear();
    	txtItemName.clear();
    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

    private void fillTable()
    {
    	for(ItemDeviceMst item:ItemTableList2)
    	{
    		ResponseEntity<ItemMst> itembyid = RestCaller.getitemMst(item.getItemId());
    		item.setItemName(itembyid.getBody().getItemName());
    	}
    	tbItemDevice.setItems(ItemTableList2);
		clDeviceType.setCellValueFactory(cellData -> cellData.getValue().getDeviceTypeproperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());


    }
    @FXML
    void actionShowAll(ActionEvent event) {
    	ResponseEntity<List<ItemDeviceMst>> itemdeciveResp = RestCaller.getAllItemDeviceMst();
    	ItemTableList2 = FXCollections.observableArrayList(itemdeciveResp.getBody());	
    	fillTable();
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
