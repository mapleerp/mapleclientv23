package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.events.ItemPopupEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class ProductionRawMaterialItemWiseReport {
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private DatePicker dpToDate;
    @FXML
    private TableView<ProductionDtlDtl> tblRawMaterial;

    @FXML
    private TableColumn<ProductionDtlDtl, String> clRawMaterial;

    @FXML
    private TableColumn<ProductionDtlDtl, Number> clQty;

    @FXML
    private TableColumn<ProductionDtlDtl, String> clUnit;
    @FXML
    void loadPopUp(MouseEvent event) {
    	showItemMst();
    }

    @FXML
    void printReport(ActionEvent event) {

    	
    }

    @FXML
    void showTable(ActionEvent event) {

    }
    @FXML
   	private void initialize() {
   		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
   		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
       }
    private void showItemMst() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Subscribe
	public void popupItemlistner(ItemPopupEvent itempopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnShow.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {

			txtItemName.setText(itempopupEvent.getItemName());
			
			
		}
	}
	 @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }
}