package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BrandMst;
import com.maple.mapleclient.entity.JobCardInHdr;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class JobCardVoucherPopUpCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<JobCardInHdr> jobcardInList = FXCollections.observableArrayList();
	VoucherNoEvent voucherNoEvent;
	StringProperty SearchString = new SimpleStringProperty();
	@FXML
	private TableView<JobCardInHdr> tblJobCard;

	@FXML
	private TableColumn<JobCardInHdr, String> clVoucherNumber;

	@FXML
	private TableColumn<JobCardInHdr, String> clFromBranch;

	@FXML
	private TextField txtSearch;

	@FXML
	private Button btnOk;

	@FXML
	private Button btnCancel;

	@FXML
	private void initialize() {
		txtSearch.textProperty().bindBidirectional(SearchString);
		LoadBrandBySearch("");
		voucherNoEvent = new VoucherNoEvent();
		tblJobCard.setItems(jobcardInList);
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
		clFromBranch.setCellValueFactory(cellData -> cellData.getValue().getfromBranchProprty());

		tblJobCard.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId() && newSelection.getVoucherNumber().length() > 0) {
					voucherNoEvent.setId(newSelection.getId());
					voucherNoEvent.setVoucherNumber(newSelection.getVoucherNumber());
					voucherNoEvent.setFromBranchCode(newSelection.getFromBranch());
				}
			}
		});

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadBrandBySearch((String) newValue);

			}
		});
	}

	@FXML
	void actionCancel(ActionEvent event) {
		Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
	}

	@FXML
	void actionOk(ActionEvent event) {
		eventBus.post(voucherNoEvent);

		Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
	}

	@FXML
	void onKeyPresstxt(KeyEvent event) {
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblJobCard.requestFocus();
			tblJobCard.getSelectionModel().selectFirst();

		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
		}
	}

	private void LoadBrandBySearch(String searchData) {


		jobcardInList.clear();

		ResponseEntity<List<JobCardInHdr>> getPendingJobs = RestCaller.getPendingVoucher();
		jobcardInList = FXCollections.observableArrayList(getPendingJobs.getBody());
		// tblJobCard.setItems(jobcardInList);
		return;
	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }


}
