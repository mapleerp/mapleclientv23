package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.PrinterMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

public class PrinterCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<PrinterMst> PrinterMstList = FXCollections.observableArrayList();
	PrinterMst printerMst  = null;
    @FXML
    private TextField txtPrinterName;

    @FXML
    private Button btnShowAll;

    @FXML
    private TextField txtDescription;

    @FXML
    private TableView<PrinterMst> tblPrinter;

    @FXML
    private TableColumn<PrinterMst, String> clPrinter;

    @FXML
    private TableColumn<PrinterMst, String> clDescription;
    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;
    @FXML
	private void initialize() {
    	tblPrinter.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					printerMst = new PrinterMst();
					printerMst.setId(newSelection.getId());
				}
			}
		});
    }
    @FXML
    void actionShowAll(ActionEvent event) {

    	ResponseEntity<List<PrinterMst>> getPrinter = RestCaller.getAllPrinterMst();
    	PrinterMstList = FXCollections.observableArrayList(getPrinter.getBody());
    	fillTable();

    }
    @FXML
    void actionDelete(ActionEvent event) {
    
    
    	if(null == printerMst)
    	{
    		return;
    	}
    	if(null == printerMst.getId())
    	{
    		return;
    	}
    	RestCaller.deletePrinterMst(printerMst.getId());
    	printerMst = null;
    	ResponseEntity<List<PrinterMst>> getAllPrinter = RestCaller.getAllPrinterMst();
    	PrinterMstList = FXCollections.observableArrayList(getAllPrinter.getBody());
    	fillTable();

    }
    private void fillTable()
    {
    	tblPrinter.setItems(PrinterMstList);
		clPrinter.setCellValueFactory(cellData -> cellData.getValue().getprinterNameProperty());
		clDescription.setCellValueFactory(cellData -> cellData.getValue().getprinterDescriptionProperty());
    }

    @FXML
    void actionSave(ActionEvent event) {
    	PrinterMst printerMst = new PrinterMst();
    	printerMst.setPrinterName(txtPrinterName.getText());
    	printerMst.setPrinterDescription(txtDescription.getText());
    	ResponseEntity<PrinterMst> respentity = RestCaller.savePrinterMst(printerMst);
    	printerMst = respentity.getBody();
    	PrinterMstList.add(printerMst);
    	fillTable();
    	printerMst = null;
    	txtDescription.clear();
    	txtPrinterName.clear();
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
