package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JobCardDtl;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ActiveJobCardReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class JobCardDtlReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<JobCardDtl> jobCardList = FXCollections.observableArrayList();


    @FXML
    private TableView<JobCardDtl> tblItems;

    @FXML
    private TableColumn<JobCardDtl, String> Customername;

    @FXML
    private TableColumn<JobCardDtl, String> clItemName;

    @FXML
    private TableColumn<JobCardDtl, Number> clQty;

    @FXML
    private TableColumn<JobCardDtl, Number> clMrp;

    @FXML
    private TableColumn<JobCardDtl, Number> clTax;

    @FXML
    private TableColumn<JobCardDtl, Number> clCess;

    @FXML
    private Button btnBack;

    @FXML
    private TextField txtCustomer;

    @FXML
    private TextField txtItemName;

    @FXML
    void Back(ActionEvent event) {
    	
    	 Stage stage = (Stage) tblItems.getScene().getWindow();
			stage.close();
    }
    
    @FXML
    void BackOnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ESCAPE) {
			  Stage stage = (Stage) tblItems.getScene().getWindow();
				stage.close();

			}
    }

	public void JobCardDtlByHdrId(String jobCardHdrId) {
		
		
		
		ResponseEntity<List<JobCardDtl>> jobCardResp = RestCaller.getJobCardDtlByHdrId(jobCardHdrId);
		jobCardList = FXCollections.observableArrayList(jobCardResp.getBody());
		
		if(jobCardList.size() > 0)
		{
			JobCardHdr jobCardHdr = new JobCardHdr();
			jobCardHdr = jobCardList.get(0).getJobCardHdr();
			ResponseEntity<AccountHeads> custResp = RestCaller.getAccountHeadsById(jobCardHdr.getCustomerId());
			AccountHeads accountHeads = custResp.getBody();
			
			txtCustomer.setText(accountHeads.getAccountName());
			txtItemName.setText(jobCardHdr.getServiceInDtl().getItemName());

		}
		fillTableDtl();
	
	}
	
	private void fillTableDtl() {

		for (JobCardDtl jobCard : jobCardList) {
			if (null != jobCard.getItemId()) {
				ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(jobCard.getItemId());
				ItemMst itemMst = itemResp.getBody();
				jobCard.setItemName(itemMst.getItemName());
				jobCard.setMrp(itemMst.getStandardPrice());
				jobCard.setCess(itemMst.getCess());
				jobCard.setTax(itemMst.getTaxRate());

			}
		}

		tblItems.setItems(jobCardList);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clCess.setCellValueFactory(cellData -> cellData.getValue().getCessProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());

	}

	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
}
