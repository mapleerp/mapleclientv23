package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ConsumptionReasonMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.GSTInputDtlAndSmryReport;
import com.maple.report.entity.StockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PricetypeWiseStockSummaryReportCtl {


    @FXML
    private DatePicker dpFromDate;

    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private ComboBox<String> cmbPriceType;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnPrintReport;

    @FXML
    private TableView<StockReport> tblStockSummaryReport;

    @FXML
    private TableColumn<StockReport, String> clmnItemName;

    @FXML
    private TableColumn<StockReport, Number> clmnQuantity;

    @FXML
    private TableColumn<StockReport, Number> clmnStandardPrice;

    @FXML
    private TableColumn<StockReport, String> clmnBranch;

    @FXML
    private TableColumn<StockReport, String> clmnCategory;
    
   
    private ObservableList<StockReport> getStockSummarydetailsList = FXCollections.observableArrayList();

    String priceid=null;
    String branch=null;
    String pricetype =null;
    
    EventBus eventBus = EventBusFactory.getEventBus();
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	
    	ResponseEntity<List<PriceDefenitionMst>> getAll = RestCaller.getAllPriceDefenition();

    	for(PriceDefenitionMst priceDefenitionMst:getAll.getBody())
    	{
    		cmbPriceType.getItems().add(priceDefenitionMst.getPriceLevelName());
    	}
    	

    	
    	ResponseEntity<List<BranchMst>> getAllBranch = RestCaller.getBranchMst();

    	for(BranchMst branchMst:getAllBranch.getBody())
    	{
    		cmbBranch.getItems().add(branchMst.getBranchName());
    	}
    	

    	
  	
    }
    @FXML
    void generateReport(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
			return;
    	}
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, "Please select to date", false);
    		return;
    	}
    	if(null == cmbBranch.getSelectionModel().getSelectedItem())
    	{
    		notifyMessage(5, "Please select to branch", false);
    		return;
    	}
    	if(null == cmbPriceType.getSelectionModel().getSelectedItem())
    	{
    		notifyMessage(5, "Please select to Price Type", false);
    		return;
    	}
    		
    		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
    		
    		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
    		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
    		
//    		ResponseEntity<List<PriceDefenitionMst>> getPriceId = RestCaller.getPriceDefenitionByName(cmbPriceType.getSelectionModel().getSelectedItem());
//    		
    		pricetype = cmbPriceType.getSelectionModel().getSelectedItem();
    		branch = cmbBranch.getSelectionModel().getSelectedItem();
    		
    		ResponseEntity<List<StockReport>> getStockSummarydetails=RestCaller.getStockSummaryDetails(sfdate,stdate,
    				cmbPriceType.getSelectionModel().getSelectedItem(),cmbBranch.getSelectionModel().getSelectedItem());
    		getStockSummarydetailsList = FXCollections.observableArrayList(getStockSummarydetails.getBody());
    		FillTable();
    		
    		dpFromDate.getEditor().clear();
    		dpToDate.getEditor().clear();
    		cmbBranch.getSelectionModel().clearSelection();
    		cmbPriceType.getSelectionModel().clearSelection();
    }

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    private void FillTable() {

		tblStockSummaryReport.setItems(getStockSummarydetailsList);

		clmnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clmnQuantity.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clmnStandardPrice.setCellValueFactory(cellData -> cellData.getValue().getstandardPriceProperty());
		clmnBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
		clmnCategory.setCellValueFactory(cellData -> cellData.getValue().getcategoryProperty());

	}
    
	@FXML
    void printReport(ActionEvent event) {
		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		


		  try { 
			  JasperPdfReportService.priceTypeWiseStockSummaryReport(sfdate,stdate,pricetype,branch);
			  } catch (JRException e) { // TODO Auto-gesnerated catch block
		  e.printStackTrace(); }
    }
	
}
