package com.maple.mapleclient.controllers;

import java.sql.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;

import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.RawMaterialReturnDtl;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.entity.ReceiptInvoiceDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ReceiptEditCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<ReceiptHdr> receiptLsitTable = FXCollections.observableArrayList();
	private ObservableList<ReceiptDtl> receiptDtlLsitTable = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();

	OwnAccount ownAccount = null;
	ReceiptHdr receiptHdr = null;
	ReceiptDtl receiptDtl = null;
	
    @FXML
    private DatePicker dpVoucherDate;
    @FXML
    private DatePicker dpDate;
    @FXML
    private TableView<ReceiptHdr> tblReceiptHdr;

    @FXML
    private TableColumn<ReceiptHdr, String> clVoucherDate;

    @FXML
    private TableColumn<ReceiptHdr, String> clVoucherNumber;

    @FXML
    private Button btnFetchData;

    @FXML
    private TableView<ReceiptDtl> tblReceiptDtl;

    @FXML
    private TableColumn<ReceiptDtl, String> clAccount;

    @FXML
    private TableColumn<ReceiptDtl, LocalDate> clDate;

    @FXML
    private TableColumn<ReceiptDtl, String> clModeOfPayment;

    @FXML
    private TableColumn<ReceiptDtl, String> clDepositBank;

    @FXML
    private TableColumn<ReceiptDtl, String> clInstNo;

    @FXML
    private TableColumn<ReceiptDtl,LocalDate> clInstDate;

    @FXML
    private TableColumn<ReceiptDtl, String> clInvoiceNo;

    @FXML
    private TableColumn<ReceiptDtl, String> clRemarks;

    @FXML
    private TableColumn<ReceiptDtl, Number> clAmount;

    @FXML
    private TextField txtAccount;

    @FXML
    private ComboBox<String> cmbModeOfPayment;

    @FXML
    private TextField txtRemarks;

    @FXML
    private TextField txtAmount;

    @FXML
    private ComboBox<String> cmbDepositBank;

    @FXML
    private DatePicker dpInsDate;

    @FXML
    private TextField txtInsNumber;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFinalSave;
    
    @FXML
   	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    	dpInsDate = SystemSetting.datePickerFormat(dpInsDate, "dd/MMM/yyyy");
    	dpVoucherDate = SystemSetting.datePickerFormat(dpVoucherDate, "dd/MMM/yyyy");
		eventBus.register(this);
		ResponseEntity<AccountHeads>accountHeadPetty = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-PETTY CASH");

    	ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
		AccountHeads accountHeads = accountHead.getBody();
		accountHeads.setId(accountHead.getBody().getId());
		cmbModeOfPayment.getItems().add("TRASNFER");
		cmbModeOfPayment.getItems().add("CHEQUE");
		cmbModeOfPayment.getItems().add("DD");
		cmbModeOfPayment.getItems().add("MOBILE PAY");
		cmbModeOfPayment.getItems().add(accountHead.getBody().getAccountName());
		cmbModeOfPayment.getItems().add(accountHeadPetty.getBody().getAccountName());

    	tblReceiptHdr.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				System.out.println(newSelection);
				if (null != newSelection.getVoucherNumber()) {
					Date fromdate = Date.valueOf(dpVoucherDate.getValue());
			    	
			    	Format formatter;
					
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					String strDate = formatter.format(fromdate);
				ResponseEntity<ReceiptHdr> respentity = RestCaller.getReceiptHdrByVDateAndVno(strDate,newSelection.getVoucherNumber());
			
				receiptHdr = new ReceiptHdr();
				receiptHdr = respentity.getBody();
				System.out.println("RECEIPT HDR "+receiptHdr);
				}

			}
		});
    	ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid= lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbDepositBank.getItems().add((String) bankName);
				
						    //accountHeads.getAccountName);
			}
		}
    	tblReceiptDtl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					
					receiptDtl = new ReceiptDtl();
					receiptDtl.setId(newSelection.getId());
					ResponseEntity<AccountHeads> getAc = RestCaller.getAccountById(newSelection.getAccount());
					txtAccount.setText(getAc.getBody().getAccountName());
					txtAmount.setText(Double.toString(newSelection.getAmount()));
					txtInsNumber.setText(newSelection.getInstnumber());
					txtRemarks.setText(newSelection.getRemark());
					cmbDepositBank.setPromptText(newSelection.getDepositBank());
					cmbDepositBank.setValue(newSelection.getDepositBank());
					cmbModeOfPayment.setPromptText(newSelection.getModeOfpayment());
					cmbModeOfPayment.setValue(newSelection.getModeOfpayment());
					
					dpDate.setValue(newSelection.getTransDate().toLocalDate());
					dpInsDate.setValue(newSelection.getInstrumentDate().toLocalDate());
					
				}

			}
		});
    	cmbModeOfPayment.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (newValue.matches(SystemSetting.getSystemBranch()+"-CASH") || newValue.matches(SystemSetting.getSystemBranch()+"-PETTY CASH") )  {
					cmbDepositBank.setDisable(true);
					txtInsNumber.setDisable(true);
					dpInsDate.setDisable(true);
					dpInsDate.setValue(null);
				} else {
					cmbDepositBank.setDisable(false);
					txtInsNumber.setDisable(false);
					dpInsDate.setDisable(false);
				}
			}
		});
    }

    private void fillReceiptHdr()
    {
    	tblReceiptHdr.setItems(receiptLsitTable);
    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
    	clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());


    }

    @FXML
    void deleteReceipt(ActionEvent event) {

    }
    @FXML
    void fetchData(ActionEvent event) {
    	

    	if(null != receiptHdr)
    	{
    		if(null != receiptHdr.getVoucherNumber())
    		{
    			ResponseEntity<List<ReceiptDtl>> getReceiptDtl = RestCaller.getReceiptDtl(receiptHdr);
    			receiptDtlLsitTable = FXCollections.observableArrayList(getReceiptDtl.getBody());
    			fillReceiptDtl();
    		}
    	}
    	showVoucherNumberAndDate();
    	
    }
    private void showVoucherNumberAndDate()
    {
    	tblReceiptHdr.getItems().clear();
    	if(null != dpVoucherDate.getValue())
    	{
     	Date fromdate = Date.valueOf(dpVoucherDate.getValue());
    	
    	Format formatter;
		
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(fromdate);
//    	ResponseEntity<List<ReceiptHdr>> getReceipts = RestCaller.getAllreceiptsByVoucherDate(strDate);
//    	List<ReceiptHdr> ReceiptList = getReceipts.getBody();
    	
    	ArrayList ReceiptList = new ArrayList();
    	ReceiptList = RestCaller.getAllreceiptsByVoucherDate(strDate);
		
    	Iterator itr = ReceiptList.iterator();

    	String voucherNo = null;;
    	String voucherDate = null;
		while (itr.hasNext()) {
			List element = (List) itr.next();
			voucherNo = (String) element.get(0);
			voucherDate = (String) element.get(1);

			if (!voucherNo.equals("") && !voucherNo.equals(null)) {

				ReceiptHdr receiptHdr = new ReceiptHdr();
				receiptHdr.setVoucherNumber(voucherNo);
				
				receiptHdr.setVoucherDate(SystemSetting.StringToUtilDate(voucherDate, "yyyy-MM-dd"));
				receiptLsitTable.add(receiptHdr);
			}
		}
    	}
    	fillReceiptHdr();
    }
    private void fillReceiptDtl()
    {
    	for(ReceiptDtl recpt:receiptDtlLsitTable )
    	{
    		ResponseEntity<AccountHeads> getAcc = RestCaller.getAccountById(recpt.getAccount());
    		recpt.setAccountName(getAcc.getBody().getAccountName());
    	}
    	tblReceiptDtl.setItems(receiptDtlLsitTable);
    	clAccount.setCellValueFactory(cellData -> cellData.getValue().getaccountProperty());
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountPropery());
    	clDate.setCellValueFactory(cellData -> cellData.getValue().gettransDateProperty());
    	clDepositBank.setCellValueFactory(cellData -> cellData.getValue().getdepositBankProperty());
    	clInstDate.setCellValueFactory(cellData -> cellData.getValue().getInstrumentDateProperty());
    	clInvoiceNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
    	clModeOfPayment.setCellValueFactory(cellData -> cellData.getValue().getmodeOfPaymentProperty());
    	clRemarks.setCellValueFactory(cellData -> cellData.getValue().getremarkProperty());
    }
    @FXML
    void loadAccounts(MouseEvent event) {
    	showPopup();
    }
    private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Accounts");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbModeOfPayment.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnFetchData.getScene().getWindow();
		if (stage.isShowing()) {

					txtAccount.setText(accountEvent.getAccountName());
	
		}

	}
	
	@Subscribe
	public void popuplistnerVoucher(VoucherNoEvent vno) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnFetchData.getScene().getWindow();
		if (stage.isShowing()) {

					String id = vno.getId();
					ResponseEntity<ReceiptHdr > receipthdrresp = RestCaller.getReceiptHdrById(id);
					receiptHdr = receipthdrresp.getBody();
					ResponseEntity<List<ReceiptDtl>> getReceiptDtl = RestCaller.getReceiptDtl(receiptHdr);
	    			receiptDtlLsitTable = FXCollections.observableArrayList(getReceiptDtl.getBody());
	    			fillReceiptDtl();
	
		}

	}

    @FXML
    void save(ActionEvent event) {
    	RestCaller.deleteReceiptDtl(receiptDtl.getId());
    	receiptDtl = new ReceiptDtl();
    	ResponseEntity<AccountHeads> getaccount = RestCaller.getAccountHeadByName(txtAccount.getText());
    	receiptDtl.setAmount(Double.parseDouble(txtAmount.getText()));
    	receiptDtl.setBranchCode(SystemSetting.systemBranch);
    	if(cmbModeOfPayment.getSelectionModel().getSelectedItem().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-CASH"))
    	{
			ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
			receiptDtl.setDebitAccountId(accountHead.getBody().getId());
    	}
    	else if(cmbModeOfPayment.getSelectionModel().getSelectedItem().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-PETTY CASH")) {
    		ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-PETTY CASH");
			receiptDtl.setDebitAccountId(accountHead.getBody().getId());
    	}
    	else
    	{
    	receiptDtl.setDebitAccountId(getaccount.getBody().getId());
    	}
    	receiptDtl.setAccount(getaccount.getBody().getId());
    	receiptDtl.setRemark(txtRemarks.getText());
		receiptDtl.setTransDate(Date.valueOf(dpDate.getValue()));
		receiptDtl.setReceipthdr(receiptHdr);
		receiptDtl.setModeOfpayment(cmbModeOfPayment.getSelectionModel().getSelectedItem());
		receiptDtl.setReceipthdr(receiptHdr);
    	if (cmbModeOfPayment.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-CASH"))
		{
    		receiptDtl.setInstrumentDate(null);
    		
			ResponseEntity<AccountHeads > accountHeadsResp = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
			
			if(null==accountHeadsResp.getBody()) {
				
				notifyMessage(5, "Branch Cash account not set");
				return;
			}
			
		}else if (cmbModeOfPayment.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-PETTY CASH"))
		{
    		receiptDtl.setInstrumentDate(null);
    		
			ResponseEntity<AccountHeads > accountHeadsResp = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-PETTY CASH");
			
			if(null==accountHeadsResp.getBody()) {
				
				notifyMessage(5, "Branch Cash account not set");
				return;
			}
			
		}
    	else {
			if (txtInsNumber.getText().trim().isEmpty()) {
				notifyMessage(5, "Type Instrument Number!!!!");
				txtInsNumber.setDisable(false);
				txtInsNumber.requestFocus();
			} 
			else if (cmbDepositBank.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase(null)) {
				notifyMessage(5, "Type Deposit Bank!!!!");
				cmbDepositBank.setDisable(false);
				cmbDepositBank.requestFocus();
			} else if (null == dpDate.getValue()) {
				notifyMessage(5, "Select Instrument Date!!!!");
				dpDate.setDisable(false);
				dpDate.requestFocus();
			} else {
				receiptDtl.setInstrumentDate(Date.valueOf(dpDate.getValue()));
				receiptDtl.setInstnumber(txtInsNumber.getText());
				receiptDtl.setDepositBank(cmbDepositBank.getSelectionModel().getSelectedItem().toString());
				ResponseEntity<AccountHeads > accountHeadsResp = RestCaller.getAccountHeadByName(receiptDtl.getDepositBank());
				if(null==accountHeadsResp.getBody()) {
					notifyMessage(5, " Bank account not set");
					return;
				}
			}
		}
    	ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receiptDtl);
    	receiptDtl = respentity.getBody();
    	RestCaller.updateReceiptDtl(receiptDtl);
    	receiptDtlLsitTable.clear();
    	receiptDtlLsitTable.add(receiptDtl);
    	fillReceiptDtl();
		
		notifyMessage(5,"Edited Successfully");
    	 clearData();
    }

    @FXML
    void finalSave(ActionEvent event) {
    	receiptHdr.setVoucherDate(Date.valueOf(dpDate.getValue()));
		RestCaller.updateReceipthdr(receiptHdr);
		RestCaller.deleteDayBookBySourceVoucher(receiptHdr.getVoucherNumber());
	    ResponseEntity<ReceiptHdr> resp= RestCaller.getReceiptHdrById(receiptHdr.getId());
	    receiptHdr = resp.getBody();
    	ResponseEntity<List<ReceiptDtl>> getReceiptDtl = RestCaller.getReceiptDtl(receiptHdr);
		receiptDtlLsitTable = FXCollections.observableArrayList(getReceiptDtl.getBody());
		fillReceiptDtl();
		for (ReceiptDtl rcpt: receiptDtlLsitTable)
		{
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(receiptHdr.getBranchCode());
			ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
			AccountHeads accountHeads = accountHead.getBody();
			if(rcpt.getModeOfpayment().equalsIgnoreCase(accountHeads.getAccountName()))
			{
				dayBook.setDrAccountName(SystemSetting.systemBranch+"-CASH");
				dayBook.setCrAmount(0.0);
			}
		
			else
			{
				dayBook.setDrAccountName(rcpt.getDepositBank());
				dayBook.setCrAmount(rcpt.getAmount());
			}
			dayBook.setDrAmount(rcpt.getAmount());
			ResponseEntity<AccountHeads>accountHead1 = RestCaller.getAccountById(rcpt.getAccount());
			AccountHeads accountHeads1 = accountHead1.getBody();
			dayBook.setNarration(accountHeads1.getAccountName()+receiptHdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(receiptHdr.getVoucherNumber());
			dayBook.setSourceVoucherType("RECEIPT");
			dayBook.setCrAccountName(accountHeads1.getAccountName());
			
			LocalDate ldate = SystemSetting.utilToLocaDate(receiptHdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(ldate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}
		if(null != dpVoucherDate.getValue())
		{
	Date fromdate = Date.valueOf(dpVoucherDate.getValue());
    	
    	Format formatter;
		
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(fromdate);
		RestCaller.updateInvoiceEditEnableMst(strDate, receiptHdr.getVoucherNumber());
		}
    	receiptDtl = null;
    	receiptHdr = null;
    	receiptDtlLsitTable.clear();
    	tblReceiptDtl.getItems().clear();
    	dpDate.setValue(null);
    	showVoucherNumberAndDate();
    }

    private void clearData()
    {
    	txtAccount.clear();
    	txtAmount.clear();
    	txtInsNumber.clear();
    	txtRemarks.clear();
    	cmbDepositBank.getSelectionModel().clearSelection();
    	cmbModeOfPayment.getSelectionModel().clearSelection();
//    	dpDate.setValue(null);
    	dpInsDate.setValue(null);
    	cmbDepositBank.setPromptText(null);
    	cmbModeOfPayment.setPromptText(null);
    }
	public void notifyMessage(int duration, String msg) {

				Image img = new Image("done.png");
				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
						.onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();

				notificationBuilder.show();
			}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }
}
