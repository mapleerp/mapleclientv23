package com.maple.mapleclient.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemImageMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ItemBatchImageCtl {

	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();

	FileChooser fileChooser = new FileChooser();

	@FXML
	private ScrollPane scrollpane;

	String categoryId;
	@FXML
	private FlowPane flowpane;

	@FXML
	private TextField txtItemName;
	@FXML
	private AnchorPane clFromItemName;

	  @FXML
	    private TextField txtCategory;

	@FXML
	private Button btnAdd;

	@FXML
	private Button reset;

	@FXML
	private Button viewitemimage;

	@FXML
	private TextField txtBatch;

	@FXML
	private TextField txtIngredients;
	


	
	@FXML
	private TextField txtSpecialOffer;

	@FXML
	void OnMouseClicked(MouseEvent event) {

		selectAndSaveImage();

	}

	  @FXML
	    void loadBatchPopup(MouseEvent event) {

		  itemBatchPopUp();
		  
	    }

	@FXML
	void addItems(ActionEvent event) {

	}

	@FXML
	void addOnEnter(KeyEvent event) {

	}

	@FXML
	void reset(ActionEvent event) {
		resetImage();

	}

	@FXML
	void loadPopUp(MouseEvent event) {
		showItemMst();
	}
	
	
private void loadCategoryPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
        @Subscribe
		public void popupOrglistner(CategoryEvent CategoryEvent) {

			Stage stage = (Stage) reset.getScene().getWindow();
			if (stage.isShowing()) {

				
				txtCategory.setText(CategoryEvent.getCategoryName());
		
				categoryId=CategoryEvent.getCategoryId();

			}
}


	private void showItemMst() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			btnAdd.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupItemlistner(KitPopupEvent kitPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			// kitDefenitionmst = new KitDefinitionMst();
			txtItemName.setText(kitPopupEvent.getItemName());
			txtBatch.clear();
			txtIngredients.clear();
			txtSpecialOffer.clear();
			
		}
	}

	@FXML
	void saveOnKey(KeyEvent event) {
		selectAndSaveImage();
	}

	@FXML
	void vewItemImage(ActionEvent event) {
		viewImage();
	}

	@FXML
	private void initialize() {
		eventBus.register(this);



	}

	private void viewImage() 
	{
		if (!txtItemName.getText().trim().isEmpty()) {
			ResponseEntity<ItemMst> item = RestCaller.getItemByNameRequestParam(txtItemName.getText());

			ResponseEntity<List<ItemImageMst>> itemImageList = RestCaller.getimagebyitem(item.getBody().getId());

			List<ItemImageMst> itemImageMstList = itemImageList.getBody();

			Iterator iter = itemImageMstList.iterator();

			while (iter.hasNext()) {
				ItemImageMst itemImageMst = (ItemImageMst) iter.next();

//
////				byte[] data = Base64.getUrlDecoder().decode( itemImageMst.getItemImage());
//			 
//				byte[] data =   itemImageMst.getItemImage();
// 
//				
//				
//				//byte[] data = itemImageMst.getItemImage();
//				
//				if(null==data) {
//					continue;
//				}
//
//				ByteArrayInputStream bis = new ByteArrayInputStream(data);
//				int i = 0;
//				i = i + 1;
//				BufferedImage bImage2 = null;
//
//				try {
//					bImage2 = ImageIO.read(bis);
//
//					Image image = SwingFXUtils.toFXImage(bImage2, null);
//
//					// imageView.setImage(image);
//
//					Node imagenode = new ImageView();
//
//					imagenode.autosize();
//
//					ImageView img = (ImageView) imagenode;
//
//					img.setFitHeight(300);
//					img.setFitWidth(200);
//					img.setImage(image);
//
//					flowpane.getChildren().add(imagenode);
//					// txtItemName.clear();
//
//				}
//
//				catch (IOException e) { // TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//
		} 
		}
			else {
//			notifyMessage(3, "Select Item", false);
//			txtItemName.requestFocus();
//			return;
		}
		// ItemImageMst itemImageMst = itemImageMstList.get(itemImageMstList.size() -
		// 1);

	}

	private void resetImage() {
		// imageView.setImage(null);

		flowpane.getChildren().clear();

	}

	private void selectAndSaveImage() {

		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Item Name", false);
			txtItemName.requestFocus();
			return;
		}
		if (txtBatch.getText().trim().isEmpty()) {
			notifyMessage(3, "Type Mrp", false);
			txtBatch.requestFocus();
			return;
		}
		
		if (txtCategory.getText().isEmpty()) {
			notifyMessage(3, "Select Category", false);
			txtCategory.requestFocus();
			return;
		}
		
		
		File selectedFile = fileChooser.showOpenDialog(btnAdd.getScene().getWindow());
		
		try {
		
			ResponseEntity<ItemMst> item = RestCaller.getItemByNameRequestParam(txtItemName.getText());

			System.out.print(selectedFile+"selected file isssssssssssssssssssssssssssssss"+item.getBody()+"item body isssssssssssssssssssssssssssssssssssss");
			
			ResponseEntity<String> responce = RestCaller.uploadImage(selectedFile, item.getBody());
			
			
			System.out.println(responce.getBody());
		
			
			ItemImageMst itemImageMst = new ItemImageMst();
			
		
			  itemImageMst.setIngredients(txtIngredients.getText());
			
			  itemImageMst.setSpecialOfferDescription(txtSpecialOffer.getText());
			  itemImageMst.setCategory(categoryId);
			  itemImageMst.setItemMst(item.getBody());
			  itemImageMst.setItemImage(responce.getBody());
			  itemImageMst.setBranchCode(SystemSetting.systemBranch);
			  
			  ResponseEntity<ItemImageMst> respentity = RestCaller.saveItemIamgeMst(itemImageMst);
			 
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 

		Image image1 = new Image(selectedFile.toURI().toString());

	// imageView.setImage(image);

		Node imagenode = new ImageView();

		imagenode.autosize();

		ImageView img = (ImageView) imagenode;

		img.setFitHeight(300);
		img.setFitWidth(200);
		img.setImage(image1);

		flowpane.getChildren().add(imagenode);
		
	
		//itemImageMst.setItemImage(img.getImage().toString().getBytes());

		// BufferedImage bufferedImage = null;
		
		try {

			BufferedImage bImage = ImageIO.read(selectedFile);
			
		 

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ImageIO.write(bImage, "png", bos);
			bos.flush();
			byte[] imageInByte = bos.toByteArray();
			bos.close();
			
			
			
			
			//byte[] encoded = Base64.getUrlEncoder().encode(imageInByte);
			
			/*
			 * byte[] encoded = Base64.getEncoder().encode("Hello".getBytes());
println(new String(encoded));   // Outputs "SGVsbG8="

byte[] decoded = Base64.getDecoder().decode(encoded);
println(new String(decoded))    // Outputs
			 */

			//itemImageMst.setItemImage(imageInByte);
			 

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		// txtItemName.clear();
		notifyMessage(3, "Image Uploaded", true);

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	/*
	 * //save image into database File file = new
	 * File("C:\\mavan-hibernate-image-mysql.gif"); byte[] bFile = new byte[(int)
	 * file.length()];
	 * 
	 * try { FileInputStream fileInputStream = new FileInputStream(file); //convert
	 * file into array of bytes fileInputStream.read(bFile);
	 * fileInputStream.close(); } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * Avatar avatar = new Avatar(); avatar.setImage(bFile);
	 * 
	 * session.save(avatar);
	 * 
	 * //Get image from database Avatar avatar2 = (Avatar)session.get(Avatar.class,
	 * avatar.getAvatarId()); byte[] bAvatar = avatar2.getImage();
	 * 
	 * try{ FileOutputStream fos = new FileOutputStream("C:\\test.gif");
	 * fos.write(bAvatar); fos.close(); }catch(Exception e){ e.printStackTrace(); }
	 */
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
	     private void itemBatchPopUp()
	 	{


	 		System.out.println("inside the popup");
	 		try {
	 			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemBatchExpiryDtlPopUp.fxml"));
	 			// fxmlLoader.setController(itemStockPopupCtl);
	 			Parent root1;
	 			root1 = (Parent) fxmlLoader.load();
	 			Stage stage = new Stage();
	 			stage.initModality(Modality.APPLICATION_MODAL);
	 			stage.initStyle(StageStyle.UNDECORATED);
	 			stage.setTitle("Stock Item");
	 			stage.initModality(Modality.APPLICATION_MODAL);
	 			stage.setScene(new Scene(root1));
	 			stage.show();
	 			txtCategory.requestFocus();

	 		} catch (Exception e) {
	 			e.printStackTrace();
	 		}
	 	
	 	}

	     @FXML
	     void loadCategoryPopup(MouseEvent event) {
	    	 loadCategoryPopup();
	     }
	
	     @Subscribe
	 	public void popupStockItemlistner(ItemBatchExpiryDtl itemBatchExpDtl) {
	 		Platform.runLater(new Runnable() {
	 			@Override
	 			public void run() {

	 				
	 				txtBatch.setText(itemBatchExpDtl.getBatch());
	 				txtCategory.requestFocus();
	 			}
	 		});
	 	}
}
