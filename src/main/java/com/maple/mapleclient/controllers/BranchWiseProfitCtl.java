package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import org.bouncycastle.asn1.cmp.ProtectedPart;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.deser.impl.SetterlessProperty;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.BranchWiseProfitReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;


public class BranchWiseProfitCtl {
	String taskid;
	String processInstanceId;
	
	
	
	private ObservableList<BranchWiseProfitReport> profitList = FXCollections.observableArrayList();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");

	String startDate = null;
	String endDate = null;
	String branch = null;
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerate;

    @FXML
    private TableView<BranchWiseProfitReport> tblBranchWiseProfit;

    @FXML
    private TableColumn<BranchWiseProfitReport, String> clBranchName;

    @FXML
    private TableColumn<BranchWiseProfitReport, Number> clProfit;
    
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblBranchWiseProfit.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getBranchName()) {
					
					if( null != startDate && null != endDate)
					{
						branch = newSelection.getBranchName();
						callBranchProfit(startDate,endDate,branch);
					}
				}
			}
		});
    	
    }

    private void callBranchProfit(String startDate, String endDate, String branch) {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ProfitByBranch.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			BranchProfitCtl branchProfitCtl =fxmlLoader.getController();
			branchProfitCtl.branchProfit(startDate, endDate, branch);
   			Stage stage = new Stage();
   			stage.setScene(new Scene(root1));
   			stage.initModality(Modality.APPLICATION_MODAL);
   			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	@FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, " Please Select FromDate...!!!", false);
    		return;
    	}
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, " Please Select ToDate...!!!", false);
    		return;
    	}
    	
    	Date sDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    	Date eDate = SystemSetting.localToUtilDate(dpToDate.getValue());
    	
    	 startDate = SystemSetting.UtilDateToString(sDate, "yyyy-MM-dd");
    	 endDate = SystemSetting.UtilDateToString(eDate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<BranchWiseProfitReport>> branchProfitResp = RestCaller.branchWiseProfit(startDate,endDate);
    	profitList = FXCollections.observableArrayList(branchProfitResp.getBody());
    	fillTable();
    	
    }
    
	private void fillTable() {
		
		tblBranchWiseProfit.setItems(profitList);
		clBranchName.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
		clProfit.setCellValueFactory(cellData -> cellData.getValue().getProfitProperty());
		clProfit.setCellFactory(tc-> new TableCell<BranchWiseProfitReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

	}
	
	 @FXML
	    void BackOnKeyPress(KeyEvent event) {

	    }

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


       private void PageReload() {
       	
 }

}
