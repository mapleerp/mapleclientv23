package com.maple.mapleclient.controllers;

import java.io.FileOutputStream;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.io.IOException;
import java.io.OutputStream;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.MenuMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.HostServices;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

@Component
public class ReportTree2 {

	String selectedText = null;
	String taskid;
	String processInstanceId;

	@FXML
	private TreeView<String> treeview;

	@FXML
	void initialize() {
		TreeItem rootItem = new TreeItem("REPORTS");

		TreeItem masterItem = new TreeItem("MASTERS");

		masterItem.getChildren().add(new TreeItem("CUSTOMER REPORT"));
		masterItem.getChildren().add(new TreeItem("ITEM MASTER"));
		masterItem.getChildren().add(new TreeItem("ACCOUNT HEADS"));
		masterItem.getChildren().add(new TreeItem("SUPPLIERS"));

		// version3.6sari
		masterItem.getChildren().add(new TreeItem("KIT REPORT"));
		// version3.6ends
		rootItem.getChildren().add(masterItem);

		TreeItem javaItem = new TreeItem("INVENTORY");
		javaItem.getChildren().add(new TreeItem("STOCK REPORT"));
		javaItem.getChildren().add(new TreeItem("MOBILE STOCK REPORT"));

		javaItem.getChildren().add(new TreeItem("ITEM STOCK REPORT"));
		javaItem.getChildren().add(new TreeItem("ITEMWISE STOCK MOVEMENT REPORT"));
		javaItem.getChildren().add(new TreeItem("CATEGORYWISE STOCK MOVEMENT REPORT"));
		javaItem.getChildren().add(new TreeItem("INVENTORY REORDER STATUS REPORT"));
		javaItem.getChildren().add(new TreeItem("STOCK VALUE REPORT"));
		javaItem.getChildren().add(new TreeItem("ITEM LOCATION REPORT"));
		javaItem.getChildren().add(new TreeItem("FAST MOVING ITEMS REPORT"));
		javaItem.getChildren().add(new TreeItem("FAST MOVING ITEMS REPORT BY SALES MODE"));

		javaItem.getChildren().add(new TreeItem("PHARMACY ITEM OR CATEGORY MOVEMENT"));

		javaItem.getChildren().add(new TreeItem("INHOUSE CONSUMPTION REPORT"));


		javaItem.getChildren().add(new TreeItem("PHARMACY ITEM OR CATEGORY MOVEMENT"));

		javaItem.getChildren().add(new TreeItem("INHOUSE CONSUMPTION REPORT"));

		rootItem.getChildren().add(javaItem);

		TreeItem accountsItem = new TreeItem("ACCOUNTING REPORTS");
		accountsItem.getChildren().add(new TreeItem("STATEMENTS OF ACCOUNT"));
		accountsItem.getChildren().add(new TreeItem("ALL CUSTOMER BILL WISE BALANCE REPORT"));
		accountsItem.getChildren().add(new TreeItem("CUSTOMER BILL WISE BALANCE REPORT"));
		accountsItem.getChildren().add(new TreeItem("SUPPLIER LEDGER REPORT"));
		accountsItem.getChildren().add(new TreeItem("RECEIPT INVOICE DETAIL REPORT"));
		accountsItem.getChildren().add(new TreeItem("PAYMENT INVOICE DETAIL REPORT"));
		accountsItem.getChildren().add(new TreeItem("TRIAL BALANCE"));
		accountsItem.getChildren().add(new TreeItem("BALANCE SHEET"));
		accountsItem.getChildren().add(new TreeItem("SUPPLIER DUE REPORT"));
		accountsItem.getChildren().add(new TreeItem("CARD RECONCILE REPORT"));
		accountsItem.getChildren().add(new TreeItem("SUNDRY DEBTOR BALANCE"));
		accountsItem.getChildren().add(new TreeItem("TAX REPORT"));
		accountsItem.getChildren().add(new TreeItem("ACCOUNT CLOSING BALANCE"));

		rootItem.getChildren().add(accountsItem);

		TreeItem dailySalesReport = new TreeItem("SALES REPORT");
		dailySalesReport.getChildren().add(new TreeItem("DAILY SALES REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("POS MONTHLY SUMMARY REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("DAY BOOK REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("HSN CODE SALE REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("RECEIPT MODE WISE REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("GROUP WISE SALES REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("GROUP WISE SALES DETAIL REPORT"));
//        dailySalesReport.getChildren().add(new TreeItem("DAILY SALES REPORT ITEM WISE"));
		dailySalesReport.getChildren().add(new TreeItem("ITEM OR CATEGORY SALE REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("SALE SUMMARY REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("SALES MODEWISE SUMMARY REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("B2C SALES REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("B2B SALES REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("DAMAGE ENTRY REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("BRANCH WISE PROFIT"));
		dailySalesReport.getChildren().add(new TreeItem("PENDING INTENT REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("OTHER BRANCH SALES REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("CUSTOMERWISE SALES REPORT"));

		TreeItem prodConversion = new TreeItem("PRODUCT CONVERSION REPORT");
		if (SystemSetting.UserHasRole("PRODUCT CONVERSION REPORT")) {
			dailySalesReport.getChildren().add(prodConversion);
		}

		dailySalesReport.getChildren().add(new TreeItem("CUSTOMER BILLWISE BY DUE DAYS"));
		dailySalesReport.getChildren().add(new TreeItem("CUSTOMER SALES REPORT"));
		dailySalesReport.getChildren().add(new TreeItem("SALES RETURN REPORT"));

		dailySalesReport.getChildren().add(new TreeItem("ITEM OR SERVICE WISE PROFIT"));

		dailySalesReport.getChildren().add(new TreeItem("SALES REPORT BY PRICE TYPE"));
		dailySalesReport.getChildren().add(new TreeItem("INSURANCE COMPANY SALES REPORT"));

		rootItem.getChildren().add(dailySalesReport);
		TreeItem pharmacySalesReturnReport = new TreeItem("PHARMACY SALES RETURN REPORT");
		pharmacySalesReturnReport.getChildren().add(new TreeItem("PHARMACY SALES RETURN REPORT"));

		rootItem.getChildren().add(pharmacySalesReturnReport);
		TreeItem saleOrderReport = new TreeItem("SALEORDER REPORT");
		saleOrderReport.getChildren().add(new TreeItem("SALEORDER REPORT"));
//    	saleOrderReport.getChildren().add(new TreeItem("DAILY ORDER DETAILS REPORT"));

		saleOrderReport.getChildren().add(new TreeItem("SALEORDER BALANCE ADVANCE REPORT"));
		saleOrderReport.getChildren().add(new TreeItem("DELIVERYBOY SALEORDER REPORT"));
		saleOrderReport.getChildren().add(new TreeItem("HOMEDELIVERY/TAKE AWAY REPORT"));
		saleOrderReport.getChildren().add(new TreeItem("SALEORDER PENDING REPORT"));
		saleOrderReport.getChildren().add(new TreeItem("DELIVERY BOY PENDING REPORT"));
		saleOrderReport.getChildren().add(new TreeItem("DAILY CAKE SETTING"));
		// ---------------version 4.9
		saleOrderReport.getChildren().add(new TreeItem("HOMEDELIVERY REPORT"));

		// ---------------version 4.9 end
		// ------------version 4.10
		saleOrderReport.getChildren().add(new TreeItem("DAILY ORDER REPORT"));

		// ---------------version 4.10 end
		rootItem.getChildren().add(saleOrderReport);

		TreeItem stcokTransferReport = new TreeItem("STOCK TRANSFER REPORT");

		stcokTransferReport.getChildren().add(new TreeItem("STOCK TRANSFER OUT REPORT"));
		stcokTransferReport.getChildren().add(new TreeItem("STOCK TRANSFER OUT DETAIL REPORT"));

		stcokTransferReport.getChildren().add(new TreeItem("STOCK TRANSFER IN REPORT"));
		stcokTransferReport.getChildren().add(new TreeItem("STOCK TRANSFER IN DETAIL REPORT"));

		// ---------------version 6.9 surya

		stcokTransferReport.getChildren().add(new TreeItem("STOCK TRANSFER DETAIL DELETED REPORT"));
		// ---------------version 6.9 surya end

		rootItem.getChildren().add(stcokTransferReport);

		TreeItem voucherReprint = new TreeItem("REPRINT");
		voucherReprint.getChildren().add(new TreeItem("VOUCHER REPRINTS"));
		rootItem.getChildren().add(voucherReprint);

		TreeItem receiptReports = new TreeItem("RECEIPT REPORT");
		receiptReports.getChildren().add(new TreeItem("RECEIPT REPORTS"));
		rootItem.getChildren().add(receiptReports);

		TreeItem paymentReports = new TreeItem("PAYMENT REPORT");
		paymentReports.getChildren().add(new TreeItem("PAYMENT REPORTS"));
		rootItem.getChildren().add(paymentReports);
		TreeItem journalReports = new TreeItem("JOURNAL REPORT");
		journalReports.getChildren().add(new TreeItem("JOURNAL REPORTS"));
		rootItem.getChildren().add(journalReports);
		TreeItem supplierItem = new TreeItem("PURCHASE REPORT");

		supplierItem.getChildren().add(new TreeItem("PURCHASE REPORT"));
		supplierItem.getChildren().add(new TreeItem("HSNCODE WISE PURCHASE REPORT"));
		supplierItem.getChildren().add(new TreeItem("SUPPLIER MONTHLY SUMMARY"));
		supplierItem.getChildren().add(new TreeItem("SUPPLIER BILLWISE BY DUE DAYS"));
		supplierItem.getChildren().add(new TreeItem("SUPPLIERWISE PURCHASE REPORT"));
		supplierItem.getChildren().add(new TreeItem("PHARMACY PURCHASE REPORT"));

		rootItem.getChildren().add(supplierItem);

		TreeItem ProductionReport = new TreeItem("PRODUCTION REPORT");

		ProductionReport.getChildren().add(new TreeItem("PRODUCTION PLANNING REPORT"));
		ProductionReport.getChildren().add(new TreeItem("RAW MATERIAL REQUIRED REPORT"));
		ProductionReport.getChildren().add(new TreeItem("RAW MATERIAL ISSUE REPORT"));
		ProductionReport.getChildren().add(new TreeItem("ACTUAL PRODUCTION REPORT"));
		ProductionReport.getChildren().add(new TreeItem("ACTUAL PRODUCTION AND PLANING SUMMARY"));
		ProductionReport.getChildren().add(new TreeItem("CONSUMPTION REPORT"));
		ProductionReport.getChildren().add(new TreeItem("REASON WISE CONSUMPTION REPORT"));

		// ProductionReport.getChildren().add(new TreeItem("RAW MATERIAL ITEMWISE"));
		rootItem.getChildren().add(ProductionReport);

		TreeItem batchWiseInventoryReport = new TreeItem("BATCHWISE INVENTORY REPORT");
		batchWiseInventoryReport.getChildren().add(new TreeItem("SHORT EXPIRY REPORT"));
		batchWiseInventoryReport.getChildren().add(new TreeItem("CATEGORYWISE SHORT EXPIRY REPORT"));
		batchWiseInventoryReport.getChildren().add(new TreeItem("ITEMWISE SHORT EXPIRY REPORT"));

		rootItem.getChildren().add(batchWiseInventoryReport);

		TreeItem consumptionReport = new TreeItem("CONSUMPTION REPORT");
		consumptionReport.getChildren().add(new TreeItem("CONSUMPTION REPORT"));

		TreeItem reasonWiseconsumptionReport = new TreeItem("REASON WISE CONSUMPTION REPORT");
		reasonWiseconsumptionReport.getChildren().add(new TreeItem("REASON WISE CONSUMPTION REPORT"));

		// --------------version 6.11 surya

		TreeItem ItemExceptionList = new TreeItem("ITEM EXCEPTION REPORT");

		ItemExceptionList.getChildren().add(new TreeItem("ITEM EXCEPTION REPORT"));
		rootItem.getChildren().add(ItemExceptionList);

		TreeItem weighingMachineReports = new TreeItem("WEIGHING MACHINE REPORT");
		weighingMachineReports.getChildren().add(new TreeItem("WEIGHING MACHINE REPORT"));
		rootItem.getChildren().add(weighingMachineReports);

		TreeItem itemStockEnquiry = new TreeItem("ITEM STOCK ENQUIRY");

		itemStockEnquiry.getChildren().add(new TreeItem("ITEM STOCK ENQUIRY"));
		rootItem.getChildren().add(itemStockEnquiry);

		TreeItem physicalStockReport = new TreeItem("PHYSICAL STOCK REPORT");
		physicalStockReport.getChildren().add(new TreeItem("PHYSICAL STOCK REPORT"));
		rootItem.getChildren().add(physicalStockReport);

		TreeItem intentReport = new TreeItem("INTENT REPORT");
		intentReport.getChildren().add(new TreeItem("ITEM WISE INTENT REPORT SUMMARY"));
		rootItem.getChildren().add(intentReport);
		
		TreeItem financialYearStatement = new TreeItem("INCOME AND EXPENCE REPORT");
		financialYearStatement.getChildren().add(new TreeItem("INCOME AND EXPENCE REPORT"));
		rootItem.getChildren().add(financialYearStatement);

		// --------------version 6.11 surya end

		/*
		 * TreeItem orgdata = new TreeItem("CHURCH"); orgdata.getChildren().add(new
		 * TreeItem("ORGANISATION CREATION")); orgdata.getChildren().add(new
		 * TreeItem("FAMILY CREATION")); rootItem.getChildren().add(orgdata);
		 */

//    	TreeItem voucherReprint = new TreeItem("VOUCHER REPRINT");
//    	if (SystemSetting.UserHasRole("VOUCHER REPRINT")) {
//
//    	rootItem2.getChildren().add(voucherReprint);
//    	}

//    	TreeItem voucherReprintItem = new TreeItem("VOUCHER REPRINT");
//    	if (SystemSetting.UserHasRole("VOUCHER REPRINT")) {
//
//    	rootItem2.getChildren().add(voucherReprintItem);
//    	}

		// javaItem.getChildren().add(new TreeItem("Stock Transfer Out Dtl"));
		// javaItem.getChildren().add(new TreeItem("Stock Transfer Out Hdr"));
		/*
		 * if (SystemSetting.UserHasRole("INTENT")) { javaItem.getChildren().add(new
		 * TreeItem("Intent Dtl")); javaItem.getChildren().add(new
		 * TreeItem("Intent Hdr")); } javaItem.getChildren().add(new
		 * TreeItem("Kit Definition Dtl")); javaItem.getChildren().add(new
		 * TreeItem("Kit Definition Mst")); if (SystemSetting.UserHasRole("PRODUCTION"))
		 * { javaItem.getChildren().add(new TreeItem("Production Dtl"));
		 * javaItem.getChildren().add(new TreeItem("production Mst")); }
		 * 
		 */

		//// 26/08/19 direct pdf generating for salesreports

		/*
		 * TreeItem webItem = new TreeItem("Sales Reports");
		 * webItem.getChildren().add(new TreeItem("Online Sales"));
		 * 
		 * if(SystemSetting.UserHasRole("DAY END")) { webItem.getChildren().add(new
		 * TreeItem("Daily Sales Summary")); }
		 * 
		 * 
		 * webItem.getChildren().add(new TreeItem("Sales Order by Date"));
		 * webItem.getChildren().add(new TreeItem("Sales Transhdr")); if
		 * (SystemSetting.UserHasRole("PURCHASE")) { webItem.getChildren().add(new
		 * TreeItem("Purchase Hdr")); } if (SystemSetting.UserHasRole("ReORDER")) {
		 * webItem.getChildren().add(new TreeItem("Reorder Mst")); } if
		 * (SystemSetting.UserHasRole("ADD SUPPLIER")) { webItem.getChildren().add(new
		 * TreeItem("Suppliers")); }
		 * 
		 * 
		 * rootItem.getChildren().add(webItem);
		 */

		treeview.setRoot(rootItem);

		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();
//                if(selectedItem.getValue().equalsIgnoreCase( "DAILY SALES REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.dailySalesreport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "ACTUAL PRODUCTION REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.actualProductionReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "SALEORDER REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.SaleOrderReports);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "SALEORDER BALANCE ADVANCE REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.SaleOrderBalAdvReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "DAILY ORDER DETAILS REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.DailyOrderDetails);
//                
//                	
//                }
//               
//                if(selectedItem.getValue().equalsIgnoreCase( "DELIVERYBOY SALEORDER REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.DeliveryboyReport);
//                
//                	
//                }
//       
//
//                if(selectedItem.getValue().equalsIgnoreCase( "HOMEDELIVERY REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.HomedeliveryReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "ITEMWISE STOCK MOVEMENT REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.StockReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "INVENTORY REORDER STATUS REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.InventoryReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "B2C SALES REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.SalesReport);
//                
//                	
//                }
//
//                if(selectedItem.getValue().equalsIgnoreCase( "DAY BOOK REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.dayBookReport);
//                
//                	
//                }
//
//                if(selectedItem.getValue().equalsIgnoreCase( "SALE SUMMARY REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.SaleSummaryReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "SUPPLIER MONTHLY SUMMARY")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.MonthlyReports);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "SUPPLIER LEDGER REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.SupplierLedger);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "VOUCHER REPRINTS")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.VoucherReprint);
//                
//                	
//                }
//                
//                if(selectedItem.getValue().equalsIgnoreCase( "SUPPLIER BILLWISE BY DUE DAYS")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.SupplierBillwise);
//                
//                	
//                }
//                
//                if(selectedItem.getValue().equalsIgnoreCase( "B2B SALES REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.b2bSaleReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "DAMAGE ENTRY REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.damageEntry);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "CUSTOMER LEDGER REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.customerLedger);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "CUSTOMER BILLWISE BY DUE DAYS")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.CustomerBillwise);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "STATEMENTS OF ACCOUNT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.AccountBalance);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "RAW MATERIAL ITEMWISE")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.RawMaterialReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "ITEM OR CATEGORY SALE REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ItemOrCategorySaleReport);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "SALES MODEWISE SUMMARY REPORT")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.salesModeWiseSummary);
//                
//                	
//                }
//
//                if(selectedItem.getValue().equalsIgnoreCase( "ORGANISATION CREATION")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.Organisation);
//                
//                	
//                }
//                if(selectedItem.getValue().equalsIgnoreCase( "FAMILY CREATION")) {
//                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.Familycreation);
//                
//                	
//                }
//
////                if(selectedItem.getValue().equalsIgnoreCase( "ORGANISATION CREATION")) {
////                	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
////                		MapleclientApplication.mainWorkArea.getChildren().clear();
////
////                	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.Organisation);
////                
////                	
////                }
////                
//
//                if(selectedItem.getValue().equalsIgnoreCase("ACCOUNT HEADS")) {
//                	try {
//						JasperPdfReportService.AccountHeadsReport();
//					} catch (JRException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("DAILY SALES REPORT ITEM WISE")) {
//                	
//						
//						try {
//							JasperPdfReportService.DailySalesReport();
//						} catch (JRException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Statement Of Accounts")) {
//                	try {
//						JasperPdfReportService.StatementOfAccountReport();
//					} catch (JRException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//                }
//                
//               
//                if(selectedItem.getValue().equalsIgnoreCase("CUSTOMER REPORT")) {
//                	try {
//						JasperPdfReportService.CustomerRegistrationReport();
//					} catch (JRException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//                }
//                
//                if(selectedItem.getValue().equalsIgnoreCase("Items")) {
//                	try {
//						JasperPdfReportService.ItemReport();
//					} catch (JRException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Daily Sales Summary")) {
//                	showDailySalesSummary();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Sales Order by Date")) {
//                	showSalesOrderbyDate();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Sales Transhdr")) {
//                	showSalesTranshdr();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Online Sales")) {
//                	showOnlineSales();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("ITEM MASTER")) {
//                	//showItemMst();
//                	try {
//						JasperPdfReportService.ItemReport();
//					} catch (JRException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Intent Dtl")) {
//                	showIntentDtl();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Intent Hdr")) {
//                	showIntentHdr();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Kit Definition Dtl")) {
//                	showKitDefinitionDtl();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Kit Definition Mst")) {
//                	showKitDefinitionMst();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Payment Dtl")) {
//                	showPaymentDtl();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Payment Hdr")) {
//                	showPaymentHdr();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Production Dtl")) {
//                	showProductionDtl();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Production Mst")) {
//                	showProductionMst();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Purchase Hdr")) {
//                	showPurchaseHdr();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Reorder Mst")) {
//                	showReorderMst();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("STOCK REPORT")) {
//                	//showStockReport();
//                	try {
//						JasperPdfReportService.DailyStockReport();
//					} catch (JRException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//                }
//                
//                if(selectedItem.getValue().equalsIgnoreCase("CustomerWise Report")) {
//                	//showStockReport();
//                	try {
//                		Date udate = SystemSetting.systemDate;
//                		String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
//						JasperPdfReportService.DailyCustomerWiseReport(date);
//					} catch (JRException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("SaleorderStatusReport")) {
//                	//showStockReport();
//                	try {
//                		Date udate = SystemSetting.systemDate;
//                		String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
//						JasperPdfReportService.SaleOrderStatusReport(date);
//					} catch (JRException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//                }
//                
//                
//                
//                if(selectedItem.getValue().equalsIgnoreCase("Stock Transfer Out Dtl")) {
//                	showStockTransferOutDtl();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Stock Transfer Out Hdr")) {
//                	showStockTransferOutHdr();
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("SUPPLIERS")) {
//                     	try {
//     						JasperPdfReportService.SupplierReport();
//     					} catch (JRException e) {
//     						// TODO Auto-generated catch block
//     						e.printStackTrace();
//     					}
//                }
//                if(selectedItem.getValue().equalsIgnoreCase("Unit Mst")) {
//                	showUnitMst();
//                }
//                
//            }
			}
		});

	}

	private void showStockTransferOutDtl() {

		String reportLocation = SystemSetting.reportPath + "/stocktransferdtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfStockTransferOutDtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showUnitMst() {

		String reportLocation = SystemSetting.reportPath + "/unitmst.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfUnitMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showSuppliers() {

		String reportLocation = SystemSetting.reportPath + "/suppliers.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfSuppliers());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showStockTransferOutHdr() {

		String reportLocation = SystemSetting.reportPath + "/stocktransferhdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfStockTransferOutHdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showReorderMst() {

		String reportLocation = SystemSetting.reportPath + "/reordermsts.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfReorderMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showStockReport() {

		String reportLocation = SystemSetting.reportPath + "/dailystockreport.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfStockReport());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showPaymentDtl() {

		String reportLocation = SystemSetting.reportPath + "/paymentdtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfPaymentDtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showPurchaseHdr() {

		String reportLocation = SystemSetting.reportPath + "/purchasehdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfPurchaseHdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showPaymentHdr() {

		String reportLocation = SystemSetting.reportPath + "/paymenthdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfPaymentHdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showProductionDtl() {

		String reportLocation = SystemSetting.reportPath + "/productiondtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfProductionDtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showProductionMst() {

		String reportLocation = SystemSetting.reportPath + "/productionmst.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfProductionMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showIntentDtl() {

		String reportLocation = SystemSetting.reportPath + "/intentdtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfIntentdtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showKitDefinitionDtl() {

		String reportLocation = SystemSetting.reportPath + "/kitdefinitiondtl.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfKitDefinitionDtl());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showKitDefinitionMst() {

		String reportLocation = SystemSetting.reportPath + "/kitdefinitionmst.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfKitDefinitionMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showIntentHdr() {

		String reportLocation = SystemSetting.reportPath + "/intenthdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfIntenthdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void reprtclick(MouseEvent event) {
		if (null != selectedText) {
			//1. Fetch command and fx from table based on the clicked item text.
			//2. load the fx similar way we did in the main frame
			
		 
			HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
			
			
			
			ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName(selectedText);
			
			List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
			
			MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
			
			
			ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(aMenuConfigMst.getId());
			List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();
	
			
			for(MenuConfigMst aMenuConfig : menuConfigMstList)
			{
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != aMenuConfig.getMenuFxml())
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(aMenuConfig.getMenuName(), dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
	 
				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						/*MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
						 
						*/
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
			}
			
		
			  

	 
 /*
	 			
			if (selectedText.equalsIgnoreCase("DAILY SALES REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.dailySalesreport);

			}
			if (null != selectedText) {
				if (selectedText.equalsIgnoreCase("PHARMACY SALES RETURN REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.pharmacySalesReturnReport);

				}
				if (null != selectedText) {
					if (selectedText.equalsIgnoreCase("POS MONTHLY SUMMARY REPORT")) {
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();

						MapleclientApplication.mainWorkArea.getChildren()
								.add(MapleclientApplication.mainFrameController.monthlySalesreport);

					}
					if (null != selectedText) {
						if (selectedText.equalsIgnoreCase("RECEIPT MODE WISE REPORT")) {
							if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
								MapleclientApplication.mainWorkArea.getChildren().clear();

							MapleclientApplication.mainWorkArea.getChildren()
									.add(MapleclientApplication.mainFrameController.receiptmodewisereport);

						}
					}
				}
				if (null != selectedText) {
					if (selectedText.equalsIgnoreCase("GROUP WISE SALES REPORT")) {
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();

						MapleclientApplication.mainWorkArea.getChildren()
								.add(MapleclientApplication.mainFrameController.groupwisesalesreport);

					}
				}

				if (null != selectedText) {
					if (selectedText.equalsIgnoreCase("GROUP WISE SALES DETAIL REPORT")) {
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();

						MapleclientApplication.mainWorkArea.getChildren()
								.add(MapleclientApplication.mainFrameController.groupwisesalesdtlreport);

					}
				}
				if (selectedText.equalsIgnoreCase("SALEORDER REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.SaleOrderReports);

				}

				if (selectedText.equalsIgnoreCase("STOCK TRANSFER OUT DETAIL REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.StockTransferOutDtlReport);

				}
				if (selectedText.equalsIgnoreCase("CATEGORYWISE STOCK MOVEMENT REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.categoryWiseStockMovmnt);

				}
				if (selectedText.equalsIgnoreCase("SHORT EXPIRY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.shortExpiryReport);

				}
				if (selectedText.equalsIgnoreCase("SALEORDER BALANCE ADVANCE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.SaleOrderBalAdvReport);

				}
				*/
			
			/*	if (selectedText.equalsIgnoreCase("MOBILE STOCK REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.mobileStockReport);

				}*/
			/*	
				if (selectedText.equalsIgnoreCase("FAST MOVING ITEMS REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.fastMovingItem);

				}

				if (selectedText.equalsIgnoreCase("FAST MOVING ITEMS REPORT BY SALES MODE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.fastMovingItemReportBySalesMode);

				}

				if (selectedText.equalsIgnoreCase("PHARMACY ITEM OR CATEGORY MOVEMENT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.pharmacyitemorcategorywisestockmovement);

				}

				// ----------------version 4.8

				if (selectedText.equalsIgnoreCase("DAILY CAKE SETTING")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.dailyCakeSetting);

				}
				// ----------------version 4.8 end

				if (selectedText.equalsIgnoreCase("SALEORDER PENDING REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.saleOrderPendingReport);

				}
				if (selectedText.equalsIgnoreCase("SUNDRY DEBTOR BALANCE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.sundryDebitReport);

				}
				// version3.6sari
				if (selectedText.equalsIgnoreCase("KIT REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.kitReport);

				}
				// version3.6end

				if (selectedText.equalsIgnoreCase("CARD RECONCILE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.CardReconcileReport);

				}

				if (selectedText.equalsIgnoreCase("SUPPLIERWISE PURCHASE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.supplierwisePurchaseReport);

				}
				if (selectedText.equalsIgnoreCase("TAX REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.taxReport);

				}

				if (selectedText.equalsIgnoreCase("INHOUSE CONSUMPTION REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.inHouseConsumptionReport);

				}
				if (selectedText.equalsIgnoreCase("ACCOUNT CLOSING BALANCE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.closingBalanceReport);

				}
				if (selectedText.equalsIgnoreCase("DAILY ORDER DETAILS REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.DailyOrderDetails);

				}
				if (selectedText.equalsIgnoreCase("RAW MATERIAL REQUIRED REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.RawMaterialRequiredReport);

				}

				if (selectedText.equalsIgnoreCase("RAW MATERIAL ITEMWISE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.RawMaterialReport);

				}
				if (selectedText.equalsIgnoreCase("SALES RETURN REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesReturnReport);

				}

				if (selectedText.equalsIgnoreCase("ITEM OR SERVICE WISE PROFIT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.serviceOrItemProfit);

				}

				if (selectedText.equalsIgnoreCase("SALES REPORT BY PRICE TYPE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesReportByPriceType);

				}

				if (selectedText.equalsIgnoreCase("DELIVERYBOY SALEORDER REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.DeliveryboyReport);

				}

				if (selectedText.equalsIgnoreCase("PRODUCT CONVERSION REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.productConversionreport);

				}

				if (selectedText.equalsIgnoreCase("PAYMENT INVOICE DETAIL REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.PaymentInvReport);

				}

				if (selectedText.equalsIgnoreCase("TRIAL BALANCE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.trialBalance);

				}

				if (selectedText.equalsIgnoreCase("HOMEDELIVERY/TAKE AWAY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.HomedeliveryReport);

				}

				if (selectedText.equalsIgnoreCase("RECEIPT INVOICE DETAIL REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.receiptInvReport);

				}
				if (selectedText.equalsIgnoreCase("ITEMWISE STOCK MOVEMENT REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.StockReport);

				}

				if (selectedText.equalsIgnoreCase("PRODUCTION PLANNING REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.productionPlanningReport);

				}
				if (selectedText.equalsIgnoreCase("STOCK REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.CategoryWiseStockReport);

				}
				if (selectedText.equalsIgnoreCase("ITEM STOCK REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.ItemStockReport);

				}
				if (selectedText.equalsIgnoreCase("STOCK VALUE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.StockValueReport);

				}
				if (selectedText.equalsIgnoreCase("INVENTORY REORDER STATUS REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.InventoryReport);

				}
				if (selectedText.equalsIgnoreCase("B2C SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.SalesReport);

				}

				if (selectedText.equalsIgnoreCase("HSN CODE SALE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.HsnCodeSaleReport);

				}
				if (selectedText.equalsIgnoreCase("CATEGORYWISE SHORT EXPIRY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.categoryWiseShortExpiry);

				}
				if (selectedText.equalsIgnoreCase("DAY BOOK REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.dayBookReport);

				}

				if (selectedText.equalsIgnoreCase("SALE SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.SaleSummaryReport);

				}
				// ---------------version 4.10

				if (selectedText.equalsIgnoreCase("DAILY ORDER REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.dailyOrderReport);

				}
				// ---------------version 4.10 end

				if (selectedText.equalsIgnoreCase("STOCK TRANSFER IN REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.StockTransferInReport);

				}

				if (selectedText.equalsIgnoreCase("STOCK TRANSFER IN DETAIL REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.stockTransferInDetailReport);

				}

				// ---------------version 6.9 surya

				if (selectedText.equalsIgnoreCase("STOCK TRANSFER DETAIL DELETED REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.stockTransferDeletedReport);

				}
				// --------------version 6.9 surya end
				if (selectedText.equalsIgnoreCase("PENDING INTENT REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.intentReport);

				}

				if (selectedText.equalsIgnoreCase("OTHER BRANCH SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.otherBranchSaleReport);

				}

				if (selectedText.equalsIgnoreCase("STOCK TRANSFER OUT REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.StockTransferOutReport);

				}

				if (selectedText.equalsIgnoreCase("SUPPLIER MONTHLY SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.MonthlyReports);

				}
//----------------version 4.9

				if (selectedText.equalsIgnoreCase("HOMEDELIVERY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.saleOrderHomeDeliveryReport);

				}
				// ----------------version 4.9 end

				if (selectedText.equalsIgnoreCase("HSNCODE WISE PURCHASE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.hsnCodePurchaseReport);

				}

				if (selectedText.equalsIgnoreCase("DELIVERY BOY PENDING REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.deliveryBoyPendingReport);

				}

				if (selectedText.equalsIgnoreCase("PURCHASE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.PurchaseReport);

				}
				if (selectedText.equalsIgnoreCase("PHARMACY PURCHASE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.pharmacyPurchaseReport);

				}
				if (selectedText.equalsIgnoreCase("SUPPLIER LEDGER REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.SupplierLedger);

				}
				if (selectedText.equalsIgnoreCase("SUPPLIER BILLWISE BY DUE DAYS")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.SupplierBillwise);

				}
				if (selectedText.equalsIgnoreCase("ACTUAL PRODUCTION REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.actualProductionReport);

				}

				if (selectedText.equalsIgnoreCase("INSURANCE COMPANY SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.insuranceCompanySales);

				}
				if (selectedText.equalsIgnoreCase("ACTUAL PRODUCTION AND PLANING SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.productionAndPlaningReport);

				}
				if (selectedText.equalsIgnoreCase("CONSUMPTION REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.consumptionReport);

				}
				if (selectedText.equalsIgnoreCase("REASON WISE CONSUMPTION REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.reasonWiseconsumptionReport);

				}
				if (selectedText.equalsIgnoreCase("ITEM EXCEPTION REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.exceptionList);

				}

				if (selectedText.equalsIgnoreCase("B2B SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.b2bSaleReport);

				}
				if (selectedText.equalsIgnoreCase("DAMAGE ENTRY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.damageEntry);

				}
				if (selectedText.equalsIgnoreCase("BRANCH WISE PROFIT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.branchProfit);

				}
				if (selectedText.equalsIgnoreCase("CUSTOMER BILL WISE BALANCE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.customerBillBalance);

				}
				if (selectedText.equalsIgnoreCase("ALL CUSTOMER BILL WISE BALANCE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.AllCustomerBalance);

				}

				if (selectedText.equalsIgnoreCase("RAW MATERIAL ISSUE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.RawMaterialIssueReport);

				}

				if (selectedText.equalsIgnoreCase("RECEIPT REPORTS")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.receiptReports);

				}
				if (selectedText.equalsIgnoreCase("WEIGHING MACHINE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.weighingMachineReport);

				}

				if (selectedText.equalsIgnoreCase("ITEM STOCK ENQUIRY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.itemStockEnquiry);

				}

				if (selectedText.equalsIgnoreCase("PHYSICAL STOCK REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.physicalStockReport);

				}

				if (selectedText.equalsIgnoreCase("ITEM WISE INTENT REPORT SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.itemWiseIntentSummaryReport);

				}
				
				
				if (selectedText.equalsIgnoreCase("INCOME AND EXPENCE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.incomeAndExpenceReport);

				}

				if (selectedText.equalsIgnoreCase("PAYMENT REPORTS")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.paymentReports);

				}
				if (selectedText.equalsIgnoreCase("JOURNAL REPORTS")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.journalReports);

				}
				if (selectedText.equalsIgnoreCase("CUSTOMER BILLWISE BY DUE DAYS")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.CustomerBillwise);

				}

				if (selectedText.equalsIgnoreCase("ITEMWISE SHORT EXPIRY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.itemWiseShortExpiry);

				}
				if (selectedText.equalsIgnoreCase("ITEM LOCATION REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.itemLocationReport);

				}
				if (selectedText.equalsIgnoreCase("CUSTOMER SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.CustomerSalesReport);

				}
				if (selectedText.equalsIgnoreCase("STATEMENTS OF ACCOUNT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.AccountBalance);

				}
				if (selectedText.equalsIgnoreCase("BALANCE SHEET")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.balanceSheet);

				}
				if (selectedText.equalsIgnoreCase("SUPPLIER DUE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.SupplierDueReport);

				}
				if (selectedText.equalsIgnoreCase("ITEM OR CATEGORY SALE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.ItemOrCategorySaleReport);

				}

				if (selectedText.equalsIgnoreCase("CUSTOMERWISE SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.customerwiseSalesReport);

				}
				if (selectedText.equalsIgnoreCase("SALES MODEWISE SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesModeWiseSummary);

				}

				if (selectedText.equalsIgnoreCase("ACCOUNT HEADS")) {
					try {
						JasperPdfReportService.AccountHeadsReport();
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (selectedText.equalsIgnoreCase("DAILY SALES REPORT ITEM WISE")) {

					try {
						JasperPdfReportService.DailySalesReport();
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				if (selectedText.equalsIgnoreCase("Statement Of Accounts")) {
					try {
						JasperPdfReportService.StatementOfAccountReport();
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (selectedText.equalsIgnoreCase("CUSTOMER REPORT")) {
					try {
						JasperPdfReportService.CustomerRegistrationReport();
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (selectedText.equalsIgnoreCase("Items")) {
					try {
						JasperPdfReportService.ItemReport();
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (selectedText.equalsIgnoreCase("Daily Sales Summary")) {
					showDailySalesSummary();
				}
				if (selectedText.equalsIgnoreCase("Sales Order by Date")) {
					showSalesOrderbyDate();
				}
				if (selectedText.equalsIgnoreCase("Sales Transhdr")) {
					showSalesTranshdr();
				}
				if (selectedText.equalsIgnoreCase("Online Sales")) {
					showOnlineSales();
				}
				if (selectedText.equalsIgnoreCase("ITEM MASTER")) {
					// showItemMst();
					try {
						JasperPdfReportService.ItemReport();
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (selectedText.equalsIgnoreCase("Intent Dtl")) {
					showIntentDtl();
				}
				if (selectedText.equalsIgnoreCase("Intent Hdr")) {
					showIntentHdr();
				}
				if (selectedText.equalsIgnoreCase("Kit Definition Dtl")) {
					showKitDefinitionDtl();
				}
				if (selectedText.equalsIgnoreCase("Kit Definition Mst")) {
					showKitDefinitionMst();
				}
				if (selectedText.equalsIgnoreCase("Payment Dtl")) {
					showPaymentDtl();
				}
				if (selectedText.equalsIgnoreCase("Payment Hdr")) {
					showPaymentHdr();
				}
				if (selectedText.equalsIgnoreCase("Production Dtl")) {
					showProductionDtl();
				}
				if (selectedText.equalsIgnoreCase("Production Mst")) {
					showProductionMst();
				}
				if (selectedText.equalsIgnoreCase("Purchase Hdr")) {
					showPurchaseHdr();
				}
				if (selectedText.equalsIgnoreCase("Reorder Mst")) {
					showReorderMst();
				}
//      if(selectedText.equalsIgnoreCase("STOCK REPORT")) {
//      	//showStockReport();
//      	try {
//				JasperPdfReportService.DailyStockReport();
//			} catch (JRException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//      }

				if (selectedText.equalsIgnoreCase("CustomerWise Report")) {
					// showStockReport();
					try {
						Date udate = SystemSetting.systemDate;
						String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
						JasperPdfReportService.DailyCustomerWiseReport(date);
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (selectedText.equalsIgnoreCase("SaleorderStatusReport")) {
					// showStockReport();
					try {
						Date udate = SystemSetting.systemDate;
						String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
						JasperPdfReportService.SaleOrderStatusReport(date);
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (selectedText.equalsIgnoreCase("Stock Transfer Out Dtl")) {
					showStockTransferOutDtl();
				}
				if (selectedText.equalsIgnoreCase("Stock Transfer Out Hdr")) {
					showStockTransferOutHdr();
				}
				if (selectedText.equalsIgnoreCase("SUPPLIERS")) {
					try {
						JasperPdfReportService.SupplierReport();
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (selectedText.equalsIgnoreCase("Unit Mst")) {
					showUnitMst();
				}

			
			}
			*/
		}
}

	private void showItemMst() {

		String reportLocation = SystemSetting.reportPath + "/itemmst.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfItemMst());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showOnlineSales() {

		String reportLocation = SystemSetting.reportPath + "/onlinesales.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfOnlineSales());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showCustomerRegistration() {

		String reportLocation = SystemSetting.reportPath + "/customerreg.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			// notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfCustomerRegistration());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showDailySalesSummary() {

		String reportLocation = SystemSetting.reportPath + "/dailysales.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			// notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfDailySalesSummary());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showSalesOrderbyDate() {

		String reportLocation = SystemSetting.reportPath + "/salesorderbydate.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			// notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfSalesOrderbyDate());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showSalesTranshdr() {

		String reportLocation = SystemSetting.reportPath + "/salestranshdr.pdf";
		if (!SystemSetting.deleteFile(reportLocation)) {
			// Notify to close the report ands try again
			// notifyMessageFailure(5, "Please close the report and try again");
			// return;
		}

		try {
			OutputStream out = new FileOutputStream(reportLocation);
			out.write(RestCaller.getpdfReportOfSalesTranshdr());

			out.close();

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(reportLocation);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void notifyMessageFailure(int duration, String msg) {

		Image img = new Image("failed.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }

}