package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.digester.annotations.handlers.SetPropertiesLoaderHandler;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ItemNotInBatchPriceReport;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class BatchPriceDefinitionCtl {
	String taskid;
	String processInstanceId;
	/*
	 * This window is used at the head office machine. All the stocks will be
	 * available at the cloud
	 */
	private ObservableList<BatchPriceDefinition> priceDefenitionListTable = FXCollections.observableArrayList();

	private ObservableList<ItemNotInBatchPriceReport> itemNotInBatchPriceReportList = FXCollections
			.observableArrayList();

	BatchPriceDefinition batchPriceDefinition = null;
	ItemMst itemMst = null;
	EventBus eventBus = EventBusFactory.getEventBus();

	@FXML
	private Button btnShowItemsNotInBatchPrice;
	@FXML
	private TextField txtItemName;

	@FXML
	private ComboBox<String> cmbPriceType;

	@FXML
	private TextField txtAmount;
	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnMultiLevelShowAll;

	@FXML
	private DatePicker dpMultilevelStartDate;

	@FXML
	private Button btnDelete;

	@FXML
	private ComboBox<String> cmbUnit;
	@FXML
	private TableView<BatchPriceDefinition> tblMultiLevelPriceEdit;

	@FXML
	private TableColumn<BatchPriceDefinition, String> clMultiLevelItemName;

	@FXML
	private TableColumn<BatchPriceDefinition, String> clMultiLevelPriceType;

	@FXML
	private TableColumn<BatchPriceDefinition, Number> clMultiLevelAmount;

	@FXML
	private TableColumn<BatchPriceDefinition, LocalDate> clMultiLevelStartDate;

	@FXML
	private TableColumn<BatchPriceDefinition, LocalDate> clMultiLevelEndDate;

	@FXML
	private TableColumn<BatchPriceDefinition, String> clUnit;

	@FXML
	private TableColumn<BatchPriceDefinition, String> clBatch;

	@FXML
	private void initialize() {
		dpMultilevelStartDate = SystemSetting.datePickerFormat(dpMultilevelStartDate, "dd/MMM/yyyy");
		eventBus.register(this);
		ArrayList unit = new ArrayList();

		unit = RestCaller.SearchUnit();
		Iterator itr = unit.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object unitName = lm.get("unitName");
			Object unitId = lm.get("id");
			if (unitId != null) {
				cmbUnit.getItems().add((String) unitName);

			}

		}
		cmbPriceType.getItems().clear();

		System.out.println("=====PriceTypeOnKEyPress===");
		ArrayList priceType = new ArrayList();
		RestTemplate restTemplate = new RestTemplate();
		priceType = RestCaller.getPriceDefinition();
		Iterator itr1 = priceType.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object priceLevelName = lm.get("priceLevelName");
			Object id = lm.get("id");
			if (id != null) {
				cmbPriceType.getItems().add((String) priceLevelName);

			}

		}
		tblMultiLevelPriceEdit.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						if (null != newSelection.getId()) {

							batchPriceDefinition = new BatchPriceDefinition();
							batchPriceDefinition.setId(newSelection.getId());
							batchPriceDefinition.setAmount(newSelection.getAmount());
							txtAmount.setText(newSelection.getAmount() + "");
							batchPriceDefinition.setBatch(newSelection.getBatch());
							txtBatch.setText(newSelection.getBatch());
							batchPriceDefinition.setItemId(newSelection.getItemId());
							ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(newSelection.getItemId());
							ItemMst itemMst = itemMstResp.getBody();
							if (null != itemMst) {
								batchPriceDefinition.setItemName(itemMst.getItemName());
								txtItemName.setText(itemMst.getItemName());

							}
							batchPriceDefinition.setPriceId(newSelection.getPriceId());
							ResponseEntity<PriceDefenitionMst> priceDefMst = RestCaller
									.getPriceNameById(newSelection.getPriceId());
							PriceDefenitionMst priceDefenitionMst = priceDefMst.getBody();
							if (null != priceDefenitionMst) {
								batchPriceDefinition.setPriceType(priceDefenitionMst.getPriceLevelName());
								cmbPriceType.getSelectionModel().select(priceDefenitionMst.getPriceLevelName());

							}
							batchPriceDefinition.setStartDate(newSelection.getStartDate());
							dpMultilevelStartDate.setValue(newSelection.getStartDate().toLocalDate());
							batchPriceDefinition.setUnitId(newSelection.getUnitId());

							ResponseEntity<UnitMst> unitResp = RestCaller.getunitMst(newSelection.getUnitId());
							UnitMst unitMst = unitResp.getBody();

							if (null != unitMst) {
								batchPriceDefinition.setUnitName(unitMst.getUnitName());
								cmbUnit.getSelectionModel().select(unitMst.getUnitName());
								;
							}
						}
					}

				});

	}

	@FXML
	void AmountOnKeyPressed(KeyEvent event) {

	}

	@FXML
	void DeleteMultilevelPrice(ActionEvent event) {

		if (null != batchPriceDefinition) {
			if (null != batchPriceDefinition.getId()) {
				RestCaller.deleteBatchPriceDefinition(batchPriceDefinition.getId());
				notifyMessage(5, "Deleted!!!");
				LoadPriceDefenition();
			}
		}
	}

	@FXML
	void ItemPopUpView(KeyEvent event) {

	}

	@FXML
	void MultiLevelShowAll(ActionEvent event) {
		LoadPriceDefenition();
	}

	@FXML
	void PriceTypeOnKEyPress(MouseEvent event) {

	}

	@FXML
	void Save(ActionEvent event) {
		if (null == cmbPriceType.getValue()) {

			notifyMessage(5, " Please select Price Type...!!!");
			return;

		} else if (txtAmount.getText().isEmpty()) {
			notifyMessage(5, " Please Enter Amount...!!!");
			return;
		} else if (null == dpMultilevelStartDate.getValue()) {
			notifyMessage(5, "Please Select Start Date...!!!");
			return;

		}

		if (null != batchPriceDefinition) {
			if (null == batchPriceDefinition.getId()) {
				batchPriceDefinition = new BatchPriceDefinition();

			}
		} else {
			batchPriceDefinition = new BatchPriceDefinition();

		}
		batchPriceDefinition.setBatch(txtBatch.getText());
		batchPriceDefinition.setBranchCode(SystemSetting.systemBranch);
		batchPriceDefinition.setStartDate(Date.valueOf(dpMultilevelStartDate.getValue()));
		ResponseEntity<ItemMst> restItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		itemMst = restItem.getBody();
		batchPriceDefinition.setItemId(itemMst.getId());
		batchPriceDefinition.setItemName(itemMst.getItemName());
		String pricetype = cmbPriceType.getValue();
		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller.getPriceDefenitionByName(pricetype);
		PriceDefenitionMst price = new PriceDefenitionMst();
		price = priceDefenitionSaved.getBody().get(0);
		if (null != price) {
			batchPriceDefinition.setPriceId(price.getId());
		}
		if (null != cmbUnit.getValue()) {
			ResponseEntity<UnitMst> unit = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
			batchPriceDefinition.setUnitId(unit.getBody().getId());
		}

		Double amount = 0.0;
		Double percentage = 0.0;
		percentage = Double.parseDouble(txtAmount.getText());

		if (price.getPriceCalculator().equalsIgnoreCase("MRP_MINUS_FIXEDPERCENTAGE")) {
			amount = itemMst.getStandardPrice() - ((percentage * itemMst.getStandardPrice()) / 100);
		} else if (price.getPriceCalculator().equalsIgnoreCase("COST_PRICE_PLUS_FIXEDPERCENTAGE")) {

			ResponseEntity<List<PriceDefenitionMst>> priceDefenitionMstResp = RestCaller
					.getPriceDefenitionByName("COST PRICE");
			List<PriceDefenitionMst> priceDefenitionMstList = priceDefenitionMstResp.getBody();

			if (priceDefenitionMstList.size() > 0) {
				String date = SystemSetting.UtilDateToString(SystemSetting.getSystemDate(), "yyyy-MM-dd");
				ResponseEntity<List<BatchPriceDefinition>> pricedefenitionResp = RestCaller
						.getBatchPriceDefenitionByCostPrice(itemMst.getId(), priceDefenitionMstList.get(0).getId(),
								itemMst.getUnitId(), date, txtBatch.getText());

				if (pricedefenitionResp.getBody().size() == 0) {
					notifyMessage(5, "Please add cost price to the item " + itemMst.getItemName());
					return;
				}
				BatchPriceDefinition priceDefinition = pricedefenitionResp.getBody().get(0);

				if (null != priceDefinition) {
					Double costPrice = priceDefinition.getAmount();

					amount = costPrice + ((percentage * costPrice) / 100);

				} else {
					notifyMessage(5, "Please add cost price to the item " + itemMst.getItemName());
					return;
				}

			} else {
				notifyMessage(5, "Type Cost Price is not present...!!!");
				return;
			}
		} else {

			amount = Double.parseDouble(txtAmount.getText());
		}

		batchPriceDefinition.setAmount(amount);

		if (null != batchPriceDefinition.getId()) {

		}
		ResponseEntity<BatchPriceDefinition> respentity = RestCaller.saveBatchPriceDefinition(batchPriceDefinition);
		batchPriceDefinition = respentity.getBody();
		priceDefenitionListTable.add(batchPriceDefinition);
		LoadPriceDefenition();
		notifyMessage(5, "Saved Successfully");

		txtAmount.clear();
		txtBatch.clear();
		txtItemName.clear();
		dpMultilevelStartDate.setValue(null);
		cmbPriceType.getSelectionModel().clearSelection();
		cmbUnit.getSelectionModel().clearSelection();

		if (null != taskid) {
			TaskWindow();

		}

	}

	private void fillTable() {
		tblMultiLevelPriceEdit.setItems(priceDefenitionListTable);
		for (BatchPriceDefinition p : priceDefenitionListTable) {
			if (null != p.getItemId()) {
				ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(p.getItemId());
				p.setItemName(respentity.getBody().getItemName());
				clMultiLevelItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			}
			if (null != p.getPriceId()) {
				ResponseEntity<PriceDefenitionMst> respentity = RestCaller.getPriceNameById(p.getPriceId());
				p.setPriceType(respentity.getBody().getPriceLevelName());
				clMultiLevelPriceType.setCellValueFactory(cellData -> cellData.getValue().getPriceTypeProperty());
			}
			if (p.getUnitId() != null) {
				ResponseEntity<UnitMst> unit = RestCaller.getunitMst(p.getUnitId());
				p.setUnitName(unit.getBody().getUnitName());
			}
		}
		clMultiLevelAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		clMultiLevelStartDate.setCellValueFactory(cellData -> cellData.getValue().getStartDateProperty());
		clMultiLevelEndDate.setCellValueFactory(cellData -> cellData.getValue().getEndDateProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
	}

	private void LoadPriceDefenition() {

		if (!txtItemName.getText().trim().isEmpty() && null != txtItemName.getText()) {

			ResponseEntity<ItemMst> itemMstResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
			ItemMst itemMst = itemMstResp.getBody();

			ResponseEntity<List<BatchPriceDefinition>> priceDefinitionSaved = RestCaller
					.SearchBatchPriceDefinitionByItemId(itemMst.getId());

			if (null != priceDefinitionSaved.getBody()) {
				priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
				fillTable();
			}
		} else if (null != cmbPriceType.getSelectionModel().getSelectedItem()) {

			ResponseEntity<PriceDefenitionMst> priceDefResp = RestCaller
					.getPriceDefenitionMstByName(cmbPriceType.getSelectionModel().getSelectedItem().toString());

			PriceDefenitionMst priceDefenitionMst = priceDefResp.getBody();

			ResponseEntity<List<BatchPriceDefinition>> priceDefinitionSaved = RestCaller
					.SearchBatchPriceDefinitionByPriceTypeId(priceDefenitionMst.getId());

			if (null != priceDefinitionSaved.getBody()) {
				priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
				fillTable();
			}

		} else {
			ResponseEntity<List<BatchPriceDefinition>> priceDefinitionSaved = RestCaller.SearchBatchPriceDefinition();
			if (null != priceDefinitionSaved.getBody()) {
				priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
				fillTable();
			}
		}

		cmbPriceType.getSelectionModel().clearSelection();
		txtBatch.clear();
		txtItemName.clear();
		cmbUnit.getSelectionModel().clearSelection();

	}

	@FXML
	void ShowItemPopup(MouseEvent event) {
		itemBatchPopUp();
	}

	@FXML
	void StartDateOnKEyPressed(KeyEvent event) {

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {

				txtItemName.setText(itemPopupEvent.getItemName());
				cmbUnit.setValue(itemPopupEvent.getUnitName());
				txtBatch.setText(itemPopupEvent.getBatch());

			}
		});
	}

	@Subscribe
	public void popupStockItemlistner(ItemBatchExpiryDtl itemBatchExpDtl) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {

				txtItemName.setText(itemBatchExpDtl.getItemName());
				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemBatchExpDtl.getItemName());
				ResponseEntity<UnitMst> getunit = RestCaller.getunitMst(getItem.getBody().getUnitId());
				txtBatch.setText(itemBatchExpDtl.getBatch());
				cmbUnit.setValue(getunit.getBody().getUnitName());
			}
		});
	}

	private void itemBatchPopUp() {

		System.out.println("inside the popup");
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemBatchExpiryDtlPopUp.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbPriceType.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void ItemPopup() {

		System.out.println("inside the popup");
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbPriceType.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	@FXML
	void actionShowItemsNotInBp(ActionEvent event) {

		ResponseEntity<List<ItemNotInBatchPriceReport>> itemList = RestCaller.getItemsNotInBatchPrice();
		itemNotInBatchPriceReportList = FXCollections.observableArrayList(itemList.getBody());
		try {
			JasperPdfReportService.itemNotInBatchPriceDefinition(itemNotInBatchPriceReportList);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		// PageReload(itemId,batch);
	}

	// ..................Sibi.....05/05/2021................//

	private void PageReload(String itemId, String batch) {

		txtItemName.setText(itemId);
		txtBatch.setText(batch);

	}

	private void TaskWindow() {
		ResponseEntity<List<MenuConfigMst>> menuTaskListResp = RestCaller.MenuConfigMstByMenuName("TASK LIST");
		List<MenuConfigMst> menuTaskList = menuTaskListResp.getBody();

		Node dynamicWindow;
		try {

			dynamicWindow = FXMLLoader.load(getClass().getResource("/fxml/" + menuTaskList.get(0).getMenuFxml()));

			// task newSelection.getId()

			// Clear screen before loading the Page.
			if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
				MapleclientApplication.mainWorkArea.getChildren().clear();

			MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

			MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
			MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
			MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
			MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
