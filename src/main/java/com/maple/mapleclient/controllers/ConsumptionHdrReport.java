package com.maple.mapleclient.controllers;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ConsumptionHdrReport {


	String voucherNumber;
	String reason;
	String voucherDate;
	@JsonIgnore
	StringProperty voucherNumberProperty;
	@JsonIgnore
	StringProperty reasonProperty;
	@JsonIgnore
	StringProperty voucherDatePropery;
	
	
	

	public ConsumptionHdrReport() {
		this.voucherNumberProperty = new SimpleStringProperty();
		this.reasonProperty = new SimpleStringProperty();
		this.voucherDatePropery =new SimpleStringProperty();
		
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
    @JsonIgnore
	public StringProperty getVoucherNumberProperty() {
	voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}

	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	@JsonIgnore
	public StringProperty getReasonProperty() {
		reasonProperty.set(reason);
		return reasonProperty;
	}

	public void setReasonProperty(StringProperty reasonProperty) {
		this.reasonProperty = reasonProperty;
	}
@JsonIgnore
	public StringProperty getVoucherDatePropery() {
	voucherDatePropery.set(voucherDate);
		return voucherDatePropery;
	}

	public void setVoucherDatePropery(StringProperty voucherDatePropery) {
		this.voucherDatePropery = voucherDatePropery;
	}

	@Override
	public String toString() {
		return "ConsumptionHdrReport [voucherNumber=" + voucherNumber + ", reason=" + reason + ", voucherDate="
				+ voucherDate + ", voucherNumberProperty=" + voucherNumberProperty + ", reasonProperty="
				+ reasonProperty + ", voucherDatePropery=" + voucherDatePropery + "]";
	}
	
	
	
}
