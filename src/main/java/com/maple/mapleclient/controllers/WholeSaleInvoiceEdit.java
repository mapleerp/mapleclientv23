package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javax.management.loading.PrivateClassLoader;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;

import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPopUp;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class WholeSaleInvoiceEdit {
	
	String taskid;
	String processInstanceId;
	
	Boolean reportView = false;

	private ObservableList<SalesTransHdr> salesTransHdrList = FXCollections.observableArrayList();
	String sdate = null;
	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();

	SalesTransHdr salesTransHdr = null;
	String custId = "";
	boolean customerIsBranch = false;
	EventBus eventBus = EventBusFactory.getEventBus();
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");
	SalesDtl salesDtl = null;
	
	
	String storeFromPopUp = null;

	
	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtTax;

	@FXML
	private TextField txtItemcode;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnMrp;

	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TextField txtSBICard;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private TextField txtSodexoCard;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtYesCard;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField custname;

	@FXML
	private TextField custAdress;

	@FXML
	private TextField gstNo;

	@FXML
	private DatePicker dpDate;

	@FXML
	private Button btnFetchInvoice;

	@FXML
	private TableView<SalesTransHdr> tblInvoice;

	@FXML
	private TableColumn<SalesTransHdr, String> clVoucherNo;

	@FXML
	private TableColumn<SalesTransHdr, String> clCustomerName;

	@FXML
	private void initialize() {
		
//		if(SystemSetting.getUser().getUserName().equalsIgnoreCase("test") || 
//				SystemSetting.getUser().getUserName().equalsIgnoreCase("maplesystem") ) {
//			return;
//			
//		}
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		eventBus.register(this);

		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtBatch.textProperty().bindBidirectional(batchProperty);

		tblInvoice.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getVoucherNumber()) {
					salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(newSelection.getVoucherNumber(), sdate);
					custname.setText(salesTransHdr.getAccountHeads().getAccountName());
					custAdress.setText(salesTransHdr.getAccountHeads().getPartyAddress1());
					gstNo.setText(salesTransHdr.getAccountHeads().getPartyGst());
					custId = salesTransHdr.getAccountHeads().getId();
					getSalesDtls();
				}
			}
		});
		
		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					
					txtBatch.setText(newSelection.getBatchCode());
					
					txtBarcode.setText(newSelection.getBarcode());
					txtItemname.setText(newSelection.getItemName());
					txtItemcode.setText(newSelection.getItemCode());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));
					txtTax.setText(String.valueOf(newSelection.getTaxRate()));
					System.out.println("idddddddddd"+newSelection.getId());
					salesDtl = new SalesDtl();
					salesDtl.setId(newSelection.getId());
				}
			}
		});
		
	
		

	}
	
	public void getSalesByVoucherNo(String vucherNo)
	{
		salesTransHdr = RestCaller.getSalesTransHdrByVoucherNo(vucherNo);
		custname.setText(salesTransHdr.getAccountHeads().getAccountName());
		custAdress.setText(salesTransHdr.getAccountHeads().getPartyAddress1());
		gstNo.setText(salesTransHdr.getAccountHeads().getPartyGst());
		ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
		System.out.println(SalesDtlResponse.getBody());
		saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
		itemDetailTable.getItems().clear();
		FillTable();
		btnSave.setVisible(false);
		custAdress.setEditable(false);
		custname.setEditable(false);
		gstNo.setEditable(false);
		btnAdditem.setVisible(false);
		btnDeleterow.setVisible(false);
		txtCashtopay.setEditable(false);
		btnFetchInvoice.setVisible(false);
		dpDate.setEditable(false);
		tblInvoice.setVisible(false);
	}

	private void getSalesDtls() {
		ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
		System.out.println(SalesDtlResponse.getBody());
		saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
		itemDetailTable.getItems().clear();
		FillTable();
	}

	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
		if (null != summary.getTotalAmount()) {
			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
		}

	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

//		if(SystemSetting.getUser().getUserName().equalsIgnoreCase("test") || 
//				SystemSetting.getUser().getUserName().equalsIgnoreCase("maplesystem") ) {
//			return;
			
//		}

		
		
		loadCustomerPopup();
	}

	@FXML
	void FetchInvoice(ActionEvent event) {
		fetchinvoice();
	}

	@FXML
	void FetchInvoiceOnKeyPress(KeyEvent event) {

		
//		if(SystemSetting.getUser().getUserName().equalsIgnoreCase("test") || 
//				SystemSetting.getUser().getUserName().equalsIgnoreCase("maplesystem") ) {
//			return;
//			
//		}
//
//		
		
		if (event.getCode() == KeyCode.ENTER) {
			fetchinvoice();
		}
	}

	private void fetchinvoice() {
		tblInvoice.getItems().clear();
		itemDetailTable.getItems().clear();
		custname.clear();
		custAdress.clear();
		gstNo.clear();
		if (null == dpDate.getValue()) {

		} else {

			Date date = SystemSetting.localToUtilDate(dpDate.getValue());
			sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

			ArrayList invoice = new ArrayList();
			invoice = RestCaller.getWholeSaleInvoiceBillByUser(sdate,SystemSetting.getUser().getId(),SystemSetting.systemBranch);

			String voucherNo = "";
			String customerName = "";
			Iterator itr = invoice.iterator();

			while (itr.hasNext()) {
				List element = (List) itr.next();
				voucherNo = (String) element.get(0);
				customerName = (String) element.get(1);

				if (null != voucherNo) {

					SalesTransHdr salesTransHdr = new SalesTransHdr();
					salesTransHdr.setVoucherNumber(voucherNo);
					salesTransHdr.setCustomerName(customerName);
					salesTransHdrList.add(salesTransHdr);

				}
			}
			
			FillInvoiceTable();

		}

	}

	private void FillInvoiceTable() {

		tblInvoice.setItems(salesTransHdrList);
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
		clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();
	}

	private void addItem() {
		
		//boolean StopEdit = true;
		//if(StopEdit) {
		//	return;
		//}
		
		itemDetailTable.getItems().clear();
		if (null != salesTransHdr) {
			if (null != salesTransHdr.getId()) {
				if (txtQty.getText().trim().isEmpty()) {

					notifyMessage(5, " Please Enter Quantity...!!!", false);
					return;

				}

				if (txtItemname.getText().trim().isEmpty()) {
					notifyMessage(5, " Please Select Item Name...!!!", false);
					txtItemname.requestFocus();
					return;
				} else 
				{
					
					ArrayList items = new ArrayList();
					items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());
					Double chkQty = 0.0;
					
					//========new function for find the storewise stock qty==============// 
					ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
					chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(),txtBatch.getText(),storeFromPopUp); 
					
					String itemId = null;
					Iterator itr = items.iterator();
					while (itr.hasNext()) {
						List element = (List) itr.next();
//						chkQty = (Double) element.get(4);
						itemId = (String) element.get(7);
					}
					

//					if(chkQty < Double.parseDouble(txtQty.getText()))
//					{
//						notifyMessage(3, "Not in Stock!!!", true);
//						txtQty.clear();
//						txtItemname.clear();
//						txtBarcode.clear();
//						txtBatch.clear();
//						txtRate.clear();
//						txtBarcode.requestFocus();
//						ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
//						saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
//						FillTable();
//						return;
//					}

					if (null == salesDtl) {
						salesDtl = new SalesDtl();
					}
					if (null != salesDtl.getId()) {
						System.out.println("toDeleteSale.getId()" + salesDtl.getId());
						RestCaller.deleteSalesDtl(salesDtl.getId());

					}
					/*
					 * new url for getting account heads instead of customer mst=========05/0/2022
					 */
//					ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerById(salesTransHdr.getCustomerMst().getId());
					ResponseEntity<AccountHeads> accountHeadsResp=RestCaller.getAccountHeadsById(salesTransHdr.getAccountHeads().getId());
					AccountHeads accountHeads = accountHeadsResp.getBody();
					if (null == accountHeads) {
						return;
					}
					salesTransHdr.setAccountHeads(accountHeads);

					
					
					if(null!=accountHeads.getCustomerDiscount()) {
						salesTransHdr.setDiscount((accountHeads.getCustomerDiscount().toString()));
					}else {
						salesTransHdr.setDiscount("0");
					}
					
					salesDtl.setSalesTransHdr(salesTransHdr);
					salesDtl.setItemName(txtItemname.getText());
					salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());

					String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();
					System.out.println(batch);
					salesDtl.setBatchCode(batch);

					Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());

					salesDtl.setQty(qty);

					salesDtl.setItemCode(txtItemcode.getText());
					Double mrpRateIncludingTax = 00.0;
					mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());

					salesDtl.setMrp(mrpRateIncludingTax);
					salesDtl.setStandardPrice(mrpRateIncludingTax);
					Double taxRate = 00.0;

					ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
					ItemMst item = respsentity.getBody();
					
					if(!txtBarcode.getText().trim().isEmpty())
					{
						if(!txtBarcode.getText().equalsIgnoreCase(respsentity.getBody().getBarCode()))
						{
							notifyMessage(1, "Sorry... invalid item barcode!!!",false);
							txtBarcode.requestFocus();
							return;
						}
						
					}
//					Double itemsqty = 0.0;
//					ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
//							item.getId(), txtBatch.getText());
//					saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
//					if (saleListItemTable.size() > 0) {
//						Double PrevQty = 0.0;
//						for (SalesDtl saleDtl : saleListItemTable) {
//							if (!saleDtl.getUnitId().equalsIgnoreCase(item.getUnitId())) {
//								PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
//										item.getUnitId(), saleDtl.getQty());
//							} else {
//								PrevQty = saleDtl.getQty();
//							}
//							itemsqty = itemsqty + PrevQty;
//						}
//					}
//					else
//					{
//						itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId);
//					}
//				
//				
//					if (chkQty < itemsqty) {
//						notifyMessage(1, "Not in Stock!!!",false);
//						txtQty.clear();
//						txtItemname.clear();
//						txtBarcode.clear();
//						txtBatch.clear();
//						txtRate.clear();
//						txtBarcode.requestFocus();
//						ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
//						saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
//						FillTable();
//						return;
//					}
//				
//
//				else if (chkQty < itemsqty + Double.parseDouble(txtQty.getText())) {
//					txtQty.clear();
//					txtItemname.clear();
//					txtBarcode.clear();
//					txtBatch.clear();
//					txtRate.clear();
//					txtBarcode.requestFocus();
//					notifyMessage(1, "Not in Stock!!!",false);
//					return;
//				}
//		salesDtl.setUnitName(unitMst.getUnitName());
					taxRate = Double.parseDouble(txtTax.getText());

					salesDtl.setTaxRate(taxRate);

					salesDtl.setItemId(item.getId());
					salesDtl.setUnitId(item.getUnitId());

					ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());
					salesDtl.setUnitName(unitMst.getBody().getUnitName());

					Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
					salesDtl.setRate(rateBeforeTax);
					if(null != salesTransHdr.getAccountHeads().getDiscountProperty())
					{
						
					
					if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE"))
					{
						calcDiscountOnBasePrice(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate);
						
					}
					if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP"))
					{
//						salesDtl.setTaxRate(0.0);
						calcDiscountOnMRP(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate);
					}
					if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX"))
					{
					ambrossiaDiscount(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate);
					}
					}
					else
					{
						double cessAmount = 0.0;
						double cessRate = 0.0;
						salesDtl.setRate(rateBeforeTax);
						if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C"))
						{
							if (item.getCess() > 0) {
								cessRate = item.getCess();
								rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
							
							salesDtl.setRate(rateBeforeTax);
							cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;
							}
							else {
								cessAmount = 0.0;
								cessRate = 0.0;
							}
							salesDtl.setRate(rateBeforeTax);
							salesDtl.setCessRate(cessRate);
							salesDtl.setCessAmount(cessAmount);
						}
						
						salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
						double sgstTaxRate = taxRate / 2;
						double cgstTaxRate = taxRate / 2;
						salesDtl.setCgstTaxRate(cgstTaxRate);

						salesDtl.setSgstTaxRate(sgstTaxRate);
						String companyState = SystemSetting.getUser().getCompanyMst().getState();
						String customerState = "KERALA";
						try {
							customerState = salesTransHdr.getAccountHeads().getCustomerState();
						} catch (Exception e) {

						}

						if (null == customerState) {
							customerState = "KERALA";
						}

						if (null == companyState) {
							companyState = "KERALA";
						}

						if (customerState.equalsIgnoreCase(companyState)) {
							salesDtl.setSgstTaxRate(taxRate / 2);

							salesDtl.setCgstTaxRate(taxRate / 2);

							salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

							salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

							salesDtl.setIgstTaxRate(0.0);
							salesDtl.setIgstAmount(0.0);
						

						} else {
							salesDtl.setSgstTaxRate(0.0);

							salesDtl.setCgstTaxRate(0.0);

							salesDtl.setCgstAmount(0.0);

							salesDtl.setSgstAmount(0.0);

							salesDtl.setIgstTaxRate(taxRate);
							salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

						}
					}
					
//					

					String companyState = SystemSetting.getUser().getCompanyMst().getState();
					String customerState = "KERALA";
					try {
						customerState = salesTransHdr.getAccountHeads().getCustomerState();
					} catch (Exception e) {

					}

					if (null == customerState) {
						customerState = "KERALA";
					}

					if (null == companyState) {
						companyState = "KERALA";
					}

					if (customerState.equalsIgnoreCase(companyState)) {
						salesDtl.setSgstTaxRate(taxRate / 2);

						salesDtl.setCgstTaxRate(taxRate / 2);

						salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

						salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

						salesDtl.setIgstTaxRate(0.0);
						salesDtl.setIgstAmount(0.0);
					

					} else {
						salesDtl.setSgstTaxRate(0.0);

						salesDtl.setCgstTaxRate(0.0);

						salesDtl.setCgstAmount(0.0);

						salesDtl.setSgstAmount(0.0);

						salesDtl.setIgstTaxRate(taxRate);
						salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

					}
				

				
				BigDecimal settoamount = new BigDecimal(
						(Double.parseDouble(txtQty.getText()) *salesDtl.getRate() ));
				
				double includingTax= (settoamount.doubleValue()*salesDtl.getTaxRate())/100;
				double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
				BigDecimal setamount = new BigDecimal(amount);
				setamount = setamount.setScale(2, BigDecimal.ROUND_CEILING);
				salesDtl.setAmount(setamount.doubleValue());
					ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);

					ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());

					List<SalesDtl> salesDtlList = respentityList.getBody();

					/*
					 * Call Rest to get the summary and set that to the display fields
					 */

					// salesDtl.setTempAmount(amount);
					saleListTable.clear();
					saleListTable.setAll(salesDtlList);

					// ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
					// salesDtl = respentity.getBody();

					// saleListTable.add(salesDtl);
					
					FillTable();

					txtItemname.setText("");
					txtBarcode.setText("");
					txtQty.setText("");
					txtTax.setText("");
					txtRate.setText("");

					txtItemcode.setText("");
					txtBatch.setText("");
					txtBarcode.requestFocus();
					salesDtl = new SalesDtl();
				}
				
			} else {
				notifyMessage(5, " Please Select voucher number...!!!", false);
			}
		}
	}
		private void calcDiscountOnBasePrice(SalesTransHdr salesTransHdr,Double rateBeforeTax,
				ItemMst item,Double mrpRateIncludingTax,double taxRate)
		{
			if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
			{
				Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
				BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_CEILING);
				salesDtl.setRate(BrateAfterDiscount.doubleValue());
				BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
				rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
				//salesDtl.setMrp(rateBefrTax.doubleValue());
				salesDtl.setStandardPrice(rateBefrTax.doubleValue());
			}
			else
			{
			salesDtl.setRate(rateBeforeTax);
			}
			double cessAmount = 0.0;
			double cessRate = 0.0;

			if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();

					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

					System.out.println("rateBeforeTax---------" + rateBeforeTax);
					
					if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
					{
						Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
						BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
						BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_CEILING);
						salesDtl.setRate(BrateAfterDiscount.doubleValue());
						BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
						rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
						//salesDtl.setMrp(rateBefrTax.doubleValue());
						salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
					}
					else
					{
					salesDtl.setRate(rateBeforeTax);
					}
					//salesDtl.setRate(rateBeforeTax);

					cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

					/*
					 * Recalculate RateBefore Tax if Cess is applied
					 */

				}
			} else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}

			salesDtl.setCessRate(cessRate);
			salesDtl.setCessAmount(cessAmount);
				

			
		}

		
		private void ambrossiaDiscount(SalesTransHdr salesTransHdr,Double rateBeforeTax,
				ItemMst item,Double mrpRateIncludingTax,double taxRate)
		{
			
			if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
			{
			double discoutAmount = (rateBeforeTax * salesTransHdr.getAccountHeads().getCustomerDiscount())/100;
			BigDecimal BrateAfterDiscount = new BigDecimal(discoutAmount);
			
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_CEILING);
			double newRate = rateBeforeTax - discoutAmount;
			salesDtl.setDiscount(discoutAmount);
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_CEILING);
			salesDtl.setRate(BnewRate.doubleValue());
			BigDecimal BrateBeforeTax = new BigDecimal(rateBeforeTax);
			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_CEILING);
			//salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
			salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());
			}
			else
			{
			salesDtl.setRate(rateBeforeTax);
			}
			double cessAmount = 0.0;
			double cessRate = 0.0;
			if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();

					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

					System.out.println("rateBeforeTax---------" + rateBeforeTax);
					
					if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
					{
						Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
						BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
						BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_CEILING);
						salesDtl.setRate(BrateAfterDiscount.doubleValue());
						BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
						rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
						//salesDtl.setMrp(rateBefrTax.doubleValue());
						salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
					}
					else
					{
					salesDtl.setRate(rateBeforeTax);
					}
					//salesDtl.setRate(rateBeforeTax);

					cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

					/*
					 * Recalculate RateBefore Tax if Cess is applied
					 */

				}
			} else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}

			salesDtl.setCessRate(cessRate);
			salesDtl.setCessAmount(cessAmount);
		}
		private void calcDiscountOnMRP(SalesTransHdr salesTransHdr,Double rateBeforeTax,
				ItemMst item,Double mrpRateIncludingTax,double taxRate)
		{
			if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
			{
				Double rateAfterDiscount = (100 * mrpRateIncludingTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
				Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

				BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
				BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);
				
				BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_CEILING);
				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_CEILING);
				salesDtl.setRate(BnewrateBeforeTax.doubleValue());
				//BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			//	rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
				//salesDtl.setMrp(BrateAfterDiscount.doubleValue());
				salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
			}
			else
			{
			salesDtl.setRate(rateBeforeTax);
			}
			double cessAmount = 0.0;
			double cessRate = 0.0;

			if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
				if (item.getCess() > 0) {
					cessRate = item.getCess();

					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

					System.out.println("rateBeforeTax---------" + rateBeforeTax);
					
					if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
					{
						Double rateAfterDiscount = (100 * mrpRateIncludingTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
						Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

						BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
						BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_CEILING);
						salesDtl.setRate(BrateAfterDiscount.doubleValue());
						BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
						rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
						//salesDtl.setMrp(mrpRateIncludingTax);
						salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
					}
					else
					{
					salesDtl.setRate(rateBeforeTax);
					}
					//salesDtl.setRate(rateBeforeTax);

					cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

					/*
					 * Recalculate RateBefore Tax if Cess is applied
					 */

				}
			} else {
				cessAmount = 0.0;
				cessRate = 0.0;
			}

			salesDtl.setCessRate(cessRate);
			salesDtl.setCessAmount(cessAmount);
				
			
		}
	@FXML
	void deleteRow(ActionEvent event) {

		if (null != salesDtl) {
			if (null != salesDtl.getId()) {
				System.out.println("toDeleteSale.getId()" + salesDtl.getId());
				RestCaller.deleteSalesDtl(salesDtl.getId());
				txtItemname.clear();
				txtItemcode.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtTax.clear();
				txtRate.clear();
				txtQty.clear();
				ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
				saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
				FillTable();
				salesDtl = new SalesDtl();
				notifyMessage(5, " Item Deleted Successfully", true);

			}
		}

	}

	@FXML
	void hold(ActionEvent event) {

	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}
	
	   @FXML
	    void FinalSaveOnPress(KeyEvent event) {
		   if (event.getCode() == KeyCode.ENTER) {
		   finalSave();
		   }
	    }

	   private void finalSave() {
		   
			//boolean StopEdit = true;
			//if(StopEdit) {
			//	return;
			//}
			
			


			/*
			 * FinalSave Final
			 */
			try {

				txtChangeamount.setText("00.0");
				Double paidAmount = 0.0;
				Double cardAmount = 0.0;
				String card = "";
				if (!txtSBICard.getText().trim().isEmpty()) {
					card = txtSBICard.getText();
				} else if (!txtYesCard.getText().trim().isEmpty()) {
					card = txtYesCard.getText();
				} else if (!txtSodexoCard.getText().trim().isEmpty()) {
					card = txtSodexoCard.getText();
				}
				salesTransHdr.setCardNo(card);
				salesTransHdr.setEditedStatus("YES");
				Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
				salesTransHdr.setInvoiceAmount(invoiceAmount);

				if (!txtPaidamount.getText().trim().isEmpty()) {

					paidAmount = Double.parseDouble(txtPaidamount.getText());
					salesTransHdr.setCashPay(paidAmount);
				} else {
					salesTransHdr.setCashPay(0.0);
				}

				if (!txtcardAmount.getText().trim().isEmpty()) {

					cardAmount = Double.parseDouble(txtcardAmount.getText());
					salesTransHdr.setCardamount(cardAmount);
				} else {
					salesTransHdr.setCardamount(0.0);
				}

				salesTransHdr.setPaidAmount(salesTransHdr.getCashPay() + salesTransHdr.getCardamount());

				if (!txtChangeamount.getText().trim().isEmpty()) {
					Double changeAmount = Double.parseDouble(txtChangeamount.getText());
					salesTransHdr.setChangeAmount(changeAmount);
				}
				
				/*
				 * new url for getting account heads instead of customer mst=========05/0/2022
				 */
//				CustomerMst customerMst = RestCaller.getCustomerByName(custname.getText()).getBody();
				ResponseEntity<AccountHeads> accountHeadsResp=RestCaller.getAccountHeadsByName(custname.getText());
				if(null != accountHeadsResp.getBody())
				{
					salesTransHdr.setAccountHeads(accountHeadsResp.getBody());
					if (null == accountHeadsResp.getBody().getPartyGst() || accountHeadsResp.getBody().getPartyGst().length() < 13) {
						salesTransHdr.setSalesMode("B2C");
					} else {
						salesTransHdr.setSalesMode("B2B");
					}
				}
				salesTransHdr.setCustomerId(custId);
				eventBus.post(salesTransHdr);

				// Double
				// change=Double.parseDouble(txtPaidamount.getText())-Double.parseDouble(txtCashtopay.getText());
				// txtChangeamount.setText(Double.toString(change));
				// btnSave.setDisable(false);
				// btnSave.setDisable(true);

				ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
				if (saledtlSaved.getBody().size() == 0) {
					return;
				}
				System.out.println("=====salesTransHdr.getCustomerId()======" + custId);

				ResponseEntity<BranchMst> branchMst = RestCaller.getBranchMstById(custId);

				BranchMst branch = new BranchMst();
				branch = branchMst.getBody();

				if (null != branch) {
					salesTransHdr.setSalesMode("BRANCH_SALES");
				}  

				//String financialYear = SystemSetting.getFinancialYear();
				//String vNo = RestCaller.getVoucherNumber(financialYear + "CRD");
				// if((Double.parseDouble(txtCashtopay.getText())) > (paidAmount+cardAmount))
				// {

				//salesTransHdr.setVoucherNumber(vNo);
				
//				salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
				
				
//				Date date = Date.valueOf(LocalDate.now());
	//
//				salesTransHdr.setVoucherDate(date);
				// Call Rest to find if customer is Branch

				if (customerIsBranch) {
					salesTransHdr.setIsBranchSales("Y");
				} else {
					salesTransHdr.setIsBranchSales("N");
				}
				 ResponseEntity<AccountHeads> custentity = RestCaller.getAccountHeadsByName(custname.getText());
				ResponseEntity<AccountReceivable> accountReceivableResp = RestCaller.getAccountReceivableBySalesTransHdrId(salesTransHdr.getId());
				  AccountReceivable accountReceivable = accountReceivableResp.getBody();
				 
				  if(null == accountReceivable)
				  {
					   accountReceivable = new AccountReceivable();
					   accountReceivable.setAccountId(custentity.getBody().getId());
					   accountReceivable.setAccountHeads(custentity.getBody());
				  accountReceivable.setAccountHeads(custentity.getBody());
				 
				  accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
				  LocalDate dueDate = LocalDate.now().plusDays(custentity.getBody().getCreditPeriod());
				  accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
				  accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
				  accountReceivable.setBalanceAmount(Double.parseDouble(txtCashtopay.getText()));
				  accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
				  accountReceivable.setSalesTransHdr(salesTransHdr);
				  accountReceivable.setRemark("Wholesale");
				  LocalDate ldate =SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
				  accountReceivable.setVoucherDate(java.sql.Date.valueOf(ldate));
				  accountReceivable.setPaidAmount(0.0);
				  
				  ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
				  } else {
					  accountReceivable.setAccountId(custentity.getBody().getId());
					 
					  accountReceivable.setAccountHeads(custentity.getBody());
					  accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
					  LocalDate due = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
					  LocalDate dueDate = due.plusDays(custentity.getBody().getCreditPeriod());
					  accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
					  accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
					  accountReceivable.setBalanceAmount(Double.parseDouble(txtCashtopay.getText()));
					  accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
					  accountReceivable.setSalesTransHdr(salesTransHdr);
					  accountReceivable.setRemark("Wholesale");
					  accountReceivable.setVoucherDate(salesTransHdr.getVoucherDate());
					  accountReceivable.setPaidAmount(0.0);
					  
					  
					  RestCaller.updateAccountReceivableWithVoucherDate(accountReceivable);
				}
				  
				   RestCaller.deleteSalesReceiptsByTransHdr(salesTransHdr);
				 
				  
				   SalesReceipts salesReceipts = new SalesReceipts();
				 
				 
					salesReceipts.setReceiptMode("CREDIT");
					 salesReceipts.setReceiptAmount(invoiceAmount);
					 salesReceipts.setSalesTransHdr(salesTransHdr);
					 salesReceipts.setAccountId(custentity.getBody().getId());
					 salesReceipts.setBranchCode(salesTransHdr.getBranchCode());
					 
					 RestCaller.saveSalesReceipts(salesReceipts);
				  //Update ItemBatchDtl
					 try
					 {
					 RestCaller.deleteItemBatchDtlByParentId(salesTransHdr.getId());
					 }
					 catch (Exception e) {
						System.out.println(e);
					}
				  
				System.out.println(salesTransHdr);
				  
//				  if(null==salesTransHdr.getVoucherNumber()) {
						RestCaller.updateSalesTranshdr(salesTransHdr);
//					}
				  salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
					Format formatter;
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					String strDate = formatter.format(salesTransHdr.getVoucherDate());
				  // Update Status of invoice edit enable mst
				 String s=  RestCaller.updateInvoiceEditEnableMst(strDate,salesTransHdr.getVoucherNumber());
				  
				   
				notifyMessage(5, "Sales Saved",true);

			


				NewJasperPdfReportService.TaxInvoiceReport(salesTransHdr.getVoucherNumber(), strDate);
//			JasperPdfReportService.TaxInvoiceReport("2019-2020CRD000009","2019-07-12");

				salesTransHdr = null;
				txtcardAmount.setText("");
				txtCashtopay.setText("");
				txtPaidamount.setText("");
				txtSBICard.setText("");
				txtSodexoCard.setText("");
				txtYesCard.setText("");
				custname.setText("");
				custAdress.setText("");
				gstNo.setText("");
				saleListTable.clear();
				custId = null;
				dpDate.setValue(null);
				tblInvoice.getItems().clear();

				// }
			} catch (Exception e) {
				e.printStackTrace();
			}

		

		
		
	}

	private void showPopup() {
		
			try {
				System.out.println("STOCK POPUP");
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
				// fxmlLoader.setController(itemStockPopupCtl);
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				txtQty.requestFocus();
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		

	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {

	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {
		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtItemname.requestFocus();
			showPopup();
		}
		if (event.getCode() == KeyCode.ENTER) {
			if(txtBarcode.getText().trim().isEmpty())
			btnSave.requestFocus();
		}

	}

	@FXML
	void onEnterCustPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();

		}
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		}
	}

	@FXML
	void save(ActionEvent event) {
		finalSave();
	}

	@FXML
	void toPrintChange(KeyEvent event) {

	}

	@FXML
	void unHold(ActionEvent event) {

	}

	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtItemname.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			custname.setText(customerEvent.getCustomerName());
			custAdress.setText(customerEvent.getCustomerAddress());
			gstNo.setText(customerEvent.getCustomerGst());
			custId = customerEvent.getCustId();
			System.out.println("custIdcustIdcustId" + custId);
			customerIsBranch = isCustomerABranch(customerEvent.getCustomerName());

		}

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			itemNameProperty.set(itemPopupEvent.getItemName());
			// salesDtl.setItemName(itemNameProperty.get());
			txtBarcode.setText(itemPopupEvent.getBarCode());
			batchProperty.set(itemPopupEvent.getBatch());
			
			
			if(null != itemPopupEvent.getStoreName()) {
				storeFromPopUp = itemPopupEvent.getStoreName();
			}else {
				storeFromPopUp = "MAIN";
			}

			txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
			txtTax.setText(Double.toString(itemPopupEvent.getTaxRate()));
			ResponseEntity<AccountHeads> custMst = RestCaller.getAccountHeadsById(custId);
			Date udate = SystemSetting.getApplicationDate();
			String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		
			ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
					itemPopupEvent.getItemId(), custMst.getBody().getPriceTypeId(), itemPopupEvent.getUnitId(),
					itemPopupEvent.getBatch(),sdate);
			if (null != batchPriceDef.getBody()) {
				txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
			} else {
				ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
						itemPopupEvent.getItemId(), custMst.getBody().getPriceTypeId(), itemPopupEvent.getUnitId(),sdate);

				if (null != pricebyItem.getBody()) {
					
						ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp2 = RestCaller.getPriceDefenitionMstByName("MRP");
						if(null != priceDefenitionMstResp2.getBody())
						{
					if(!pricebyItem.getBody().getPriceId().equalsIgnoreCase(priceDefenitionMstResp2.getBody().getId()))
					txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
					
//					else
//					{
//						txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
//					}
				}
				}
			}

			System.out.println(itemPopupEvent.toString());

		}
	}

	private boolean isCustomerABranch(String customerName) {
		ArrayList branchRest = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		branchRest = RestCaller.SearchBranchLocalhost();
		Iterator itr1 = branchRest.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			System.out.println("---branchRest---" + lm);
			String branchName = (String) lm.get("branchName");
			if (customerName.equalsIgnoreCase(branchName)) {
				return true;
			}

		}

		return false;
	}
	
	public void getVoucherNoAndDate(String voucherNo, java.sql.Date voucherDate, String accountId) {
		
		reportView = true;
		btnAdditem.setDisable(true);
		btnDeleterow.setDisable(true);
		btnSave.setDisable(true);
		btnFetchInvoice.setDisable(true);
		
		String sqlDateString = SystemSetting.SqlDateTostring(voucherDate);
		System.out.println(sqlDateString);
		Date uDate = SystemSetting.StringToUtilDate(sqlDateString, "dd/MM/yyyy");
		System.out.println(uDate);
		
		String utilStringDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		System.out.println(utilStringDate);
		
		salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNo, utilStringDate);
		
		if(null == salesTransHdr)
		{
			return;
		}
		
		ResponseEntity<AccountHeads> customerResp = RestCaller.getAccountHeadsById(accountId);
		AccountHeads customerMst = customerResp.getBody();
		
		custname.setText(customerMst.getAccountName());
		custAdress.setText(customerMst.getPartyAddress());
		gstNo.setText(customerMst.getPartyGst());
		
		ResponseEntity<List<SalesDtl>> salesDtlListResp = RestCaller.getSalesDtl(salesTransHdr);
		List<SalesDtl> salesDtlList = salesDtlListResp.getBody();
		if(salesDtlList.size() > 0)
		{
			saleListTable = FXCollections.observableArrayList(salesDtlListResp.getBody());
			FillTable();
		}
		
		
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @FXML
    void EscOnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ESCAPE) {
			if(reportView)
			{
				Stage stage = (Stage) btnFetchInvoice.getScene().getWindow();
				stage.close();
			}
		}

    }
	
	  @FXML
	    void EscOnKeypress1(KeyEvent event) {
		  
			if (event.getCode() == KeyCode.ESCAPE) {
				if(reportView)
				{
					Stage stage = (Stage) btnFetchInvoice.getScene().getWindow();
					stage.close();
				}
			}

	    }

	    @FXML
	    void EscOnKeypress2(KeyEvent event) {

	    	if (event.getCode() == KeyCode.ESCAPE) {
				if(reportView)
				{
					Stage stage = (Stage) btnFetchInvoice.getScene().getWindow();
					stage.close();
				}

			}
	    }

	    @FXML
	    void EscOnKeypress3(KeyEvent event) {

	    	if (event.getCode() == KeyCode.ESCAPE) {
	    		if(reportView)
				{
					Stage stage = (Stage) btnFetchInvoice.getScene().getWindow();
					stage.close();
				}
			}
	    }

	    @FXML
	    void EscOnKeypress4(KeyEvent event) {

	    	if (event.getCode() == KeyCode.ESCAPE) {
	    		if(reportView)
				{
					Stage stage = (Stage) btnFetchInvoice.getScene().getWindow();
					stage.close();
				}
			}
	    }
	    
	    @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
