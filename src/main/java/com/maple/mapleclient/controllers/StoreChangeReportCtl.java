package com.maple.mapleclient.controllers;

import java.sql.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.StoreChangeDtl;
import com.maple.mapleclient.entity.StoreChangeMst;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class StoreChangeReportCtl {
	
	String taskid;
	String processInstanceId;
	
	String value=null;
	
	private ObservableList<StoreChangeMst> storeChangeMstList = FXCollections.observableArrayList();
	private ObservableList<StoreChangeDtl> storeChangeDtllist = FXCollections.observableArrayList();

	StoreChangeMst storeChangeMst = null;
    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnClear;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TableView<StoreChangeMst> tblStoreMst;

    @FXML
    private TableColumn<StoreChangeMst, String> clDate;

    @FXML
    private TableColumn<StoreChangeMst, String> clVoucherNumber;

    @FXML
    private TableColumn<StoreChangeMst, String> clfromStore;

    @FXML
    private TableColumn<StoreChangeMst, String> clToStore;

    @FXML
    private TableView<StoreChangeDtl> tblStoreDtl;

    @FXML
    private TableColumn<StoreChangeDtl, String> clItemName;

    @FXML
    private TableColumn<StoreChangeDtl, Number> clQty;

    @FXML
    private TableColumn<StoreChangeDtl, String> clUnit;

    @FXML
    private TableColumn<StoreChangeDtl, Number> clRate;
    @FXML
    private TableColumn<StoreChangeDtl, String> clBatch;
    @FXML
    private TableColumn<StoreChangeDtl, Number> clAmount;
    @FXML
	private void initialize() {
    	
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblStoreMst.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					storeChangeMst = new StoreChangeMst();
					storeChangeMst.setId(newSelection.getId());
					storeChangeMst.setStore(newSelection.getStore());
					storeChangeMst.setToStore(newSelection.getToStore());
					storeChangeMst.setVoucherDate(newSelection.getVoucherDate());
					ResponseEntity<List<StoreChangeDtl>> storeChangeresp = RestCaller.findStoreChangeByHdrId(newSelection.getId());
					storeChangeDtllist = FXCollections.observableArrayList(storeChangeresp.getBody());
					value = storeChangeDtllist.get(0).getId();
					fillDtlTable();
				}
			}
		});
    	
    }
    @FXML
    void actionClear(ActionEvent event) {
    	dpFromDate.setValue(null);
    	dpToDate.setValue(null);
    	tblStoreDtl.getItems().clear();
    	tblStoreMst.getItems().clear();
    	storeChangeDtllist.clear();
    	value = null;
    }

    @FXML
    void actionPrint(ActionEvent event) {
    	if(null != value) {
    	try {
			JasperPdfReportService.storeChangeReport(storeChangeMst);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	}else {
    		notifyMessage(3, "Please select the header value", false);
    	}

    }

    @FXML
    void actionShow(ActionEvent event) {

    	if(null == dpFromDate.getValue())
    	{
    		dpFromDate.requestFocus();
    		notifyMessage(3,"Please Select From Date",false);
    		return;
    	}
    	if(null == dpToDate.getValue())
    	{
    		dpToDate.requestFocus();
    		notifyMessage(3,"Please Select To Date",false);
    		return;
    	}
    	Date fdate = Date.valueOf(dpFromDate.getValue());
    	String fsdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
    	Date tdate = Date.valueOf(dpToDate.getValue());
    	String tsdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
    	ResponseEntity<List<StoreChangeMst>> storechangeMst = RestCaller.StoreChangeMstBtwnDate(fsdate,tsdate);
    	storeChangeMstList = FXCollections.observableArrayList(storechangeMst.getBody());
    	fillTable();
    	
    }
    private void fillDtlTable()
    {
    	for(StoreChangeDtl str : storeChangeDtllist)
    	{
    		ResponseEntity<UnitMst> getunit = RestCaller.getunitMst(str.getUnitId());
    		str.setUnitName(getunit.getBody().getUnitName());
//    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(str.getItemId());
//    		str.setItemName(getItem.getBody().getItemName());
    	}
    	tblStoreDtl.setItems(storeChangeDtllist);
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

    }
    private void fillTable()
    {
    	tblStoreMst.setItems(storeChangeMstList);
		clDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clfromStore.setCellValueFactory(cellData -> cellData.getValue().getStoreProperty());
		clToStore.setCellValueFactory(cellData -> cellData.getValue().getToStoreProperty());

    }

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

		
		
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}
}
