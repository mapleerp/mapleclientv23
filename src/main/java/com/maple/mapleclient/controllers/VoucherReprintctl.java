package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;

import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import java.sql.Date;
import java.sql.SQLException;
import java.text.Format;
import java.util.ArrayList;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.ibm.icu.text.SimpleDateFormat;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.POSThermalPrintFactory;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.IntentInHdr;
import com.maple.mapleclient.entity.OtherBranchSalesTransHdr;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.entity.SysDateMst;
import com.maple.mapleclient.entity.VoucherReprint;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class VoucherReprintctl {

	boolean moveCaretToPos = false;
	int caretPos;

	ArrayList<String> voucher = new ArrayList<String>();

	String taskid;
	String processInstanceId;

	POSThermalPrintABS printingSupport;

	private static final Date Date = null;

	private ObservableList<VoucherReprint> getdetailstoprint = FXCollections.observableArrayList();
	VoucherReprint voucherReprint = null;
	SalesTransHdr salesTransHdr = null;
	PurchaseHdr purchaserHdr = null;
	StockTransferOutHdr stockTransferOutHdr = null;
	IntentInHdr intentInHdr = null;
	@FXML
	private DatePicker dpVoucherDate;

//	@FXML
//	private ComboBox<String> cmbVoucherNo;

	@FXML
	private Button btnFetch;

	@FXML
	private Button btnPrint;

	@FXML
	private ChoiceBox<String> voucherType;

	// ================Anandu================

	@FXML
	private TextField txtVouchetSearch;

	@FXML
	private ListView<String> lstVoucherNumber;

	StringProperty voucherNumberProperty = new SimpleStringProperty("");

	// ================Anandu== end==============

	private void saveSalesProperty(SalesTransHdr salesTransHdr) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SalesPropertyTransHdr.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			SalesPropertyTransHdrCtl itemStockPopupCtl = fxmlLoader.getController();
			itemStockPopupCtl.salesTransHdrId = salesTransHdr.getId();
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.WINDOW_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
//			stage.show();
			stage.showAndWait();
		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	void Print(ActionEvent event) {

		if (voucherType.getValue().equals("PURCHASETYPE")) {
			purchase();
		} else if (voucherType.getValue().equals("SALESTYPE")) {
			sales();
		} else if (voucherType.getValue().equals("STKTROUTTYPE")) {
			stock();
		} else if (voucherType.getValue().equals("INTENT REQUESTS")) {
			intent();
		} else if (voucherType.getValue().equals("OTHER BRANCH SALE")) {

			PrintOtherBranchSales();

		}

	}

	private void PrintOtherBranchSales() {

		if (null != lstVoucherNumber.getSelectionModel().getSelectedItems()) {
			String voucherNo = lstVoucherNumber.getSelectionModel().getSelectedItem().toString();
			java.util.Date uDate = Date.valueOf(dpVoucherDate.getValue());
			String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

			OtherBranchSalesTransHdr otherBranchSalesTransHdr = RestCaller
					.getOtherBranchSalesTransHdrByVoucherAndDate(voucherNo, vDate);

			ResponseEntity<BranchMst> branchMst1 = RestCaller
					.getBranchMstById(otherBranchSalesTransHdr.getAccountHeads().getId());
			try {

				if (null == branchMst1.getBody()) {

					JasperPdfReportService.OtherBranchMstTaxInvoiceReport(voucherNo, vDate);

				} else {
					JasperPdfReportService.OtherBranchMstStockTrasferReport(otherBranchSalesTransHdr, voucherNo, vDate);

				}

			} catch (JRException e) {
				e.printStackTrace();
			}
		}
	}

	private void purchase() {

		if (null != lstVoucherNumber.getSelectionModel().getSelectedItem()) {
			String voucherNo = lstVoucherNumber.getSelectionModel().getSelectedItem().toString();
			java.util.Date uDate = Date.valueOf(dpVoucherDate.getValue());
			String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
			try {
				JasperPdfReportService.PurchaseInvoiceReport(voucherNo, vDate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void sales() {
		if (null != lstVoucherNumber.getSelectionModel().getSelectedItem()) {
			// voucherReprint = new VoucherReprint();
			String voucherNo = lstVoucherNumber.getSelectionModel().getSelectedItem().toString();
			java.util.Date uDate = Date.valueOf(dpVoucherDate.getValue());

			String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

			SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNo, vDate);
			if (null == salesTransHdr) {
				return;
			}
			if (null != salesTransHdr) {

				if (salesTransHdr.getSalesMode().equalsIgnoreCase("POS")) {
					try {
						System.out.println("PrintInvoiceThermalPrinter");
						printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (null != salesTransHdr.getIsBranchSales()
						&& salesTransHdr.getIsBranchSales().equalsIgnoreCase("Y")) {
					try {
						System.out.println("OtherBranchMstTaxInvoiceReport");

						JasperPdfReportService.OtherBranchMstTaxInvoiceReport(salesTransHdr.getVoucherNumber(), vDate);

					} catch (Exception e) {
						e.printStackTrace();
					}

				} else {

					try {
						System.out.println("TaxInvoiceReport");

						NewJasperPdfReportService.TaxInvoiceReport(voucherNo, vDate);
					} catch (JRException e) {
						e.printStackTrace();
					}
				}
			}

			// ---------------version 4.20 surya

		}
	}

	private void stock() {
		if (null != lstVoucherNumber.getSelectionModel().getSelectedItem()) {
			// voucherReprint = new VoucherReprint();
			String voucherNo = lstVoucherNumber.getSelectionModel().getSelectedItem().toString();
			java.util.Date uDate = Date.valueOf(dpVoucherDate.getValue());
			String voucherDate = SystemSetting.UtilDateToString(uDate, "dd-MM-yyyy");
//    		voucherReprint.setVoucherDate(uDate);
//    		Format formatter;
//    		formatter = new SimpleDateFormat("dd-MM-yyyy");
//    		String voucherDate = formatter.format(voucherReprint.getVoucherDate());

			try {
				JasperPdfReportService.StockTransferInvoiceReport(voucherNo, voucherDate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private void intent() {
		if (null != lstVoucherNumber.getSelectionModel().getSelectedItem()) {
			// voucherReprint = new VoucherReprint();
			String voucherNo = lstVoucherNumber.getSelectionModel().getSelectedItem().toString();
			java.util.Date uDate = Date.valueOf(dpVoucherDate.getValue());
			String voucherDate = SystemSetting.UtilDateToString(uDate, "dd-MM-yyyy");
//    		voucherReprint.setVoucherDate(uDate);
//    		Format formatter;
//    		formatter = new SimpleDateFormat("dd-MM-yyyy");
//    		String voucherDate = formatter.format(voucherReprint.getVoucherDate());

			try {
				JasperPdfReportService.IntentInReport(voucherNo, voucherDate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@FXML
	void Fetch(ActionEvent event) {

		if (null == dpVoucherDate) {
			notifyMessage(3, "Please select date", false);
			dpVoucherDate.requestFocus();
			return;
		}

		

		VoucherReprint voucherReprint = new VoucherReprint();
		voucherReprint.setVoucherType(voucherType.getSelectionModel().getSelectedItem().toString());
		java.util.Date uDate = Date.valueOf(dpVoucherDate.getValue());
		

		String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
		java.util.Date appDate = Date.valueOf(sysdate);
		
     
		 System.out.println("voucherDate is before applicationDate");
		  if (uDate.compareTo(appDate)< 0) {
			
			
		notifyMessage(3, "Please select correct date", false);
		dpVoucherDate.requestFocus();
		return;
			
			
		}
		
		

		String voucherDate = SystemSetting.UtilDateToString(uDate, "dd-MM-yyyy");

		if ("SALESTYPE" == voucherType.getSelectionModel().getSelectedItem().toString()) {
			fetchSalesVoucher("SALESTYPE", voucherDate);
		} else if ("PURCHASETYPE" == voucherType.getSelectionModel().getSelectedItem().toString()) {

			PurchaseReprint("PURCHASETYPE", voucherDate);
		} else if ("STKTROUTTYPE" == voucherType.getSelectionModel().getSelectedItem().toString()) {

			StockTransferReprint("STKTROUTTYPE", voucherDate);

		} else if ("INTENT REQUESTS" == voucherType.getSelectionModel().getSelectedItem().toString()) {

			IntentReprint("INTENT REQUESTS", voucherDate);
		} else if ("OTHER BRANCH SALE" == voucherType.getSelectionModel().getSelectedItem().toString()) {

			FetchOtherBranchSaleInvoice(voucherDate);

		}
	}

	private void FetchOtherBranchSaleInvoice(String voucherDate) {

		ResponseEntity<List<OtherBranchSalesTransHdr>> OtherBranchSalesTransHdrResp = RestCaller
				.OtherBranchSalesTransHdrByDate(voucherDate);
		List<OtherBranchSalesTransHdr> otherBranchSaleList = OtherBranchSalesTransHdrResp.getBody();

		lstVoucherNumber.getItems().clear();
		for (OtherBranchSalesTransHdr sales : otherBranchSaleList) {
			lstVoucherNumber.getItems().add((String) sales.getVoucherNumber());

		}

	}

	private void fetchSalesVoucher(String voucherType, String voucherDate) {
		lstVoucherNumber.getItems().clear();
		voucher = new ArrayList();
		voucher = RestCaller.ShowVoucherNoSales(voucherType, voucherDate);
		System.out.println("inside sales function" + voucherType + voucherDate);
		for (int i = 0; i < voucher.size(); i++) {
			System.out.println("=====iiiiii=====" + i);
			lstVoucherNumber.getItems().add((String) voucher.get(i));
		}
	}

	private void PurchaseReprint(String voucherType, String voucherDate) {
		lstVoucherNumber.getItems().clear();
		voucher = new ArrayList();
		voucher = RestCaller.ShowVoucherNoSales(voucherType, voucherDate);
		for (int i = 0; i < voucher.size(); i++) {
			lstVoucherNumber.getItems().add((String) voucher.get(i));
		}
	}

	private void StockTransferReprint(String voucherType, String voucherDate) {
		lstVoucherNumber.getItems().clear();
		voucher = new ArrayList();
		voucher = RestCaller.ShowVoucherNoSales(voucherType, voucherDate);
		System.out.println("inside sales function" + voucherType + voucherDate);
		for (int i = 0; i < voucher.size(); i++) {

			lstVoucherNumber.getItems().add((String) voucher.get(i));
		}
	}

	private void IntentReprint(String voucherType, String voucherDate) {
		lstVoucherNumber.getItems().clear();
		voucher = new ArrayList();
		voucher = RestCaller.ShowVoucherNoSales(voucherType, voucherDate);
		System.out.println("inside intent function" + voucherType + voucherDate);
		for (int i = 0; i < voucher.size(); i++) {

			lstVoucherNumber.getItems().add((String) voucher.get(i));
		}
	}

	@FXML
	private void initialize() {

		txtVouchetSearch.textProperty().bindBidirectional(voucherNumberProperty);

		dpVoucherDate = SystemSetting.datePickerFormat(dpVoucherDate, "dd/MMM/yyyy");

		voucherType.getItems().add("SALESTYPE");
		voucherType.getItems().add("PURCHASETYPE");
		voucherType.getItems().add("STKTROUTTYPE");

		voucherType.getItems().add("INTENT REQUESTS");

		voucherType.getItems().add("OTHER BRANCH SALE");

		printingSupport = POSThermalPrintFactory.getPOSThermalPrintABS();
		try {
			printingSupport.setupLogo();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

//=================MAP-164==========================================================		
		voucherNumberProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if (null != newValue) {
					lstVoucherNumber.getItems().clear();
					for (String vou : voucher) {
						if (vou.contains((String) newValue)) {
							lstVoucherNumber.getItems().add(vou);
						}

					}
				} else {
					lstVoucherNumber.getItems().clear();

					for (String vou : voucher) {
						lstVoucherNumber.getItems().add(vou);
					}
				}

			}
		});
//		

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

	}

	

}
