package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


import javafx.scene.control.Label;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountMergeMst;
import com.maple.mapleclient.entity.StockValueReports;

import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;


public class AccountMergeCtl {
	String taskid;
	String processInstanceId;
	
	
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<AccountHeads> accountHeadsList = FXCollections.observableArrayList();
	
	private ObservableList<AccountMergeMst> accountMergeMstList = FXCollections.observableArrayList();


    @FXML
    private TextField txtFromAccount;

    @FXML
    private TextField txtToAccount;

    @FXML
    private Button btnMerge;
    
    @FXML
    private Button btnShowAll;
    
    @FXML
    private Button btnClear;

//    @FXML
//    private TableView<AccountHeads> tblAccount;
    
    @FXML
    private TableView<AccountMergeMst> tblAccount;

    @FXML
    private TableColumn<AccountMergeMst, String> clAccountName;
    

    @FXML
    private TableColumn<AccountMergeMst, String> clToAccount;

    @FXML
    private TableColumn<AccountMergeMst, String> clFromAccount;

    @FXML
    private TableColumn<AccountMergeMst, String> clUpdatedTime;
    
    @FXML
    private Label lblResponse;
    
    @FXML
	private void initialize() {
    	
    	eventBus.register(this);
    	
    }
    
    @FXML
    void Clear(ActionEvent event) {
    	
    	txtFromAccount.clear();
    	txtToAccount.clear();
    	tblAccount.getItems().clear();

    }
    
    @FXML
    void ShowAll(ActionEvent event) {
    	
    	ResponseEntity<List<AccountMergeMst>> fromAccountMergeMstResp = RestCaller.getAccountMergeDtls();
    	List<AccountMergeMst> accountMergeMst = fromAccountMergeMstResp.getBody();
    	
    	
    	
    	accountMergeMstList=FXCollections.observableArrayList(accountMergeMst);
     	tblAccount.setItems(accountMergeMstList);
     	FillTableAccountMrgDtl();

    }


    @FXML
    void Merge(ActionEvent event) {

      
    	
    	if(null == txtFromAccount.getText())
    	{
    		notifyMessage(3, "Please select from account", false);
    		return;
    	}
    	
    	if(null == txtToAccount.getText())
    	{
    		notifyMessage(3, "Please select to account", false);
    		return;
    	}
    	
    	if(txtFromAccount.getText().equals(txtToAccount.getText()))
    	{
    		notifyMessage(3, "Unable to merge", false);
    		return;
    	}
    	
    	if(null == txtToAccount.getText())
    	{
    		notifyMessage(3, "Please select to account", false);
    		return;
    	}
   

    	ResponseEntity<AccountHeads> fromAccountHeadsResp = RestCaller.getAccountHeadByName(txtFromAccount.getText());
    	AccountHeads fromAccountHeads = fromAccountHeadsResp.getBody();
    

    	
    	ResponseEntity<AccountHeads> toAccountHeadsResp = RestCaller.getAccountHeadByName(txtToAccount.getText());
    	AccountHeads toAccountHeads = toAccountHeadsResp.getBody();
    	
    	ResponseEntity<String> accountMerge = RestCaller.accountMerge(fromAccountHeads.getId(),toAccountHeads.getId());
    	
    	ResponseEntity<AccountMergeMst> AccountMergeMstResp = RestCaller.getAccountMergeMstById(fromAccountHeads.getId(),toAccountHeads.getId());
    	AccountMergeMst accountMergeMst = AccountMergeMstResp.getBody();
    	
    	tblAccount.getItems().clear();
    	accountMergeMstList.add(accountMergeMst);
    	tblAccount.setItems(accountMergeMstList);
    	FillTableAccountMrgDtl();
//    	
//    	accountHeadsList.add(fromAccountHeads);
    	txtFromAccount.clear();
    	txtToAccount.clear();
//    	FillTable();

    }

    private void FillTable() {

//    	tblAccount.setItems(accountHeadsList);
	//	clAccountName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		

    	
	}
    private void FillTableAccountMrgDtl()
    {
    	//clAccountName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
    	
    	clToAccount.setCellValueFactory(cellData -> cellData.getValue().getToAccountNameProperty());
    	clFromAccount.setCellValueFactory(cellData -> cellData.getValue().getFromAccountNameProperty());
    	clUpdatedTime.setCellValueFactory(cellData -> cellData.getValue().getUpdatedDateProperty());
    }

	@FXML
    void fromAccountPopup(KeyEvent event) {


    	if (event.getCode() == KeyCode.ENTER) {
    		fromAccountshowPopup();

		}
    }

    @FXML
    void fromAccountPopupClicked(MouseEvent event) {
    	fromAccountshowPopup();

    }

    @FXML
    void toAccountPupup(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		toAccountshowPopup();

		}
    }

    @FXML
    void toAccountPupupClicked(MouseEvent event) {
    	
    	toAccountshowPopup();

    }
    
    
    
	private void fromAccountshowPopup() {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtFromAccount.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	private void toAccountshowPopup() {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtFromAccount.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	
	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------fromAccount-------------");
		Stage stage = (Stage) txtFromAccount.getScene().getWindow();
		if (stage.isShowing()) {

			txtFromAccount.setText(accountEvent.getAccountName());
	
		}

	}
	
	
	@Subscribe
	public void popuplistner(AccountPopupEvent accountPopupEvent) {

		System.out.println("------AccountEvent-------toAccount-------------");
		Stage stage = (Stage) txtToAccount.getScene().getWindow();
		if (stage.isShowing()) {

			txtToAccount.setText(accountPopupEvent.getAccountName());

		}

	}
	

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}

}
