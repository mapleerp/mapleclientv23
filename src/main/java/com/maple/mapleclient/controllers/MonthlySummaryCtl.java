package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;



import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class MonthlySummaryCtl {
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpStartDate;

    @FXML
    private DatePicker dpEndDate;

    @FXML
    private Button btnGenerateReport;
    
    @FXML
    private ComboBox<String> cmbReportType;
    
    @FXML
    private ComboBox<String> cmbPerson;
    
    
	@FXML
	private void initialize() {
		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
		cmbReportType.getItems().add("Supplier Monthly Summary");
		cmbReportType.getItems().add("Supplier Ledger Report");
		cmbReportType.getItems().add("Customer Ledger Report");
		cmbReportType.getItems().add("Supplier BillWise By Due Days");
		cmbReportType.getItems().add("Customer BillWise By Due Days");
		cmbReportType.getItems().add("CustomerWise Report");
		cmbReportType.getItems().add("Damage Entry Report");
	}
    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	 generateReport();

    }

    private void generateReport() {
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(5, "please select start date", false);
    		
    	} else if (null == dpEndDate) {
    		
    		notifyMessage(5, "please select end date", false);
    		
//		} else if (null == cmbPerson.getValue()) {
//			
//			notifyMessage(5, "please select supplier/customer", false);
//			
		}else {
			
			String endDate = "";
			Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
			String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
			
			if(null != dpEndDate.getValue())
			{
			Date edate = SystemSetting.localToUtilDate(dpEndDate.getValue());
			endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
			}
			
			
			if(cmbReportType.getValue().equals("Supplier Monthly Summary"))
			{
				
				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(cmbPerson.getValue());
				
				try {
					JasperPdfReportService.MonthlySummaryReport(strtDate,endDate,accountHeads.getBody().getId());
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} else if (cmbReportType.getValue().equals("Supplier Ledger Report")) {
				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(cmbPerson.getValue());
				System.out.println(strtDate);
				System.out.println(accountHeads.getBody().getId());
;
				/*
				 * try {
				 * 
				 * JasperPdfReportService.SupplierLedgerReport(strtDate,supplier.getBody().getId
				 * ()); } catch (JRException e) {
				 * 
				 * System.out.println(e); // TODO Auto-generated catch block
				 * e.printStackTrace(); }
				 */
			}else if (cmbReportType.getValue().equals("Customer Ledger Report")) {

				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(cmbPerson.getValue());
					try {
						JasperPdfReportService.CustomerLedgerReport(strtDate,endDate,accountHeads.getBody().getId());
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}else if (cmbReportType.getValue().equals("Supplier BillWise By Due Days")) {

				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(cmbPerson.getValue());
				try {
					JasperPdfReportService.SupplierBillWiseReport(strtDate,endDate,accountHeads.getBody().getId());
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			
		}else if (cmbReportType.getValue().equals("Customer BillWise By Due Days")) {
			ResponseEntity<AccountHeads> customer = RestCaller.getAccountHeadsByName(cmbPerson.getValue());

			try {
				JasperPdfReportService.CustomerBillWiseReport(strtDate,endDate,customer.getBody().getId());
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
		else if (cmbReportType.getValue().equals("Damage Entry Report")) {
			//ResponseEntity<DamageEntryReport> damage = RestCaller.getDamageEntries(cmbPerson.getValue());

			try {
				JasperPdfReportService.DamageEntriesReport(strtDate,endDate,SystemSetting.systemBranch);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
		}
		}
	
    @FXML
    void focusButton(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		if(null != dpEndDate.getValue())
    		{
    			generateReport();
    		}
		}

    }

    @FXML
    void focusEndDate(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		dpEndDate.requestFocus();
		}

    }
    
    @FXML
    void setValuesToCmbPerson(MouseEvent event) {
    	
    	if(cmbReportType.getSelectionModel().getSelectedItem().equals("Supplier Monthly Summary") ||
    			cmbReportType.getSelectionModel().getSelectedItem().equals("Supplier Ledger Report") ||
    			cmbReportType.getSelectionModel().getSelectedItem().equals("Supplier BillWise By Due Days"))
    		
		{
    		setSupplier();
		} 
    	if(cmbReportType.getSelectionModel().getSelectedItem().equals("Customer Ledger Report") ||
    			cmbReportType.getSelectionModel().getSelectedItem().equals("Customer BillWise By Due Days") ||
    			cmbReportType.getSelectionModel().getSelectedItem().equals("CustomerWise Report"))
    	{
    		setCustomer();
    	}

    }
    
    private void setSupplier() {
    	
		ArrayList suppliers = new ArrayList();
		suppliers = RestCaller.SearchSupplierByName();
		Iterator itr = suppliers.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object supName = lm.get("supplierName");
			Object SupId = lm.get("id");
			if (null != SupId) {
				cmbPerson.getItems().add((String)supName);
			}

		}
		
	}
    private void setCustomer() {
    	
    	ArrayList suppliers = new ArrayList();
		suppliers = RestCaller.SearchCustomerByName();
		Iterator itr = suppliers.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object customerName = lm.get("customerName");
			Object id = lm.get("id");
			if (null != id) {
				cmbPerson.getItems().add((String)customerName);
			}

		}
		
	}
    
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }

}
