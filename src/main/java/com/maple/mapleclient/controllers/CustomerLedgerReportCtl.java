package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.Date;

import org.controlsfx.control.Notifications;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class CustomerLedgerReportCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();
	String custId = "";
	@FXML
	private DatePicker dpStartDate;

	@FXML
	private DatePicker dpEndDate;

	@FXML
	private Button btnGenerateReport;

	@FXML
	private TextField txtCustomer;

	@FXML
	void CustomerPopUp(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();

		}
	}

	@FXML
	void GenerateReport(ActionEvent event) {

		if (null == dpStartDate.getValue()) {
			notifyMessage(5, "Please select start date", false);
			return;
		} else if (null == dpEndDate.getValue()) {
			notifyMessage(5, "Please select end date", false);
			return;
		} else {

			generateReport();
		}

	}

	private void generateReport() {

		String endDate = "";
		Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpEndDate.getValue());
		endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");

		try {
			JasperPdfReportService.CustomerLedgerReport(strtDate, endDate, custId);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void GenerateReportOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			generateReport();
		}
	}

	@FXML
	void focusTxtXustomer(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCustomer.requestFocus();

		}
	}

	@FXML
	void focusEndDate(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpEndDate.requestFocus();

		}
	}

	@FXML
	private void initialize() {
		dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		System.out.println("sssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		eventBus.register(this);
	}

	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			btnGenerateReport.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnGenerateReport.getScene().getWindow();
		if (stage.isShowing()) {

			txtCustomer.setText(customerEvent.getCustomerName());
			custId = customerEvent.getCustId();

		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
