package com.maple.mapleclient.controllers;

import java.util.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.input.KeyEvent;
import net.sf.jasperreports.engine.JRException;

public class SalesModeWiseReportCtl {
	
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpDate;
    @FXML
   	private void initialize() {
   		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
       }
    @FXML
    private Button btnGenerateReport;

    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null != dpDate.getValue()) {
			Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
			String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
			try {
				JasperPdfReportService.SalesModeWiseSummary(date);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
    }

    @FXML
    void focusEndDate(KeyEvent event) {

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }

}