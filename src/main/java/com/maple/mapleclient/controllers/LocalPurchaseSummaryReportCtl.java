package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;

import com.maple.report.entity.LocalPurchaseSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class LocalPurchaseSummaryReportCtl {
	
	private ObservableList<LocalPurchaseSummaryReport> localPurchaseSummaryReportList = FXCollections.observableArrayList();
	
	   @FXML
	    private DatePicker dpfromDate;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private Button btnShow;

	    @FXML
	    private Button btnGenerateReport;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private TableView<LocalPurchaseSummaryReport> tblReport;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, String> clDate;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, String> clInvno;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, String> clSupplier;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, String> cltin;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, String> clSupinvo;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, String> clInDate;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, String> clPoNo;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, Number> clNetInvc;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, Number> clBgst;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, Number> clGst;

	    @FXML
	    private TableColumn<LocalPurchaseSummaryReport, String> clUser;

	    @FXML
	    void Clear(ActionEvent event) {
	    	
	    	dpfromDate.setValue(null);
	    	dpToDate.setValue(null);
	    	tblReport.getItems().clear();

	    }

	    @FXML
	    void Report(ActionEvent event) {
	    	
	    	if(null==dpfromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    		
	    	}
	    	if (null==dpToDate.getValue()) {
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    		
	    	}
	    	
	    	Date fdate = SystemSetting.localToUtilDate(dpfromDate.getValue());
			String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

			Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String edate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
			
			try {
				JasperPdfReportService.localPurchaseSummaryDtlReport(sdate, edate);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
	    	

	   }

	    @FXML
	    void Show(ActionEvent event) {
	    	
	    	Date fdate = SystemSetting.localToUtilDate(dpfromDate.getValue());
			String fromdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

			Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String todate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
	    	
	    	ResponseEntity<List<LocalPurchaseSummaryReport>> localPurchaseSummaryReport=RestCaller.getlocalPurchaseSummaryDtlReport(fromdate,todate);
	    			localPurchaseSummaryReportList = FXCollections.observableArrayList(localPurchaseSummaryReport.getBody());
	    	fillTable();

	    }
	    
	    
	    private void fillTable() {
		
	    	tblReport.setItems(localPurchaseSummaryReportList);
	    	clDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
	    	clInvno.setCellValueFactory(cellData -> cellData.getValue().getInvoiceNumberProperty());
	    	clSupplier.setCellValueFactory(cellData -> cellData.getValue().getSupplierProperty());
	    	cltin.setCellValueFactory(cellData -> cellData.getValue().getTinNoProperty());
	    	clSupinvo.setCellValueFactory(cellData -> cellData.getValue().getSupplierInvoiceNumberProperty());
	    	clInDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
	    	clPoNo.setCellValueFactory(cellData -> cellData.getValue().getpONumberProperty());
	    	clNetInvc.setCellValueFactory(cellData -> cellData.getValue().getNetInvoiceProperty());
	    	clBgst.setCellValueFactory(cellData -> cellData.getValue().getBeforeGstProperty());
	    	clGst.setCellValueFactory(cellData -> cellData.getValue().getGstProperty());
	    	clUser.setCellValueFactory(cellData -> cellData.getValue().getUserProperty());
	    	
			
	    }

		public void notifyMessage(int duration, String msg, boolean success) {

	 			Image img;
	 			if (success) {
	 				img = new Image("done.png");

	 			} else {
	 				img = new Image("failed.png");
	 			}

	 			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
	 					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
	 					.onAction(new EventHandler<ActionEvent>() {
	 						@Override
	 						public void handle(ActionEvent event) {
	 							System.out.println("clicked on notification");
	 						}
	 					});
	 			notificationBuilder.darkStyle();
	 			notificationBuilder.show();

	 	}
		


}
