package com.maple.mapleclient.controllers;


import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.RetailSalesDetailReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class RetailSalesDetailReportCtl {

	
	private ObservableList<RetailSalesDetailReport> retailSalesDetailReportList = FXCollections.observableArrayList();
	
	@FXML
    private DatePicker dpFrom;

    @FXML
    private DatePicker dpTo;

    @FXML
    private Button btnGenerate;

    @FXML
    private Button btnPrint;

    @FXML
    private TableView<RetailSalesDetailReport> tblRetailSalesDetails;

    @FXML
    private TableColumn<RetailSalesDetailReport, String> clmnInvoice;

    @FXML
    private TableColumn<RetailSalesDetailReport, String> clmnVoucher;

    @FXML
    private TableColumn<RetailSalesDetailReport, String> clmnCustomer;

    @FXML
    private TableColumn<RetailSalesDetailReport, String> clmnPatient;

    @FXML
    private TableColumn<RetailSalesDetailReport, String> clmnItem;

    @FXML
    private TableColumn<RetailSalesDetailReport, String> clmnGroup;

    @FXML
    private TableColumn<RetailSalesDetailReport, String> clmnExpiry;

    @FXML
    private TableColumn<RetailSalesDetailReport, String> clmnBatch;

    @FXML
    private TableColumn<RetailSalesDetailReport, Number> clmnQuantity;

    @FXML
    private TableColumn<RetailSalesDetailReport, Number> clmnRate;

    @FXML
    private TableColumn<RetailSalesDetailReport, Number> clmnTax;

    @FXML
    private TableColumn<RetailSalesDetailReport, Number> clmnSaleValue;

    @FXML
    private TableColumn<RetailSalesDetailReport, Number> clmnCost;

    @FXML
    private TableColumn<RetailSalesDetailReport, Number> clmnCostValue;

    EventBus eventBus = EventBusFactory.getEventBus();
    
    @FXML
	private void initialize() {
//    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
//    	dpFromDate = SystemSetting.datePickerFormat(dpFrom, "dd/MMM/yyyy");
    	eventBus.register(this);
    	
    	
    	
    	
    }
    
    @FXML
    void generateReport(ActionEvent event) {
    	
    	Date fdate = SystemSetting.localToUtilDate(dpFrom.getValue());
    	String fromDate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
    	
    	Date tdate = SystemSetting.localToUtilDate(dpTo.getValue());
    	String toDate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");

    	ResponseEntity<List<RetailSalesDetailReport>> retailSalesDetailReport1 = RestCaller.getRetailSalesDetailReport(fromDate,toDate);
    	retailSalesDetailReportList = FXCollections.observableArrayList(retailSalesDetailReport1.getBody());
		
		
		
		if (retailSalesDetailReportList.size() > 0) {
			
			
			fillTable();
			dpFrom.getEditor().clear();;
			dpTo.getEditor().clear();
			
		}
    
    
    }

    private void fillTable() {
    	tblRetailSalesDetails.setItems(retailSalesDetailReportList);
		clmnInvoice.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
		clmnVoucher.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumProperty());
		clmnCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clmnPatient.setCellValueFactory(cellData -> cellData.getValue().getPatientNameProperty());
		clmnItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clmnGroup.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
		clmnExpiry.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clmnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clmnQuantity.setCellValueFactory(cellData -> cellData.getValue().getQuantityProperty());
		clmnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clmnTax.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clmnSaleValue.setCellValueFactory(cellData -> cellData.getValue().getSaleValueProperty());
		clmnCost.setCellValueFactory(cellData -> cellData.getValue().getCostProperty());
		clmnCostValue.setCellValueFactory(cellData -> cellData.getValue().getCostValueProperty());
		
	}

	@FXML
    void printReport(ActionEvent event) {

		Date fdate = SystemSetting.localToUtilDate(dpFrom.getValue());
		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		Date tdate = SystemSetting.localToUtilDate(dpTo.getValue());
		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		

		  try { JasperPdfReportService.retailSalesDetailREport(sfdate,
				  stdate); } catch (JRException e) { // TODO Auto-gesnerated catch block
		  e.printStackTrace(); }
    }
}
