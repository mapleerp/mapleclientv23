package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.report.entity.AccountBalanceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class IncomeAndExpenceReportCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<AccountBalanceReport> accountBalanceList = FXCollections.observableArrayList();


    @FXML
    private Button btnGenarateReport;

    @FXML
    private DatePicker dpDate;
    @FXML
 	private void initialize() {
     	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
     	
  }

    @FXML
    void GenarateReport(ActionEvent event) {
    	
    	if(null == dpDate.getValue())
    	{
    		notifyMessage(3, "Please select Date", false);
    		dpDate.requestFocus();
    		return;
    	}
    	
    	Date fdate = SystemSetting.localToUtilDate(dpDate.getValue());
		String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		

		try {
			JasperPdfReportService.IncomeAndExpenceReport(sdate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
