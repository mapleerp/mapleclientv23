package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.ExportTaxReport;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.TaxReport;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;

public class TaxReportCtl {
	
	String taskid;
	String processInstanceId;
	
	ExportTaxReport exportTaxReport=new ExportTaxReport();

	private ObservableList<TaxReport> taxReportList = FXCollections.observableArrayList();

	List<TaxReport> salesTaxReportList=new ArrayList<TaxReport>();
	
	List<TaxReport> purchaseTaxList=new ArrayList<TaxReport>();
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private Button btnShowReport;
    @FXML
    private Button btnExpotToExcel;
    

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TableView<TaxReport> tblPurchasTaxReport;

    @FXML
    private TableColumn<TaxReport, Number> clumTaxRate;

    @FXML
    private TableColumn<TaxReport, Number> clumnSgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnCgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnIgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnCessAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnTotalTaxAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnAssesableAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnNetAmount;

    @FXML
    private TableView<TaxReport> tblSalesTaxReport;

    @FXML
    private TableColumn<TaxReport, Number> clumSalesTaxRate;

    @FXML
    private TableColumn<TaxReport, Number> clumnSalesSgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnSalesCgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnSalesIgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnSalesCessAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnSalesTotalTaxAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnSlalesAssesableAmnt;

    @FXML
    private TableColumn<TaxReport, Number> clmnSaleNetAmount;

    @FXML
    private TableView<TaxReport> tblFinalTaxReport;

    @FXML
    private TableColumn<TaxReport, Number> clumFinalTaxRate;

    @FXML
    private TableColumn<TaxReport, Number> clumnFinalSgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnFinalCgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnFinalIgstAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnFinalCessAmount;

    @FXML
    private TableColumn<TaxReport, Number> clumnFinalTotalAmount;
    
    

    @FXML
    private TextField txtPurcahseSgstTotal;

    @FXML
    private TextField txtPurchaseCgstTotal;

    @FXML
    private TextField txtPurchaseIgstTotal;

    @FXML
    private TextField txtPurchaseCssAmtTotal;

    @FXML
    private TextField txtPurchaseTotaTaxAmt;

    @FXML
    private TextField txtPurchaseTotalAssesableAmt;

    @FXML
    private TextField txtPurchaseNetAmnt;

    @FXML
    private TextField txtSalesSgstAmntTotal;

    @FXML
    private TextField txtSalesCgstTotalAmnt;

    @FXML
    private TextField txtSalesIgstAmntTotal;

    @FXML
    private TextField txtSalesCessAmntTotal;

    @FXML
    private TextField txtSalesTotalTaxAmnt;

    @FXML
    private TextField txtSalesAssesablAmntTotal;

    @FXML
    private TextField txtSalesNetAmntTotal;


    @FXML
    void ShowReport(ActionEvent event) {

    	if(null==dpFromDate.getValue()||null==dpToDate.getValue()) {
			return ;
		}
    	java.util.Date fromDate =Date.valueOf(dpFromDate.getValue());
      	java.util.Date toDate =Date.valueOf(dpToDate.getValue());
    	String stringFromDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
    	String stringToDate=SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
    	
    	 
    	ResponseEntity<List<TaxReport>> taxReport=RestCaller.taxReport(stringFromDate,stringToDate,SystemSetting.systemBranch);
	    salesTaxReportList=taxReport.getBody();
    	taxReportList = FXCollections.observableArrayList(taxReport.getBody());
		
		
		
		tblSalesTaxReport.setItems(taxReportList);
		
		ResponseEntity<List<TaxReport>> purchaseTaxReport=RestCaller.purchaseTaxReport(stringFromDate,stringToDate,SystemSetting.systemBranch);
		taxReportList=FXCollections.observableArrayList(purchaseTaxReport.getBody());
		 purchaseTaxList=purchaseTaxReport.getBody();
		tblPurchasTaxReport.setItems(taxReportList);
		

		FillTable();
	 
		Double salesSgstAmnt=0.0;
		Double salesCgstAmntTotal=0.0;
		Double cessAmntTotal=0.0;
		Double salesTaxTotal=0.0;
		Double salesIgstAmtTotal=0.0;
		Double salesAssesableAmntTotal=0.0;
		Double salesTaxNetAmntTotal=0.0;
    	for(int i=0;i<salesTaxReportList.size();i++) {
    		Double	 salesTotalSgstAmnt=+salesTaxReportList.get(i).getSalesSgstAmount();
    		salesSgstAmnt =salesTotalSgstAmnt+salesSgstAmnt;
    		Double cgstAmount=+salesTaxReportList.get(i).getSalesSgstAmount();
    		salesCgstAmntTotal=cgstAmount+salesCgstAmntTotal;
    		Double cessAmnt=+salesTaxReportList.get(i).getSalesCessAmount();
    		cessAmntTotal=cessAmnt+cessAmntTotal;
    		
    		Double salesTaxAmnt=+salesTaxReportList.get(i).getTotalSaleTaxAmount();
    		
    	
    		salesTaxTotal=salesTaxAmnt+salesTaxAmnt;

    	
    		Double salesIgstAmntTotal=+salesTaxReportList.get(i).getSalesIgstAmount();
    		salesIgstAmtTotal=salesIgstAmntTotal+salesIgstAmtTotal;
    		Double assableAmnt=+salesTaxReportList.get(i).getSalesTaxAssesableAmount();
    		salesAssesableAmntTotal=assableAmnt+salesAssesableAmntTotal;
    		Double salesNetAmnt=+salesTaxReportList.get(i).getNetSalesAmount();
    		salesTaxNetAmntTotal=salesNetAmnt+salesTaxNetAmntTotal;
    	}
    	txtSalesSgstAmntTotal.setText(String.valueOf(Math.round(salesSgstAmnt)));
    	txtSalesCgstTotalAmnt.setText(String.valueOf(Math.round(salesCgstAmntTotal)));
    	txtSalesCessAmntTotal.setText(String.valueOf(Math.round(cessAmntTotal)));
    	txtSalesTotalTaxAmnt.setText(String.valueOf(Math.round(salesCgstAmntTotal+salesSgstAmnt+salesIgstAmtTotal)));
    	
    	
    	txtSalesIgstAmntTotal.setText(String.valueOf(Math.round(salesIgstAmtTotal)));
    	txtSalesAssesablAmntTotal.setText(String.valueOf(Math.round(salesAssesableAmntTotal)));
    	txtSalesNetAmntTotal.setText(String.valueOf(Math.round(salesTaxNetAmntTotal)));
    
        Double purchaseSgstAmntTotal=0.0;
    	Double purachaseCgstAmntTotal=0.0;
    	Double purchaseTotalAssableAmnt=0.0;
    	Double netPurchaseAmountTotal=0.0;
    	Double purchaseIgatTotalAmnt=0.0;
    	Double purchaseCessAmntTotal=0.0;
    	for(int j=0;j<purchaseTaxList.size();j++){
    		
    		
    		Double purchaseSgstAmnt=+purchaseTaxList.get(j).getPurchaseSgstAmount();
    		purchaseSgstAmntTotal=purchaseSgstAmnt+purchaseSgstAmntTotal;
    	Double purchaseCgstAmnt=purchaseTaxList.get(j).getPurchaseCgstAmount();
    	purachaseCgstAmntTotal=purchaseCgstAmnt+purachaseCgstAmntTotal;
    	Double purchaseAssasableAmnt=purchaseTaxList.get(j).getPurchaseTaxAssesableAmount();
    	purchaseTotalAssableAmnt=purchaseAssasableAmnt+purchaseTotalAssableAmnt;
    	if(null==purchaseTaxList.get(j).getPurchaseCessAmount()) {
    		purchaseCessAmntTotal=0.0;
    	}else {
    		Double purchaseCessAmnt=+purchaseTaxList.get(j).getPurchaseCessAmount();
    		purchaseCessAmntTotal=purchaseCessAmntTotal+purchaseCessAmntTotal;
    	}
    
    	
    	if(null==purchaseTaxList.get(j).getPurchaseIgstAmount()) {
    		purchaseIgatTotalAmnt=0.0;
    	}else {
    	Double purchaseIgstAmnt=+purchaseTaxList.get(j).getPurchaseIgstAmount();
    	
    	purchaseIgatTotalAmnt=purchaseIgstAmnt+purchaseIgatTotalAmnt;
    	}
    	Double purchaseAmount=purchaseTaxList.get(j).getNetPurchaseAmount();
    	netPurchaseAmountTotal=purchaseAmount+netPurchaseAmountTotal;
    	
    	}
    	
    	txtPurcahseSgstTotal.setText(String.valueOf(Math.round(purchaseSgstAmntTotal)));
    	txtPurchaseCgstTotal.setText(String.valueOf(Math.round(purachaseCgstAmntTotal)));
    	txtPurchaseTotaTaxAmt.setText(String.valueOf(Math.round(purchaseSgstAmntTotal+purachaseCgstAmntTotal)));
    	txtPurchaseTotalAssesableAmt.setText(String.valueOf(Math.round(purchaseTotalAssableAmnt)));
    	txtPurchaseNetAmnt.setText(String.valueOf(Math.round(netPurchaseAmountTotal)));
    	txtPurchaseIgstTotal.setText(String.valueOf(Math.round(purchaseIgatTotalAmnt)));
     	txtPurchaseCssAmtTotal.setText(String.valueOf(Math.round(purchaseCessAmntTotal)));
    }

    
    @FXML
   	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblFinalTaxReport.setVisible(false);
   	}
    
    

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

	}
	private void FillTable()
	{
		// sales tax table		
		clumSalesTaxRate.setCellValueFactory(cellData -> cellData.getValue().getSalesTaxRateProperty());
		clumnSalesSgstAmount.setCellValueFactory(cellData -> cellData.getValue().getSalesSgstAmountProperty());
		clumnSalesCgstAmount.setCellValueFactory(cellData -> cellData.getValue().getSalesCgstAmountProperty());
		clumnSalesIgstAmount.setCellValueFactory(cellData -> cellData.getValue().getSalesIgstAmountProperty());
		clumnSalesCessAmount.setCellValueFactory(cellData -> cellData.getValue().getSalesCessAmountProperty());
		clumnSalesTotalTaxAmount.setCellValueFactory(cellData -> cellData.getValue().getTotalSaleTaxAmountProperty());
		clumnSlalesAssesableAmnt.setCellValueFactory(cellData -> cellData.getValue().getSalesTaxAssesableAmountProperty());
		clmnSaleNetAmount.setCellValueFactory(cellData -> cellData.getValue().getNetSalesAmountProperty());
	// purchas tax table
		clumTaxRate.setCellValueFactory(cellData -> cellData.getValue().getPurchaseTaxRateProperty());
	
		clumnSgstAmount.setCellValueFactory(cellData -> cellData.getValue().getPurchaseSgstAmountProperty());
	
		clumnCgstAmount.setCellValueFactory(cellData -> cellData.getValue().getPurchaseCgstAmountProperty());
		clumnIgstAmount.setCellValueFactory(cellData -> cellData.getValue().getPurchaseIgstAmountProperty());
		clumnCessAmount.setCellValueFactory(cellData -> cellData.getValue().getPurchaseCessAmountProperty());
		clumnTotalTaxAmount.setCellValueFactory(cellData -> cellData.getValue().getTotalPurchaseTaxAmountProperty());
		clumnAssesableAmount.setCellValueFactory(cellData -> cellData.getValue().getPurchaseTaxAssesableAmountProperty());
		clumnNetAmount.setCellValueFactory(cellData -> cellData.getValue().getNetPurchaseAmountProperty());
		
	}
    

    @FXML
    void printReport(ActionEvent event) {

    }
    
	@FXML
	void excepotToExcel(ActionEvent event) {
		if(null==dpFromDate.getValue()||null==dpToDate.getValue()) {
			return ;
		}

    	java.util.Date fromDate =Date.valueOf(dpFromDate.getValue());
      	java.util.Date toDate =Date.valueOf(dpToDate.getValue());
    	String stringFromDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
    	String stringToDate=SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
    	
    	
    	if(salesTaxReportList.size()>0) {

		
 
		exportTaxReport.exportToExcel("TaxReport"+fromDate+toDate+".xls", salesTaxReportList,purchaseTaxList);
    	}
    	else {
    		return ;
    	}
		
	}
	
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}
}
