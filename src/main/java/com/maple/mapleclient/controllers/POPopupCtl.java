package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.PurchaseOrderHdr;
import com.maple.mapleclient.events.PoNumberEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class POPopupCtl {
	
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PurchaseOrderHdr> purchaseList = FXCollections.observableArrayList();
	PoNumberEvent poNumberEvent;
	PurchaseOrderHdr purchaseOrderHdr;
	StringProperty SearchString = new SimpleStringProperty();
    @FXML
    private TableView<PurchaseOrderHdr> tblPONumber;

    @FXML
    private TableColumn<PurchaseOrderHdr, String> clPONumber;

    @FXML
    private TableColumn<PurchaseOrderHdr, String> clSupplierName;



    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
    @FXML
    private void initialize() {
    	eventBus.register(this);
   
	 poNumberEvent = new PoNumberEvent();
	 	LoadPoNumberBySearch("");
	 tblPONumber.setItems(purchaseList);
	 
	
 	clPONumber.setCellValueFactory(cellData -> cellData.getValue().getPoNumberProperty());
 	clSupplierName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
 	 System.out.print(purchaseList.size()+"purchase list size issssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
 	tblPONumber.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			    //if (newSelection != null) {
			    	//if(null!=newSelection.getSupplierId()&& newSelection.getpONum().length()>0) {
			    		poNumberEvent.setPoNumber(newSelection.getpONum());
			    
 	
 		if (newSelection != null) {
			    	if(null!=newSelection.getSupplierId()&& newSelection.getpONum().length()>0) {
			    		poNumberEvent.setPoNumber(newSelection.getpONum());
			    		poNumberEvent.setSupplierName(newSelection.getSupplierName());
			    		poNumberEvent.setSupId(newSelection.getSupplierId());
			    		poNumberEvent.setSupGst(newSelection.getSupplierInvNo());
			    		
			    		System.out.print(poNumberEvent.getPoNumber()+"po number isssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
			    		System.out.print("inside the ponumber event________________");
			    		eventBus.post(poNumberEvent);
			    		
			    		
			    		System.out.print("inside the new selection isssssssssssssssssssssssssssssssssssssssssssssssssss");
			    	}
			   }
			});
 	
 	 
	 SearchString.addListener(new ChangeListener(){
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					System.out.print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
					LoadPoNumberBySearch((String)newValue);
					
				}
	        });
	 
    }
    @FXML
    void actionCancel(ActionEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }
    
    @FXML
    void txtKeyPress(KeyEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tblPONumber.requestFocus();
    		tblPONumber.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			stage.close();
		}
    }

    @FXML
    void actionOk(ActionEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	
		stage.close();
    }
    
    public void LoadPoNumberBySearch(String search)
    {
    	
    	ArrayList poList = new ArrayList();
    	purchaseList.clear();
    	poList = RestCaller.SearchPoNumber(search);
    	String supName;
    	String poNumber;
    	String supGst;
    	String supId;
    	Iterator itr = poList.iterator();
    	while (itr.hasNext()) {
			
			List element = (List) itr.next();
			poNumber = (String) element.get(1);
	
			  supName = (String) element.get(0);
			  supGst = (String) element.get(2);
			  supId = (String) element.get(3);
				System.out.print(supName+"supplier name isssssssssssssssssssssssssssssssssssssssss");
			 if (null != supId) {
					System.out.println("accaccaccacc44");
					 purchaseOrderHdr = new PurchaseOrderHdr();
					purchaseOrderHdr.setSupplierName(supName);
					purchaseOrderHdr.setpONum(poNumber);
					
					System.out.print(purchaseOrderHdr.getSupplierName()+purchaseOrderHdr.getpONum()+"suppliernmae and po number are");
					
					System.out.print(purchaseOrderHdr.getSupplierName()+"supplier name issssssssssssssssssssssssssss");
					
					
					
					System.out.print(purchaseOrderHdr);
					purchaseOrderHdr.setpONum(poNumber);
					System.out.print(purchaseOrderHdr.getpONum()+"po number issssssssssssssssssssssssssss");
					
					
					
					
					purchaseOrderHdr.setSupplierInvNo(supGst);
					purchaseOrderHdr.setSupplierId(supId);
					purchaseList.add(purchaseOrderHdr);
					
					System.out.print(purchaseOrderHdr+"purchase order  isssssss");
		}
    }
		return;
    	
    	
    	
    	
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
