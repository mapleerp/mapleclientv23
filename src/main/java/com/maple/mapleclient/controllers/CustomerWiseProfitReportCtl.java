package com.maple.mapleclient.controllers;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.BranchWiseProfitReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class CustomerWiseProfitReportCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<BranchWiseProfitReport> customerProfitList = FXCollections.observableArrayList();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");


    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenarateReport;

    @FXML
    private TableView<BranchWiseProfitReport> tblReport;

    @FXML
    private TableColumn<BranchWiseProfitReport, String> clCustomer;

    @FXML
    private TableColumn<BranchWiseProfitReport, Number> clProfit;

    @FXML
    void BackOnKeyPress(KeyEvent event) {

    }
    @FXML
   	private void initialize() {
       	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
       	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    }
    @FXML
    void GenarateReport(ActionEvent event) {
    	

    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(3, "Please select From Date", false);
    		dpFromDate.requestFocus();
    		return;
    	}
    	

    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(3, "Please select To Date", false);
    		dpToDate.requestFocus();
    		return;
    	}
    	
    	Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String edate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		
		ResponseEntity<List<BranchWiseProfitReport>> customerWiseProfitList = RestCaller.CustomerWiseProfitReport(sdate,edate);
		customerProfitList = FXCollections.observableArrayList(customerWiseProfitList.getBody());
		
    	FillTable();

    }
    
	private void FillTable() {
		tblReport.setItems(customerProfitList);
		
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clProfit.setCellValueFactory(cellData -> cellData.getValue().getProfitProperty());
		
		clProfit.setCellFactory(tc-> new TableCell<BranchWiseProfitReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});


	}

    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }
}
