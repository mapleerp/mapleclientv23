package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.DamageDtl;
import com.maple.mapleclient.entity.DamageHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class DamageEntryCtl {

	String taskid;
	String processInstanceId;

	private ObservableList<DamageDtl> damageListTable = FXCollections.observableArrayList();

	private EventBus eventBus = EventBusFactory.getEventBus();
	ItemMst itemmst = new ItemMst();
	DamageDtl damageDtl = null;
	DamageHdr damageHdr = null;

	@FXML
	private TextField txtItem;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnFinalSubmit;

	@FXML
	private Button btnDelete;

	@FXML
	private TextField txtNarration;
	@FXML
	private TableView<DamageDtl> tblDamage;

	@FXML
	private TableColumn<DamageDtl, String> clItem;

	@FXML
	private TableColumn<DamageDtl, Number> clQty;

	@FXML
	private TableColumn<DamageDtl, String> clReason;

	@FXML
	private TableColumn<DamageDtl, String> clBatch;

	@FXML
	private TableColumn<DamageDtl, String> clNarration;
	@FXML
    private TableColumn<DamageDtl, String> clstore;

	@FXML
	Button btnEditVOucherDate;

	@FXML
	private ComboBox<String> cmbReason;
	@FXML
	private TextField txtstore;
	@FXML
	private void initialize() {

		eventBus.register(this);
		System.out.println("ssssssss=====initialize=====sssssssssss");

		tblDamage.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					damageDtl = new DamageDtl();
					damageDtl.setId(newSelection.getId());

				}
			}

		});

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		cmbReason.getItems().add("Damaged Items");
		cmbReason.getItems().add("Write Off");

	}
	
	@FXML
	void itempopup(MouseEvent event) {

		loadItemPopup();

	}
	
	private void addItem() {

		if (null == txtItem.getText()) {
			notifyMessage(5, " Please enter item!!!");
			return;
		} else if (null == txtQty.getText()) {
			notifyMessage(5, " Please enter quantity...!!!");
			return;
		} else if (null == txtBatch.getText()) {
			notifyMessage(5, " Please enter batch...!!!");
			return;
		} else if (null == txtstore.getText()) {
			notifyMessage(5, " Please enter store...!!!");
			return;
		} else if (null == cmbReason.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please select the reason value");
			return;
		} else {
			ArrayList items = new ArrayList();

			items = RestCaller.getSingleStockItemByName(txtItem.getText(), txtBatch.getText(), txtstore.getText());
			Double chkQty = 0.0;
			String itemId = null;
			String store=null;
			
			Iterator itr = items.iterator();
			while (itr.hasNext()) {
				List element = (List) itr.next();
				chkQty = (Double) element.get(4);
				itemId = (String) element.get(7);
				store =(String)  element.get(10);
				
			}
			if (chkQty < Double.parseDouble(txtQty.getText())) {
				notifyMessage(1, "Not in Stock!!!");
				txtQty.clear();
				txtItem.clear();
				txtItem.requestFocus();
				return;
			}

			if (null == damageHdr) {
				damageHdr = new DamageHdr();
				LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
				Date date = Date.valueOf(ldate);
				damageHdr.setVoucherDate(date);

				damageHdr.setBranchCode(SystemSetting.getSystemBranch());
				ResponseEntity<DamageHdr> respentity = RestCaller.saveDamageHdr(damageHdr);
				damageHdr = respentity.getBody();
				System.out.println("added damage hdr" + damageHdr);
			}
			if (null != damageHdr) {
				if (null != damageHdr.getId()) {
					Double itemsqty = 0.0;
					itemsqty = RestCaller.DamageDtlItemQty(damageHdr.getId(), itemmst.getId(), txtBatch.getText());
					if (chkQty < itemsqty + Double.parseDouble(txtQty.getText())) {
						notifyMessage(1, "Not in Stock!!!");
						txtQty.clear();
						txtItem.clear();
						txtItem.requestFocus();
						return;
					}
					damageDtl = new DamageDtl();

					damageDtl.setDamageHdr(damageHdr);

					damageDtl.setNarration(txtNarration.getText());
					damageDtl.setItemId(itemmst.getId());
					// -----------sharon --------------adding store in damageDtl-------
					if(null!=txtstore.getText()) {
					damageDtl.setStore(txtstore.getText());
					System.out.println("store name------------------"+txtstore.getText());
					}
					damageDtl.setQty(Double.parseDouble(txtQty.getText()));
					if (null != txtBatch) {
						String batch = txtBatch.getText() == null ? "NOBATCH" : txtBatch.getText();
						if (batch.trim().length() > 0) {
							damageDtl.setBatchCode(batch);
						} else
							damageDtl.setBatchCode("NOBATCH");
					} else {
						damageDtl.setBatchCode(txtBatch.getText());
					}

					damageDtl.setReason(cmbReason.getSelectionModel().getSelectedItem());
//					if(!txtBatch.getText().trim().isEmpty())
//					damageDtl.setBatchCode(txtBatch.getText());
//					else
//						damageDtl.setBatchCode("NOBATCH");
					System.out.println("====damageDtl====" + damageDtl.getBatchCode());
					System.out.println("====damageDtl====" + damageDtl.getStore());
					ResponseEntity<DamageDtl> respentity1 = RestCaller.savedamageDtl(damageDtl);
					
					damageDtl = respentity1.getBody();
					System.out.println("====damageDtlgsfgdfg====" + damageDtl);
					damageListTable.add(damageDtl);
					FillTable();
					System.out.println("after table is filled");
					txtNarration.clear();

					txtItem.setText("");
					txtQty.setText("");
					txtBatch.setText("");
					txtstore.setText("");
					txtItem.requestFocus();
				}
			}
		}
	}
	

	private void FillTable() {
		System.out.println("-------------FillTable-" + damageListTable);

		tblDamage.setItems(damageListTable);
		for (DamageDtl damage : damageListTable) {

			if (null != damage.getItemId()) {
				ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(damage.getItemId());
				damage.setItemName(respentity.getBody().getItemName());
				clItem.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());

			}
			clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
			clNarration.setCellValueFactory(cellData -> cellData.getValue().getnarrationProperty());
			clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
			clstore.setCellValueFactory(cellData -> cellData.getValue().getStoreNameProperty());
			clReason.setCellValueFactory(cellData -> cellData.getValue().getReasonProperty());
		}
		tblDamage.setItems(damageListTable);
		System.out.println("-------------inside fill tableee");
	}
	
	
	
	@FXML
	void Delete(ActionEvent event) {

		if (null != damageDtl.getId()) {
			RestCaller.deleteDamageDtl(damageDtl.getId());
			notifyMessage(5, " Deleted Successfully");
			tblDamage.getItems().clear();
			ResponseEntity<List<DamageDtl>> damagedtlSaved = RestCaller.getDamageDtl(damageHdr);
			damageListTable = FXCollections.observableArrayList(damagedtlSaved.getBody());
			// ==========to fill table again after delete===========//
			if (damageListTable != null) {

				for (DamageDtl dmg : damageListTable) {

					ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(dmg.getItemId());
					dmg.setItemName(respentity.getBody().getItemName());
				}
			}
			tblDamage.setItems(damageListTable);
		} else {
			notifyMessage(5, "Please select the Item!!!");
		}
	}

	

	@FXML
	void qtyOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtNarration.requestFocus();
		}
	}

	@FXML
	void narrationOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnAdd.requestFocus();
		}
	}

	@FXML
	void batchOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

		}
	}

	@FXML
	void addOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			addItem();
		}
	}

	@FXML
	void AddItem(ActionEvent event) {
		addItem();

	}

	@FXML
	void itemOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			loadItemPopup();
		}
	}

	@FXML
	void saveOnKey(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}
	}

	


	@FXML
	void FinalSubmit(ActionEvent event) {

		finalSave();
	}

	private void finalSave() {
		boolean batch = false;
		String stockVerification = RestCaller.StockVerificationByDamageEntry(damageHdr.getId());
		if (null != stockVerification) {
			notifyMessage(5, stockVerification);

			// return;

		} else {
			System.out.println("inside save.........................");

			ResponseEntity<List<DamageDtl>> damagedtlSaved = RestCaller.getDamageDtl(damageHdr);

			if (damagedtlSaved.getBody().size() == 0) {
				return;
			}

			String financialYear = SystemSetting.getFinancialYear();
			String vNo = RestCaller.getVoucherNumber(financialYear + "DMG");
			damageHdr.setVoucheNumber(vNo);
			notifyMessage(5, "Details Saved Successfully!!!!");
			damageListTable = FXCollections.observableArrayList(damagedtlSaved.getBody());
//		for(DamageDtl damage: damageListTable)
//		{
//			if(!damage.getBatchCode().equalsIgnoreCase("NOBATCH"))
//			{
//				batch = true;
//				break;
//			}
//		}
//		if(!batch)
//		{
//		RestCaller.updateDamageHdr(damageHdr);
//		}
//		else
//		{
			RestCaller.updateDamageHdrBatch(damageHdr);
//		}
			damageHdr = null;
			batch = false;
			damageListTable.clear();
			tblDamage.getItems().clear();
			txtItem.setText("");
			txtQty.setText("");
		}
	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		System.out.println("-----------item------------"+itemPopupEvent.getStoreName());
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtItem.setText(itemPopupEvent.getItemName());
					itemmst.setId(itemPopupEvent.getItemId());
					txtBatch.setText(itemPopupEvent.getBatch());
					txtstore.setText(itemPopupEvent.getStoreName());
				}

			});
			txtQty.requestFocus();
			// ===to automaticaly focus on quantity after popup dtls entered//
		}
	}

	

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void loadItemPopup() {
		/*
		 * Function to display popup window and show list of items to select.
		 */
		System.out.println("ssssssss=====loadItemPopup=====sssssssssss");

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
