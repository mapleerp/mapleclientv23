package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;

import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPopUp;
import com.maple.mapleclient.entity.UserMst;

import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.UserPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class UserPopupCtl {
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	boolean initializedCalled = false;
	UserPopupEvent userPopupEvent;
	

	private ObservableList<UserMst> popUpUserList = FXCollections.observableArrayList();

	
	StringProperty SearchString = new SimpleStringProperty();

    @FXML
    private TextField txtname;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<UserMst> tablePopupView;

    @FXML
    private TableColumn<UserMst, String> clUserName;

    @FXML
    private TableColumn<UserMst, String> clBranch;
    
    @FXML
    private void initialize()
    {
    	
		userPopupEvent = new UserPopupEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);

		btnCancel.setCache(true);

		txtname.textProperty().bindBidirectional(SearchString);
		
		LoadUserPopupBySearch("");

		tablePopupView.setItems(popUpUserList);
		
		clUserName.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
		clBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
		
		

		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getUserName() && newSelection.getUserName().length() > 0) {
					
					userPopupEvent.setUserName(newSelection.getUserName());
					userPopupEvent.setId(newSelection.getId());
					userPopupEvent.setBranchCode(newSelection.getBranchCode());
					
//					eventBus.post(userPopupEvent);
				}
			}
		});
    }

    private void LoadUserPopupBySearch(String searchData) {


		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
    	popUpUserList.clear();
		ArrayList userList = new ArrayList();
		UserMst userMst = new UserMst();
		RestTemplate restTemplate = new RestTemplate();

		userList = RestCaller.SearchUserByName(searchData);

		Iterator itr = userList.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			
			Object userName = lm.get("userName");
			Object branch = lm.get("branchCode");
			
			Object id = lm.get("id");
			if (null != id) {
				userMst = new UserMst();
				userMst.setUserName((String) userName);
				userMst.setBranchCode((String) branch);

				userMst.setId((String) id);
				
				System.out.println(lm);

				popUpUserList.add(userMst);
			}

		}
		
		

		return;

	}

	@FXML
    private Button btnCancel;

    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			eventBus.post(userPopupEvent);
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP
				|| event.getCode() == KeyCode.KP_UP) {

		} else {
			txtname.requestFocus();
		}

		initializedCalled = false;
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tablePopupView.requestFocus();
    		tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {

			userPopupEvent.setUserName(null);
			eventBus.post(userPopupEvent);
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
		if (event.getCode() == KeyCode.BACK_SPACE && txtname.getText().length() == 0) {
			userPopupEvent.setUserName(null);
			eventBus.post(userPopupEvent);
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		userPopupEvent.setUserName(null);
		eventBus.post(userPopupEvent);
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		eventBus.post(userPopupEvent);
		stage.close();
    }

}
