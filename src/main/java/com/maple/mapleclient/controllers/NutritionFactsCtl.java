package com.maple.mapleclient.controllers;



import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
public class NutritionFactsCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<NutritionMst> nutritionMstTable = FXCollections.observableArrayList();

	NutritionMst nutritionMst=null;
    @FXML
    private TextField txtNutritionFacts;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShow;

    @FXML
    private TableView<NutritionMst> tblNutritionFacts;

    @FXML
    private TableColumn<NutritionMst, String> clNutritionFacts;

    
    @FXML
	private void initialize() {
    	tblNutritionFacts.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					nutritionMst = new NutritionMst();
					nutritionMst.setId(newSelection.getId());
				}
			}
		});
    }
    @FXML
    void deleteItem(ActionEvent event) {

    	if(null != nutritionMst)
    	{
    		if(null != nutritionMst.getId())
    		{
    			RestCaller.deleteNutritionFacts(nutritionMst.getId());
    			showAll();
    			notifyMessage(5,"deleted!!!");
    		}
    		
    	}
    }


    @FXML
    void showAll(ActionEvent event) {
    	showAll();
    }

    
    @FXML
    void saveNutritionFact(ActionEvent event) {
    	if(txtNutritionFacts.getText().isEmpty())
    		
    	{

    		notifyMessage(5,"Enter Nutrition Facts");
    		txtNutritionFacts.requestFocus();
    		return;
    	}
    	
    	
    	nutritionMst = new NutritionMst();
    	ResponseEntity<NutritionMst> getsalesType = RestCaller.getNutritionByname(txtNutritionFacts.getText());
    	if(null == getsalesType.getBody()) {
    		
    		
    	nutritionMst.setNutrition(txtNutritionFacts.getText());
    	ResponseEntity<NutritionMst> respentity = RestCaller.saveNutritionMst(nutritionMst);
    	nutritionMst = respentity.getBody();
    	nutritionMstTable.add(nutritionMst);
    	fillTable();
    	txtNutritionFacts.clear();
    	
    	nutritionMst = null;
    	notifyMessage(5,"Saved!!!");
    	}
    	else
    	{
    		notifyMessage(5,"Item already Exist!!!");
    		return;
    	}
    	
    }
    
    private void fillTable()
    {
    	tblNutritionFacts.setItems(nutritionMstTable);
    	clNutritionFacts.setCellValueFactory(cellData -> cellData.getValue().getNutritionProperty());
		
    }
    
   
    private void showAll()
    {
    	ResponseEntity<List<NutritionMst>> salesTypeSaved = RestCaller.getNutritionMst();
    	if(null != salesTypeSaved.getBody())
    	{
    		nutritionMstTable = FXCollections.observableArrayList(salesTypeSaved.getBody());
    	fillTable();
    	}
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
