package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.DamageDtl;
import com.maple.mapleclient.entity.ItemBranchDtl;
import com.maple.mapleclient.entity.ItemBranchHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ItemBranchCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<ItemBranchDtl> itemBranchList = FXCollections.observableArrayList();
//	StringProperty SearchString = new SimpleStringProperty();
	private ObservableList<ItemMst> itemList = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();
	ItemBranchHdr itemBranchHdr=null;
	ItemBranchDtl itemBranchDtl = null;
	ItemBranchDtl itemtoDelete = null;
	String itemId = "";
    @FXML
    private TextField txtItemName;

    @FXML
    private ComboBox<String> cmbCategory;

    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    private TableView<ItemBranchDtl> tblItems;

    @FXML
    private TableColumn<ItemBranchDtl, String> clItemName;

    @FXML
    private TableColumn<ItemBranchDtl, String> clBranch;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFinalSubmit;
    @FXML
	private void initialize() {
    	eventBus.register(this);
//    	txtItemName.textProperty().bindBidirectional(SearchString);
    	ArrayList cat = new ArrayList();
		 
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				cmbCategory.getItems().add((String) categoryName);
			}

		}
		ArrayList branch = new ArrayList();
		branch = RestCaller.SearchBranch();
		Iterator itr2 = branch.iterator();
		while (itr2.hasNext()) {
			LinkedHashMap lm1 = (LinkedHashMap) itr2.next();
			Object branchName = lm1.get("branchName");
			Object id = lm1.get("id");
			if (id != null) {
				cmbBranch.getItems().add((String) branchName);
			}

		}
//		SearchString.addListener(new ChangeListener() {
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//				System.out.println("item Search changed");
//				LoadItemBySearch((String) newValue);
//			}
//		});
//		
		tblItems.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					itemtoDelete = new ItemBranchDtl();
					itemtoDelete.setId(newSelection.getId());
					}
			}
		});
    }
    @FXML
    void AddItem(ActionEvent event) {
    	
    	 if(!cmbCategory.getSelectionModel().isEmpty() && !txtItemName.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Type any One");
    		cmbCategory.getSelectionModel().clearSelection();
    		txtItemName.clear();
    		return;
    	}
    	else if(cmbBranch.getSelectionModel().isEmpty())
    	{
    		notifyMessage(5, "Please Select branch");
    		return;
    		
    	}
    	
    	else
    	{
    	if(null==itemBranchHdr)
    	{
    		itemBranchHdr = new ItemBranchHdr();
    		itemBranchHdr.setVoucherDate(Date.valueOf(LocalDate.now()));

    		ResponseEntity<ItemBranchHdr> respentity = RestCaller.saveItemBranchHdr(itemBranchHdr);
    		itemBranchHdr = respentity.getBody();
    		
    	}
    	itemBranchDtl = new ItemBranchDtl();
    	if(cmbCategory.getSelectionModel().isEmpty())
    	{
    		itemBranchDtl.setItemId(itemId);
    		itemBranchDtl.setBranch(cmbBranch.getSelectionModel().getSelectedItem());
    		itemBranchDtl.setItemBranchHdr(itemBranchHdr);
    	
    		ResponseEntity<ItemBranchDtl> respentity = RestCaller.saveItemBranchDtl(itemBranchDtl);
    		itemBranchDtl = respentity.getBody();
    		itemBranchDtl.setItemName(txtItemName.getText());
    		itemBranchList.add(itemBranchDtl);
    		filltable();
    	}
    	else
    	{
    		ResponseEntity<CategoryMst> categorySaved = RestCaller.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem());
    		ResponseEntity<List<ItemMst>> saveditem = RestCaller.getItemsByCategory(categorySaved.getBody());
    		itemList = FXCollections.observableArrayList(saveditem.getBody());
    		for(ItemMst itemmst : itemList)
    		{
    			
    			itemBranchDtl.setItemId(itemmst.getId());
    			itemBranchDtl.setBranch(cmbBranch.getSelectionModel().getSelectedItem());
    	    	itemBranchDtl.setItemBranchHdr(itemBranchHdr);
    	    	ResponseEntity<ItemBranchDtl> respentity = RestCaller.saveItemBranchDtl(itemBranchDtl);
    	    	itemBranchDtl = respentity.getBody();
    	    	itemBranchDtl.setItemName(itemmst.getItemName());
    	    	itemBranchList.add(itemBranchDtl);
    	    	filltable();
    	    	itemBranchDtl = new ItemBranchDtl();
    		}
    	}
    	cmbCategory.setDisable(false);
    	txtItemName.clear();
    	cmbBranch.getSelectionModel().clearSelection();
    	cmbCategory.getSelectionModel().clearSelection();
    }
    }

    @FXML
    void DeleteItem(ActionEvent event) {

    	
    	if(null != itemtoDelete.getId())
    	{
    		RestCaller.deleteItemBranchDeletebyId(itemtoDelete.getId());
    		
    		tblItems.getItems().clear();
    		notifyMessage(5, "Deleted!!!");
    		ResponseEntity<List<ItemBranchDtl>> respentity = RestCaller.getItemBranchDtlByHdrId(itemBranchHdr.getId());
        	
    		itemBranchList = FXCollections.observableArrayList(respentity.getBody());
    		
    		for(ItemBranchDtl itembranch : itemBranchList)
    		{
    			ResponseEntity<ItemMst> rest = RestCaller.getitemMst(itembranch.getItemId());
    			itembranch.setItemName(rest.getBody().getItemName());

    		}
    		tblItems.setItems(itemBranchList);
    		}
    	}
    

    @FXML
    void itemOnEnterClick(MouseEvent event) {

    	showPopup();
  
    }

    @FXML
    void FinalSave(ActionEvent event) {
    	ResponseEntity<List<ItemBranchDtl>> respentity = RestCaller.getItemBranchDtlByHdrId(itemBranchHdr.getId());
		if (respentity.getBody().size() == 0) {
			notifyMessage(5,"Item Not found!!!");
			return;
		}
		String financialYear =SystemSetting.getFinancialYear();
		String sNo = RestCaller.getVoucherNumber(financialYear + "ITEMBRANCH");
		itemBranchHdr.setVoucherNumber(sNo);
		RestCaller.updateItemBranchHdr(itemBranchHdr);
		notifyMessage(5,"Item Details Saved");
		itemBranchHdr = null;
		txtItemName.clear();
		cmbBranch.getSelectionModel().clearSelection();
		cmbCategory.getSelectionModel().clearSelection();
		tblItems.getItems().clear();
    }

    @FXML
    void itemOnEnter(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		showPopup();
    		
		}
    	
    }
	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtItemName.setText(itemPopupEvent.getItemName());
			itemId = itemPopupEvent.getItemId();
		

		}
	}
    private void showPopup()
	{
		System.out.println("-------------ShowItemPopup-------------");

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
    private void filltable()
    {
    	tblItems.setItems(itemBranchList);
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
    	clBranch.setCellValueFactory(cellData -> cellData.getValue().getbranchProperty());
    	
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }
	
}