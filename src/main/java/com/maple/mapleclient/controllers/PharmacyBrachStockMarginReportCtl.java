package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PharmacyBrachStockMarginReport;
import com.maple.report.entity.PharmacyLocalPurchaseDetailsReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class PharmacyBrachStockMarginReportCtl {
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PharmacyBrachStockMarginReport> PharmacyBrachStockMarginReportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpfromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnClear;
    
    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    private TableView<PharmacyBrachStockMarginReport> tblReport;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, String> clgroupName;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, String> clitemName;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, String> clbatchCode;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, String> clitemCode;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, String> clexpiryDate;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, Number> clquantity;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, Number> clrate;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, Number> clamount;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, Number> clpurchaseRate;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, Number> clpurchaseValue;

    @FXML
    private TableColumn<PharmacyBrachStockMarginReport, Number> clmarginValue;

    @FXML
    void Clear(ActionEvent event) {
    	
    	dpfromDate.setValue(null);
    	dpToDate.setValue(null);
    	tblReport.getItems().clear();
    	cmbBranch.setValue(null);


    }

    @FXML
    void Report(ActionEvent event) {
    	
    	if(null==dpfromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
    		return;
    		
    	}
    	if (null==dpToDate.getValue()) {
    		notifyMessage(5, "Please select to date", false);
    		return;
    		
    	}
    	
    	Date fdate = SystemSetting.localToUtilDate(dpfromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String edate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		
		try {
			JasperPdfReportService.PharmacyBranchStockMarginReports(sdate, edate,cmbBranch.getSelectionModel().getSelectedItem());
		} catch (Exception e) {
			e.printStackTrace();
		}
		

    }

    @FXML
    void Show(ActionEvent event) {
    	Date fdate = SystemSetting.localToUtilDate(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String todate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<PharmacyBrachStockMarginReport>> pharmacyBrachStockMarginReport=RestCaller.getPharmacyBranchStockMarginReport(fromdate, todate,cmbBranch.getSelectionModel().getSelectedItem());
    	PharmacyBrachStockMarginReportList = FXCollections.observableArrayList(pharmacyBrachStockMarginReport.getBody());
    	fillTable();

    }
    
	private void fillTable() {
		
		tblReport.setItems(PharmacyBrachStockMarginReportList);
	    
	
		clgroupName.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
		clitemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clbatchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clitemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
		clexpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clquantity.setCellValueFactory(cellData -> cellData.getValue().getQuantityProperty());
		clrate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clamount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clpurchaseRate.setCellValueFactory(cellData -> cellData.getValue().getPurchaseRateProperty());
		clpurchaseValue.setCellValueFactory(cellData -> cellData.getValue().getPurchaseValueProperty());
		clmarginValue.setCellValueFactory(cellData -> cellData.getValue().getMarginValueProperty());
		
	}

	public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

	}
	
	@FXML
	private void initialize() {
		
    	
    	
		 setBranches(); eventBus.register(this);
	//	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		

	}
private void setBranches() {
   		
   		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
   		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
   		branchMstList = branchMstRep.getBody();
   		
   		for(BranchMst branchMst : branchMstList)
   		{
   			cmbBranch.getItems().add(branchMst.getBranchCode());
   		
   			
   		}
   		 
   	}

}
