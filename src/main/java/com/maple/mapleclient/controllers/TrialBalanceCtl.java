package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.math3.ml.neuralnet.UpdateAction;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.ExportStatementOfAccountToExcel;
import com.maple.maple.util.ExportTrialBalanceToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.PurchaseOrderDtl;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.TrialBalanceDtl;
import com.maple.mapleclient.entity.TrialBalanceHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;
import com.maple.report.entity.BranchWiseProfitReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TrialBalanceCtl {
	
	
	String taskid;
	String processInstanceId;
	
	String trialBalanceHdrId;
	
	Date udate = null;
	
	private ObservableList<AccountBalanceReport> accountBalanceList = FXCollections.observableArrayList();
	private ObservableList<TrialBalanceHdr> reportIdDetailList = FXCollections.observableArrayList();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");
	ExportTrialBalanceToExcel exportTrialBalanceToExcell = new ExportTrialBalanceToExcel();
	
	TrialBalanceHdr trialBalanceHdr = null;
	TrialBalanceDtl trialBalanceDtl = null;
	
    @FXML
    private Button btnGenerateReport;
    

    @FXML
    private DatePicker dpFromDate;
    
    @FXML
    private Button btnExportToExcel;

    @FXML
    private Button btnView;
    
    @FXML
    private Button btnReportId;


    @FXML
    private TableView<AccountBalanceReport> tblTrialBalance;

    @FXML
    private TableColumn<AccountBalanceReport, String> clAccount;

    @FXML
    private TableColumn<AccountBalanceReport, Number> clDebitAmount;

    @FXML
    private TableColumn<AccountBalanceReport, Number> clCreditAmount;
    
    @FXML
    private TableView<TrialBalanceHdr> tblReportId;

    @FXML
    private TableColumn<TrialBalanceHdr, String> clmnReport;


    @FXML
    private TextField txtDebitTotal;

    @FXML
    private TextField txtCreditTotal;
    String accountId = null;
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	tblTrialBalance.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getAccountHeads()) {
					
					ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadByName(newSelection.getAccountHeads());
					AccountHeads accountHeads = accountHeadsResp.getBody();
					
					if(null != accountHeads)
					{
						accountId = accountHeads.getId();
					}
				}
			}
		});
    	
    	
    	
    	
    	tblReportId.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getReportId()) {
					
					ResponseEntity<TrialBalanceHdr> trialBalanceHdrDetails = RestCaller.getTrialBalanceHdrByReportId(newSelection.getReportId());
					TrialBalanceHdr trialBalanceHdr = trialBalanceHdrDetails.getBody();
					
					if(null != trialBalanceHdr)
					{
					trialBalanceHdrId = trialBalanceHdr.getId();					
					}
				}
			}
		});
    	
    	
    }

    @FXML
    void tableOnEnter(KeyEvent event) {
    	if(event.getCode()==KeyCode.ENTER)
    	{
    		showStateMentOfAccount(accountId);
    	}
    }
    private void showStateMentOfAccount(String account) {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/AccountBalanceByAccountHeads.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			AccountBalanceByAccountIdCtl accountCtl =fxmlLoader.getController();
			accountCtl.setAccountId(account);
   			Stage stage = new Stage();
   			stage.setScene(new Scene(root1));
   			stage.initModality(Modality.APPLICATION_MODAL);
   			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	@FXML
    void GenerateReport(ActionEvent event) {
		
		if(null == dpFromDate.getValue())
		{
			notifyMessage(2, "pleas select date", false);
			dpFromDate.requestFocus();
			return;
		}
		
		udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		
		Date date = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String edate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
		
		Date rdate = SystemSetting.applicationDate;
		String reportdate = SystemSetting.UtilDateToString(rdate, "yyyy-MM-dd");
		
		accountBalanceList.clear();
		tblTrialBalance.getItems().clear();
    	
    	ArrayList items = new ArrayList();
		items = RestCaller.getTrialBalance(sdate,edate,reportdate);

		String account = "";
		Double debitAmount = 0.0;
		Double creditAmount = 0.0;
		//Double amount = 0.0;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			account = (String) element.get(0);
			debitAmount = (Double) element.get(2);
			creditAmount = (Double) element.get(1);
			
			if(debitAmount==0 && creditAmount==0) {
				continue;
			}
			//amount = (Double) element.get(3);
			if (null != account) {
				
				AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
				accountBalanceReport.setAccountHeads(account);
				 
				 
					accountBalanceReport.setDebit(debitAmount);
					accountBalanceReport.setCredit(creditAmount);
				 
				
				accountBalanceList.add(accountBalanceReport);

			}


    }
		
		
		
		
		fillTable();
    }

	private void fillTable() {
		tblTrialBalance.setItems(accountBalanceList);
		
		BigDecimal creditTotal = new BigDecimal("0.0");
		BigDecimal  debitTotal = new BigDecimal("0.0");
		for(AccountBalanceReport account : accountBalanceList)
		{
			creditTotal = creditTotal.add(new BigDecimal( account.getCredit()));
			debitTotal = debitTotal.add( new BigDecimal(account.getDebit()));
		}
		
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountHeadsProperty());
		clCreditAmount.setCellValueFactory(cellData -> cellData.getValue().getCreditProperty());

		clDebitAmount.setCellValueFactory(cellData -> cellData.getValue().getDebitProperty());
		clDebitAmount.setCellFactory(tc-> new TableCell<AccountBalanceReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		clCreditAmount.setCellFactory(tc-> new TableCell<AccountBalanceReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		
		//BigDecimal bdDebit = new BigDecimal(debitTotal);
		//BigDecimal bdCredit = new BigDecimal(creditTotal);
		debitTotal=debitTotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		creditTotal=creditTotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		
		
		txtCreditTotal.setText(creditTotal.toPlainString());
		txtDebitTotal.setText(debitTotal.toPlainString());
		
		
	}


	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	

    @FXML
    void exportToExcel(ActionEvent event) {

    
    	

		if(null == dpFromDate.getValue())
		{
			notifyMessage(2, "pleas select date", false);
			dpFromDate.requestFocus();
			return;
		}
		
		Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		
		Date date = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String edate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
		
		Date rdate = SystemSetting.applicationDate;
		String reportdate = SystemSetting.UtilDateToString(rdate, "yyyy-MM-dd");
		
		accountBalanceList.clear();
		tblTrialBalance.getItems().clear();
    	
    	ArrayList items = new ArrayList();
		items = RestCaller.getTrialBalance(sdate,edate,reportdate);

		String account = "";
		Double debitAmount = 0.0;
		Double creditAmount = 0.0;
	 
		Iterator itr = items.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			account = (String) element.get(0);
			debitAmount = (Double) element.get(2);
			creditAmount = (Double) element.get(1);
			if(debitAmount==0 && creditAmount==0) {
				continue;
			}
		
			if (null != account) {
				
				AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
				accountBalanceReport.setAccountHeads(account);
				accountBalanceReport.setDebit(debitAmount);
				accountBalanceReport.setCredit(creditAmount);
				 
				accountBalanceList.add(accountBalanceReport);

			}

		
			

    }
		String totalCredit=	txtCreditTotal.getText();

	       
    	
		 exportTrialBalanceToExcell.exportToExcel("ExportTrailBalanceToExcel"+sdate+".xls", items,sdate,totalCredit);
    	
    }
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}
    @FXML
    void showReportId(ActionEvent event) {
    	
    	reportIdDetailList.clear();
		tblReportId.getItems().clear();
		tblTrialBalance.getItems().clear();
		txtCreditTotal.clear();
		txtDebitTotal.clear();
    	Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		
		String userId=SystemSetting.getUserId();
		
		ArrayList reportIdDetails = new ArrayList();
		reportIdDetails = RestCaller.getReportId(sdate,userId);

		String reportID;
		Iterator itr = reportIdDetails.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			reportID = (String) element.get(2);
			if (null != reportID) {
				
				TrialBalanceHdr trialBalanceHdr1 = new TrialBalanceHdr();
				trialBalanceHdr1.setReportId(reportID);
			
				reportIdDetailList.add(trialBalanceHdr1);

			}
    }
		fillReportIdtable();
    }
    private void fillReportIdtable() {
		tblReportId.setItems(reportIdDetailList);
		clmnReport.setCellValueFactory(cellData -> cellData.getValue().getReportIdProperty());
	}

	@FXML
    void viewAccount(ActionEvent event) {
    	if(null != trialBalanceHdrId) {
   		
    		accountBalanceList.clear();
    		tblTrialBalance.getItems().clear();
        	
        	ArrayList trialBalanceDtlDetails = new ArrayList();
        	trialBalanceDtlDetails = RestCaller.getTrialBalanceDtlByHdrId(trialBalanceHdrId);

    		String account = "";
    		Double debitAmount = 0.0;
    		Double creditAmount = 0.0;
    		Iterator itr = trialBalanceDtlDetails.iterator();
    		while (itr.hasNext()) {

    			List element = (List) itr.next();
    			account = (String) element.get(1);
    			debitAmount = (Double) element.get(2);
    			creditAmount = (Double) element.get(3);
    			if (null != account) {
    				AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
    				accountBalanceReport.setAccountHeads(account);
    				accountBalanceReport.setCredit(creditAmount);
    				accountBalanceReport.setDebit(debitAmount);
    			
    				accountBalanceList.add(accountBalanceReport);
    			}
    			}
    		fillTable();
    	}
    	else {
    		notifyMessage(5, "Please select report ID", false);
    	}
    }

   	private void PageReload(String hdrId) {

   	}
}
