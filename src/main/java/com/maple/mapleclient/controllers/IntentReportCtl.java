package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.IntentInDtl;
import com.maple.mapleclient.entity.IntentInHdr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class IntentReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<IntentInDtl> intentInDtlList1 = FXCollections.observableArrayList();

    @FXML
    private Button btnShow;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private DatePicker dpFromDate;



    @FXML
    private TableView<IntentInDtl> tblPending;

    @FXML
    private TableColumn<IntentInDtl,String> clPendingDate;

    @FXML
    private TableColumn<IntentInDtl, String> clPendingItem;

    @FXML
    private TableColumn<IntentInDtl, Number> clPendingQty;

    @FXML
    private TableColumn<IntentInDtl, String> clPendingUnit;

    @FXML
    private TableColumn<IntentInDtl, String> clPendingBranch;
    
    @FXML
 	private void initialize() {
     	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
     	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
  }
    @FXML
    void actionShow(ActionEvent event) {
    	java.util.Date frDate =Date.valueOf(dpFromDate.getValue());
    	String fdate = SystemSetting.UtilDateToString(frDate, "yyyy-MM-dd");
    	
    	java.util.Date toDate =Date.valueOf(dpToDate.getValue());
    	String tdate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
    	ArrayList intent = new ArrayList();
		intent = RestCaller.getPendingIntent(fdate,tdate);
		String itemName;
		String branchCode;
		String unit;
		Double qty;
		Double allocatedQty;
		String id;

		String voucherDate;
		Iterator itr = intent.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			branchCode = (String) element.get(4);
			unit = (String) element.get(3);
			qty = (Double) element.get(1);
			allocatedQty = (Double) element.get(2);
			id = (String) element.get(5);
			voucherDate =  (String) element.get(6);
			java.util.Date rdate= 	SystemSetting.StringToUtilDate(voucherDate, "yyyy-MM-dd");
			//qty = (Integer) element.get(4);
			if (null != itemName) 
			{
				IntentInDtl intentInDtl = new IntentInDtl();
				intentInDtl.setId(id);
				intentInDtl.setItemName(itemName);
				intentInDtl.setQty(qty);
				
				intentInDtl.setAllocatedQty(allocatedQty);
				intentInDtl.setBalanceQty(qty-allocatedQty);
				intentInDtl.setBranchCode(branchCode);
				intentInDtl.setUnitName(unit);
				intentInDtl.setVoucherDate(SystemSetting.UtilDateToString(rdate));
				intentInDtlList1.add(intentInDtl);
				fillIntentInDtlPeniding();
			}
		}
    
    }
    private void fillIntentInDtlPeniding()
    {
    	tblPending.setItems(intentInDtlList1);
    	
		clPendingItem.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clPendingQty.setCellValueFactory(cellData -> cellData.getValue().getbalanceProperty());
		clPendingUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
		clPendingBranch.setCellValueFactory(cellData -> cellData.getValue().getbranchCodeProperty());
		clPendingDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
