package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bouncycastle.pqc.math.linearalgebra.BigEndianConversions;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportB2bSalesDtlToExcel;
import com.maple.maple.util.ExportB2bSalesToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.B2bSalesReport;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import net.sf.jasperreports.engine.JRException;

public class B2BSalesReportCtl {
	String taskid;
	String processInstanceId;

	ExportB2bSalesToExcel ExportB2bSalesToExcel=new ExportB2bSalesToExcel();
	ExportB2bSalesDtlToExcel ExportB2bSalesDtlToExcel = new ExportB2bSalesDtlToExcel();
    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnGenerateReport;
    
    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btExporToExcel;



    @FXML
    private Button btnExportDtl;

    
    

    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null != dpDate.getValue()) {
			Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
			String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
			Date utodate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String stodate = SystemSetting.UtilDateToString(utodate, "yyyy-MM-dd");
			try {
				JasperPdfReportService.B2BSalesSummary(date,stodate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    @FXML
    void actionExportB2bDtl(ActionEvent event) {
    	Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
		String Fdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		Date UTOdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String Tdate=SystemSetting.UtilDateToString(UTOdate, "yyyy-MM-dd");
		List<B2bSalesReport> reportList=new ArrayList<B2bSalesReport>();
    	ResponseEntity<List<B2bSalesReport>> b2bSalesReport = RestCaller.getB2bSaleDtlSummary(Fdate,Tdate);

    	reportList=b2bSalesReport.getBody();
    	for(B2bSalesReport b2b:reportList)
    	{
    		BigDecimal bdtaxable = new BigDecimal(b2b.getTaxableValue());
    		bdtaxable = bdtaxable.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setTaxableValue(bdtaxable.doubleValue());
    		
    		String udates = SystemSetting.UtilDateToString(b2b.getVoucherDate(), "yyyy-MM-dd");
    		b2b.setSvoucherDate(udates);	
    	}
    	ExportB2bSalesDtlToExcel.exportToExcel("B2bSalesDtlToExcel"+Fdate+".xls", reportList);

    }

    @FXML
   	private void initialize() {
       	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
       	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    }

    @FXML
    void focusEndDate(KeyEvent event) {

    }

    @FXML
    void exportToExcel(ActionEvent event) {
    	if(null==dpDate.getValue()) {
    		notifyMessage(5, "Please select from date", false);
    		return;
    	}
    	if(null==dpToDate.getValue()) {
    		notifyMessage(5, "Please select  to date", false);
    		return;
    	}
    	Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
		String Fdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		Date UTOdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String Tdate=SystemSetting.UtilDateToString(UTOdate, "yyyy-MM-dd");
    	List<B2bSalesReport> reportList=new ArrayList<B2bSalesReport>();
    	ResponseEntity<List<B2bSalesReport>> b2bSalesReport = RestCaller.getB2bSaleSummary(Fdate,Tdate);

    	reportList=b2bSalesReport.getBody();
       System.out.println(reportList.size()+"list size isssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
    	ExportB2bSalesToExcel.exportToExcel("B2bSalesToExcel"+Fdate+".xls", reportList);
    	
    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


      private void PageReload() {
      
      }
      	
}
