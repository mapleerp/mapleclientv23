package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PharmacyDailySalesTransactionReport;
import com.maple.report.entity.PoliceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyDailySalesTransactionReportCtl {
//  (0_0)       SharonN  

	private ObservableList<PharmacyDailySalesTransactionReport> PharmacyDailySalesTransactionReportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dptoDate;

    @FXML
    private Button btnPrintReport;

    @FXML
    private Button btnGenerate;

    @FXML
    private DatePicker dpfromDate;

    @FXML
    private TableView<PharmacyDailySalesTransactionReport> tbldailysalestransactionreport;
    @FXML
    private TableColumn<PharmacyDailySalesTransactionReport, String> clDate;

    @FXML
    private TableColumn<PharmacyDailySalesTransactionReport, String> clVoucherNumber;

    @FXML
    private TableColumn<PharmacyDailySalesTransactionReport, String> clCustomerName;

    @FXML
    private TableColumn<PharmacyDailySalesTransactionReport, Number> clInvoiceAmount;

    @FXML
    private TableColumn<PharmacyDailySalesTransactionReport, Number> clInvoiceDiscount;

    @FXML
    private TableColumn<PharmacyDailySalesTransactionReport, String> clTinNumber;

    @FXML
    private TableColumn<PharmacyDailySalesTransactionReport,String> clsalesMan;
    @FXML
	private void initialize() {
		dpfromDate = SystemSetting.datePickerFormat(dpfromDate, "dd/MMM/yyyy");
		dptoDate = SystemSetting.datePickerFormat(dptoDate, "dd/MMM/yyyy");
		tbldailysalestransactionreport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getCustomerName()) {

					
					fillTable();


				}
			}
		});

	}
    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null == dpfromDate.getValue()) {

			notifyMessage(3, "please select From date", false);
			dpfromDate.requestFocus();
			return;
		}
		if (null == dptoDate.getValue()) {

			notifyMessage(3, "please select Todate", false);
			dptoDate.requestFocus();
			return;
		}

		java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dptoDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		ResponseEntity<List<PharmacyDailySalesTransactionReport>> DailySalesTransactionReportResp = RestCaller.getPharmacyDailySalesTransactionReport(fromdate, todate);

		PharmacyDailySalesTransactionReportList = FXCollections.observableArrayList(DailySalesTransactionReportResp.getBody());

		fillTable();

	}
    private void fillTable() {
    	tbldailysalestransactionreport.setItems(PharmacyDailySalesTransactionReportList);

		clDate.setCellValueFactory(cellData -> cellData.getValue().getDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clInvoiceAmount.setCellValueFactory(cellData -> cellData.getValue().getInvoiceAmountProperty());
		clInvoiceDiscount.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDiscountProperty());
		clsalesMan.setCellValueFactory(cellData -> cellData.getValue().getSalesManProperty());
		clTinNumber.setCellValueFactory(cellData -> cellData.getValue().getTinNumberProperty());
		
	}
    @FXML
	void PrintReport(ActionEvent event) {
		
		
		java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dptoDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		try {
			JasperPdfReportService.PharmacyDailySalesTransactionReport(fromdate, todate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    

	 public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}

   
}
