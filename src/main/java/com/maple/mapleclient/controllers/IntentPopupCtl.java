
 package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.IntentHdr;
import com.maple.mapleclient.entity.IntentInHdr;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.events.IntentPopupEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class IntentPopupCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	IntentPopupEvent intentEvent;
    @FXML
    private TextField intenttxt;

    @FXML
    private Button IntentSubmit;

    @FXML
    private TableView<IntentInHdr> intenttbl;

    @FXML
    private TableColumn<IntentInHdr, String> custName;
    
    @FXML
    private TableColumn<IntentInHdr, String> clVoucherNo;

    @FXML
    private TableColumn<IntentInHdr, String> clFromBranch;
    
    private ObservableList<IntentInHdr> intentList = FXCollections.observableArrayList();
		StringProperty SearchString = new SimpleStringProperty();
		
    @FXML
    private Button intentcancel;
    @FXML
	private void initialize() {
    	
    	intenttxt.textProperty().bindBidirectional(SearchString);
    	intentEvent = new IntentPopupEvent();
    	eventBus.register(this);
    	LoadintentNoPopupBySearch("");
    	intenttbl.setItems(intentList);
    	custName.setCellValueFactory(cellData -> cellData.getValue().getidProperty());
    	clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
    	clFromBranch.setCellValueFactory(cellData -> cellData.getValue().gefromBranchProperty());
    	intenttbl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
    		  if (newSelection != null) {
			    	if(null!=newSelection.getId()&& newSelection.getInVoucherNumber().length()>0) {
			    		intentEvent.setIntentNumber(newSelection.getId());
			    		intentEvent.setFromBranch(newSelection.getFromBranch());
			    		//intentEvent.setVoucherDate(newSelection.getInVoucherDate());
			    		intentEvent.setVoucherNumber(newSelection.getInVoucherNumber());
			    		eventBus.post(intentEvent);
			    		
			    	}
			    }
			});
	    	 SearchString.addListener(new ChangeListener(){
					@Override
					public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					 					
						LoadintentNoPopupBySearch((String)newValue);
					}
		        });
    }
    
    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) IntentSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			intenttxt.requestFocus();
		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
       	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    				intenttbl.requestFocus();
    				intenttbl.getSelectionModel().selectFirst();
    			}
    			if (event.getCode() == KeyCode.ESCAPE) {
    				Stage stage = (Stage) IntentSubmit.getScene().getWindow();
    				stage.close();
    			}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) intentcancel.getScene().getWindow();
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) IntentSubmit.getScene().getWindow();
		stage.close();
    }
    private void LoadintentNoPopupBySearch(String searchData) {
    	
    	
    	ArrayList intnt = new ArrayList();
    	intentList.clear();
    	intnt = RestCaller.SearchIntentNo(searchData);
    	Iterator itr = intnt.iterator();
    	while (itr.hasNext()) {
    		 LinkedHashMap element = (LinkedHashMap) itr.next();
    		 Object id = (String) element.get("id");
    		 Object inVoucherNumber = (String) element.get("inVoucherNumber");
    		 Object fromBranch = (String)element.get("fromBranch");
    		 //Object inVoucherDate = (Date)element.get("inVoucherDate");
    		 if (null != id) {
    			 IntentInHdr intntinHdr = new IntentInHdr();
    			 intntinHdr.setId((String)id);
    			 intntinHdr.setFromBranch((String)fromBranch);
    			 intntinHdr.setInVoucherNumber((String)inVoucherNumber);
    			 //intntinHdr.setInVoucherDate((Date)inVoucherDate);
    			 intentList.add(intntinHdr);
    		 }
    	}
    	return;
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }


}
