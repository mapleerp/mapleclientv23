package com.maple.mapleclient.controllers;

import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.JournalHdr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockTransferInReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class StockTransferInDetailReportCtl {
	
	String taskid;
	String processInstanceId;

	List<String> categoryList = new ArrayList<String>();
	private ObservableList<StockTransferInReport> stockTransferInReportList = FXCollections.observableArrayList();

	@FXML
	private DatePicker dpFromDate;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private Button btnGenarateReport;

	@FXML
	private ComboBox<String> cmbFromBranch;

	@FXML
	private ComboBox<String> cmbCategory;

	@FXML
	private Button BtnAddCategory;

	@FXML
	private ListView<String> listCategory;

	@FXML
	private TableView<StockTransferInReport> tblStockTransfeReport;

	@FXML
	private TableColumn<StockTransferInReport, String> clInvoiceDate;

	@FXML
	private TableColumn<StockTransferInReport, String> clVoucherNumber;

	@FXML
	private TableColumn<StockTransferInReport, String> clFromBranch;

	@FXML
	private TableColumn<StockTransferInReport, String> clItemName;

	@FXML
	private TableColumn<StockTransferInReport, String> clGroupName;

	@FXML
	private TableColumn<StockTransferInReport, String> clItemCode;

	@FXML
	private TableColumn<StockTransferInReport, String> clBatch;

	@FXML
	private TableColumn<StockTransferInReport, String> clExpiryDate;

	@FXML
	private TableColumn<StockTransferInReport, Number> clQty;

	@FXML
	private TableColumn<StockTransferInReport, String> clUnitName;

	@FXML
	private TableColumn<StockTransferInReport, Number> clRate;

	@FXML
	private TableColumn<StockTransferInReport, Number> clAmount;

	@FXML
	private TableColumn<StockTransferInReport, String> clUserLogin;
	
	  @FXML
	    private Button btnPrintReport;


	@FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		ResponseEntity<List<CategoryMst>> categoryMstResp = RestCaller.getCategoryMst();
		List<CategoryMst> categoryList = categoryMstResp.getBody();
		for (CategoryMst categoryMst : categoryList) {
			cmbCategory.getItems().add(categoryMst.getCategoryName());
		}

		ResponseEntity<List<BranchMst>> branchMstResp = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = branchMstResp.getBody();

		for (BranchMst branch : branchMstList) {
			cmbFromBranch.getItems().add(branch.getBranchName());
		}

	}

	@FXML
	void AddCategory(ActionEvent event) {

		if (null == cmbCategory.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Please Select Category", false);
			return;
		}

		categoryList.add(cmbCategory.getSelectionModel().getSelectedItem().toString());

		fillCategoryList(categoryList);

	}

	private void fillCategoryList(List<String> categoryList) {
		listCategory.getItems().clear();

		for (String str : categoryList) {
			listCategory.getItems().add(str);
		}

	}

	@FXML
	void GenarateReport(ActionEvent event) {

		if (null == dpFromDate.getValue()) {
			notifyMessage(3, "please select from date", false);
			dpFromDate.requestFocus();
			return;
		}
		if (null == dpToDate.getValue()) {
			notifyMessage(3, "please select to date", false);
			dpToDate.requestFocus();
			return;
		}

		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String edate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");

		String fromBranchCode = null;
		String categoryIds = "";

		if (null != cmbFromBranch.getSelectionModel().getSelectedItem()) {
			ResponseEntity<BranchMst> branchMstResp = RestCaller
					.getBranchMstByName(cmbFromBranch.getSelectionModel().getSelectedItem());

			BranchMst branchMst = branchMstResp.getBody();
			fromBranchCode = branchMst.getBranchCode();
		}

		if (null != categoryList) {
			for (String category : categoryList) {
				ResponseEntity<CategoryMst> categoryMstResp = RestCaller.getCategoryByName(category);
				CategoryMst categoryMst = categoryMstResp.getBody();
				if (null != categoryMst) {

					categoryIds = categoryIds.concat(categoryMst.getId());
					categoryIds = categoryIds.concat(";");

				}

			}
		}

		if (!categoryIds.equalsIgnoreCase("") && null != fromBranchCode) {
			ResponseEntity<List<StockTransferInReport>> stockTransferInDetailReportResp = RestCaller
					.StockTransferInDetailReportByFromBranch(SystemSetting.getUser().getBranchCode(), fromBranchCode,
							categoryIds, sdate, edate);
			stockTransferInReportList = FXCollections.observableArrayList(stockTransferInDetailReportResp.getBody());

		}

		else if (null != fromBranchCode) {
			ResponseEntity<List<StockTransferInReport>> stockTransferInDetailReportResp = RestCaller
					.StockTransferInDetailReportByFromBranch(SystemSetting.getUser().getBranchCode(), fromBranchCode,
							sdate, edate);
			stockTransferInReportList = FXCollections.observableArrayList(stockTransferInDetailReportResp.getBody());

		} else if (!categoryIds.equalsIgnoreCase("")) {
			
			ResponseEntity<List<StockTransferInReport>> stockTransferInDetailReportResp = RestCaller
					.StockTransferInDetailReportByCategory(SystemSetting.getUser().getBranchCode(), categoryIds, sdate,
							edate);
			
			stockTransferInReportList = FXCollections.observableArrayList(stockTransferInDetailReportResp.getBody());

		} else {
			ResponseEntity<List<StockTransferInReport>> stockTransferInDetailReportResp = RestCaller
					.StockTransferInDetailReport(SystemSetting.getUser().getBranchCode(), sdate, edate);
			stockTransferInReportList = FXCollections.observableArrayList(stockTransferInDetailReportResp.getBody());

		}

		fillTable();
		
		categoryIds = null;
		fromBranchCode = null;
//		categoryIds = null;
//		fromBranchCode = null;
//		cmbFromBranch.getSelectionModel().clearSelection();
//		listCategory.getItems().clear();
//		categoryList = new ArrayList<String>();

	}
	
	
    @FXML
    void PrintReport(ActionEvent event) {
    	if (null == dpFromDate.getValue()) {
			notifyMessage(3, "please select from date", false);
			dpFromDate.requestFocus();
			return;
		}
		if (null == dpToDate.getValue()) {
			notifyMessage(3, "please select to date", false);
			dpToDate.requestFocus();
			return;
		}

		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String edate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");

		String fromBranchCode = null;
		String categoryIds = "";

		if (null != cmbFromBranch.getSelectionModel().getSelectedItem()) {
			ResponseEntity<BranchMst> branchMstResp = RestCaller
					.getBranchMstByName(cmbFromBranch.getSelectionModel().getSelectedItem());

			BranchMst branchMst = branchMstResp.getBody();
			fromBranchCode = branchMst.getBranchCode();
		}

		if (null != categoryList) {
			for (String category : categoryList) {
				ResponseEntity<CategoryMst> categoryMstResp = RestCaller.getCategoryByName(category);
				CategoryMst categoryMst = categoryMstResp.getBody();
				if (null != categoryMst) {

					categoryIds = categoryIds.concat(categoryMst.getId());
					categoryIds = categoryIds.concat(";");

				}

			}
		}

		if (!categoryIds.equalsIgnoreCase("") && null != fromBranchCode) {
			
			try {
				JasperPdfReportService.StockTransferInDetailReport(SystemSetting.getUser().getBranchCode(), fromBranchCode,
						categoryIds, sdate, edate);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}

		else if (null != fromBranchCode) {
			
			try {
				
			
			JasperPdfReportService.StockTransferInDetailReportByFromBranch(SystemSetting.getUser().getBranchCode(), fromBranchCode,
		 sdate, edate);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		} else if (!categoryIds.equalsIgnoreCase("")) {
			
			

			try {
				
				
				JasperPdfReportService.StockTransferInDetailReportByCategory(SystemSetting.getUser().getBranchCode(), categoryIds,
			 sdate, edate);
				} catch (Exception e) {
					// TODO: handle exception
				}
		} else {
			ResponseEntity<List<StockTransferInReport>> stockTransferInDetailReportResp = RestCaller
					.StockTransferInDetailReport(SystemSetting.getUser().getBranchCode(), sdate, edate);
			stockTransferInReportList = FXCollections.observableArrayList(stockTransferInDetailReportResp.getBody());


			try {
				
				
				JasperPdfReportService.StockTransferInDetailReport(sdate, edate);
				} catch (Exception e) {
					// TODO: handle exception
				}
			
		}
		categoryIds = null;
		fromBranchCode = null;
		
//		cmbFromBranch.getSelectionModel().clearSelection();
//		listCategory.getItems().clear();
//		categoryList = new ArrayList<String>();

    }

	private void fillTable() {

		tblStockTransfeReport.setItems(stockTransferInReportList);

		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clFromBranch.setCellValueFactory(cellData -> cellData.getValue().getFromBranchProperty());
		clGroupName.setCellValueFactory(cellData -> cellData.getValue().getCategoryProperty());
		clInvoiceDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
		clItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clUnitName.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
//		clUserLogin.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());

	}
	
	   @FXML
	    void Cleare(ActionEvent event) {
		   
			
			cmbFromBranch.getSelectionModel().clearSelection();
			listCategory.getItems().clear();
			categoryList = new ArrayList<String>();
			dpFromDate.setValue(null);
			dpToDate.setValue(null);

	    }

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}

}
