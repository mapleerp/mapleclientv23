package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.entity.LocationMst;
import com.maple.mapleclient.events.JobCardEvent;
import com.maple.mapleclient.events.LocationEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class LocationPopupCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<LocationMst> locationList = FXCollections.observableArrayList();
	boolean initializedCalled = false;
	LocationEvent locationEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	StringProperty SearchString = new SimpleStringProperty();


    @FXML
    private TextField txtLocation;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<LocationMst> tblLocation;

    @FXML
    private TableColumn<LocationMst, String> clLocation;

    @FXML
    private Button btnCancel;
    
    @FXML
	private void initialize() {
		if (initializedCalled)
			return;

		initializedCalled = true;

		locationEvent = new LocationEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		txtLocation.textProperty().bindBidirectional(SearchString);

		btnCancel.setCache(true);
		
		LoadItemPopupBySearch("");
		
		tblLocation.setItems(locationList);
		
		clLocation.setCellValueFactory(cellData -> cellData.getValue().getLocationProperty());


		tblLocation.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					
					locationEvent = new LocationEvent();
					locationEvent.setLocationId(newSelection.getId());
					locationEvent.setLocationName(newSelection.getLocation());
					
					eventBus.post(locationEvent);
					
				}
			}

		});
		
		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadItemPopupBySearch((String) newValue);
			}
		});

	}

    protected void LoadItemPopupBySearch(String searchData) {
    	ArrayList items = new ArrayList();

    	locationList.clear();

		items = RestCaller.SearchLocation(searchData);
		
		String location = null;
		String id = null;
		
		Iterator itr = items.iterator();

		while (itr.hasNext()) {
			
			List element = (List) itr.next();
			location = (String) element.get(0);
			id = (String) element.get(1);
			
			if (null != id) {
				LocationMst locationMst = new LocationMst();
				locationMst.setLocation(location);
				locationMst.setId(id);
				locationList.add(locationMst);

			}
			
		}
		
		
	}

	@FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}  else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblLocation.requestFocus();
			tblLocation.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();

    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }

}
