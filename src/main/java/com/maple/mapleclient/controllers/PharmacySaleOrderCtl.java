package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.OrderTakerMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SaleOrderReceipt;
import com.maple.mapleclient.entity.SalesManMst;
import com.maple.mapleclient.entity.SalesOrderDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.SalesPropertiesConfigMst;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.SalesTypeMst;
import com.maple.mapleclient.entity.SiteMst;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.SummarySalesOderDtl;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.AcountPopUpAssetEvent;
import com.maple.mapleclient.events.CategorywiseItemSearchEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.HoldedCustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacySaleOrderCtl {

	String taskid;
	String processInstanceId;
	SalesTransHdr salesTransHdr;
	String localCustId = null;
	// private ObservableList<SalesTypeMst> saleTypeTable =
	// FXCollections.observableArrayList();
	private ObservableList<PriceDefinition> priceDefenitionList = FXCollections.observableArrayList();
	private ObservableList<BatchPriceDefinition> BatchpriceDefenitionList = FXCollections.observableArrayList();

	private ObservableList<SiteMst> siteMstList = FXCollections.observableArrayList();
	SaleOrderReceipt saleOrderReceipt = new SaleOrderReceipt();
	private ObservableList<SaleOrderReceipt> salesReceiptsList = FXCollections.observableArrayList();
	private static final Logger logger = LoggerFactory.getLogger(PurchaseCtl.class);
	String invoiceNumberPrefix = SystemSetting.WHOLE_SALES_PREFIX;
	String gstInvoicePrefix = SystemSetting.GST_INVOCE_PREFIX;
	String vanSalesPrefix = SystemSetting.GST_INVOCE_PREFIX;
	EventBus eventBus = EventBusFactory.getEventBus();
	// ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();

	private ObservableList<SalesOrderDtl> saleOrderListItemTable = FXCollections.observableArrayList();
	private ObservableList<SalesOrderDtl> saleOrderListTable = FXCollections.observableArrayList();
	String salesReceiptVoucherNo = null;
	String store = "MAIN";
	Double changeAmount = 0.0;
	@FXML
	private TextField txtDiscount;

	@FXML
	private Label lblAmount;
	@FXML
	private TextField txtDiscountAmt;

	@FXML
	private TextField txtPriceType;
	@FXML
	private TextField txtPreviousBalance;
	@FXML
	private Button btnSearch;
	@FXML
	private TextField txtLocalCustomer;

	@FXML
	private Label lblCreditPeriod;

	@FXML
	private TextField txtWarranty;

	@FXML
	void ShowLocalCustomerPopup(ActionEvent event) {

		showLocalCustPopup();
	}

	private void showLocalCustPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LocalCustPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);

			Parent root = loader.load();
			LocalCustPopupCtl popupctl = loader.getController();
			popupctl.localCustomerPopupByCustomerId(custId);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			gstNo.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupLocalCustomerlistner(LocalCustomerEvent localCustomerEvent) {
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			txtLocalCustomer.setText(localCustomerEvent.getLocalCustAddress());
			localCustId = localCustomerEvent.getLocalCustid();
			System.out.println("custIdcustIdcustId" + custId);

		}

	}

	@FXML
	private TextField txtAmtAftrDiscount;
	SalesOrderDtl salesOrderDtl = null;
	SalesOrderTransHdr salesOrderTransHdr = null;
	UnitMst unitMst = null;
	Double cashPaidAmount = 0.0;
	double qtyTotal = 0;
	double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	String custId = "";
	boolean customerIsBranch = false;

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty discount = new SimpleStringProperty("");
	StringProperty discountAmt = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");

	StringProperty barcodeProperty = new SimpleStringProperty("");

	StringProperty taxRateProperty = new SimpleStringProperty("");

	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");

	@FXML
	private TableView<SalesOrderDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnQty;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnRate;
	@FXML
	private TableColumn<SalesOrderDtl, String> columnMrp;
	@FXML
	private TableColumn<SalesOrderDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesOrderDtl, LocalDate> columnExpiryDate;
	@FXML
	private TableColumn<SalesOrderDtl, Number> clAmount;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtItemcode;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField txtLoginDate;
	@FXML
	private TextField txtSBICard;

	@FXML
	private TextField txtSodexoCard;

	@FXML
	private TextField txtYesCard;
	@FXML
	private TextField custname;
	@FXML
	private TextField custAdress;
	@FXML
	private TextField gstNo;

	@FXML
	private ComboBox<String> cmbStore;

	@FXML
	private ComboBox<String> cmbSalesMan;

	@FXML
	void onEnterCustPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();
		}
	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

		loadCustomerPopup();
	}

	@FXML
	void discountOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (!txtDiscount.getText().isEmpty()) {
				txtDiscountAmt.clear();
				Double amtaftradiscount;
				Double discountamt = (Double.parseDouble(txtCashtopay.getText())
						* Double.parseDouble(txtDiscount.getText())) / 100;
				BigDecimal disamt = new BigDecimal(discountamt);
				disamt = disamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				txtDiscountAmt.setText(disamt.toPlainString());
				// txtDiscountAmt.setEditable(false);
				amtaftradiscount = Double.parseDouble(txtCashtopay.getText())
						- Double.parseDouble(txtDiscountAmt.getText());
				BigDecimal amtaftdiscount = new BigDecimal(amtaftradiscount);
				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
				txtAmtAftrDiscount.setEditable(false);
			}

		}
	}

	@FXML
	void discountAmntOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtDiscountAmt.getText().isEmpty()) {
				txtDiscount.clear();
				Double discountAftrAmt = 0.0, discount = 0.0;
				discount = (Double.parseDouble(txtDiscountAmt.getText()) / Double.parseDouble(txtCashtopay.getText()))
						* 100;
				BigDecimal disamt = new BigDecimal(discount);
				disamt = disamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtDiscount.setText(disamt.toPlainString());
				discountAftrAmt = Double.parseDouble(txtCashtopay.getText())
						- Double.parseDouble(txtDiscountAmt.getText());
				BigDecimal amtaftdiscount = new BigDecimal(discountAftrAmt);
				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
				txtAmtAftrDiscount.setEditable(false);
			}

		}
	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {
		// showPopup();
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	@FXML
	private void initialize() {

		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("WHOLESALE_RATE_EDIT");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("NO")) {
				txtRate.setEditable(false);
			} else {
				txtRate.setEditable(true);
			}
		}
		eventBus.register(this);

		txtLoginDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate));
		txtDiscount.setVisible(false);
		txtAmtAftrDiscount.setVisible(false);
		txtDiscountAmt.setVisible(false);
//		cmbSaleType.getItems().add("VAN SALE");
//		cmbSaleType.getItems().add("COUNTER SALE");
		ResponseEntity<List<SalesTypeMst>> salesTypeSaved = RestCaller.getSalesTypeMst();
		if (null == salesTypeSaved.getBody()) {
			notifyMessage(5, "Please Add Voucher Type");
			return;
		}
		// saleTypeTable = FXCollections.observableArrayList(salesTypeSaved.getBody());

//		for (SalesTypeMst salesType : saleTypeTable) {
//			cmbSaleType.getItems().add(salesType.getSalesType());
//
//		}

		logger.info("========INITIALIZATION STARTED IN WHOLE SALE WINDOW ===============");
		/*
		 * Create an instance of salesOrderDtl. salesOrderTransHdr entity will be
		 * refreshed after final submit. salesOrderDtl will be added on AddItem Function
		 * 
		 */

		// salesOrderDtl = new salesOrderDtl();
		/*
		 * Fieds with Entity property
		 */

		txtDiscountAmt.textProperty().bindBidirectional(discountAmt);
		txtDiscount.textProperty().bindBidirectional(discount);
		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);
		// txtItemcode.textProperty().bindBidirectional(salesOrderDtl.getItemCodeProperty());
		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPaidamount.setText(oldValue);
				}
			}
		});
		txtChangeamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtChangeamount.setText(oldValue);
				}
			}
		});
		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashtopay.setText(oldValue);
				}
			}
		});
		txtSBICard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSBICard.setText(oldValue);
				}
			}
		});

		txtYesCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtYesCard.setText(oldValue);
				}
			}
		});

		txtSodexoCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSodexoCard.setText(oldValue);
				}
			}
		});
		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});

		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtPaidamount.getText().length() > 0)) {

					double paidAmount = Double.parseDouble(txtPaidamount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmt = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmount = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((cardAmount) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtcardAmount.getText().length() > 0)) {

					double cardAmt = Double.parseDouble(txtcardAmount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmount = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmt = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((paidAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
				}
			}
		});

		// btnSave.setDisable(true);
		itemDetailTable.setItems(saleOrderListTable);

		txtBarcode.requestFocus();

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					SalesOrderDtl salesOrderDtl = new SalesOrderDtl();
					txtBatch.setText(newSelection.getBatchCode());
					salesOrderDtl = new SalesOrderDtl();
					salesOrderDtl.setId(newSelection.getId());
					if (null != newSelection.getOfferReferenceId()) {
						(salesOrderDtl).setOfferReferenceId(newSelection.getOfferReferenceId());
					}

					if (null != newSelection.getSchemeId()) {
						salesOrderDtl.setSchemeId(newSelection.getSchemeId());
					}
					salesOrderDtl.setItemId(newSelection.getItemId());
					salesOrderDtl.setSalesOrderTransHdr(salesOrderTransHdr);
					// (newSelection.getsalesOrderTransHdr());
					salesOrderDtl.setQty(newSelection.getQty());
					salesOrderDtl.setUnitId(newSelection.getUnitId());
					salesOrderDtl.setUnitName(newSelection.getUnitName());
					txtBarcode.setText(newSelection.getBarcode());
					txtItemname.setText(newSelection.getItemName());
					txtItemcode.setText(newSelection.getItemCode());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));
					cmbUnit.setValue(newSelection.getUnitName());
				}
			}
		});
		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				// ----------------------------new version 2 surya

				if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO")) {
					return;
				}
				// ----------------------------new version 2 surya end

				ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadsByName(custname.getText());

				AccountHeads accountHeads = getAccountHeads.getBody();

				// ResponseEntity<PriceDefinition> getpriceDef = RestCaller.get
				ResponseEntity<UnitMst> getUnit = RestCaller
						.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

				UnitMst unitMst = getUnit.getBody();

				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

				ItemMst item = getItem.getBody();

				if (null == item || null == item.getId()) {
					return;
				}

				String unitId = "";

				if (null == unitMst) {
					unitId = item.getUnitId();

				} else {
					unitId = unitMst.getId();
				}

//				Date udate = (java.sql.Date) SystemSetting.getApplicationDate();
//				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				java.util.Date udate = SystemSetting.getApplicationDate();
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
						getItem.getBody().getId(), getAccountHeads.getBody().getPriceTypeId(), getUnit.getBody().getId(),
						txtBatch.getText(), sdate);
				if (null != batchPriceDef.getBody()) {
					txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
				} else {
					txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));

					ResponseEntity<PriceDefinition> priceDef = RestCaller.getPriceDefenitionByItemIdAndUnit(
							item.getId(), accountHeads.getPriceTypeId(), unitMst.getId(), sdate);
					if (null != priceDef.getBody()) {
						txtRate.setText(Double.toString(priceDef.getBody().getAmount()));

					}

					else

					{
						ResponseEntity<List<PriceDefinition>> pricebyItem = RestCaller
								.getPriceByItemId(getItem.getBody().getId(), getAccountHeads.getBody().getPriceTypeId());
						priceDefenitionList = FXCollections.observableArrayList(pricebyItem.getBody());

						if (null != pricebyItem.getBody())

						{
							for (PriceDefinition price : priceDefenitionList) {
								if (null == price.getUnitId()) {
									txtRate.setText(Double.toString(price.getAmount()));

								}
							}
						}
					}

				}

			}
		});

		ResponseEntity<List<StoreMst>> storeMstListResp = RestCaller.getAllStoreMst();
		List<StoreMst> storeMstList = storeMstListResp.getBody();

		for (StoreMst store : storeMstList) {
			cmbStore.getItems().add(store.getName());
		}

		ResponseEntity<List<SalesManMst>> salesManMstResp = RestCaller.findAllSalesMan();
		List<SalesManMst> salesManMstList = salesManMstResp.getBody();

		for (SalesManMst salesMan : salesManMstList) {
			cmbSalesMan.getItems().add(salesMan.getSalesManName());
		}

		logger.info("======== WHOLE SALE WINDOW INITIALIZATION COMPLETED");

		// txtCreditPeriod.setVisible(false);

	}

	@FXML
	void deleteRow(ActionEvent event) {

		try {

			if (null != salesOrderDtl) {
				if (null != salesOrderDtl.getId()) {

					if (null == salesOrderDtl.getSchemeId()) {

						RestCaller.deleteSalesOrderDtl(salesOrderDtl.getId());

						System.out.println("toDeleteSale.getId()" + salesOrderDtl.getId());
//						RestCaller.deletesalesOrderDtl(salesOrderDtl.getId());
						txtItemname.clear();
						txtItemcode.clear();
						txtBarcode.clear();
						txtBatch.clear();
						txtRate.clear();
						txtQty.clear();
						ResponseEntity<List<SalesOrderDtl>> SalesOrderDtlResponse = RestCaller
								.getSalesOrderDtl(salesOrderTransHdr);
						saleOrderListTable = FXCollections.observableArrayList(SalesOrderDtlResponse.getBody());
						FillTable();
						salesOrderDtl = new SalesOrderDtl();

						notifyMessage(1, " Item Deleted Successfully");

					} else {
						notifyMessage(1, " Offer can not be delete");
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void hold(ActionEvent event) {
		txtAmtAftrDiscount.clear();
		txtBarcode.clear();
		txtBatch.clear();
		txtcardAmount.clear();
		txtCashtopay.clear();
		txtChangeamount.clear();
		txtDiscount.clear();
		txtYesCard.clear();
		txtSodexoCard.clear();
		txtSBICard.clear();
		txtRate.clear();
		txtQty.clear();
		txtPriceType.clear();
		txtItemcode.clear();
		txtItemname.clear();
		txtPaidamount.clear();
		custAdress.clear();
		custname.clear();
		custId = null;
		saleOrderListTable.clear();
		// saleTypeTable.clear();
		// cmbSaleType.getSelectionModel().clearSelection();
		salesOrderDtl = null;
		salesOrderTransHdr = null;

		// txtCreditPeriod.setVisible(false);

		notifyMessage(5, "Sales Holded");

	}

	@FXML
	void saveOnkey(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}
	}

	@FXML
	void saveOnCtl(KeyEvent event) {
//    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
//    		finalSave();
//    	}
	}

	@FXML
	void FinalSaveOnPress(KeyEvent event) {

		logger.info("===== WHOLE SALE FINAL SAVE STARTED ============");
		if (event.getCode() == KeyCode.ENTER) {
			finalSave();
		}
	}

	private void finalSave() {

		if (null != salesOrderTransHdr) {

			txtChangeamount.setText("00.0");
			Double cashPaidAmount = 0.0;
			Double sodexoCard = 0.0;
			Double cardAmount = 0.0;
			String card = "";
			String cardType = "";

			Double changeAmount = 0.0;

			if (!custname.getText().trim().isEmpty()) {
				ResponseEntity<LocalCustomerMst> resplocal = RestCaller.getLocalCustomerbyName(custname.getText());
				if (null != resplocal.getBody()) {
					System.out.println(resplocal.getBody());
					salesOrderTransHdr.setLocalCustomerId(resplocal.getBody());
					custId = resplocal.getBody().getId();
				}
			}
			if (custname.getText().trim().isEmpty()) {
				ResponseEntity<AccountHeads> resplocalgstcust = RestCaller.getAccountHeadsByNameAndBranchCode("POS",
						SystemSetting.systemBranch);
				salesOrderTransHdr.setAccountHeads(resplocalgstcust.getBody());
			} else {

				ResponseEntity<AccountHeads> resplocalgstcust = RestCaller.getAccountHeadsByName(custname.getText());

				AccountHeads accountHeads = resplocalgstcust.getBody();

				if (null == accountHeads) {
					ResponseEntity<List<AccountHeads>> customerMstResp = RestCaller.AccountHeadsByGst(gstNo.getText());
					accountHeads = customerMstResp.getBody().get(0);
				}
				System.out.print("qqqqqqqqqqqqqqqqqqqqqqqqqqqqq" + accountHeads);

				salesOrderTransHdr.setAccountHeads(accountHeads);
			}

			Double cashtoPay = Double.parseDouble(txtCashtopay.getText());
			salesOrderTransHdr.setInvoiceAmount(cashtoPay);

//			if (!txtCashPaidamount.getText().trim().isEmpty()) {
//				cashPaidAmount = Double.parseDouble(txtCashPaidamount.getText());
			if (!txtcardAmount.getText().trim().isEmpty()) {
				Double cardSale = 0.0;
				Double totalSale = 0.0;
				cardSale = Double.parseDouble(txtcardAmount.getText());
				totalSale = cashPaidAmount + cardSale;
				if (Double.parseDouble(txtCashtopay.getText()) < totalSale) {
					cashPaidAmount = Double.parseDouble(txtCashtopay.getText()) - cardSale;
				}
			} else if (cashPaidAmount > Double.parseDouble(txtCashtopay.getText())) {
				cashPaidAmount = Double.parseDouble(txtCashtopay.getText());

			}
			salesOrderTransHdr.setCashPay(cashPaidAmount);

			saleOrderReceipt = new SaleOrderReceipt();

			if (null == salesReceiptVoucherNo) {
				salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
				saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
			} else {
				saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
			}

			saleOrderReceipt.setReceiptMode("CASH");
			saleOrderReceipt.setReceiptAmount(cashPaidAmount);
			saleOrderReceipt.setUserId(SystemSetting.getUser().getId());
			saleOrderReceipt.setBranchCode(SystemSetting.systemBranch);
			// set accout id

			String account = SystemSetting.systemBranch + "-CASH";
			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(account);
			saleOrderReceipt.setAccountId(accountHeads.getBody().getId());
//	    		
//	    		LocalDate date = LocalDate.now();
//	    		java.util.Date udate = SystemSetting.localToUtilDate(date);
			saleOrderReceipt.setRereceiptDate(SystemSetting.getApplicationDate());
			saleOrderReceipt.setSalesOrderTransHdr(salesOrderTransHdr);
			System.out.println(saleOrderReceipt);
			ResponseEntity<SaleOrderReceipt> respEntity = RestCaller.saveSaleOrderReceipts(saleOrderReceipt);
			saleOrderReceipt = respEntity.getBody();
		}

		if (!txtChangeamount.getText().trim().isEmpty()) {
			changeAmount = Double.parseDouble(txtChangeamount.getText());
			salesOrderTransHdr.setChangeAmount(changeAmount);
		}
		salesOrderTransHdr.setOrderStatus("Orderd");

		salesOrderTransHdr.setPaidAmount((cashPaidAmount) - changeAmount);
		eventBus.post(salesOrderTransHdr);

		if (!custname.getText().trim().isEmpty()) {
			
			ResponseEntity<List<SalesOrderDtl>> saledtlSaved = RestCaller.getSalesOrderDtl(salesOrderTransHdr);
			if (saledtlSaved.getBody().size() == 0) {
				return;
			}

			String financialYear = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "dd-MM-YY");
			String vNo = RestCaller.getVoucherNumber(financialYear + SystemSetting.systemBranch + "CRD");

			salesOrderTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
			salesOrderTransHdr.setVoucherNumber(vNo);
			salesOrderTransHdr.setSalesMode("SALEORDER");
			salesOrderTransHdr.setOrderStatus("Orderd");
//			salesOrderTransHdr.setVoucherNumber(vNo);
			if (!txtCashtopay.getText().trim().isEmpty()) {
				Double cashtoPay = Double.parseDouble(txtCashtopay.getText());
				salesOrderTransHdr.setInvoiceAmount(cashtoPay);

			}
			salesOrderTransHdr.setSaleOrderReceiptVoucherNumber(salesReceiptVoucherNo);

			System.out.println("Voucher Date " + salesOrderTransHdr.getVoucherDate());
			System.out.println("Voucher Date " + SystemSetting.systemDate);

			// salesOrderTransHdr.setCustomerId();
			RestCaller.updateSalesOrderTranshdr(salesOrderTransHdr);
			notifyMessage(5, " Order to Saved");

			ResponseEntity<SalesOrderTransHdr> salesOrderTransHdrSaves = RestCaller
					.getSaleOrderTransHdrById(salesOrderTransHdr.getId());
			salesOrderTransHdr = salesOrderTransHdrSaves.getBody();

			System.out.println("Voucher Date" + salesOrderTransHdr.getVoucherDate());

			ResponseEntity<List<SaleOrderReceipt>> saleOrderReceiptList = RestCaller
					.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());

			List<SaleOrderReceipt> saleOrderReceipts = saleOrderReceiptList.getBody();
			for (SaleOrderReceipt receipts : saleOrderReceipts) {
				DayBook dayBook = new DayBook();
				dayBook.setBranchCode(SystemSetting.systemBranch);
				ResponseEntity<AccountHeads> accountHead1 = RestCaller
						.getAccountById(salesOrderTransHdr.getAccountHeads().getId());
				AccountHeads accountHeads1 = accountHead1.getBody();
				dayBook.setDrAccountName(accountHeads1.getAccountName());
				dayBook.setDrAmount(receipts.getReceiptAmount());
				dayBook.setSourceVoucheNumber(salesOrderTransHdr.getVoucherNumber());
				if (receipts.getReceiptMode().contains("CASH")) {

					String account = SystemSetting.systemBranch + "-CASH";
					ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(account);
					dayBook.setDrAccountName(accountHeads.getBody().getAccountName());

					dayBook.setNarration("SALESORDER CASH ADVANCE");
					dayBook.setCrAmount(0.0);
					dayBook.setCrAccountName(salesOrderTransHdr.getAccountHeads().getAccountName());

				} else {

					ResponseEntity<AccountHeads> accountHeads = RestCaller
							.getAccountHeadByName(receipts.getReceiptMode());
					dayBook.setDrAccountName(accountHeads.getBody().getAccountName());

					dayBook.setNarration("SALESORDER CARD ADVANCE");
					dayBook.setCrAmount(receipts.getReceiptAmount());
					dayBook.setCrAccountName(salesOrderTransHdr.getAccountHeads().getAccountName());
				}
				dayBook.setSourceVoucherType("SALESORDER RECEIPTS");

				LocalDate rdate = SystemSetting.utilToLocaDate(salesOrderTransHdr.getVoucherDate());
				dayBook.setsourceVoucherDate(Date.valueOf(rdate));
				ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			}

			Format formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			String vdate = formatter.format(salesOrderTransHdr.getVoucherDate());

			// Version 1.6 Starts
			salesOrderTransHdr = null;
			txtcardAmount.setText("");
			txtCashtopay.setText("");

			custname.setText("");
			custAdress.setText("");
			gstNo.setText("");

			custId = null;
			txtChangeamount.clear();

			amountTotal = 0.0;

			salesOrderDtl = null;
			itemDetailTable.getItems().clear();
			salesOrderTransHdr = null;

			try {
				JasperPdfReportService.PharmacySaleOrderInvoice(vNo, vdate);
			} catch (JRException e) {
				e.printStackTrace();
				// logger.info("Whole Sale " + e);
			}

		}

		lblAmount.setVisible(false);

		txtcardAmount.setVisible(false);

		// getHoldedSaleOrder();

	}

	private void saveSalesProperty() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SalesPropertyTransHdr.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			SalesPropertyTransHdrCtl itemStockPopupCtl = fxmlLoader.getController();
			itemStockPopupCtl.salesTransHdrId = salesOrderTransHdr.getId();
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.WINDOW_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
//			stage.show();
			stage.showAndWait();
		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	@FXML
	void save(ActionEvent event) {

		finalSave();
		// salesOrderTransHdr = null;
		// salesOrderDtl = null;
	}

	@FXML
	void EnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if (event.getSource() != null)
				;

			txtQty.requestFocus();
		}

	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtItemname.requestFocus();
			showPopup();
		}
		if (event.getCode() == KeyCode.ENTER) {
			if (txtBarcode.getText().trim().isEmpty())
				btnSave.requestFocus();
		}
		if (event.getCode() == KeyCode.DOWN) {
			itemDetailTable.requestFocus();
		}
	}

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnSave.setDisable(false);

			Double change = Double.parseDouble(txtPaidamount.getText()) - Double.parseDouble(txtCashtopay.getText());
			txtChangeamount.setText(Double.toString(change));

		}

	}

	@FXML
	void calcPaid(KeyEvent event) {

	}

	@FXML
	void unHold(ActionEvent event) {
		loadHoldedSales();

		// txtCreditPeriod.setVisible(true);

	}

	@Subscribe
	public void popupCustomerlistner(HoldedCustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			custname.setText(customerEvent.getCustomerName());
			custAdress.setText(customerEvent.getAddress());
			gstNo.setText(customerEvent.getCustGst());
			custId = customerEvent.getCustomerId();

			String customerSite = SystemSetting.customer_site_selection;
			if (customerSite.equalsIgnoreCase("TRUE")) {
				txtLocalCustomer.setDisable(false);
				txtLocalCustomer.clear();
			}

			ResponseEntity<PriceDefenitionMst> priceDef = RestCaller.getPriceNameById(customerEvent.getCustPriceTYpe());
			if (null != priceDef.getBody()) {
				txtPriceType.setText(priceDef.getBody().getPriceLevelName());
			}
			salesOrderTransHdr = RestCaller.getSalesOrderTransHdr(customerEvent.getHdrId());
			// cmbSaleType.setValue(salesOrderTransHdr.getVoucherType());
			// cmbSaleType.getSelectionModel().select(salesOrderTransHdr.getVoucherType());
			saleOrderListTable.clear();
			ResponseEntity<List<SalesOrderDtl>> respentity = RestCaller.getSalesOrderDtl(salesOrderTransHdr);
			saleOrderListTable = FXCollections.observableArrayList(respentity.getBody());
			FillTable();
			// cmbSaleType.getSelectionModel().select(salesOrderTransHdr.getVoucherType().toString());

		}

	}

	private Boolean confirmMessage() {

		return null;
	}

	private void deleteAllsalesOrderDtl() {

		ResponseEntity<List<SalesOrderDtl>> saledtlSaved = RestCaller.getSalesOrderDtl(salesOrderTransHdr);
		List<SalesOrderDtl> saledtlList = saledtlSaved.getBody();
		for (SalesOrderDtl sales : saledtlList) {
			RestCaller.deleteSalesOrderDtl(sales.getId());
		}

		ResponseEntity<List<SalesOrderDtl>> salesOrderDtlResponse = RestCaller.getSalesOrderDtl(salesOrderTransHdr);
		saleOrderListTable = FXCollections.observableArrayList(salesOrderDtlResponse.getBody());
		if (saleOrderListTable.size() == 0) {
			RestCaller.deleteSalesOrderTransHdr(salesOrderTransHdr.getId());
			salesOrderTransHdr = null;
			saleOrderListTable.clear();
			itemDetailTable.getItems().clear();
		}

	}

	private void loadHoldedSales() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/HoldedCustomer.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtBarcode.requestFocus();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	private void FillTable() {

		logger.info("=============pharmacy Sale started fill table!!=============");
		itemDetailTable.setItems(saleOrderListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		// columnCessRate.setCellValueFactory(cellData ->
		// cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getexpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		amountTotal = RestCaller.getSumofSalesOrderReceiptbySalesOrdertransHdrId(salesOrderTransHdr.getId());
		txtCashtopay.setText(Double.toString(amountTotal));

		Summary summary = null;

		if (null == salesOrderTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesOrderTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}
		if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			summary = RestCaller.getSalesWindowOrderSummaryDiscount(salesOrderTransHdr.getId());
		} else {
			summary = RestCaller.getSalesOrderSummary(salesOrderTransHdr.getId());
		}
		if (null != summary.getTotalAmount()) {
			salesOrderTransHdr.setInvoiceAmount(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
		}

	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();
	}

	private void addItem() {

		// version2.0
		/// check financial year
		btnSave.setDisable(false);
		ResponseEntity<List<FinancialYearMst>> getFinancialYear = RestCaller.getAllFinancialYear();
		if (getFinancialYear.getBody().size() == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}
		int count = RestCaller.getFinancialYearCount();
		if (count == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}
		// version2.0ends
		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("DAY_END_LOCKED_IN_WHOLESALE");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("YES")) {
				ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
				DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
				System.out.println("Sys Date before add" + SystemSetting.applicationDate);
				if (null != dayEndClosureHdr) {
					if (null != dayEndClosureHdr.getDayEndStatus()) {
						String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(),
								"yyyy-MM-dd");
						String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
						java.sql.Date prDate = java.sql.Date.valueOf(process_date);
						Date sDate = java.sql.Date.valueOf(sysdate);
						int i = prDate.compareTo(sDate);
						if (i > 0 || i == 0) {
							notifyMessage(1, " Day End Already Done for this Date !!!!");
							return;
						}
					}
				}

			}
			Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.applicationDate);

			if (!dayendtodone) {
				notifyMessage(1, "Day End should be done before changing the date !!!!");
				return;
			}
			System.out.println("Sys Date before add" + SystemSetting.applicationDate);
		}

		logger.info("Inside Pharmacy Sale add item");

		if (txtQty.getText().trim().isEmpty()) {

			notifyMessage(5, " Please Enter Quantity...!!!");
			return;

		}
		if (custname.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Enter Customer Name...!!!");
			custname.requestFocus();
			return;
		}
		if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Select Item Name...!!!");
			txtItemname.requestFocus();
			return;
		}
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Type Quantity...!!!");
			txtQty.requestFocus();
			return;
		}

		if (txtBatch.getText().trim().isEmpty() && SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("YES")) {

			notifyMessage(1, "Item Batch is not present!!!");
			txtBatch.requestFocus();
			return;

		}

//		if (null == cmbSaleType.getValue()) {
//			notifyMessage(5, " Please Select Sale Type...!!!");
//			cmbSaleType.requestFocus();
//			return;
//		}

//		if (SystemSetting.CUSTOMER_CDREDIT_PERIOD.equalsIgnoreCase("YES")) {
//			if (txtCreditPeriod.getText().trim().isEmpty() || (txtCreditPeriod.getText()).equalsIgnoreCase("0")) {
//				notifyMessage(5, " Please Enter Credit Period...!!!");
//				cmbSaleType.requestFocus();
//				return;
//			}
//		}

		ResponseEntity<UnitMst> getUnitBYItem = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

		if (!txtBarcode.getText().trim().isEmpty()) {
			if (!txtBarcode.getText().equalsIgnoreCase(getItem.getBody().getBarCode())) {
				notifyMessage(1, "Sorry... invalid item barcode!!!");
				txtBarcode.requestFocus();
				return;
			}

		}
		// ----------------------------new version 2 surya

		Double chkQty = 0.0;
		String itemId = getItem.getBody().getId();

		if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("YES") || !txtBatch.getText().trim().isEmpty()) {
			// ----------------------------new version 2 surya end

			ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId());
			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				if (null == getmulti.getBody()) {
					notifyMessage(5, "Please Add the item in Multi Unit");
					return;
				}
			}
			ArrayList items = new ArrayList();
			items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());

		//chkQty = RestCaller.getQtyFromItemBatchDtlDailyByItemIdAndQty(getItem.getBody().getId(),txtBatch.getText());
			
			
			//------------------Here   stock checking  from ItemBatchMst--------------------------------sharon --------28/06/2021----------------------------------------------------
			chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(),txtBatch.getText());
			
			
			Iterator itr = items.iterator();
			while (itr.hasNext()) {
				List element = (List) itr.next();
//			chkQty = (Double) element.get(4);
				itemId = (String) element.get(7);

			}

			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
						getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
						Double.parseDouble(txtQty.getText()));
				System.out.println(conversionQty);
				if (chkQty < conversionQty && (!SystemSetting.isNEGATIVEBILLING())) {
					notifyMessage(1, "Not in Stock!!!");
					txtQty.clear();
					txtItemname.clear();
					txtBarcode.clear();
					txtBatch.clear();
					txtRate.clear();
					txtBarcode.requestFocus();
					return;
				}
			} else if (chkQty < Double.parseDouble(txtQty.getText()) && (!SystemSetting.isNEGATIVEBILLING())) {
				notifyMessage(5, "Not in Stock!!!");
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
			// ----------------------------new version 2 surya

		}
		// ===============sharon============================
//		if (!custname.getText().trim().isEmpty()) {
//			ResponseEntity<LocalCustomerMst> resplocal = RestCaller.getLocalCustomerbyName(custname.getText());
//			if (null != resplocal.getBody()) {
//				System.out.println(resplocal.getBody());
//				salesOrderTransHdr.setLocalCustomerId(resplocal.getBody());
//				custId = resplocal.getBody().getId();
//			}
//		}
		// ===============sharon============================
		// ----------------------------new version 2 surya end

		if (null == salesOrderTransHdr || null == salesOrderTransHdr.getId()) {
			createsalesOrderTransHdr();
		}

		if (null == salesOrderTransHdr) {
			return;
		}

		// ----------------------------new version 2 surya

		if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO") && txtBatch.getText().trim().isEmpty()
				&& (!SystemSetting.isNEGATIVEBILLING())) {

			ShowItemStockPopup(salesTransHdr, getItem.getBody().getId());

			ResponseEntity<List<SalesOrderDtl>> respentityList = RestCaller.getSalesOrderDtl(salesOrderTransHdr);

			List<SalesOrderDtl> salesOrderDtlList = respentityList.getBody();
			saleOrderListTable.clear();
			saleOrderListTable.setAll(salesOrderDtlList);
			FillTable();

			txtItemname.setText("");
			txtBarcode.setText("");
			txtQty.setText("");
			txtRate.setText("");

			cmbUnit.getSelectionModel().clearSelection();
			txtItemcode.setText("");
			txtBatch.setText("");
			txtBarcode.requestFocus();
			txtWarranty.clear();
			salesOrderDtl = new SalesOrderDtl();
			return;
		}

		// ----------------------------new version 2 surya end

		String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();

		Double itemsqty = 0.0;
		ResponseEntity<List<SalesOrderDtl>> getsalesOrderDtl = RestCaller
				.getSalesOrderDtlByItemAndBatch(salesOrderTransHdr.getId(), getItem.getBody().getId(), batch);
		saleOrderListItemTable = FXCollections.observableArrayList(getsalesOrderDtl.getBody());
		if (saleOrderListItemTable.size() > 1) {
			Double PrevQty = 0.0;
			for (SalesOrderDtl saleDtl : saleOrderListItemTable) {
				if (!saleDtl.getUnitId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
							getItem.getBody().getUnitId(), saleDtl.getQty());
				} else {
					PrevQty = saleDtl.getQty();
				}
				itemsqty = itemsqty + PrevQty;
			}
		} else {
			itemsqty = RestCaller.SalesOrderDtlItemQty(salesOrderTransHdr.getId(), itemId, txtBatch.getText());
		}
		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if (chkQty < itemsqty + conversionQty && (!SystemSetting.isNEGATIVEBILLING())) {
				notifyMessage(1, "Not in Stock!!!");
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
		}

		else if (chkQty < itemsqty + Double.parseDouble(txtQty.getText()) && (!SystemSetting.isNEGATIVEBILLING())) {
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();
			notifyMessage(5, "No Stock!!");
			return;
		}
		if (null == salesOrderDtl) {
			salesOrderDtl = new SalesOrderDtl();
		}
		if (null != salesOrderDtl.getId()) {

			RestCaller.deleteSalesOrderDtl(salesOrderDtl.getId());

			logger.info("Pharmacy Sale delete sales Dtl completed!!");

			if (null != salesOrderDtl.getSchemeId()) {
				return;
			}

		}

		salesOrderDtl.setSalesOrderTransHdr(salesOrderTransHdr);
		salesOrderDtl.setItemName(txtItemname.getText());
		salesOrderDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());

		System.out.println(batch);
		salesOrderDtl.setBatchCode(batch);

		logger.info("Pharmacy Sale amount Calculation Started!!");
		Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());

		salesOrderDtl.setQty(qty);

		salesOrderDtl.setItemCode(txtItemcode.getText());
		Double mrpRateIncludingTax = 00.0;
		logger.info("Fetching tax");
		mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
		logger.info("Fetching tax finished");
		salesOrderDtl.setMrp(mrpRateIncludingTax);

		Double taxRate = 00.0;

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesOrderDtl.getItemName()); // itemmst
																													// =
		ItemMst item = respsentity.getBody();
		salesOrderDtl.setBarcode(item.getBarCode());
//		salesOrderDtl.setUnitName(unitMst.getUnitName());
		salesOrderDtl.setStandardPrice(item.getStandardPrice());

		salesOrderDtl.setListPrice(mrpRateIncludingTax);

		if (null == salesOrderTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesOrderTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}

		if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double discount = 0.0;
			discount = mrpRateIncludingTax
					- (mrpRateIncludingTax * salesOrderTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			salesOrderDtl.setListPrice(discount);
		}

//		if(null != item.getTaxRate())
//		{
//			salesOrderDtl.setTaxRate(item.getTaxRate());
//			taxRate = item.getTaxRate();
//
//		} else {
//			salesOrderDtl.setTaxRate(0.0);
//		}
		salesOrderDtl.setItemId(item.getId());

		if (!txtBatch.getText().trim().isEmpty()) {
			ResponseEntity<List<ItemBatchExpiryDtl>> batchExpiryDtlResp = RestCaller
					.getItemBatchExpByItemAndBatch(salesOrderDtl.getItemId(), salesOrderDtl.getBatch());

			List<ItemBatchExpiryDtl> itemBatchExpiryDtlList = batchExpiryDtlResp.getBody();
			if (itemBatchExpiryDtlList.size() > 0 && null != itemBatchExpiryDtlList) {
				ItemBatchExpiryDtl itemBatchExpiryDtl = itemBatchExpiryDtlList.get(0);

				if (null != itemBatchExpiryDtl) {
					String expirydate = SystemSetting.UtilDateToString(itemBatchExpiryDtl.getExpiryDate(),
							"yyyy-MM-dd");
					salesOrderDtl.setexpiryDate(java.sql.Date.valueOf(expirydate));
				}
			}
		}

		ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
		salesOrderDtl.setUnitId(getUnit.getBody().getId());
		salesOrderDtl.setUnitName(getUnit.getBody().getUnitName());

		// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());

		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesOrderDtl.getItemId());
		if (getTaxMst.getBody().size() > 0) {
			for (TaxMst taxMst : getTaxMst.getBody()) {

				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesOrderTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {
					logger.info(e.toString());

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesOrderDtl.setCgstTaxRate(taxMst.getTaxRate());
//						BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesOrderDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesOrderDtl.setSgstTaxRate(taxMst.getTaxRate());
//						BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesOrderDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesOrderDtl.setIgstTaxRate(0.0);
					salesOrderDtl.setIgstAmount(0.0);
					ResponseEntity<TaxMst> taxMst1 = RestCaller.getTaxMstByItemIdAndTaxId(salesOrderDtl.getItemId(),
							"IGST");
					if (null != taxMst1.getBody()) {

						salesOrderDtl.setTaxRate(taxMst1.getBody().getTaxRate());
					}
				} else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesOrderDtl.setCgstTaxRate(0.0);
						salesOrderDtl.setCgstAmount(0.0);
						salesOrderDtl.setSgstTaxRate(0.0);
						salesOrderDtl.setSgstAmount(0.0);

						salesOrderDtl.setTaxRate(taxMst.getTaxRate());
						salesOrderDtl.setIgstTaxRate(taxMst.getTaxRate());
//							BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesOrderDtl.setIgstAmount(igstAmount.doubleValue());
					}
				}
				if (salesOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesOrderDtl.setCessRate(taxMst.getTaxRate());
//						BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesOrderDtl.setCessAmount(cessAmount.doubleValue());
					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesOrderDtl.setAddCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//							Double.valueOf(txtRate.getText()));
//					salesOrderDtl.setAddCessAmount(cessAmount.doubleValue());
				}

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + salesOrderDtl.getIgstTaxRate()
					+ salesOrderDtl.getCessRate() + salesOrderDtl.getAddCessRate() + salesOrderDtl.getSgstTaxRate()
					+ salesOrderDtl.getCgstTaxRate());
			salesOrderDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = RestCaller.TaxCalculator(salesOrderDtl.getIgstTaxRate(), rateBeforeTax);
			salesOrderDtl.setIgstAmount(igstAmount.doubleValue() * salesOrderDtl.getQty());
			BigDecimal SgstAmount = RestCaller.TaxCalculator(salesOrderDtl.getSgstTaxRate(), rateBeforeTax);
			salesOrderDtl.setSgstAmount(SgstAmount.doubleValue() * salesOrderDtl.getQty());
			BigDecimal CgstAmount = RestCaller.TaxCalculator(salesOrderDtl.getCgstTaxRate(), rateBeforeTax);
			salesOrderDtl.setCgstAmount(CgstAmount.doubleValue() * salesOrderDtl.getQty());
			BigDecimal cessAmount = RestCaller.TaxCalculator(salesOrderDtl.getCessRate(), rateBeforeTax);
			salesOrderDtl.setCessAmount(cessAmount.doubleValue() * salesOrderDtl.getQty());
			BigDecimal addcessAmount = RestCaller.TaxCalculator(salesOrderDtl.getAddCessRate(), rateBeforeTax);
			salesOrderDtl.setAddCessAmount(addcessAmount.doubleValue() * salesOrderDtl.getQty());
		}

		else {

// verificed

			if (getUnit.getBody().getId().equalsIgnoreCase(item.getUnitId())) {
				salesOrderDtl.setStandardPrice(item.getStandardPrice());
			} else {

				ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),
						getUnit.getBody().getId());
				MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
				salesOrderDtl.setStandardPrice(multiUnitMst.getPrice());

			}
			if (null != item.getTaxRate()) {
				salesOrderDtl.setTaxRate(item.getTaxRate());
				taxRate = item.getTaxRate();

			} else {
				salesOrderDtl.setTaxRate(0.0);
			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);

			// if Discount

			// Calculate discount on base price
			if (null != salesOrderTransHdr.getAccountHeads().getDiscountProperty()) {

				if (salesOrderTransHdr.getAccountHeads().getDiscountProperty()
						.equalsIgnoreCase("ON BASIS OF BASE PRICE")) {
					calcDiscountOnBasePrice(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);

				}
				if (salesOrderTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP")) {
//				salesOrderDtl.setTaxRate(0.0);
					calcDiscountOnMRP(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);
				}
				if (salesOrderTransHdr.getAccountHeads().getDiscountProperty()
						.equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX")) {
					ambrossiaDiscount(salesOrderTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);
				}
			} else {

				double cessAmount = 0.0;
				double cessRate = 0.0;
				salesOrderDtl.setRate(rateBeforeTax);
				if (salesOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (item.getCess() > 0) {
						cessRate = item.getCess();
						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

						salesOrderDtl.setRate(rateBeforeTax);
						cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;
					} else {
						cessAmount = 0.0;
						cessRate = 0.0;
					}
					salesOrderDtl.setRate(rateBeforeTax);
					salesOrderDtl.setCessRate(cessRate);
					salesOrderDtl.setCessAmount(cessAmount);
					salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);
				}

				// salesOrderDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesOrderDtl.setCgstTaxRate(cgstTaxRate);

				salesOrderDtl.setSgstTaxRate(sgstTaxRate);
				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesOrderTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {
					logger.info(e.toString());

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					salesOrderDtl.setSgstTaxRate(taxRate / 2);

					salesOrderDtl.setCgstTaxRate(taxRate / 2);

					Double cgstAmt = 0.0, sgstAmt = 0.0;
					cgstAmt = salesOrderDtl.getCgstTaxRate() * salesOrderDtl.getQty() * salesOrderDtl.getRate() / 100;
					BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
					bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesOrderDtl.setCgstAmount(bdCgstAmt.doubleValue());
					sgstAmt = salesOrderDtl.getSgstTaxRate() * salesOrderDtl.getQty() * salesOrderDtl.getRate() / 100;
					BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
					bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesOrderDtl.setSgstAmount(bdsgstAmt.doubleValue());

					salesOrderDtl.setIgstTaxRate(0.0);
					salesOrderDtl.setIgstAmount(0.0);

				} else {
					salesOrderDtl.setSgstTaxRate(0.0);

					salesOrderDtl.setCgstTaxRate(0.0);

					salesOrderDtl.setCgstAmount(0.0);

					salesOrderDtl.setSgstAmount(0.0);

					salesOrderDtl.setIgstTaxRate(taxRate);
					salesOrderDtl.setIgstAmount(
							salesOrderDtl.getIgstTaxRate() * salesOrderDtl.getQty() * salesOrderDtl.getRate() / 100);

				}
			}
		}

		String companyState = SystemSetting.getUser().getCompanyMst().getState();
		String customerState = "KERALA";
		try {
			customerState = salesOrderTransHdr.getAccountHeads().getCustomerState();
		} catch (Exception e) {
			logger.info(e.toString());

		}

		if (null == customerState) {
			customerState = "KERALA";
		}

		if (null == companyState) {
			companyState = "KERALA";
		}
		String gstType = RestCaller.getGSTTypeByCustomerState(customerState);
		if (gstType.equalsIgnoreCase("GST")) {
			salesOrderDtl.setSgstTaxRate(taxRate / 2);

			salesOrderDtl.setCgstTaxRate(taxRate / 2);

			Double cgstAmt = 0.0, sgstAmt = 0.0;
			cgstAmt = salesOrderDtl.getCgstTaxRate() * salesOrderDtl.getQty() * salesOrderDtl.getRate() / 100;
			BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
			bdcgstAmt = bdcgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setCgstAmount(bdcgstAmt.doubleValue());
			sgstAmt = salesOrderDtl.getSgstTaxRate() * salesOrderDtl.getQty() * salesOrderDtl.getRate() / 100;
			BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
			bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setSgstAmount(bdsgstAmt.doubleValue());

			salesOrderDtl.setIgstTaxRate(0.0);
			salesOrderDtl.setIgstAmount(0.0);

		} else {
			salesOrderDtl.setSgstTaxRate(0.0);

			salesOrderDtl.setCgstTaxRate(0.0);

			salesOrderDtl.setCgstAmount(0.0);

			salesOrderDtl.setSgstAmount(0.0);

			salesOrderDtl.setIgstTaxRate(taxRate);
			salesOrderDtl.setIgstAmount(
					salesOrderDtl.getIgstTaxRate() * salesOrderDtl.getQty() * salesOrderDtl.getRate() / 100);

		}

		BigDecimal settoamount = new BigDecimal((Double.parseDouble(txtQty.getText()) * salesOrderDtl.getRate()));

		double includingTax = (settoamount.doubleValue() * salesOrderDtl.getTaxRate()) / 100;
		double amount = settoamount.doubleValue() + includingTax + salesOrderDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesOrderDtl.setAmount(setamount.doubleValue());
//		}
//		else
//		{
//			BigDecimal settoamount = new BigDecimal(
//					Double.parseDouble(txtQty.getText()) *salesOrderDtl.getMrp() );
//			settoamount = settoamount.setScale(2, BigDecimal.ROUND_CEILING);
//			salesOrderDtl.setAmount(settoamount.doubleValue());
//		}
//		if (getUnit.getBody().getId().equalsIgnoreCase(item.getUnitId())) {
//			salesOrderDtl.setStandardPrice(item.getStandardPrice());
//		} else {
//
//			ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),
//					getUnit.getBody().getId());
//			MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
//			salesOrderDtl.setStandardPrice(multiUnitMst.getPrice());
//
//		}
		salesOrderDtl.setAddCessRate(0.0);
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp1 = RestCaller.getPriceDefenitionMstByName("MRP");
		PriceDefenitionMst priceDefenitionMst1 = priceDefenitionMstResp1.getBody();
		if (null != priceDefenitionMst1) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			/*
			 * fetch for batchprice definition
			 */
			ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller.getBatchPriceDefinition(
					salesOrderDtl.getItemId(), priceDefenitionMst1.getId(), salesOrderDtl.getUnitId(),
					salesOrderDtl.getBatch(), sdate);
			if (null != batchpriceDef.getBody()) {
				salesOrderDtl.setMrp(batchpriceDef.getBody().getAmount());
			} else {
				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						salesOrderDtl.getItemId(), priceDefenitionMst1.getId(), salesOrderDtl.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();
				if (null != priceDefinition) {
					salesOrderDtl.setMrp(priceDefinition.getAmount());
				}
			}
		}

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if (null != priceDefenitionMst) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller.getBatchPriceDefinition(
					salesOrderDtl.getItemId(), priceDefenitionMst.getId(), salesOrderDtl.getUnitId(),
					salesOrderDtl.getBatch(), sdate);
			if (null != batchpriceDef.getBody()) {
				salesOrderDtl.setCostPrice(batchpriceDef.getBody().getAmount());
			} else {
				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						salesOrderDtl.getItemId(), priceDefenitionMst.getId(), salesOrderDtl.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();
				if (null != priceDefinition) {
					salesOrderDtl.setCostPrice(priceDefinition.getAmount());
				}
			}
		}

		// -------------------new version 1.4

		if (!txtWarranty.getText().trim().isEmpty()) {
			salesOrderDtl.setWarrantySerial(txtWarranty.getText());
		}

		// -------------------new version 1.4 end

		String logdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
		ResponseEntity<SalesOrderDtl> respentity = RestCaller.saveSalesOrderDtlWithLoginDate(salesOrderDtl, logdate);
//		//---------------offer---------------

		logger.info("Pharmacy Sale Save sals Dtl COMPLETED!!");
		ResponseEntity<List<SalesOrderDtl>> respentityList = RestCaller
				.getSalesOrderDtl(salesOrderDtl.getSalesOrderTransHdr());

		List<SalesOrderDtl> salesOrderDtlList = respentityList.getBody();

		/*
		 * Call Rest to get the summary and set that to the display fields
		 */

		// salesOrderDtl.setTempAmount(amount);
		saleOrderListTable.clear();
		saleOrderListTable.setAll(salesOrderDtlList);

		// ResponseEntity<salesOrderDtl> respentity =
		// RestCaller.savesalesOrderDtl(salesOrderDtl);
		// salesOrderDtl = respentity.getBody();

		// saleListTable.add(salesOrderDtl);

		FillTable();

		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");

		cmbUnit.getSelectionModel().clearSelection();
		txtItemcode.setText("");
		txtBatch.setText("");
		txtBarcode.requestFocus();
		txtWarranty.clear();
		salesOrderDtl = new SalesOrderDtl();
		logger.info("====================Pharmacy Sale ADD ITEM FINISHED!!================");

	}

	// ----------------------------new version 2 surya

	private void ShowItemStockPopup(SalesTransHdr salesOrderTransHdr, String itemid) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemWiseStockPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			ItemWiseStockPopupCtl itemWiseStockPopupCtl = fxmlLoader.getController();
			itemWiseStockPopupCtl.LoadItemPopupBySearch(itemid, salesOrderTransHdr);

			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}
	// ----------------------------new version 2 surya end

	private void ambrossiaDiscount(SalesOrderTransHdr salesOrderTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {

		System.out.print("mrpRateIncludingTax" + mrpRateIncludingTax);

		if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discountAmount = (mrpRateIncludingTax * salesOrderTransHdr.getAccountHeads().getCustomerDiscount())
					/ 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

//		 

			System.out.println("BrateAfterDiscount*****************" + BrateAfterDiscount);
			System.out.println("discountAmount*****************" + discountAmount);
//		
			salesOrderDtl.setDiscount(discountAmount);

			salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);

			double newRate = BrateAfterDiscount.doubleValue();
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate));

			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			salesOrderDtl.setRate(BrateBeforeTax.doubleValue());

			// salesOrderDtl.setMrp(Double.parseDouble(txtRate.getText()));
			// salesOrderDtl.setStandardPrice(BrateBeforeTax.doubleValue());
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
			salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				double discountAmount = (mrpRateIncludingTax * salesOrderTransHdr.getAccountHeads().getCustomerDiscount())
						/ 100;
				BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

//			
				salesOrderDtl.setDiscount(discountAmount);

				double newRate = BrateAfterDiscount.doubleValue();
				BigDecimal BnewRate = new BigDecimal(newRate);
				BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate + cessRate));

				BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesOrderDtl.setRate(BrateBeforeTax.doubleValue());

				// salesOrderDtl.setMrp(Double.parseDouble(txtRate.getText()));
				// salesOrderDtl.setStandardPrice(BrateBeforeTax.doubleValue());

				System.out.println("rateBeforeTax---------" + BrateBeforeTax);

//				if (salesOrderTransHdr.getCustomerMst().getCustomerDiscount() > 0) {
//					Double rateAfterDiscount = (100 * rateBeforeTax)
//							/ (100 + salesOrderTransHdr.getCustomerMst().getCustomerDiscount());
//					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
//					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					
//					
//					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
//					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
//					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					// salesOrderDtl.setMrp(rateBefrTax.doubleValue());
//					salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
//				} else {
//					salesOrderDtl.setRate(rateBeforeTax);
//				}

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);
		salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);

	}

	private void calcDiscountOnBasePrice(SalesOrderTransHdr salesOrderTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesOrderTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);
			// salesOrderDtl.setMrp(rateBefrTax.doubleValue());
			// salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
			salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesOrderTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesOrderDtl.setMrp(rateBefrTax.doubleValue());
					// salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesOrderDtl.setRate(rateBeforeTax);
					salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);
				}
				// salesOrderDtl.setRate(rateBeforeTax);

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);
		salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);

	}

	private void calcDiscountOnMRP(SalesOrderTransHdr salesOrderTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * mrpRateIncludingTax)
					/ (100 + salesOrderTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRate(BnewrateBeforeTax.doubleValue());
			salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);
			// BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			// rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
			// salesOrderDtl.setMrp(BrateAfterDiscount.doubleValue());
			// salesOrderDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
			salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * mrpRateIncludingTax)
							/ (100 + salesOrderTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesOrderDtl.setMrp(mrpRateIncludingTax);
					// salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesOrderDtl.setRate(rateBeforeTax);
					salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);
				}
				// salesOrderDtl.setRate(rateBeforeTax);

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);
		salesOrderDtl.setRateBeforeDiscount(rateBeforeTax);

	}

	private void showPopup() {

		// ----------------------------new version 2 surya

		if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("YES")) {

			// ----------------------------new version 2 surya end

			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
				// fxmlLoader.setController(itemStockPopupCtl);
				Parent root1;
				ItemStockPopupCtl itemStockPopupCtl = fxmlLoader.getController();
				itemStockPopupCtl.windowName = "WHOLESALE";
				root1 = (Parent) fxmlLoader.load();
				if (!custname.getText().trim().isEmpty()) {
					ItemStockPopupCtl itemStockPopupCtl1 = fxmlLoader.getController();
					itemStockPopupCtl1.getCustomer(custname.getText());
				}

				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} catch (IOException e) {
				//
				e.printStackTrace();
			}
			// ----------------------------new version 2 surya

		} else {
			try {
				System.out.println("STOCK POPUP");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				// loader.setController(itemPopupCtl);
				Parent root = loader.load();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				// stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				txtQty.requestFocus();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// ----------------------------new version 2 surya end

		txtQty.requestFocus();
	}

	@FXML
	void AddItemOnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}

	}

	private void loadCustomerPopup() {

		if (salesOrderTransHdr != null) {

			Alert a = new Alert(AlertType.CONFIRMATION);
			a.setHeaderText("Changing Customer...");
			a.setContentText("The details will be deleted");
			a.showAndWait().ifPresent((btnType) -> {
				if (btnType == ButtonType.OK) {

					deleteAllsalesOrderDtl();

					try {
						txtLocalCustomer.clear();
						FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
						Parent root1;

						root1 = (Parent) fxmlLoader.load();
						Stage stage = new Stage();

						stage.initModality(Modality.APPLICATION_MODAL);
						stage.initStyle(StageStyle.UNDECORATED);
						stage.setTitle("ABC");
						stage.initModality(Modality.APPLICATION_MODAL);
						stage.setScene(new Scene(root1));
						stage.show();

						txtItemname.requestFocus();

					} catch (IOException e) {
						//
						e.printStackTrace();
					}

				} else if (btnType == ButtonType.CANCEL) {

					return;

				}
			});
//			Boolean confirm = confirmMessage();

		} else {
			try {

				txtLocalCustomer.clear();
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

				txtItemname.requestFocus();

			} catch (IOException e) {
				//
				e.printStackTrace();
			}
		}
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
		// ----------------------------new version 2 surya

		if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("YES")) {
			// ----------------------------new version 2 surya end

			double applicableDiscount = 0.0;
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
			ItemMst item = new ItemMst();
			item = getItem.getBody();

//			item.setRank(item.getRank() + 1);
//			RestCaller.updateRankItemMst(item);
			Stage stage = (Stage) btnAdditem.getScene().getWindow();

			if (stage.isShowing()) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						cmbUnit.getItems().clear();

						Platform.runLater(() -> {
							itemNameProperty.set(itemPopupEvent.getItemName());

						});

						Platform.runLater(() -> {
							txtBarcode.setText(itemPopupEvent.getBarCode());

						});
						Platform.runLater(() -> {
							txtRate.setText(Double.toString(itemPopupEvent.getMrp()));

						});
						Platform.runLater(() -> {
							cmbUnit.getItems().add(itemPopupEvent.getUnitName());
							cmbUnit.setValue(itemPopupEvent.getUnitName());
						});

						Platform.runLater(() -> {
							batchProperty.set(itemPopupEvent.getBatch());

						});

						ResponseEntity<AccountHeads> custMst = RestCaller.getAccountHeadsById(custId);

						java.util.Date udate = SystemSetting.getApplicationDate();
						String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

						ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
								itemPopupEvent.getItemId(), custMst.getBody().getPriceTypeId(),
								itemPopupEvent.getUnitId(), itemPopupEvent.getBatch(), sdate);
						if (null != batchPriceDef.getBody()) {
							Platform.runLater(() -> {
								txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));

							});

						} else {
							ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
									itemPopupEvent.getItemId(), custMst.getBody().getPriceTypeId(),
									itemPopupEvent.getUnitId(), sdate);

							if (null != pricebyItem.getBody()) {

								// version 2.3
								Platform.runLater(() -> {
									txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));

								});
								// version2.3ends

								ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp2 = RestCaller
										.getPriceDefenitionMstByName("MRP");
								if (null != priceDefenitionMstResp2.getBody()) {
									if (!pricebyItem.getBody().getPriceId()
											.equalsIgnoreCase(priceDefenitionMstResp2.getBody().getId()))
										Platform.runLater(() -> {
											txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));

										});

//						else
//						{
//							txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
//						}
								}
							}
						}
						ResponseEntity<List<BatchPriceDefinition>> batchPrice = RestCaller
								.getBatchPriceDefinitionByItemId(itemPopupEvent.getItemId(),
										custMst.getBody().getPriceTypeId(), txtBatch.getText(), sdate);
						BatchpriceDefenitionList = FXCollections.observableArrayList(batchPrice.getBody());
						if (BatchpriceDefenitionList.size() > 0) {
							for (BatchPriceDefinition priceDef : BatchpriceDefenitionList) {
								ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
								if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {

									Platform.runLater(() -> {
										cmbUnit.getItems().add(getUnit.getBody().getUnitName());
										cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
									});

								}
							}
						}

						else {

							ResponseEntity<List<PriceDefinition>> price = RestCaller
									.getPriceByItemId(itemPopupEvent.getItemId(), custMst.getBody().getPriceTypeId());
							priceDefenitionList = FXCollections.observableArrayList(price.getBody());

							if (priceDefenitionList.size() > 0) {
								// cmbUnit.getItems().clear();
								for (PriceDefinition priceDef : priceDefenitionList) {
									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
									if (!getUnit.getBody().getUnitName()
											.equalsIgnoreCase(itemPopupEvent.getUnitName())) {
										Platform.runLater(() -> {
											cmbUnit.getItems().add(getUnit.getBody().getUnitName());
											cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
										});

									}
								}
							}
						}

						ResponseEntity<TaxMst> taxMst = RestCaller.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(),
								"IGST");
						if (null != taxMst.getBody()) {
						}

						if (null != itemPopupEvent.getExpiryDate()) {
							// salesOrderDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
						}

//				if(custMst.getBody().getCustomerDiscount()>0)
//				{
//					double discount;
//					discount =Double.parseDouble(txtRate.getText())*(custMst.getBody().getCustomerDiscount()/100);
//					applicableDiscount = Double.parseDouble(txtRate.getText())-discount;
//					txtRate.setText(Double.toString(applicableDiscount));
//				}
//				
						if (null != itemPopupEvent.getItemPriceLock()) {
							if (itemPopupEvent.getItemPriceLock().equalsIgnoreCase("YES")) {

								Platform.runLater(() -> {
									txtRate.setEditable(false);

								});
							} else {

								Platform.runLater(() -> {
									txtRate.setEditable(true);

								});
							}
						}

						System.out.println(itemPopupEvent.toString());
					}
				});
			}
			// ----------------------------new version 2 surya

		} else {

			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
			ItemMst item = getItem.getBody();
			if (null != item.getItemName()) {
				Platform.runLater(() -> {
					txtItemname.setText(item.getItemName());

				});

			}

			if (null != item.getBarCode()) {
				Platform.runLater(() -> {
					txtBarcode.setText(item.getBarCode());

				});

			}

			ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(item.getUnitId());
			UnitMst unitMst = unitMstResp.getBody();
			if (null != unitMst) {
				if (null != cmbUnit) {

					Platform.runLater(() -> {
						// weightfromMachine.set( voucherNoEvent.getWeightData().trim());
						cmbUnit.getItems().add(unitMst.getUnitName());
					});

					if (null != cmbUnit.getSelectionModel()) {
						if (null != unitMst.getUnitName()) {

							Platform.runLater(() -> {
								// weightfromMachine.set( voucherNoEvent.getWeightData().trim());
								cmbUnit.getSelectionModel().select(unitMst.getUnitName());
							});

						}
					}
				}
			}

			if (null != itemPopupEvent.getMrp()) {

				Platform.runLater(() -> {
					// weightfromMachine.set( voucherNoEvent.getWeightData().trim());
					txtRate.setText(item.getStandardPrice().toString());
				});

			}

			ArrayList itemBatch = new ArrayList();
			String logDate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
			itemBatch = RestCaller.SearchStockItemByBatchAndItemId(item.getId(), logDate);

			if (itemBatch.size() == 1) {
				Iterator itr = itemBatch.iterator();
				while (itr.hasNext()) {
					List element = (List) itr.next();

					Platform.runLater(() -> {
						String batch = (String) element.get(9);
						txtBatch.setText(batch);
					});

				}

			}

		}

		// ----------------------------new version 2 surya end

	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadsById(customerEvent.getCustId());
		AccountHeads accounthead = new AccountHeads();
		accounthead = getAccountHeads.getBody();
//		custmrMst.setRank(custmrMst.getRank() + 1);
//		RestCaller.updateCustomerRank(custmrMst);

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			String customerSite = SystemSetting.customer_site_selection;
			if (customerSite.equalsIgnoreCase("TRUE")) {
				txtLocalCustomer.setDisable(false);
				txtLocalCustomer.clear();
			}

			String sdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
			custname.setText(customerEvent.getCustomerName());
			ResponseEntity<BranchMst> branchMst = RestCaller.getBranchMstByName(customerEvent.getCustomerName());

//			if (null != branchMst.getBody()) {
//				txtSalesType.setText("Branch Sales");
//			} else {
//				txtSalesType.setText("Customer Sales");
//			}
			custAdress.setText(customerEvent.getCustomerAddress());
			gstNo.setText(customerEvent.getCustomerGst());
			custId = customerEvent.getCustId();

			if (SystemSetting.CUSTOMER_CDREDIT_PERIOD.equalsIgnoreCase("YES")) {
				// txtCreditPeriod.setVisible(true);
				lblCreditPeriod.setVisible(true);
				// txtCreditPeriod.setText(customerEvent.getCreditPeriod().toString());
			}

			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsById(custId);
			ResponseEntity<PriceDefenitionMst> priceDef = RestCaller
					.getPriceNameById(accountHeads.getBody().getPriceTypeId());
			if (null != priceDef.getBody()) {
				txtPriceType.setText(priceDef.getBody().getPriceLevelName());

			} else {
				txtPriceType.clear();

			}
			ResponseEntity<BranchMst> branchMstResp = RestCaller.getBranchMstByName(customerEvent.getCustomerName());

			if (null != branchMstResp.getBody()) {
				customerIsBranch = true;
			}

			// ResponseEntity<T>
			Double custBalance = RestCaller.getCustomerBalance(custId, sdate);
			BigDecimal bCustBalance = new BigDecimal(custBalance);
			bCustBalance = bCustBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			txtPreviousBalance.setText(bCustBalance.toString());
		}

	}

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private boolean isCustomerABranch(String customerName) {
		ArrayList branchRest = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		branchRest = RestCaller.SearchBranchLocalhost();
		Iterator itr1 = branchRest.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			System.out.println("---branchRest---" + lm);
			String branchName = (String) lm.get("branchName");
			if (customerName.equalsIgnoreCase(branchName)) {
				return true;
			}

		}

		return false;
	}

	@FXML
	void searchAction(ActionEvent event) {
	}

	@FXML
	void itemNameClick(MouseEvent event) {
//		if (null == cmbSaleType.getSelectionModel() || null == cmbSaleType.getSelectionModel().getSelectedItem()) {
//			notifyMessage(3, "Select Voucher Type");
//			cmbSaleType.requestFocus();
//			return;
//		}
		if (custname.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Customer");
			custname.requestFocus();
			return;
		}

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategorywiseItemSearch.fxml"));
			Parent root1;
			CategorywiseItemSearchCtl categorywiseItemSearchCtl = loader.getController();
			categorywiseItemSearchCtl.customerName = custname.getText();
//			if (null != cmbSaleType.getSelectionModel()) {
//				categorywiseItemSearchCtl.voucherType = cmbSaleType.getSelectionModel().getSelectedItem();
//			}
			categorywiseItemSearchCtl.customerIsBranch = customerIsBranch;
			categorywiseItemSearchCtl.txtLocalCustomer = txtLocalCustomer.getText();
			categorywiseItemSearchCtl.localCustId = localCustId;
			categorywiseItemSearchCtl.windowType = "WHOLESALE";
			if (null == salesOrderTransHdr) {
				createsalesOrderTransHdr();
				categorywiseItemSearchCtl.salesTransHdr = salesTransHdr;
			} else {
				categorywiseItemSearchCtl.salesTransHdr = salesTransHdr;
			}
			root1 = (Parent) loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root1));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

	}

	@Subscribe
	public void categoryItemSearchListener(CategorywiseItemSearchEvent categorywiseItemSearchEvent) {

		{
			salesOrderTransHdr = categorywiseItemSearchEvent.getSalesOrderTransHdrId();
			ResponseEntity<List<SalesOrderDtl>> respentityList = RestCaller.getSalesOrderDtl(salesOrderTransHdr);

			List<SalesOrderDtl> salesOrderDtlList = respentityList.getBody();

			/*
			 * Call Rest to get the summary and set that to the display fields
			 */

			// salesOrderDtl.setTempAmount(amount);
			saleOrderListTable.clear();
			saleOrderListTable.setAll(salesOrderDtlList);
			FillTable();
		}

	}

	private void createsalesOrderTransHdr() {

		salesOrderTransHdr = new SalesOrderTransHdr();
		salesOrderTransHdr.setInvoiceAmount(0.0);
		salesOrderTransHdr.getId();

		// salesOrderTransHdr.setVoucherType(cmbSaleType.getSelectionModel().getSelectedItem());

		logger.info("===========Pharmacy Sale get customer by Id in Add item!!");
		ResponseEntity<AccountHeads> accountHeadsResponce = RestCaller.getAccountHeadsById(custId);

		AccountHeads accountHeads = accountHeadsResponce.getBody();
		if (null == accountHeads) {
			return;
		}

		salesOrderTransHdr.setAccountHeads(accountHeads);

		if (null != accountHeads.getCustomerDiscount()) {
			salesOrderTransHdr.setDiscount((accountHeads.getCustomerDiscount().toString()));
		} else {
			salesOrderTransHdr.setDiscount("0");
		}

		if (!txtLocalCustomer.getText().trim().isEmpty()) {
			ResponseEntity<LocalCustomerMst> LocalCustomerMstResp = RestCaller.getLocalCustomerById(localCustId);
			LocalCustomerMst localCustomerMst = LocalCustomerMstResp.getBody();
			if (null != localCustomerMst) {
				salesOrderTransHdr.setLocalCustomerMst(localCustomerMst);
			}

		} else {
			ResponseEntity<LocalCustomerMst> localCustomerResp = RestCaller.getLocalCustomerbyName("LOCALCUSTOMER");
			LocalCustomerMst localCustomerMst = localCustomerResp.getBody();

			salesOrderTransHdr.setLocalCustomerId(localCustomerMst);
		}

		/*
		 * If Customer Has a valid GST , then B2B Invoice , otherwise its B2C
		 */
		if (null == accountHeads.getPartyGst() || accountHeads.getPartyGst().length() < 13) {
			salesOrderTransHdr.setSalesMode("B2C");
		} else {
			salesOrderTransHdr.setSalesMode("B2B");
		}

		salesOrderTransHdr.setCreditOrCash("CREDIT");
		salesOrderTransHdr.setCustomiseSalesMode("WHOLESALEWINDOW");
		salesOrderTransHdr.setUserId(SystemSetting.getUser().getId());
		salesOrderTransHdr.setBranchCode(SystemSetting.systemBranch);
		LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);

		String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
		Date date = SystemSetting.StringToSqlDateSlash(sysdate, "yyyy-MM-dd");
		salesOrderTransHdr.setVoucherDate(date);
		if (customerIsBranch) {
			salesOrderTransHdr.setIsBranchSales("Y");
		} else {
			salesOrderTransHdr.setIsBranchSales("N");
		}
		logger.info("==============Pharmacy Sale save Sales trans Hdr started!!");
		String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
//
//		if (!txtCreditPeriod.getText().trim().isEmpty()) {
//			salesOrderTransHdr.setCreditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
//		}

		if (null != cmbSalesMan.getSelectionModel().getSelectedItem()) {
			ResponseEntity<List<SalesManMst>> salesManMstResp = RestCaller
					.getSalesManMstByName(cmbSalesMan.getSelectionModel().getSelectedItem().toString());

			List<SalesManMst> salesManList = salesManMstResp.getBody();

			if (salesManList.size() > 0) {
				if (null != salesManList.get(0)) {
					if (null != salesManList.get(0).getId()) {
						salesOrderTransHdr.setSalesManId(salesManList.get(0).getId());

					} else {
						notifyMessage(3, "Invalid salesman...!");
						return;
					}

				} else {
					notifyMessage(3, "Invalid salesman...!");
					return;
				}

			} else {
				notifyMessage(3, "Invalid salesman...!");
				return;

			}
		}

		ResponseEntity<SalesOrderTransHdr> getsales = RestCaller
				.getNullSalesOrderTransHdrByCustomer(salesOrderTransHdr.getId(), sdate, "WHOLESALEWINDOW");
		if (null != getsales.getBody()) {
			salesOrderTransHdr = getsales.getBody();
		} else {

			ResponseEntity<SalesOrderTransHdr> respentity = RestCaller.saveSalesOrderHdr(salesOrderTransHdr);
			logger.info("Pharmacy Sale save Sales trans Hdr completed!!");
			salesOrderTransHdr = respentity.getBody();
		}

	}

	@Subscribe
	public void batchWiseSalesListner(String sales) {
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			if (sales.equalsIgnoreCase("SALES")) {
				ResponseEntity<List<SalesOrderDtl>> respentityList = RestCaller.getSalesOrderDtl(salesOrderTransHdr);

				List<SalesOrderDtl> salesOrderDtlList = respentityList.getBody();
				saleOrderListTable.clear();
				saleOrderListTable.setAll(salesOrderDtlList);
				FillTable();
				txtItemname.setText("");
				txtBarcode.setText("");
				txtQty.setText("");
				txtRate.setText("");
				cmbUnit.getSelectionModel().clearSelection();
				txtItemcode.setText("");
				txtBatch.setText("");
				txtBarcode.requestFocus();
				txtWarranty.clear();
				salesOrderDtl = new SalesOrderDtl();
			}

		}

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
}
//////////////////////////////////////end pharmacy sales order//////////////////////////
