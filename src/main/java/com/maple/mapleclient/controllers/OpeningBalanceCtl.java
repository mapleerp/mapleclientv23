package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.OpeningBalanceMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountEventCr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class OpeningBalanceCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<OpeningBalanceMst> openingList = FXCollections.observableArrayList();

	EventBus eventBus = EventBusFactory.getEventBus();

	OpeningBalanceMst openingBalanceMst = null;
    @FXML
    private TableView<OpeningBalanceMst> tbOpeningBalance;
    @FXML
    private TableColumn<OpeningBalanceMst, String> clDate;

    @FXML
    private TableColumn<OpeningBalanceMst,String> clAccount;

    @FXML
    private TableColumn<OpeningBalanceMst, Number> clDebitAmount;

    @FXML
    private TableColumn<OpeningBalanceMst, Number> clCreditAmount;

    @FXML
    private DatePicker dpTransDate;

    @FXML
    private TextField txtAccount;

    @FXML
    private TextField txtDebitAmount;

    @FXML
    private TextField txtCreditAmount;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnDelete;
    @FXML
	private void initialize() {
    	dpTransDate = SystemSetting.datePickerFormat(dpTransDate, "dd/MMM/yyyy");
		eventBus.register(this);
		tbOpeningBalance.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					openingBalanceMst = new OpeningBalanceMst();
					openingBalanceMst.setId(newSelection.getId());
					ResponseEntity<AccountHeads> accByid = RestCaller.getAccountById(newSelection.getAccountId());
					txtAccount.setText(accByid.getBody().getAccountName());
					txtCreditAmount.setText(newSelection.getCreditAmount()+"");
					txtDebitAmount.setText(newSelection.getDebitAmount()+"");
					
				}
			}
		});
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == openingBalanceMst)
    	{
    		return;
    	}
    	if(null == openingBalanceMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteOpeningBalanceById(openingBalanceMst.getId());
    	notifyMessage(3,"Deleted");
    	openingBalanceMst = null;
    	ResponseEntity<List<OpeningBalanceMst>> openingBalanceList = RestCaller.getAllOpeningBalance();
    	openingList = FXCollections.observableArrayList(openingBalanceList.getBody());
    	fillTable();
    }

    @FXML
    void actionSave(ActionEvent event) {
    	if(null != openingBalanceMst)
    	{
    	if(null != openingBalanceMst.getId())
    	{
    		ResponseEntity<AccountHeads> getAccHead = RestCaller.getAccountHeadByName(txtAccount.getText());
	    	openingBalanceMst.setAccountId(getAccHead.getBody().getId());
	    	if(txtCreditAmount.getText().trim().isEmpty())
	    	{
	    		openingBalanceMst.setCreditAmount(0.0);
	    	}
	    	else
	    	{
	    	openingBalanceMst.setCreditAmount(Double.parseDouble(txtCreditAmount.getText()));
	    	}
	    	if(txtDebitAmount.getText().trim().isEmpty())
	    	{
	    		openingBalanceMst.setDebitAmount(0.0);
	    	}
	    	else
	    	{
	    	openingBalanceMst.setDebitAmount(Double.parseDouble(txtDebitAmount.getText()));
	    	}
	    	openingBalanceMst.setTransDate(Date.valueOf(dpTransDate.getValue()));
	    	RestCaller.updateOpeningBalance(openingBalanceMst);
	    	ResponseEntity<List<OpeningBalanceMst>> openingBalanceList = RestCaller.getAllOpeningBalance();
	    	openingList = FXCollections.observableArrayList(openingBalanceList.getBody());
    	}
    	}
    	else
	    {
	    	openingBalanceMst = new OpeningBalanceMst();
	    	ResponseEntity<AccountHeads> getAccHead = RestCaller.getAccountHeadByName(txtAccount.getText());
	    	openingBalanceMst.setAccountId(getAccHead.getBody().getId());
	    	if(txtCreditAmount.getText().trim().isEmpty())
	    	{
	    		openingBalanceMst.setCreditAmount(0.0);
	    	}
	    	else
	    	{
	    	openingBalanceMst.setCreditAmount(Double.parseDouble(txtCreditAmount.getText()));
	    	}
	    	if(txtDebitAmount.getText().trim().isEmpty())
	    	{
	    		openingBalanceMst.setDebitAmount(0.0);
	    	}
	    	else
	    	{
	    	openingBalanceMst.setDebitAmount(Double.parseDouble(txtDebitAmount.getText()));
	    	}
	    	openingBalanceMst.setTransDate(Date.valueOf(dpTransDate.getValue()));
	    	java.util.Date uDate = Date.valueOf(dpTransDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
	    	ResponseEntity<FinancialYearMst> getFinancialYr = RestCaller.getFinancialYear(startDate);
	    	if(null != getFinancialYr.getBody())
	    	{
	    		openingBalanceMst.setFinancialYearMst( getFinancialYr.getBody());
	    	}
	    	else
	    	{
	    		notifyMessage(3,"Financial Year Not Set");
	    		return;
	    	}
	    	ResponseEntity<OpeningBalanceMst> openinBalanceBYAccId = RestCaller.getOpeningBalanceByAccId(openingBalanceMst.getAccountId());
	    	if(null == openinBalanceBYAccId.getBody())
	    	{
	    	ResponseEntity<OpeningBalanceMst> respentity = RestCaller.saveOpeningBalanceMst(openingBalanceMst);
	    	openingBalanceMst  = respentity.getBody();
	    	openingList.add(openingBalanceMst);
	    	}
	    	else
	    	{
	    		notifyMessage(3,"Already Saved");
	    		return;
	    	}
	    }
    	fillTable();
    	clearFields();
    	openingBalanceMst = null;
    } 
    @FXML
    void onDebitAmountEnter(KeyEvent event) {

    	if(event.getCode()== KeyCode.ENTER)
    	{
    		txtCreditAmount.requestFocus();
    	}
    }
    @FXML
    void creditAmountOnEnter(KeyEvent event) {

    	if(event.getCode()== KeyCode.ENTER)
    	{
    		btnSave.requestFocus();
    	}
    

    }

    private void fillTable()
    {
    	for(OpeningBalanceMst openingBalance:openingList)
    	{
    		ResponseEntity<AccountHeads> accById =RestCaller.getAccountById(openingBalance.getAccountId());
    		openingBalance.setAccountName(accById.getBody().getAccountName());
    	}
    	tbOpeningBalance.setItems(openingList);
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getaccountNameproperty());
		clCreditAmount.setCellValueFactory(cellData -> cellData.getValue().getcreditAmountProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().gettransDateProperty());
		clDebitAmount.setCellValueFactory(cellData -> cellData.getValue().getdebitAmountProperty());

    }
    private void clearFields()
    {
    	txtAccount.clear();
    	txtCreditAmount.clear();
    	txtDebitAmount.clear();
    	
    	
    }

    @FXML
    void actionShowAll(ActionEvent event) {

    	ResponseEntity<List<OpeningBalanceMst>> openingBalanceList = RestCaller.getAllOpeningBalance();
    	openingList = FXCollections.observableArrayList(openingBalanceList.getBody());
    	fillTable();
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountEvent.getAccountName());
		}
	}
    @FXML
    void loadPopUp(KeyEvent event) {


		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtDebitAmount.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


    private void PageReload() {
    	
  }
}
