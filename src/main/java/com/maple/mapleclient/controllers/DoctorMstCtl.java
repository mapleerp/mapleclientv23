package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.maple.mapleclient.entity.NewDoctorMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
	
	
	@RestController
	public class DoctorMstCtl {
		
		private ObservableList<NewDoctorMst> doctorMstTable = FXCollections.observableArrayList();

		NewDoctorMst newDoctorMst;
		
	    @FXML
	    private TextField txtFullName;

	    @FXML
	    private TextField txtPhoneNumber;

	    @FXML
	    private TextField txtPlace;

	    @FXML
	    private TextField txtDesignation;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private Button btnAdd;

	    @FXML
	    private Button btnShow;

	    @FXML
	    private Button btnDeleteItem;

	    @FXML
	    private TableView<NewDoctorMst> tblDoctorDetails;

	    @FXML
	    private TableColumn<NewDoctorMst, String> clFullName;

	    @FXML
	    private TableColumn<NewDoctorMst, String> clDesignation;

	    @FXML
	    private TableColumn<NewDoctorMst, String> clPhoneNumber;

	    @FXML
	    private TableColumn<NewDoctorMst, String> clPlace;
	    
	    
		@FXML
		private void initialize() {
			
			tblDoctorDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					System.out.println("getSelectionModel");
					if (null != newSelection.getId()) {

						newDoctorMst = new NewDoctorMst();
						newDoctorMst.setId(newSelection.getId());
					}
				}
			});
		}

	    @FXML
	    void Add(ActionEvent event) {
	    	save();
	    }

	    private void save() {
	    	NewDoctorMst newDoctorMst = new NewDoctorMst();
			if (null == txtFullName.getText()) {
				notifyMessage(5, "Please Enter the Name", false);
				return;
			}

			if (txtDesignation.getText().trim().isEmpty()) {
				notifyMessage(5, "Please Enter  Designation", false);
				return;
			}
			if (null == txtPhoneNumber.getText())
			{
				notifyMessage(5, "Please enter Phone Number", false);
				return;
			}
			if (txtPlace.getText().trim().isEmpty()) {
				notifyMessage(5, "Please Enter  Place ", false);
				return;
			}
			
			newDoctorMst.setName(txtFullName.getText());
			newDoctorMst.setDesignation(txtDesignation.getText());
			newDoctorMst.setPhoneNumber(txtPhoneNumber.getText());
			newDoctorMst.setPlace(txtPlace.getText());

			ResponseEntity<NewDoctorMst> respEntity = RestCaller.createNewDoctorMst(newDoctorMst);

			{
				notifyMessage(5, "Saved ", true);
				txtFullName.clear();
				txtDesignation.clear();
				txtPhoneNumber.clear();
				txtPlace.clear();
				showAll();
				return;
			}

			
		}

		private void showAll() {
			ResponseEntity<List<NewDoctorMst>> respentity = RestCaller.getNewDoctorMst();
			doctorMstTable = FXCollections.observableArrayList(respentity.getBody());

			System.out.print(doctorMstTable.size() + "observable list size issssssssssssssssssssss");
			fillTable();

		}
		private void fillTable() {
			tblDoctorDetails.setItems(doctorMstTable);

			clFullName.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());

			clDesignation.setCellValueFactory(cellData -> cellData.getValue().getDesignationProperty());

			clPhoneNumber.setCellValueFactory(cellData -> cellData.getValue().getPhoneNumberProperty());
			
            clPlace.setCellValueFactory(cellData -> cellData.getValue().getPlaceProperty());
		}

	
		
		@FXML
	    void Delete(ActionEvent event) {
			if (null != newDoctorMst) {

				if (null != newDoctorMst.getId()) {
					RestCaller.deleteNewDoctorMst(newDoctorMst.getId());

					showAll();
					notifyMessage(5, " Detail deleted", false);
					return;
				}
			}
	    }

	    @FXML
	    void ShowAll(ActionEvent event) {
	    	showAll();
	    }

	    @FXML
	    void add(KeyEvent event) {

	    }

	    @FXML
	    void clearDetails(ActionEvent event) {
	    	txtFullName.clear();
	    	txtDesignation.clear();
	    	txtPhoneNumber.clear();
	    	txtPlace.clear();
	    	tblDoctorDetails.getItems().clear();
	    }
	    
	    public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
	    @FXML
	    void nameOnEnter(KeyEvent event) {

			if (event.getCode() == KeyCode.ENTER) {
				txtDesignation.requestFocus();
			}
	    }

	    @FXML
	    void placeOnEnter(KeyEvent event) {

	    }
	    @FXML
	    void DesignationOnEnter(KeyEvent event) {

			if (event.getCode() == KeyCode.ENTER) {
				txtPhoneNumber.requestFocus();
			}
	    }

	    @FXML
	    void PhoneOnEnter(KeyEvent event) {

			if (event.getCode() == KeyCode.ENTER) {
				txtPlace.requestFocus();
			}
	    }
	}



