package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.TSCBarcode;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.IntentHdr;
import com.maple.mapleclient.entity.IntentInDtl;
import com.maple.mapleclient.entity.IntentInHdr;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.IntentPopupEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

@Controller
public class stockTransferCtl {

	String taskid;
	String processInstanceId;

	private EventBus eventBus = EventBusFactory.getEventBus();
	String stock_tarnsfer_prefix = SystemSetting.STOCK_TRANSFER_PREFIX;
	private ObservableList<StockTransferOutHdr> stockOutHdrList = FXCollections.observableArrayList();

	private ObservableList<StockTransferOutDtl> StockTransferList = FXCollections.observableArrayList();
	private ObservableList<IntentInDtl> intentList = FXCollections.observableArrayList();
	IntentInHdr intentInHdr = null;

	StockTransferOutHdr stkOutHdr = null;
	UnitMst unitmst;
	ItemMst itemmst;
	double amount = 0;
	boolean multi = false;
	Double totalamount;
	Integer count = 0;
	// double totalAmount = 0;
	/*
	 * Instance Variable stockTransferOutDtl to hold each item while adding and
	 * selected item for delete. One instance of this class will be in memmory to
	 * hold the items top be added.
	 * 
	 */
	StockTransferOutDtl stockTransferOutDtl = new StockTransferOutDtl();
	@FXML
	private Button btnFetch;
	@FXML
	private ComboBox<String> cmbToBranch;

	@FXML
	private TextField txtIntentNo;

	@FXML
	private Button btnVoucherFetch;
	@FXML
	private DatePicker dpVoucherDate;
	@FXML
	private TableView<StockTransferOutHdr> tbVoucher;

	@FXML
	private TableColumn<StockTransferOutHdr, String> clVoucher;

	@FXML
	private TextField txtStatutoryVoucher;

	@FXML
	private DatePicker dpStkTrasferDate;

	@FXML
	private Button btnGenerateVoucher;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtItemCode;

	@FXML
	private TextField txtBatchCode;

	@FXML
	private DatePicker dpManfctrDate;

	@FXML
	private TextField txtTotalAmt;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtTaxRate;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtRecordId;

	@FXML
	private DatePicker dpExpiryDate;

	@FXML
	private TextField txtUnit;

	@FXML
	private Button btnstockTransferAdd;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnSubmit;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtMRP;

	@FXML
	private TextField txtStore;

	@FXML
	private TableView<StockTransferOutDtl> tblStockTrasfer;

	@FXML
	private TableColumn<StockTransferOutDtl, String> clStktransferItemName;

	@FXML
	private TableColumn<StockTransferOutDtl, String> clStktransferBatch;

	@FXML
	private TableColumn<StockTransferOutDtl, Number> clStkTransferQty;

	@FXML
	private TableColumn<StockTransferOutDtl, Number> clStkTransferRate;

	@FXML

	private TableColumn<StockTransferOutDtl, LocalDate> clStkTransferExpiryDate;

	@FXML
	private TableColumn<StockTransferOutDtl, Number> clStkTransferAmount;

	@FXML
	private TableColumn<StockTransferOutDtl, String> clStktransferBarcode;

	@FXML
	private TableColumn<StockTransferOutDtl, Number> clStkMRP;

	@FXML
	private TableColumn<StockTransferOutDtl, Number> clSlNo;

	// --------------------new version 1.4 surya

	@FXML
	private TableView<StockTransferOutHdr> tblHoldedItems;

	@FXML
	private TableColumn<StockTransferOutHdr, String> clHoldedToBranch;

//    @FXML
//    private TableColumn<StockTransferOutHdr, ?> clHoldedDate;

	@FXML
	private Button btnHoldStockTransfer;

	@FXML
	private Button tbtnUnHoldStockTransfer;

	@FXML
	void HoldStockTransfer(ActionEvent event) {

		StockTransferList.clear();
		txtIntentNo.setText("");
		txtTotalAmt.setText("");
		dpExpiryDate.getEditor().clear();
		cmbToBranch.getSelectionModel().clearSelection();

		notifyMessage(5, "Details Saved Successfully!!!");

		dpExpiryDate.setValue(null);

		StockTransferList.clear();
		txtIntentNo.setText("");
		txtTotalAmt.setText("");
		dpExpiryDate.getEditor().clear();
		cmbToBranch.getSelectionModel().clearSelection();

		String vNo = stkOutHdr.getVoucherNumber();
		stkOutHdr = null;
		stockTransferOutDtl = new StockTransferOutDtl();
		btnSubmit.setDisable(true);

		getHoldedStockTransfer();

	}

	@FXML
	void UnHoldStockTransfer(ActionEvent event) {

		if (null == stkOutHdr) {
			notifyMessage(3, "Please select stock transfer details");
			return;
		}
		if (null == stkOutHdr.getId()) {

			notifyMessage(3, "Please select stock transfer details ");
			return;

		}

		ResponseEntity<StockTransferOutHdr> stockTransferOutHdr = RestCaller
				.getStockTransferOutHdrById(stkOutHdr.getId());
		stkOutHdr = stockTransferOutHdr.getBody();

		if (null != stkOutHdr.getToBranch()) {
			cmbToBranch.getSelectionModel().select(stkOutHdr.getToBranch());
		}

		ResponseEntity<List<StockTransferOutDtl>> stockTrasferOutDtlResp = RestCaller.getStockTransferOutDtl(stkOutHdr);
		StockTransferList = FXCollections.observableArrayList(stockTrasferOutDtlResp.getBody());

		if (null != stockTrasferOutDtlResp.getBody()) {
			FillTable();
			setTotal();

			btnSubmit.setDisable(false);

		}

	}

	private void getHoldedStockTransfer() {

		ResponseEntity<List<StockTransferOutHdr>> stockTransferOutHdrResp = RestCaller.getHoldedStockTransfer();
		stockOutHdrList = FXCollections.observableArrayList(stockTransferOutHdrResp.getBody());

		for (StockTransferOutHdr stockTransferOutHdr : stockOutHdrList) {
			BranchMst branchMst = RestCaller.getBranchDtls(stockTransferOutHdr.getToBranch());
			stockTransferOutHdr.setToBranchName(branchMst.getBranchName());
		}

		tblHoldedItems.setItems(stockOutHdrList);
		clHoldedToBranch.setCellValueFactory(cellData -> cellData.getValue().getToBranchProperty());

	}

	// -------------------new version 1.4 surya end

	@FXML
	private void initialize() {
//		dpExpiryDate = SystemSetting.datePickerFormat(dpExpiryDate, "dd/MMM/yyyy");
//		dpManfctrDate = SystemSetting.datePickerFormat(dpManfctrDate, "dd/MMM/yyyy");
//		dpStkTrasferDate = SystemSetting.datePickerFormat(dpStkTrasferDate, "dd/MMM/yyyy");
//		dpVoucherDate = SystemSetting.datePickerFormat(dpVoucherDate, "dd/MMM/yyyy");
		btnSubmit.setDisable(true);
		stockTransferOutDtl = new StockTransferOutDtl();
		eventBus.register(this);
		dpStkTrasferDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		dpStkTrasferDate.setEditable(false);

		txtQty.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		/*
		 * txtMRP.textProperty().addListener(new ChangeListener<String>() {
		 */
		/*
		 * @Override public void changed(ObservableValue<? extends String> observable,
		 * String oldValue, String newValue) { if
		 * (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) { txtMRP.setText(oldValue);
		 * } } }); ArrayList branch = new ArrayList(); RestTemplate restTemplate = new
		 * RestTemplate(); branch = RestCaller.SearchBranchs(); Iterator itr =
		 * branch.iterator(); while (itr.hasNext()) { LinkedHashMap lm = (LinkedHashMap)
		 * itr.next(); Object branchName = lm.get("branchName"); Object branchId =
		 * lm.get("id"); if (branchId != null) { cmbToBranch.getItems().add((String)
		 * branchName); } } txtIntentNo.requestFocus();
		 * 
		 * ArrayList branchRest = new ArrayList();
		 * 
		 * branchRest = RestCaller.SearchBranchs(); Iterator itr1 =
		 * branchRest.iterator(); while (itr1.hasNext()) { LinkedHashMap lm =
		 * (LinkedHashMap) itr1.next(); System.out.println("---branchRest---"+lm);
		 * Object branchName = lm.get("branchName"); Object branchId = lm.get("id"); if
		 * (branchId != null) { if(!branchId.equals("camunda-admin")) {
		 * System.out.println("---branchName---"+branchId);
		 * cmbToBranch.getItems().add((String) branchId); } } }
		 */

		// --------get branch from rest

		ResponseEntity<List<BranchMst>> branchRestListResp = RestCaller.getBranchDtl();

		List branchRestList = branchRestListResp.getBody();
		Iterator itr1 = branchRestList.iterator();

		while (itr1.hasNext()) {

			BranchMst lm = (BranchMst) itr1.next();
			if (!lm.getBranchCode().equalsIgnoreCase(SystemSetting.systemBranch))
				cmbToBranch.getItems().add(lm.getBranchName());
		}

		// ----------------------new version 1.4 surya

		try {
			getHoldedStockTransfer();

			tblHoldedItems.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {

						stkOutHdr = new StockTransferOutHdr();
						stkOutHdr.setId(newSelection.getId());

						System.out.println(newSelection.getId());
					}
				}
			});

		} catch (Exception e) {
			System.out.print(e.toString());
		}
		// ----------------------new version 1.4 surya end

	}

	@FXML
	void addOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void add(ActionEvent event) {
		addItem();

	}

	private void FillTable() {

		for (StockTransferOutDtl stock : StockTransferList) {
//			ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(stock.getItemId());
//			ItemMst itemMst = itemMstResp.getBody();

			stock.setItemName(stock.getItemId().getItemName());
		}

		tblStockTrasfer.setItems(StockTransferList);

		clSlNo.setCellValueFactory(cellData -> cellData.getValue().getDisplaySlNoProperty());
		clStkTransferAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clStktransferBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clStkTransferExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clStktransferItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clStkTransferQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clStkTransferRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clStktransferBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
		clStkMRP.setCellValueFactory(cellData -> cellData.getValue().getmrpProperty());

	}

	@FXML
	void submit(ActionEvent event) {

		/*
		 * Final Save
		 */

		ResponseEntity<List<StockTransferOutDtl>> stockTransferoutdtlSaved = RestCaller
				.getStockTransferOutDtl(stkOutHdr);
		if (stockTransferoutdtlSaved.getBody().size() == 0) {
			return;
		}

		btnSubmit.setDisable(true);

		if (null != txtIntentNo.getText()) {
			stkOutHdr.setIntentNumber(txtIntentNo.getText());
		}

		stkOutHdr.setIntentNumber(txtIntentNo.getText());

		if (null == stkOutHdr || null == stkOutHdr.getId()) {
			btnSubmit.setDisable(false);
			return;
		}

		if (null == cmbToBranch.getSelectionModel() || null == cmbToBranch.getSelectionModel().getSelectedItem()) {
			notifyMessage(2, "Select To Branch");
			cmbToBranch.requestFocus();
			btnSubmit.setDisable(false);
			return;
		}

		// Setting Branch
		ResponseEntity<BranchMst> branchRestListResp = RestCaller
				.getBranchMstByName(cmbToBranch.getSelectionModel().getSelectedItem());
		if (null != branchRestListResp.getBody()) {
			String branchcode = branchRestListResp.getBody().getBranchCode();
			stkOutHdr.setToBranch(branchcode);
		}

//		if (null == stkOutHdr.getVoucherDate()) {

			Date date = Date.valueOf(dpStkTrasferDate.getValue());

			Format formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd");

			stkOutHdr.setVoucherDate(date);

//		}

//		Format formatter;
//		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(stkOutHdr.getVoucherDate());


		String stockVerification = RestCaller.StockVerificationByStockTransferFromItmBatchDtl(stkOutHdr.getId());
		if (null != stockVerification && !SystemSetting.isNEGATIVEBILLING()) {
			notifyMessage(5, stockVerification);
			btnSubmit.setDisable(false);
			return;
		}

		try {

			RestCaller.updateStockTransferOutHdr(stkOutHdr);
		} catch (Exception e) {

			System.out.println(e.toString());
			btnSubmit.setDisable(false);
			throw e;

		}

		String stkHdrID = stkOutHdr.getId();

		ResponseEntity<StockTransferOutHdr> stockTrasferOutHdrResp = RestCaller.getStockTransferOutHdrById(stkHdrID);

		stkOutHdr = stockTrasferOutHdrResp.getBody();
		
		String vNo= stkOutHdr.getVoucherNumber();

		StockTransferList = null;

		txtIntentNo.setText("");
		txtTotalAmt.setText("");
		dpExpiryDate.getEditor().clear();
		cmbToBranch.getSelectionModel().clearSelection();

		stkOutHdr = null;
		stockTransferOutDtl = new StockTransferOutDtl();
		btnSubmit.setDisable(true);

		if (SystemSetting.STOCKTRANSFER_FORMAT.equalsIgnoreCase("POS")) {
			JasperPdfReportService.stockTransferOutPrintPOSformat(stkHdrID);
		} else {

			try {

				JasperPdfReportService.StockTransferInvoiceReport(vNo, strDate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		StockTransferList = null;
		stkHdrID = "";
		txtIntentNo.setText("");
		txtTotalAmt.setText("");
		dpExpiryDate.getEditor().clear();
		cmbToBranch.getSelectionModel().clearSelection();

		tblStockTrasfer.getItems().clear();
		
		notifyMessage(5, "Details Saved Successfully!!!");

		dpExpiryDate.setValue(null);

		getHoldedStockTransfer();

		count = 0;
	}

	@FXML
	void onEnterItemName(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (SystemSetting.isNEGATIVEBILLING() || SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO")) {
				showItemPopup();

			} else {
				showStockitemPopup();

			}

		}

	}

	private void showItemPopup() {

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void expirydateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtMRP.requestFocus();
		}
	}

	@FXML
	void barcodekeyPress(KeyEvent event) {

//    	if (event.getCode() == KeyCode.ENTER) {
//			
//		}

		String barcodeTxt = event.getText();
		System.out.print(barcodeTxt);
		if (event.getCode() == KeyCode.BACK_SPACE) {
			txtItemName.requestFocus();

			if (SystemSetting.isNEGATIVEBILLING() || SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO")) {
				showItemPopup();

			} else {
				showStockitemPopup();
			}
		}
		if (event.getCode() == KeyCode.ENTER) {
			if (barcodeTxt.isEmpty()) {
				btnSubmit.requestFocus();
			} else {

				barcodeTxt = txtBarcode.getText();
				ArrayList items = new ArrayList();
				items = RestCaller.SearchStockItemByBarcode(barcodeTxt);
				if (items.size() > 1) {
					multi = true;
					showPopupByBarcode();
				} else {
					multi = false;
					String itemName = "";
					String barcode = "";
					Double mrp = 0.0;
					String unitname = "";
					Double qty = 0.0;
					Double cess = 0.0;
					Double tax = 0.0;
					String itemId = "";
					String unitId = "";
					Iterator itr = items.iterator();
					String batch = "";

					while (itr.hasNext()) {

						List element = (List) itr.next();
						itemName = (String) element.get(0);
						barcode = (String) element.get(1);
						unitname = (String) element.get(2);
						mrp = (Double) element.get(3);
						qty = (Double) element.get(4);
						cess = (Double) element.get(5);
						tax = (Double) element.get(6);
						itemId = (String) element.get(7);
						unitId = (String) element.get(8);
						batch = (String) element.get(9);

						if (null != itemName) {

							txtItemName.setText(itemName);
							// txtItemCode.setText(itemPopupEvent.getItemCode());
							ResponseEntity<ItemMst> itemMst = RestCaller.getitemMst(itemId);
							stockTransferOutDtl.setItemId(itemMst.getBody());
							stockTransferOutDtl.setUnitId(unitId);
							txtBarcode.setText(barcode);
							txtUnit.setText(unitname);
							txtMRP.setText(Double.toString(mrp));
							// txtItemCode.setText(itemPopupEvent.getItemCode());
							txtBatchCode.setText(batch);

							txtRate.setText(txtMRP.getText());
							txtQty.requestFocus();
							// addItem();
						}
					}
				}
			}
		}

	}

	@FXML
	void mrpOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnstockTransferAdd.requestFocus();
		}
	}

	@FXML
	void itempopup(MouseEvent event) {

		if (SystemSetting.isNEGATIVEBILLING() || SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO")) {
			showItemPopup();

		} else {
			showStockitemPopup();

		}
	}

	@FXML
	void batchCodeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtQty.requestFocus();
		}

	}

	@FXML
	void rateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpExpiryDate.requestFocus();
		}

	}

	@FXML
	void intentOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
//			dpStkTrasferDate.requestFocus();
			loadintentPopup();
		}
	}

	@FXML
	void branchOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtItemName.requestFocus();
			if (SystemSetting.isNEGATIVEBILLING() || SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO")) {
				showItemPopup();

			} else {
				showStockitemPopup();
			}
		}

	}

	@FXML
	void stkTransferDate(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbToBranch.requestFocus();
		}

	}

	@FXML
	void stkTransferDateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		}
	}

	@FXML
	void qtyOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtRate.requestFocus();
		}

	}

	@FXML
	void expiryDateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnstockTransferAdd.requestFocus();
		}
	}

	@FXML
	void btnDelete(ActionEvent event) {
		try {
			if (null != stockTransferOutDtl.getId()) {

				RestCaller.deleteStockTransferOutDtl(stockTransferOutDtl.getId());

				notifyMessage(5, "Deleted Successfully!!!");
				tblStockTrasfer.getItems().clear();
				ResponseEntity<List<StockTransferOutDtl>> stockTransferoutdtlSaved = RestCaller
						.getStockTransferOutDtl(stkOutHdr);
				StockTransferList = FXCollections.observableArrayList(stockTransferoutdtlSaved.getBody());
				if (StockTransferList.size() > 0) {

					/*
					 * Call Rest to get the Total Amount
					 */

					for (StockTransferOutDtl stk : StockTransferList) {

//						ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(stk.getItemMst().getId());
						stk.setItemName(stk.getItemId().getItemName());

					}
					setTotal();

//					RestCaller.updateStockTransferSlNo(stkOutHdr.getId());
				}
				txtBarcode.setText("");
				setTotal();
				tblStockTrasfer.setItems(StockTransferList);
				clearfield();
				dpExpiryDate.setValue(null);
			} else {
				notifyMessage(5, "Please select the Item!!!");
			}
			txtBarcode.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	@FXML
	void voucherFetchAction(ActionEvent event) {
		stockOutHdrList.clear();
		// tblStockTrasfer.getItems().clear();

		if (null == dpVoucherDate.getValue()) {
			return;
		}
		LocalDate date = dpVoucherDate.getValue();
		java.util.Date usdate = SystemSetting.localToUtilDate(date);
		String sdate = SystemSetting.UtilDateToString(usdate, "yyyy-MM-dd");
		ArrayList invoice = new ArrayList();
		invoice = RestCaller.getStockTransferVoucher(sdate, SystemSetting.getUser().getId());
		String voucherNo = "";
		Iterator itr = invoice.iterator();

		while (itr.hasNext()) {
			String element = (String) itr.next();
			// voucherNo = (String) element.get(0);
			if (null != element) {
				StockTransferOutHdr stk = new StockTransferOutHdr();
				stk.setVoucherNumber(element);
				stockOutHdrList.add(stk);
			}
		}
		fillStockTransferHdr();

	}

	private void fillStockTransferHdr() {
		tbVoucher.setItems(stockOutHdrList);
		clVoucher.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());

	}

	@FXML
	void voucherNumberOnClick(MouseEvent event) {

		String vno = tbVoucher.getSelectionModel().getSelectedItem().getVoucherNumber();
		LocalDate date = dpVoucherDate.getValue();
		java.util.Date usdate = SystemSetting.localToUtilDate(date);
		String sdate = SystemSetting.UtilDateToString(usdate, "yyyy-MM-dd");
		ResponseEntity<StockTransferOutHdr> getStockTransferOutHdr = RestCaller
				.getstockTransferOutHdrByVNoAndDate(sdate, vno);
		stkOutHdr = getStockTransferOutHdr.getBody();
		txtIntentNo.setText(getStockTransferOutHdr.getBody().getIntentNumber());
		cmbToBranch.setValue(getStockTransferOutHdr.getBody().getToBranch());
		dpStkTrasferDate.setValue(getStockTransferOutHdr.getBody().getVoucherDate().toLocalDate());
		btnSubmit.setDisable(false);

	}

	@FXML
	void tableToTextBox(MouseEvent event) {

		if (tblStockTrasfer.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tblStockTrasfer.getSelectionModel();
			StockTransferList = selectionModel.getSelectedItems();
			for (StockTransferOutDtl stk : StockTransferList) {
				txtItemName.setText(stk.getItemName());
				txtQty.setText(Double.toString(stk.getQty()));
				txtBatchCode.setText(stk.getBatch());
				txtRate.setText(Double.toString(stk.getRate()));
				// txtItemCode.setText(stk.getItemCode());
				stockTransferOutDtl.setUnitId(stk.getUnitId().toString());
				ResponseEntity<UnitMst> respentity = RestCaller.getunitMst(stockTransferOutDtl.getUnitId());
				unitmst = respentity.getBody();
				txtUnit.setText(unitmst.getUnitName());
				stockTransferOutDtl.setId(stk.getId());
				txtMRP.setText(Double.toString(stk.getMrp()));
				txtBarcode.setText(stk.getBarcode());
				stockTransferOutDtl.setItemId(stk.getItemId());
				// java.util.Date utilExpiryDate =
				// convertToDateViaInstant(stk.getexpiryDate().toLocalDate());

				if (null != stk.getexpiryDate()) {
					dpExpiryDate.setValue(stk.getexpiryDate().toLocalDate());
				}

			}

		}
	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnstockTransferAdd.getScene().getWindow();
		if (stage.isShowing()) {
			if (!multi) {
				dpExpiryDate.setValue(null);
				txtItemName.setText(itemPopupEvent.getItemName());
				ResponseEntity<ItemMst> itemmst= RestCaller.getitemMst(itemPopupEvent.getItemId());
				stockTransferOutDtl.setItemId(itemmst.getBody());
				stockTransferOutDtl.setUnitId(itemPopupEvent.getUnitId());
				Platform.runLater(() -> {
					txtStore.setText(itemPopupEvent.getStoreName());
				});
				txtBarcode.setText(itemPopupEvent.getBarCode());
				txtUnit.setText(itemPopupEvent.getUnitName());
				txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));

				txtBatchCode.setText(itemPopupEvent.getBatch());

				Platform.runLater(() -> {

					if (SystemSetting.isNEGATIVEBILLING()) {
						txtBatchCode.setText("NOBATCH");
					}
				});

				txtRate.setText(txtMRP.getText());

				ResponseEntity<List<ItemBatchExpiryDtl>> expiryResp = RestCaller
						.getItemBatchExpByItemAndBatch(itemPopupEvent.getItemId(), itemPopupEvent.getBatch());
				if (!expiryResp.getBody().isEmpty()) {
					ItemBatchExpiryDtl itemBatchExpiryDtl = expiryResp.getBody().get(0);
					if (null != itemBatchExpiryDtl) {
						dpExpiryDate.setValue(SystemSetting.utilToLocaDate(itemBatchExpiryDtl.getExpiryDate()));

					}
				}
				java.util.Date udate = SystemSetting.getApplicationDate();
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				ResponseEntity<PriceDefenitionMst> getpriceDefMst = RestCaller
						.getPriceDefenitionMstByName("STOCK TRANSFER");
				if (null != getpriceDefMst.getBody()) {
					/*
					 * Check for Batch price definition
					 */
					ResponseEntity<BatchPriceDefinition> batchPrice = RestCaller.getBatchPriceDefinition(
							itemPopupEvent.getItemId(), getpriceDefMst.getBody().getId(), itemPopupEvent.getUnitId(),
							itemPopupEvent.getBatch(), sdate);
					if (null != batchPrice.getBody()) {
						txtRate.setText(batchPrice.getBody().getAmount() + "");
					} else {
						ResponseEntity<PriceDefinition> getPrice = RestCaller.getPriceDefenitionByItemIdAndUnit(
								itemPopupEvent.getItemId(), getpriceDefMst.getBody().getId(),
								itemPopupEvent.getUnitId(), sdate);
						if (null != getPrice.getBody()) {
							txtRate.setText(Double.toString(getPrice.getBody().getAmount()));
						}
					}
				}

				if (!(txtBatchCode.getText().trim().isEmpty())) {
					List<ItemBatchMst> itemBatchMstList = RestCaller.getBatchExpDate(txtBatchCode.getText());

					ItemBatchMst itemBatchMst = null;
					if (itemBatchMstList.size() > 0) {
						itemBatchMst = itemBatchMstList.get(0);
					}

					try {
						if (null != itemBatchMst) {
							Date expdaten = (Date) itemBatchMst.getExpDate();

							dpExpiryDate.setValue(expdaten.toLocalDate());
						}
					} catch (Exception e) {
						System.out.print(e.toString());
					}
					multi = false;
				}

				txtQty.requestFocus();
			} else if (multi) {
				txtItemName.setText(itemPopupEvent.getItemName());
				ResponseEntity<ItemMst> itemMst= RestCaller.getitemMst(itemPopupEvent.getItemId());
				stockTransferOutDtl.setItemId(itemMst.getBody());
				stockTransferOutDtl.setUnitId(itemPopupEvent.getUnitId());
				txtBarcode.setText(itemPopupEvent.getBarCode());
				txtUnit.setText(itemPopupEvent.getUnitName());
				txtStore.setText(itemPopupEvent.getStoreName());
				txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));
				java.util.Date udate = SystemSetting.getApplicationDate();
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				ResponseEntity<PriceDefenitionMst> getpriceDefMst = RestCaller
						.getPriceDefenitionMstByName("STOCK TRANSFER");
				if (null != getpriceDefMst.getBody()) {
					/*
					 * Check for Batch price definition
					 */
					ResponseEntity<BatchPriceDefinition> batchPrice = RestCaller.getBatchPriceDefinition(
							itemPopupEvent.getItemId(), getpriceDefMst.getBody().getId(), itemPopupEvent.getUnitId(),
							itemPopupEvent.getBatch(), sdate);
					if (null != batchPrice.getBody()) {
						txtRate.setText(batchPrice.getBody().getAmount() + "");
					} else {
						ResponseEntity<PriceDefinition> getPrice = RestCaller.getPriceDefenitionByItemIdAndUnit(
								itemPopupEvent.getItemId(), getpriceDefMst.getBody().getId(),
								itemPopupEvent.getUnitId(), sdate);
						if (null != getPrice.getBody()) {
							txtRate.setText(Double.toString(getPrice.getBody().getAmount()));
						}
					}
				} else {
					txtRate.setText(txtMRP.getText());
				}

//				if (!(txtBatchCode.getText().trim().isEmpty())) {
//					List<ItemBatchMst> itemBatchMstList = RestCaller.getBatchExpDate(txtBatchCode.getText());
//
//					ItemBatchMst itemBatchMst = null;
//					if (itemBatchMstList.size() > 0) {
//						itemBatchMst = itemBatchMstList.get(0);
//					}
//
//					try {
//						if (null != itemBatchMst) {
//							Date expdaten = (Date) itemBatchMst.getExpDate();
//
//							dpExpiryDate.setValue(expdaten.toLocalDate());
//						}
//					} catch (Exception e) {
//						System.out.print(e.toString());
//					}
//
//				}
				txtQty.setText("1");
				addItem();
				multi = false;
			}
		}

	}

	private void clearfield() {
		txtItemName.setText("");
		txtQty.setText("");
		txtRate.setText("");
		txtUnit.setText("");
		txtBarcode.setText("");
		txtMRP.setText("");
		txtBatchCode.setText("");
		dpExpiryDate.getEditor().clear();
		txtBarcode.setText("");
		txtMRP.setText("");

	}

	private void showStockitemPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtQty.requestFocus();
	}

	private void setTotal() {

		totalamount = RestCaller.getSummaryStocktotal(stkOutHdr.getId());
		txtTotalAmt.setText(Double.toString(totalamount));
	}

	private void addItem() {

//		try {

		if (cmbToBranch.getSelectionModel().isEmpty()) {

			notifyMessage(5, "Please Select Branch Name!!!");
			cmbToBranch.requestFocus();
			return;
		} else if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Select Item Name!!!");
			txtItemName.requestFocus();
			return;
		}

		else if (txtQty.getText().trim().isEmpty()) {

			notifyMessage(5, "Please Type Quantity!!!");
			txtQty.requestFocus();
			return;
		} else if (txtRate.getText().trim().isEmpty()) {

			notifyMessage(5, "Please Type Rate!!!");
			txtRate.requestFocus();
			return;
		} else {
			btnSubmit.setDisable(false);

			ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
			DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();

			if (null != dayEndClosureHdr) {
				LocalDate dayEndProcessDate = SystemSetting.utilToLocaDate(dayEndClosureHdr.getProcessDate());
				LocalDate stockTransDate = dpStkTrasferDate.getValue();

				int dateComparison = stockTransDate.compareTo(dayEndProcessDate);

				if (dateComparison <= 0) {
					notifyMessage(5, "Stock transfer on previous date is not possible");
					return;
				}
			}
			String storeName = null;
			if (null != txtStore.getText()) {
				storeName = txtStore.getText();
			} else {
				storeName = "MAIN";
			}
			ArrayList items = new ArrayList();

//			items = RestCaller.getSingleStockItemByName(txtItemName.getText(), txtBatchCode.getText(),storeName);
			items = RestCaller.getSingleStockItemByName(txtItemName.getText(), txtBatchCode.getText());

			Double chkQty = 0.0;
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
			chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(), txtBatchCode.getText(),
					storeName);

			String itemId = null;
			Iterator itr = items.iterator();
			while (itr.hasNext()) {
				List element = (List) itr.next();
//				chkQty = (Double) element.get(4);
				itemId = (String) element.get(7);
			}

			System.out.println(SystemSetting.isNEGATIVEBILLING());
			if ((chkQty < Double.parseDouble(txtQty.getText())) && !SystemSetting.isNEGATIVEBILLING()) {
				notifyMessage(1, "Not in Stock!!!");
				txtQty.clear();
				txtItemName.clear();
				txtBarcode.clear();
				txtBatchCode.clear();
				txtRate.clear();
				txtUnit.clear();
				txtMRP.clear();
				txtBarcode.requestFocus();
				return;
			}
			if (null == stkOutHdr) {
				stkOutHdr = new StockTransferOutHdr();
				stkOutHdr.setIntentNumber(txtIntentNo.getText());

				// ===========changes done by anandu for passing the branch code instead of
				// branch name.=========
				ResponseEntity<BranchMst> branchRestListResp2 = RestCaller
						.getBranchMstByName(cmbToBranch.getSelectionModel().getSelectedItem());
				if (null != branchRestListResp2.getBody()) {
					String branchcode2 = branchRestListResp2.getBody().getBranchCode();
					stkOutHdr.setToBranch((String) branchcode2);
				}
				/*
				 * try { ArrayList branch = new ArrayList();
				 * 
				 * branch = RestCaller.SearchBranch(); Iterator itr = branch.iterator(); while
				 * (itr.hasNext()) { LinkedHashMap lm = (LinkedHashMap) itr.next(); Object
				 * branchName = lm.get("branchName"); String toBranch =
				 * cmbToBranch.getSelectionModel().getSelectedItem().toString(); if
				 * (toBranch.equalsIgnoreCase((String) branchName)) { Object branchCode =
				 * lm.get("branchCode"); stkOutHdr.setToBranch((String) branchCode); } } } catch
				 * (Exception e) { stkOutHdr = null; System.out.println(e.toString()); return; }
				 */

				stkOutHdr.setVoucherType("STOUT");
				stkOutHdr.setFromBranch(SystemSetting.getSystemBranch());
				// stkOutHdr.setVoucherNumber("null");
				stkOutHdr.setVoucherDate(Date.valueOf(dpStkTrasferDate.getValue()));
				ResponseEntity<StockTransferOutHdr> respentity = RestCaller.saveStockTransferOutHdr(stkOutHdr);
				stkOutHdr = respentity.getBody();
				System.out.println("Stock Hdr:" + stkOutHdr);

			}

			Double prevQty = 0.0;
			ResponseEntity<List<StockTransferOutDtl>> getStockOutDtlPrevious = RestCaller
					.getStockTransferOutDtlByItemIdAndBatchAndHdrId(stkOutHdr.getId(), stockTransferOutDtl.getItemId().getId(),
							txtBatchCode.getText());
			// StockTransferList =
			// FXCollections.observableArrayList(getStockOutDtlPrevious.getBody());
			for (StockTransferOutDtl stkdtl : getStockOutDtlPrevious.getBody()) {
				prevQty = prevQty + stkdtl.getQty();
			}
			if (prevQty + Double.parseDouble(txtQty.getText()) > chkQty && !SystemSetting.isNEGATIVEBILLING()) {
				notifyMessage(1, "Not in Stock!!!");
				txtQty.clear();
				txtItemName.clear();
				txtBarcode.clear();
				txtBatchCode.clear();
				txtRate.clear();
				txtUnit.clear();
				txtMRP.clear();
				txtBarcode.requestFocus();
				return;
			}
			count = count + 1;
			stockTransferOutDtl.setSlNo(count);
			stockTransferOutDtl.setStockTransferOutHdr(stkOutHdr);
			stockTransferOutDtl.setItemNameProperty(txtItemName.getText());
			stockTransferOutDtl.setBatch(txtBatchCode.getText());
			stockTransferOutDtl.setQty(Double.parseDouble(txtQty.getText()));
			stockTransferOutDtl.setRate(Double.parseDouble(txtRate.getText()));
			stockTransferOutDtl.setBarcode(txtBarcode.getText());
			stockTransferOutDtl.setMrp(Double.parseDouble(txtMRP.getText()));
			stockTransferOutDtl.setTaxRate(0.0);
			stockTransferOutDtl.setStoreName(txtStore.getText());

			if (null == dpExpiryDate.getValue()) {
				stockTransferOutDtl.setexpiryDate(null);
			} else {
				stockTransferOutDtl.setexpiryDate(Date.valueOf(dpExpiryDate.getValue()));
			}
			amount = Double.parseDouble(txtQty.getText()) * Double.parseDouble(txtRate.getText());
			stockTransferOutDtl.setAmount(amount);
			System.out.println("Stock Detail:" + stockTransferOutDtl);
			
			if(null == stockTransferOutDtl.getBarcode() || getItem.getBody().getBarCode() != stockTransferOutDtl.getBarcode()) {
				
				stockTransferOutDtl.setBarcode(getItem.getBody().getBarCode());
			}
			
			ResponseEntity<StockTransferOutDtl> respentity = RestCaller.saveStockTransferOutDtl(stockTransferOutDtl);
//			if (stockTransferOutDtl.getId() == null) {
//
//
//				stockTransferOutDtl = respentity.getBody();
//				setTotal();
//				stockTransferOutDtl.setItemName(txtItemName.getText());
//				StockTransferList.add(stockTransferOutDtl);
//
//				FillTable();
//				clearfield();
//				stockTransferOutDtl = new StockTransferOutDtl();
//				txtBarcode.requestFocus();
//
//			} else {

			tblStockTrasfer.getItems().clear();
			ResponseEntity<List<StockTransferOutDtl>> stockTransferoutdtlSaved = RestCaller
					.getStockTransferOutDtl(stkOutHdr);

			StockTransferList = FXCollections.observableArrayList(stockTransferoutdtlSaved.getBody());
			for (StockTransferOutDtl stk : StockTransferList) {
//				ResponseEntity<ItemMst> respentityitem = RestCaller.getitemMst(stk.getItemMst().getId());
				stk.setItemName(stk.getItemId().getItemName());

			}
			setTotal();
			tblStockTrasfer.setItems(StockTransferList);
			FillTable();

			clearfield();
			stockTransferOutDtl = new StockTransferOutDtl();
			txtBarcode.requestFocus();
//			}
			dpExpiryDate.setValue(null);

		}
//		} catch (Exception e) {
//			e.printStackTrace();
//			stkOutHdr = null;
//			System.out.println(e.toString());
//			return;
//		}

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void loadintentPopup() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/IntentPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//					dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void printBarcode(StockTransferOutHdr stockTransferouthdr) {
		int i = 0;

		ResponseEntity<List<StockTransferOutDtl>> stocklist = RestCaller.getStockTransferOutDtl(stockTransferouthdr);
		for (i = 0; i < stocklist.getBody().size(); i++) {
			ResponseEntity<ItemMst> itemmst = RestCaller.getitemMst(stocklist.getBody().get(i).getItemId().getId());

//				Alert a = new Alert(AlertType.CONFIRMATION);
//				a.setHeaderText("Printing...");
//				a.setContentText("Do you want to Print Barcode for "+itemmst.getBody().getItemName());
//				a.showAndWait().ifPresent((btnType) -> 
//				{
//					if (btnType == ButtonType.OK) 
//				{
//				
			LocalDate ldate = dpManfctrDate.getValue();
			java.util.Date uProdDate = SystemSetting.localToUtilDate(ldate);
			String sProdDate = SystemSetting.UtilDateToString(uProdDate, "dd-MM-yyyy");

			String ManufactureDate = "Packed:" + sProdDate;
			LocalDate expdate = ldate.plusDays(itemmst.getBody().getBestBefore());
			java.util.Date uexpdate = SystemSetting.localToUtilDate(expdate);
			String ExpiryDate = "BestBefore:" + SystemSetting.UtilDateToString(uexpdate, "dd-MM-yyyy");
			String ItemName = itemmst.getBody().getItemName();
			String Barcode = stocklist.getBody().get(i).getBarcode();
			String NumberOfCopies = stocklist.getBody().get(i).getQty().toString();
			String Mrp = "Mrp:" + itemmst.getBody().getStandardPrice();
			String IngLine1 = "Ingredients:" + itemmst.getBody().getBarCodeLine1();
			String IngLine2 = itemmst.getBody().getBarCodeLine2();
			String PrinterNAme = SystemSetting.printer_name;
			String netWt = itemmst.getBody().getNetWeight();
			String batch = stocklist.getBody().get(i).getBatch();
			TSCBarcode.manualPrint(NumberOfCopies, ManufactureDate, ExpiryDate, ItemName, Barcode, Mrp, IngLine1,
					IngLine2, PrinterNAme, netWt, batch);

//				} else if (btnType == ButtonType.CANCEL) {
			//
//					return;
			//
//				}
//			});
		}
	}

	@Subscribe
	public void inetntListner(IntentPopupEvent intentPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnstockTransferAdd.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtIntentNo.setText(intentPopupEvent.getIntentNumber());
					intentInHdr = new IntentInHdr();
					intentInHdr.setId(txtIntentNo.getText());
//				try
//				{
//				ArrayList branch = new ArrayList();
//				RestTemplate restTemplate = new RestTemplate();
//				branch = RestCaller.SearchBranch();
//				Iterator itr = branch.iterator();
//				while (itr.hasNext()) {
//					LinkedHashMap lm = (LinkedHashMap) itr.next();
//					Object branchCode = lm.get("branchCode");
//					String toBranch = intentPopupEvent.getFromBranch();
//					if(toBranch.equalsIgnoreCase((String)branchCode))
//					{
//						Object branchName = lm.get("branchName");
//						cmbToBranch.getSelectionModel().select((String)branchName);
//					}
//				}
				}
			});
		}
//				catch(Exception e)
//				{
//					return;
//				}
//			
	}

//		}
	@FXML
	void fetchData(ActionEvent event) {
		ResponseEntity<List<IntentInDtl>> respintentSaved = RestCaller.getintentdtlsbyId(txtIntentNo.getText());
		intentList = FXCollections.observableArrayList(respintentSaved.getBody());

		if (null == stkOutHdr) {
			stkOutHdr.setIntentNumber(txtIntentNo.getText());
			try {
				ArrayList branch = new ArrayList();

				branch = RestCaller.SearchBranch();
				Iterator itr = branch.iterator();
				while (itr.hasNext()) {
					LinkedHashMap lm = (LinkedHashMap) itr.next();
					Object branchName = lm.get("branchName");
					String toBranch = cmbToBranch.getSelectionModel().getSelectedItem().toString();
					if (toBranch.equalsIgnoreCase((String) branchName)) {
						Object branchCode = lm.get("branchCode");
						stkOutHdr.setToBranch((String) branchCode);
					}
				}
			} catch (Exception e) {
				return;
			}
			stkOutHdr.setFromBranch(SystemSetting.getSystemBranch());
			stkOutHdr.setVoucherDate(Date.valueOf(dpStkTrasferDate.getValue()));
			stkOutHdr.setVoucherType("STKTROUTTYPE");

			ResponseEntity<StockTransferOutHdr> respentity = RestCaller.saveStockTransferOutHdr(stkOutHdr);
			stkOutHdr = respentity.getBody();
		}
		for (IntentInDtl intent : intentList) {
			stockTransferOutDtl = new StockTransferOutDtl();
			ArrayList items = new ArrayList();
			items = RestCaller.SearchStockItemByName(intent.getItemId());
			ResponseEntity<ItemMst> itemmst= RestCaller.getitemMst(intent.getItemId());
			stockTransferOutDtl.setItemId(itemmst.getBody());
			String itemName = "";
			String barcode = "";
			Double mrp = 0.0;
			String unitname = "";
			Double qty = 0.0;
			Double cess = 0.0;
			Double tax = 0.0;
			String itemId = "";
			String unitId = "";
			Iterator itr = items.iterator();
			String batch = "";

			while (itr.hasNext()) {

				List element = (List) itr.next();
				itemName = (String) element.get(0);
				barcode = (String) element.get(1);
				unitname = (String) element.get(2);
				mrp = (Double) element.get(3);
				qty = (Double) element.get(4);
				cess = (Double) element.get(5);
				tax = (Double) element.get(6);
				itemId = (String) element.get(7);
				unitId = (String) element.get(8);
				batch = (String) element.get(9);
				if (null != itemName) {
					stockTransferOutDtl.setBarcode(barcode);
					stockTransferOutDtl.setBatch(batch);
					stockTransferOutDtl.setItemName(itemName);
					stockTransferOutDtl.setMrp(mrp);
					stockTransferOutDtl.setQty(qty);
					stockTransferOutDtl.setQty(intent.getQty());
					stockTransferOutDtl.setRate(mrp);
					stockTransferOutDtl.setStockTransferOutHdr(stkOutHdr);
					stockTransferOutDtl.setTaxRate(tax);
					stockTransferOutDtl.setUnitId(unitname);
					stockTransferOutDtl.setAmount(stockTransferOutDtl.getQty() * intent.getRate());
					if (null == dpExpiryDate.getValue()) {
						stockTransferOutDtl.setexpiryDate(null);
					} else {
						stockTransferOutDtl.setexpiryDate(Date.valueOf(dpExpiryDate.getValue()));
					}
					ResponseEntity<StockTransferOutDtl> respentity = RestCaller
							.saveStockTransferOutDtl(stockTransferOutDtl);
					stockTransferOutDtl = respentity.getBody();
					setTotal();
					stockTransferOutDtl.setItemName(itemName);
					StockTransferList.add(stockTransferOutDtl);
					FillTable();

				}
			}
		}
	}

	private void showPopupByBarcode() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			ItemStockPopupCtl popctl = fxmlLoader.getController();
			popctl.LoadItemPopupByBarcode(txtBarcode.getText());

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

	}

}
