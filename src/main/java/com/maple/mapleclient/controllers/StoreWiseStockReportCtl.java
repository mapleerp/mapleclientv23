package com.maple.mapleclient.controllers;

import java.text.DecimalFormat;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.text.NumberFormat;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.mapleclient.entity.ReorderMst;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DailyPaymentSummaryReport;
import com.maple.report.entity.StockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class StoreWiseStockReportCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<StockReport> reorderList = FXCollections.observableArrayList();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");

	@FXML
	private ComboBox<String> cmbStore;

	@FXML
	private Button btnGenarateReport;

	@FXML
	private TableView<StockReport> tblStockReport;

	@FXML
	private TableColumn<StockReport, String> clItem;

	@FXML
	private TableColumn<StockReport, Number> clQty;
	
    @FXML
    private TableColumn<StockReport, String> clStore;

	@FXML
	private Button btnPrintReport;

	@FXML
	void PrintReport(ActionEvent event) {
		String store = null;

		if (null == cmbStore.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Please select store...!", false);
			return;
		}

		store = cmbStore.getSelectionModel().getSelectedItem().toString();
		ResponseEntity<List<StoreMst>> storeMstResp = RestCaller.StoreMstByStoreName(store);
		List<StoreMst> storeMstList = storeMstResp.getBody();
		try {
			JasperPdfReportService.storeWiseStockReport(storeMstList.get(0).getShortCode());
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private void initialize() {

		ResponseEntity<List<StoreMst>> storeMstList = RestCaller.getAllStoreMst();
		List<StoreMst> storeList = storeMstList.getBody();

		for (StoreMst store : storeList) {
			cmbStore.getItems().add(store.getName());
		}
	}

	@FXML
	void GenarateReport(ActionEvent event) {

		String store = null;

		if (null == cmbStore.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Please select store...!", false);
			return;
		}

		store = cmbStore.getSelectionModel().getSelectedItem().toString();

		ResponseEntity<List<StoreMst>> storeMstResp = RestCaller.StoreMstByStoreName(store);
		List<StoreMst> storeMstList = storeMstResp.getBody();

		if (storeMstList.size() == 0) {
			notifyMessage(3, "store not present...!", false);
			return;
		}

		ResponseEntity<List<StockReport>> stockReportResp = RestCaller
				.getStoreWiseStockReport(storeMstList.get(0).getShortCode());
		reorderList = FXCollections.observableArrayList(stockReportResp.getBody());

		tblStockReport.setItems(reorderList);

		clItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());

		clQty.setCellFactory(tc -> new TableCell<StockReport, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		
		clStore.setCellValueFactory(cellData -> cellData.getValue().getStoreNameProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}


}
