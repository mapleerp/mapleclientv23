package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PharmacyItemWiseSalesReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyItemWiseSalesReportCtl {
//  (0_0)       SharonN  
	
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PharmacyItemWiseSalesReport> PharmacyItemWiseSalesReportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dptoDate;

    @FXML
    private Button btnPrintReport;

    @FXML
    private Button btnGenerate;

    @FXML
    private DatePicker dpfromDate;
    
    @FXML
    private ListView<String> lstItem;

    @FXML
    private TextField txtItemName;
    
    @FXML
    private Button btnAdd;

    @FXML
    private TableView<PharmacyItemWiseSalesReport> tblItemWiseSalesreport;

    @FXML
    private TableColumn<PharmacyItemWiseSalesReport, String> clinvoiceDate;

    @FXML
    private TableColumn<PharmacyItemWiseSalesReport,String> clCustomerName;

    @FXML
    private TableColumn<PharmacyItemWiseSalesReport, String> clitem;

    @FXML
    private TableColumn<PharmacyItemWiseSalesReport, Number> clqty;

    @FXML
    private TableColumn<PharmacyItemWiseSalesReport, Number> clunitPrice;
    @FXML
    
    String itename="";
    @FXML
	private void initialize() {
		dpfromDate = SystemSetting.datePickerFormat(dpfromDate, "dd/MMM/yyyy");
		dptoDate = SystemSetting.datePickerFormat(dptoDate, "dd/MMM/yyyy");
		eventBus.register(this);
   	    lstItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		tblItemWiseSalesreport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getCustomerName()) {

					
					fillTable();


				}
			}
		});

	}
    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null == dpfromDate.getValue()) {

			notifyMessage(3, "please select From date", false);
			dpfromDate.requestFocus();
			return;
		}
		if (null == dptoDate.getValue()) {

			notifyMessage(3, "please select Todate", false);
			dptoDate.requestFocus();
			return;
		}
		if(null==lstItem.getSelectionModel().getSelectedItem()) {
    		notifyMessage(5, "plz add Item Name",false);
    		return;
    	}
    	
		
		
     List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
        
    	//String itename="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		itename= itename.concat(s);
    	} 

		java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dptoDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		//ResponseEntity<List<PharmacyItemWiseSalesReport>> pharmacyItemWiseSalesReportResp = RestCaller.getPharmacyItemWiseSalesReport(fromdate, todate);
		ResponseEntity<List<PharmacyItemWiseSalesReport>> pharmacyItemWiseSalesReportResp = RestCaller.getPharmacyItemWiseSalesReportByItemName(fromdate, todate,itename);
		PharmacyItemWiseSalesReportList = FXCollections.observableArrayList(pharmacyItemWiseSalesReportResp.getBody());
		 itename="";

		fillTable();
    }
    
    @FXML
    void clear(ActionEvent event) {
    	clear();
    }
    private void clear() {
		
    	dptoDate.setValue(null);
    	dpfromDate.setValue(null);
		 lstItem.getItems().clear();
		 txtItemName.clear();
		  tblItemWiseSalesreport.getItems().clear();
		  itename="";
		
	}
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
private void fillTable() {
	tblItemWiseSalesreport.setItems(PharmacyItemWiseSalesReportList);

	clinvoiceDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
	clitem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
	clqty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
	clunitPrice.setCellValueFactory(cellData -> cellData.getValue().getUnitPriceProperty());
}
    @FXML
    void PrintReport(ActionEvent event) {
    	
    	if (null == dpfromDate.getValue()) {

			notifyMessage(3, "please select From date", false);
			dpfromDate.requestFocus();
			return;
		}
		if (null == dptoDate.getValue()) {

			notifyMessage(3, "please select Todate", false);
			dptoDate.requestFocus();
			return;
		}
		if(null==lstItem.getSelectionModel().getSelectedItem()) {
    		notifyMessage(5, "plz add Item Name",false);
    		return;
    	}
		
		 List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
	        
	    	//String itename="";
	    	for(String s:selectedItems)
	    	{
	    		s = s.concat(";");
	    		itename= itename.concat(s);
	    	} 
    	java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dptoDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		try {
			JasperPdfReportService.PharmacyItemWiseSalesReport(fromdate, todate, itename);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 itename="";
	}
    
    
    
    @FXML
    void onEnterGrop(ActionEvent event) {
    	
    	
    	 loadItemPopup();

    }
	private void loadItemPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	   @FXML
	    void AddItem(ActionEvent event) {
		   
			lstItem.getItems().add(txtItemName.getText());
	    	lstItem.getSelectionModel().select(txtItemName.getText());
	    	txtItemName.clear();
	    

	    }
	
	 @Subscribe
		public void popupOrglistner(ItemPopupEvent itemPopupEvent) {

			Stage stage = (Stage) btnPrintReport.getScene().getWindow();
			if (stage.isShowing()) {

				
				txtItemName.setText(itemPopupEvent.getItemName());
		
			

			}
}
    
    }

