package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.ExportLocalCustomerToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class LoyaltyCustomerReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<LocalCustomerMst> reportList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	ExportLocalCustomerToExcel exportLocalCustomerToExcel = new ExportLocalCustomerToExcel();
    @FXML
    private TextField txtCustomerName;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnExport;
    @FXML
    private TableView<LocalCustomerMst> tbCustomer;

    @FXML
    private TableColumn<LocalCustomerMst, String> clCustomerName;

    @FXML
    private TableColumn<LocalCustomerMst, String> clPhoneNumber;

    @FXML
    private TableColumn<LocalCustomerMst, String> clAddress;

    @FXML
    private TableColumn<LocalCustomerMst, String> clAddress1;

    @FXML
    private TableColumn<LocalCustomerMst, String> clDOB;

    @FXML
    private TableColumn<LocalCustomerMst, String> clWeddingDate;
    @FXML
	private void initialize() {
    	txtCustomerName.textProperty().bindBidirectional(SearchString);
    	
    	SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				System.out.println("item Search changed");
				LoadCustomerBySearch((String) newValue);
			}
		});
    }
    protected void LoadCustomerBySearch(String searchData) {
	
    	reportList.clear();
		ArrayList customer = new ArrayList();
		LocalCustomerMst cust = new LocalCustomerMst();
		RestTemplate restTemplate = new RestTemplate();

		customer = RestCaller.SearchLocalCustomer(searchData);
//		customer = RestCaller.SearchCustomerByName();
		Iterator itr = customer.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			// Iterator itr2 = lm.keySet().iterator();
			// while(itr2.hasNext()) {

			Object localCustomerName = lm.get("localcustomerName");
			Object Address = lm.get("address");
			Object PhoneNo = lm.get("phoneNo");
			Object PhoneNO2 = lm.get("phoneNO2");
			Object id = lm.get("id");
			Object dob = lm.get("dateOfBirth");
			Object weddigdate = lm.get("weddingDate");

			if (null != id) {
				cust = new LocalCustomerMst();
				cust.setAddress((String)Address);
				cust.setLocalcustomerName((String)localCustomerName);
				cust.setPhoneNo((String)PhoneNo);
				cust.setPhoneNO2((String)PhoneNO2);
				cust.setId((String)id);
				if(null != dob)
				{
					cust.setDateOfBirth(SystemSetting.StringToUtilDate((String)dob, "yyyy-MM-dd"));

				}
				if(null != weddigdate)
				{
					cust.setWeddingDate(SystemSetting.StringToUtilDate((String)weddigdate, "yyyy-MM-dd"));

				}

				System.out.println(lm);

				reportList.add(cust);
			}

		}
		fillTable();
		

		return;

    	
	}
	@FXML
    void actionExport(ActionEvent event) {
		ResponseEntity<List<LocalCustomerMst>> locCust = RestCaller.getAllLocalCustomer();
    	reportList = FXCollections.observableArrayList(locCust.getBody());
    	exportLocalCustomerToExcel.exportToExcel("LocalCustomerExcel.xls", reportList);
    }

    @FXML
    void actionShowAll(ActionEvent event) {

    	ResponseEntity<List<LocalCustomerMst>> locCust = RestCaller.getAllLocalCustomer();
    	reportList = FXCollections.observableArrayList(locCust.getBody());
    	fillTable();
    }
    private void fillTable()
    {
    	tbCustomer.setItems(reportList);
		clAddress.setCellValueFactory(cellData -> cellData.getValue().getAddressProperty());
		clAddress1.setCellValueFactory(cellData -> cellData.getValue().getAddressLine1Property());
		clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clDOB.setCellValueFactory(cellData -> cellData.getValue().getDateofBirthProperty());
		clPhoneNumber.setCellValueFactory(cellData -> cellData.getValue().getPhoneNoProperty());
		clWeddingDate.setCellValueFactory(cellData -> cellData.getValue().getWeddingDateProperty());

    }
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }

}
