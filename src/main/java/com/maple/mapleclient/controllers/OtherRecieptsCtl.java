package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.google.common.eventbus.EventBus;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class OtherRecieptsCtl {
	
	
	String processInstanceId;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	ReceiptDtl receipt;
	Double totalamount;
	String taskid;
	ReceiptHdr receipthdr = null;
	ReceiptDtl recieptDelete;
	
	private ObservableList<ReceiptDtl> receiptList1 = FXCollections.observableArrayList();
	@FXML
	private TextField txtAccount;

    @FXML
    private ComboBox<String> cmbModeofReceipt;

	@FXML
	private TextField txtRemark;
	@FXML
	private Button btnClear;

	@FXML
	private TextField txtAmount;

	@FXML
	private TextField txtInstrumentNumber;

	@FXML
	private TextField txtVoucherNumber;

	@FXML
	private ComboBox<String> cmbDepositBank;
	

    @FXML
    private TextField txtDate;
    
	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnFinalSubmit;

	@FXML
	private Button btnShowAll;

	@FXML
	private DatePicker dpTransDate;

	@FXML
	private DatePicker dpInstrumentDate;

	@FXML
	private TextField txtTotal;

	@FXML
	private TableView<ReceiptDtl> tblReceiptWindow;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC1;

	@FXML
	private TableColumn<ReceiptDtl, LocalDate> RWC2;

	@FXML

	private TableColumn<ReceiptDtl, String> RWC4;

	@FXML
	private TableColumn<ReceiptDtl, Number> RWC5;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC6;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC7;

	@FXML
	private TableColumn<ReceiptDtl, LocalDate> RWC8;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC9;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC11;

	@FXML
	private void initialize() {
		dpInstrumentDate = SystemSetting.datePickerFormat(dpInstrumentDate, "dd/MMM/yyyy");
		dpTransDate = SystemSetting.datePickerFormat(dpTransDate, "dd/MMM/yyyy");
		txtDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd"));
		eventBus.register(this);
		
		//------------new version surya 1.9 
		
//		ResponseEntity<AccountHeads> accountHead = RestCaller
//				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
//
//		cmbModeofReceipt.getItems().add("TRASNFER");
//		cmbModeofReceipt.getItems().add("CHEQUE");
//		cmbModeofReceipt.getItems().add("DD");
//		cmbModeofReceipt.getItems().add("MOBILE PAY");
//		cmbModeofReceipt.getItems().add(accountHead.getBody().getAccountName());
		
		ResponseEntity<List<ReceiptModeMst>> receiptModeMstListResp = RestCaller.getAllReceiptMode();
		List<ReceiptModeMst> receiptModeMstList = receiptModeMstListResp.getBody();
		
		for(ReceiptModeMst receiptModeMst : receiptModeMstList)
		{
			cmbModeofReceipt.getItems().add(receiptModeMst.getReceiptMode());
		}
		//------------new version surya 1.9 end


		cmbModeofReceipt.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				//------------------new version 1.9 surya 
			if(null!=newValue) {
				ResponseEntity<List<ReceiptModeMst>> receiptmodeList = RestCaller.getReceiptModeMstByName(newValue);
				ReceiptModeMst receiptModeMst = receiptmodeList.getBody().get(0);
				//------------------new version 1.9 surya end

				if (newValue.matches(SystemSetting.getSystemBranch() + "-CASH")) {
					cmbDepositBank.setDisable(true);
					txtInstrumentNumber.setDisable(true);
					dpInstrumentDate.setDisable(true);
				} 
				//------------------new version 1.9 surya 
				else if (receiptModeMst.getCreditCardStatus().equalsIgnoreCase("YES")) {
					
					cmbDepositBank.setDisable(true);
					txtInstrumentNumber.setDisable(true);
					dpInstrumentDate.setDisable(true);
				}
				//------------------new version 1.9 surya end

				else {
				
					cmbDepositBank.setDisable(false);
					txtInstrumentNumber.setDisable(false);
					dpInstrumentDate.setDisable(false);
				}
				
			}
			}

		});
		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
		ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid = lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbDepositBank.getItems().add((String) bankName);

				// accountHeads.getAccountName);
			}
		}
		tblReceiptWindow.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					recieptDelete = new ReceiptDtl();
					recieptDelete.setId(newSelection.getId());
				}
			}
		});
	}

	private void clearfields() {
		txtAccount.clear();
		txtAmount.clear();
		cmbDepositBank.getSelectionModel().clearSelection();
		cmbModeofReceipt.getSelectionModel().clearSelection();
		txtInstrumentNumber.clear();
		txtRemark.clear();
	}

	private void filltable() {
		tblReceiptWindow.setItems(receiptList1);
		RWC1.setCellValueFactory(cellData -> cellData.getValue().getaccountProperty());
		RWC11.setCellValueFactory(cellData -> cellData.getValue().getremarkProperty());
		RWC5.setCellValueFactory(cellData -> cellData.getValue().getamountPropery());
		RWC6.setCellValueFactory(cellData -> cellData.getValue().getdepositBankProperty());
		RWC2.setCellValueFactory(cellData -> cellData.getValue().gettransDateProperty());
		RWC8.setCellValueFactory(cellData -> cellData.getValue().getInstrumentDateProperty());
		RWC4.setCellValueFactory(cellData -> cellData.getValue().getmodeOfPaymentProperty());
		RWC7.setCellValueFactory(cellData -> cellData.getValue().getinstnumberProperty());
		RWC9.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
	}

	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Accounts");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbModeofReceipt.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	
	@Subscribe
	public void popuplistner(AccountPopupEvent accountPopupEvent) {

		System.out.println("------AccountEvent-------toAccount-------------");
		Stage stage = (Stage) btnClear.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountPopupEvent.getAccountName());

		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * @Subscribe public void popuplistner(AccountEvent accountEvent) {
	 * 
	 * System.out.println("------AccountEvent-------popuplistner-------------");
	 * Stage stage = (Stage) btnClear.getScene().getWindow(); if (stage.isShowing())
	 * { Platform.runLater(new Runnable() {
	 * 
	 * @Override public void run() {
	 * txtAccount.setText(accountEvent.getAccountName()); } }); }
	 * 
	 * }
	 */

	private void addItem() {
		btnFinalSubmit.setDisable(false);
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr)
			if (null != dayEndClosureHdr.getDayEndStatus()) {
				{
					String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(),
							"yyyy-MM-dd");
					String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
					java.sql.Date prDate = Date.valueOf(process_date);
					Date sDate = Date.valueOf(sysdate);
					int i = prDate.compareTo(sDate);
					if (i > 0 || i == 0) {
						notifyMessage(5, " Day End Already Done for this Date !!!!");
						return;
					}
				}
			}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}
//		if(txtAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch+"-PETTY CASH"))
//		{
//			notifyMessage(5, "Petty Cash Receipt Entry Not POssible From Here!!!!");
//			txtAccount.clear();
//			txtAccount.requestFocus();
//			return;
//		}
		if(txtAccount.getText().trim().equalsIgnoreCase(cmbModeofReceipt.getSelectionModel().getSelectedItem()))
		{
			notifyMessage(5,"Debit And Credit Accounts should not be same");
			return;
		}
		
		if (txtAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "Select Account Number!!!!");
			txtAccount.requestFocus();
			return;
		}  if (cmbModeofReceipt.getSelectionModel().isEmpty()) {
			notifyMessage(5, "Select Mode Of Receipt!!!!");
			cmbModeofReceipt.requestFocus();

			return;
		}  if (txtAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "Type Amount!!!!");
			txtAmount.requestFocus();
			return;
		} 
//		if (null == dpTransDate.getValue()) {
//			notifyMessage(5, "Please Select Date!!!!");
//			dpTransDate.requestFocus();
//			return;
//		} 
		
			if (null == receipthdr) {
				receipthdr = new ReceiptHdr();

				receipthdr.setTransdate(SystemSetting.systemDate);
				receipthdr.setVoucherDate(SystemSetting.systemDate);
				receipthdr.setBranchCode(SystemSetting.systemBranch);

				ResponseEntity<ReceiptHdr> respentity = RestCaller.saveReceipthdr(receipthdr);
				receipthdr = respentity.getBody();
			}
			receipt = new ReceiptDtl();
			ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountHeadByName(txtAccount.getText());

			receipt.setAccount(accSaved.getBody().getId());
			receipt.setRemark(receipt.getRemark());
			receipt.setAmount(Double.parseDouble(txtAmount.getText()));
			
			//-----------------new version 1.9 surya 
			ResponseEntity<List<ReceiptModeMst>> receiptmodeList = RestCaller.
					getReceiptModeMstByName(cmbModeofReceipt.getSelectionModel().getSelectedItem().toString());
			
			ReceiptModeMst receiptModeMst = receiptmodeList.getBody().get(0);
			//-----------------new version 1.9 surya end

			
			if (cmbModeofReceipt.getSelectionModel().getSelectedItem().toString()
					.equalsIgnoreCase(SystemSetting.getSystemBranch() + "-CASH")) {
				receipt.setInstrumentDate(null);
				receipt.setRemark(txtRemark.getText());

				receipt.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));

				receipt.setModeOfpayment(cmbModeofReceipt.getSelectionModel().getSelectedItem());
				receipt.setReceipthdr(receipthdr);

				ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
						.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
				if (null == accountHeadsResp.getBody()) {
					notifyMessage(5, "Branch Cash account not set");
					return;
				}
				AccountHeads accountHeads = accountHeadsResp.getBody();
				receipt.setDebitAccountId(accountHeads.getId());

				ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);

				receipt = respentity.getBody();
				setTotal();
				receipt.setAccountName(txtAccount.getText());
				receiptList1.add(receipt);
				filltable();
			} 
			
			
			//----------------new version 1.9 surya 
			else if (receiptModeMst.getCreditCardStatus().equalsIgnoreCase("YES")) {


				if (cmbDepositBank.getSelectionModel().isEmpty()) {
					notifyMessage(5, "Type Deposit Bank!!!!");
					cmbDepositBank.setDisable(false);

					cmbDepositBank.requestFocus();
					return;
				}

//						receipt.setInstrumentDate(Date.valueOf(dpInstrumentDate.getValue()));
//						receipt.setInstnumber(txtInstrumentNumber.getText());
				receipt.setDepositBank(cmbDepositBank.getSelectionModel().getSelectedItem());
				receipt.setRemark(txtRemark.getText());

				ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadByName(receipt.getDepositBank());

				if (null == accountHeadsResp.getBody()) {

					notifyMessage(5, " Bank account not set");
					return;
				}

				receipt.setDebitAccountId(accountHeadsResp.getBody().getId());

				receipt.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
				receipt.setInvoiceNumber(txtVoucherNumber.getText());
				receipt.setModeOfpayment(cmbModeofReceipt.getSelectionModel().getSelectedItem());
				receipt.setReceipthdr(receipthdr);

				receipt.setAccountName(txtAccount.getText());
				ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);
				receipt = respentity.getBody();
				setTotal();
				receiptList1.add(receipt);
				filltable();

				
			}
			//----------------new version 1.9 surya end

			else 
			{
				if (txtInstrumentNumber.getText().trim().isEmpty()) {
					notifyMessage(5, "Type Instrument Number!!!!");
					txtInstrumentNumber.setDisable(false);
					txtInstrumentNumber.requestFocus();
					return;
				} 
				if (cmbDepositBank.getSelectionModel().isEmpty()) {
					notifyMessage(5, "Type Deposit Bank!!!!");
					cmbDepositBank.setDisable(false);
					cmbDepositBank.requestFocus();
					return;
				} 
				if (null == dpInstrumentDate.getValue()) {
					notifyMessage(5, "Select Instrument Date!!!!");
					dpInstrumentDate.setDisable(false);
					dpInstrumentDate.requestFocus();
					return;
				}
					receipt.setInstrumentDate(Date.valueOf(dpInstrumentDate.getValue()));
					receipt.setInstnumber(txtInstrumentNumber.getText());
					receipt.setDepositBank(cmbDepositBank.getSelectionModel().getSelectedItem());
					receipt.setRemark(txtRemark.getText());
					receipt.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));

					receipt.setModeOfpayment(cmbModeofReceipt.getSelectionModel().getSelectedItem());
					receipt.setReceipthdr(receipthdr);

					ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
							.getAccountHeadByName(receipt.getDepositBank());

					if (null == accountHeadsResp.getBody()) {

						notifyMessage(5, " Bank account not set");
						return;
					}

					receipt.setDebitAccountId(accountHeadsResp.getBody().getId());

					ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);
					receipt = respentity.getBody();

					setTotal();
					receipt.setAccountName(txtAccount.getText());
					receiptList1.add(receipt);
					filltable();
				}

			
		
		txtAccount.requestFocus();
		clearfields();
		btnSave.setDisable(true);
	}

	@FXML
	void bankOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpInstrumentDate.requestFocus();

		}
	}

	@FXML
	void modeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtRemark.requestFocus();

		}
	}

	@FXML
	void clear(ActionEvent event) {

		txtTotal.clear();
	}

	@FXML
	void save(ActionEvent event) {
		addItem();
	}

	@FXML
	void loadpopup(MouseEvent event) {
		showPopup();
	}

	@FXML
	void OnEnterClick(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtAccount.getText().trim().isEmpty())
				cmbModeofReceipt.requestFocus();
			else
				showPopup();

		}
	}

	@FXML
	void amountKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtInstrumentNumber.isDisable()) {
				btnSave.requestFocus();
			} else
				txtInstrumentNumber.requestFocus();
		}
	}

	@FXML
	void instrumnetOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbDepositBank.requestFocus();
		}
	}

	@FXML
	void bankOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpInstrumentDate.requestFocus();
		}
	}

	@FXML
	void SaveOnKeyPrss(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void voucherDateKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();

		}
	}

	@FXML
	void invoiceKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();

		}
	}

	@FXML
	void instrumentDate(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}
	}

	@FXML
	void RemarkKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();

		}
	}

	@FXML
	void delete(ActionEvent event) {
		btnSave.setDisable(false);
		RestCaller.deleteReceiptDtl(recieptDelete.getId());
		notifyMessage(5, "Deleted");

		tblReceiptWindow.getItems().clear();
		ResponseEntity<List<ReceiptDtl>> receiptdtlSaved = RestCaller.getReceiptDtl(receipthdr);
		receiptList1 = FXCollections.observableArrayList(receiptdtlSaved.getBody());
		for (ReceiptDtl recptdtl : receiptList1) {
			ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountById(recptdtl.getAccount());
			recptdtl.setAccountName(accSaved.getBody().getAccountName());
		}
		tblReceiptWindow.setItems(receiptList1);
		txtTotal.clear();
		setTotal();
		
	}

	@FXML
	void finalSubmit(ActionEvent event) {
		btnFinalSubmit.setDisable(true);
		btnSave.setDisable(false);
		ResponseEntity<List<ReceiptDtl>> receiptdtlSaved = RestCaller.getReceiptDtl(receipthdr);
		if (receiptdtlSaved.getBody().size() == 0) {
			return;
		}
		String financialYear = SystemSetting.getFinancialYear();
		String vNo = RestCaller.getVoucherNumber(financialYear + "RECEIPT");
		receipthdr.setVoucherNumber(vNo);
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		RestCaller.updateReceipthdr(receipthdr);
		for (ReceiptDtl rcpt : receiptList1) {
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(receipthdr.getBranchCode());
			ResponseEntity<AccountHeads> accountHead = RestCaller
					.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
			AccountHeads accountHeads = accountHead.getBody();
			if (rcpt.getModeOfpayment().equalsIgnoreCase(accountHeads.getAccountName())) {
				dayBook.setDrAccountName(SystemSetting.systemBranch + "-CASH");
				dayBook.setCrAmount(0.0);
			}

			else {
				dayBook.setDrAccountName(rcpt.getDepositBank());
				dayBook.setCrAmount(rcpt.getAmount());
			}
			dayBook.setDrAmount(rcpt.getAmount());
			ResponseEntity<AccountHeads> accountHead1 = RestCaller.getAccountById(rcpt.getAccount());
			AccountHeads accountHeads1 = accountHead1.getBody();
			dayBook.setNarration(accountHeads1.getAccountName() + receipthdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(receipthdr.getVoucherNumber());
			dayBook.setSourceVoucherType("RECEIPT");
			dayBook.setCrAccountName(accountHeads1.getAccountName());

			LocalDate ldate = SystemSetting.utilToLocaDate(receipthdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(ldate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}

		String strDate = formatter.format(receipthdr.getVoucherDate());
		txtTotal.clear();
		notifyMessage(5, "Saved Successfully!!!!");
		tblReceiptWindow.getItems().clear();
		String voucherNo =receipthdr.getVoucherNumber();
		ResponseEntity<AccountHeads> customerMst = RestCaller.getAccountHeadsById(receiptdtlSaved.getBody().get(0).getAccount());
		if(null != customerMst.getBody())
		{
		Double amt = RestCaller.ReceiptInvoiceSettlement(receiptdtlSaved.getBody().get(0).getAccount(), receiptdtlSaved.getBody().get(0).getAmount(), receipthdr.getId(), strDate);
		}
		receipthdr = null;
		if(null != taskid)
		{
			RestCaller.completeTask(taskid);
		}
		taskid = null;
		try {

			JasperPdfReportService.ReceiptInvoice(voucherNo, strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	private void setTotal() {

		totalamount = RestCaller.getSummaryReceiptTotal(receipthdr.getId());
		txtTotal.setText(Double.toString(totalamount));
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			Stage stage = (Stage) btnClear.getScene().getWindow();
			if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				ResponseEntity<AccountHeads> accountHead = RestCaller.getAccountById(taskWindowDataEvent.getAccountId());
				txtAccount.setText(accountHead.getBody().getAccountName());
				SalesTransHdr getSalesHdr = RestCaller.getSalesTransHdrByVoucherAndDate(taskWindowDataEvent.getVoucherNumber(), taskWindowDataEvent.getVoucherDate() );
				if(null != getSalesHdr)
				{
					txtAmount.setText(getSalesHdr.getInvoiceAmount()+"");
				}
			}
	 }
	 


}
