package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.JournalHdr;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.events.AccountEventCr;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;


public class JournalEditCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();

	double totalDebitamount;
	double totalCreditamount;
	
	JournalDtl journalDtlDelete;


	JournalHdr journalHdr = null;
	JournalDtl journalDtl = null;
	String journalHdrId = null;
	private ObservableList<JournalDtl> journalList = FXCollections.observableArrayList();

	@FXML
	private TextField txtCreditAccount;

	@FXML
	private DatePicker dpJournalDate;

	@FXML
	private TextField txtRemarks;

    @FXML
    private Button btnClose;
	@FXML
	private TextField txtDebitAccount;

	@FXML
	private TextField txtAmount;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnClear;

	@FXML
	private Button btnDelete;

	@FXML
	private TextField txtVoucherNumber;

	@FXML
	private Button btnFetchDetails;

	@FXML
	private TableView<JournalDtl> tbJournal;

	@FXML
	private TableColumn<JournalDtl, String> clAccount;

	@FXML
	private TableColumn<JournalDtl, String> clRemarks;

	@FXML
	private TableColumn<JournalDtl, Number> clDebit;

	@FXML
	private TableColumn<JournalDtl, Number> clCredit;

    @FXML
    private DatePicker dpdate;
	@FXML
	private TextField txtDebitTotal;

	@FXML
	private TextField txtCreditTotal;

	@FXML
	private Button btnSubmit;
	

    @FXML
    private ComboBox<String> cmbType;

	@FXML
	private void initialize() {
		dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");
		dpJournalDate = SystemSetting.datePickerFormat(dpJournalDate, "dd/MMM/yyyy");
		eventBus.register(this);
		
		cmbType.getItems().add("CREDIT");
		cmbType.getItems().add("DEBIT");


		
		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
		
		tbJournal.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					
					ResponseEntity<AccountHeads> account = RestCaller.getAccountById(newSelection.getAccountHead());
					AccountHeads accountHeads = account.getBody();
					
					txtCreditAccount.setText(accountHeads.getAccountName());
					if(newSelection.getCreditAmount() != 0)
					{
						cmbType.getSelectionModel().select("CREDIT");
						txtAmount.setText(Double.toString(newSelection.getCreditAmount()));
					}
					
					if(newSelection.getDebitAmount() != 0)
					{
						cmbType.getSelectionModel().select("DEBIT");
						txtAmount.setText(Double.toString(newSelection.getDebitAmount()));

					}
					txtRemarks.setText(newSelection.getRemarks());
					journalDtl = new JournalDtl();
					journalDtl.setId(newSelection.getId());
				}
			}
		});
	}

	@FXML
	void AddJournal(ActionEvent event) {
		saveJournal();
	}

    @FXML
    void actionClose(ActionEvent event) {

    }
	private void saveJournal() {

		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();

		if (txtCreditAccount.getText().trim().equalsIgnoreCase("cash")) {
			notifyMessage(5, "Cash entry not possible",false);
			return;
		}
//		if (txtDebitAccount.getText().trim().equalsIgnoreCase("cash")) {
//			notifyMessage(5, "Cash entry not possible",false);
//			return;
//		}
		if (txtCreditAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch + "-cash")) {
			notifyMessage(5, "Cash entry not possible",false);
			return;
		}
//		if (txtDebitAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch + "-cash")) {
//			notifyMessage(5, "Cash entry not possible",false);
//			return;
//		}
		if (txtCreditAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch + "-petty cash")) {
			notifyMessage(5, "Cash entry not possible",false);
			return;
		}
//		if (txtDebitAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch + "-petty cash")) {
//			notifyMessage(5, "Cash entry not possible",false);
//			return;
//		}
//		if (null == dpJournalDate.getValue()) {
//			notifyMessage(5, "Please Select Date!!",false);
//			dpJournalDate.requestFocus();
//			return;
//		} else 
		if (txtCreditAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Select Account!!",false);
			txtCreditAccount.requestFocus();
			return;
		}
		if (txtAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Type   Amount!!",false);
			txtAmount.requestFocus();
			return;
		}
		
		if(null == cmbType.getValue())
		{
			notifyMessage(5, "Please Select Type Credit/Debit!!",false);
			txtAmount.requestFocus();
			return;
		}

		if (null == journalHdr) {
			journalHdr = new JournalHdr();
			journalHdr.setVoucherDate(Date.valueOf(dpdate.getValue()));

			journalHdr.setBranchCode(SystemSetting.systemBranch);
			journalHdr.setTransDate(Date.valueOf(dpdate.getValue()));
			ResponseEntity<JournalHdr> respentity = RestCaller.saveJournalHdr(journalHdr);
			journalHdr = respentity.getBody();
		}
		if(null != journalDtl)
		{
			if(null == journalDtl.getId())
			{
				journalDtl = new JournalDtl();

			}
		} else {
			journalDtl = new JournalDtl();

		}
		journalDtl.setJournalHdr(journalHdr);

		/*
		 * Find Account head from Account table
		 */

		ResponseEntity<AccountHeads> accountHeadsResponce = RestCaller.getAccountHeadByName(txtCreditAccount.getText());
		AccountHeads accountHeads = accountHeadsResponce.getBody();
		
		String type = cmbType.getSelectionModel().getSelectedItem().toString();
		
		if(type.equalsIgnoreCase("CREDIT"))
		{
			journalDtl.setAccountHead(accountHeads.getId());
			journalDtl.setCreditAmount(Double.parseDouble(txtAmount.getText()));
			journalDtl.setDebitAmount(0.0);
			journalDtl.setRemarks(txtRemarks.getText());
			ResponseEntity<JournalDtl> respentity = RestCaller.saveJournalDtl(journalDtl);
			journalDtl = respentity.getBody();
			ResponseEntity<List<JournalDtl>> journalDtlSaved = RestCaller.getJournalDtl(journalHdr);
			journalList = FXCollections.observableArrayList(journalDtlSaved.getBody());
			clearfields();

			Filltable();
			setTotal();
		}
		
		if(type.equalsIgnoreCase("DEBIT"))
		{
		journalDtl.setJournalHdr(journalHdr);
		ResponseEntity<AccountHeads> accountHeadsdr = RestCaller.getAccountHeadByName(txtCreditAccount.getText());
		AccountHeads accountHeadsDr = accountHeadsdr.getBody();
		journalDtl.setAccountHead(accountHeadsDr.getId());
		journalDtl.setCreditAmount(0.0);
		journalDtl.setDebitAmount(Double.parseDouble(txtAmount.getText()));
		journalDtl.setRemarks(txtRemarks.getText());
		ResponseEntity<JournalDtl> respentity1 = RestCaller.saveJournalDtl(journalDtl);
		journalDtl = respentity1.getBody();
		ResponseEntity<List<JournalDtl>> journalDtlSaved = RestCaller.getJournalDtl(journalHdr);
		journalList = FXCollections.observableArrayList(journalDtlSaved.getBody());
		clearfields();
		Filltable();
		setTotal();
		}
	}
	
	private void clearfields() {
		txtCreditAccount.clear();
		txtAmount.clear();
		txtRemarks.clear();
		cmbType.getSelectionModel().clearSelection();
	}


	@FXML
	void DeleteFields(ActionEvent event) {
		
		if (null != journalDtl.getId()) {
			RestCaller.deleteJournalDtl(journalDtl.getId());
			notifyMessage(5, "Deleted!!!",true);
			tbJournal.getItems().clear();
			txtDebitTotal.clear();
			txtCreditTotal.clear();
			ResponseEntity<List<JournalDtl>> journalDtlSaved = RestCaller.getJournalDtl(journalHdr);
			journalList = FXCollections.observableArrayList(journalDtlSaved.getBody());
			journalDtl = null;
			clearfields();

			Filltable();
			setTotal();
		}

	}

	@FXML
	void FetchDetails(ActionEvent event) {

		if (null != journalHdrId) {

			ResponseEntity<JournalHdr> journalHdrResp = RestCaller.JournalHdrById(journalHdrId);
			journalHdr = journalHdrResp.getBody();
			dpdate.setValue(SystemSetting.utilToLocaDate(journalHdr.getVoucherDate()));
			ResponseEntity<List<JournalDtl>> journalDtlSaved = RestCaller.getJournalDtl(journalHdr);
			journalList = FXCollections.observableArrayList(journalDtlSaved.getBody());

			Filltable();
			setTotal();
		}

	}

	private void Filltable() {
		for (JournalDtl journaDtl : journalList) {
			ResponseEntity<AccountHeads> acc = RestCaller.getAccountById(journaDtl.getAccountHead());
			journaDtl.setAccountHeadName(acc.getBody().getAccountName());
		}
		tbJournal.setItems(journalList);

		clAccount.setCellValueFactory(cellData -> cellData.getValue().getaccountHeadProperty());
		clRemarks.setCellValueFactory(cellData -> cellData.getValue().getremarksProperty());
		clDebit.setCellValueFactory(cellData -> cellData.getValue().getdebitAmountProperty());
		clCredit.setCellValueFactory(cellData -> cellData.getValue().getcreditAmountProperty());
	}

	private void setTotal() {
		totalDebitamount = RestCaller.getJournalDebitTotal(journalHdr.getId());
		txtDebitTotal.setText(Double.toString(totalDebitamount));
		totalCreditamount = RestCaller.getJournalCreditTotal(journalHdr.getId());
		txtCreditTotal.setText(Double.toString(totalCreditamount));
	}

	@FXML
	void VoucherNumberPopup(MouseEvent event) {

		voucherNumberPopup();

	}

	private void voucherNumberPopup() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/JournalVoucherNumberPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			btnFetchDetails.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void accountOnEnter(KeyEvent event) {

	}

	@FXML
	void clearfield(ActionEvent event) {
		
		journalDtl = null;
		journalHdr = null;
		txtCreditAccount.clear();
		txtVoucherNumber.clear();
		txtRemarks.clear();
		txtAmount.clear();
		tbJournal.getItems().clear();
		txtDebitTotal.clear();
		txtCreditTotal.clear();

	}

	@FXML
	void creditOnEnter(KeyEvent event) {

	}

	@FXML
	void debitOnEnter(KeyEvent event) {

	}

	@FXML
	void finalSub(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
    		finalSubmit();
    	}
	}

	@FXML
	void finalSubmit(ActionEvent event) {
		finalSubmit();
	}

	@FXML
	void loadpopUp(MouseEvent event) {
		showPopupCr();
	}
	
	private void showPopupCr() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			 AccountPopup popupctl = loader.getController();
			popupctl.resultEventName="DebitAccount";
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			txtAmount.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void remarkOnEnter(KeyEvent event) {

	}

	@FXML
	void saveOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			saveJournal();
		}
	}

	@FXML
	void showAccountHead(MouseEvent event) {
		showPopup();

	}
	
	private void finalSubmit()
	{
		ResponseEntity<List<JournalDtl>> journalDtlSaved = RestCaller.getJournalDtl(journalHdr);
		journalList = FXCollections.observableArrayList(journalDtlSaved.getBody());
		if (journalDtlSaved.getBody().size() == 0) {
			notifyMessage(5, "Item Not found!!!",false);
			return;
		}
		if (Double.parseDouble(txtDebitTotal.getText()) == Double.parseDouble(txtCreditTotal.getText())) {

			String financialYear = SystemSetting.getFinancialYear();
			ResponseEntity<List<JournalDtl>> journalSaved = RestCaller.getJournalDtl(journalHdr);
			journalList = FXCollections.observableArrayList(journalSaved.getBody());
			if(null != dpdate.getValue())
			{
			journalHdr.setVoucherDate(Date.valueOf(dpdate.getValue()));
//			journalHdr.setTransDate(Date.valueOf(dpdate.getValue()));
			}
			RestCaller.updateJournalFinalSave(journalHdr);
			ResponseEntity<JournalHdr> journalHdrresp = RestCaller.JournalHdrById(journalHdr.getId());
			journalHdr = journalHdrresp.getBody();
			RestCaller.deleteDayBookBySourceVoucher(journalHdr.getVoucherNumber());
			
			for (JournalDtl journal: journalList)
			{
				DayBook dayBook = new DayBook();
				dayBook.setBranchCode(journalHdr.getBranchCode());
				ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountById(journal.getAccountHead());
				AccountHeads accountHeads = accountHead.getBody();
					dayBook.setCrAccountName(accountHeads.getAccountName());
				dayBook.setCrAmount(journal.getCreditAmount());
				dayBook.setNarration(accountHeads.getAccountName()+journalHdr.getVoucherNumber());
				dayBook.setSourceVoucheNumber(journalHdr.getVoucherNumber());
				dayBook.setSourceVoucherType("JOURNAL");
				dayBook.setDrAccountName(accountHeads.getAccountName());
				dayBook.setDrAmount(journal.getDebitAmount());
				LocalDate ldate = SystemSetting.utilToLocaDate(journalHdr.getVoucherDate());
				dayBook.setsourceVoucherDate(Date.valueOf(ldate));
				ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			}
			notifyMessage(5, "Journal Details Saved!!!",true);
			journalHdr = null;
			journalDtl = null;
			clearfields();
			txtCreditTotal.clear();
			txtDebitTotal.clear();
			dpJournalDate.setValue(null);
			tbJournal.getItems().clear();
			journalList.clear();
			txtVoucherNumber.clear();
		} else
			notifyMessage(5, "Credit and Debit Amount Total should be Equal!!!",false);

	}

	
	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			 AccountPopup popupctl = loader.getController();
			popupctl.resultEventName="CreditAccount";
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			txtDebitAccount.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupVoucherNumberListner(VoucherNoEvent voucherNoEvent) {

		Stage stage = (Stage) btnFetchDetails.getScene().getWindow();
		if (stage.isShowing()) {

			txtVoucherNumber.setText(voucherNoEvent.getVoucherNumber());
			journalHdrId = voucherNoEvent.getId();

		}

	}
	
	@Subscribe
	public void popuplistner(AccountEventCr accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			txtCreditAccount.setText(accountEvent.getAccountName());
		}
	}
	
//	@Subscribe
//	public void popuplistnerVoucherNoEvent(VoucherNoEvent vno) {
//
//		System.out.println("------AccountEvent-------popuplistner-------------");
//		Stage stage = (Stage) btnSave.getScene().getWindow();
//		if (stage.isShowing()) {
//
//			txtVoucherNumber.setText(vno.getVoucherNumber());
//		}
//	}
//	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }

}
