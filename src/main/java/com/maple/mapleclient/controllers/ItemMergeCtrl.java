package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.DamageDtl;
import com.maple.mapleclient.entity.ItemMergeMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.ItemPopupEventForMerge;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;


public class ItemMergeCtrl {
	String taskid;
	String processInstanceId;
	
	
	private ObservableList<ItemMergeMst> ItemMergMstList = FXCollections.observableArrayList();
	ItemMergeMst itemMergeMst;
    String fromItemBarCode;
    String fromItemId;
    String toItemId;
    String toItemBarCode;
    
	EventBus eventBus = EventBusFactory.getEventBus();
    @FXML
    private AnchorPane clFromItemName;

    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFinalSave;

    @FXML
    private TextField txtMergeto;

    @FXML
    private TableView<ItemMergeMst> tblItems;
    @FXML
    private DatePicker date;


    @FXML
    private TableColumn<ItemMergeMst, String> clfromtemName;

    @FXML
    private TableColumn<ItemMergeMst, String> clFromitemBarcode;

    @FXML
    private TableColumn<ItemMergeMst, String> cltoItemName;

    @FXML
    private TableColumn<ItemMergeMst, String> cltoItemBarcode;

    @FXML
    private TableColumn<ItemMergeMst, String> clUsername;

    @FXML
    private TableColumn<ItemMergeMst, String> clDate;
    @FXML
    private Button btnShowAll;
    
    @FXML
    void ShowAll(ActionEvent event) {
    	
    	ResponseEntity<List<ItemMergeMst>> itemMergeResp = RestCaller.getAllItemMergeMst();    

    	ItemMergMstList = FXCollections.observableArrayList(itemMergeResp.getBody());
    	FillTable();

    }
    
    @FXML
    void addItems(ActionEvent event) {
    	addItems();
    }
    	
    private void addItems()
    {
    	
    	 if(date.getValue() == null) {
			notifyMessage(5, " Please enter date!!!");
			return;
		} else if ( txtItemName.getText().isEmpty()) {
			notifyMessage(5, " Please enter fromitem...!!!");
			return;
       }else if (txtMergeto.getText().isEmpty()) {
			notifyMessage(5, " Please enter toitem...!!!");
			return;
       }else 
       {
    	ItemMergeMst itemMergeMst=new ItemMergeMst();
    	itemMergeMst.setFromItemName(txtItemName.getText());
    	itemMergeMst.setFromItemId(fromItemId);
        itemMergeMst.setToItemName(txtMergeto.getText());
    	itemMergeMst.setToItemId(toItemId);
    	itemMergeMst.setUserId(SystemSetting.getUser().getUserName());
        itemMergeMst.setFromItemBarCode(fromItemBarCode);
       
        itemMergeMst.setBranchCode(SystemSetting.getSystemBranch());
    	itemMergeMst.setToItemBarCode(toItemBarCode);
    	Date local=Date.valueOf(date.getValue());
    	itemMergeMst.setVoucherDate(local);
    	ResponseEntity<ItemMergeMst> respentity = RestCaller.getItemMergeSave(itemMergeMst);
    	 System.out.println("============"+itemMergeMst);
    	 ItemMergeMst itemMergeMsttest = new ItemMergeMst();
    	 itemMergeMsttest = respentity.getBody();
    	 itemMergeMst = respentity.getBody();
    	 itemMergeMst.getId();
    	ItemMergMstList.add(itemMergeMst);
    	FillTable();
    	notifyMessage(5, "save sucessfully!!!");
    	txtItemName.setText("");
    	txtMergeto.setText("");
    	date.setValue(null);
       }
    	
    }

    @FXML
    void addOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			
    		addItems();
    	}

    }
    @FXML
    void actionDate(KeyEvent event) {
    if (event.getCode() == KeyCode.ENTER) {
			
    	txtItemName.requestFocus();
    	}

    }
    @FXML
    void itemMerge(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		showPopups();
    		btnAdd.requestFocus();
        	}

    }

    @FXML
    void deleteItems(ActionEvent event) {

    }

    @FXML
    void finalSave(ActionEvent event) {
    	
    	RestCaller.updateItemMerge(fromItemId,toItemId,fromItemBarCode);
    	System.out.println("================"+fromItemBarCode);
    	tblItems.getItems().clear();
    	notifyMessage(5, "final save completed!!!");

    	//finalSave();


    }

    @FXML
    void itemOnClick(MouseEvent event) {
    	showPopup();
    }

    @FXML
    void itemOnEnter(KeyEvent event) {
    if (event.getCode() == KeyCode.ENTER) {
			
    		txtMergeto.requestFocus();
    		showPopup();
    
    	}
    }

    @FXML
    void toitemOnClick(MouseEvent event) {
    	showPopups();
    }

    @FXML
    void saveOnKey(KeyEvent event) {
}
    
    
    @FXML
	private void initialize() {
    	date = SystemSetting.datePickerFormat(date, "dd/MMM/yyyy");
    	eventBus.register(this);
    }
    
    
    private void showPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopupForMerge.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}
    
    private void showPopups() {
  		try {
  			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
  			// fxmlLoader.setController(itemStockPopupCtl);
  			Parent root1;

  			root1 = (Parent) fxmlLoader.load();
  			Stage stage = new Stage();

  			stage.initModality(Modality.APPLICATION_MODAL);
  			stage.initStyle(StageStyle.UNDECORATED);
  			stage.setTitle("Stock Item");
  			stage.initModality(Modality.APPLICATION_MODAL);
  			stage.setScene(new Scene(root1));
  			stage.show();

  		} catch (IOException e) {

  			e.printStackTrace();
  		}

  	}
    private void finalSave()
    {
    	System.out.println("inside save.........................");

    	ResponseEntity<List<ItemMergeMst>> itemMergeMstSaved = RestCaller.getItemMergeMst(itemMergeMst);

    	if (itemMergeMstSaved.getBody().size() == 0) {
    		return;
    	} else {

    		//RestCaller.updateItemMerge(itemMergeMst);
    		itemMergeMst = null;
    		tblItems.getItems().clear();
    	
    	
    	
    	}
    }

    @Subscribe
	public void popupMergeItemlistner(ItemPopupEventForMerge itemPopupEvent) {

		Stage stage = (Stage) btnAdd.getScene().getWindow();

		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtItemName.setText(itemPopupEvent.getItemName());
			
			fromItemBarCode=itemPopupEvent.getBarCode();
			fromItemId=itemPopupEvent.getItemId();
			
		}
			});
	}
    }
		
	@Subscribe
	public void popupItemlistner(KitPopupEvent kitPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtMergeto.setText(kitPopupEvent.getItemName());
			toItemBarCode=kitPopupEvent.getBarCode();
			toItemId=kitPopupEvent.getItemId();
				}
			});
		}
	}
	private void FillTable() {

		tblItems.setItems(ItemMergMstList);
		clfromtemName.setCellValueFactory(cellData -> cellData.getValue().getFromItemNameProperty());
		clFromitemBarcode.setCellValueFactory(cellData -> cellData.getValue().getFromItemBarCodeProperty());
		cltoItemName.setCellValueFactory(cellData -> cellData.getValue().getToItemNameProperty());
		cltoItemBarcode.setCellValueFactory(cellData -> cellData.getValue().getToItemBarCodeProperty());
		clUsername.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
		
	}
				
	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}	
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}