package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportGstSalesToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.B2bSalesReport;
import com.maple.report.entity.HsnWisePurchaseDtlReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class GstSalesReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<B2bSalesReport> reportList = FXCollections.observableArrayList();
	ExportGstSalesToExcel exportGstSalesToExcel = new ExportGstSalesToExcel();
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnExport;

    @FXML
    private TableView<B2bSalesReport> tblGstSales;

    @FXML
    private TableColumn<B2bSalesReport, String> clSalesMode;

    @FXML
    private TableColumn<B2bSalesReport, Number> clTaxRate;

    @FXML
    private TableColumn<B2bSalesReport, Number> clTaxableValue;

    @FXML
    private TableColumn<B2bSalesReport, Number> clcgst;

    @FXML
    private TableColumn<B2bSalesReport, Number> clSgst;

    @FXML
    private TableColumn<B2bSalesReport, Number> clcess;

    @FXML
    private TableColumn<B2bSalesReport, Number> clIgstAmount;

    @FXML
    private TableColumn<B2bSalesReport, Number> clTotalAmount;

    @FXML
    private TableColumn<B2bSalesReport, Number> clTaxAmount;
    @FXML
   	private void initialize() {
       	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
       	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    }
    @FXML
    void actionExport(ActionEvent event) {
    	Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		Date utodate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stodate = SystemSetting.UtilDateToString(utodate, "yyyy-MM-dd");
		
    	ResponseEntity<List<B2bSalesReport>> gstSales = RestCaller.getgstSalesReport(date,stodate);
    	reportList = FXCollections.observableArrayList(gstSales.getBody());
    	for(B2bSalesReport b2b:reportList)
    	{
    		
    		BigDecimal bdcess = new BigDecimal(b2b.getCessAmount());
    		bdcess = bdcess.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setCessAmount(bdcess.doubleValue());
    		
    		
    		BigDecimal bdcgst = new BigDecimal(b2b.getCgstAmount());
    		bdcgst = bdcgst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setCgstAmount(bdcgst.doubleValue());
    		
    		BigDecimal bdsgst = new BigDecimal(b2b.getSgstAmount());
    		bdsgst = bdsgst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setSgstAmount(bdsgst.doubleValue());
    		
    		BigDecimal bdigst = new BigDecimal(b2b.getIgstAmount());
    		bdigst = bdigst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setIgstAmount(bdigst.doubleValue());
    		
    		BigDecimal bdtax = new BigDecimal(b2b.getTaxableAmount());
    		bdtax = bdtax.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setTaxableAmount(bdtax.doubleValue());
    		
    		BigDecimal bdtaxAmt = new BigDecimal(b2b.getTaxAmount());
    		bdtaxAmt = bdtaxAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setTaxAmount(bdtaxAmt.doubleValue());
    		
    		BigDecimal bdamount = new BigDecimal(b2b.getAmount());
    		bdamount = bdamount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setAmount(bdamount.doubleValue());
    	}
    	exportGstSalesToExcel.exportToExcel("GstSalesDtl"+date+".xls", reportList);

    }

    @FXML
    void actionPrint(ActionEvent event) {
    	Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		Date utodate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stodate = SystemSetting.UtilDateToString(utodate, "yyyy-MM-dd");
		
		try {
    		JasperPdfReportService.gstSalesReport(date, stodate);
    	} catch (JRException e) {
    		e.printStackTrace();
    	}
    }

    @FXML
    void actionShow(ActionEvent event) {
    	Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		Date utodate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stodate = SystemSetting.UtilDateToString(utodate, "yyyy-MM-dd");
		
    	ResponseEntity<List<B2bSalesReport>> gstSales = RestCaller.getgstSalesReport(date,stodate);
    	reportList = FXCollections.observableArrayList(gstSales.getBody());
    	fillTable();
    }
    private void fillTable()
    {
    	for(B2bSalesReport b2b:reportList)
    	{
    		
    		BigDecimal bdcess = new BigDecimal(b2b.getCessAmount());
    		bdcess = bdcess.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setCessAmount(bdcess.doubleValue());
    		
    		
    		BigDecimal bdcgst = new BigDecimal(b2b.getCgstAmount());
    		bdcgst = bdcgst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setCgstAmount(bdcgst.doubleValue());
    		
    		BigDecimal bdsgst = new BigDecimal(b2b.getSgstAmount());
    		bdsgst = bdsgst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setSgstAmount(bdsgst.doubleValue());
    		
    		BigDecimal bdigst = new BigDecimal(b2b.getIgstAmount());
    		bdigst = bdigst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setIgstAmount(bdigst.doubleValue());
    		
    		BigDecimal bdtax = new BigDecimal(b2b.getTaxableAmount());
    		bdtax = bdtax.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setTaxableAmount(bdtax.doubleValue());
    		
    		BigDecimal bdtaxAmt = new BigDecimal(b2b.getTaxAmount());
    		bdtaxAmt = bdtaxAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setTaxAmount(bdtaxAmt.doubleValue());
    		
    		BigDecimal bdamount = new BigDecimal(b2b.getAmount());
    		bdamount = bdamount.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		b2b.setAmount(bdamount.doubleValue());
    		
    		
    		
    	}
    	tblGstSales.setItems(reportList);
		clcess.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
		clcgst.setCellValueFactory(cellData -> cellData.getValue().getCgstAmountProperty());
		clIgstAmount.setCellValueFactory(cellData -> cellData.getValue().getIgstAmountProperty());
		clSalesMode.setCellValueFactory(cellData -> cellData.getValue().getSalesModeProperty());
		clSgst.setCellValueFactory(cellData -> cellData.getValue().getSgstAmountProperty());
		clTaxableValue.setCellValueFactory(cellData -> cellData.getValue().getTaxableValueProperty());
		clTaxAmount.setCellValueFactory(cellData -> cellData.getValue().getTaxAmountProperty());
		clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clTotalAmount.setCellValueFactory(cellData -> cellData.getValue().getTotalAmountProperty());
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
