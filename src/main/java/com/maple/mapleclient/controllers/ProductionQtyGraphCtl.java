package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ProductionQtyGraphCtl {
	String taskid;
	String processInstanceId;
	  private ObservableList<String> curDate = FXCollections.observableArrayList();
	    private ObservableList<Double> values = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();
	XYChart.Series<String, Double> series;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtItemName;

    @FXML
    private ComboBox<String> cmbGraphType;

    @FXML
    private ListView<String> lstBranches;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnClear;

    @FXML
    private BarChart<String, Double> productionGraph;

    @FXML
    private CategoryAxis yAxis;

    @FXML
    private NumberAxis xAxis;

    @FXML
    void actionClear(ActionEvent event) {

    	txtItemName.clear();
    	lstBranches.getSelectionModel().clearSelection();
    	cmbGraphType.getSelectionModel().clearSelection();
    	curDate.clear();
   	 	values.clear();
   	 	productionGraph.getData().clear();
    }
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	eventBus.register(this);	
    	ResponseEntity<List<BranchMst>> getAllBranches = RestCaller.getBranchDtl();
    	for(BranchMst branchMst:getAllBranches.getBody())
    	{
    		lstBranches.getItems().add(branchMst.getBranchCode());
    	}
    	cmbGraphType.getItems().add("PRODUCTION PLANNING");
    	cmbGraphType.getItems().add("ACTUAL PRODUCTION");
    	
    }
    public void notifyMessage(int duration, String msg, boolean success) {
  		Image img;
  		if (success) {
  			img = new Image("done.png");

  		} else {
  			img = new Image("failed.png");
  		}

  		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
  				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
  				.onAction(new EventHandler<ActionEvent>() {
  					@Override
  					public void handle(ActionEvent event) {
  						System.out.println("clicked on notification");
  					}
  				});
  		notificationBuilder.darkStyle();
  		notificationBuilder.show();

  	}
    @FXML
    void actionShow(ActionEvent event) {
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5,"Select From Date",false);
    		dpFromDate.requestFocus();
    		return;
    	}
    	if(null == dpToDate.getValue())
    	{

    		notifyMessage(5,"Select To Date",false);
    		dpToDate.requestFocus();
    		return;
    	}
    	if(txtItemName.getText().trim().isEmpty())
    	{
    		notifyMessage(2,"Select ItemName", false);
    		txtItemName.requestFocus();
    		return;
    	}
    	if(cmbGraphType.getSelectionModel().isEmpty())
    	{
    		notifyMessage(2,"Select Graph Type",false);
    		return;
    	}
    	if(lstBranches.getSelectionModel().isEmpty())
    	{
    		notifyMessage(2,"Select Branch", false);
    		return;
    	}
    	
   	 series= new XYChart.Series<String, Double>();
 	LocalDate dpDate1 = dpFromDate.getValue();
 	java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
 	 String  strtDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
 	
 	 LocalDate dpDate2= dpToDate.getValue();
 		java.util.Date tDate  = SystemSetting.localToUtilDate(dpDate2);
 		 String  strfDate = SystemSetting.UtilDateToString(tDate, "yyyy-MM-dd");
 		 ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
 		if(cmbGraphType.getSelectionModel().getSelectedItem().equalsIgnoreCase("PRODUCTION PLANNING"))
 		{
 		 
 		 List<HashMap<String, Double>> map = new  ArrayList<HashMap<String,Double>>();
 		 map = RestCaller.getProductionPlanningQty(getItem.getBody().getId(),lstBranches.getSelectionModel().getSelectedItem(),strtDate,strfDate);
 		 System.out.println(map);
 		 Iterator itr = map.iterator();
 			while (itr.hasNext()) {
 				
 				HashMap<String, Double> mapValue = new HashMap<String, Double>();
 				mapValue = (HashMap<String, Double>) itr.next();
 			
 				for(Map.Entry<String, Double> entry : mapValue.entrySet())
 				{
 					curDate.add(entry.getKey());
 					values.add(entry.getValue());
 				}
 			}
 			series.setName("PLANNING QTY");
 		}
 		if(cmbGraphType.getSelectionModel().getSelectedItem().equalsIgnoreCase("ACTUAL PRODUCTION"))
 		{
 			 
 			 List<HashMap<String, Double>> map = new  ArrayList<HashMap<String,Double>>();
 			 map = RestCaller.getActulaProductionQty(getItem.getBody().getId(),lstBranches.getSelectionModel().getSelectedItem(),strtDate,strfDate);
 			 System.out.println(map);
 			 Iterator itr = map.iterator();
 				while (itr.hasNext()) {
 					
 					HashMap<String, Double> mapValue = new HashMap<String, Double>();
 					mapValue = (HashMap<String, Double>) itr.next();
 				
 					for(Map.Entry<String, Double> entry : mapValue.entrySet())
 					{
 						curDate.add(entry.getKey());
 						values.add(entry.getValue());
 					}
 				}
 				series.setName("ACTUAL QTY");
 		}	 
 		
 		createBarGraph(curDate,values);
    }
    public void createBarGraph(ObservableList<String> xaxis,ObservableList<Double> yaxis)
    {
    	
    	
    	for(int i = 0;i<xaxis.size();i++)
    	{
    	
    			 series.getData().add(new XYChart.Data<>(xaxis.get(i),yaxis.get(i)));
    		
    		
    	}
    	//series = new XYChart.Series<String, Double>();
    	productionGraph.getData().add(series);
//    	}
    	
    }
    private void showItemMst() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbGraphType.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    @FXML
    void loadPopUp(MouseEvent event) {

    	showItemMst();
    }
    @Subscribe
	public void popupItemlistner(KitPopupEvent kitPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnShow.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			// kitDefenitionmst = new KitDefinitionMst();
			txtItemName.setText(kitPopupEvent.getItemName());
		}
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
