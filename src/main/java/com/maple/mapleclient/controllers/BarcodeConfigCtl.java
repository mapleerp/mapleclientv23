package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BarcodeConfigurationMst;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CompanyMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import jdk.nashorn.internal.ir.CatchNode;

public class BarcodeConfigCtl {
	String taskid;
	String processInstanceId;




	EventBus eventBus = EventBusFactory.getEventBus();


	CompanyMst companyMst = null;
	
	BarcodeConfigurationMst  barcodeConfigurationMst = null;



    @FXML
    private TextField txtBarcodex;

    @FXML
    private Button btnsave;

    @FXML
    private Button showall;

    @FXML
    private TextField txtMrpX;
    @FXML
    private TextField txtMrpY;
    @FXML
    private Button btndelete;

    @FXML
    private ComboBox<String> cmbBranchCode;

    @FXML
    private Button btnInitalize;

    @FXML
    private TextField txtExpDateY;
    
    
    @FXML
    private TextField txtExpDateX;
    
    @FXML
    private TextField txtManufactureDateY;
    
    @FXML
    private TextField txtManufactureDateX;
    
    @FXML
    private TextField txtItemNameX;

    @FXML
    private TextField txtItemnameY;

    @FXML
    private TextField txtBarcodeY;
   
   
    @FXML
    private TextField txtInline1Y;

    @FXML
    private TextField txtInline2x;

    @FXML
    private TextField txtInline2Y;

    @FXML
    private TextField txtInline1X;

    @FXML
    private Button btnGetAll;

  
	@FXML
	private CheckBox chMyBranch;
	
	@FXML
	private Button btnsubmit;



	@FXML
	private TextField branchEmail;






	@FXML
	private void initialize() {
		btndelete.setVisible(false);
		try {
			ResponseEntity<BarcodeConfigurationMst>barcodeConfiguration = RestCaller.getBarcodeConfig();
			
		       barcodeConfigurationMst=barcodeConfiguration.getBody();
			if(null!=barcodeConfigurationMst) {
				txtBarcodex.setText(barcodeConfigurationMst.getBarcodeX());;
			    txtBarcodeY.setText(barcodeConfigurationMst.getBarcodeY());
			    
				txtExpDateX.setText(barcodeConfigurationMst.getExpiryDateX());;
				txtExpDateY.setText(barcodeConfigurationMst.getExpiryDateY());;
				txtInline1X.setText(barcodeConfigurationMst.getIngLine1X());
				txtInline1Y.setText(barcodeConfigurationMst.getIngLine1Y());
				txtInline2x.setText(barcodeConfigurationMst.getIngLine2X());;
				txtInline2Y.setText(barcodeConfigurationMst.getIngLine2Y());
				txtItemNameX.setText(barcodeConfigurationMst.getItemNameX());
				txtItemnameY.setText(barcodeConfigurationMst.getItemNameY());
				txtManufactureDateX.setText(barcodeConfigurationMst.getManufactureDateX());
				txtMrpX.setText(barcodeConfigurationMst.getMrpX());
				txtMrpY.setText(barcodeConfigurationMst.getMrpy());
				txtManufactureDateY.setText(barcodeConfigurationMst.getManufactureDateY());
			}
		
		
		}
		catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	@FXML
	void InitailizeData(ActionEvent event) {

		String msg = RestCaller.initailizeData(SystemSetting.getSystemBranch());
		notifyMessage(5, msg);
		
	}

	@FXML
	void showall(ActionEvent event) {

		showBarcodeConfiguration();
	}

	private void showAllBranches() {
	//	branchCreationList.clear();
		ResponseEntity<List<BarcodeConfigurationMst>> respentitybarcodeconfig = RestCaller.returnValueOfBarcodeConfigmst();

	}

	@FXML
	void branchNameOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtItemNameX.requestFocus();
		}
	}

	@FXML
	void branchCodeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
//			branchcode.setText(branchcode.getText().toUpperCase());
	//		branchgst.requestFocus();
		}
	}

	@FXML
	void branchGstOnEnter(KeyEvent event) {
		/*
		 * if (event.getCode() == KeyCode.ENTER) { cmbState.requestFocus(); }
		 */
	}

	@FXML
	void branchStateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchEmail.requestFocus();
		}
	}

	@FXML
	void branchEmailOnEnter(KeyEvent event) {
		/*
		 * if (event.getCode() == KeyCode.ENTER) { branchPhoneNo.requestFocus(); }
		 */

	}

	@FXML
	void branchPhoneOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
	//		branchWbsite.requestFocus();
		}
	}

	@FXML
	void branchWebsiteOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		//	branchAddress1.requestFocus();
		}
	}

	@FXML
	void branchAddress1OnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		//	branchAddress2.requestFocus();
		}
	}

	@FXML
	void branchAddress2OnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		//	bankaccno.requestFocus();
		}
	}

private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			cmbBranchCode.getItems().add(branchMst.getBranchCode());
		
			
		}
		 
	}


	@FXML
	void branchAccountNoOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		//	bankname.requestFocus();
		}
	}

	@FXML
	void branchBanckNameOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		//	bankbranch.requestFocus();
		}
	}

	@FXML
	void branchBankBranchOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		//	ifsccode.requestFocus();
		}
	}

	@FXML
	void branchIfscOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			chMyBranch.requestFocus();
		}
	}

	@FXML
	void FocusOnSaveBtn(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnsave.requestFocus();
		}
	}

	@FXML
	void delete(ActionEvent event) {

	//	deleteBranch();

	}

	/*
	 * private void deleteBranch() {
	 * 
	 * if (null != branchcreation) { if (null != branchcreation.getId()) {
	 * 
	 * RestCaller.deleteBranchMst(branchcreation.getId()); notifyMessage(5,
	 * "Deleted!!!"); ResponseEntity<List<BarcodeConfigCtl>> respentitygroup =
	 * RestCaller.returnValueOfBranchmst(); barcodeConfigCreationList =
	 * FXCollections.observableArrayList(barcodeConfigList.getBody()); if (null !=
	 * barcodeConfigCreationList) {
	 * tblbarcodeconfig.setItems(barcodeConfigCreationList); }
	 * barcodeConfigCreationList.getItems().clear(); // filltable(); }
	 * 
	 * } }
	 */

	@FXML
	void finalsubmit(ActionEvent event) {

	}

	@FXML
	void SaveOnEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			saveBarCodeConFinguration();
		} else if (event.getCode() == KeyCode.RIGHT) {
			btndelete.requestFocus();
		}
	}

	@FXML
	void DeleteOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		//	deleteBranch();
		} else if (event.getCode() == KeyCode.RIGHT) {
			showall.requestFocus();
		} else if (event.getCode() == KeyCode.LEFT) {
			btnsave.requestFocus();
		}
	}


	public void showBarcodeConfiguration(){
		
		ResponseEntity<BarcodeConfigurationMst>barcodeConfiguration = RestCaller.getBarcodeConfig();
		
	       barcodeConfigurationMst=barcodeConfiguration.getBody();
		if(null!=barcodeConfigurationMst) {
			txtBarcodex.setText(barcodeConfigurationMst.getBarcodeX());;
		    txtBarcodeY.setText(barcodeConfigurationMst.getBarcodeY());
		    
			txtExpDateX.setText(barcodeConfigurationMst.getExpiryDateX());;
			txtExpDateY.setText(barcodeConfigurationMst.getExpiryDateY());;
			txtInline1X.setText(barcodeConfigurationMst.getIngLine1X());
			txtInline1Y.setText(barcodeConfigurationMst.getIngLine1Y());
			txtInline2x.setText(barcodeConfigurationMst.getIngLine2X());;
			txtInline2Y.setText(barcodeConfigurationMst.getIngLine2Y());
			txtItemNameX.setText(barcodeConfigurationMst.getItemNameX());
			txtItemnameY.setText(barcodeConfigurationMst.getItemNameY());
			txtManufactureDateX.setText(barcodeConfigurationMst.getManufactureDateX());
			txtMrpX.setText(barcodeConfigurationMst.getMrpX());
			txtMrpY.setText(barcodeConfigurationMst.getMrpy());
			txtManufactureDateY.setText(barcodeConfigurationMst.getManufactureDateY());
		}
		else {
			
			notifyMessage(5, "No Data to show!!!!!!");
		}
	
	}




	
	@FXML
	void save(ActionEvent event) {
		saveBarCodeConFinguration ();
	}

	@FXML
	void OnEnterShowAll(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showAllBranches();
		} else if (event.getCode() == KeyCode.RIGHT) {
		//	btnBranchAsCustomer.requestFocus();
		} else if (event.getCode() == KeyCode.LEFT) {
			showall.requestFocus();
		}
	}
	
	

	private void saveBarCodeConFinguration() {

		if (txtBarcodex.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter barcode x axis");
			return;
		} else if (txtBarcodeY.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter barcode y axis");
			return;
		} else if (txtExpDateX.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter exp date x");
			return;
		} else if (txtExpDateY.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter expdate y");
			return;
		} else if ( txtInline2x.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter ingredient 2 x axis");
			return;
		} else if (txtInline1X.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter ingredient line x");
			return;
		}  else if (txtInline1Y.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter branch address");
			return;
		} else if ( txtInline2Y.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter ingredient 2 Y axis");
			return;
		} else if (txtMrpX.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter MRP X axis");
			return;
		} else if ( txtMrpY.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter MRP Y axis");
			return;
		}else if (txtManufactureDateY.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Manufacture date y axis");
			return;
		} else if ( txtManufactureDateX.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter manufacture date x axis");
			return;
		}else if (txtItemNameX.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Item Name x axis");
			return;
		} else if ( txtItemnameY.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter itemname Y axis");
			return;
		}


	       
		
		
	try {
		
	
		
	
	
		if(null!=barcodeConfigurationMst) {
			
			barcodeConfigurationMst.setBranchCode(SystemSetting.getSystemBranch());
			barcodeConfigurationMst.setBarcodeX(txtBarcodex.getText());
			barcodeConfigurationMst.setBarcodeY(txtBarcodeY.getText());
			barcodeConfigurationMst.setExpiryDateX(txtExpDateX.getText());
			barcodeConfigurationMst.setExpiryDateY(txtExpDateY.getText());
			barcodeConfigurationMst.setIngLine1X(txtInline1X.getText());
			barcodeConfigurationMst.setIngLine1Y(txtInline1Y.getText());
			barcodeConfigurationMst.setIngLine2X(txtInline2x.getText());
			barcodeConfigurationMst.setIngLine2Y(txtInline2Y.getText());
			barcodeConfigurationMst.setItemNameX(txtItemNameX.getText());
			barcodeConfigurationMst.setItemNameY(txtItemnameY.getText());
			barcodeConfigurationMst.setManufactureDateX(txtManufactureDateX.getText());
			barcodeConfigurationMst.setManufactureDateY(txtManufactureDateY.getText());
			barcodeConfigurationMst.setMrpX(txtMrpX.getText());
			barcodeConfigurationMst.setMrpy(txtMrpY.getText());
			
			ResponseEntity<BarcodeConfigurationMst> respentity = RestCaller.updateBarcodeConfigurationMst(barcodeConfigurationMst);
			notifyMessage(5, "updated ");
				/*
				 * txtBarcodex.setText(""); txtBarcodeY.setText(""); txtExpDateX.setText("");
				 * txtInline1X.clear(); txtExpDateY.clear(); txtInline1Y.clear();
				 * txtInline2Y.setText(""); txtInline2x.setText(""); txtItemNameX.setText("");
				 * txtItemnameY.setText(""); txtManufactureDateX.setText("");
				 * txtManufactureDateY.setText(""); txtMrpX.setText(""); txtMrpY.setText("");
				 */
			
		}else {
		
		
	
		            barcodeConfigurationMst=new BarcodeConfigurationMst();

	
					barcodeConfigurationMst.setBranchCode(SystemSetting.getSystemBranch());
					barcodeConfigurationMst.setBarcodeX(txtBarcodex.getText());
					barcodeConfigurationMst.setBarcodeY(txtBarcodeY.getText());
					barcodeConfigurationMst.setExpiryDateX(txtExpDateX.getText());
					barcodeConfigurationMst.setExpiryDateY(txtExpDateY.getText());
					barcodeConfigurationMst.setIngLine1X(txtInline1X.getText());
					barcodeConfigurationMst.setIngLine1Y(txtInline1Y.getText());
					barcodeConfigurationMst.setIngLine2X(txtInline2x.getText());
					barcodeConfigurationMst.setIngLine2Y(txtInline2Y.getText());
					barcodeConfigurationMst.setItemNameX(txtItemNameX.getText());
					barcodeConfigurationMst.setItemNameY(txtItemnameY.getText());
					barcodeConfigurationMst.setManufactureDateX(txtManufactureDateX.getText());
					barcodeConfigurationMst.setManufactureDateY(txtManufactureDateY.getText());
					barcodeConfigurationMst.setMrpX(txtMrpX.getText());
					barcodeConfigurationMst.setMrpy(txtMrpY.getText());
					
					ResponseEntity<BarcodeConfigurationMst> respentity = RestCaller.saveBarcodeConfigurationMst(barcodeConfigurationMst);
					barcodeConfigurationMst = respentity.getBody();
					notifyMessage(5, "saved");
				/*
				 * txtBarcodex.setText(""); txtBarcodeY.setText(""); txtExpDateX.setText("");
				 * txtInline1X.clear(); txtExpDateY.clear(); txtInline1Y.clear();
				 * txtInline2Y.setText(""); txtInline2x.setText(""); txtItemNameX.setText("");
				 * txtItemnameY.setText(""); txtManufactureDateX.setText("");
				 * txtManufactureDateY.setText(""); txtMrpX.setText(""); txtMrpY.setText("");
				 */
	
			//version4.3 
		}
	}
	catch (Exception e) {
		System.out.print(e.getMessage()+"exception isssssssss");
	}
	
	}
		
		
	    //version4.3 end
//	}

	
	




	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	
    @FXML
    void GetAllBranches(ActionEvent event) {
    	String msg = RestCaller.getallMastrs("BranchMst");
    			

    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


      private void PageReload() {
      	
}

}
  
