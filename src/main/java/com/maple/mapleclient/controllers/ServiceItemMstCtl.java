package com.maple.mapleclient.controllers;

import java.sql.Date;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BrandMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ProductMst;
import com.maple.mapleclient.entity.ServiceItemMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class ServiceItemMstCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<CategoryMst> categoryList = FXCollections.observableArrayList();
	private ObservableList<ItemMst> serviceItemMstList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	

	

	ItemMst serviceItemMst = null;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtMrp;

	@FXML
	private TextField txtTax;

	@FXML
	private TextField txtCess;

	@FXML
	private ComboBox<String> cmbCategory;

	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnClear;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowAll;

	@FXML
	private TableView<ItemMst> tblServiceItem;

	@FXML
	private TableColumn<ItemMst, String> clItem;

	@FXML
	private TableColumn<ItemMst, Number> clMrp;

	@FXML
	private TableColumn<ItemMst, Number> clTaxRate;

	@FXML
	private TableColumn<ItemMst, Number> clCessRate;

	@FXML
	private TableColumn<ItemMst, String> clCategory;

	@FXML
	private TableColumn<ItemMst, String> clStatus;
	
    @FXML
    private TextField txtCostPrice;

	@FXML
	private void initialize() {
		
	

		txtItemName.textProperty().bindBidirectional(SearchString);
		// set category
		ArrayList cat = new ArrayList();
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				cmbCategory.getItems().add((String) categoryName);
				CategoryMst categoryMst = new CategoryMst();
				categoryMst.setCategoryName((String) categoryName);
				categoryMst.setId((String) id);
				categoryList.add(categoryMst);
			}
		}

		cmbUnit.getSelectionModel().select("NOS");

		txtCess.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCess.setText(oldValue);
				}
			}
		});

		txtMrp.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCess.setText(oldValue);
				}
			}
		});

		txtTax.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtTax.setText(oldValue);
				}
			}
		});

		tblServiceItem.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId() && newSelection.getItemName().length() > 0) {
					if (null != newSelection.getId()) {

						ResponseEntity<ItemMst> serviceItemResp = RestCaller.getitemMst(newSelection.getId());
						serviceItemMst = new ItemMst();
						serviceItemMst = serviceItemResp.getBody();

						txtItemName.setText(serviceItemMst.getItemName());
						txtCess.setText(Double.toString(serviceItemMst.getCess()));
						txtMrp.setText(Double.toString(serviceItemMst.getStandardPrice()));
						txtTax.setText(Double.toString(serviceItemMst.getTaxRate()));

						ResponseEntity<CategoryMst> categoryMstResp = RestCaller
								.getCategoryById(serviceItemMst.getCategoryId());
						CategoryMst categoryMst = categoryMstResp.getBody();

						cmbCategory.getSelectionModel().select(categoryMst.getCategoryName());

					}

				}

			}

		});

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				System.out.println("Supplier Search changed");
				LoadItemBySearch((String) newValue);
			}
		});
	}

	private void LoadItemBySearch(String searchData) {
		tblServiceItem.getItems().clear();
		serviceItemMstList.clear();
		ArrayList item = new ArrayList();

		item = RestCaller.SearchItemByNameLimited(searchData);
		Iterator itr = item.iterator();

		Object id = null;

		while (itr.hasNext()) {
			java.util.LinkedHashMap element = (LinkedHashMap) itr.next();

			Object itemName = element.get("itemName");
			Object standardPrice = element.get("standardPrice");
			Object taxRate = element.get("taxRate");
			Object unitId = element.get("unitId");
			Object categoryId = element.get("categoryId");
			Object isDeleted = element.get("isDeleted");
			Object cessRate = element.get("cess");
			Object serviceOrGoods = element.get("serviceOrGoods");

			id = element.get("id");
			String item_type = (String) serviceOrGoods;

			if (null != serviceOrGoods) {
				if(item_type.equalsIgnoreCase("SERVICE ITEM"))
				{
				ItemMst itemMst = new ItemMst();
				itemMst.setItemName((String) itemName);
				itemMst.setStandardPrice((Double) standardPrice);
				itemMst.setTaxRate((Double) taxRate);
				itemMst.setUnitId((String) unitId);
				itemMst.setCategoryId((String) categoryId);
				itemMst.setId((String) id);
				itemMst.setCess((Double) cessRate);
				itemMst.setIsDeleted((String) isDeleted);
				serviceItemMstList.add(itemMst);
				}

			}

		}
		tblServiceItem.setItems(serviceItemMstList);
		fillTable();

	}

	@FXML
	void CategoryOnPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			cmbUnit.requestFocus();
		}

	}

	@FXML
	void CessOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbCategory.requestFocus();
		}
	}

	@FXML
	void Clear(ActionEvent event) {

		showAll();

		txtCess.clear();
		txtItemName.clear();
		txtMrp.clear();
		txtTax.clear();
		cmbCategory.getSelectionModel().clearSelection();

	}

	@FXML
	void Delete(ActionEvent event) {

		if (null != serviceItemMst) {
			if (null != serviceItemMst.getId()) {
				serviceItemMst.setIsDeleted("YES");
				RestCaller.updateItemMst(serviceItemMst);

				showAll();

				txtCess.clear();
				txtItemName.clear();
				txtMrp.clear();
				txtTax.clear();
				cmbCategory.getSelectionModel().clearSelection();
			}

		}

	}

	@FXML
	void ItemNameOnPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtMrp.requestFocus();
		}

	}

	@FXML
	void MrpOnPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtTax.requestFocus();
		}
	}

	@FXML
	void UnitOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCostPrice.requestFocus();
		}
	}
	

    @FXML
    void CostPriceOnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			saveItem();
		}
    }

	@FXML
	void Save(ActionEvent event) {

		saveItem();
	}

	private void saveItem() {

		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Item Name!!!", false);
			txtItemName.requestFocus();
			return;
		}
		if (txtMrp.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Standard Price!!!", false);
			txtMrp.requestFocus();
			return;
		}
		if (txtTax.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter TaxRate!!!", false);
			txtTax.requestFocus();
			return;
		}
		if (txtCess.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter CessRate!!!", false);
			txtCess.requestFocus();
			return;
		}
		if (null == cmbCategory.getValue()) {
			notifyMessage(5, "Please select Category!!!", false);
			cmbCategory.requestFocus();
			return;
		}

		serviceItemMst = new ItemMst();
		serviceItemMst.setItemName(txtItemName.getText());
		serviceItemMst.setStandardPrice(Double.parseDouble(txtMrp.getText()));
		serviceItemMst.setTaxRate(Double.parseDouble(txtTax.getText()));
		serviceItemMst.setServiceOrGoods("SERVICE ITEM");
		serviceItemMst.setCess(Double.parseDouble(txtCess.getText()));
		serviceItemMst.setIsDeleted("NO");
		serviceItemMst.setBestBefore(0);
		serviceItemMst.setRank(0);
		serviceItemMst.setBranchCode(SystemSetting.systemBranch);
		
		serviceItemMst.setRank(0);
		

		String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "SI");
		vNo = vNo + SystemSetting.getUser().getCompanyMst().getId();
		serviceItemMst.setId(vNo);

		ResponseEntity<CategoryMst> categoryMstResp = RestCaller
				.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem().toString());
		CategoryMst categoryMst = categoryMstResp.getBody();

		if (null == categoryMst) {
			notifyMessage(5, "Category not available!!!", false);
			return;
		}
		serviceItemMst.setCategoryId(categoryMst.getId());

		ResponseEntity<UnitMst> unitResp = RestCaller.getUnitByName("NOS");
		UnitMst unitMSt = unitResp.getBody();

		if (null == unitMSt) {
			return;
		}
		serviceItemMst.setUnitId(unitMSt.getId());
		ResponseEntity<ItemMst> savedItem = RestCaller.saveItemmst(serviceItemMst);
		serviceItemMst = savedItem.getBody();
		
		if(null != serviceItemMst)
		{
			serviceItemMstList.add(serviceItemMst);
			fillTable();

			if(!txtCostPrice.getText().trim().isEmpty())
			{
				ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
						.getPriceDefenitionMstByName("COST PRICE");
				PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
				if (null != priceDefenitionMst) {
					
						PriceDefinition price = new PriceDefinition();
						price.setItemId(serviceItemMst.getId());
						price.setUnitId(serviceItemMst.getUnitId());
						price.setPriceId(priceDefenitionMst.getId());
						price.setAmount(Double.parseDouble(txtCostPrice.getText()));
						LocalDate lDate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
						price.setStartDate(Date.valueOf(lDate));
						price.setBranchCode(SystemSetting.systemBranch);
						
						ResponseEntity<PriceDefinition> respentityPrice = RestCaller.savePriceDefenition(price);

				}
			}
		}

		
		txtCess.clear();
		txtItemName.clear();
		txtMrp.clear();
		txtTax.clear();
		cmbCategory.getSelectionModel().clearSelection();
		txtCostPrice.clear();

	}

	private void fillTable() {

		for (ItemMst item : serviceItemMstList) {
			ResponseEntity<CategoryMst> categoryResp = RestCaller.getCategoryById(item.getCategoryId());
			CategoryMst categoryMst = categoryResp.getBody();
			if (null != categoryMst) {
				item.setCategoryName(categoryMst.getCategoryName());
			}
			if (item.getIsDeleted().equalsIgnoreCase("NO")) {
				item.setDeletedStatus("ACTIVE");
			} else {
				item.setDeletedStatus("DELETED");
			}
		}
		tblServiceItem.setItems(serviceItemMstList);

		clCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
		clCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessProperty());

		clItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

		clMrp.setCellValueFactory(cellData -> cellData.getValue().getStandardPriceProperty());

		clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clStatus.setCellValueFactory(cellData -> cellData.getValue().getDeletedStatusProperty());

	}

	@FXML
	void ShowAll(ActionEvent event) {

		showAll();
	}

	private void showAll() {
		ResponseEntity<List<ItemMst>> serviceItemList = RestCaller.getAllSeriviceItems();
		serviceItemMstList = FXCollections.observableArrayList(serviceItemList.getBody());
		fillTable();
	}

	@FXML
	void TaxOnPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtCess.requestFocus();
		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {
		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload(hdrId);
			}


		private void PageReload(String hdrId) {

		}


}
