package com.maple.mapleclient.controllers;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.GSTInputDtlAndSmryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class GSTInputSummaryReportCtl {
	
	
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");
	 @FXML
	    private TableView<GSTInputDtlAndSmryReport> tblSummary;
	@FXML
    private TableColumn<GSTInputDtlAndSmryReport, String> clmnBranch;

    @FXML
    private TableColumn<GSTInputDtlAndSmryReport, String> clmnGSTPercent;

    @FXML
    private TableColumn<GSTInputDtlAndSmryReport, Number> clmnTotalExcludingGST;

    @FXML
    private TableColumn<GSTInputDtlAndSmryReport, Number> clmnGSTAmount;

    @FXML
    private TableColumn<GSTInputDtlAndSmryReport, Number> clmnTotalPurchase;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnPrintReport;
    
private ObservableList<GSTInputDtlAndSmryReport> gstInputDtlAndSmryReportList = FXCollections.observableArrayList();
    
    EventBus eventBus = EventBusFactory.getEventBus();
    
    @FXML
	private void initialize() {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	eventBus.register(this);
  	
    }

    @FXML
    void generateReport(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
			return;
    	}
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, "Please select to date", false);
    		return;
    	}
    	
    		
    		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
    		
    		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
    		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
    		
    		ResponseEntity<List<GSTInputDtlAndSmryReport>> gstInputSummary=RestCaller.getGstInputDetails(sfdate,stdate);
    		gstInputDtlAndSmryReportList = FXCollections.observableArrayList(gstInputSummary.getBody());
    		
    		
    		
    		if (gstInputDtlAndSmryReportList.size() > 0) {
    			
    			
    			filltableOfGstInputSummary();
    			dpFromDate.getEditor().clear();
    			dpToDate.getEditor().clear();
    			
    		}

    }

    private void filltableOfGstInputSummary() {
    	tblSummary.setItems(gstInputDtlAndSmryReportList);
		clmnBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchProperty());
		clmnGSTPercent.setCellValueFactory(cellData -> cellData.getValue().getGstPercentProperty());
		clmnTotalExcludingGST.setCellValueFactory(cellData -> cellData.getValue().getTotalPurchaseExcludingGstProperty());
		clmnTotalExcludingGST.setCellFactory(tc-> new TableCell<GSTInputDtlAndSmryReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		clmnGSTAmount.setCellValueFactory(cellData -> cellData.getValue().getGstAmountProperty());
		clmnGSTAmount.setCellFactory(tc-> new TableCell<GSTInputDtlAndSmryReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		clmnTotalPurchase.setCellValueFactory(cellData -> cellData.getValue().getTotalPurchaseProperty());
		clmnTotalPurchase.setCellFactory(tc-> new TableCell<GSTInputDtlAndSmryReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		
		

		
	}

	private void notifyMessage(int i, String string, boolean b) {
    	Image img;
		if (b) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
		
	}

	@FXML
    void printReport(ActionEvent event) {
		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		


		  try { JasperPdfReportService.GstInputSummaryReport(sfdate,
				  stdate); } catch (JRException e) { // TODO Auto-gesnerated catch block
		  e.printStackTrace(); }
    }
}
