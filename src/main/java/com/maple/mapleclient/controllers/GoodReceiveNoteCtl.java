package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AdditionalExpense;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.GoodReceiveNoteDtl;
import com.maple.mapleclient.entity.GoodReceiveNoteHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPropertyConfig;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.OtherBranchSalesDtl;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.GoodReceiveNoteDtl;
import com.maple.mapleclient.entity.GoodReceiveNoteHdr;
import com.maple.mapleclient.entity.PurchaseOrderDtl;
import com.maple.mapleclient.entity.PurchaseOrderHdr;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.ItemPropertyInstance;
import com.maple.mapleclient.events.PoNumberEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class GoodReceiveNoteCtl {

	
	
	private static final Logger logger = LoggerFactory.getLogger(GoodReceiveNoteCtl.class);
	private ObservableList<GoodReceiveNoteHdr> purchaseHdrListTable = FXCollections.observableArrayList();
	private ObservableList<GoodReceiveNoteHdr> GoodReceiveNoteHdrList = FXCollections.observableArrayList();
	private ObservableList<GoodReceiveNoteDtl> goodReceiveNoteDtlList = FXCollections.observableArrayList();
	private ObservableList<GoodReceiveNoteDtl> goodReceiveNoteListTable = FXCollections.observableArrayList();
	private ObservableList<PurchaseOrderDtl> purchaseOrderListTable = FXCollections.observableArrayList();
	private ObservableList<GoodReceiveNoteDtl> goodReceiveNoteListTable1 = FXCollections.observableArrayList();
	private ObservableList<GoodReceiveNoteDtl> purchaseDtlList = FXCollections.observableArrayList();
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();

	ItemPopupCtl itemPopupCtl = new ItemPopupCtl();
	GoodReceiveNoteHdr goodReceiveNoteHdr = null;
	PurchaseOrderDtl purchaseOrderDtl = null;
	PurchaseOrderHdr purchaseOrderHdr = null;
	GoodReceiveNoteDtl goodReceiveNoteDtl;
	String supplierID = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	String taskid;
	String processInstanceId;

	double grandTotal = 0;
	double expenseTotal = 0;
	String supplierName = "";
	String itemId = null;
	String sdate = null;

	/*
	 * To manage Item Serial
	 */
	StringProperty itemSerialProperty = new SimpleStringProperty("");
	StringProperty cessAmtProperty = new SimpleStringProperty("");

	/*
	 * To Enable disable Expiry date / Manu Date based on Batch
	 */
	StringProperty batchProperty = new SimpleStringProperty("");
	StringProperty taxAmountProperty = new SimpleStringProperty("");

	/*
	 * To Manage Total Amount Property
	 */

	StringProperty totalAmountProperty = new SimpleStringProperty("");

	@FXML
	private TextField txtSupplierName;

	@FXML
	private TextField txtSupplierGst;

	@FXML
	private DatePicker dpSupplierInvDate;

	@FXML
	private TextField txtSupplierInvNo;

	@FXML
	private TextField txtPONum;

	@FXML
	private Button btnLoadFromPO;

	@FXML
	private TextField txtQtyTotal;

	@FXML
	private Button btnHolPurchase;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnAddItem;

	@FXML
	private Button btnDeleteItem;

	@FXML
	private DatePicker dpExpiryDate;

	@FXML
	private Label lblExpryDate;

	@FXML
	private DatePicker dpManufacture;

	@FXML
	private Label lblManufDate;

	@FXML
	private TextField txtBatch;

	@FXML
	private TextField txtMRP;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtItemSerial;

	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private ComboBox<String> cmbStore;
	
	@FXML
    private ComboBox<String> cmbBranch;
	@FXML
	private Button btnClear;
	@FXML
	private TableView<GoodReceiveNoteDtl> tblItemDetails;

	@FXML
	private TableColumn<GoodReceiveNoteDtl, String> clItemName;

	@FXML
	private TableColumn<GoodReceiveNoteDtl, Number> clQty;

	@FXML
	private TableColumn<GoodReceiveNoteDtl, String> clBatch;

    @FXML
    private TableColumn<GoodReceiveNoteDtl, String> clstore;
    
	@FXML
	private void initialize() {

		dpExpiryDate = SystemSetting.datePickerFormat(dpExpiryDate, "dd/MMM/yyyy");
		dpManufacture = SystemSetting.datePickerFormat(dpManufacture, "dd/MMM/yyyy");
		dpSupplierInvDate = SystemSetting.datePickerFormat(dpSupplierInvDate, "dd/MMM/yyyy");

		logger.info("================== INITIALIZATION STARTED");
		setCmbBranchCode();
		txtSupplierName.requestFocus();

		// -----new GoodReceiveNoteDtl----------
		goodReceiveNoteDtl = new GoodReceiveNoteDtl();

		/*
		 * Bind below three fields to Property so that calculation of Amout or Rate will
		 * happen as the user types in
		 */
		// txtQty.textProperty().bindBidirectional(quantityProperty);
		txtItemSerial.textProperty().bindBidirectional(itemSerialProperty);
		itemSerialProperty.set("1");
		txtBatch.textProperty().bindBidirectional(batchProperty);

		eventBus.register(this);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtMRP.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtMRP.setText(oldValue);
				}
			}
		});

		txtItemSerial.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtItemSerial.setText(oldValue);
				}
			}
		});

		/*
		 * Listen the property change and calculate corresponding values and set to the
		 * text box
		 */

		batchProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0) {
					dpExpiryDate.setDisable(true);
					dpManufacture.setDisable(true);
					lblExpryDate.setDisable(true);
					lblManufDate.setDisable(true);

				} else {
					dpExpiryDate.setDisable(false);
					dpManufacture.setDisable(false);
					lblExpryDate.setDisable(false);
					lblManufDate.setDisable(false);
				}

			}
		});

		// ==========table selection
		tblItemDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					goodReceiveNoteDtl = new GoodReceiveNoteDtl();
					txtItemName.setText(newSelection.getItemName());
					ResponseEntity<ItemMst> getItems = RestCaller.getItemByNameRequestParam(txtItemName.getText());

					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItems.getBody().getUnitId());
					txtBarcode.setText(getItems.getBody().getBarCode());
					cmbUnit.getSelectionModel().select(getUnit.getBody().getUnitName());
					cmbUnit.setValue(getUnit.getBody().getUnitName());
					txtQty.setText(Double.toString(newSelection.getQty()));
					txtMRP.setText(Double.toString(getItems.getBody().getStandardPrice()));

					System.out.println("Batchhhhhhhhhhhhhhhhhhhhhhhhhhh" + newSelection.getBatch());
					txtBatch.setText(newSelection.getBatch());
					dpExpiryDate.setValue(newSelection.getexpiryDate().toLocalDate());
					System.out.println("getSelectionModel--getId");

					System.out.println("getSelectionModel--getId----------------" + newSelection.getId());

					goodReceiveNoteDtl.setId(newSelection.getId());

					goodReceiveNoteDtl.setPurchaseOrderDtl(newSelection.getPurchaseOrderDtl());

					System.out.println("DELETE--" + goodReceiveNoteDtl.getId());

				}
			}
		});

		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null!=newValue) {

					ResponseEntity<UnitMst> initResp = RestCaller.getUnitByName(newValue);
					UnitMst unitMst = initResp.getBody();

					if (null == unitMst || null == itemId) {
						return;
					}

					ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
							.getPriceDefenitionMstByName("COST PRICE");
					PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
					if (null != priceDefenitionMst) {

						ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
								.getPriceDefinitionByItemIdAndPriceId(itemId, priceDefenitionMst.getId(),
										unitMst.getId());

						PriceDefinition priceDefinition = priceDefenitionResp.getBody();

						if (null != priceDefinition) {
						}

					}
				}

			}

		});

		ResponseEntity<List<StoreMst>> storeMstListResp = RestCaller.getAllStoreMst();
		List<StoreMst> storeMstList = storeMstListResp.getBody();

		cmbStore.getSelectionModel().select("MAIN");

		for (StoreMst storeMst : storeMstList) {
			cmbStore.getItems().add(storeMst.getShortCode());
		}

		logger.info("goodReceiveNote =============== INITIALIZATION COMPLETED");

	}

	@FXML
	void AddItem(ActionEvent event) {
		addItem();
	}
	private void setCmbBranchCode() {

		ResponseEntity<List<BranchMst>> branchListResp = RestCaller.getBranchMst();
		List<BranchMst> branchList = branchListResp.getBody();
		
		for(BranchMst branch
				 : branchList)
		{
			//cmbBranch.setValue(branch.getBranchName());
			cmbBranch.getItems().add(branch.getBranchName());
		}
	}
	@FXML
	void actionClear(ActionEvent event) {
		clearFieldshdr();
		clearFields();
		tblItemDetails.getItems().clear();
		goodReceiveNoteHdr=null;
		goodReceiveNoteDtl=null;
	}

	private void addItem() {
		ResponseEntity<List<ItemPropertyInstance>> propertyinstanceList = null;
		if (null == goodReceiveNoteHdr) {

			if (null == supplierID)

			{
				notifyMessage(5, "You must select Supplier", false);
				goodReceiveNoteHdr = null;
				return;
			}

			if (null == dpSupplierInvDate.getValue()) {

				notifyMessage(5, "Please select Suppier Invoice date", false);
				goodReceiveNoteHdr = null;
				return;

			}

			if (txtSupplierInvNo.getText().trim().isEmpty()) {
				notifyMessage(5, "Please enter Suppier Invoice Number", false);
				goodReceiveNoteHdr = null;
				return;
			}

			goodReceiveNoteHdr = new GoodReceiveNoteHdr();

			goodReceiveNoteHdr.setSupplierId(supplierID);
			System.out.println("ddddd" + supplierID);

			goodReceiveNoteHdr.setFinalSavedStatus("N");
			goodReceiveNoteHdr.setpONum(txtPONum.getText());

			goodReceiveNoteHdr.setSupplierInvNo(txtSupplierInvNo.getText());

			goodReceiveNoteHdr.setInvoiceDate(Date.valueOf(dpSupplierInvDate.getValue()));
			goodReceiveNoteHdr.setToBranch(cmbBranch.getSelectionModel().getSelectedItem());
			goodReceiveNoteHdr.setBranchCode(SystemSetting.getSystemBranch());
			

			goodReceiveNoteHdr.setVoucherType("PURCHASETYPE");

			goodReceiveNoteHdr.setPurchaseType("Local Purchase");

			logger.info("=====goodReceiveNoteHdr======" + goodReceiveNoteHdr);
			ResponseEntity<GoodReceiveNoteHdr> respentity = RestCaller.saveGoodReceiveNoteHdr(goodReceiveNoteHdr);
			goodReceiveNoteHdr = respentity.getBody();

		}

		/*
		 * Set GoodReceiveNoteHdr to GoodReceiveNoteDtl.
		 */
		if (null != goodReceiveNoteDtl) {
			if (null != goodReceiveNoteDtl.getId()) {

				ResponseEntity<GoodReceiveNoteDtl> getPur = RestCaller
						.getGoodReceiveNoteDtlById(goodReceiveNoteDtl.getId());
				if (null != getPur.getBody().getPurchaseOrderDtl()) {
					if (!getPur.getBody().getPurchaseOrderDtl().equalsIgnoreCase(null))

					{
						PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
						ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller
								.getPurchaseOrderDtlById(getPur.getBody().getPurchaseOrderDtl());
						puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
						puchaseOrderDtl.setReceivedQty(
								Double.parseDouble(txtQty.getText()) + puchaseOrderDtl.getReceivedQty());
						if (puchaseOrderDtl.getQty() > puchaseOrderDtl.getReceivedQty()) {
							puchaseOrderDtl.setStatus("OPEN");
						} else {
							puchaseOrderDtl.setStatus("CLOSED");
						}
						RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
						ResponseEntity<PurchaseOrderHdr> getPurOrdrHdr = RestCaller
								.getPurchaseOrderByDtlId(puchaseOrderDtl.getId());
						String vdate = SystemSetting.UtilDateToString(getPurOrdrHdr.getBody().getVoucherDate(),
								"yyyy-MM-dd");
						ResponseEntity<List<PurchaseOrderDtl>> getGoodReceiveNoteDtl = RestCaller
								.getPurchaseOrderDtlByVoucherNoAndDate(getPurOrdrHdr.getBody().getVoucherNumber(),
										vdate);
						getPurOrdrHdr.getBody().setFinalSavedStatus("OPEN");
						RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());
					}
				}
				RestCaller.goodReceiveNoteDtlDelete(goodReceiveNoteDtl.getId());
				goodReceiveNoteDtl = null;
				getGoodReceiveNoteDtls();
			}
		}
		if (null != goodReceiveNoteHdr) {
			/*
			 * Check if the item has property configuration
			 */

			goodReceiveNoteDtl = new GoodReceiveNoteDtl();

			logger.info("ADD ITEM STATRED=========");
			if (txtItemName.getText().trim().isEmpty()) {
				notifyMessage(5, "Please select Item...!!!", false);
				return;
			}
			String ItemName = txtItemName.getText();
			ResponseEntity<ItemMst> resp = RestCaller.getItemByNameRequestParam(ItemName);
			ItemMst item = resp.getBody();

			if (null == item) {
				notifyMessage(5, "Please select Item...!!!", false);
				return;

			}
			System.out.println("-------------AddItem-------------");
			boolean valDtl = validationDtl();
			if (valDtl == false) {
				return;
			}

			boolean expiry = true;

			if (null != txtBatch) {
				String batch = txtBatch.getText() == null ? "NOBATCH" : txtBatch.getText();

				if (batch.trim().length() > 0) {
					goodReceiveNoteDtl.setBatch(batch);
					expiry = false;
					goodReceiveNoteDtl.setBatch(txtBatch.getText());
					if (null != dpExpiryDate.getValue()) {
						goodReceiveNoteDtl.setexpiryDate(Date.valueOf(dpExpiryDate.getValue()));
						expiry = true;

					} else {
						notifyMessage(5, "Please select Expirydate...!!!");
						return;
					}

					if (null != dpManufacture.getValue()) {
						Date dateMfg = Date.valueOf(dpManufacture.getValue());

						goodReceiveNoteDtl.setmanufactureDate(dateMfg);
					}
				} else {
					goodReceiveNoteDtl.setBatch("NOBATCH");
					if (goodReceiveNoteDtl.getBatch().equalsIgnoreCase("NOBATCH")) {
						/*
						 * Automatically Generate batch if item has property
						 */
						String autoBatch = RestCaller.generateBatch(item.getId());
						goodReceiveNoteDtl.setBatch(autoBatch);
					}
					goodReceiveNoteDtl.setexpiryDate(null);
				}
			}

			if (null != goodReceiveNoteHdr.getId()) {
				if (valDtl == true) {

					if (expiry == true) {

						goodReceiveNoteDtl.setGoodReceiveNoteHdr(goodReceiveNoteHdr);
						System.out.println("getItemIdgetItemId" + goodReceiveNoteDtl.getItemId());

						goodReceiveNoteDtl.setItemName(item.getItemName());
						goodReceiveNoteDtl.setBarcode(item.getBarCode());

						goodReceiveNoteDtl.setItemId(item.getId());
						

						goodReceiveNoteDtl.setMrp(Double.parseDouble(txtMRP.getText()));
						goodReceiveNoteDtl.setQty(Double.parseDouble(txtQty.getText()));
						if (null == cmbUnit.getValue()) {

							ResponseEntity<UnitMst> unitId = RestCaller
									.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

							goodReceiveNoteDtl.setUnitId(unitId.getBody().getId());
						} else {
							ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
							goodReceiveNoteDtl.setUnitId(getUnit.getBody().getId());
						}
						goodReceiveNoteDtl.setItemSerial(Integer.parseInt(txtItemSerial.getText()));

						if (null != cmbStore.getSelectionModel().getSelectedItem().toString()) {
							goodReceiveNoteDtl.setStore(cmbStore.getSelectionModel().getSelectedItem().toString());
						} else {
							goodReceiveNoteDtl.setStore("MAIN");
						}
						ResponseEntity<GoodReceiveNoteDtl> respentity = RestCaller
								.saveGoodReceiveNoteDtl(goodReceiveNoteDtl);

						goodReceiveNoteDtl = respentity.getBody();
						setTotal();

						goodReceiveNoteListTable.add(goodReceiveNoteDtl);
						itemSerialProperty.set(Integer.parseInt(itemSerialProperty.get()) + 1 + "");

						/*
						 * Pop up for item properties
						 */
						ResponseEntity<List<ItemPropertyConfig>> itempropertylist = RestCaller
								.getItemPropertyConfigByItemId(goodReceiveNoteDtl.getItemId());
						if (itempropertylist.getBody().size() > 0) {
							/*
							 * Generate pop up for itemproperty
							 */
							loadItemPropertyPopup(goodReceiveNoteDtl, propertyinstanceList);
						}
						FillTable();
						clearFields();

					}
				}
			} else {

				return;

			}
		} else {
			return;
		}
		logger.info("ADD ITEM  COMPLETED=================");

		goodReceiveNoteDtl = new GoodReceiveNoteDtl();

		txtItemName.requestFocus();

	}

	private void setTotal() {
		Summary sumList = new Summary();
		
		sumList = RestCaller.getSummaryGoodReceiveNoteHDr(goodReceiveNoteHdr.getId());
		if (null != sumList) {
			txtQtyTotal.setText(Double.toString(sumList.getTotalQty()));
		}
	
	}

	private void clearFields() {
		txtItemName.setText("");
		txtQty.setText("");
		txtBarcode.setText("");
		txtBatch.setText("");
		txtMRP.setText("");
		cmbUnit.getSelectionModel().clearSelection();
		dpExpiryDate.setValue(null);
		dpManufacture.setValue(null);

	}

	private void FillTable() {

	
		tblItemDetails.setItems(goodReceiveNoteListTable);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clstore.setCellValueFactory(cellData -> cellData.getValue().getStoreProperty());
	}



	private boolean validationDtl() {

		boolean flag = false;
		if (txtMRP.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter MRP", false);
			return false;
		} else if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Quantity", false);
			return false;
		} else {

			flag = true;

		}

		return flag;

	}

	private void getGoodReceiveNoteDtls() {

		goodReceiveNoteListTable.clear();
		ArrayList pur = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		pur = RestCaller.SearchGoodReceiveNoteDtls(goodReceiveNoteHdr.getId());
		Iterator itr = pur.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object itemName = lm.get("itemName");
			Object qty = lm.get("qty");

			Object batch = lm.get("batch");
			Object expiryDate = lm.get("expiryDate");
			Object id = lm.get("id");
			if (id != null) {
				GoodReceiveNoteDtl goodReceiveNoteDtl = new GoodReceiveNoteDtl();
				goodReceiveNoteDtl.setItemName((String) itemName);
				goodReceiveNoteDtl.setQty((Double) qty);
				goodReceiveNoteDtl.setId((String) id);
				goodReceiveNoteDtl.setBatch((String) batch);
				if (null != expiryDate)
					goodReceiveNoteDtl.setexpiryDate(Date.valueOf((String) expiryDate));
				goodReceiveNoteListTable.add(goodReceiveNoteDtl);

			}

		}
		FillTable();

		

	}

	@FXML
	void DeleteItem(ActionEvent event) {
		
		
		
		
		  if (null != goodReceiveNoteDtl) { if (null != goodReceiveNoteDtl.getId()) {
		  
		  RestCaller.goodReceiveNoteDtlDelete(goodReceiveNoteDtl.getId());
		  /////////////////////
		  
		  ResponseEntity<List<GoodReceiveNoteDtl>> goodReceiveNotedtlSaved =
		  RestCaller.getGoodReceiveNoteDtld(goodReceiveNoteHdr);
		  goodReceiveNoteListTable
		  =FXCollections.observableArrayList(goodReceiveNotedtlSaved.getBody());
		  FillTable();
		  
		  clearFields(); 
		  setTotal();
		  
		  }
		  }
		 
		
		
		
		
		
		/*
		 * RestCaller.goodReceiveNoteDtlDelete(goodReceiveNoteDtl.getId());
		 * 
		 * if (null != goodReceiveNoteDtl) {
		 * 
		 * if (null != goodReceiveNoteDtl.getId()) {
		 * 
		 * 
		 * ResponseEntity<GoodReceiveNoteDtl> getPur =
		 * RestCaller.getGoodReceiveNoteDtlById(goodReceiveNoteDtl.getId());
		 * 
		 * 
		 * if (null !=getPur.getBody().getPurchaseOrderDtl()) {
		 * if(!getPur.getBody().getPurchaseOrderDtl().equalsIgnoreCase(null))
		 * 
		 * { PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
		 * ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl =
		 * RestCaller.getPurchaseOrderDtlById(getPur.getBody().getPurchaseOrderDtl());
		 * puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
		 * puchaseOrderDtl.setReceivedQty(0.0); puchaseOrderDtl.setStatus("OPEN");
		 * RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
		 * ResponseEntity<PurchaseOrderHdr> getPurOrdrHdr = RestCaller
		 * .getPurchaseOrderByDtlId(puchaseOrderDtl.getId()); String vdate =
		 * SystemSetting.UtilDateToString(getPurOrdrHdr.getBody().getVoucherDate(),
		 * "yyyy-MM-dd"); ResponseEntity<List<PurchaseOrderDtl>> getPurchaseDtl =
		 * RestCaller .getPurchaseOrderDtlByVoucherNoAndDate(getPurOrdrHdr.getBody().
		 * getVoucherNumber(), vdate);
		 * 
		 * getPurOrdrHdr.getBody().setFinalSavedStatus("OPEN");
		 * RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody()); } }
		 * 
		 * RestCaller.goodReceiveNoteDtlDelete(goodReceiveNoteDtl.getId());
		 * goodReceiveNoteDtl = null; ///////////////////// getGoodReceiveNoteDtls();
		 * 
		 * clearFields();
		 * }}
		 */
		  
		 }

	@FXML
	void ExpiryDateOnkeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnAddItem.requestFocus();
		}
	}

	

	private void clearFieldshdr() {
		clearFields();
		supplierID = null;
		txtSupplierGst.setText("");
		txtSupplierInvNo.setText("");
		txtSupplierName.setText("");
		dpSupplierInvDate.setValue(null);
		txtPONum.setText("");
		txtQtyTotal.setText("");

	}

	@FXML
	void ItemNameOnkeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtItemName.clear();
			txtBarcode.clear();
			txtQty.clear();
			txtMRP.clear();
			txtBatch.clear();
			showPopup();
			txtQty.requestFocus();
		}
	}

	private void showPopup() {
		try {
			logger.info("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void ManfDAteOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpExpiryDate.requestFocus();
		}
	}

	@FXML
	void PoNoOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtItemName.requestFocus();
			loadPOpopUp(supplierID);
		}
	}

	@FXML
	void PoNumClicked(MouseEvent event) {
		loadPOpopUp(supplierID);
	}

	private void loadPOpopUp(String supplierID) {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			logger.info("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PONumberPopUp.fxml"));
			Parent root = loader.load();
			POPopupCtl popupctl = loader.getController();
			popupctl.LoadPoNumberBySearch(supplierID);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();

			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void QtyOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtMRP.requestFocus();
		}
	}

	@FXML
	void Save(ActionEvent event) {
		finalSave();
	}

	private void finalSave() {

		

		try {

			ResponseEntity<List<GoodReceiveNoteDtl>> goodreceivedtlSaved = RestCaller.getGoodReceiveNoteDtl(goodReceiveNoteHdr);
			List<GoodReceiveNoteDtl> goodreceivedtlList = goodreceivedtlSaved.getBody();
			if (null == goodreceivedtlList || goodreceivedtlList.size() == 0) {
				return;
			}

			goodReceiveNoteHdr.setSupplierId(supplierID);

//			ResponseEntity<Supplier> supplier = RestCaller.getSupplier(goodReceiveNoteHdr.getSupplierId());
			String vouchernumber = goodReceiveNoteHdr.getVoucherNumber();
			goodReceiveNoteHdr.setFinalSavedStatus("OPEN");
			if (null == vouchernumber) {
				String financialYear = SystemSetting.getFinancialYear();
				String pNo = RestCaller.getVoucherNumber(financialYear + "PO");

				goodReceiveNoteHdr.setVoucherNumber(pNo);

			}
			java.util.Date uDate = goodReceiveNoteHdr.getInvoiceDate();
			String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

			RestCaller.updateGoodReceiveNoteHdr(goodReceiveNoteHdr);

			notifyMessage(5, "data has been saved successfully", true);

			String vNo = goodReceiveNoteHdr.getVoucherNumber();
			goodReceiveNoteHdr = null;
			goodReceiveNoteDtl = null;
			goodReceiveNoteListTable.clear();
			itemSerialProperty.set("1");
			tblItemDetails.getItems().clear();
			purchaseOrderHdr = null;

			clearFieldshdr();
			
		} catch (Exception e) {
			
			notifyMessage(5, "error! data not saved", true);
			
			e.printStackTrace();
		}

	}

	@FXML
	void SaveOnKey(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}
	}

	@FXML
	void ShowItemPopup(MouseEvent event) {
		showPopup();
	}

	@FXML
	void SupplierInvDateKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtSupplierGst.requestFocus();
		}
	}

	@FXML
	void SupplierInvNoOnKeyPresssed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtPONum.requestFocus();
		}
	}

	@FXML
	void SupplierOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtSupplierGst.requestFocus();
		}
	}

	@FXML
	void actionLoadPo(ActionEvent event) {
		PurchaseOrderDtlToGoodReceiveNoteDtl();
	}

	private void PurchaseOrderDtlToGoodReceiveNoteDtl() {

		goodReceiveNoteHdr = null;
		String poId = txtPONum.getText();// .toString();
		ResponseEntity<PurchaseOrderHdr> purchaseHdrSaved = RestCaller.getPurchaseOrderHdrByVoucherNo(poId);
		PurchaseOrderHdr purchaseOrderHdr = purchaseHdrSaved.getBody();

		// -----------get list of purchase order dtl by hdr
		ResponseEntity<List<PurchaseOrderDtl>> purchaseDtlSaved = RestCaller.getPurchaseOrderDtl(purchaseOrderHdr);
		List<PurchaseOrderDtl> purchaseOrderDtlList = purchaseDtlSaved.getBody();
		// ---- iterote dtls
		for (PurchaseOrderDtl purOrderDtl : purchaseOrderDtlList) {

			if (null == goodReceiveNoteHdr) {
				goodReceiveNoteHdr = new GoodReceiveNoteHdr();

				if (null == dpSupplierInvDate.getValue()) {

					notifyMessage(5, "Please select Suppier Invoice date", false);
					goodReceiveNoteHdr = null;
					return;

				}

				if (null == goodReceiveNoteHdr.getSupplierInvNo()) {
					notifyMessage(5, "Please enter Suppier Invoice Number", false);
					goodReceiveNoteHdr = null;
					return;
				}
				goodReceiveNoteHdr.setSupplierId(purchaseOrderHdr.getSupplierId());
				goodReceiveNoteHdr.setpONum(purchaseOrderHdr.getpONum());
				goodReceiveNoteHdr.setNarration(purchaseOrderHdr.getNarration());
				goodReceiveNoteHdr.setSupplierInvNo(purchaseOrderHdr.getSupplierInvNo());
				goodReceiveNoteHdr.setInvoiceDate(Date.valueOf(dpSupplierInvDate.getValue()));
				goodReceiveNoteHdr.setBranchCode(purchaseOrderHdr.getBranchCode());
				goodReceiveNoteHdr.setVoucherType(purchaseOrderHdr.getVoucherType());
				logger.info("=====goodReceiveNoteHdr======" + goodReceiveNoteHdr);
				ResponseEntity<GoodReceiveNoteHdr> respentity1 = RestCaller.saveGoodReceiveNoteHdr(goodReceiveNoteHdr);
				goodReceiveNoteHdr = respentity1.getBody();

			}
			goodReceiveNoteDtl = new GoodReceiveNoteDtl();

			goodReceiveNoteDtl.setBarcode(purOrderDtl.getBarcode());
			goodReceiveNoteDtl.setBinNo(purOrderDtl.getBinNo());
			goodReceiveNoteDtl.setItemId(purOrderDtl.getItemId());
			
			
			if (null != purOrderDtl.getItemSerial()) {
				goodReceiveNoteDtl.setItemSerial(Integer.parseInt(purOrderDtl.getItemSerial()));
			}
			
			goodReceiveNoteDtl.setUnitId(purOrderDtl.getUnitId());
			goodReceiveNoteDtl.setQty(purOrderDtl.getQty());
			goodReceiveNoteDtl.setGoodReceiveNoteHdr(goodReceiveNoteHdr);
			goodReceiveNoteDtl.setMrp(purOrderDtl.getMrp());
			
			
			if (null != purOrderDtl.getBatch() && !purOrderDtl.getBatch().trim().isEmpty()) {
				goodReceiveNoteDtl.setBatch(purOrderDtl.getBatch());
			}
			else {
				goodReceiveNoteDtl.setBatch("NOBATCH");
			}
			
			
			ResponseEntity<ItemMst> resItem = RestCaller.getitemMst(purOrderDtl.getItemId());
			goodReceiveNoteDtl.setItemName(resItem.getBody().getItemName());
			
			ResponseEntity<GoodReceiveNoteDtl> respentity2 = RestCaller.saveGoodReceiveNoteDtl(goodReceiveNoteDtl);
			goodReceiveNoteDtl = respentity2.getBody();
			
			goodReceiveNoteDtl.setItemName(resItem.getBody().getItemName());
			goodReceiveNoteListTable.add(goodReceiveNoteDtl);
			
			goodReceiveNoteDtl = null;
			FillTable();

			setTotal();
		}

	}

	@Subscribe
	public void popuplistner(SupplierPopupEvent supplierEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtSupplierGst.setText(supplierEvent.getSupplierGST());
					txtSupplierName.setText(supplierEvent.getSupplierName());
					supplierName = supplierEvent.getSupplierName();
					supplierID = supplierEvent.getSupplierId();
					/*
					 * for some reason id is null
					 */
					if (null == supplierID) {
						ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(supplierEvent.getSupplierName());
						supplierID = accountHeads.getBody().getId();
					}
				}
			});

		}

	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnLoadFromPO.getScene().getWindow();
		if (stage.isShowing()) {
			cmbUnit.getItems().clear();

			itemId = itemPopupEvent.getItemId();
			txtItemName.setText(itemPopupEvent.getItemName());
			txtBarcode.setText(itemPopupEvent.getBarCode());

			txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));
			cmbUnit.getItems().add(itemPopupEvent.getUnitName());
			cmbUnit.setValue(itemPopupEvent.getUnitName());
			ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller.getMultiUnitByItemId(itemPopupEvent.getItemId());

			multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());

			if (!multiUnitList.isEmpty()) {

				for (MultiUnitMst multiUniMst : multiUnitList) {

					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
					cmbUnit.getItems().add(getUnit.getBody().getUnitName());
				}

			} else {
				cmbUnit.setValue(itemPopupEvent.getUnitName());
			}

			ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
					.getPriceDefenitionMstByName("COST PRICE");
			PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
			if (null != priceDefenitionMst) {

				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						itemPopupEvent.getItemId(), priceDefenitionMst.getId(), itemPopupEvent.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();

				if (null != priceDefinition) {
				}

			}
		}
	}

	@FXML
	void addItem(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void batchActon(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtBatch.getText().trim().isEmpty()) {
				dpManufacture.requestFocus();
			} else
				btnAddItem.requestFocus();
		}
	}

	@FXML
	void showPopup(MouseEvent event) {

		System.out.println("-------------showPopup-------------");
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/supplierPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupPolistner(PoNumberEvent poNumberEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnLoadFromPO.getScene().getWindow();
		if (stage.isShowing()) {
			cmbUnit.getItems().clear();
			txtQtyTotal.clear();

			txtPONum.setText(poNumberEvent.getPoNumber());
			txtSupplierGst.setText(poNumberEvent.getSupGst());
			txtSupplierName.setText(poNumberEvent.getSupplierName());
			supplierName = poNumberEvent.getSupplierName();
			supplierID = poNumberEvent.getSupId();
			tblItemDetails.getItems().clear();
			purchaseDtlList.clear();
			ResponseEntity<PurchaseOrderHdr> getPurOrder = RestCaller
					.getPurchaseOrderHdrByVoucherNo(txtPONum.getText());
			purchaseOrderHdr = getPurOrder.getBody();
			ResponseEntity<List<PurchaseOrderDtl>> respentity = RestCaller
					.getPurchaseOrderDtlByVoucherNo(txtPONum.getText());

			if (null != respentity.getBody()) {

				int i = 1;
				for (PurchaseOrderDtl purOrderDtl : respentity.getBody()) {

					if (null == goodReceiveNoteHdr) {
						goodReceiveNoteHdr = new GoodReceiveNoteHdr();

						if (null == dpSupplierInvDate.getValue()) {

							notifyMessage(5, "Please select Suppier Invoice date", false);
							goodReceiveNoteHdr = null;
							return;

						}

						if (txtSupplierInvNo.getText().trim().isEmpty()) {
							notifyMessage(5, "Please enter Suppier Invoice Number", false);
							goodReceiveNoteHdr = null;
							return;
						}
						goodReceiveNoteHdr.setSupplierId(supplierID);
						goodReceiveNoteHdr.setpONum(txtPONum.getText());
						goodReceiveNoteHdr.setSupplierInvNo(txtSupplierInvNo.getText());
						goodReceiveNoteHdr.setInvoiceDate(Date.valueOf(dpSupplierInvDate.getValue()));
						goodReceiveNoteHdr.setBranchCode(SystemSetting.getSystemBranch());

						logger.info("=====goodReceiveNoteHdr======" + goodReceiveNoteHdr);
						ResponseEntity<GoodReceiveNoteHdr> respentity1 = RestCaller
								.saveGoodReceiveNoteHdr(goodReceiveNoteHdr);
						goodReceiveNoteHdr = respentity1.getBody();

					}
					goodReceiveNoteDtl = new GoodReceiveNoteDtl();

					goodReceiveNoteDtl.setItemSerial(i);
					txtItemSerial.setText(++i + "");
					goodReceiveNoteDtl.setBarcode(purOrderDtl.getBarcode());
					goodReceiveNoteDtl.setBatch(purOrderDtl.getBatch());
					goodReceiveNoteDtl.setBinNo(purOrderDtl.getBinNo());
					goodReceiveNoteDtl.setItemId(purOrderDtl.getItemId());
					goodReceiveNoteDtl.setItemSerial(Integer.parseInt(purOrderDtl.getItemSerial()));
					goodReceiveNoteDtl.setUnitId(purOrderDtl.getUnitId());
					goodReceiveNoteDtl.setQty(purOrderDtl.getQty() - purOrderDtl.getReceivedQty());
					goodReceiveNoteDtl.setGoodReceiveNoteHdr(goodReceiveNoteHdr);
					goodReceiveNoteDtl.setMrp(purOrderDtl.getMrp());
					goodReceiveNoteDtl.setBatch("NOBATCH");

					ResponseEntity<ItemMst> resItem = RestCaller.getitemMst(purOrderDtl.getItemId());
					goodReceiveNoteDtl.setItemName(resItem.getBody().getItemName());
					ResponseEntity<GoodReceiveNoteDtl> respentity2 = RestCaller
							.saveGoodReceiveNoteDtl(goodReceiveNoteDtl);
					goodReceiveNoteDtl = respentity2.getBody();
					goodReceiveNoteDtl.setItemName(resItem.getBody().getItemName());
					goodReceiveNoteListTable.add(goodReceiveNoteDtl);
					goodReceiveNoteDtl = null;
					FillTable();

					txtQtyTotal.clear();

					setTotal();
				}
			}
		}
	}
	private void loadItemPropertyPopup(GoodReceiveNoteDtl goodReceiveNoteDtl,
			ResponseEntity<List<ItemPropertyInstance>> itemPropertyInstanceList) {

		try {
			logger.info("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPropertyInstance.fxml"));
			Parent root = loader.load();
			ItemPropertyInstancePopUpctl itempropertyPopup = loader.getController();
			if (null == itemPropertyInstanceList) {
				itempropertyPopup.setItemAndBBatch(goodReceiveNoteDtl);
			} else if (null == itemPropertyInstanceList.getBody()) {
				itempropertyPopup.setItemAndBBatch(goodReceiveNoteDtl);

			} else {
				itempropertyPopup.getPreviousPropertyInstance(goodReceiveNoteDtl, itemPropertyInstanceList);

			}
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.WINDOW_MODAL);
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}

	}

	/*This code without using itemPropertyInstance  
	 * 
	 * 
	 * 
	 * private void loadItemPropertyPopup(GoodReceiveNoteDtl goodReceiveNoteDtl,
	 * ResponseEntity<List<ItemPropertyInstance>> itemPropertyInstanceList) {
	 * 
	 * try { logger.info("inside the popup"); FXMLLoader loader = new
	 * FXMLLoader(getClass().getResource("/fxml/ItemPropertyInstance.fxml")); Parent
	 * root = loader.load(); ItemPropertyInstancePopUpctl itempropertyPopup =
	 * loader.getController();
	 * 
	 * Stage stage = new Stage(); stage.setScene(new Scene(root));
	 * stage.initStyle(StageStyle.UNDECORATED); stage.setTitle("Stock Item");
	 * stage.initModality(Modality.WINDOW_MODAL); stage.show();
	 * 
	 * } catch (Exception e) { e.printStackTrace();
	 * System.out.println(e.toString()); }
	 * 
	 * }
	 */

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
}
