package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemBatchDtl;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.OpeningStockDtl;
import com.maple.mapleclient.entity.PhysicalStockDtl;
import com.maple.mapleclient.entity.PhysicalStockHdr;
import com.maple.mapleclient.entity.WeighingItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockMigrationReport;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class StockMigrationCtl {

	
	 
	
	private ObservableList<OpeningStockDtl> itemDetailsList = FXCollections.observableArrayList();
	
	ItemMst itemMst = new ItemMst();
	
	OpeningStockDtl  openingStockDtl= null;
	EventBus eventBus = EventBusFactory.getEventBus();
	String itemId = "";
	String barcode = "";
	boolean multi = false;
	
	 @FXML
	    private TextField txtRate;

	
	  @FXML
	    private Button btnStokMigtn;

	    @FXML
	    private Button btnDelete;
	    
	    @FXML
	    private Button btnShowAll;
	    
	    @FXML
	    private TextField txtQty;

	    @FXML
	    private DatePicker dpExpiryDate;

	    @FXML
	    private Button btnAddItem;

//	    @FXML
//	    private Button btnDeleteRecord;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private Button btnSave;

	    @FXML
	    private TextField txtItemName;
//
//	    @FXML
//	    private ComboBox<String> cmbType;

//	    @FXML
//	    private ComboBox<String> cmbHeaderId;
//
//	    @FXML
//	    private Button btnFetchData;
	    
        @FXML
	    private TextField txtBarcode;

	    @FXML
	    private TextField txtBatchCode;
	    
	    @FXML
	    private DatePicker dpDate;
	    
	    @FXML
	    private TextField txtcurrentStock;


	    

	    @FXML
	    private TableView<OpeningStockDtl> tblDetails;

	    @FXML
	    private TableColumn<OpeningStockDtl, String> clItemName;

	    @FXML
	    private TableColumn<OpeningStockDtl, String> clBarcode;

	    @FXML
	    private TableColumn<OpeningStockDtl, String> clBatch;

	   @FXML
	    private TableColumn<OpeningStockDtl, Number> clRate;
	    
	    @FXML
	    private TableColumn<OpeningStockDtl, Number> clQtyIn;

	    @FXML
	    private TableColumn<OpeningStockDtl, Number> clQtyOut;
	    @FXML
	    void AddPhysicalStock(KeyEvent event) {

	    }
	    
	    
	    @FXML
	    void AddItem(ActionEvent event) {
	    	
	    	
	    
	    	if(null==txtQty.getText())
	    	{
	    		notifyMessage(5, "Please enter QTY..!!!");
	    		return;
	    	}
	    	
	    	
	    	
	    	openingStockDtl = new OpeningStockDtl();
	    	openingStockDtl.setBranchCode(SystemSetting.getUser().getBranchCode());
	    	openingStockDtl.setBarcode(txtBarcode.getText());
	    	openingStockDtl.setBatch(txtBatchCode.getText());
	    	openingStockDtl.setQtyIn(Double.parseDouble(txtQty.getText()));
	    	openingStockDtl.setMrp(Double.parseDouble(txtRate.getText()));
	    	openingStockDtl.setExpDate(Date.valueOf(dpExpiryDate.getValue()));
	    	openingStockDtl.setParticulars("OPENINGSTOCK");
	    	openingStockDtl.setStore("MAIN");
	        ResponseEntity<ItemMst> itemmst = RestCaller.getItemByNameRequestParam(txtItemName.getText());
	    	openingStockDtl.setItemId(itemmst.getBody().getId());
	    	ResponseEntity <OpeningStockDtl> openingStockDtlSaved = RestCaller.getSaveToOpeningStockDtll(openingStockDtl);
	    	openingStockDtl = openingStockDtlSaved.getBody();
	    	itemDetailsList.add(openingStockDtl);
	    	fillTable();
	    	clearDtl();
	    	openingStockDtl=null;
	    	fillTable();
//	    	AddItemFunction();

	    }
//
//	    @FXML
//	    void AddPhysicalStock(KeyEvent event) {
//	    	
//	    	if (event.getCode() == KeyCode.ENTER) {
//				if (null != dpExpiryDate.getValue()) {
//					AddItemFunction();
//				}
//			}
//
//    }

	    @FXML
	    void DeleteRecord(ActionEvent event) {
	    	if (null != openingStockDtl) {
				System.out.println("-----openingStockDtl-------" + openingStockDtl);

				if (null != openingStockDtl.getId()) {
					//RestCaller.deletePhysicalStockDtl(physicalStockDtl.getId());
					tblDetails.getItems().clear();
					
//					ResponseEntity<List<PhysicalStockDtl>> physicalStockDtlSaved = RestCaller
//							.getPhysicalStockDtl(physicalStockHdr);
//					physicalStockDtlListTable = FXCollections.observableArrayList(physicalStockDtlSaved.getBody());
//					//tblDetails.setItems(physicalStockDtlListTable);
//					
				 
					notifyMessage(5, "Deleted Successfully..!!!");

				}

			}
//			cmbHeaderId.getItems().clear();
			getHeaderId();
	    	
	    }
	    
	    @FXML
	    void FocusRate(KeyEvent event) {
	    	
	    	if (event.getCode() == KeyCode.ENTER) {
				txtRate.requestFocus();
			}

	    }
	    
	    @FXML
	    void dpDateKeyPress(KeyEvent event) {
	    	
	    	if (event.getCode() == KeyCode.ENTER) {
				txtItemName.requestFocus();
			}

	    }

//	    @FXML
//	    void FetchData(ActionEvent event) {
//	    	// cmbHeaderId.getItems().clear();
//			// getHeaderId();
//
//			if (null != cmbHeaderId.getSelectionModel().getSelectedItem()) {
//
//				physicalStockHdr = new PhysicalStockHdr();
//				physicalStockDtl = new PhysicalStockDtl();
//				physicalStockHdr.setId(cmbHeaderId.getSelectionModel().getSelectedItem());
//				if (null != physicalStockHdr.getId()) {
//					if (null != physicalStockHdrListTable) {
//						for (PhysicalStockHdr ph : physicalStockHdrListTable) {
//							if (ph.getId().equals(physicalStockHdr.getId())) {
//							Date date = ph.getVoucherDate();
//							dpDate.setValue(date.toLocalDate());
//							}
//						}
//					}
//
//				}
//
//				System.out.println("----physicalStockHdr----" + physicalStockHdr);
//
//				ResponseEntity<List<PhysicalStockDtl>> physicalStockDtlSaved = RestCaller
//						.getPhysicalStockDtl(physicalStockHdr);
//				ResponseEntity<PhysicalStockHdr> respentity = RestCaller.getPhysicalHdr(physicalStockHdr);
//				physicalStockHdr = respentity.getBody();
//				dpDate.setValue(physicalStockHdr.getVoucherDate().toLocalDate());
//				physicalStockDtlListTable = FXCollections.observableArrayList(physicalStockDtlSaved.getBody());
//
//				FillTable();
//
//			}

//	    }

	    @FXML
	    void FinalSave(ActionEvent event) {
	    	
	    	String vno = RestCaller.getVoucherNumber("OS");
	    	for(OpeningStockDtl open : itemDetailsList)
	    	{
	    		if(null==open.getVoucherNumber())
	    		{
	    		open.setVoucherNumber(vno);
	    		open.setVoucherDate(SystemSetting.getApplicationDate());
	    		RestCaller.updateOpeningStock(open);
	    		}
	    	}
	    	
	    	notifyMessage(5, "Updated Successfully..!!!");
	    	tblDetails.getItems().clear();
	    	itemDetailsList.clear();

	    }

	    @FXML
	    void FocusOnBatchCode(KeyEvent event) {
	    	

			if (event.getCode() == KeyCode.BACK_SPACE) {

				txtItemName.requestFocus();

				showPopup();
			}
//			if (event.getCode() == KeyCode.ENTER) {
//				if (txtBarcode.getText().trim().isEmpty()) {
//				} else {
//
//					if (null == txtBarcode) {
//						return;
//
//					}
//					if (null == txtBarcode.getText()) {
//						return;
//
//					}
//					String subBarcode = null;
//					if (txtBarcode.getText().trim().length() >= 12) {
//						String barcode1 = txtBarcode.getText();
//						subBarcode = barcode1.substring(0, 7);
//					}
//					ArrayList items = new ArrayList();
//
//					items = RestCaller.SearchStockItemByBarcode(txtBarcode.getText());
//					if (!items.isEmpty()) {
//						if (txtBarcode.getText().startsWith("21")) {
//							if (null == subBarcode) {
//								subBarcode = txtBarcode.getText();
//							}
//							ResponseEntity<WeighingItemMst> weighingItems = RestCaller
//									.getWeighingItemMstByBarcode(subBarcode);
//							if (null != weighingItems.getBody()) {
//								items = RestCaller.SearchStockItemByBarcode(subBarcode);
//								if (txtBarcode.getText().trim().length() > 7) {
//									txtQty.setText(txtBarcode.getText().substring(7, 9) + "."
//											+ txtBarcode.getText().substring(9, 12));
//								}
//								txtBarcode.setText(subBarcode);
//							}
//						}
//						if (items.size() > 1) {
//							multi = true;
//							showPopupByBarcode();
//						} else {
//							multi = false;
//							String itemName = "";
//							String barcode = "";
//							Double mrp = 0.0;
//							String unitname = "";
////						Double qty = 0.0;
////						Double cess = 0.0;
//							Double tax = 0.0;
//							String itemId = "";
//							String unitId = "";
//							Iterator itr = items.iterator();
//							String batch = "";
//
//							while (itr.hasNext()) {
//
//								List element = (List) itr.next();
//								itemName = (String) element.get(0);
//								barcode = (String) element.get(1);
//								unitname = (String) element.get(2);
//								mrp = (Double) element.get(3);
////							qty = (Double) element.get(4);
////							cess = (Double) element.get(5);
//								tax = (Double) element.get(6);
//								itemId = (String) element.get(7);
//								unitId = (String) element.get(8);
//								batch = (String) element.get(9);
//								if (null != itemName) {
//									txtBarcode.setText(barcode);
//									txtBatchCode.setText(batch);
//									txtItemName.setText(itemName);
//									txtRate.setText(Double.toString(mrp));
//									ResponseEntity<List<ItemBatchExpiryDtl>> getItemBatch = RestCaller
//											.getItemBatchExpByItemAndBatch(itemId, batch);
//									if (!getItemBatch.getBody().isEmpty()) {
//										dpExpiryDate.setValue(SystemSetting
//												.utilToLocaDate(getItemBatch.getBody().get(0).getExpiryDate()));
//									}
//									ArrayList itemsArray = new ArrayList();
//									itemsArray = RestCaller.SearchStockItemByName(txtItemName.getText());
//									Double qty = 0.0;
//									txtcurrentStock.setText("0");
//									Iterator itr1 = itemsArray.iterator();
//									while (itr1.hasNext()) {
//										List element1 = (List) itr1.next();
//										qty = (Double) element1.get(4);
//										if (qty > 0.0)
//											txtcurrentStock.setText(qty.toString());
//										else
//											txtcurrentStock.setText("0");
//									}
//								}
//							}
//						}
//						txtQty.requestFocus();
//					}
//
//					else {
//						ResponseEntity<List<ItemMst>> getItems = RestCaller.getItemByBarcode(txtBarcode.getText());
//						txtItemName.setText(getItems.getBody().get(0).getItemName());
//						txtcurrentStock.setText("0.0");
//						txtBatchCode.requestFocus();
//
//					}
//				}

//			}
	    	

	    }

	    @FXML
	    void FocusOnManufacturingDate(KeyEvent event) {


	    }

	    @FXML
	    void OnActionClear(ActionEvent event) {
	    	
	    	txtBarcode.setText("");
			txtBatchCode.setText("");
			txtItemName.setText("");
			txtQty.setText("");
			txtRate.setText("");
			tblDetails.getItems().clear();
			// dpDate.setValue(null);
//			cmbHeaderId.getItems().clear();
			getHeaderId();
//			cmbType.getSelectionModel().clearSelection();

	    }

//	    @FXML
//	    void SetPhysicalStockHdr(MouseEvent event) {
//	    	
//	    	getHeaderId();
//
//	    }

	    @FXML
	    void ShowItemPopup(MouseEvent event) {
	    	
//	    	if (cmbType.getSelectionModel().isEmpty()) {
//				notifyMessage(3, "Select Type");
//				return;
//			}
//
//			if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("NEW STOCK")) {
//
//				ShowItemPopup();
//			} else if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("STOCK CORRECTION"))
//
//			{
//				if (SystemSetting.isNEGATIVEBILLING()) {
//					ShowItemPopup();

//				} else {
//					showPopup();
//
//				}
//
//}
	    	
	    	showPopup();
	    	
	    }
	    
	    
	    
	    @FXML
	    void ShowPopup(KeyEvent event) {
	    	
//	    	if (event.getCode() == KeyCode.ENTER) {
//				if (txtItemName.getText().length() > 0) {
//					txtBarcode.requestFocus();
//				} else {
//
//					if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("NEW STOCK")) {
//						showPopup();
//						// ShowItemPopup();
//					} else if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("STOCK CORRECTION"))
//
//					{
//						if (SystemSetting.isNEGATIVEBILLING()) {
//							if (SystemSetting.isNEGATIVEBILLING()) {
//								showPopup();
//							} else {
//								ShowItemPopup();
//							}
//						}
//
//					}
//				}
//
//			}
	    	
	    	ShowItemPopup();
	    }

	   
	    
	    
	    
	    
	    @FXML
		private void initialize() {
//	    	
//	    	tblDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//				if (newSelection != null) {
//					System.out.println("getSelectionModel");
//					if (null != newSelection.getId()) {
//
//						 itemBatchDtl = new ItemBatchDtl();
//						itemBatchDtl.setId(newSelection.getId());
//					}
//				}
//			});
	    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
	    	dpExpiryDate = SystemSetting.datePickerFormat(dpExpiryDate, "dd/MMM/yyyy");
	    	dpDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
			dpDate.setEditable(false);
			eventBus.register(this);
			//cmbType.getItems().add("NEW STOCK");
			//cmbType.getItems().add("STOCK CORRECTION");
			

			//cmbType.valueProperty().addListener(new ChangeListener<String>() {
//				@Override
//				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//					if (!newValue.equalsIgnoreCase(null)) {
//						if (newValue.equalsIgnoreCase("NEW STOCK")) {
//							txtBatchCode.setEditable(true);
//							dpExpiryDate.setDisable(false);
//						} else {
//							txtBatchCode.setEditable(false);
//							dpExpiryDate.setDisable(true);
//						}
//					}
//				}
//			});

			txtQty.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtQty.setText(oldValue);
					}
				}
			});

			txtRate.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtRate.setText(oldValue);
					}
				}
			});

			tblDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {

					if (null != newSelection.getId()) {

						
						
						openingStockDtl = new OpeningStockDtl();
						openingStockDtl.setId(newSelection.getId());
						openingStockDtl.setParticulars(newSelection.getParticulars());
						openingStockDtl.setVoucherNumber(newSelection.getVoucherNumber());
						
						System.out.println("-------physicalStockDtl------" + openingStockDtl);

					}
				}
			});

			getHeaderId();

		
	    
	    
	    }
	    
	    

	    @FXML
	    void StockMigration(ActionEvent event) {
	    	
	    	ResponseEntity<String> stockmigrationstatus =RestCaller.stockMigration();
	    	
	    	notifyMessage(5, "Stock Migration  completed..!!!");

	    }

	    @FXML
	    void delete(ActionEvent event) {
	    	
	    	   if(null==openingStockDtl.getVoucherNumber())
	    	   {

				RestCaller.deleteOpeningStockDtl(openingStockDtl.getId());
				ResponseEntity<List<OpeningStockDtl>> openingStockDtlReport =RestCaller.getOpeningStockDtlnull();
		    	itemDetailsList = FXCollections.observableArrayList(openingStockDtlReport.getBody());
				fillTable();
				
				notifyMessage(5, "Deleted!!!");
	    	   }
	    	   else {
	    		   
	    		   notifyMessage(5, "Voucher Number Generated...!!!");
	    	   }
				
	                   }

	    
	    @FXML
	    void ShowAll(ActionEvent event) {
	    	
	    	
	    	
	    	ResponseEntity<List<OpeningStockDtl>> openingStockDtlReport =RestCaller.getOpeningStockDtl();
	    	itemDetailsList = FXCollections.observableArrayList(openingStockDtlReport.getBody());
	    	fillTable();

	    }
	    
	    
	    private void fillTable() {
	    
	    	for(OpeningStockDtl open:itemDetailsList)
	    	{
	    		ResponseEntity<ItemMst> itemById = RestCaller.getitemMst(open.getItemId());
	    		open.setItemName(itemById.getBody().getItemName());
	    	}
	    	
	    	tblDetails.setItems(itemDetailsList);
	    	
	    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	    	clBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
	    	clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
	        clRate.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
	        clQtyIn.setCellValueFactory(cellData -> cellData.getValue().getQtyInProperty());
	        clQtyOut.setCellValueFactory(cellData -> cellData.getValue().getQtyOutProperty());
			
		}
	    
	    public void notifyMessage(int duration, String msg) {

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}
	    
//========================================code from physicalstock ctl=========================//////////////////	    
	    
	    
	    
	    
	    
	    private void showPopup() {
			try {
			


				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));

				Parent root1;
				ItemStockPopupCtl itemStockPopupCtl = fxmlLoader.getController();
				itemStockPopupCtl.windowName = "POS";
				root1 = (Parent) fxmlLoader.load();
				String cst = null;
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

				txtQty.requestFocus();
				// }

				txtBarcode.requestFocus();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	    
	    
	    
//	    
//	    
//	    
//	    
//	    private void showPopupByBarcode() {
//			try {
//				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
//				// fxmlLoader.setController(itemStockPopupCtl);
//				Parent root1;
//
//				root1 = (Parent) fxmlLoader.load();
//				Stage stage = new Stage();
//
//				ItemStockPopupCtl popctl = fxmlLoader.getController();
//				popctl.LoadItemPopupByBarcode(txtBarcode.getText());
//
//				stage.initModality(Modality.APPLICATION_MODAL);
//				stage.initStyle(StageStyle.UNDECORATED);
//				stage.setTitle("Stock Item");
//				stage.initModality(Modality.APPLICATION_MODAL);
//				stage.setScene(new Scene(root1));
//				stage.show();
//
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//
//		}
	    
		private void ShowItemPopup() {

			try {
				System.out.println("inside the popup");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				Parent root = loader.load();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				stage.show();
				txtQty.requestFocus();

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	
		
		private void clearDtl() {
			dpExpiryDate.setValue(null);
			//dpManfDate.setValue(null);
			txtBarcode.setText("");
			txtBatchCode.setText("");
			txtItemName.setText("");
			txtQty.setText("");
			txtRate.setText("");
			txtcurrentStock.clear();

		}
		
		
		private void getHeaderId() {

			ArrayList head = new ArrayList();
			RestTemplate restTemplate1 = new RestTemplate();
			head = RestCaller.SearchPhysicalStockHeader();
			Iterator itr = head.iterator();
			while (itr.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				System.out.println("------lm-------" + lm);
				Object headerId = lm.get("id");
				Object voucherDate = lm.get("voucherDate");
				if (headerId != null) {

					//cmbHeaderId.getItems().add((String) headerId);
					PhysicalStockHdr phHdr = new PhysicalStockHdr();
					phHdr.setId((String) headerId);
//					phHdr.setVoucherDate((Date)voucherDate);

//					physicalStockHdrListTable.add(phHdr);
				}

			}

		}
		
		@Subscribe
		public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

			System.out.println("------@@@-------popupItemlistner-------@@@@------" + itemPopupEvent.getItemName());
			Stage stage = (Stage) btnStokMigtn.getScene().getWindow();
			if (stage.isShowing()) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (!multi) {
							txtBarcode.setText(itemPopupEvent.getBarCode());
							txtBatchCode.setText(itemPopupEvent.getBatch());
							txtItemName.setText(itemPopupEvent.getItemName());
							txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
							if (null != itemPopupEvent.getExpiryDate())
								// version4.3
								dpExpiryDate.setValue(itemPopupEvent.getExpiryDate().toLocalDate());
							itemId = itemPopupEvent.getItemId();
							barcode = itemPopupEvent.getBarCode();
							ArrayList items = new ArrayList();
							items = RestCaller.SearchStockItemByName(itemPopupEvent.getItemName());
							Double qty = 0.0;
							txtcurrentStock.setText("0");
							Iterator itr = items.iterator();
							while (itr.hasNext()) {
								List element = (List) itr.next();
								qty = (Double) element.get(4);
								if (qty > 0.0)
									txtcurrentStock.setText(qty.toString());
								else
									txtcurrentStock.setText("0");
							}
							System.out.println("---------itemid -----------" + itemId + "-----barcode------" + barcode);

						} else if (multi) {
							txtBarcode.setText(itemPopupEvent.getBarCode());
							txtBatchCode.setText(itemPopupEvent.getBatch());
							txtItemName.setText(itemPopupEvent.getItemName());
							txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
							itemId = itemPopupEvent.getItemId();
							barcode = itemPopupEvent.getBarCode();
							ArrayList items = new ArrayList();
							items = RestCaller.SearchStockItemByName(txtItemName.getText());
							Double qty = 0.0;
							txtcurrentStock.setText("0");
							Iterator itr = items.iterator();
							while (itr.hasNext()) {
								List element = (List) itr.next();
								qty = (Double) element.get(4);
								if (qty > 0.0)
									txtcurrentStock.setText(qty.toString());
								else
									txtcurrentStock.setText("0");
							}

							ResponseEntity<List<ItemBatchExpiryDtl>> getItemBatch = RestCaller
									.getItemBatchExpByItemAndBatch(itemId, txtBatchCode.getText());
							if (!getItemBatch.getBody().isEmpty()) {
								dpExpiryDate.setValue(
										SystemSetting.utilToLocaDate(getItemBatch.getBody().get(0).getExpiryDate()));
							}
						}
					}
				});
			}
		
		
		}


}
