package com.maple.mapleclient.controllers;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class SubscriptionValidityCtl {
	
	@FXML
    private TextField txtUserName;

    @FXML
    private TextField txtPassword;

    @FXML
    private Button btnUpdate;

    @FXML
    void OnUpdate(MouseEvent event) {
    	
    	if (null != txtUserName.getText()) 
    	
    	  { notifyMessage(5, "Please select Username", false);
  		return;
    	  }
    	
    		if (null != txtPassword.getText())
    		{
    			notifyMessage(5, "Please select Password", false);
        		return;
    		}
    		
    			
    	ResponseEntity<String> subscriptionRespo = RestCaller.UpdateSubscription(txtUserName.getText(),txtPassword.getText());
    		
    	  }

    
    
    
    private void notifyMessage(int i, String string, boolean b) {
			
	    	Image img;
			if (b) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

	    	
		}


	
	

}
