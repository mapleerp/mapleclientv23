package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class CategoryPopupCtl {
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<CategoryMst> categoryList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

	CategoryEvent categoryEvent;

    @FXML
    private TextField txtname;

    
    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<CategoryMst> tablePopupView;

    @FXML
    private TableColumn<CategoryMst, String> categoryName;

    @FXML
    private Button btnCancel;

    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtname.requestFocus();
		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    
    @FXML
	private void initialize() {
    	LoadCategoryBySearch("");
		
		txtname.textProperty().bindBidirectional(SearchString);
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		btnCancel.setCache(true);
		
		tablePopupView.setItems(categoryList);
		
		categoryName.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
		
		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
				
					categoryEvent = new CategoryEvent();
					categoryEvent.setCategoryId(newSelection.getId());
					categoryEvent.setCategoryName(newSelection.getCategoryName());
					eventBus.post(categoryEvent);
				}
			}
		});


		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadCategoryBySearch((String) newValue);
			}
		});

    }
    
	private void LoadCategoryBySearch(String searchData) {

		categoryList.clear();
		ArrayList categoryArray = new ArrayList();
		categoryArray = RestCaller.searchCategoryByName(searchData);
		Iterator itr = categoryArray.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (null != id) {
				CategoryMst category = new CategoryMst();
				category.setCategoryName((String) categoryName);
				category.setId((String) id);
				categoryList.add(category);
			}

		}
		
		

		return;

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	       private void PageReload() {
	       	
	 }

}
