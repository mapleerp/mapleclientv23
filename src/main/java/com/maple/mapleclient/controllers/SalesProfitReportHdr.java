package com.maple.mapleclient.controllers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesProfitReportHdr {
	
	String taskid;
	String processInstanceId;


	
	String id;
	String voucherNumber;
	String voucherDate;
	String customer;
	Double invoiceAmount;
	String receiptMode;
    Double profit;
    
    String batch;
    
    
    
	@JsonIgnore
	private StringProperty customerProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private DoubleProperty InvoiceAmountProperty;

	@JsonIgnore
	private StringProperty receiptModeProperty;
	

	
	@JsonIgnore
	private DoubleProperty profitProperty;
    
	@JsonIgnore
	private StringProperty batchProperty;
	

public SalesProfitReportHdr() {
	
	
	    
	
	this.customerProperty = new SimpleStringProperty();
	this.voucherDateProperty =new SimpleStringProperty();
	this.voucherNumberProperty = new SimpleStringProperty();
	this.receiptModeProperty = new SimpleStringProperty();
	this.InvoiceAmountProperty = new SimpleDoubleProperty();
	this.profitProperty = new SimpleDoubleProperty();
	this.batchProperty = new SimpleStringProperty();
			
	}






public String getBatch() {
	return batch;
}

public void setBatch(String batch) {
	this.batch = batch;
}


public StringProperty getBatchProperty() {
	if(null != batch) {
		batchProperty.set(batch);
	}
	return batchProperty;
}


public void setBatchProperty(StringProperty batchProperty) {
	this.batchProperty = batchProperty;
}






public String getId() {
	return id;
}




public void setId(String id) {
	this.id = id;
}




public String getVoucherNumber() {
	return voucherNumber;
}




public void setVoucherNumber(String voucherNumber) {
	this.voucherNumber = voucherNumber;
}




public String getVoucherDate() {
	return voucherDate;
}




public void setVoucherDate(String voucherDate) {
	this.voucherDate = voucherDate;
}




public String getCustomer() {
	return customer;
}




public void setCustomer(String customer) {
	this.customer = customer;
}




public Double getInvoiceAmount() {
	return invoiceAmount;
}




public void setInvoiceAmount(Double invoiceAmount) {
	this.invoiceAmount = invoiceAmount;
}




public String getReceiptMode() {
	return receiptMode;
}




public void setReceiptMode(String receiptMode) {
	this.receiptMode = receiptMode;
}




public Double getProfit() {
	return profit;
}




public void setProfit(Double profit) {
	this.profit = profit;
}



@JsonIgnore
public StringProperty getCustomerProperty() {
	customerProperty.set(customer);
	return customerProperty;
}




public void setCustomerProperty(StringProperty customerProperty) {
	this.customerProperty = customerProperty;
}



@JsonIgnore
public StringProperty getVoucherDateProperty() {
	voucherDateProperty.set(voucherDate);
	
	return voucherDateProperty;
}




public void setVoucherDateProperty(StringProperty voucherDateProperty) {
	this.voucherDateProperty = voucherDateProperty;
}



@JsonIgnore
public StringProperty getVoucherNumberProperty() {
	voucherNumberProperty.set(	voucherNumber);
	return voucherNumberProperty;
}




public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
	this.voucherNumberProperty = voucherNumberProperty;
}



@JsonIgnore
public DoubleProperty getInvoiceAmountProperty() {
	InvoiceAmountProperty.set(invoiceAmount);
	return InvoiceAmountProperty;
}




public void setInvoiceAmountProperty(DoubleProperty invoiceAmountProperty) {
	InvoiceAmountProperty = invoiceAmountProperty;
}



@JsonIgnore
public StringProperty getReceiptModeProperty() {
	
	receiptModeProperty.set(receiptMode);
	return receiptModeProperty;
}




public void setReceiptModeProperty(StringProperty receiptModeProperty) {
	this.receiptModeProperty = receiptModeProperty;
}



@JsonIgnore
public DoubleProperty getProfitProperty() {
	
	if(null!=profit) {
	profitProperty.set(profit);
	}else {
		profitProperty.set(0.0);
	}
	return profitProperty;
}




public void setProfitProperty(DoubleProperty profitProperty) {
	this.profitProperty = profitProperty;
}





@Override
public String toString() {
	return "SalesProfitReportHdr [taskid=" + taskid + ", processInstanceId=" + processInstanceId + ", id=" + id
			+ ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", customer=" + customer
			+ ", invoiceAmount=" + invoiceAmount + ", receiptMode=" + receiptMode + ", profit=" + profit + ", batch="
			+ batch + ", customerProperty=" + customerProperty + ", voucherDateProperty=" + voucherDateProperty
			+ ", voucherNumberProperty=" + voucherNumberProperty + ", InvoiceAmountProperty=" + InvoiceAmountProperty
			+ ", receiptModeProperty=" + receiptModeProperty + ", profitProperty=" + profitProperty + ", batchProperty="
			+ batchProperty + "]";
}






@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


private void PageReload(String hdrId) {
	
}
    






}
