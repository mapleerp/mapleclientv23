package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.fasterxml.jackson.annotation.JacksonInject.Value;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.LocalCustomerDtl;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.NutritionValueDtl;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class localCustomerCtl {
	
	String taskid;
	String processInstanceId;
	
	LocalCustomerMst localCustomerMst = null;
	LocalCustomerDtl localCustomerDtl = null;
	private ObservableList<LocalCustomerDtl> localCustomerDtlList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();



    @FXML
    private TextField txtCustName;

    @FXML
    private TextField txtAddress;

    @FXML
    private TextField txtPhoneNo1;

    @FXML
    private TextField txtPhneNo2;

    @FXML
    private DatePicker dpCustDateOfBirth;

    @FXML
    private DatePicker dpCustWeddingDate;

    @FXML
    private TextField txtRelationName;

    @FXML
    private TextField txtRelation;

    @FXML
    private DatePicker dpRelationDOB;

    @FXML
    private Button tbnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private TableView<LocalCustomerDtl> tblCustDetails;

    @FXML
    private TableColumn<LocalCustomerDtl, String> clName;

    @FXML
    private TableColumn<LocalCustomerDtl, String> clRelation;

    @FXML
    private TableColumn<LocalCustomerDtl, String> clDOB;
    
    @FXML
    private Button btnFinalSave;
    
    
	@FXML
	private void initialize() {
		dpCustDateOfBirth = SystemSetting.datePickerFormat(dpCustDateOfBirth, "dd/MMM/yyyy");
		dpCustWeddingDate = SystemSetting.datePickerFormat(dpCustWeddingDate, "dd/MMM/yyyy");
		dpRelationDOB = SystemSetting.datePickerFormat(dpRelationDOB, "dd/MMM/yyyy");
		eventBus.register(this);
		

//		txtPhoneNo1.textProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				
//				if(null!=newValue) {
//					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?") || ((Double.parseDouble(newValue))>5000)) {
//						
//						txtPhoneNo1.setText(oldValue);
//					}
//				}
//			}
//		});
//		
//		txtPhneNo2.textProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				
//				if(null!=newValue) {
//					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?") || ((Double.parseDouble(newValue))>5000)) {
//						
//						txtPhneNo2.setText(oldValue);
//					}
//				}
//			}
//		});
		
		tblCustDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					
					localCustomerDtl = new LocalCustomerDtl();
					localCustomerDtl.setId(newSelection.getId());
				}
			}
		});
		
	}

    @FXML
    void BackOnKeyPress(KeyEvent event) {

    }

    @FXML
    void Delete(ActionEvent event) {
    	
    	if(null != localCustomerDtl)
    	{
    		if(null != localCustomerDtl.getId())
    		{
    			
    			RestCaller.deleteLocalCustomerDtl(localCustomerDtl.getId());
    			
    			ResponseEntity<List<LocalCustomerDtl>> localCustListResp = RestCaller.getLocalCustomerDtlByHdrId(localCustomerMst.getId()); 
    			localCustomerDtlList = FXCollections.observableArrayList(localCustListResp.getBody());
    			FillTable();    			
    		} else {
    			notifyMessage(3, "Please select relation", false);
    			return;
			}
    	} else {
			notifyMessage(3, "Please select relation", false);
			return;
		}
    }
    
    @FXML
    void OnFocusAddress(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		
    		if(txtCustName.getText().trim().isEmpty())
    		{
    			showLocalCustPopup();
    		} else {
    			txtAddress.requestFocus();

			}
		}
    }
    
    @FXML
    void OnFocusPhoneNo1(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtPhoneNo1.requestFocus();
		}
    }
    
    @FXML
    void OnFocusPhoneNo2(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtPhneNo2.requestFocus();
		}
    }
    
    @FXML
    void OnFocusDpDOB(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			dpCustDateOfBirth.requestFocus();
		}
    }
    
    
    @FXML
    void OnFocusWeddingDate(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			dpCustWeddingDate.requestFocus();
		}
    }
    
    @FXML
    void OnFocusRelationName(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtRelationName.requestFocus();
		}
    }
    
    @FXML
    void OnFocusRelation(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtRelation.requestFocus();
		}
    }
    
    @FXML
    void OnFocusRelationDOB(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			dpRelationDOB.requestFocus();
		}
    }
    
    @FXML
    void OnFocusSave(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		tbnSave.requestFocus();
		}

    }
    
    @FXML
    void SaveOnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
        	SaveLocalCustomer();
		}
    }

    @FXML
    void Save(ActionEvent event) {
    	
    	SaveLocalCustomer();
    	
    }
    
    private void SaveLocalCustomer() {

    	
    	if(txtCustName.getText().trim().isEmpty())
    	{
    		notifyMessage(3, "Please enter customer name", false);
    		txtCustName.requestFocus();
    		return;
    	}
    	if(txtAddress.getText().trim().isEmpty())
    	{
    		notifyMessage(3, "Please enter customer address", false);
    		txtAddress.requestFocus();
    		return;
    	}
    	
    	if(txtPhoneNo1.getText().trim().isEmpty())
    	{
    		notifyMessage(3, "Please enter customer phone number", false);
    		txtPhoneNo1.requestFocus();
    		return;
    	}
    	
    	
    	if (!txtCustName.getText().contains(txtPhoneNo1.getText())) {

			notifyMessage(5, "Please add customer 1st phone number to the end of customer name",false);
			return;
		} 
    	
    	if(null == localCustomerMst)
    	{
    		
    		localCustomerMst = new LocalCustomerMst();
    		localCustomerMst.setLocalcustomerName(txtCustName.getText());
    		localCustomerMst.setAddress(txtAddress.getText());
    		localCustomerMst.setPhoneNo(txtPhoneNo1.getText());
    		
    		if(!txtPhneNo2.getText().trim().isEmpty())
    		{
        		localCustomerMst.setPhoneNO2(txtPhneNo2.getText());
    		}
    		
    		
    		if(null != dpCustWeddingDate.getValue())
    		{
        		localCustomerMst.setWeddingDate(SystemSetting.localToUtilDate(dpCustWeddingDate.getValue()));
    		}
    		
    		if(null != dpCustDateOfBirth.getValue())
    		{
        		localCustomerMst.setDateOfBirth(SystemSetting.localToUtilDate(dpCustDateOfBirth.getValue()));
    		}
    		
    		ResponseEntity<LocalCustomerMst> localCustResp = RestCaller.saveLocalCustomer(localCustomerMst);
    		localCustomerMst = localCustResp.getBody();
    		
    	}
    	
    	if(!txtRelationName.getText().trim().isEmpty())
    	{
    		localCustomerDtl = new LocalCustomerDtl();
        	
        	localCustomerDtl.setName(txtRelationName.getText());
        	if(!txtRelation.getText().trim().isEmpty())
        	{
            	localCustomerDtl.setRelation(txtRelation.getText());

        	}
        	if(null != dpRelationDOB.getValue())
        	{
            	localCustomerDtl.setDateOfBirth(SystemSetting.localToUtilDate(dpRelationDOB.getValue()));

        	}
        	localCustomerDtl.setLocalCustomerMst(localCustomerMst);
        	
    		ResponseEntity<LocalCustomerDtl> localCustResp = RestCaller.saveLocalCustomerDtl(localCustomerDtl);
    		
    	}
    	
		ResponseEntity<List<LocalCustomerDtl>> localCustListResp = RestCaller.getLocalCustomerDtlByHdrId(localCustomerMst.getId()); 

		localCustomerDtlList = FXCollections.observableArrayList(localCustListResp.getBody());
		
		notifyMessage(3, "Loyality CUstomer Successfully Saved...!", true);
		
		FillTable();
		
		txtRelationName.clear();
		txtRelation.clear();
		dpRelationDOB.setValue(null);
		localCustomerDtl = null;

    		
	}
    
	private void showLocalCustPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/LocalCustPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtRelationName.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
    void FinalSave(ActionEvent event) {
		
		if(null == localCustomerMst)
		{
			notifyMessage(3, "Please select local customer...!", false);
			return;
		}
		if(null == localCustomerMst.getId())
		{
			notifyMessage(3, "Please select local customer...!", false);
			return;
		}
		
		if (!txtCustName.getText().contains(txtPhoneNo1.getText())) {

			notifyMessage(5, "Please add customer 1st phone number to the end of customer name",false);
			return;
		} 
		
		localCustomerMst.setLocalcustomerName(txtCustName.getText());
		localCustomerMst.setAddress(txtAddress.getText());
		localCustomerMst.setPhoneNo(txtPhoneNo1.getText());
		
		if(null == txtPhneNo2.getText())
		{
    		localCustomerMst.setPhoneNO2(txtPhneNo2.getText());
		}
		
		if(null != dpCustDateOfBirth.getValue())
		{
    		localCustomerMst.setDateOfBirth(SystemSetting.localToUtilDate(dpCustDateOfBirth.getValue()));
		}
		if(null != dpCustWeddingDate.getValue())
		{
    		localCustomerMst.setWeddingDate(SystemSetting.localToUtilDate(dpCustWeddingDate.getValue()));
		}
		
		RestCaller.updateLocalCustomer(localCustomerMst);
    	
    	localCustomerMst = null;
    	localCustomerDtl = null;
    	
    	txtAddress.clear();
    	txtCustName.clear();
    	txtPhneNo2.clear();
    	txtPhoneNo1.clear();
    	txtRelation.clear();
    	txtRelationName.clear();
    	txtRelationName.clear();
		txtRelation.clear();
		dpRelationDOB.setValue(null);
		dpCustWeddingDate.setValue(null);
		dpCustDateOfBirth.setValue(null);
    	
    	tblCustDetails.getItems().clear();

    }
    
	private void FillTable() {
		
		tblCustDetails.setItems(localCustomerDtlList);

		clDOB.setCellValueFactory(cellData -> cellData.getValue().getDobProperty());
		clName.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		clRelation.setCellValueFactory(cellData -> cellData.getValue().getRelationProperty());

	}
	
	@Subscribe
	public void popupLocalCustomerlistner(LocalCustomerEvent localCustomerEvent) {

    	localCustomerMst = null;
    	localCustomerDtl = null;
    	
		txtAddress.clear();
    	txtCustName.clear();
    	txtPhneNo2.clear();
    	txtPhoneNo1.clear();
    	txtRelation.clear();
    	txtRelationName.clear();
    	txtRelationName.clear();
		txtRelation.clear();
		dpRelationDOB.setValue(null);
		dpCustWeddingDate.setValue(null);
		dpCustDateOfBirth.setValue(null);
    	
    	tblCustDetails.getItems().clear();

		Stage stage = (Stage) tbnSave.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					
					txtCustName.setText(localCustomerEvent.getLocalCustName());
					txtAddress.setText(localCustomerEvent.getLocalCustAddress());

					txtPhoneNo1.setText(localCustomerEvent.getLocalCustPhoneNo());
					txtPhneNo2.setText(localCustomerEvent.getLocalCustPhone2());
					if(null != localCustomerEvent.getDateOfBirth())
					{
						dpCustDateOfBirth.setValue(SystemSetting.utilToLocaDate(localCustomerEvent.getDateOfBirth()));

					}
					
					if(null != localCustomerEvent.getWeddingDate())
					{
						dpCustWeddingDate.setValue(SystemSetting.utilToLocaDate(localCustomerEvent.getWeddingDate()));

					}
					
					ResponseEntity<LocalCustomerMst> localCustomerResp = RestCaller.getLocalCustomerbyId(localCustomerEvent.getLocalCustid());
					localCustomerMst = localCustomerResp.getBody();
					
					ResponseEntity<List<LocalCustomerDtl>> localCustListResp = RestCaller.getLocalCustomerDtlByHdrId(localCustomerMst.getId()); 
					localCustomerDtlList = FXCollections.observableArrayList(localCustListResp.getBody());
					FillTable();
					
				}
			});
		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }

}
