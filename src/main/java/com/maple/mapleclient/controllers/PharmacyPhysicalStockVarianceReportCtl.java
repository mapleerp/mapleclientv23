package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PharmacyPhysicalStockVarianceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyPhysicalStockVarianceReportCtl {
//  (0_0)       SharonN  

	private ObservableList<PharmacyPhysicalStockVarianceReport> physicalStockVarianceReportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dptoDate;

    @FXML
    private Button btnPrintReport;

    @FXML
    private Button btnGenerate;

    @FXML
    private DatePicker dpfromDate;

    @FXML
    private TableView<PharmacyPhysicalStockVarianceReport> tblphysicalstockvariance;

   

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, String> clVoucherNumber;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, String> clstockcorrectionDate;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, String> clitemgroup;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, String> clitemname;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, String> clbatchcode;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, Number> clsystemqty;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, Number> clphysicalQty;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, Number> clvarianceqty;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, Number> clprice;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, Number> clcost;

    @FXML
    private TableColumn<PharmacyPhysicalStockVarianceReport, String> clusername;
	@FXML
	private void initialize() {
		dpfromDate = SystemSetting.datePickerFormat(dpfromDate, "dd/MMM/yyyy");
		dptoDate = SystemSetting.datePickerFormat(dptoDate, "dd/MMM/yyyy");
		tblphysicalstockvariance.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getItemName()) {

					
					fillTable();


				}
			}
		});

	}
    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null == dpfromDate.getValue()) {

			notifyMessage(3, "please select From date", false);
			dpfromDate.requestFocus();
			return;
		}
		if (null == dptoDate.getValue()) {

			notifyMessage(3, "please select Todate", false);
			dptoDate.requestFocus();
			return;
		}

		java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dptoDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		ResponseEntity<List<PharmacyPhysicalStockVarianceReport>> physicalStockVarianceResp = RestCaller.getPharmacyPhysicalStockVarianceReport(fromdate, todate);

		physicalStockVarianceReportList = FXCollections.observableArrayList(physicalStockVarianceResp.getBody());

		fillTable();

	}
	 public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
	private void fillTable() {
		tblphysicalstockvariance.setItems(physicalStockVarianceReportList);

		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clstockcorrectionDate.setCellValueFactory(cellData -> cellData.getValue().getStockcorrectionDateProperty());
		clitemgroup.setCellValueFactory(cellData -> cellData.getValue().getItemGroupProperty());
		clitemname.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clbatchcode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clsystemqty.setCellValueFactory(cellData -> cellData.getValue().getSystemQtyProperty());
		clphysicalQty.setCellValueFactory(cellData -> cellData.getValue().getPhysicalQtyProperty());
		clvarianceqty.setCellValueFactory(cellData -> cellData.getValue().getVarianceQtyProperty());
		clprice.setCellValueFactory(cellData -> cellData.getValue().getPriceProperty());
		clcost.setCellValueFactory(cellData -> cellData.getValue().getCostProperty());
		clusername.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
	}
    @FXML
    void PrintReport(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dptoDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		try {
			JasperPdfReportService.PhysicalStockVarianceReport(fromdate, todate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}