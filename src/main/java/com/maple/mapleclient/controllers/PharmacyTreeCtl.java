package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;

public class PharmacyTreeCtl {

	String taskid;
	String processInstanceId;
	String selectedText = null;
	@FXML
	private TreeView<?> treeview;

	@FXML
	void initialize() {

		TreeItem rootItem = new TreeItem("PHARMACY");
		

		HashMap menuWindowQueue = MapleclientApplication.mainFrameController.getMenuWindowQueue();

		ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("PHARMACY");

		List<MenuConfigMst> listMenuConfig = listMenuConficMstBody.getBody();

		MenuConfigMst aMenuConfigMst = listMenuConfig.get(0);

		String parentId = aMenuConfigMst.getId();

		ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
		List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

		for (MenuConfigMst aMenuConfig : menuConfigMstList) {

			// masterItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));

			if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
				rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
			}

		}

		// rootItem.getChildren().add(masterItem);

		treeview.setRoot(rootItem);

		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text 12345 : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();

				ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller
						.MenuConfigMstByMenuName(selectedText);

				List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();

				MenuConfigMst aMenuConfig = menuConfigMstList.get(0);

				Node dynamicWindow = (Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if (null == dynamicWindow) {
					if (null != selectedText) {
						try {
							dynamicWindow = FXMLLoader
									.load(getClass().getResource("/fxml/" + aMenuConfig.getMenuFxml()));
							menuWindowQueue.put(selectedText, dynamicWindow);

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}

				try {

					// Clear screen before loading the Page.
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

					MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
					MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
					MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
					MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					dynamicWindow.requestFocus();

				} catch (Exception e) {
					e.printStackTrace();
				}

				/*
				 * if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) { if
				 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
				 * MapleclientApplication.mainWorkArea.getChildren().clear();
				 * 
				 * MapleclientApplication.mainWorkArea.getChildren()
				 * .add(MapleclientApplication.mainFrameController.kotManagement);
				 * 
				 * }
				 * 
				 */

			}

		});
////		TreeItem rootItem = new TreeItem("Pharmacy");
//
//		TreeItem masterItem = new TreeItem("MASTERS");
//		masterItem.getChildren().add(new TreeItem("INSURANCE CREATION"));
//		masterItem.getChildren().add(new TreeItem("PATIENT CREATION"));
//		masterItem.getChildren().add(new TreeItem("CORPORATE SALES"));
//		masterItem.getChildren().add(new TreeItem("IMPORT PURCHASE"));
////		rootItem.getChildren().add(masterItem);
//		treeview.setRoot(masterItem);
//		
//		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
//				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
//				System.out.println("Selected Text : " + selectedItem.getValue());
//				selectedText = selectedItem.getValue();
//			}
//			});
	}

	@FXML
	void reprtclick(MouseEvent event) {

		if (null != selectedText) { // 1. Fetch command and fx from table based on the clicked item text.
			// 2. load the fx similar way we did in the main frame

			HashMap menuWindowQueue = MapleclientApplication.mainFrameController.getMenuWindowQueue();

			ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller
					.MenuConfigMstByMenuName(selectedText);

			List<MenuConfigMst> listMenuConfig = listMenuConficMstBody.getBody();

			MenuConfigMst aMenuConfigMst = listMenuConfig.get(0);

			ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller
					.MenuConfigMstByParentId(aMenuConfigMst.getId());
			List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

			for (MenuConfigMst aMenuConfig : menuConfigMstList) {

				Node dynamicWindow = (Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if (null == dynamicWindow) {
					if (null != aMenuConfig.getMenuFxml()) {
						try {
							dynamicWindow = FXMLLoader
									.load(getClass().getResource("/fxml/" + aMenuConfig.getMenuFxml()));
							menuWindowQueue.put(aMenuConfig.getMenuName(), dynamicWindow);

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}

				try {

					// Clear screen before loading the Page.
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

					/*
					 * MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
					 * MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
					 * MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
					 * MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 * dynamicWindow.requestFocus();
					 * 
					 */
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}
		/*
		 * if (null != selectedText) {
		 * 
		 * 
		 * HashMap menuWindowQueue=
		 * MapleclientApplication.mainFrameController.getMenuWindowQueue();
		 * 
		 * 
		 * 
		 * ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody =
		 * RestCaller.MenuConfigMstByMenuName(selectedText);
		 * 
		 * List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
		 * 
		 * MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
		 * 
		 * 
		 * ResponseEntity<MenuConfigMst> menuConficMst =
		 * RestCaller.MenuConfigMstById(aMenuConfigMst.getId()); MenuConfigMst
		 * menuConfigMst = menuConficMst.getBody();
		 * 
		 * 
		 * 
		 * Node dynamicWindow = (Node) menuWindowQueue.get(selectedText);
		 * if(null==dynamicWindow) { if(null != menuConfigMst.getMenuFxml()) { try {
		 * dynamicWindow =
		 * FXMLLoader.load(getClass().getResource("/fxml/"+menuConfigMst.getMenuFxml()))
		 * ; menuWindowQueue.put(menuConfigMst.getMenuName(), dynamicWindow);
		 * 
		 * } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * }
		 * 
		 * }
		 * 
		 * try {
		 * 
		 * // Clear screen before loading the Page. if
		 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
		 * MapleclientApplication.mainWorkArea.getChildren().clear();
		 * 
		 * MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
		 * 
		 * MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
		 * MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
		 * MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
		 * MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
		 * dynamicWindow.requestFocus();
		 * 
		 * 
		 * } catch (Exception e) { e.printStackTrace(); }
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * if (selectedText.equalsIgnoreCase("PATIENT CREATION")) { if
		 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
		 * MapleclientApplication.mainWorkArea.getChildren().clear();
		 * 
		 * MapleclientApplication.mainWorkArea.getChildren()
		 * .add(MapleclientApplication.mainFrameController.patientCreation);
		 * 
		 * }
		 * 
		 * if (selectedText.equalsIgnoreCase("INSURANCE CREATION")) { if
		 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
		 * MapleclientApplication.mainWorkArea.getChildren().clear();
		 * 
		 * MapleclientApplication.mainWorkArea.getChildren()
		 * .add(MapleclientApplication.mainFrameController.insuranceCompany);
		 * 
		 * } if (selectedText.equalsIgnoreCase("CORPORATE SALES")) { if
		 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
		 * MapleclientApplication.mainWorkArea.getChildren().clear();
		 * 
		 * MapleclientApplication.mainWorkArea.getChildren()
		 * .add(MapleclientApplication.mainFrameController.pharmacyCorporateSales);
		 * 
		 * }
		 * 
		 * 
		 * 
		 * if (selectedText.equalsIgnoreCase("IMPORT PURCHASE")) { if
		 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
		 * MapleclientApplication.mainWorkArea.getChildren().clear();
		 * 
		 * MapleclientApplication.mainWorkArea.getChildren()
		 * .add(MapleclientApplication.mainFrameController.importPurchase);
		 * 
		 * }
		 * }
		 */ 

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
