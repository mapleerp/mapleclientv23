package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ActualProductionDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.ProductionBatchStockDtl;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.ProductBatchStockEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ItemBatchWiseStockPopupCtl {
	String taskid;
	String processInstanceId;

	ProductBatchStockEvent productBatchStockEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	ProductionBatchStockDtl productionBatchStockDtl;
	ActualProductionDtl actualProductionDtl;
	private ObservableList<ProductionBatchStockDtl> popUpItemList = FXCollections.observableArrayList();
	private ObservableList<ProductionBatchStockDtl> popUpItemListsaved = FXCollections.observableArrayList();
	private ObservableList<KitDefinitionDtl> kitDtlSaved = FXCollections.observableArrayList();


    @FXML
    private TableView<ProductionBatchStockDtl> tblItemBatchMst;

    @FXML
    private TableColumn<ProductionBatchStockDtl, String> clItemName;

    @FXML
    private TableColumn<ProductionBatchStockDtl, String> clBatch;

    @FXML
    private TableColumn<ProductionBatchStockDtl, LocalDate> clExpiryDate;

    @FXML
    private TableColumn<ProductionBatchStockDtl, Number> clStock;

    @FXML
    private TableColumn<ProductionBatchStockDtl, Number> clAllocatedQty;

    
    @FXML
    private TableView<ProductionBatchStockDtl> tbProductionBatch;

    @FXML
    private TableColumn<ProductionBatchStockDtl, String> clProductItemName;

    @FXML
    private TableColumn<ProductionBatchStockDtl, String> clProductBatch;

    @FXML
    private TableColumn<ProductionBatchStockDtl, LocalDate> clProductExpiry;

    @FXML
    private TableColumn<ProductionBatchStockDtl, Number> clProductStock;

    @FXML
    private TableColumn<ProductionBatchStockDtl, Number> clProductQty;
    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtBatch;

    @FXML
    private TextField txtQty;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnOk;

	@FXML
	private void initialize() {

		productBatchStockEvent = new ProductBatchStockEvent();
		eventBus.register(this);
//		LoadItemPopupBySearch("");
		tblItemBatchMst.setItems(popUpItemList);
		clAllocatedQty.setCellValueFactory(cellData -> cellData.getValue().getallocatedQtyProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clStock.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());


		tblItemBatchMst.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getItemName() && newSelection.getItemName().length() > 0) {
					
					if (null != newSelection.getItemName()) {
						productBatchStockEvent.setItemName(newSelection.getItemName());
						txtItemName.setText(newSelection.getItemName());
					}
					if(null != newSelection.getAllocatedQty())
					{
						productBatchStockEvent.setAllocatedQty(newSelection.getAllocatedQty());
						
					}
					if(null != newSelection.getBatch())
					{
						productBatchStockEvent.setBatch(newSelection.getBatch());
						txtBatch.setText(newSelection.getBatch());

					}
					if(null != newSelection.getexpiryDate())
					{
						productBatchStockEvent.setExpiryDate(newSelection.getexpiryDate());
					}
					if(null != newSelection.getQty())
					{
						productBatchStockEvent.setQty(newSelection.getQty());

					}
				}
	
			}
		});
		
		tbProductionBatch.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				
				productionBatchStockDtl = new ProductionBatchStockDtl();
				productionBatchStockDtl.setId(newSelection.getId());
			}
			});
	}
	
	 @FXML
	    private Button btnDelete;

	    @FXML
	    void actionDelete(ActionEvent event) {
	    	if(null == productionBatchStockDtl)
	    	{
	    		return;
	    	}
	    	if(null == productionBatchStockDtl.getId())
	    	{
	    		return;
	    	}
	    	
	    	RestCaller.deleteProductionBatchStockDtl( productionBatchStockDtl.getId());
	    	
    		ResponseEntity<List<ProductionBatchStockDtl>> getProoduction = RestCaller.getProductionBatchStockDtlByActualProduction(actualProductionDtl.getId());
    		popUpItemListsaved = FXCollections.observableArrayList(getProoduction.getBody());
    		fillTable();
    		

	    }

	
    @FXML
    void addItem(ActionEvent event) {
    	
    	productionBatchStockDtl = new ProductionBatchStockDtl();
    	productionBatchStockDtl.setAllocatedQty(Double.parseDouble(txtQty.getText()));
    	productionBatchStockDtl.setBatch(txtBatch.getText());
    	if(null != productBatchStockEvent.getExpiryDate())
    	{
    	productionBatchStockDtl.setexpiryDate(productBatchStockEvent.getExpiryDate());
    	}
    	ResponseEntity<ItemMst> items = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	productionBatchStockDtl.setItemId(items.getBody().getId());
    	productionBatchStockDtl.setQty(productBatchStockEvent.getQty());
    	productionBatchStockDtl.setActualProductionDtl(actualProductionDtl);
    	ResponseEntity<List<ProductionDtlDtl>> getprodDtlDtl = RestCaller.getProductionDtlDtl(actualProductionDtl.getProductionDtl().getId());
    	for(ProductionDtlDtl prdDtlDtl:getprodDtlDtl.getBody())
			{
				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(prdDtlDtl.getItemName());
				if(productionBatchStockDtl.getItemId().equalsIgnoreCase(getItem.getBody().getId()))
				{
					if(productionBatchStockDtl.getAllocatedQty() > prdDtlDtl.getQty())
					{
						notifyMessage(5,"Required Qty is"+  prdDtlDtl.getQty());
						return;
					}
					Double totalAllocatedQty = RestCaller.getBatchProdTotalAllocatedQty(productionBatchStockDtl.getItemId(),actualProductionDtl.getId());

					if(totalAllocatedQty+productionBatchStockDtl.getAllocatedQty() > prdDtlDtl.getQty() )
					{
						notifyMessage(5,"Required Qty is"+  prdDtlDtl.getQty());
						return;
					}
				}
				
			}
		
    	ResponseEntity<ProductionBatchStockDtl> respentity = RestCaller.saveProductionBatchStockDtl(productionBatchStockDtl);
    	productionBatchStockDtl = respentity.getBody();
    	popUpItemListsaved.add(productionBatchStockDtl);
    	fillTable();

    	txtBatch.clear();
    	txtItemName.clear();
    	txtQty.clear();
    	
    }
    private void fillTable()
    {
    	for(ProductionBatchStockDtl prod : popUpItemListsaved)
		{
			ResponseEntity<ItemMst> itemsMst = RestCaller.getitemMst(prod.getItemId());
			prod.setItemName(itemsMst.getBody().getItemName());
			
		}
    	tbProductionBatch.setItems(popUpItemListsaved);
		clProductQty.setCellValueFactory(cellData -> cellData.getValue().getallocatedQtyProperty());
		clProductBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clProductExpiry.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clProductItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clProductStock.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());


    }
	public void LoadItemPopupBySearch(String searchData,ActualProductionDtl actualProductionDtl1) {
	
		actualProductionDtl = new ActualProductionDtl();
		actualProductionDtl = actualProductionDtl1;
		if(!searchData.equalsIgnoreCase(null))
		{
		ResponseEntity<ItemMst> items = RestCaller.getItemByNameRequestParam(searchData);
		ResponseEntity<List<ProductionBatchStockDtl>> getproductionBatch = RestCaller.getProductionBatchStockDtl(items.getBody().getId());
		
		popUpItemList = FXCollections.observableArrayList(getproductionBatch.getBody());
		for(ProductionBatchStockDtl prod : popUpItemList)
		{
			ResponseEntity<ItemMst> itemsMst = RestCaller.getitemMst(prod.getItemId());
			prod.setItemName(itemsMst.getBody().getItemName());
			
		}
		tblItemBatchMst.setItems(popUpItemList);
		ResponseEntity<List<ProductionBatchStockDtl>> getProoduction = RestCaller.getProductionBatchStockDtlByActualProduction(actualProductionDtl1.getId());
		popUpItemListsaved = FXCollections.observableArrayList(getProoduction.getBody());
		fillTable();
		}
	}
	 @FXML
	 void finalSubmit(ActionEvent event) {
		 Integer countOfBatch= RestCaller.getBatchItemStock(actualProductionDtl.getItemId());
 		
 		Integer countOfSavedBatch = RestCaller.getSavedBatchItemStock(actualProductionDtl.getId());
 		if(countOfBatch != countOfSavedBatch)
 		{
 			notifyMessage(5,"Set the batch Stock Details!!!");
				return;
			
 		}
		 productionBatchStockDtl = null;
		 actualProductionDtl = null;
		 popUpItemList.clear();
		 popUpItemListsaved.clear();
		 tblItemBatchMst.getItems().clear();
		 tbProductionBatch.getItems().clear();
		 
		 Stage stage = (Stage) btnAdd.getScene().getWindow();
			stage.close();
	 }
	 public void notifyMessage(int duration, String msg) {
			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();

			notificationBuilder.show();
		}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
}
