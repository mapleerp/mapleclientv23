package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ProductMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountEventCr;
import com.maple.mapleclient.events.AccountEventDr;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class AccountNewPopupCtl {
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	
	AccountPopupEvent accountEvent;
	AccountEventDr accountEventDr;
	AccountEventCr accountEventCr;
	public static String resultEventName="";
    @FXML
    private TextField searchBox;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
    @FXML
    private TableView<AccountHeads> tblAccountPupup;

    @FXML
    private TableColumn<AccountHeads, String> clAcoount;
    
    private ObservableList<AccountHeads> accountHeads = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	
    @FXML
	private void initialize() {
    	
    	searchBox.textProperty().bindBidirectional(SearchString);
    	accountEventDr = new AccountEventDr();
		accountEventCr = new AccountEventCr();
    	accountEvent = new AccountPopupEvent();
    	
    	LoadAccountPopupBySearch("");
    	tblAccountPupup.setItems(accountHeads);
    	
    	clAcoount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
    	

    	 tblAccountPupup.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			    if (newSelection != null) {
			    	if(null!=newSelection.getId()&& newSelection.getAccountName().length()>0) {
			    	
			    		accountEventDr.setAccountId(newSelection.getId());
						accountEventDr.setAccountName(newSelection.getAccountName());
						accountEventDr.setGroupOnly(newSelection.getGroupOnly());
						accountEventDr.setMachineId(newSelection.getMachineId());
						accountEventDr.setParentId(newSelection.getParentId());
						accountEventDr.setTaxId(newSelection.getTaxId());
						
						
						accountEventCr.setAccountId(newSelection.getId());
						accountEventCr.setAccountName(newSelection.getAccountName());
						accountEventCr.setGroupOnly(newSelection.getGroupOnly());
						accountEventCr.setMachineId(newSelection.getMachineId());
						accountEventCr.setParentId(newSelection.getParentId());
						accountEventCr.setTaxId(newSelection.getTaxId());
			    		
			    		
			    		
			    		accountEvent.setAccountId(newSelection.getId());
			    		accountEvent.setAccountName(newSelection.getAccountName());
			    		accountEvent.setGroupOnly(newSelection.getGroupOnly());
			    		accountEvent.setMachineId(newSelection.getMachineId());
			    		accountEvent.setParentId(newSelection.getParentId());
			    		accountEvent.setTaxId(newSelection.getTaxId());
						eventBus.post(accountEvent); 


			    	}
			    }
			});
    	 
    	 SearchString.addListener(new ChangeListener(){
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				 					
					LoadAccountPopupBySearch((String)newValue);
					
				}
	        });
    }


    @FXML
    void ActionCancel(ActionEvent event) {
    	
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
    void ActionOk(ActionEvent event) {
    	
    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	if (resultEventName.equalsIgnoreCase("DebitAccount")) {
			eventBus.post(accountEventDr);
		} else if (resultEventName.equalsIgnoreCase("CreditAccount")) {
			eventBus.post(accountEventCr);
		} else {
			eventBus.post(accountEvent);
		}
		
		
		resultEventName="";
		stage.close();
    }
    
    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblAccountPupup.requestFocus();
			tblAccountPupup.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
		}
    }
    
    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			if (resultEventName.equalsIgnoreCase("DebitAccount")) {
				eventBus.post(accountEventDr);
			} else if (resultEventName.equalsIgnoreCase("CreditAccount")) {
				eventBus.post(accountEventCr);
			} else {
				eventBus.post(accountEvent);
			}

			
			
			resultEventName="";
			
			stage.close();
		}  else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		} else {
			searchBox.requestFocus();
		}
    }
    
//	private void LoadAccountPopupBySearch(String searchData) {
//
//		/*
//		 * This method populate the instance variable popUpItemList , which the source
//		 * of data for the Table.
//		 */
//		ArrayList account = new ArrayList();
//		/*
//		 * Clear the Table before calling the Rest
//		 */
//
//		accountHeads.clear();
//		
//		account = RestCaller.SearchAccountByName(searchData);
//		Iterator itr = account.iterator();
//		System.out.println("accaccaccacc22");
//		while (itr.hasNext()) {
//			
//			 LinkedHashMap element = (LinkedHashMap) itr.next();
//			 System.out.println("accaccaccacc33"+element.get("id"));
//			 Object accountName = (String) element.get("accountName");
//			 Object id = (String) element.get("id");
//			 Object parentId = (String) element.get("parentId");
//			 Object groupOnly = (String) element.get("groupOnly");
//			 Object machineId = (String) element.get("machineId");
//			 Object taxId = (String) element.get("taxId");
//			
//			if (null != id) {
//				System.out.println("accaccaccacc44");
//				AccountHeads acc = new AccountHeads();
//				acc.setAccountName((String)accountName);
//				acc.setId((String)id);
//				acc.setGroupOnly((String)groupOnly);
//				acc.setMachineId((String)machineId);
//				acc.setParentId((String)parentId);
//				acc.setTaxId((String)taxId);
//				System.out.println("accaccaccacc"+acc);
//				accountHeads.add(acc);
//
//			}
//		}
//
//		return;
//
//	}
    
    
    
    private void LoadAccountPopupBySearch(String searchData) {
    	/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ArrayList account = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		accountHeads.clear();

		account = RestCaller.SearchAccountByName(searchData);
		String id;
		String accountName;
		Iterator itr = account.iterator();
		System.out.println("account Name search");
		while (itr.hasNext()) {
			
			List element = (List) itr.next();
			accountName = (String) element.get(1);
			  id = (String) element.get(0);
			 
				
				 Object parentId = (String) element.get(2);
				 Object groupOnly = (String) element.get(3);
				 Object machineId = (String) element.get(4);
				 Object taxId = (String) element.get(5);
			 if (null != id) {
					System.out.println("if id is not null");
					
					AccountHeads acc = new AccountHeads();
					acc.setAccountName((String)accountName);
					acc.setId((String)id);
					acc.setGroupOnly((String)groupOnly);
					acc.setMachineId((String)machineId);
					acc.setParentId((String)parentId);
					acc.setTaxId((String)taxId);
					System.out.println("account Name PopUp"+acc);
					accountHeads.add(acc);
					
					
					
					
		}
    }
		return;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}
	
}
