package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ActualProductionDtl;
import com.maple.mapleclient.entity.ProductionDtl;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ActualProductionReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import net.sf.jasperreports.engine.JRException;

public class ActualProductionReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<ActualProductionReport> productionDtlList = FXCollections.observableArrayList();
	private ObservableList<ProductionDtlDtl> productionDtlDtlList = FXCollections.observableArrayList();
	UnitMst unitmst = null;
	String itemName;
    @FXML
    private DatePicker dpToDate;
    @FXML
    private DatePicker dpFromDate;
    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrintRawMaterial;
    @FXML
    private Button btnPrint;
    
    @FXML
    private Button btnPrintItemwiseRawMaterial;
    
    @FXML
    private TableView<ActualProductionReport> tbActualProduction;

    @FXML
    private TableColumn<ActualProductionReport, LocalDate> clDate;

    @FXML
    private TableColumn<ActualProductionReport, String> clVoucher;

    @FXML
    private TableColumn<ActualProductionReport,String> clKit;

    @FXML
    private TableColumn<ActualProductionReport, String> clBatch;

    @FXML
    private TableColumn<ActualProductionReport, Number> clQty;

    @FXML
    private TableColumn<ActualProductionReport, String> clUnit;
    
    @FXML
    private TableView<ProductionDtlDtl> tbRawMaterial;

    @FXML
    private TableColumn<ProductionDtlDtl, String> clRawMaterial;

    @FXML
    private TableColumn<ProductionDtlDtl, Number> clRawmaterailQty;

    @FXML
    private TableColumn<ProductionDtlDtl, String> clrawMaterialUnit;
    
    
    @FXML
    void printItemWise(ActionEvent event) {
    	
    	try {
			JasperPdfReportService.ProductionRawMaterialReportItemWise(productionDtlDtlList,itemName);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    

    }
    
    @FXML
    void printReport(ActionEvent event) {
    	if (null != dpFromDate.getValue()) {
			Date ufromdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
			String fromdate = SystemSetting.UtilDateToString(ufromdate, "yyyy-MM-dd");
    	
    	if (null != dpToDate.getValue()) {
			Date utodate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String todate = SystemSetting.UtilDateToString(utodate, "yyyy-MM-dd");
			
	    	try {
				JasperPdfReportService.ActualProductionDtlReport(fromdate, todate, SystemSetting.systemBranch);

			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		}
    	}
    }
    
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
 }
    @FXML
    void PrintRawMaterial(ActionEvent event) {
    	java.util.Date uDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    	String fdate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
    	
    	java.util.Date uDate1 = SystemSetting.localToUtilDate(dpToDate.getValue());
    	String tdate = SystemSetting.UtilDateToString(uDate1, "yyyy-MM-dd");
    	
    	try {
			JasperPdfReportService.ProductionRawMaterialReportBetweenDate(fdate,tdate, SystemSetting.systemBranch);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
    }

    @FXML
    void showRawMaterial(MouseEvent event) {
    	if (tbActualProduction.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tbActualProduction.getSelectionModel();
			productionDtlList = selectionModel.getSelectedItems();
			for (ActualProductionReport prod : productionDtlList) {
				itemName = prod.getItemName();
				ResponseEntity<ActualProductionDtl> actualproductiondtlSaved = RestCaller.getActualProductionById(prod.getId());
			
				ResponseEntity<List<ProductionDtlDtl>> productiondtldtlSaved = RestCaller.getProductionDtlDtl(actualproductiondtlSaved.getBody().getProductionDtl().getId());
				productionDtlDtlList = FXCollections.observableArrayList(productiondtldtlSaved.getBody());
				for (ProductionDtlDtl proddtldtl : productionDtlDtlList)
				{
					try
					{

						BigDecimal bdQty = new BigDecimal(proddtldtl.getQty());
						bdQty = bdQty.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						proddtldtl.setQty(bdQty.doubleValue());
						
					ResponseEntity<UnitMst> respunit = RestCaller.getunitMst(proddtldtl.getUnitId());
					unitmst = respunit.getBody();
				} catch (Exception e) {
					return;
				}
				proddtldtl.setUnitName(unitmst.getUnitName());
				// productionDtlDtl.setUnitName(unitmst.getUnitName());
				// productionDtlDtlList.set(5,proddtldtl );
			}
			}
			tbRawMaterial.setItems(productionDtlDtlList);
			clRawmaterailQty.setCellValueFactory(cellData ->cellData.getValue().getqtyProperty());
	    	clRawMaterial.setCellValueFactory(cellData ->cellData.getValue().getitemNameProperty());
	    	clrawMaterialUnit.setCellValueFactory(cellData ->cellData.getValue().getunitNameProperty());
    	}
			
    }
    @FXML
    void showReport(ActionEvent event) {
    	tbActualProduction.getItems().clear();
      	if (null != dpFromDate.getValue()) {
    			Date ufromdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    			String fromdate = SystemSetting.UtilDateToString(ufromdate, "yyyy-MM-dd");
        	
        	if (null != dpToDate.getValue()) {
    			Date utodate = SystemSetting.localToUtilDate(dpToDate.getValue());
    			String todate = SystemSetting.UtilDateToString(utodate, "yyyy-MM-dd");
    			//productionDtlList.clear();
        	ResponseEntity<List<ActualProductionReport>> actualProductionReport = RestCaller.getActualProductioReportId(fromdate, todate,SystemSetting.systemBranch);
        	productionDtlList = FXCollections.observableArrayList(actualProductionReport.getBody());
        	fillTable();
        	}
        	}
    }
   
    private void fillTable()
    {
    	tbActualProduction.setItems(productionDtlList);
    	clBatch.setCellValueFactory(cellData ->cellData.getValue().getbatchProperty());
    	clDate.setCellValueFactory(cellData ->cellData.getValue().getvoucherDateProperty());
    	clKit.setCellValueFactory(cellData ->cellData.getValue().getitemNameProperty());
    	clQty.setCellValueFactory(cellData ->cellData.getValue().getactualQtyProperty());
    	clUnit.setCellValueFactory(cellData ->cellData.getValue().getunitNameProperty());
    	clVoucher.setCellValueFactory(cellData ->cellData.getValue().getvoucherNoProperty());
    }

    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


      private void PageReload() {
      	
      	
  		
  	}
}
