package com.maple.mapleclient.controllers;

import java.io.DataOutput;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesAnalysis;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SalesInvoiceReport;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class SalesAnalysisReportCtl {
	
	String taskid;
	String processInstanceId;

	
	private ObservableList<SalesInvoiceReport> salesAnalysisList = FXCollections.observableArrayList();

	List<String> salesDtlId = null;
//	StringProperty cardAmountLis = new SimpleStringProperty("");
//	StringProperty balanceAmountLis = new SimpleStringProperty("");

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenartion;

    @FXML
    private TableView<SalesInvoiceReport> tblSalesReport;

    @FXML
    private TableColumn<SalesInvoiceReport, String> clDate;

    @FXML
    private TableColumn<SalesInvoiceReport, String> clInvoiceNo;

    @FXML
    private TableColumn<SalesInvoiceReport, String> clItemName;

    @FXML
    private TableColumn<SalesInvoiceReport, Number> clQty;

    @FXML
    private TableColumn<SalesInvoiceReport, Number> clAmont;
    

    @FXML
    private TextField txtAmount;

    @FXML
    private TextField txtTotalAmount;

    @FXML
    private TextField txtBalance;
    
    @FXML
    private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblSalesReport.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	salesDtlId = new ArrayList<String>();
    }
    

    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(2, "Please Select from date", false);
    		dpFromDate.requestFocus();
    		return;
    	}
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(2, "Please Select to date", false);
    		dpToDate.requestFocus();
    		return;
    	}
    	
    	Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
    	
    	Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
    	String endDate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<SalesInvoiceReport>> salesReport = RestCaller.
    			getSalesAnalysisReport(startDate,endDate);
    	
    	salesAnalysisList = FXCollections.observableArrayList(salesReport.getBody());
    	
    	fillTable();

    }
    
    @FXML
    private Button btnCalculate;

    @FXML
    void Calculation(ActionEvent event) {
    	
    	Double amount = 0.0;
    	System.out.println("selectd items"+tblSalesReport.getSelectionModel().getSelectedItems());
    	int selesctedItemsNo = tblSalesReport.getSelectionModel().getSelectedItems().size();
    	for(int i=0; i<selesctedItemsNo; i++)
    	{
    		amount = amount + tblSalesReport.getSelectionModel().getSelectedItems().get(i).getAmount();
    		
    	}
    	
		BigDecimal bdAmount= new BigDecimal(amount);
		bdAmount = bdAmount.setScale(2, BigDecimal.ROUND_CEILING);
    	txtAmount.setText(bdAmount.toString());
    	
    	Double balance = Double.parseDouble(txtTotalAmount.getText())-amount;
    	BigDecimal bdBalance= new BigDecimal(balance);
    	bdBalance = bdBalance.setScale(2, BigDecimal.ROUND_CEILING);
    	txtBalance.setText(bdBalance.toString());
    }

    private void fillTable() {
		

    	tblSalesReport.setItems(salesAnalysisList);
    	Double total = 0.0;
    	for(SalesInvoiceReport sales : salesAnalysisList)
    	{

    		total = total+sales.getAmount();
    	}
    		
    		
		clAmont.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clInvoiceNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());



		
		BigDecimal bdCashToPay = new BigDecimal(total);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
    	txtTotalAmount.setText(bdCashToPay.toString());
    	

		
	}

	@FXML
    void SelectItems(MouseEvent event) {

    }
    

    @FXML
    private Button btnFinalSave;

    @FXML
    void FinalSave(ActionEvent event) {
    	
    	for(int i=0; i<tblSalesReport.getSelectionModel().getSelectedItems().size();i++)
    	{
    		SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
    		salesInvoiceReport = tblSalesReport.getSelectionModel().getSelectedItems().get(i);
    		
    		ResponseEntity<SalesDtl> salesDtlResp = RestCaller.getSalesDtlBydtlId(salesInvoiceReport.getId());
    		SalesDtl salesDtl = salesDtlResp.getBody();
    		if(null != salesDtl)
    		{
    			SalesAnalysis salesAnalysis = new SalesAnalysis();
        		salesAnalysis.setSalesDtl(salesDtl);
        		salesAnalysis.setBranchCode(SystemSetting.systemBranch);
        		
        		ResponseEntity<SalesAnalysis> salesAnalysisResp = RestCaller.saveSalesAnalysis(salesAnalysis);
    		}
  
    	}
    	
    	tblSalesReport.getItems().clear();
    	txtAmount.clear();
    	txtBalance.clear();
    	txtTotalAmount.clear();

    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }

}
