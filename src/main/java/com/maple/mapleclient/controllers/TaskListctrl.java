package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.Task;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.SupplierPaymentEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

@Component
public class TaskListctrl {
	
	String taskid;
	String processInstanceId;
	
	HashMap taskListWindows = new HashMap();
	TaskWindowDataEvent taskWindowDataEvent = null;
	private ObservableList<Task> userTaskList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();

	@FXML
	private Tab tabUserId;
	
	  @FXML
	    private ComboBox<String> cmbtaskassine;

	@FXML
	private TableView<Task> UserTaskListTable;

	@FXML
	private TableColumn<Task, String> TaskNameCol;

	@FXML
	private TableColumn<Task, String> CreatedDateCol;

	@FXML
	private TableColumn<Task, String> columnGroup;

	@FXML
	private TableColumn<Task, String> columnVoucherNumber;

	@FXML
	private TableColumn<Task, String> columnVoucherDate;

	@FXML
	private TableColumn<Task, String> columnSystemId;

	@FXML
	private TableColumn<Task, String> columnBusinessId;

	@FXML
	private TableColumn<Task, String> columnWindow;

	@FXML
	private TextField txtfld;

	@FXML
	private Button btns;

	@FXML
	void loadTask(ActionEvent event) {
		loadTask();

	}

	@FXML
	void onMouseClicked(MouseEvent event) {

	}

	@FXML
	private void initialize() {
		
	
		cmbtaskassine.getItems().add(SystemSetting.userId);
		cmbtaskassine.getItems().add(SystemSetting.systemBranch);
		
		TaskNameCol.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		CreatedDateCol.setCellValueFactory(cellData -> cellData.getValue().getDueDateProperty());
		columnGroup.setCellValueFactory(cellData -> cellData.getValue().getCandidateGroupProperty());
		columnVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		columnVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		columnSystemId.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
		columnBusinessId.setCellValueFactory(cellData -> cellData.getValue().getBusinessIdProperty());
		columnWindow.setCellValueFactory(cellData -> cellData.getValue().getWindowNameProperty());

		UserTaskListTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			/*if (newSelection != null) {

				if (null != newSelection.getName() && newSelection.getName().length() > 0) {
					if (newSelection.getName().equalsIgnoreCase("Supplier Payment")) {
						// Generate Payment Task
						SupplierPaymentEvent supplierPaymentEvent = new SupplierPaymentEvent();
						supplierPaymentEvent.setPurchaseHdrId(newSelection.getBusinessId());
						ResponseEntity<PurchaseHdr> purchaseHdr = RestCaller
								.getPurchaseHdr(newSelection.getBusinessId());
						String supplierId = purchaseHdr.getBody().getSupplierId();
						supplierPaymentEvent.setSupplierId(supplierId);
						supplierPaymentEvent.setTakId(newSelection.getId());
						System.out.println("WINDOW NAME" + newSelection.getWindowName());
						eventBus.post(supplierPaymentEvent);
						System.out.println("Ents Bus Posted  :  " + supplierPaymentEvent);
					}

				}

//			    	if(null != newSelection.getId() && null != newSelection.getWindowName())
//			    	{
//			    		showWindow(newSelection.getWindowName());
//			    	}
			}*/
			

			if (null != newSelection.getName() && null!=newSelection.getWindowName() && newSelection.getWindowName().length() > 0) {
				try {

					Node dynamicWindow = (Node) taskListWindows.get(newSelection.getWindowName());
					if (null == dynamicWindow) {

						dynamicWindow = FXMLLoader
								.load(getClass().getResource("/fxml/" + newSelection.getWindowName()));
						taskListWindows.put(newSelection.getWindowName(), dynamicWindow);

						// task newSelection.getId()
					}

					try {

						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();

						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

						MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);

					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				catch (Exception e) {
					System.out.println(e.toString());
				}

			}
			System.out.println("TASK =================" + newSelection.getWindowName());
			taskWindowDataEvent = new TaskWindowDataEvent();
			taskWindowDataEvent.setId(newSelection.getId());
			taskWindowDataEvent.setVoucherNumber(newSelection.getVoucherNumber());
			taskWindowDataEvent.setVoucherDate(newSelection.getVoucherDate());
			taskWindowDataEvent.setEntityId(newSelection.getBusinessId());
			taskWindowDataEvent.setAccountId(newSelection.getAccountId());
			taskWindowDataEvent.setProcessInstanceId(newSelection.getProcessInstanceId());
			taskWindowDataEvent.setBusinessProcessId(newSelection.getBusinessId());
			
			
			eventBus.post(taskWindowDataEvent);
			// Stage stage = (Stage) btns.getScene().getWindow();
			// stage.close();
			/*
			 * POST Parameters into eventBus. All the parameters to be embedded in one
			 * class. eg:- taskid, windowname, workdlowid etc.
			 * 
			 * On TaskActionWindow, subscribe to TaskParameterEvent. If the Window name
			 * matches, process task else ignore it
			 * 
			 */

		});

	}

//	private void showWindow(String windowName) {
//
//		if(windowName.equalsIgnoreCase( "RECEIPT EDIT")) {
//	    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//	    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//	    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.receiptEdit);
//	    
//	    }
//	}

	private void loadTask() {

		try {
			userTaskList.clear();
		} catch (Exception e) {
			System.out.println("Error " + e.toString());
		}

		
		//UserTaskListTable
		//ResponseEntity<ArrayList> taksList = RestCaller.getTaskByGroup(SystemSetting.getUser().getBranchCode());
		
		ResponseEntity<ArrayList> taksList = RestCaller.getTaskByGroup(
				SystemSetting.systemBranch);
		
		
		Iterator itr = taksList.getBody().iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			// Iterator itr2 = lm.keySet().iterator();
			// while(itr2.hasNext()) {

			Object taskName = lm.get("name");
			Object created = lm.get("created");
			Object due = lm.get("due");
			Object assignee = lm.get("assignee");
			Object taskId = lm.get("id");
			Object processInstanceId = lm.get("processInstanceId");

			if (null != taskName) {
				Task task = new Task();

				if (null != assignee)
					task.setAssignee((String) assignee);

				if (null != taskName)
					task.setName((String) taskName);

				if (null != created) {
					String createdDatestr = (String) created;
					task.setCreatedOn(createdDatestr);
				}
				task.setId((String) taskId);

				LinkedHashMap response = (LinkedHashMap) RestCaller.getTaskVariables((String) taskId);

				LinkedHashMap vnumber = (LinkedHashMap) response.get("voucherNumber");
				String voucherNumber = (String) vnumber.get("value");

				task.setVoucherNumber(voucherNumber);

				LinkedHashMap vdate = (LinkedHashMap) response.get("voucherDate");
				String voucheDate = (String) vdate.get("value");
				task.setVoucherDate(voucheDate);
				try {
//				LinkedHashMap businessId = (LinkedHashMap) response.get("businessProcessId");
					LinkedHashMap businessId = (LinkedHashMap) response.get("id");

				task.setBusinessId((String) businessId.get("value"));
				System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@");

				System.out.println(task.getBusinessId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					LinkedHashMap accountId = (LinkedHashMap) response.get("accountId");
					task.setAccountId((String) accountId.get("value"));
				} catch (Exception e) {
					e.printStackTrace();
				}

				task.setProcessInstanceId((String)processInstanceId);
				 
				
				//processInstanceId
				// Window name is found from the Task Name ,\

				/*
				 * try { LinkedHashMap window = (LinkedHashMap) response.get("window"); String
				 * windowName = (String) window.get("value"); task.setWindowName(windowName); }
				 * catch (Exception e) {
				 * 
				 * }
				 */

				// taskName
				/*
				 * 1. Find Fxml Name from task name 2. id is same as hdr id. Load window with id
				 */

				ResponseEntity<List<MenuConfigMst>> menuTaskListResp = RestCaller
						.MenuConfigMstByMenuName((String) taskName);
				List<MenuConfigMst> menuTaskList = menuTaskListResp.getBody();

				if (menuTaskList.size() > 0) {
					String FxName = menuTaskList.get(0).getMenuFxml();

					task.setWindowName(FxName);
				}

				userTaskList.add(task);

			}

			// UserTaskListTable.setItems(userTaskList);
			// call supplier rest

			FilteredList<Task> filteredData = new FilteredList<Task>(userTaskList, s -> true);

			txtfld.textProperty().addListener(obs -> {
				String filter = txtfld.getText();
				if (filter == null || filter.length() == 0) {
					filteredData.setPredicate(s -> true);
				} else {
					filteredData.setPredicate(s -> s.getName().toUpperCase().contains(filter.toUpperCase()));
				}
			});
			UserTaskListTable.setItems(filteredData);

		}

	}
	
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}
}

/*
 * 3/4 x .5 5 FT Roll 3 roll
 */
