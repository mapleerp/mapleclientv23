package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.SupplierPriceMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

@RestController
public class SupplierPriceMstCtl {
	
	String taskid;
	String processInstanceId;

	
	private ObservableList<SupplierPriceMst> supplierPriceTable = FXCollections.observableArrayList();
	

	SupplierPriceMst supplierPriceMst;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	 @FXML
	    private TableView<SupplierPriceMst> tblSupplierPrice;

	     String itemId;
	     String supplierId;
	    @FXML
	    private TableColumn<SupplierPriceMst, String> clSupplierName;

	    @FXML
	    private TableColumn<SupplierPriceMst, String> clItemName;

	    @FXML
	    private TableColumn<SupplierPriceMst, Number> clDP;

	    @FXML
	    private TableColumn<SupplierPriceMst, String> clUpatedDate;
	    @FXML
	    private TableColumn<SupplierPriceMst, String> id;


	    @FXML
	    private Button btnAdd;

	    @FXML
	    private Button btnShowAll;

	    @FXML
	    private Button btnDelete;

	    @FXML
	    private TextField txtsupplierid;

	    @FXML
	    private TextField txtItemId;
	    @FXML
	    private TextField txtdp;

	    @FXML
	    private DatePicker dpUpdatedDate;

	    @FXML
	    void AddItem(ActionEvent event) {
	    	
	    	save();
	    }

	    @FXML
	    void DeleteItem(ActionEvent event) {

	         
	    	
	           if(null != supplierPriceMst)
	           {
	    	
	    		if(null != supplierPriceMst.getId())
	    		{
	    			RestCaller.deleteSupplierPriceMst(supplierPriceMst.getId());
	    			
	    			showAll();
	    			notifyMessage(5, " Item deleted", false);
					return;
	    		}
	           }
	    	

	    }

	    @FXML
	    void ShowAllItems(ActionEvent event) {
	    	
	    	showAll();
	    }

	    
// supplier popup window
	    @FXML
	    void onActionSupplier(ActionEvent event) {
	    	System.out.println("-------------showPopup-------------");
    		try {
    			System.out.println("inside the popup");
    			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/supplierPopup.fxml"));
    			Parent root = loader.load();
    			Stage stage = new Stage();
    			stage.setScene(new Scene(root));
    			stage.initModality(Modality.APPLICATION_MODAL);
    			stage.show();
    		} catch (Exception e) {
    			e.printStackTrace();
    		
	    	

	    	}
	    }
	    

		

	    @FXML
	    void saveOnKey(KeyEvent event) {
	    	
	    }
	    
	    
			public void showAll() {

				ResponseEntity<List<SupplierPriceMst>> respentity = RestCaller.getSupplierPrice();
	            supplierPriceTable = FXCollections.observableArrayList(respentity.getBody());
	            
	            System.out.print(supplierPriceTable.size()+"observable list size issssssssssssssssssssss");
	            fillTable();
				
			}
			   @FXML
			    void onAction(ActionEvent event) {

					try {
						System.out.println("STOCK POPUP");
						FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
					
						Parent root = loader.load();
						Stage stage = new Stage();
						stage.setScene(new Scene(root));
				
						stage.show();


					} catch (Exception e) {
						e.printStackTrace();
					}
				
			    }
			
		@FXML
		private void initialize() {
			
			dpUpdatedDate = SystemSetting.datePickerFormat(dpUpdatedDate, "dd/MMM/yyyy");
			eventBus.register(this);

			tblSupplierPrice.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {

						supplierPriceMst = new SupplierPriceMst();
						supplierPriceMst.setId(newSelection.getId());
					}
				}
			});
		}
			
		public void save() {

		SupplierPriceMst	supplierPriceMst=new SupplierPriceMst();
			if (null == txtsupplierid.getText()) {
				notifyMessage(5, "Please select Supplier name", false);
				return;
			}
			
			if(txtItemId.getText().trim().isEmpty())
			{
				notifyMessage(5, "Please select Item name", false);
				return;
			}
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemId.getText());
			ItemMst itemMst = getItem.getBody();
			if (null == itemMst) {
				notifyMessage(5, "Item not found", false);
				return;
			}
			supplierPriceMst.setItemId(itemMst.getId());
		
			supplierPriceMst.setSupplierId(supplierId);
			
			supplierPriceMst.setdP(Double.valueOf(txtdp.getText()));
			
			Date dpDate=SystemSetting.localToUtilDate(dpUpdatedDate.getValue());
			supplierPriceMst.setUpdatedDate(dpDate);
	

			ResponseEntity<SupplierPriceMst> respEntity = RestCaller.createSupplierPriceMst(supplierPriceMst);
			
			
			{
				notifyMessage(5, "Saved ", true);
				txtdp.clear();
				txtItemId.clear();
				txtsupplierid.clear();
				dpUpdatedDate.setValue(null);
				
				showAll();
				return;
			}
			
	
		}

		public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}

		
		
		@Subscribe
		public void popupOrglistner(ItemPopupEvent itemPopupEvent) {

			Stage stage = (Stage) btnAdd.getScene().getWindow();
			if (stage.isShowing()) {

				
				
				Platform.runLater(() -> {
					txtItemId.setText(itemPopupEvent.getItemName());
					itemId=itemPopupEvent.getItemId();
	            });

			}
		}
		
		
		@Subscribe
		public void popuplistner(SupplierPopupEvent supplierEvent) {

			System.out.println("-------------popuplistner-------------");
			Stage stage = (Stage) btnAdd.getScene().getWindow();
			if (stage.isShowing()) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
				
				
						Platform.runLater(() -> {
							txtsupplierid.setText(supplierEvent.getSupplierName()); 
							supplierId=supplierEvent.getSupplierId();
			            });
			
				
					}
				});
				
				
			}
		}
		
		

	private void fillTable() {
		
		  tblSupplierPrice.setItems(supplierPriceTable);
		  
		
		  clSupplierName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
		  clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());


		  clUpatedDate.setCellValueFactory(cellData ->
		  cellData.getValue().getUpdatedDateProperty());
		  
		  clDP.setCellValueFactory(cellData ->
		  cellData.getValue().getDpProperty());
		  id.setCellValueFactory(cellData ->
		  cellData.getValue().getIdProperty());
		 }
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}
}
