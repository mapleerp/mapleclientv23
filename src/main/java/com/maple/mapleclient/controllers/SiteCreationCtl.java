package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.OrgMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class SiteCreationCtl
{
	String taskid;
	String processInstanceId;
	
	/* Site details is saved to LocalCustomerMst */
	private ObservableList<LocalCustomerMst> localCustomerMstList = FXCollections.observableArrayList();

	EventBus eventBus = EventBusFactory.getEventBus();
	String custId = "";
	LocalCustomerMst localCustomerMst;
    @FXML
    private Button save;
	    @FXML
	    private Button btnGenerateReport;

	    @FXML
	    private TextField txtsitename;

	    @FXML
	    private TextField txtsiteaddress1;

	    @FXML
	    private TextField txtsiteaddress2;

	    @FXML
	    private TextField txtlandmark;

	    @FXML
	    private TextField txtmobilenumber;
	    @FXML
	    private Button Delete;

	    @FXML
	    private Button showAll;
	    
	    
	    @FXML
	    private TextField txtcustomername;
	    @FXML
	    private TableView<LocalCustomerMst> siteTable;

	    @FXML
	    private TableColumn<LocalCustomerMst,String> custName;

	    @FXML
	    private TableColumn<LocalCustomerMst, String> siteName;

	    @FXML
	    private TableColumn<LocalCustomerMst, String> address;

	    @FXML
	    private TableColumn<LocalCustomerMst, String> lanmark;

	    @FXML
	    private TableColumn<LocalCustomerMst, String> mobileno;

	    @FXML
	    private TextField txtLocalCustName;
	    
	    @FXML
	    void onEnterLocalCustName(KeyEvent event) {

	    	if (event.getCode() == KeyCode.ENTER) {
	    		txtsitename.requestFocus();
			}
	    }



	    @FXML
	    void onEnterCustName(KeyEvent event) {
	    	
	    	loadCustomerPopup();
	    	//showLocalCustPopup();
	   
	    	
	    }
	    @FXML
	    void onClickToAddress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		txtsiteaddress1.requestFocus();
			}
	    	
	    }

	    @FXML
	    void onClickToAddress2(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		txtsiteaddress2.requestFocus();
			}
	    	
	    }

	    @FXML
	    void onClickToLandmark(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		txtlandmark.requestFocus();
			}
	    
	    }

	    @FXML
	    void onClickTomobile(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		txtmobilenumber.requestFocus();
			}
	    
	    	//txtmobilenumber.requestFocus();
	    }
	    
	    @FXML
		private void initialize() {
	    	eventBus.register(this);
	    	siteTable.getSelectionModel().selectedItemProperty().addListener((obs,
	    			  oldSelection, newSelection) -> { if (newSelection != null) {
	    	
	    			  if (null != newSelection.getId())
	    			  {
	    			  
	    				  localCustomerMst  = new LocalCustomerMst(); 
	    				  localCustomerMst.setId(newSelection.getId()); } }
	    			  });
	    }
	    
	    @FXML
	    void onClickToSave(KeyEvent event) {
	    	
	    	if (event.getCode() == KeyCode.ENTER) {
	    		save.requestFocus();
	    		Save();
			}
	    
	    	//save.requestFocus();
	    }
	   
	    @FXML
	    void OnClickSave(ActionEvent event) {

	    	Save();
	    	
	    }
	    
	    @FXML
	    void OnClickShowall(ActionEvent event) {
	    	
	    	  showAll();
	    	
	    }
	    private void showAll()
	    {
	    	ResponseEntity<List<LocalCustomerMst>> orgMstResp = RestCaller.getAllLocalCustomer();
	    	localCustomerMstList = FXCollections.observableArrayList(orgMstResp.getBody());
	    	siteTable.getItems().clear();
			FillTable();
	    }

	    @FXML
	    void OnClickDelete(ActionEvent event) {

			  if(null != localCustomerMst) {
			if(null != localCustomerMst.getId()) {
//			  RestCaller.deleteLocalCustomerMst(localCustomerMst.getId());
			  notifyMessage(5,"Deleted!!!");
			  
			  siteTable.getItems().clear();
			  showAll();
			  
			  } }
	    	
	    }
	    
	    private void Save()
	    {
	    if(txtcustomername.getText().trim().isEmpty())
	    {
	    		notifyMessage(5, "please select customer");
	    		txtcustomername.requestFocus();
	    		return;
	    }
	    
	    if(txtLocalCustName.getText().trim().isEmpty())
	    {
	    		notifyMessage(5, "please enter customer name");
	    		txtLocalCustName.requestFocus();
	    		return;
	    }else if(txtsitename.getText().trim().isEmpty())
    	{
			notifyMessage(5, "please enter siteaddress");
			txtsitename.requestFocus();
			return;
    	}else if(txtsiteaddress1.getText().trim().isEmpty())
	    {
				notifyMessage(5, "please enter siteaddress");
				txtsiteaddress1.requestFocus();
				return;
	    }else if(txtsiteaddress2.getText().trim().isEmpty())
	    {
				notifyMessage(5, "please enter siteaddress");
				txtsiteaddress2.requestFocus();
				return;
	    }else if(txtlandmark.getText().trim().isEmpty())
	    {
		      notifyMessage(5, "please enter landmark");
		      txtlandmark.requestFocus();
		  	return;
	    }else if(txtmobilenumber.getText().trim().isEmpty())
	    {
			notifyMessage(5, "please enter mobilenumber");
			txtmobilenumber.requestFocus();
			return;
	    }
		else
		{
				
	    	LocalCustomerMst localCustomerMst=new LocalCustomerMst();
	    	
	    	String customerName = txtcustomername.getText();
	    	
	    	localCustomerMst.setLocalcustomerName(txtLocalCustName.getText());
	    	
	    	
	    	
	    	ResponseEntity<AccountHeads> customerMsts = RestCaller.getAccountHeadsById(custId);
	    	if(null == customerMsts.getBody())
	    	{
	    		notifyMessage(5, "Invalid Customer");
				return;
	    	}
	    	
	    	String customerId = customerMsts.getBody().getId();
	    	
	    	localCustomerMst.setCustomerId(customerId);
	    	
	    	String Address = txtsitename.getText() ;
	    	
	    	localCustomerMst.setAddress(Address);
	    	
	    	String  Address1 = txtsiteaddress1.getText() ;
	    	
	    	localCustomerMst.setAddressLine1(Address1);
	    	
	    	String Address2 = txtsiteaddress2.getText() ;
	    	
	    	localCustomerMst.setAddressLine2(Address2);
	    	
	    	String  LandMark = txtlandmark.getText() ;
	    	
	    	localCustomerMst.setLandMark(LandMark);
	    	
	    	String MobileNumber = txtmobilenumber.getText()  ;
	    	
	    	localCustomerMst.setPhoneNo(MobileNumber);
	    	
	    	
	    	ResponseEntity<LocalCustomerMst> respentity = RestCaller.saveLocalCustomer(localCustomerMst);
	    	localCustomerMst = respentity.getBody();
	    	notifyMessage(5, "Save Successfully Completed");
	    	localCustomerMstList.add(localCustomerMst);
	    	FillTable();
	    	txtcustomername.setText("");
	    	txtsitename.setText("");
	    	txtsiteaddress1.setText("");
	    	txtsiteaddress2.setText("");
	    	txtlandmark.setText("");
	    	txtmobilenumber.setText("");
	    	txtLocalCustName.clear();
			}
	    }
	    public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}
	    
	    private void loadCustomerPopup() {
			/*
			 * Function to display popup window and show list of suppliers to select.
			 */
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				txtLocalCustName.requestFocus();
			

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	    
	    private void showLocalCustPopup() {
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/LocalCustPopup.fxml"));
				// fxmlLoader.setController(itemStockPopupCtl);
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    private void FillTable() {

	    	siteTable.setItems(localCustomerMstList);
	    	custName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
	    	siteName.setCellValueFactory(cellData -> cellData.getValue().getAddressProperty());
	    	address.setCellValueFactory(cellData -> cellData.getValue().getAddressLine1Property());
	    	lanmark.setCellValueFactory(cellData -> cellData.getValue().getLandMarkProperty());
	    	mobileno.setCellValueFactory(cellData -> cellData.getValue().getPhoneNoProperty());
			
		}
	    
	    @Subscribe
		public void popupLocalCustomerlistner(LocalCustomerEvent localCustomerEvent) {

			Stage stage = (Stage) save.getScene().getWindow();
			if (stage.isShowing()) {

				txtcustomername.setText(localCustomerEvent.getLocalCustName());
				custId = localCustomerEvent.getLocalCustid();
				System.out.println("custIdcustIdcustId" + custId);
			}
	    }
		@Subscribe
		public void popupCustomerlistner(CustomerEvent customerEvent) {

			Stage stage = (Stage) save.getScene().getWindow();
			if (stage.isShowing()) {

				txtcustomername.setText(customerEvent.getCustomerName());
				custId = customerEvent.getCustId();
			
			}

		}
		 @Subscribe
		   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		   		//Stage stage = (Stage) btnClear.getScene().getWindow();
		   		//if (stage.isShowing()) {
		   			taskid = taskWindowDataEvent.getId();
		   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
		   			
		   		 
		   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
		   			System.out.println("Business Process ID = " + hdrId);
		   			
		   			 PageReload(hdrId);
		   		}


		   	private void PageReload(String hdrId) {

		   	}


	}


