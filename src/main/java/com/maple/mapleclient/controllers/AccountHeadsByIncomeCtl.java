package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class AccountHeadsByIncomeCtl {
	String taskid;
	String processInstanceId;
	
	
	private ObservableList<AccountHeads> accountHeadsList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();




	private EventBus eventBus = EventBusFactory.getEventBus();
	AccountEvent accountEvent;
	public static String resultEventName = "";
	@FXML
	private TextField searchBox;

	@FXML
	private Button btnOk;

	@FXML
	private Button btnCancel;
	@FXML
	private TableView<AccountHeads> tblAccountPupup;

	@FXML
	private TableColumn<AccountHeads, String> clAcoount;


	@FXML
	private void initialize() {
		
		eventBus.register(this);
		accountEvent = new AccountEvent();


		searchBox.textProperty().bindBidirectional(SearchString);

    	LoadAccountPopupBySearch("");

		tblAccountPupup.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId() && newSelection.getAccountName().length() > 0) {
					
					accountEvent.setAccountId(newSelection.getId());
					accountEvent.setAccountName(newSelection.getAccountName());
					accountEvent.setGroupOnly(newSelection.getGroupOnly());
					accountEvent.setMachineId(newSelection.getMachineId());
					accountEvent.setParentId(newSelection.getParentId());
					accountEvent.setTaxId(newSelection.getTaxId());
//			    		if(resultEventName.equalsIgnoreCase("DebitAccount"))
//			    		{
//			    		eventBus.post(accountEventDr); 
//			    		}
//			    		else if(resultEventName.equalsIgnoreCase("CreditAccount"))
//			    		{
//			    			eventBus.post(accountEventCr); 
//			    		}
//			    		else
//			    		{
//			    			eventBus.post(accountEvent); 
//			    		}
//			    		
//			    		resultEventName="";
				}
			}
		});

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadAccountPopupBySearch((String) newValue);

			}
		});
	}

	@FXML
	void ActionCancel(ActionEvent event) {

		Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
	}

	@FXML
	void ActionOk(ActionEvent event) {

		Stage stage = (Stage) btnOk.getScene().getWindow();

			eventBus.post(accountEvent);

		stage.close();
	}

	@FXML
	void OnKeyPressTxt(KeyEvent event) {
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblAccountPupup.requestFocus();
			tblAccountPupup.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
		}
	}

	@FXML
	void OnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnOk.getScene().getWindow();

			
				eventBus.post(accountEvent);

			resultEventName = "";

			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP
				|| event.getCode() == KeyCode.KP_UP) {

		} else {
			searchBox.requestFocus();
		}
	}

	private void LoadAccountPopupBySearch(String searchData) {
		
		
		System.out.println("INCOME");

		
		
		ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadByName("INCOME");
		AccountHeads accountHeads = accountHeadsResp.getBody();

		
		accountHeadsList.clear();
		tblAccountPupup.getItems().clear();


		if (null == accountHeads) {
			return;
		}

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ResponseEntity<List<AccountHeads>> account = RestCaller.SearchAccountByNameByParentId(accountHeads.getId(),
				searchData);
		/*
		 * Clear the Table before calling the Rest
		 */

		accountHeadsList = FXCollections.observableArrayList(account.getBody());
		System.out.println(accountHeadsList);

		tblAccountPupup.setItems(accountHeadsList);

		fillTable();

	}

	private void fillTable() {

		clAcoount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}


}
