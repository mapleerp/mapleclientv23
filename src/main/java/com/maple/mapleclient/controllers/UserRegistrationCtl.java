package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.GroupMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.entity.ProcessPermissionMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class UserRegistrationCtl {
	
	String taskid;
	String processInstanceId;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ProcessPermissionMst> processList = FXCollections.observableArrayList();

	private ObservableList< UserMst> userRegistrationList = FXCollections.observableArrayList();
	private ObservableList< BranchMst> branchList = FXCollections.observableArrayList();
	UserMst userregistration = null;
    @FXML
    private TextField usrName;

    @FXML
    private TextField usrfullname;
    
    @FXML
    private Button btnPOSOperator;

    @FXML
    private ComboBox<String> cmbUserPermisssion;
    @FXML
    private Button btnposAdmin;
    @FXML
    private Button btnRefresh;

    @FXML
    private ComboBox<String> usrbranch;
    @FXML
    private PasswordField txtpassword;

    @FXML
    private PasswordField txtpassword1;

    @FXML
    private Button usrsubmit;

    @FXML
    private Button btnAddPermission;
    @FXML
    private Button btnsearch;

    @FXML
    private TableView<UserMst> usrtbl;
 
    @FXML
    private TableColumn<UserMst,String> us2;

    @FXML
    private TableColumn<UserMst,String> us3;

    @FXML
    private TableColumn<UserMst,String> us4;

    @FXML
    private Button btnShowAll;
    @FXML
	private void initialize() {
    	
    	eventBus.register(this);
    	userregistration = new UserMst();
    	BranchMst newBranchMst = new BranchMst();
    	showBranchHeadGroup();
		 ResponseEntity<List<BranchMst>> accountSaved =	 RestCaller.getBranchDtl(); 
		 branchList =FXCollections.observableArrayList(accountSaved.getBody());
		 cmbUserPermisssion.getItems().add("CHURCH OPERATOR");
		 cmbUserPermisssion.getItems().add("POS ADMIN");
		 cmbUserPermisssion.getItems().add("POS OPERATOR");
		 cmbUserPermisssion.getItems().add("PHARMACY OPERATOR");
		 cmbUserPermisssion.getItems().add("ACCOUNT OPERATOR");
	
		 btnposAdmin.setVisible(false);
		 btnPOSOperator.setVisible(false);
		 btnAddPermission.setVisible(true);
		  filltable();
			usrtbl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					System.out.println("getSelectionModel");
					if (null != newSelection.getId()) {

					 usrName.setText(newSelection.getUserName());
					 usrfullname.setText(newSelection.getFullName());
					 usrbranch.getSelectionModel().select(newSelection.getBranchCode());
					 txtpassword.setText(newSelection.getPassword());
					 txtpassword1.setText(newSelection.getPassword1());
					}
				}
			});
    	
    }
    private void showBranchHeadGroup()
    {
    	ArrayList cat = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		cat = RestCaller.SearchBranchs();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object unitName = lm.get("branchCode");
			Object unitId = lm.get("id");
			if (unitId != null) {
				usrbranch.getItems().add((String) unitName);
				BranchMst newUnitMst = new BranchMst();
				
				newUnitMst.setId((String) unitId);
				branchList.add(newUnitMst);
			}
		}

		}
    @FXML
    void PosOperatorRoles(ActionEvent event) {
    	
    	ProcessPermissionMst 	processPermissionMst = new ProcessPermissionMst();
    	if(usrName.getText().trim().length()==0)
    	{
    		notifyMessage(5,"Please Select User");
    		return;
    	}
    	ResponseEntity<UserMst> respentity = RestCaller.getUserByName(usrName.getText());
    	userregistration = respentity.getBody();
    	if(null == userregistration)
    	{
    		notifyMessage(5,"Please Add the User");
    		return;
    	}
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<List<ProcessPermissionMst>> savedprocessPermissionMst = RestCaller.getProcessPermissionbyUserId(userregistration.getId());
    	processList = FXCollections.observableArrayList(savedprocessPermissionMst.getBody());
    	for(ProcessPermissionMst process:processList)
    	{
    		if(process.getUserId().equalsIgnoreCase(userregistration.getId()))
    		{
    			notifyMessage(5,"Already Done");
    			return;
    		}
    	}
    	ResponseEntity<ProcessMst>processSaved = RestCaller.getProcessDtls("POS WINDOW");
    	
    	processPermissionMst.setProcessId(processSaved.getBody().getId());
    	
    	String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNo);
    	ResponseEntity<ProcessPermissionMst> respentity1 = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedKot = RestCaller.getProcessDtls("KOT POS");
    	processPermissionMst.setProcessId(processSavedKot.getBody().getId());
    	String vNokot = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNokot);
    	ResponseEntity<ProcessPermissionMst> respentitykot = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedWHOLESALE = RestCaller.getProcessDtls("WHOLESALE");
    	processPermissionMst.setProcessId(processSavedWHOLESALE.getBody().getId());
    	String vNoWHOLESALE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoWHOLESALE);
    	ResponseEntity<ProcessPermissionMst> respentityWHOLESALE = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedONLINESALES = RestCaller.getProcessDtls("ONLINE SALES");
    	processPermissionMst.setProcessId(processSavedONLINESALES.getBody().getId());
    	String vNoONLINESALES = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoONLINESALES);
    	ResponseEntity<ProcessPermissionMst> respentityONLINESALES = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedSALEORDER = RestCaller.getProcessDtls("SALEORDER");
    	processPermissionMst.setProcessId(processSavedSALEORDER.getBody().getId());
    	String vNoSALEORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALEORDER);
    	ResponseEntity<ProcessPermissionMst> respentitySALEORDER = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedDAYEND = RestCaller.getProcessDtls("DAY END CLOSURE");
    	processPermissionMst.setProcessId(processSavedDAYEND.getBody().getId());
    	String vNoDAYEND = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoDAYEND);
    	ResponseEntity<ProcessPermissionMst> respentityDAYEND = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPURCHASE = RestCaller.getProcessDtls("PURCHASE");
    	processPermissionMst.setProcessId(processPURCHASE.getBody().getId());
    	String vNoPURCHASE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPURCHASE);
    	ResponseEntity<ProcessPermissionMst> respentityPURCHASE = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACCEPTSTOCK = RestCaller.getProcessDtls("ACCEPT STOCK");
    	processPermissionMst.setProcessId(processACCEPTSTOCK.getBody().getId());
    	String vNoACCEPTSTOCK = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACCEPTSTOCK);
    	ResponseEntity<ProcessPermissionMst> respentityACCEPTSTOCK = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSTOCKTRANSFER = RestCaller.getProcessDtls("STOCK TRANSFER");
    	processPermissionMst.setProcessId(processSTOCKTRANSFER.getBody().getId());
    	String vNoSTOCKTRANSFER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSTOCKTRANSFER);
    	ResponseEntity<ProcessPermissionMst> respentitySTOCKTRANSFER = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACCOUNTS = RestCaller.getProcessDtls("ACCOUNTS");
    	processPermissionMst.setProcessId(processACCOUNTS.getBody().getId());
    	String vNoACCOUNTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACCOUNTS);
    	ResponseEntity<ProcessPermissionMst> respentityACCOUNTS = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPRODUCTION = RestCaller.getProcessDtls("PRODUCTION PLANNING");
    	processPermissionMst.setProcessId(processPRODUCTION.getBody().getId());
    	String vNoPRODUCTION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPRODUCTION);
    	ResponseEntity<ProcessPermissionMst> respentityPRODUCTION = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPHYSICALSTOCK = RestCaller.getProcessDtls("PHYSICAL STOCK");
    	processPermissionMst.setProcessId(processPHYSICALSTOCK.getBody().getId());
    	String vNoPHYSICALSTOCK = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPHYSICALSTOCK);
    	ResponseEntity<ProcessPermissionMst> respentityPHYSICALSTOCK = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACTUALPRODUCTION = RestCaller.getProcessDtls("ACTUAL PRODUCTION");
    	processPermissionMst.setProcessId(processACTUALPRODUCTION.getBody().getId());
    	String vNoACTUALPRODUCTION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACTUALPRODUCTION);
    	ResponseEntity<ProcessPermissionMst> respentityACTUALPRODUCTION = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESRETURN = RestCaller.getProcessDtls("SALES RETURN");
    	processPermissionMst.setProcessId(processSALESRETURN.getBody().getId());
    	String vNoSALESRETURN = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESRETURN);
    	ResponseEntity<ProcessPermissionMst> respentitySALESRETURN= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESFROMORDER = RestCaller.getProcessDtls("SALES FROM ORDER");
    	processPermissionMst.setProcessId(processSALESFROMORDER.getBody().getId());
    	String vNoSALESFROMORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESFROMORDER);
    	ResponseEntity<ProcessPermissionMst> respentitySALESFROMORDER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESORDEREDIT = RestCaller.getProcessDtls("SALES ORDER EDIT");
    	processPermissionMst.setProcessId(processSALESORDEREDIT.getBody().getId());
    	String vNoSALESORDEREDIT = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESORDEREDIT);
    	ResponseEntity<ProcessPermissionMst> respentitySALESORDEREDIT= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processDAMAGEENTRY= RestCaller.getProcessDtls("DAMAGE ENTRY");
    	processPermissionMst.setProcessId(processDAMAGEENTRY.getBody().getId());
    	String vNoDAMAGEENTRY = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoDAMAGEENTRY);
    	ResponseEntity<ProcessPermissionMst> respentityDAMAGEENTRY= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processReORDER= RestCaller.getProcessDtls("ReORDER");
    	processPermissionMst.setProcessId(processReORDER.getBody().getId());
    	String vNoReORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoReORDER);
    	ResponseEntity<ProcessPermissionMst> respentityReORDER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processUSERREGISTRATION= RestCaller.getProcessDtls("USER REGISTRATION");
    	processPermissionMst.setProcessId(processUSERREGISTRATION.getBody().getId());
    	String vNoUSERREGISTRATION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoUSERREGISTRATION);
    	ResponseEntity<ProcessPermissionMst> respentityUSERREGISTRATION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	notifyMessage(5,"Successfully Added as POS Operator");
    	usrfullname.clear();
		usrName.clear();
		txtpassword.clear();
		txtpassword1.clear();
    }
    @FXML
    void posAdminRoles(ActionEvent event) {
    	
    	ProcessPermissionMst 	processPermissionMst = new ProcessPermissionMst();
    	if(usrName.getText().trim().length()==0)
    	{
    		notifyMessage(5,"Please Select User");
    		return;
    	}
    	ResponseEntity<UserMst> respentity = RestCaller.getUserByName(usrName.getText());
    	userregistration = respentity.getBody();
    	if(null == userregistration)
    	{
    		notifyMessage(5,"Please Add the User");
    		return;
    	}
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<List<ProcessPermissionMst>> savedprocessPermissionMst = RestCaller.getProcessPermissionbyUserId(userregistration.getId());
    	processList = FXCollections.observableArrayList(savedprocessPermissionMst.getBody());
    	for(ProcessPermissionMst process:processList)
    	{
    		if(process.getUserId().equalsIgnoreCase(userregistration.getId()))
    		{
    			notifyMessage(5,"Already Done");
    			return;
    		}
    	}
ResponseEntity<ProcessMst>processSaved = RestCaller.getProcessDtls("POS WINDOW");
    	
    	processPermissionMst.setProcessId(processSaved.getBody().getId());
    	
    	String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNo);
    	ResponseEntity<ProcessPermissionMst> respentity1 = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedKot = RestCaller.getProcessDtls("KOT POS");
    	processPermissionMst.setProcessId(processSavedKot.getBody().getId());
    	String vNokot = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNokot);
    	ResponseEntity<ProcessPermissionMst> respentitykot = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedWHOLESALE = RestCaller.getProcessDtls("WHOLESALE");
    	processPermissionMst.setProcessId(processSavedWHOLESALE.getBody().getId());
    	String vNoWHOLESALE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoWHOLESALE);
    	ResponseEntity<ProcessPermissionMst> respentityWHOLESALE = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedONLINESALES = RestCaller.getProcessDtls("ONLINE SALES");
    	processPermissionMst.setProcessId(processSavedONLINESALES.getBody().getId());
    	String vNoONLINESALES = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoONLINESALES);
    	ResponseEntity<ProcessPermissionMst> respentityONLINESALES = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedSALEORDER = RestCaller.getProcessDtls("SALEORDER");
    	processPermissionMst.setProcessId(processSavedSALEORDER.getBody().getId());
    	String vNoSALEORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALEORDER);
    	ResponseEntity<ProcessPermissionMst> respentitySALEORDER = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedDAYEND = RestCaller.getProcessDtls("DAY END CLOSURE");
    	processPermissionMst.setProcessId(processSavedDAYEND.getBody().getId());
    	String vNoDAYEND = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoDAYEND);
    	ResponseEntity<ProcessPermissionMst> respentityDAYEND = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPURCHASE = RestCaller.getProcessDtls("PURCHASE");
    	processPermissionMst.setProcessId(processPURCHASE.getBody().getId());
    	String vNoPURCHASE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPURCHASE);
    	ResponseEntity<ProcessPermissionMst> respentityPURCHASE = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACCEPTSTOCK = RestCaller.getProcessDtls("ACCEPT STOCK");
    	processPermissionMst.setProcessId(processACCEPTSTOCK.getBody().getId());
    	String vNoACCEPTSTOCK = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACCEPTSTOCK);
    	ResponseEntity<ProcessPermissionMst> respentityACCEPTSTOCK = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSTOCKTRANSFER = RestCaller.getProcessDtls("STOCK TRANSFER");
    	processPermissionMst.setProcessId(processSTOCKTRANSFER.getBody().getId());
    	String vNoSTOCKTRANSFER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSTOCKTRANSFER);
    	ResponseEntity<ProcessPermissionMst> respentitySTOCKTRANSFER = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACCOUNTS = RestCaller.getProcessDtls("ACCOUNTS");
    	processPermissionMst.setProcessId(processACCOUNTS.getBody().getId());
    	String vNoACCOUNTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACCOUNTS);
    	ResponseEntity<ProcessPermissionMst> respentityACCOUNTS = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPRODUCTION = RestCaller.getProcessDtls("PRODUCTION PLANNING");
    	processPermissionMst.setProcessId(processPRODUCTION.getBody().getId());
    	String vNoPRODUCTION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPRODUCTION);
    	ResponseEntity<ProcessPermissionMst> respentityPRODUCTION = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPHYSICALSTOCK = RestCaller.getProcessDtls("PHYSICAL STOCK");
    	processPermissionMst.setProcessId(processPHYSICALSTOCK.getBody().getId());
    	String vNoPHYSICALSTOCK = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPHYSICALSTOCK);
    	ResponseEntity<ProcessPermissionMst> respentityPHYSICALSTOCK = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACTUALPRODUCTION = RestCaller.getProcessDtls("ACTUAL PRODUCTION");
    	processPermissionMst.setProcessId(processACTUALPRODUCTION.getBody().getId());
    	String vNoACTUALPRODUCTION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACTUALPRODUCTION);
    	ResponseEntity<ProcessPermissionMst> respentityACTUALPRODUCTION = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESRETURN = RestCaller.getProcessDtls("SALES RETURN");
    	processPermissionMst.setProcessId(processSALESRETURN.getBody().getId());
    	String vNoSALESRETURN = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESRETURN);
    	ResponseEntity<ProcessPermissionMst> respentitySALESRETURN= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESFROMORDER = RestCaller.getProcessDtls("SALES FROM ORDER");
    	processPermissionMst.setProcessId(processSALESFROMORDER.getBody().getId());
    	String vNoSALESFROMORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESFROMORDER);
    	ResponseEntity<ProcessPermissionMst> respentitySALESFROMORDER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESORDEREDIT = RestCaller.getProcessDtls("SALES ORDER EDIT");
    	processPermissionMst.setProcessId(processSALESORDEREDIT.getBody().getId());
    	String vNoSALESORDEREDIT = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESORDEREDIT);
    	ResponseEntity<ProcessPermissionMst> respentitySALESORDEREDIT= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processOWNACCOUNTSETTLEMENT = RestCaller.getProcessDtls("OWN ACCOUNT SETTLEMENT");
    	processPermissionMst.setProcessId(processOWNACCOUNTSETTLEMENT.getBody().getId());
    	String vNoOWNACCOUNTSETTLEMENT = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoOWNACCOUNTSETTLEMENT);
    	ResponseEntity<ProcessPermissionMst> respentityOWNACCOUNTSETTLEMENT= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSUPPLIERPAYMENTS = RestCaller.getProcessDtls("SUPPLIER PAYMENTS");
    	processPermissionMst.setProcessId(processSUPPLIERPAYMENTS.getBody().getId());
    	String vNoSUPPLIERPAYMENTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSUPPLIERPAYMENTS);
    	ResponseEntity<ProcessPermissionMst> respentitySUPPLIERPAYMENTS= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processREPORTS = RestCaller.getProcessDtls("REPORTS");
    	processPermissionMst.setProcessId(processREPORTS.getBody().getId());
    	String vNoREPORTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoREPORTS);
    	ResponseEntity<ProcessPermissionMst> respentityREPORTS= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processDAMAGEENTRY= RestCaller.getProcessDtls("DAMAGE ENTRY");
    	processPermissionMst.setProcessId(processDAMAGEENTRY.getBody().getId());
    	String vNoDAMAGEENTRY = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoDAMAGEENTRY);
    	ResponseEntity<ProcessPermissionMst> respentityDAMAGEENTRY= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processReORDER= RestCaller.getProcessDtls("ReORDER");
    	processPermissionMst.setProcessId(processReORDER.getBody().getId());
    	String vNoReORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoReORDER);
    	ResponseEntity<ProcessPermissionMst> respentityReORDER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processITEMMASTER= RestCaller.getProcessDtls("ITEM MASTER");
    	processPermissionMst.setProcessId(processITEMMASTER.getBody().getId());
    	String vNoITEMMASTER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoITEMMASTER);
    	ResponseEntity<ProcessPermissionMst> respentityITEMMASTER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPRODUCTCONVERSION= RestCaller.getProcessDtls("PRODUCT CONVERSION");
    	processPermissionMst.setProcessId(processPRODUCTCONVERSION.getBody().getId());
    	String vNoPRODUCTCONVERSION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPRODUCTCONVERSION);
    	ResponseEntity<ProcessPermissionMst> respentityPRODUCTCONVERSION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processCUSTOMER= RestCaller.getProcessDtls("CUSTOMER");
    	processPermissionMst.setProcessId(processCUSTOMER.getBody().getId());
    	String vNoCUSTOMER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoCUSTOMER);
    	ResponseEntity<ProcessPermissionMst> respentityCUSTOMER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processADDSUPPLIER= RestCaller.getProcessDtls("ADD SUPPLIER");
    	processPermissionMst.setProcessId(processADDSUPPLIER.getBody().getId());
    	String vNoADDSUPPLIER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoADDSUPPLIER);
    	ResponseEntity<ProcessPermissionMst> respentityADDSUPPLIER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processMULTIUNIT= RestCaller.getProcessDtls("MULTI UNIT");
    	processPermissionMst.setProcessId(processMULTIUNIT.getBody().getId());
    	String vNoMULTIUNIT = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoMULTIUNIT);
    	ResponseEntity<ProcessPermissionMst> respentityMULTIUNIT= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processKITDEFINITION= RestCaller.getProcessDtls("KIT DEFINITION");
    	processPermissionMst.setProcessId(processKITDEFINITION.getBody().getId());
    	String vNoKITDEFINITION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoKITDEFINITION);
    	ResponseEntity<ProcessPermissionMst> respentityKITDEFINITION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPRICEDEFINITION= RestCaller.getProcessDtls("PRICE DEFINITION");
    	processPermissionMst.setProcessId(processPRICEDEFINITION.getBody().getId());
    	String vNoPRICEDEFINITION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPRICEDEFINITION);
    	ResponseEntity<ProcessPermissionMst> respentityPRICEDEFINITION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processKOTMANAGER= RestCaller.getProcessDtls("KOT MANAGER");
    	processPermissionMst.setProcessId(processKOTMANAGER.getBody().getId());
    	String vNoKOTMANAGER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoKOTMANAGER);
    	ResponseEntity<ProcessPermissionMst> respentityKOTMANAGER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processUSERREGISTRATION= RestCaller.getProcessDtls("KOT MANAGER");
    	processPermissionMst.setProcessId(processUSERREGISTRATION.getBody().getId());
    	String vNoUSERREGISTRATION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoUSERREGISTRATION);
    	ResponseEntity<ProcessPermissionMst> respentityUSERREGISTRATION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSCHEME= RestCaller.getProcessDtls("SCHEME");
    	processPermissionMst.setProcessId(processSCHEME.getBody().getId());
    	String vNoSCHEME = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSCHEME);
    	ResponseEntity<ProcessPermissionMst> respentitySCHEME= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSCHEMEDETAILS= RestCaller.getProcessDtls("SCHEME DETAILS");
    	processPermissionMst.setProcessId(processSCHEMEDETAILS.getBody().getId());
    	String vNoSCHEMEDETAILS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSCHEMEDETAILS);
    	ResponseEntity<ProcessPermissionMst> respentitySCHEMEDETAILS= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPROCESSPERMISSION= RestCaller.getProcessDtls("PROCESS PERMISSION");
    	processPermissionMst.setProcessId(processPROCESSPERMISSION.getBody().getId());
    	String vNoPROCESSPERMISSION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPROCESSPERMISSION);
    	ResponseEntity<ProcessPermissionMst> respentityPROCESSPERMISSION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processCONFIGURATIONS= RestCaller.getProcessDtls("CONFIGURATIONS");
    	processPermissionMst.setProcessId(processCONFIGURATIONS.getBody().getId());
    	String vNoCONFIGURATIONS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoCONFIGURATIONS);
    	ResponseEntity<ProcessPermissionMst> respentityCONFIGURATIONS= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processRECEIPTMODE= RestCaller.getProcessDtls("RECEIPT MODE");
    	processPermissionMst.setProcessId(processRECEIPTMODE.getBody().getId());
    	String vNoRECEIPTMODE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoRECEIPTMODE);
    	ResponseEntity<ProcessPermissionMst> respentityRECEIPTMODE= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processVOUCHERTYPE= RestCaller.getProcessDtls("VOUCHER TYPE");
    	processPermissionMst.setProcessId(processVOUCHERTYPE.getBody().getId());
    	String vNoVOUCHERTYPE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoVOUCHERTYPE);
    	ResponseEntity<ProcessPermissionMst> respentityVOUCHERTYPE= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	notifyMessage(5,"Successfully Added as POS Administrator");
    	usrfullname.clear();
		usrName.clear();
		txtpassword.clear();
		txtpassword1.clear();
		
    }
    
    public void posAdmin() {

    	
    	ProcessPermissionMst 	processPermissionMst = new ProcessPermissionMst();
    	if(usrName.getText().trim().length()==0)
    	{
    		notifyMessage(5,"Please Select User");
    		return;
    	}
    	ResponseEntity<UserMst> respentity = RestCaller.getUserByName(usrName.getText());
    	userregistration = respentity.getBody();
    	if(null == userregistration)
    	{
    		notifyMessage(5,"Please Add the User");
    		return;
    	}
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<List<ProcessPermissionMst>> savedprocessPermissionMst = RestCaller.getProcessPermissionbyUserId(userregistration.getId());
    	processList = FXCollections.observableArrayList(savedprocessPermissionMst.getBody());
    	for(ProcessPermissionMst process:processList)
    	{
    		if(process.getUserId().equalsIgnoreCase(userregistration.getId()))
    		{
    			notifyMessage(5,"Already Done");
    			return;
    		}
    	}
ResponseEntity<ProcessMst>processSaved = RestCaller.getProcessDtls("POS WINDOW");
    	
    	processPermissionMst.setProcessId(processSaved.getBody().getId());
    	
    	String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNo);
    	ResponseEntity<ProcessPermissionMst> respentity1 = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedKot = RestCaller.getProcessDtls("KOT POS");
    	processPermissionMst.setProcessId(processSavedKot.getBody().getId());
    	String vNokot = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNokot);
    	ResponseEntity<ProcessPermissionMst> respentitykot = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedWHOLESALE = RestCaller.getProcessDtls("WHOLESALE");
    	processPermissionMst.setProcessId(processSavedWHOLESALE.getBody().getId());
    	String vNoWHOLESALE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoWHOLESALE);
    	ResponseEntity<ProcessPermissionMst> respentityWHOLESALE = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedONLINESALES = RestCaller.getProcessDtls("ONLINE SALES");
    	processPermissionMst.setProcessId(processSavedONLINESALES.getBody().getId());
    	String vNoONLINESALES = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoONLINESALES);
    	ResponseEntity<ProcessPermissionMst> respentityONLINESALES = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedSALEORDER = RestCaller.getProcessDtls("SALEORDER");
    	processPermissionMst.setProcessId(processSavedSALEORDER.getBody().getId());
    	String vNoSALEORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALEORDER);
    	ResponseEntity<ProcessPermissionMst> respentitySALEORDER = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedDAYEND = RestCaller.getProcessDtls("DAY END CLOSURE");
    	processPermissionMst.setProcessId(processSavedDAYEND.getBody().getId());
    	String vNoDAYEND = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoDAYEND);
    	ResponseEntity<ProcessPermissionMst> respentityDAYEND = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPURCHASE = RestCaller.getProcessDtls("PURCHASE");
    	processPermissionMst.setProcessId(processPURCHASE.getBody().getId());
    	String vNoPURCHASE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPURCHASE);
    	ResponseEntity<ProcessPermissionMst> respentityPURCHASE = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACCEPTSTOCK = RestCaller.getProcessDtls("ACCEPT STOCK");
    	processPermissionMst.setProcessId(processACCEPTSTOCK.getBody().getId());
    	String vNoACCEPTSTOCK = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACCEPTSTOCK);
    	ResponseEntity<ProcessPermissionMst> respentityACCEPTSTOCK = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSTOCKTRANSFER = RestCaller.getProcessDtls("STOCK TRANSFER");
    	processPermissionMst.setProcessId(processSTOCKTRANSFER.getBody().getId());
    	String vNoSTOCKTRANSFER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSTOCKTRANSFER);
    	ResponseEntity<ProcessPermissionMst> respentitySTOCKTRANSFER = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACCOUNTS = RestCaller.getProcessDtls("ACCOUNTS");
    	processPermissionMst.setProcessId(processACCOUNTS.getBody().getId());
    	String vNoACCOUNTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACCOUNTS);
    	ResponseEntity<ProcessPermissionMst> respentityACCOUNTS = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPRODUCTION = RestCaller.getProcessDtls("PRODUCTION PLANNING");
    	processPermissionMst.setProcessId(processPRODUCTION.getBody().getId());
    	String vNoPRODUCTION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPRODUCTION);
    	ResponseEntity<ProcessPermissionMst> respentityPRODUCTION = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPHYSICALSTOCK = RestCaller.getProcessDtls("PHYSICAL STOCK");
    	processPermissionMst.setProcessId(processPHYSICALSTOCK.getBody().getId());
    	String vNoPHYSICALSTOCK = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPHYSICALSTOCK);
    	ResponseEntity<ProcessPermissionMst> respentityPHYSICALSTOCK = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACTUALPRODUCTION = RestCaller.getProcessDtls("ACTUAL PRODUCTION");
    	processPermissionMst.setProcessId(processACTUALPRODUCTION.getBody().getId());
    	String vNoACTUALPRODUCTION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACTUALPRODUCTION);
    	ResponseEntity<ProcessPermissionMst> respentityACTUALPRODUCTION = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESRETURN = RestCaller.getProcessDtls("SALES RETURN");
    	processPermissionMst.setProcessId(processSALESRETURN.getBody().getId());
    	String vNoSALESRETURN = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESRETURN);
    	ResponseEntity<ProcessPermissionMst> respentitySALESRETURN= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESFROMORDER = RestCaller.getProcessDtls("SALES FROM ORDER");
    	processPermissionMst.setProcessId(processSALESFROMORDER.getBody().getId());
    	String vNoSALESFROMORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESFROMORDER);
    	ResponseEntity<ProcessPermissionMst> respentitySALESFROMORDER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESORDEREDIT = RestCaller.getProcessDtls("SALES ORDER EDIT");
    	processPermissionMst.setProcessId(processSALESORDEREDIT.getBody().getId());
    	String vNoSALESORDEREDIT = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESORDEREDIT);
    	ResponseEntity<ProcessPermissionMst> respentitySALESORDEREDIT= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processOWNACCOUNTSETTLEMENT = RestCaller.getProcessDtls("OWN ACCOUNT SETTLEMENT");
    	processPermissionMst.setProcessId(processOWNACCOUNTSETTLEMENT.getBody().getId());
    	String vNoOWNACCOUNTSETTLEMENT = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoOWNACCOUNTSETTLEMENT);
    	ResponseEntity<ProcessPermissionMst> respentityOWNACCOUNTSETTLEMENT= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSUPPLIERPAYMENTS = RestCaller.getProcessDtls("SUPPLIER PAYMENTS");
    	processPermissionMst.setProcessId(processSUPPLIERPAYMENTS.getBody().getId());
    	String vNoSUPPLIERPAYMENTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSUPPLIERPAYMENTS);
    	ResponseEntity<ProcessPermissionMst> respentitySUPPLIERPAYMENTS= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processREPORTS = RestCaller.getProcessDtls("REPORTS");
    	processPermissionMst.setProcessId(processREPORTS.getBody().getId());
    	String vNoREPORTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoREPORTS);
    	ResponseEntity<ProcessPermissionMst> respentityREPORTS= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processDAMAGEENTRY= RestCaller.getProcessDtls("DAMAGE ENTRY");
    	processPermissionMst.setProcessId(processDAMAGEENTRY.getBody().getId());
    	String vNoDAMAGEENTRY = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoDAMAGEENTRY);
    	ResponseEntity<ProcessPermissionMst> respentityDAMAGEENTRY= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processReORDER= RestCaller.getProcessDtls("ReORDER");
    	processPermissionMst.setProcessId(processReORDER.getBody().getId());
    	String vNoReORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoReORDER);
    	ResponseEntity<ProcessPermissionMst> respentityReORDER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processITEMMASTER= RestCaller.getProcessDtls("ITEM MASTER");
    	processPermissionMst.setProcessId(processITEMMASTER.getBody().getId());
    	String vNoITEMMASTER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoITEMMASTER);
    	ResponseEntity<ProcessPermissionMst> respentityITEMMASTER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPRODUCTCONVERSION= RestCaller.getProcessDtls("PRODUCT CONVERSION");
    	processPermissionMst.setProcessId(processPRODUCTCONVERSION.getBody().getId());
    	String vNoPRODUCTCONVERSION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPRODUCTCONVERSION);
    	ResponseEntity<ProcessPermissionMst> respentityPRODUCTCONVERSION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processCUSTOMER= RestCaller.getProcessDtls("CUSTOMER");
    	processPermissionMst.setProcessId(processCUSTOMER.getBody().getId());
    	String vNoCUSTOMER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoCUSTOMER);
    	ResponseEntity<ProcessPermissionMst> respentityCUSTOMER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processADDSUPPLIER= RestCaller.getProcessDtls("ADD SUPPLIER");
    	processPermissionMst.setProcessId(processADDSUPPLIER.getBody().getId());
    	String vNoADDSUPPLIER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoADDSUPPLIER);
    	ResponseEntity<ProcessPermissionMst> respentityADDSUPPLIER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processMULTIUNIT= RestCaller.getProcessDtls("MULTI UNIT");
    	processPermissionMst.setProcessId(processMULTIUNIT.getBody().getId());
    	String vNoMULTIUNIT = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoMULTIUNIT);
    	ResponseEntity<ProcessPermissionMst> respentityMULTIUNIT= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processKITDEFINITION= RestCaller.getProcessDtls("KIT DEFINITION");
    	processPermissionMst.setProcessId(processKITDEFINITION.getBody().getId());
    	String vNoKITDEFINITION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoKITDEFINITION);
    	ResponseEntity<ProcessPermissionMst> respentityKITDEFINITION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPRICEDEFINITION= RestCaller.getProcessDtls("PRICE DEFINITION");
    	processPermissionMst.setProcessId(processPRICEDEFINITION.getBody().getId());
    	String vNoPRICEDEFINITION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPRICEDEFINITION);
    	ResponseEntity<ProcessPermissionMst> respentityPRICEDEFINITION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processKOTMANAGER= RestCaller.getProcessDtls("KOT MANAGER");
    	processPermissionMst.setProcessId(processKOTMANAGER.getBody().getId());
    	String vNoKOTMANAGER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoKOTMANAGER);
    	ResponseEntity<ProcessPermissionMst> respentityKOTMANAGER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processUSERREGISTRATION= RestCaller.getProcessDtls("KOT MANAGER");
    	processPermissionMst.setProcessId(processUSERREGISTRATION.getBody().getId());
    	String vNoUSERREGISTRATION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoUSERREGISTRATION);
    	ResponseEntity<ProcessPermissionMst> respentityUSERREGISTRATION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSCHEME= RestCaller.getProcessDtls("SCHEME");
    	processPermissionMst.setProcessId(processSCHEME.getBody().getId());
    	String vNoSCHEME = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSCHEME);
    	ResponseEntity<ProcessPermissionMst> respentitySCHEME= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSCHEMEDETAILS= RestCaller.getProcessDtls("SCHEME DETAILS");
    	processPermissionMst.setProcessId(processSCHEMEDETAILS.getBody().getId());
    	String vNoSCHEMEDETAILS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSCHEMEDETAILS);
    	ResponseEntity<ProcessPermissionMst> respentitySCHEMEDETAILS= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPROCESSPERMISSION= RestCaller.getProcessDtls("PROCESS PERMISSION");
    	processPermissionMst.setProcessId(processPROCESSPERMISSION.getBody().getId());
    	String vNoPROCESSPERMISSION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPROCESSPERMISSION);
    	ResponseEntity<ProcessPermissionMst> respentityPROCESSPERMISSION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processCONFIGURATIONS= RestCaller.getProcessDtls("CONFIGURATIONS");
    	processPermissionMst.setProcessId(processCONFIGURATIONS.getBody().getId());
    	String vNoCONFIGURATIONS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoCONFIGURATIONS);
    	ResponseEntity<ProcessPermissionMst> respentityCONFIGURATIONS= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processRECEIPTMODE= RestCaller.getProcessDtls("RECEIPT MODE");
    	processPermissionMst.setProcessId(processRECEIPTMODE.getBody().getId());
    	String vNoRECEIPTMODE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoRECEIPTMODE);
    	ResponseEntity<ProcessPermissionMst> respentityRECEIPTMODE= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processVOUCHERTYPE= RestCaller.getProcessDtls("VOUCHER TYPE");
    	processPermissionMst.setProcessId(processVOUCHERTYPE.getBody().getId());
    	String vNoVOUCHERTYPE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoVOUCHERTYPE);
    	ResponseEntity<ProcessPermissionMst> respentityVOUCHERTYPE= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	notifyMessage(5,"Successfully Added as POS Administrator");
    	usrfullname.clear();
		usrName.clear();
		txtpassword.clear();
		txtpassword1.clear();
		cmbUserPermisssion.setValue("");
    
    }
    
    
    public void posOperator() {

    	
    	ProcessPermissionMst 	processPermissionMst = new ProcessPermissionMst();
    	if(usrName.getText().trim().length()==0)
    	{
    		notifyMessage(5,"Please Select User");
    		return;
    	}
    	ResponseEntity<UserMst> respentity = RestCaller.getUserByName(usrName.getText());
    	userregistration = respentity.getBody();
    	if(null == userregistration)
    	{
    		notifyMessage(5,"Please Add the User");
    		return;
    	}
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<List<ProcessPermissionMst>> savedprocessPermissionMst = RestCaller.getProcessPermissionbyUserId(userregistration.getId());
    	processList = FXCollections.observableArrayList(savedprocessPermissionMst.getBody());
    	for(ProcessPermissionMst process:processList)
    	{
    		if(process.getUserId().equalsIgnoreCase(userregistration.getId()))
    		{
    			notifyMessage(5,"Already Done");
    			return;
    		}
    	}
    	ResponseEntity<ProcessMst>processSaved = RestCaller.getProcessDtls("POS WINDOW");
    	
    	processPermissionMst.setProcessId(processSaved.getBody().getId());
    	
    	String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNo);
    	ResponseEntity<ProcessPermissionMst> respentity1 = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedKot = RestCaller.getProcessDtls("KOT POS");
    	processPermissionMst.setProcessId(processSavedKot.getBody().getId());
    	String vNokot = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNokot);
    	ResponseEntity<ProcessPermissionMst> respentitykot = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedWHOLESALE = RestCaller.getProcessDtls("WHOLESALE");
    	processPermissionMst.setProcessId(processSavedWHOLESALE.getBody().getId());
    	String vNoWHOLESALE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoWHOLESALE);
    	ResponseEntity<ProcessPermissionMst> respentityWHOLESALE = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedONLINESALES = RestCaller.getProcessDtls("ONLINE SALES");
    	processPermissionMst.setProcessId(processSavedONLINESALES.getBody().getId());
    	String vNoONLINESALES = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoONLINESALES);
    	ResponseEntity<ProcessPermissionMst> respentityONLINESALES = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedSALEORDER = RestCaller.getProcessDtls("SALEORDER");
    	processPermissionMst.setProcessId(processSavedSALEORDER.getBody().getId());
    	String vNoSALEORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALEORDER);
    	ResponseEntity<ProcessPermissionMst> respentitySALEORDER = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSavedDAYEND = RestCaller.getProcessDtls("DAY END CLOSURE");
    	processPermissionMst.setProcessId(processSavedDAYEND.getBody().getId());
    	String vNoDAYEND = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoDAYEND);
    	ResponseEntity<ProcessPermissionMst> respentityDAYEND = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPURCHASE = RestCaller.getProcessDtls("PURCHASE");
    	processPermissionMst.setProcessId(processPURCHASE.getBody().getId());
    	String vNoPURCHASE = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPURCHASE);
    	ResponseEntity<ProcessPermissionMst> respentityPURCHASE = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACCEPTSTOCK = RestCaller.getProcessDtls("ACCEPT STOCK");
    	processPermissionMst.setProcessId(processACCEPTSTOCK.getBody().getId());
    	String vNoACCEPTSTOCK = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACCEPTSTOCK);
    	ResponseEntity<ProcessPermissionMst> respentityACCEPTSTOCK = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSTOCKTRANSFER = RestCaller.getProcessDtls("STOCK TRANSFER");
    	processPermissionMst.setProcessId(processSTOCKTRANSFER.getBody().getId());
    	String vNoSTOCKTRANSFER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSTOCKTRANSFER);
    	ResponseEntity<ProcessPermissionMst> respentitySTOCKTRANSFER = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACCOUNTS = RestCaller.getProcessDtls("ACCOUNTS");
    	processPermissionMst.setProcessId(processACCOUNTS.getBody().getId());
    	String vNoACCOUNTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACCOUNTS);
    	ResponseEntity<ProcessPermissionMst> respentityACCOUNTS = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPRODUCTION = RestCaller.getProcessDtls("PRODUCTION PLANNING");
    	processPermissionMst.setProcessId(processPRODUCTION.getBody().getId());
    	String vNoPRODUCTION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPRODUCTION);
    	ResponseEntity<ProcessPermissionMst> respentityPRODUCTION = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processPHYSICALSTOCK = RestCaller.getProcessDtls("PHYSICAL STOCK");
    	processPermissionMst.setProcessId(processPHYSICALSTOCK.getBody().getId());
    	String vNoPHYSICALSTOCK = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoPHYSICALSTOCK);
    	ResponseEntity<ProcessPermissionMst> respentityPHYSICALSTOCK = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processACTUALPRODUCTION = RestCaller.getProcessDtls("ACTUAL PRODUCTION");
    	processPermissionMst.setProcessId(processACTUALPRODUCTION.getBody().getId());
    	String vNoACTUALPRODUCTION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoACTUALPRODUCTION);
    	ResponseEntity<ProcessPermissionMst> respentityACTUALPRODUCTION = RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESRETURN = RestCaller.getProcessDtls("SALES RETURN");
    	processPermissionMst.setProcessId(processSALESRETURN.getBody().getId());
    	String vNoSALESRETURN = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESRETURN);
    	ResponseEntity<ProcessPermissionMst> respentitySALESRETURN= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESFROMORDER = RestCaller.getProcessDtls("SALES FROM ORDER");
    	processPermissionMst.setProcessId(processSALESFROMORDER.getBody().getId());
    	String vNoSALESFROMORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESFROMORDER);
    	ResponseEntity<ProcessPermissionMst> respentitySALESFROMORDER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processSALESORDEREDIT = RestCaller.getProcessDtls("SALES ORDER EDIT");
    	processPermissionMst.setProcessId(processSALESORDEREDIT.getBody().getId());
    	String vNoSALESORDEREDIT = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoSALESORDEREDIT);
    	ResponseEntity<ProcessPermissionMst> respentitySALESORDEREDIT= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processDAMAGEENTRY= RestCaller.getProcessDtls("DAMAGE ENTRY");
    	processPermissionMst.setProcessId(processDAMAGEENTRY.getBody().getId());
    	String vNoDAMAGEENTRY = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoDAMAGEENTRY);
    	ResponseEntity<ProcessPermissionMst> respentityDAMAGEENTRY= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processReORDER= RestCaller.getProcessDtls("ReORDER");
    	processPermissionMst.setProcessId(processReORDER.getBody().getId());
    	String vNoReORDER = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoReORDER);
    	ResponseEntity<ProcessPermissionMst> respentityReORDER= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	
    	processPermissionMst = new ProcessPermissionMst();
    	processPermissionMst.setUserId(userregistration.getId());
    	ResponseEntity<ProcessMst>processUSERREGISTRATION= RestCaller.getProcessDtls("USER REGISTRATION");
    	processPermissionMst.setProcessId(processUSERREGISTRATION.getBody().getId());
    	String vNoUSERREGISTRATION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
        processPermissionMst.setId(vNoUSERREGISTRATION);
    	ResponseEntity<ProcessPermissionMst> respentityUSERREGISTRATION= RestCaller.saveProcessPermissionMst(processPermissionMst);
    	notifyMessage(5,"Successfully Added as POS Operator");
    	usrfullname.clear();
		usrName.clear();
		txtpassword.clear();
		txtpassword1.clear();
    
    	cmbUserPermisssion.setValue("");
    	
    }
    @FXML
    void Refresh(ActionEvent event) {

    	usrbranch.getItems().clear();
    	ArrayList cat = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		cat = RestCaller.SearchBranchs();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object unitName = lm.get("branchCode");
			Object unitId = lm.get("id");
			if (unitId != null) {
				usrbranch.getItems().add((String) unitName);
				BranchMst newUnitMst = new BranchMst();
				
				newUnitMst.setId((String) unitId);
				branchList.add(newUnitMst);
			}
		}
		usrfullname.clear();
		usrName.clear();
		txtpassword.clear();
		
    }


    @FXML
    void Search(ActionEvent event) {

    }

    @FXML
    void Submit(ActionEvent event) {
    	
    submit();	
    	
    	

    }
    @FXML
    void ShowAll(ActionEvent event) {

    	ResponseEntity<List<UserMst>> respentity = RestCaller.getAllusers();
    	userRegistrationList = FXCollections.observableArrayList(respentity.getBody());
    	filltable();
		
    
    }
    @FXML
    void submitOnEnter(KeyEvent event) {

    }
    private void submit()
    {
    	
    	if(usrName.getText().trim().isEmpty())
    	{
    		notifyMessage(5, "Please Enter user Name...!!!");
    	} else if (usrfullname.getText().trim().isEmpty()) {
    		notifyMessage(5, "Please Enter fullname...!!!");
    		
    	} 	
    	
    	else
    	{
    	usrtbl.getItems();
    	userregistration = new UserMst();
    
    	
    	userregistration.setUserName(usrName.getText());
 
    	
    	userregistration.setFullName(usrfullname.getText());
    	userregistration.setBranchCode(usrbranch.getValue());
    	
   
    	userregistration.setPassword(txtpassword.getText());
    	userregistration.setPassword1(txtpassword1.getText());
    
    	}
    	
    	if(null!=SystemSetting.getUser()) {
    		userregistration.setCompanyMst(SystemSetting.getUser().getCompanyMst());
    	}
    	if(txtpassword.getText().equalsIgnoreCase(txtpassword1.getText()))
    	{
    	ResponseEntity<UserMst> respentity = RestCaller.userRegistration(userregistration,SystemSetting.systemBranch);
    	userregistration = respentity.getBody();
    	if (null != userregistration) {
    		
    		
    		
    		userRegistrationList.add(userregistration);
    		usrtbl.setItems(userRegistrationList);
    		notifyMessage(5, "Registration Successfully Completed");
    		
    		filltable();
    		clear();

    		eventBus.post(userregistration);
    	}
    	
    	 else {

    		notifyMessage(5, "Please Fill Your Fields");

    	}
    }
    	else
    	{
    	
    		notifyMessage(5,"Password Mismatch!!!! Retype password");
    		txtpassword1.clear();
    	
    	}
    	
    	}
    	public void notifyMessage(int duration, String msg) {
    
    		Image img = new Image("done.png");
    		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
    				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
    				.onAction(new EventHandler<ActionEvent>() {
    					@Override
    					public void handle(ActionEvent event) {
    						System.out.println("clicked on notification");
    					}
    				});
    		notificationBuilder.darkStyle();
    		notificationBuilder.show();
    	}
    	public void filltable() {
    		
    		usrtbl.setItems(userRegistrationList);
    		us2.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
    		us3.setCellValueFactory(cellData -> cellData.getValue().getUserFullNameProperty());
    		us4.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
    		//us3.setCellValueFactory(cellData -> cellData.getValue().getCustomerMailProperty());
    	
    	}

    	public void clear() {
    		usrName.setText("");
    		usrfullname.setText("");
    		usrbranch.setValue("");
    		txtpassword.setText("");
    		txtpassword1.setText("");

    		

    	}
    	
    	public void churchOperator() {
    		
    		System.out.print("inside church operator");
    	
    		ProcessPermissionMst 	processPermissionMst = new ProcessPermissionMst();
        	if(usrName.getText().trim().length()==0)
        	{
        		notifyMessage(5,"Please Select User");
        		return;
        	}
        	ResponseEntity<UserMst> respentity = RestCaller.getUserByName(usrName.getText());
        	userregistration = respentity.getBody();
        	if(null == userregistration)
        	{
        		notifyMessage(5,"Please Add the User");
        		return;
        	}
    		processPermissionMst = new ProcessPermissionMst();
        	processPermissionMst.setUserId(userregistration.getId());
        	ResponseEntity<ProcessMst>processUSERREGISTRATION= RestCaller.getProcessDtls("ORGANIZATION");
        	processPermissionMst.setProcessId(processUSERREGISTRATION.getBody().getId());
        	String vNoORGANIZATION = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
            processPermissionMst.setId(vNoORGANIZATION);
        	ResponseEntity<ProcessPermissionMst> respentityORGANIZATION= RestCaller.saveProcessPermissionMst(processPermissionMst);
        	
        	
        	processPermissionMst = new ProcessPermissionMst();
        	processPermissionMst.setUserId(userregistration.getId());
        	ResponseEntity<ProcessMst>processREPORTS = RestCaller.getProcessDtls("REPORTS");
        	processPermissionMst.setProcessId(processREPORTS.getBody().getId());
        	String vNoREPORTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
            processPermissionMst.setId(vNoREPORTS);
        	ResponseEntity<ProcessPermissionMst> respentityREPORTS= RestCaller.saveProcessPermissionMst(processPermissionMst);
		/*
		 * processPermissionMst = new ProcessPermissionMst();
		 * processPermissionMst.setUserId(userregistration.getId());
		 * ResponseEntity<ProcessMst>processACCOUNTS =
		 * RestCaller.getProcessDtls("ACCOUNTS");
		 * processPermissionMst.setProcessId(processACCOUNTS.getBody().getId()); String
		 * vNoACCOUNTS =
		 * RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
		 * processPermissionMst.setId(vNoACCOUNTS); ResponseEntity<ProcessPermissionMst>
		 * respentityACCOUNTS =
		 * RestCaller.saveProcessPermissionMst(processPermissionMst);
		 */
        	
        	notifyMessage(5,"Successfully Added as Church OPerator");
        	usrfullname.clear();
    		usrName.clear();
    		txtpassword.clear();
    		txtpassword1.clear();
        	cmbUserPermisssion.setValue("");
    	}
    	
    	
    	public void accountOperator() {
    		ProcessPermissionMst 	processPermissionMst = new ProcessPermissionMst();
        	if(usrName.getText().trim().length()==0)
        	{
        		notifyMessage(5,"Please Select User");
        		return;
        	}
        	ResponseEntity<UserMst> respentity = RestCaller.getUserByName(usrName.getText());
        	userregistration = respentity.getBody();
        	if(null == userregistration)
        	{
        		notifyMessage(5,"Please Add the User");
        		return;
        	}
    	  	
        	processPermissionMst = new ProcessPermissionMst();
        	processPermissionMst.setUserId(userregistration.getId());
        	ResponseEntity<ProcessMst>processACCOUNTS = RestCaller.getProcessDtls("ACCOUNTS");
        	processPermissionMst.setProcessId(processACCOUNTS.getBody().getId());
        	String vNoACCOUNTS = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"PP");
            processPermissionMst.setId(vNoACCOUNTS);
        	ResponseEntity<ProcessPermissionMst> respentityACCOUNTS = RestCaller.saveProcessPermissionMst(processPermissionMst);
        	
        	notifyMessage(5,"Successfully Added as Acccount Operator");
        	usrfullname.clear();
    		usrName.clear();
    		txtpassword.clear();
    		txtpassword1.clear();
        	cmbUserPermisssion.setValue("");
    	}
    	@FXML
        void addPermission(ActionEvent event) {

    		if(null==cmbUserPermisssion.getValue()) {
    			
    			notifyMessage(5,"Plz Select a Permission");
    			return;
    		}
    		if(cmbUserPermisssion.getSelectionModel().getSelectedItem().equalsIgnoreCase("CHURCH OPERATOR")) {
				 churchOperator();
			  }
    			if(cmbUserPermisssion.getSelectionModel().getSelectedItem().equalsIgnoreCase("POS ADMIN"));{
    				posAdmin();
    			}if(cmbUserPermisssion.getSelectionModel().getSelectedItem().equalsIgnoreCase("POS OPERATOR")) {
    			posOperator();
    			}if(cmbUserPermisssion.getSelectionModel().getSelectedItem().equalsIgnoreCase("ACCOUNT OPERATOR")) {
    				
    			}
    			
    	
    		
    	
    			
    		}
    	
    	  @Subscribe
  	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
  	   		//if (stage.isShowing()) {
  	   			taskid = taskWindowDataEvent.getId();
  	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  	   			
  	   		 
  	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  	   			System.out.println("Business Process ID = " + hdrId);
  	   			
  	   			 PageReload(hdrId);
  	   		}


  	   	private void PageReload(String hdrId) {

  	   	}
    		
    		
        
    	


}
