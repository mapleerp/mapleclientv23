package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DayEndClosureDtl;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.DayEndReportStore;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReport;
import com.maple.mapleclient.entity.SessionEndClosureMst;
import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.entity.StockValueReports;
import com.maple.mapleclient.entity.StockVerificationMst;
import com.maple.mapleclient.entity.SysDateMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.entity.VoucherNumberDtlReport;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;
import com.maple.report.entity.DailyPaymentSummaryReport;
import com.maple.report.entity.DailyReceiptsSummaryReport;
import com.maple.report.entity.ReceiptModeReport;
import com.maple.report.entity.SalesModeWiseBillReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class DayEndReportCtl {
	String taskid;
	String processInstanceId;

	DayEndClosureHdr dayEndClosure = null;
	DayEndClosureDtl dayEndClosureDtl = null;
	DayEndClosureDtl dayEndClosureDtlToDelete = null;
	SessionEndDtl sessionEndDtl = null;

	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");

	SessionEndClosureMst sessionEndClosureMst = null;

	private ObservableList<SessionEndDtl> sessionEndDtlList = FXCollections.observableArrayList();

	private ObservableList<SalesModeWiseBillReport> billSeriesList = FXCollections.observableArrayList();

	private ObservableList<DailyReceiptsSummaryReport> receiptReportList = FXCollections.observableArrayList();

	private ObservableList<DailyPaymentSummaryReport> paymentReportList = FXCollections.observableArrayList();

	private ObservableList<SalesReport> salesReportList = FXCollections.observableArrayList();

	private ObservableList<DayEndClosureHdr> dayEndClosureHdrList = FXCollections.observableArrayList();

	private ObservableList<DayEndClosureDtl> dayEndClosureListTable = FXCollections.observableArrayList();

	Integer stock_count = SystemSetting.STOCK_VERIFICATION_COUNT;

	String SMSENABLE = SystemSetting.SMSENABLE;
	String PHONENUMBER = SystemSetting.PHONENUMBER;
	@FXML
	private TextField txtCashSale;

	@FXML
	private TextField txtCardSale;

//	@FXML
//	private TextField txtDenomination;

	// version 1.9
	@FXML
	private TextField txtCashVariance;
	// version 1.9 ends

	@FXML
	private TextField txtCount;

	@FXML
	private Button btnAdd;

	@FXML
	private TableView<DayEndClosureDtl> tblDayEnd;

	@FXML
	private TableColumn<DayEndClosureDtl, Number> clDenomination;

	@FXML
	private TableColumn<DayEndClosureDtl, Number> clCount;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnPhonePay;

	@FXML
	private Button btnSeodexo;
	@FXML
	private DatePicker dpProcessDate;

	@FXML
	private Button btnNeftSales;
	@FXML
	private Button btnFetch;

	@FXML
	private Button btnClear;
	@FXML
	private TextField txtChangeInCash;
	@FXML
	private TextField txtPhysicalCash;

	@FXML
	private Button btnSave;
	@FXML
	private TextField txtNeftSales;
	@FXML
	private Button report;

	@FXML
	private TextField txtPhonePay;

	@FXML
	private TextField txtSodexo;
	@FXML
	private DatePicker dpReportDate;

	@FXML
	private Button btnGenerateReport;

	@FXML
	private Button btnPrint;

	// ---------------------------session end ---------------------------------

	@FXML
	private TextField txtSlNo;

	@FXML
	private TextField txtSessionCashSale;

	@FXML
	private TextField txtSessionCardSale;

	@FXML
	private TextField txtSessionDenomination;

	@FXML
	private TextField txtSessionCount;

	@FXML
	private Button btnSessionAdd;

	@FXML
	private TableView<SessionEndDtl> tblSessionEnd;

	@FXML
	private TableColumn<SessionEndDtl, Number> clSessionDenomination;

	@FXML
	private TableColumn<SessionEndDtl, Number> clSessionCount;

	@FXML
	private Button btnSessionDelete;

	@FXML
	private Button btnSessionFetchDate;

	@FXML
	private Button btnSessionClear;

	@FXML
	private TextField txtSessionPhysicalCash;

	@FXML
	private Button btnSessionSave;

	@FXML
	private Button btnSessionReport;

	@FXML
	private TextField txtSessionDate;
	@FXML
	private TextField txtStartingbillNo;

	@FXML
	private TextField txtEndingBillNo;

	@FXML
	private TextField txtTotalBill;

	@FXML
	private TextField txtOpeningPOsBilNo;

	@FXML
	private Button btnPreviousCardSale;

	@FXML
	private TextField txtPreviousCardSale;
	@FXML
	private TextField txtClosingBillNo;

	@FXML
	private TextField txtOpeningWholeSaleBillNo;
	@FXML
	private Button btnTodaysCashSale;

	@FXML
	private Button btnPreviousCashSO;

	@FXML
	private TextField txtTodaysCashSale;

	@FXML
	private TextField txtPreviousCashSo;
	@FXML
	private TextField txtClosingWholeSaleBillNo;

	@FXML
	private TextField txtCashSalesOrder;

	@FXML
	private TextField txtCreditTocash;

	@FXML
	private Button btnCredittoCash;
	@FXML
	private TextField txtCardSalesOrder;

	@FXML
	private TextField txtCashSales;

	@FXML
	private Button btnStartingBillNo;

	@FXML
	private Button btnOpeningWholeSaleBillNo;

	@FXML
	private Button btnTotalBill;

	@FXML
	private Button btnClosingBillNo;

	@FXML
	private Button btnOpeningPOsBillNo;

	@FXML
	private Button btnEndingbillNo;

	@FXML
	private Button btnCardSalesOrder;

	@FXML
	private Button btnCashSalesOrder;

	@FXML
	private Button btnCashSales;

	@FXML
	private Button btnGpaySales;

	@FXML
	private Button btnEbsales;

	@FXML
	private Button btnPaytmSales;

	@FXML
	private Button btncardSales;

	@FXML
	private Button btnCreditSales;

	@FXML
	private Button btnTotalCash;

	@FXML
	private Button btnSwiggySales;

	@FXML
	private Button btnZomatoSales;

	@FXML
	private Button btnTotalSales;

	@FXML
	private Button btnClosingetttyCash;

	@FXML
	private Button btnTotalCashAtDrawer;

	@FXML
	private Button btnCashVariance;

	@FXML
	private DatePicker dpReportDate1;
	@FXML
	private TextField txtEbs;

	@FXML
	private TextField txtSwiggy;

	@FXML
	private TextField txtZomato;

	@FXML
	private TextField txtTotalSales;

	@FXML
	private TextField txtClosingPettyCash;

	@FXML
	private TextField txtTotalCash;

	@FXML
	private TextField txtGpay;

	@FXML
	private TextField txtPaytm;

	@FXML
	private TextField txtTotalPOsBill;
	@FXML
	private TextField txtTotalWholesaleBill;
	@FXML
	private TextField txtCardSales;

	@FXML
	private TextField txtCreditSales;

	@FXML
	private TextField txtTotalcashAtDrawer;

	@FXML
	private TextField txtCardSettlement;

	@FXML
	private TextField txtCashVariancenew;

	@FXML
	private ComboBox<String> cmbDenomination;

//    @FXML
//    void FocusOnCount(KeyEvent event) {
//
//    }

	@FXML
	void FocusOnSessionAddButton(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSessionAdd.requestFocus();
		}
	}

	@FXML
	void FocusOnSessionCount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtSessionCount.requestFocus();
		}
	}

	@FXML
	void SessionAddDayEndDtl(ActionEvent event) {
		AddSessionEnd();
	}

	@FXML
	void SessionAddItem(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			AddSessionEnd();
		}

	}

	private void AddSessionEnd() {
		if (txtSessionDate.getText().trim().isEmpty()) {
			notifyMessage(5, "Invalid date", false);
			return;
		}

		if (null == SystemSetting.getUser().getId()) {
			notifyMessage(5, "Invalid user", false);
			return;
		}

		ResponseEntity<UserMst> userResp = RestCaller.getUserNameById(SystemSetting.getUser().getId());
		UserMst userMst = userResp.getBody();

		if (null == userMst) {
			notifyMessage(5, "Invalid user", false);
			return;
		}
		String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");

		if (null == sessionEndClosureMst) {
			Integer slNo = RestCaller.findLastUpdatedSessionSlNoByDate(sdate);

			sessionEndClosureMst = new SessionEndClosureMst();
			sessionEndClosureMst.setUserId(userMst.getId());
			sessionEndClosureMst.setBranchCode(SystemSetting.systemBranch);
			sessionEndClosureMst.setSlNo(1);
			if (null != slNo) {
				sessionEndClosureMst.setSlNo(slNo + 1);
			}

			sessionEndClosureMst.setSessionDate(SystemSetting.systemDate);

			ResponseEntity<SessionEndClosureMst> saved = RestCaller.saveSessionEndMst(sessionEndClosureMst);
			sessionEndClosureMst = saved.getBody();

		}

		if (null == sessionEndClosureMst) {
			sessionEndClosureMst = null;
			return;
		}

		sessionEndDtl = new SessionEndDtl();
		sessionEndDtl.setCount(Double.parseDouble(txtSessionCount.getText()));
		sessionEndDtl.setDenomination(Double.parseDouble(txtSessionDenomination.getText()));
		sessionEndDtl.setSessionEndClosure(sessionEndClosureMst);

		ResponseEntity<SessionEndDtl> sessionSaved = RestCaller.saveSessionDtl(sessionEndDtl);
		sessionEndDtl = sessionSaved.getBody();
		if (null == sessionEndDtl) {
			notifyMessage(5, "not saved", false);
		}

		showAll();
		txtSessionCount.clear();
		txtSessionDenomination.clear();
		sessionEndDtl = null;

		txtSessionDenomination.requestFocus();

	}

	private void showAll() {
		if (null != sessionEndClosureMst) {
			if (null != sessionEndClosureMst.getId()) {
				sessionEndDtlList.clear();
				ResponseEntity<List<SessionEndDtl>> sessionEndDtlListResp = RestCaller
						.findSessionEndDtlByHdrId(sessionEndClosureMst.getId());
				sessionEndDtlList = FXCollections.observableArrayList(sessionEndDtlListResp.getBody());
				FillTableSessionDtl();

			}
		}
	}

	private void FillTableSessionDtl() {

		tblSessionEnd.setItems(sessionEndDtlList);
		clSessionCount.setCellValueFactory(cellData -> cellData.getValue().getCountProperty());
		clSessionDenomination.setCellValueFactory(cellData -> cellData.getValue().getDenominationProperty());

		Double physicalCash = RestCaller.findTotalAmountOfSessionEndByHdrId(sessionEndClosureMst.getId());
		BigDecimal bdCashToPay = new BigDecimal(physicalCash);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);

		txtSessionPhysicalCash.setText(bdCashToPay.toString());

	}

	@FXML
	void SessionClearAll(ActionEvent event) {

	}

	@FXML
	void SessionDeleteDayEndDtl(ActionEvent event) {

		if (null != sessionEndDtl) {
			if (null != sessionEndDtl.getId()) {
				RestCaller.deleteSessionDtl(sessionEndDtl.getId());
				showAll();
			}
		}

	}

	@FXML
	void SessionFetchData(ActionEvent event) {

	}

	@FXML
	void SessionFinalSave(ActionEvent event) {

		if (null != sessionEndClosureMst) {

			ResponseEntity<DayEndClosureHdr> totalCashAndCardSale = RestCaller
					.getTotalCashAndCardSale(SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd"));

			DayEndClosureHdr dayEndClosureHdr = new DayEndClosureHdr();
			dayEndClosureHdr = totalCashAndCardSale.getBody();
			if (null != dayEndClosureHdr) {
				sessionEndClosureMst.setCardSale(dayEndClosureHdr.getCardSale());
				sessionEndClosureMst.setCardSale2(dayEndClosureHdr.getCardSale2());
				sessionEndClosureMst.setCashSale(dayEndClosureHdr.getCashSale());
			}

			sessionEndClosureMst.setPhysicalCash(Double.parseDouble(txtSessionPhysicalCash.getText()));

			RestCaller.updateSessinEndMst(sessionEndClosureMst);
			try {
				JasperPdfReportService.PrintSessionEndReport(sessionEndClosureMst, txtSessionDate.getText());
			} catch (Exception e) {
				System.out.println(e);

			}

			txtSessionPhysicalCash.clear();
			tblSessionEnd.getItems().clear();
			sessionEndClosureMst = null;

		}

	}

	@FXML
	void SessionShowReport(ActionEvent event) {

		if (txtSlNo.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter SlNo", false);
			return;
		}

		if (txtSessionDate.getText().trim().isEmpty()) {
			notifyMessage(5, "Invalid date", false);
			return;
		}

		ResponseEntity<SessionEndClosureMst> sessionSaved = RestCaller
				.findSessionEndClosureMstBySlNoAndDate(txtSlNo.getText(), txtSessionDate.getText());
		SessionEndClosureMst sessionEndClosureMst = sessionSaved.getBody();

		try {

			JasperPdfReportService.PrintSessionEndReport(sessionEndClosureMst, txtSessionDate.getText());
		} catch (Exception e) {
			System.out.println(e);
		}
		sessionEndClosureMst = null;
		txtSlNo.clear();
	}

	@FXML
	private void initialize() {
		
		txtCardSettlement.setText("0");

		cmbDenomination.getItems().add("2000");
		cmbDenomination.getItems().add("1000");
		cmbDenomination.getItems().add("500");
		cmbDenomination.getItems().add("200");
		cmbDenomination.getItems().add("100");
		cmbDenomination.getItems().add("50");
		cmbDenomination.getItems().add("20");
		cmbDenomination.getItems().add("10");
		cmbDenomination.getItems().add("COIN");

		dpProcessDate = SystemSetting.datePickerFormat(dpProcessDate, "dd/MMM/yyyy");
		dpReportDate = SystemSetting.datePickerFormat(dpReportDate, "dd/MMM/yyyy");
		dpReportDate1 = SystemSetting.datePickerFormat(dpReportDate1, "dd/MMM/yyyy");
		dpProcessDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		dpProcessDate.setEditable(false);
		txtSessionDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd"));

		tblSessionEnd.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					sessionEndDtl = new SessionEndDtl();
					sessionEndDtl.setId(newSelection.getId());
				}
			}
		});

		tblDayEnd.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					dayEndClosureDtl = new DayEndClosureDtl();
					dayEndClosureDtl.setId(newSelection.getId());
				}
			}
		});

	}

//-------------------------------------------------------------------------------------------

	@FXML
	void PrintReport(ActionEvent event) {

		if (null == dpReportDate.getValue()) {
			notifyMessage(5, " Please select report date...!!!", false);
			dpReportDate.requestFocus();
			return;
		}

		java.util.Date udate = SystemSetting.localToUtilDate(dpReportDate.getValue());
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		ResponseEntity<DayEndClosureHdr> getDayEnd = RestCaller.getDayEndClosureByDate(sdate);
		if (null != getDayEnd.getBody()) {
			if (null == getDayEnd.getBody().getDayEndStatus()) {
				notifyMessage(5, " Please Save Day End Before Taking PrintOut...!!!", false);
				return;
			}
		}
		try {

			// JasperPdfReportService.dayEndReportByDate(sdate);

			// ---------------version 4.11

			System.out.println("----------day_end_format--------" + SystemSetting.day_end_format);

			if (SystemSetting.day_end_format.equalsIgnoreCase("YES")) {
				JasperPdfReportService.DayEndThermalReport(sdate);

			} else {
				// ---------------version 4.11 end

				NewJasperPdfReportService.getDayend(SystemSetting.systemBranch, sdate);
				// ---------------version 4.11

			}
			// ---------------version 4.11 end

			// JasperPdfReportService.getDayend(SystemSetting.systemBranch,sdate);
		} catch (Exception e) {
			System.out.println(e);

		}

	}

	@FXML
	void GenerateReport(ActionEvent event) {

		if (null == dpReportDate.getValue()) {
			notifyMessage(5, " Please select report date...!!!", false);
			dpReportDate.requestFocus();
			return;
		}

		java.util.Date udate = SystemSetting.localToUtilDate(dpReportDate.getValue());
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

		DayEndReportByDate(sdate);

		BillSeries(sdate);

		SalesSummary(sdate);

		PaymentSummary(sdate);

		ReceiptSummary(sdate);

		// version 1.9

		Double variance = 0.0;
		for (DayEndClosureHdr dayend : dayEndClosureHdrList) {
			variance = variance + dayend.getCashSale();
		}
		for (DailyReceiptsSummaryReport recp : receiptReportList) {
			variance = variance + recp.getTotalCash();
		}
		ResponseEntity<List<ReceiptModeReport>> saleOrderResceiptModeReport = RestCaller
				.getSaleOrderReceiptModeReport(sdate);
		List<ReceiptModeReport> saleOrderReceiptModeReport = saleOrderResceiptModeReport.getBody();

		for (ReceiptModeReport rept : saleOrderReceiptModeReport) {
			variance = variance + rept.getAmount();
		}
		for (DayEndClosureHdr dayend : dayEndClosureHdrList) {
			variance = variance - dayend.getPhysicalCash();
		}
		txtCashVariance.setText(Double.toString(variance));
		// version 1.9 ends

	}

	private void BillSeries(String sdate) {

		// ------------------version 1.5
		// ResponseEntity<List<SalesModeWiseBillReport>> billSeries =
		// RestCaller.BillSeriesDayEnd(sdate);

		ResponseEntity<List<SalesModeWiseBillReport>> billSeries = RestCaller.BillSeriesDayEndByInvoiceSeries(sdate);
		// ------------------version 1.5 end

		billSeriesList = FXCollections.observableArrayList(billSeries.getBody());

		tblBillSeries.setItems(billSeriesList);
		clBillBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
		clMinVoucher.setCellValueFactory(cellData -> cellData.getValue().getMinNoProperty());
		clMaxVoucher.setCellValueFactory(cellData -> cellData.getValue().getMaxNoProperty());
		clBillCount.setCellValueFactory(cellData -> cellData.getValue().getCountProperty());
		clBillSalesMode.setCellValueFactory(cellData -> cellData.getValue().getSalesModeProperty());

	}

	private void ReceiptSummary(String sdate) {
		ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(sdate);
		receiptReportList = FXCollections.observableArrayList(receiptReport.getBody());

		tblReceipt.setItems(receiptReportList);

		clReceiptCash.setCellValueFactory(cellData -> cellData.getValue().getTotalCashProperty());
		clReceiptMode.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());
		clDebitAmount.setCellValueFactory(cellData -> cellData.getValue().getDebitAccountProperty());

		clReceiptCash.setCellFactory(tc -> new TableCell<DailyReceiptsSummaryReport, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

	}

	private void PaymentSummary(String sdate) {

		ResponseEntity<List<DailyPaymentSummaryReport>> paymentReport = RestCaller.PaymentSummaryDayEnd(sdate);
		paymentReportList = FXCollections.observableArrayList(paymentReport.getBody());

		tblPayment.setItems(paymentReportList);

		clPaymentMode.setCellValueFactory(cellData -> cellData.getValue().getPaymentModeProperty());
		clCreditAmount.setCellValueFactory(cellData -> cellData.getValue().getCreditAccountProperty());

		clPaymentCash.setCellValueFactory(cellData -> cellData.getValue().getTotalCashProperty());

		clPaymentCash.setCellFactory(tc -> new TableCell<DailyPaymentSummaryReport, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

	}

	private void SalesSummary(String sdate) {

		ResponseEntity<List<SalesReport>> salesReport = RestCaller.getSalesSummaryReportDayEnd(sdate);

		salesReportList = FXCollections.observableArrayList(salesReport.getBody());

		Double totalAmount = 0.0;
		Double invoiceAmount = 0.0;
		Double creditAmount = 0.0;

		for (SalesReport sales : salesReportList) {
			totalAmount = totalAmount + sales.getCard() + sales.getCash();
			invoiceAmount = invoiceAmount + sales.getInvovicetotal();
			creditAmount = invoiceAmount - totalAmount;

			sales.setCreditAmount(creditAmount);
		}

		tblSalesSummary.setItems(salesReportList);

		clSalesCredit.setCellValueFactory(cellData -> cellData.getValue().getCreditAmountProperty());

		clSalesBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
		clSalesCard.setCellValueFactory(cellData -> cellData.getValue().getCardProperty());
		clSalesCash.setCellValueFactory(cellData -> cellData.getValue().getCashProperty());
		clSalesTotalAmount.setCellValueFactory(cellData -> cellData.getValue().getTotalProperty());
		clSalesInvoiceAmount.setCellValueFactory(cellData -> cellData.getValue().getInvovicetotalProperty());

		clSalesCredit.setCellFactory(tc -> new TableCell<SalesReport, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		clSalesCard.setCellFactory(tc -> new TableCell<SalesReport, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

		clSalesCash.setCellFactory(tc -> new TableCell<SalesReport, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

		clSalesTotalAmount.setCellFactory(tc -> new TableCell<SalesReport, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

		clSalesInvoiceAmount.setCellFactory(tc -> new TableCell<SalesReport, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

	}

	private void DayEndReportByDate(String sdate) {

		ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(sdate);
		dayEndClosureHdrList = FXCollections.observableArrayList(dayEndReport.getBody());

		for (DayEndClosureHdr hdr : dayEndClosureHdrList) {

			ResponseEntity<List<SalesReport>> salesReportResp = RestCaller.getSalesSummaryReportDayEnd(sdate);
			List<SalesReport> salesReportDayend = salesReportResp.getBody();

			for (SalesReport sales : salesReportDayend) {
				hdr.setCardSale(sales.getCard());
//				dayEndClosure.setCardSale2(sales.getCardSale2());
				hdr.setCashSale(sales.getCash());
			}

			ResponseEntity<List<DailyPaymentSummaryReport>> paymentReport = RestCaller.PaymentSummaryDayEnd(sdate);
			List<DailyPaymentSummaryReport> paymentSummaryReport = paymentReport.getBody();

			for (DailyPaymentSummaryReport payment : paymentSummaryReport) {
				if (payment.getPaymentMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
					Double cash = 0.0;
					cash = payment.getTotalCash();
					if (null != hdr.getCashSale()) {
						cash = cash + hdr.getCashSale();
					}
					hdr.setCashSale(cash);
				}

			}

			ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(sdate);

			List<DailyReceiptsSummaryReport> receiptSummary = receiptReport.getBody();

			for (DailyReceiptsSummaryReport receipts : receiptSummary) {
				if (receipts.getReceiptMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
					Double cash = 0.0;
					cash = receipts.getTotalCash();
					if (null != hdr.getCashSale()) {
						cash = cash + hdr.getCashSale();
					}
					hdr.setCashSale(cash);
				}

				if (receipts.getReceiptMode().equalsIgnoreCase("CARD")) {
					Double card = 0.0;
					card = receipts.getTotalCash();
					if (null != hdr.getCardSale()) {
						card = card + hdr.getCardSale();
					}
					hdr.setCardSale(card);
				}

			}

			if (null != hdr.getUserId()) {
				ResponseEntity<UserMst> userResp = RestCaller.getUserNameById(hdr.getUserId());
				UserMst userMst = userResp.getBody();
				if (null != userMst) {
					hdr.setUserName(userMst.getUserName());

				}
			}

		}
		// version 1.9
		// Restcaller for pettyCash

		ResponseEntity<AccountHeads> accountHeads = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-PETTY CASH");
		AccountHeads accountHeads1 = accountHeads.getBody();

		ResponseEntity<List<AccountBalanceReport>> accountBalance = RestCaller.getAccountBalanceReport(sdate, sdate,
				accountHeads1.getId());

		java.util.Date udate = SystemSetting.localToUtilDate(dpReportDate.getValue());
		String strDate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		ResponseEntity<List<ReceiptModeReport>> saleOrderResceiptModeReport = RestCaller
				.getSaleOrderReceiptModeReport(strDate);
		List<ReceiptModeReport> saleOrderReceiptModeReport = saleOrderResceiptModeReport.getBody();

		for (DayEndClosureHdr dyend : dayEndClosureHdrList) {
			for (AccountBalanceReport acc : accountBalance.getBody()) {
				dyend.setPettyCash(acc.getClosingBalance());

			}
		}

//    	ResponseEntity<List<SalesReport>> salesReport = RestCaller.getSalesSummaryReportDayEnd(sdate);
//    		
//    		salesReportList = FXCollections.observableArrayList(salesReport.getBody());
//        	
//        	for (SalesReport salesReprt: salesReportList)
//        	{
//        		for(DayEndClosureHdr dayEnd :dayEndClosureHdrList )
//        		{
//        			dayEnd.setCashSale(salesReprt.getCash());
//        			dayEnd.setCardSale(salesReprt.getCard());
//        		}
//        	}
		tblPhysicalCash.setItems(dayEndClosureHdrList);
		clPhysicalUser.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
		clPhysicalBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
		clPhysicalCard.setCellValueFactory(cellData -> cellData.getValue().getCardSaleProperty());
		clPhysicalCash.setCellValueFactory(cellData -> cellData.getValue().getPhysicalCashProperty());
		clCash.setCellValueFactory(cellData -> cellData.getValue().getCashSaleProperty());

		clPettyCash.setCellValueFactory(cellData -> cellData.getValue().getpettyCashProperty());

		// version 1.9 ends

		clPhysicalCard.setCellFactory(tc -> new TableCell<DayEndClosureHdr, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

		clPhysicalCash.setCellFactory(tc -> new TableCell<DayEndClosureHdr, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		clCash.setCellFactory(tc -> new TableCell<DayEndClosureHdr, Number>() {
			@Override
			protected void updateItem(Number value, boolean empty) {
				if (value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
	}

	@FXML
	private TableView<SalesReport> tblSalesSummary;

	@FXML
	private TableColumn<SalesReport, Number> clSalesCredit;

	@FXML
	private TableColumn<SalesReport, String> clSalesBranch;

	@FXML
	private TableColumn<SalesReport, Number> clSalesCash;

	@FXML
	private TableColumn<SalesReport, Number> clSalesCard;

	@FXML
	private TableColumn<SalesReport, Number> clSalesTotalAmount;

	@FXML
	private TableColumn<SalesReport, Number> clSalesInvoiceAmount;

	@FXML
	private TableView<DayEndClosureHdr> tblPhysicalCash;

	@FXML
	private TableColumn<DayEndClosureHdr, String> clPhysicalBranch;

	@FXML
	private TableColumn<DayEndClosureHdr, String> clPhysicalUser;

	@FXML
	private TableColumn<DayEndClosureHdr, Number> clCash;

	@FXML
	private TableColumn<DayEndClosureHdr, Number> clPhysicalCard;

	// version 1.9
	@FXML
	private TableColumn<DayEndClosureHdr, Number> clPettyCash;
	// version 1.9 ends

	@FXML
	private TableColumn<DayEndClosureHdr, Number> clPhysicalCash;

	@FXML
	private TableView<DailyPaymentSummaryReport> tblPayment;

	@FXML
	private TableColumn<DailyPaymentSummaryReport, String> clPaymentMode;

	@FXML
	private TableColumn<DailyPaymentSummaryReport, Number> clPaymentCash;

	@FXML
	private TableColumn<DailyPaymentSummaryReport, String> clCreditAmount;

	@FXML
	private TableView<DailyReceiptsSummaryReport> tblReceipt;

	@FXML
	private TableColumn<DailyReceiptsSummaryReport, String> clReceiptMode;

	@FXML
	private TableColumn<DailyReceiptsSummaryReport, Number> clReceiptCash;

	@FXML
	private TableColumn<DailyReceiptsSummaryReport, String> clDebitAmount;

	@FXML
	private TableView<SalesModeWiseBillReport> tblBillSeries;

	@FXML
	private TableColumn<SalesModeWiseBillReport, String> clBillBranch;

	@FXML
	private TableColumn<SalesModeWiseBillReport, String> clMinVoucher;

	@FXML
	private TableColumn<SalesModeWiseBillReport, String> clMaxVoucher;

	@FXML
	private TableColumn<SalesModeWiseBillReport, String> clBillSalesMode;

	@FXML
	private TableColumn<SalesModeWiseBillReport, String> clBillCount;

	@FXML
	void AddDayEndDtl(ActionEvent event) {
		addDayEndClosure();

	}

	private void addDayEndClosure() {

		if (null == dpProcessDate.getValue()) {
			notifyMessage(5, " Please select process date...!!!", false);
			dpProcessDate.requestFocus();
			return;
		}
		if (null == cmbDenomination.getValue() || cmbDenomination.getSelectionModel().getSelectedItem().isEmpty()) {
			notifyMessage(5, " Please enter Denomination...!!!", false);
			cmbDenomination.requestFocus();
			return;
		}
		if (txtCount.getText().trim().isEmpty()) {
			notifyMessage(5, " Please enter Count...!!!", false);
			txtCount.requestFocus();
			return;
		}
		LocalDate dpDate = dpProcessDate.getValue();
		java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
		if (null != dayEndClosuredtlSaved.getBody()) {
			if (null != dayEndClosuredtlSaved.getBody().getDayEndStatus()) {
				notifyMessage(5, "Day End Already Done", false);
				cmbDenomination.getSelectionModel().clearSelection();
				txtCount.clear();

				return;
			}
		}

		ResponseEntity<DayEndClosureHdr> dayEndresp = RestCaller.getDayEndClosureByDate(strDate);
		dayEndClosure = dayEndresp.getBody();
//		if (null != dayEndClosure) {
//			// btnAdd.setDisable(true);
//			// btnDelete.setDisable(true);
//			// btnSave.setDisable(true);
//			BigDecimal cardSale = new BigDecimal(dayEndClosure.getCardSale());
//			cardSale = cardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//			txtCardSale.setText(cardSale.toPlainString());
//			// txtCardSale2.setText(Double.toString(dayEndClosure.getCardSale2()));
//			BigDecimal cashSale = new BigDecimal(dayEndClosure.getCashSale());
//			cashSale = cashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//			txtCashSale.setText(cashSale.toPlainString());
//			// txtPhysicalCash.setText(Double.toString(dayEndClosure.getPhysicalCash()));
//			getDayEndDtlByHdrId();
//
//		}
		if (null == dayEndClosure) {

//			ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
//			if (null != dayEndClosuredtlSaved.getBody()) {
//				if (null != dayEndClosuredtlSaved.getBody().getDayEndStatus()) {
//					notifyMessage(5, "Day End Already Done");
//					txtDenomination.clear();
//					txtCount.clear();
//
//					return;
//				}
//			}
			dayEndClosure = new DayEndClosureHdr();
			Date date = Date.valueOf(dpProcessDate.getValue());
			dayEndClosure.setBranchCode(SystemSetting.getSystemBranch());
			System.out.println(SystemSetting.getUser());

			System.out.println(SystemSetting.getUser().getId());
			dayEndClosure.setUserId(SystemSetting.getUser().getId());
			dayEndClosure.setProcessDate(date);
			if (!txtChangeInCash.getText().trim().isEmpty()) {
				dayEndClosure.setChangeInCash(Double.parseDouble(txtChangeInCash.getText()));
			} else {
				dayEndClosure.setChangeInCash(0.0);
			}
			ResponseEntity<DayEndClosureHdr> respentity = RestCaller.saveDayEndClosureHdr(dayEndClosure);
			dayEndClosure = respentity.getBody();
		}

		dayEndClosureDtl = new DayEndClosureDtl();
		dayEndClosureDtl.setDayEndClosure(dayEndClosure);

		String denominationStr = cmbDenomination.getSelectionModel().getSelectedItem().toString();

		if (denominationStr.equalsIgnoreCase("COIN")) {
			denominationStr = "1";
		}

		Double denomination = Double.parseDouble(denominationStr);

		dayEndClosureDtl.setDenomination(denomination);
		dayEndClosureDtl.setCount(Double.parseDouble(txtCount.getText()));

		ResponseEntity<DayEndClosureDtl> respentity = RestCaller.saveDayEndClosureDtl(dayEndClosureDtl);
		dayEndClosureDtl = respentity.getBody();
		
		ResponseEntity<List<DayEndClosureDtl>> dayEndClosureDtlListResp = RestCaller.getDayEndClosureDtl(dayEndClosure);
		
		dayEndClosureListTable = FXCollections.observableArrayList(dayEndClosureDtlListResp.getBody());

		FillTable();
		// dayEndClosureDtl = null;
		cmbDenomination.getSelectionModel().clearSelection();
		txtCount.clear();
		cmbDenomination.requestFocus();
		notifyMessage(5, "Saved!!", true);

	}

	@FXML
	void ClearAll(ActionEvent event) {

		tblDayEnd.getItems().clear();
		btnAdd.setDisable(false);
		btnDelete.setDisable(false);
		btnSave.setDisable(false);
		txtCardSale.clear();
		txtPhysicalCash.clear();

		txtCashSale.clear();
		txtCount.clear();
		cmbDenomination.getSelectionModel().clearSelection();
		dpProcessDate.setValue(null);
		dayEndClosure = null;
		dayEndClosureDtl = null;
		
		txtCardSettlement.setText("0");

	}

	@FXML
	void DeleteDayEndDtl(ActionEvent event) {
		if (null != dayEndClosureDtl) {
			RestCaller.dayEndClosureDtlDelete(dayEndClosureDtl.getId());

			getDayEndDtlByHdrId();

		}
	}

	@FXML
	void FetchData(ActionEvent event) {
		LocalDate dpDate = dpProcessDate.getValue();
		System.out.println(dpProcessDate.getValue()
				+ "date picker value issssssssssssssssssssssssssssssssssssssssssssssssssssss&&&&&&&&&&&&&&&&&");
		java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
		dayEndClosure = dayEndClosuredtlSaved.getBody();
		if (null != dayEndClosure) {
			// btnAdd.setDisable(true);
			// btnDelete.setDisable(true);
			// btnSave.setDisable(true);
			
			 if(null !=dayEndClosure.getDayEndStatus()) {
			BigDecimal cardSale = new BigDecimal(dayEndClosure.getCardSale());
			cardSale = cardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtCardSale.setText(cardSale.toPlainString());
			// txtCardSale2.setText(Double.toString(dayEndClosure.getCardSale2()));
			BigDecimal cashSale = new BigDecimal(dayEndClosure.getCashSale());
			cashSale = cashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtCashSale.setText(cashSale.toPlainString());
			// txtPhysicalCash.setText(Double.toString(dayEndClosure.getPhysicalCash()));
			 }
			getDayEndDtlByHdrId();

		} else {

			tblDayEnd.getItems().clear();
			notifyMessage(5, " No data to show...!!!", false);
			txtCardSale.clear();

			txtCashSale.clear();
			txtCount.clear();
			cmbDenomination.getSelectionModel().clearSelection();
			txtPhysicalCash.clear();
		}
	}

	@FXML
	void FinalSave(ActionEvent event) throws JRException {

		if (txtCardSettlement.getText().trim().isEmpty()) {
			notifyMessage(2, "Please enter card settlement amount", false);
			txtCardSettlement.requestFocus();
			return;
		}

		if (stock_count > 0) {
			LocalDate dpDate = dpProcessDate.getValue();
			java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
			String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

			ResponseEntity<StockVerificationMst> stockMst = RestCaller.getStockVerificationMst(strDate);
			if (null != stockMst.getBody()) {
				notifyMessage(5, "Please Enter the Stock Verification Details", false);
				return;
			}
		}

		if (null != dayEndClosure) {
			LocalDate dpDate = dpProcessDate.getValue();
			java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
			String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

			ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
			if (null != dayEndClosuredtlSaved.getBody()) {
				if (null != dayEndClosuredtlSaved.getBody().getDayEndStatus()) {
					notifyMessage(5, "Day End Already Done", false);

					return;
				}
			}

			//

			ResponseEntity<List<SalesReport>> salesReportResp = RestCaller.getSalesSummaryReportDayEnd(strDate);
			List<SalesReport> salesReport = salesReportResp.getBody();

			for (SalesReport sales : salesReport) {
				dayEndClosure.setCardSale(sales.getCard());
//				dayEndClosure.setCardSale2(sales.getCardSale2());
				dayEndClosure.setCashSale(sales.getCash());
			}

			ResponseEntity<List<DailyPaymentSummaryReport>> paymentReport = RestCaller.PaymentSummaryDayEnd(strDate);
			List<DailyPaymentSummaryReport> paymentSummaryReport = paymentReport.getBody();

			for (DailyPaymentSummaryReport payment : paymentSummaryReport) {
				if (payment.getPaymentMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
					Double cash = 0.0;
					cash = payment.getTotalCash();
					if (null != dayEndClosure.getCashSale()) {
						cash = cash + dayEndClosure.getCashSale();
					}
					dayEndClosure.setCashSale(cash);
				}

			}

			ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(strDate);

			List<DailyReceiptsSummaryReport> receiptSummary = receiptReport.getBody();

			for (DailyReceiptsSummaryReport receipts : receiptSummary) {
				if (receipts.getReceiptMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
					Double cash = 0.0;
					cash = receipts.getTotalCash();
					if (null != dayEndClosure.getCashSale()) {
						cash = cash + dayEndClosure.getCashSale();
					}
					dayEndClosure.setCashSale(cash);
				}

				if (receipts.getReceiptMode().equalsIgnoreCase("CARD")) {
					Double card = 0.0;
					card = receipts.getTotalCash();
					if (null != dayEndClosure.getCardSale()) {
						card = card + dayEndClosure.getCardSale();
					}
					dayEndClosure.setCardSale(card);
				}

			}

//			ResponseEntity<DayEndClosureHdr> totalCashAndCardSale = RestCaller.getTotalCashAndCardSale(strDate);
//			DayEndClosureHdr dayEndClosureHdr = new DayEndClosureHdr();
//			dayEndClosureHdr = totalCashAndCardSale.getBody();
//			if (null != dayEndClosureHdr) {
//				dayEndClosure.setCardSale(dayEndClosureHdr.getCardSale());
//				dayEndClosure.setCardSale2(dayEndClosureHdr.getCardSale2());
//				dayEndClosure.setCashSale(dayEndClosureHdr.getCashSale());
//			}
			if (txtChangeInCash.getText().trim().isEmpty()) {
				dayEndClosure.setChangeInCash(0.0);
			} else {
				dayEndClosure.setChangeInCash(Double.parseDouble(txtChangeInCash.getText()));
			}
			dayEndClosure.setPhysicalCash(Double.parseDouble(txtPhysicalCash.getText()));

//			dayEndClosure.setCardSale(Double.parseDouble(txtCashSale.getText()));
//			dayEndClosure.setCardSale(Double.parseDouble(txtCardSale.getText()));

			dayEndClosure.setDayEndStatus("DONE");

			dayEndClosure.setCardSettlementAmount(Double.parseDouble(txtCardSettlement.getText()));

			RestCaller.updateDayEndClosure(dayEndClosure);

			ResponseEntity<SysDateMst> respetnity = RestCaller.getSysDate(SystemSetting.systemBranch);
//			if(null == respetnity.getBody())
//			{
//				SysDateMst sysDateMst = new SysDateMst();
//				if(null != sysDateMst.getAplicationDate())
//				{
//				LocalDate ldate =SystemSetting.utilToLocaDate(sysDateMst.getAplicationDate());
//				
//				sysDateMst.setAplicationDate(SystemSetting.localToUtilDate(ldate.plusDays(1)));
//				}
//				else
//				{
//					sysDateMst.setAplicationDate(Date.valueOf(LocalDate.now().plusDays(1)));
//				}
//				sysDateMst.setBranchCode(SystemSetting.systemBranch);
//				sysDateMst.setSystemDate(Date.valueOf(LocalDate.now()));
//				ResponseEntity<SysDateMst> respentity = RestCaller.saveSysDateMst(sysDateMst);
//				
//			}
//			else
//			{
			SysDateMst sysDateMst = new SysDateMst();
			sysDateMst = respetnity.getBody();
//				LocalDate ldate =SystemSetting.utilToLocaDate(dpProcessDate.getValue());
			sysDateMst.setAplicationDate(SystemSetting.localToUtilDate(dpProcessDate.getValue().plusDays(1)));
			sysDateMst.setSystemDate(Date.valueOf(LocalDate.now()));
			sysDateMst.setBranchCode(SystemSetting.systemBranch);
			RestCaller.updateSysDate(sysDateMst);

//			ResponseEntity<SysDateMst> respetnity1 = RestCaller
//					.getSysDate(SystemSetting.systemBranch);
//			
//			SysDateMst sysDateMst = null;
//			sysDateMst = respetnity1.getBody();
//			if (sysDateMst != null) {
//				System.out.println("SysDate Mst"+sysDateMst);
//				sysDateMst.setAplicationDate(SystemSetting.localToUtilDate(dpProcessDate.getValue().plusDays(1)));
//				sysDateMst.setSystemDate(java.sql.Date.valueOf(LocalDate.now()));
//				sysDateMst.setBranchCode(SystemSetting.systemBranch);
//				//RestCaller.updateSysDate(sysDateMst);
//				if (sysDateMst.getSystemDate() != sysDateMst.getAplicationDate()) {
//					//notifyMessage(5, " System Date and Application Dates are different");
//				}
//
//			}
//			else {
//				sysDateMst = new SysDateMst();
//				sysDateMst.setAplicationDate(SystemSetting.localToUtilDate(dpProcessDate.getValue().plusDays(1)));
//				sysDateMst.setBranchCode(SystemSetting.systemBranch);
//				sysDateMst.setSystemDate(java.sql.Date.valueOf(LocalDate.now()));
//				ResponseEntity<SysDateMst> respentity = RestCaller.saveSysDateMst(sysDateMst);
//				System.out.println("New ---SysDate Mst"+sysDateMst);
//				System.out.println();
//			}
			/*
			 * Save Day end Store
			 */

			ResponseEntity<DayEndReportStore> dayendreportStore = RestCaller.getDayEndReportFromStore(strDate);
			if (null == dayendreportStore.getBody()) {
				saveDayEndReportStore();

			}

			// Send Sms
			if (SMSENABLE.equalsIgnoreCase("YES")) {
				ResponseEntity<DayEndReportStore> dayEndReportStore = RestCaller.getDayEndReportFromStore(strDate);
				String physicalCash = "0.0";
				String cardSale = "0.0";
				String cashSale = "0.0";
				String totalSale = "0.0";
				String creditSale = "0";
				if (null != dayEndReportStore.getBody()) {
					physicalCash = dayEndReportStore.getBody().getPhysicalCash();
					cardSale = dayEndReportStore.getBody().getCardSale();
					cashSale = dayEndReportStore.getBody().getCash();
					totalSale = dayEndReportStore.getBody().getTotalSales();
					// creditSale = dayEndReportStore.getBody().get
				}
				String messageBody = "Date =" + strDate + " PhysicalCash=" + physicalCash + " Card Sale=" + cardSale
						+ " Cash Sale=" + cashSale + " Total Sales=" + totalSale;

				String msg = RestCaller.sendSMS(PHONENUMBER, messageBody);

			}
//			}

			if (SystemSetting.day_end_format.equalsIgnoreCase("YES")) {
				JasperPdfReportService.DayEndThermalReport(strDate);

			}

			if (SystemSetting.ONLINEDAYENDREPORT.equalsIgnoreCase("WITHOUTCASHBALANCE")) {
				JasperPdfReportService.DayEndReportWithoutCashBalance(strDate);

			} else if (SystemSetting.ONLINEDAYENDREPORT.equalsIgnoreCase("BAKERY")) {
				NewJasperPdfReportService.getDayend(SystemSetting.getUser().getBranchCode(), strDate);
			} else {
				JasperPdfReportService.HardWareDayEndReport(strDate);

			}

			dayEndClosure = null;
			dpProcessDate.setValue(null);
			tblDayEnd.getItems().clear();
			txtPhysicalCash.clear();
			
			txtCardSettlement.setText("0");


			RestCaller.deleteSalesTransHdrVoucherNull();
		}
		
		
	}

	@FXML
	void FocusOnAddButton(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnAdd.requestFocus();
		}
	}

	@FXML
	void FocusOnCount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCount.requestFocus();
		}

	}

	@FXML
	void FocusOnDenomination(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbDenomination.requestFocus();
		}

	}

	@FXML
	void ShowReport(ActionEvent event) {
		
		
		if (null == dpProcessDate.getValue()) {
			notifyMessage(5, " Please select report date...!!!", false);
			dpReportDate.requestFocus();
			return;
		}

		java.util.Date udate = SystemSetting.localToUtilDate(dpProcessDate.getValue());
		if(!hasDayEndDone(udate)){
			return;
		}
		 
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

		try {
			System.out.println("day-----------------report");

			if (SystemSetting.day_end_format.equalsIgnoreCase("YES")) {
				JasperPdfReportService.DayEndThermalReport(sdate);
			}

			if (SystemSetting.getUser().getCompanyMst().getCompanyName().equalsIgnoreCase("MAPLE")) {
				JasperPdfReportService.PharmacyDayEndReport(sdate);
				return;
			}

			if (SystemSetting.ONLINEDAYENDREPORT.equalsIgnoreCase("WITHOUTCASHBALANCE")) {
				JasperPdfReportService.DayEndReportWithoutCashBalance(sdate);

			} else if (SystemSetting.ONLINEDAYENDREPORT.equalsIgnoreCase("BAKERY")) {
				NewJasperPdfReportService.getDayend(SystemSetting.getUser().getBranchCode(), sdate);
			} else {
				System.out.println("day--------hardware---------report");

				JasperPdfReportService.HardWareDayEndReport(sdate);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@FXML
	void addItem(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addDayEndClosure();
		}
	}

	private void getDayEndDtlByHdrId() {

		tblDayEnd.getItems().clear();

		ArrayList pur = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		pur = RestCaller.getDayEndDtlByHdrId(dayEndClosure.getId());
		Iterator itr = pur.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object denomination = lm.get("denomination");
			Object count = lm.get("count");
			Object id = lm.get("id");
			if (id != null) {

				dayEndClosureDtl = new DayEndClosureDtl();
				dayEndClosureDtl.setId((String) id);
				dayEndClosureDtl.setDenomination((Double) denomination);
				dayEndClosureDtl.setCount((Double) count);

				dayEndClosureListTable.add(dayEndClosureDtl);

			}
		}
		FillTable();

	}

	private void FillTable() {

		tblDayEnd.setItems(dayEndClosureListTable);
		clDenomination.setCellValueFactory(cellData -> cellData.getValue().getDenominationProperty());
		clCount.setCellValueFactory(cellData -> cellData.getValue().getCountProperty());

		Double sum = RestCaller.getSummaryDayEndClosure(dayEndClosure.getId());
		if (null != sum) {
			txtPhysicalCash.setText(Double.toString(sum));
		} else {
			txtPhysicalCash.setText("");
		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	private void clearFields() {
		txtOpeningPOsBilNo.clear();
		txtClosingBillNo.clear();
		txtTotalPOsBill.clear();
		txtOpeningWholeSaleBillNo.clear();
		txtClosingWholeSaleBillNo.clear();
		txtTotalWholesaleBill.clear();
		txtCashSalesOrder.clear();
		txtCardSalesOrder.clear();
		txtTodaysCashSale.clear();
		txtCreditTocash.clear();
		txtPreviousCashSo.clear();
		txtCashSales.clear();
		txtCreditSales.clear();
		txtPreviousCardSale.clear();
		txtCardSales.clear();
		txtPaytm.clear();
		txtGpay.clear();
		txtEbs.clear();
		txtSwiggy.clear();
		txtZomato.clear();
		txtTotalSales.clear();
		txtClosingPettyCash.clear();
		// txtClosingCashBalance.clear();
		txtTotalCash.clear();
		txtTotalcashAtDrawer.clear();
		txtCashVariancenew.clear();
		txtNeftSales.clear();
		txtPhonePay.clear();
		txtSodexo.clear();
	}

	@FXML
	void generateAction(ActionEvent event) {
		clearFields();
		LocalDate dpDate = dpProcessDate.getValue();
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		Double dtodayscashSale = 0.0, dPreviousCashSaleOrder = 0.0, dTodaysCashSaleOrder = 0.0;
		Double yesBank = 0.0;
		Double sbiBank = 0.0;
		Double ebs = 0.0;
		Double cashr = 0.0;
		Double cash = 0.0;
		Double creditr = 0.0;
		Double gpayr = 0.0;
		Double syggyr = 0.0;
		Double zomator = 0.0;
		Double paytmr = 0.0;

		// Call url
		ResponseEntity<List<SalesModeWiseBillReport>> billSeries = RestCaller.BillSeriesDayEndByInvoiceSeries(strDate);
		List<SalesModeWiseBillReport> salesMOdeList = billSeries.getBody();

		ResponseEntity<VoucherNumberDtlReport> voucherNumberDtlReport = RestCaller
				.getInvoiceNumberStatistics(SystemSetting.getSystemBranch(), strDate);

		// rest doubl
		Double creditConvertTOCashforToday = RestCaller.getCreditSaleAdjstmnt(SystemSetting.getSystemBranch(), strDate);
		BigDecimal bcreditConvertTOCashforToday = new BigDecimal(creditConvertTOCashforToday);
		bcreditConvertTOCashforToday = bcreditConvertTOCashforToday.setScale(0, BigDecimal.ROUND_HALF_EVEN);

		txtCreditTocash.setText(bcreditConvertTOCashforToday.toPlainString());
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getSystemBranch());

		// -----------------version 1.11
		long BSPOSMAXNO = 0L;
		long BSPOSMINNO = 0L;
		long BSPOSCOUNT = 0L;

		long BSWSMAXNO = 0L;
		long BSWSMINNO = 0L;
		long BSWSCOUNT = 0L;
		txtSwiggy.setText(0.0 + "");
		txtZomato.setText(0.0 + "");
		txtCashVariancenew.setText(0.0 + "");
		txtGpay.setText(0.0 + "");
		txtPaytm.setText(0.0 + "");
		txtEbs.setText(0.0 + "");
		txtCardSales.setText(0.0 + "");
		txtCreditSales.setText(0.0 + "");

		txtPhonePay.setText(0.0 + "");
		txtNeftSales.setText(0.0 + "");
		txtSodexo.setText(0.0 + "");

		txtClosingBillNo.setText(voucherNumberDtlReport.getBody().getPosEndingBillNumber() + "");
		txtOpeningPOsBilNo.setText(voucherNumberDtlReport.getBody().getPosStartingBillNumber() + "");
		txtTotalPOsBill.setText(voucherNumberDtlReport.getBody().getPosBillCount() + "");

		txtClosingWholeSaleBillNo.setText(voucherNumberDtlReport.getBody().getWholeSaleEndingBillNumber() + "");
		txtOpeningWholeSaleBillNo.setText(voucherNumberDtlReport.getBody().getWholeSaleStartingBillNumber() + "");
		txtTotalWholesaleBill.setText(voucherNumberDtlReport.getBody().getWholeSaleBillCount() + "");

		ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-" + "PETTY CASH");
		AccountHeads accountHeads = accountHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> pettyCashAccountBalance = RestCaller.getAccountBalanceReport(strDate,
				strDate, accountHeads.getId());

		List<AccountBalanceReport> pettyCash = pettyCashAccountBalance.getBody();
		if (pettyCash.size() > 0) {
			if (pettyCash.get(0).getClosingBalanceType().equalsIgnoreCase("Cr")) {

				double pcash = pettyCash.get(0).getClosingBalance();
				txtClosingPettyCash.setText(pcash + "");
			} else {
				double pcash = pettyCash.get(0).getClosingBalance();
				txtClosingPettyCash.setText(pcash + "");
			}
		} else {
			txtClosingPettyCash.setText("0");
		}

		ResponseEntity<AccountHeads> cashHeadsResp = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-CASH");
		AccountHeads cashAccountHeads = cashHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> cashAccountBalance = RestCaller.getAccountBalanceReport(strDate,
				strDate, cashAccountHeads.getId());

		List<AccountBalanceReport> casbalance = cashAccountBalance.getBody();

		ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(strDate);
		List<DailyReceiptsSummaryReport> receipts = receiptReport.getBody();

		Double receiptCash = 0.0;
		for (DailyReceiptsSummaryReport rec : receipts) {
			if (rec.getReceiptMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
				receiptCash = rec.getTotalCash();
			}
		}

		double dClosingCashBal = 0.0;
		if (null != casbalance && casbalance.size() > 0) {

			dClosingCashBal = casbalance.get(0).getClosingBalance();
		} else {
			dClosingCashBal = 0;
		}

//			BigDecimal bdClosingBal = new BigDecimal(dClosingCashBal);
//
//			bdClosingBal = bdClosingBal.setScale(0, bdClosingBal.ROUND_HALF_EVEN);
//			parameters.put("ClosingCash",0);

		double doublePettyCash = 0.0;

		if (null == pettyCash || pettyCash.size() == 0) {
			doublePettyCash = 0.0;
		} else {
			if (pettyCash.get(0).getClosingBalanceType().equalsIgnoreCase("Cr")) {
				doublePettyCash = 0 - pettyCash.get(0).getClosingBalance();
			} else {
				doublePettyCash = pettyCash.get(0).getClosingBalance();
			}
		}

		double doubleCashBalance = 0.0;
		if (null == casbalance || casbalance.size() == 0) {
			doubleCashBalance = 0.0;
		} else {
			doubleCashBalance = casbalance.get(0).getClosingBalance();
		}

		Double totalCash = doubleCashBalance + doublePettyCash;

		BigDecimal bdTotalCash = new BigDecimal(totalCash);

		bdTotalCash = bdTotalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);

//			parameters.put("totalCash", bdTotalCash.toPlainString());

		ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(strDate);
		List<DayEndClosureHdr> dayEndClosureHdr = dayEndReport.getBody();

		double dPhysicalCashP = 0.0;

		if (null != dayEndClosureHdr && dayEndClosureHdr.size() > 0) {
			dPhysicalCashP = dayEndClosureHdr.get(0).getPhysicalCash();
		} else {
			dPhysicalCashP = 0;
		}
		BigDecimal bdPhysicalCash = new BigDecimal(dPhysicalCashP);

		bdPhysicalCash = bdPhysicalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);

		txtTotalcashAtDrawer.setText(bdPhysicalCash.toPlainString());

//		parameters.put("CashVariance", (casbalance.get(0).getClosingBalance()+receiptCash)-dayEndClosureHdr.get(0).getPhysicalCash());

//		parameters.put("CashVariance", (casbalance.get(0).getClosingBalance()+ pettyCash.get(0).getClosingBalance())-dayEndClosureHdr.get(0).getPhysicalCash()+"");

		// version 1.12 end

//-----------------version 1.11 end

		java.sql.Date utilDate = SystemSetting.StringToSqlDateSlash(strDate, "yyyy-MM-dd");

//			parameters.put("strDate", utilDate);
//----------version 1.11

		ResponseEntity<List<ReceiptModeReport>> getReceiptMod = RestCaller.getReceiptModeReport(strDate);
		Double totalSales = 0.0;

//		ResponseEntity<List<ReceiptModeReport>> getReceiptMode = RestCaller.getSaleOrderReceiptModeReport(strDate);

		ResponseEntity<List<ReceiptModeMst>> respReceiptMode = RestCaller.getAllReceiptMode();
		// ArrayList receiptModeList = (ArrayList) respReceiptMode.getBody();
		ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeCash = RestCaller.getReceiptModeReportByMode(strDate,
				SystemSetting.getSystemBranch() + "-CASH");
		if (getReceiptAmtByModeCash.getBody().size() > 0) {
			dtodayscashSale = dtodayscashSale + getReceiptAmtByModeCash.getBody().get(0).getAmount();

		}
		for (ReceiptModeMst receiptModeMst : respReceiptMode.getBody()) {
			ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByMode = RestCaller.getReceiptModeReportByMode(strDate,
					receiptModeMst.getReceiptMode());

			if (getReceiptAmtByMode.getBody().size() > 0) {

				if (receiptModeMst.getReceiptMode().equalsIgnoreCase("YES BANK"))

				{
					BigDecimal bYesBankr = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
					bYesBankr = bYesBankr.setScale(0, BigDecimal.ROUND_HALF_EVEN);

//					parameters.put("yesBankr",bYesBankr.toPlainString());
				}

				if (receiptModeMst.getReceiptMode().equalsIgnoreCase("PAYTM")) {
					BigDecimal bpaytm = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
					bpaytm = bpaytm.setScale(0, BigDecimal.ROUND_HALF_EVEN);

					txtPaytm.setText(bpaytm.toPlainString());
				}

				if (receiptModeMst.getReceiptMode().equalsIgnoreCase("EBS")) {
					BigDecimal bEbsr = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
					bEbsr = bEbsr.setScale(0, BigDecimal.ROUND_HALF_EVEN);
					txtEbs.setText(bEbsr.toPlainString());
				}
				if (receiptModeMst.getReceiptMode().equalsIgnoreCase("GPAY")) {
					BigDecimal bGpay = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
					bGpay = bGpay.setScale(0, BigDecimal.ROUND_HALF_EVEN);
					txtGpay.setText(bGpay.toPlainString());
				}

			}
		}
		ResponseEntity<Double> getCashPA = RestCaller.getPreviousAdvanceAmount(strDate, "CASH");

		try {
			dPreviousCashSaleOrder = getCashPA.getBody().doubleValue();
			BigDecimal bdPreviousCashSaleOrder = new BigDecimal(dPreviousCashSaleOrder);
			bdPreviousCashSaleOrder = bdPreviousCashSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtPreviousCashSo.setText(bdPreviousCashSaleOrder.toPlainString());
		} catch (Exception e) {
			System.out.println("Error converting to  double value - Provius advance YES BANK");
		}

		Double sbiBankso = 0.0;
		Double ebsso = 0.0;
		Double cashso = 0.0;
		txtCashSalesOrder.setText(0.0 + "");
		txtCardSalesOrder.setText(0.0 + "");
		BigDecimal bdtodayscashSaletoShow = new BigDecimal(dtodayscashSale);
		bdtodayscashSaletoShow = bdtodayscashSaletoShow.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTodaysCashSale.setText(bdtodayscashSaletoShow.toPlainString());
		txtCashSales.setText(bdtodayscashSaletoShow.toPlainString());
		dtodayscashSale = dtodayscashSale + creditConvertTOCashforToday - dPreviousCashSaleOrder;
		BigDecimal bdtodayscashSale = new BigDecimal(dtodayscashSale);
		bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		ResponseEntity<List<ReceiptModeReport>> getReceiptMode = RestCaller.getSaleOrderReceiptModeReportByMode(strDate,
				"CASH");
		if (getReceiptMode.getBody().size() > 0)

		{
			dtodayscashSale = dtodayscashSale + getReceiptMode.getBody().get(0).getAmount();
			bdtodayscashSale = new BigDecimal(dtodayscashSale);
			bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			// txtCashSales.setText(bdtodayscashSale.toPlainString());

			BigDecimal bdtodayscashSaleOrder = new BigDecimal(getReceiptMode.getBody().get(0).getAmount());
			bdtodayscashSaleOrder = bdtodayscashSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			txtCashSalesOrder.setText(bdtodayscashSaleOrder.toPlainString());
		}

		Double dTodyasCard = 0.0;
		try {
			dTodyasCard = RestCaller.getSumOfSaleReceiptCard(strDate);
		} catch (Exception e) {
			System.out.println(e);
		}

		BigDecimal bdtodayscardSale = new BigDecimal(dTodyasCard);
		bdtodayscardSale = bdtodayscardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);

		Double previousCardAmount = 0.0;
		// Get previous card from sale order

		ResponseEntity<Double> previousCardAmountResp = RestCaller.getPreviousAdvanceAmountCard(strDate);
		if (null != previousCardAmountResp.getBody()) {
			previousCardAmount = previousCardAmountResp.getBody().doubleValue();
		}
		BigDecimal bdpreviousCardAmount = new BigDecimal(previousCardAmount);
		bdpreviousCardAmount = bdpreviousCardAmount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtPreviousCardSale.setText(bdpreviousCardAmount.toPlainString());
		// Previous card - todays card
		Double netTodaysCard = 0.0;
		netTodaysCard = dTodyasCard - previousCardAmount;
		BigDecimal bdnetTodaysCard = new BigDecimal(netTodaysCard);
		bdnetTodaysCard = bdnetTodaysCard.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtCardSales.setText(bdnetTodaysCard.toPlainString());
		Double cardSaleOrder = 0.0;

		cardSaleOrder = RestCaller.getSumOfSaleOrderReceiptCard(strDate);
		BigDecimal bdtodayscardSaleOrder = new BigDecimal(cardSaleOrder);
		bdtodayscardSaleOrder = bdtodayscardSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtCardSalesOrder.setText(bdtodayscardSaleOrder.toPlainString());
		ResponseEntity<List<ReceiptModeReport>> getReceiptModeNEft = RestCaller.getReceiptModeReportByMode(strDate,
				"NEFT");

		if (getReceiptModeNEft.getBody().size() > 0)

		{
			BigDecimal bneft = new BigDecimal(getReceiptModeNEft.getBody().get(0).getAmount());
			bneft = bneft.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtNeftSales.setText(bneft.toPlainString());
		}
		ResponseEntity<AccountHeads> getAcc = RestCaller.getAccountHeadByName("SWIGGY");
		ResponseEntity<List<ReceiptModeReport>> getReceiptModeSWIGGY = RestCaller.getReceiptModeReportByAccID(strDate,
				getAcc.getBody().getId());

		Double creditOnline = 0.0;
		if (getReceiptModeSWIGGY.getBody().size() > 0)

		{
			creditOnline = creditOnline + getReceiptModeSWIGGY.getBody().get(0).getAmount();
			BigDecimal bswiggy = new BigDecimal(getReceiptModeSWIGGY.getBody().get(0).getAmount());
			bswiggy = bswiggy.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtSwiggy.setText(bswiggy.toPlainString());
		}
		ResponseEntity<AccountHeads> getAcczomato = RestCaller.getAccountHeadByName("ZOMATO");
		ResponseEntity<List<ReceiptModeReport>> getReceiptModeZOMATO = RestCaller.getReceiptModeReportByAccID(strDate,
				getAcczomato.getBody().getId());

		if (getReceiptModeZOMATO.getBody().size() > 0)

		{
			creditOnline = creditOnline + getReceiptModeZOMATO.getBody().get(0).getAmount();
			BigDecimal bzomato = new BigDecimal(getReceiptModeZOMATO.getBody().get(0).getAmount());
			bzomato = bzomato.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			txtZomato.setText(bzomato.toPlainString());
		}

		ResponseEntity<List<ReceiptModeReport>> getReceiptModeCREDIT = RestCaller.getReceiptModeReportByMode(strDate,
				"CREDIT");

		if (getReceiptModeCREDIT.getBody().size() > 0)

		{
			BigDecimal bcredit = new BigDecimal(
					getReceiptModeCREDIT.getBody().get(0).getAmount() - creditConvertTOCashforToday);
			bcredit = bcredit.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			txtCreditSales.setText(bcredit.toPlainString());
		}

		ResponseEntity<List<ReceiptModeReport>> getReceiptModePhoePay = RestCaller.getReceiptModeReportByMode(strDate,
				"PHONE PAY");

		if (getReceiptModePhoePay.getBody().size() > 0)

		{
			BigDecimal bphonepay = new BigDecimal(getReceiptModePhoePay.getBody().get(0).getAmount());
			bphonepay = bphonepay.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			txtPhonePay.setText(bphonepay.toPlainString());
		}
		ResponseEntity<List<ReceiptModeReport>> getReceiptModeSodexo = RestCaller.getReceiptModeReportByMode(strDate,
				"SODEXO");

		if (getReceiptModeSodexo.getBody().size() > 0)

		{
			BigDecimal bsodexo = new BigDecimal(getReceiptModeSodexo.getBody().get(0).getAmount());
			bsodexo = bsodexo.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			txtSodexo.setText(bsodexo.toPlainString());
		}

		dClosingCashBal = dtodayscashSale - doublePettyCash;
		BigDecimal bdClosingCashBal = new BigDecimal(dClosingCashBal);
		bdClosingCashBal = bdClosingCashBal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		// txtClosingCashBalance.setText(bdClosingCashBal.toPlainString());
		Double totalCashPlusPettyCash = 0.0;
		totalCashPlusPettyCash = dtodayscashSale + doublePettyCash;
		BigDecimal bdtotalCashPlusPettyCash = new BigDecimal(totalCashPlusPettyCash);
		bdtotalCashPlusPettyCash = bdtotalCashPlusPettyCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalCash.setText(bdtotalCashPlusPettyCash.toPlainString());
		txtCashSales.setText(bdtodayscashSale.toPlainString());

		Double cashVariance = dtodayscashSale - dPhysicalCashP;
		BigDecimal bdCashVariance = new BigDecimal(cashVariance);
		bdCashVariance = bdCashVariance.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtCashVariancenew.setText(bdCashVariance.toPlainString());
		// Find total Sale
		double totalSale = RestCaller.getInvoiceTotalSales(strDate);
		BigDecimal btotalSale = new BigDecimal(totalSale);
		btotalSale = btotalSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);

		txtTotalSales.setText(btotalSale.toPlainString());

	}

	@FXML
	void cashSaleOrder(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showInvoicePopup(strDate);

	}

	@FXML
	void cashSalesAction(ActionEvent event) {

	}

	@FXML
	void previousCashSo(ActionEvent event) {

		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showPreviousSaleOrderCashReport(strDate, "CASH");
	}

	@FXML
	void todaysCashSale(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showSalesReceiptsPopup(strDate, SystemSetting.getSystemBranch() + "-CASH");
	}

	@FXML
	void previousCardSale(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showPreviousCardSalesReceiptsPopup(strDate);
	}

	@FXML
	void payTmSales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showSalesReceiptsPopup(strDate, "PAYTM");
	}

	@FXML
	void phonepay(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showSalesReceiptsPopup(strDate, "PHONE PAY");
	}

	@FXML
	void neftSales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showSalesReceiptsPopup(strDate, "NEFT");
	}

	@FXML
	void sodexoSales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showSalesReceiptsPopup(strDate, "SODEXO");
	}

	@FXML
	void gpaySales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showSalesReceiptsPopup(strDate, "GPAY");
	}

	@FXML
	void ebsSales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showSalesReceiptsPopup(strDate, "EBS");
	}

	@FXML
	void swiggySales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		ResponseEntity<AccountHeads> getAcc = RestCaller.getAccountHeadByName("SWIGGY");
		showSalesReceiptsPopupByAccId(strDate, getAcc.getBody().getId());
	}

	@FXML
	void zomatoSales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		ResponseEntity<AccountHeads> getAcc = RestCaller.getAccountHeadByName("ZOMATO");
		showSalesReceiptsPopupByAccId(strDate, getAcc.getBody().getId());
	}

	@FXML
	void creditSales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showSalesReceiptsPopup(strDate, "CREDIT");

	}

	@FXML
	void cardSales(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showCardSalesReceiptsPopup(strDate);
	}

	@FXML
	void cardSaleOrder(ActionEvent event) {
		java.util.Date uDate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

		showCardSalesOrderReceiptsPopup(strDate);
	}

	@FXML
	void PrintReportnew(ActionEvent event) {
		if (null == dpReportDate1.getValue()) {
			notifyMessage(5, " Please select report date...!!!", false);
			dpReportDate1.requestFocus();
			return;
		}
		
		
		 
			java.util.Date udate = SystemSetting.localToUtilDate(dpReportDate1.getValue());
		 
		 

		if(!hasDayEndDone(udate)){
			return;
		}
		
 
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		
		try {

			if (SystemSetting.day_end_format.equalsIgnoreCase("YES")) {
				JasperPdfReportService.DayEndThermalReport(sdate);

			}

			if (SystemSetting.ONLINEDAYENDREPORT.equalsIgnoreCase("WITHOUTCASHBALANCE")) {
				JasperPdfReportService.DayEndReportWithoutCashBalance(sdate);

			} else if (SystemSetting.ONLINEDAYENDREPORT.equalsIgnoreCase("BAKERY")) {
				NewJasperPdfReportService.getDayend(SystemSetting.getUser().getBranchCode(), sdate);
			} else {
				JasperPdfReportService.HardWareDayEndReport(sdate);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	private void showInvoicePopup(String date) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderReceiptCashReport.fxml"));
			Parent root = loader.load();
			SaleOrderReceiptCashReportPopUpCtl popupctl = loader.getController();
			popupctl.showSaleOrderReceiptReport(date, "CASH");

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showPreviousSaleOrderCashReport(String date, String mode) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderReceiptCashReport.fxml"));
			Parent root = loader.load();
			SaleOrderReceiptCashReportPopUpCtl popupctl = loader.getController();
			popupctl.showSaleOrderCashReport(date, mode);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//   				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showSalesReceiptsPopup(String date, String mode) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderReceiptCashReport.fxml"));
			Parent root = loader.load();
			SaleOrderReceiptCashReportPopUpCtl popupctl = loader.getController();
			popupctl.showSalesReceiptBymode(date, mode);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showSalesReceiptsPopupByAccId(String date, String acid) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderReceiptCashReport.fxml"));
			Parent root = loader.load();
			SaleOrderReceiptCashReportPopUpCtl popupctl = loader.getController();
			popupctl.showSalesReceiptBymodeByCredit(date, acid);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//   				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showCardSalesReceiptsPopup(String date) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderReceiptCashReport.fxml"));
			Parent root = loader.load();
			SaleOrderReceiptCashReportPopUpCtl popupctl = loader.getController();
			popupctl.showSalesReceiptByCard(date);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showPreviousCardSalesReceiptsPopup(String date) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderReceiptCashReport.fxml"));
			Parent root = loader.load();
			SaleOrderReceiptCashReportPopUpCtl popupctl = loader.getController();
			popupctl.showSalesPreviousReceiptByCard(date);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//   				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showCardSalesOrderReceiptsPopup(String date) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderReceiptCashReport.fxml"));
			Parent root = loader.load();
			SaleOrderReceiptCashReportPopUpCtl popupctl = loader.getController();
			popupctl.showSaleOrderCardReport(date);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//  				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveDayEndReportStore() {

		String branchCode = SystemSetting.systemBranch;
		java.util.Date udate = SystemSetting.localToUtilDate(dpProcessDate.getValue());
		String rdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		DayEndReportStore dayEndReportStore = new DayEndReportStore();

		Double dtodayscashSale = 0.0, dPreviousCashSaleOrder = 0.0, dTodaysCashSaleOrder = 0.0;
		ResponseEntity<VoucherNumberDtlReport> voucherNumberDtlReport = RestCaller
				.getInvoiceNumberStatistics(branchCode, rdate);
		Double creditConvertTOCashforToday = RestCaller.getCreditSaleAdjstmnt(branchCode, rdate);
		BranchMst branchMst = RestCaller.getBranchDtls(branchCode);

		dayEndReportStore.setPosMaxBillNo(voucherNumberDtlReport.getBody().getPosEndingBillNumber() + "");
		dayEndReportStore.setPosMinBillNo(voucherNumberDtlReport.getBody().getPosStartingBillNumber() + "");
		dayEndReportStore.setPosBillCount(voucherNumberDtlReport.getBody().getPosBillCount() + "");

		dayEndReportStore.setWholeSaleMaxBillNo(voucherNumberDtlReport.getBody().getWholeSaleEndingBillNumber() + "");
		dayEndReportStore.setWholeSaleMinBillNo(voucherNumberDtlReport.getBody().getWholeSaleStartingBillNumber() + "");
		dayEndReportStore.setWholeSaleBillCount(voucherNumberDtlReport.getBody().getWholeSaleBillCount() + "");

		dayEndReportStore.setPreviousCashSaleOrder("0");

		ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-" + "PETTY CASH");
		AccountHeads accountHeads = accountHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> pettyCashAccountBalance = RestCaller.getAccountBalanceReport(rdate,
				rdate, accountHeads.getId());

		List<AccountBalanceReport> pettyCash = pettyCashAccountBalance.getBody();
		if (pettyCash.size() > 0) {
			if (pettyCash.get(0).getClosingBalanceType().equalsIgnoreCase("Cr")) {

				double pcash = pettyCash.get(0).getClosingBalance();
				dayEndReportStore.setPettyCash("-" + pcash + "");
			} else {
				double pcash = pettyCash.get(0).getClosingBalance();
				dayEndReportStore.setPettyCash(pcash + "");
			}
		} else {
			dayEndReportStore.setPettyCash("0");
		}

		ResponseEntity<AccountHeads> cashHeadsResp = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-CASH");
		AccountHeads cashAccountHeads = cashHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> cashAccountBalance = RestCaller.getAccountBalanceReport(rdate, rdate,
				cashAccountHeads.getId());

		List<AccountBalanceReport> casbalance = cashAccountBalance.getBody();

		try {
			ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(rdate);
			List<DailyReceiptsSummaryReport> receipts = receiptReport.getBody();

			Double receiptCash = 0.0;
			for (DailyReceiptsSummaryReport rec : receipts) {
				if (rec.getReceiptMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
					receiptCash = rec.getTotalCash();
				}
			}

			double dClosingCashBal = 0.0;
			if (null != casbalance && casbalance.size() > 0) {

				dClosingCashBal = casbalance.get(0).getClosingBalance();
			} else {
				dClosingCashBal = 0;
			}

			double doublePettyCash = 0.0;

			if (null == pettyCash || pettyCash.size() == 0) {
				doublePettyCash = 0.0;
			} else {
				if (pettyCash.get(0).getClosingBalanceType().equalsIgnoreCase("Cr")) {
					doublePettyCash = 0 - pettyCash.get(0).getClosingBalance();
				} else {
					doublePettyCash = pettyCash.get(0).getClosingBalance();
				}
			}

			double doubleCashBalance = 0.0;
			if (null == casbalance || casbalance.size() == 0) {
				doubleCashBalance = 0.0;
			} else {
				doubleCashBalance = casbalance.get(0).getClosingBalance();
			}

			Double totalCash = doubleCashBalance + doublePettyCash;
			BigDecimal bdTotalCash = new BigDecimal(totalCash);
			bdTotalCash = bdTotalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(rdate);
			List<DayEndClosureHdr> dayEndClosureHdr = dayEndReport.getBody();

			double dPhysicalCashP = 0.0;

			if (null != dayEndClosureHdr && dayEndClosureHdr.size() > 0) {
				dPhysicalCashP = dayEndClosureHdr.get(0).getPhysicalCash();
			} else {
				dPhysicalCashP = 0;
			}
			BigDecimal bdPhysicalCash = new BigDecimal(dPhysicalCashP);

			dayEndReportStore.setPhysicalCash(bdPhysicalCash.toPlainString());
			java.sql.Date utilDate = SystemSetting.StringToSqlDateSlash(rdate, "yyyy-MM-dd");
			dayEndReportStore.setDate(utilDate);
			ResponseEntity<List<ReceiptModeReport>> getReceiptMod = RestCaller.getReceiptModeReport(rdate);
			Double totalSales = 0.0;
			ResponseEntity<List<ReceiptModeMst>> respReceiptMode = RestCaller.getAllReceiptMode();
			ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeCash = RestCaller
					.getReceiptModeReportByMode(rdate, SystemSetting.getSystemBranch() + "-CASH");
			if (getReceiptAmtByModeCash.getBody().size() > 0) {
				dtodayscashSale = dtodayscashSale + getReceiptAmtByModeCash.getBody().get(0).getAmount();
			}
			for (ReceiptModeMst receiptModeMst : respReceiptMode.getBody()) {
				ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByMode = RestCaller
						.getReceiptModeReportByMode(rdate, receiptModeMst.getReceiptMode());

				if (getReceiptAmtByMode.getBody().size() > 0) {

					if (receiptModeMst.getReceiptMode().equalsIgnoreCase("YES BANK"))

					{
						BigDecimal bYesBankr = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
						bYesBankr = bYesBankr.setScale(0, BigDecimal.ROUND_HALF_EVEN);
						dayEndReportStore.setYesBankReceipt(bYesBankr.toPlainString());
					}

					if (receiptModeMst.getReceiptMode().equalsIgnoreCase("PAYTM")) {
						BigDecimal bpaytm = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
						bpaytm = bpaytm.setScale(0, BigDecimal.ROUND_HALF_EVEN);
						dayEndReportStore.setPaytmReceipt(bpaytm.toPlainString());
					}

					if (receiptModeMst.getReceiptMode().equalsIgnoreCase("EBS")) {
						BigDecimal bEbsr = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
						bEbsr = bEbsr.setScale(0, BigDecimal.ROUND_HALF_EVEN);
						dayEndReportStore.setEbsReceipt(bEbsr.toPlainString());
					}
					if (receiptModeMst.getReceiptMode().equalsIgnoreCase("GPAY")) {
						BigDecimal bGpay = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
						bGpay = bGpay.setScale(0, BigDecimal.ROUND_HALF_EVEN);
						dayEndReportStore.setGpayReceipt(bGpay.toPlainString());
					}

				}
			}
			ResponseEntity<Double> getCashPA = RestCaller.getPreviousAdvanceAmount(rdate, "CASH");

			try {
				dPreviousCashSaleOrder = getCashPA.getBody().doubleValue();
				BigDecimal bdPreviousCashSaleOrder = new BigDecimal(dPreviousCashSaleOrder);
				bdPreviousCashSaleOrder = bdPreviousCashSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);
				dayEndReportStore.setPreviousCashSaleOrder(bdPreviousCashSaleOrder.toPlainString());
			} catch (Exception e) {
				System.out.println("Error converting to  double value - Provius advance YES BANK");
			}

			Double saleOrderPlusTodaysCash = 0.0;
			dayEndReportStore.setCashSaleOrder("0");
			dayEndReportStore.setCardSaleOrder("0");

			dtodayscashSale = dtodayscashSale + creditConvertTOCashforToday - dPreviousCashSaleOrder;
			System.out.println(" credit" + creditConvertTOCashforToday);
			System.out.println("Todays Cash including credit" + dtodayscashSale);
			BigDecimal bdtodayscashSale = new BigDecimal(dtodayscashSale);
			bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			dayEndReportStore.setCash(bdtodayscashSale.toPlainString());
			ResponseEntity<List<ReceiptModeReport>> getReceiptMode = RestCaller
					.getSaleOrderReceiptModeReportByMode(rdate, "CASH");
			if (getReceiptMode.getBody().size() > 0)

			{
				saleOrderPlusTodaysCash = dtodayscashSale + getReceiptMode.getBody().get(0).getAmount();
				bdtodayscashSale = new BigDecimal(dtodayscashSale);
				bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal bdtodayscashSaleOrder = new BigDecimal(getReceiptMode.getBody().get(0).getAmount());
				bdtodayscashSaleOrder = bdtodayscashSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);
				dayEndReportStore.setCashSaleOrder(bdtodayscashSaleOrder.toPlainString());
			} else {
				saleOrderPlusTodaysCash = dtodayscashSale;
			}

			Double dTodyasCard = 0.0;
			try {
				dTodyasCard = RestCaller.getSumOfSaleReceiptCard(rdate);
			} catch (Exception e) {
				System.out.println(e);
			}
			Double previousCardAmount = 0.0;
			ResponseEntity<Double> previousCardAmountResp = RestCaller.getPreviousAdvanceAmountCard(rdate);
			if (null != previousCardAmountResp.getBody()) {
				previousCardAmount = previousCardAmountResp.getBody().doubleValue();
			}
			dTodyasCard = dTodyasCard - previousCardAmount;
			BigDecimal bdtodayscardSale = new BigDecimal(dTodyasCard);
			bdtodayscardSale = bdtodayscardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			dayEndReportStore.setCardSale(bdtodayscardSale.toPlainString());

			BigDecimal bdpreviousCardAmount = new BigDecimal(previousCardAmount);
			bdpreviousCardAmount = bdpreviousCardAmount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			Double cardSaleOrder = 0.0;
			cardSaleOrder = RestCaller.getSumOfSaleOrderReceiptCard(rdate);
			BigDecimal bdtodayscardSaleOrder = new BigDecimal(cardSaleOrder);
			bdtodayscardSaleOrder = bdtodayscardSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			dayEndReportStore.setCardSaleOrder(bdtodayscardSaleOrder.toPlainString());

			ResponseEntity<AccountHeads> getAcc = RestCaller.getAccountHeadByName("SWIGGY");
			ResponseEntity<List<ReceiptModeReport>> getReceiptModeSWIGGY = RestCaller.getReceiptModeReportByAccID(rdate,
					getAcc.getBody().getId());

			if (getReceiptModeSWIGGY.getBody().size() > 0)

			{
				BigDecimal bswiggy = new BigDecimal(getReceiptModeSWIGGY.getBody().get(0).getAmount());
				bswiggy = bswiggy.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				dayEndReportStore.setSwiggyReceipt(bswiggy.toPlainString());
			}
			ResponseEntity<AccountHeads> getAcczomato = RestCaller.getAccountHeadByName("ZOMATO");
			ResponseEntity<List<ReceiptModeReport>> getReceiptModeZOMATO = RestCaller.getReceiptModeReportByAccID(rdate,
					getAcczomato.getBody().getId());

			if (getReceiptModeZOMATO.getBody().size() > 0)

			{
				BigDecimal bzomato = new BigDecimal(getReceiptModeZOMATO.getBody().get(0).getAmount());
				bzomato = bzomato.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				dayEndReportStore.setZomatoReceipt(bzomato.toPlainString());
			}

			ResponseEntity<List<ReceiptModeReport>> getReceiptModeCREDIT = RestCaller.getReceiptModeReportByMode(rdate,
					"CREDIT");

			if (getReceiptModeCREDIT.getBody().size() > 0)

			{
				BigDecimal bcredit = new BigDecimal(
						getReceiptModeCREDIT.getBody().get(0).getAmount() - creditConvertTOCashforToday);
				bcredit = bcredit.setScale(0, BigDecimal.ROUND_HALF_EVEN);
				dayEndReportStore.setCreditReceipt(bcredit.toPlainString());
			}

			ResponseEntity<List<ReceiptModeReport>> getReceiptModeNEFT = RestCaller.getReceiptModeReportByMode(rdate,
					"NEFT");

			if (getReceiptModeNEFT.getBody().size() > 0)

			{
				BigDecimal bneftt = new BigDecimal(getReceiptModeNEFT.getBody().get(0).getAmount());
				bneftt = bneftt.setScale(0, BigDecimal.ROUND_HALF_EVEN);
				dayEndReportStore.setNeftSales(bneftt.toPlainString());
			}

			dClosingCashBal = totalCash - doublePettyCash;
			BigDecimal bdClosingCashBal = new BigDecimal(dClosingCashBal);
			bdClosingCashBal = bdClosingCashBal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			BigDecimal bdtotalCash = new BigDecimal(saleOrderPlusTodaysCash);
			bdtotalCash = bdtotalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			dayEndReportStore.setTotalCash(bdtotalCash.toPlainString());
			Double cashVariance = saleOrderPlusTodaysCash - dPhysicalCashP;
			BigDecimal bdCashVariance = new BigDecimal(cashVariance);
			bdCashVariance = bdCashVariance.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			dayEndReportStore.setCashVarience(bdCashVariance.toPlainString());
			double totalSale = RestCaller.getInvoiceTotalSales(rdate);
			BigDecimal btotalSale = new BigDecimal(totalSale);
			btotalSale = btotalSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			BranchMst branchMstresp = RestCaller.getBranchDtls(SystemSetting.getSystemBranch());
			dayEndReportStore.setBranchName(branchMstresp.getBranchName());
			dayEndReportStore.setBranchCode(SystemSetting.getSystemBranch());
			dayEndReportStore.setTotalSales(btotalSale.toPlainString());
			ResponseEntity<DayEndReportStore> dayEndReportStoreSaved = RestCaller
					.saveDayEndReportStore(dayEndReportStore);

		} catch (Exception e) {
			System.out.print(e.getMessage());
		}

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
	
	private boolean hasDayEndDone(java.util.Date udate ) {
		
 
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		ResponseEntity<DayEndClosureHdr> getDayEnd = RestCaller.getDayEndClosureByDate(sdate);
		if (null != getDayEnd.getBody()) {
			if (null == getDayEnd.getBody().getDayEndStatus()) {
				notifyMessage(5, " Please Save Day End Before Taking PrintOut...!!!", false);
				return false;
			}
			 
		}else {
			return false;
		}

		return true;
	}

}
