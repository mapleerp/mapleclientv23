package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ser.std.StdArraySerializers.BooleanArraySerializer;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.ExportItemMstToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.TSCBarcode;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BarcodeFormatMst;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.IngredientsMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPropertyConfig;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.entity.NutritionValueDtl;
import com.maple.mapleclient.entity.NutritionValueMst;
import com.maple.mapleclient.entity.ParamMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.UnitDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.entity.WeighingItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

@Controller

public class ItemMstCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();
	ExportItemMstToExcel exportItemMstToExcel = new ExportItemMstToExcel();
	IngredientsMst ingredientMst = null;
	ItemMst itemmst;
	UnitMst unitmst;
	ItemPropertyConfig itemPropertyConfig = null;
//	ItemMst itemmst;
	NutritionValueDtl nutritionValueDtl = null;
	NutritionValueMst nutritionValueMst = null;
	private ObservableList<NutritionValueDtl> nutrintionValueList = FXCollections.observableArrayList();
	private ObservableList<NutritionMst> nutritionList = FXCollections.observableArrayList();
	private ObservableList<ItemMst> ItemTableList = FXCollections.observableArrayList();
	private ObservableList<ItemMst> itemList = FXCollections.observableArrayList();
	private ObservableList<UnitMst> unitList = FXCollections.observableArrayList();
	private ObservableList<UnitDtl> unitdtList = FXCollections.observableArrayList();
	private ObservableList<CategoryMst> categoryList = FXCollections.observableArrayList();
	private ObservableList<ItemMst> ItemTableList2 = FXCollections.observableArrayList();
	private ObservableList<CategoryMst> categoryList2 = FXCollections.observableArrayList();
	private ObservableList<ItemPropertyConfig> ItemPropertyList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();
	@FXML
	private TableView<ItemMst> tblItemList;

	@FXML
	private TextField txtReoder;

	@FXML
	private TableColumn<ItemMst, String> clItemName;

	@FXML
	private TableColumn<ItemMst, String> clItemCategory;

	@FXML
	private TableColumn<ItemMst, String> clItemUnit;

	@FXML
	private TableColumn<ItemMst, Number> clItemSellingPrice;

	@FXML
	private Button btnCategoryDelete;
	@FXML
	private Button btnRefresh;

	@FXML
	private TableColumn<ItemMst, Number> clItemTax;

	@FXML
	private TableColumn<ItemMst, String> clStatus;

	@FXML
	private ComboBox<String> choiceCategory;

	@FXML
	private TextField txtitemName;

	@FXML
	private TextField txtbarCode;

	@FXML
	private TextField txtitemCode;

	@FXML
	private TextField txthsnCode;

	@FXML
	private TextField txtGroup;
	@FXML
	private TextField txtBestBefore;
	@FXML
	private TextField txtCategory;

	@FXML
	private TextField txtsellingPrice;

	@FXML
	private TextField TxtStateCess;

	@FXML
	private TextField txttaxRate;

	@FXML
	private Button btnbarCode;

	@FXML
	private Button btnitemCode;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnclr;

	@FXML
	private Button btnshowAll;

	@FXML
	private Button btnSubmit;

	@FXML
	private ComboBox<String> choiceUnit;

	@FXML
	private TextField txtunitName;

	@FXML
	private TextField txtunitDescription;

	@FXML
	private TextField txtprecision;

	@FXML
	private Button btndlt;

	@FXML
	private Button btnshowall;

	@FXML
	private Button btnsave;
	@FXML
	private TextField txtname1;

	@FXML
	private TextField txtprimaryunit;

	@FXML
	private TextField txtconversion;

	@FXML
	private Button search1;

	@FXML
	private Button btnshownall1;

	@FXML
	private Button btnSave1;

	@FXML
	private Button btndelete1;

	@FXML
	private ChoiceBox<?> cmbSecondaryunit;

	@FXML
	private TextField txtIventoryCategory;

	@FXML
	private ComboBox<String> cmbInventoryParentId;

	@FXML
	private Button btnInventoryAdd;

	@FXML
	private Button btnInventoryDelete;

	
	@FXML
	private CheckBox chkOnline;

	@FXML
	private CheckBox chkCategoryOnline;
	
    @FXML
    private CheckBox chkUnitOnline;
	
	@FXML
	private TableView<CategoryMst> tblCategoryList;

	@FXML
	private TableColumn<CategoryMst, String> clCategoryName;

	@FXML
	private TableColumn<CategoryMst, String> clParentCategory;

	@FXML
	private Button btnInventoryShowAll;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtItemId;

	@FXML
	private TextField txtPrinterName;

	@FXML
	private TextField txtItemNameNutritionValue;
	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtMrp;

	@FXML
	private TextField txtDuration;

	@FXML
	private TextField txtbestBefore;

	@FXML
	private TextField txtIngredient1;

	@FXML
	private TextField txtIngredient2;

	@FXML
	private DatePicker dpProduction;

	@FXML
	private Button btnSaveBarcode;

	@FXML
	private Button btnFetch;
	@FXML
	private Button btnCalibrate;

	@FXML
	private TextField txtservingSize;

	@FXML
	private TextField txtcalories;

	@FXML
	private TextField txtFat;

	@FXML
	private ComboBox<String> cmbNutrition;

	@FXML
	private TextField txtNutritionValue;

	@FXML
	private TextField txtpercentage;

	@FXML
	private TextField txtSerial;

	@FXML
	private TableView<NutritionValueDtl> tblNutrition;

	@FXML
	private TableColumn<NutritionValueDtl, String> clNutrition;

	@FXML
	private TableColumn<NutritionValueDtl, String> clValue;

	@FXML
	private TableColumn<NutritionValueDtl, String> clPercentage;

	@FXML
	private TableColumn<NutritionValueDtl, String> clSerial;

	@FXML
	private Button btnAdd;

	@FXML
	private ComboBox<String> cmbBarcodeFormat;

	@FXML
	private Button btnExortToExcel;

	@FXML
	private Button btnDeleteNutritionFacts;

	@FXML
	private Button btnClear;

	@FXML
	private Button btnFinalSave;

	@FXML
	void AddItems(ActionEvent event) {

		addItems();
	}

	@FXML
	void deleteItems(ActionEvent event) {
		if (null != nutritionValueDtl.getId()) {
			RestCaller.deleteNutritionValueDtl(nutritionValueDtl.getId());
			ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller
					.getNutritionValueDtlByMstId(nutritionValueMst);
			nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
			fillTable();
		}
	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnCalibrate.getScene().getWindow();
		if (stage.isShowing()) {

			Platform.runLater(new Runnable() {
				@Override
				public void run() {

					itemmst = new ItemMst();
					txtItemNameNutritionValue.setText(itemPopupEvent.getItemName());
					itemmst.setId(itemPopupEvent.getItemId());
					ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(itemmst.getId());
					itemmst = respentity.getBody();
					// txtQty.setText(Integer.toString(itemPopupEvent.getQty()));

				}
			});
		}
	}

	@FXML
	void finalSave(ActionEvent event) {
		if (null != nutritionValueMst) {

			nutritionValueMst.setItemMst(itemmst);
			nutritionValueMst.setCalories(txtcalories.getText());
			nutritionValueMst.setFat(txtFat.getText());
			nutritionValueMst.setServingSize(txtservingSize.getText());
			RestCaller.updateNutritionValueMst(nutritionValueMst);
			txtcalories.clear();
			txtFat.clear();
			txtItemNameNutritionValue.clear();
			txtservingSize.clear();
			tblNutrition.getItems().clear();
			txtpercentage.clear();
			nutritionValueDtl = null;
			nutritionValueMst = null;
			nutritionList.clear();
		}
	}

	@FXML
	void nutritionOnEnter(KeyEvent event) {

	}

	@FXML
	void onActionBestBefore(ActionEvent event) {

		if (null == txtIngredient1.getText()) {
			txtIngredient1.requestFocus();
		}

	}

	@FXML
	void calibrateAction(ActionEvent event) {
		if (txtPrinterName.getText().trim().isEmpty()) {
			notifyMessage(5, "please PrinterName", false);
			return;
		}

		String PrinterNAme = SystemSetting.printer_name;
		TSCBarcode.caliberate(PrinterNAme);
	}

	@FXML
	void onActionEnterDuration(ActionEvent event) {
		LocalDate bestBefore = dpProduction.getValue();

		Integer duration = Integer.valueOf(txtDuration.getText());
		LocalDate expDate = bestBefore.plusDays(duration);
		txtbestBefore.setText(String.valueOf(expDate));
	}

	@FXML
	void onActionEnterQty(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if (null == txtDuration.getText() || txtDuration.getText().trim().length() == 0) {
				txtDuration.requestFocus();
			} else {
				btnSaveBarcode.requestFocus();
			}
		}
	}

	private void fetchData() {
		ResponseEntity<NutritionValueMst> savedNutritionMst = RestCaller.getNutritionValueMstByItemMst(itemmst);

		nutritionValueMst = savedNutritionMst.getBody();
		if (null != nutritionValueMst) {
			txtcalories.setText(savedNutritionMst.getBody().getCalories());
			txtFat.setText(savedNutritionMst.getBody().getFat());
			txtservingSize.setText(savedNutritionMst.getBody().getServingSize());
			ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller
					.getNutritionValueDtlByMstId(nutritionValueMst);
			nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
			fillTable();
		} else {
			txtcalories.clear();
			txtFat.clear();
			txtItemName.clear();
			txtNutritionValue.clear();
			txtpercentage.clear();
			txtSerial.clear();
			txtservingSize.clear();
			tblNutrition.getItems().clear();
		}
	}

	private void addItems() {

		ResponseEntity<NutritionValueMst> savedNutritionMst = RestCaller.getNutritionValueMstByItemMst(itemmst);

		if (null != savedNutritionMst.getBody()) {
			nutritionValueMst = new NutritionValueMst();

			fetchData();
		}

		if (null == nutritionValueMst) {
			nutritionValueMst = new NutritionValueMst();
			nutritionValueMst.setItemMst(itemmst);
			nutritionValueMst.setCalories(txtcalories.getText());
			nutritionValueMst.setFat(txtFat.getText());
			nutritionValueMst.setServingSize(txtservingSize.getText());
			ResponseEntity<NutritionValueMst> respentity = RestCaller.saveNutritionValueMst(nutritionValueMst);
			nutritionValueMst = respentity.getBody();
		}

		if (txtSerial.getText().isEmpty()) {
			notifyMessage(5, "Please Type Serial Number");
			txtSerial.requestFocus();
			return;
		}
		nutritionValueDtl = new NutritionValueDtl();
		nutritionValueDtl.setNutritionValueMst(nutritionValueMst);
		nutritionValueDtl.setNutrition(cmbNutrition.getSelectionModel().getSelectedItem());
		nutritionValueDtl.setValue(txtNutritionValue.getText());

		nutritionValueDtl.setPercentage(txtpercentage.getText());

		nutritionValueDtl.setSerial(txtSerial.getText());

		ResponseEntity<NutritionValueDtl> getNutrition = RestCaller.getNutritionValueMstByItemMstAndNutrition(
				nutritionValueMst.getId(), cmbNutrition.getSelectionModel().getSelectedItem());
		if (null != getNutrition.getBody()) {
			nutritionValueDtl = getNutrition.getBody();

			nutritionValueDtl.setNutritionValueMst(nutritionValueMst);
			nutritionValueDtl.setNutrition(cmbNutrition.getSelectionModel().getSelectedItem());
			nutritionValueDtl.setValue(txtNutritionValue.getText());

			nutritionValueDtl.setPercentage(txtpercentage.getText());

			nutritionValueDtl.setSerial(txtSerial.getText());

			RestCaller.updateNUtritionValueDtl(nutritionValueDtl);
			ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller
					.getNutritionValueDtlByMstId(nutritionValueMst);
			nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
		} else {
			ResponseEntity<NutritionValueDtl> respentity = RestCaller.saveNutritionValueDtl(nutritionValueDtl);
			nutritionValueDtl = respentity.getBody();
			nutrintionValueList.add(nutritionValueDtl);
		}
		fillTable();
		cmbNutrition.getSelectionModel().clearSelection();
		txtNutritionValue.clear();
		txtpercentage.clear();
		txtSerial.clear();
		cmbNutrition.requestFocus();
	}

	private void fillTable() {
		tblNutrition.setItems(nutrintionValueList);
		clNutrition.setCellValueFactory(cellData -> cellData.getValue().getnutritionNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());
		clPercentage.setCellValueFactory(cellData -> cellData.getValue().getpercentageProperty());
		clSerial.setCellValueFactory(cellData -> cellData.getValue().getserialProperty());

	}

	@FXML
	void percentageOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtSerial.requestFocus();
		}
	}

	@FXML
	void printEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			print();

		}
	}

	@FXML
	void PrintAction(KeyEvent event) {
		print();
	}

	@FXML
	void saveAction(ActionEvent event) {
		print();
	}

	@FXML
	void serialOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnAdd.requestFocus();
		}
	}

	@FXML
	void servingSizeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtcalories.requestFocus();
		}
	}

	@FXML
	void fetchData(ActionEvent event) {

		fetchData();
	}

	@FXML
	void clearItems(ActionEvent event) {

		txtcalories.clear();
		txtFat.clear();
		txtItemName.clear();
		txtservingSize.clear();
		tblNutrition.getItems().clear();
		txtpercentage.clear();
		nutritionValueDtl = null;
		nutritionValueMst = null;
		nutritionList.clear();

	}

	@FXML
	void showpopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			eventBus.register(this);
			showPopup();
			// txtQty.requestFocus();

			/*
			 * if(txtDuration.getText().equalsIgnoreCase(null)) {
			 * 
			 * txtDuration.requestFocus(); }
			 * 
			 * if(txtIngredient1.getText().equalsIgnoreCase(null)) {
			 * txtIngredient1.requestFocus();
			 * }if(txtIngredient2.getText().equalsIgnoreCase(null)) {
			 * txtIngredient2.requestFocus();
			 * 
			 * } else { txtQty.requestFocus(); }
			 */
		}
	}

	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			dpProduction.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void valueOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtpercentage.requestFocus();
		}
	}

	@FXML
	void actionOnIngredients2(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnSaveBarcode.requestFocus();
			print();
		}

	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {
		txtItemName.clear();
		txtBarcode.clear();
		txtBarcode.clear();
		txtIngredient1.clear();
		txtDuration.clear();
		txtDuration.clear();
		txtMrp.clear();
		txtIngredient2.clear();
		System.out.println("------@@@-------popupItemlistner-------@@@@------" + itemPopupEvent.getItemName());
		Stage stage = (Stage) btnCalibrate.getScene().getWindow();
		if (stage.isShowing()) {

			txtItemName.setText(itemPopupEvent.getItemName());

			String itemId = itemPopupEvent.getItemId();

			ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(itemId);
			if (respentity == null) {

				return;
			}

			ItemMst itemMst = respentity.getBody();

			if (null != itemMst.getBarCode()) {
				txtBarcode.setText(itemMst.getBarCode());
			}
			txtMrp.setText(String.valueOf(itemMst.getStandardPrice()));

			if (null != itemMst.getBarCodeLine1()) {
				txtIngredient1.setText(itemMst.getBarCodeLine1());
			}

			if (null != itemMst.getBarCodeLine2()) {
				txtIngredient2.setText(itemMst.getBarCodeLine2());
			}

			if (null != itemMst.getBestBefore()) {
				txtDuration.setText(itemMst.getBestBefore() + "");
				LocalDate bestBeforeDays = dpProduction.getValue();

				LocalDate expDate = bestBeforeDays.plusDays(itemMst.getBestBefore());
				txtbestBefore.setText(String.valueOf(expDate));
				// txtQty.requestFocus();
			} else {
				txtDuration.requestFocus();
			}

		}
	}

	@FXML
	void actionPressedTo(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtIngredient1.getText().trim().isEmpty()) {
				txtIngredient1.requestFocus();

			} else if (txtIngredient2.getText().trim().isEmpty()) {
				txtIngredient2.requestFocus();
			} else {
				btnSaveBarcode.requestFocus();
			}
		}

	}

	@FXML
	void actionBestBefore(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtIngredient1.getText().trim().isEmpty()) {
				txtIngredient1.requestFocus();

				System.out.print("                         inside best vbefore action issssssssssssssssssssssssss");
			}
		}

	}

	@FXML
	void actionOnIngredients(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtIngredient2.requestFocus();
		}

	}

	@FXML
	void BarcodeEnterActionSS(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (txtbarCode.getText().trim().isEmpty()) {
				String barcode = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
				txtbarCode.setText(barcode);
			}
			choiceUnit.requestFocus();
		}

	}

	@FXML
	void loadItem(MouseEvent event) {

		showItem();
	}

	@FXML
	void caloriesOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtFat.requestFocus();
		}
	}

	@FXML
	void addOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItems();
		}
	}

	@FXML
	void fatOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbNutrition.requestFocus();
		}
	}

	@FXML
	void Refresh(ActionEvent event) {
		UnitDtl unitdtl = unitdtList.get(0);
		RestCaller.CallRest(UnitDtl.class, unitdtl, "");
		eventBus.post(unitdtList.get(0));
	}

	@FXML
	void HsnCodeEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtsellingPrice.requestFocus();
		}

	}

	@FXML
	void FocusReodrWaitingTime(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtReoder.requestFocus();
		}

	}

	@FXML
	void AddItemAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (txtReoder.getText().length() > 0) {
				txtBestBefore.requestFocus();
			}
		}
	}

	@FXML
	void ItemCodeEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (txtitemCode.getText().trim().isEmpty()) {
				String itemcode = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
				txtitemCode.setText(itemcode);
			}
			choiceCategory.requestFocus();
		}

	}

	@FXML
	void ItemNameEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtbarCode.requestFocus();
		}

	}

	@FXML
	void SellingPriceEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			TxtStateCess.requestFocus();
		}

	}

	@FXML
	void StateCessEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

		}
	}

	@FXML
	void TaxRateEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtitemCode.requestFocus();
		}

	}

	@FXML
	void actionDeleteCategoryNew(ActionEvent event) {

		ResponseEntity<CategoryMst> categoryMst = RestCaller.getCategoryByName(txtIventoryCategory.getText());
		if (null == categoryMst) {
			return;
		}
		if (null == categoryMst.getBody()) {
			return;
		}
		if (null == categoryMst.getBody().getId()) {
			return;
		}
		RestCaller.CategoryDeleteById(categoryMst.getBody().getId());
		tblCategoryList.getItems().clear();
		notifyMessage(3, "DELETED");
		txtIventoryCategory.clear();
	}

	@FXML
	void PrecisionEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnsave.requestFocus();
		}

	}

	@FXML
	void UnitDescriptionEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtprecision.requestFocus();
		}

	}

	@FXML
	void UnitNameEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtunitDescription.requestFocus();
		}

	}

	@FXML
	void CategoryNameEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			cmbInventoryParentId.requestFocus();
		}
	}

	@FXML
	void ConvertionEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnSave1.requestFocus();
		}

	}

	@FXML
	void PrimaryUnitEnterActionSSs(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtconversion.requestFocus();
		}

	}

	@FXML
	void ItemNameunitCreationEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			cmbSecondaryunit.requestFocus();
		}

	}

	@FXML
	void bestBeforeEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtBestBefore.getText().trim().length() > 0) {
				submitItem();
			}
		}
	}

	@FXML
	void ComboCategoryEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txthsnCode.requestFocus();
		}

	}

	@FXML
	void ComboUniteFocusAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txttaxRate.requestFocus();
		}

	}

	@FXML
	void SecondaryUnitEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtprimaryunit.requestFocus();
		}
	}

	@FXML
	void ParentIdEnterAction(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnInventoryAdd.requestFocus();
		}
	}

	@FXML
	void ActionShowAllCategory(ActionEvent event) {

		categoryList2.clear();

		ArrayList cat = new ArrayList();
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object parentId = lm.get("parentId");
			Object id = lm.get("id");
			if (id != null) {
				CategoryMst category = new CategoryMst();
				category.setCategoryName((String) categoryName);
				category.setParentId((String) parentId);
				category.setId((String) id);
				categoryList2.add(category);

			}

		}
		FillTableCategoryList();

	}

	private void showItem() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			txtservingSize.requestFocus();
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void OnSearchUnit(ActionEvent event) {

	}

	@FXML
	void onDeleteUnit(ActionEvent event) {

	}

	@FXML
	void onSaveUnit(ActionEvent event) {
		UnitDtl unitdtl = unitdtList.get(0);
		RestCaller.CallRest(UnitDtl.class, unitdtl, "");
		eventBus.post(unitdtList.get(0));

	}

	@FXML
	void onShowAllunit(ActionEvent event) {

	}

	@FXML
	void barcode(ActionEvent event) {

		String barcode = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
		txtbarCode.setText(barcode);

	}

	@FXML
	void clear(ActionEvent event) {
		clearField();
		tblCategoryList.getItems().clear();
		tblItemList.getItems().clear();
		itemmst = null;

	}

	public void clearField() {
		txtReoder.clear();
		txtbarCode.clear();
		txtitemName.clear();
		txttaxRate.clear();
		txtitemCode.clear();
		txthsnCode.clear();
		txtsellingPrice.clear();
		TxtStateCess.clear();
//		tblItemList.getItems().clear();
		choiceCategory.getSelectionModel().clearSelection();
		choiceUnit.getSelectionModel().clearSelection();
		choiceUnit.setDisable(false);

		txtBestBefore.clear();
	}

	private void print() {
		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "please select item", false);
			return;
		}
		if (txtDuration.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter Duration", false);
			return;
		}
		if (txtbestBefore.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter best before date", false);
			return;
		}
		if (txtIngredient1.getText().trim().isEmpty())

		{
			txtIngredient1.setText("");
			notifyMessage(5, "please incredient", false);
			return;
		}

		ResponseEntity<ItemMst> respentity = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		ItemMst itemMst = respentity.getBody();
		itemMst.setBarCode(txtBarcode.getText());
		itemMst.setBestBefore(Integer.valueOf(txtDuration.getText()));
		itemMst.setBarCodeLine1(txtIngredient1.getText());
		itemMst.setBarCodeLine2(txtIngredient2.getText());
		RestCaller.updateBarcode(itemMst);

		/*
		 * String NumberOfCopies = txtQty.getText(); String ManufactureDate = "Mf:" +
		 * String.valueOf(dpProduction.getValue()); String ExpiryDate = "Exp:" +
		 * txtbestBefore.getText(); String ItemName = txtItemName.getText(); String
		 * Barcode = txtBarcode.getText(); String Mrp = "Mrp:" + txtMrp.getText();
		 * String IngLine1 = txtIngredient1.getText(); String IngLine2 =
		 * txtIngredient2.getText(); String PrinterNAme = SystemSetting.printer_name;
		 * 
		 * 
		 * TSCBarcode.manualPrint(NumberOfCopies, ManufactureDate, ExpiryDate, ItemName,
		 * Barcode, Mrp, IngLine1, IngLine2, PrinterNAme);
		 */

		BarCodeTextclear();

	}

	private void BarCodeTextclear() {

		txtItemName.clear();
		txtBarcode.clear();
		txtMrp.clear();
		txtDuration.clear();
		txtbestBefore.setText("");
		txtIngredient1.clear();
		txtIngredient2.clear();
		dpProduction.setValue(null);
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	void delete(ActionEvent event) {

		if (null != itemmst) {

			System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
			if (null != itemmst.getId()) {
				System.out.println("YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY");
				itemmst.setIsDeleted("Y");
				String s = RestCaller.deleteItem(itemmst.getId());
				itemmst = null;
				choiceUnit.setDisable(false);
				showItemDetials();
				clearField();
			}

		}

	}

	@FXML
	void itemcode(ActionEvent event) {

		String itemcode = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
		txtitemCode.setText(itemcode);

	}

	@FXML
	void loadImage(ActionEvent event) {

	}

	@FXML
	void mobilePublish(ActionEvent event) {

	}

	@FXML
	void ActionGetCategory(MouseEvent event) {

	}

	@FXML
	void ActionGetUnit(MouseEvent event) {

//		ItemMst itemmst = new ItemMst();
//		itemList.add(itemmst);
//		choiceUnit.setItems(null);
	}

	@FXML
	void ActionAddCategory(ActionEvent event) {

		CategoryMst categoryMst = new CategoryMst();
		ResponseEntity<CategoryMst> categoryMstresp = RestCaller.getCategoryByName(txtIventoryCategory.getText());
		// if(null != categoryMstresp)
		// {
		if (null != categoryMstresp.getBody()) {
			categoryMst.setId(categoryMstresp.getBody().getId());
			categoryMst.setCategoryName(txtIventoryCategory.getText());

			if (null != cmbInventoryParentId.getSelectionModel().getSelectedItem()) {
				String catName = cmbInventoryParentId.getSelectionModel().getSelectedItem().toString();
				for (CategoryMst cc : categoryList) {
					if (cc.getCategoryName() == catName) {
						categoryMst.setParentId(cc.getId());
					}
				}
			}
			// put
			RestCaller.updateCategory(categoryMst);
			ClearCategory();
			eventBus.post(categoryMst);
			// categoryList2.add(categoryMst);
			// get all category list
			choiceCategory.getItems().clear();
			cmbInventoryParentId.getItems().clear();
			ArrayList cat = new ArrayList();

			cat = RestCaller.SearchCategory();

			Iterator itr1 = cat.iterator();
			while (itr1.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr1.next();
				Object categoryName = lm.get("categoryName");
				Object id = lm.get("id");
				if (id != null) {
					choiceCategory.getItems().add((String) categoryName);
					cmbInventoryParentId.getItems().add((String) categoryName);
					CategoryMst category = new CategoryMst();
					category.setCategoryName((String) categoryName);
					category.setId((String) id);
					categoryList.add(category);
				}

			}

			FillTableCategoryList();
			ClearCategory();
		}
		// }
		else {
			categoryList2.clear();

			if (null != cmbInventoryParentId.getSelectionModel().getSelectedItem()) {
				String catName = cmbInventoryParentId.getSelectionModel().getSelectedItem().toString();
				for (CategoryMst cc : categoryList) {
					if (cc.getCategoryName() == catName) {
						categoryMst.setParentId(cc.getId());
					}
				}
			}
			String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "CAT");

			categoryMst.setId(vNo + SystemSetting.getUser().getCompanyMst().getId());
			categoryMst.setCategoryName(txtIventoryCategory.getText());
			
			//===========code for identify whether the category is saved on offline or online mode=======01-07-2021====
			
			String status = null;
			ResponseEntity<ParamValueConfig> getParamValueCat = RestCaller.getParamValueConfig("OFFLINE_CATEGORY_CREATION");
			ParamValueConfig paramValueConfig = getParamValueCat.getBody();
			if(chkCategoryOnline.isSelected() || null == paramValueConfig || paramValueConfig.getValue().equalsIgnoreCase("NO")) {
				ResponseEntity<CategoryMst> respentity = RestCaller.saveCategory(categoryMst);
				categoryMst = respentity.getBody();
			}else {
				status = "ONLINE";
				ResponseEntity<CategoryMst> respentity = RestCaller.saveCategory(categoryMst,status);
				categoryMst = respentity.getBody();
			}
			
			
			
			//=================code end for saving category==============================
			
			

			eventBus.post(categoryMst);
			categoryList2.add(categoryMst);
			// get all category list
			choiceCategory.getItems().clear();
			cmbInventoryParentId.getItems().clear();
			ArrayList cat = new ArrayList();

			cat = RestCaller.SearchCategory();
			Iterator itr1 = cat.iterator();
			while (itr1.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr1.next();
				Object categoryName = lm.get("categoryName");
				Object id = lm.get("id");
				if (id != null) {
					choiceCategory.getItems().add((String) categoryName);
					cmbInventoryParentId.getItems().add((String) categoryName);
					CategoryMst category = new CategoryMst();
					category.setCategoryName((String) categoryName);
					category.setId((String) id);
					categoryList.add(category);
				}

			}

			FillTableCategoryList();
			ClearCategory();

		}
	}

	private void ClearCategory() {

		txtIventoryCategory.setText("");
		cmbInventoryParentId.getSelectionModel().clearSelection();
	}

	@FXML
	void ActionDeleteCategory(ActionEvent event) {

	}

	@FXML
	private void initialize() {
		eventBus.register(this);
		cmbPropertyType.getItems().add("STRING");
		cmbPropertyType.getItems().add("NUMBER");
		cmbPropertyType.getItems().add("BOOLEAN");
		dpProduction.setValue(LocalDate.now());
		btnCalibrate.setVisible(false);

		// cmbBarcodeFormat.getItems().add(e);
		
		
		/*
		 * //check box for deciding the item save process is done on either offline or
		 * online=======01-07-2021 ResponseEntity<ParamValueConfig> getParamValue =
		 * RestCaller.getParamValueConfig("OFFLINE_ITEM_CREATION"); if (null !=
		 * getParamValue.getBody()) {
		 * 
		 * if (getParamValue.getBody().getValue().equalsIgnoreCase("YES")) {
		 * chkOnline.setDisable(false); } else { chkOnline.setDisable(true); } } else {
		 * chkOnline.setDisable(true);
		 * 
		 * } chkOnline.selectedProperty().addListener( (obs, oldSelection, newSelection)
		 * -> { if(chkOnline.isSelected()) { chkOnline.setText("Offline"); }else {
		 * chkOnline.setText("Online"); } });
		 */
		
		//============================end=====================01-07-2021==============
		
		
		//check box for deciding the category save process is done on either offline or online=======01-07-2021
		/*
		 * ResponseEntity<ParamValueConfig> getParamValueCat =
		 * RestCaller.getParamValueConfig("OFFLINE_CATEGORY_CREATION"); if (null !=
		 * getParamValueCat.getBody()) { if
		 * (getParamValueCat.getBody().getValue().equalsIgnoreCase("YES")) {
		 * chkCategoryOnline.setDisable(false); } else {
		 * chkCategoryOnline.setDisable(true); } }else {
		 * chkCategoryOnline.setDisable(true);
		 * 
		 * }
		 * 
		 * chkCategoryOnline.selectedProperty().addListener( (obs, oldSelection,
		 * newSelection) -> { if(chkCategoryOnline.isSelected()) {
		 * chkCategoryOnline.setText("Offline"); }else {
		 * chkCategoryOnline.setText("Online"); } });
		 */
		//============================end=====================01-07-2021==============
		
				
		//check box for deciding the unit save process is done on either offline or online=======01-07-2021
		/*
		 * ResponseEntity<ParamValueConfig> getParamValueUnit =
		 * RestCaller.getParamValueConfig("OFFLINE_UNIT_CREATION"); if (null !=
		 * getParamValueUnit.getBody()) { if
		 * (getParamValueUnit.getBody().getValue().equalsIgnoreCase("YES")) {
		 * chkUnitOnline.setDisable(false); } else { chkUnitOnline.setDisable(true); }
		 * }else { chkUnitOnline.setDisable(true);
		 * 
		 * } chkUnitOnline.selectedProperty().addListener( (obs, oldSelection,
		 * newSelection) -> { if(chkUnitOnline.isSelected()) {
		 * chkUnitOnline.setText("Offline"); }else { chkUnitOnline.setText("Online"); }
		 * });
		 */
				
		//============================end=====================01-07-2021==============		
				
				
		ResponseEntity<List<BarcodeFormatMst>> barcodeFormatResp = RestCaller.getAllBarCodeFormatMst();

		List<BarcodeFormatMst> barcodeFormatList = new ArrayList<BarcodeFormatMst>();
		barcodeFormatList = barcodeFormatResp.getBody();

		for (BarcodeFormatMst barcodeFormat : barcodeFormatList) {
			cmbBarcodeFormat.getItems().add(barcodeFormat.getBarcodeFormatName());

		}

//		txtprintername.setText(SystemSetting.getNutrition_facts_printer());
		// eventBus.register(this);
		ResponseEntity<List<NutritionMst>> savedNutritions = RestCaller.getAllNutritions();
		nutritionList = FXCollections.observableArrayList(savedNutritions.getBody());
		for (NutritionMst nutrtionMst : nutritionList) {
			cmbNutrition.getItems().add(nutrtionMst.getNutrition());
		}
		tblNutrition.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getId()) {
					nutritionValueDtl = new NutritionValueDtl();
					nutritionValueDtl.setId(newSelection.getId());

				}

			}

		});

		tbItemProperty.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getId()) {
					itemPropertyConfig = new ItemPropertyConfig();
					itemPropertyConfig.setId(newSelection.getId());

				}

			}

		});
		txtitemName.textProperty().bindBidirectional(SearchString);

		// --------------
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clItemCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
		clItemUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clItemSellingPrice.setCellValueFactory(cellData -> cellData.getValue().getStandardPriceProperty());
		clItemTax.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clStatus.setCellValueFactory(cellData -> cellData.getValue().getDeletedStatusProperty());

		TxtStateCess.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					TxtStateCess.setText(oldValue);
				}
			}
		});

		txttaxRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txttaxRate.setText(oldValue);
				}
			}
		});

		txtsellingPrice.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtsellingPrice.setText(oldValue);
				}
			}

		});
		txtprecision.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtprecision.setText(oldValue);
				}
			}

		});
//		eventBus.register(this);
		UnitMst unitMst = new UnitMst();
		// set unit
		ArrayList unit = new ArrayList();

		unit = RestCaller.SearchUnit();
		Iterator itr = unit.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object unitName = lm.get("unitName");
			Object unitId = lm.get("id");
			if (unitId != null) {
				choiceUnit.getItems().add((String) unitName);
				UnitMst newUnitMst = new UnitMst();
				newUnitMst.setUnitName((String) unitName);
				newUnitMst.setId((String) unitId);
				unitList.add(newUnitMst);
			}

		}

		// set category
		ArrayList cat = new ArrayList();

		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				choiceCategory.getItems().add((String) categoryName);
				cmbInventoryParentId.getItems().add((String) categoryName);
				CategoryMst categoryMst = new CategoryMst();
				categoryMst.setCategoryName((String) categoryName);
				categoryMst.setId((String) id);
				categoryList.add(categoryMst);
			}

		}

		// --------------------------------

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				LoadItemBySearch((String) newValue);
			}
		});

		itemmst = new ItemMst();
		tblItemList.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getItemName() && newSelection.getItemName().length() > 0) {
					if (null != newSelection.getId()) {

						ResponseEntity<ItemMst> itemDetailsSaved = RestCaller.getitemMst(newSelection.getId());
						itemmst = itemDetailsSaved.getBody();

						if (null != itemmst) {
							if (null != itemmst.getItemName()) {
								txtitemName.setText(itemmst.getItemName());
							}
							if (null != itemmst.getBarCode()) {
								txtbarCode.setText(itemmst.getBarCode());
							}
							if (null != itemmst.getTaxRate()) {

								txttaxRate.setText(Double.toString(itemmst.getTaxRate()));
							}
							if (null != itemmst.getItemCode()) {
								txtitemCode.setText(itemmst.getItemCode());
							}
							if (null != itemmst.getItemCode()) {
								txtitemCode.setText(itemmst.getItemCode());
							}
							if (null != itemmst.getHsnCode()) {
								txthsnCode.setText(itemmst.getHsnCode());
							}
							if (null != itemmst.getReorderWaitingPeriod()) {
								txtReoder.setText(Integer.toString(itemmst.getReorderWaitingPeriod()));
							}
							if (null != itemmst.getItemCode()) {
								txtitemCode.setText(itemmst.getItemCode());
							}
//							if (null != itemmst.getHsnCode()) {
//								txthsnCode.setText(itemmst.getHsnCode());
//							}
							if (null != itemmst.getCess()) {
								TxtStateCess.setText(Double.toString(itemmst.getCess()));
							}
							if (null != itemmst.getStandardPrice()) {
								txtsellingPrice.setText(Double.toString(itemmst.getStandardPrice()));
							}
							if (null != itemmst.getReorderWaitingPeriod()) {
								txtReoder.setText(Integer.toString(itemmst.getReorderWaitingPeriod()));
							}

							if (null != itemmst.getBestBefore()) {
								txtBestBefore.setText(Integer.toString(itemmst.getBestBefore()));
							}

							if (null != itemmst.getCategoryId()) {
								for (CategoryMst m : categoryList) {
									if (m.getId().equals(itemmst.getCategoryId())) {
										String catName = m.getCategoryName();
										choiceCategory.getSelectionModel().select(catName);
									}
								}
							}

							choiceUnit.setDisable(true);

							if (null != itemmst.getUnitId()) {

								for (UnitMst m : unitList) {
									if (m.getId().equals(itemmst.getUnitId())) {

										choiceUnit.getSelectionModel().select(m.getUnitName());
									}
								}
							}
						}

					}

				}
			}
		});

		tblCategoryList.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					txtIventoryCategory.setText(newSelection.getCategoryName());
					cmbInventoryParentId.setValue(newSelection.getParentName());

				}
			}
		});

	}

	@FXML
	void save(ActionEvent event) {
////==========unit remove commands

		UnitMst unitmst = new UnitMst();
		if (txtunitName.getText().trim().isEmpty()) {
			notifyMessage(5, "please select unitmame", false);
			return;
		}
		if (txtunitDescription.getText().trim().isEmpty()) {
			notifyMessage(5, "please select unitdescription", false);
			return;
		}
		if (txtprecision.getText().trim().isEmpty()) {
			notifyMessage(5, "please select precesion", false);
			return;
		}
		unitmst.setUnitName(txtunitName.getText());
		unitmst.setUnitDescription(txtunitDescription.getText());
		unitmst.setUnitPrecision(Double.parseDouble(txtprecision.getText()));
		String vNo = RestCaller.getVoucherNumber(SystemSetting.getUser().getCompanyMst().getId() + "UT");
		unitmst.setId(vNo);
		unitmst.setBranchCode(SystemSetting.systemBranch);
		
		
 		//===========code for identify whether the unit is saved on offline or online mode=======01-07-2021====

		String status = null;
		
		/*
		 * ResponseEntity<ParamValueConfig> getParamValueUnit =
		 * RestCaller.getParamValueConfig("OFFLINE_UNIT_CREATION"); ParamValueConfig
		 * paramValueConfig = getParamValueUnit.getBody(); if(chkUnitOnline.isSelected()
		 * || null == paramValueConfig ||
		 * paramValueConfig.getValue().equalsIgnoreCase("NO")) {
		 * RestCaller.CallRest(UnitMst.class, unitmst, "unitmst"); }else { status =
		 * "ONLINE"; RestCaller.CallRest(UnitMst.class, unitmst, "unitmst",status); }
		 */
		
		RestCaller.CallRest(UnitMst.class, unitmst, "unitmst");
		
		//=================code end for saving unit==============================
		
		eventBus.post(unitmst);
		notifyMessage(5, "Saved Sucessfully", false);
		txtunitName.clear();
		txtunitDescription.clear();
		txtprecision.clear();
		choiceUnit.getItems().clear();
		ArrayList unit = new ArrayList();

		unit = RestCaller.SearchUnit();
		Iterator itr = unit.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object unitName = lm.get("unitName");
			Object unitId = lm.get("id");
			if (unitId != null) {
				choiceUnit.getItems().add((String) unitName);
				UnitMst newUnitMst = new UnitMst();
				newUnitMst.setUnitName((String) unitName);
				newUnitMst.setId((String) unitId);
				unitList.add(newUnitMst);
			}

		}
	}

	@FXML
	void showall(ActionEvent event) {

		showItemDetials();

//		tblItemList.getItems().clear();
//		ArrayList cat = new ArrayList();
//		 
//		cat = RestCaller.SearchItemMsts();
//		Iterator itr1 = cat.iterator();
//		while (itr1.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr1.next();
//			Object itemName = lm.get("itemName");
//			Object standardPrice = lm.get("standardPrice");
//			Object taxRate = lm.get("taxRate");
//			Object unitId = lm.get("unitId");
//			Object categoryId = lm.get("categoryId");
//			Object isDeleted = lm.get("isDeleted");
//			Object sgst = lm.get("sgst");
//			Object id = lm.get("id");
//			if (id != null) {
//				ItemMst itemMst = new ItemMst();
//				itemMst.setItemName((String) itemName);
//				itemMst.setStandardPrice((Double) standardPrice);
//				itemMst.setTaxRate((Double) taxRate);
//				itemMst.setUnitId((String) unitId);
//				itemMst.setCategoryId((String) categoryId);
//				itemMst.setId((String) id);
//				itemMst.setIsDeleted((String) isDeleted);
//				itemMst.setSgst((Double) sgst);
//				ItemTableList2.add(itemMst);
//				ShowAllFillTable();
//			}
//
//		}

	}

	@FXML

	void submit(ActionEvent event) {
		submitItem();

	}

	public void submitItem() {

		ItemMst itemMst = new ItemMst();

		if (txtitemCode.getText().trim().isEmpty()) {

			notifyMessage(5, " Please enter Item Code...!!!");

			return;

		} else if (txttaxRate.getText().trim().isEmpty()) {

			notifyMessage(5, " Please enter Tax rate...!!!");

			return;

		} else if (txtsellingPrice.getText().trim().isEmpty()) {

			notifyMessage(5, " Please enter Selling Price...!!!");

			return;

		} 
//		else if (txthsnCode.getText().trim().isEmpty()) {
//
//			notifyMessage(5, " Please enter Hsn Code...!!!");
//
//			return;
//
//		} 
		else if (txtbarCode.getText().trim().isEmpty()) {

			notifyMessage(5, " Please enter BarCode...!!!");
			return;

		} else if (choiceUnit.getSelectionModel().isEmpty()) {

			notifyMessage(5, " Please Select Unit...!!!");

			return;

		} else if (choiceCategory.getSelectionModel().isEmpty()) {

			notifyMessage(5, " Please Select Category...!!!");
			return;

		} else {

			if (null != itemmst) {
				itemMst.setId(itemmst.getId());
			}
			if (txtitemName.getText().contains("%") || (txtitemName.getText().contains("/"))
					|| (txtitemName.getText().contains("?")) || txtitemName.getText().contains("&")
					|| txtitemName.getText().contains("*") || txtitemName.getText().contains("$")) {
				notifyMessage(5, "ItemName Invalid");
				return;
			}
			String itemName = txtitemName.getText().trim();

			itemName = itemName.replaceAll("'", "");
			itemName = itemName.replaceAll("&", "");

			itemName = itemName.replaceAll("/", "");
			itemName = itemName.replaceAll("%", "");
			itemName = itemName.replaceAll("\"", "");
			itemMst.setItemName(itemName);
			itemMst.setItemCode(txtitemCode.getText().trim());
			itemMst.setTaxRate(Double.parseDouble(txttaxRate.getText()));
			itemMst.setStandardPrice(Double.parseDouble(txtsellingPrice.getText()));
			itemMst.setServiceOrGoods("ITEM MST");
			itemMst.setRank(0);
			if (txthsnCode.getText().trim().isEmpty()) {
				itemMst.setHsnCode("0");
			}
			else {
			itemMst.setHsnCode(txthsnCode.getText().trim());
			}

			itemMst.setBarcodeFormat(cmbBarcodeFormat.getSelectionModel().getSelectedItem());
			if (!txtBestBefore.getText().trim().isEmpty()) {
				itemMst.setBestBefore(Integer.parseInt(txtBestBefore.getText()));
			} else {
				itemMst.setBestBefore(0);
			}
			itemMst.setBarCode(txtbarCode.getText().trim());
			if (!TxtStateCess.getText().trim().isEmpty()) {

				itemMst.setCess(Double.parseDouble(TxtStateCess.getText()));
			} else {
				itemMst.setCess(0.0);
			}

			if (!txtReoder.getText().trim().isEmpty()) {

				itemMst.setReorderWaitingPeriod(Integer.parseInt(txtReoder.getText()));
			} else {
				itemMst.setReorderWaitingPeriod(0);
			}

			String categoryName = choiceCategory.getSelectionModel().getSelectedItem().toString();

			ResponseEntity<CategoryMst> response = RestCaller.getCategoryByName(categoryName);
			CategoryMst categoryMst = response.getBody();

			/*
			 * for (CategoryMst u : categoryList) { if (u.getCategoryName() == categoryName)
			 * { itemMst.setCategoryId(u.getId()); } }
			 */

			itemMst.setCategoryId(categoryMst.getId());

			itemMst.setIsDeleted("N");
			itemMst.setBranchCode(SystemSetting.systemBranch);
			String unitName = choiceUnit.getSelectionModel().getSelectedItem().toString();
			for (UnitMst u : unitList) {
				if (u.getUnitName() == unitName) {
					itemMst.setUnitId(u.getId());

				}
			}
			ItemTableList2.clear();

			// version4.4
			ResponseEntity<ItemMst> getItemByName = RestCaller.getItemByNameRequestParam(txtitemName.getText());
			if (null != getItemByName.getBody()) {
				notifyMessage(3, "Item Already Exist", false);
				return;
			}

//			ResponseEntity<ItemMst> oldItemResp = RestCaller.getItemByName(itemMst.getItemName());
//			ItemMst oldItem = oldItemResp.getBody();
//			
//			if(null==oldItem) {
//				  oldItemResp = RestCaller.getItemByItemBarcode(itemMst);
//				  oldItem = oldItemResp.getBody();
//			}
			/*
			 * if (null != itemMst.getId()) {
			 * 
			 * RestCaller.updateItemMst(itemMst); notifyMessage(5, "Item Edited"); return; }
			 * if(null != getItemByName.getBody()) {
			 * 
			 * return; }
			 * 
			 * 
			 * else {
			 */
			// version4.4end
			String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "I");
			itemMst.setId(vNo + SystemSetting.getUser().getCompanyMst().getCompanyName());

			// =====Barcode duplication checking==========
			ResponseEntity<List<ItemMst>> getItemByBarcode = RestCaller.getItemByBarcode(txtbarCode.getText());
			if (getItemByBarcode.getBody().size() > 0) {
				notifyMessage(3, "Barcode Already Exist,Please Change the barcode!!!");
				return;
			}
//				
			// ==========================================
			
			
			
			//===========code for identify whether the item is saved on offline or online mode=======01-07-2021====
			String status = null;
			//ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("OFFLINE_ITEM_CREATION");
			//ParamValueConfig paramValueConfig = getParamValue.getBody();
//			if(chkOnline.isSelected() || null == paramValueConfig || paramValueConfig.getValue().equalsIgnoreCase("NO")) {
//				ResponseEntity<ItemMst> respentity = RestCaller.saveItemmst(itemMst);
//				itemMst = respentity.getBody();
//			}else {
//				status = "ONLINE";
//				ResponseEntity<ItemMst> respentity = RestCaller.saveItemmst(itemMst,status);
//				itemMst = respentity.getBody();
//			}
			//==========MAP-141-=====================================anandu===============
			/*
			 * if(null==paramValueConfig ||
			 * paramValueConfig.getValue().equalsIgnoreCase("YES")) { status = "ONLINE";
			 * ResponseEntity<ItemMst> respentity = RestCaller.saveItemmst(itemMst,status);
			 * itemMst = respentity.getBody(); if(null== itemMst) {
			 * notifyMessage(3,"Online Item creation failed"); } } else
			 * if(chkOnline.isSelected() ||
			 * paramValueConfig.getValue().equalsIgnoreCase("NO")) { ResponseEntity<ItemMst>
			 * respentity = RestCaller.saveItemmst(itemMst); itemMst = respentity.getBody();
			 * }
			 */
			
			
			status = "ONLINE";
			ResponseEntity<ItemMst> respentity = RestCaller.saveItemmst(itemMst);
			

			//=================code end for saving item==============================			
			

			savePriceDefenition(itemMst);
//			saveBatchPriceDefinition(itemMst);
			if (txtbarCode.getText().startsWith("21") && txtbarCode.getText().trim().length() == 7) {
				WeighingItemMst weighingItemMst = new WeighingItemMst();
				weighingItemMst.setBarcode(txtbarCode.getText());
				weighingItemMst.setItemId(itemMst.getId());
				weighingItemMst.setBranchCode(SystemSetting.getUser().getBranchCode());
				ResponseEntity<WeighingItemMst> savedWeighingItems = RestCaller
						.getWeighingItemMstByItemId(itemMst.getId());
				if (null != savedWeighingItems.getBody()) {
					RestCaller.updateWeighingItemMst(weighingItemMst);
				} else {
					ResponseEntity<WeighingItemMst> resp = RestCaller.saveWeighingItemMst(weighingItemMst);
				}
			}

			tblItemList.getItems().clear();
			ItemTableList2.add(itemMst);
			eventBus.post(itemMst);
			clearField();
			ShowAllFillTable();
			itemmst = null;
			itemmst = new ItemMst();
			txtitemName.requestFocus();
			choiceUnit.setDisable(false);
		}
	}

	private void savePriceDefenition(ItemMst itemMst) {

		PriceDefinition priceDefenition = new PriceDefinition();
		priceDefenition.setItemId(itemMst.getId());
		priceDefenition.setBranchCode(SystemSetting.systemBranch);
		priceDefenition.setEndDate(null);
		String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
		priceDefenition.setStartDate(Date.valueOf(sdate));
		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller.getPriceDefenitionByName("MRP");
		PriceDefenitionMst price = new PriceDefenitionMst();
		price = priceDefenitionSaved.getBody().get(0);
		if (null != price) {
			priceDefenition.setPriceId(price.getId());
		}
		priceDefenition.setAmount(itemMst.getStandardPrice());
		priceDefenition.setUnitId(itemMst.getUnitId());
		ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(priceDefenition);
		priceDefenition = respentity.getBody();

	}
//private void saveBatchPriceDefinition(ItemMst itemMst) {
//		
//		BatchPriceDefinition batchpriceDefenition = new BatchPriceDefinition();
//		batchpriceDefenition.setItemId(itemMst.getId());
//		batchpriceDefenition.setBranchCode(SystemSetting.systemBranch);
//		batchpriceDefenition.setEndDate(null);
//		String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
//		batchpriceDefenition.setStartDate(Date.valueOf(sdate));
//		batchpriceDefenition.setBatch("NOBATCH");
//		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
//				.getPriceDefenitionByName("MRP");
//		PriceDefenitionMst price = new PriceDefenitionMst();
//		price = priceDefenitionSaved.getBody().get(0);
//		if (null != price) {
//			priceDefenition.setPriceId(price.getId());
//		}
//		priceDefenition.setAmount(itemMst.getStandardPrice());
//		priceDefenition.setUnitId(itemMst.getUnitId());
//		ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(priceDefenition);
//		priceDefenition = respentity.getBody();
//
//	}

	public ObservableList<ItemMst> getItemMst() {

		return itemList;

	}

	public ObservableList<UnitMst> getUnitMst() {

		return unitList;

	}

	private void ShowAllFillTable() {

		tblItemList.setItems(ItemTableList2);

		for (ItemMst i : ItemTableList2) {
			if (null != i.getUnitId()) {
				for (UnitMst u : unitList) {
					if (i.getUnitId().equals(u.getId())) {
						i.setUnitName(u.getUnitName());
						clItemUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
					}
				}
			}

			if (null != i.getCategoryId()) {
				for (CategoryMst u : categoryList) {
					if (i.getCategoryId().equals(u.getId())) {
						i.setCategoryName(u.getCategoryName());
						clItemCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
					}
				}
			}

			if (null != i.getItemName()) {
				clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

			}
			if (null != i.getTaxRate()) {
				clItemTax.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
			}
			if (null != i.getStandardPrice()) {
				clItemSellingPrice.setCellValueFactory(cellData -> cellData.getValue().getStandardPriceProperty());
			}
			if (null != i.getIsDeleted()) {

				if (i.getIsDeleted().equals("Y")) {
					i.setDeletedStatus("DELETED");
					clStatus.setCellValueFactory(cellData -> cellData.getValue().getDeletedStatusProperty());
				} else {
					i.setDeletedStatus("ACTIVE");
					clStatus.setCellValueFactory(cellData -> cellData.getValue().getDeletedStatusProperty());
				}
			}
			if (null != i.getId()) {
				txtItemId.setText(i.getId());
			}

		}

	}

	// ------------------
	private void FillTableCategoryList() {
		tblCategoryList.setItems(categoryList2);
		for (CategoryMst i : categoryList2) {
			for (CategoryMst u : categoryList) {
				if (null != i.getParentId()) {
					if (i.getParentId().equals(u.getId())) {
						i.setParentName(u.getCategoryName());
						clParentCategory.setCellValueFactory(cellData -> cellData.getValue().getParentNameProperty());
					}
				}
			}

			clCategoryName.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());

		}

	}

	/*
	 * Why has supplier to do this item Master creation ?
	 */
	private void LoadItemBySearch(String searchData) {
		tblItemList.getItems().clear();

		itemList.clear();
		ArrayList item = new ArrayList();

		item = RestCaller.SearchItemByNameLimited(searchData);
		Iterator itr = item.iterator();

		Object id = null;

		while (itr.hasNext()) {
			java.util.LinkedHashMap element = (LinkedHashMap) itr.next();

			Object itemName = element.get("itemName");
			Object standardPrice = element.get("standardPrice");
			Object taxRate = element.get("taxRate");
			Object unitId = element.get("unitId");
			Object categoryId = element.get("categoryId");
			Object isDeleted = element.get("isDeleted");
			Object bestBefore = element.get("bestBefore");
			id = element.get("id");

			if (null != itemName) {
				ItemMst itemMst = new ItemMst();
				itemMst.setItemName((String) itemName);
				itemMst.setStandardPrice((Double) standardPrice);
				itemMst.setTaxRate((Double) taxRate);
				itemMst.setUnitId((String) unitId);
				itemMst.setCategoryId((String) categoryId);
				itemMst.setId((String) id);

				itemMst.setBestBefore((Integer) bestBefore);
				itemMst.setIsDeleted((String) isDeleted);
				ItemTableList2.add(itemMst);

			}

		}
		ShowAllFillTable();
		if (null == id)
			txtItemId.clear();
	}

	private void showItemDetials() {

		tblItemList.getItems().clear();
		ArrayList cat = new ArrayList();

		cat = RestCaller.SearchItemMsts();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object itemName = lm.get("itemName");
			Object standardPrice = lm.get("standardPrice");
			Object taxRate = lm.get("taxRate");
			Object unitId = lm.get("unitId");
			Object categoryId = lm.get("categoryId");
			Object isDeleted = lm.get("isDeleted");
			Object sgst = lm.get("sgst");
			Object id = lm.get("id");
			if (id != null) {
				ItemMst itemMst = new ItemMst();
				itemMst.setItemName((String) itemName);
				itemMst.setStandardPrice((Double) standardPrice);
				itemMst.setTaxRate((Double) taxRate);
				itemMst.setUnitId((String) unitId);
				itemMst.setCategoryId((String) categoryId);
				itemMst.setId((String) id);
				itemMst.setIsDeleted((String) isDeleted);
				itemMst.setSgst((Double) sgst);
				ItemTableList2.add(itemMst);

			}

		}
		ShowAllFillTable();

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	/*
	 * private void tableToTextITem() {
	 * 
	 * if (tblItemList.getSelectionModel().getSelectedItem() != null) {
	 * TableViewSelectionModel selectionModel = tblItemList.getSelectionModel();
	 * supplierList1 = selectionModel.getSelectedItems(); for (Supplier sup :
	 * supplierList1) { txtSupplierName.setText(sup.getSupplierName());
	 * txtAddress.setText(sup.getAddress()); txtEmailid.setText(sup.getEmailid());
	 * txtGst.setText(sup.getSupGST()); txtPhoneNo.setText(sup.getPhoneNo());
	 * txtCompanyName.setText(sup.getCompany()); btnSave.setDisable(true);
	 * 
	 * ArrayList sup1 = new ArrayList();
	 * 
	 * sup1 = RestCaller.SearchSupplierByName(txtSupplierName.getText()); Iterator
	 * itr = sup1.iterator(); while (itr.hasNext()) { LinkedHashMap lm =
	 * (LinkedHashMap) itr.next(); Object id = lm.get("id"); Object acc_id =
	 * lm.get("account_id"); supplier.setAccount_id((String)acc_id);
	 * supplier.setId((String)id); }
	 * 
	 * } } }
	 */

	@FXML
	void exportToExcel(ActionEvent event) {

		List<ItemMst> itemMstList = new ArrayList<ItemMst>();

		tblItemList.getItems().clear();
		ArrayList itemExport = RestCaller.exportItemMsts();
		ArrayList cat = new ArrayList();

		cat = RestCaller.SearchItemMsts();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object itemName = lm.get("itemName");
			Object standardPrice = lm.get("standardPrice");
			Object taxRate = lm.get("taxRate");
			Object unitId = lm.get("unitName");
			Object categoryId = lm.get("unitName");
			Object isDeleted = lm.get("isDeleted");
			Object itemCode = lm.get("itemCode");
			Object sgst = lm.get("sgst");
			Object id = lm.get("id");
			if (id != null) {
				ItemMst itemMst = new ItemMst();
				itemMst.setItemName((String) itemName);
				itemMst.setStandardPrice((Double) standardPrice);
				itemMst.setTaxRate((Double) taxRate);
				itemMst.setUnitId((String) unitId);
				itemMst.setCategoryId((String) categoryId);
				itemMst.setId((String) id);
				itemMst.setIsDeleted((String) isDeleted);
				itemMst.setSgst((Double) sgst);
				ItemTableList2.add(itemMst);
				itemMstList.add(itemMst);
			}

		}
		if (itemExport.size() > 0) {

			exportItemMstToExcel.exportToExcel("ItemMasterToTally" + SystemSetting.getSystemBranch() + ".xls",
					itemExport);
		}

		ShowAllFillTable();

	}

	@FXML
	private TextField txtPropertyItem;

	@FXML
	private TextField txtPropertyName;

	@FXML
	private TableView<ItemPropertyConfig> tbItemProperty;

	@FXML
	private TableColumn<ItemPropertyConfig, String> clPropertyItemName;

	@FXML
	private TableColumn<ItemPropertyConfig, String> clItemProperty;
	@FXML
	private TableColumn<ItemPropertyConfig, String> clPropertyType;
	@FXML
	private Button btnPropertySave;

	@FXML
	private Button btnPropertyEdit;

	@FXML
	private Button btnPropertyDelete;

	@FXML
	private ComboBox<String> cmbPropertyType;

	@FXML
	void actionPropertyDelete(ActionEvent event) {

		if (null == itemPropertyConfig) {
			return;
		}
		if (null == itemPropertyConfig.getId()) {
			return;
		}
		try {
			RestCaller.deleteItemPropertyConfig(itemPropertyConfig.getId());
			notifyMessage(3, "Deleted");
		} catch (Exception e) {

			notifyMessage(3, e.toString());
		}
		itemPropertyConfig = null;
		tbItemProperty.getItems().clear();
		ItemPropertyList.clear();
		ResponseEntity<List<ItemPropertyConfig>> itemproConfig = RestCaller.getAllItemPropertyConfig();
		ItemPropertyList = FXCollections.observableArrayList(itemproConfig.getBody());
		fillPropertyTable();
	}

	@FXML
	void actionPropertyShowAll(ActionEvent event) {

		ResponseEntity<List<ItemPropertyConfig>> itemproConfig = RestCaller.getAllItemPropertyConfig();
		ItemPropertyList = FXCollections.observableArrayList(itemproConfig.getBody());
		fillPropertyTable();
	}

	@FXML
	void actionPropertySave(ActionEvent event) {
		if (txtPropertyName.getText().contains("%") || (txtPropertyName.getText().contains("/"))
				|| (txtPropertyName.getText().contains("?")) || txtPropertyName.getText().contains("&")
				|| txtPropertyName.getText().contains("*") || txtPropertyName.getText().contains("$")) {
			notifyMessage(5, "ItemName Invalid");
			return;
		}
		String PropertyName = txtPropertyName.getText().trim();

		PropertyName = PropertyName.replaceAll("'", "");
		PropertyName = PropertyName.replaceAll("&", "");

		PropertyName = PropertyName.replaceAll("/", "");
		PropertyName = PropertyName.replaceAll("%", "");
		PropertyName = PropertyName.replaceAll("\"", "");
		itemPropertyConfig = new ItemPropertyConfig();
		itemPropertyConfig.setItemNameProperty(txtPropertyItem.getText());
		ResponseEntity<ItemMst> itemmst = RestCaller.getItemByNameRequestParam(txtPropertyItem.getText());
		itemPropertyConfig.setItemId(itemmst.getBody().getId());
		itemPropertyConfig.setPropertyName(PropertyName);
		itemPropertyConfig.setBranchCode(SystemSetting.getUser().getBranchCode());
		itemPropertyConfig.setPropertyType(cmbPropertyType.getSelectionModel().getSelectedItem());
		ResponseEntity<ItemPropertyConfig> itempro = RestCaller
				.getItemPropertyConfigByItemNameAndProperty(itemmst.getBody().getId(), PropertyName);
		if (null != itempro.getBody()) {
			notifyMessage(3, "Already Saved");
			return;
		} else {
			ResponseEntity<ItemPropertyConfig> respenity = RestCaller.saveItemPropertyConfig(itemPropertyConfig);
			itemPropertyConfig = respenity.getBody();
			ItemPropertyList.add(itemPropertyConfig);
		}
		fillPropertyTable();
		txtPropertyName.clear();
		txtPropertyName.requestFocus();

	}

	private void fillPropertyTable() {
		for (ItemPropertyConfig itemproconfig : ItemPropertyList) {
			ResponseEntity<ItemMst> items = RestCaller.getitemMst(itemproconfig.getItemId());
			itemproconfig.setItemName(items.getBody().getItemName());
		}
		tbItemProperty.setItems(ItemPropertyList);
		clPropertyItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clPropertyType.setCellValueFactory(cellData -> cellData.getValue().getPropertyTypeProperty());
		clItemProperty.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
	}

	@FXML
	void onItemNameEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadItemPOpUp();
		}
	}

	@Subscribe
	public void itemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		if (stage.isShowing()) {

			txtPropertyItem.setText(itemPopupEvent.getItemName());
		}
	}

	private void loadItemPOpUp() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtbarCode.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

	@FXML
	void focusOnHsnCode(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txthsnCode.requestFocus();
		}
	}

}
