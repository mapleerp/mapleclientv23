package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AccountSlNoUpdationCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<AccountHeads> stockTransferInDtlList = FXCollections.observableArrayList();

	
	EventBus eventBus = EventBusFactory.getEventBus();
	


    @FXML
    private TextField txtAccount;

    @FXML
    private TextField txtSlNo;

    @FXML
    private Button btnShowAll;

    @FXML
    private ComboBox<String> cmbAccountType;

    @FXML
    private Button btnCleare;

    @FXML
    private Button btnSave;

    @FXML
    private TableView<AccountHeads> tblReport;

    @FXML
    private TableColumn<AccountHeads, String> clAccount;

//    @FXML
//    private TableColumn<?, ?> clAccountType;

    @FXML
    private TableColumn<AccountHeads, Number> clSlNo;
    
    @FXML
   	private void initialize() {
    	
    	eventBus.register(this);

    	
    	cmbAccountType.getItems().add("INCOME");
    	cmbAccountType.getItems().add("EXPENSE");

    }

    @FXML
    void Cleare(ActionEvent event) {
    	

    	txtAccount.clear();
    	txtSlNo.clear();
    	tblReport.getItems().clear();
    	cmbAccountType.getSelectionModel().clearSelection();

    }

    @FXML
    void Save(ActionEvent event) {
    	
    	if(txtAccount.getText().trim().isEmpty())
    	{
    		notifyMessage(3, "Please select Account", false);
    		txtAccount.requestFocus();
    		return;
    	}
    	if(txtSlNo.getText().trim().isEmpty())
    	{
    		notifyMessage(3, "Please enter Serial Number", false);
    		txtSlNo.requestFocus();
    		return;
    	}
    	
    	ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadByName(txtAccount.getText());
    	AccountHeads accountHeads = accountHeadsResp.getBody();
    	accountHeads.setSerialNumber(Integer.parseInt(txtSlNo.getText()));
    	RestCaller.updateAccountHeads(accountHeads);
    	
    	ResponseEntity<List<AccountHeads>> accountHeadsListResp = RestCaller.getAccountHeadsSerialNumber();
    	stockTransferInDtlList = FXCollections.observableArrayList(accountHeadsListResp.getBody());
    	
    	fillTable();
    	
    	txtAccount.clear();
    	txtSlNo.clear();
    	cmbAccountType.getSelectionModel().clearSelection();

    }

    @FXML
    void ShowAll(ActionEvent event) {
    	
    	ResponseEntity<List<AccountHeads>> accountHeadsResp = RestCaller.getAccountHeadsSerialNumber();
    	stockTransferInDtlList = FXCollections.observableArrayList(accountHeadsResp.getBody());
    	
    	fillTable();

    }

    private void fillTable() {

    	tblReport.setItems(stockTransferInDtlList);
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		clSlNo.setCellValueFactory(cellData -> cellData.getValue().getSerialNumberProperty());

	}

	@FXML
    void getAccountPopup(MouseEvent event) {
    	
    	showPopup();

    }
    
	private void showPopup() {
		
		if(null == cmbAccountType.getSelectionModel().getSelectedItem())
		{
			notifyMessage(3, "Please select account type", false);
			cmbAccountType.requestFocus();
			return;
		}
		ResponseEntity<AccountHeads> parentHeadsResp = RestCaller.
				getAccountHeadByName(cmbAccountType.getSelectionModel().getSelectedItem().toString());
		
		AccountHeads parrentAccountHeads = parentHeadsResp.getBody();

		
		if(cmbAccountType.getSelectionModel().getSelectedItem().equalsIgnoreCase("EXPENSE"))
		{
		
		try {
			
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopupByParentAccount.fxml"));
			Parent root = loader.load();
			
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtSlNo.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}else {
			
			try {
				
				
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountHeadsByIncomeHeads.fxml"));
				Parent root = loader.load();
				
				// PopupCtl popupctl = loader.getController();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				txtSlNo.requestFocus();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}

	}
	
	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		Stage stage = (Stage) txtAccount.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountEvent.getAccountName());

		}

	}
	
	
	 public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload();
			}


	    private void PageReload() {
	    	
	    	
			
		}


}
