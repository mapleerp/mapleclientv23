package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemBatchDtl;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PhysicalStockDtl;
import com.maple.mapleclient.entity.PhysicalStockHdr;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.WeighingItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class PhysicalStockCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<PhysicalStockDtl> physicalStockDtlListTable = FXCollections.observableArrayList();
	private ObservableList<PhysicalStockHdr> physicalStockHdrListTable = FXCollections.observableArrayList();
	ItemMst itemMst = new ItemMst();
	PhysicalStockDtl physicalStockDtl = null;
	PhysicalStockHdr physicalStockHdr = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	String itemId = "";
	String barcode = "";
	boolean multi = false;
	@FXML
	private ComboBox<String> cmbType;
	@FXML
	private TextField txtcurrentStock;
	@FXML
	private DatePicker dpManfDate;

	@FXML
	private DatePicker dpExpiryDate;

	@FXML
	private ComboBox<String> cmbHeaderId;

	@FXML
	private DatePicker dpDate;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtBatchCode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtRate;

	@FXML
	private Button btnAddItem;

	@FXML
	private Button btnDeleteRecord;

	@FXML
	private Button btnFetchData;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TableView<PhysicalStockDtl> tblPhysicalStock;

	@FXML
	private TableColumn<PhysicalStockDtl, String> clItemName;

	@FXML
	private TableColumn<PhysicalStockDtl, String> clBarcode;

	@FXML
	private TableColumn<PhysicalStockDtl, Number> clQty;

	@FXML
	private TableColumn<PhysicalStockDtl, Number> clRate;

	@FXML
	private TableColumn<PhysicalStockDtl, String> clBatchCode;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnClear;
	
    @FXML
    private ComboBox<String> cmbStore;

	@FXML
	void SetPhysicalStockHdr(MouseEvent event) {
		getHeaderId();
	}
	@FXML
	void OnActionClear(ActionEvent event) {

		txtBarcode.setText("");
		txtBatchCode.setText("");
		txtItemName.setText("");
		txtQty.setText("");
		txtRate.setText("");
		tblPhysicalStock.getItems().clear();
		// dpDate.setValue(null);
		cmbHeaderId.getItems().clear();
		getHeaderId();
		cmbType.getSelectionModel().clearSelection();

	}

	@FXML
	void dpDateKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtItemName.requestFocus();
		}

	}

	@FXML
	void FocusQty(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtQty.requestFocus();
		}

	}

	@FXML
	void FocusOnBatchCode(KeyEvent event) {

		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtItemName.requestFocus();

			showPopup();
			// ShowItemPopup();
		}
		if (event.getCode() == KeyCode.ENTER) {
			if (txtBarcode.getText().trim().isEmpty()) {
			} else {

				if (null == txtBarcode) {
					return;

				}
				if (null == txtBarcode.getText()) {
					return;

				}
				String subBarcode = null;
				if (txtBarcode.getText().trim().length() >= 12) {
					String barcode1 = txtBarcode.getText();
					subBarcode = barcode1.substring(0, 7);
				}
				ArrayList items = new ArrayList();

				items = RestCaller.SearchStockItemByBarcode(txtBarcode.getText());
				if (!items.isEmpty()) {
					if (txtBarcode.getText().startsWith("21")) {
						if (null == subBarcode) {
							subBarcode = txtBarcode.getText();
						}
						ResponseEntity<WeighingItemMst> weighingItems = RestCaller
								.getWeighingItemMstByBarcode(subBarcode);
						if (null != weighingItems.getBody()) {
							items = RestCaller.SearchStockItemByBarcode(subBarcode);
							if (txtBarcode.getText().trim().length() > 7) {
								txtQty.setText(txtBarcode.getText().substring(7, 9) + "."
										+ txtBarcode.getText().substring(9, 12));
							}
							txtBarcode.setText(subBarcode);
						}
					}
					if (items.size() > 1) {
						multi = true;
						showPopupByBarcode();
					} else {
						multi = false;
						String itemName = "";
						String barcode = "";
						Double mrp = 0.0;
						String unitname = "";
//					Double qty = 0.0;
//					Double cess = 0.0;
						Double tax = 0.0;
						String itemId = "";
						String unitId = "";
						Iterator itr = items.iterator();
						String batch = "";

						while (itr.hasNext()) {

							List element = (List) itr.next();
							itemName = (String) element.get(0);
							barcode = (String) element.get(1);
							unitname = (String) element.get(2);
							mrp = (Double) element.get(3);
//						qty = (Double) element.get(4);
//						cess = (Double) element.get(5);
							tax = (Double) element.get(6);
							itemId = (String) element.get(7);
							unitId = (String) element.get(8);
							batch = (String) element.get(9);
							if (null != itemName) {
								txtBarcode.setText(barcode);
								txtBatchCode.setText(batch);
								txtItemName.setText(itemName);
								txtRate.setText(Double.toString(mrp));
								ResponseEntity<List<ItemBatchExpiryDtl>> getItemBatch = RestCaller
										.getItemBatchExpByItemAndBatch(itemId, batch);
								if (!getItemBatch.getBody().isEmpty()) {
									dpExpiryDate.setValue(SystemSetting
											.utilToLocaDate(getItemBatch.getBody().get(0).getExpiryDate()));
								}
								ArrayList itemsArray = new ArrayList();
								itemsArray = RestCaller.SearchStockItemByName(txtItemName.getText());
								Double qty = 0.0;
								txtcurrentStock.setText("0");
								Iterator itr1 = itemsArray.iterator();
								while (itr1.hasNext()) {
									List element1 = (List) itr1.next();
									qty = (Double) element1.get(4);
									if (qty > 0.0)
										txtcurrentStock.setText(qty.toString());
									else
										txtcurrentStock.setText("0");
								}
							}
						}
					}
					txtQty.requestFocus();
				}

				else {
					ResponseEntity<List<ItemMst>> getItems = RestCaller.getItemByBarcode(txtBarcode.getText());
					txtItemName.setText(getItems.getBody().get(0).getItemName());
					txtcurrentStock.setText("0.0");
					txtBatchCode.requestFocus();

				}
			}

		}
	}

	@FXML
	void FocusRate(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtRate.requestFocus();
		}

	}

	@FXML
	void FocusOnManufacturingDate(KeyEvent event) {

		if (event.getCode() == KeyCode.RIGHT) {
			dpManfDate.requestFocus();
		}
		if (event.getCode() == KeyCode.ENTER) {
			AddItemFunction();
		}

	}

	@FXML
	void FocusOnExpiryDate(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			dpExpiryDate.requestFocus();
		}
	}

	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		dpExpiryDate = SystemSetting.datePickerFormat(dpExpiryDate, "dd/MMM/yyyy");
		dpManfDate = SystemSetting.datePickerFormat(dpManfDate, "dd/MMM/yyyy");
		dpDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		dpDate.setEditable(false);
		eventBus.register(this);
		cmbType.getItems().add("NEW STOCK");
		cmbType.getItems().add("STOCK CORRECTION");

		cmbType.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.equalsIgnoreCase(null)) {
					if (newValue.equalsIgnoreCase("NEW STOCK")) {
						txtBatchCode.setEditable(true);
						dpExpiryDate.setDisable(false);
					} else {
						txtBatchCode.setEditable(false);
						dpExpiryDate.setDisable(true);
					}
				}
			}
		});

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		tblPhysicalStock.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getId()) {

					physicalStockDtl = new PhysicalStockDtl();

					physicalStockDtl.setId(newSelection.getId());
					System.out.println("-------physicalStockDtl------" + physicalStockDtl);

				}
			}
		});
	

		getHeaderId();
		
		
		ResponseEntity<List<StoreMst>> storeMstListResp = RestCaller.getAllStoreMst();
		List<StoreMst> storeMstList = storeMstListResp.getBody();
		
		cmbStore.getSelectionModel().select("MAIN");

		for(StoreMst storeMst : storeMstList)
		{
			cmbStore.getItems().add(storeMst.getShortCode());
		}

	}


	@FXML
	void ShowPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (txtItemName.getText().length() > 0) {
				txtBarcode.requestFocus();
			} else {

				if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("NEW STOCK")) {
					showPopup();
					// ShowItemPopup();
				} else if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("STOCK CORRECTION"))

				{
					if (SystemSetting.isNEGATIVEBILLING()) {
						if (SystemSetting.isNEGATIVEBILLING()) {
							showPopup();
						} else {
							ShowItemPopup();
						}
					}

				}
			}

		}

	}


	@FXML
	void AddPhysicalStock(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (null != dpExpiryDate.getValue()) {
				AddItemFunction();
			}
		}

	}

	private void AddItemFunction() {

		if (validationHdr() == true) {
			if (null == physicalStockHdr) {
				physicalStockHdr = new PhysicalStockHdr();
				Date date = Date.valueOf(dpDate.getValue());
				System.out.println("-----------date--------" + date);
				physicalStockHdr.setVoucherDate(date);
				physicalStockHdr.setBranchCode(SystemSetting.systemBranch);
				ResponseEntity<PhysicalStockHdr> respentity = RestCaller.savePhysicalStockHdr(physicalStockHdr);
				physicalStockHdr = respentity.getBody();
				System.out.println("---@@--------physicalStockHdr------@@---" + physicalStockHdr);
			}
			if (null != physicalStockHdr.getId()) {

				if (validationDtl() == true) {
					
					///--------------------sharon--------6/4/21---------
					
					if(null==txtBatchCode.getText())
					{
						notifyMessage(3, "Please enter batch");
						txtBatchCode.setText("NOBATCH");
						/* return; */
					} /*
						 * else if(txtBatchCode.getText().equalsIgnoreCase("NOBATCH")){ notifyMessage(3,
						 * "NOBATH Not allowed"); txtBatchCode.requestFocus();
						 * 
						 * return; }
						 */
					
//					if(null == dpExpiryDate.getValue())
//					{
//						notifyMessage(3, "Please select expiry date");
//						dpExpiryDate.requestFocus();
//
//						return;
//					}
					///--------sharon------End--6/4/21--------------//

					System.out.println("-@@--itemId----" + itemId + "---barcode---" + barcode);
					physicalStockDtl = new PhysicalStockDtl();
					if (null != txtBatchCode.getText()) {
						String batch = txtBatchCode.getText() == null ? "NOBATCH" : txtBatchCode.getText();

						if (batch.trim().length() > 0) {
							physicalStockDtl.setBatch(batch);
						} else

							physicalStockDtl.setBatch("NOBATCH");
					}
//					if (physicalStockDtl.getBatch().equalsIgnoreCase("NOBATCH")) {
//						if (null != dpExpiryDate.getValue()) {
//							dpExpiryDate.setValue(null);
//							notifyMessage(3, "Cannot Set Expiry Date in NoBatch");
//							return;
//
//						}
//					}

					
					// Date expiryDate = Date.valueOf(dpExpiryDate.getValue());
//					//----------------------version 6.3 surya
//					if(physicalStockDtl.getBatch() != "NOBATCH")
//					{
//						notifyMessage(3, "Please select ExpiryDate");
//						return;
//
//						
//					}
//					//----------------------version 6.3 surya end

					if (null == dpExpiryDate.getValue()) {
						
						LocalDate toDayDate = LocalDate.now();
						toDayDate = toDayDate.plusYears(1);
						
						java.util.Date date = SystemSetting.localToUtilDate(toDayDate);
						
						physicalStockDtl.setExpiryDate(date);
					} else {

						String strDate = dpExpiryDate.getValue().toString();

						java.util.Date utlDate = SystemSetting.StringToUtilDate(strDate, "yyyy-MM-dd");
						String sqDate = SystemSetting.UtilDateToString(utlDate, "yyyy-MM-dd");
						Date sdate = SystemSetting.StringToSqlDateSlash(sqDate, "yyyy-MM-dd");
						physicalStockDtl.setExpiryDate(sdate);
					}
					if (null != dpManfDate.getValue()) {
						Date manDate = Date.valueOf(dpManfDate.getValue());
						physicalStockDtl.setManufactureDate(manDate);
					}
					ResponseEntity<ItemMst> items = RestCaller.getItemByNameRequestParam(txtItemName.getText());
					physicalStockDtl.setItemId(items.getBody().getId());
					physicalStockDtl.setItemName(txtItemName.getText());
					physicalStockDtl.setBarcode(txtBarcode.getText());

					physicalStockDtl.setMrp(Double.parseDouble(txtRate.getText()));
					physicalStockDtl.setPhysicalStockHdrId(physicalStockHdr);
					physicalStockDtl.setQtyIn(Double.parseDouble(txtQty.getText()));
					if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("NEW STOCK")) {
						
						ResponseEntity<List<ItemBatchDtl>> ItemBatchDtlList = RestCaller
								.getitemBatchDtlByItemId(items.getBody().getId(), physicalStockDtl.getBatch());
						  
						if (null != ItemBatchDtlList) {
							if (ItemBatchDtlList.getBody().size() > 0) {
								notifyMessage(3, "Item Already Exist In Stock");
								clearDtl();
								return;
							}
							///----------------------------6/4/21---------
							txtBarcode.setEditable(true);
							txtBatchCode.setEditable(true);
							///--------------------end--------6/4/21---------
						}
					}
					if(null != txtcurrentStock.getText()) {
					physicalStockDtl.setSystemQty(Double.parseDouble(txtcurrentStock.getText()));
					}
					ResponseEntity<PhysicalStockDtl> respentity = RestCaller.savePhysicalStockDtl(physicalStockDtl);
					physicalStockDtl = respentity.getBody();
					System.out.println("---@@--------physicalStockDtl------@@---" + physicalStockDtl);
					physicalStockDtlListTable.add(physicalStockDtl);
					FillTable();
					clearDtl();
					cmbHeaderId.getItems().clear();
					getHeaderId();
					physicalStockDtl = null;
					itemId = "";
					barcode = "";

				}
			}
		}

		txtItemName.requestFocus();

	}

	@FXML
	void AddItem(ActionEvent event) {

		AddItemFunction();

	}

	@FXML
	void FetchData(ActionEvent event) {

		// cmbHeaderId.getItems().clear();
		// getHeaderId();

		if (null != cmbHeaderId.getSelectionModel().getSelectedItem()) {

			physicalStockHdr = new PhysicalStockHdr();
			physicalStockDtl = new PhysicalStockDtl();
			physicalStockHdr.setId(cmbHeaderId.getSelectionModel().getSelectedItem());
			if (null != physicalStockHdr.getId()) {
				if (null != physicalStockHdrListTable) {
					for (PhysicalStockHdr ph : physicalStockHdrListTable) {
						if (ph.getId().equals(physicalStockHdr.getId())) {
//							Date date = ph.getVoucherDate();
//							dpDate.setValue(date.toLocalDate());
						}
					}
				}

			}

			System.out.println("----physicalStockHdr----" + physicalStockHdr);

			ResponseEntity<List<PhysicalStockDtl>> physicalStockDtlSaved = RestCaller
					.getPhysicalStockDtl(physicalStockHdr);
			ResponseEntity<PhysicalStockHdr> respentity = RestCaller.getPhysicalHdr(physicalStockHdr);
			physicalStockHdr = respentity.getBody();
			dpDate.setValue(physicalStockHdr.getVoucherDate().toLocalDate());
			physicalStockDtlListTable = FXCollections.observableArrayList(physicalStockDtlSaved.getBody());

			FillTable();

		}

	}

	@FXML
	void FinalSave(ActionEvent event) {

		finalSave();
	}

	private void finalSave() {
		if (null != physicalStockHdr) {
			if (null != physicalStockHdr.getId()) {

				ResponseEntity<List<PhysicalStockDtl>> physicalStockDtlSaved = RestCaller
						.getPhysicalStockDtl(physicalStockHdr);
				if (physicalStockDtlSaved.getBody().size() == 0) {
					return;
				}
				physicalStockDtl = new PhysicalStockDtl();
				List<PhysicalStockDtl> pdysicalStockDtlList = physicalStockDtlSaved.getBody();

				String financialYear = SystemSetting.getFinancialYear();
				String psNo = RestCaller.getVoucherNumber(financialYear + "PSVN");

				physicalStockHdr.setVoucherNumber(psNo);

				java.util.Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				java.sql.Date sqlDate = Date.valueOf(sdate);
				physicalStockHdr.setVoucherDate(sqlDate);
				RestCaller.updatePhysicalStockHdr(physicalStockHdr);

				// Do not update price on physical stock update. This will be a loophole in the
				// system
				/*
				 * for (PhysicalStockDtl physicalDtl:physicalStockDtlSaved.getBody()) {
				 * ResponseEntity<ItemMst> getitem =
				 * RestCaller.getItemByNameRequestParam(physicalDtl.getItemName()); ItemMst
				 * itemMst = new ItemMst(); itemMst = getitem.getBody();
				 * itemMst.setStandardPrice(physicalDtl.getMrp()); itemMst.setRank(0);
				 * RestCaller.updateItemMst(itemMst); }
				 */

				/*
				 * for(PhysicalStockDtl ph : pdysicalStockDtlList) { String expDate = null; if
				 * (null != ph.getExpiryDate()) { Format formatter;
				 * 
				 * formatter = new SimpleDateFormat("dd-MM-yyyy"); expDate =
				 * formatter.format(ph.getExpiryDate()); //expDate
				 * =SystemSetting.UtilDateToString(ph.getExpiryDate()); }
				 * 
				 * ResponseEntity<ArrayList> respentity =
				 * RestCaller.saveStockFromPhysicalStock(ph.getItemName(),ph.getBarcode(),
				 * ph.getBatch(),Double.toString(ph.getQtyIn()),Double.toString(ph.getMrp()),
				 * expDate);
				 * 
				 * }
				 */

				pdysicalStockDtlList = null;
				ItemBatchMst item = new ItemBatchMst();
				item.setBarcode(psNo);
				physicalStockHdr = null;
				tblPhysicalStock.getItems().clear();
				java.util.Date toDate = SystemSetting.systemDate;
				String vFDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
				String msg = RestCaller.updateitemBatchMstWithDtl();

				// dpDate.setValue(null);
				// cmbHeaderId.getItems().clear();
				notifyMessage(5, "Successfully saved...!!!");
				cmbHeaderId.getItems().clear();
				getHeaderId();
				cmbType.getSelectionModel().clearSelection();

//				} catch (Exception e) {
//					e.printStackTrace();
//				}

			}
		}
	}

	@FXML
	void saveOnKey(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}
	}

	@FXML
	void deleteRecord(ActionEvent event) {

		if (null != physicalStockDtl) {
			System.out.println("-----physicalStockDtl-------" + physicalStockDtl);

			if (null != physicalStockDtl.getId()) {
				RestCaller.deletePhysicalStockDtl(physicalStockDtl.getId());
				tblPhysicalStock.getItems().clear();
				ResponseEntity<List<PhysicalStockDtl>> physicalStockDtlSaved = RestCaller
						.getPhysicalStockDtl(physicalStockHdr);
				physicalStockDtlListTable = FXCollections.observableArrayList(physicalStockDtlSaved.getBody());
				tblPhysicalStock.setItems(physicalStockDtlListTable);
				notifyMessage(5, "Deleted Successfully..!!!");

			}

		}
		cmbHeaderId.getItems().clear();
		getHeaderId();

	}

	private void showPopup() {
		try {
			/*
			 * if (SystemSetting.isNEGATIVEBILLING()) { FXMLLoader fxmlLoader = new
			 * FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml")); Parent root1;
			 * String cst = null; root1 = (Parent) fxmlLoader.load(); Stage stage = new
			 * Stage(); stage.initModality(Modality.APPLICATION_MODAL);
			 * stage.initStyle(StageStyle.UNDECORATED); stage.setTitle("Stock Item");
			 * stage.initModality(Modality.APPLICATION_MODAL); stage.setScene(new
			 * Scene(root1)); stage.show();
			 * 
			 * txtQty.requestFocus(); } else {
			 */

			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));

			Parent root1;
			ItemStockPopupCtl itemStockPopupCtl = fxmlLoader.getController();
			itemStockPopupCtl.windowName = "POS";
			root1 = (Parent) fxmlLoader.load();
			String cst = null;
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtQty.requestFocus();
			// }

			txtBarcode.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * private void ShowStockItemPopup() {
	 * 
	 * try { System.out.println("inside the popup"); FXMLLoader loader = new
	 * FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml")); Parent root
	 * = loader.load(); Stage stage = new Stage(); stage.setScene(new Scene(root));
	 * stage.show(); txtQty.requestFocus();
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * }
	 */
	@FXML
	void ShowItemPopup(MouseEvent event) {

		if (cmbType.getSelectionModel().isEmpty()) {
			notifyMessage(3, "Select Type");
			return;
		}

		if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("NEW STOCK")) {

			ShowItemPopup();
		} else if (cmbType.getSelectionModel().getSelectedItem().equalsIgnoreCase("STOCK CORRECTION"))

		{
			if (SystemSetting.isNEGATIVEBILLING()) {
				ShowItemPopup();

			} else {
				showPopup();

			}

		}
	}

	private void ShowItemPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("------@@@-------popupItemlistner-------@@@@------" + itemPopupEvent.getItemName());
		Stage stage = (Stage) btnDeleteRecord.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					if (!multi) {
						txtBarcode.setText(itemPopupEvent.getBarCode());
						txtBatchCode.setText(itemPopupEvent.getBatch());
						txtItemName.setText(itemPopupEvent.getItemName());
						txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
						if (null != itemPopupEvent.getExpiryDate())
							// version4.3
							dpExpiryDate.setValue(itemPopupEvent.getExpiryDate().toLocalDate());
						itemId = itemPopupEvent.getItemId();
						barcode = itemPopupEvent.getBarCode();
						ArrayList items = new ArrayList();
						items = RestCaller.SearchStockItemByName(itemPopupEvent.getItemName());
						Double qty = 0.0;
						txtcurrentStock.setText("0");
						Iterator itr = items.iterator();
						while (itr.hasNext()) {
							List element = (List) itr.next();
							qty = (Double) element.get(4);
							if (qty > 0.0)
								txtcurrentStock.setText(qty.toString());
							else
								txtcurrentStock.setText("0");
						}
						System.out.println("---------itemid -----------" + itemId + "-----barcode------" + barcode);

					} else if (multi) {
						txtBarcode.setText(itemPopupEvent.getBarCode());
						txtBatchCode.setText(itemPopupEvent.getBatch());
						txtItemName.setText(itemPopupEvent.getItemName());
						txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
						itemId = itemPopupEvent.getItemId();
						barcode = itemPopupEvent.getBarCode();
						ArrayList items = new ArrayList();
						items = RestCaller.SearchStockItemByName(txtItemName.getText());
						Double qty = 0.0;
						txtcurrentStock.setText("0");
						Iterator itr = items.iterator();
						while (itr.hasNext()) {
							List element = (List) itr.next();
							qty = (Double) element.get(4);
							if (qty > 0.0)
								txtcurrentStock.setText(qty.toString());
							else
								txtcurrentStock.setText("0");
						}

						ResponseEntity<List<ItemBatchExpiryDtl>> getItemBatch = RestCaller
								.getItemBatchExpByItemAndBatch(itemId, txtBatchCode.getText());
						if (!getItemBatch.getBody().isEmpty()) {
							dpExpiryDate.setValue(
									SystemSetting.utilToLocaDate(getItemBatch.getBody().get(0).getExpiryDate()));
						}
					}
				}
			});
		}
	}

	private boolean validationHdr() {
		boolean flag = false;

		if (null == dpDate.getValue()) {
			notifyMessage(5, "Please select date...!!!");
		} else {
			flag = true;
		}
		return flag;
	}

	private boolean validationDtl() {
		boolean flag = false;

		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter quantity...!!!");

		} else if (txtItemName.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter item..!!!");
		} else if (txtBarcode.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter barcode...!!!");
		} else if (txtRate.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter rate...!!!");
//		} else if (null == dpExpiryDate.getValue()) {
//
//			notifyMessage(5, "Please Select Expiry Date...!!!");
		} else {
			flag = true;
		}

		return flag;
	}

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void FillTable() {

		tblPhysicalStock.setItems(physicalStockDtlListTable);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		clBatchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());

	}

	private void clearDtl() {
		dpExpiryDate.setValue(null);
		dpManfDate.setValue(null);
		txtBarcode.setText("");
		txtBatchCode.setText("");
		txtItemName.setText("");
		txtQty.setText("");
		txtRate.setText("");
		txtcurrentStock.clear();

	}

	private void getHeaderId() {

		ArrayList head = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		head = RestCaller.SearchPhysicalStockHeader();
		Iterator itr = head.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			System.out.println("------lm-------" + lm);
			Object headerId = lm.get("id");
			Object voucherDate = lm.get("voucherDate");
			if (headerId != null) {

				cmbHeaderId.getItems().add((String) headerId);
				PhysicalStockHdr phHdr = new PhysicalStockHdr();
				phHdr.setId((String) headerId);
//				phHdr.setVoucherDate((Date)voucherDate);

//				physicalStockHdrListTable.add(phHdr);
			}

		}

	}

	private void showPopupByBarcode() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			ItemStockPopupCtl popctl = fxmlLoader.getController();
			popctl.LoadItemPopupByBarcode(txtBarcode.getText());

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
