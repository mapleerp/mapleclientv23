package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.IntentInDtl;
import com.maple.mapleclient.entity.IntentInHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.IntentInVoucherNumberEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AcceptIntentCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<IntentInDtl> intentinDtlList = FXCollections.observableArrayList();

	IntentInHdr intentInHdr = null;

	IntentInDtl intentInDtl = null;

	@FXML
	private TextField txtIntentNumber;

	@FXML
	private Button btnShow;

	@FXML
	private Button btnAccept;

	@FXML
	private TextField txtFromBranch;

	@FXML
	private DatePicker dpInVoucherDate;

	@FXML
	private Button btnClear;

	@FXML
	private TableView<IntentInDtl> tbItems;

	@FXML
	private TableColumn<IntentInDtl, String> clItemName;

	@FXML
	private TableColumn<IntentInDtl, String> clUnit;

	@FXML
	private TableColumn<IntentInDtl, Number> clQty;

	@FXML
	private TableColumn<IntentInDtl, Number> clRate;

	// ---------------new version 1.4 surya
	@FXML
	private TableColumn<?, ?> clBalanceQty;

	@FXML
	private ComboBox<String> cmbType;

	@FXML
	private TableView<?> tblAllocatedItem;

	@FXML
	private TableColumn<?, ?> clAllocatedItemName;

	@FXML
	private TableColumn<?, ?> clAllocatedUnit;

	@FXML
	private TableColumn<?, ?> clAllocatedQty;

	@FXML
	private TableColumn<?, ?> clAllocatedRate;

	@FXML
	private TableColumn<?, ?> clAllocatedType;

	@FXML
	private TextField txtQty;

	@FXML
	private Button btnAllocateItem;

	@FXML
	void AllocateItem(ActionEvent event) {

		if (null == cmbType.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Please select Allocation type", false);
			return;
		}

		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(3, "Please select Allocation type", false);
			return;
		}

		String allocationType = cmbType.getSelectionModel().getSelectedItem().toString();
		if (allocationType.equalsIgnoreCase("SALE"))
			;
		{
			Sale(intentInDtl);
		}

	}

	private void Sale(IntentInDtl intentInDtl2) {

		SalesTransHdr salesTransHdr = new SalesTransHdr();
		
		BranchMst branchMst = RestCaller.getBranchDtls(intentInDtl2.getBranchCode());
		
		/*
		 * new url for getting account heads instead of customer mst=========05/0/2022
		 */
//		ResponseEntity<CustomerMst> customerResp = RestCaller.getCustomerById(branchMst.getId());
		ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadsById(branchMst.getId());
		AccountHeads accountHeads = accountHeadsResp.getBody();
		
		
		salesTransHdr.setBranchCode(SystemSetting.getUser().getBranchCode());
		salesTransHdr.setCustomerId(accountHeads.getId());
		salesTransHdr.setAccountHeads(accountHeads);
		salesTransHdr.setIsBranchSales("Y");
		salesTransHdr.setUserId(SystemSetting.getUser().getId());

		ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
		salesTransHdr = respentity.getBody();


	}

	@FXML
	private Button btnFinalSave;

	@FXML
	void FinalSave(ActionEvent event) {

	}

	// ---------------new version 1.4 surya end

	@FXML
	private void initialize() {
		eventBus.register(this);

		// ---------------new version 1.4 surya
		dpInVoucherDate = SystemSetting.datePickerFormat(dpInVoucherDate, "dd/MMM/yyyy");
		cmbType.getItems().add("SALE");
		cmbType.getItems().add("SALEORDER");
		cmbType.getItems().add("STOCKTRANSFER");

		tbItems.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					ResponseEntity<IntentInDtl> intentInDtlResp = RestCaller.getIntentInDtlById(newSelection.getId());
					intentInDtl = intentInDtlResp.getBody();

					txtQty.setText(intentInDtl.getBalanceQty().toString());
				}
			}
		});

		// ---------------new version 1.4 surya end

	}

	@FXML
	void loadPopUp(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadVoucherPopUp();
		}
	}

	@Subscribe
	public void popuplistner(IntentInVoucherNumberEvent voucherNoEvent) {

		System.out.println("------Voucher-------popuplistner-------------" + voucherNoEvent);
		Stage stage = (Stage) btnShow.getScene().getWindow();
		if (stage.isShowing()) {
			intentInHdr = new IntentInHdr();
			intentInHdr.setInVoucherNumber(voucherNoEvent.getIntentInVoucherNumber());
			intentInHdr.setId(voucherNoEvent.getId());
			txtIntentNumber.setText(voucherNoEvent.getIntentInVoucherNumber());
			txtFromBranch.setText(voucherNoEvent.getFromBranch());
			dpInVoucherDate.setValue(SystemSetting.utilToLocaDate(voucherNoEvent.getInVoucherDate()));
		}
	}

	private void loadVoucherPopUp() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/IntentInVoucherNumberPopUp.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void actionAcceptIntent(ActionEvent event) {

		RestCaller.updateIntentInhdr(intentInHdr);
		clearData();
	}

	private void clearData() {

		txtFromBranch.clear();
		txtIntentNumber.clear();
		tbItems.getItems().clear();
		intentinDtlList.clear();
		dpInVoucherDate.setValue(null);
		txtIntentNumber.requestFocus();

	}

	@FXML
	void actionClear(ActionEvent event) {
		clearData();
	}

	@FXML
	void actionShow(ActionEvent event) {

		if (null == intentInHdr) {
			notifyMessage(3, "Please select Intent ", false);
			return;
		}

		if (null == intentInHdr.getId()) {
			notifyMessage(3, "Please select Intent ", false);
			return;
		}

		tbItems.getItems().clear();
		intentinDtlList.clear();
		ResponseEntity<List<IntentInDtl>> intentInDtlList = RestCaller.getintentdtlsbyId(intentInHdr.getId());
		intentinDtlList = FXCollections.observableArrayList(intentInDtlList.getBody());
	
		fillTable();
	}

	private void fillTable() {
		for (IntentInDtl intentDtl : intentinDtlList) {
			ResponseEntity<ItemMst> itemmst = RestCaller.getitemMst(intentDtl.getItemId());
			if (null != itemmst.getBody()) {
				intentDtl.setItemName(itemmst.getBody().getItemName());
			}
			ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(intentDtl.getUnitId());
			if (null != unitMst.getBody()) {
				intentDtl.setUnitName(unitMst.getBody().getUnitName());
			}
		}

		tbItems.setItems(intentinDtlList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getrateProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}

}
