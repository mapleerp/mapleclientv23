package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.events.ReceiptModeEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ReceiptModePopUpCtl {
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<ReceiptModeMst> receiptModeMstList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	ReceiptModeEvent receiptModeEvent;
    @FXML
    private TextField txtname;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<ReceiptModeMst> tblPopupView;

    @FXML
    private TableColumn<ReceiptModeMst, String> receiptMode;

    @FXML
    private Button btnCancel;

    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
			
			eventBus.post(receiptModeEvent);
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtname.requestFocus();

		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tblPopupView.requestFocus();
    		tblPopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
			eventBus.post(receiptModeEvent);

		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {
    	eventBus.post(receiptModeEvent);

    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    @FXML
	private void initialize() {
		txtname.textProperty().bindBidirectional(SearchString);

		receiptModeEvent= new ReceiptModeEvent();
		eventBus.register(this);


		tblPopupView.setItems(receiptModeMstList);
		receiptMode.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());




		LoadReceiptModeBySearch("");
		

		tblPopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getReceiptMode() && newSelection.getReceiptMode().length() > 0) {
					receiptModeEvent.setReceiptMode(newSelection.getReceiptMode());
					receiptModeEvent.setReceiptModeId(newSelection.getId());
					eventBus.post(receiptModeEvent);

				}
			}
		});

		 

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadReceiptModeBySearch((String) newValue);
			}
		});

	}
    private void LoadReceiptModeBySearch(String searchData) {

		receiptModeMstList.clear();
		ArrayList receiptmodeArray = new ArrayList();
		receiptmodeArray = RestCaller.SearchReceiptModeByName(searchData);
		Iterator itr = receiptmodeArray.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object receiptModes = lm.get("receiptMode");
			Object id = lm.get("id");
			if (null != id) {
				ReceiptModeMst receiptMode = new ReceiptModeMst();
				receiptMode.setReceiptMode((String) receiptModes);
				receiptMode.setId((String) id);
				receiptModeMstList.add(receiptMode);
			}

		}
		
		

		return;

	}

	public void ReceiptModeWithOutInsurance() {

		receiptModeMstList.clear();
		ArrayList receiptmodeArray = new ArrayList();
		receiptmodeArray = RestCaller.getReceiptModeWithOutInsurance();
		Iterator itr = receiptmodeArray.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object receiptModes = lm.get("receiptMode");
			Object id = lm.get("id");
			if (null != id) {
				ReceiptModeMst receiptMode = new ReceiptModeMst();
				receiptMode.setReceiptMode((String) receiptModes);
				receiptMode.setId((String) id);
				receiptModeMstList.add(receiptMode);
			}

		}
		
	}
	
}
