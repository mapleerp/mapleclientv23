package com.maple.mapleclient.controllers;
import java.io.IOException;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.lowagie.text.ListItem;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ItemWiseDtlReport;
import com.maple.mapleclient.entity.PharmacyItemMovementAnalisysReport;
import com.maple.mapleclient.entity.PharmacySalesReturnReport;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.ItemBatchEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
	
public class PharmacyItemOrCategoryWiseStockMovementReportCtl {
	String taskid;
	String processInstanceId;


	private ObservableList<PharmacyItemMovementAnalisysReport> pharmacyItemMovementAnalisysReporList = FXCollections.observableArrayList();
	
	
	private EventBus eventBus = EventBusFactory.getEventBus();
    @FXML
    private Button btnShowReport;

    @FXML
    private Button btPrintReport;

    @FXML
    private Button btnAdd;

    @FXML
    private ListView<String> lstItem;

    @FXML
    private Button btnClear;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    private TextField txtCategoryName;

    @FXML
    private TextField txtItemName;
    
    @FXML
    private ListView<String> listCategory;
    @FXML
    private TextField txtBatch;
 
    Boolean batchFieldStatus=false;
    @FXML
    private TableView<PharmacyItemMovementAnalisysReport> tblItemMovementTbl;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clTrEntryDate;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clGroupName;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clItemName;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clItemCode;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clBatchCode;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clDescription;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clUnitName;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clPartyName;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, String> clVoucherNumber;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, Number> clOPeningBalance;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, Number> clInwardQty;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, Number> clOutwardQty;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, Number> clBalance;

    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, Number> clValue;
    @FXML
    private TableColumn<PharmacyItemMovementAnalisysReport, Number> clCost;



    @FXML
    void AddItem(ActionEvent event) {

		
		  if(!txtCategoryName.getText().isEmpty()) {
			  listCategory.getItems().add(txtCategoryName.getText());
			  listCategory.getSelectionModel().select(txtCategoryName.getText());
		  txtCategoryName.clear(); 
		  }
		 
    	
    	if(!txtItemName.getText().isEmpty())
    		lstItem.getItems().add(txtItemName.getText());
    	lstItem.getSelectionModel().select(txtItemName.getText());
    	txtItemName.clear();
    }

    @FXML
    void clear(ActionEvent event) {
    	clear();
    	
    }

  
    @FXML
    void printReport(ActionEvent event) {
    	
    	// -----------------anandu 29-04-2021-------------------
    	Date fromdate = Date.valueOf(dpFromDate.getValue());
    	Date toDate = Date.valueOf(dpToDate.getValue());
    	
    	Format formatter;
		
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(fromdate);
		String endDate = formatter.format(toDate);
    	//ListView<String> listCategory;
		if(!txtItemName.getText().trim().isEmpty() && !listCategory.getSelectionModel().isEmpty())
		{
			notifyMessage(5,"Select any One",false);
			return;
		}
		else
		{
			String item = txtItemName.getText();
//		if(null != strDate && null != endDate && item.isEmpty() && listCategory.getSelectionModel().isEmpty())
		if(null != strDate && null != endDate && !lstItem.getSelectionModel().isEmpty() && !listCategory.getSelectionModel().isEmpty())
		{
//			ResponseEntity<List<ItemWiseDtlReport>> itemListSaved = RestCaller.getMonthSales(SystemSetting.systemBranch,strDate,endDate);
//			lstItem = (ListView<String>) FXCollections.observableArrayList(itemListSaved.getBody());
			
			
			List<String> selectedCategory = listCategory.getSelectionModel().getSelectedItems();
	        
	    	String cat="";
	    	if(selectedCategory.isEmpty()) {
	    		cat="NO-CATEGORY;";
			}else {
	    	for(String s:selectedCategory)
	    	{
	    		
	    		s = s.concat(";");
	    		cat= cat.concat(s);
	    		//System.out.print("yyyyyyyyyyyyyyyyyyyyyyyyyyyy"+cat);
	    	} 
			}
	    	List<String>selectedItems=lstItem.getSelectionModel().getSelectedItems();
	    	
	    	String items="";
	    	if(selectedItems.isEmpty()) {
	    		items="NO-ITEM;";
	    	}else {
	    	for(String i:selectedItems)
	    	{
	    		if(selectedItems.isEmpty()) {
	    		
	    		}
	    		i = i.concat(";");
	    		items= items.concat(i);
	    		//System.out.print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzz"+item);
	    	}} 
	    	String batch;
	    	if(txtBatch.getText().isEmpty()) {
	    		batch="NO-BATCH-SELECTED";
	    	}else {
	    		batch=txtBatch.getText();
	    		//System.out.print("rrrrrrrrrrrrrrrrrrrrrrrrrrrrr"+batch);
	    	}
			
			
			try {
//				JasperPdfReportService.Pharmacyitemmovement(strDate, endDate, cmbBranch.getSelectionModel().getSelectedItem(), txtCategoryName.getText(), item, txtBatch.getText());
				JasperPdfReportService.Pharmacyitemmovement(strDate, endDate, cmbBranch.getSelectionModel().getSelectedItem(),cat , items, batch);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}
			//-----------------------------------------
    }

    @FXML
    void showReport(ActionEvent event) {

    	
    	if(null==dpFromDate.getValue()) {
    		notifyMessage(5, "select from date",false);
    		return;
    	}
    	if(null==dpToDate.getValue()) {
    		notifyMessage(5, "select to date",false);
    		return;
    	}
    	if(null==cmbBranch.getValue()) {
    		notifyMessage(5, "select Branch",false);
    		return;
    	}
    	if(null==lstItem.getSelectionModel().getSelectedItem()) {
    		notifyMessage(5, "plz add Grouph",false);
    		return;
    	}
    	
  List<String> selectedCategory = listCategory.getSelectionModel().getSelectedItems();
        
    	String cat="";
    	if(selectedCategory.isEmpty()) {
    		cat="NO-CATEGORY;";
		}else {
    	for(String s:selectedCategory)
    	{
    		
    		s = s.concat(";");
    		cat= cat.concat(s);
    		//System.out.print("yyyyyyyyyyyyyyyyyyyyyyyyyyyy"+cat);
    	} 
		}
    	List<String>selectedItems=lstItem.getSelectionModel().getSelectedItems();
    	
    	String item="";
    	if(selectedItems.isEmpty()) {
    		item="NO-ITEM;";
    	}else {
    	for(String i:selectedItems)
    	{
    		if(selectedItems.isEmpty()) {
    		
    		}
    		i = i.concat(";");
    		item= item.concat(i);
    		//System.out.print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzz"+item);
    	}} 
    	String batch;
    	if(txtBatch.getText().isEmpty()) {
    		batch="NO-BATCH-SELECTED";
    	}else {
    		batch=txtBatch.getText();
    		//System.out.print("rrrrrrrrrrrrrrrrrrrrrrrrrrrrr"+batch);
    	}

    	
    	 	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
			String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
			ResponseEntity<List<PharmacyItemMovementAnalisysReport>> resp=RestCaller.getPharmacyItemMovement(startDate,endDate,cmbBranch.getSelectionModel().getSelectedItem(),cat,item,batch);
			pharmacyItemMovementAnalisysReporList = FXCollections.observableArrayList(resp.getBody());
    
    
    
			//System.out.print();
			tblItemMovementTbl.setItems(pharmacyItemMovementAnalisysReporList);
   
		fillTable();
//    	clear();
    }
	
private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			cmbBranch.getItems().add(branchMst.getBranchCode());
		
			
		}
		 
	}


private void loadCategoryPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
        @Subscribe
		public void popupOrglistner(CategoryEvent CategoryEvent) {

			Stage stage = (Stage) btPrintReport.getScene().getWindow();
			if (stage.isShowing()) {

				
				txtCategoryName.setText(CategoryEvent.getCategoryName());
		
			

			}
}

@FXML
void onEnterGrop(ActionEvent event) {

  loadCategoryPopup();
}

public void clear() {
	  
//	  
	  dpFromDate.setValue(null);
	  dpToDate.setValue(null);
	  cmbBranch.setValue(null);
	  lstItem.getItems().clear();
	  listCategory.getItems().clear();
	  txtBatch.clear();
	  tblItemMovementTbl.getItems().clear();
	  
}
    @FXML
   	private void initialize() {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	setBranches();
    	eventBus.register(this);
    	 lstItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	 
    	 if(batchFieldStatus=true) {
    		 txtBatch.setVisible(true);
    	 }else {
    		 txtBatch.setVisible(false);
    	 }
    	 
    }
    
    @FXML
    void OnActionBatch(ActionEvent event) {
    	
    	showPopup();
    	
    	
    }
	
    
    private void showPopup() {

    	System.out.print("inside show popup");
    	  List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
	        
	    	String item="";
	    	for(String s:selectedItems)
	    	{
	    		s = s.concat(";");
	    		item= item.concat(s);
	    	} 
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/BatchPopup.fxml"));
			Parent root = loader.load();
			BatchPopupCtl batchPopupCtl=loader.getController();
		       String searchString=String.valueOf(item);
		       System.out.print(searchString+"search string osssssssssssssssssssssssssssssssssssssssssss");
					batchPopupCtl.getItemBatchMstByBatch(item);
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
    
	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {
		
		txtItemName.setText(itemPopupEvent.getItemName());
	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}


	private void showStockitemPopup() {
		  List<String> selectedItems = listCategory.getSelectionModel().getSelectedItems();
	        
	    	String cat="";
	    	for(String s:selectedItems)
	    	{
	    		s = s.concat(";");
	    		cat= cat.concat(s);
	    	} 
	    	System.out.print(cat+"category isssssssssssssssssssssssssssssssssssssssssssssss");
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/itemPopupCategoryWise.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			
			ItemPopupCategoryWiseCtl itemPopupCategoryWiseCtl=fxmlLoader.getController();
		   	
			
			    
			itemPopupCategoryWiseCtl.LoadItemPopupBySearch("", cat);
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
    @FXML
    void onItemPopup(ActionEvent event) {
    	 showStockitemPopup();
    }
	
    @Subscribe
	public void popupBatchlistner(ItemBatchEvent itemBatchEvent) {
   
    	txtBatch.setText(itemBatchEvent.getBatchCode());
     	batchFieldStatus=true;
	}
    
    
    
   public void fillTable() {
	   
	   

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	

        //tblItemMovementTbl.setItems(pharmacyItemMovementAnalisysReporList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		//System.out.print("111111111111111111111111111111111111111111111111111111111");

		clBatchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clTrEntryDate.setCellValueFactory(cellData -> cellData.getValue().getTrEntryDateProperty());
		clGroupName.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
		clItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
   
		clDescription.setCellValueFactory(cellData -> cellData.getValue().getDescriptionProperty());
		clUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clPartyName.setCellValueFactory(cellData -> cellData.getValue().getPartyNameProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clOPeningBalance.setCellValueFactory(cellData -> cellData.getValue().getOpeningBalanceProperty());

		clInwardQty.setCellValueFactory(cellData -> cellData.getValue().getInwardQtyProperty());
		clOutwardQty.setCellValueFactory(cellData -> cellData.getValue().getOutwardQtyProperty());
		clPartyName.setCellValueFactory(cellData -> cellData.getValue().getPartyNameProperty());
		clBalance.setCellValueFactory(cellData -> cellData.getValue().getBalanceProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());
		clCost.setCellValueFactory(cellData -> cellData.getValue().getCostProperty());
   }
   @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }

    
}


