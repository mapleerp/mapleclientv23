package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.InvoiceFormatMst;
import com.maple.mapleclient.entity.InvoiceTermsAndConditionsMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class InvoiceTermsAndConditionsCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<InvoiceTermsAndConditionsMst> reportList = FXCollections.observableArrayList();

	InvoiceTermsAndConditionsMst invoiceTermsAndConditionsMst=null;
    @FXML
    private TextField txtTermsAndConditions;

    @FXML
    private TextField txtSlNo;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;

    @FXML
    private TableView<InvoiceTermsAndConditionsMst> tbTermsAndConditions;

    @FXML
    private TableColumn<InvoiceTermsAndConditionsMst, String> clSlno;

    @FXML
    private TableColumn<InvoiceTermsAndConditionsMst, String> clTermsAnddConditions;
    @FXML
	private void initialize() {
    	tbTermsAndConditions.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					invoiceTermsAndConditionsMst = new InvoiceTermsAndConditionsMst();
					invoiceTermsAndConditionsMst.setId(newSelection.getId());
				}
			}
		});
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == invoiceTermsAndConditionsMst)
    	{
    		return;
    	}
    	if(null == invoiceTermsAndConditionsMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteInvoiceTermsAndConditions( invoiceTermsAndConditionsMst.getId());
    	ResponseEntity<List<InvoiceTermsAndConditionsMst>> invoiceTerms = RestCaller.getAllInvoiceTermsAndConditions();
    	reportList = FXCollections.observableArrayList(invoiceTerms.getBody());
    	fillTable();
    }

    private void addItem()
    {

    	invoiceTermsAndConditionsMst =new InvoiceTermsAndConditionsMst();
    	invoiceTermsAndConditionsMst.setBranchCode(SystemSetting.getUser().getBranchCode());
    	invoiceTermsAndConditionsMst.setTermsAndConditions(txtTermsAndConditions.getText());
    	ResponseEntity<InvoiceTermsAndConditionsMst> respentity = RestCaller.saveInvoiceTermsAndConditionsMst(invoiceTermsAndConditionsMst);
    	invoiceTermsAndConditionsMst = respentity.getBody();
    	reportList.add(invoiceTermsAndConditionsMst);
    	fillTable();
    	txtSlNo.clear();
    	txtTermsAndConditions.clear();
    	txtTermsAndConditions.requestFocus();
    }
    @FXML
    void actionSave(ActionEvent event) {
    	addItem();
    }
    private void fillTable()
    {
    	tbTermsAndConditions.setItems(reportList);
		clSlno.setCellValueFactory(cellData -> cellData.getValue().getslNoProperty());
		clTermsAnddConditions.setCellValueFactory(cellData -> cellData.getValue().gettermsAndConditionsProperty());

    }

    @FXML
    void actionShowAll(ActionEvent event) {

    	ResponseEntity<List<InvoiceTermsAndConditionsMst>> invoiceTerms = RestCaller.getAllInvoiceTermsAndConditions();
    	reportList = FXCollections.observableArrayList(invoiceTerms.getBody());
    	fillTable();
    }
    @FXML
    void termsOnEnter(KeyEvent event) {

    	if(event.getCode()==KeyCode.ENTER)
    	{
    		btnSave.requestFocus();
    	}
    }
    @FXML
    void saveonEnter(KeyEvent event) {
    	if(event.getCode()==KeyCode.ENTER)
    	{
    		addItem();
    	}
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }
}
