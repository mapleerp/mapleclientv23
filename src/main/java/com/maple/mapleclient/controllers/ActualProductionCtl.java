package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.Format;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.ibm.icu.text.SimpleDateFormat;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ActualProductionDtl;
import com.maple.mapleclient.entity.ActualProductionHdr;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.DamageDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ProductionBatchStockDtl;
import com.maple.mapleclient.entity.ProductionDtl;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.entity.ReqRawMaterial;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ActualProductionCtl {

	String taskid;
	String processInstanceId;
	ActualProductionDtl actualProductionDtl = null;
	ActualProductionHdr actualProductionHdr = null;
	ProductionDtl productionDtl = new ProductionDtl();
	ItemMst itemmst = null;
	private ObservableList<ActualProductionDtl> productionDtlList = FXCollections.observableArrayList();
	private ObservableList<ProductionDtl> prodList = FXCollections.observableArrayList();
	private ObservableList<ProductionBatchStockDtl> productionBatchStk = FXCollections.observableArrayList();
	private ObservableList<ProductionBatchStockDtl> productionBatchStkSaved = FXCollections.observableArrayList();
	private ObservableList<ReqRawMaterial> reqRawMaterialList = FXCollections.observableArrayList();
	private ObservableList<KitDefinitionDtl> kitDefinitionDtlList = FXCollections.observableArrayList();

	boolean batch = false;

	@FXML
	private TextField txtKitName;

	@FXML
	private Button btnShowKit;

	@FXML
	private TextField txtBatch;

	@FXML
	private TextField txtActualQty;

	@FXML
	private Button btnPartiallySaved;
	@FXML
	private DatePicker dpProductionDate;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnClear;

	@FXML
	private TableView<ReqRawMaterial> tblReqQtyTable;

	@FXML
	private TableColumn<ReqRawMaterial, String> clItemName;

	@FXML
	private TableColumn<ReqRawMaterial, String> clmUnit;

	@FXML
	private TableColumn<ReqRawMaterial, Number> clReqQty;
	@FXML
	private Button btnAddAll;
	@FXML
	private TableView<ProductionDtl> tblProduction1;

	@FXML
	private TableColumn<ProductionDtl, String> clKitname;

	@FXML
	private TableColumn<ProductionDtl, String> clBatch;

	@FXML
	private TableColumn<ProductionDtl, Number> clQty;

	@FXML
	private TableColumn<ProductionDtl, String> columnId;

	@FXML
	private TableView<ActualProductionDtl> tbProduction2;

	@FXML
	private TableColumn<ActualProductionDtl, String> clProdKitname;

	@FXML
	private TableColumn<ActualProductionDtl, String> clProdBatch;

	@FXML
	private TableColumn<ActualProductionDtl, Number> clProdActualQty;

	@FXML
	private TableColumn<ActualProductionDtl, String> clProdId;
	@FXML
	private TableColumn<ActualProductionDtl, Number> clProductionCost;

	@FXML
	private Button btnSubmit;

	@FXML
	private ComboBox<String> cmbStore;

	@FXML
	void FinalSubmit(ActionEvent event) {
		tblReqQtyTable.setVisible(false);
		// ------------cheack rawmaterial stock------------------

//    	ResponseEntity<List<ReqRawMaterial>> reqRowMaterial = RestCaller.getReqRowMaterial(actualProductionHdr.getId());
//    	reqRawMaterialList=FXCollections.observableArrayList(reqRowMaterial.getBody());
//    	if(reqRawMaterialList.size()>0) {
//    		fillRawMaterialTable();
//    	tblReqQtyTable.setVisible(true);
//    	}

		// ------------cheack rawmaterial stock------------------

//    	else {
		ResponseEntity<List<ActualProductionDtl>> actualpod = RestCaller.getActualProductionDtl(actualProductionHdr);
		productionDtlList = FXCollections.observableArrayList(actualpod.getBody());
		int prddtlsize = productionDtlList.size();
		if (actualpod.getBody().size() == 0) {

			return;
		}
		try {
			String financialYear = SystemSetting.getFinancialYear();
			String sNo = RestCaller.getVoucherNumber(financialYear + "ACTPROD");
			actualProductionHdr.setVoucherNumber(sNo);

			RestCaller.updateBatchWiseActualProductionHdr(actualProductionHdr);
			ResponseEntity<List<ActualProductionDtl>> actualpod1 = RestCaller
					.getActualProductionDtl(actualProductionHdr);
			productionDtlList = FXCollections.observableArrayList(actualpod1.getBody());
			for (ActualProductionDtl act : productionDtlList) {
				act.getProductionDtl().setStatus("Y");
				RestCaller.updateProductionDtl(act.getProductionDtl());
			}
			for (ActualProductionDtl actprod : actualpod.getBody()) {
				updatePricedef(actprod);
				updateBatchPriceDef(actprod);
			}
			txtActualQty.clear();
			txtBatch.clear();
			txtKitName.clear();
			tblProduction1.getItems().clear();
			tbProduction2.getItems().clear();

			productionDtlList.clear();

			productionDtlList.clear();
			productionBatchStk.clear();
			productionBatchStkSaved.clear();
			actualpod = null;
			actualProductionDtl = null;
			tbProduction2.getItems().clear();
			actualProductionHdr = null;
			productionDtlList.clear();
			showKitByDate();
			tblReqQtyTable.setVisible(false);
			notifyMessage(3, "Saved Successfully");
		} catch (Exception e) {

			ResponseEntity<List<ReqRawMaterial>> reqRowMaterial = RestCaller
					.getReqRowMaterial(actualProductionHdr.getId());
			reqRawMaterialList = FXCollections.observableArrayList(reqRowMaterial.getBody());
			if (reqRawMaterialList.size() > 0) {
				fillRawMaterialTable();
				tblReqQtyTable.setVisible(true);
			}

			RestCaller.deleteActualProductionDtlsByHdrId(actualProductionHdr.getId());

			txtActualQty.clear();
			txtBatch.clear();
			txtKitName.clear();
			tblProduction1.getItems().clear();
			tbProduction2.getItems().clear();
			productionDtlList.clear();
			productionDtlList.clear();
			productionBatchStk.clear();
			productionBatchStkSaved.clear();
			actualpod = null;
			actualProductionDtl = null;
			tbProduction2.getItems().clear();
			actualProductionHdr = null;
			productionDtlList.clear();
//			showKitByDate();
			tblReqQtyTable.setVisible(false);
			notifyMessage(3, "Not in stock");
			e.printStackTrace();
		}

	}

	private void updateBatchPriceDef(ActualProductionDtl actprod) {

		BatchPriceDefinition priceDefenition = new BatchPriceDefinition();

		priceDefenition.setItemId(actprod.getItemId());
		priceDefenition.setBranchCode(SystemSetting.systemBranch);
		priceDefenition.setEndDate(null);
		priceDefenition.setStartDate(Date.valueOf(dpProductionDate.getValue()));

		String pricetype = "COST PRICE";
		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller.getPriceDefenitionByName(pricetype);
		PriceDefenitionMst price = new PriceDefenitionMst();
		price = priceDefenitionSaved.getBody().get(0);
		if (null != price) {
			priceDefenition.setPriceId(price.getId());
		}
		BigDecimal costprice = new BigDecimal(actprod.getProductionCost());
		costprice = costprice.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		priceDefenition.setAmount(costprice.doubleValue());

		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(actprod.getItemId());
		priceDefenition.setUnitId(getItem.getBody().getUnitId());
		priceDefenition.setBatch(actprod.getBatch());
		ResponseEntity<BatchPriceDefinition> respentity = RestCaller.saveBatchPriceDefinition(priceDefenition);

	}

	private void updatePricedef(ActualProductionDtl actprod) {

		PriceDefinition priceDefenition = new PriceDefinition();

		priceDefenition.setItemId(actprod.getItemId());
		priceDefenition.setBranchCode(SystemSetting.systemBranch);
		priceDefenition.setEndDate(null);
		priceDefenition.setStartDate(Date.valueOf(dpProductionDate.getValue()));

		String pricetype = "COST PRICE";
		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller.getPriceDefenitionByName(pricetype);
		PriceDefenitionMst price = new PriceDefenitionMst();
		price = priceDefenitionSaved.getBody().get(0);
		if (null != price) {
			priceDefenition.setPriceId(price.getId());
		}
		BigDecimal costprice = new BigDecimal(actprod.getProductionCost());
		costprice = costprice.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		priceDefenition.setAmount(costprice.doubleValue());

		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(actprod.getItemId());
		priceDefenition.setUnitId(getItem.getBody().getUnitId());
		ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(priceDefenition);

	}

	@FXML
	void actionPartiallySaved(ActionEvent event) {
		java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);
		ResponseEntity<ActualProductionHdr> getHdr = RestCaller.getActualProductionByDate(voucherDate);
		if (null != getHdr.getBody()) {
			actualProductionHdr = getHdr.getBody();
			fillActualProductionDtl(actualProductionHdr);
		}

	}

	@FXML
	private void initialize() {
		// dpProductionDate.setValue(LocalDate.now());
		tblReqQtyTable.setVisible(false);
		dpProductionDate = SystemSetting.datePickerFormat(dpProductionDate, "dd/MMM/yyyy");
		tbProduction2.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					actualProductionDtl = new ActualProductionDtl();
					actualProductionDtl.setId(newSelection.getId());

				}
			}

		});

		ResponseEntity<List<StoreMst>> storeMstListResp = RestCaller.getAllStoreMst();
		List<StoreMst> storeMstList = storeMstListResp.getBody();

		cmbStore.getSelectionModel().select("MAIN");

		for (StoreMst storeMst : storeMstList) {
			cmbStore.getItems().add(storeMst.getShortCode());
		}
	}

	@FXML
	void actionDelete(ActionEvent event) {

		if (null == actualProductionDtl.getId()) {
			return;
		}
		RestCaller.deleteActualProdutcionDtlById(actualProductionDtl.getId());
		fillActualProductionDtl(actualProductionHdr);
	}

	@FXML
	void AddAll(ActionEvent event) {

		java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);
		ResponseEntity<ActualProductionHdr> getHdr = RestCaller.getActualProductionByDate(voucherDate);
		if (null != getHdr.getBody()) {
			actualProductionHdr = getHdr.getBody();
		}
		if (null == actualProductionHdr) {
			actualProductionHdr = new ActualProductionHdr();
			actualProductionHdr.setVoucherDate(Date.valueOf(dpProductionDate.getValue()));
			actualProductionHdr.setBranchCode(SystemSetting.systemBranch);
			ResponseEntity<ActualProductionHdr> respentityHdr = RestCaller.saveActualProductionHdr(actualProductionHdr);
			actualProductionHdr = respentityHdr.getBody();
		}
		for (ProductionDtl prddtl : prodList) {
			actualProductionDtl = new ActualProductionDtl();
			ResponseEntity<ActualProductionDtl> getByDtlId = RestCaller.getActualProductionDtlById(prddtl.getId());

			if (null != getByDtlId.getBody()) {
				actualProductionDtl = getByDtlId.getBody();
				fillActualProductionDtl(actualProductionHdr);
			}

			actualProductionDtl.setStore("MAIN");

			if (null != prddtl.getStore()) {
				actualProductionDtl.setStore(prddtl.getStore());

			}
			actualProductionDtl.setActualQty(prddtl.getQty());
			actualProductionDtl.setBatch(prddtl.getBatch());
			actualProductionDtl.setItemId(prddtl.getItemId());
			actualProductionDtl.setProductionDtl(productionDtl);
			double prodCost = calculateProdCost(voucherDate, prddtl);
			BigDecimal productionCost = new BigDecimal(prodCost);
			productionCost = productionCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			actualProductionDtl.setProductionCost(productionCost.doubleValue());
			actualProductionDtl.setActualProductionHdr(actualProductionHdr);
			productionDtl.setId(prddtl.getId());
			ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(prddtl.getItemId());
			itemmst = respitem.getBody();

//			String ratMaterial=rawMaterialCheck(productionDtl);
//			if(ratMaterial == null)
//			{
			ResponseEntity<ActualProductionDtl> respentity = RestCaller.saveActualProductionDtl(actualProductionDtl);
			actualProductionDtl = respentity.getBody();

			actualProductionDtl.setItemName(respitem.getBody().getItemName());
			productionDtlList.add(actualProductionDtl);
			actualproductionfill();
//			}
//			else
//			{
//				notifyMessage(5,"Raw Materials Not in Stock"+ratMaterial);
//				
//			}
		}
		tblProduction1.getItems().clear();
		showKitByDate();

		// print

	}

	public String rawMaterialCheck(ProductionDtl prodDtl) {
		String items = null;
		ResponseEntity<List<ProductionDtlDtl>> getProdDtlDtl = RestCaller.getProductionDtlDtl(prodDtl.getId());
		for (ProductionDtlDtl prodDtlDtl : getProdDtlDtl.getBody()) {

			double sysQty = 0.0;
			Map<String, Double> map = new HashMap<String, Double>();
			map = RestCaller.getRequiredMaterial(prodDtlDtl.getRawMaterialItemId(), prodDtlDtl.getQty());
			if (map.size() == 0) {
				if (null == items) {
					items = "";
				}
				items = items + "," + prodDtlDtl.getItemName();
			} else {
				for (Map.Entry<String, Double> entry : map.entrySet()) {
					sysQty = sysQty + entry.getValue();
				}
				if (prodDtlDtl.getQty() > sysQty) {
					if (null == items) {
						items = "";
					}

					items = items + "," + prodDtlDtl.getItemName();

				}
			}

		}

		return items;
	}

	@FXML
	void addItem(ActionEvent event) {

		java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);
		ResponseEntity<ActualProductionHdr> getHdr = RestCaller.getActualProductionByDate(voucherDate);
		if (null != getHdr.getBody()) {
			actualProductionHdr = getHdr.getBody();
			fillActualProductionDtl(actualProductionHdr);
		}
		if (null == actualProductionHdr) {
			actualProductionHdr = new ActualProductionHdr();
			actualProductionHdr.setVoucherDate(Date.valueOf(dpProductionDate.getValue()));
			actualProductionHdr.setBranchCode(SystemSetting.getSystemBranch());
			ResponseEntity<ActualProductionHdr> respentityHdr = RestCaller.saveActualProductionHdr(actualProductionHdr);
			actualProductionHdr = respentityHdr.getBody();
		}
		actualProductionDtl = new ActualProductionDtl();
		ResponseEntity<ActualProductionDtl> getByDtlId = RestCaller.getActualProductionDtlById(productionDtl.getId());

//    	String ratMaterial=rawMaterialCheck(productionDtl);
//		if(ratMaterial != null)
//		{
//			notifyMessage(5,"Raw Materials Not in Stock"+ratMaterial);
//			return;
//		}
		if (null != getByDtlId.getBody()) {
			actualProductionDtl = getByDtlId.getBody();
		}
		actualProductionDtl.setStore("MAIN");

		if (null != productionDtl.getStore()) {
			actualProductionDtl.setStore(productionDtl.getStore());

		}
		actualProductionDtl.setActualProductionHdr(actualProductionHdr);
		actualProductionDtl.setBatch(txtBatch.getText());
		actualProductionDtl.setActualQty(Double.valueOf(txtActualQty.getText()));
		actualProductionDtl.setProductionDtl(productionDtl);
		double prodCost = calculateProdCost(voucherDate, productionDtl);
		BigDecimal productionCost = new BigDecimal(prodCost);
		productionCost = productionCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		actualProductionDtl.setProductionCost(productionCost.doubleValue());

		ResponseEntity<ItemMst> itemsaved = RestCaller.getItemByNameRequestParam(txtKitName.getText());
		actualProductionDtl.setItemId(itemsaved.getBody().getId());
		ResponseEntity<ActualProductionDtl> respentity = RestCaller.saveActualProductionDtl(actualProductionDtl);
		actualProductionDtl = respentity.getBody();
//		actualProductionDtl.setItemName(itemsaved.getBody().getItemName());
		// =====================================================

		ResponseEntity<List<ActualProductionDtl>> actualProductionListResp = RestCaller
				.getActualProductionDtl(actualProductionHdr);
		productionDtlList = FXCollections.observableArrayList(actualProductionListResp.getBody());
//		productionDtlList.add(actualProductionDtl);
		actualproductionfill();

//    	ResponseEntity<List<ProductionBatchStockDtl>> getproductionBatch = RestCaller.getProductionBatchStockDtl(actualProductionDtl.getItemId());
//    	if(getproductionBatch.getBody().size()>0)
//		{
//    	loadProductionBatchPopUp(actualProductionDtl);
//		}
		productionDtl = new ProductionDtl();
		showKitByDate();

		txtActualQty.clear();
		txtBatch.clear();
		txtKitName.clear();
	}

	private void fillActualProductionDtl(ActualProductionHdr actualProductionHdr) {
		ResponseEntity<List<ActualProductionDtl>> getactDtl = RestCaller.getActualProductionDtl(actualProductionHdr);
		productionDtlList = FXCollections.observableArrayList(getactDtl.getBody());
		for (ActualProductionDtl act : productionDtlList) {
			ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(act.getItemId());
			act.setItemName(getItem.getBody().getItemName());
		}
		tbProduction2.setItems(productionDtlList);

		clProdActualQty.setCellValueFactory(cellData -> cellData.getValue().getactualQtyProperty());
		clProdKitname.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clProdBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clProdId.setCellValueFactory(cellData -> cellData.getValue().getidProperty());
		clProductionCost.setCellValueFactory(cellData -> cellData.getValue().getProductionCostProperty());

	}

	@FXML
	void allRawMaterialReq(ActionEvent event) {

		// print
	}

	public double calculateProdCost(String voucherDate, ProductionDtl prodDtl) {
		ResponseEntity<List<ProductionDtlDtl>> getRawMaterial = RestCaller.getProductionDtlDtl(prodDtl.getId());

		ResponseEntity<PriceDefenitionMst> getPriceId = RestCaller.getPriceDefenitionMstByName("COST PRICE");
		double prodCost = 0.0;
		for (ProductionDtlDtl productionDtlDtl : getRawMaterial.getBody()) {

			ResponseEntity<PriceDefinition> getPrice = RestCaller.getPriceDefenitionByCostPrice(
					productionDtlDtl.getRawMaterialItemId(), getPriceId.getBody().getId(), productionDtlDtl.getUnitId(),
					voucherDate);
			if (null != getPrice.getBody()) {
				// prodCost = prodCost +
				// (getPrice.getBody().getAmount()*(productionDtlDtl.getQty()/prodDtl.getQty())*actualProductionDtl.getProductionDtl().getQty());
				prodCost = prodCost + (getPrice.getBody().getAmount() * productionDtlDtl.getQty() / prodDtl.getQty());

				// prodCost = prodCost+
				// (getPrice.getBody().getAmount()*productionDtlDtl.getQty()/prodDtl.getQty()*actualProductionDtl.getActualQty());

			} else {
				ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(productionDtlDtl.getRawMaterialItemId());
				prodCost = prodCost + (getItem.getBody().getStandardPrice() * productionDtlDtl.getQty());
				// prodCost = prodCost +
				// (getItem.getBody().getStandardPrice()*(productionDtlDtl.getQty()/prodDtl.getQty())*actualProductionDtl.getActualQty()
				// );

			}

		}
		return prodCost / actualProductionDtl.getActualQty();
	}

	@FXML
	void clearfileds(ActionEvent event) {

		txtActualQty.clear();
		txtBatch.clear();
		txtKitName.clear();
		tblProduction1.getItems().clear();
		tbProduction2.getItems().clear();
		productionDtlList.clear();
	}

	@FXML
	void ShowKit(ActionEvent event) {
		showKitByDate();

	}

	@FXML
	void showItemPopUp(MouseEvent event) {

	}

	@FXML
	void tableToShowDtl(MouseEvent event) {

		if (tblProduction1.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tblProduction1.getSelectionModel();
			prodList = selectionModel.getSelectedItems();
			for (ProductionDtl pck : prodList) {

				itemmst.setId(pck.getItemId());
				txtKitName.setText(pck.getKitname());
				txtBatch.setText(pck.getBatch());
				txtActualQty.setText(Double.toString(pck.getQty()));
				productionDtl.setId(pck.getId());
				productionDtl.setBatch(pck.getBatch());
				productionDtl.setItemId(pck.getItemId());
				productionDtl.setKitname(pck.getKitname());
				productionDtl.setQty(pck.getQty());

				productionDtl.setStore("MAIN");
				if (null != pck.getStore()) {
					productionDtl.setStore(pck.getStore());

				}
//			
			}
		}

	}

	@FXML
	void tableToShowDtl2(MouseEvent event) {
		if (tbProduction2.getSelectionModel().getSelectedItem() != null) {

			TableViewSelectionModel selectionModel = tbProduction2.getSelectionModel();
			productionDtlList = selectionModel.getSelectedItems();
			for (ActualProductionDtl pck : productionDtlList) {
				ResponseEntity<ItemMst> getItems = RestCaller.getitemMst(pck.getItemId());
				txtKitName.setText(getItems.getBody().getItemName());
				// loadProductionBatchPopUp(pck);
			}
		}
	}

	private void filltable() {
		for (ProductionDtl prd : prodList) {
			ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(prd.getItemId());
			itemmst = respitem.getBody();
			prd.setKitname(itemmst.getItemName());
		}
		tblProduction1.setItems(prodList);
		clKitname.setCellValueFactory(cellData -> cellData.getValue().getKitnameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		columnId.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());

	}

	private void loadProductionBatchPopUp(ActualProductionDtl actualProductionDtl) {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemBatchWiseStockMgmtPopup.fxml"));
			Parent root = loader.load();
			ItemBatchWiseStockPopupCtl popupctl = loader.getController();
			popupctl.LoadItemPopupBySearch(txtKitName.getText(), actualProductionDtl);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//					dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void actualproductionfill()

	{
		tbProduction2.setItems(productionDtlList);

		for (ActualProductionDtl actualProductionDtl : productionDtlList) {
			ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(actualProductionDtl.getItemId());
			if (null != itemMstResp.getBody()) {
				actualProductionDtl.setItemName(itemMstResp.getBody().getItemName());
			}
		}

		clProdBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clProdId.setCellValueFactory(cellData -> cellData.getValue().getidProperty());
		clProdKitname.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clProdActualQty.setCellValueFactory(cellData -> cellData.getValue().getactualQtyProperty());
		clProductionCost.setCellValueFactory(cellData -> cellData.getValue().getProductionCostProperty());

	}

	private void fillRawMaterialTable() {
		tblReqQtyTable.setItems(reqRawMaterialList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clmUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
		clReqQty.setCellValueFactory(cellData -> cellData.getValue().getReqQtyProperty());

	}

	private void showKitByDate() {

		if (null == dpProductionDate.getValue()) {
			notifyMessage(2, "please select date");
			return;
		}
		String store = "MAIN";

		if (!cmbStore.getSelectionModel().getSelectedItem().trim().isEmpty()) {
			store = cmbStore.getSelectionModel().getSelectedItem().toString();
		}

		java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);

		ResponseEntity<List<ProductionDtl>> productionDtlsaved = RestCaller
				.getActualProductionDtlsByDateAndStore(voucherDate, store);
		prodList = FXCollections.observableArrayList(productionDtlsaved.getBody());

		if (!productionDtlList.isEmpty())

//	    	
			for (ActualProductionDtl act : productionDtlList) {
				for (int i = 0; i < prodList.size(); i++) {
					ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(prodList.get(i).getItemId());
					itemmst = respitem.getBody();
					prodList.get(i).setKitname(itemmst.getItemName());
					if (act.getProductionDtl().getId().equalsIgnoreCase(prodList.get(i).getId())) {

						prodList.remove(i);
					}

				}
			}
//			
		else {
			for (ProductionDtl proddtl : prodList) {
				ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(proddtl.getItemId());
				itemmst = respitem.getBody();
				proddtl.setKitname(itemmst.getItemName());

			}
		}
		filltable();

	}

	public void notifyMessage(int duration, String msg) {
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

}