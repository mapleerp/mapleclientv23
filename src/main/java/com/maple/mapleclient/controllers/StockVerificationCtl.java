package com.maple.mapleclient.controllers;

import java.util.List;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.StockVerificationDtl;
import com.maple.mapleclient.entity.StockVerificationMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class StockVerificationCtl {
	
	String taskid;
	String processInstanceId;
	
	
	private ObservableList<StockVerificationDtl> StockVerificationDtlList = FXCollections.observableArrayList();

	StockVerificationDtl stockVerificationDtl = null;
	StockVerificationMst stockVerificationMst = null;
    @FXML
    private TableView<StockVerificationDtl> tbStcokVerification;

    @FXML
    private TableColumn<StockVerificationDtl, String> clItemName;

    @FXML
    private TableColumn<StockVerificationDtl, Number> clQty;

    @FXML
    private TextField txtItemName;
    
    @FXML
    private TextField txtQty;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFinalSave;
    @FXML
	private void initialize() {
    	java.util.Date toDate =SystemSetting.systemDate;
		String vFDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
		ResponseEntity<StockVerificationMst> getStock = RestCaller.getStockVerificationMstByVoucherDate(vFDate);

    	stockVerificationMst = new StockVerificationMst();
    	if(null != getStock.getBody())
    	stockVerificationMst = getStock.getBody();
    	
    	ResponseEntity<List<StockVerificationDtl>> getStockDtl = RestCaller.getStockVerificationDtlByHdrId(stockVerificationMst.getId());
    	if(null != getStockDtl.getBody())
    	StockVerificationDtlList = FXCollections.observableArrayList(getStockDtl.getBody());
    
    	fillTable();
    	
    	tbStcokVerification.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					stockVerificationDtl = new StockVerificationDtl();
					stockVerificationDtl.setId(newSelection.getId());
					stockVerificationDtl.setItemId(newSelection.getItemId());
					stockVerificationDtl.setSystemQty(RestCaller.getStockOfItem(newSelection.getItemId()));
					txtItemName.setText(newSelection.getItemName());
				}
			}
		});
    }
    private void fillTable()
    {
    	for(StockVerificationDtl stk:StockVerificationDtlList)
    	{
    		ResponseEntity<ItemMst> getItems = RestCaller.getitemMst(stk.getItemId());
    		stk.setItemName(getItems.getBody().getItemName());
    	}
    	tbStcokVerification.setItems(StockVerificationDtlList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());


    }
    @FXML
    void AddItem(ActionEvent event) {
    	stockVerificationDtl.setQty(Double.parseDouble(txtQty.getText()));
    	
    	RestCaller.updateStockVerification(stockVerificationDtl);
    	notifyMessage(5,"Stock Entered");
    	txtItemName.clear();
    	txtQty.clear();
    	
    	ResponseEntity<List<StockVerificationDtl>> getStockDtl = RestCaller.getStockVerificationDtlByHdrId(stockVerificationMst.getId());
    	StockVerificationDtlList = FXCollections.observableArrayList(getStockDtl.getBody());
    	fillTable();
    }

    @FXML
    void DeleteItem(ActionEvent event) {

    	stockVerificationDtl.setQty(0.0);
    	RestCaller.updateStockVerification(stockVerificationDtl);
    	txtItemName.clear();
    	txtQty.clear();
    	
    	notifyMessage(5,"Stock Cleared");
    	ResponseEntity<List<StockVerificationDtl>> getStockDtl = RestCaller.getStockVerificationDtlByHdrId(stockVerificationMst.getId());
    	StockVerificationDtlList = FXCollections.observableArrayList(getStockDtl.getBody());
    	fillTable();
    }
    @FXML
    void FinalSave(ActionEvent event) {
		String financialYear = SystemSetting.getFinancialYear();
		String sNo = RestCaller.getVoucherNumber(financialYear + "STKVER");
		stockVerificationMst.setVoucherNumber(sNo);
		stockVerificationMst.setUserId(SystemSetting.getUser().getId());
		stockVerificationMst.setBranchCode(SystemSetting.systemBranch);
		stockVerificationMst.setVoucherDate(SystemSetting.systemDate);
		RestCaller.updateStockVerificationMst(stockVerificationMst);
		notifyMessage(5,"All Items Added Successfully");
		txtItemName.clear();
    	txtQty.clear();
		stockVerificationDtl = null;
		stockVerificationMst = null;
		tbStcokVerification.getItems().clear();

    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}


}
