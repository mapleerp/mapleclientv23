package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties.Authentication;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ConsumptionReport;
import com.sun.glass.ui.Application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import sun.util.logging.resources.logging;

public class ConsumptionDetailReportCtl {

	@FXML
    private AnchorPane btnClear;

    @FXML
    private DatePicker txtFromDate;

    @FXML
    private DatePicker txtToDate;

    @FXML
    private Button btnShowReport;

    @FXML
    private Button btnPrint;

    @FXML
    private TextField txtCategory;

    @FXML
    private ListView<String> lstCategory;

    @FXML
    private Button btnAdd;

    @FXML
    private TableView<ConsumptionReport> tblTable;

    @FXML
    private TableColumn<ConsumptionReport, String> clDepart;

    @FXML
    private TableColumn<ConsumptionReport, String> clVocherNo;

    @FXML
    private TableColumn<ConsumptionReport, String> clDate;

    @FXML
    private TableColumn<ConsumptionReport, String> clUsername;

    @FXML
    private TableColumn<ConsumptionReport, String> clmnItemName;

    @FXML
    private TableColumn<ConsumptionReport, String> clmnItemGroup;

    @FXML
    private TableColumn<ConsumptionReport, String> clmnBatch;

    @FXML
    private TableColumn<ConsumptionReport, String> clmnExpiry;

    @FXML
    private TableColumn<ConsumptionReport, Number> clmnQuantity;

    @FXML
    private TableColumn<ConsumptionReport, Number> clmnRate;

    @FXML
    private TableColumn<ConsumptionReport, Number> clAmount;
    
    private ObservableList<ConsumptionReport> consumptioReportList = FXCollections.observableArrayList();

    private EventBus eventBus = EventBusFactory.getEventBus();
    @FXML
	private void initialize() {
    	txtToDate = SystemSetting.datePickerFormat(txtToDate, "dd/MMM/yyyy");
    	txtFromDate = SystemSetting.datePickerFormat(txtFromDate, "dd/MMM/yyyy");
    	eventBus.register(this);
		lstCategory.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
    }
    
    @FXML
    void addCategory(ActionEvent event) {
    	lstCategory.getItems().add(txtCategory.getText());
		lstCategory.getSelectionModel().select(txtCategory.getText());
		txtCategory.clear();
    }

    @FXML
    void clear(ActionEvent event) {
    	
    	tblTable.getItems().clear();
    	txtFromDate.getEditor().clear();;
    	txtToDate.getEditor().clear();
    	lstCategory.getItems().clear();
    	txtCategory.clear();

    }

    @FXML
    void print(ActionEvent event) {
    	if(null==txtFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
    		return;
    		
    	}
    	if (null==txtToDate.getValue()) {
    		notifyMessage(5, "Please select to date", false);
    		return;
    		
    	}
    	java.util.Date uDate = Date.valueOf(txtFromDate.getValue());
    	String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
    	java.util.Date uDate1 = Date.valueOf(txtToDate.getValue());
    	String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
    	
    	String username = SystemSetting.getUser().getUserName();
    	
    	List<String> selectedItems = lstCategory.getSelectionModel().getSelectedItems();
    	
		String cat = "";
		for (String s : selectedItems) {
			s = s.concat(";");
			cat = cat.concat(s);
		}
    	
		if(cat.equalsIgnoreCase("")) {
    	try {
    		JasperPdfReportService.consumptionDetailReportWithoutCategory(strDate,strDate1,username) ;
		} catch (Exception e) {
			// TODO: handle exception
		}
    	}else {
    		try {
    			JasperPdfReportService.consumptionDetailReport(strDate,strDate1,cat,username) ;
    			
    		} catch (Exception e) {
    			// TODO: handle exception
    		}
    	}
    }

    @FXML
    void showReport(ActionEvent event) {
    	if(null == txtFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
			return;
    	}
    	if(null == txtToDate.getValue())
    	{
    		notifyMessage(5, "Please select to date", false);
    		return;
    	}
    	
    	java.util.Date uDate = Date.valueOf(txtFromDate.getValue());
    	String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
    	java.util.Date uDate1 = Date.valueOf(txtToDate.getValue());
    	String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
    	
    	String username = SystemSetting.getUser().getUserName();
    	
    	List<String> selectedItems = lstCategory.getSelectionModel().getSelectedItems();
    	
    	
		String cat = "";
		for (String s : selectedItems) {
			s = s.concat(";");
			cat = cat.concat(s);
		}
		
		if(cat.equalsIgnoreCase("")) {
    	
    	ResponseEntity<List<ConsumptionReport>> consumptionReportResp = RestCaller.getConsumptionDetailWithoutCategory( strDate,strDate1,username);
    	consumptioReportList = FXCollections.observableArrayList(consumptionReportResp.getBody());
    	}
    	else {
    		ResponseEntity<List<ConsumptionReport>> consumptionReportResp = RestCaller.getConsumptionDetail( strDate,strDate1,cat,username);
        	consumptioReportList = FXCollections.observableArrayList(consumptionReportResp.getBody());
    	}
             if (consumptioReportList.size() > 0) {
			
			
			filltable();
			
			
		}
    }
    
    private void filltable() {
    	
    	tblTable.setItems(consumptioReportList);
    	
    	clDepart.setCellValueFactory(cellData -> cellData.getValue().getDepartmentProperty());
    	clVocherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
    	clDate.setCellValueFactory(cellData -> cellData.getValue().getDateProperty());
    	clUsername.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
    	clmnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clmnItemGroup.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
    	clmnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
    	clmnExpiry.setCellValueFactory(cellData -> cellData.getValue().getExpiryProperty());
    	clmnQuantity.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clmnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		
	}
    
    private void notifyMessage(int i, String string, boolean b) {
			
	    	Image img;
			if (b) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

	    	
		}
    
    @FXML
    void onEnterGrop(ActionEvent event) {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    @Subscribe
	public void popupOrglistner(CategoryEvent CategoryEvent) {

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtCategory.setText(CategoryEvent.getCategoryName());

		}

	}

}
