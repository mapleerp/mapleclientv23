package com.maple.mapleclient.controllers;

import java.awt.event.FocusEvent;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.ReorderMst;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;

import com.maple.mapleclient.events.SupplierPopupEvent;

import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import javafx.util.Duration;

@Controller
public class ReorderCtl {
	
	String taskid;
	String processInstanceId;
	EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<ReorderMst> reorderList = FXCollections.observableArrayList();
	private ObservableList<ReorderMst> reorderResponseList = FXCollections.observableArrayList();

	private ObservableList<ItemBatchMst> reOrderListTable = FXCollections.observableArrayList();
	ReorderMst reorderMst = null;
	String itemId = "";
	String supplierId = "";

	StringProperty SearchString = new SimpleStringProperty();

	@FXML
	private TextField rdrItemName;
	@FXML
	private TextField rdrSupName;

 
	@FXML
	private DatePicker rdrStartDt;

	@FXML
	private DatePicker rdrEndDt;

	@FXML
	private TextField rdrMinQty;

	@FXML
	private TextField rdrMaxQty;

	@FXML
	private Button rdrsv;

    @FXML
    private Button rdrsv1;
    @FXML
    private Button rdrSearch;
    @FXML
    private Button rdrsv11;
    @FXML
    private TableView<ItemBatchMst> tab2;
	@FXML
	private TableView<ReorderMst> rdrTable;

   @FXML
   private TableColumn<ItemBatchMst, String> itemName;

   @FXML
	private TableColumn<ItemBatchMst,Number> qty;

	@FXML
	private TableColumn<ReorderMst, String> rdrC1;

	@FXML
	private TableColumn<ReorderMst, String> rdrC2;

	@FXML
	private TableColumn<ReorderMst, LocalDate> rdrC3;

	@FXML
	private TableColumn<ReorderMst, LocalDate> rdrC4;

	@FXML
	private TableColumn<ReorderMst, Number> rdrC5;

	@FXML
	private TableColumn<ReorderMst, Number> rdrC6;

	public String ItemNamePropertyy;

	@FXML
	private void initialize() {
		rdrStartDt = SystemSetting.datePickerFormat(rdrStartDt, "dd/MMM/yyyy");
		rdrEndDt = SystemSetting.datePickerFormat(rdrEndDt, "dd/MMM/yyyy");
		try {
			

			eventBus.register(this);

			reorderMst = new ReorderMst();

			

			rdrMinQty.textProperty().addListener(new ChangeListener<String>() {

				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						rdrMinQty.setText(oldValue);
					}
				}
			});

			rdrMaxQty.textProperty().addListener(new ChangeListener<String>() {

				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						rdrMaxQty.setText(oldValue);
					}
				}
			});
			rdrItemName.requestFocus();
	
			rdrItemName.textProperty().bindBidirectional(SearchString);
			
			SearchString.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					System.out.println("item Search changed");
					ReoderBySearch((String) newValue);
				}
			});
			//ReoderBySearch("");
			///showAll();
 

		}

		catch (Exception e) {
			Stage owner;

			Alert alert = new Alert(AlertType.NONE, "confirm delete", ButtonType.OK);

			alert.setTitle("500 SERVER ERROR");
			alert.setContentText("SERVER IS TEMPORARILY DOWN...!!");
			alert.initModality(Modality.APPLICATION_MODAL);

			alert.showAndWait();

		}

	}


    @FXML
    void rdrSearch(ActionEvent event) {
    	ResponseEntity<List<ItemBatchMst>> respentityReorder = RestCaller.returnStatus();
    	reOrderListTable = FXCollections.observableArrayList(respentityReorder.getBody());
    	fillTable();
		//ReoderBySearch(itemId);
    }
    @FXML
    void rdrReorderSearch(ActionEvent event) {

//    	ResponseEntity<List<ReorderMst>> respentityReorders =RestCaller.returnValueOfReoder();
		String batch = rdrItemName.getText() == null ? "NOITEM" : rdrItemName.getText();
		String sup = rdrSupName.getText() == null ? "NOSUP" : rdrSupName.getText();
		if(batch.trim().length() == 0|| sup.trim().length() == 0 )
		{
			ResponseEntity<List<ReorderMst>> respentityReorders =RestCaller.returnValueOfReoder();
    		reorderList=FXCollections.observableArrayList(respentityReorders.getBody());
		}
		else if(batch.trim().equalsIgnoreCase("NOITEM") || sup.trim().equalsIgnoreCase("NOSUP") )
    	{
    		ResponseEntity<List<ReorderMst>> respentityReorders =RestCaller.returnValueOfReoder();
    		reorderList=FXCollections.observableArrayList(respentityReorders.getBody());
    	}
		
		else if(!batch.trim().equalsIgnoreCase("NOITEM") || !sup.trim().equalsIgnoreCase("NOSUP") )
    	{
    		ResponseEntity<List<ReorderMst>> respentityReorders = RestCaller.returnReoderItemWise(itemId,supplierId);
    		reorderList=FXCollections.observableArrayList(respentityReorders.getBody());
    	}
    		
    	FillTable();
       // rdrItemName.setText("");
    	//rdrSupName.setText("");
    	batch = null;
    	
    }
	@FXML

	void supplierPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			
		}
	}

	@FXML
	void itemPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			
			LoaditemPopup();

		}
	}

	@FXML
	void sdpressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			rdrEndDt.requestFocus();

		}
	}

	@FXML
	void maxqpressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			rdrsv.requestFocus();

		}
	}

	@FXML
	void mqpressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			rdrMaxQty.requestFocus();

		}
	}

	@FXML
	void edpressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			rdrMinQty.requestFocus();

		}

	}

	@FXML
	void supplierNameClicked(MouseEvent event) {
		supplierPopUp();
	}

	@Subscribe
	public void popuplistner(SupplierPopupEvent supplierEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) rdrsv1.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			supplierId = supplierEvent.getSupplierId();
			rdrSupName.setText(supplierEvent.getSupplierName());
				}
			});
		}
	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itempopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) rdrsv1.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
		
			itemId = itempopupEvent.getItemId();
			rdrItemName.setText(itempopupEvent.getItemName());
		
			
			supplierPopUp();

		}

	}
	
	

    @FXML
    void supplierNamePressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		supplierPopUp();
		}
    }

   
	@FXML
	void itemNameClicked(MouseEvent event) {
		
		//LoaditemPopup();
	}
	private void LoaditemPopup()
	{
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			ItemPopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		

		} catch (Exception e) {
			e.printStackTrace();
		}
		rdrSupName.requestFocus();
	
		
	}

	@FXML
	void rdrDelete(ActionEvent event) {
		if (null != reorderMst.getId()) {

			RestCaller.deleteReOrderMst(reorderMst.getId());
			ResponseEntity<List<ReorderMst>> respentityReorder = RestCaller.returnValueOfReoder();
			reorderList = FXCollections.observableArrayList(respentityReorder.getBody());

			rdrTable.getItems().clear();
			FillTable();
			//rdrTable.setItems(reorderList);

		}

	}

	@FXML
	void tableToDelete(MouseEvent event) {
		if (rdrTable.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = rdrTable.getSelectionModel();
			reorderResponseList = selectionModel.getSelectedItems();
			for (ReorderMst reorder : reorderResponseList) {
				reorderMst.setId(reorder.getId());

			}
		}

	}

	@FXML
	void SaveKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			SaveReorder();
			//clearfields();
		}
	}

	@FXML
	void rdrSave(ActionEvent event) {

		SaveReorder();
		//Clear();

	}

	private void Clear() {
		rdrEndDt.setValue(null);
		rdrStartDt.setValue(null);
		rdrSupName.setText("");
		rdrItemName.setText("");
		rdrMaxQty.setText("");
		rdrMinQty.setText("");

	}


	private void FillTable() {

		rdrTable.setItems(reorderList);
		for (ReorderMst r : reorderList) {
			if (null != r.getItemId()) {
				ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(r.getItemId());
				r.setItemName(respentity.getBody().getItemName());
				rdrC1.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			}
			if (null != r.getSupplierId()) {
				ResponseEntity<AccountHeads> accountHeadsResponce = RestCaller.getAccountHeadsById(r.getSupplierId());
				AccountHeads accountHeads = accountHeadsResponce.getBody();
				r.setSupplierName(accountHeads.getAccountName());
				rdrC2.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
			}
		
		}
		rdrC3.setCellValueFactory(cellData -> cellData.getValue().getStartingDateProperty());
		rdrC4.setCellValueFactory(cellData -> cellData.getValue().getEndingDateProperty());
		rdrC5.setCellValueFactory(cellData -> cellData.getValue().getMinQtyProperty());
		rdrC6.setCellValueFactory(cellData -> cellData.getValue().getMaxQtyProperty());
		
	}
	private void fillTable() {
		tab2.setItems(reOrderListTable);
		for (ItemBatchMst r : reOrderListTable) {
			if (null != r.getItemId()) {
				ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(r.getItemId());
				r.setItemName(respentity.getBody().getItemName());
				itemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			}
		}
		
		qty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
	}
	
	private void clearfields() {
		rdrItemName.setText("");
		rdrMaxQty.setText("");
		rdrMinQty.setText("");
	}

	private void supplierPopUp() {
		System.out.println("-------------showPopup-------------");
		System.out.println("-------------showPopup-------------");
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/supplierPopup.fxml"));
			Parent root = loader.load();
		
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			rdrStartDt.requestFocus();
		
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void SaveReorder() {

		reorderMst = new ReorderMst();
		boolean flag = false;

		if (rdrItemName.getText() == null || rdrItemName.getText().isEmpty()) {

			notifyMessage(5, "please enter Itemname", false);
			return;
		} else if (rdrSupName.getText() == null || rdrSupName.getText().isEmpty()) {

			notifyMessage(5, "please enter supplier name", false);
			return;
		} else if (rdrStartDt.getValue() == null) {

			notifyMessage(5, "pls enter start date", false);
			return;
		} else if (rdrEndDt.getValue() == null) {
			notifyMessage(5, "pls enter end date", false);
			return;
		} else if (rdrMaxQty.getText().trim().isEmpty()) {
			notifyMessage(5, "pls enter maximum quantity", false);
			return;
		} else if (rdrMinQty.getText().trim().isEmpty()) {

			notifyMessage(5, "pls enter minimum quantity", false);
			return;
		} else {

			Date startDate = Date.valueOf(rdrStartDt.getValue());
			reorderMst.setStartingDate(startDate);
			reorderMst.setItemId(itemId);
			reorderMst.setSupplierId(supplierId);
			Date endDate = Date.valueOf(rdrEndDt.getValue());
			reorderMst.setEndingDate(endDate);
			Double min = Double.parseDouble(rdrMinQty.getText());
	        reorderMst.setMinQty(min);
			Double max = Double.parseDouble(rdrMaxQty.getText());
			reorderMst.setMaxQty(max);
			int da=startDate.compareTo(endDate);
	
			reorderMst.setBranchCode(SystemSetting.systemBranch);
			if(da==-1 || da==0)
			{
			if(min<max )
			{
		
		    
			try {
				ResponseEntity<ReorderMst> respentity = RestCaller.saveReoder(reorderMst);
				reorderMst = respentity.getBody();
				reorderList.add(reorderMst);
			
				Clear();
				rdrItemName.requestFocus();
				
				ResponseEntity<List<ReorderMst>> respentityReorder = RestCaller.returnValueOfReoder();
				reorderList = FXCollections.observableArrayList(respentityReorder.getBody());
//
				rdrTable.getItems().clear();
				FillTable();

				
			} catch (Exception e) {
 				System.out.println("Error saving data " + e.toString());
				return;
			}
			
		

			notifyMessage(5, "Re Order Saved", false);

		}
		
		else
		{
			notifyMessage(5, " select max qty  larger than min qty", false);

		}
		}
			else
			{
				notifyMessage(5, "please select proper dates", false);
			}
		}
	}

	private boolean filedValidation() {

		if (null == rdrItemName.getText() || rdrItemName.getText().length() == 0) {
			System.out.println("Failed");

			notifyMessage(5, "Please provide Item Name", false);
			return false;
		}
		if (null == rdrStartDt.getValue()) {
			return false;
		}
		if (null == rdrEndDt.getValue()) {
			return false;
		}
		return true;
	}

	private void ReoderBySearch(String searchData) {

		rdrTable.getItems().clear();

		ResponseEntity<List<ReorderMst>> cat = RestCaller.SearchReoderByItemName(searchData);

		reorderList = FXCollections.observableArrayList(cat.getBody());
		FillTable();
	}
	

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	private void showAll()
	{
		 ResponseEntity<List<ReorderMst>> respentityReorder = RestCaller.returnValueOfReoder();
		 reorderList =  FXCollections.observableArrayList(respentityReorder.getBody());
		FillTable();

	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }

}