package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.eclipse.jdt.internal.compiler.lookup.CatchParameterBinding;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.PurchaseOrderDtl;
import com.maple.mapleclient.entity.PurchaseOrderHdr;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.AccountHeadsPopupEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class PuchaseOrderCtl {
	
	String taskid;
	String processInstanceId;
	
	
	EventBus eventBus = EventBusFactory.getEventBus();

	String supplierName = "";
	String supplierID = null;
	PurchaseOrderHdr purchaseOrderHdr = null;
	PurchaseOrderDtl purchaseOrderDtl = null;
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();
	private ObservableList<PurchaseOrderDtl> purchaseListTable = FXCollections.observableArrayList();

	StringProperty amountProperty = new SimpleStringProperty("");
	StringProperty rateProperty = new SimpleStringProperty("");
	StringProperty quantityProperty = new SimpleStringProperty("");
	StringProperty taxTateProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	/*
	 * To manage Item Serial
	 */
	StringProperty itemSerialProperty = new SimpleStringProperty("");
	StringProperty cessAmtProperty = new SimpleStringProperty("");

	/*
	 * To Enable disable Expiry date / Manu Date based on Batch
	 */
	StringProperty batchProperty = new SimpleStringProperty("");
	StringProperty taxAmountProperty = new SimpleStringProperty("");

	/*
	 * To Manage Total Amount Property
	 */

	StringProperty totalAmountProperty = new SimpleStringProperty("");
	@FXML
    private TextField txtQtyTotal;

    @FXML
    private TextField txtAmtTotal;

    @FXML
    private TextField txtGrandTotal;

    @FXML
    private Button btnSave;


    @FXML
    private TableView<PurchaseOrderDtl> tblItemDetails;

    @FXML
    private TableColumn<PurchaseOrderDtl, String> clItemName;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clQty;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clPurRate;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clTaxRate;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clamt;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clMRP;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clNetCost;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clCess;

    @FXML
    private Button btnAddItem;

    @FXML
    private Button btnDeleteItem;

    @FXML
    private TextField txtMRP;

    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtBarcode;

    @FXML
    private TextField txtQty;

    @FXML
    private TextField txtPurchseRate;

    @FXML
    private TextField txtAmount;

    @FXML
    private TextField txtItemSerial;

    @FXML
    private TextField txtCessRate;

    @FXML
    private TextField txtTaxRate;
    @FXML
    private ComboBox<String> cmbUnit;
    @FXML
    private TextField txtSupplierName;

    @FXML
    private TextField txtSupplierGst;

    @FXML
    private DatePicker dpOurVoucherDate;


    @FXML
    private TextField txtOurVoucherNo;

    @FXML
    private TextField txtInvoiceTotal;

    @FXML
    private TextField txtNarration;


    @FXML
    void AddItem(ActionEvent event) {

    	addItem();
    }

    @FXML
    void DeleteItem(ActionEvent event) {
    	if (null != purchaseOrderDtl) {
			if (null != purchaseOrderDtl.getId()) {

				RestCaller.purchaseOrderDtlDelete(purchaseOrderDtl.getId());
				/////////////////////
			
				ResponseEntity<List<PurchaseOrderDtl>> purchasetdtlSaved = RestCaller.getPurchaseOrderDtl(purchaseOrderHdr);
				purchaseListTable =FXCollections.observableArrayList(purchasetdtlSaved.getBody());
				FillTable();
			//	setTotal();
			}
		}
    }

    @FXML
    void ExpiryDateOnkeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			btnAddItem.requestFocus();
		}
    }
	@FXML
	private void initialize() {
		dpOurVoucherDate = SystemSetting.datePickerFormat(dpOurVoucherDate, "dd/MMM/yyyy");
		eventBus.register(this);
		txtAmount.textProperty().bindBidirectional(amountProperty);
		txtQty.textProperty().bindBidirectional(quantityProperty);
		txtPurchseRate.textProperty().bindBidirectional(rateProperty);
		txtTaxRate.textProperty().bindBidirectional(taxTateProperty);
		txtItemSerial.textProperty().bindBidirectional(itemSerialProperty);
		itemSerialProperty.set("1");
		txtCessRate.textProperty().bindBidirectional(cessRateProperty);
		txtInvoiceTotal.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtInvoiceTotal.setText(oldValue);
				}
			}
		});
		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});

		txtMRP.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtMRP.setText(oldValue);
				}
			}
		});

		txtPurchseRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPurchseRate.setText(oldValue);
				}
			}
		});

		txtItemSerial.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtItemSerial.setText(oldValue);
				}
			}
		});

		txtTaxRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtTaxRate.setText(oldValue);
				}
			}
		});
		txtCessRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCessRate.setText(oldValue);
				}
			}
		});
		
		amountProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0)) {

					double qty = Double.parseDouble(txtQty.getText());
					double amount = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal(amount / qty);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						rateProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		quantityProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;

				if ((txtPurchseRate.getText().length() > 0)) {
					double rate = Double.parseDouble(txtPurchseRate.getText());

					double qty = Double.parseDouble((String) newValue);
					if (rate > 0) {
						BigDecimal newAmount = new BigDecimal(qty * rate);
						newAmount = newAmount.setScale(3, BigDecimal.ROUND_CEILING);
						amountProperty.set(newAmount.toPlainString());

					}
				} else if ((txtAmount.getText().length() > 0)) {
					double amount = Double.parseDouble(txtAmount.getText());

					double qty = Double.parseDouble((String) newValue);
					if (amount > 0) {
						BigDecimal newRate = new BigDecimal(amount / qty);
						newRate = newRate.setScale(3, BigDecimal.ROUND_CEILING);
						rateProperty.set(newRate.toPlainString());

					}
				}

				if ((txtTaxRate.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					double tax = Double.parseDouble(txtTaxRate.getText());
					double rate = Double.parseDouble(txtPurchseRate.getText());
					double qty = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * tax) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						taxAmountProperty.set(newrate.toPlainString());

					}
				}

				if ((txtPurchseRate.getText().length() > 0) && txtCessRate.getText().length() > 0) {

					double rate = Double.parseDouble(txtPurchseRate.getText());
					double cess = Double.parseDouble(txtCessRate.getText());
					double qty = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * cess) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						cessAmtProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		rateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;

				if ((txtQty.getText().length() > 0)) {
					double qty = Double.parseDouble(txtQty.getText());

					double rate = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newAmount = new BigDecimal(qty * rate);
						newAmount = newAmount.setScale(3, BigDecimal.ROUND_CEILING);
						amountProperty.set(newAmount.toPlainString());

					}
				}

				if ((txtTaxRate.getText().length() > 0) && txtQty.getText().length() > 0) {

					double tax = Double.parseDouble(txtTaxRate.getText());
					double qty = Double.parseDouble(txtQty.getText());
					double rate = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * tax) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						taxAmountProperty.set(newrate.toPlainString());

					}
				}

				if ((txtQty.getText().length() > 0) && txtCessRate.getText().length() > 0) {

					double qty = Double.parseDouble(txtQty.getText());
					double cess = Double.parseDouble(txtCessRate.getText());
					double rate = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * cess) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						cessAmtProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		
		// --------------tax amt
		taxTateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					double qty = Double.parseDouble(txtQty.getText());
					double rate = Double.parseDouble(txtPurchseRate.getText());
					double tax = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * tax) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						taxAmountProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		cessRateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					double qty = Double.parseDouble(txtQty.getText());
					double rate = Double.parseDouble(txtPurchseRate.getText());
					double cess = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * cess) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						cessAmtProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		// ==========table selection
		tblItemDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					purchaseOrderDtl = new PurchaseOrderDtl();
					System.out.println("getSelectionModel--getId");

					System.out.println("DELETE24-" + newSelection.getId());

					purchaseOrderDtl.setId(newSelection.getId());
					System.out.println("DELETE--" + purchaseOrderDtl.getId());

				}
			}
		});

	}

    @FXML
    void InvcTotalOnKeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtItemName.requestFocus();
		}
    }

    @FXML
    void ItemNameOnkeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			showPopup();
			txtQty.requestFocus();
			// txtBarcode.requestFocus();
		}
    }



    @FXML
    void NarrationOnKeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		txtInvoiceTotal.requestFocus();
		}
    }

    @FXML
    void OurVoucherNoOnKeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtInvoiceTotal.requestFocus();
		}

    }

    @FXML
    void OurVouchrDateOnKeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtNarration.requestFocus();
		}
    }

    @FXML
    void PoNoOnKeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtItemName.requestFocus();
		}
    }

    @FXML
    void PurchaseRateOnKeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
    }

    @FXML
    void QtyOnKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtPurchseRate.requestFocus();
		}

    }

    @FXML
    void Save(ActionEvent event) {
    	finalSave();
    }
    private void finalSave() {
		try {

			Double invoiceAmount = 0.0;
			ResponseEntity<List<PurchaseOrderDtl>> purchasetdtlSaved = RestCaller.getPurchaseOrderDtl(purchaseOrderHdr);
			if (purchasetdtlSaved.getBody().size() == 0) {
				return;
			}
			
			purchaseOrderHdr.setSupplierId(supplierID);

			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsById(purchaseOrderHdr.getSupplierId());
			String vouchernumber = purchaseOrderHdr.getVoucherNumber();
			purchaseOrderHdr.setFinalSavedStatus("OPEN");
			if (null == vouchernumber) {
				String financialYear = SystemSetting.getFinancialYear();
				String pNo = RestCaller.getVoucherNumber(financialYear + "PO");

				purchaseOrderHdr.setVoucherNumber(pNo);

				java.util.Date date = SystemSetting.localToUtilDate(dpOurVoucherDate.getValue());
				java.sql.Date sqlDate = new java.sql.Date(date.getTime());

				purchaseOrderHdr.setVoucherDate(sqlDate);
			}
			purchaseOrderHdr.setInvoiceTotal(Double.parseDouble(txtGrandTotal.getText()));
			java.util.Date uDate = purchaseOrderHdr.getVoucherDate();
			String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
			
			
			RestCaller.updatePurchaseOrderHdr(purchaseOrderHdr);
			
			
//			RestCaller.completeTask(taskid);
			
			
			notifyMessage(5, "saved", true);
			
			//version1.7
			String vNo =purchaseOrderHdr.getVoucherNumber();
			purchaseOrderHdr = null;
			purchaseOrderDtl = null;
			purchaseListTable.clear();
			itemSerialProperty.set("1");
			tblItemDetails.getItems().clear();
			
			clearFieldshdr();
			// uDate = SystemSetting.StringToUtilDate(vDate,"yyyy-MM-dd");
			JasperPdfReportService.PurchaseOrderInvoiceReport(vNo, vDate);

			//version1.7 ends
			

			// uDate = SystemSetting.StringToUtilDate(vDate,"yyyy-MM-dd");
			//JasperPdfReportService.PurchaseOrderInvoiceReport(purchaseOrderHdr.getVoucherNumber(), vDate);

		 
			 

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    private void clearFieldshdr() {
		clearFields();
		supplierID = null;
		txtSupplierGst.setText("");
		txtSupplierName.setText("");
		txtOurVoucherNo.setText("");
		txtInvoiceTotal.setText("");
		txtNarration.setText("");
		txtQtyTotal.setText("");
		txtAmtTotal.setText("");
		txtGrandTotal.clear();

	}

    @FXML
    void SaveOnKey(KeyEvent event) {

    }

    @FXML
    void ShowItemPopup(MouseEvent event) {

    }

    @FXML
    void SupplierInvDateKeyPressed(KeyEvent event) {

    }

    @FXML
    void SupplierInvNoOnKeyPresssed(KeyEvent event) {

    }

    @FXML
    void SupplierOnKeyPressed(KeyEvent event) {

    }

    @FXML
    void addItem(KeyEvent event) {

    }

    @FXML
    void batchActon(KeyEvent event) {

    }

    @FXML
    void showPopup(MouseEvent event) {
    	System.out.println("-------------showPopup-------------");
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPartyPopUpCtl.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			dpOurVoucherDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

    }
    @Subscribe
	public void popuplistner(AccountHeadsPopupEvent accountHeadsPopupEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) btnAddItem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtSupplierGst.setText(accountHeadsPopupEvent.getPartyGst());
			txtSupplierName.setText(accountHeadsPopupEvent.getPartyName());
			supplierName = accountHeadsPopupEvent.getPartyName();
			// purchasehdr.setSupplierId(supplierEvent.getSupplierId());
			supplierID = accountHeadsPopupEvent.getId();
			System.out.println(supplierID+"supplierIDsupplierID");
			/*
			 * for some reason id is null
			 */
			if (null == supplierID) {
				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(accountHeadsPopupEvent.getPartyName());
				supplierID = accountHeads.getBody().getId();
			}
				}
			});
		}

	}
    private void clearFields() {
		txtAmount.setText("");
		txtItemName.setText("");
		txtQty.setText("");
		txtBarcode.setText("");
		txtCessRate.setText("");
		// txtFreeQty.setText("");
		txtPurchseRate.setText("");
		// txtItemSerial.setText("");
		txtTaxRate.setText("");
		// txtPreviousMRP.setText("");
		txtMRP.setText("");
		cmbUnit.getSelectionModel().clearSelection();
	}

    private void addItem() {

		if (null == purchaseOrderHdr) {

			if (null == supplierID)

			{
				notifyMessage(5, "Please select Supplier", false);
				purchaseOrderHdr = null;
				return;
			}


			if (null == dpOurVoucherDate.getValue()) {

				notifyMessage(5, "Please select voucher date", false);
				purchaseOrderHdr = null;
				return;
			}
			
			
			if (null == txtItemName.getText())

			{
				notifyMessage(5, "Please select Item", false);
				purchaseOrderHdr = null;
				return;
			}
			
			if (null == txtBarcode.getText())

			{
				notifyMessage(5, "Barcode not defined", false);
				purchaseOrderHdr = null;
				return;
			}
			
			if (null == txtQty.getText())
			{
				notifyMessage(5, "Please enter quantity", false);
				purchaseOrderHdr = null;
				return;
			}
			
			
			if (null == txtPurchseRate.getText())
			{
				notifyMessage(5, "Please enter Purchase rate", false);
				purchaseOrderHdr = null;
				return;
			}
			
			
			if (null == txtAmount.getText())
			{
				notifyMessage(5, "Amount shouldn't be a null  value", false);
				purchaseOrderHdr = null;
				return;
			}
			
			
			if (null == txtItemSerial.getText())
			{
				notifyMessage(5, "Item Serial shouldn't be a null  value", false);
				purchaseOrderHdr = null;
				return;
			}
			
			
			if (null == cmbUnit.getValue())
			{
				notifyMessage(5, "Please select the unit", false);
				purchaseOrderHdr = null;
				return;
			}
			
			
			if (null == txtCessRate.getText())
			{
				notifyMessage(5, "Cess rate shouldn't be a null  value", false);
				purchaseOrderHdr = null;
				return;
			}
			
			
			if (null == txtMRP.getText())
			{
				notifyMessage(5, "MRP shouldn't be a null  value", false);
				purchaseOrderHdr = null;
				return;
			}
			
			
			if (null == txtTaxRate.getText())
			{
				notifyMessage(5, "Tax rate shouldn't be a null  value", false);
				purchaseOrderHdr = null;
				return;
			}
//				
//				else {
			purchaseOrderHdr = new PurchaseOrderHdr();
//					purchasehdr = new PurchaseHdr();
			// supplierID was stored from subscription
			
			purchaseOrderHdr.setProcessInstanceId(processInstanceId);
			purchaseOrderHdr.setTaskId(taskid);
			
			
			purchaseOrderHdr.setSupplierId(supplierID);
			System.out.println("ddddd" + supplierID);

//					if (!(txtPaymentDue.getText().trim().isEmpty())) {
////						purchasehdr.set(txtPaymentDue.getText());
//					}
			purchaseOrderHdr.setFinalSavedStatus("OPEN");
			// purchasehdr.setPurchaseType(cmbPurchaseType.getSelectionModel().getSelectedItem());
			Date dateOurVouch = Date.valueOf(dpOurVoucherDate.getValue());
			purchaseOrderHdr.setNarration(txtNarration.getText());
			purchaseOrderHdr.setVoucherDate(dateOurVouch);
			if (!txtInvoiceTotal.getText().trim().isEmpty()) {
				purchaseOrderHdr.setInvoiceTotal(Double.parseDouble(txtInvoiceTotal.getText()));
			}
			purchaseOrderHdr.setSupplierInvDate(null);

			purchaseOrderHdr.setBranchCode(SystemSetting.getSystemBranch());

			purchaseOrderHdr.setVoucherType("PURCHASE ORDER");

			ResponseEntity<PurchaseOrderHdr> respentity = RestCaller.savePurchaseHdrOrderHdr(purchaseOrderHdr);
			purchaseOrderHdr = respentity.getBody();

//				}

		}

		/*
		 * Set PurchaseHdr to PurchaseDtl.
		 */
		if (null != purchaseOrderHdr) {
			purchaseOrderDtl = new PurchaseOrderDtl();

			if (txtItemName.getText().trim().isEmpty()) {
				notifyMessage(5, "Please select Item...!!!", false);
				return;
			}
			String ItemName = txtItemName.getText();
			ResponseEntity<ItemMst> resp = RestCaller.getItemByNameRequestParam(ItemName);
			ItemMst item = resp.getBody();

			if (null == item) {
				notifyMessage(5, "Please select Item...!!!", false);
				return;

			}
			System.out.println("-------------AddItem-------------");
			boolean valDtl = validationDtl();
			System.out.print("kkkkkkkkkkkkkkk"+valDtl);
			if (valDtl == false) {
				return;
			}

			boolean expiry = true;

			
			if (null != purchaseOrderHdr.getId()) {
				if (valDtl == true) {

					if (expiry == true) {

						purchaseOrderDtl.setPurchaseOrderHdr(purchaseOrderHdr);
						System.out.println("getItemIdgetItemId" + purchaseOrderDtl.getItemId());

						purchaseOrderDtl.setItemName(item.getItemName());
						purchaseOrderDtl.setBarcode(item.getBarCode());
						purchaseOrderDtl.setTaxRate(item.getTaxRate());
						purchaseOrderDtl.setDiscount(0.0);
						purchaseOrderDtl.setItemId(item.getId());

//				if (null != purchaseDtl.getItemId()) {

					
						if (!txtPurchseRate.getText().trim().isEmpty()) {

							purchaseOrderDtl.setNetCost(txtPurchseRate.getText());

						}
					
						
						purchaseOrderDtl.setCessRate(Double.parseDouble(txtCessRate.getText()));
						purchaseOrderDtl.setMrp(Double.parseDouble(txtMRP.getText()));
						purchaseOrderDtl.setQty(Double.parseDouble(txtQty.getText()));
						purchaseOrderDtl.setTaxRate(Double.parseDouble(txtTaxRate.getText()));
						purchaseOrderDtl.setPurchseRate(Double.parseDouble(txtPurchseRate.getText()));
						purchaseOrderDtl.setAmount(Double.parseDouble(txtAmount.getText()));
						purchaseOrderDtl.setStatus("OPEN");
						purchaseOrderDtl.setReceivedQty(0.0);
						// purchaseDtl.setUnit(unit);
						if (null == cmbUnit.getValue()) 
						{

							ResponseEntity<UnitMst> unitId = RestCaller
									.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

							purchaseOrderDtl.setUnitId(unitId.getBody().getId());
						} else {
							ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
							purchaseOrderDtl.setUnitId(getUnit.getBody().getId());
						}
						ResponseEntity<PurchaseOrderDtl> respentity = RestCaller.savePurchaseOrderDtl(purchaseOrderDtl);
						purchaseOrderDtl = respentity.getBody();
						setTotal();
						purchaseListTable.add(purchaseOrderDtl);
						itemSerialProperty.set(Integer.parseInt(itemSerialProperty.get()) + 1 + "");
						FillTable();
						clearFields();
//				} else {
//					notifyMessage(5, "Please select Item...!!!",false);
//					return;
//
//				}
					}
				}
			} else {
//			purchasehdr = null;

				return;

			}
		} else {
			return;
		}

		purchaseOrderDtl = new PurchaseOrderDtl();

		txtItemName.requestFocus();

	}
    private void FillTable() {

		tblItemDetails.setItems(purchaseListTable);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clamt.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clMRP.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clPurRate.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxAmtProperty());
		clCess.setCellValueFactory(cellData -> cellData.getValue().getCessAmtProperty());
		clNetCost.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
	}

	private void setTotal() {

		Summary sumList = new Summary();
		sumList = RestCaller.getSummaryOrderHDr(purchaseOrderHdr.getId());
	
		txtGrandTotal.setText(Double.toString(sumList.getTotalAmount() + sumList.getTotalTax()));
		txtQtyTotal.setText(Double.toString(sumList.getTotalQty()));
		txtAmtTotal.setText(Double.toString(sumList.getTotalAmount()));
	
//txtTaxSplit.setText(sumList);
	}
    public boolean validationDtl() {

		boolean flag = false;
		if (txtCessRate.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter Cess Rate", false);
			return false;

		} else if (txtMRP.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter MRP", false);
			return false;
		} else if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Quantity", false);
			return false;
		} else if (txtTaxRate.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter TaxRate", false);
			return false;

		} else if (txtPurchseRate.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter purchase Rate", false);
			return false;

		} else {

			flag = true;

		}

		return flag;
	}
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}


    private void showPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    @Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

    	try {
			
		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAddItem.getScene().getWindow();
		if (stage.isShowing()) {
			cmbUnit.getItems().clear();
			txtItemName.setText(itemPopupEvent.getItemName());
			txtBarcode.setText(itemPopupEvent.getBarCode());


			txtTaxRate.setText(Double.toString(itemPopupEvent.getTaxRate()));
			txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));
			txtCessRate.setText(Double.toString(itemPopupEvent.getCess()));
			cmbUnit.getItems().add(itemPopupEvent.getUnitName());
			//cmbUnit.setValue(itemPopupEvent.getUnitName());
			ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller.getMultiUnitByItemId(itemPopupEvent.getItemId());

			multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());

			if (!multiUnitList.isEmpty()) {

				for (MultiUnitMst multiUniMst : multiUnitList) {

					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
					cmbUnit.getItems().add(getUnit.getBody().getUnitName());
				}

			} else {
//				cmbUnit.getItems().clear();
//				cmbUnit.getItems().add(itemPopupEvent.getUnitName());
//				cmbUnit.setPromptText(itemPopupEvent.getUnitName());
				//cmbUnit.setValue(itemPopupEvent.getUnitName());
			}
		}
	
    	
		} catch (Exception e) {
			// TODO: handle exception
		}
    }
    
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


    private void PageReload() {
    	
    	
		
	}
    
}
