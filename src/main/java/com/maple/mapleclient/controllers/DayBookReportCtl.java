package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import net.sf.jasperreports.engine.JRException;

public class DayBookReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<DayBook> DayBookList = FXCollections.observableArrayList();
    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnPrintReport;
    @FXML
    private TextField txtTotalDebit;

    @FXML
    private TextField txtTotalCredit;

    @FXML
    private TextField txtcashBalance;
    @FXML
    private TableView<DayBook> tbDayBook;
    @FXML
    private TableColumn<DayBook, String> clVoucherNo;

    @FXML
    private TableColumn<DayBook, LocalDate> clVoucherDate;

    @FXML
    private TableColumn<DayBook, String> clVoucherType;

    @FXML
    private TableColumn<DayBook, String> clNarration;

    @FXML
    private TableColumn<DayBook, Number> clDebitAmt;

    @FXML
    private TableColumn<DayBook, Number> clCreditAmt;

    @FXML
    private TableColumn<DayBook, String> clDebitName;

    @FXML
    private TableColumn<DayBook, String> clCreditAcName;

    @FXML
    private Button btnShowReport;
    
    @FXML
   	private void initialize() {
       	
       	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    }
    @FXML
    void ShowReport(ActionEvent event) {
    	

		DayBookList.clear();
		tbDayBook.getItems().clear();
		LocalDate dpDate1 = dpDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<DayBook>> dayBookSaved = RestCaller.getDayBookReport(SystemSetting.systemBranch,strDate);
    	double debit= 0.0,credit = 0.0;
    	DayBookList = FXCollections.observableArrayList(dayBookSaved.getBody());
    	for(DayBook dayBook: DayBookList)
    	{
    		if(null == dayBook.getCrAmount())
    			dayBook.setCrAmount(0.0);
    		if(null == dayBook.getDrAmount())
    				dayBook.setDrAmount(0.0);
    		credit = credit+dayBook.getCrAmount();
    		debit = debit+dayBook.getDrAmount();
    	}
    	BigDecimal totalcredit = new BigDecimal(credit);
    	totalcredit = totalcredit.setScale(0, BigDecimal.ROUND_HALF_EVEN);
    	txtTotalCredit.setText(totalcredit.toPlainString());
    	BigDecimal totaldebit = new BigDecimal(debit);
    	totaldebit = totaldebit.setScale(0, BigDecimal.ROUND_HALF_EVEN);
    	txtTotalDebit.setText(totaldebit.toPlainString());
    	txtcashBalance.setText(Double.toString((Double.parseDouble(txtTotalDebit.getText())-Double.parseDouble(txtTotalCredit.getText()))));
    	fillTable();
    	
    	
    }
    @FXML
    void PrintReport(ActionEvent event) {
    	LocalDate dpDate1 = dpDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		try {
			JasperPdfReportService.DayBookReport(SystemSetting.systemBranch, strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	

    }
    private void fillTable()
    {
    	tbDayBook.setItems(DayBookList);
		clCreditAcName.setCellValueFactory(cellData -> cellData.getValue().getcrAccountNameProperty());
		clCreditAmt.setCellValueFactory(cellData -> cellData.getValue().getcrAmountProperty());
		clDebitAmt.setCellValueFactory(cellData -> cellData.getValue().getdrAmountProperty());
		clDebitName.setCellValueFactory(cellData -> cellData.getValue().getdrAccountNameProperty());
		clNarration.setCellValueFactory(cellData -> cellData.getValue().getnarrationProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getsourceVoucherDateProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getsourceVoucherNumberProperty());
		clVoucherType.setCellValueFactory(cellData -> cellData.getValue().getsourceVoucherTypeProperty());
		
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }
}
