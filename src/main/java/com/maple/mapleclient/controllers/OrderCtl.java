package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class OrderCtl {
	String taskid;
	String processInstanceId;

	@FXML
	private AnchorPane a2;

	@FXML
	private TextField txtItemname1;

	@FXML
	private TextField txtRate1;

	@FXML
	private TextField txtTax1;

	@FXML
	private TextField txtItemcode1;

	@FXML
	private Button btnAdditem1;

	@FXML
	private TextField txtBarcode1;

	@FXML
	private TextField txtQty1;

	@FXML
	private TextField txtBatch1;

	@FXML
	private TableView<?> itemDetailTable;

	@FXML
	private TableColumn<?, ?> columnItemName;

	@FXML
	private TableColumn<?, ?> columnBarCode;

	@FXML
	private TableColumn<?, ?> columnQty;

	@FXML
	private TableColumn<?, ?> columnTaxRate;

	@FXML
	private TableColumn<?, ?> columnMrp;

	@FXML
	private TableColumn<?, ?> columnBatch;

	@FXML
	private TableColumn<?, ?> columnCessRate;

	@FXML
	private TableColumn<?, ?> columnUnitName;

	@FXML
	private TableColumn<?, ?> columnExpiryDate;

	@FXML
	private TextField txtSBICard;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private TextField txtSodexoCard;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtYesCard;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay1;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private AnchorPane a1;

	@FXML
	private TextField custordername;

	@FXML
	private TextField convertedinvoice;

	@FXML
	private TextField orderadvice;

	@FXML
	private Button address;

	@FXML
	private Button gstno;

	@FXML
	private TextField convertedinvoicedate;

	@FXML
	private TextField targetdept;

	@FXML
	private TextField msgtoprint;

	@FXML
	private TextField txtSBICard1;

	@FXML
	private TextField txtcardAmount1;

	@FXML
	private TextField txtSodexoCard1;

	@FXML
	private TextField txtSodexoCard11;

	@FXML
	void CustomerPopUp(MouseEvent event) {

	}

	@FXML
	void addItemButtonClick(ActionEvent event) {

	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {

	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {

	}

	@FXML
	void onEnterCustPopup(KeyEvent event) {

	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {

	}

	@FXML
	void save(ActionEvent event) {

	}

	@FXML
	void toPrintChange(KeyEvent event) {

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
