package com.maple.mapleclient.controllers;

import java.io.DataOutput;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountPayable;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.ReorderMst;
import com.maple.mapleclient.entity.SchEligiAttrListDef;
import com.maple.mapleclient.entity.SchEligibilityAttribInst;
import com.maple.mapleclient.entity.SchOfferAttrInst;
import com.maple.mapleclient.entity.SchOfferAttrListDef;
import com.maple.mapleclient.entity.SchSelectAttrListDef;
import com.maple.mapleclient.entity.SchSelectionAttribInst;
import com.maple.mapleclient.entity.SchemeInstance;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.ItemBatchEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

public class SchemeDetailsCtl {
	String taskid;
	String processInstanceId;

	ItemPopupCtl itemPopupCtl = new ItemPopupCtl();
	EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<SchemeInstance> SchemeInstanceList = FXCollections.observableArrayList();
	private ObservableList<SchSelectAttrListDef> schSelectAttrListDefList = FXCollections.observableArrayList();
	private ObservableList<SchEligiAttrListDef> eligibilityAttrList = FXCollections.observableArrayList();
	private ObservableList<SchOfferAttrListDef> schOfferAttrListDefList = FXCollections.observableArrayList();
	SchEligibilityAttribInst schEligibilityAttribInst = null;
	SchemeInstance schemeInstance = null;

	@FXML
	private Button btnShowAll;

	private ObservableList<SchOfferAttrInst> schemeOfferList = FXCollections.observableArrayList();
	private ObservableList<SchSelectionAttribInst> schemeSelectionList = FXCollections.observableArrayList();

	private ObservableList<SchEligibilityAttribInst> schemeList = FXCollections.observableArrayList();

	@FXML
	private ComboBox<String> cmbSchemeName;

	@FXML
	private ComboBox<String> cmbAttributeType;

	@FXML
	private ComboBox<String> cmbAttributeName;

	@FXML
	private TextField txtAttributeValue;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnDelete;

	@FXML
	private TableView<SchEligibilityAttribInst> tblSchemeDtls;

	@FXML
	private TableColumn<SchEligibilityAttribInst, String> clAttributeType;

	@FXML
	private TableColumn<SchEligibilityAttribInst, String> clAttributeName;

	@FXML
	private TableColumn<SchEligibilityAttribInst, String> clAttributeValue;

	@FXML
	private Button FinalSave;

	@FXML
	void ShowAll(ActionEvent event) {

		ShowAllInstance();
	}

	private void ShowAllInstance() {

		if (null == cmbSchemeName.getValue()) {
			notifyMessage(5, "Please Select Scheme", false);
			return;
		}

		schemeInstance = new SchemeInstance();
		ResponseEntity<SchemeInstance> schemeInstanceSaved = RestCaller.getSchemeInstance(cmbSchemeName.getValue());
		schemeInstance = schemeInstanceSaved.getBody();

		ResponseEntity<List<SchEligibilityAttribInst>> eligibilityInstResp = RestCaller
				.getSchEligibAttrInstByEligibIdAndSchemeId(schemeInstance.getEligibilityId(),schemeInstance.getId());
		schemeList = FXCollections.observableArrayList(eligibilityInstResp.getBody());

		for (SchEligibilityAttribInst eligibility : schemeList) {
			eligibility.setAttributeType("ELIGIBILITY CRITERIA");
		}

		ResponseEntity<List<SchSelectionAttribInst>> selectionInstResp = RestCaller
				.getSchSelectionAttribInstSelectionIDAndBySchemeId(schemeInstance.getId(),schemeInstance.getSelectionId());
		schemeSelectionList = FXCollections.observableArrayList(selectionInstResp.getBody());

		for (SchSelectionAttribInst selection : schemeSelectionList) {
			SchEligibilityAttribInst eligibilityAttribInst = new SchEligibilityAttribInst();
			eligibilityAttribInst.setAttributeName(selection.getAttributeName());
			eligibilityAttribInst.setId(selection.getId());
			eligibilityAttribInst.setAttributeValue(selection.getAttributeValue());
			eligibilityAttribInst.setAttributeType("SELECTION CRITERIA");

			schemeList.add(eligibilityAttribInst);
		}

		ResponseEntity<List<SchOfferAttrInst>> offerInstResp = RestCaller
				.getSchOfferAttrInstBySchemeIdAndOfferId(schemeInstance.getId(),schemeInstance.getOfferId());
		schemeOfferList = FXCollections.observableArrayList(offerInstResp.getBody());

		for (SchOfferAttrInst offer : schemeOfferList) {
			SchEligibilityAttribInst eligibilityAttribInst = new SchEligibilityAttribInst();
			eligibilityAttribInst.setAttributeName(offer.getAttributeName());
			eligibilityAttribInst.setId(offer.getId());
			eligibilityAttribInst.setAttributeValue(offer.getAttributeValue());
			eligibilityAttribInst.setAttributeType("OFFER DETAILS");

			schemeList.add(eligibilityAttribInst);
		}

		FillTable();

	}

	@FXML
	void AddSchemeDtls(ActionEvent event) {

		if (null == cmbSchemeName.getValue()) {
			notifyMessage(5, "Please Select Scheme", false);
			return;
		}
		if (null == cmbAttributeType.getValue()) {

			notifyMessage(5, "Please Select Attribute type", false);
			return;
		}
		if (null == cmbAttributeName.getValue()) {
			notifyMessage(5, "Please Select Attribute Name", false);
			return;
		}
		if (txtAttributeValue.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter attribute value", false);
			return;
		}

		schemeInstance = new SchemeInstance();
		ResponseEntity<SchemeInstance> schemeInstanceSaved = RestCaller.getSchemeInstance(cmbSchemeName.getValue());
		schemeInstance = schemeInstanceSaved.getBody();
		String attributeValue = txtAttributeValue.getText();

		if (cmbAttributeType.getValue().equalsIgnoreCase("SELECTION CRITERIA")) {
			ResponseEntity<SchSelectAttrListDef> SchSelectAttrListDefResp = RestCaller
					.getSchSelectionAttributeNameByName(cmbAttributeName.getValue());
			SchSelectAttrListDef schSelectAttrListDef = SchSelectAttrListDefResp.getBody();

			if (null != schSelectAttrListDef) {

				if (schSelectAttrListDef.getAttrib_type().equalsIgnoreCase("DATE")) {
					boolean validDate = checkValidDate(attributeValue);
					if (!validDate) {
						notifyMessage(5, "Please enter valid date(dd-MM-yyyy)", false);
						return;
					}

				}
				if (schSelectAttrListDef.getAttrib_type().equalsIgnoreCase("DAY")) {

					boolean validDay = checkValidDay(attributeValue);

					if (!validDay) {
						notifyMessage(5, "Please enter valid day", false);
						return;
					}

				}

				if (schSelectAttrListDef.getAttrib_type().equalsIgnoreCase("MONTH")) {
					boolean validMonth = checkValidMonth(attributeValue);
					if (!validMonth) {
						notifyMessage(5, "Please enter valid month", false);
						return;
					}

				}

				SchSelectionAttribInst schSelectionAttribInst = new SchSelectionAttribInst();
				
				
				String voucherNumber =  RestCaller.getVoucherNumber("SSAI");
				
				schSelectionAttribInst.setId(voucherNumber);
				schSelectionAttribInst.setBranchCode(SystemSetting.systemBranch);
				schSelectionAttribInst.setAttributeValue(attributeValue);
				schSelectionAttribInst.setAttributeName(schSelectAttrListDef.getAttribName());
				schSelectionAttribInst.setAttributeType(schSelectAttrListDef.getAttrib_type());
				schSelectionAttribInst.setSchemeId(schemeInstance.getId());
				schSelectionAttribInst.setSelectionId(schSelectAttrListDef.getSelectionId());

				ResponseEntity<SchSelectionAttribInst> savedSchSelectionAttribInst = RestCaller
						.saveSchSelectionAttribInst(schSelectionAttribInst);
				schSelectionAttribInst = savedSchSelectionAttribInst.getBody();
				if (null != schSelectionAttribInst) {
					notifyMessage(5, "Successfully saved Selection Attribute Instance", true);
					schEligibilityAttribInst = new SchEligibilityAttribInst();
					schEligibilityAttribInst.setAttributeName(schSelectionAttribInst.getAttributeName());
					schEligibilityAttribInst.setAttributeType("SELECTION CRITERIA");
					schEligibilityAttribInst.setAttributeValue(schSelectionAttribInst.getAttributeValue());
					schEligibilityAttribInst.setId(schSelectionAttribInst.getId());
					schEligibilityAttribInst.setInstanceName("SchSelectionAttribInst");
					schemeList.add(schEligibilityAttribInst);
					FillTable();
					clearFields();
				}
			}
		}

		if (cmbAttributeType.getValue().equalsIgnoreCase("OFFER DETAILS")) {
			ResponseEntity<SchOfferAttrListDef> schOfferAttrListDefResp = RestCaller
					.getSchemeOfferAttributeListDefByName(cmbAttributeName.getValue());
			SchOfferAttrListDef schOfferAttrListDef = schOfferAttrListDefResp.getBody();
			if (null != schOfferAttrListDef) {

				if (schOfferAttrListDef.getAttribType().equalsIgnoreCase("DOUBLE")) {
					Boolean validDouble = checkValidDoubleValue(attributeValue);
					if (!validDouble) {
						notifyMessage(5, "Please enter double value", false);
						return;
					}
				}

				if (schOfferAttrListDef.getAttribType().equalsIgnoreCase("BOOLEAN")) {
					Boolean validBool = checkBoolean(attributeValue);
					if (!validBool) {
						notifyMessage(5, "Please enter YES/NO", false);
						return;
					}
				}
				if (schOfferAttrListDef.getAttribType().equalsIgnoreCase("STRING")) {
					if (schOfferAttrListDef.getAttribName().equalsIgnoreCase("ITEMNAME")) {
						Boolean validItemName = checkItem(attributeValue);

						if (!validItemName) {
							notifyMessage(5, "Please select valid item", false);
							return;
						}
					}
					
					if (schOfferAttrListDef.getAttribType().equalsIgnoreCase("BATCH_CODE")) {

							Boolean validBatch = checkBatchCode(attributeValue);

							if (!validBatch) {
								notifyMessage(5, "Invalid batch_code", false);
								return;
							}
						}
					
				}

				

				SchOfferAttrInst schOfferAttrInst = new SchOfferAttrInst();
				
				
				String voucherNumber =  RestCaller.getVoucherNumber("SOAI");
				
				schOfferAttrInst.setId(voucherNumber);
				
				schOfferAttrInst.setAttributeValue(attributeValue);
				schOfferAttrInst.setAttributeName(schOfferAttrListDef.getAttribName());
				schOfferAttrInst.setAttributeType(schOfferAttrListDef.getAttribType());
				schOfferAttrInst.setBranchCode(SystemSetting.systemBranch);
				schOfferAttrInst.setSchemeId(schemeInstance.getId());
				schOfferAttrInst.setOfferId(schOfferAttrListDef.getOfferId());

				ResponseEntity<SchOfferAttrInst> savedSchOfferAttrInst = RestCaller
						.saveSchOfferAttrInst(schOfferAttrInst);
				schOfferAttrInst = savedSchOfferAttrInst.getBody();
				if (null != schOfferAttrInst) {
					notifyMessage(5, "Successfully saved Offer Attribute Instance", true);

					schEligibilityAttribInst = new SchEligibilityAttribInst();
					schEligibilityAttribInst.setAttributeName(schOfferAttrInst.getAttributeName());
					schEligibilityAttribInst.setAttributeType("OFFER DETAILS");
					schEligibilityAttribInst.setAttributeValue(schOfferAttrInst.getAttributeValue());
					schEligibilityAttribInst.setId(schOfferAttrInst.getId());
					schEligibilityAttribInst.setInstanceName("SchOfferAttrInst");
					schemeList.add(schEligibilityAttribInst);
					FillTable();
					clearFields();
				}

			}
		}

		if (cmbAttributeType.getValue().equalsIgnoreCase("ELIGIBILITY CRITERIA")) {
			ResponseEntity<SchEligiAttrListDef> SchEligiAttrListDefResp = RestCaller
					.getSchEligibilityAttrListDefByName(cmbAttributeName.getValue(), schemeInstance.getEligibilityId());
			SchEligiAttrListDef schEligiAttrListDef = SchEligiAttrListDefResp.getBody();
			if (null != schEligiAttrListDef) {
				if (schEligiAttrListDef.getAttribType().equalsIgnoreCase("DOUBLE")) {
					Boolean validDouble = checkValidDoubleValue(attributeValue);
					if (!validDouble) {
						notifyMessage(5, "Please enter double value", false);
						return;
					}
				}

				if (schEligiAttrListDef.getAttribType().equalsIgnoreCase("STRING")) {

					if (schEligiAttrListDef.getAttribName().equalsIgnoreCase("ITEMNAME")) {
						Boolean validItemName = checkItem(attributeValue);

						if (!validItemName) {
							notifyMessage(5, "Invalid item", false);
							return;
						}
					}

					if (schEligiAttrListDef.getAttribName().equalsIgnoreCase("CATEGORY")) {
						Boolean validCategory = checkCategory(attributeValue);

						if (!validCategory) {
							notifyMessage(5, "Invalid category", false);
							return;
						}
					}
					
					if (schEligiAttrListDef.getAttribName().equalsIgnoreCase("BATCH_CODE")) {

						Boolean validBatch = checkBatchCode(attributeValue);

						if (!validBatch) {
							notifyMessage(5, "Invalid batch_code", false);
							return;
						}
					}

				}

				schEligibilityAttribInst = new SchEligibilityAttribInst();
				String voucherNumber =  RestCaller.getVoucherNumber("SEAI");
				schEligibilityAttribInst.setId(voucherNumber);
				schEligibilityAttribInst.setBranchCode(SystemSetting.systemBranch);
				schEligibilityAttribInst.setAttributeName(schEligiAttrListDef.getAttribName());
				schEligibilityAttribInst.setAttributeType(schEligiAttrListDef.getAttribType());
				schEligibilityAttribInst.setAttributeValue(attributeValue);
				schEligibilityAttribInst.setSchemeId(schemeInstance.getId());
				schEligibilityAttribInst.setEligibilityId(schEligiAttrListDef.getEligibilityId());

				ResponseEntity<SchEligibilityAttribInst> savedSchEligibilityAttrInst = RestCaller
						.saveSchEligibilityAttrInst(schEligibilityAttribInst);
				schEligibilityAttribInst = savedSchEligibilityAttrInst.getBody();
				if (null != schEligibilityAttribInst) {
					notifyMessage(5, "Successfully saved Eligibility Attribute Instance", true);

					schEligibilityAttribInst.setInstanceName("SchEligibilityAttribInst");
					schEligibilityAttribInst.setAttributeType("ELIGIBILITY CRITERIA");
					schemeList.add(schEligibilityAttribInst);
					FillTable();

					clearFields();
				}

			}
		}

	}

	private Boolean checkBatchCode(String attributeValue) {
		
		ItemMst itemMst = new ItemMst();
		
		if (cmbAttributeType.getValue().equals("ELIGIBILITY CRITERIA")) {

			itemMst = checkEligibilityAttrInst();
			
		}
		
		if (cmbAttributeType.getValue().equals("OFFER DETAILS")) {

			itemMst = checkOfferAttrbInst();
		}
		
		if(null == itemMst)
		{
			return false;
		}
		
		ResponseEntity<ItemBatchMst> itemBatchResp = RestCaller.getItemBatchMstByItemIdAndBatch(attributeValue,itemMst.getId());
		ItemBatchMst itemBatchMst = itemBatchResp.getBody();
		if(null != itemBatchMst)
		{
			return true;
		}
		
		return false;
	}

	private Boolean checkCategory(String attributeValue) {
		ResponseEntity<CategoryMst> categoryMstResp = RestCaller.getCategoryByName(attributeValue);
		CategoryMst categoryMst = categoryMstResp.getBody();
		if (null != categoryMst) {
			return true;
		} else {
			return false;
		}
	}

	private void clearFields() {
		txtAttributeValue.clear();
		cmbAttributeName.getSelectionModel().clearSelection();
//		cmbAttributeType.getSelectionModel().clearSelection();

	}

	private Boolean checkItem(String attributeValue) {

		ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(attributeValue);
		ItemMst itemMst = itemResp.getBody();
		if (null != itemMst) {
			return true;
		} else {
			return false;
		}
	}

	private Boolean checkBoolean(String attributeValue) {
		if (attributeValue.equalsIgnoreCase("YES")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("NO")) {
			return true;
		} else {
			return false;
		}
	}

	private Boolean checkValidDoubleValue(String attributeValue) {

		try {
			Double value = Double.parseDouble(attributeValue);
			return true;

		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}

	private boolean checkValidMonth(String attributeValue) {

		if (attributeValue.equalsIgnoreCase("JANUARY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("FEBRUARY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("MARCH")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("APRIL")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("MAY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("JUNE")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("JULY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("AUGUST")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("SEPTEMBER")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("OCTOBER")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("NOVEMBER")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("DECEMBER")) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkValidDate(String attributeValue) {
		try {

			java.util.Date udate = new SimpleDateFormat("dd-MM-yyyy").parse(attributeValue);
			System.out.println("Scheme------------------------");
			System.out.println(udate);
			return true;
		} catch (Exception e) {
			// TODO: handle exception

			return false;
		}
	}

	private boolean checkValidDay(String attributeValue) {

		if (attributeValue.equalsIgnoreCase("SUNDAY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("MONDAY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("TUESDAY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("WEDNESDAY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("THURSDAY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("FRIDAY")) {
			return true;
		} else if (attributeValue.equalsIgnoreCase("SATURDAY")) {
			return true;
		} else {

			return false;

		}

	}

	@FXML
	void DleteSchemeDtls(ActionEvent event) {
		if (null != schEligibilityAttribInst) {
			if (null != schEligibilityAttribInst.getId()) {

				String attributeType = schEligibilityAttribInst.getAttributeType();
				if (attributeType.equalsIgnoreCase("SELECTION CRITERIA")) {
					RestCaller.deleteSchSelectionAttribInst(schEligibilityAttribInst.getId());

				}
				if (attributeType.equalsIgnoreCase("ELIGIBILITY CRITERIA")) {
					RestCaller.deleteSchEligibilityAttribInst(schEligibilityAttribInst.getId());

				}

				if (attributeType.equalsIgnoreCase("OFFER DETAILS")) {
					RestCaller.deleteSchOfferAttrInst(schEligibilityAttribInst.getId());

				}

				ShowAllInstance();
			}
		}

	}

	@FXML
	void FinalSave(ActionEvent event) {
		
		if (null != cmbSchemeName.getValue()) {
			schemeInstance = new SchemeInstance();
			ResponseEntity<SchemeInstance> schemeInstanceSaved = RestCaller.getSchemeInstance(cmbSchemeName.getValue());
			schemeInstance = schemeInstanceSaved.getBody();
			
			if(null == schemeInstance)
			{
				return;
			}
			
			RestCaller.updateSchemeInstance(schemeInstance.getId());
			
			schemeInstance = null;
			tblSchemeDtls.getItems().clear();
			schEligibilityAttribInst = null;
		}
		

	}

	@FXML
	private void initialize() {

		eventBus.register(this);

		cmbAttributeType.getItems().add("SELECTION CRITERIA");
		cmbAttributeType.getItems().add("ELIGIBILITY CRITERIA");
		cmbAttributeType.getItems().add("OFFER DETAILS");

		setSchemeDetails();

		tblSchemeDtls.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					schEligibilityAttribInst = new SchEligibilityAttribInst();
					schEligibilityAttribInst.setId(newSelection.getId());
					System.out.println(newSelection.getId());
					schEligibilityAttribInst.setAttributeType(newSelection.getAttributeType());
					System.out.println(newSelection.getAttributeType());

				}
			}
		});

	}

	private void setSchemeDetails() {
		cmbSchemeName.getItems().clear();

		ResponseEntity<List<SchemeInstance>> schemeinstancesaved = RestCaller.getAllSchemeInstance();
		SchemeInstanceList = FXCollections.observableArrayList(schemeinstancesaved.getBody());
		for (SchemeInstance scheme : SchemeInstanceList) {
			if (null != scheme.getId() && null != scheme.getSchemeName()) {
				cmbSchemeName.getItems().add((String) scheme.getSchemeName());
			}
		}

	}

	@FXML
	void SetSchemeName(MouseEvent event) {

		setSchemeDetails();

	}

	@FXML
	void SetAttributeType(MouseEvent event) {

	}

	@FXML
	void OnItemNameSelection(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (null != cmbAttributeName.getValue()) {
				if (cmbAttributeName.getValue().equals("ITEMNAME")) {

					showItemPopup();
				}

				if (cmbAttributeName.getValue().equals("CATEGORY")) {

					showCategoryPopup();
				}
				if (cmbAttributeName.getValue().equals("BATCH_CODE")) {
					
					ItemMst itemMst = new ItemMst();
					
					if (cmbAttributeType.getValue().equals("ELIGIBILITY CRITERIA")) {

						itemMst = checkEligibilityAttrInst();
					}
					
					if (cmbAttributeType.getValue().equals("OFFER DETAILS")) {

						itemMst = checkOfferAttrbInst();
					}

					if(null != itemMst)
					{
						showItemBatchPopup(itemMst.getId());
					}
						
					
					
				}
			}
		}
	}

	private ItemMst checkOfferAttrbInst() {
		
		if (null != cmbSchemeName.getValue()) {
			schemeInstance = new SchemeInstance();
			ResponseEntity<SchemeInstance> schemeInstanceSaved = RestCaller.getSchemeInstance(cmbSchemeName.getValue());
			schemeInstance = schemeInstanceSaved.getBody();
			
			if(null == schemeInstance)
			{
				return null;
			}
		}
		
		ResponseEntity<SchOfferAttrInst> offerInstResp = RestCaller
				.SchOfferAttrInstByAttrNameAndSchemeId("ITEMNAME", schemeInstance.getId());
	
		SchOfferAttrInst offerInst = offerInstResp.getBody();
		
		if (null == offerInst) {
			notifyMessage(5, "first add attibute ITEM NAME", false);
			return null;
		}
		
		ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(offerInst.getAttributeValue());
		ItemMst itemMst = itemResp.getBody();
		return itemMst;
	}

	private ItemMst checkEligibilityAttrInst() {
		
		if (null != cmbSchemeName.getValue()) {
			schemeInstance = new SchemeInstance();
			ResponseEntity<SchemeInstance> schemeInstanceSaved = RestCaller.getSchemeInstance(cmbSchemeName.getValue());
			schemeInstance = schemeInstanceSaved.getBody();
			
			if(null == schemeInstance)
			{
				return null;
			}
		}
		
		ResponseEntity<SchEligibilityAttribInst> eligibInstResp = RestCaller
				.SchEligibilityAttribInstByAttribName("ITEMNAME", schemeInstance.getId());
	
		SchEligibilityAttribInst eligibInst = eligibInstResp.getBody();
		
		if (null == eligibInst) {
			notifyMessage(5, "first add attibute ITEM NAME", false);
			return null;
		}
		
		ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(eligibInst.getAttributeValue());
		ItemMst itemMst = itemResp.getBody();
		return itemMst;
		
	}

	@FXML
	void SetAttributeName(MouseEvent event) {

		if (null != cmbSchemeName.getValue()) {
			schemeInstance = new SchemeInstance();
			ResponseEntity<SchemeInstance> schemeInstanceSaved = RestCaller.getSchemeInstance(cmbSchemeName.getValue());
			schemeInstance = schemeInstanceSaved.getBody();
		}

		if (null != schemeInstance) {

			if (cmbAttributeType.getValue().equals("ELIGIBILITY CRITERIA")) {

				setEligibilityAttrNames(schemeInstance);
			}

			if (cmbAttributeType.getValue().equals("SELECTION CRITERIA")) {

				setSelectionAttrNames(schemeInstance);

			}

			if (cmbAttributeType.getValue().equals("OFFER DETAILS")) {

				setOfferAttribName(schemeInstance);

			}
		}

	}

	private void setOfferAttribName(SchemeInstance schemeInst) {
		cmbAttributeName.getItems().clear();
		ResponseEntity<List<SchOfferAttrListDef>> offerSaved = RestCaller
				.getOfferAttributeNameByOfferId(schemeInst.getOfferId());

		schOfferAttrListDefList = FXCollections.observableArrayList(offerSaved.getBody());

		for (SchOfferAttrListDef offer : schOfferAttrListDefList) {
			if (null != offer.getId() && null != offer.getAttribName()) {

				ResponseEntity<SchOfferAttrInst> offerResp = RestCaller
						.SchOfferAttrInstByAttrNameAndSchemeIdAndOfferId(offer.getAttribName(), schemeInst.getId(),schemeInst.getOfferId());
				if (null == offerResp.getBody()) {
					cmbAttributeName.getItems().add((String) offer.getAttribName());
				}

			}
		}
	}

	private void setSelectionAttrNames(SchemeInstance schemeInsta) {
		cmbAttributeName.getItems().clear();
		ResponseEntity<List<SchSelectAttrListDef>> savedDetails = RestCaller
				.getSelectionAttributeBySelectionId(schemeInsta.getSelectionId());

		schSelectAttrListDefList = FXCollections.observableArrayList(savedDetails.getBody());

		for (SchSelectAttrListDef selection : schSelectAttrListDefList) {
			if (null != selection.getId() && null != selection.getAttribName()) {

				ResponseEntity<SchSelectionAttribInst> selectionResp = RestCaller
						.SchSelectionAttribInstByAttrNameAndSchemeIdAndSelectionId(selection.getAttribName(), schemeInsta.getId(),schemeInsta.getSelectionId());
				if (null == selectionResp.getBody()) {
					cmbAttributeName.getItems().add((String) selection.getAttribName());
				}
			}
		}

	}
	private void setEligibilityAttrNames(SchemeInstance schemeInst) {

		cmbAttributeName.getItems().clear();

		ResponseEntity<List<SchEligiAttrListDef>> eligibilitySaved = RestCaller
				.getEligibilityAttributeNameByEligibilityId(schemeInst.getEligibilityId());

		eligibilityAttrList = FXCollections.observableArrayList(eligibilitySaved.getBody());

		for (SchEligiAttrListDef eligibility : eligibilityAttrList) {
			if (null != eligibility.getId() && null != eligibility.getAttribName()) {

				ResponseEntity<SchEligibilityAttribInst> eligibilityInstResp = RestCaller
						.SchEligibilityAttribInstByAttribNameAndEligibilityId(eligibility.getAttribName(), schemeInst.getId(),schemeInst.getEligibilityId());
				if (null == eligibilityInstResp.getBody()) {
					cmbAttributeName.getItems().add((String) eligibility.getAttribName());
				}
			}
		}

	}

	private void FillTable() {

		tblSchemeDtls.setItems(schemeList);
		clAttributeValue.setCellValueFactory(cellData -> cellData.getValue().getAttributeValueProperty());
		clAttributeType.setCellValueFactory(cellData -> cellData.getValue().getAttributeTypeProperty());
		clAttributeName.setCellValueFactory(cellData -> cellData.getValue().getAttributeNameProperty());

	}

	private void getAllSchemeDetails() {

		if (null != cmbSchemeName.getValue()) {
			schemeInstance = new SchemeInstance();
			ResponseEntity<SchemeInstance> schemeInstanceSaved = RestCaller.getSchemeInstance(cmbSchemeName.getValue());
			schemeInstance = schemeInstanceSaved.getBody();
		}

		if (null != schemeInstance) {
			ResponseEntity<List<SchEligibilityAttribInst>> schemeSaved = RestCaller
					.getAllSchemeDtls(schemeInstance.getId());
			schemeList = FXCollections.observableArrayList(schemeSaved.getBody());

//			FillTable();

		}

	}

	private void showItemPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			btnAdd.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void showItemBatchPopup(String itemId) {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemBatchPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			ItemBatchPopupCtl popupctl = loader.getController();
			popupctl.itemBatchMstPopupByItemId(itemId);
			
			
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			btnAdd.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	private void showCategoryPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			btnAdd.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtAttributeValue.setText(itemPopupEvent.getItemName());

		}
	}

	@Subscribe
	public void CategoryPopuplistner(CategoryEvent categoryEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtAttributeValue.setText(categoryEvent.getCategoryName());
		}
	}
	
	@Subscribe
	public void itemBatchPopuplistner(ItemBatchEvent itemBatchEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtAttributeValue.setText(itemBatchEvent.getBatchCode());
		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload(hdrId);
			}


		private void PageReload(String hdrId) {

		}

}
