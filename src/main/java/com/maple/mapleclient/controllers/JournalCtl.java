package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.JournalHdr;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountEventCr;
import com.maple.mapleclient.events.AccountEventDr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;


public class JournalCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();
	JournalHdr journalHdr = null;
	JournalDtl journalDtl = null;
	JournalDtl journalDtlDelete;
	double totalDebitamount;
	double totalCreditamount;
	private ObservableList<JournalDtl> journalList = FXCollections.observableArrayList();

    @FXML
    private TextField txtCreditAccount;

    @FXML
    private DatePicker dpJournalDate;

    @FXML
    private TextField txtRemarks;

    @FXML
    private TextField txtDebitAccount;

    @FXML
    private TextField txtAmount;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnDelete;

	@FXML
	private TableView<JournalDtl> tbJournal;

	@FXML
	private TableColumn<JournalDtl, String> clAccount;

	@FXML
	private TableColumn<JournalDtl, String> clRemarks;

	@FXML
	private TableColumn<JournalDtl, Number> clDebit;

	@FXML
	private TableColumn<JournalDtl, Number> clCredit;
	@FXML
	private TextField txtDebitTotal;

	@FXML
	private TextField txtCreditTotal;

	@FXML
	private Button btnSubmit;
	


	@FXML
	void showAccountHead(MouseEvent event) {
		showPopup();
	}

	@FXML
	private void initialize() {
		dpJournalDate = SystemSetting.datePickerFormat(dpJournalDate, "dd/MMM/yyyy");
		eventBus.register(this);
		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
	
		tbJournal.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					journalDtlDelete = new JournalDtl();
					journalDtlDelete.setId(newSelection.getId());
				}
			}
		});
	}

	@FXML
	void accountOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtRemarks.requestFocus();

		}
	}
    @FXML
    void loadpopUp(MouseEvent event) {

    	showPopupCr();
      }
	@FXML
	void remarkOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}
	}

	@FXML
	void debitOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();
		}
	}

	@FXML
	void creditOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtRemarks.requestFocus();
		}
	}

	@FXML
	void AddJournal(ActionEvent event) {
		saveJournal();
	}

	@FXML
	void saveOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			saveJournal();
		}
	}
    @FXML
    void finalSub(KeyEvent event) {
    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
    		finalSubmit();
    	}
    }

	@FXML
	void finalSubmit(ActionEvent event) {
		finalSubmit();
	}
	private void finalSubmit()
	{
		ResponseEntity<List<JournalDtl>> journalDtlSaved = RestCaller.getJournalDtl(journalHdr);
		journalList = FXCollections.observableArrayList(journalDtlSaved.getBody());
		if (journalDtlSaved.getBody().size() == 0) {
			notifyMessage(5, "Item Not found!!!");
			return;
		}
		if (Double.parseDouble(txtDebitTotal.getText()) == Double.parseDouble(txtCreditTotal.getText())) {

			String financialYear = SystemSetting.getFinancialYear();
			String sNo = RestCaller.getVoucherNumber(financialYear + "JOURNAL");
			ResponseEntity<List<JournalDtl>> journalSaved = RestCaller.getJournalDtl(journalHdr);
			journalList = FXCollections.observableArrayList(journalSaved.getBody());
//			for(JournalDtl journDtl : journalList)
//			{
//				ResponseEntity<Supplier> supSaved = RestCaller.getSupplierByAccountById(journDtl.getAccountHead());
//				
//				if(null != supSaved.getBody())
//				{
//					OwnAccount ownAccount = new OwnAccount();
//					ownAccount.setAccountId(journDtl.getAccountHead());
//					ownAccount.setAccountType("SUPPLIER");
//					ownAccount.setCreditAmount(journDtl.getCreditAmount());
//					ownAccount.setDebitAmount(journDtl.getDebitAmount());
//					ownAccount.setOwnAccountStatus("PENDING");
//					ownAccount.setRealizedAmount(0.0);
//					ownAccount.setVoucherDate(Date.valueOf(dpJournalDate.getValue()));
//					ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
//					ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
//				}
//				else
//				{
//					ResponseEntity<CustomerMst> custSaved = RestCaller.getCustomerById(journDtl.getAccountHead());
//					if(null != custSaved.getBody())
//					{
//						OwnAccount ownAccount = new OwnAccount();
//						ownAccount.setAccountId(journDtl.getAccountHead());
//						ownAccount.setAccountType("CUSTOMER");
//						ownAccount.setCreditAmount(journDtl.getCreditAmount());
//						ownAccount.setDebitAmount(journDtl.getDebitAmount());
//						ownAccount.setOwnAccountStatus("PENDING");
//						ownAccount.setRealizedAmount(0.0);
//						ownAccount.setVoucherDate(Date.valueOf(dpJournalDate.getValue()));
//						ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
//						ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
//					}
//				}
//			}
		
			journalHdr.setVoucherNumber(sNo);
			RestCaller.updateJournalFinalSave(journalHdr);
			for (JournalDtl journal: journalList)
			{
				DayBook dayBook = new DayBook();
				dayBook.setBranchCode(journalHdr.getBranchCode());
				ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountById(journal.getAccountHead());
				AccountHeads accountHeads = accountHead.getBody();
					dayBook.setCrAccountName(accountHeads.getAccountName());
				dayBook.setCrAmount(journal.getCreditAmount());
				dayBook.setNarration(accountHeads.getAccountName()+journalHdr.getVoucherNumber());
				dayBook.setSourceVoucheNumber(journalHdr.getVoucherNumber());
				dayBook.setSourceVoucherType("JOURNAL");
				dayBook.setDrAccountName(accountHeads.getAccountName());
				dayBook.setDrAmount(journal.getDebitAmount());
				LocalDate ldate = SystemSetting.utilToLocaDate(journalHdr.getVoucherDate());
				dayBook.setsourceVoucherDate(Date.valueOf(ldate));
				ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			}
			notifyMessage(5, "Journal Details Saved!!!");
			journalHdr = null;
			journalDtl = null;
			clearfields();
			txtCreditTotal.clear();
			txtDebitTotal.clear();
			dpJournalDate.setValue(null);
			tbJournal.getItems().clear();
			journalList.clear();
		} else
			notifyMessage(5, "Credit and Debit Amount Total should be Equal!!!");

	}

	@FXML
	void clearfield(ActionEvent event) {
		clearfields();
		dpJournalDate.setValue(null);
		tbJournal.getItems().clear();
		txtCreditTotal.clear();
		txtDebitTotal.clear();
	}

	@FXML
	void DeleteFields(ActionEvent event) {

		if (null != journalDtlDelete.getId()) {
			RestCaller.deleteJournalDtl(journalDtlDelete.getId());
			notifyMessage(5, "Deleted!!!");
			tbJournal.getItems().clear();
			txtDebitTotal.clear();
			txtCreditTotal.clear();
			ResponseEntity<List<JournalDtl>> journalDtlSaved = RestCaller.getJournalDtl(journalHdr);
			journalList = FXCollections.observableArrayList(journalDtlSaved.getBody());
			if (null != journalList) {
				tbJournal.setItems(journalList);
				setTotal();
				clearfields();
			}
		}
	}

	private void clearfields() {
		txtCreditAccount.clear();
		txtDebitAccount.clear();
		txtDebitTotal.clear();
		txtAmount.clear();
		txtCreditTotal.clear();
		txtRemarks.clear();
		
		// dpJournalDate.setValue(null);
	}

	private void Filltable() {
		for(JournalDtl journaDtl : journalList)
		{
			ResponseEntity<AccountHeads> acc = RestCaller.getAccountById(journaDtl.getAccountHead());
			journaDtl.setAccountHeadName(acc.getBody().getAccountName());
		}
		tbJournal.setItems(journalList);
		
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getaccountHeadProperty());
		clRemarks.setCellValueFactory(cellData -> cellData.getValue().getremarksProperty());
		clDebit.setCellValueFactory(cellData -> cellData.getValue().getdebitAmountProperty());
		clCredit.setCellValueFactory(cellData -> cellData.getValue().getcreditAmountProperty());
	}

	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root = loader.load();
			AccountNewPopupCtl popupctl = loader.getController();
			popupctl.resultEventName="CreditAccount";
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			txtDebitAccount.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void showPopupCr() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root = loader.load();
			AccountNewPopupCtl popupctl = loader.getController();
			popupctl.resultEventName="DebitAccount";
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			txtAmount.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popuplistner(AccountEventCr accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			txtCreditAccount.setText(accountEvent.getAccountName());
		}
	}
	@Subscribe
	public void popuplistner(AccountEventDr accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			txtDebitAccount.setText(accountEvent.getAccountName());
		}
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void saveJournal() {
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
//		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
//		System.out.println("Sys Date before add" + SystemSetting.systemDate);
//		if (null != dayEndClosureHdr) {
//
//			if (null != dayEndClosureHdr.getDayEndStatus()) {
//				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
//				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
//				java.sql.Date prDate = Date.valueOf(process_date);
//				Date sDate = Date.valueOf(sysdate);
//				int i = prDate.compareTo(sDate);
//				if (i > 0 || i == 0) {
//					notifyMessage(1, " Day End Already Done for this Date !!!!");
//					return;
//				}
//			}
//		}
//		
//		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);
//		
//		if(!dayendtodone)
//		{
//			notifyMessage(1, "Day End should be done before changing the date !!!!");
//			return;
//		}
		
	if(txtCreditAccount.getText().trim().equalsIgnoreCase("cash"))
	{
		notifyMessage(5,"Cash entry not possible");
		return;
	}
	if(txtDebitAccount.getText().trim().equalsIgnoreCase("cash"))
	{
		notifyMessage(5,"Cash entry not possible");
		return;
	}
	if(txtCreditAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch+"-cash"))
	{
		notifyMessage(5,"Cash entry not possible");
		return;
	}
	if(txtDebitAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch+"-cash"))
	{
		notifyMessage(5,"Cash entry not possible");
		return;
	}
	if(txtCreditAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch+"-petty cash"))
	{
		notifyMessage(5,"Cash entry not possible");
		return;
	}
	if(txtDebitAccount.getText().trim().equalsIgnoreCase(SystemSetting.systemBranch+"-petty cash"))
	{
		notifyMessage(5,"Cash entry not possible");
		return;
	}
		if (null == dpJournalDate.getValue()) {
			notifyMessage(5, "Please Select Date!!");
			dpJournalDate.requestFocus();
			return;
		} else if (txtCreditAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Select Account!!");
			txtCreditAccount.requestFocus();
			return;
		}
		if (txtAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Type Credit/Debit  Amount!!");
			txtAmount.requestFocus();
			return;
		}

		if (null == journalHdr) {
			journalHdr = new JournalHdr();
			journalHdr.setVoucherDate(Date.valueOf(dpJournalDate.getValue()));
		 
	
			 
			journalHdr.setBranchCode(SystemSetting.systemBranch);
			journalHdr.setTransDate(Date.valueOf(dpJournalDate.getValue()));
			ResponseEntity<JournalHdr> respentity = RestCaller.saveJournalHdr(journalHdr);
			journalHdr = respentity.getBody();
		}
		journalDtl = new JournalDtl();
		journalDtl.setJournalHdr(journalHdr);
		
		
		
		/*
		 * Find Account head from Account table
		 */
		
		ResponseEntity<AccountHeads> accountHeadsResponce = RestCaller.getAccountHeadByName(txtCreditAccount.getText());
		AccountHeads accountHeads = accountHeadsResponce.getBody();
		journalDtl.setAccountHead(accountHeads.getId());
		journalDtl.setCreditAmount(Double.parseDouble(txtAmount.getText()));
		journalDtl.setDebitAmount(0.0);
		journalDtl.setRemarks(txtRemarks.getText());
		ResponseEntity<JournalDtl> respentity = RestCaller.saveJournalDtl(journalDtl);
		journalDtl = respentity.getBody();
		journalList.add(journalDtl);
		Filltable();
		setTotal();
		journalDtl = new JournalDtl();
		journalDtl.setJournalHdr(journalHdr);
		ResponseEntity<AccountHeads> accountHeadsdr = RestCaller.getAccountHeadByName(txtDebitAccount.getText());
		AccountHeads accountHeadsDr = accountHeadsdr.getBody();
		journalDtl.setAccountHead(accountHeadsDr.getId());
		journalDtl.setCreditAmount(0.0);
		journalDtl.setDebitAmount(Double.parseDouble(txtAmount.getText()));
		journalDtl.setRemarks(txtRemarks.getText());
		ResponseEntity<JournalDtl> respentity1 = RestCaller.saveJournalDtl(journalDtl);
		journalDtl = respentity1.getBody();
		journalList.add(journalDtl);
		clearfields();
		Filltable();
		setTotal();
	}

	private void setTotal() {
		totalDebitamount = RestCaller.getJournalDebitTotal(journalHdr.getId());
		txtDebitTotal.setText(Double.toString(totalDebitamount));
		totalCreditamount = RestCaller.getJournalCreditTotal(journalHdr.getId());
		txtCreditTotal.setText(Double.toString(totalCreditamount));
	}
	 @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
