package com.maple.mapleclient.controllers;


import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;

import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.InsuranceWiseSalesReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class InsuranceSalesReportCtl {
	
	InsuranceWiseSalesReport insuranceWiseSalesReport;
	
	@FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TableView<InsuranceWiseSalesReport> tblDetails;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, String> clInvDate;

    @FXML
    private TableColumn<InsuranceWiseSalesReport,String> clVouNo;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, String> clCusName;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, String> clPatientId;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, String> clInsuComapny;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, String> clInsCard;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, String> clPolcType;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, Number> clCreAmount;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, Number> clCashPaid;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, Number> clCardPaid;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, Number> clInsAmount;

    @FXML
    private TableColumn<InsuranceWiseSalesReport, Number> clInvAmount;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnGenerate;

    @FXML
    private Button btnClear;
    
    private ObservableList<InsuranceWiseSalesReport> insuranceWiseSalesList = FXCollections.observableArrayList();

    @FXML
    void KeyFromDate(KeyEvent event) {

    }

    @FXML
    void KeyToDate(KeyEvent event) {

    }
    @FXML
 	private void initialize() {
     	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
     	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
  }
    @FXML
    void OnActionClear(ActionEvent event) {
     
    	tblDetails.getItems().clear();
    	dpFromDate.getEditor().clear();;
		dpToDate.getEditor().clear();
    	
    }

    @FXML
    void OnActionShow(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
			return;
    	}
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, "Please select to date", false);
    		return;
    	}
    	
    		
    		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
    		
    		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
    		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
    		
    		ResponseEntity<List<InsuranceWiseSalesReport>> insuranceWiseSalesResp=RestCaller.getInsuranceWiseSalesReportReport(sfdate,stdate);
    		insuranceWiseSalesList = FXCollections.observableArrayList(insuranceWiseSalesResp.getBody());
    		
    		
    		
    		if (insuranceWiseSalesList.size() > 0) {
    			
    			
    			filltable();
    			
    			
    		}
    		
    
    	

    }

    private void filltable() {
    	
    	tblDetails.setItems(insuranceWiseSalesList);
    	
    	clInvDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
    	clCusName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
    	clPatientId.setCellValueFactory(cellData -> cellData.getValue().getPatientIdProperty());
    	clInsuComapny.setCellValueFactory(cellData -> cellData.getValue().getInsComapanyProperty());
    	clInsCard.setCellValueFactory(cellData -> cellData.getValue().getInsuCardProperty());
    	clPolcType.setCellValueFactory(cellData -> cellData.getValue().getPolicyTypeProperty());
    	clCreAmount.setCellValueFactory(cellData -> cellData.getValue().getCreditAmountProperty());
    	clCashPaid.setCellValueFactory(cellData -> cellData.getValue().getCashPaidProperty());
    	clCardPaid.setCellValueFactory(cellData -> cellData.getValue().getCardPaidProperty());
    	clInsAmount.setCellValueFactory(cellData -> cellData.getValue().getInsuAmountProperty());
    	clInvAmount.setCellValueFactory(cellData -> cellData.getValue().getInvoiceAmountProperty());
    	clVouNo.setCellValueFactory(cellData -> cellData.getValue().getVocherNumberProperty());
		
	}

	@FXML
    void OnActionReport(ActionEvent event) {
		
		if(null==dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
    		return;
    		
    	}
    	if (null==dpToDate.getValue()) {
    		notifyMessage(5, "Please select to date", false);
    		return;
    		
    	}
    	
    	Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String edate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		
		try {
			JasperPdfReportService.insurancewisesalesreport(sdate, edate);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		


    }
    
    
  		private void notifyMessage(int i, String string, boolean b) {
  			
  	    	Image img;
  			if (b) {
  				img = new Image("done.png");

  			} else {
  				img = new Image("failed.png");
  			}

  			Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
  					.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
  					.onAction(new EventHandler<ActionEvent>() {
  						@Override
  						public void handle(ActionEvent event) {
  							System.out.println("clicked on notification");
  						}
  					});
  			notificationBuilder.darkStyle();
  			notificationBuilder.show();

  	    	
  		}


}
