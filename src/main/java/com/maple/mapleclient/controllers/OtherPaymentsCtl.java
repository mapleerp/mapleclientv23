package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.javapos.print.ChequePrint;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class OtherPaymentsCtl {

	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PaymentDtl> paymentList = FXCollections.observableArrayList();
	PaymentHdr paymenthdr = null;
	PaymentDtl paymentDtl = null;
	PaymentDtl paymentDelete = null;
	double totalamount = 0.0;
	@FXML
	private Button btnAdd;

	@FXML
	private TextField txtGrandTotal;

	@FXML
	private Button btnDelete;

	@FXML
	private TextField txtDate;

	@FXML
	private Button btnFinalSubmit;

	@FXML
	private TextField txtAccount;

	@FXML
	private TextField txtRemarks;

	@FXML
	private TextField txtAmount;

	@FXML
	private TextField txtInstrumentNumber;

	@FXML
	private TextField txtVoucherNo;

	@FXML
	private ComboBox<String> cmbBank;

	@FXML
	private ComboBox<String> cmbModeOfPayment;

	@FXML
	private DatePicker dpInstrumentDate;

	@FXML
	private DatePicker dpTransaDate;

	@FXML
	private TableView<PaymentDtl> tbPayment;

	@FXML
	private TableColumn<PaymentDtl, String> clAccount;

	@FXML
	private TableColumn<PaymentDtl, String> clModeOfPay;

	@FXML
	private TableColumn<PaymentDtl, String> clRemarks;

	@FXML
	private TableColumn<PaymentDtl, LocalDate> clTransaDate;

	@FXML
	private TableColumn<PaymentDtl, Number> clAmount;

	@FXML
	private TableColumn<PaymentDtl, String> clInstrumentNo;

	@FXML
	private TableColumn<PaymentDtl, String> voucher;

	@FXML
	private TableColumn<PaymentDtl, String> clBankAccount;

	@FXML
	private TableColumn<PaymentDtl, LocalDate> clInstrumentDate;

	@FXML
	private void initialize() {
		dpInstrumentDate = SystemSetting.datePickerFormat(dpInstrumentDate, "dd/MMM/yyyy");
		dpTransaDate = SystemSetting.datePickerFormat(dpTransaDate, "dd/MMM/yyyy");
		System.out.print(
				"inside the initialize issssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		eventBus.register(this);
		txtDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd"));

		// ----------------new version 1.9 surya
//		ResponseEntity<AccountHeads> accountHead = RestCaller
//				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
//
//		cmbModeOfPayment.getItems().add("TRASNFER");
//		cmbModeOfPayment.getItems().add("CHEQUE");
//		cmbModeOfPayment.getItems().add("DD");
//		cmbModeOfPayment.getItems().add("NEFT");
//		cmbModeOfPayment.getItems().add("RTGS");
//		cmbModeOfPayment.getItems().add(accountHead.getBody().getAccountName());

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstListResp = RestCaller.getAllReceiptMode();
		List<ReceiptModeMst> receiptModeMstList = receiptModeMstListResp.getBody();

		System.out.print(receiptModeMstList.size()
				+ "receipt mode issssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss################################################################################################");
		for (ReceiptModeMst receiptModeMst : receiptModeMstList) {
			// if(receiptModeMst.getCreditCardStatus().equalsIgnoreCase("NO"))
			// {
			cmbModeOfPayment.getItems().add(receiptModeMst.getReceiptMode());
			// }
		}

		// ----------------new version 1.9 surya end

		/*
		 * ArrayList bankList = new ArrayList(); bankList = RestCaller.getAllBankmsts();
		 * Iterator itr1 = bankList.iterator(); while (itr1.hasNext()) { LinkedHashMap
		 * lm = (LinkedHashMap) itr1.next(); Object bankName = lm.get("bankName"); if
		 * (bankName != null) { cmbBank.getItems().add((String) bankName); //
		 * accountHeads.getAccountName); } }
		 */
		
		//---fetch bank accounts from account heads and set into bank account combo box....similar to pdc payment..
		ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid= lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbBank.getItems().add((String) bankName);
				
						    //accountHeads.getAccountName);
			}
		}

		cmbModeOfPayment.valueProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null != newValue) {
					if (newValue.matches(SystemSetting.getSystemBranch() + "-CASH")) {
						cmbBank.setDisable(true);
						txtInstrumentNumber.setDisable(true);
						dpInstrumentDate.setDisable(true);
					} else {
						cmbBank.setDisable(false);
						txtInstrumentNumber.setDisable(false);
						dpInstrumentDate.setDisable(false);
					}

				}
			}
		});

		cmbModeOfPayment.valueProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					if (!newValue.equals(SystemSetting.systemBranch + "-CASH")) {

						ArrayList bankList = new ArrayList();
						bankList = RestCaller.getAllBankAccounts();
						Iterator itr1 = bankList.iterator();
						while (itr1.hasNext()) {
							LinkedHashMap lm = (LinkedHashMap) itr1.next();
							Object bankName = lm.get("accountName");
							Object bankid = lm.get("id");
							String bankId = (String) bankid;
							if (bankName != null) {
								cmbBank.getItems().add((String) bankName);

								// accountHeads.getAccountName);
							}
						}
					}
				}
			}
		});

		tbPayment.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					paymentDelete = new PaymentDtl();
					paymentDelete.setId(newSelection.getId());
				}
			}
		});
		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
	}

	@FXML
	void AddItems(ActionEvent event) {
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}

			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}
		if (null != cmbModeOfPayment.getSelectionModel()) {
			if (txtAccount.getText().trim().equalsIgnoreCase(cmbModeOfPayment.getSelectionModel().getSelectedItem())) {
				notifyMessage(5, "Debit And Credit Accounts should not be same");
				return;
			}
		}
		if (null == paymenthdr) {
			paymenthdr = new PaymentHdr();
			paymenthdr.setBranchCode(SystemSetting.systemBranch);
			paymenthdr.setCompanyId(SystemSetting.getUser().getCompanyMst().getId());
			paymenthdr.setVoucherDate(SystemSetting.systemDate);
			ResponseEntity<PaymentHdr> respentity = RestCaller.savePaymenthdr(paymenthdr);
			paymenthdr = respentity.getBody();
		}
		paymentDtl = new PaymentDtl();
		paymentDtl.setAccount(txtAccount.getText());
		ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountHeadByName(txtAccount.getText());
		paymentDtl.setAccountId(accSaved.getBody().getId());
		paymentDtl.setAmount(Double.parseDouble(txtAmount.getText()));
		paymentDtl.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
		paymentDtl.setRemark(txtRemarks.getText());
		paymentDtl.setModeOfPayment(cmbModeOfPayment.getSelectionModel().getSelectedItem());
		String CASH = SystemSetting.systemBranch + "-CASH";
		if (cmbModeOfPayment.getValue().equals(CASH)) {
			ResponseEntity<AccountHeads> creditAccount = RestCaller.getAccountHeadByName(cmbModeOfPayment.getValue());
			paymentDtl.setCreditAccountId(creditAccount.getBody().getId());
		} else {
			ResponseEntity<AccountHeads> creditAccount = RestCaller.getAccountHeadByName(cmbBank.getValue());
			paymentDtl.setCreditAccountId(creditAccount.getBody().getId());
		}

		paymentDtl.setPaymenthdr(paymenthdr);
		if (cmbModeOfPayment.getSelectionModel().getSelectedItem().toString()
				.equalsIgnoreCase(SystemSetting.getSystemBranch() + "-CASH")) {
			paymentDtl.setInstrumentDate(null);
			paymentDtl.setInstrumentNumber(txtInstrumentNumber.getText());
			paymentDtl.setBankAccountNumber(cmbBank.getSelectionModel().getSelectedItem());
			ResponseEntity<PaymentDtl> respentity = RestCaller.savePaymentdtls(paymentDtl);
			paymentDtl = respentity.getBody();
		} else {
			if (txtInstrumentNumber.getText().trim().isEmpty()) {
				notifyMessage(5, "Type Instrument Number!!!!");
				txtInstrumentNumber.setDisable(false);
				txtInstrumentNumber.requestFocus();
				return;
			} else if (cmbBank.getSelectionModel().getSelectedItem().trim().isEmpty()) {
				notifyMessage(5, "Type Deposit Bank!!!!");
				cmbBank.setDisable(false);
				cmbBank.requestFocus();
				return;
			} else if (null == dpInstrumentDate.getValue()) {
				notifyMessage(5, "Select Instrument Date!!!!");
				dpInstrumentDate.setDisable(false);
				dpInstrumentDate.requestFocus();
				return;
			} else {
				paymentDtl.setInstrumentDate(Date.valueOf(dpInstrumentDate.getValue()));
				paymentDtl.setInstrumentNumber(txtInstrumentNumber.getText());
				paymentDtl.setBankAccountNumber(cmbBank.getSelectionModel().getSelectedItem());
				ResponseEntity<PaymentDtl> respentity = RestCaller.savePaymentdtls(paymentDtl);
				paymentDtl = respentity.getBody();
			}

		}
		setTotal();
		paymentDtl.setAccount(txtAccount.getText());
		paymentList.add(paymentDtl);
		fillTable();

		txtAccount.clear();
		txtAmount.clear();
		txtInstrumentNumber.clear();
		txtRemarks.clear();
		cmbBank.getSelectionModel().clearSelection();
		cmbModeOfPayment.getSelectionModel().clearSelection();

		btnAdd.setDisable(true);
	}

	@FXML
	void modeOfPayOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtRemarks.requestFocus();
		}
	}

	@FXML
	void remarksOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();
		}
	}

	@FXML
	void transDateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();
		}
	}

	@FXML
	void amountOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtInstrumentNumber.isDisable()) {
				btnAdd.requestFocus();
			} else
				txtInstrumentNumber.requestFocus();
		}
	}

	@FXML
	void DeleteItem(ActionEvent event) {
		btnAdd.setDisable(false);
		if (null != paymentDelete.getId()) {
			RestCaller.deletePaymentDtl(paymentDelete.getId());
			notifyMessage(5, "Deleted!!!");
			ResponseEntity<List<PaymentDtl>> paymentdtlSaved = RestCaller.getPaymentDtl(paymenthdr);
			paymentList = FXCollections.observableArrayList(paymentdtlSaved.getBody());
			if (paymentList != null) {
				for (PaymentDtl payment : paymentList) {
					ResponseEntity<AccountHeads> respentity = RestCaller.getAccountById(payment.getAccountId());
					payment.setAccount(respentity.getBody().getAccountName());
				}

			}
			tbPayment.setItems(paymentList);
		}
		paymentDtl = null;
		setTotal();

	}

	@FXML
	void loadPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	@FXML
	void FinalSubmit(ActionEvent event) {
		btnFinalSubmit.setDisable(true);
		ChequePrint chequePrint = new ChequePrint();
		ResponseEntity<List<PaymentDtl>> paymentdtlSaved = RestCaller.getPaymentDtl(paymenthdr);
		if (paymentdtlSaved.getBody().size() == 0) {
			return;
		}

		String financialYear = SystemSetting.getFinancialYear();
		String vNo = RestCaller.getVoucherNumber(financialYear + "PYN");

		paymenthdr.setVoucherNumber(vNo);
		RestCaller.updatePaymenthdr(paymenthdr);
		for (PaymentDtl paymnt : paymentList) {
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(paymenthdr.getBranchCode());
			ResponseEntity<AccountHeads> accountHead = RestCaller
					.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
			AccountHeads accountHeads = accountHead.getBody();
			if (paymnt.getModeOfPayment().equalsIgnoreCase(accountHeads.getAccountName())) {
				dayBook.setCrAccountName(SystemSetting.systemBranch + "-CASH");
				dayBook.setDrAmount(0.0);
			}

			else {
				dayBook.setCrAccountName(paymnt.getBankAccountNumber());
				dayBook.setDrAmount(paymnt.getAmount());

			}
			dayBook.setCrAmount(paymnt.getAmount());
			ResponseEntity<AccountHeads> accountHead1 = RestCaller.getAccountById(paymnt.getAccountId());
			AccountHeads accountHeads1 = accountHead1.getBody();
			dayBook.setNarration(accountHeads1.getAccountName() + paymenthdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(paymenthdr.getVoucherNumber());
			dayBook.setSourceVoucherType("PAYMENTS");
			dayBook.setDrAccountName(accountHeads1.getAccountName());
			LocalDate ldate = SystemSetting.utilToLocaDate(paymenthdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(ldate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}
		clearFields();
		java.util.Date uDate = paymenthdr.getVoucherDate();
		String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		ResponseEntity<AccountHeads> account = RestCaller
				.getAccountHeadsById(paymentdtlSaved.getBody().get(0).getAccountId());
		if (null != account.getBody()) {
			Double balance = RestCaller.PaymentInvoiceSettlemnt(paymentdtlSaved.getBody().get(0).getAccountId(),
					paymentdtlSaved.getBody().get(0).getAmount(), paymenthdr.getId(), vDate);
		}
		paymenthdr = null;
		paymentList.clear();

		txtGrandTotal.clear();

		if (paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("CHEQUE")
				|| paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("NEFT")
				|| paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("RTGS")) {
			Alert a = new Alert(AlertType.CONFIRMATION);
			a.setHeaderText("Cheque Print");
			a.setContentText("Do you Want To Print Cheque");
			a.showAndWait().ifPresent((btnType) -> {

				if (btnType == ButtonType.OK) {
					String accountName;
					BigDecimal bamout = new BigDecimal(paymentdtlSaved.getBody().get(0).getAmount());
					bamout = bamout.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					String amount = bamout + "";
					String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");
					ResponseEntity<AccountHeads> getAccname = RestCaller
							.getAccountById(paymentdtlSaved.getBody().get(0).getAccountId());
					String bankName = paymentdtlSaved.getBody().get(0).getBankAccountNumber();
					if (paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("NEFT")) {
						accountName = "NEFT-" + getAccname.getBody().getAccountName();
					} else if (paymentdtlSaved.getBody().get(0).getModeOfPayment().equalsIgnoreCase("RTGS")) {
						accountName = "RTGS-" + getAccname.getBody().getAccountName();
					} else {
						accountName = getAccname.getBody().getAccountName();
					}
					System.out.println("ACCOUNT NAME-" + accountName);
					System.out.println("AMOUNT" + paymentdtlSaved.getBody().get(0).getAmount() + "");
					System.out.println("AMOUNT IN WORDS" + amountinwords);
					System.out.println("DATE" + vDate);
					String sdate = SystemSetting.UtilDateToString(uDate, "dd MM yyyy");
					System.out.println("DATE" + sdate);
					try {
						chequePrint.PrintInvoiceThermalPrinter(bankName, accountName, amount, amountinwords, sdate);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
		}
		try {
			JasperPdfReportService.PaymentReport(vNo, vDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btnAdd.setDisable(false);
		btnFinalSubmit.setDisable(false);
	}

	@FXML
	void ShowAccountPopup(MouseEvent event) {
		showPopup();
	}

	private void clearFields() {
		txtAccount.clear();
		txtAmount.clear();
		txtInstrumentNumber.clear();
		txtRemarks.clear();
		cmbBank.getSelectionModel().clearSelection();
		cmbModeOfPayment.getSelectionModel().clearSelection();

	}

	private void fillTable() {
		tbPayment.setItems(paymentList);
		for (PaymentDtl payment : paymentList) {
			ResponseEntity<AccountHeads> respentity = RestCaller.getAccountById(payment.getAccountId());
			payment.setAccount(respentity.getBody().getAccountName());
		}
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());
		clRemarks.setCellValueFactory(cellData -> cellData.getValue().getRemarkProperty());
		clModeOfPay.setCellValueFactory(cellData -> cellData.getValue().getModeOfPaymentProperty());
		clInstrumentNo.setCellValueFactory(cellData -> cellData.getValue().getInstrumentNumberProperty());
		clBankAccount.setCellValueFactory(cellData -> cellData.getValue().getBankAccountNumberProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clInstrumentDate.setCellValueFactory(cellData -> cellData.getValue().getInstrumentDateProperty());
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}

	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Accounts");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbModeOfPayment.requestFocus();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(AccountPopupEvent accountPopupEvent) {

		System.out.println("------AccountEvent-------toAccount-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountPopupEvent.getAccountName());

		}

	}
	
	
	/*
	 * @Subscribe public void popuplistner(AccountEvent accountEvent) {
	 * 
	 * System.out.println("------AccountEvent-------popuplistner-------------");
	 * txtAccount.setText(accountEvent.getAccountName()); Stage stage = (Stage)
	 * btnAdd.getScene().getWindow(); if (stage.isShowing()) {
	 * 
	 * txtAccount.setText(accountEvent.getAccountName());
	 * 
	 * }
	 * 
	 * }
	 */
	/*
	 * @Subscribe public void popuplistner(AccountEvent accountEvent) {
	 * 
	 * System.out.println("------AccountEvent-------popuplistner-------------");
	 * Stage stage = (Stage) txtAccount.getScene().getWindow(); if
	 * (stage.isShowing()) { Platform.runLater(new Runnable() {
	 * 
	 * @Override public void run() {
	 * txtAccount.setText(accountEvent.getAccountName());
	 * 
	 * System.out.print(accountEvent.getAccountName()
	 * +"account head name issssssssssssssssssssssssssssssssssssssssssssssssss"); }
	 * }); }
	 * 
	 * }
	 */

	private void setTotal() {

		totalamount = RestCaller.getPaymentTotal(paymenthdr.getId());
		txtGrandTotal.setText(Double.toString(totalamount));
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

}
