package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.IntentInDtl;
import com.maple.mapleclient.entity.IntentInHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class IntentStockTransfer {
	String taskid;
	String processInstanceId;
	private ObservableList<IntentInHdr> intentInHdrList = FXCollections.observableArrayList();
	private ObservableList<IntentInDtl> intentInDtlList = FXCollections.observableArrayList();
	private ObservableList<IntentInDtl> intentInDtlList1 = FXCollections.observableArrayList();
	private ObservableList<StockTransferOutDtl> StockTransferList = FXCollections.observableArrayList();


	IntentInDtl intentInDtl = null;
	ItemMst itemMst = null;
	StockTransferOutHdr stockTransferOutHdr = null;
	StockTransferOutDtl stockTransferOutDtl = null;
	String intentNo ="";
	Integer count = 0;
    @FXML
    private DatePicker dpDate;

    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    private Button btnShow;
    @FXML
    private TableView<StockTransferOutDtl> tblStockTrasfer;

    @FXML
    private TableColumn<StockTransferOutDtl, String> clStktransferItemName;

    @FXML
    private TableColumn<StockTransferOutDtl, String> clStktransferBarcode;

    @FXML
    private TableColumn<StockTransferOutDtl, String> clStktransferBatch;

    @FXML
    private TableColumn<StockTransferOutDtl, Number> clStkTransferRate;

    @FXML
    private TableColumn<StockTransferOutDtl,Number> clStkTransferQty;

    @FXML
    private TableColumn<StockTransferOutDtl,Number> clStkMRP;

    @FXML
    private TableColumn<StockTransferOutDtl,Number> clStkTransferAmount;
  
    @FXML
    private TableView<IntentInDtl> tblIntentDtl;

    @FXML
    private TableColumn<IntentInDtl, String> clItemName;

    @FXML
    private TableColumn<IntentInDtl, Number> clQty;
    

    @FXML
    private TableColumn<IntentInDtl, String> clUnit;
    
    
    @FXML
    private TableView<IntentInDtl> tblPending;

    @FXML
    private TableColumn<IntentInDtl, String> clPendingDate;

    
    @FXML
    private TableColumn<IntentInDtl, String> clPendingItem;

    @FXML
    private TableColumn<IntentInDtl, Number> clPendingQty;

    @FXML
    private TableColumn<IntentInDtl, String> clPendingUnit;


    @FXML
    private TableColumn<IntentInDtl, String> clPendingBranch;
    
    @FXML
    private TextField txtQty;

    @FXML
    private Button btnStockTransfer;
    
    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;
    


    @FXML
    private Button btnClearIntent;

    @FXML
    private Button btnShowPending;
    
    
    
	@FXML
	private void initialize() {
		
		
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		tblIntentDtl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					intentInDtl = new IntentInDtl();
					intentInDtl.setId(newSelection.getId());
					ResponseEntity<ItemMst > getitem = RestCaller.getItemByNameRequestParam(newSelection.getItemName());
					itemMst = getitem.getBody();
					txtQty.setText(Double.toString(newSelection.getQty()-newSelection.getAllocatedQty()));
				}
			}
			});
		
		tblStockTrasfer.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					stockTransferOutDtl = new StockTransferOutDtl();
					stockTransferOutDtl.setId(newSelection.getId());
					
				}
			}
			});
		
		tblPending.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					intentInDtl = new IntentInDtl();
					intentInDtl.setId(newSelection.getId());
					ResponseEntity<ItemMst > getitem = RestCaller.getItemByNameRequestParam(newSelection.getItemName());
					itemMst = getitem.getBody();
					cmbBranch.getSelectionModel().select(newSelection.getBranchCode());
					txtQty.setText(Double.toString(newSelection.getQty()-newSelection.getAllocatedQty()));
				}
			}
			});
		
		ResponseEntity<List<BranchMst>> branchRestListResp = RestCaller.getBranchDtl();
		List branchRestList =branchRestListResp.getBody();
		Iterator itr1  = branchRestList.iterator();
		
		while (itr1.hasNext() ) {
			
			BranchMst lm = (BranchMst) itr1.next();
			if(!lm.getBranchCode().equalsIgnoreCase(SystemSetting.systemBranch))
			cmbBranch.getItems().add(lm.getBranchCode());
		}
		
	}
	    @FXML
	    void actionAdd(ActionEvent event) {
			count = count + 1;

	    	if (null == stockTransferOutHdr) {
	    		stockTransferOutHdr = new StockTransferOutHdr();
	    		stockTransferOutHdr.setIntentNumber(intentNo);
	    		stockTransferOutHdr.setToBranch((String) cmbBranch.getSelectionModel().getSelectedItem());
				
				/*
				 * try { ArrayList branch = new ArrayList();
				 * 
				 * branch = RestCaller.SearchBranch(); Iterator itr = branch.iterator(); while
				 * (itr.hasNext()) { LinkedHashMap lm = (LinkedHashMap) itr.next(); Object
				 * branchName = lm.get("branchName"); String toBranch =
				 * cmbToBranch.getSelectionModel().getSelectedItem().toString(); if
				 * (toBranch.equalsIgnoreCase((String) branchName)) { Object branchCode =
				 * lm.get("branchCode"); stkOutHdr.setToBranch((String) branchCode); } } } catch
				 * (Exception e) { stkOutHdr = null; System.out.println(e.toString()); return; }
				 */
				
				
	    		stockTransferOutHdr.setVoucherType("STOUT");
	    		stockTransferOutHdr.setFromBranch(SystemSetting.getSystemBranch());
				// stkOutHdr.setVoucherNumber("null");
	    		if(null == dpDate.getValue())
	    		{
	    			stockTransferOutHdr.setVoucherDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.getSystemDate())));
	    		}
	    		else
	    		{
	    		stockTransferOutHdr.setVoucherDate(Date.valueOf(dpDate.getValue()));
	    		}
	    		
				ResponseEntity<StockTransferOutHdr> respentity = RestCaller.saveStockTransferOutHdr(stockTransferOutHdr);
				stockTransferOutHdr = respentity.getBody();
				System.out.println("Stock Hdr:" + stockTransferOutHdr);

			}
	    	stockTransferOutDtl = new StockTransferOutDtl();
	    	stockTransferOutDtl.setSlNo(count);
			stockTransferOutDtl.setStockTransferOutHdr(stockTransferOutHdr);
			stockTransferOutDtl.setItemNameProperty(itemMst.getItemName());
			stockTransferOutDtl.setItemId(itemMst);
			stockTransferOutDtl.setUnitId(itemMst.getUnitId());
			stockTransferOutDtl.setBatch("NOBATCH");
			stockTransferOutDtl.setIntentDtlId(intentInDtl.getId());
			stockTransferOutDtl.setQty(Double.parseDouble(txtQty.getText()));
			java.util.Date udate = SystemSetting.getApplicationDate();
			String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		
			ResponseEntity<PriceDefenitionMst> getpriceDefMst = RestCaller.getPriceDefenitionMstByName("STOCK TRANSFER");
			
			/*
			 * Check for Batch price definition
			 */
			ResponseEntity<BatchPriceDefinition> batchPrice = RestCaller.getBatchPriceDefinition(itemMst.getId(),
					getpriceDefMst.getBody().getId(), itemMst.getUnitId(), "NOBATCH", sdate);
			if(null != batchPrice.getBody())
			{
				stockTransferOutDtl.setRate(batchPrice.getBody().getAmount());
			}
			else
			{
			ResponseEntity<PriceDefinition> getPrice = RestCaller.getPriceDefenitionByItemIdAndUnit(
					itemMst.getId(), getpriceDefMst.getBody().getId(), itemMst.getUnitId(),sdate);
			if(null != getPrice.getBody())
			{
				stockTransferOutDtl.setRate(getPrice.getBody().getAmount());
			}
			else
			{
				stockTransferOutDtl.setRate(itemMst.getStandardPrice());
			}
			}
			stockTransferOutDtl.setBarcode(itemMst.getBarCode());
			stockTransferOutDtl.setMrp(itemMst.getStandardPrice());
			stockTransferOutDtl.setTaxRate(0.0);
			stockTransferOutDtl.setexpiryDate(null);
			stockTransferOutDtl.setAmount(Double.parseDouble(txtQty.getText()) *stockTransferOutDtl.getRate() );
			System.out.println("Stock Detail:" + stockTransferOutDtl);
			ResponseEntity<StockTransferOutDtl> respentity = RestCaller.saveStockTransferOutDtl(stockTransferOutDtl);
			if (stockTransferOutDtl.getId() == null) {

				stockTransferOutDtl = respentity.getBody();
				StockTransferList.add(stockTransferOutDtl);
				FillTable();
				
			} 
			else {

				tblStockTrasfer.getItems().clear();
				ResponseEntity<List<StockTransferOutDtl>> stockTransferoutdtlSaved = RestCaller.getStockTransferOutDtl(stockTransferOutHdr);
				StockTransferList = FXCollections.observableArrayList(stockTransferoutdtlSaved.getBody());
				
				FillTable();
				
			}
			txtQty.clear();

	    }
	    

		private void FillTable() {
			System.out.println("-------------FillTable-------------");
			for (StockTransferOutDtl stk : StockTransferList) {
				ResponseEntity<ItemMst> respentityitem = RestCaller.getitemMst(stk.getItemId().getId());
				stk.setItemName(respentityitem.getBody().getItemName());

			}
			tblStockTrasfer.setItems(StockTransferList);
			
			clStkTransferAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
			clStktransferBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
			clStktransferItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			clStkTransferQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
			clStkTransferRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
			clStktransferBarcode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
			clStkMRP.setCellValueFactory(cellData -> cellData.getValue().getmrpProperty());

		}
	   

	    @FXML
	    void actionDelete(ActionEvent event) {
	    	try {
				if (null != stockTransferOutDtl.getId()) {

					RestCaller.deleteStockTransferOutDtl(stockTransferOutDtl.getId());

					
					tblStockTrasfer.getItems().clear();
					ResponseEntity<List<StockTransferOutDtl>> stockTransferoutdtlSaved = RestCaller
							.getStockTransferOutDtl(stockTransferOutHdr);
					StockTransferList = FXCollections.observableArrayList(stockTransferoutdtlSaved.getBody());
					FillTable();
				}
	    	}
	    	 catch (Exception e) {
	 			return;
	    }
	    }

		public void notifyMessage(int duration, String msg) {

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}

	    
	    @FXML
	    void actionShowPending(ActionEvent event) {

	    	tblPending.getItems().clear();
	    	intentInDtlList1.clear();
	    	if(cmbBranch.getSelectionModel().isEmpty())
	    	{
	    		notifyMessage(5,"Please Select Branch");
	    		
	    		
//	    		ArrayList intent = new ArrayList();
//	    		intent = RestCaller.getPendingIntent();
//	    		String itemName;
//	    		String branchCode;
//	    		String unit;
//	    		Double qty;
//	    		Double allocatedQty;
//	    		String id;
//
//	    		String voucherDate;
//	    		Iterator itr = intent.iterator();
//	    		while (itr.hasNext()) {
//
//	    			List element = (List) itr.next();
//	    			itemName = (String) element.get(0);
//	    			branchCode = (String) element.get(4);
//	    			unit = (String) element.get(3);
//	    			qty = (Double) element.get(1);
//	    			allocatedQty = (Double) element.get(2);
//	    			id = (String) element.get(5);
//	    			voucherDate =  (String) element.get(6);
//	    			java.util.Date rdate= 	SystemSetting.StringToUtilDate(voucherDate, "yyyy-MM-dd");
//	    			//qty = (Integer) element.get(4);
//	    			if (null != itemName) 
//	    			{
//	    				IntentInDtl intentInDtl = new IntentInDtl();
//	    				intentInDtl.setId(id);
//	    				intentInDtl.setItemName(itemName);
//	    				intentInDtl.setQty(qty);
//	    				
//	    				intentInDtl.setAllocatedQty(allocatedQty);
//	    				intentInDtl.setBalanceQty(qty-allocatedQty);
//	    				intentInDtl.setBranchCode(branchCode);
//	    				intentInDtl.setUnitName(unit);
//	    				intentInDtl.setVoucherDate(SystemSetting.UtilDateToString(rdate));
//	    				intentInDtlList1.add(intentInDtl);
//	    				fillIntentInDtlPeniding();
//	    			}
//	    		}
	    	}
	    	else
	    	{
	    		ArrayList intent = new ArrayList();
	    		intent = RestCaller.getPendingIntentByBranch(cmbBranch.getSelectionModel().getSelectedItem());
	    		String itemName;
	    		String branchCode;
	    		String unit;
	    		Double qty;
	    		String id;
	    		String voucherDate;
	    		Double allocatedQty;
	    		Iterator itr = intent.iterator();
	    		while (itr.hasNext()) {

	    			List element = (List) itr.next();
	    			itemName = (String) element.get(0);
	    			branchCode = (String) element.get(4);
	    			unit = (String) element.get(3);
	    			qty = (Double) element.get(1);
	    			allocatedQty = (Double) element.get(2);
	    			id = (String) element.get(5);
	    			voucherDate =  (String) element.get(6);
	    			java.util.Date rdate= 	SystemSetting.StringToUtilDate(voucherDate, "yyyy-MM-dd");
	    			//qty = (Integer) element.get(4);
	    			if (null != itemName) 
	    			{
	    				IntentInDtl intentInDtl = new IntentInDtl();
	    				intentInDtl.setId(id);
	    				intentInDtl.setVoucherDate(SystemSetting.UtilDateToString(rdate));
	    				intentInDtl.setItemName(itemName);
	    				intentInDtl.setQty(qty);
	    				intentInDtl.setAllocatedQty(allocatedQty);

	    				intentInDtl.setBalanceQty(qty-allocatedQty);
	    				intentInDtl.setBranchCode(branchCode);
	    				intentInDtl.setUnitName(unit);
	    				intentInDtlList1.add(intentInDtl);
	    				System.out.println(intentInDtlList1);
	    				fillIntentInDtlPeniding();
	    			}
	    		}
	    	}
	    }

    @FXML
    void actionShow(ActionEvent event) {
    	
    	java.util.Date toDate =Date.valueOf(dpDate.getValue());
    	String rdate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<IntentInHdr>> respentity = RestCaller.getIntentInHdr(rdate,cmbBranch.getSelectionModel().getSelectedItem());
    	intentInHdrList = FXCollections.observableArrayList(respentity.getBody());
    	for(IntentInHdr intentHdr:intentInHdrList)
    	{
    	
    	ResponseEntity<List<IntentInDtl>> respentity1 = RestCaller.getintentdtlsbyId( intentHdr.getId());
		intentInDtlList = FXCollections.observableArrayList(respentity1.getBody());
		fillIntentInDtl();
    	}

    }
    private void fillIntentInDtlPeniding()
    {
    	tblPending.setItems(intentInDtlList1);
    	
		clPendingItem.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clPendingQty.setCellValueFactory(cellData -> cellData.getValue().getbalanceProperty());
		clPendingUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
		clPendingBranch.setCellValueFactory(cellData -> cellData.getValue().getbranchCodeProperty());
		clPendingDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());

    }

    private void fillIntentInDtl()
    {
    	tblIntentDtl.setItems(intentInDtlList);
    	for(IntentInDtl intentIn:intentInDtlList)
    	{
    		ResponseEntity<ItemMst> getItems = RestCaller.getitemMst(intentIn.getItemId());
    		intentIn.setItemName(getItems.getBody().getItemName());
    	ResponseEntity<UnitMst > getUnit = RestCaller.getunitMst(intentIn.getUnitId());
    	intentIn.setUnitName(getUnit.getBody().getUnitName());
    	}
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
    }
    @FXML
    void actionClearIntent(ActionEvent event) {
    	
    	String msg = RestCaller.UpdateAllIntent();
    	notifyMessage(5,"All Intent Cleared");

    }
    @FXML
    void actionStockTransfer(ActionEvent event) {
    	
    	ResponseEntity<List<StockTransferOutDtl>> stockTransferoutdtlSaved = RestCaller.getStockTransferOutDtl(stockTransferOutHdr);
    	StockTransferList = FXCollections.observableArrayList(stockTransferoutdtlSaved.getBody());
		if (stockTransferoutdtlSaved.getBody().size() == 0) {
			return;
		}

		for(StockTransferOutDtl stkoutdtl:StockTransferList)
		{
			for(IntentInDtl intent:intentInDtlList)
			{
				if(intent.getId().equalsIgnoreCase(stkoutdtl.getIntentDtlId()))
				{
				intent.setAllocatedQty(intent.getAllocatedQty()+stkoutdtl.getQty());
				if(intent.getQty()-intent.getAllocatedQty()>0)
				{
					intent.setStatus("PENDING");
				}
				else
				{
					intent.setStatus("COMPLETED");
				}
				RestCaller.updateIntentDtl(intent);
				}
			}
			for(IntentInDtl intent1:intentInDtlList1)
			{
				if(intent1.getId().equalsIgnoreCase(stkoutdtl.getIntentDtlId()))
				{
					intent1.setAllocatedQty(intent1.getAllocatedQty()+stkoutdtl.getQty());
				if(intent1.getQty()-intent1.getAllocatedQty()>0)
				{
					intent1.setStatus("PENDING");
				}
				else
				{
					intent1.setStatus("COMPLETED");
				}
				RestCaller.updateIntentDtl(intent1);
				}
			}
		}
		
		String financialYear = SystemSetting.getFinancialYear();
		String sNo = RestCaller.getVoucherNumber(financialYear + "STON");
		stockTransferOutHdr.setVoucherNumber(sNo);
		// RestCaller.saveStockTransferOutHdr(stkOutHdr);

		Date date = Date.valueOf(LocalDate.now());

		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");

		stockTransferOutHdr.setVoucherDate(date);

		try {

			RestCaller.updateStockTransferOutHdr(stockTransferOutHdr);
		} catch (Exception e) {

			System.out.println(e.toString());
			throw e;

		}
		
		
		
		
		

		/*
		 * Print out Stock transfer
		 */

		String strDate = formatter.format(stockTransferOutHdr.getVoucherDate());

		try {

			JasperPdfReportService.StockTransferInvoiceReport(stockTransferOutHdr.getVoucherNumber(), strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tblStockTrasfer.getItems().clear();
		StockTransferList.clear();
		stockTransferOutHdr = null;
		stockTransferOutDtl = null;
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
