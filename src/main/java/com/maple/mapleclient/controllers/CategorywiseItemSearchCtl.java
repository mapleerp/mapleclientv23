package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.CategoryManagementMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.events.CategorywiseItemSearchEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.mapleclient.service.SalesDtlServiceImpl;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class CategorywiseItemSearchCtl {
	String taskid;
	String processInstanceId;
	EventBus eventBus = EventBusFactory.getEventBus();

	CategorywiseItemSearchEvent categorywiseItemSearchEvent;
	StringProperty SearchString = new SimpleStringProperty();

	SalesDtlServiceImpl salesDtlService = new SalesDtlServiceImpl();
	public static SalesTransHdr salesTransHdr = null;
	public static String customerName=null;
	public static String voucherType=null;
	public static String localCustId=null;
	public static String txtLocalCustomer = null;
	public static String windowType=null;
	
	private ObservableList<ItemStockPopUp> itemList = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();

	ItemPopupEvent itemPopupEvent;
	private ObservableList<Button> buttonList = FXCollections.observableArrayList();
	 Button button1 = new Button();
	 String categoryName ="";
	 SalesDtl salesDtl=null;
	 public static boolean customerIsBranch = false;
	@FXML
    private ScrollPane scpane;

    @FXML
    private TextField txtItemName;


    @FXML
    private GridPane gridpane;
    @FXML
    private Button btnBack;
    
    @FXML
    private Button btnSave;

    @FXML
    private TextField txtTotalAmount;
    @FXML
    private TableView<ItemStockPopUp> tblItems;

    @FXML
    private TableColumn<ItemStockPopUp, String> clItemName;

    @FXML
    private TableColumn<ItemStockPopUp, Number> clItemStdPrice;

    @FXML
    private TableColumn<ItemStockPopUp, String> clItemBarcode;

    @FXML
    private TableColumn<ItemStockPopUp, String> clItemCode;
    @FXML
    private TableColumn<ItemStockPopUp, Number> clItemQty;
    
    @FXML
    private TableView<SalesDtl> tbSalesDtl;

    @FXML
    private TableColumn<SalesDtl, String> clSalesItem;

    @FXML
    private TableColumn<SalesDtl, String> clSalesRate;

    @FXML
    private TableColumn<SalesDtl, String> clSalesQty;

    @FXML
    private TableColumn<SalesDtl, String> clSalesBatch;

    @FXML
    private TableColumn<SalesDtl, String> clSalesTax;

    @FXML
    private TableColumn<SalesDtl, Number> clSalesAmount;

    @FXML
    private Button btnAddItem;

    @FXML
    private Button btnDeleteItem;

    @FXML
    private TextField txtRate;

    @FXML
    private TextField txtQty;

    @FXML
    void actionAddItem(ActionEvent event) {
    	  ResponseEntity<List<FinancialYearMst>> getFinancialYear =
    			  RestCaller.getAllFinancialYear(); if(getFinancialYear.getBody().size()==0) {
    			  notifyMessage(3,"Please Add Financial Year In the Configuration Menu");
    			  return; } int count = RestCaller.getFinancialYearCount(); if(count ==0) {
    			  notifyMessage(3,"Please Add Financial Year In the Configuration Menu");
    			  return; }
    			 
    					//version2.0ends
    			ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("DAY_END_LOCKED_IN_WHOLESALE");
    			if(null != getParamValue.getBody())
    			{
    				if(getParamValue.getBody().getValue().equalsIgnoreCase("YES"))
    				{
    					ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
    					DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
    					System.out.println("Sys Date before add" + SystemSetting.applicationDate);
    					if (null != dayEndClosureHdr) {
    						if (null != dayEndClosureHdr.getDayEndStatus()) {
    							String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
    							String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
    							java.sql.Date prDate = java.sql.Date.valueOf(process_date);
    							Date sDate = java.sql.Date.valueOf(sysdate);
    							int i = prDate.compareTo(sDate);
    							if (i > 0 || i == 0) {
    								notifyMessage(1, " Day End Already Done for this Date !!!!");
    								return;
    							}
    						}
    					}
    					
    				}
    				Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.applicationDate);

    				if (!dayendtodone) {
    					notifyMessage(1, "Day End should be done before changing the date !!!!");
    					return;
    				}
    				System.out.println("Sys Date before add" + SystemSetting.systemDate);
    			}
    			if(null == voucherType || voucherType.equalsIgnoreCase(null) || voucherType.equalsIgnoreCase(""))
    			{
    				notifyMessage(5, " Please Select Voucher Type");
//    				Stage stage = (Stage) btnSave.getScene().getWindow();
//    				stage.close();
    				return;
    			}
    			if(null == customerName || customerName.equalsIgnoreCase(null) || customerName.equalsIgnoreCase(""))
    			{
    				notifyMessage(5, " Please Select Customer Name");
//    				Stage stage = (Stage) btnSave.getScene().getWindow();
//    				stage.close();
    				return;

    			}
    			if (txtQty.getText().trim().isEmpty()) {

    				notifyMessage(5, " Please Enter Quantity...!!!");
    				return;

    			}
    			if(null == itemPopupEvent)
    			{
    				return;
    			}
    			ArrayList items = new ArrayList();
    			items = RestCaller.getSingleStockItemByName(itemPopupEvent.getItemName(), itemPopupEvent.getBatch());

    			Double chkQty = 0.0;
    			String itemId = null;
    		
    			Iterator itr = items.iterator();
    			while (itr.hasNext()) {
    				List element = (List) itr.next();
    				chkQty = (Double) element.get(4);
    				itemId = (String) element.get(7);
    				
    				
    			}

    			
    			if (chkQty < Double.parseDouble(txtQty.getText())) {
    				notifyMessage(5, "Not in Stock!!!");
    				txtQty.clear();
    				txtRate.clear();
    				return;
    			}
    			Double itemsqty = 0.0;
    			ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
    					itemPopupEvent.getItemId(), itemPopupEvent.getBatch());
    			saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
    			if (saleListItemTable.size() > 1) {
    				Double PrevQty = 0.0;
    				for (SalesDtl saleDtl : saleListItemTable) {
    					{
    						PrevQty = saleDtl.getQty();
    					}
    					itemsqty = itemsqty + PrevQty;
    				}
    			} else {
    				itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId,itemPopupEvent.getBatch());
    			}
    			if (chkQty < itemsqty + Double.parseDouble(txtQty.getText())) {
    				txtQty.clear();
    				txtRate.clear();
    				notifyMessage(5, "No Stock!!");
    				return;
    			}
    			Double qty,rate;
    			qty= Double.parseDouble(txtQty.getText());
    			rate = Double.parseDouble(txtRate.getText());
    			String salesResp = 	salesDtlService.AddSalesDtl(salesTransHdr.getId(),itemPopupEvent.getItemId(),itemPopupEvent.getUnitId(),
    					itemPopupEvent.getBatch(),qty,rate);
    			
    			ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

    			List<SalesDtl> salesDtlList = respentityList.getBody();

    			/*
    			 * Call Rest to get the summary and set that to the display fields
    			 */

    			// salesDtl.setTempAmount(amount);
    			saleListTable.clear();
    			saleListTable.setAll(salesDtlList);
    			FillTable();
    			txtRate.clear();
    			txtQty.clear();
    			tblItems.getSelectionModel().clearSelection();
    }
    @FXML
    void actionDeleteItem(ActionEvent event) {


		try {

			if (null != salesDtl) {
				if (null != salesDtl.getId()) {

					if (null == salesDtl.getSchemeId()) {

						RestCaller.deleteSalesDtl(salesDtl.getId());

						System.out.println("toDeleteSale.getId()" + salesDtl.getId());
//						RestCaller.deleteSalesDtl(salesDtl.getId());
						txtRate.clear();
						txtQty.clear();
						ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
						saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
						FillTable();
						salesDtl = new SalesDtl();

						notifyMessage(1, " Item Deleted Successfully");

					} else {
						notifyMessage(1, " Offer can not be delete");
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
    }
    @FXML
    void actionBack(ActionEvent event) {
    	tblItems.getItems().clear();
    	categoryName ="";
    	drawButton();
    }
    @FXML
	private void initialize() {
		txtItemName.textProperty().bindBidirectional(SearchString);

    	eventBus.register(this);
		scpane.setContent(gridpane);
		
		drawButton();
		tblItems.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getItemId()) {

					itemPopupEvent = new ItemPopupEvent();
					itemPopupEvent.setBatch(newSelection.getBatch());
					itemPopupEvent.setItemId(newSelection.getItemId());
					itemPopupEvent.setItemName(newSelection.getItemName());
					itemPopupEvent.setUnitId(newSelection.getUnitId());
					itemPopupEvent.setItemPriceLock(newSelection.getitemPriceLock());
					txtRate.setText(Double.toString(newSelection.getMrp()));
					//Get Price Definition
					ResponseEntity<AccountHeads> accountHeads=RestCaller.getAccountHeadsByName(customerName);
					Date udate = SystemSetting.getApplicationDate();
					String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
				
					ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
							itemPopupEvent.getItemId(), accountHeads.getBody().getPriceTypeId(), itemPopupEvent.getUnitId(),
							itemPopupEvent.getBatch(),sdate);
					if (null != batchPriceDef.getBody()) {
						txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
					} else {
						ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
								itemPopupEvent.getItemId(), accountHeads.getBody().getPriceTypeId(), 
								itemPopupEvent.getUnitId(),sdate);

						if (null != pricebyItem.getBody()) {
							
							
							//version 2.3
							txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
							//version2.3ends
							
							
								ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp2 = RestCaller.getPriceDefenitionMstByName("MRP");
								if(null != priceDefenitionMstResp2.getBody())
								{
							if(!pricebyItem.getBody().getPriceId().equalsIgnoreCase(priceDefenitionMstResp2.getBody().getId()))
							txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
								}
						}
					}
					if(null != itemPopupEvent.getItemPriceLock())
					{
						if(itemPopupEvent.getItemPriceLock().equalsIgnoreCase("YES"))
						{
							txtRate.setEditable(false);
						}
						else
						{
							txtRate.setEditable(true);
						}
					}
				}
			}
		});
		
		tbSalesDtl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesDtl = new SalesDtl();
					salesDtl = new SalesDtl();
					salesDtl.setId(newSelection.getId());
					if (null != newSelection.getOfferReferenceId()) {
						salesDtl.setOfferReferenceId(newSelection.getOfferReferenceId());
					}

					if (null != newSelection.getSchemeId()) {
						salesDtl.setSchemeId(newSelection.getSchemeId());
					}
					salesDtl.setItemId(newSelection.getItemId());
					salesDtl.setSalesTransHdr(newSelection.getSalesTransHdr());
					salesDtl.setQty(newSelection.getQty());
					salesDtl.setUnitId(newSelection.getUnitId());
					salesDtl.setUnitName(newSelection.getUnitName());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));
				}
			}
		});
		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				System.out.println("Supplier Search changed");
				loadItemSearch((String) newValue);
			}
		});
    }
    
    
    private void drawButton()
    {
    	ResponseEntity<List<CategoryMst>> getParentCategory = null;
    	 ObservableList<CategoryMst> catArrayList = FXCollections.observableArrayList();

    	ResponseEntity<List<CategoryManagementMst>>categoryManagementMstList=null;
    if(categoryName.equalsIgnoreCase(""))
    {
    	getParentCategory = RestCaller.getParentCategory();
    	catArrayList = FXCollections.observableArrayList(getParentCategory.getBody());
    }
    else
    {
    	categoryManagementMstList = RestCaller.getChildCategoryManagementMst(categoryName);
////    	if(categoryManagementMstList.getBody().size()==0)
////    	{
//    	getParentCategory = RestCaller.getChildCategory(categoryName);
////    	}
    }
    if(null !=categoryManagementMstList )
    {
    for(CategoryManagementMst cat:categoryManagementMstList.getBody())
    {
    	ResponseEntity<CategoryMst> getcat = RestCaller.getCategoryById(cat.getCategoryId());
    	catArrayList.add(getcat.getBody());
    }
    }
	int j =1;
	int k =0;
	int rowNo =0;
	for(int i =0;i<catArrayList.size();i++)
 	{
 		k=k+1;
 		//int colNo = getTable.getBody().size()/5;
 		if(k==8)
 		{
 			k=1;
 			j=j+3;
 			rowNo = 0;
 		}
 		String s =catArrayList.get(i).getCategoryName();
	  button1 = new Button(catArrayList.get(i).getCategoryName()); 
	  gridpane.setMinSize(400, 200); 
	 button1.setMinHeight(50);
	 button1.setPrefWidth(200);
	 button1.setMaxHeight(75);
	 button1.setMinHeight(75);
	 button1.setMinWidth(75);
	 button1.setMaxHeight(75);
	 button1.setMaxWidth(150);
	 gridpane.setPadding(new Insets(10, 10, 10, 10)); 
	 gridpane.setVgap(5); 
	 gridpane.setHgap(5);       
	 gridpane.setAlignment(Pos.CENTER); 
    
	 gridpane.add(button1,rowNo++,j); 
	 buttonList.add(button1);
 	
     button1.setOnMouseClicked((new EventHandler<MouseEvent>() { 
         public void handle(MouseEvent event) { 

            button1=(Button) event.getSource();
             categoryName = button1.getText();
//            int cr =rowNo;
            for(int i=0;i<gridpane.getChildren().size();i++)
            {
            gridpane.getChildren().removeAll(buttonList);
            }
            if(!categoryName.equalsIgnoreCase(""))
    		{
//    		ResponseEntity<List<CategoryMst>> getChildCategory = RestCaller.getChildCategory(categoryName);
//            if(getChildCategory.getBody().size()>0)
//            {
            	drawButton();
//            }
//            else
//            {
            	ArrayList items = new ArrayList();
            	items = RestCaller.searchStockItemByCategory(categoryName);
            	String itemName = "";
        		String barcode = "";
        		Double mrp = 0.0;
        		String unitname = "";
        		Double qty = 0.0;
        		Double cess = 0.0;
        		Double tax = 0.0;
        		String itemId = "";
        		String unitId = "";
        		String itemCode="";
        		Iterator itr = items.iterator();
        		String batch = "";
        		String expdate;
        		String itemPriceLock;

        		while (itr.hasNext()) {

        			List element = (List) itr.next();
        			itemName = (String) element.get(0);
        			barcode = (String) element.get(1);
        			unitname = (String) element.get(2);
        			mrp = (Double) element.get(3);
        			qty = (Double) element.get(4);
        			cess = (Double) element.get(5);
        			tax = (Double) element.get(6);
        			itemId = (String) element.get(7);
        			unitId = (String) element.get(8);
        			batch = (String) element.get(9);
        			itemCode = (String) element.get(10);
        			expdate = (String) element.get(11);
        			itemPriceLock =  (String) element.get(12);
        			if (null != itemName) {
        				ItemStockPopUp itemStockPopUp = new ItemStockPopUp();
        				itemStockPopUp.setBarcode((String) barcode);
        				itemStockPopUp.setCess(null == cess ? 0 : cess);
        				itemStockPopUp.setItemId(null == itemId ? "" : itemId);
        				itemStockPopUp.setItemName(null == itemName ? "" : itemName);
        				itemStockPopUp.setTax(null == tax ? 0 : tax);
        				itemStockPopUp.setUnitId(null == unitId ? "" : unitId);
        				itemStockPopUp.setUnitname(null == unitname ? "" : unitname);
        				itemStockPopUp.setQty(null == qty ? 0 : qty);
        				itemStockPopUp.setBatch(null == batch ? "" : batch);
        				itemStockPopUp.setMrp(null == mrp ? 0 : mrp);
        				itemStockPopUp.setItemcode((String) itemCode);
        				itemStockPopUp.setitemPriceLock(null == itemPriceLock ? "" : itemPriceLock);
        				if(null != expdate)
        				{
        				//String expdateS = SystemSetting.UtilDateToString(expdate);
        					Date udate = SystemSetting.StringToUtilDate(expdate, "yyyy-MM-dd");
        				LocalDate ldate=SystemSetting.utilToLocaDate(udate);
        				itemStockPopUp.setexpiryDate(java.sql.Date.valueOf(ldate));
        				}
        				itemList.add(itemStockPopUp);

        			}
        		}
            	fillTable();
            }
    		}
//         }
         }));
 	}
	}
    
    private void fillTable()
    {
    	tblItems.setItems(itemList);
		clItemBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		clItemCode.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clItemStdPrice.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clItemQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    }
    @FXML
    void actionSave(ActionEvent event) {
    	

    	categorywiseItemSearchEvent = new CategorywiseItemSearchEvent();
    	categorywiseItemSearchEvent.setSalesTransHdrId(salesTransHdr);


        	eventBus.post(categorywiseItemSearchEvent);
    	Stage stage = (Stage) btnSave.getScene().getWindow();
    	salesTransHdr = null;
		stage.close();

    }

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	
	private void FillTable() {

		tbSalesDtl.setItems(saleListTable);
		clSalesItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clSalesQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clSalesTax.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clSalesRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clSalesBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		clSalesAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Summary summary = null;
		
		
		if(null==salesTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}
		if(salesTransHdr.getAccountHeads().getCustomerDiscount()>0)
		{
			 summary = RestCaller.getSalesWindowSummaryDiscount(salesTransHdr.getId());
		}
		else
		{
		 summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
		}
		if (null != summary.getTotalAmount()) {
			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtTotalAmount.setText(bdCashToPay.toPlainString());
		} else {
			txtTotalAmount.setText("");
		}
	}

  private void loadItemSearch(String data)
  {
	  itemList.clear();
	  tblItems.getItems().clear();
	  ArrayList items = new ArrayList();
  	items = RestCaller.SearchStockItemByNameWithCompWithoutZero(data);
  	String itemName = "";
		String barcode = "";
		Double mrp = 0.0;
		String unitname = "";
		Double qty = 0.0;
		Double cess = 0.0;
		Double tax = 0.0;
		String itemId = "";
		String unitId = "";
		String itemCode="";
		Iterator itr = items.iterator();
		String batch = "";
		String expdate;
		String itemPriceLock;

		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			barcode = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			qty = (Double) element.get(4);
			cess = (Double) element.get(5);
			tax = (Double) element.get(6);
			itemId = (String) element.get(7);
			unitId = (String) element.get(8);
			batch = (String) element.get(9);
			itemCode = (String) element.get(10);
			expdate = (String) element.get(11);
			itemPriceLock =  (String) element.get(12);
			if (null != itemName) {
				ItemStockPopUp itemStockPopUp = new ItemStockPopUp();
				itemStockPopUp.setBarcode((String) barcode);
				itemStockPopUp.setCess(null == cess ? 0 : cess);
				itemStockPopUp.setItemId(null == itemId ? "" : itemId);
				itemStockPopUp.setItemName(null == itemName ? "" : itemName);
				itemStockPopUp.setTax(null == tax ? 0 : tax);
				itemStockPopUp.setUnitId(null == unitId ? "" : unitId);
				itemStockPopUp.setUnitname(null == unitname ? "" : unitname);
				itemStockPopUp.setQty(null == qty ? 0 : qty);
				itemStockPopUp.setBatch(null == batch ? "" : batch);
				itemStockPopUp.setMrp(null == mrp ? 0 : mrp);
				itemStockPopUp.setItemcode((String) itemCode);
				itemStockPopUp.setitemPriceLock(null == itemPriceLock ? "" : itemPriceLock);
				if(null != expdate)
				{
				//String expdateS = SystemSetting.UtilDateToString(expdate);
					Date udate = SystemSetting.StringToUtilDate(expdate, "yyyy-MM-dd");
				LocalDate ldate=SystemSetting.utilToLocaDate(udate);
				itemStockPopUp.setexpiryDate(java.sql.Date.valueOf(ldate));
				}
				itemList.add(itemStockPopUp);

			}
		}
  	fillTable();
  }
  @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


     private void PageReload() {
     	
}

}
