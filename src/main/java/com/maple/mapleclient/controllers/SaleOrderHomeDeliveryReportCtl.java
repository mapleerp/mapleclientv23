package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SaleOrderReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class SaleOrderHomeDeliveryReportCtl {
	
	String taskid;
	String processInstanceId;
	
	private ObservableList<SaleOrderReport> saleOrderListTable = FXCollections.observableArrayList();


    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnGenerate;

    @FXML
    private TableView<SaleOrderReport> tblReport;

    @FXML
    private TableColumn<SaleOrderReport, String> clOrderNumber;

    @FXML
    private TableColumn<SaleOrderReport, String> clCustomer;

    @FXML
    private TableColumn<SaleOrderReport, String> clPhone;

    @FXML
    private TableColumn<SaleOrderReport, String> clType;

    @FXML
    private TableColumn<SaleOrderReport, String> clDueTime;

    @FXML
    private TableColumn<SaleOrderReport, String> clItem;

    @FXML
    private TableColumn<SaleOrderReport, Number> clTotal;

    @FXML
    private TableColumn<SaleOrderReport, Number> clAdvance;

    @FXML
    private TableColumn<SaleOrderReport, Number> clBalance;
    @FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		
    }
    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpDate.getValue())
    	{
    		notifyMessage(3, "Please select date...!", false);
    		return;
    	}
    	
    	
    	Date date = SystemSetting.localToUtilDate(dpDate.getValue());
    	String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
    	ResponseEntity<List<SaleOrderReport>> saleOrderList = RestCaller.SaleOrderHomeDeliveryReport(sdate);

    	saleOrderListTable = FXCollections.observableArrayList(saleOrderList.getBody());
    	FillTable();

    }
    
    private void FillTable() {

		tblReport.setItems(saleOrderListTable);
		
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustNameProperty());
		clAdvance.setCellValueFactory(cellData -> cellData.getValue().getAdvanceProperty());
		clDueTime.setCellValueFactory(cellData -> cellData.getValue().getDueTimeProperty());
		clBalance.setCellValueFactory(cellData -> cellData.getValue().getBalanceProperty());
		clItem.setCellValueFactory(cellData -> cellData.getValue().getItemProperty());
		clOrderNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
		clPhone.setCellValueFactory(cellData -> cellData.getValue().getCustPhnProperty());
		clType.setCellValueFactory(cellData -> cellData.getValue().getTypeProperty());
		clTotal.setCellValueFactory(cellData -> cellData.getValue().getInvoiceProperty());
		clItem.setCellValueFactory(cellData -> cellData.getValue().getItemProperty());

	}

    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }

}
