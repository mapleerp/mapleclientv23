package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPropertyConfig;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.events.ItemPropertyInstance;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ItemPropertyIntent;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ItemPropertyIntentCtl {

	private ObservableList<ItemPropertyConfig> ItemPropertyConfigList = FXCollections.observableArrayList();
	private ObservableList<ItemPropertyIntent> itemPropertyInstanceList = FXCollections.observableArrayList();

	ItemPropertyConfig itempropertyConfig = null;
	ItemPropertyIntent itemPropertyIntent = null;
	
	int propertyInstancePosition = -1;
	
	private EventBus eventBus = EventBusFactory.getEventBus();


	@FXML
	private TableView<ItemPropertyIntent> tblPropertyInstance;

	@FXML
	private TableColumn<ItemPropertyIntent, String> clProperty;

	@FXML
	private TableColumn<ItemPropertyIntent, String> clValue;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtProperty;

	@FXML
	private TextField txtValue;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnClose;

	@FXML
	private TableView<ItemPropertyConfig> tbPropertyConfig;

	@FXML
	private TableColumn<ItemPropertyConfig, String> clPropertyConfig;
	
	@FXML
	private void initialize() {
		
		eventBus.register(this);
		
		tbPropertyConfig.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getId()) {
					itempropertyConfig = new ItemPropertyConfig();
					txtProperty.setText(newSelection.getPropertyName());
				}

			}

		});
		
		tblPropertyInstance.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getPropertyName()) {
					
					propertyInstancePosition = tblPropertyInstance.getSelectionModel().getSelectedIndex();
					System.out.println("propertyInstancePosition"+propertyInstancePosition);
				}

			}

		});


	}

	@FXML
	void actionClose(ActionEvent event) {
		
		eventBus.post(itemPropertyInstanceList);
		Stage stage = (Stage) btnClose.getScene().getWindow();
		stage.close();
	}

	@FXML
	void actionDelete(ActionEvent event) {
		
	
		if(propertyInstancePosition > -1)
		{
			itemPropertyInstanceList.remove(propertyInstancePosition);

			fillTableItemProperty();
		}
	}

	@FXML
	void actionSave(ActionEvent event) {
		
		itemPropertyIntent = new ItemPropertyIntent();
		
		itemPropertyIntent.setPropertyName(txtProperty.getText());
		itemPropertyIntent.setValue(txtValue.getText());
		
		itemPropertyInstanceList.add(itemPropertyIntent);
		fillTableItemProperty();
		
		txtProperty.clear();
		txtValue.clear();

	}

	private void fillTableItemProperty() {
		tblPropertyInstance.setItems(itemPropertyInstanceList);
		clProperty.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());

	}

	public void setItemProperties(String itemId) {

		ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(itemId);
		ItemMst itemMst = itemMstResp.getBody();
		txtItemName.setText(itemMst.getItemName());

		ResponseEntity<List<ItemPropertyConfig>> itempropertylistList = RestCaller
				.getItemPropertyConfigByItemId(itemId);
		ItemPropertyConfigList = FXCollections.observableArrayList(itempropertylistList.getBody());
		fillTable();
	}

	private void fillTable() {
		
		tbPropertyConfig.setItems(ItemPropertyConfigList);
		clPropertyConfig.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

}
