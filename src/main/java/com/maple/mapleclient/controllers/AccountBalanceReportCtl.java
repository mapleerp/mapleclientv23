package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;
import com.maple.report.entity.BranchAssetReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class AccountBalanceReportCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<BranchAssetReport> accountBalanceReportList = FXCollections.observableArrayList();


    @FXML
    private Button btnCustomerReport;

    @FXML
    private Button btnSupplier;

    @FXML
    private DatePicker dpDate;

    @FXML
    private TableView<BranchAssetReport> tblReport;

    @FXML
    private TableColumn<BranchAssetReport, String> clAccount;

    @FXML
    private TableColumn<BranchAssetReport, Number> clBalance;

    @FXML
    private TextField txtBalance;
    
    @FXML
    private Button btnBankReport;

    @FXML
    void CustomerReport(ActionEvent event) {
    	
    	if (null == dpDate.getValue()) {
    		notifyMessage(3, "Please select date", false);
    		return;
		}
    	
    	Date sdate = SystemSetting.localToUtilDate(dpDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<BranchAssetReport>> accountBalanceResp = RestCaller.getAccountBalanceByDate(strtDate);
    	
    	accountBalanceReportList = FXCollections.observableArrayList(accountBalanceResp.getBody());
    	FillTable();

    }

    private void FillTable() {

    	Double balance = 0.0;
    	tblReport.setItems(accountBalanceReportList);
    	
    	for(BranchAssetReport branchAsset : accountBalanceReportList)
    	{
    		balance = balance + branchAsset.getAmount();
    	}
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
		clBalance.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		
		
		BigDecimal bdCashToPay = new BigDecimal(balance);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		txtBalance.setText(bdCashToPay.toPlainString());
		
	}

	@FXML
    void SupplierReport(ActionEvent event) {
    	
    	if (null == dpDate.getValue()) {
    		notifyMessage(3, "Please select date", false);
    		return;
		}
    	
    	Date sdate = SystemSetting.localToUtilDate(dpDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<BranchAssetReport>> accountBalanceResp = RestCaller.getSupplierAccountBalanceByDate(strtDate);
    	
    	accountBalanceReportList = FXCollections.observableArrayList(accountBalanceResp.getBody());
    	FillTable();
    

    }
	 @FXML
		private void initialize() {
	    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
	 }
    
    @FXML
    void BankReport(ActionEvent event) {
    	
    	if (null == dpDate.getValue()) {
    		notifyMessage(3, "Please select date", false);
    		return;
		}
    	
    	Date sdate = SystemSetting.localToUtilDate(dpDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<BranchAssetReport>> accountBalanceResp = RestCaller.getBankAccountBalanceByDate(strtDate);
    	
    	accountBalanceReportList = FXCollections.observableArrayList(accountBalanceResp.getBody());
    	FillTable();

    }

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload();
			}


	    private void PageReload() {
	    	
	    	
			
		}
}
