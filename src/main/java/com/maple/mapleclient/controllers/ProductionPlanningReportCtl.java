package com.maple.mapleclient.controllers;

import java.math.BigDecimal;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Date;
import java.text.Format;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.ibm.icu.text.SimpleDateFormat;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ProductionDtl;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class ProductionPlanningReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<ProductionDtl> productionDtlList = FXCollections.observableArrayList();
	private ObservableList<ProductionDtlDtl> productionDtlDtlList = FXCollections.observableArrayList();
	String itemName;
	ProductionDtl productionDtl = null;
	ItemMst itemmst = null;
	UnitMst unitmst = null;
    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnShow;
    @FXML
    private Button btnPrintItemwiseRawMaterial;
    @FXML
    private Button btnPrint;

    @FXML
    private TableView<ProductionDtl> tbKit;

    @FXML
    private TableColumn<ProductionDtl, String> clKit;

    @FXML
    private TableColumn<ProductionDtl, String> clBatch;

    @FXML
    private TableColumn<ProductionDtl, Number> clQty;

    @FXML
    private TableView<ProductionDtlDtl> tblRawMaterial;

    @FXML
    private TableColumn<ProductionDtlDtl, String> clMaterial;
    
    @FXML
    private TableColumn<ProductionDtlDtl, Number> clRawMatQty;
    @FXML
    void printItemWise(ActionEvent event) {
    	
    	try {
			JasperPdfReportService.ProductionRawMaterialReportItemWise(productionDtlDtlList,itemName);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    @FXML
    private void initialize() 
    {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    	tbKit.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

				
						productionDtl = new ProductionDtl();
					productionDtl.setId(newSelection.getId());
					itemName = newSelection.getKitname();
					clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
		 			clMaterial.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		 		
		 			clRawMatQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
					ResponseEntity<List<ProductionDtlDtl>> productiondtldtlSaved = RestCaller.getProductionDtlDtl(productionDtl.getId());
							productionDtlDtlList = FXCollections.observableArrayList(productiondtldtlSaved.getBody());
							for (ProductionDtlDtl proddtldtl : productionDtlDtlList)
							{
								try
								{
									BigDecimal bdQty = new BigDecimal(proddtldtl.getQty());
									bdQty = bdQty.setScale(3, BigDecimal.ROUND_CEILING);
									proddtldtl.setQty(bdQty.doubleValue());
								ResponseEntity<UnitMst> respunit = RestCaller.getunitMst(proddtldtl.getUnitId());
								unitmst = respunit.getBody();
							} catch (Exception e) {
								return;
							}
							proddtldtl.setUnitName(unitmst.getUnitName());
							
						}
						tblRawMaterial.setItems(productionDtlDtlList);

						productionDtl = null;
				}
			}
		});
    }
    @FXML
    private TableColumn<ProductionDtlDtl, String> clUnit;

    @FXML
    void print(ActionEvent event) {
     	java.util.Date uDate = Date.valueOf(dpDate.getValue());
     
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);
    	try {
			JasperPdfReportService.ProductionDtlReport(voucherDate, SystemSetting.systemBranch);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void show(ActionEvent event) {
    	tbKit.getItems().clear();
	 	java.util.Date uDate = Date.valueOf(dpDate.getValue());
       
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);
		
		ResponseEntity<List<ProductionDtl>> productionDtlsaved = RestCaller.getKItDtlsbyDate(voucherDate);
		productionDtlList = FXCollections.observableArrayList(productionDtlsaved.getBody());

		if(!productionDtlList.isEmpty())
		{
		for (ProductionDtl proddtl : productionDtlList) {
			ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(proddtl.getItemId());
			itemmst = respitem.getBody();
			proddtl.setKitname(itemmst.getItemName());

		}
		tbKit.setItems(productionDtlList);
		clKit.setCellValueFactory(cellData -> cellData.getValue().getKitnameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
    }

}
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
