package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class CustomiseSalesModeCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<MenuConfigMst> menuConfigMstList = FXCollections.observableArrayList();

	MenuConfigMst menuConfigMst = null;
    @FXML
    private TextField txtMenuName;

    @FXML
    private ComboBox<String> cmbWIndowName;

    @FXML
    private TextField txtSalesMode;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnDelete;

    @FXML
    private TableView<MenuConfigMst> tblMenu;

    @FXML
    private TableColumn<MenuConfigMst, String> clMenuName;

    @FXML
    private TableColumn<MenuConfigMst,String> clSalesMode;
    @FXML
	private void initialize() {
    	cmbWIndowName.getItems().add("kotpos2.fxml");
    	cmbWIndowName.getItems().add("kotpos3_lowresolution.fxml");
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	
    	if(null != menuConfigMst)
    	{
    		if(null != menuConfigMst.getId())
    		{
    			RestCaller.DeleteMenuConfigMst(menuConfigMst.getId());
    			
    			ResponseEntity<List<MenuConfigMst>> menuConfigMstListResp = RestCaller.getAllMenuConfigMst();
    	    	menuConfigMstList = FXCollections.observableArrayList(menuConfigMstListResp.getBody());
    	    	FillTable();
    	    	
    	    	menuConfigMst = null;
    	    	
    		}
    		
    	}

    	
    
    }

    @FXML
    void actionSave(ActionEvent event) {

    	
    	if(txtMenuName.getText().trim().isEmpty())
    	{
    		notifyMessage(3, "Please enter menu name", false);
    		txtMenuName.requestFocus();
    		return;
    	}
    	menuConfigMst = new MenuConfigMst();
    	menuConfigMst.setMenuName(txtMenuName.getText());
    	menuConfigMst.setMenuFxml(cmbWIndowName.getSelectionModel().getSelectedItem());
    	menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
    	menuConfigMst.setCustomiseSalesMode(txtSalesMode.getText());
    	ResponseEntity<MenuConfigMst> menuConfigMstResp = RestCaller.saveMenuConfigMst(menuConfigMst);
    	menuConfigMst = menuConfigMstResp.getBody();
    	ResponseEntity<List<MenuConfigMst>> menuConfigMstListResp = RestCaller.getAllMenuConfigMst();
    	menuConfigMstList = FXCollections.observableArrayList(menuConfigMstListResp.getBody());
    	FillTable();
    	txtMenuName.clear();
    	txtSalesMode.clear();
    	menuConfigMst = null;
    
    }
    private void FillTable() {
		tblMenu.setItems(menuConfigMstList);
		clSalesMode.setCellValueFactory(cellData -> cellData.getValue().getCustomiseSalesModeProperty());
		clMenuName.setCellValueFactory(cellData -> cellData.getValue().getMenuNameProperty());
	}

    @FXML
    void actionShow(ActionEvent event) {

    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }
}
