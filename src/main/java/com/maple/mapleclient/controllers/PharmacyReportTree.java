package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;

public class PharmacyReportTree {
	
	String taskid;
	String processInstanceId;

	String selectedText = null;
	@FXML
	private TreeView<String> treeview;

	@FXML
	void initialize() {


		

		TreeItem rootItem2 = new TreeItem("REPORTS");

		TreeItem bincardreportItem = new TreeItem("BIN CARD REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(bincardreportItem);
		}

		TreeItem branchstockmarginreportItem = new TreeItem("BRANCH STOCK MARGIN REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(branchstockmarginreportItem);
		}

		TreeItem branchstockreportItem = new TreeItem("BRANCH STOCK REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(branchstockreportItem);
		}

		TreeItem consumptiondetailreportItem = new TreeItem("CONSUMPTION DETAIL REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(consumptiondetailreportItem);
		}

		TreeItem consumptionsummaryreportItem = new TreeItem("CONSUMPTION SUMMARY REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(consumptionsummaryreportItem);
		}
		TreeItem dailysalesreportItem = new TreeItem("DAILY SALES REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(dailysalesreportItem);
		}
		TreeItem dayclosingreportItem = new TreeItem("DAY CLOSING REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(dayclosingreportItem);
		}
		
		TreeItem dayendreportItem = new TreeItem("DAY END REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(dayendreportItem);
		}

		TreeItem gstinputdetailreportItem = new TreeItem("GST INPUT DETAIL REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(gstinputdetailreportItem);
		}
		TreeItem gstinputsummaryreportItem = new TreeItem("GST INPUT SUMMARY REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(gstinputsummaryreportItem);
		}
		TreeItem gstoutputdetailreportItem = new TreeItem("GST OUTPUT DETAIL REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(gstoutputdetailreportItem);
		}
		TreeItem gstoutputsummaryreportItem = new TreeItem("GST OUTPUT SUMMARY REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(gstoutputsummaryreportItem);
		}
		TreeItem importpurchasesummaryreportItem = new TreeItem("IMPORT PURCHASE SUMMARY REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(importpurchasesummaryreportItem);
		}
		TreeItem insurancereportfromserverItem = new TreeItem("INSURANCE REPORT FROM SERVER");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(insurancereportfromserverItem);
		}
		TreeItem inverntorystockreportItem = new TreeItem("INVERNTORY STOCK REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(inverntorystockreportItem);
		}
		TreeItem localpurchasedetailItem = new TreeItem("LOCAL PURCHASE DETAIL");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(localpurchasedetailItem);
		}
		TreeItem localpurchasesummaryreportItem = new TreeItem("LOCAL PURCHASE SUMMARY REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(localpurchasesummaryreportItem);
		}
		TreeItem physicalstockvarianceItem = new TreeItem("PHYSICAL STOCK VARIANCE");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(physicalstockvarianceItem);
		}
		TreeItem policereportItem = new TreeItem("POLICE REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(policereportItem);
		}
		TreeItem purchasereturndetailItem = new TreeItem("PURCHASE RETURN DETAIL");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(purchasereturndetailItem);
		}
		TreeItem purchasereturnsummaryItem = new TreeItem("PURCHASE RETURN SUMMARY");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(purchasereturnsummaryItem);
		}
		TreeItem retailsalesdetailItem = new TreeItem("RETAIL SALES DETAIL");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(retailsalesdetailItem);
		}
		TreeItem salesreportcustomerwiseItem = new TreeItem("SALES REPORT-CUSTOMERWISE");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreportcustomerwiseItem);
		}
		TreeItem salesreportdailysalestransactionItem = new TreeItem("SALES REPORTDAILY SALES TRANSACTION");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreportdailysalestransactionItem);
		}
		TreeItem salesreportgroupwisesalesItem = new TreeItem("SALES REPORTGROUPWISE SALES");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreportgroupwisesalesItem);
		}
		TreeItem salesreportgroupwisesalesdetailsItem = new TreeItem("SALES REPORTGROUPWISE SALES DETAILS");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreportgroupwisesalesdetailsItem);
		}
		TreeItem salesreportitemwisesalesreportItem = new TreeItem("SALES REPORTITEMWISE SALES REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreportitemwisesalesreportItem);
		}
		TreeItem salesreportsalesreturnreportItem = new TreeItem("SALES REPORT SALES RETURN REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreportsalesreturnreportItem);
		}
		TreeItem salesreportwriteoffdetailsclientreportItem = new TreeItem("SALES REPORT WRITE OFFDETAILS CLIENT REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreportwriteoffdetailsclientreportItem);
		}
		TreeItem salesreportwriteoffsummaryreportItem = new TreeItem("SALES REPORT WRITE OFF SUMMARY REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreportwriteoffsummaryreportItem);
		}
		TreeItem salesreturndetailItem = new TreeItem("SALES RETURN DETAIL");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreturndetailItem);
		}
		TreeItem salesreturnsummaryItem = new TreeItem("SALES RETURN SUMMARY");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(salesreturnsummaryItem);
		}
		TreeItem shortexpiryreportItem = new TreeItem("SHORT EXPIRY REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(shortexpiryreportItem);
		}
		TreeItem stocktransferindetailItem = new TreeItem("STOCK TRANSFER IN DETAIL");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(stocktransferindetailItem);
		}
		TreeItem stocktransferinsummaryItem = new TreeItem("STOCK TRANSFER IN SUMMARY");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(stocktransferinsummaryItem);
		}
		TreeItem stocktransferoutdetailsItem = new TreeItem("STOCK TRANSFER OUT DETAILS");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(stocktransferoutdetailsItem);
		}
		TreeItem stocktransferoutsummaryItem = new TreeItem("STOCK TRANSFER OUT SUMMARY");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(stocktransferoutsummaryItem);
		}
		TreeItem wholesalesdetailItem = new TreeItem("WHOLE SALES DETAIL");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(wholesalesdetailItem);
		}
		TreeItem wholesalessummaryItem = new TreeItem("WHOLE SALES SUMMARY");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(wholesalessummaryItem);
		}
		TreeItem writeoffdetailsreportItem = new TreeItem("WRITE OFF DETAILS REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(writeoffdetailsreportItem);
		}
		TreeItem writeoffsummaryreportItem = new TreeItem("WRITE OFF SUMMARY REPORT");
		if (SystemSetting.systemBranch.equalsIgnoreCase("PHARMACY_CLOUD")) {

			rootItem2.getChildren().add(writeoffsummaryreportItem);
		}

		treeview.setRoot(rootItem2);

		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();



				if (selectedItem.getValue().equalsIgnoreCase("BIN CARD REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();
					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.bincardreport);

				}

				if (selectedItem.getValue().equalsIgnoreCase("BRANCH STOCK MARGIN REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.branchstockmarginreport);
				}


				if (selectedItem.getValue().equalsIgnoreCase("BRANCH STOCK REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.branchstockreport);

				}

				if (selectedItem.getValue().equalsIgnoreCase("CONSUMPTION DETAIL REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.consumptiondetailreport);

				}

				if (selectedItem.getValue().equalsIgnoreCase("CONSUMPTION SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.consumptionsummaryreport);

				}


				if (selectedItem.getValue().equalsIgnoreCase("DAILY SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.dailysalesreport);

				}

				if (selectedItem.getValue().equalsIgnoreCase("DAY CLOSING REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.dayclosingreport);

				}

				if (selectedItem.getValue().equalsIgnoreCase("DAY END REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.dayendreport);

				}
				if (selectedItem.getValue().equalsIgnoreCase("GST INPUT DETAIL REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.gstinputdetailreport);

				}
				
				if (selectedItem.getValue().equalsIgnoreCase("GST INPUT SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.gstinputsummaryreport);

				}
				if (selectedItem.getValue().equalsIgnoreCase("GST OUTPUT DETAIL REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.gstoutputdetailreport);

				}
				//---------------------------------------------------------------------------------------
				if (selectedItem.getValue().equalsIgnoreCase("GST OUTPUT SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.gstoutputsummaryreport);

				}
				
				
				if (selectedItem.getValue().equalsIgnoreCase("IMPORT PURCHASE SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.importpurchasesummaryreport);

				}if (selectedItem.getValue().equalsIgnoreCase("INSURANCE REPORT FROM SERVER")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.insurancereportfromserver);

				}if (selectedItem.getValue().equalsIgnoreCase("INVERNTORY STOCK REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.inverntorystockreport);

				}if (selectedItem.getValue().equalsIgnoreCase("LOCAL PURCHASE DETAIL")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.localpurchasedetail);

				}if (selectedItem.getValue().equalsIgnoreCase("LOCAL PURCHASE SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.localpurchasesummaryreport);

				}if (selectedItem.getValue().equalsIgnoreCase("PHYSICAL STOCK VARIANCE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.physicalstockvariance);

				}if (selectedItem.getValue().equalsIgnoreCase("POLICE REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.policereport);

				}if (selectedItem.getValue().equalsIgnoreCase("PURCHASE RETURN DETAIL")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.purchasereturndetail);

				}if (selectedItem.getValue().equalsIgnoreCase("PURCHASE RETURN SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.purchasereturnsummary);

				}if (selectedItem.getValue().equalsIgnoreCase("RETAIL SALES DETAIL")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.retailsalesdetail);

				}if (selectedItem.getValue().equalsIgnoreCase("RETAIL SALES SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.retailsalessummary);

				}if (selectedItem.getValue().equalsIgnoreCase("SALES REPORT-CUSTOMERWISE SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreportcustomerwisesalesreport);

				}if (selectedItem.getValue().equalsIgnoreCase("SALES REPORT-DAILY SALES TRANSACTION")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreportdailysalestransaction);

				}if (selectedItem.getValue().equalsIgnoreCase("SALES REPORT-GROUPWISE SALES")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreportgroupwisesales);

				}if (selectedItem.getValue().equalsIgnoreCase("SALES REPORT-GROUPWISE SALES DETAILS")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreportgroupwisesalesdetails);

				}if (selectedItem.getValue().equalsIgnoreCase("SALES REPORT-ITEMWISE SALES REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreportitemwisesalesreport);

				}
				
		

				if (selectedItem.getValue().equalsIgnoreCase("SALES REPORT-SALES RETURN REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreportsalesreturnreport);

				}


				if (selectedItem.getValue().equalsIgnoreCase("SALES REPORT-WRITE OFFDETAILS CLIENT REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreportwriteoffdetailsclientreport);

				}
				//**********************************************************
				if (selectedItem.getValue().equalsIgnoreCase("SALES REPORT-WRITE OFF SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreportwriteoffsummaryreport);

				}
				if (selectedItem.getValue().equalsIgnoreCase("SALES RETURN DETAIL")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreturndetail);

				}
				if (selectedItem.getValue().equalsIgnoreCase("SALES RETURN SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.salesreturnsummary);

				}
				if (selectedItem.getValue().equalsIgnoreCase("SHORT EXPIRY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.shortexpiryreport);

				}
				if (selectedItem.getValue().equalsIgnoreCase("STOCK TRANSFER IN DETAIL")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.stocktransferindetail);

				}
				if (selectedItem.getValue().equalsIgnoreCase("STOCK TRANSFER IN SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.stocktransferinsummary);

				}
				if (selectedItem.getValue().equalsIgnoreCase("STOCK TRANSFER OUT DETAILS")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.stocktransferoutdetails);

				}
				if (selectedItem.getValue().equalsIgnoreCase("STOCK TRANSFER OUT SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.stocktransferoutsummary);

				}
				if (selectedItem.getValue().equalsIgnoreCase("WHOLE SALES DETAIL")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.wholesalesdetail);

				}
				if (selectedItem.getValue().equalsIgnoreCase("WHOLE SALES SUMMARY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.wholesalessummary);

				}
				if (selectedItem.getValue().equalsIgnoreCase("WRITE OFF DETAILS REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.writeoffdetailsreport);

				}
				if (selectedItem.getValue().equalsIgnoreCase("WRITE OFF SUMMARY REPORT")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.writeoffsummaryreport);

				}
				
				

			}

		});

	}

	@FXML
	void cofigOnClick(MouseEvent event) {
		if (null != selectedText) {
	


			if (selectedText.equalsIgnoreCase("BRANCH STOCK REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.branchstockreport);

			}


			if (selectedText.equalsIgnoreCase("BIN CARD REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.bincardreport);

			}
			if (selectedText.equalsIgnoreCase("BRANCH STOCK MARGIN REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.branchstockmarginreport);

			}
			
			if (selectedText.equalsIgnoreCase("CONSUMPTION DETAIL REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.consumptiondetailreport);

			}

			if (selectedText.equalsIgnoreCase("CONSUMPTION SUMMARY REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.consumptionsummaryreport);

			}


			if (selectedText.equalsIgnoreCase("DAILY SALES REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.dailysalesreport);

			}

			if (selectedText.equalsIgnoreCase("DAY CLOSING REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.dayclosingreport);

			}

			if (selectedText.equalsIgnoreCase("DAY END REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.dayendreport);

			}
			if (selectedText.equalsIgnoreCase("GST INPUT DETAIL REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.gstinputdetailreport);

			}
			
			if (selectedText.equalsIgnoreCase("GST INPUT SUMMARY REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.gstinputsummaryreport);

			}
			if (selectedText.equalsIgnoreCase("GST OUTPUT DETAIL REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.gstoutputdetailreport);

			}
			//---------------------------------------------------------------------------------------
			if (selectedText.equalsIgnoreCase("GST OUTPUT SUMMARY REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.gstoutputsummaryreport);

			}
			
			
			if (selectedText.equalsIgnoreCase("IMPORT PURCHASE SUMMARY REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.importpurchasesummaryreport);

			}if (selectedText.equalsIgnoreCase("INSURANCE REPORT FROM SERVER")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.insurancereportfromserver);

			}if (selectedText.equalsIgnoreCase("INVERNTORY STOCK REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.inverntorystockreport);

			}if (selectedText.equalsIgnoreCase("LOCAL PURCHASE DETAIL")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.localpurchasedetail);

			}if (selectedText.equalsIgnoreCase("LOCAL PURCHASE SUMMARY REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.localpurchasesummaryreport);

			}if (selectedText.equalsIgnoreCase("PHYSICAL STOCK VARIANCE")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.physicalstockvariance);

			}if (selectedText.equalsIgnoreCase("POLICE REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.policereport);

			}if (selectedText.equalsIgnoreCase("PURCHASE RETURN DETAIL")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.purchasereturndetail);

			}if (selectedText.equalsIgnoreCase("PURCHASE RETURN SUMMARY")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.purchasereturnsummary);

			}if (selectedText.equalsIgnoreCase("RETAIL SALES DETAIL")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.retailsalesdetail);

			}if (selectedText.equalsIgnoreCase("RETAIL SALES SUMMARY")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.retailsalessummary);

			}if (selectedText.equalsIgnoreCase("SALES REPORT-CUSTOMERWISE SALES REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreportcustomerwisesalesreport);

			}if (selectedText.equalsIgnoreCase("SALES REPORT-DAILY SALES TRANSACTION")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreportdailysalestransaction);

			}if (selectedText.equalsIgnoreCase("SALES REPORT-GROUPWISE SALES")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreportgroupwisesales);

			}if (selectedText.equalsIgnoreCase("SALES REPORT-GROUPWISE SALES DETAILS")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreportgroupwisesalesdetails);

			}if (selectedText.equalsIgnoreCase("SALES REPORT-ITEMWISE SALES REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreportitemwisesalesreport);

			}
			
	

			if (selectedText.equalsIgnoreCase("SALES REPORT-SALES RETURN REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreportsalesreturnreport);

			}


			if (selectedText.equalsIgnoreCase("SALES REPORT-WRITE OFFDETAILS CLIENT REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreportwriteoffdetailsclientreport);

			}
			//**********************************************************
			if (selectedText.equalsIgnoreCase("SALES REPORT-WRITE OFF SUMMARY REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreportwriteoffsummaryreport);

			}
			if (selectedText.equalsIgnoreCase("SALES RETURN DETAIL")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreturndetail);

			}
			if (selectedText.equalsIgnoreCase("SALES RETURN SUMMARY")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.salesreturnsummary);

			}
			if (selectedText.equalsIgnoreCase("SHORT EXPIRY REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.shortexpiryreport);

			}
			if (selectedText.equalsIgnoreCase("STOCK TRANSFER IN DETAIL")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.stocktransferindetail);

			}
			if (selectedText.equalsIgnoreCase("STOCK TRANSFER IN SUMMARY")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.stocktransferinsummary);

			}
			if (selectedText.equalsIgnoreCase("STOCK TRANSFER OUT DETAILS")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.stocktransferoutdetails);

			}
			if (selectedText.equalsIgnoreCase("STOCK TRANSFER OUT SUMMARY")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.stocktransferoutsummary);

			}
			if (selectedText.equalsIgnoreCase("WHOLE SALES DETAIL")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.wholesalesdetail);

			}
			if (selectedText.equalsIgnoreCase("WHOLE SALES SUMMARY")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.wholesalessummary);

			}
			if (selectedText.equalsIgnoreCase("WRITE OFF DETAILS REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.writeoffdetailsreport);

			}
			if (selectedText.equalsIgnoreCase("WRITE OFF SUMMARY REPORT")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.writeoffsummaryreport);

			}
			
			
			
			

		}
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}


}
