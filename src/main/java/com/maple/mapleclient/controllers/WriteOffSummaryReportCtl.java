package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.WriteOffDetailAndSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class WriteOffSummaryReportCtl {

	
	private ObservableList<WriteOffDetailAndSummaryReport> writeOffSummaryReportList = FXCollections.observableArrayList();
	
	@FXML
    private DatePicker dpFrom;

    @FXML
    private DatePicker dpTo;

    @FXML
    private Button btnGenerate;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnClear;

    @FXML
    private TableView<WriteOffDetailAndSummaryReport> tblWriteOffSummary;

    @FXML
    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnVoucher;

    @FXML
    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnDate;

    @FXML
    private TableColumn<WriteOffDetailAndSummaryReport, Number> clmnAmount;

    @FXML
    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnDepartment;

    @FXML
    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnUser;

    @FXML
    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnRemarks;
	
    
    @FXML
   	private void initialize() {
       	dpFrom = SystemSetting.datePickerFormat(dpFrom, "dd/MMM/yyyy");
       	dpTo = SystemSetting.datePickerFormat(dpTo, "dd/MMM/yyyy");
    }
    
    @FXML
    void clear(ActionEvent event) {
    	clearField();
    }

    private void clearField() {
    	dpFrom.setValue(null);
    	dpTo.setValue(null);
    	tblWriteOffSummary.getItems().clear();
	}

	@FXML
    void generateReport(ActionEvent event) {
		if(null == dpFrom.getValue())
    	{
    		notifyMessage(5, "Please select From date", false);
    		return;
    		
    	} else if (null == dpTo.getValue()) {
    		notifyMessage(5, "Please select To date", false);
    		return;
		} else {
					    	
		Date fromDate = SystemSetting.localToUtilDate(dpFrom.getValue());
		String fDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
		
		
		Date toDate = SystemSetting.localToUtilDate(dpTo.getValue());
		String tDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
		
		String reason = "Write off";
		
		ResponseEntity<List<WriteOffDetailAndSummaryReport>> damageEntryReport = RestCaller.getWriteOffSummary(fDate, tDate,
				SystemSetting.systemBranch,reason);

		writeOffSummaryReportList = FXCollections.observableArrayList(damageEntryReport.getBody());
		
		fillTable();
		
		}
    }
	
	private void fillTable() {
		tblWriteOffSummary.setItems(writeOffSummaryReportList);	
		
		clmnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clmnVoucher.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clmnDate.setCellValueFactory(cellData -> cellData.getValue().getDateProperty());
		clmnDepartment.setCellValueFactory(cellData -> cellData.getValue().getDepartmentProperty());
		clmnUser.setCellValueFactory(cellData -> cellData.getValue().getUserProperty());
		clmnRemarks.setCellValueFactory(cellData -> cellData.getValue().getRemarksProperty());
		
		
	}

	private void notifyMessage(int duration, String msg, boolean success) {
    	Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

    @FXML
    void printReport(ActionEvent event) {
    	if(null == dpFrom.getValue())
    	{
    		notifyMessage(5, "Please select From date", false);
    		return;
    		
    	} else if (null == dpTo.getValue()) {
    		notifyMessage(5, "Please select To date", false);
    		return;
		} else {
					    	
		Date fromDate = SystemSetting.localToUtilDate(dpFrom.getValue());
		String fDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
		
		
		Date toDate = SystemSetting.localToUtilDate(dpTo.getValue());
		String tDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
		

    	try {
    		String reason = "Write off";
			JasperPdfReportService.writeOffSummaryReport(fDate,tDate,SystemSetting.systemBranch,reason);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
    }
}
