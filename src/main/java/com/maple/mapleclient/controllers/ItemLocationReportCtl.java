package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemLocationMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ItemLocationReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;

public class ItemLocationReportCtl {
	String taskid;
	String processInstanceId;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ItemLocationMst> itemLocationList = FXCollections.observableArrayList();
	 
	@FXML
	    private Button btnPrint;

    @FXML
    private TextField txtItemName;

    @FXML
    private ComboBox<String> cmbShelf;

    @FXML
    private ComboBox<String> cmbRack;
    @FXML
    private ComboBox<String> cmbFloor;
    @FXML
    private TableView<ItemLocationMst> tblItemLocation;

    @FXML
    private TableColumn<ItemLocationMst, String> clItemName;

    @FXML
    private TableColumn<ItemLocationMst, String> clFloor;

    @FXML
    private TableColumn<ItemLocationMst, String> clShelf;

    @FXML
    private TableColumn<ItemLocationMst, String> clRack;
    @FXML
    private Button btnClear;
    @FXML
    private Button btnShow;
    @FXML
   	private void initialize() {

   		eventBus.register(this);
   		ResponseEntity<List<String>> floorresp = RestCaller.getAllFloor();

		for(int i=0;i<floorresp.getBody().size();i++)
		{
			cmbFloor.getItems().add(floorresp.getBody().get(i));
		}
		ResponseEntity<List<String>> shelfresp = RestCaller.getAllShelf();
		for(int i =0;i<shelfresp.getBody().size();i++)
		{
			cmbShelf.getItems().add(shelfresp.getBody().get(i));
		}
		ResponseEntity<List<String>> rackresp = RestCaller.getAllRack();
		for(int i=0;i<rackresp.getBody().size();i++)
		{
			cmbRack.getItems().add(rackresp.getBody().get(i));
		}
    }
   

    @FXML
    void actionClear(ActionEvent event) {

    	txtItemName.clear();
    	cmbFloor.getSelectionModel().clearSelection();
    	cmbRack.getSelectionModel().clearSelection();
    	cmbShelf.getSelectionModel().clearSelection();
    	tblItemLocation.getItems().clear();
    	
    }

    @FXML
    void actionShow(ActionEvent event) {
    	if(!txtItemName.getText().trim().isEmpty())
    	{
    	ResponseEntity<ItemMst> getitemByName = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	ResponseEntity<List<ItemLocationMst>> getItemLoc = RestCaller.getAllItemLocationMstByItemId(getitemByName.getBody().getId());
    	itemLocationList=FXCollections.observableArrayList(getItemLoc.getBody());
    	fillTable();
    	}
    	else if(txtItemName.getText().trim().isEmpty() && null!= cmbFloor.getSelectionModel().getSelectedItem() && null == cmbShelf.getSelectionModel().getSelectedItem() && null == cmbRack.getSelectionModel().getSelectedItem())
    	{
        	ResponseEntity<List<ItemLocationMst>> getItemLoc = RestCaller.getAllItemLocationMstByFloor(cmbFloor.getSelectionModel().getSelectedItem());
        	itemLocationList=FXCollections.observableArrayList(getItemLoc.getBody());
        	fillTable();
    	}
    	else if(null != cmbFloor.getSelectionModel().getSelectedItem() && null != cmbShelf.getSelectionModel().getSelectedItem() && null == cmbRack.getSelectionModel().getSelectedItem())
    	{
    		ResponseEntity<List<ItemLocationMst>> itemLocationReportList = RestCaller
    				.getItemLocationFloorAndShelf(cmbFloor.getSelectionModel().getSelectedItem(),cmbShelf.getSelectionModel().getSelectedItem());
    		itemLocationList=FXCollections.observableArrayList(itemLocationReportList.getBody());
        	fillTable();
    	}
    	else if(null != cmbFloor.getSelectionModel().getSelectedItem() && null != cmbShelf.getSelectionModel().getSelectedItem() && null != cmbRack.getSelectionModel().getSelectedItem())
    	{
    		ResponseEntity<List<ItemLocationMst>> itemLocationReportList = RestCaller
    				.getItemLocationFloorAndShelfAndRack(cmbFloor.getSelectionModel().getSelectedItem(),cmbShelf.getSelectionModel().getSelectedItem(),cmbRack.getSelectionModel().getSelectedItem());
    		itemLocationList=FXCollections.observableArrayList(itemLocationReportList.getBody());
        	fillTable();
    	}
    }
    private void fillTable()
    {
    	for(ItemLocationMst itemLoc : itemLocationList)
    	{
    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(itemLoc.getItemId());
    		itemLoc.setItemName(getItem.getBody().getItemName());
    	}
    	tblItemLocation.setItems(itemLocationList);
		clFloor.setCellValueFactory(cellData -> cellData.getValue().getfloorProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clRack.setCellValueFactory(cellData -> cellData.getValue().getrackProperty());
		clShelf.setCellValueFactory(cellData -> cellData.getValue().getshelfProperty());

    }
    @FXML
    void itemPopUp(KeyEvent event) {

    	if(event.getCode()==KeyCode.ENTER)
    	{
    		showPopup();
    	}
    } 
    @FXML
    void actionPrint(ActionEvent event) {
    	if(!txtItemName.getText().trim().isEmpty())
    	{
    	ResponseEntity<ItemMst> getitemByName = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	
    	try {
			JasperPdfReportService.ItemLocationReportbyItemId(getitemByName.getBody().getId());
		} catch (JRException e) {
			e.printStackTrace();
		}
    	}
    	else if(null != cmbFloor.getSelectionModel().getSelectedItem() && null == cmbShelf.getSelectionModel().getSelectedItem() && null == cmbRack.getSelectionModel().getSelectedItem())
    	{
    		try {
    			JasperPdfReportService.ItemLocationReportbyFloor(cmbFloor.getSelectionModel().getSelectedItem());
    		} catch (JRException e) {
    			e.printStackTrace();
    		}
    	}
    	else if(null != cmbFloor.getSelectionModel().getSelectedItem() && null != cmbShelf.getSelectionModel().getSelectedItem() && null != cmbRack.getSelectionModel().getSelectedItem())
    	{
    		try {
    			JasperPdfReportService.ItemLocationReportbyFloorAndShelfAndRack(cmbFloor.getSelectionModel().getSelectedItem(),cmbShelf.getSelectionModel().getSelectedItem(),
    					cmbRack.getSelectionModel().getSelectedItem());
    		} catch (JRException e) {
    			e.printStackTrace();
    		}
    	}
    }

    
    private void showPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnShow.getScene().getWindow();
		if (stage.isShowing()) {
			txtItemName.setText(itemPopupEvent.getItemName());
		}
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
