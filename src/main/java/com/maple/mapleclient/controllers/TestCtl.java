package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class TestCtl {
	
	String taskid;
	String processInstanceId;
 

	public void doTest() {
		
		String taskid;
		String processInstanceId;

		ResponseEntity<ItemMst> items = null;
		SalesTransHdr salesTransHdr = new SalesTransHdr();
		SalesDtl salesDtl = new SalesDtl();
		 

		ObservableList<ItemMst> ItemList =  FXCollections.observableArrayList();
		for (int i = 0; i < 5; i++) {

			salesTransHdr.setBranchCode(SystemSetting.systemBranch);
			salesTransHdr.setCardamount(0.0);
			salesTransHdr.setCardNo("001");
			salesTransHdr.setCashPaidSale(15.2);
			salesTransHdr.setCashPay(45.2);
			salesTransHdr.setCreditOrCash("credit");
			salesTransHdr.setDeliveryBoyId("44");
			salesTransHdr.setInvoiceAmount(55.2);
			salesTransHdr.setVoucherType("test");
			salesTransHdr.setVoucherNumber("test77777");
			Date d = SystemSetting.systemDate;
			salesTransHdr.setVoucherDate(d);
			salesTransHdr.setSodexoAmount(56.1);
			salesTransHdr.setServingTableName("table");
			salesTransHdr.setPaidAmount(898.23);
			salesTransHdr.setItemDiscount(56.2);
			salesTransHdr.setInvoiceDiscount(44.1);
			ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
			salesTransHdr = respentity.getBody();
			ResponseEntity<List<ItemMst>> itemsaved = RestCaller.getItemNames();
			ItemList = FXCollections.observableArrayList(itemsaved.getBody());
			int j = 0;
			for (ItemMst itemmst : ItemList) {
				j++;
				if (j > 10)
					return;
				else {
					items = RestCaller.getItemByNameRequestParam(itemmst.getId());

					salesDtl.setAddCessRate(5.2);
					salesDtl.setBarcode("BCD001");
					salesDtl.setBatch("Batch");
					salesDtl.setCessAmount(55.1);
					salesDtl.setBatchCode("batchcode");
					salesDtl.setCessRate(5.2);
					salesDtl.setUnitName("PKT");
					salesDtl.setTaxRate(5.1);
					salesDtl.setSgstTaxRate(89.3);
					salesDtl.setSgstAmount(5.3);
					salesDtl.setSalesTransHdr(salesTransHdr);
					salesDtl.setRate(7.1);
					salesDtl.setQty(7.2);
					salesDtl.setMrp(2.3);
					salesDtl.setMrp(8.9);
					salesDtl.setItemName(items.getBody().getItemName());
					salesDtl.setItemId(items.getBody().getId());
					salesDtl.setItemCode(items.getBody().getItemCode());
					ResponseEntity<SalesDtl> repentity1 = RestCaller.saveSalesDtl(salesDtl);
				}
			}
 
		}
	}
	
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}
}
