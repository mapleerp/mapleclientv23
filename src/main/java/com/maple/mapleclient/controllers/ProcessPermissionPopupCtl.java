package com.maple.mapleclient.controllers;

import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.Iterator;
import java.util.LinkedHashMap;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.events.ProcessPermissionEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ProcessPermissionPopupCtl {
	
	String taskid;
	String processInstanceId;

	
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<ProcessMst> processPermissionList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

	ProcessPermissionEvent processPermissionEvent;
	 @FXML
	    private TextField txtPermission;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<ProcessMst> tablePopupView;

    @FXML
    private TableColumn<ProcessMst, String> permissionName;

    @FXML
    private Button btnCancel;

    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtPermission.requestFocus();
		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    
    @FXML
	private void initialize() {
    	LoadMenuBySearch("");
		
    	txtPermission.textProperty().bindBidirectional(SearchString);
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		btnCancel.setCache(true);
		
		tablePopupView.setItems(processPermissionList);
		
		permissionName.setCellValueFactory(cellData -> cellData.getValue().getprocessNameProperty());
		
		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
				
					System.out.print("inside processPermissionEvent");
					processPermissionEvent = new ProcessPermissionEvent();
					processPermissionEvent.setId(newSelection.getId());
					processPermissionEvent.setProcessName(newSelection.getProcessName());
					eventBus.post(processPermissionEvent);
					
				}
			}
		});


		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadMenuBySearch((String) newValue);
			}
		});

    }
    
    
	private void LoadMenuBySearch(String searchData) {

		processPermissionList.clear();
		ArrayList menuArray = new ArrayList();
		menuArray = RestCaller.searchProcessPermissionByName(searchData);
		
		System.out.print(menuArray.size()+"process name isssssssssssssssssssssssssssssssssssssssssssssssssss");
		Iterator itr = menuArray.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object processName = lm.get("processName");
			
			System.out.print(processName+"process name isssssssssssssssssssssssssssssssssssssssssssssss");
			Object id = lm.get("id");
			if (null != id) {
				ProcessMst ProcessPermissionMst = new ProcessMst();
				System.out.print("inside the processmst object issssssssssssssssssssssssssssssssssssssssssssss");
				ProcessPermissionMst.setProcessName((String)processName);
				ProcessPermissionMst.setId((String) id);
				processPermissionList.add(ProcessPermissionMst);
			}

		}
		
		

		return;

	}
	 @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }
}
