package com.maple.mapleclient.controllers;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;


import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ReceiptModeWiseReport {
	
	String taskid;
	String processInstanceId;
	

	
	String voucherNumber;
	
	String receiptMode;
	String voucherDate;

	Double receiptAmount;
	
	String receiptAmt;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	

    @JsonIgnore
    private StringProperty  receiptAmtProperty;
	@JsonIgnore
	private StringProperty receiptModeProperty;

	@JsonIgnore
	private StringProperty voucherDateProperty;
	

	@JsonIgnore
	private DoubleProperty receiptAmountProperty;
	
    public ReceiptModeWiseReport() {
    	
    	
	    
    	

		this.voucherNumberProperty = new SimpleStringProperty();
	
		this.receiptModeProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();
		this.receiptAmountProperty=new SimpleDoubleProperty();
		this.receiptAmtProperty=new SimpleStringProperty();
	
	}
	
	







	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}




	public StringProperty setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}















	





	public StringProperty  setReceiptModeProperty(StringProperty receiptModeProperty) {
		receiptModeProperty.set(receiptMode);
		return receiptModeProperty;
		
		
	}






	@JsonIgnore
	public StringProperty getReceiptModeProperty() {
		receiptModeProperty.set(receiptMode);
		return receiptModeProperty;
	}



	public StringProperty setVoucherDateProperty(StringProperty voucherDateProperty) {
		
		
		
		voucherDateProperty.set(voucherDate);
		
		return voucherDateProperty;
	}



	@JsonIgnore
	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(voucherDate);
		return voucherDateProperty;
	}



	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}






	public Double getReceiptAmount() {
		return receiptAmount;
	}






	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}




	@JsonIgnore
	public DoubleProperty getReceiptAmountProperty() {
		receiptAmountProperty.set(receiptAmount);
		return receiptAmountProperty;
	}

	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }




}
