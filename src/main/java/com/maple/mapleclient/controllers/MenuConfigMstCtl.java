package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.ReorderMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;

public class MenuConfigMstCtl {

	String taskid;
	String processInstanceId;

	private ObservableList<MenuConfigMst> menuConfigMstList = FXCollections.observableArrayList();
	MenuConfigMst menuConfigMst = null;

	StringProperty SearchString = new SimpleStringProperty();

	@FXML
	private TextField txtMenuName;

	@FXML
	private TextField txtFxmlName;

	@FXML
	private ComboBox<String> cmbDescription;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private TableView<MenuConfigMst> tblMenuConfig;

	@FXML
	private TableColumn<MenuConfigMst, String> clMenuName;

	@FXML
	private TableColumn<MenuConfigMst, String> clFxmlName;

	@FXML
	private TableColumn<MenuConfigMst, String> clDescription;

	@FXML
	private void initialize() {

		cmbDescription.getItems().add("ACTIONABLE MENU ITEM");
		cmbDescription.getItems().add("SUBACTIONABLE MENU ITEM");

		tblMenuConfig.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					menuConfigMst = new MenuConfigMst();
					menuConfigMst.setId(newSelection.getId());

				}
			}
		});
		txtMenuName.textProperty().bindBidirectional(SearchString);
		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				System.out.println("item Search changed");
				LoadMenuBySearch((String) newValue);
			}
		});

		ResponseEntity<List<MenuConfigMst>> menuConfigMstListResp = RestCaller.getAllMenuConfigMst();
		menuConfigMstList = FXCollections.observableArrayList(menuConfigMstListResp.getBody());
		FillTable();

	}
	

	@FXML
	void DELETE(ActionEvent event) {

		if (null != menuConfigMst) {
			if (null != menuConfigMst.getId()) {
				RestCaller.DeleteMenuConfigMst(menuConfigMst.getId());

				ResponseEntity<List<MenuConfigMst>> menuConfigMstListResp = RestCaller.getAllMenuConfigMst();
				menuConfigMstList = FXCollections.observableArrayList(menuConfigMstListResp.getBody());
				FillTable();

				menuConfigMst = null;

			}

		}

	}

	private void LoadMenuBySearch(String searchData) {

		tblMenuConfig.getItems().clear();
		ArrayList cat = new ArrayList();
		RestTemplate restTemplate = new RestTemplate();
//		searchData = "cust";
		cat = RestCaller.SearchMenuByName(searchData);
		System.out.print(
				cat.size() + "menu list size issssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		Iterator itr = cat.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object id = lm.get("id");
			Object menuName = lm.get("menuName");
			Object menuDescription = lm.get("menuDescription");
			Object menuFxml = lm.get("menuFxml");

			System.out.print(menuName + "menu name issssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");

			if (menuName != null) {
				MenuConfigMst menuConfig = new MenuConfigMst();

				try {
					menuConfig.setId(String.valueOf(id));
					menuConfig.setMenuName(String.valueOf(menuName));

					menuConfig.setMenuDescription(String.valueOf(menuDescription));
					menuConfig.setMenuFxml(String.valueOf(menuFxml));
					menuConfigMstList.add(menuConfig);
					tblMenuConfig.setItems(menuConfigMstList);
					System.out.print(menuConfigMstList.size()
							+ "MENU CONFIG LIST SIZE ISSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
				} catch (Exception e) {
					System.out.println(e.toString());
				}

				FillTable();
			}
		}
	}

	@FXML
	void SAVE(ActionEvent event) {

		if (txtMenuName.getText().trim().isEmpty()) {
			notifyMessage(3, "Please enter menu name", false);
			txtMenuName.requestFocus();
			return;
		}

//    	if(txtFxmlName.getText().trim().isEmpty())
//    	{
//    		notifyMessage(3, "Please enter menu fxml name", false);
//    		txtFxmlName.requestFocus();
//    		return;
//    	}

		if (null == cmbDescription.getValue()) {
			notifyMessage(3, "Please enter description name", false);
			cmbDescription.requestFocus();
			return;
		}

		menuConfigMst = new MenuConfigMst();
		menuConfigMst.setMenuDescription(cmbDescription.getSelectionModel().getSelectedItem().toString());
		if (!txtFxmlName.getText().trim().isEmpty()) {
			menuConfigMst.setMenuFxml(txtFxmlName.getText());

		}
		menuConfigMst.setMenuName(txtMenuName.getText());

		ResponseEntity<MenuConfigMst> menuConfigMstResp = RestCaller.saveMenuConfigMst(menuConfigMst);
		menuConfigMst = menuConfigMstResp.getBody();

		ResponseEntity<List<MenuConfigMst>> menuConfigMstListResp = RestCaller.getAllMenuConfigMst();
		menuConfigMstList = FXCollections.observableArrayList(menuConfigMstListResp.getBody());
		FillTable();

		txtFxmlName.clear();
		txtMenuName.clear();
		cmbDescription.getSelectionModel().clearSelection();
		menuConfigMst = null;
	}

	private void FillTable() {

		tblMenuConfig.setItems(menuConfigMstList);

		clDescription.setCellValueFactory(cellData -> cellData.getValue().getMenuDescriptionProperty());
		clFxmlName.setCellValueFactory(cellData -> cellData.getValue().getMenuFxmlProperty());
		clMenuName.setCellValueFactory(cellData -> cellData.getValue().getMenuNameProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }


}
