package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemLocationMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.SalesTypeMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ItemLocationCtl {
	String taskid;
	String processInstanceId;
	ItemLocationMst itemLocationMst =null;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ItemLocationMst> itemLocationList = FXCollections.observableArrayList();

    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnClear;

    @FXML
    private ComboBox<String> cmbFloor;

    @FXML
    private ComboBox<String> cmbShelf;

    @FXML
    private ComboBox<String> cmbRack;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;

    @FXML
    private ComboBox<String> cmbCategory;

    @FXML
    private TableView<ItemLocationMst> tblItemLocation;

    @FXML
    private TableColumn<ItemLocationMst, String> clItemName;

    @FXML
    private TableColumn<ItemLocationMst, String> clFloor;

    @FXML
    private TableColumn<ItemLocationMst, String> clShelf;

    @FXML
    private TableColumn<ItemLocationMst, String> clRack;
    @FXML
	private void initialize() {

		eventBus.register(this);
		fillComboBox();
		tblItemLocation.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					itemLocationMst = new ItemLocationMst();
					itemLocationMst.setId(newSelection.getId());
				}
			}
		});
    }

    @FXML
    void actionClear(ActionEvent event) {

    	clearFileds();
    	fillComboBox();
    }

    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == itemLocationMst)
    	{
    		return;
    	}
    	if(null == itemLocationMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteItemLocationMst(itemLocationMst.getId());
    	ResponseEntity<List<ItemLocationMst>> getall = RestCaller.getAllItemLocationMst();
    	itemLocationList = FXCollections.observableArrayList(getall.getBody());
    	fillTable();
    			
    	
    }

    private void fillComboBox()
    {
    	ArrayList cat = new ArrayList();
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			
			Object id = lm.get("id");
			if (id != null) {
				CategoryMst category = new CategoryMst();
				cmbCategory.getItems().add((String) categoryName);
				
			}

		}    	
		ResponseEntity<List<String>> floorresp = RestCaller.getAllFloor();

		for(int i=0;i<floorresp.getBody().size();i++)
		{
			cmbFloor.getItems().add(floorresp.getBody().get(i));
		}
		ResponseEntity<List<String>> shelfresp = RestCaller.getAllShelf();
		for(int i =0;i<shelfresp.getBody().size();i++)
		{
			cmbShelf.getItems().add(shelfresp.getBody().get(i));
		}
		ResponseEntity<List<String>> rackresp = RestCaller.getAllRack();
		for(int i=0;i<rackresp.getBody().size();i++)
		{
			cmbRack.getItems().add(rackresp.getBody().get(i));
		}
    }
    @FXML
    void actionSave(ActionEvent event) {
    	if(!txtItemName.getText().trim().isEmpty() && null !=cmbCategory.getSelectionModel().getSelectedItem())
    	{
    		notifyMessage(3,"Select either category or Item");
    		txtItemName.clear();
    		cmbCategory.getSelectionModel().clearSelection();
    		return;
    	}
    	if(txtItemName.getText().trim().isEmpty() && null ==cmbCategory.getSelectionModel())
    	{
    		notifyMessage(3,"Select either category or Item");
    		return;
    	}
    	itemLocationMst = new ItemLocationMst();
    	if(null != cmbFloor.getSelectionModel()) {
    	itemLocationMst.setFloor(cmbFloor.getSelectionModel().getSelectedItem());
    	}
    	if(null != cmbRack.getSelectionModel()) {
    	itemLocationMst.setRack(cmbRack.getSelectionModel().getSelectedItem());
    	}
    	if(null != cmbShelf.getSelectionModel())
    	{
    	itemLocationMst.setShelf(cmbShelf.getSelectionModel().getSelectedItem());
    	}
    	if(!txtItemName.getText().trim().isEmpty())
    	{
    	ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	itemLocationMst.setItemId(getItem.getBody().getId());
    	itemLocationMst.setBranchCode(SystemSetting.systemBranch);
    	
    	ResponseEntity<ItemLocationMst> getItemLoc = RestCaller.getItemLocationMst(itemLocationMst.getItemId(),itemLocationMst.getFloor(),
    			itemLocationMst.getShelf(),itemLocationMst.getRack());
    	if(null == getItemLoc.getBody())
    	{
    		
    	ResponseEntity<ItemLocationMst> respentity = RestCaller.saveItemLocationMst(itemLocationMst);
    	itemLocationMst = respentity.getBody();
    	itemLocationList.add(itemLocationMst);
    	fillTable();
    	clearFileds();
    	fillComboBox();
    	
    	}
    	else
    	{
    		notifyMessage(3,"Already Saved!!!");
    		clearFileds();
    		fillComboBox();
    		return;
    	}
    	}
    	else
    	{
    		ResponseEntity<CategoryMst> getCategoryByName= RestCaller.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem());
    		ResponseEntity<List<ItemMst>> itemList = RestCaller.getItemsByCategory(getCategoryByName.getBody());
    		for(int i=0;i<itemList.getBody().size();i++)
    		{
    			itemLocationMst = new ItemLocationMst();
    	    	if(null != cmbFloor.getSelectionModel()) {
    	    	itemLocationMst.setFloor(cmbFloor.getSelectionModel().getSelectedItem());
    	    	}
    	    	if(null != cmbRack.getSelectionModel()) {
    	    	itemLocationMst.setRack(cmbRack.getSelectionModel().getSelectedItem());
    	    	}
    	    	if(null != cmbShelf.getSelectionModel())
    	    	{
    	    	itemLocationMst.setShelf(cmbShelf.getSelectionModel().getSelectedItem());
    	    	}
    			itemLocationMst.setItemId(itemList.getBody().get(i).getId());
    			ResponseEntity<ItemLocationMst> getItemLoc = RestCaller.getItemLocationMst(itemLocationMst.getItemId(),itemLocationMst.getFloor(),
    	    			itemLocationMst.getShelf(),itemLocationMst.getRack());
    	    	if(null == getItemLoc.getBody())
    	    	{
    	    	ResponseEntity<ItemLocationMst> respentity = RestCaller.saveItemLocationMst(itemLocationMst);
    	    	itemLocationMst = respentity.getBody();
    	    	itemLocationList.add(itemLocationMst);
    	    	
    	    	}
    		}
    		fillTable();
	    	clearFileds();
	    	fillComboBox();
    	}
    	
    	itemLocationMst = null;
    }
    @FXML
    void floorOnEnter(KeyEvent event) {

    	if(event.getCode()==KeyCode.ENTER)
    	{
    		cmbShelf.requestFocus();
    	}
    }
    @FXML
    void shelfOnEnter(KeyEvent event) {
    	if(event.getCode()==KeyCode.ENTER)
    	{
    		cmbRack.requestFocus();
    	}
    }

    private void clearFileds()
    {
    	cmbCategory.getSelectionModel().clearSelection();
    	txtItemName.clear();
    	
    	cmbRack.setPromptText(null);
    	cmbShelf.setPromptText(null);
    	cmbFloor.getSelectionModel().clearSelection();
    	cmbRack.getSelectionModel().clearSelection();
    	cmbShelf.getSelectionModel().clearSelection();
    	cmbFloor.getItems().clear();
    	cmbRack.getItems().clear();
    	cmbShelf.getItems().clear();
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

    private void fillTable()
    {
    	for(ItemLocationMst itemLoc : itemLocationList)
    	{
    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(itemLoc.getItemId());
    		itemLoc.setItemName(getItem.getBody().getItemName());
    	}
    	tblItemLocation.setItems(itemLocationList);
		clFloor.setCellValueFactory(cellData -> cellData.getValue().getfloorProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clRack.setCellValueFactory(cellData -> cellData.getValue().getrackProperty());
		clShelf.setCellValueFactory(cellData -> cellData.getValue().getshelfProperty());

    }
    @FXML
    void actionShowAll(ActionEvent event) {
    	ResponseEntity<List<ItemLocationMst>> getall = RestCaller.getAllItemLocationMst();
    	itemLocationList = FXCollections.observableArrayList(getall.getBody());
    	fillTable();
    			
    }
    @FXML
    void itemPopUp(KeyEvent event) {

    	if(event.getCode()==KeyCode.ENTER)
    	{
    		showPopup();
    	}
    }
	private void showPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			cmbFloor.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {
			txtItemName.setText(itemPopupEvent.getItemName());
		}
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
