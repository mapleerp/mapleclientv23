package com.maple.mapleclient.controllers;

import java.math.BigDecimal;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ProductionBatchStockDtl;
import com.maple.mapleclient.entity.SaleOrderReceipt;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SaleOrderReceiptReport;
import com.maple.report.entity.SaleOrderReport;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.collections.FXCollections;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
public class SaleOrderReceiptCashReportPopUpCtl {
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<SaleOrderReceiptReport> popUpItemList = FXCollections.observableArrayList();

    @FXML
    private TableView<SaleOrderReceiptReport> tbSaleOrderReceipt;

    @FXML
    private TableColumn<SaleOrderReceiptReport, String> clVoucherNumber;

    @FXML
    private TableColumn<SaleOrderReceiptReport, String> clCustomer;
    @FXML
    private TableColumn<SaleOrderReceiptReport, Number> clReceiptAmount;
    @FXML
    private TextField txtTotalAmount;
    @FXML
    private Button btnCancel;

    @FXML
    void actionCancel(ActionEvent event) {
     	Stage stage = (Stage) btnCancel.getScene().getWindow();
		stage.close();
    }
    @FXML
	private void initialize() {
    	eventBus.register(this);
    	tbSaleOrderReceipt.setItems(popUpItemList);
    	clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getsourceVoucherNumberProperty());
    	clReceiptAmount.setCellValueFactory(cellData -> cellData.getValue().getreceiptAmountProperty());
    	clCustomer.setCellValueFactory(cellData -> cellData.getValue().getcustomerNameProperty());

    }
    public void showSaleOrderReceiptReport(String date,String mode)
    {
    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptReport= RestCaller.getSaleOrderReceiptByModeAndDate(date, mode);
    	popUpItemList = FXCollections.observableArrayList(salesOrderReceiptReport.getBody());
    	Double total = 0.0;
    	for(SaleOrderReceiptReport sale:popUpItemList)
    	{
    		if(null != sale.getReceiptAmount())
    		{
    			total = total +  sale.getReceiptAmount();
    					
    		}
    	}
    	BigDecimal bdtotal= new BigDecimal(total);
    	bdtotal = bdtotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalAmount.setText(bdtotal.toPlainString());
    	tbSaleOrderReceipt.setItems(popUpItemList);
    }
    public void showSaleOrderCardReport(String date)
    {
    	Double total = 0.0;
    	
    	
    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptYesBankReport= RestCaller.getSaleOrderReceiptByCard(date);
    	popUpItemList = FXCollections.observableArrayList(salesOrderReceiptYesBankReport.getBody());

    	for(SaleOrderReceiptReport sal:popUpItemList)
    	{
    		total = total + sal.getReceiptAmount();
    		
    	}
    	
    	tbSaleOrderReceipt.setItems(popUpItemList);
    	BigDecimal bdtotal= new BigDecimal(total);
    	bdtotal = bdtotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalAmount.setText(bdtotal.toPlainString());
    }
    
    public void showSaleOrderCashReport(String date,String mode)
    {
    	Double total = 0.0;
    	
    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptYesBankReport= RestCaller.getPreviousAdvanceAmountDtl(date,mode);
    	popUpItemList = FXCollections.observableArrayList(salesOrderReceiptYesBankReport.getBody());

    	for(SaleOrderReceiptReport sal:popUpItemList)
    	{
    		total = total + sal.getReceiptAmount();
    		
    	}
    	
    	tbSaleOrderReceipt.setItems(popUpItemList);
    	BigDecimal bdtotal= new BigDecimal(total);
    	bdtotal = bdtotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalAmount.setText(bdtotal.toPlainString());
    }
    public void showSalesReceiptBymode(String date,String mode)
    {
    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptReport= RestCaller.getSaleReceiptByModeAndDate(date, mode);
    	popUpItemList = FXCollections.observableArrayList(salesOrderReceiptReport.getBody());
    	tbSaleOrderReceipt.setItems(popUpItemList);
    	Double total = 0.0;
    	for(SaleOrderReceiptReport sale:popUpItemList)
    	{
    		if(null != sale.getReceiptAmount())
    		{
    			total = total +  sale.getReceiptAmount();
    					
    		}
    	}
    	BigDecimal bdtotal= new BigDecimal(total);
    	bdtotal = bdtotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalAmount.setText(bdtotal.toPlainString());
    	tbSaleOrderReceipt.setItems(popUpItemList);
    }
    
    /// for credit bill swigy and zomato
    public void showSalesReceiptBymodeByCredit(String date,String accid)
    {
    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptReport= RestCaller.getSaleReceiptByAccidAndDate(date,accid);
    	popUpItemList = FXCollections.observableArrayList(salesOrderReceiptReport.getBody());
    	tbSaleOrderReceipt.setItems(popUpItemList);
    	Double total = 0.0;
    	for(SaleOrderReceiptReport sale:popUpItemList)
    	{
    		if(null != sale.getReceiptAmount())
    		{
    			total = total +  sale.getReceiptAmount();
    					
    		}
    	}
    	BigDecimal bdtotal= new BigDecimal(total);
    	bdtotal = bdtotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalAmount.setText(bdtotal.toPlainString());
    	tbSaleOrderReceipt.setItems(popUpItemList);
    }
    public void showSalesReceiptByCard(String date)
    {
    	Double total = 0.0;
    	//total = RestCaller.getSumOfSaleOrderReceiptCard(date);
    	
    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptReport= RestCaller.getSaleReceiptByCard(date);
    	popUpItemList = FXCollections.observableArrayList(salesOrderReceiptReport.getBody());
//    	ArrayList<SaleOrderReceiptReport> saleOrderArray = new ArrayList<SaleOrderReceiptReport>();
//    	for(SaleOrderReceiptReport sal:salesOrderReceiptyesBank.getBody())
//    	{
//    		saleOrderArray.add(sal);
//    	}
//    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptSbi= RestCaller.getSaleReceiptByModeAndDate(date, "SBI");
//    	for(SaleOrderReceiptReport sal:salesOrderReceiptSbi.getBody())
//    	{
//    		saleOrderArray.add(sal);
//    	}
//    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptEbs= RestCaller.getSaleReceiptByModeAndDate(date, "EBS");
//    	for(SaleOrderReceiptReport sal:salesOrderReceiptEbs.getBody())
//    	{
//    		saleOrderArray.add(sal);
//    	}
    	for(SaleOrderReceiptReport sal : popUpItemList)
    	{
    		total = total +sal.getReceiptAmount();
    	
    	}
    	BigDecimal bdtotal= new BigDecimal(total);
    	bdtotal = bdtotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalAmount.setText(bdtotal.toPlainString());
    	tbSaleOrderReceipt.setItems(popUpItemList);
    }
    
    
    
    public void showSalesPreviousReceiptByCard(String date)
    {
    	Double total = 0.0;
    	//total = RestCaller.getSumOfSaleOrderReceiptCard(date);
    	
    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptReport= RestCaller.getSaleOrderReceiptPreviousCard(date);
    	popUpItemList = FXCollections.observableArrayList(salesOrderReceiptReport.getBody());
//    	ArrayList<SaleOrderReceiptReport> saleOrderArray = new ArrayList<SaleOrderReceiptReport>();
//    	for(SaleOrderReceiptReport sal:salesOrderReceiptyesBank.getBody())
//    	{
//    		saleOrderArray.add(sal);
//    	}
//    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptSbi= RestCaller.getSaleReceiptByModeAndDate(date, "SBI");
//    	for(SaleOrderReceiptReport sal:salesOrderReceiptSbi.getBody())
//    	{
//    		saleOrderArray.add(sal);
//    	}
//    	ResponseEntity<List<SaleOrderReceiptReport>> salesOrderReceiptEbs= RestCaller.getSaleReceiptByModeAndDate(date, "EBS");
//    	for(SaleOrderReceiptReport sal:salesOrderReceiptEbs.getBody())
//    	{
//    		saleOrderArray.add(sal);
//    	}
    	for(SaleOrderReceiptReport sal : popUpItemList)
    	{
    		total = total +sal.getReceiptAmount();
    	
    	}
    	BigDecimal bdtotal= new BigDecimal(total);
    	bdtotal = bdtotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalAmount.setText(bdtotal.toPlainString());
    	tbSaleOrderReceipt.setItems(popUpItemList);
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }
}
