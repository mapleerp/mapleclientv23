package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.DatePicker;

import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.PurchaseOrderDtl;
import com.maple.mapleclient.entity.PurchaseOrderHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.events.AccountHeadsPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

public class PurchaseOrderManagementCtl {
	
	String taskid;
	String processInstanceId;

	//version1.7 new file
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PurchaseOrderHdr> purchaseList = FXCollections.observableArrayList();
	private ObservableList<PurchaseOrderDtl> purchaseOrderDtlList = FXCollections.observableArrayList();
	private ObservableList<PurchaseDtl> purchaseDtlList = FXCollections.observableArrayList();

	Integer j =0;
	PurchaseOrderHdr purchaseOrderHdr= null;
	PurchaseOrderDtl purchaseOrderDtl = null;
	PurchaseHdr purchaseHdr = null;
	PurchaseDtl purchaseDtl = null;
	ArrayList<String> podtlid = null;

    @FXML
    private DatePicker dpOurVoucherDat;
    @FXML
    private TextField txtSupName;

    @FXML
    private TableView<PurchaseOrderHdr> tbPoNumber;

    @FXML
    private TableColumn<PurchaseOrderHdr, String> clVoucherDate;

    @FXML
    private TableColumn<PurchaseOrderHdr, String> clPoNumber;
    
    @FXML
    private TableColumn<PurchaseOrderHdr, String> clNarration;

    @FXML
    private TableView<PurchaseOrderDtl> tblPoDtl;

    @FXML
    private TableColumn<PurchaseOrderDtl, String> clPOItemName;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clPOQty;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clPORate;

    @FXML
    private TableColumn<PurchaseOrderDtl, Number> clPOAmount;
    @FXML
    private Button btnClosePurchase;
    @FXML
    private Button btnAddtoPurchase;

    @FXML
    private TableView<PurchaseDtl> tblPurchaseDtl;

    @FXML
    private TableColumn<PurchaseDtl, String> clPurchaseItem;

    @FXML
    private TableColumn<PurchaseDtl, Number> clPurchaseQty;

    @FXML
    private TableColumn<PurchaseDtl, Number> clPurchaseRate;

    @FXML
    private TableColumn<PurchaseDtl, Number> clPurchaseAmt;

    @FXML
    private Button btnDeleteFromPurchase;

    @FXML
    private Button btnFinalSave;
    @FXML
  	private void initialize() {
    	eventBus.register(this);
    	
    	dpOurVoucherDat = SystemSetting.datePickerFormat(dpOurVoucherDat, "dd/MMM/yyyy");
    	tblPoDtl.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	podtlid = new ArrayList<String>();
    	tblPoDtl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					for(int i=0;i<podtlid.size();i++)
					{
						if(podtlid.get(i).equalsIgnoreCase(newSelection.getId()))
						{
							podtlid.remove(i);
						}
					}
					podtlid.add(newSelection.getId());

	
							
				}
			}
		});
    	tblPurchaseDtl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					
					purchaseDtl= new PurchaseDtl();
					purchaseDtl.setId(newSelection.getId());
					ResponseEntity<PurchaseDtl> getPur= RestCaller.getPurchaseDtlById(newSelection.getId());
					purchaseDtl = getPur.getBody();
					//purchaseDtl.setPurchaseOrderDtl(newSelection.getPurchaseOrderDtl());
							
				}
			}
		});

    	tbPoNumber.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					purchaseOrderHdr = new PurchaseOrderHdr();
					purchaseOrderHdr.setId(newSelection.getId());
					purchaseOrderHdr.setVoucherNumber(newSelection.getVoucherNumber());

					String vdate = SystemSetting.UtilDateToString(newSelection.getVoucherDate(),"yyyy-MM-dd");
					
					
					ResponseEntity<List<PurchaseOrderDtl>> getPurchaseDtl =RestCaller.getPurchaseOrderDtlByVoucherNoAndDate(purchaseOrderHdr.getVoucherNumber(),vdate);
					
					purchaseOrderDtlList = FXCollections.observableArrayList(getPurchaseDtl.getBody());
					fillPurchaseDtl();		
							
				}
			}
		});
    }
    private void fillPurchaseDtl()
    {
    	tblPoDtl.setItems(purchaseOrderDtlList);
    	for(PurchaseOrderDtl purDtl:purchaseOrderDtlList)
    	{
    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(purDtl.getItemId());
    		purDtl.setItemName(getItem.getBody().getItemName());
    		purDtl.setQty(purDtl.getQty()-purDtl.getReceivedQty());
    	}
    	clPOItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clPOQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clPOAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clPORate.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
    }
    @FXML
    void actionClosePurchase(ActionEvent event) {

    	if(txtSupName.getText().trim().isEmpty())
    	{
    		String s= RestCaller.CloseAllPurchaseOrder();
    	}
    	else
    	{
    		ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadsByName(txtSupName.getText());

    		String s= RestCaller.CloseAllPurchaseOrderBySupId(getAccountHeads.getBody().getId());
    	}
    	
    }
    @FXML
    void actiionFinalSave(ActionEvent event) {
    	
    	
//    	for(PurchaseDtl pur:purchaseDtlList)
//    	{
//    		PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
//    		ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller.getPurchaseOrderDtlById(pur.getPurchaseOrderDtl().getId());
//    		 puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
//    		puchaseOrderDtl.setStatus("CLOSED");
//    		RestCaller.updatePurchaseOrderDtl(purchaseOrderDtl);
//
//    	}
    //	ResponseEntity<List<PurchaseOrderDtl>> getPurchaseDtl =RestCaller.getPurchaseOrderDtlByVoucherNoAndDate(purchaseOrderHdr.getVoucherNumber(),vdate);
		
    	for(PurchaseOrderHdr porderHdr:purchaseList)
    	{
    		String vdate = SystemSetting.UtilDateToString(porderHdr.getVoucherDate(),"yyyy-MM-dd");
			
			
    		
    		ResponseEntity<List<PurchaseOrderDtl>> getPurchaseDtl =RestCaller.getPurchaseOrderDtlByVoucherNoAndDate(porderHdr.getVoucherNumber(),vdate);
    		if(getPurchaseDtl.getBody().size()==0)
    		{
    			porderHdr.setFinalSavedStatus("CLOSED");
				RestCaller.updatePurchaseOrderHdr(porderHdr);

    		}
    	}
    	tblPoDtl.getItems().clear();
    	tblPurchaseDtl.getItems().clear();
    	tbPoNumber.getItems().clear();
    	purchaseDtlList.clear();
    	purchaseList.clear();
    	purchaseOrderDtlList.clear();

    	purchaseHdr=null;
    	txtSupName.clear();
    }

    @FXML
    void actionAddToPurchase(ActionEvent event) {

    	if(null == purchaseHdr)
    	{
    		purchaseHdr = new PurchaseHdr();
    		ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadsByName(txtSupName.getText());
    		purchaseHdr.setSupplierId(getAccountHeads.getBody().getId());
    		
    		purchaseHdr.setSupplierAddress(getAccountHeads.getBody().getPartyAddress1());
    		purchaseHdr.setSupplierName(getAccountHeads.getBody().getAccountName());
    		purchaseHdr.setBranchCode(SystemSetting.systemBranch);
    		purchaseHdr.setCompanyMst(SystemSetting.getUser().getCompanyMst());
    		purchaseHdr.setFinalSavedStatus("N");
//    		Date invDate = Date.valueOf(SystemSetting.systemDate);
//    		
//    		purchaseHdr.setInvoiceDate(Date.valueOf((SystemSetting.UtilDateToString(SystemSetting.systemDate))));
    		purchaseHdr.setourVoucherDate(Date.valueOf(dpOurVoucherDat.getValue()));
    		purchaseHdr.setInvoiceDate(Date.valueOf(dpOurVoucherDat.getValue()));
    		purchaseHdr.setVoucherType("PURCHASETYPE");
    		ResponseEntity<PurchaseHdr> respentity = RestCaller.savePurchaseHdr(purchaseHdr);
    		purchaseHdr = respentity.getBody();
    		
    	}
    	
    	
    	for(int i = 0;i<podtlid.size();i++)
    	{
    		j++;
    		purchaseDtl = new PurchaseDtl();
    		PurchaseOrderDtl purchaseOrderDtl = new PurchaseOrderDtl();
    		ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller.getPurchaseOrderDtlById(podtlid.get(i));
    		purchaseOrderDtl = getPurchaseOrdrDtl.getBody();
    		purchaseOrderDtl.setStatus("CLOSED");
    		//purchaseOrderDtl.setReceivedQty(purchaseOrderDtl.getQty());
    		RestCaller.updatePurchaseOrderDtl(purchaseOrderDtl);
    		purchaseDtl.setPurchaseOrderDtl(podtlid.get(i));
    		purchaseDtl.setAmount(getPurchaseOrdrDtl.getBody().getAmount());
    		purchaseDtl.setBarcode(getPurchaseOrdrDtl.getBody().getBarcode());
    		purchaseDtl.setBatch(getPurchaseOrdrDtl.getBody().getBatch());
    		purchaseDtl.setCessAmt(getPurchaseOrdrDtl.getBody().getCessAmt());
    		purchaseDtl.setCessRate(getPurchaseOrdrDtl.getBody().getCessRate());
    		purchaseDtl.setItemId(getPurchaseOrdrDtl.getBody().getItemId());
    		purchaseDtl.setItemName(getPurchaseOrdrDtl.getBody().getItemName());
    		purchaseDtl.setDiscount(0.0);
    		purchaseDtl.setQty(getPurchaseOrdrDtl.getBody().getQty()-getPurchaseOrdrDtl.getBody().getReceivedQty());
    		purchaseDtl.setPurchseRate(getPurchaseOrdrDtl.getBody().getPurchseRate());
    		purchaseDtl.setTaxAmt(getPurchaseOrdrDtl.getBody().getTaxAmt());
    		purchaseDtl.setTaxRate(getPurchaseOrdrDtl.getBody().getTaxRate());
    		purchaseDtl.setItemSerial(j);
    		purchaseDtl.setMrp(getPurchaseOrdrDtl.getBody().getMrp());
    		purchaseDtl.setUnitId(getPurchaseOrdrDtl.getBody().getUnitId());
    		purchaseDtl.setUnitName(getPurchaseOrdrDtl.getBody().getUnitName());
    		purchaseDtl.setPurchaseHdr(purchaseHdr);
    		purchaseDtl.setBatch("NOBATCH");
    		purchaseDtl.setNetCost(getPurchaseOrdrDtl.getBody().getPurchseRate());
    		ResponseEntity<PurchaseDtl> respentity = RestCaller.savePurchaseDtl(purchaseDtl);
			purchaseDtl = respentity.getBody();
			purchaseDtlList.add(purchaseDtl);
			fillSavedPurchaseDtl();
    	}
    	podtlid = new ArrayList<String>();
    	purchaseDtl= null;
    }
    
    private void fillSavedPurchaseDtl()
    {
    	tblPurchaseDtl.setItems(purchaseDtlList);
//    	for(PurchaseDtl pur:purchaseDtlList)
//    	{
//    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(pur.getItemId());
//    		pur.setItemName(getItem.getBody().getItemName());
//    	}
    	clPurchaseItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clPurchaseAmt.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clPurchaseQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clPurchaseRate.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
    }

    @FXML
    void actionDeletefromPurchase(ActionEvent event) {
    	
    	if(null != purchaseDtl)
    	{
    		if(null != purchaseDtl.getId())
    		{
    			PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
        		ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller.getPurchaseOrderDtlById( purchaseDtl.getPurchaseOrderDtl());
        		 puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
        		puchaseOrderDtl.setStatus("OPEN");
        		RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
    			
    			RestCaller.purchaseDtlDelete(purchaseDtl.getId());
				purchaseDtl = null;
				getPurchaseDtls() ;
    		}
    	}

    }
    private void getPurchaseDtls() {

    	purchaseDtlList.clear();
		ArrayList pur = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		pur = RestCaller.SearchPurchaseDtls(purchaseHdr.getId());
		Iterator itr = pur.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object itemName = lm.get("itemName");
			Object qty = lm.get("qty");
			Object purchseRate = lm.get("purchseRate");
			Object taxAmt = lm.get("taxAmt");
			Object cessAmt = lm.get("cessAmt");
			Object amount = lm.get("amount");
			Object mrp = lm.get("mrp");
			Object netCost = lm.get("netCost");
			Object batch = lm.get("batch");
			Object expiryDate = lm.get("expiryDate");
			Object id = lm.get("id");
			if (id != null) {
				PurchaseDtl purchaseDtl = new PurchaseDtl();
				purchaseDtl.setAmount((Double) amount);
				purchaseDtl.setItemName((String) itemName);
				purchaseDtl.setQty((Double) qty);
				purchaseDtl.setPurchseRate((Double) purchseRate);
				purchaseDtl.setTaxAmt((Double) taxAmt);
				purchaseDtl.setCessAmt((Double) cessAmt);
				purchaseDtl.setMrp((Double) mrp);
				purchaseDtl.setNetCost((Double) netCost);
				purchaseDtl.setId((String) id);
				purchaseDtl.setBatch((String) batch);
				
				purchaseDtl.setexpiryDate(Date.valueOf((String) expiryDate));
				purchaseDtlList.add(purchaseDtl);

			}

		}
		fillSavedPurchaseDtl();
    }

    @Subscribe
	public void popuplistner(AccountHeadsPopupEvent accountHeadsPopupEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) btnAddtoPurchase.getScene().getWindow();
		if (stage.isShowing()) {

			txtSupName.setText(accountHeadsPopupEvent.getPartyName());

			ResponseEntity<List<PurchaseOrderHdr>> getPo = RestCaller.getPurchaseOrderHdrByStatsAndSupId(
					accountHeadsPopupEvent.getPartyId(),"OPEN");
			purchaseList = FXCollections.observableArrayList(getPo.getBody());
			fillPOHdr();

		}

	}
    private void fillPOHdr()
    {
    	tbPoNumber.setItems(purchaseList);
    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
    	clPoNumber.setCellValueFactory(cellData -> cellData.getValue().getPoNumberProperty());
    	clNarration.setCellValueFactory(cellData -> cellData.getValue().getNarrationProperty());



    }
  
    @FXML
    void loadSupplierPopUp(MouseEvent event) {

	try {
		System.out.println("inside the popup");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPartyPopUpCtl.fxml"));
		Parent root = loader.load();
		// PopupCtl popupctl = loader.getController();
		Stage stage = new Stage();
		stage.setScene(new Scene(root));
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
//		dpOurVoucherDate.requestFocus();
	} catch (Exception e) {
		e.printStackTrace();
	}
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
