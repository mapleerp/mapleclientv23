package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PurchaseReturnDetailAndSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PurchaseReturnSummaryReportCtl {
	
	
		private ObservableList<PurchaseReturnDetailAndSummaryReport> purchaseReturnDetailAndSummaryList = FXCollections.observableArrayList();
	
	 	@FXML
	    private DatePicker dpFrom;

	    @FXML
	    private DatePicker dpTo;

	    @FXML
	    private Button btnGenerate;

	    @FXML
	    private Button btnPrint;

	    @FXML
	    private TableView<PurchaseReturnDetailAndSummaryReport> tblPurchaseReturnSummary;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clmnReturnDate;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clmnPurchaseVoucher;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clmnSupplier;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clmnReturnVoucher;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, Number> clmnBeforeGST;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, Number> clmnGST;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, Number> clmnTotal;

	    @FXML
	    private TableColumn<PurchaseReturnDetailAndSummaryReport, String> clmnUser;

	    @FXML
	    void generateReport(ActionEvent event) {
	    	
	    	if(null == dpFrom.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
				return;
	    	}
	    	if(null == dpTo.getValue())
	    	{
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    	}
	    	
	    		
	    		Date fdate = SystemSetting.localToUtilDate(dpFrom.getValue());
	    		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
	    		
	    		Date tdate = SystemSetting.localToUtilDate(dpTo.getValue());
	    		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
	    		
	    		ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> purchaseReturnDetailAndSummary=RestCaller.getPurchaseReturnDetailAndSummary(sfdate,stdate);
	    		purchaseReturnDetailAndSummaryList = FXCollections.observableArrayList(purchaseReturnDetailAndSummary.getBody());
	    		
	    		
	    		
	    		if (purchaseReturnDetailAndSummaryList.size() > 0) {
	    			
	    			
	    			filltable();
	    			dpFrom.getEditor().clear();;
	    			dpTo.getEditor().clear();
	    			
	    		}

	    }

	    private void notifyMessage(int i, String string, boolean b) {
	    	Image img;
			if (b) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}

		private void filltable() {

			tblPurchaseReturnSummary.setItems(purchaseReturnDetailAndSummaryList);
			clmnReturnDate.setCellValueFactory(cellData -> cellData.getValue().getReturnVoucherDateProperty());
			clmnPurchaseVoucher.setCellValueFactory(cellData -> cellData.getValue().getPurchaseReturnVoucherProperty());
			clmnSupplier.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
			clmnReturnVoucher.setCellValueFactory(cellData -> cellData.getValue().getReturnVoucherNumProperty());
			clmnBeforeGST.setCellValueFactory(cellData -> cellData.getValue().getBeforeGSTProperty());
			clmnGST.setCellValueFactory(cellData -> cellData.getValue().getGstProperty());
			clmnTotal.setCellValueFactory(cellData -> cellData.getValue().getTotalProperty());
			clmnUser.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
		}

		@FXML
	    void printReport(ActionEvent event) {
			Date fdate = SystemSetting.localToUtilDate(dpFrom.getValue());
    		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
    		
    		Date tdate = SystemSetting.localToUtilDate(dpTo.getValue());
    		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
    		

			  try { JasperPdfReportService.purchaseReturnDetailAndSummary(sfdate,
					  stdate); } catch (JRException e) { // TODO Auto-gesnerated catch block
			  e.printStackTrace(); }
	    	
	    }

}
