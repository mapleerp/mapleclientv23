package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SundaryDebtorBalanceToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;



public class SundryDebitorBalanceReportCtl {
	
	String taskid;
	String processInstanceId;
	
	SundaryDebtorBalanceToExcel sundaryDebtorBalanceToExcel=new SundaryDebtorBalanceToExcel();
	private ObservableList<AccountBalanceReport> accountBalanceList = FXCollections.observableArrayList();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");


    @FXML
    private DatePicker dpFromDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnExportToExcel;

    @FXML
    private TableView<AccountBalanceReport> tblTrialBalance;

    @FXML
    private TableColumn<AccountBalanceReport, String> clAccount;

    @FXML
    private TableColumn<AccountBalanceReport, Number> clDebitAmount;

    @FXML
    private TableColumn<AccountBalanceReport, Number> clCreditAmount;

    @FXML
    private TextField txtDebitTotal;

    @FXML
    private TextField txtCreditTotal;
    @FXML
   	private void initialize() {
   		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
   		
       }
    @FXML
    void GenerateReport(ActionEvent event) {
    	

		
		if(null == dpFromDate.getValue())
		{
			notifyMessage(2, "pleas select date", false);
			dpFromDate.requestFocus();
			return;
		}
		
		Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		
		accountBalanceList.clear();
		tblTrialBalance.getItems().clear();
    	
    	ArrayList items = new ArrayList();
		items = RestCaller.getSundryDebitorBalance(sdate);

		String account = "";
		Double debitAmount = 0.0;
		Double creditAmount = 0.0;
		Double amount = 0.0;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			account = (String) element.get(0);
			debitAmount = (Double) element.get(2);
			creditAmount = (Double) element.get(1);
			amount = (Double) element.get(3);
			if (null != account) {
				
				AccountBalanceReport accountBalanceReport = new AccountBalanceReport();
				accountBalanceReport.setAccountHeads(account);
				if(amount >=0)
				{
					accountBalanceReport.setDebit(amount);
					accountBalanceReport.setCredit(0.0);
				} else {
					accountBalanceReport.setCredit(amount*-1);
					accountBalanceReport.setDebit(0.0);

				}
				accountBalanceList.add(accountBalanceReport);

			}


    }
		
		fillTable();
    

    }
    
    private void fillTable() {
		tblTrialBalance.setItems(accountBalanceList);
		
		BigDecimal creditTotal = new BigDecimal("0.0");
		BigDecimal  debitTotal = new BigDecimal("0.0");
		for(AccountBalanceReport account : accountBalanceList)
		{
			creditTotal = creditTotal.add(new BigDecimal( account.getCredit()));
			debitTotal = debitTotal.add( new BigDecimal(account.getDebit()));
		}
		
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountHeadsProperty());
		clCreditAmount.setCellValueFactory(cellData -> cellData.getValue().getCreditProperty());

		clDebitAmount.setCellValueFactory(cellData -> cellData.getValue().getDebitProperty());
		clDebitAmount.setCellFactory(tc-> new TableCell<AccountBalanceReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		clCreditAmount.setCellFactory(tc-> new TableCell<AccountBalanceReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		
		//BigDecimal bdDebit = new BigDecimal(debitTotal);
		//BigDecimal bdCredit = new BigDecimal(creditTotal);
		debitTotal=debitTotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		creditTotal=creditTotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		
		
		txtCreditTotal.setText(creditTotal.toPlainString());
		txtDebitTotal.setText(debitTotal.toPlainString());
	}


    @FXML
    void exportToExcel(ActionEvent event) {

    	java.util.Date uDate =  SystemSetting.localToUtilDate(dpFromDate.getValue());
			String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			ArrayList items = new ArrayList();
			items = RestCaller.getSundryDebitorBalance(strDate);
			String account = "";
			Double debitAmount = 0.0;
			Double creditAmount = 0.0;
			Double amount = 0.0;
			Iterator itr = items.iterator();
			while (itr.hasNext()) {

				List element = (List) itr.next();
				account = (String) element.get(0);
				debitAmount = (Double) element.get(2);
				creditAmount = (Double) element.get(1);
	
				System.out.print(account+"account name issssssssssssssssssssssssssssssssssssssss");

			
			ArrayList<AccountBalanceReport> accountBalanceReportList= new ArrayList<AccountBalanceReport>();
			AccountBalanceReport accountBalanceReport=new AccountBalanceReport();
		
    
    
	accountBalanceReport.setAccountHeads(account);
	accountBalanceReport.setDebit(debitAmount);
	accountBalanceReport.setCredit(creditAmount);
	accountBalanceReportList.add(accountBalanceReport);

   System.out.print(accountBalanceReportList.size()+"list size isssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");  

			sundaryDebtorBalanceToExcel.exportToExcel("SundaryDebtorBalance"+strDate+".xls", accountBalanceReportList);
    	
}
	

	
    }

    @FXML
    void tableOnEnter(KeyEvent event) {

    }
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}

}
