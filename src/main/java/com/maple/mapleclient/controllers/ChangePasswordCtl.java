package com.maple.mapleclient.controllers;
 

 
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

	public class ChangePasswordCtl {
		String taskid;
		String processInstanceId;


		UserMst userMst = null;

		   @FXML
		    private PasswordField currentpass;

		    @FXML
		    private PasswordField newpass;

		    @FXML
		    private PasswordField confirmpass;

	    @FXML
	    private Button btnsave;

	    @FXML
	    void save(ActionEvent event) {
	    	
	    	if(currentpass.getText().trim().isEmpty())
	    	{
	    		notifyMessage(3,"Type Current Password");
	    		currentpass.requestFocus();
	    		return;
	    	}
	    	if(newpass.getText().trim().isEmpty())
	    	{
	    		notifyMessage(3,"Type New Password");
	    		newpass.requestFocus();
	    		return;
	    	}
	    	if(confirmpass.getText().trim().isEmpty())
	    	{
	    		notifyMessage(3,"Confirm Password");
	    		confirmpass.requestFocus();
	    		return;
	    	}
	    	if(!newpass.getText().trim().equals(confirmpass.getText().trim()))
	    	{
	    		notifyMessage(3,"Password miss match");
	    		confirmpass.requestFocus();
	    		return;
	    	}
	    	ResponseEntity<UserMst> getUser = RestCaller.getUserNameById(SystemSetting.getUser().getId());
	    	if(null != getUser.getBody())
	    	{
	    	if(currentpass.getText().trim().equals(getUser.getBody().getPassword()))
	    	{
	    		userMst = getUser.getBody();
	    		userMst.setPassword(newpass.getText().trim());
	    		RestCaller.updatePassword(userMst);
	    		notifyMessage(3, "Password Changed Successfully");
	    		currentpass.clear();
	    		newpass.clear();
	    		confirmpass.clear();
	    		
	    		
	    	}
	    	else
	    	{
	    		notifyMessage(3,"Current Password InCorrect");
	    		return;
	    	}
	    	}
	    	
	    }
	    @FXML
	    void currentPasswordOnEnter(KeyEvent event) {

	    	if(event.getCode()==KeyCode.ENTER)
	    	{
	    		newpass.requestFocus();
	    	}
	    }

	    @FXML
	    void newPasswordOnEnter(KeyEvent event) {
	    	if(event.getCode()==KeyCode.ENTER)
	    	{
	    		confirmpass.requestFocus();
	    	}
	    }
	    @FXML
	    void confrmpassOnEnter(KeyEvent event) {
	    	if(event.getCode()==KeyCode.ENTER)
	    	{
	    		btnsave.requestFocus();
	    	}
	    }

	    public void notifyMessage(int duration, String msg) {
			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();

			notificationBuilder.show();
		}
	    @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	     private void PageReload() {
	     	
	}
	}

 
