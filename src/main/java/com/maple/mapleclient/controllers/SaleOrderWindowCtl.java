
package com.maple.mapleclient.controllers;

import java.io.IOException;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;

import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.OrderTakerMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SaleOrderEdit;
import com.maple.mapleclient.entity.SaleOrderReceipt;
import com.maple.mapleclient.entity.SalesOrderDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.SummarySalesOderDtl;
import com.maple.mapleclient.entity.SysDateMst;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.application.Platform;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

public class SaleOrderWindowCtl {
	
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PriceDefinition> priceDefenitionList = FXCollections.observableArrayList();
	private ObservableList<BatchPriceDefinition> BatchpriceDefenitionList = FXCollections.observableArrayList();

	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
	SaleOrderReceipt saleOrderReceipt = new SaleOrderReceipt();
	private ObservableList<SaleOrderReceipt> salesReceiptsList = FXCollections.observableArrayList();
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();

	private ObservableList<SalesOrderTransHdr> holdedSalesDtl = FXCollections.observableArrayList();

	@FXML
	private ComboBox<String> cmbUnit;

	// ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();
	private ObservableList<SalesOrderDtl> saleListTable = FXCollections.observableArrayList();

	SalesOrderDtl salesOrderDtl = null;
	SalesOrderTransHdr salesOrderTransHdr = null;
	LocalCustomerMst localCustomerMst = null;
	SalesOrderDtl saleOrderDelete = null;
	String GSTcustomerId;
	double qtyTotal = 0;
	double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	String custId = "";
	double cardAmount = 0.0;

	String salesReceiptVoucherNo = null;

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty sodexoAmtProperty = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");

	StringProperty barcodeProperty = new SimpleStringProperty("");

	StringProperty taxRateProperty = new SimpleStringProperty("");

	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");
	@FXML
	private TableView<SalesOrderDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnBarCode;

	@FXML
	private Button btnRefresh;
	@FXML
	private TextField txtMessage;
	@FXML
	private TableColumn<SalesOrderDtl, String> columnQty;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnMrp;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnRate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesOrderDtl, Number> columnAmount;
	@FXML
	private TableColumn<SalesOrderDtl, String> columnMessage;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtTax;

	@FXML
	private TextField txtItemcode;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtCashPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField custname;
	@FXML
	private TextField custAdress;
	@FXML
	private TextField gstNo;
	@FXML
	private ComboBox<String> cmbOrderTaker;

	@FXML
	private ComboBox<String> cmbDeliveryBoy;
	@FXML
	private TextField txtinvoicedue;

	@FXML
	private TextField txtorderadvice;

	@FXML
	private TextField txtmsgtoprint;

	@FXML
	private TextField txttargetdept;

	@FXML
	private DatePicker orderDuedate;
	@FXML
	private ComboBox<String> targetdept;

	@FXML
	private TextField txtCustPhoneNo;

	@FXML
	private TextField txtCustPhoneNo2;

	@FXML
	private ComboBox<String> cmbDeliveryMode;

	@FXML
	private TextField txtOrderDueTime;

	@FXML
	private ComboBox<String> cmbDueTime;

	@FXML
	private ComboBox<String> cmbOrderBy;

	@FXML
	private Label lblCardType;

	@FXML
	private Label lblAmount;

	@FXML
	private Button btnCardSale;

	// ----------------------new version 1.4 surya

	@FXML
	private Button btnHold;

	@FXML
	private Button btnUnHold;

	@FXML
	private TableView<SalesOrderTransHdr> tblHoldReport;

	@FXML
	private TableColumn<SalesOrderTransHdr, String> clCustomer;

	@FXML
	private TableColumn<SalesOrderTransHdr, String> clAccount;
	@FXML
    private Button btnclear;
	


	@FXML
	void Hold(ActionEvent event) {

		FillTable();
		
		

		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtTax.setText("");
		txtRate.setText("");
		txtMessage.clear();
		cmbUnit.getItems().clear();
		txtItemcode.setText("");
		txtBarcode.requestFocus();
		salesOrderDtl = new SalesOrderDtl();
		cmbDueTime.getSelectionModel().select("pm");
		lblCardType.setVisible(false);
		lblAmount.setVisible(false);
		cmbCardType.setVisible(false);
		txtCrAmount.setVisible(false);
		AddCardAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		tblCardAmountDetails.setVisible(false);
		txtcardAmount.setVisible(false);

		salesOrderTransHdr = null;
		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtCashPaidamount.setText("");
		txtCustPhoneNo2.clear();
		txtCustPhoneNo.clear();
		custname.setText("");
		custAdress.setText("");
		gstNo.setText("");
		txtorderadvice.setText("");
		txtmsgtoprint.setText("");
		orderDuedate.setValue(null);
		targetdept.setValue(null);
		custId = null;
		txtChangeamount.clear();
		saleListTable.clear();
		targetdept.getSelectionModel().clearSelection();
		txtOrderDueTime.clear();
		cmbDueTime.getSelectionModel().clearSelection();
		cmbOrderTaker.getSelectionModel().clearSelection();
		cmbDeliveryMode.getSelectionModel().clearSelection();
		cmbDeliveryBoy.getSelectionModel().clearSelection();
		cmbOrderBy.getSelectionModel().clearSelection();
		tblCardAmountDetails.getItems().clear();
		amountTotal = 0.0;

		salesOrderDtl = null;

		localCustomerMst = null;
		saleOrderDelete = null;

		getHoldedSaleOrder();

	}

	@FXML
	void UnHold(ActionEvent event) {

		if (null == salesOrderTransHdr) {

			notifyMessage(3, "Please select saleorder", false);
			return;

		}

		if (null == salesOrderTransHdr.getId()) {

			notifyMessage(3, "Please select saleorder", false);
			return;

		}

		ResponseEntity<SalesOrderTransHdr> salesOrderTransHdrResp = RestCaller
				.getSaleOrderTransHdrById(salesOrderTransHdr.getId());
		salesOrderTransHdr = salesOrderTransHdrResp.getBody();

		setSalesOrderDetails(salesOrderTransHdr);

	}

	// ----------------------new version 1.4 surya end

	@FXML
	void VisibleCardSale(ActionEvent event) {

		visibleCardSale();
	}

	@FXML
	void actionHold(ActionEvent event) {

	}

	@FXML
	void actionUnHold(ActionEvent event) {

	}

	private void visibleCardSale() {

		lblCardType.setVisible(true);
		lblAmount.setVisible(true);
		cmbCardType.setVisible(true);
		txtCrAmount.setVisible(true);
		AddCardAmount.setVisible(true);
		btnDeleteCardAmount.setVisible(true);
		tblCardAmountDetails.setVisible(true);
		txtcardAmount.setVisible(true);

	}

	private void getHoldedSaleOrder() {

		ResponseEntity<List<SalesOrderTransHdr>> salesOrderTransHdrResp = RestCaller.getHoldedSalesOrderTransHdr();
		List<SalesOrderTransHdr> salesOrderTransHdrList = salesOrderTransHdrResp.getBody();

		for (SalesOrderTransHdr sale : salesOrderTransHdrList) {
			sale.setLocalCustomerName(sale.getLocalCustomerId().getLocalcustomerName());
			sale.setCustomerName(sale.getAccountHeads().getAccountName());
		}

		for (SalesOrderTransHdr sale : salesOrderTransHdrList) {
			System.out.println("Local Customer==" + sale.getLocalCustomerName());
			System.out.println("Customer==" + sale.getCustomerName());

		}

		holdedSalesDtl = FXCollections.observableArrayList(salesOrderTransHdrList);
		tblHoldReport.setItems(holdedSalesDtl);
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getLocalCustomerNameProperty());
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());

	}

	// -----------------------------------------------------------------------------
	@FXML
	private TableView<SaleOrderReceipt> tblCardAmountDetails;

	@FXML
	private TableColumn<SaleOrderReceipt, String> clCardTye;

	@FXML
	private TableColumn<SaleOrderReceipt, Number> clCardAmount;
	@FXML
	private ComboBox<String> cmbCardType;

	@FXML
	void FocusOnCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCrAmount.requestFocus();
		}
	}

	@FXML
	void FocusOnPhoneNo(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCustPhoneNo.requestFocus();
		}
	}

	@FXML
	void FocusOnPhoneNo2(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCustPhoneNo2.requestFocus();
		}
	}

	@FXML
	void FocusOnGst(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			gstNo.requestFocus();
		}
	}

	@FXML
	void AddItemOnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}

	}

	@FXML
	void setCardType(MouseEvent event) {

		setCardType();
	}

	private void setCardType() {
		cmbCardType.getItems().clear();

		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());

		for (ReceiptModeMst receiptModeMst : receiptModeList) {
			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
				cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
			}
		}
	}

	@FXML
	private TextField txtCrAmount;

	@FXML
	void KeyPressFocusAddBtn(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (txtCrAmount.getText().length() == 0 && null == cmbCardType.getValue()) {
				btnSave.requestFocus();
			} else {

				AddCardAmount.requestFocus();
			}
		}

	}

	@FXML
	private Button AddCardAmount;

	@FXML
	void addCardAmount(ActionEvent event) {

		addCardAmount();

	}

	@FXML
	void KeyPressAddCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			addCardAmount();
		}

	}

	@FXML
	private Button btnDeleteCardAmount;

	@FXML
	void DeleteCardAmount(ActionEvent event) {

		if (null != saleOrderReceipt) {
			if (null != saleOrderReceipt.getId()) {
				RestCaller.deleteSalesOrderReceipts(saleOrderReceipt.getId());
				tblCardAmountDetails.getItems().clear();
				ResponseEntity<List<SaleOrderReceipt>> salesreceiptResp = RestCaller
						.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());
				salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

				filltableCardAmount();

			}
		}
	}

	@FXML
	void KeyPressDeleteCardAmount(KeyEvent event) {

	}

	private void addCardAmount() {

		if (null != salesOrderTransHdr) {

			if (null == cmbCardType.getValue()) {

				notifyMessage(5, "Please select card type!!!!");
				cmbCardType.requestFocus();
				return;

			} else if (txtCrAmount.getText().trim().isEmpty()) {

				notifyMessage(5, "Please enter amount!!!!");
				txtCrAmount.requestFocus();
				return;

			} else {

				saleOrderReceipt = new SaleOrderReceipt();

				if (null == salesReceiptVoucherNo) {
					salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
					saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
				} else {
					saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
				}

				saleOrderReceipt.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());
				saleOrderReceipt.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
				saleOrderReceipt.setUserId(SystemSetting.getUser().getId());
				saleOrderReceipt.setBranchCode(SystemSetting.systemBranch);
				// set account id
				ResponseEntity<AccountHeads> accountHeads = RestCaller
						.getAccountHeadByName(cmbCardType.getSelectionModel().getSelectedItem());
				saleOrderReceipt.setAccountId(accountHeads.getBody().getId());

				// release 1.6

//				LocalDate date = LocalDate.now();
//				java.util.Date udate = SystemSetting.localToUtilDate(date);
				saleOrderReceipt.setRereceiptDate(SystemSetting.getApplicationDate());
				saleOrderReceipt.setSalesOrderTransHdr(salesOrderTransHdr);
				System.out.println(saleOrderReceipt);
				ResponseEntity<SaleOrderReceipt> respEntity = RestCaller.saveSaleOrderReceipts(saleOrderReceipt);
				saleOrderReceipt = respEntity.getBody();

				if (null != saleOrderReceipt) {
					if (null != saleOrderReceipt.getId()) {
//						notifyMessage(5, "Successfully added cardAmount");
//	    				cardAmount = cardAmount+Double.parseDouble(txtCrAmount.getText());
//	    				txtcardAmount.setText(Double.toString(cardAmount));
						salesReceiptsList.add(saleOrderReceipt);
						filltableCardAmount();
					}
				}

				txtCrAmount.clear();
				cmbCardType.getSelectionModel().clearSelection();
				cmbCardType.requestFocus();
				saleOrderReceipt = null;

			}
		} else {
			notifyMessage(5, "Please add Item!!!!");
			return;
		}
	}

	private void filltableCardAmount() {

		tblCardAmountDetails.setItems(salesReceiptsList);
		clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
		clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

		cardAmount = RestCaller.getSumofSalesOrderReceiptbySalesOrdertransHdrId(salesOrderTransHdr.getId());
		txtcardAmount.setText(Double.toString(cardAmount));

	}
	// -------------------------------------------------------------------------------

	@FXML
	void ShowGstCustomerPopUp(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();

		}

	}

	@FXML
	void CustomerPopUpByClick(MouseEvent event) {

		loadCustomerPopup();

	}

	@FXML
	void onEnterCustPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!custname.getText().trim().isEmpty() && custname.getText().length() > 0 && null != custname.getText()) {
				custAdress.requestFocus();
			} else {
				showLocalCustPopup();
			}

		}

	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

		if (custname.getText().trim().isEmpty()) {
			showLocalCustPopup();
		}
	}

	@FXML
	void Refresh(ActionEvent event) {
		cmbDeliveryBoy.getItems().clear();
		cmbOrderTaker.getItems().clear();

		ResponseEntity<List<DeliveryBoyMst>> respdelivery = RestCaller.getDeliveryBoyMstByStatus();
		for (DeliveryBoyMst deliverymst : respdelivery.getBody()) {
			cmbDeliveryBoy.getItems().add(deliverymst.getDeliveryBoyName());
		}
		cmbDeliveryMode.getItems().clear();
		cmbDeliveryMode.getItems().add("TAKE_AWAY");
		cmbDeliveryMode.getItems().add("HOME_DELIVERY");

		ResponseEntity<List<OrderTakerMst>> resporder = RestCaller.getOrderTakerMstByStatus();
		for (OrderTakerMst orTakerMst : resporder.getBody()) {
			cmbOrderTaker.getItems().add(orTakerMst.getOrderTakerName());
		}

	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {
		showPopup();
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtMessage.requestFocus();

		}
	}

	@FXML
	void messageOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnAdditem.requestFocus();

		}
	}

	@FXML
	void FocusOnCardType(KeyEvent event) {
		if (event.getCode() == KeyCode.LEFT) {

			visibleCardSale();
			cmbCardType.requestFocus();
		} else if (event.getCode() == KeyCode.ENTER) {

			btnSave.requestFocus();

		} else if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	@FXML
	void FocusFinalSave(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			finalSave();
		}

	}

	@FXML
	private void initialize() {
		/*
		 * Create an instance of SalesOrderDtl. SalesOrderTransHdr entity will be
		 * refreshed after final submit. SalesOrderDtl will be added on AddItem Function
		 * 
		 */
		
		orderDuedate = SystemSetting.datePickerFormat(orderDuedate, "dd/MMM/yyyy");
		
		setCardType();
		cmbOrderBy.getItems().add("Email");
		cmbOrderBy.getItems().add("Phone");
		cmbOrderBy.getItems().add("Counter");
		cmbOrderBy.getItems().add("WhatsApp");
		cmbOrderBy.getItems().add("Direct");
		cmbOrderBy.getItems().add("EBS");

		ResponseEntity<List<DeliveryBoyMst>> respdelivery = RestCaller.getDeliveryBoyMst();
		for (DeliveryBoyMst deliverymst : respdelivery.getBody()) {
			cmbDeliveryBoy.getItems().add(deliverymst.getDeliveryBoyName());
		}

		cmbDeliveryMode.getItems().add("TAKE_AWAY");
		cmbDeliveryMode.getItems().add("HOME_DELIVERY");

		cmbDueTime.getItems().add("am");
		cmbDueTime.getItems().add("pm");
		cmbDueTime.getSelectionModel().select("pm");

		ResponseEntity<List<OrderTakerMst>> resporder = RestCaller.getOrderTakerMst();
		for (OrderTakerMst orTakerMst : resporder.getBody()) {
			cmbOrderTaker.getItems().add(orTakerMst.getOrderTakerName());
		}

		salesOrderDtl = new SalesOrderDtl();
		/*
		 * Fieds with Entity property
		 */

		txtCashPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);
		txtItemcode.textProperty().bindBidirectional(salesOrderDtl.getItemCodeProperty());
		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);
		txtTax.textProperty().bindBidirectional(taxRateProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		txtTax.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtTax.setText(oldValue);
				}
			}
		});
		txtCashPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashPaidamount.setText(oldValue);
				}
			}
		});

		txtCustPhoneNo.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCustPhoneNo.setText(oldValue);
				}
			}
		});
		txtCustPhoneNo2.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCustPhoneNo2.setText(oldValue);
				}
			}
		});
		txtChangeamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtChangeamount.setText(oldValue);
				}
			}
		});
		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashtopay.setText(oldValue);
				}
			}
		});

		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});

		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (gstNo.getText().trim().isEmpty()) {
					ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
					ResponseEntity<UnitMst> getUnit = RestCaller
							.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
//				cmbUnit.setValue(cmbUnit.getSelectionModel().getSelectedItem());
					ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller
							.getMultiUnitbyprimaryunit(getItem.getBody().getId(), getUnit.getBody().getId());
					if (null != getMultiUnit.getBody()) {
						txtRate.setText(Double.toString(getMultiUnit.getBody().getPrice()));
					} else
						txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));
				} else {
					ResponseEntity<AccountHeads> getCust = RestCaller.getAccountHeadsByName(gstNo.getText());
					// ResponseEntity<PriceDefinition> getpriceDef = RestCaller.get
					ResponseEntity<UnitMst> getUnit = RestCaller
							.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

					ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
					java.util.Date udate = SystemSetting.getApplicationDate();
					String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

					ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
							getItem.getBody().getId(), getCust.getBody().getPriceTypeId(), getUnit.getBody().getId(),
							txtBatch.getText(), sdate);
					if (null != batchPriceDef.getBody()) {
						txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
					} else {
						txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));

						ResponseEntity<PriceDefinition> priceDef = RestCaller.getPriceDefenitionByItemIdAndUnit(
								getItem.getBody().getId(), getCust.getBody().getPriceTypeId(),
								getUnit.getBody().getId(), sdate);
						if (null != priceDef.getBody()) {
							txtRate.setText(Double.toString(priceDef.getBody().getAmount()));

						}

						else

						{
							ResponseEntity<List<PriceDefinition>> pricebyItem = RestCaller
									.getPriceByItemId(getItem.getBody().getId(), getCust.getBody().getPriceTypeId());
							priceDefenitionList = FXCollections.observableArrayList(pricebyItem.getBody());

							if (null != pricebyItem.getBody())

							{
								for (PriceDefinition price : priceDefenitionList) {
									if (null == price.getUnitId()) {
										txtRate.setText(Double.toString(price.getAmount()));

									}
								}
							}
						}

					}
				}
			}
		});
		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtCashPaidamount.getText().length() > 0)) {

					double paidAmount = Double.parseDouble(txtCashPaidamount.getText());

					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmt = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmount = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((cardAmount) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtcardAmount.getText().length() > 0)) {

					double cardAmt = Double.parseDouble(txtcardAmount.getText());

					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmount = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmt = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((paidAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesOrderDtl = new SalesOrderDtl();

					ResponseEntity<SalesOrderDtl> salesOrderDtlResp = RestCaller
							.getSalesOrderDtlById(newSelection.getId());
					salesOrderDtl = salesOrderDtlResp.getBody();

					ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(salesOrderDtl.getItemId());
					ItemMst itemMst = itemResp.getBody();

					txtItemname.setText(itemMst.getItemName());
					txtQty.setText(Double.toString(salesOrderDtl.getQty()));
					txtBarcode.setText(itemMst.getBarCode());
					txtRate.setText(Double.toString(salesOrderDtl.getMrp()));
					if (null != salesOrderDtl.getOrderMsg()) {
						txtMessage.setText(salesOrderDtl.getOrderMsg());
					}

					ResponseEntity<UnitMst> unitResp = RestCaller.getunitMst(salesOrderDtl.getUnitId());
					UnitMst unitMst = unitResp.getBody();

					cmbUnit.getSelectionModel().select(unitMst.getUnitName());

				}
			}
		});

		tblCardAmountDetails.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						if (null != newSelection.getId()) {

							saleOrderReceipt = new SaleOrderReceipt();
							saleOrderReceipt.setId(newSelection.getId());
						}
					}
				});
		eventBus.register(this);

		// btnSave.setDisable(true);
		itemDetailTable.setItems(saleListTable);
//		targetdept.getItems().add("PRODUCTION");
//		targetdept.getItems().add("DELIVERY");
//		targetdept.getItems().add("SALES");
		ResponseEntity<List<BranchMst>> branchRestListResp = RestCaller.getBranchDtl();

		List branchRestList = branchRestListResp.getBody();
		Iterator itr1 = branchRestList.iterator();

		while (itr1.hasNext()) {

			BranchMst lm = (BranchMst) itr1.next();
			targetdept.getItems().add(lm.getBranchCode());
			txtBarcode.requestFocus();
		}

		// ----------------------new version 1.4 surya
		try {
			tblHoldReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {

						salesOrderTransHdr = new SalesOrderTransHdr();
						salesOrderTransHdr.setId(newSelection.getId());

					}
				}
			});

			getHoldedSaleOrder();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		// ----------------------new version 1.4 surya end

	}

	// ----------------------new version 1.4 surya

	private void setSalesOrderDetails(SalesOrderTransHdr salesOrderTransHdr2) {

//		ResponseEntity<LocalCustomerMst> localCustResp = RestCaller.getLocalCustomerbyId(salesOrderTransHdr2.getLocalCustomerId().getId());
//		 localCustomerMst = localCustResp.getBody();
//		
//		ResponseEntity<CustomerMst> custResp = RestCaller.getCustomerById(salesOrderTransHdr2.getCustomerId().geti);
//		CustomerMst customerMst = custResp.getBody();
//		
//		custId = customerMst.getId();

		if (null != salesOrderTransHdr2.getLocalCustomerId()) {

			if (null != salesOrderTransHdr2.getLocalCustomerId().getLocalcustomerName()) {
				custname.setText(salesOrderTransHdr2.getLocalCustomerId().getLocalcustomerName());

			}

			if (null != salesOrderTransHdr2.getLocalCustomerId().getAddress()) {
				custAdress.setText(salesOrderTransHdr2.getLocalCustomerId().getAddress());

			}

			if (null != salesOrderTransHdr2.getLocalCustomerId().getPhoneNo()) {
				txtCustPhoneNo.setText(salesOrderTransHdr2.getLocalCustomerId().getPhoneNo());

			}

			if (null != salesOrderTransHdr2.getLocalCustomerId().getPhoneNO2()) {
				txtCustPhoneNo2.setText(salesOrderTransHdr2.getLocalCustomerId().getPhoneNO2());

			}

		}

		if (null != salesOrderTransHdr2.getAccountHeads()) {
			if (null != salesOrderTransHdr2.getAccountHeads().getAccountName()) {
				gstNo.setText(salesOrderTransHdr2.getAccountHeads().getAccountName());

			}

		}

		if (null != salesOrderTransHdr2.getOrderAdvice()) {
			txtorderadvice.setText(salesOrderTransHdr2.getOrderAdvice());
		}
		if (null != salesOrderTransHdr2.getOrderMessageToPrint()) {
			txtmsgtoprint.setText(salesOrderTransHdr2.getOrderMessageToPrint());
		}
		if (null != salesOrderTransHdr2.getTargetDepartment()) {
			targetdept.getSelectionModel().select(salesOrderTransHdr2.getTargetDepartment());
		}

		if (null != salesOrderTransHdr2.getOrderDue()) {
			orderDuedate.setValue(SystemSetting.utilToLocaDate(salesOrderTransHdr2.getOrderDue()));
		}

		if (null != salesOrderTransHdr2.getOrderTimeMode()) {
			cmbDueTime.getSelectionModel().select(salesOrderTransHdr2.getOrderTimeMode());

		}
		if (null != salesOrderTransHdr2.getOrderDueTime()) {
			txtOrderDueTime.setText(salesOrderTransHdr2.getOrderDueTime());
		}

		if (null != salesOrderTransHdr2.getSalesManId()) {

			ResponseEntity<OrderTakerMst> resporder = RestCaller
					.getOrderTakerMstById(salesOrderTransHdr2.getSalesManId());
			OrderTakerMst orderTakerMst = resporder.getBody();

			cmbOrderTaker.getSelectionModel().select(orderTakerMst.getOrderTakerName());

		}

		if (null != salesOrderTransHdr2.getDeliveryMode()) {
			cmbDeliveryMode.getSelectionModel().select(salesOrderTransHdr2.getDeliveryMode());

		}

		if (null != salesOrderTransHdr2.getDeliveryBoyId()) {

			ResponseEntity<DeliveryBoyMst> respdel = RestCaller
					.getDeliveryBoyMstById(salesOrderTransHdr2.getDeliveryBoyId());
			DeliveryBoyMst deliveryBoyMst = respdel.getBody();
			cmbDeliveryBoy.getSelectionModel().select(deliveryBoyMst.getDeliveryBoyName());

		}

		if (null != salesOrderTransHdr2.getOrdedBy()) {
			cmbOrderBy.getSelectionModel().select(salesOrderTransHdr2.getOrdedBy());
		}

		ResponseEntity<List<SalesOrderDtl>> salesOrderDtlResp = RestCaller
				.getsaleOrderDtlByHdrId(salesOrderTransHdr2.getId());

		saleListTable = FXCollections.observableArrayList(salesOrderDtlResp.getBody());

		FillTable();

	}

	// ----------------------new version 1.4 surya end

	@FXML
	void deleteRow(ActionEvent event) {

	}

	@FXML
	void DeleteItem(ActionEvent event) {
		if (null != salesOrderDtl.getId()) {
			RestCaller.deleteSalesOrderDtls(salesOrderDtl.getId());
			notifyMessage(5, "Item Deleted");

			itemDetailTable.getItems().clear();
			ResponseEntity<List<SalesOrderDtl>> saleorderDtls = RestCaller
					.getsaleOrderDtlByHdrId(salesOrderTransHdr.getId());
			saleListTable = FXCollections.observableArrayList(saleorderDtls.getBody());
			itemDetailTable.setItems(saleListTable);

			txtItemname.clear();
			txtQty.clear();
			txtBarcode.clear();
			txtMessage.clear();
			cmbUnit.getSelectionModel().clearSelection();
			txtRate.clear();
			salesOrderDtl = null;

		}
	}

	@FXML
	void OrderDueKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			cmbDueTime.requestFocus();
		}

	}

	@FXML
	void FocusOnOrderTaker(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbOrderTaker.requestFocus();
		}

	}

	@FXML
	void FocusOnDeliveryMode(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbDeliveryMode.requestFocus();
		}
	}

	@FXML
	void FocusOnDeliveryBoy(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbDeliveryBoy.requestFocus();
		}
	}

	@FXML
	void FocusOnOrderBy(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbOrderBy.requestFocus();
		}
	}

	@FXML
	void FocusOnTxtItem(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtItemname.requestFocus();
		}
	}

	@FXML
	void hold(ActionEvent event) {

	}

	@FXML
	void save(ActionEvent event) {

		/*
		 * FINALSAVE
		 */
		finalSave();

	}

	private void finalSave() {
		if (null != salesOrderTransHdr) {

			txtChangeamount.setText("00.0");
			Double cashPaidAmount = 0.0;
			Double sodexoCard = 0.0;
			Double cardAmount = 0.0;
			String card = "";
			String cardType = "";

			Double changeAmount = 0.0;

			if (!custname.getText().trim().isEmpty()) {
				ResponseEntity<LocalCustomerMst> resplocal = RestCaller.getLocalCustomerbyName(custname.getText());
				if (null != resplocal.getBody()) {
					System.out.println(resplocal.getBody());
					salesOrderTransHdr.setLocalCustomerId(resplocal.getBody());
					custId = resplocal.getBody().getId();
				}
			}
			if (gstNo.getText().trim().isEmpty()) {
				ResponseEntity<AccountHeads> resplocalgstcust = RestCaller.getAccountHeadsByNameAndBranchCode("POS",
						SystemSetting.systemBranch);
				salesOrderTransHdr.setAccountHeads(resplocalgstcust.getBody());
			} else {

				ResponseEntity<AccountHeads> resplocalgstcust = RestCaller.getAccountHeadsByNameAndBranchCode(gstNo.getText(),
						SystemSetting.systemBranch);

				AccountHeads customerMst = resplocalgstcust.getBody();
				if (null == customerMst) {
					ResponseEntity<AccountHeads> customerMstResp = RestCaller.getAccountHeadsByName(gstNo.getText());
					customerMst = customerMstResp.getBody();
				}
				salesOrderTransHdr.setAccountHeads(customerMst);
			}

			Double cashtoPay = Double.parseDouble(txtCashtopay.getText());
			salesOrderTransHdr.setInvoiceAmount(cashtoPay);

			if (!txtCashPaidamount.getText().trim().isEmpty()) {
				cashPaidAmount = Double.parseDouble(txtCashPaidamount.getText());
				if (!txtcardAmount.getText().trim().isEmpty()) {
					Double cardSale = 0.0;
					Double totalSale = 0.0;
					cardSale = Double.parseDouble(txtcardAmount.getText());
					totalSale = cashPaidAmount + cardSale;
					if (Double.parseDouble(txtCashtopay.getText()) < totalSale) {
						cashPaidAmount = Double.parseDouble(txtCashtopay.getText()) - cardSale;
					}
				} else if (cashPaidAmount > Double.parseDouble(txtCashtopay.getText())) {
					cashPaidAmount = Double.parseDouble(txtCashtopay.getText());

				}
				salesOrderTransHdr.setCashPay(cashPaidAmount);

				saleOrderReceipt = new SaleOrderReceipt();

				if (null == salesReceiptVoucherNo) {
					salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
					saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
				} else {
					saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
				}

				saleOrderReceipt.setReceiptMode("CASH");
				saleOrderReceipt.setReceiptAmount(cashPaidAmount);
				saleOrderReceipt.setUserId(SystemSetting.getUser().getId());
				saleOrderReceipt.setBranchCode(SystemSetting.systemBranch);
				// set accout id

				String account = SystemSetting.systemBranch + "-CASH";
				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(account);
				saleOrderReceipt.setAccountId(accountHeads.getBody().getId());
//	    		
//	    		LocalDate date = LocalDate.now();
//	    		java.util.Date udate = SystemSetting.localToUtilDate(date);
				saleOrderReceipt.setRereceiptDate(SystemSetting.getApplicationDate());
				saleOrderReceipt.setSalesOrderTransHdr(salesOrderTransHdr);
				System.out.println(saleOrderReceipt);
				ResponseEntity<SaleOrderReceipt> respEntity = RestCaller.saveSaleOrderReceipts(saleOrderReceipt);
				saleOrderReceipt = respEntity.getBody();
			}

			if (!txtcardAmount.getText().trim().isEmpty()) {
				cardAmount = Double.parseDouble(txtcardAmount.getText());
				salesOrderTransHdr.setCardamount(cardAmount);
			}

			if (!txtChangeamount.getText().trim().isEmpty()) {
				changeAmount = Double.parseDouble(txtChangeamount.getText());
				salesOrderTransHdr.setChangeAmount(changeAmount);
			}
			salesOrderTransHdr.setOrderStatus("Orderd");

			salesOrderTransHdr.setPaidAmount((cardAmount + cashPaidAmount) - changeAmount);
			eventBus.post(salesOrderTransHdr);

			if (cashPaidAmount > 0.0 && cardAmount == 0.0) {

				salesOrderTransHdr.setPaymentMode("CASH");
			} else if (cardAmount > 0.0 && cashPaidAmount == 0.0) {
				salesOrderTransHdr.setPaymentMode("CARD");
			} else {
				salesOrderTransHdr.setPaymentMode("MULTIMODE");
			}

			if (gstNo.getText().trim().isEmpty() || !custname.getText().trim().isEmpty()) {
				if (custname.getText().trim().isEmpty()) {
					notifyMessage(5, " Please enter customer name", false);
					return;
				} else if (custAdress.getText().trim().isEmpty()) {

					notifyMessage(5, " Please enter customer address", false);
					return;
				} else if (txtCustPhoneNo.getText().trim().isEmpty()) {

					notifyMessage(5, " Please enter customer phone no.", false);
					return;
				}

				if (txtCustPhoneNo.getText().trim().length() < 10) {
					notifyMessage(5, " Invalid Phone no.", false);
					return;
				} else if (!custname.getText().contains(txtCustPhoneNo.getText())) {

					notifyMessage(5, "Please add customer 1st phone number to the end of customer name");
					return;
				} else {
					localCustomerMst = new LocalCustomerMst();
					localCustomerMst.setId(custId);
					localCustomerMst.setLocalcustomerName(custname.getText());
					localCustomerMst.setAddress(custAdress.getText());
					localCustomerMst.setPhoneNo(txtCustPhoneNo.getText());
					localCustomerMst.setPhoneNO2(txtCustPhoneNo2.getText());
					RestCaller.updateLocalCustomer(localCustomerMst);

				}
			}
			ResponseEntity<List<SalesOrderDtl>> saledtlSaved = RestCaller.getSalesOrderDtl(salesOrderTransHdr);
			if (saledtlSaved.getBody().size() == 0) {
				return;
			}

			String financialYear = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "dd-MM-YY");
			String vNo = RestCaller.getVoucherNumber(financialYear + SystemSetting.systemBranch + "CRD");
//			vNo = financialYear.concat(vNo);
			// if((Double.parseDouble(txtCashtopay.getText())) > (paidAmount+cardAmount))
			// {

//		Date date = Date.valueOf(LocalDate.now());
//			String strDate = SystemSetting.UtilDateToString(uDate);
//
//			uDate = SystemSetting.StringToUtilDate(strDate, "yyyy-MM-dd");

			salesOrderTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
			salesOrderTransHdr.setVoucherNumber(vNo);
			salesOrderTransHdr.setSalesMode("SALEORDER");
			salesOrderTransHdr.setOrderStatus("Orderd");
//			salesOrderTransHdr.setVoucherNumber(vNo);
			if (!txtCashtopay.getText().trim().isEmpty()) {
				cashtoPay = Double.parseDouble(txtCashtopay.getText());
				salesOrderTransHdr.setInvoiceAmount(cashtoPay);

			}
			salesOrderTransHdr.setSaleOrderReceiptVoucherNumber(salesReceiptVoucherNo);

			System.out.println("Voucher Date " + salesOrderTransHdr.getVoucherDate());
			System.out.println("Voucher Date " + SystemSetting.systemDate);
		

			RestCaller.updateSalesOrderTranshdr(salesOrderTransHdr);
			notifyMessage(5, " Order to Saved");

			ResponseEntity<SalesOrderTransHdr> salesOrderTransHdrSaves = RestCaller
					.getSaleOrderTransHdrById(salesOrderTransHdr.getId());
			salesOrderTransHdr = salesOrderTransHdrSaves.getBody();

			System.out.println("Voucher Date" + salesOrderTransHdr.getVoucherDate());

			ResponseEntity<List<SaleOrderReceipt>> saleOrderReceiptList = RestCaller
					.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());

			List<SaleOrderReceipt> saleOrderReceipts = saleOrderReceiptList.getBody();
			for (SaleOrderReceipt receipts : saleOrderReceipts) {
				DayBook dayBook = new DayBook();
				dayBook.setBranchCode(SystemSetting.systemBranch);
				ResponseEntity<AccountHeads> accountHead1 = RestCaller
						.getAccountById(salesOrderTransHdr.getAccountHeads().getId());
				AccountHeads accountHeads1 = accountHead1.getBody();
				dayBook.setDrAccountName(accountHeads1.getAccountName());
				dayBook.setDrAmount(receipts.getReceiptAmount());
				dayBook.setSourceVoucheNumber(salesOrderTransHdr.getVoucherNumber());
				if (receipts.getReceiptMode().contains("CASH")) {

					String account = SystemSetting.systemBranch + "-CASH";
					ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(account);
					dayBook.setDrAccountName(accountHeads.getBody().getAccountName());

					dayBook.setNarration("SALESORDER CASH ADVANCE");
					dayBook.setCrAmount(0.0);
					dayBook.setCrAccountName(salesOrderTransHdr.getAccountHeads().getAccountName());

				} else {

					ResponseEntity<AccountHeads> accountHeads = RestCaller
							.getAccountHeadByName(receipts.getReceiptMode());
					dayBook.setDrAccountName(accountHeads.getBody().getAccountName());

					dayBook.setNarration("SALESORDER CARD ADVANCE");
					dayBook.setCrAmount(receipts.getReceiptAmount());
					dayBook.setCrAccountName(salesOrderTransHdr.getAccountHeads().getAccountName());
				}
				dayBook.setSourceVoucherType("SALESORDER RECEIPTS");

				LocalDate rdate = SystemSetting.utilToLocaDate(salesOrderTransHdr.getVoucherDate());
				dayBook.setsourceVoucherDate(Date.valueOf(rdate));
				ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			}

//			
//			DayBook dayBook = new DayBook();
//			dayBook.setBranchCode(SystemSetting.systemBranch);
//			
//			dayBook.setCrAccountName(salesOrderTransHdr.getCustomerId().getCustomerName());
//			dayBook.setCrAmount(salesOrderTransHdr.getAdvanceAmount());
//			dayBook.setNarration("SALEORDER CASH ADVANCE");
//			dayBook.setSourceVoucheNumber(salesOrderTransHdr.getVoucherNumber());
//			dayBook.setSourceVoucherType("SALEORDER RECEIPT");
//			dayBook.setDrAccountName(salesOrderTransHdr.getCustomerId().getCustomerName());
//			dayBook.setDrAmount(salesOrderTransHdr.getAdvanceAmount());
//
//			LocalDate rdate = SystemSetting.utilToLocaDate(salesOrderTransHdr.getVoucherDate());
//			dayBook.setsourceVoucherDate(java.sql.Date.valueOf(rdate));
//			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			Format formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			String vdate = formatter.format(salesOrderTransHdr.getVoucherDate());

			// Version 1.6 Starts
			salesOrderTransHdr = null;
			txtcardAmount.setText("");
			txtCashtopay.setText("");
			txtCashPaidamount.setText("");
			txtCustPhoneNo2.clear();
			txtCustPhoneNo.clear();
			custname.setText("");
			custAdress.setText("");
			gstNo.setText("");
			txtorderadvice.setText("");
			txtmsgtoprint.setText("");
			orderDuedate.setValue(null);
			targetdept.setValue(null);
			custId = null;
			txtChangeamount.clear();
			saleListTable.clear();
			targetdept.getSelectionModel().clearSelection();
			txtOrderDueTime.clear();
			cmbDueTime.getSelectionModel().clearSelection();
			cmbOrderTaker.getSelectionModel().clearSelection();
			cmbDeliveryMode.getSelectionModel().clearSelection();
			cmbDeliveryBoy.getSelectionModel().clearSelection();
			cmbOrderBy.getSelectionModel().clearSelection();
			tblCardAmountDetails.getItems().clear();
			amountTotal = 0.0;

			salesOrderDtl = null;

			localCustomerMst = null;
			saleOrderDelete = null;
			// Version 1.6 Ends

			try {
				JasperPdfReportService.SaleOrderReport(vNo, vdate);
			} catch (JRException e) {
				e.printStackTrace();
			}

			// Version 1.6 Comment start
			/*
			 * salesOrderTransHdr = null; txtcardAmount.setText("");
			 * txtCashtopay.setText(""); txtCashPaidamount.setText("");
			 * txtCustPhoneNo2.clear(); txtCustPhoneNo.clear(); custname.setText("");
			 * custAdress.setText(""); gstNo.setText(""); txtorderadvice.setText("");
			 * txtmsgtoprint.setText(""); orderDuedate.setValue(null);
			 * targetdept.setValue(null); custId = null; txtChangeamount.clear();
			 * saleListTable.clear(); targetdept.getSelectionModel().clearSelection();
			 * txtOrderDueTime.clear(); cmbDueTime.getSelectionModel().clearSelection();
			 * cmbOrderTaker.getSelectionModel().clearSelection();
			 * cmbDeliveryMode.getSelectionModel().clearSelection();
			 * cmbDeliveryBoy.getSelectionModel().clearSelection();
			 * cmbOrderBy.getSelectionModel().clearSelection();
			 * tblCardAmountDetails.getItems().clear(); amountTotal = 0.0;
			 * 
			 * salesOrderDtl = null;
			 * 
			 * localCustomerMst = null; saleOrderDelete = null;
			 */

			// Version 1.6 Comment ends

			// }

		}
		cmbDueTime.getSelectionModel().select("pm");
		lblCardType.setVisible(false);
		lblAmount.setVisible(false);
		cmbCardType.setVisible(false);
		txtCrAmount.setVisible(false);
		AddCardAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		tblCardAmountDetails.setVisible(false);
		txtcardAmount.setVisible(false);

		getHoldedSaleOrder();

	}

	@FXML
	void DateOnKEyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtOrderDueTime.requestFocus();
		}

	}

	@FXML
	void EnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			// if(event.getSource());

			txtQty.requestFocus();
		}

	}

	@FXML
	void FocusOnOrderDueDate(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			orderDuedate.requestFocus();
		}
	}

	@FXML
	void FocusOnOrderAdvice(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtorderadvice.requestFocus();
		}

	}

	@FXML
	void FocusOnMssgToPrint(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			txtmsgtoprint.requestFocus();
		}
	}

	@FXML
	void FocusOnTagetDpt(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			String msg = txtmsgtoprint.getText();
			txtMessage.setText(msg);
			targetdept.requestFocus();
		}
	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtItemname.requestFocus();
			showPopup();
		}
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
			txtCashPaidamount.requestFocus();
		}

	}

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnSave.setDisable(false);

			Double change = Double.parseDouble(txtCashPaidamount.getText())
					- Double.parseDouble(txtCashtopay.getText());
			txtChangeamount.setText(Double.toString(change));

		}

	}

	@FXML
	void calcPaid(KeyEvent event) {
		/*
		 * 
		 * if (event.getCode() == KeyCode.ENTER) { if (null == salesOrderTransHdr) {
		 * salesOrderTransHdr = new SalesOrderTransHdr();
		 * salesOrderTransHdr.setInvoiceAmount(0.0); salesOrderTransHdr.getId();
		 * ResponseEntity<SalesOrderTransHdr> respentity =
		 * RestCaller.saveSalesOrderHdr(salesOrderTransHdr); salesOrderTransHdr =
		 * respentity.getBody();
		 * 
		 * } ItemMst itemmst = new ItemMst();
		 * 
		 * salesDtl.setItemName(txtItemname.getText());
		 * salesDtl.setBarCode(txtBarcode.getText());
		 * itemmst.setBarCode(txtBarcode.getText()); ResponseEntity<List<ItemMst>>
		 * respsentity = RestCaller.getSalesbarcode(itemmst); // itemmst =
		 * respenstity.getBody();
		 * 
		 * // salesDtl.setRate(Double.parseDouble(txtRate.getText())); Double qty =
		 * Double.parseDouble(txtQty.getText()); salesDtl.setQty(qty);
		 * 
		 * // salesDtl.setUnit(choiceUnit.getStyle());
		 * salesDtl.setItemCode(txtItemcode.getText()); Double eRate = 00.0; eRate =
		 * Double.parseDouble(txtRate.getText()); salesDtl.setRate(eRate); Double TaRate
		 * = 00.0; TaRate = Double.parseDouble(txtTax.getText());
		 * salesDtl.setTaxRate(TaRate);
		 * 
		 * salesDtl.setSalesOrderTransHdr(salesOrderTransHdr);
		 * 
		 * ResponseEntity<SalesOrderDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		 * salesDtl = respentity.getBody(); Double amount =
		 * Double.parseDouble(txtQty.getText()) * Double.parseDouble(txtRate.getText())
		 * + Double.parseDouble(txtTax.getText());
		 * salesOrderTransHdr.setCashPaidSale(amount); //
		 * salesDtl.setTempAmount(amount); saleListTable.add(salesDtl);
		 * 
		 * FillTable();
		 * 
		 * amountTotal = amountTotal + salesOrderTransHdr.getCashPaidSale();
		 * txtCashtopay.setText(Double.toString(amountTotal)); txtItemname.setText("");
		 * txtBarcode.setText(""); txtQty.setText(""); txtTax.setText("");
		 * txtRate.setText("");
		 * 
		 * txtItemcode.setText("");
		 * 
		 * }
		 * 
		 * if (event.getCode() == KeyCode.BACK_SPACE) {
		 * 
		 * txtBarcode.requestFocus();
		 * 
		 * }
		 * 
		 */}

	@FXML
	void unHold(ActionEvent event) {

	}

	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		columnMessage.setCellValueFactory(cellData -> cellData.getValue().getOrderMsgProperty());

		Summary summary = null;

		if (null == salesOrderTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesOrderTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}
		if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			summary = RestCaller.getSalesWindowOrderSummaryDiscount(salesOrderTransHdr.getId());
		} else {
			summary = RestCaller.getSalesOrderSummary(salesOrderTransHdr.getId());
		}
		if (null != summary.getTotalAmount()) {
			salesOrderTransHdr.setInvoiceAmount(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
		}

	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();
	}

	private void addItem() {

		
//		txtQty.setText("1");
		if (null == orderDuedate.getValue()) {
			notifyMessage(5, "Please select Due date");
			return;
		} else if (null == cmbOrderBy.getValue()) {
			notifyMessage(5, "Please select order By");
			return;
		} else if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select item");
			return;
		} else if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Qty");
			return;
		} else if (null == cmbDeliveryMode.getValue()) {

			notifyMessage(5, "Please select deliverymode");
			return;
		} else if (null == cmbOrderTaker.getValue()) {

			notifyMessage(5, "Please select order taker");
			return;
		} else if (txtOrderDueTime.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter order time");
			return;
		} else if (cmbDeliveryMode.getSelectionModel().getSelectedItem().equals("HOME_DELIVERY")
				&& cmbDeliveryBoy.getSelectionModel().isEmpty()) {

			notifyMessage(5, "Please select delivery boy");
			return;
		} else if (custname.getText().trim().isEmpty() && gstNo.getText().trim().isEmpty()) {
			notifyMessage(5, " Please enter customer name", false);
			return;
		} else if (custname.getText().trim().isEmpty()) {

			notifyMessage(5, " Please select localcustomer", false);
			return;
		} else if (!custname.getText().contains(txtCustPhoneNo.getText())) {

			notifyMessage(5, "Please add customer 1st phone number to the end of customer name");
			return;
		} else {

			if (null == salesOrderTransHdr || null == salesOrderTransHdr.getId()) {
				salesOrderTransHdr = new SalesOrderTransHdr();
//				salesOrderTransHdr.setInvoiceAmount(0.0);
				salesOrderTransHdr.setOrdedBy(cmbOrderBy.getValue());
				salesOrderTransHdr.getId();
				salesOrderTransHdr.setUserId(SystemSetting.userId);
				salesOrderTransHdr.setOrderTimeMode(cmbDueTime.getValue());
				salesOrderTransHdr.setOrderDueTime(txtOrderDueTime.getText());
				if (!custname.getText().trim().isEmpty()) {
					ResponseEntity<LocalCustomerMst> resplocal = RestCaller.getLocalCustomerbyName(custname.getText());
					if (null != resplocal.getBody()) {
						System.out.println(resplocal.getBody());
						salesOrderTransHdr.setLocalCustomerId(resplocal.getBody());
					} else {
						localCustomerMst = new LocalCustomerMst();
						localCustomerMst.setLocalcustomerName(custname.getText());
						localCustomerMst.setAddress(custAdress.getText());
						localCustomerMst.setPhoneNo(txtCustPhoneNo.getText());
						localCustomerMst.setPhoneNO2(txtCustPhoneNo2.getText());

						ResponseEntity<LocalCustomerMst> respentity1 = RestCaller.saveLocalCustomer(localCustomerMst);
						localCustomerMst = respentity1.getBody();
						custId = respentity1.getBody().getId();
						salesOrderTransHdr.setLocalCustomerId(localCustomerMst);
						System.out.println(localCustomerMst);

					}
				}

				if (gstNo.getText().trim().isEmpty()) {
					ResponseEntity<AccountHeads> resplocalgstcust1 = RestCaller.getAccountHeadsByNameAndBranchCode("POS",
							SystemSetting.systemBranch);
					System.out.println(resplocalgstcust1.getBody());

					if (null == resplocalgstcust1.getBody().getCustomerDiscount()) {
						String s = RestCaller.updateCustDiscount();
					}

					ResponseEntity<AccountHeads> resplocalgstcust = RestCaller.getAccountHeadsByNameAndBranchCode("POS",
							SystemSetting.systemBranch);

					// version3.5end

					salesOrderTransHdr.setAccountHeads(resplocalgstcust.getBody());

				} else {
					// version3.5 sari
					//ResponseEntity<AccountHeads> resplocalgstcust1 = RestCaller.getGstCustomerByName(gstNo.getText());
					ResponseEntity<AccountHeads> resplocalgstcust1 = RestCaller.getAccountHeadsByName(gstNo.getText());
					System.out.println(resplocalgstcust1.getBody());

					if (null == resplocalgstcust1.getBody().getCustomerDiscount()) {
						String s = RestCaller.updateCustDiscount();
					}
					//ResponseEntity<AccountHeads> resplocalgstcust = RestCaller.getGstCustomerByName(gstNo.getText());
					ResponseEntity<AccountHeads> resplocalgstcust = RestCaller.getAccountHeadsByName(gstNo.getText());
					// version3.5end

					salesOrderTransHdr.setAccountHeads(resplocalgstcust.getBody());

				}
				salesOrderTransHdr.setCreditOrCash("CREDIT");

				if (!cmbDeliveryBoy.getSelectionModel().isEmpty()
						&& cmbDeliveryMode.getSelectionModel().getSelectedItem().equals("HOME_DELIVERY")) {
					ResponseEntity<DeliveryBoyMst> deliveryboyMstResp = RestCaller.getDelivaryBoyByName(
							cmbDeliveryBoy.getSelectionModel().getSelectedItem().toString().trim());
					salesOrderTransHdr.setDeliveryBoyId(deliveryboyMstResp.getBody().getId());
				}
				ResponseEntity<OrderTakerMst> resporder = RestCaller
						.getOrderTakerId(cmbOrderTaker.getSelectionModel().getSelectedItem().toString().trim());
				salesOrderTransHdr.setSalesManId(resporder.getBody().getId());
				salesOrderTransHdr.setBranchCode(SystemSetting.getSystemBranch());
				salesOrderTransHdr.setDeliveryMode(cmbDeliveryMode.getValue());
				salesOrderTransHdr.setSalesMode("SALEORDER");
				Date invoiceDate = Date.valueOf(orderDuedate.getValue());
				salesOrderTransHdr.setOrderDue(invoiceDate);
				LocalDate dueDate = invoiceDate.toLocalDate();
				salesOrderTransHdr.setOrderDueDay(dueDate.getDayOfWeek().toString());
				// salesOrderTransHdr.setTargetDepartment(targetdept.getSelectionModel().getSelectedItem().toString());
				if (null != targetdept.getValue()) {
					salesOrderTransHdr.setTargetDepartment(targetdept.getValue());
				}

				if (!txtorderadvice.getText().trim().isEmpty()) {
					salesOrderTransHdr.setOrderAdvice(txtorderadvice.getText());
				}
				System.out.println("=====salesOrderTransHdr======" + salesOrderTransHdr);
				System.out.println("=====CustomerId======" + salesOrderTransHdr.getAccountHeads());
				ResponseEntity<SalesOrderTransHdr> respentity = RestCaller.saveSalesOrderHdr(salesOrderTransHdr);
				salesOrderTransHdr = respentity.getBody();
				System.out.println("=====salesOrderTransHdr==saved====" + salesOrderTransHdr);

//			  }
			}
			ResponseEntity<UnitMst> getUnitBYItem = RestCaller
					.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
			
			if(null == getItem.getBody())
			{
				notifyMessage(3, "Invalid item", false);
				return;
			}
			
			if(!txtBarcode.getText().trim().isEmpty())
			{
				if(!txtBarcode.getText().equalsIgnoreCase(getItem.getBody().getBarCode()))
				{
					notifyMessage(1, "Sorry... invalid item barcode!!!",false);
					txtBarcode.requestFocus();
					return;
				}
				
			}

			ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId());
			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				if (null == getmulti.getBody()) {
					notifyMessage(5, "Please Add the item in Multi Unit", false);
					return;
				}
			}

			String itemId = null;

			if (null == salesOrderTransHdr) {

				return;
			}

			if (null == salesOrderDtl) {
				salesOrderDtl = new SalesOrderDtl();
			}
			if (null != salesOrderDtl.getId()) {

				RestCaller.deleteSalesOrderDtls(salesOrderDtl.getId());

			}

			salesOrderDtl.setSalesOrderTransHdr(salesOrderTransHdr);
			salesOrderDtl.setItemName(txtItemname.getText());
			salesOrderDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());
			Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());
			
			salesOrderDtl.setQty(qty);

			ResponseEntity<List<SalesOrderDtl>> getSalesDtl = RestCaller
					.SalesOrderDtlByItemAndBatch(salesOrderTransHdr.getId(), getItem.getBody().getId());
			if (null != getSalesDtl.getBody()) {
				List<SalesOrderDtl> salesQtyCheck = getSalesDtl.getBody();

				if (salesQtyCheck.size() > 0) {

					for (SalesOrderDtl sales : salesQtyCheck) {
						qty = qty + sales.getQty();
					}

					salesOrderDtl.setQty(qty);
				}

			}
			Double mrpRateIncludingTax = 00.0;
			mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
			salesOrderDtl.setMrp(mrpRateIncludingTax);

			Double taxRate = 00.0;

			ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesOrderDtl.getItemName()); // itemmst
																														// =
			ItemMst item = respsentity.getBody();

			salesOrderDtl.setItemCode(item.getItemCode());
			salesOrderDtl.setBarcode(item.getBarCode());
			salesOrderDtl.setStandardPrice(item.getStandardPrice());
			salesOrderDtl.setTaxRate(0.0);
//			}
			salesOrderDtl.setItemId(item.getId());

			if (!txtMessage.getText().trim().isEmpty()) {
				salesOrderDtl.setOrderMsg(txtMessage.getText());
			}
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
			salesOrderDtl.setUnitId(getUnit.getBody().getId());
			salesOrderDtl.setUnitName(getUnit.getBody().getUnitName());

			String logdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
			ResponseEntity<SalesOrderDtl> respentity = RestCaller.saveSalesOrderDtlWithLoginDate(salesOrderDtl,
					logdate);
			salesOrderDtl = respentity.getBody();

			ResponseEntity<List<SalesOrderDtl>> saleOrderList = RestCaller
					.getsaleOrderDtlByHdrId(salesOrderDtl.getSalesOrderTransHdr().getId());

			saleListTable.clear();
			saleListTable = FXCollections.observableArrayList(saleOrderList.getBody());
			FillTable();

			txtItemname.setText("");
			txtBarcode.setText("");
			txtQty.setText("");
			txtRate.setText("");
			cmbUnit.getSelectionModel().clearSelection();
			txtBarcode.requestFocus();
			salesOrderDtl = new SalesOrderDtl();
			txtMessage.clear();

		}
	}

	private void ambrossiaDiscount(SalesOrderTransHdr saleOrderTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {

		if (saleOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discoutAmount = (rateBeforeTax * saleOrderTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(discoutAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			double newRate = rateBeforeTax - discoutAmount;
			salesOrderDtl.setDiscount(discoutAmount);
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRate(BnewRate.doubleValue());
			BigDecimal BrateBeforeTax = new BigDecimal(rateBeforeTax);
			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
//		salesOrderDtl.sets(BrateBeforeTax.doubleValue());
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (saleOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (saleOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + saleOrderTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesOrderDtl.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);

	}

	private void calcDiscountOnBasePrice(SalesOrderTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(rateBefrTax.doubleValue());
//			salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesOrder/Dtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesOrderDtl.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);

	}

	private void calcDiscountOnMRP(SalesOrderTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * mrpRateIncludingTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRate(BnewrateBeforeTax.doubleValue());
			// BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			// rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
			// salesDtl.setMrp(BrateAfterDiscount.doubleValue());
//			salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * mrpRateIncludingTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(mrpRateIncludingTax);
//					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesOrderDtl.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);

	}

	private void showPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	private void showPopup() {
//		try {
//			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
//			// fxmlLoader.setController(itemStockPopupCtl);
//			Parent root1;
//
//			root1 = (Parent) fxmlLoader.load();
//			Stage stage = new Stage();
//
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.initStyle(StageStyle.UNDECORATED);
//			stage.setTitle("Stock Item");
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.setScene(new Scene(root1));
//			stage.show();
//			txtQty.requestFocus();
//		} catch (IOException e) {
//		
//			e.printStackTrace();
//		}
//
//	}

	private void showLocalCustPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/LocalCustPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			gstNo.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			gstNo.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		double applicableDiscount = 0.0;
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					cmbUnit.getItems().clear();
					itemNameProperty.set(itemPopupEvent.getItemName());
					cmbUnit.getItems().add(itemPopupEvent.getUnitName());
					cmbUnit.setValue(itemPopupEvent.getUnitName());
					// salesDtl.setItemName(itemNameProperty.get());
					/*
					 * itemNameProperty.set(itemPopupEvent.getItemName());
					 * 
					 * txtBarcode.setText(itemPopupEvent.getBarCode());
					 * batchProperty.set(itemPopupEvent.getBatch());
					 * 
					 * txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
					 * txtTax.setText(Double.toString(itemPopupEvent.getTaxRate()));
					 * 
					 */

					batchProperty.set(itemPopupEvent.getBatch());
					salesOrderDtl = new SalesOrderDtl();
					salesOrderDtl.setAddCessRate(itemPopupEvent.getAddCessRate());
					salesOrderDtl.setBatchCode(batchProperty.get());

					barcodeProperty.set(itemPopupEvent.getBarCode());

					salesOrderDtl.setBarcode(barcodeProperty.get());
					if (null != itemPopupEvent.getCess()) {
						salesOrderDtl.setCessRate(itemPopupEvent.getCess());
					} else {
						salesOrderDtl.setCessRate(0.0);
					}
					if (null != itemPopupEvent.getCgstTaxRate()) {
						salesOrderDtl.setCgstTaxRate(itemPopupEvent.getCgstTaxRate());
					} else {
						salesOrderDtl.setCgstTaxRate(0.0);
					}

					if (null != itemPopupEvent.getExpiryDate()) {
						salesOrderDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
					}

					salesOrderDtl.setItemCode(itemPopupEvent.getItemCode());

					salesOrderDtl.setItemTaxaxId(itemPopupEvent.getItemTaxaxId());

					mrpProperty.set(itemPopupEvent.getMrp() + "");
					salesOrderDtl.setMrp(Double.parseDouble(mrpProperty.get()));
					salesOrderDtl.setSgstTaxRate(itemPopupEvent.getSgstTaxRate());

					taxRateProperty.set(itemPopupEvent.getTaxRate() + "");
					salesOrderDtl.setTaxRate(Double.parseDouble(taxRateProperty.get()));
					salesOrderDtl.setUnitName(itemPopupEvent.getUnitName());

					System.out.println(itemPopupEvent.toString());
					if (gstNo.getText().trim().isEmpty()) {
						ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller
								.getMultiUnitByItemId(itemPopupEvent.getItemId());

						multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
						if (!multiUnitList.isEmpty()) {

							for (MultiUnitMst multiUniMst : multiUnitList) {

								ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
								cmbUnit.getItems().add(getUnit.getBody().getUnitName());
							}
							System.out.println(itemPopupEvent.toString());
						}
					}
					ResponseEntity<AccountHeads> getCustomer = RestCaller.getAccountHeadsByName(gstNo.getText());
					java.util.Date udate = SystemSetting.getApplicationDate();
					String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

					ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
							itemPopupEvent.getItemId(), getCustomer.getBody().getPriceTypeId(),
							itemPopupEvent.getUnitId(), itemPopupEvent.getBatch(), sdate);
					if (null != batchPriceDef.getBody()) {
						txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
					} else {
						ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
								itemPopupEvent.getItemId(), getCustomer.getBody().getPriceTypeId(),
								itemPopupEvent.getUnitId(), sdate);

						if (null != pricebyItem.getBody()) {
							txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));

						}
					}
//			ResponseEntity<List<BatchPriceDefinition>> batchPrice = RestCaller.getBatchPriceDefinitionByItemId(itemPopupEvent.getItemId(), getCustomer.getBody().getPriceTypeId(),txtBatch.getText());
//
//			BatchpriceDefenitionList = FXCollections.observableArrayList(batchPrice.getBody());
//			if(BatchpriceDefenitionList.size()>0)
//			{
//				cmbUnit.getItems().clear();
//				for (BatchPriceDefinition priceDef : BatchpriceDefenitionList) {
//					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
//					if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
//						cmbUnit.getItems().add(getUnit.getBody().getUnitName());
//						cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
//					}
//					else
//					{
//						cmbUnit.getItems().add(itemPopupEvent.getUnitName());
//						cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
//					}
//				}
//			}
//	
//			else
//			{
					ResponseEntity<List<PriceDefinition>> price = RestCaller
							.getPriceByItemId(itemPopupEvent.getItemId(), getCustomer.getBody().getPriceTypeId());
					priceDefenitionList = FXCollections.observableArrayList(price.getBody());

					if (null != price.getBody()) {
//				cmbUnit.getItems().clear();
						for (PriceDefinition priceDef : priceDefenitionList) {
							ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
							if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
								cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
							} else {
								cmbUnit.getItems().add(itemPopupEvent.getUnitName());
								cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
							}
						}
					}
//		}
//		if(getCustomer.getBody().getCustomerDiscount()>0.0)
//		{
//			double discount;
//			discount =Double.parseDouble(txtRate.getText())*(getCustomer.getBody().getCustomerDiscount()/100);
//			applicableDiscount = Double.parseDouble(txtRate.getText())-discount;
//			txtRate.setText(Double.toString(applicableDiscount));
//		}
				}
			});
		}
	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {

					if (null != salesOrderTransHdr) {
						if (null != salesOrderTransHdr.getAccountHeads()) {
							Alert a = new Alert(AlertType.CONFIRMATION);
							a.setHeaderText("Account Changing...!");
							a.setContentText("The saleorder details will be deleted");
							a.showAndWait().ifPresent((btnType) -> {
								if (btnType == ButtonType.OK) {

									deleteSalesOrderTransHdrAndDetails(salesOrderTransHdr);

									clearField();

								} else if (btnType == ButtonType.CANCEL) {

									return;

								}
							});
						}
					}

					gstNo.setText(customerEvent.getCustomerName());
					GSTcustomerId = customerEvent.getCustId();
				}
			});
		}

	}

	protected void clearField() {

//		cmbDueTime.getSelectionModel().select("pm");
		lblCardType.setVisible(false);
		lblAmount.setVisible(false);
		cmbCardType.setVisible(false);
		txtCrAmount.setVisible(false);
		AddCardAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		tblCardAmountDetails.setVisible(false);
		txtcardAmount.setVisible(false);

		salesOrderTransHdr = null;
		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtCashPaidamount.setText("");
//		txtCustPhoneNo2.clear();
//		txtCustPhoneNo.clear();
//		custname.setText("");
//		custAdress.setText("");
		gstNo.setText("");
//		txtorderadvice.setText("");
//		txtmsgtoprint.setText("");
//		orderDuedate.setValue(null);
//		targetdept.setValue(null);
		custId = null;
		txtChangeamount.clear();
		saleListTable.clear();
		targetdept.getSelectionModel().clearSelection();
//		txtOrderDueTime.clear();
//		cmbDueTime.getSelectionModel().clearSelection();
//		cmbOrderTaker.getSelectionModel().clearSelection();
//		cmbDeliveryMode.getSelectionModel().clearSelection();
//		cmbDeliveryBoy.getSelectionModel().clearSelection();
//		cmbOrderBy.getSelectionModel().clearSelection();
		tblCardAmountDetails.getItems().clear();
		amountTotal = 0.0;

		salesOrderDtl = null;

		localCustomerMst = null;
		saleOrderDelete = null;
	}

	protected void deleteSalesOrderTransHdrAndDetails(SalesOrderTransHdr salesOrderTransHdr2) {

		ResponseEntity<List<SalesOrderDtl>> saleOrderDtlListResp = RestCaller
				.getsaleOrderDtlByHdrId(salesOrderTransHdr2.getId());
		List<SalesOrderDtl> saleOrderDtlList = saleOrderDtlListResp.getBody();

		for (SalesOrderDtl sale : saleOrderDtlList) {
			RestCaller.deleteSalesOrderDtls(sale.getId());
		}

		RestCaller.deleteSalesOrderTransHdr(salesOrderTransHdr2.getId());

	}

	@Subscribe
	public void popupLocalCustomerlistner(LocalCustomerEvent localCustomerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {

					custname.setText(localCustomerEvent.getLocalCustName());
					if (null != localCustomerEvent.getLocalCustid()) {
						custAdress.setText(localCustomerEvent.getLocalCustAddress());
						txtCustPhoneNo.setText(localCustomerEvent.getLocalCustPhoneNo());
						txtCustPhoneNo2.setText(localCustomerEvent.getLocalCustPhone2());
						custId = localCustomerEvent.getLocalCustid();
						System.out.println("custIdcustIdcustId" + custId);
					} else {
						custAdress.requestFocus();
					}

				}
			});
		}

	}

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   private void PageReload(String hdrId) {
	   	
	   }
	   
	   @FXML
	    void clearAll(ActionEvent event) {
		   
		   custname.clear();
		   txtorderadvice.clear();
		   cmbOrderTaker.getItems().clear();
		   custAdress.clear();
		   targetdept.getItems().clear();
		   cmbDeliveryMode.getItems().clear();
		   txtCustPhoneNo.clear();
		   orderDuedate.setValue(null);
		   cmbDeliveryBoy.getItems().clear();
		   txtCustPhoneNo2.clear();
		   txtOrderDueTime.clear();
		   cmbDueTime.getItems().clear();
		   cmbOrderBy.getItems().clear();
		   gstNo.clear();
		   txtItemname.clear();
		   cmbUnit.getItems().clear();
		   txtBarcode.clear();
		   txtRate.clear();
		   txtQty.clear();
		   txtMessage.clear();
		   itemDetailTable.getItems().clear();
		   txtCashtopay.clear();
		   txtCashPaidamount.clear();
		   txtChangeamount.clear();
		   tblHoldReport.getItems().clear();
		   priceDefenitionList=null;
		   BatchpriceDefenitionList=null;
		   receiptModeList=null;
		   saleOrderReceipt=null;
		   salesReceiptsList=null;
		   multiUnitList=null;
		   holdedSalesDtl=null;
		   saleListTable=null;
		   
		   salesOrderDtl = null;
			 salesOrderTransHdr = null;
			 localCustomerMst = null;
			 saleOrderDelete = null;
		 GSTcustomerId="";
			 qtyTotal = 0;
			 amountTotal = 0;
			discountTotal = 0;
			 taxTotal = 0;
			 cessTotal = 0;
			 discountBfTaxTotal = 0;
			 grandTotal = 0;
			 expenseTotal = 0;
			 custId = "";
			 cardAmount = 0.0;

			salesReceiptVoucherNo = null;
			
			notifyMessage(3, "Cleared....!",false);

	    }

}
