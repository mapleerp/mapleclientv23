package com.maple.mapleclient.controllers;


import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.maple.mapleclient.entity.TaxLedger;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

@RestController
public class TaxLedgerCtl {
	private ObservableList<TaxLedger> taxLedgerTable = FXCollections.observableArrayList();

	
	TaxLedger taxLedger;
	@FXML
    private TextField txtTaxPercent;

    @FXML
    private TextField txtTaxName;

    @FXML
    private TextField txtLedger;

    @FXML
    private TextField txtTallyLedgerName;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnDeleteItem;

    @FXML
    private TableView<TaxLedger> tblTaxLedgerDetails;

    @FXML
    private TableColumn<TaxLedger, String> clTaxPercent;

    @FXML
    private TableColumn<TaxLedger, String> clTallyLedgerName;

    @FXML
    private TableColumn<TaxLedger, String> clTaxName;

    @FXML
    private TableColumn<TaxLedger, String> clLedger;
    
    @FXML
	private void initialize() {
		
		tblTaxLedgerDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					taxLedger = new TaxLedger();
					taxLedger.setId(newSelection.getId());
				}
			}
		});
	}

    @FXML
    void Add(ActionEvent event) {
    	save();
    }

    private void save() {
    	TaxLedger taxLedger = new TaxLedger();
		if (txtTaxPercent.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter the TaxPercent", false);
			return;
		}

		if (txtTaxName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter  TaxName", false);
			return;
		}
		if (txtTallyLedgerName.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please enter TallyLedgerName", false);
			return;
		}
		if (txtLedger.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter  Ledger ", false);
			return;
		}
		
		taxLedger.setTaxPercent(txtTaxPercent.getText());
		taxLedger.setTaxName(txtTaxName.getText());
		taxLedger.setTallyLedgerName(txtTallyLedgerName.getText());
		taxLedger.setLedger(txtLedger.getText());

		ResponseEntity<TaxLedger> respEntity = RestCaller.createTaxLedger(taxLedger);

		{
			notifyMessage(5, "Saved ", true);
			txtTaxPercent.clear();
			txtTaxName.clear();
			txtTallyLedgerName.clear();
			txtLedger.clear();
			showAll();
			return;
		}

		
	}
    
    private void showAll() {
		ResponseEntity<List<TaxLedger>> respentity = RestCaller.getTaxLedger();
		taxLedgerTable = FXCollections.observableArrayList(respentity.getBody());

		System.out.print(taxLedgerTable.size() + "observable list size issssssssssssssssssssss");
		fillTable();

	}
	private void fillTable() {
		tblTaxLedgerDetails.setItems(taxLedgerTable);

		clTaxPercent.setCellValueFactory(cellData -> cellData.getValue().getTaxPercentProperty());

		clTaxName.setCellValueFactory(cellData -> cellData.getValue().getTaxNameProperty());

		clTallyLedgerName.setCellValueFactory(cellData -> cellData.getValue().getTallyLedgerNameProperty());
		
        clLedger.setCellValueFactory(cellData -> cellData.getValue().getLedgerProperty());
	}

    @FXML
    void Delete(ActionEvent event) {
    	if (null != taxLedger) {

			if (null != taxLedger.getId()) {
				RestCaller.deleteTaxLedger(taxLedger.getId());

				showAll();
				notifyMessage(5, " Detail deleted", false);
				return;
			}
		}
    }

    @FXML
    void ShowAll(ActionEvent event) {
    	showAll();
    }

    @FXML
    void add(KeyEvent event) {
    	
    }

    @FXML
    void clearDetails(ActionEvent event) {
    	txtLedger.clear();
    	txtTallyLedgerName.clear();
    	txtTaxName.clear();
    	txtTaxPercent.clear();

    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

    @FXML
    void ledgerOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnAdd.requestFocus();
		}
    }

    @FXML
    void tallyLedgerNameOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtLedger.requestFocus();
		}
    }

    @FXML
    void taxNameOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtTallyLedgerName.requestFocus();
		}
    }

    @FXML
    void taxPercentOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtTaxName.requestFocus();
		}
    }
}
