package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.mapleclient.entity.InvoiceFormatMst;
import com.maple.mapleclient.entity.LocalCustomerDtl;
import com.maple.mapleclient.entity.MenuMst;
import com.maple.mapleclient.entity.TermsAndConditionsMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class TermsAndConditionCtl {
	
	String taskid;
	String processInstanceId;

	TermsAndConditionsMst termsAndConditionMst = null;
	private ObservableList<TermsAndConditionsMst> termsAndConditionMstList = FXCollections.observableArrayList();

	
	

	@FXML
	private ComboBox<String> cmbFormat;

	@FXML
	private TextField txtSlno;

	@FXML
	private TextField txtCondition;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowAll;
	
	 @FXML
	    private Button btnprint;

	@FXML
	private TableView<TermsAndConditionsMst> tblReport;

	@FXML
	private TableColumn<TermsAndConditionsMst, String> clFormat;

	@FXML
	private TableColumn<TermsAndConditionsMst, String> clSlNo;

	@FXML
	private TableColumn<TermsAndConditionsMst, String> clCondition;
	
	@FXML
	private void initialize() {
		
		ResponseEntity<List<InvoiceFormatMst>> invoiceFormatResp = RestCaller.getAllJasperByPriceType();
		List<InvoiceFormatMst> invoiceFormatList = invoiceFormatResp.getBody();
		
		for(InvoiceFormatMst invoice : invoiceFormatList)
		{
			cmbFormat.getItems().add(invoice.getJasperName());
		}
		
		tblReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					termsAndConditionMst = new TermsAndConditionsMst();
					termsAndConditionMst.setId(newSelection.getId());

				}
			}
		});
	}

	@FXML
	void BackOnKeyPress(KeyEvent event) {

	}

	@FXML
	void Delete(ActionEvent event) {
		
		if(null != termsAndConditionMst)
		{
			if(null != termsAndConditionMst.getId())
			{
				RestCaller.deleteTermsAndConditionMst(termsAndConditionMst.getId());
				
				ResponseEntity<List<TermsAndConditionsMst>> termsAndConditonRespList = RestCaller.getAllTermsAndConditionMst();
				termsAndConditionMstList = FXCollections.observableArrayList(termsAndConditonRespList.getBody());
				
				FillTable();
				
			} else {
				notifyMessage(3, "Please select terms and condition...!", false);
				return;
			}
		} else {
			notifyMessage(3, "Please select terms and condition...!", false);
			return;
		}

	}

	@FXML
	void SAVE(ActionEvent event) {

		saveTermsAndCondition();
	}

	@FXML
	void SaveOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			saveTermsAndCondition();
		}
	}

	private void saveTermsAndCondition() {

		if (null == cmbFormat.getValue()) {
			notifyMessage(3, "Please select format...!", false);
			cmbFormat.requestFocus();
			return;
		}
		if (txtSlno.getText().trim().isEmpty()) {
			notifyMessage(3, "Please enter serial number...!", false);
			txtSlno.requestFocus();
			return;
		}

		if (txtCondition.getText().trim().isEmpty()) {
			notifyMessage(3, "Please enter terms and condition...!", false);
			txtCondition.requestFocus();
			return;
		}

		termsAndConditionMst = new TermsAndConditionsMst();

		termsAndConditionMst.setConditions(txtCondition.getText());
		termsAndConditionMst.setInvoiceFormat(cmbFormat.getValue());
		termsAndConditionMst.setSerialNumber(txtSlno.getText());

		ResponseEntity<TermsAndConditionsMst> termsAndConditionResp = RestCaller
				.saveTermsAndConditionMst(termsAndConditionMst);
		
		ResponseEntity<List<TermsAndConditionsMst>> termsAndConditonRespList = RestCaller.getAllTermsAndConditionMst();
		termsAndConditionMstList = FXCollections.observableArrayList(termsAndConditonRespList.getBody());
		
		FillTable();
		
		cmbFormat.getSelectionModel().clearSelection();
		txtCondition.clear();
		txtSlno.clear();
	}

	private void FillTable() {

		tblReport.setItems(termsAndConditionMstList);
		
		clCondition.setCellValueFactory(cellData -> cellData.getValue().getConditionProperty());
		clFormat.setCellValueFactory(cellData -> cellData.getValue().getInvoiceFormatProperty());
		clSlNo.setCellValueFactory(cellData -> cellData.getValue().getSerialNumberProperty());

	}

	@FXML
	void ShowAll(ActionEvent event) {
		
		ResponseEntity<List<TermsAndConditionsMst>> termsAndConditonRespList = RestCaller.getAllTermsAndConditionMst();
		termsAndConditionMstList = FXCollections.observableArrayList(termsAndConditonRespList.getBody());
		
		FillTable();
	}

	@FXML
	void focusOnCondition(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCondition.requestFocus();
		}
	}

	@FXML
	void focusOnSave(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}
	}

	@FXML
	void focusOnSlNo(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtSlno.requestFocus();
		}
	}
	
	
	@FXML
    void print(ActionEvent event) {
		
		  JasperPdfReportService.temsandconditions(cmbFormat.getSelectionModel().getSelectedItem());
    }

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
