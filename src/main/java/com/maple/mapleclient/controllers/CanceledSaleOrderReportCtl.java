package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.SalesOrderDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
 
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class CanceledSaleOrderReportCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<SalesOrderTransHdr> dailyCanceledOrderList = FXCollections.observableArrayList();
	private ObservableList<SalesOrderDtl> saleListTable = FXCollections.observableArrayList();

	
	@FXML
	private DatePicker dpToDate;

	@FXML
	private DatePicker dpFromDate;

	@FXML
	private Button btnGenarateReport;

	@FXML
	private TableView<SalesOrderTransHdr> tblSOHdr;

	@FXML
	private TableColumn<SalesOrderTransHdr, String> clVoucherDate;

	@FXML
	private TableColumn<SalesOrderTransHdr, String> clVoucherNumber;

	@FXML
	private TableColumn<SalesOrderTransHdr, String> clLocalCustomer;

	@FXML
	private TableColumn<SalesOrderTransHdr,String> clAccount;

	@FXML
	private TableColumn<SalesOrderTransHdr, Number> clAmount;
	
    @FXML
    private TableColumn<SalesOrderTransHdr, String> clReason;

	@FXML
	private TableView<SalesOrderDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnQty;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnRate;

	@FXML
	private TableColumn<SalesOrderDtl, Number> columnAmount;

	@FXML
	private TableColumn<SalesOrderDtl,String> columnBatch;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnExpiryDate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnCessRate;

	@FXML
	private TextField txtGrandTotal;
	@FXML
	private void initialize() {
		
		 
		    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		 
		tblSOHdr.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					ResponseEntity<List<SalesOrderDtl>> salesDtlList  = RestCaller.getSalesOrderDtl(newSelection);
					saleListTable= FXCollections.observableArrayList(salesDtlList.getBody());
					FillTable();
				}
			}
		});
	}
	@FXML
	void GenarateReport(ActionEvent event) {

		if (null == dpFromDate.getValue()) {
			notifyMessage(3, "Please select from date", false);
			dpFromDate.requestFocus();
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(3, "Please select from date", false);
			dpToDate.requestFocus();
			return;
		}

		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		java.util.Date tDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(tDate, "yyy-MM-dd");
		
		ResponseEntity<List<SalesOrderTransHdr>> canceledOrderResp = RestCaller.getCanceledSaleOrder(strDate,endDate);
		dailyCanceledOrderList = FXCollections.observableArrayList(canceledOrderResp.getBody());

		fillHdrTable();
	}
	private void fillHdrTable()
	{
		Double gTotal =0.0;
		for(SalesOrderTransHdr salesorder:dailyCanceledOrderList)
		{
			salesorder.setCustomerName(salesorder.getAccountHeads().getAccountName());
			salesorder.setLocalCustomerName(salesorder.getLocalCustomerId().getLocalcustomerName());
			gTotal = gTotal + salesorder.getInvoiceAmount();
		}
		tblSOHdr.setItems(dailyCanceledOrderList);
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getInvoiceAmtProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
		clLocalCustomer.setCellValueFactory(cellData -> cellData.getValue().getLocalCustomerNameProperty());
		clReason.setCellValueFactory(cellData -> cellData.getValue().getCancelationReasonProperty());

		
		txtGrandTotal.setText(gTotal.toString());
		
		
	}
	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

	

	}
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	       private void PageReload() {
	       	
	 }

}
