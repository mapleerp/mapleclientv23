package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.PaymentInvoiceDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PaymentVoucher;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRException;

public class PaymentInvoiceReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<PaymentVoucher> PaymentInvoiceTable = FXCollections.observableArrayList();
	private ObservableList<PaymentInvoiceDtl> PaymentInvoiceDtlTable = FXCollections.observableArrayList();
	private ObservableList<OwnAccount> OwnAccList = FXCollections.observableArrayList();

	String accountId = null;
	PaymentHdr paymentHdr = null;
	PaymentInvoiceDtl paymentInvDtl = null;
	EventBus eventBus = EventBusFactory.getEventBus();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtAccount;

    @FXML
    private Button btnShowData;

    @FXML
    private Button btnPrintData;

    @FXML
    private TableView<PaymentVoucher> tbReceipts;

    @FXML
    private TableColumn<PaymentVoucher, LocalDate> clDate;

    @FXML
    private TableColumn<PaymentVoucher, String> clPaymentvoucher;

    @FXML
    private TableColumn<PaymentVoucher, String> clAccount;

    @FXML
    private TableColumn<PaymentVoucher, Number> clAmount;

    @FXML
    private TableView<PaymentInvoiceDtl> tbInvoiceDtl;

    @FXML
    private TableColumn<PaymentInvoiceDtl, String> clInvDtlInvNo;

    @FXML
    private TableColumn<PaymentInvoiceDtl, Number> clInvDtlPaidAmt;

  
    private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
    	try {
//			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root1;

			root1 = (Parent) loader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			btnShowData.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    @FXML

	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	tbReceipts.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					paymentHdr = new PaymentHdr();
					paymentHdr.setId(newSelection.getId());
					ResponseEntity<List<PaymentInvoiceDtl>> receiptInDtlSaved = RestCaller.getPaymentInvDtlById(paymentHdr);
					PaymentInvoiceDtlTable = FXCollections.observableArrayList(receiptInDtlSaved.getBody());
					fillInvoiceDetailTable();
					ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller.getOwnAccountDtlsByPaymentHdrId(paymentHdr.getId());
					OwnAccList = FXCollections.observableArrayList(ownAccSaved.getBody());
		    		fillOwnAcc();
				}
			}
		});
    }
    private void fillOwnAcc()
    {
    	for (OwnAccount owc : OwnAccList)
    	{
    		paymentInvDtl = new PaymentInvoiceDtl();
    		paymentInvDtl.setInvoiceNumber("OWN ACCOUNT");
    		paymentInvDtl.setPaidAmount(owc.getDebitAmount());
    		PaymentInvoiceDtlTable.add(paymentInvDtl);
    	}
    	fillInvoiceDetailTable();
    }
    private void fillInvoiceDetailTable()
    {
    	tbInvoiceDtl.setItems(PaymentInvoiceDtlTable);
    	clInvDtlInvNo.setCellValueFactory(cellData -> cellData.getValue().getInvoiceNumberProperty());
    	clInvDtlPaidAmt.setCellValueFactory(cellData -> cellData.getValue().getPaidAmountProperty());
    }
    @FXML
    void loadAccount(MouseEvent event) {
    	
    	loadCustomerPopup() ;
    }
	@Subscribe
	public void popuplistner(AccountPopupEvent accountPopupEvent) {

		System.out.println("------AccountPopupEvent-------popuplistner-------------");
		Stage stage = (Stage) btnPrintData.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountPopupEvent.getAccountName());
			accountId = accountPopupEvent.getAccountId();
		}
	}

	  @FXML
	    void printData(ActionEvent event) {
	    	
		  String endDate = "";
			Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
			String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

			Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
			endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
			if(!txtAccount.getText().isEmpty())
			{
				try {
					
					JasperPdfReportService.PaymentReportByAccountBetweenDate(accountId,strtDate,endDate);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
			else
			{
try {
					
					JasperPdfReportService.PaymentReportByBetweenDate(strtDate,endDate);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
	    }
    @FXML
    void showData(ActionEvent event) {
    	String endDate = "";
		Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
		endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		if(!txtAccount.getText().isEmpty())
		{
    	ResponseEntity<List<PaymentVoucher>> respentity  = RestCaller.getPaymentHdrBetweenDateByAccount(accountId,strtDate,endDate);
    	PaymentInvoiceTable = FXCollections.observableArrayList(respentity.getBody());
		}
		else
		{
			ResponseEntity<List<PaymentVoucher>> respentity  = RestCaller.getPaymentHdrBetweenDate(strtDate,endDate);
			PaymentInvoiceTable = FXCollections.observableArrayList(respentity.getBody());
		}
		fillReceiptHdr();
    }

    private void fillReceiptHdr()
    {
    	tbReceipts.setItems(PaymentInvoiceTable);
    	clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
    	clPaymentvoucher.setCellValueFactory(cellData -> cellData.getValue().getvoucherNoProperty());
    	clAccount.setCellValueFactory(cellData -> cellData.getValue().getaccountNameProperty());
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }
}
