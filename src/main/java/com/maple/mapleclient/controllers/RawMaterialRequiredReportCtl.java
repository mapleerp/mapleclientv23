package com.maple.mapleclient.controllers;

import java.time.LocalDate;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ProductionDtlsReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class RawMaterialRequiredReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<ProductionDtlsReport> productionList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private DatePicker dpTodate;

    @FXML
    private TableView<ProductionDtlsReport> tblReport;

    @FXML
    private TableColumn<ProductionDtlsReport, LocalDate> clDate;

    @FXML
    private TableColumn<ProductionDtlsReport, String> clItemName;

    @FXML
    private TableColumn<ProductionDtlsReport, Number> clQty;

    @FXML
    private TableColumn<ProductionDtlsReport, String> clUnit;
    
    @FXML
   	private void initialize() {
   		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
   		dpTodate = SystemSetting.datePickerFormat(dpTodate, "dd/MMM/yyyy");
       }
    @FXML
    void printReport(ActionEvent event) {
      	java.util.Date uDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    	String fdate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
    	
    	java.util.Date uDate1 = SystemSetting.localToUtilDate(dpTodate.getValue());
    	String tdate = SystemSetting.UtilDateToString(uDate1, "yyyy-MM-dd");
    	
    	try {
			JasperPdfReportService.ProductionRawMaterialReportBetweenDate(fdate,tdate, SystemSetting.systemBranch);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @FXML
    void showReport(ActionEvent event) {
      	java.util.Date uDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    	String fdate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
    	
    	java.util.Date uDate1 = SystemSetting.localToUtilDate(dpTodate.getValue());
    	String tdate = SystemSetting.UtilDateToString(uDate1, "yyyy-MM-dd");
		ResponseEntity<List<ProductionDtlsReport>> productionDtlList = RestCaller.getProductionSummaryReportBetweenDate(fdate, tdate);
		productionList = FXCollections.observableArrayList(productionDtlList.getBody());
		tblReport.setItems(productionList);
		clDate.setCellValueFactory(cellData ->cellData.getValue().getvoucherDateProperty());
		clItemName.setCellValueFactory(cellData ->cellData.getValue().getitemNameProperty());
		clQty.setCellValueFactory(cellData ->cellData.getValue().getqtyProperty());
		clUnit.setCellValueFactory(cellData ->cellData.getValue().getunitNameProperty());




    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }

}
