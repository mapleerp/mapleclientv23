package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.POSThermalPrintFactory;
import com.maple.maple.util.MapleConstants;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BarcodeBatchMst;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.CardSalesDtl;

import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.LocalCustomerDtl;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.LoyalityVouchers;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesFinalSave;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.SysDateMst;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.entity.WeighingItemMst;
//import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.WeighingMachineDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class possWindowCtl {

	String taskid;
	String processInstanceId;
	SalesReceipts selectedReceipt = new SalesReceipts();

	private static final Logger logger = LoggerFactory.getLogger(possWindowCtl.class);

	boolean initializedCalled = false;
//	SalesReceipts salesReceipts = new SalesReceipts();
	CardSalesDtl cardSalesDtl = new CardSalesDtl();
	private ObservableList<LoyalityVouchers> loyaltyVoucherList = FXCollections.observableArrayList();

	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
	private ObservableList<SalesReceipts> salesReceiptsList = FXCollections.observableArrayList();
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();
	ReceiptModeMst receiptModeMst = null;
	ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();

	Double rateBeforeDiscount = 0.0;
	boolean multi = false;
	UnitMst unitMst = null;
	String previous_invamt_pos = SystemSetting.SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS;
	String invoiceNumberPrefix = SystemSetting.POS_SALES_PREFIX;
	POSThermalPrintABS printingSupport;
	String salesReceiptVoucherNo = null;

	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
	private ObservableList<SalesTransHdr> saleTransListTable = FXCollections.observableArrayList();
	// BranchMst branch = null;
	SalesDtl salesDtl = null;
	SalesTransHdr salesTransHdr = null;
	String fixedQty = null;
	double qtyTotal = 0;
	// double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	double cardAmount = 0.0;
	String custId = "";

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty sodexoAmountLis = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");

	StringProperty barcodeProperty = new SimpleStringProperty("");

	StringProperty taxRateProperty = new SimpleStringProperty("");

	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");

	String storeFromPopUp = null;

	@FXML
	private TextField txtBarcode;

	@FXML
	private Label lblPreviosAmt;
	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnMrp;
	@FXML
	private ComboBox<String> cmbUnit;
	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TableView<SalesTransHdr> tblHold;

	@FXML
	private TableColumn<SalesTransHdr, String> clholdedId;

	@FXML
	private TableColumn<SalesTransHdr, String> clUser;

	@FXML
	private TableColumn<SalesTransHdr, Number> clInvoiceAmt;
	@FXML
	private TextField txtTotalNumberofItems;
	@FXML
	private TextField txtItemname;
	@FXML
	private TextField txtposDiscount;
	@FXML
	private TextField txtRate;

	@FXML
	private Button btnAdditem;

	@FXML
	private TableView<LoyalityVouchers> tblLoyaltyVoucher;

	@FXML
	private TableColumn<LoyalityVouchers, String> clVoucherLabel;

	@FXML
	private TableColumn<LoyalityVouchers, String> clLoyaltyVoucher;

	@FXML
	private TableColumn<LoyalityVouchers, Number> clLoyaltyAmount;

	@FXML
	private TableColumn<LoyalityVouchers, String> clExpiryDate;

	@FXML
	private TableColumn<LoyalityVouchers, Number> clPercentage;

	@FXML
	private TableColumn<LoyalityVouchers, Number> clMinAmountForRedemption;
	@FXML
	private TableColumn<LoyalityVouchers, String> clLoyaltyCardType;

	@FXML
	private Button btnReedem;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Label lblAmt;
	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private Label lblDiscount;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;
	@FXML
	private TextField txtChangeamount;

	@FXML
	private Button btnCardSale;

	@FXML
	private Button btnVisibleHoldedBills;

	@FXML
	private Label lblCardType;

	@FXML
	private Label lblAmount;

	@FXML
	private TextField txtWeighingMachineInput;

	// --------------------new version 1.7 surya

	@FXML
	private Label lblLocalCust;

	@FXML
	private TextField txtLocalCustomer;

	@FXML
	private Button btnClear;

	@FXML
	private TextField txtStore;

	@FXML
	void LocalCustomerPopUp(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			lblLocalCust.setText(null);

			ResponseEntity<ParamValueConfig> paramval = RestCaller.getParamValueConfig("MAPLE_LOYALITY_CUSTOMER");
			ParamValueConfig paramValueConfig = paramval.getBody();
			if (null != paramValueConfig) {
				if (paramValueConfig.getValue().equalsIgnoreCase("YES")) {
					if (txtLocalCustomer.getText().trim().isEmpty()) {
						showLocalCustPopup();

					} else {

						String phoneNo = txtLocalCustomer.getText();
						ResponseEntity<LocalCustomerMst> localCust = RestCaller.getLocalCustomerbyPhone(phoneNo);
						LocalCustomerMst localCustomerMst = localCust.getBody();
						if (null != localCustomerMst) {
							lblLocalCust.setText(localCustomerMst.getLocalcustomerName());
						} else {

							notifyMessage(3, "Local Customer Not Found...!");
							return;
						}
					}
				}
			} else {
				callLoyaltyApi();
			}

		}

	}

	private void showLocalCustPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/LocalCustPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtLocalCustomer.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupLocalCustomerlistner(LocalCustomerEvent localCustomerEvent) {

		Stage stage = (Stage) txtLocalCustomer.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {

					txtLocalCustomer.setText(localCustomerEvent.getLocalCustName());
					lblLocalCust.setText(localCustomerEvent.getLocalCustName());
				}
			});
		}

	}

	// --------------------new version 1.7 surya end

	@FXML
	void VIsibleHoldedBills(ActionEvent event) {
		holdedPOs();
		tblHold.setVisible(true);
		btnUnhold.setVisible(true);

	}

	@FXML
	void VisibleCardSale(ActionEvent event) {

		visibleCardSale();
	}

	private void visibleCardSale() {

		tblCardAmountDetails.setVisible(true);
		cmbCardType.setVisible(true);
		txtCrAmount.setVisible(true);
		AddCardAmount.setVisible(true);
		btnDeleteCardAmount.setVisible(true);
		txtcardAmount.setVisible(true);
		lblAmount.setVisible(true);
		lblCardType.setVisible(true);

	}
	// --------------------------------------------------------------------------------------------------

	@FXML
	private ComboBox<String> cmbCardType;

	@FXML
	private Button AddCardAmount;

	@FXML
	private TextField txtCrAmount;

	@FXML
	void addCardAmount(ActionEvent event) {
		addCardAmount();

	}

	@FXML
	void KeyPressedOnWindow(KeyEvent event) {
		txtBarcode.requestFocus();
	}

	private void addCardAmount() {
		SalesReceipts salesReceipts = new SalesReceipts();
		if (null != salesTransHdr) {

			if (null == cmbCardType.getValue()) {

				notifyMessage(1, "Please select card type!!!!");
				cmbCardType.requestFocus();
				return;

			} else if (txtCrAmount.getText().trim().isEmpty()) {

				notifyMessage(1, "Please enter amount!!!!");
				txtCrAmount.requestFocus();
				return;

			} else {
				Double prcardAmount = 0.0;
				prcardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
				Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
				Double cardAmount = Double.parseDouble(txtCrAmount.getText());
				if ((prcardAmount + cardAmount) > invoiceAmount) {
					notifyFailureMessage(3, "Card Amount should be less than or equal to invoice Amount");
					return;
				}
				// rest call for salesReceipt voucher number
				// generaly voucherNo. generation done by backend, its a special case
				
				if (null == salesReceiptVoucherNo) {
					salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getUser().getBranchCode());
					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
				} else {
					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
				}
//				ResponseEntity<AccountHeads> accountHeads = RestCaller
//						.getAccountHeadByName(cmbCardType.getSelectionModel().getSelectedItem());

				/*
				 * if no account, create account and give it back
				 */

				String cardType = cmbCardType.getSelectionModel().getSelectedItem();
				Boolean accountStatus = true;
				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByNameAndStatus(
						cardType, accountStatus);

				salesReceipts.setAccountId(accountHeads.getBody().getId());

				salesReceipts.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());

				salesReceipts.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
				salesReceipts.setUserId(SystemSetting.getUser().getId());
				salesReceipts.setBranchCode(SystemSetting.systemBranch);

//				LocalDate date = LocalDate.now();
//				java.util.Date udate = SystemSetting.localToUtilDate(date);
				salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
				salesReceipts.setSalesTransHdr(salesTransHdr);
				System.out.println(salesReceipts);

				ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
				salesReceipts = respEntity.getBody();

				// ------------------------cardSalesDtl saving

				cardSalesDtl.setBranchCode(SystemSetting.systemBranch);
				cardSalesDtl.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
				cardSalesDtl.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());
				cardSalesDtl.setSalesTransHdrId(salesTransHdr);
				cardSalesDtl.setRereceiptDate(SystemSetting.getApplicationDate());
				cardSalesDtl.setUserId(SystemSetting.getUser().getId());
				cardSalesDtl.setAccountId(accountHeads.getBody().getId());
				cardSalesDtl.setSalesReceiptId(salesReceipts.getId());

				ResponseEntity<CardSalesDtl> response = RestCaller.saveCardSaleDtl(cardSalesDtl);
				cardSalesDtl = response.getBody();

				if (null != salesReceipts) {
					if (null != salesReceipts.getId()) {
						notifyMessage(1, "Successfully added cardAmount");
//				cardAmount = cardAmount+Double.parseDouble(txtCrAmount.getText());
//				txtcardAmount.setText(Double.toString(cardAmount));
						salesReceiptsList.add(salesReceipts);
						filltableCardAmount();
					}
				}

				txtCrAmount.clear();
				cmbCardType.getSelectionModel().clearSelection();
				cmbCardType.requestFocus();
				salesReceipts = null;

			}
		} else {
			notifyMessage(1, "Please add Item!!!!");
			return;
		}

	}

	private void filltableCardAmount() {

		tblCardAmountDetails.setItems(salesReceiptsList);
		clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
		clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

		cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
		txtcardAmount.setText(Double.toString(cardAmount));

	}

	@FXML
	void FocusOnCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCrAmount.requestFocus();
		}

	}

	@FXML
	void KeyPressAddCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			addCardAmount();
		}

	}

	@FXML
	void setCardType(MouseEvent event) {

		setCardType();

	}

	@FXML
	private TableView<SalesReceipts> tblCardAmountDetails;

	@FXML
	private TableColumn<SalesReceipts, String> clCardTye;

	@FXML
	private TableColumn<SalesReceipts, Number> clCardAmount;

	@FXML
	private Button btnDeleteCardAmount;

	@FXML
	void DeleteCardAmount(ActionEvent event) {
		
		
		if (null != selectedReceipt) {
			if (null != selectedReceipt.getId()) {

				RestCaller.deleteSalesReceipts(selectedReceipt.getId());

				getAllCardAmount();

			}

		}

	}

	private void getAllCardAmount() {
		tblCardAmountDetails.getItems().clear();
		ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
				.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
		salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

		try {
			cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
		} catch (Exception e) {

			txtcardAmount.setText("");
		}

		filltableCardAmount();
	}

	@FXML
	void KeyPressDeleteCardAmount(KeyEvent event) {

	}

	@FXML
	void KeyPressFocusAddBtn(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (txtCrAmount.getText().length() == 0 && null == cmbCardType.getValue()) {
				btnSave.requestFocus();
			} else {

				AddCardAmount.requestFocus();
			}
		}

	}

//    @FXML
//    void FocusOnAddBtn(KeyEvent event) {
//    	
//    }

	private void setCardType() {
		cmbCardType.getItems().clear();

		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());

		for (ReceiptModeMst receiptModeMst : receiptModeList) {
			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
				cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
			}
		}
	}

	double amountTotal = 0;

	@FXML
	void onEnterCustPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();

		}
	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

		loadCustomerPopup();
	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {
		showPopup();
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0) {
				addItem();
				txtBarcode.requestFocus();

			} else if (txtWeighingMachineInput.getText().length() > 0) {
				addItem();
				txtBarcode.requestFocus();

			}

		} else if (event.getCode() == KeyCode.LEFT) {

			txtBarcode.requestFocus();

		} else if (event.getCode() == KeyCode.BACK_SPACE && txtQty.getText().length() == 0) {
			txtBarcode.requestFocus();
		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if (txtItemname.getText().trim().isEmpty() && !txtBarcode.getText().trim().isEmpty()) {
				barcodeExec(txtBarcode.getText().trim());
				// barcodeExec();

			} else {
				showPopup();

			}
		}
	}

	@FXML
	private void initialize() {
		txtposDiscount.setVisible(false);
		lblDiscount.setVisible(false);

		txtItemname.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue,
					Boolean newPropertyValue) {
				if (newPropertyValue) {
//					barcodeExec(txtBarcode.getText().trim());
				}

			}
		});

		txtBarcode.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observableValue, String s, String s2) {

				System.out.println("String s = " + s + " String  s2 =" + s2);

				// barcodeExec(s2);

			}
		});

		// Check whether Loyalty Application Enabled

		checkForLoyalty();
		ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("PosDiscount");
		if (null == getParam.getBody()) {
			txtposDiscount.setVisible(false);
			lblDiscount.setVisible(false);
		} else {
			if (getParam.getBody().getValue().equalsIgnoreCase("YES")) {
				txtposDiscount.setVisible(true);
				lblDiscount.setVisible(true);
			} else {
				txtposDiscount.setVisible(false);
				lblDiscount.setVisible(false);
			}
		}
		eventBus.register(this);

		holdedPOs();
		/*
		 * Load logo to printing support once
		 */

		printingSupport = POSThermalPrintFactory.getPOSThermalPrintABS();
		try {
			printingSupport.setupLogo();

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			printingSupport.setupBottomline(SystemSetting.getPosInvoiceBottomLine());

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			printingSupport.setupBottomline2(SystemSetting.getPosInvoiceBottomLine2());

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			printingSupport.setupBottomline3(SystemSetting.getPosInvoiceBottomLine3());

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		setCardType();

		/*
		 * final submit. SalesDtl will be added on AddItem Function
		 * 
		 */
		// salesDtl = new SalesDtl();

		/*
		 * Fieds with Entity property
		 */
		salesDtl = new SalesDtl();

		/*
		 * branch = new BranchMst(); ResponseEntity<BranchMst> respentity =
		 * RestCaller.SearchMyBranch(); branch = respentity.getBody();
		 */

		txtCashtopay.setStyle("-fx-text-inner-color: red;-fx-font-size: 24px;");

		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);

		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue && !newValue.equalsIgnoreCase("")) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?") || ((Double.parseDouble(newValue)) > 5000)) {

						Platform.runLater(() -> {
							txtQty.setText(oldValue);
						});

					}
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtRate.setText(oldValue);
					}
				}
			}
		});
		txtposDiscount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtposDiscount.setText(oldValue);
					}

				}
			}
		});

		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null != newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtPaidamount.setText(oldValue);
					}
				}
			}
		});

		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null != newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtCashtopay.setText(oldValue);
					}
				}
			}
		});

		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null != newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtcardAmount.setText(oldValue);
					}
				}
			}
		});

		// Multi Unit Billing is disabled for Testing - REGY on June 19 2020
		/*
		 * cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
		 * 
		 * @Override public void changed(ObservableValue<? extends String> observable,
		 * String oldValue, String newValue) { ResponseEntity<ItemMst> getItem =
		 * RestCaller.getItemByNameRequestParam(txtItemname.getText());
		 * 
		 * 
		 * ItemMst selecteditem = getItem.getBody();
		 * 
		 * String selectUnit = cmbUnit.getSelectionModel().getSelectedItem();
		 * 
		 * ResponseEntity<UnitMst> getUnit = RestCaller .getUnitByName(selectUnit);
		 * 
		 * 
		 * UnitMst selectedUnitMst = getUnit.getBody();
		 * 
		 * 
		 * ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller
		 * .getMultiUnitbyprimaryunit(selecteditem.getId(), selectedUnitMst.getId());
		 * 
		 * 
		 * if (null != getMultiUnit.getBody()) {
		 * txtRate.setText(Double.toString(getMultiUnit.getBody().getPrice())); } else {
		 * 
		 * txtRate.setText(Double.toString(selecteditem.getStandardPrice())); } } });
		 * 
		 */

		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if ((txtcardAmount.getText().length() >= 0)) {

					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;

					}

					try {
						paidAmount = Double.parseDouble(txtPaidamount.getText());
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					try {
						cardPaid = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
					if (newrate.doubleValue() < 0) {

						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
					} else {

						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
					}

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if ((txtPaidamount.getText().length() > 0)) {

					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;
					}

					try {
						cardPaid = Double.parseDouble(txtcardAmount.getText());
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					try {
						paidAmount = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());

					if (newrate.doubleValue() < 0) {

						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
					} else {

						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
					}

				}

				if (((String) newValue).length() == 0) {
					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;
					}

					try {
						cardPaid = Double.parseDouble(txtcardAmount.getText());
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					try {
						paidAmount = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
				}

			}
		});

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesDtl = new SalesDtl();
					salesDtl.setId(newSelection.getId());
					if (null != newSelection.getOfferReferenceId()) {
						salesDtl.setOfferReferenceId(newSelection.getOfferReferenceId());
					}

					if (null != newSelection.getSchemeId()) {
						salesDtl.setSchemeId(newSelection.getSchemeId());
					}
					salesDtl.setItemId(newSelection.getItemId());
					salesDtl.setSalesTransHdr(newSelection.getSalesTransHdr());

					salesDtl.setQty(newSelection.getQty());
					txtBarcode.setText(newSelection.getBarcode());
					txtBatch.setText(newSelection.getBatchCode());
					txtItemname.setText(newSelection.getItemName());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));

					cmbUnit.setValue(newSelection.getUnitName());

				}
			}
		});

		itemDetailTable.setOnKeyPressed((event) -> {
			if (event.getCode().isDigitKey()) {
				txtQty.requestFocus();
			} else if (event.getCode() == KeyCode.ENTER) {
				txtQty.requestFocus();
			} else if (event.getCode() == KeyCode.DELETE) {
				deleteRow();
			}
		});

		tblHold.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesTransHdr = new SalesTransHdr();
					salesTransHdr.setId(newSelection.getId());
				}
			}
		});

		tblCardAmountDetails.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						if (null != newSelection.getId()) {

							selectedReceipt = newSelection;
							 
						}
					}
				});
		// btnSave.setDisable(true);
		itemDetailTable.setItems(saleListTable);

//		holdedPOs();

		if (SystemSetting.HOLDPROPERTY) {
			btnVisibleHoldedBills.setVisible(true);

		} else {
			btnVisibleHoldedBills.setVisible(false);
		}
		txtBarcode.requestFocus();
	}

	private void checkForLoyalty() {
		ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("LOYALTYENABLED");
		if (null == getParam.getBody()) {
			tblLoyaltyVoucher.setVisible(false);
			btnReedem.setVisible(false);

		} else if (getParam.getBody().getValue().equalsIgnoreCase("YES")) {
			tblLoyaltyVoucher.setVisible(true);
			btnReedem.setVisible(true);
		} else {
			tblLoyaltyVoucher.setVisible(false);
			btnReedem.setVisible(false);
		}

	}

	@FXML
	void deleteRow(ActionEvent event) {
		deleteRow();
	}

	private void deleteRow() {
		try {

			if (null != salesDtl) {
				if (null != salesDtl.getId()) {

					if (null == salesDtl.getSchemeId()) {

						// Save Data to SalesDeleteDtl
						ResponseEntity<SalesDtl> getSales = RestCaller.getSalesDtlBydtlId(salesDtl.getId());
						if (null != getSales) {
//							if (null != getSales.getBody()) {
//								salesDtl = getSales.getBody();
//								SalesDltdDtl salesDeleteDtl = new SalesDltdDtl();
//								salesDeleteDtl.setAddCessAmount(salesDtl.getAddCessAmount());
//								salesDeleteDtl.setAddCessRate(salesDtl.getAddCessRate());
//								salesDeleteDtl.setAmount(salesDtl.getAmount());
//								salesDeleteDtl.setBarcode(salesDtl.getBarcode());
//								salesDeleteDtl.setBatch(salesDtl.getBatch());
//								salesDeleteDtl.setBarcode(salesDtl.getBarcode());
//								salesDeleteDtl.setCessAmount(salesDtl.getCessAmount());
//								salesDeleteDtl.setCessRate(salesDtl.getCessRate());
//								salesDeleteDtl.setCgstAmount(salesDtl.getCgstAmount());
//								salesDeleteDtl.setCgstTaxRate(salesDtl.getCgstTaxRate());
//								salesDeleteDtl.setCostPrice(salesDtl.getCostPrice());
//								salesDeleteDtl.setUpdatedTime(salesDtl.getUpdatedTime());
//								salesDeleteDtl.setUnitName(salesDtl.getUnitName());
//								salesDeleteDtl.setUnitId(salesDtl.getUnitId());
//								salesDeleteDtl.setTaxRate(salesDtl.getTaxRate());
//								salesDeleteDtl.setStatus(salesDtl.getStatus());
//								salesDeleteDtl.setStandardPrice(salesDtl.getStandardPrice());
//								salesDeleteDtl.setSgstTaxRate(salesDtl.getSgstTaxRate());
//								salesDeleteDtl.setSgstAmount(salesDtl.getSgstAmount());
//								salesDeleteDtl.setSchemeId(salesDtl.getSchemeId());
//								salesDeleteDtl.setReturnedQty(salesDtl.getReturnedQty());
//								salesDeleteDtl.setRate(salesDtl.getRate());
//								salesDeleteDtl.setQty(salesDtl.getQty());
//								salesDeleteDtl.setOfferReferenceId(salesDtl.getOfferReferenceId());
//								salesDeleteDtl.setMrp(salesDtl.getMrp());
//								salesDeleteDtl.setItemTaxaxId(salesDtl.getItemTaxaxId());
//								salesDeleteDtl.setItemId(salesDtl.getItemId());
//								salesDeleteDtl.setItemName(salesDtl.getItemName());
//								salesDeleteDtl.setIgstTaxRate(salesDtl.getIgstTaxRate());
//								salesDeleteDtl.setIgstAmount(salesDtl.getIgstAmount());
//								salesDeleteDtl.setIgstTaxRate(salesDtl.getIgstTaxRate());
//								salesDeleteDtl.setDiscount(salesDtl.getDiscount());
//								salesDeleteDtl.setExpiryDate(salesDtl.getExpiryDate());
//								salesDeleteDtl.setVoucherDate(SystemSetting.systemDate);
//								salesDeleteDtl.setBranchCode(SystemSetting.systemBranch);
//								ResponseEntity<SalesDltdDtl> getSalesDelete = RestCaller
//										.saveDeletedSalesDtl(salesDeleteDtl);
//							}
						}
						RestCaller.deleteSalesDtl(salesDtl.getId());

						System.out.println("toDeleteSale.getId()" + salesDtl.getId());
//						RestCaller.deleteSalesDtl(salesDtl.getId());
						txtItemname.clear();
						txtBarcode.clear();
						txtBatch.clear();
						txtRate.clear();
						txtQty.clear();
						ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
						saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
						FillTable();
						salesDtl = new SalesDtl();
						Integer countOfSalesDtl = RestCaller.getCountOfSalesDtl(salesTransHdr.getId());
						if (null != countOfSalesDtl) {
							txtTotalNumberofItems.setText(Integer.toString(countOfSalesDtl));
						}
						notifyMessage(1, " Item Deleted Successfully");
						txtBarcode.requestFocus();

					} else {
						notifyMessage(1, " Offer can not be delete");
					}

				}
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@FXML
	void hold(ActionEvent event) {
		Hold();

	}

	private void Hold() {
		txtBarcode.clear();
		txtBatch.clear();
		txtcardAmount.clear();
		txtCashtopay.clear();
		txtChangeamount.clear();
		txtCrAmount.clear();
		txtItemname.clear();
		txtRate.clear();
		txtQty.clear();
		txtPaidamount.clear();
		saleListTable.clear();
		salesDtl = null;
		// salesDtl = new SalesDtl();
		 
		salesReceiptsList.clear();
		salesTransHdr = null;

//		holdedPOs();

		txtBarcode.requestFocus();
		holdedPOs();
	}

	private void holdedPOs() {
		ResponseEntity<List<SalesTransHdr>> holdedpos = RestCaller
				.getHoldedPOSSalesByUser(SystemSetting.getUser().getId());
		saleTransListTable = FXCollections.observableArrayList(holdedpos.getBody());
		if (saleTransListTable.size() > 0) {
			tblHold.setVisible(true);
			btnUnhold.setVisible(true);
//			tblHold.getItems().clear();
		}

		for (SalesTransHdr salesTrans : saleTransListTable) {
//			if(SystemSetting.getUser().getId().equalsIgnoreCase(salesTrans.getUserId()))
//			{
			if (null == salesTrans.getTakeOrderNumber()) {
				Double invoiceAmount = 0.0;
				ResponseEntity<UserMst> getuser = RestCaller.getUserNameById(salesTrans.getUserId());
				if (null != getuser.getBody()) {
					salesTrans.setUserName(getuser.getBody().getUserName());
				} else {
					salesTrans.setUserName("TEST");
				}
				ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTrans);
				for (SalesDtl salesdt : saledtlSaved.getBody()) {
					invoiceAmount = invoiceAmount + salesdt.getAmount();
				}
				salesTrans.setInvoiceAmount(invoiceAmount);
			}
			tblHold.setItems(saleTransListTable);
			clholdedId.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());
			clUser.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
			clInvoiceAmt.setCellValueFactory(cellData -> cellData.getValue().getinvoiceAmountProperty());
		}
//		}
		txtBarcode.requestFocus();
	}

	@FXML
	void save(ActionEvent event) {

		try {
			FinalSave();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@FXML
	void EnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			// if(event.getSource());

			txtQty.requestFocus();
		}

	}

	@FXML
	void SaveButton(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			try {
				FinalSave();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@FXML
	void holdOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (txtBarcode.getText().trim().isEmpty()) {
				txtPaidamount.requestFocus();
				return;
			} else {
				// barcodeExec();
			}
		}

		if (event.getCode() == KeyCode.ENTER) {
			Hold();
		}
	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

		System.out.println(event.getCode());

		if (event.getCode() == KeyCode.ENTER) {

			txtQty.requestFocus();

			/*
			 * Scanner scanner = new Scanner(System.in);
			 * 
			 * String readString = scanner.nextLine(); while(readString!=null) {
			 * System.out.println("THIS IS THE BARCODE" + readString);
			 * 
			 * if (readString.isEmpty()) { System.out.println("Read Enter Key."); }
			 * 
			 * if (scanner.hasNextLine()) { readString = scanner.nextLine(); } else {
			 * readString = null; } }
			 */

			if (txtBarcode.getText().trim().isEmpty()) {
				txtPaidamount.requestFocus();

				return;

			} else {
				// txtQty.setText("1");
				barcodeExec(txtBarcode.getText().trim());
			}

			try {
				if (event.getCode() == KeyCode.H && event.isControlDown()) {
					btnHold.requestFocus();

				}

//			if (event.getCode() == KeyCode.BACK_SPACE && txtBarcode.getText().trim().length() == 0) {
//
//				txtItemname.requestFocus();
//				showPopup();
//			}

				if (event.getCode() == KeyCode.BACK_SPACE && txtBarcode.getText().trim().length() == 0) {

					showPopup();
				}

				if (event.getCode() == KeyCode.ENTER) {

					if (txtBarcode.getText().trim().isEmpty()) {
						txtBarcode.requestFocus();
					} else {
						/// barcodeExec();
					}
				}

				if (event.getCode() == KeyCode.DOWN) {
					itemDetailTable.requestFocus();
					itemDetailTable.getSelectionModel().selectFirst();

				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (event.getCode() == KeyCode.BACK_SPACE) {
			showPopup();

		}

		if (event.getCode() == KeyCode.DOWN) {
			txtPaidamount.requestFocus();
		}
	}

	private void barcodeExec(String barcodestring) {

		// txtQty.setText("1");

		if (null == txtBarcode) {
			return;

		}
		if (null == txtBarcode.getText() || txtBarcode.getText().trim().isEmpty()) {
			return;

		}
		/*
		 * String subBarcode = null; if (txtBarcode.getText().trim().length() >= 12) {
		 * String barcode1 = txtBarcode.getText(); subBarcode = barcode1.substring(0,
		 * 7); }
		 */

		ResponseEntity<BarcodeBatchMst> barcodeBatchMstResp = RestCaller.getBarcodeBatchMstById(barcodestring);

		BarcodeBatchMst barcodeBatchMst = barcodeBatchMstResp.getBody();

		if (null != barcodeBatchMst) {
			barcodestring = barcodeBatchMst.getBarcode() + "-" + barcodeBatchMst.getBatchCode();
		}

		ArrayList items = new ArrayList();

		items = RestCaller.SearchStockItemByBarcode(barcodestring);

		if (SystemSetting.isNEGATIVEBILLING()) {
			items = RestCaller.ItemSearchByBarcode(barcodestring);
		} else {
			items = RestCaller.SearchStockItemByBarcode(barcodestring);
		}

		if (barcodestring.trim().contains("-")) {
			System.out.println("Barcode has -");

			// Following will split the barcode into 2, barcode and batchcode

			String[] barcodearr = barcodestring.split("-", 2);

			// String[] barcodearr = txtBarcode.getText().split("-",2);

			String barcode = barcodearr[0];
			String batch = barcodearr[1];

			batch = batch.trim();

			barcodestring = barcode;

			items = RestCaller.SearchStockItemByBarcodeAndBatch(barcode, batch);

			if (items.size() == 0) {
				if (SystemSetting.isNEGATIVEBILLING()) {
					items = RestCaller.ItemSearchByBarcode(barcode);
				} else {
					items = RestCaller.SearchStockItemByBarcode(barcode);
				}
			}

		}

		if (items.size() > 1) {
			multi = true;

			showPopupByBarcode(barcodestring);

		} else {
			multi = false;

			String itemName = "";
			String barcode = "";
			Double mrp = 0.0;
			String unitname = "";
//			Double qty = 0.0;
//			Double cess = 0.0;
			Double tax = 0.0;
			String itemId = "";
			String unitId = "";

			Iterator itr = items.iterator();
			String batch = "";

			while (itr.hasNext()) {

				List element = (List) itr.next();
				itemName = (String) element.get(0);
				barcode = (String) element.get(1);
				unitname = (String) element.get(2);
				mrp = (Double) element.get(3);
//				qty = (Double) element.get(4);
//				cess = (Double) element.get(5);
				tax = (Double) element.get(6);
				itemId = (String) element.get(7);
				unitId = (String) element.get(8);
				batch = (String) element.get(9);

				if (null != itemName) {
					salesDtl = new SalesDtl();
					salesDtl.setBarcode(barcode);
					salesDtl.setUnitId(unitId);
//					salesDtl.setUnitName(unitname);
					salesDtl.setMrp(mrp);
//					salesDtl.setCessRate(cess);
					salesDtl.setTaxRate(tax);
					salesDtl.setItemId(itemId);
					salesDtl.setBatchCode(batch);
					txtItemname.setText(itemName);
					txtBatch.setText(batch);

 
					txtRate.setText(Double.toString(mrp));

					cmbUnit.setValue(unitname);

					ResponseEntity<ParamValueConfig> paramval = RestCaller.getParamValueConfig("BARCODE_DEFAULT_QTY");
					ParamValueConfig paramValueConfig = paramval.getBody();

					if (null != paramValueConfig) {

						if (paramValueConfig.getValue().equalsIgnoreCase("YES")) {
							txtQty.setText("1");
							addItem();
						} else {
							txtQty.requestFocus();

						}

					} else {
						txtQty.requestFocus();

					}

//					addItem();

				}

			}
		}

	}

	@FXML
	void actionstart(ActionEvent event) {
		itemDetailTable.requestFocus();
	}

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnSave.setDisable(false);

			Double change = Double.parseDouble(txtPaidamount.getText()) - Double.parseDouble(txtCashtopay.getText());
			txtChangeamount.setText(Double.toString(change));
			btnSave.requestFocus();
		} else if (event.getCode() == KeyCode.LEFT) {
			visibleCardSale();
			cmbCardType.requestFocus();

		} else if (event.getCode() == KeyCode.UP) {
			txtBarcode.requestFocus();
		} else if (event.getCode() == KeyCode.BACK_SPACE) {
			if (txtPaidamount.getText().isEmpty()) {
				txtBarcode.requestFocus();
			}
		} else if (event.getCode() == KeyCode.S && event.isControlDown()) {
			try {
				FinalSave();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (event.getCode() == KeyCode.U && event.isControlDown()) {

			if (SystemSetting.HOLDPROPERTY) {
				tblHold.setVisible(true);
				btnUnhold.setVisible(true);
				holdedPOs();
			}

		}

	}

	@FXML
	void calcPaid(KeyEvent event) {
		/*
		 * 
		 * if (event.getCode() == KeyCode.ENTER) { if (null == salesTransHdr) {
		 * salesTransHdr = new SalesTransHdr(); salesTransHdr.setInvoiceAmount(0.0);
		 * salesTransHdr.getId(); ResponseEntity<SalesTransHdr> respentity =
		 * RestCaller.saveSalesHdr(salesTransHdr); salesTransHdr = respentity.getBody();
		 * 
		 * } ItemMst itemmst = new ItemMst();
		 * 
		 * salesDtl.setItemName(txtItemname.getText());
		 * salesDtl.setBarCode(txtBarcode.getText());
		 * itemmst.setBarCode(txtBarcode.getText()); ResponseEntity<List<ItemMst>>
		 * respsentity = RestCaller.getSalesbarcode(itemmst); // itemmst =
		 * respenstity.getBody();
		 * 
		 * // salesDtl.setRate(Double.parseDouble(txtRate.getText())); Double qty =
		 * Double.parseDouble(txtQty.getText()); salesDtl.setQty(qty);
		 * 
		 * // salesDtl.setUnit(choiceUnit.getStyle());
		 * salesDtl.setItemCode(txtItemcode.getText()); Double eRate = 00.0; eRate =
		 * Double.parseDouble(txtRate.getText()); salesDtl.setRate(eRate); Double TaRate
		 * = 00.0; TaRate = Double.parseDouble(txtTax.getText());
		 * salesDtl.setTaxRate(TaRate);
		 * 
		 * salesDtl.setSalesTransHdr(salesTransHdr);
		 * 
		 * ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		 * salesDtl = respentity.getBody(); Double amount =
		 * Double.parseDouble(txtQty.getText()) * Double.parseDouble(txtRate.getText())
		 * + Double.parseDouble(txtTax.getText());
		 * salesTransHdr.setCashPaidSale(amount); // salesDtl.setTempAmount(amount);
		 * saleListTable.add(salesDtl);
		 * 
		 * FillTable();
		 * 
		 * amountTotal = amountTotal + salesTransHdr.getCashPaidSale();
		 * txtCashtopay.setText(Double.toString(amountTotal)); txtItemname.setText("");
		 * txtBarcode.setText(""); txtQty.setText(""); txtTax.setText("");
		 * txtRate.setText("");
		 * 
		 * txtItemcode.setText("");
		 * 
		 * }
		 * 
		 * if (event.getCode() == KeyCode.BACK_SPACE) {
		 * 
		 * txtBarcode.requestFocus();
		 * 
		 * }
		 * 
		 */}

	@FXML
	void holdOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.UP) {
			btnUnhold.requestFocus();
		}
	}

	@FXML
	void tablekeypress(KeyEvent event) {

		if (event.getCode() == KeyCode.DELETE) {
			deleteRow();
			txtBarcode.requestFocus();
		}

		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtBarcode.requestFocus();
			txtBarcode.clear();
			txtBatch.clear();
			txtItemname.clear();
			txtQty.clear();
			txtRate.clear();
			itemDetailTable.getSelectionModel().clearSelection();
			salesDtl = null;

		}
		if (event.getCode() == KeyCode.ESCAPE) {
			tblHold.requestFocus();
			tblHold.getSelectionModel().selectFirst();
		}
		initializedCalled = false;

	}

	@FXML
	void unHold(ActionEvent event) {

		unHold();
	}

	@FXML
	void unholdKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			unHold();
		}
	}

	private void unHold() {
		if (salesTransHdr != null) {
			if (salesTransHdr.getId() != null) {
				salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
				ResponseEntity<List<SalesDtl>> respentity = RestCaller.getSalesDtl(salesTransHdr);
				saleListTable = FXCollections.observableArrayList(respentity.getBody());
				FillTable();

				getAllCardAmount();
			}
		}

		txtBarcode.clear();
		txtItemname.clear();
		txtBatch.clear();
		txtBarcode.clear();
		txtRate.clear();
		txtQty.clear();

	}

	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());

		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		txtCashtopay.setText(getTotalInvoiceAmount().toPlainString());
	}

	private BigDecimal getTotalInvoiceAmount() {
		BigDecimal bdCashToPay = new BigDecimal("0");
		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());

		if (null != summary.getTotalAmount()) {

			salesTransHdr.setInvoiceAmount(summary.getTotalAmount());
			bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		}
		return bdCashToPay;
	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();
	}

	private void addItem() {

		// version2.0
		/// check financial year

		System.out.println("System Date ================" + SystemSetting.getSystemDate());
		System.out.println("Appliciation Date ================" + SystemSetting.getApplicationDate());

		ResponseEntity<List<FinancialYearMst>> getFinancialYear = RestCaller.getAllFinancialYear();
		if (getFinancialYear.getBody().size() == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}
		int count = RestCaller.getFinancialYearCount();
		if (count == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}

		if (SystemSetting.systemBranch.equalsIgnoreCase(null)) {
			notifyMessage(5, "Incorrect Branch");
			return;
		}
		

		if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(1, " Please type Item...!!!");
			txtItemname.requestFocus();
		} else if (txtBatch.getText().trim().isEmpty()) {
			notifyMessage(1, "Item Batch is not present!!!");
			txtBatch.requestFocus();
			return;
		} else {

			Double qty = 0.0;
			if (txtQty.getText().trim().isEmpty() || null == txtQty.getText()) {
				if (null != txtWeighingMachineInput.getText() && !txtWeighingMachineInput.getText().trim().isEmpty()) {

					qty = Double.parseDouble(txtWeighingMachineInput.getText());
					if (qty == 0.0) {
						notifyMessage(1, "Please enter qty!!!");
						txtQty.requestFocus();
						return;
					}
				} else {
					notifyMessage(1, "Please enter qty!!!");
					txtQty.requestFocus();
					return;
				}
			} else {
				qty = Double.parseDouble(txtQty.getText());
			}
			ResponseEntity<UnitMst> getUnitBYItem = RestCaller
					.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

			String barcode = txtBarcode.getText();

			if (!txtBarcode.getText().trim().isEmpty()) {
				String[] barcodearr = txtBarcode.getText().split("-", 2);
				barcode = barcodearr[0];

			}

			ResponseEntity<BarcodeBatchMst> barcodeBatchMstResp = RestCaller
					.getBarcodeBatchMstById(txtBarcode.getText());
			BarcodeBatchMst barcodeBatchMst = barcodeBatchMstResp.getBody();

			if (null != barcodeBatchMst) {
				barcode = barcodeBatchMst.getBarcode();
			}

			if (!barcode.equalsIgnoreCase(getItem.getBody().getBarCode())) {

				notifyMessage(1, "Sorry... invalid item barcode!!!");
				txtBarcode.requestFocus();
				return;

			}

			ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId());
			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				if (null == getmulti.getBody()) {
					notifyMessage(5, "Please Add the item in Multi Unit");
					return;
				}
			}
			try {

				ArrayList items = new ArrayList();
				items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());
				Double chkQty = 0.0;
//				chkQty = RestCaller.getQtyFromItemBatchDtlDailyByItemIdAndQty(getItem.getBody().getId(),
//						txtBatch.getText());

				// ------------------------Here stock checking from
				// ItemBatchMst--------
				// --------28/06/2021----------------------------------------------
				chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(), txtBatch.getText(),
						storeFromPopUp);
				String itemId = null;
				Iterator itr = items.iterator();
				while (itr.hasNext()) {
					List element = (List) itr.next();
					itemId = (String) element.get(7);
				}
				
				ResponseEntity<KitDefinitionMst> getKitMst = RestCaller.getKitByItemId(getItem.getBody().getId());
				if (null == getKitMst.getBody()) {
					if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
						Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
								getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(), qty);
						System.out.println(conversionQty);
						if ((chkQty < conversionQty) && (!SystemSetting.isNEGATIVEBILLING())) {
							notifyMessage(1, "Not in Stock!!!");
							txtQty.clear();
							txtItemname.clear();
							txtBarcode.clear();
							txtBatch.clear();
							txtRate.clear();
							cmbUnit.getSelectionModel().clearSelection();
							txtBarcode.requestFocus();
							return;
						}
					} else if ((chkQty < qty) && (!SystemSetting.isNEGATIVEBILLING())) {
						notifyMessage(1, "Not in Stock!!!");
						txtQty.clear();
						txtItemname.clear();
						txtBarcode.clear();
						txtBatch.clear();
						txtRate.clear();
						cmbUnit.getSelectionModel().clearSelection();
						txtBarcode.requestFocus();
						return;
					}
				}
				 
				if (null == salesTransHdr || null == salesTransHdr.getId()) { // || null == salesTransHdr.getId()
					salesTransHdr = new SalesTransHdr();

					/*
					 * new url for getting account heads instead of customer mst=========05/0/2022
					 */
//					ResponseEntity<CustomerMst> customerMstResponse = RestCaller.getCustomerByNameAndBarchCode("POS",
//							SystemSetting.systemBranch);
					ResponseEntity<AccountHeads> accountHeadsResponse = RestCaller.getAccountHeadsByNameAndBranchCode(
							"POS" + "_" + SystemSetting.systemBranch, SystemSetting.systemBranch);
					AccountHeads accountHeads = accountHeadsResponse.getBody();

					salesTransHdr.setAccountHeads(accountHeads);
					salesTransHdr.setInvoiceAmount(0.0);
					salesTransHdr.setUserId(SystemSetting.getUserId());
					salesTransHdr.setBranchCode(SystemSetting.systemBranch);
					salesTransHdr.getId();
					salesTransHdr.setCustomerId(accountHeads.getId());
					salesTransHdr.setSalesMode("POS");
					salesTransHdr.setVoucherType("SALESTYPE");
					salesTransHdr.setCreditOrCash("CASH");
					salesTransHdr.setIsBranchSales("N");

					ResponseEntity<ParamValueConfig> paramval = RestCaller
							.getParamValueConfig("MAPLE_LOYALITY_CUSTOMER");
					ParamValueConfig paramValueConfig = paramval.getBody();

					if (null != paramValueConfig) {
						if (paramValueConfig.getValue().equalsIgnoreCase("YES")) {
							if (!txtLocalCustomer.getText().trim().isEmpty()
									&& lblLocalCust.getText().trim().isEmpty()) {
								notifyMessage(3, "Local Customer Not Found..!");
								txtLocalCustomer.requestFocus();
								return;

							}

							if (lblLocalCust.getText().length() > 0) {
								ResponseEntity<LocalCustomerMst> localCustResp = RestCaller
										.getLocalCustomerbyName(lblLocalCust.getText());
								LocalCustomerMst localCustomerMst = localCustResp.getBody();

								if (null == localCustomerMst) {
									notifyMessage(3, "Local Customer Not Found..!");
									txtLocalCustomer.requestFocus();
									return;
								}

								salesTransHdr.setLocalCustomerMst(localCustomerMst);

							}
						} else {
							// ----------------call loyality customer url
						}
					} else {

						// ----------------call loyality customer url

					}

					salesTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
					ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
					salesTransHdr = respentity.getBody();

				}

				if (null == salesTransHdr || null == salesTransHdr.getId()) {
					return;
				}
				System.out.println("Sys Date after header add" + SystemSetting.systemDate);

				if (null != salesDtl) {
					if (null != salesDtl.getId()) {

						RestCaller.deleteSalesDtl(salesDtl.getId());
						salesDtl = null;

					}
				}

				salesDtl = new SalesDtl();

				salesDtl.setSalesTransHdr(salesTransHdr);
				salesDtl.setItemName(txtItemname.getText());
				salesDtl.setBarcode(barcode);

				String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();
				System.out.println(batch);
				salesDtl.setBatchCode(batch);

//				Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());

				salesDtl.setQty(qty);
				Double mrpRateIncludingTax = 00.0;
				ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("PosDiscount");
				if (null != getParam.getBody()) {
					if (getParam.getBody().getValue().equalsIgnoreCase("YES")) {
						if (!txtposDiscount.getText().trim().isEmpty()) {
							calculateDiscount();
						}
					}
				}
				mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
				salesDtl.setMrp(mrpRateIncludingTax);

				Double taxRate = 00.0;

				if (qty != 0.0 || fixedQty != null) {

					ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst
																														// =
					ItemMst item = respsentity.getBody();

					salesDtl.setBarcode(item.getBarCode());
					salesDtl.setItemCode(item.getItemCode());
					salesDtl.setStandardPrice(item.getStandardPrice());

					/*
					 * Calculate Rate Before Tax
					 */

					salesDtl.setItemId(item.getId());
					if (null == cmbUnit.getValue()) {
						ResponseEntity<UnitMst> getUnit = RestCaller
								.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
						salesDtl.setUnitId(getUnit.getBody().getId());
						salesDtl.setUnitName(getUnit.getBody().getUnitName());
					} else {
						ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
						salesDtl.setUnitId(getUnit.getBody().getId());
						salesDtl.setUnitName(getUnit.getBody().getUnitName());
					}

//					ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());
//					salesDtl.setUnitName(unitMst.getBody().getUnitName());
//					

					ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());

					if (getTaxMst.getBody().size() > 0) {
						for (TaxMst taxMst : getTaxMst.getBody()) {
							if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
								salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//								BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//								salesDtl.setIgstAmount(igstAmount.doubleValue());
								salesDtl.setTaxRate(taxMst.getTaxRate());
							}
							if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
								salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//								BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//								salesDtl.setCgstAmount(CgstAmount.doubleValue());
							}
							if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
								salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//								BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//								salesDtl.setSgstAmount(SgstAmount.doubleValue());
							}
							if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
								salesDtl.setCessRate(taxMst.getTaxRate());
//								BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//								salesDtl.setCessAmount(cessAmount.doubleValue());
							}
							if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
								salesDtl.setAddCessRate(taxMst.getTaxRate());
//								BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//								salesDtl.setAddCessAmount(cessAmount.doubleValue());
							}

						}
						Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + salesDtl.getIgstTaxRate()
								+ salesDtl.getCessRate() + salesDtl.getAddCessRate());
						salesDtl.setRate(rateBeforeTax);
						BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
						salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
						BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
						salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
						BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
						salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
						BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
						salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
						BigDecimal adcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
						salesDtl.setAddCessAmount(adcessAmount.doubleValue() * salesDtl.getQty());
					}

					else {
						taxRate = item.getTaxRate();
						salesDtl.setTaxRate(taxRate);
						Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
						salesDtl.setRate(rateBeforeTax);

						double cessRate = item.getCess();
						double cessAmount = 0.0;
						if (cessRate > 0) {

							rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
							salesDtl.setRate(rateBeforeTax);
							cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;
						}

						salesDtl.setCessRate(cessRate);

						salesDtl.setCessAmount(cessAmount);

						salesDtl.setSgstTaxRate(taxRate / 2);

						salesDtl.setCgstTaxRate(taxRate / 2);

						salesDtl.setCgstAmount(
								salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

						salesDtl.setSgstAmount(
								salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);
					}

					salesDtl.setAmount(qty * Double.parseDouble(txtRate.getText()));

					ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
							.getPriceDefenitionMstByName("COST PRICE");
					PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
					if (null != priceDefenitionMst) {
						String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
						ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller.getBatchPriceDefinition(
								salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(),
								salesDtl.getBatch(), sdate);
						if (null != batchpriceDef.getBody()) {
							salesDtl.setCostPrice(batchpriceDef.getBody().getAmount());
						} else {
							ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
									.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst.getId(),
											salesDtl.getUnitId(), sdate);

							PriceDefinition priceDefinition = priceDefenitionResp.getBody();
							if (null != priceDefinition) {
								salesDtl.setCostPrice(priceDefinition.getAmount());
							}
						}
					}

					// ===============added by anandu for the purpose of storewise stock
					// updation=====29-07-2021====
					if (!txtStore.getText().trim().isEmpty()) {
						salesDtl.setStore(txtStore.getText());
					}
					// ==========end=================
					
					

					ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);

					salesDtl = respentity.getBody();

					System.out.println("Sys Date save dtl" + SystemSetting.systemDate);
					
					
					/*
					 * stock checking ----------- 04-03-2022-------------
					 */
					/*
					 * logger.info(".........Stock checking started..........."); String
					 * stockVerification =
					 * RestCaller.StockVerificationBySalesTransHdr(salesTransHdr.getId());
					 * logger.info(".........Stock checking reply......" + stockVerification); if
					 * (!stockVerification.equalsIgnoreCase(MapleConstants.STOCKOK)) {
					 * logger.info(stockVerification + " not equal to " + MapleConstants.STOCKOK);
					 * notifyMessage(5, stockVerification); return; }
					 * logger.info(".........Stock checking completed...........");
					 */
					
					ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());

					List<SalesDtl> salesDtlList = respentityList.getBody();

					/*
					 * Call Rest to get the summary and set that to the display fields
					 */

					// salesDtl.setTempAmount(amount);
					saleListTable.clear();
					saleListTable.setAll(salesDtlList);
					System.out.println("Sys Date before fill" + SystemSetting.systemDate);
					FillTable();

					// amountTotal = amountTotal + salesTransHdr.getCashPaidSale();
					// txtCashtopay.setText(summary.getTotalAmount() + "");

					txtItemname.setText("");
					txtBarcode.setText("");

					txtQty.setText("");
					txtRate.setText("");
					txtBatch.setText("");
					txtposDiscount.clear();

					txtStore.setText("");

					txtBarcode.requestFocus();
					cmbUnit.getSelectionModel().clearSelection();
					salesDtl = new SalesDtl();
					Integer countOfSalesDtl = RestCaller.getCountOfSalesDtl(salesTransHdr.getId());
					if (null != countOfSalesDtl) {
						txtTotalNumberofItems.setText(Integer.toString(countOfSalesDtl));
					}
					// salesDtl = new SalesDtl();
				} else {
					notifyMessage(1, " Item please enter Qty...!!!");
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e);
			}
		}
		txtQty.setText("");
		txtRate.setText("");
		txtWeighingMachineInput.clear();
		System.out.println("Sys Date after add" + SystemSetting.systemDate);
		txtBarcode.requestFocus();
	}

	private void calculateDiscount() {

		Double discount = Double.parseDouble(txtposDiscount.getText());
		Double rate = Double.parseDouble(txtRate.getText());
		Double discountAmount = rate * (discount / 100);
		Double rateAfterDiscount = rate - discountAmount;
		BigDecimal bdrateAfterDiscount = new BigDecimal(rateAfterDiscount);
		bdrateAfterDiscount = bdrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
		txtRate.setText(bdrateAfterDiscount.toPlainString());

	}

	private void showPopup() {
		try {

			if (SystemSetting.isNEGATIVEBILLING() || SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO")) {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				Parent root1;
				String cst = null;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

				txtQty.requestFocus();
			} else {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));

				Parent root1;
				ItemStockPopupCtl itemStockPopupCtl = fxmlLoader.getController();
				itemStockPopupCtl.windowName = "POS";
				root1 = (Parent) fxmlLoader.load();
				String cst = null;
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

				txtQty.requestFocus();
			}

			// txtBarcode.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void showPopupByBarcode(String barcode) {
		try {
			if (SystemSetting.isNEGATIVEBILLING()) {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				Parent root1;
				String cst = null;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				ItemPopupCtl popctl = fxmlLoader.getController();
				popctl.LoadItemPopupBySearch(barcode);

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} else {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
				// fxmlLoader.setController(itemStockPopupCtl);
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				ItemStockPopupCtl popctl = fxmlLoader.getController();
				popctl.LoadItemPopupByBarcode(barcode);

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
		ItemMst item = new ItemMst();
		item = getItem.getBody();

		ResponseEntity<UnitMst> getItemUnit = RestCaller.getunitMst(item.getUnitId());

		UnitMst itemUnit = getItemUnit.getBody();

//		item.setRank(item.getRank() + 1);
//		RestCaller.updateRankItemMst(item);
		if (null == txtItemname) {
			return;
		}

		if (null == txtItemname.getScene()) {
			return;
		}
		try {

			Stage stage = (Stage) txtItemname.getScene().getWindow();
			if (stage.isShowing()) {

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						// Update UI here.
						if (null == itemPopupEvent.getItemName()) {
							txtBarcode.requestFocus();
							return;
						}

						if (!multi) {

							cmbUnit.getItems().clear();

							itemNameProperty.set(itemPopupEvent.getItemName());

							txtItemname.setText(itemPopupEvent.getItemName());
							txtBarcode.setText(itemPopupEvent.getBarCode());

							if (null == itemPopupEvent.getBatch()) {
								batchProperty.set("NOBATCH");
							} else {
								batchProperty.set(itemPopupEvent.getBatch());
							}

							if (null != itemPopupEvent.getStoreName()) {
								storeFromPopUp = itemPopupEvent.getStoreName();
								txtStore.setText(itemPopupEvent.getStoreName());
							} else {
								storeFromPopUp = "MAIN";
							}

							txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
							rateBeforeDiscount = itemPopupEvent.getMrp();
							cmbUnit.getItems().add(itemUnit.getUnitName());

							cmbUnit.setValue(itemUnit.getUnitName());

							cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());

							ResponseEntity<TaxMst> taxMst = RestCaller
									.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
							if (null != taxMst.getBody()) {
							}

							if (null != itemPopupEvent.getExpiryDate()) {
								// salesDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
							}

							ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller
									.getMultiUnitByItemId(itemPopupEvent.getItemId());

							multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
							if (!multiUnitList.isEmpty()) {

								for (MultiUnitMst multiUniMst : multiUnitList) {

									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								}
								System.out.println(itemPopupEvent.toString());
							}

						}

						else if (multi) {

							itemNameProperty.set(itemPopupEvent.getItemName());

							txtItemname.setText(itemPopupEvent.getItemName());
							txtBarcode.setText(itemPopupEvent.getBarCode());
							if (null == itemPopupEvent.getBatch()) {
								batchProperty.set("NOBATCH");
							} else {
								batchProperty.set(itemPopupEvent.getBatch());
							}

							txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
							rateBeforeDiscount = itemPopupEvent.getMrp();
							cmbUnit.getItems().add(itemPopupEvent.getUnitName());
							cmbUnit.setValue(itemPopupEvent.getUnitName());
							cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());

							if (null != itemPopupEvent.getStoreName()) {
								storeFromPopUp = itemPopupEvent.getStoreName();
								txtStore.setText(itemPopupEvent.getStoreName());
							} else {
								storeFromPopUp = "MAIN";
							}
							// salesDtl.setAddCessRate(itemPopupEvent.getAddCessRate());
							// salesDtl.setBatchCode(batchProperty.get());

							// barcodeProperty.set(itemPopupEvent.getBarCode());

							// salesDtl.setBarcode(barcodeProperty.get());

							// salesDtl.setCessRate(itemPopupEvent.getCess());

							// salesDtl.setCgstTaxRate(itemPopupEvent.getCgstTaxRate());
							ResponseEntity<TaxMst> taxMst = RestCaller
									.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
							if (null != taxMst.getBody()) {
							}
							if (null != itemPopupEvent.getExpiryDate()) {
								// salesDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
							}

							ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller
									.getMultiUnitByItemId(itemPopupEvent.getItemId());

							multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
							if (!multiUnitList.isEmpty()) {

								for (MultiUnitMst multiUniMst : multiUnitList) {

									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								}
								System.out.println(itemPopupEvent.toString());
							}
							// txtQty.setText("1");
							// if(!txtItemname.getText().trim().isEmpty())
							addItem();
							multi = false;
							System.out.println(itemPopupEvent.toString());
						}
					}
				});

			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@FXML
	void addItemOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtBarcode.requestFocus();
		}
	}

	@FXML
	void saveOnKey(KeyEvent event) {

		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			itemDetailTable.requestFocus();
			txtBarcode.requestFocus();
			itemDetailTable.getSelectionModel().selectFirst();

		}
		if (event.getCode() == KeyCode.ESCAPE) {
			// Stage stage = (Stage) btnAdditem.getScene().getWindow();
			// stage.close();
		}
	}

	private void FinalSave() throws SQLException {

		

	    ResponseEntity<SysDateMst> systemdatemstrepos = RestCaller.getSysDate(SystemSetting.systemBranch);
		
		SysDateMst	sysDateMstObject = systemdatemstrepos.getBody();
		
		String Application_date = SystemSetting.UtilDateToString(sysDateMstObject.getAplicationDate(),"yyyy-MM-dd");
		java.sql.Date appDate = java.sql.Date.valueOf(Application_date);
		
		
		ResponseEntity<DayEndClosureHdr> maxofDayrespo = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdrObject = maxofDayrespo.getBody();
		
		String process_date = SystemSetting.UtilDateToString(dayEndClosureHdrObject.getProcessDate(), "yyyy-MM-dd");
		java.sql.Date lastDayEndDate = java.sql.Date.valueOf(process_date);
		
		String transactions = SystemSetting.UtilDateToString(salesTransHdr.getVoucherDate(), "yyyy-MM-dd");
		java.sql.Date transaction = java.sql.Date.valueOf(transactions);
		
		allowTransactionForTheDate(transaction,appDate,lastDayEndDate);
		
		
		/*
		 * Initial Checks
		 */
		if (null == salesTransHdr) {
			return;
		}
		
		/*
		 * stock check changes to add button
		 */
		String stockVerification = RestCaller.StockVerificationBySalesTransHdr(salesTransHdr.getId());

		if (!stockVerification.equalsIgnoreCase(MapleConstants.STOCKOK)) {
			notifyMessage(5, stockVerification);
			return;
		}

		if (!verifyLocalCustomer()) {
			return;
		}

		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		if (saledtlSaved.getBody().size() == 0) {
			return;
		}

		// Initial Checks completed

		Double cardAmount = 0.0; // typed in card
		Double cashToPay = 0.0; // Total Invoice - typed in card

		Double AmountTenderd = 0.0; // Typed in cash
		Double changeAmount = 0.0; // Balnce amount to return

		if (!txtcardAmount.getText().trim().isEmpty()) {
			cardAmount = Double.parseDouble(txtcardAmount.getText());
		} else {
			cardAmount = 0.0;
		}
		cashToPay = getTotalInvoiceAmount().doubleValue() - cardAmount;

		if (!txtPaidamount.getText().trim().isEmpty()) {
			try {
				AmountTenderd = Double.parseDouble(txtPaidamount.getText());
			} catch (Exception e) {
				logger.error("Number Parsing error On paid amount {} ", e);
				AmountTenderd = 0.0;
			}
		}
		
		/*
		 * to check the paid amount is less or not than cash to pay....
		 */
		 if (AmountTenderd < cashToPay) {

			notifyMessage(1, "Please Enter Valid Amount!!!!");

			return;
		}
		
		changeAmount = AmountTenderd - cashToPay;

		if (null == salesReceiptVoucherNo) {
			salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
		}

		SalesReceipts salesReceiptsForCash = createReceiptForCashPaid(cashToPay);

		setTransHdrValues(cashToPay, changeAmount);

		SalesFinalSave salesFinalSave = buildFinalSaveEntity(salesReceiptsForCash );
	
		
		RestCaller.FinalSaveForSales(salesFinalSave);

	/*	ResponseEntity<List<SalesReceipts>> salesreceipt = RestCaller
				.getSalesReceiptsByTransHdrId(salesTransHdr.getId());
		*/
		
		//salesReceiptsList = FXCollections.observableArrayList(salesreceipt.getBody());
		String salesTransHdrId = salesTransHdr.getId();

		 
		cleanUpAftrSave();
		//holdedPOs();
		/*
		 * Now Print Invoice
		 */
		printingSupport.PrintInvoiceThermalPrinter(salesTransHdrId);
		salesTransHdrId = null;
		makeCardFieldInvisible();
	}

	private SalesFinalSave buildFinalSaveEntity(SalesReceipts salesReceiptsForCash ) {
		SalesFinalSave salesFinalSave = new SalesFinalSave();
		AccountReceivable accountReceivable = null;
		DayBook dayBook = null;

		
		salesFinalSave.setAccountReceivable(accountReceivable);
		salesFinalSave.setDayBook(dayBook);
		salesFinalSave.setSalesReceipts(salesReceiptsForCash);
		salesFinalSave.setSalesTransHdr(salesTransHdr);
		return salesFinalSave;
	}

	private void setTransHdrValues(Double cashToPay, Double changeAmount) {
		salesTransHdr.setSalesReceiptsVoucherNumber(salesReceiptVoucherNo);

		salesTransHdr.setSalesMode("POS");

		salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
		salesTransHdr.setCardNo("");
		salesTransHdr.setBranchCode(SystemSetting.systemBranch);
		salesTransHdr.setCardType("");
		salesTransHdr.setCashPay(cashToPay);
		salesTransHdr.setCardamount(cardAmount);

		salesTransHdr.setPaidAmount(cardAmount + cashToPay);
		salesTransHdr.setChangeAmount(changeAmount);
		salesTransHdr.setSodexoAmount(0.0);

		salesTransHdr.setInvoiceAmount(cashToPay);

	}

	private SalesReceipts createReceiptForCashPaid(Double cashToPay) {
		String branchCash = SystemSetting.systemBranch + "-" + "CASH";
		SalesReceipts salesReceipts = new SalesReceipts();

		Boolean accountCreateStatus = true;
		ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByNameAndStatus(branchCash,
				accountCreateStatus);

		salesReceipts.setVoucherNumber(salesReceiptVoucherNo);

		salesReceipts.setAccountId(accountHeads.getBody().getId());
		salesReceipts.setReceiptMode(branchCash);

		salesReceipts.setReceiptAmount(cashToPay);
		salesReceipts.setUserId(SystemSetting.getUser().getId());
		salesReceipts.setBranchCode(SystemSetting.systemBranch);
		salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
		salesReceipts.setSalesTransHdr(salesTransHdr);
		return salesReceipts;
	}

	private void cleanUpAftrSave() {
		salesTransHdr = null;

		if (previous_invamt_pos.equalsIgnoreCase("YES")) {
			lblAmt.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
			lblPreviosAmt.setText("Amount:");
			lblAmt.setText(txtCashtopay.getText());
		}
		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtPaidamount.setText("");

		txtChangeamount.clear();
		saleListTable.clear();
		txtBarcode.requestFocus();
		salesDtl = new SalesDtl();

		salesTransHdr = null;
		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtPaidamount.setText("");

		txtChangeamount.clear();
		saleListTable.clear();
		salesDtl = new SalesDtl();

		saleListTable.clear();
		txtBarcode.requestFocus();
		salesDtl = new SalesDtl();
		tblCardAmountDetails.getItems().clear();
		salesReceiptVoucherNo = null;
		txtItemname.clear();
		txtBarcode.clear();
		txtBatch.clear();
		txtRate.clear();

		txtQty.clear();
		salesReceiptsList.clear();

	}

	private void makeCardFieldInvisible() {
		tblCardAmountDetails.setVisible(false);
		cmbCardType.setVisible(false);
		txtCrAmount.setVisible(false);
		AddCardAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		txtcardAmount.setVisible(false);
		lblAmount.setVisible(false);
		tblHold.getItems().clear();

		lblCardType.setVisible(false);
		txtLocalCustomer.clear();
		lblLocalCust.setText("");
		tblLoyaltyVoucher.getItems().clear();

	}

	private boolean verifyLocalCustomer() {

		if (null != lblLocalCust.getText()) {

			if (!lblLocalCust.getText().trim().isEmpty()) {
				ResponseEntity<LocalCustomerMst> localCustResp = RestCaller
						.getLocalCustomerbyName(lblLocalCust.getText());
				LocalCustomerMst localCustomerMst = localCustResp.getBody();

				if (null == localCustomerMst) {
					notifyMessage(3, "Local Customer Not Found..!");
					txtLocalCustomer.requestFocus();
					return false;
				}

				salesTransHdr.setLocalCustomerMst(localCustomerMst);

			}
		}
		return true;
	}

	@Subscribe
	public void popuplistner(WeighingMachineDataEvent voucherNoEvent) {

		String wData = voucherNoEvent.getWeightData().trim();

		if (wData.toUpperCase().contains("KG")) {

			txtWeighingMachineInput.setText(voucherNoEvent.getWeightData().trim());
		}

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	public void notifyFailureMessage(int duration, String msg) {

		Image img = new Image("failed.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	@FXML
	void actionReedem(ActionEvent event) {

		Double LoyaltyAmount = tblLoyaltyVoucher.getSelectionModel().getSelectedItem().getAmount();
		Double minAmtForRedemption = tblLoyaltyVoucher.getSelectionModel().getSelectedItem()
				.getMinAmountForRedemption();
		if (minAmtForRedemption > Double.parseDouble(txtCashtopay.getText())) {
			notifyFailureMessage(4, "Minimum Amount is Greater than Invoice Amount,Please Select Another Voucher");
			return;
		} else {
			/*
			 * SaveSales Receipt for LOYALTY Sales Mode
			 */
			//saveLoyaltyReceipt(LoyaltyAmount);
		}

	}

 

	private void callLoyaltyApi() {
		/*
		 * Api for loyalty
		 */
		// String msg= RestCaller.callLoyaltyApi(txtLocalCustomer.getText());
		LoyalityVouchers loyaltyVoucher = new LoyalityVouchers();
		loyaltyVoucher.setAmount(100.0);
		loyaltyVoucher.setBranchCode(SystemSetting.getUser().getBranchCode());
		loyaltyVoucher.setCardType("TEST");
		loyaltyVoucher.setExpiryDate(SystemSetting.getApplicationDate());
		loyaltyVoucher.setMinAmountForRedemption(250.0);
		loyaltyVoucher.setPercentage(10.0);
		loyaltyVoucher.setVoucherNumber("VN001");
		loyaltyVoucher.setVoucherLabel("TEST");
		loyaltyVoucherList.add(loyaltyVoucher);
		tblLoyaltyVoucher.setItems(loyaltyVoucherList);
		clVoucherLabel.setCellValueFactory(cellData -> cellData.getValue().getVoucherLabelProperty());
		clLoyaltyVoucher.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clPercentage.setCellValueFactory(cellData -> cellData.getValue().getPercentageProperty());
		clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clLoyaltyAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clMinAmountForRedemption
				.setCellValueFactory(cellData -> cellData.getValue().getMinAmountForRedemptionProperty());
		clLoyaltyCardType.setCellValueFactory(cellData -> cellData.getValue().getcardTypeProperty());

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

	@FXML
	void ClearAll(ActionEvent event) {

		txtItemname.clear();
		txtBarcode.clear();
		cmbUnit.getItems().clear();
		txtBatch.clear();
		txtQty.clear();
		txtRate.clear();
		txtposDiscount.clear();
		txtLocalCustomer.clear();
		tblLoyaltyVoucher.getItems().clear();
		txtTotalNumberofItems.clear();
		itemDetailTable.getItems().clear();
		txtCashtopay.clear();
		txtPaidamount.clear();
		txtChangeamount.clear();
		tblHold.getItems().clear();

		 
		cardSalesDtl = null;
		loyaltyVoucherList.clear();
		receiptModeList.clear();
		salesReceiptsList.clear();
		multiUnitList.clear();
		receiptModeMst = null;
		itemStockPopupCtl = null;
		rateBeforeDiscount = 0.0;
		unitMst = null;
		salesReceiptVoucherNo = null;
		saleListTable.clear();
		saleListItemTable.clear();
		saleTransListTable.clear();

		salesDtl = null;
		salesTransHdr = null;
		fixedQty = null;
		qtyTotal = 0;

		discountTotal = 0;
		taxTotal = 0;
		cessTotal = 0;
		discountBfTaxTotal = 0;
		grandTotal = 0;
		expenseTotal = 0;
		cardAmount = 0.0;
		custId = "";

		notifyMessage(3, "Cleared....!");

	}
	
	public boolean allowTransactionForTheDate(Date transaction,Date applicationDate,Date lastDayEndDate) {
	
		return transaction.after(lastDayEndDate) && (transaction.after(applicationDate)||
				transaction.compareTo(applicationDate)==0	);
		
		
	}

}
