package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.events.AccountHeadsPopupEvent;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PurchaseReport;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class SupplierWisePurchaseReportCtl {
	
	String taskid;
	String processInstanceId;
	
	private ObservableList<PurchaseHdr> purchaseList = FXCollections.observableArrayList();
	private ObservableList<PurchaseReport> reportList = FXCollections.observableArrayList();

	private EventBus eventBus = EventBusFactory.getEventBus();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;


    @FXML
    private TextField txtvoucher;

    @FXML
    private TextField txtItemGroup;

    @FXML
    private ListView<String> lsGroup;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnClear;
    
    @FXML
    private Button btnAdd;
    @FXML
    private TableView<PurchaseReport> tbPurchase;

    @FXML
    private TableColumn<PurchaseReport, String> clSupName;

    @FXML
    private TableColumn<PurchaseReport, String> clVoucherDate;

    @FXML
    private TableColumn<PurchaseReport, String> clItemName;

    @FXML
    private TableColumn<PurchaseReport, Number> clQty;

    @FXML
    private TableColumn<PurchaseReport, Number> clPurchaseRate;

    @FXML
    private TableColumn<PurchaseReport, Number> clAmount;

    @FXML
    private TableColumn<PurchaseReport, String> clSupInvNo;
    @FXML
    void actionAdd(ActionEvent event) {
    	lsGroup.getItems().add(txtItemGroup.getText());
    	lsGroup.getSelectionModel().select(txtItemGroup.getText());
    	txtItemGroup.clear();
    }

    @FXML
 	private void initialize() 
    {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	eventBus.register(this);
    }

    @FXML
    void actionShow(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	}
    	if(selectedItems.size()>0)
    	{
    	ResponseEntity<List<PurchaseReport>> purchasereportList = RestCaller.getSupplierWisePurchaseReport(cat,startDate,endDate,txtvoucher.getText());
    	reportList = FXCollections.observableArrayList(purchasereportList.getBody());
    	}
    	else
    	{
    		ResponseEntity<List<PurchaseReport>> purchasereportList = RestCaller.getSupplierWisePurchaseReportWithDate(startDate,endDate,txtvoucher.getText());
        	reportList = FXCollections.observableArrayList(purchasereportList.getBody());
    	}
    	fillTable();
    	
    }
    private void fillTable()
    {
    	tbPurchase.setItems(reportList);
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clPurchaseRate.setCellValueFactory(cellData -> cellData.getValue().getpurchaseRateProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		clSupInvNo.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
		clSupName.setCellValueFactory(cellData -> cellData.getValue().getsupNameProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		
    	
    }
    
	  @Subscribe
		public void popupOrglistner(CategoryEvent CategoryEvent) {

			Stage stage = (Stage) btnAdd.getScene().getWindow();
			if (stage.isShowing()) {
				txtItemGroup.setText(CategoryEvent.getCategoryName());
			}
		}
    @FXML
    void loadCategoryPopUp(MouseEvent event) {
    	try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			btnAdd.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
   
    public void notifyMessage(int duration, String msg, boolean success) {
		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @FXML
    void actionPrint(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	try {
    		JasperPdfReportService.SupplierwisePurchaseReport(startDate, endDate, cat,txtvoucher.getText());
		} catch (JRException e) {
			e.printStackTrace();
		}
    }
    @FXML
    void actionClear(ActionEvent event) {

    	tbPurchase.getItems().clear();
    	purchaseList.clear();
    	reportList.clear();
    	lsGroup.getItems().clear();
    	dpFromDate.setValue(null);
    	dpToDate.setValue(null);
    	txtvoucher.clear();
    }
    @FXML
    void loadSupplier(MouseEvent event) {


		System.out.println("-------------showPopup-------------");
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPartyPopUpCtl.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtItemGroup.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	
    }
    @Subscribe
	public void popuplistner(AccountHeadsPopupEvent accountHeadsPopupEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			txtvoucher.setText(accountHeadsPopupEvent.getPartyName());
				}
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}

}

