package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.javapos.print.WeighingMachinePrint;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.WeighingMachine;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.WeighBridgeMaterialMst;
import com.maple.mapleclient.entity.WeighBridgeTareWeight;
import com.maple.mapleclient.entity.WeighBridgeVehicleMst;
import com.maple.mapleclient.entity.WeighBridgeWeights;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.events.WeighBridgeTareWeightEvent;
import com.maple.mapleclient.events.WeighingMachineDataEvent;
import com.maple.mapleclient.events.WeighingMachineFirstWtEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WeighingMachineDisplayCtl {
	
	String taskid;
	String processInstanceId;
	
	StringProperty weightfromMachine = new SimpleStringProperty("");
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private static WeighBridgeWeights firstWeight;
    @FXML
    private TextField txtNumberOfCopies;
	@FXML
	private TextField txtfirstwtdate;

	@FXML
	private Button btnLookUpTareWt;

	@FXML
	private Button btnLookUpFirstWt;

	@FXML
	private TextField txtWeight;

	@FXML
	private Button btnPrint;

	@FXML
	private Button btngetvalue;

	@FXML
	private TextField txtvhno;

	@FXML
	private TextField firstwt;

	@FXML
	private Button btngetfirstwt;

	@FXML
	private Button btngettarewt;
	@FXML
	private Button btnReprint;
	@FXML
	private ComboBox<String> cmbvehicletype;
	@FXML
	private Button btnClear;

	@FXML
	private TextField vhrate;

	@FXML
	private Button btnsavetare;

	@FXML
	private ComboBox<String> combomaterial;

	@FXML
	private void initialize() {
		eventBus.register(this);
		txtWeight.textProperty().bindBidirectional(weightfromMachine);
		
		Thread t = new Thread(new WeighingMachine(SystemSetting.WEIGHBRIDGEPORT, SystemSetting.WEIGHBRIDGEBAUDRATE,
				SystemSetting.WEIGHBRIDGEDATABITS, SystemSetting.WEIGHBRIDGESTOPBIT, SystemSetting.WEIGHBRIDGEPARITY));
		t.start();
		
		
	}

	@FXML
	void actionClear(ActionEvent event) {
		txtvhno.clear();
		firstwt.clear();
		txtfirstwtdate.clear();
		cmbvehicletype.getSelectionModel().clearSelection();
		vhrate.clear();
		combomaterial.getSelectionModel().clearSelection();
	}

	@FXML
	void actionReprint(ActionEvent event) {

		/*
		 * Function to display popup window and show list of items to select.
		 */
		System.out.println("ssssssss=====loadItemPopup=====sssssssssss");

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/WeighinMachineReprintPopUp.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
//
//    private void setMaterialList() {
//    	
//    	combomaterial.getItems().clear();
//		setVehicleType();
//
//		setMaterialList();
//
//	}

	private void setMaterialList() {

		combomaterial.getItems().clear();

		ResponseEntity<List<WeighBridgeMaterialMst>> materialListResp = RestCaller.getAllWeighBridgeMaterialMst();

		List<WeighBridgeMaterialMst> materialList = materialListResp.getBody();
		for (WeighBridgeMaterialMst material : materialList) {
			combomaterial.getItems().add(material.getMaterial());

		}
	}

	private void setVehicleType() {

		cmbvehicletype.getItems().clear();

		ResponseEntity<List<WeighBridgeVehicleMst>> vehicleListResp = RestCaller.getAllWeighBridgeVehicleMst();

		List<WeighBridgeVehicleMst> vehicleList = vehicleListResp.getBody();
		for (WeighBridgeVehicleMst vehicle : vehicleList) {
			cmbvehicletype.getItems().add(vehicle.getVehicletype());
		}

	}

	@FXML
	void getvalue(ActionEvent event) {

		//// comPort.setComPortParameters(baudRate, 8, 1, 0);
	 

		/*Thread t = new Thread(new WeighingMachine(SystemSetting.WEIGHBRIDGEPORT, SystemSetting.WEIGHBRIDGEBAUDRATE,
				SystemSetting.WEIGHBRIDGEDATABITS, SystemSetting.WEIGHBRIDGESTOPBIT, SystemSetting.WEIGHBRIDGEPARITY));
		t.start();
*/
		
	 
	}

	@FXML
	void Print(ActionEvent event) {

		if (null == cmbvehicletype.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please select vehicle type", false);
			return;
		}

		if (null == combomaterial.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please select material ", false);
			return;

		}

		if (null == vhrate.getText()) {
			notifyMessage(5, "Please enter rate ", false);
			return;
		}
		if(null == txtvhno.getText())
		{
			notifyMessage(5, "Please enter vehicle No ", false);
			return;
		}

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getSystemBranch());

		ResponseEntity<WeighBridgeVehicleMst> vehicleType = RestCaller
				.getWeighBridgeVehicleMstByVehicleType(cmbvehicletype.getSelectionModel().getSelectedItem().toString());
		WeighBridgeVehicleMst weighBridgeVehicleMst = vehicleType.getBody();

		if (null == weighBridgeVehicleMst) {
			WeighBridgeVehicleMst weighBridgeVehicleMstToSave = new WeighBridgeVehicleMst();
			weighBridgeVehicleMstToSave.setBranchMst(branchMst);
			weighBridgeVehicleMstToSave.setCompanyMst(SystemSetting.getUser().getCompanyMst());
			weighBridgeVehicleMstToSave.setRate(Integer.parseInt(vhrate.getText()));
			weighBridgeVehicleMstToSave.setVehicletype(cmbvehicletype.getSelectionModel().getSelectedItem());
			ResponseEntity<WeighBridgeVehicleMst> vehicleTypeSaved = RestCaller
					.SaveWeighBridgeVehicleMst(weighBridgeVehicleMstToSave);
			weighBridgeVehicleMst = vehicleTypeSaved.getBody();
		}

		ResponseEntity<WeighBridgeMaterialMst> materilaSavedResp = RestCaller
				.getWeighBridgeMaterialMstByMaterial(combomaterial.getSelectionModel().getSelectedItem().toString());

		WeighBridgeMaterialMst weighBridgeMaterialMst = materilaSavedResp.getBody();

		if (null == weighBridgeMaterialMst) {
			WeighBridgeMaterialMst weighBridgeMaterialMstSaved = new WeighBridgeMaterialMst();
			weighBridgeMaterialMstSaved.setBranchMst(branchMst);
			weighBridgeMaterialMstSaved.setCompanyMst(SystemSetting.getUser().getCompanyMst());
			weighBridgeMaterialMstSaved.setMaterial(combomaterial.getSelectionModel().getSelectedItem());

			ResponseEntity<WeighBridgeMaterialMst> materialSaved = RestCaller
					.SaveWeighBridgeMaterialMst(weighBridgeMaterialMstSaved);

			weighBridgeMaterialMst = materialSaved.getBody();

		}

		WeighBridgeWeights weighBridgeWeights = new WeighBridgeWeights();

		weighBridgeWeights.setBranchMst(branchMst);
		weighBridgeWeights.setCompanyMst(SystemSetting.getUser().getCompanyMst());

		weighBridgeWeights.setMaterialtypeid(weighBridgeMaterialMst.getId());
		weighBridgeWeights.setVehicletypeid(weighBridgeVehicleMst.getId());

		weighBridgeWeights.setRate(weighBridgeVehicleMst.getRate());

		LocalDateTime today = LocalDateTime.now();

		weighBridgeWeights.setVoucherDate(today);

		String weightFromMachine = txtWeight.getText();
		weightFromMachine = weightFromMachine.replaceAll("KG", "");

		Integer machineWeight = Integer.parseInt(weightFromMachine.trim());

		Integer firstWeightValue = Integer.parseInt(firstwt.getText());

		weighBridgeWeights.setPreviousweight(firstWeightValue);

		if (null != firstWeight) {
			weighBridgeWeights.setPreviousweightid(firstWeight.getId());
		}

		weighBridgeWeights.setVehicleno(txtvhno.getText());

		weighBridgeWeights.setMachineweight(machineWeight);

		weighBridgeWeights.setFirstweightdate(txtfirstwtdate.getText());

		if (firstWeightValue > machineWeight) {
			int netValue = firstWeightValue - machineWeight;
			weighBridgeWeights.setNetweight(netValue + "");
		} else {
			int netValue = machineWeight - firstWeightValue;
			weighBridgeWeights.setNetweight(netValue + "");
		}
		String vNo = RestCaller.getVoucherNumber("WB");
		weighBridgeWeights.setVoucherNumber(vNo);

		ResponseEntity<WeighBridgeWeights> weighBridgeWeightsSaved = RestCaller
				.SaveWeighBridgeWeights(weighBridgeWeights);

		if (null != firstWeight) {
			firstWeight.setNextweight(machineWeight);
			firstWeight.setNextweightid(weighBridgeWeightsSaved.getBody().getId());

			ResponseEntity<WeighBridgeWeights> firstWeightSaved = RestCaller.SaveWeighBridgeWeights(firstWeight);
		}

		firstWeight = null;

		WeighingMachinePrint wprint = new WeighingMachinePrint();

		String wtToPrint = "";
		if (null == txtWeight.getText()) {
			wtToPrint = "00000";
		} else {
			wtToPrint = txtWeight.getText();
		}

		try {

			Integer firstWtValue = Integer.parseInt(firstwt.getText());

			String weightFromMachine2 = wtToPrint;
			weightFromMachine2 = weightFromMachine2.replaceAll("KG", "");

			Integer machineWtValue = Integer.parseInt(weightFromMachine2.trim());
			Integer noOfCopies = 1;
			if(!txtNumberOfCopies.getText().trim().isEmpty())
			{
				noOfCopies =Integer.parseInt(txtNumberOfCopies.getText());
			}
			
			
			if (firstWtValue > machineWtValue) {
				int netValue = firstWtValue - machineWtValue;
				while(noOfCopies>0)
				{
				wprint.PrintInvoiceThermalPrinter(firstWtValue + "", machineWtValue + "", netValue + "",
						txtvhno.getText(), vhrate.getText(), getCurrentTimeStamp(), vNo);
				noOfCopies= noOfCopies-1;
				}
			} else {
				int netValue = machineWtValue - firstWtValue;
				while(noOfCopies>0)
				{
				wprint.PrintInvoiceThermalPrinter(machineWtValue + "", firstWtValue + "", netValue + "",
						txtvhno.getText(), vhrate.getText(), getCurrentTimeStamp(), vNo);
				noOfCopies= noOfCopies-1;
				}
			}

			/*
			 * String firstWeight, String secondWt, String netWeight, String vehicleNO,
			 * String rateAmount , String dateInvoice)
			 */
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		firstwt.setText("0");
		txtvhno.clear();
	}

	public static String getCurrentTimeStamp() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm");// dd/MM/yyyy
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	@FXML
	void getTareWeight(ActionEvent event) {

		// fetch from WeghBridgeTareweight where Vehicle number match.

		String vehicleNo = txtvhno.getText();

		ResponseEntity<WeighBridgeTareWeight> weighBridgeTareWeight = RestCaller
				.getWeighBridgeTareWeightByVehicleNo(vehicleNo);

		if (null != weighBridgeTareWeight) {
			firstwt.setText(weighBridgeTareWeight.getBody().getTareweight() + "");
		}

	}

	@FXML
	void getLookUpFirstWt(ActionEvent event) {

		loadFirstWeight();
	}

	@FXML
	void getLookUpTareWt(ActionEvent event) {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WehighingBridgeTareWeightPopUp.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistnerTareWeight(WeighBridgeTareWeightEvent weighBridgeTareWeightEvent) {

		Stage stage = (Stage) btngetfirstwt.getScene().getWindow();
		if (stage.isShowing()) {

			firstwt.setText(weighBridgeTareWeightEvent.getTareweight().toString());
			txtvhno.setText(weighBridgeTareWeightEvent.getVehicleno());

		}

	}

	private void loadFirstWeight() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WeighingMachineFirstWtPopUp.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popuplistner(WeighingMachineFirstWtEvent weighingMachineFirstWtEvent) {

		Stage stage = (Stage) btngetfirstwt.getScene().getWindow();
		if (stage.isShowing()) {

			firstwt.setText(weighingMachineFirstWtEvent.getFirstWeight().toString());
			txtvhno.setText(weighingMachineFirstWtEvent.getVehicleNo());
			txtfirstwtdate.setText(weighingMachineFirstWtEvent.getVoucherDate().toString());
		}

	}

	@FXML
	void getFirstWeight(ActionEvent event) {
		// Find machineWeight from first record in WeighBridgeWeights where firstWeight
		// is null/0 sort on voucherDate in descending.

		String vehicleNo = txtvhno.getText();

		ResponseEntity<WeighBridgeWeights> weighBridgeWeights = RestCaller.getFirstWeightByVehicleNo(vehicleNo);

		if (null != weighBridgeWeights) {

			firstWeight = weighBridgeWeights.getBody();

			firstwt.setText(firstWeight.getMachineweight() + "");
		}

	}

	@FXML
	void savetareweght(ActionEvent event) {

		String vehicleNo = txtvhno.getText();

		ResponseEntity<WeighBridgeTareWeight> weighBridgeTareWeightBody = RestCaller
				.getWeighBridgeTareWeightByVehicleNo(vehicleNo);

		if (null != weighBridgeTareWeightBody.getBody()) {

			WeighBridgeTareWeight weighBridgeTareWeight = weighBridgeTareWeightBody.getBody();

			String weightFromMachine = txtWeight.getText();
			weightFromMachine = weightFromMachine.replaceAll("KG", "");

			Integer tareWt = Integer.parseInt(weightFromMachine.trim());

			weighBridgeTareWeight.setTareweight(tareWt);

			ResponseEntity<WeighBridgeTareWeight> weighBridgeTareWeightSaved = RestCaller
					.SaveWeighBridgeTareWeight(weighBridgeTareWeight);

		} else {

			WeighBridgeTareWeight weighBridgeTareWeight = new WeighBridgeTareWeight();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getSystemBranch());

			weighBridgeTareWeight.setBranchMst(branchMst);
			weighBridgeTareWeight.setCompanyMst(SystemSetting.getUser().getCompanyMst());

			String weightFromMachine = txtWeight.getText();
			weightFromMachine = weightFromMachine.replaceAll("KG", "");

			Integer tareWt = Integer.parseInt(weightFromMachine.trim());

			weighBridgeTareWeight.setTareweight(tareWt);
			weighBridgeTareWeight.setVehicleno(txtvhno.getText());

			ResponseEntity<WeighBridgeTareWeight> weighBridgeTareWeightSaved = RestCaller
					.SaveWeighBridgeTareWeight(weighBridgeTareWeight);
		}

	}

	@FXML
	void setMaterial(MouseEvent event) {

		setMaterialList();

	}

	@FXML
	void setVehicleType(MouseEvent event) {

		setVehicleType();

	}

	@FXML
	void onactionvehicle(ActionEvent event) {
		if (null != event.getSource()
				&& (null != ((ComboBox) event.getSource()).getSelectionModel().getSelectedItem())) {

			String selectedITem = ((ComboBox) event.getSource()).getSelectionModel().getSelectedItem().toString();

			ResponseEntity<WeighBridgeVehicleMst> vehicleType = RestCaller
					.getWeighBridgeVehicleMstByVehicleType(selectedITem);
			WeighBridgeVehicleMst weighBridgeVehicleMst = vehicleType.getBody();
			if (null != weighBridgeVehicleMst) {
				vhrate.setText(weighBridgeVehicleMst.getRate() + "");

				vhrate.setEditable(false);
			} else {
				vhrate.setEditable(true);

			}
			System.out.println(selectedITem);
		}
	}

	@FXML
	void vhNoEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			btngetfirstwt.requestFocus();
	}

	@FXML
	void getFirstWtEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			btngettarewt.requestFocus();

	}

	@FXML
	void getTareWtEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			firstwt.requestFocus();
	}

	@FXML
	void firstWtEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			cmbvehicletype.requestFocus();
	}

	@FXML
	void vhTypeEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			vhrate.requestFocus();
	}

	@FXML
	void rateEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			combomaterial.requestFocus();
	}

	@FXML
	void materialEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			btngetvalue.requestFocus();
	}

	@FXML
	void readyEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			btnPrint.requestFocus();
	}

	@FXML
	void printEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			txtvhno.requestFocus();
	}

	@FXML
	void savetareEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER)
			btnPrint.requestFocus();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@Subscribe
	synchronized public void popuplistner(WeighingMachineDataEvent voucherNoEvent) {

		String wData = voucherNoEvent.getWeightData().trim();

		
		if (wData.toUpperCase().contains("KG")) {
			
			Platform.runLater(() -> {
				weightfromMachine.set( voucherNoEvent.getWeightData().trim());
            });
			

			
		}
  

	}
	
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}
  

}
