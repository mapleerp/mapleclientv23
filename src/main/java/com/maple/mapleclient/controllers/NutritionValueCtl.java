package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.TSCBarcode;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.entity.NutritionValueDtl;
import com.maple.mapleclient.entity.NutritionValueMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class NutritionValueCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<NutritionMst> nutritionList = FXCollections.observableArrayList();
	private ObservableList<NutritionValueDtl> nutrintionValueList = FXCollections.observableArrayList();

	NutritionValueDtl nutritionValueDtl = null;
	NutritionValueMst nutritionValueMst = null;
	private EventBus eventBus = EventBusFactory.getEventBus();

	ItemMst itemmst;

    @FXML
    private TextField txtservingSize;

    @FXML
    private TextField txtcalories;

    @FXML
    private Button btnPrint;
    @FXML
    private TextField txtFat;
    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnFinalSave;
    @FXML
    private TextField txtprintername;

    @FXML
    private ComboBox<String> cmbNutrition;

    @FXML
    private TextField txtNutritionValue;

    @FXML
    private TableView<NutritionValueDtl> tblNutrition;
    @FXML
    private TableColumn<NutritionValueDtl, String> clNutrition;

    @FXML
    private TableColumn<NutritionValueDtl, String> clValue;
    

    @FXML
    private TableColumn<NutritionValueDtl, String> clSerial;
    @FXML
    private TableColumn<NutritionValueDtl, String> clPercentage;
    @FXML
    private TextField txtpercentage;
    @FXML
    private Button btnAdd;
    @FXML
    private TextField txtSerial;
    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFetch;

    @FXML
    private Button btnClear;

    
    @FXML
    private TextField txtNoOfcopies;
    @FXML
	private void initialize()
	{
    	txtservingSize.setEditable(false);
    	txtcalories.setEditable(false);
    	txtFat.setEditable(false);
    	txtpercentage.setEditable(false);
    	txtSerial.setEditable(false);
    //	txtprintername.setEditable(false);
    	txtNutritionValue.setEditable(false);
    	btnDelete.setVisible(false);
    	btnAdd.setVisible(false);
    	btnFinalSave.setVisible(false);
    	txtprintername.setText(SystemSetting.getNutrition_facts_printer());
		eventBus.register(this);
    	ResponseEntity<List<NutritionMst>> savedNutritions = RestCaller.getAllNutritions();
    	nutritionList = FXCollections.observableArrayList(savedNutritions.getBody());
    	for (NutritionMst nutrtionMst:nutritionList)
    	{
    	cmbNutrition.getItems().add(nutrtionMst.getNutrition());
    	}
    	tblNutrition.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
					
					if (null != newSelection.getId()) {
						nutritionValueDtl = new NutritionValueDtl();
						nutritionValueDtl.setId(newSelection.getId());

					}

				}
			
		});
	}
    @FXML
    void finalSave(ActionEvent event) {
    	if(null != nutritionValueMst)
    	{
    		
    		nutritionValueMst.setItemMst(itemmst);
    		nutritionValueMst.setCalories(txtcalories.getText());
    		nutritionValueMst.setFat(txtFat.getText());
    		nutritionValueMst.setServingSize(txtservingSize.getText());
    		nutritionValueMst.setPrinterName(txtprintername.getText());
    		RestCaller.updateNutritionValueMst(nutritionValueMst);
    		txtcalories.clear();
        	txtFat.clear();
        	txtItemName.clear();
        	txtservingSize.clear();
        	tblNutrition.getItems().clear();
        	txtpercentage.clear();
        	nutritionValueDtl = null;
        	nutritionValueMst = null;
        	nutritionList.clear();
    	}
    }

    @FXML
    void clearItems(ActionEvent event) {

    	txtcalories.clear();
    	txtFat.clear();
    	txtItemName.clear();
    	txtservingSize.clear();
    	tblNutrition.getItems().clear();
    	txtpercentage.clear();
    	nutritionValueDtl = null;
    	nutritionValueMst = null;
    	nutritionList.clear();
    	
    }

    @FXML
    void loadItem(MouseEvent event) {
    	
    	showItem();
    }
   
    
    @FXML
    void deleteItems(ActionEvent event) {
    	if(null != nutritionValueDtl.getId())
    	{
    		RestCaller.deleteNutritionValueDtl(nutritionValueDtl.getId());
    		ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller.getNutritionValueDtlByMstId(nutritionValueMst);
        	nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
        	fillTable();
    	}

    }
  
    @FXML
    void AddItems(ActionEvent event) {

  
    	addItems();
    }
    private void addItems()
    {

    	ResponseEntity<NutritionValueMst> savedNutritionMst = RestCaller.getNutritionValueMstByItemMst(itemmst);

    	if(null != savedNutritionMst.getBody())
    	{
    		nutritionValueMst = new NutritionValueMst();

    		fetchData();
    	}
    	
    	if(null == nutritionValueMst)
    	{
    		nutritionValueMst = new NutritionValueMst();
    		nutritionValueMst.setItemMst(itemmst);
    		nutritionValueMst.setCalories(txtcalories.getText());
    		nutritionValueMst.setFat(txtFat.getText());
    		nutritionValueMst.setServingSize(txtservingSize.getText());
    		nutritionValueMst.setPrinterName(txtprintername.getText());

    		ResponseEntity<NutritionValueMst> respentity = RestCaller.saveNutritionValueMst(nutritionValueMst);
    		nutritionValueMst = respentity.getBody();
    	}
    		
    	if(txtSerial.getText().isEmpty())
    	{
    		notifyMessage(5,"Please Type Serial Number");
    		txtSerial.requestFocus();
    		return;
    	}
    	nutritionValueDtl = new NutritionValueDtl();
    	nutritionValueDtl.setNutritionValueMst(nutritionValueMst);
    	nutritionValueDtl.setNutrition(cmbNutrition.getSelectionModel().getSelectedItem());
    	nutritionValueDtl.setValue(txtNutritionValue.getText());
    	
    	nutritionValueDtl.setPercentage(txtpercentage.getText());
    	
    	nutritionValueDtl.setSerial(txtSerial.getText());
    	
    	ResponseEntity<NutritionValueDtl> getNutrition = RestCaller.getNutritionValueMstByItemMstAndNutrition(nutritionValueMst.getId(),cmbNutrition.getSelectionModel().getSelectedItem());
    	if(null != getNutrition.getBody())
    	{
    		nutritionValueDtl = getNutrition.getBody();
    		
    		nutritionValueDtl.setNutritionValueMst(nutritionValueMst);
        	nutritionValueDtl.setNutrition(cmbNutrition.getSelectionModel().getSelectedItem());
        	nutritionValueDtl.setValue(txtNutritionValue.getText());
        	
        	nutritionValueDtl.setPercentage(txtpercentage.getText());
        	
        	nutritionValueDtl.setSerial(txtSerial.getText());
        	
    		RestCaller.updateNUtritionValueDtl(nutritionValueDtl);
    		ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller.getNutritionValueDtlByMstId(nutritionValueMst);
        	nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
    	}
    	else
    	{
    	ResponseEntity<NutritionValueDtl> respentity = RestCaller.saveNutritionValueDtl(nutritionValueDtl);
    	nutritionValueDtl = respentity.getBody();
    	nutrintionValueList.add(nutritionValueDtl);
    	}
    	fillTable();
    	cmbNutrition.getSelectionModel().clearSelection();
    	txtNutritionValue.clear();
    	txtpercentage.clear();
    	txtSerial.clear();
    	cmbNutrition.requestFocus();
    }

    @FXML
    void fetchData(ActionEvent event) {

    	 fetchData();
    }
    private void fetchData()
    {
    	ResponseEntity<NutritionValueMst> savedNutritionMst = RestCaller.getNutritionValueMstByItemMst(itemmst);
    
    	nutritionValueMst = savedNutritionMst.getBody();
    	if(null !=nutritionValueMst )
    	{
    	txtcalories.setText(savedNutritionMst.getBody().getCalories());
    	txtFat.setText(savedNutritionMst.getBody().getFat());
    	 txtservingSize.setText(savedNutritionMst.getBody().getServingSize());  	
    	 txtprintername.setText(SystemSetting.getNutrition_facts_printer());
    	ResponseEntity<List<NutritionValueDtl>> respentity = RestCaller.getNutritionValueDtlByMstId(nutritionValueMst);
    	nutrintionValueList = FXCollections.observableArrayList(respentity.getBody());
    	fillTable();
    	}
    	else
    	{
    		txtcalories.clear();
    		txtFat.clear();
    		txtItemName.clear();
    		txtNutritionValue.clear();
    		txtpercentage.clear();
    		txtSerial.clear();
    		txtservingSize.clear();
    		tblNutrition.getItems().clear();
    	}
    }
    private void fillTable()
    {
    	tblNutrition.setItems(nutrintionValueList);
		clNutrition.setCellValueFactory(cellData -> cellData.getValue().getnutritionNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());
		clPercentage.setCellValueFactory(cellData -> cellData.getValue().getpercentageProperty());
		clSerial.setCellValueFactory(cellData -> cellData.getValue().getserialProperty());


    }
    @Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			itemmst = new ItemMst();
			txtItemName.setText(itemPopupEvent.getItemName());
			itemmst.setId(itemPopupEvent.getItemId());
			ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(itemmst.getId());
			itemmst = respentity.getBody();
			// txtQty.setText(Integer.toString(itemPopupEvent.getQty()));

		}

	}

    @FXML
    void servingSizeOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtcalories.requestFocus();
    	}

    }
    @FXML
    void fatOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		cmbNutrition.requestFocus();
    	}
    }
    @FXML
    void valueOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtpercentage.requestFocus();
    	}
    }
    @FXML
    void percentageOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtSerial.requestFocus();
    	}
    }
    @FXML
    void serialOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		btnAdd.requestFocus();
    	}
    }

    @FXML
    void addOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		addItems();
    	}
    }

    @FXML
    void caloriesOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtFat.requestFocus();
    	}
    }
    @FXML
    void nutritionOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		txtNutritionValue.requestFocus();
    	}
    }
    private void showItem()
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			txtservingSize.requestFocus();
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public void notifyMessage(int duration, String msg) {
  		System.out.println("OK Event Receid");
  		Image img = new Image("done.png");
  		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
  				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
  				.onAction(new EventHandler<ActionEvent>() {
  					@Override
  					public void handle(ActionEvent event) {
  						System.out.println("clicked on notification");
  					}
  				});
  		notificationBuilder.darkStyle();
  		notificationBuilder.show();
  	}
    
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    
    @FXML
    void actionPrint(ActionEvent event) {
    	if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter item  name", false);
			return;
		}
    	if (txtservingSize.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter item name ", false);
			return;
		}
    	if (txtcalories.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter calories", false);
			return;
		}
    	if (txtFat.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter fat", false);
			return;
		}
    	if (txtNoOfcopies.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter no of copies", false);
			return;
		}
    	String itemName=txtservingSize.getText();
    	String servingSize=txtservingSize.getText();
    	String calories=txtcalories.getText();
    	String fat=txtFat.getText();
    	Integer nofCopies=Integer.valueOf(txtNoOfcopies.getText());
    	if (txtprintername.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter printer name", false);
			return;
		}
    	String printerName=SystemSetting.getNutrition_facts_printer();
  	     List<NutritionValueDtl> dtlList=new ArrayList<NutritionValueDtl>();
		  for(int i=0;i<nutrintionValueList.size();i++) {
			  
			  System.out.println("Looping for Nutri value ");
			  
			  NutritionValueDtl nutritionDtl =new NutritionValueDtl();
			  nutritionDtl.setNutrition(nutrintionValueList.get(i).getNutrition());
			  nutritionDtl.setValue(nutrintionValueList.get(i).getValue());
			  nutritionDtl.setPercentage(nutrintionValueList.get(i).getPercentage());
			  nutritionDtl.setSerial(nutrintionValueList.get(i).getSerial());
			  dtlList.add(nutritionDtl);
		  
		  }


		System.out.println("Starting pring fn");
    	TSCBarcode.nutritionPrint(printerName,itemName, servingSize, calories, fat,dtlList ,nofCopies);

    }
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
