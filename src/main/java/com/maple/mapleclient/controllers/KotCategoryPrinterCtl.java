package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KotCategoryMst;
import com.maple.mapleclient.entity.KotItemMst;
import com.maple.mapleclient.entity.PrinterMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class KotCategoryPrinterCtl {
	String taskid;
	String processInstanceId;

	private EventBus eventBus = EventBusFactory.getEventBus();

	KotItemMst kotItemMst = null;
	private ObservableList<KotItemMst> KotItemMstList = FXCollections.observableArrayList();
	private ObservableList<ItemMst> itemMstList = FXCollections.observableArrayList();
	private ObservableList<ItemMst> itemNotInKotList = FXCollections.observableArrayList();
	private ObservableList<CategoryMst> catList = FXCollections.observableArrayList();

	private ObservableList<KotCategoryMst> KotCategoryMstList = FXCollections.observableArrayList();
	@FXML
	private Button btnItemNotInKot;

	@FXML
	private TextField txtSearchItemName;

	@FXML
	private TableView<CategoryMst> tbCatNotInKot;

	@FXML
	private Button btnSearch;

	@FXML
	private TableColumn<CategoryMst, String> clCategoryNotInKot;

	@FXML
	private TableView<ItemMst> tbItemNotInKot;

	@FXML
	private TableColumn<ItemMst, String> clItemNotInKOt;

	@FXML
	private TextField txtIteName;

	@FXML
	private Button btncategoryNotSetinKot;

	@FXML
	private TableView<KotItemMst> tblItems;

	StringProperty SearchString = new SimpleStringProperty();

	@FXML
	private TableColumn<KotItemMst, String> clItemName;

	@FXML
	private TableView<ItemMst> tbItemWithoutCat;

	@FXML
	private TableColumn<ItemMst, String> clItemWithoutCat;

	@FXML
	private Button btnItemSave;

	@FXML
	private Button btnItemDelete;

	@FXML
	private Button btnItemShowAll;

	KotCategoryMst kotCategoryMst = null;

	@FXML
	private ComboBox<String> cmbCategory;

	@FXML
	private Button btnShowItemsWithNull;

	@FXML
	private ComboBox<String> cmbPrinter;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowall;

	@FXML
	private TableView<KotCategoryMst> tbCategoryPrint;

	@FXML
	private TableColumn<KotCategoryMst, String> clCategory;

	@FXML
	private TableColumn<KotCategoryMst, String> clPrinter;

	@FXML
	private void initialize() {
		eventBus.register(this);
		txtSearchItemName.textProperty().bindBidirectional(SearchString);
		tblItems.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					kotItemMst = new KotItemMst();
					kotItemMst.setId(newSelection.getId());
				}
			}
		});

		tbCategoryPrint.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					kotCategoryMst = new KotCategoryMst();
					kotCategoryMst.setId(newSelection.getId());
				}
			}
		});
		ArrayList cat = new ArrayList();

		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				cmbCategory.getItems().add((String) categoryName);

			}

		}

		ResponseEntity<List<PrinterMst>> getAllPrinter = RestCaller.getAllPrinterMst();
		for (PrinterMst printerMst : getAllPrinter.getBody()) {
			cmbPrinter.getItems().add(printerMst.getPrinterName());
		}
		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				searchItemNotInKotItems((String) newValue);
			}

		});
	}

	// -------------------------version 1.0 -23-2-21- surya

	@FXML
	private Button btnAddAllItems;

	@FXML
	void AddAllItemsToKotItems(ActionEvent event) {

		ResponseEntity<List<ItemMst>> getitems = RestCaller.searchItemNotInKot("");
		itemNotInKotList = FXCollections.observableArrayList(getitems.getBody());

		for (ItemMst itemMst : itemNotInKotList) {

			ResponseEntity<List<KotItemMst>> kotItemMstListResp = RestCaller.getAllKotItemMstByItemId(itemMst.getId());
			List<KotItemMst> kotItemMstList = kotItemMstListResp.getBody();

			if (kotItemMstList.size() > 0) {

				continue;
			}

			if (null != itemMst.getCategoryId()) {
				ResponseEntity<KotCategoryMst> kotCat = RestCaller.getKotCategoryMstByCatId(itemMst.getCategoryId());
				if (null != kotCat.getBody()) {

					kotItemMst = new KotItemMst();
					kotItemMst.setItemId(itemMst.getId());
					ResponseEntity<KotItemMst> respentity = RestCaller.saveKotItemmst(kotItemMst);
					kotItemMst = respentity.getBody();
					KotItemMstList.add(kotItemMst);
					fillItemTable();
					txtIteName.clear();
					txtIteName.requestFocus();
					kotItemMst = null;
				}
			}

		}

		ResponseEntity<List<ItemMst>> getitemsnotinkotResp = RestCaller.searchItemNotInKot("");
		itemNotInKotList = FXCollections.observableArrayList(getitemsnotinkotResp.getBody());

		fillItemNotInKot();

	}

	// -------------------------version 1.0 -23-2-21- surya end

	@FXML
	void actionDelete(ActionEvent event) {

		if (null == kotCategoryMst) {
			return;
		}
		if (null == kotCategoryMst.getId()) {
			return;
		}

		RestCaller.deleteKOtCatPrint(kotCategoryMst.getId());
		ResponseEntity<List<KotCategoryMst>> getAllKotPrint = RestCaller.getAllKotCategoryMst();
		KotCategoryMstList = FXCollections.observableArrayList(getAllKotPrint.getBody());
		fillTable();
		ResponseEntity<List<KotItemMst>> kotitemList = RestCaller.getAllKotItemMst();
		KotItemMstList = FXCollections.observableArrayList(kotitemList.getBody());
		fillItemTable();
	}

	@FXML
	void actionSave(ActionEvent event) {

		kotCategoryMst = new KotCategoryMst();
		ResponseEntity<CategoryMst> getCatByName = RestCaller
				.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem());
		kotCategoryMst.setCategoryId(getCatByName.getBody().getId());
		ResponseEntity<PrinterMst> getPrinter = RestCaller
				.getPrinterByName(cmbPrinter.getSelectionModel().getSelectedItem());
		kotCategoryMst.setPrinterId(getPrinter.getBody().getId());
		ResponseEntity<KotCategoryMst> getKotSaved = RestCaller
				.getKotCategoryMstByCatIdAndPrinterId(kotCategoryMst.getCategoryId(), kotCategoryMst.getPrinterId());
		if (null == getKotSaved.getBody()) {
			ResponseEntity<KotCategoryMst> respentity = RestCaller.saveKotCategoryMst(kotCategoryMst);

			kotCategoryMst = respentity.getBody();
			KotCategoryMstList.add(kotCategoryMst);

			fillTable();
		}

		cmbCategory.getSelectionModel().clearSelection();
		cmbPrinter.getSelectionModel().clearSelection();
		kotCategoryMst = null;
	}

	private void fillTable() {
		tbCategoryPrint.setItems(KotCategoryMstList);
		for (KotCategoryMst kotCat : KotCategoryMstList) {
			ResponseEntity<PrinterMst> getPrinter = RestCaller.getPrinterById(kotCat.getPrinterId());
			if (null != getPrinter.getBody()) {
				kotCat.setPrinterName(getPrinter.getBody().getPrinterName());
			}
			ResponseEntity<CategoryMst> getCat = RestCaller.getCategoryById(kotCat.getCategoryId());
			if (null != getCat.getBody()) {
				kotCat.setCategoryName(getCat.getBody().getCategoryName());
			}
		}
		clCategory.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
		clPrinter.setCellValueFactory(cellData -> cellData.getValue().getprinterNameProperty());

	}

	@FXML
	void actionShowAll(ActionEvent event) {

		ResponseEntity<List<KotCategoryMst>> getAllKotPrint = RestCaller.getAllKotCategoryMst();
		KotCategoryMstList = FXCollections.observableArrayList(getAllKotPrint.getBody());
		fillTable();
	}

	@FXML
	void actionItemDelete(ActionEvent event) {

		if (null == kotItemMst) {
			return;
		}
		if (null == kotItemMst.getId()) {
			return;
		}
		RestCaller.deleteKotItemMstById(kotItemMst.getId());
		ResponseEntity<List<KotItemMst>> kotitemList = RestCaller.getAllKotItemMst();
		KotItemMstList = FXCollections.observableArrayList(kotitemList.getBody());
		fillItemTable();
	}

	@FXML
	void actionItemSave(ActionEvent event) {

		if (txtIteName.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Item", false);
			txtIteName.requestFocus();
			return;

		}

		ResponseEntity<ItemMst> itemmstresp = RestCaller.getItemByNameRequestParam(txtIteName.getText());
		// -------------------------version 1.0 -23-2-21- surya

		ItemMst itemMst = itemmstresp.getBody();
		if (null == itemMst) {
			notifyMessage(3, "Item not found", false);
			return;
		}

		ResponseEntity<List<KotItemMst>> kotItemMstListResp = RestCaller.getAllKotItemMstByItemId(itemMst.getId());
		List<KotItemMst> kotItemMstList = kotItemMstListResp.getBody();

		if (kotItemMstList.size() > 0) {
			notifyMessage(3, "Item already saved", false);
			return;
		}

		// -------------------------version 1.0 -23-2-21- surya end

		if (null != itemmstresp.getBody().getCategoryId()) {
			ResponseEntity<KotCategoryMst> kotCat = RestCaller
					.getKotCategoryMstByCatId(itemmstresp.getBody().getCategoryId());
			if (null == kotCat.getBody()) {
				ResponseEntity<CategoryMst> catmst = RestCaller.getCategoryById(itemmstresp.getBody().getCategoryId());
				notifyMessage(3, "Please Add " + catmst.getBody().getCategoryName()
						+ " to the Printer Configuration before Saving item", false);
				return;
			}
		} else {
			notifyMessage(3, "Add Category to the Item", false);
			return;
		}
		kotItemMst = new KotItemMst();
		kotItemMst.setItemId(itemmstresp.getBody().getId());
		ResponseEntity<KotItemMst> respentity = RestCaller.saveKotItemmst(kotItemMst);
		kotItemMst = respentity.getBody();
		KotItemMstList.add(kotItemMst);
		fillItemTable();
		txtIteName.clear();
		txtIteName.requestFocus();
		kotItemMst = null;
	}

	private void fillItemTable() {
		for (KotItemMst kotitem : KotItemMstList) {
			ResponseEntity<ItemMst> items = RestCaller.getitemMst(kotitem.getItemId());
			if (null != items.getBody()) {
				kotitem.setItemName(items.getBody().getItemName());
			}
		}
		tblItems.setItems(KotItemMstList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	void actionItemShowall(ActionEvent event) {

		ResponseEntity<List<KotItemMst>> kotitemList = RestCaller.getAllKotItemMst();
		KotItemMstList = FXCollections.observableArrayList(kotitemList.getBody());
		fillItemTable();
	}

	@FXML
	void loadItemPopUp(MouseEvent event) {
		showItemMst();
	}

	private void showItemMst() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			btnItemSave.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnItemSave.getScene().getWindow();
		if (stage.isShowing()) {
			txtIteName.setText(itemPopupEvent.getItemName());
		}
	}

	@FXML
	void actionShowItemWithNullCat(ActionEvent event) {

		ResponseEntity<List<ItemMst>> getitems = RestCaller.getItemWithNullCategory();
		itemMstList = FXCollections.observableArrayList(getitems.getBody());
		fillItemWithNullCat();

	}

	private void fillItemWithNullCat() {
		tbItemWithoutCat.setItems(itemMstList);
		clItemWithoutCat.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

	}

	private void fillItemNotInKot() {
		tbItemNotInKot.setItems(itemNotInKotList);
		clItemNotInKOt.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

	}

	private void searchItemNotInKotItems(String newValue) {
		ResponseEntity<List<ItemMst>> getitems = RestCaller.searchItemNotInKot(newValue);
		itemNotInKotList = FXCollections.observableArrayList(getitems.getBody());
		fillItemNotInKot();
	}

	@FXML
	void actionItemNotInKot(ActionEvent event) {
		ResponseEntity<List<ItemMst>> getitems = RestCaller.getItemNotInKot();
		itemNotInKotList = FXCollections.observableArrayList(getitems.getBody());
		fillItemNotInKot();
	}

	@FXML
	void actionCategoryNotInKot(ActionEvent event) {
		ResponseEntity<List<String>> getitems = RestCaller.getCategoryNotInKotCategory();
		for (int i = 0; i < getitems.getBody().size(); i++) {
			ResponseEntity<CategoryMst> catMst = RestCaller.getCategoryById(getitems.getBody().get(i));
			catList = FXCollections.observableArrayList(catMst.getBody());
			fillCateList();
		}

	}

	private void fillCateList() {
		tbCatNotInKot.setItems(catList);
		clCategoryNotInKot.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());

	}

	@FXML
	void actionSearch(ActionEvent event) {
		ResponseEntity<List<KotItemMst>> kotitemList = RestCaller.getAllKotItemMstByItemName(txtIteName.getText());
		KotItemMstList = FXCollections.observableArrayList(kotitemList.getBody());
		fillItemTable();
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
