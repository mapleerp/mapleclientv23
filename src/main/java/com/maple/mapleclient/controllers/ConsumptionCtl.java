package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ActualProductionDtl;
import com.maple.mapleclient.entity.ConsumptionDtl;
import com.maple.mapleclient.entity.ConsumptionHdr;
import com.maple.mapleclient.entity.ConsumptionReasonMst;
import com.maple.mapleclient.entity.DepartmentMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ConsumptionCtl {
	String taskid;
	String processInstanceId;
	String storeName = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	ConsumptionHdr consumptionHdr = null;
	ConsumptionDtl consumptionDtl = null;
	private ObservableList<ConsumptionDtl> consumptionDtlList = FXCollections.observableArrayList();
	
	List<DepartmentMst> department = null;

    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtMrp;

    @FXML
    private TextField txtQty;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;

    @FXML
    private TextField txtStore;

    @FXML
    private TableView<ConsumptionDtl> tbConsumptionDtl;

    @FXML
    private TableColumn<ConsumptionDtl, String> clItemName;

    @FXML
    private TableColumn<ConsumptionDtl, String> clUnit;
    @FXML
    private TableColumn<ConsumptionDtl, String> clreason;
    @FXML
    private TableColumn<ConsumptionDtl, Number> clMrp;

    @FXML
    private TableColumn<ConsumptionDtl, Number> clQty;

    @FXML
    private TableColumn<ConsumptionDtl, Number> clAmount;
    @FXML
    private TableColumn<ConsumptionDtl, String> clStore;

    @FXML
    private TextField txtTotalAmount;

    @FXML
    private ComboBox<String> cmbConsumption;
    
    @FXML
    private ComboBox<String> cmbDepartment;

    
    @FXML
    private TextField txtBatch;
    @FXML
    void initialize()
    {
    	eventBus.register(this);
    	ResponseEntity<List<ConsumptionReasonMst>> getAll = RestCaller.getAllConsumptionReason();

    	for(ConsumptionReasonMst consmMst:getAll.getBody())
    	{
    		cmbConsumption.getItems().add(consmMst.getReason());
    	}
    	
    	ResponseEntity<List<DepartmentMst>> getDepartment = RestCaller.getAlldepartment();

    	for(DepartmentMst departmentMst:getDepartment.getBody())
    	{
    		cmbDepartment.getItems().add(departmentMst.getDepartmentName());
    	}
    	
    	tbConsumptionDtl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					consumptionDtl = new ConsumptionDtl();
					consumptionDtl.setId(newSelection.getId());
					txtItemName.setText(newSelection.getItemName());
					txtBatch.setText(newSelection.getBatch());
					txtMrp.setText(Double.toString(newSelection.getMrp()));
					txtQty.setText(Double.toString(newSelection.getQty()));
				}
			}
		});
    }
    private void addItem()
    {
    	if(null == cmbConsumption.getSelectionModel())
    	{
    		notifyMessage(3,"Please Select Reason");
    		return;
    	}
    	if(null == consumptionHdr)
    	{
    		consumptionHdr = new ConsumptionHdr();
    		consumptionHdr.setBranchCode(SystemSetting.systemBranch);
    		consumptionHdr.setVoucherDate(SystemSetting.systemDate);
    		
    		if(null != cmbDepartment.getSelectionModel().getSelectedItem()){
    			String departmentDummy = cmbDepartment.getSelectionModel().getSelectedItem();
    			ResponseEntity<List<DepartmentMst>> getDepartment = RestCaller.getDepartmentByName(cmbDepartment.getSelectionModel().getSelectedItem());
    			 department = getDepartment.getBody();
    		}
    		consumptionHdr.setDepartmentId(department.get(0).getId());
    		ResponseEntity<ConsumptionHdr> respentity = RestCaller.saveConsumptionHdr(consumptionHdr);
    		consumptionHdr = respentity.getBody();
    	}
    	ArrayList items = new ArrayList();
		items = RestCaller.getSingleStockItemByName(txtItemName.getText(), txtBatch.getText(),txtStore.getText());
		Double chkQty = 0.0;
		String itemId = null;
		String store=null;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			List element = (List) itr.next();
			chkQty = (Double) element.get(4);
			itemId = (String) element.get(7);
			store =(String)  element.get(10);
		}
	
			
			if (chkQty < Double.parseDouble(txtQty.getText())) {
				notifyMessage(1, "Not in Stock!!!");
				clearFileds();
				txtItemName.requestFocus();
				return;
			}
			
			if(consumptionDtlList.size()>=1)
			{
				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
				Double prevQty = 0.0;
				prevQty = RestCaller.getConsumptionTotalQtyByItem(consumptionHdr.getId(),getItem.getBody().getId(),txtBatch.getText());
				if( chkQty<prevQty+Double.parseDouble(txtQty.getText()))
				{
					notifyMessage(1, "Not in Stock!!!");
					clearFileds();
					txtItemName.requestFocus();
					return;
				}
			}
    	consumptionDtl = new ConsumptionDtl();
    	ResponseEntity<ConsumptionReasonMst > getReason = RestCaller.getConsumptionReasonMstByReason(cmbConsumption.getSelectionModel().getSelectedItem());
    	consumptionDtl.setReasonId(getReason.getBody().getId());
    	
//    	consumptionDtl.setReasonId("Remarks");
    	ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	consumptionDtl.setItemId(getItem.getBody().getId());
    	// -----------sharon --------------adding store in damageDtl-------
		if(null!=txtStore.getText()) {
			consumptionDtl.setStore(txtStore.getText());
		System.out.println("store name------------------"+txtStore.getText());
		}
    	consumptionDtl.setBatch(txtBatch.getText());
    	consumptionDtl.setConsumptionHdr(consumptionHdr);
    	consumptionDtl.setMrp(Double.parseDouble(txtMrp.getText()));
    	consumptionDtl.setQty(Double.parseDouble(txtQty.getText()));
    	consumptionDtl.setAmount(consumptionDtl.getMrp() * consumptionDtl.getQty());
    	consumptionDtl.setUnitId(getItem.getBody().getUnitId());
    	consumptionDtl.setStore(storeName);
    	ResponseEntity<ConsumptionDtl> respentity = RestCaller.saveConsumptionDtl(consumptionDtl);
    	consumptionDtl = respentity.getBody();
    	
    	System.out.println("====consumptionDtl ****====" + consumptionDtl);
    	consumptionDtlList.add(consumptionDtl);
    	fillTable();
    	clearFileds();
    	txtItemName.requestFocus();
    	
    }
    @FXML
    void actionAdd(ActionEvent event) {

    	addItem();
    
    } 
    private void clearFileds()
    {
    	txtBatch.clear();
    	txtItemName.clear();
    	txtMrp.clear();
    	txtQty.clear();
    	cmbConsumption.getSelectionModel().clearSelection();
    	txtStore.clear();
    	
    }

    private void fillTable()
    {
    	for(ConsumptionDtl consDtl:consumptionDtlList)
    	{
    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(consDtl.getItemId());
    		consDtl.setItemName(getItem.getBody().getItemName());
    		ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(consDtl.getUnitId());
    		consDtl.setUnitName(getUnit.getBody().getUnitName());
    		ResponseEntity<ConsumptionReasonMst> getConsumptionReason = RestCaller.getConsumptionReasonMstById(consDtl.getReasonId());
    		consDtl.setReason(getConsumptionReason.getBody().getReason());
    	}
    	tbConsumptionDtl.setItems(consumptionDtlList);
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
    	clMrp.setCellValueFactory(cellData -> cellData.getValue().getmrpProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
    	clreason.setCellValueFactory(cellData -> cellData.getValue().getreasonProperty());
    	clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
    	clStore.setCellValueFactory(cellData -> cellData.getValue().getStoreNameProperty());
    	try
    	{
    	double totalAmount = RestCaller.getConsumptionTotalAmount(consumptionHdr.getId());
    	txtTotalAmount.setText(Double.toString(totalAmount));
    	}
    	catch (Exception e) {
		System.out.println(e.toString());
		}
    }
    @FXML
    void actionDelete(ActionEvent event) {
    	if(null == consumptionDtl)
    	{
    		return;
    	}
    	if(null == consumptionDtl.getId())
    	{
    		return;
    	}
    	RestCaller.deleteConsumptiondelete(consumptionDtl.getId());
    	ResponseEntity<List<ConsumptionDtl>> getConsumption = RestCaller.getConsumptionDtl(consumptionHdr.getId());
    	consumptionDtlList = FXCollections.observableArrayList(getConsumption.getBody());
    	fillTable();
    	clearFileds();

    }
    
    public void notifyMessage(int duration, String msg) {
				Image img = new Image("done.png");
				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();

				notificationBuilder.show();
			}
    @FXML
    void actionFinalsave(ActionEvent event) {
    	ResponseEntity<List<ConsumptionDtl>> getConsumption = RestCaller.getConsumptionDtl(consumptionHdr.getId());
        if(getConsumption.getBody().size()==0)
        {
        	return;
        }
        String vNo = RestCaller.getVoucherNumber("CONSUMPTION");
        consumptionHdr.setTotalAmount(Double.parseDouble(txtTotalAmount.getText()));
        consumptionHdr.setVoucherNumber(vNo);
        RestCaller.updateConsumptionHdr(consumptionHdr);
        notifyMessage(3,"Saved");
        consumptionHdr =null;
        consumptionDtl = null;
        consumptionDtlList.clear();
        tbConsumptionDtl.getItems().clear();
        txtTotalAmount.clear();
    }
    @FXML
    void loadItemPopUp(MouseEvent event) {
    	showPopup();
    }
    private void showPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			String cst = null;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

//			ItemStockPopupCtl itemStockPopupCtl =fxmlLoader.getController();
//			itemStockPopupCtl.getCustomer(cst);

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();
			// txtBarcode.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
		try {

			Stage stage = (Stage) txtItemName.getScene().getWindow();
			if (stage.isShowing()) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
				txtItemName.setText(itemPopupEvent.getItemName());
				txtMrp.setText(Double.toString(itemPopupEvent.getMrp()));
				txtBatch.setText(itemPopupEvent.getBatch());
				
				if(null != itemPopupEvent.getStoreName()) {
					storeName = itemPopupEvent.getStoreName();
				}else {
					storeName = "MAIN";
				}
				txtStore.setText(itemPopupEvent.getStoreName());
			
				}
			});
		}
		}
		catch (Exception e) {
		System.out.println(e);
		}
	}
	  @FXML
	    void qtyOnEnter(KeyEvent event) {
		  
			if (event.getCode() == KeyCode.ENTER) {
				cmbConsumption.requestFocus();
			}


	    }

	    @FXML
	    void reasonOnEnter(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				btnAdd.requestFocus();
			}
	    }
	  @FXML
	    void addOnKeypress(KeyEvent event) {
		  if (event.getCode() == KeyCode.ENTER) {
			  addItem();
		  }
	    }
	  @FXML
	    void itemNameOnEnter(KeyEvent event) {
		  if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		  }
	    }
	  @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	     private void PageReload() {
	     	
	}
}
