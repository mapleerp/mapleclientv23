package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.BatchWiseStockEvaluationReport;
import com.maple.report.entity.StockReport;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class BatchWiseStockEvaluationCtl {

	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PriceDefenitionMst> priceDefenitionMstList = FXCollections.observableArrayList();
	List<BatchWiseStockEvaluationReport>branchWiseStockEvaluationReportList= new ArrayList<BatchWiseStockEvaluationReport>();
	Double amount = 0.0;
	Double percentage = 0.0;
	
	ItemMst itemMst;
	 @FXML
	    private DatePicker dpFromDate;

	    @FXML
	    private Button btnGenerate;

	    @FXML
	    private TextField txtItem;

	    @FXML
	    private DatePicker dpTodate;

	    @FXML
	    private TextField txtBatch;

	    @FXML
	    void batchOnEnter(KeyEvent event) {
	    	StockBatchPopup();
	    }

	    @FXML
	    void itemOnClicked(MouseEvent event) {
	    	itemPopUp();
	    }

	    @FXML
	    void itemOnEnter(KeyEvent event) {
	    	itemPopUp();
	    }

    @FXML
    void GenerateReport(ActionEvent event) {
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select date", false);
			return;
    	}
    	if(null == dpTodate.getValue())
    	{
    		notifyMessage(5, "Please select date", false);
			return;
    	}
    	if(txtItem.getText().trim().isEmpty())
    	{
    		notifyMessage(5, "Please select Item", false);
    		return;
    	}
    	if(txtBatch.getText().trim().isEmpty())
    	{
    		notifyMessage(5, "Please select Batch", false);
    		return;
    	}
    	
    	ResponseEntity<ItemMst>item=RestCaller.getItemByNameRequestParam(txtItem.getText());
    	
    	ItemMst itemMst = item.getBody();
		if(null == itemMst)
		{
			notifyMessage(5, "item not found please check again", false);
    		return;
		}
		
    	String batch=	txtBatch.getText();
	
    	Date udate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    	String startdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
    	Date edate = SystemSetting.localToUtilDate(dpTodate.getValue());
    	String enddate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
    	
    	try {
			JasperPdfReportService.BatchWiseStockEvaluationReport(itemMst.getId(),batch,startdate,enddate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
    	
    }
    private void StockBatchPopup() {

		System.out.println("inside the popup");
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtBatch.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    @Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {

				txtBatch.setText(itemPopupEvent.getBatch());

			}
		});
	}
    @FXML
   	private void initialize() {
    	eventBus.register(this);
		
       }


	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
    
    	private void itemPopUp() {
		try {
			System.out.println("STOCK POPUP");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
		
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
	
			stage.show();


		} catch (Exception e) {
			e.printStackTrace();
		}
	
    }
    @Subscribe
	public void popupOrglistner(ItemPopupEvent itemPopupEvent) {

		Stage stage = (Stage) txtItem.getScene().getWindow();
		if (stage.isShowing()) {

			
			
			Platform.runLater(() -> {
				txtItem.setText(itemPopupEvent.getItemName());
            });

		}
	}
    
    @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			String taskid = taskWindowDataEvent.getId();
	 			String processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	     private void PageReload() {
	     	
	 
    
	     }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}


