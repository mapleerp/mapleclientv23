package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountPayable;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.OwnAccountSettlementDtl;
import com.maple.mapleclient.entity.OwnAccountSettlementMst;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.ReceiptInvoicePopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class OwnAccountSettlementCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
    private ObservableList<AccountReceivable> InvoiceList = FXCollections.observableArrayList();
    private ObservableList<AccountReceivable> InvoiceListselected = FXCollections.observableArrayList();
    private ObservableList<AccountReceivable> InvoiceList1 = FXCollections.observableArrayList();
    private ObservableList<OwnAccountSettlementDtl> ownAccSettledList = FXCollections.observableArrayList();
    private ObservableList<OwnAccount> ownAccList = FXCollections.observableArrayList();
    private ObservableList<OwnAccount> ownAccList1 = FXCollections.observableArrayList();
    AccountReceivable accountReceivable = null;
    AccountPayable accountPayable = null;
    OwnAccount ownAccount = null;
    OwnAccountSettlementMst ownAccountSettlementMst = null;
    OwnAccountSettlementDtl ownAccountSettlementDtl = null;
    @FXML
    private Button btnClear;
    @FXML
    private TextField txtAccount;

    @FXML
    private TextField txtAdjustdAmount;

    @FXML
    private TableView<AccountReceivable> tbInvoice;

    @FXML
    private TableColumn<AccountReceivable,String> clInvoiceNo;

    @FXML
    private TableColumn<AccountReceivable, Number> clDueAmount;

    @FXML
    private TableColumn<AccountReceivable, Number> clPaidAmount;

    @FXML
    private TableColumn<AccountReceivable, Number> clBalance;
    @FXML
    private TableView<OwnAccountSettlementDtl> tblSettled;

    @FXML
    private TableColumn<OwnAccountSettlementDtl, String> clSettledInvoiceNo;

    @FXML
    private TableColumn<OwnAccountSettlementDtl, Number> clsettledDue;

    @FXML
    private TableColumn<OwnAccountSettlementDtl, Number> clSetteledPaid;

    @FXML
    private TableColumn<OwnAccountSettlementDtl, Number> clSettledBalance;

    @FXML
    private TextField txtRunningBalance;
    @FXML
    private TableView<OwnAccount> tblCredit;

    @FXML
    private TableColumn<OwnAccount, Number> clDebitAmt;

    @FXML
    private TableColumn<OwnAccount, Number> clCreditAmt;
    
    @FXML
    private TableColumn<OwnAccount, Number> clRealizedAmt;

    @FXML
    private TableColumn<OwnAccount, Number> clOwnBalance;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnFetch;

    AccountHeads accountHeads = null;
//    Supplier supplierMst = null;
    @FXML
    void clearAll(ActionEvent event) {

    	txtAccount.clear();
    	txtAdjustdAmount.clear();
    	txtRunningBalance.clear();
    	tbInvoice.getItems().clear();
    	tblCredit.getItems().clear();
    	tblSettled.getItems().clear();
    	btnFetch.setDisable(false);
//    	InvoiceList.clear();
//    	InvoiceList1.clear();
//    	
//    	ownAccSettledList.clear();
//    	ownAccList.clear();
    	
    	
    }
    @FXML
   	private void initialize() {
       	eventBus.register(this);
       	
       	tblSettled.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					if(null ==ownAccountSettlementDtl)
					{
						ownAccountSettlementDtl = new OwnAccountSettlementDtl();
						ownAccountSettlementDtl.setId(newSelection.getId());
					}

				}
			}
		});
       	}
    @FXML
    void FetchInvoices(ActionEvent event) {
    	tbInvoice.getItems().clear();
    	if(null != accountHeads.getId())
    	{
    		FetchCustomerById();
    	}
//    	else if(null != supplierMst.getId())
//    	{
//    		FetchSupplierById();
//        	
//    	}
    	
    	fillOwnAccTable();
    	
    }
   		
    private void FetchCustomerById()
    {
		ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller.getOwnAccountDtlsByCustId(accountHeads.getId());
		ownAccList = FXCollections.observableArrayList(ownAccSaved.getBody());
    	tbInvoice.getItems().clear();
		ArrayList stockin = new ArrayList();
    	InvoiceList.clear();
    	stockin = RestCaller.getAccountreceivable(accountHeads.getId());
    	Iterator itr = stockin.iterator();
    	while (itr.hasNext()) {
    		 LinkedHashMap element = (LinkedHashMap) itr.next();
    		 Object id = (String) element.get("id");
    		 Object voucherNumber = (String) element.get("voucherNumber");
    		 Object accountId = (String)element.get("accountId");
    		 Object dueAmount=(Double)element.get("dueAmount");
    		 
    		 if(null==dueAmount) {
    			 dueAmount = 0.0;
    		 }
    		 
      		 Object paidAmount=(Double)element.get("paidAmount");
      		 
      		 if(null==paidAmount) {
      			paidAmount = 0.0;
    		 }
    		 if (null != id) {
    			AccountReceivable ack = new AccountReceivable();
    			ack.setId((String) id);
    			ack.setVoucherNumber((String)voucherNumber);
    			ack.setPaidAmount((Double)paidAmount);
    			ack.setDueAmount((Double)dueAmount);
    			 ack.setBalanceAmount((Double)dueAmount - (Double)paidAmount);
    			 //ack.setVoucherDate(SystemSetting.localToUtilDate(invDate));
    			 InvoiceList.add(ack);
    			 fillInvoiceTable();
    		 }
    	}
    }
    private void FetchSupplierById()
    {
    	ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller.getOwnAccountBySupAccId(accountHeads.getId());
		ownAccList = FXCollections.observableArrayList(ownAccSaved.getBody());
    	tbInvoice.getItems().clear();
    	double ownAccAmt = 0.0;
		ownAccAmt = RestCaller.getOwnAccountBySupId(accountHeads.getId());
	
		txtRunningBalance.setText(Double.toString(ownAccAmt));
		ArrayList stockin = new ArrayList();
		    		
//		tbInvoice.getItems().clear();
		
    	stockin = RestCaller.getAccountPayable(accountHeads.getId());
    	Iterator itr = stockin.iterator();
    	while (itr.hasNext()) {

    		 LinkedHashMap element = (LinkedHashMap) itr.next();
    		 Object id = (String) element.get("id");
    		 Object voucherNumber = (String) element.get("voucherNumber");
//    		 Object voucherDate = (String)element.get("voucherDate");
//    		 Object dueDate=(String)element.get("dueDate");
    		 Object dueAmount=(Double)element.get("dueAmount");
      		 Object paidAmount=(Double)element.get("paidAmount");
      		
    		 if (null != id) {
    			 AccountReceivable ack = new AccountReceivable();
    			 ack.setId((String) id);
     			ack.setVoucherNumber((String)voucherNumber);
     			ack.setPaidAmount((Double)paidAmount);
     			ack.setDueAmount((Double)dueAmount);
     			 ack.setBalanceAmount((Double)dueAmount - (Double)paidAmount);
     			 InvoiceList.add(ack);
     			 fillInvoiceTable();
    		 }
    	}
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    private void fillOwnAccTable()
    {
    	if(null != accountHeads.getId())
    	{
    	for(OwnAccount ownAcc : ownAccList)
    	{
    			ownAcc.setBalanceAmount(ownAcc.getDebitAmount()-ownAcc.getCreditAmount()-ownAcc.getRealizedAmount());
    		
    	}
    	double ownAccAmt = 0.0;
		ownAccAmt = RestCaller.getOwnAccountBySupId(accountHeads.getId());
	
		txtRunningBalance.setText(Double.toString(ownAccAmt));
    	}
    	else if(null != accountHeads.getId())
    	{
    		for(OwnAccount ownAcc : ownAccList)
        	{
    				
        			ownAcc.setBalanceAmount(ownAcc.getCreditAmount() - ownAcc.getDebitAmount()-ownAcc.getRealizedAmount());
        	}
    		double ownAccAmt = 0.0;
    		ownAccAmt = RestCaller.getOwnAccountByCustId(accountHeads.getId());
    		txtRunningBalance.setText(Double.toString(ownAccAmt));
    	}
    	tblCredit.setItems(ownAccList);
    	clCreditAmt.setCellValueFactory(cellData -> cellData.getValue().getcreditAmountProperty());
    	clDebitAmt.setCellValueFactory(cellData -> cellData.getValue().getDebitAmountProperty());
    	clRealizedAmt.setCellValueFactory(cellData -> cellData.getValue().getrealizedAmountProperty());
    	clOwnBalance.setCellValueFactory(cellData -> cellData.getValue().getbalanceAmountProperty());
		
    }

    private void fillInvoiceTable()
    {
    	tbInvoice.setItems(InvoiceList);
    	clInvoiceNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
    	clDueAmount.setCellValueFactory(cellData -> cellData.getValue().getdueAmountProperty());
    	clPaidAmount.setCellValueFactory(cellData -> cellData.getValue().getpaidAmountProperty());
    	clBalance.setCellValueFactory(cellData -> cellData.getValue().getbalanceAmountProperty());
    	
    }
    @FXML
    void loadPopUp(MouseEvent event) {
    	

    	showPopup();
    }
    @FXML
    void AddItems(ActionEvent event) {
    	ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}
			}
		}
		
		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);
		
		if(!dayendtodone)
		{
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}

    	ownAccountSettlementDtl = new OwnAccountSettlementDtl();
    	if(null != ownAccount.getId())
		{
			ResponseEntity<OwnAccount> respentity1 = RestCaller.getOwnAccountById(ownAccount.getId());
			ownAccount = respentity1.getBody();
		}
    	ownAccountSettlementDtl.setOwnAccount(ownAccount);
    	if(null != accountReceivable.getId())
    	{
    		ownAccountSettlementDtl.setAccountReceivable(accountReceivable);
    	ownAccountSettlementDtl.setDueAmt(accountReceivable.getDueAmount());    
    	ownAccountSettlementDtl.setInvoiceNumber(accountReceivable.getVoucherNumber());
    	ownAccountSettlementDtl.setPaidAmount(Double.parseDouble(txtAdjustdAmount.getText())+accountReceivable.getPaidAmount());
    	}
    	if(null != accountPayable.getId())
    	{
    	ownAccountSettlementDtl.setAccountPayable(accountPayable);
    	ownAccountSettlementDtl.setDueAmt(accountPayable.getDueAmount());
		ownAccountSettlementDtl.setInvoiceNumber(accountPayable.getVoucherNumber());
		ownAccountSettlementDtl.setPaidAmount(Double.parseDouble(txtAdjustdAmount.getText())+accountPayable.getPaidAmount());
    	}
    	ownAccountSettlementDtl.setNewPaidAmount(Double.parseDouble(txtAdjustdAmount.getText()));
    	
    	ResponseEntity<OwnAccountSettlementDtl> respentity = RestCaller.saveOwnAccountSettlementDtl(ownAccountSettlementDtl);
    	ownAccountSettlementDtl = respentity.getBody();
    	ownAccount.setRealizedAmount(ownAccount.getRealizedAmount() + Double.parseDouble(txtAdjustdAmount.getText()));
    	RestCaller.updateOwnAccount(ownAccount);
    
			ResponseEntity<AccountPayable> respentity2 = RestCaller.getAccountPayableByVoucherId(accountPayable.getVoucherNumber());
			if(null !=respentity2.getBody())
			{
				accountPayable = respentity2.getBody();
				accountPayable.setBalanceAmount(accountPayable.getDueAmount() -Double.parseDouble(txtAdjustdAmount.getText()));
				accountPayable.setPaidAmount(Double.parseDouble(txtAdjustdAmount.getText()) + accountPayable.getPaidAmount());
				RestCaller.updateAccountPayable(accountPayable);
				ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller.getOwnAccountBySupAccId(accountHeads.getId());
				ownAccList = FXCollections.observableArrayList(ownAccSaved.getBody());
			}
			else
			{
				ResponseEntity<AccountReceivable> respentity3 = RestCaller.getAccountRecByVoucherId(accountReceivable.getVoucherNumber());
				accountReceivable = respentity3.getBody();
				accountReceivable.setBalanceAmount(accountReceivable.getDueAmount() -Double.parseDouble(txtAdjustdAmount.getText()));
				accountReceivable.setPaidAmount(Double.parseDouble(txtAdjustdAmount.getText()) + accountReceivable.getPaidAmount());
				RestCaller.updateAccountReceivablebyId(accountReceivable);
				ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller.getOwnAccountDtlsByCustId(accountHeads.getId());
				ownAccList = FXCollections.observableArrayList(ownAccSaved.getBody());
			}
			
    	ownAccSettledList.add(ownAccountSettlementDtl);
    	fillSettlementTable();
    	txtAdjustdAmount.clear();
    	if(null != accountHeads.getId())
    	{
    		FetchSupplierById();
    	}
    	else if(null != accountHeads.getId())
    	{
    		
    		FetchCustomerById();
    	}
    	fillOwnAccTable();	
    }
    @FXML
    void tblInvoiceClick(MouseEvent event) {
    	if (tbInvoice.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tbInvoice.getSelectionModel();
			InvoiceListselected = selectionModel.getSelectedItems();
    	}

    }
    @FXML
    void ownAccTableClick(MouseEvent event) {

					if (tblCredit.getSelectionModel().getSelectedItem() != null) {
						TableViewSelectionModel selectionModel1 = tblCredit.getSelectionModel();
						ownAccList = selectionModel1.getSelectedItems();
						
						for (OwnAccount ock : ownAccList) {
						for(AccountReceivable ack : InvoiceListselected)
						{
							if(ack.getBalanceAmount() < ock.getBalanceAmount())
							{
							txtAdjustdAmount.setText(Double.toString(ack.getBalanceAmount()));
							}
							else if(ock.getBalanceAmount() < ack.getBalanceAmount())
							{
								txtAdjustdAmount.setText(Double.toString(ock.getBalanceAmount()));
							}
							accountReceivable = new AccountReceivable();
							accountPayable = new AccountPayable();
							ResponseEntity<AccountReceivable> respentity = RestCaller.getAccountRecById(ack.getId());
			    			if(null == respentity.getBody())
			    			{
			    				ResponseEntity<AccountPayable> respentity1 = RestCaller.getAccountPayableById(ack.getId());
			    				accountPayable = respentity1.getBody();
			    			}
			    			else
			    				accountReceivable = respentity.getBody();
							ownAccount = new OwnAccount();
							ownAccount.setId(ock.getId());
						}
						}
    	}
    }

    private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Accounts");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    @Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnFetch.getScene().getWindow();
		if (stage.isShowing()) {

			ownAccList.clear();
			accountHeads = new AccountHeads();
//			accountHeads = new Supplier();
//			customerRegistration.setCustomerName(txtAccount.getText());
			
			txtAccount.setText(accountEvent.getAccountName());
			try
			{
			ResponseEntity<AccountHeads> custsaved = RestCaller.getAccountHeadsByName(txtAccount.getText());
			if(null!= custsaved.getBody())
			{
				
				accountHeads = custsaved.getBody();
			}
			
			else
			{
				ResponseEntity<AccountHeads> supSaved = RestCaller.getAccountHeadsById(accountEvent.getAccountId());
			if(null != supSaved.getBody())
			{
				
				accountHeads = supSaved.getBody();
				
			}
			}

					
			
			}
			
			
			catch(Exception e)
			{
				return;
			}

		}

	
    
    }
private void fillSettlementTable()
{
	for(OwnAccountSettlementDtl owc : ownAccSettledList)
	{
		owc.setBalanceAmount(owc.getDueAmt()-owc.getPaidAmount());
	}
	tblSettled.setItems(ownAccSettledList);
	clSetteledPaid.setCellValueFactory(cellData -> cellData.getValue().getpaidAmountProperty());
	clSettledBalance.setCellValueFactory(cellData -> cellData.getValue().getbalanceAmountProperty());
	clSettledInvoiceNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
	clsettledDue.setCellValueFactory(cellData -> cellData.getValue().getdueAmountProperty());
	
}
@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


private void PageReload() {
	
}


}