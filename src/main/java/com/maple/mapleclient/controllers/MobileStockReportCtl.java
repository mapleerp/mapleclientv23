package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ConsumptionReport;
import com.maple.report.entity.StockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;

public class MobileStockReportCtl {
	
	String taskid;
	String processInstanceId;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<StockReport> reportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpDate;

    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtCategoryName;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnAdd;
    @FXML
    private TableView<StockReport> tbItemList;

    @FXML
    private TableColumn<StockReport, String> clCategoryName;

    @FXML
    private TableColumn<StockReport, String> clItemName;

    @FXML
    private TableColumn<StockReport, Number> clQty;

    @FXML
    private TableColumn<StockReport, Number> clStdPrice;

    @FXML
    private ListView<String> lsCategory;
    @FXML
  	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
  		eventBus.register(this);

      	
      }
    @FXML
    void actionAdd(ActionEvent event) {

    	lsCategory.getItems().add(txtCategoryName.getText());
    	lsCategory.getSelectionModel().select(txtCategoryName.getText());
    	txtCategoryName.clear();
    
    }

    @FXML
    void actionClear(ActionEvent event) {
    	txtCategoryName.clear();
    	txtItemName.clear();
    	lsCategory.getItems().clear();
    	dpDate.setValue(null);
    	tbItemList.getItems().clear();
    	reportList.clear();
    }
    @Subscribe
	public void popupOrglistner(CategoryEvent CategoryEvent) {

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			txtCategoryName.setText(CategoryEvent.getCategoryName());
		}
	}
    @FXML
    void categoryOnKeyPress(KeyEvent event) {


    	try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			btnAdd.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
    
    
    }
    @FXML
    void actionPrint(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		try {
			JasperPdfReportService.DailyMobileStockReport(startDate);
		} catch (JRException e) {
			e.printStackTrace();
		}
    }
    @FXML
    void loadItemPopUp(KeyEvent event) {


		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	
	
    }
    @Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {
		
		txtItemName.setText(itemPopupEvent.getItemName());
	}
    @FXML
    void actionShow(ActionEvent event) {

    	String itemName="";
    	if(!txtItemName.getText().trim().isEmpty())
    	{
    		itemName = txtItemName.getText().trim();
    	}
    	List<String> selectedItems = lsCategory.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		
    		s = s.concat(";");
    		cat= cat.concat(s);
    	}
    	cat = cat.replace("&", "AND");
    	java.util.Date uDate = Date.valueOf(dpDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
    	ResponseEntity<List<StockReport>> stockreportList = RestCaller.getDailyMobilestockReportByCategoryOrItemName(SystemSetting.getUser().getBranchCode(), startDate, itemName, cat);
    	reportList = FXCollections.observableArrayList(stockreportList.getBody());
    	fillTable();
    }

    private void fillTable()
    {
    	tbItemList.setItems(reportList);
    	clCategoryName.setCellValueFactory(cellData -> cellData.getValue().getcategoryProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clStdPrice.setCellValueFactory(cellData -> cellData.getValue().getstandardPriceProperty());

    }
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
