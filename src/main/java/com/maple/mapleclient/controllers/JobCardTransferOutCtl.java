package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JobCardDtl;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.entity.JobCardOutDtl;
import com.maple.mapleclient.entity.JobCardOutHdr;
import com.maple.mapleclient.entity.LocationMst;
import com.maple.mapleclient.events.JobCardEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class JobCardTransferOutCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<JobCardOutDtl> jobCardOutDtlList = FXCollections.observableArrayList();

	private ObservableList<JobCardDtl> jobCardDtlList = FXCollections.observableArrayList();

	EventBus eventBus = EventBusFactory.getEventBus();

	JobCardHdr jobCardHdr = null;
	
	JobCardOutHdr jobCardOutHdr = null;
	
	JobCardOutDtl jobCardOutDtl = null;

    @FXML
    private DatePicker dpDate;

    @FXML
    private TextField txtCustomer;

    @FXML
    private TextField txtJobCardNo;

    @FXML
    private ComboBox<String> cmbToBranch;

    @FXML
    private Button btnSave;

    @FXML
    private TableView<JobCardOutDtl> btlJObCardDtl;

    @FXML
    private TableColumn<JobCardOutDtl, String> clItemName;

    @FXML
    private TableColumn<JobCardOutDtl, Number> clQty;
    
    @FXML
    private TableColumn<JobCardOutDtl, String> clLocation;
    
    @FXML
    private Button btnCancel;
    
    @FXML
    private Button btnFinalSave;

    @FXML
    void Cancel(ActionEvent event) {
    	
    	if(null != jobCardOutHdr)
    	{
    		ResponseEntity<List<JobCardOutDtl>> jobCardOutSaved = RestCaller.getJobCardOutDtlByHdrId(jobCardOutHdr.getId());
			jobCardOutDtlList = FXCollections.observableArrayList(jobCardOutSaved.getBody());
    	
			for(JobCardOutDtl jobCardOut: jobCardOutDtlList)
			{
				RestCaller.deleteJobCardOutDtlById(jobCardOut.getId());
			}
			
			RestCaller.deleteJobCardOutHdrById(jobCardOutHdr.getId());

    	}
    	
    	btlJObCardDtl.getItems().clear();
    	jobCardOutHdr = null;
    	jobCardHdr = null;
    	jobCardOutDtl = null;
    	dpDate.setValue(null);
    	txtCustomer.clear();
    	txtJobCardNo.clear();
    	cmbToBranch.getSelectionModel().clearSelection();

    }

    @FXML
    void FinalSave(ActionEvent event) {
    	
    	if(null == jobCardHdr && null == jobCardOutHdr)
    	{
    		return;
    	}
    	
    	String financialYear = SystemSetting.getFinancialYear();
		String voucheNo = RestCaller.getVoucherNumber(financialYear + "JC");
		
		jobCardHdr.setVoucherNumber(voucheNo);
		jobCardHdr.setStatus("BRANCH TRANSFER");
		
		String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
		java.sql.Date date = java.sql.Date.valueOf(sdate);

		jobCardHdr.setVoucherDate(date);
		RestCaller.updateJobCardHdr(jobCardHdr);
		
		jobCardHdr = RestCaller.JobCardHdrById(jobCardHdr.getId()).getBody();
		
		jobCardOutHdr.setJobCardVoucherDate(jobCardHdr.getVoucherDate());
		jobCardOutHdr.setJobCardVoucherNumber(jobCardHdr.getVoucherNumber());
		
		String vNo = RestCaller.getVoucherNumber(financialYear + "JCT");
		jobCardOutHdr.setVoucherNumber(vNo);
		RestCaller.updateJobCardOutHdr(jobCardOutHdr);
		
		btlJObCardDtl.getItems().clear();
    	jobCardOutHdr = null;
    	jobCardHdr = null;
    	jobCardOutDtl = null;
    	dpDate.setValue(null);
    	txtCustomer.clear();
    	txtJobCardNo.clear();
    	cmbToBranch.getSelectionModel().clearSelection();

    }

    
    @FXML
	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	setBranches();
    }

    @FXML
    void FocusOnCustomer(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtCustomer.requestFocus();
		}

    }

    @FXML
    void JobCardPopup(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			jobCardPopup();
		}
    }
    

    @FXML
    void JobCardPopupOnClick(MouseEvent event) {
    	jobCardPopup();
    }

    @FXML
    void SAVE(ActionEvent event) {
    	saveJobCardTransferOut();
    }

   

	@FXML
    void SaveOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			saveJobCardTransferOut();
		}

    }

    @FXML
    void SaveOnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		if(!cmbToBranch.getSelectionModel().getSelectedItem().isEmpty())
    		{
    			saveJobCardTransferOut();
    		}
		}
    }
    
    private void saveJobCardTransferOut() {
		
    	if(null == dpDate.getValue())
    	{
    		notifyMessage(5,"Please Select Date...",false);
    		dpDate.requestFocus();
    		return;
    	}
    	
    	if(txtCustomer.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Select JobCard...",false);
    		txtCustomer.requestFocus();
    		return;
    	}
    	
    	if(txtJobCardNo.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Select JobCard...",false);
    		txtJobCardNo.requestFocus();
    		return;
    	}
    	
    	if(null == cmbToBranch.getValue())
    	{
    		notifyMessage(5,"Please Select ToBranch...",false);
    		cmbToBranch.requestFocus();
    		return;
    	}
    	
    	if(null == jobCardOutHdr)
    	{
    		jobCardOutHdr = new JobCardOutHdr();
    		
    		jobCardOutHdr.setVoucherDate(SystemSetting.localToUtilDate(dpDate.getValue()));
    		jobCardOutHdr.setCustomerId(jobCardHdr.getCustomerId());
    		jobCardOutHdr.setFromBranch(SystemSetting.systemBranch);
    		jobCardOutHdr.setServiceInDtl(jobCardHdr.getServiceInDtl());
    		jobCardOutHdr.setToBranch(cmbToBranch.getSelectionModel().getSelectedItem());
    	
    		ResponseEntity<JobCardOutHdr> jobCardOutHdrSaved = RestCaller.saveJobCardOutHdr(jobCardOutHdr);
    		jobCardOutHdr = jobCardOutHdrSaved.getBody();
    		
    		if(null == jobCardOutHdr)
    		{
    			return;
    		}
    	
    	}
    	
    	if(null != jobCardOutHdr.getId())
    	{
    		ResponseEntity<List<JobCardDtl>> jobCardDtl = RestCaller.getJobCardDtlByHdrId(jobCardHdr.getId());
			jobCardDtlList = FXCollections.observableArrayList(jobCardDtl.getBody());

			for(JobCardDtl jobCard : jobCardDtlList)
			{
				jobCardOutDtl = new JobCardOutDtl();
				jobCardOutDtl.setQty(jobCard.getQty());
				jobCardOutDtl.setJobCardOutHdr(jobCardOutHdr);
				jobCardOutDtl.setItemId(jobCard.getItemId());
				jobCardOutDtl.setLocationId(jobCard.getLocationId());
				ResponseEntity<JobCardOutDtl> jobCardOutSaved = RestCaller.saveJobCardOutDtl(jobCardOutDtl);
			}
			
			ResponseEntity<List<JobCardOutDtl>> jobCardOutSaved = RestCaller.getJobCardOutDtlByHdrId(jobCardOutHdr.getId());
			jobCardOutDtlList = FXCollections.observableArrayList(jobCardOutSaved.getBody());
			fillTable();
    	}
    	
	}
    
	private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			cmbToBranch.getItems().add(branchMst.getBranchCode());
		
			
		}
		 
	}
	
	private void jobCardPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/jobCardPopup.fxml"));
			Parent root1;

			String cst = null;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbToBranch.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Subscribe
	public void popupJobCardListner(JobCardEvent jobCardEvent) {

		try {
			Stage stage = (Stage) txtCustomer.getScene().getWindow();
			if (stage.isShowing()) {

				txtCustomer.setText(jobCardEvent.getCustomerName());

				ResponseEntity<JobCardHdr> jobCardHdrResp = RestCaller.JobCardHdrById(jobCardEvent.getJobCardId());
				jobCardHdr = jobCardHdrResp.getBody();
				
				txtJobCardNo.setText(jobCardHdr.getId());

				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	private void fillTable() {

		for (JobCardOutDtl jobCard : jobCardOutDtlList) {
			if (null != jobCard.getItemId()) {
				ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(jobCard.getItemId());
				ItemMst itemMst = itemResp.getBody();
				jobCard.setItemName(itemMst.getItemName());
			}

			if (null != jobCard.getLocationId()) {
				ResponseEntity<LocationMst> locationResp = RestCaller.findLocationMstById(jobCard.getLocationId());
				LocationMst locationMst = new LocationMst();

				locationMst = locationResp.getBody();
				if (null != locationMst) {
					jobCard.setLocation(locationMst.getLocation());
				}
			}
		}

		btlJObCardDtl.setItems(jobCardOutDtlList);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clLocation.setCellValueFactory(cellData -> cellData.getValue().getLocationProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }


}
