package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.ReceiptRowAddListener;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.RawMaterialReturnDtl;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.entity.ReceiptInvoiceDtl;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ReceiptInvoice;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRException;

public class ReceiptInvDtlReportCtl {
	
	String taskid;
	String processInstanceId;

	ReceiptInvoice receiptInvoice= null;
	ReceiptHdr receiptHdr = null;
	ReceiptInvoiceDtl receiptInvDtl = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ReceiptInvoice> ReceiptInvoiceTable = FXCollections.observableArrayList();
	private ObservableList<ReceiptInvoiceDtl> receiptInvoiceDtlList = FXCollections.observableArrayList();
	private ObservableList<OwnAccount> ownAccList = FXCollections.observableArrayList();
	String custId = "";
    @FXML
    private DatePicker dpFromDate;
    @FXML
    private Button btnPrint;
    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtAccount;

    @FXML
    private Button btnShow;

    @FXML
    private TableView<ReceiptInvoice> tbReceipts;

    @FXML
    private TableColumn<ReceiptInvoice, LocalDate> clDate;

    @FXML
    private TableColumn<ReceiptInvoice, String> clReceiptVoucher;

    @FXML
    private TableColumn<ReceiptInvoice,String> clAccount;
    @FXML
    private TableColumn<ReceiptInvoice, Number> clAmount;
    @FXML
    private TableView<ReceiptInvoiceDtl> tbInvoiceDtl;
    @FXML
    private TableColumn<ReceiptInvoiceDtl, String> clInvDtlInvNo;

    @FXML
    private TableColumn<ReceiptInvoiceDtl, Number> clInvDtlPaidAmt;

    @FXML
    void showData(ActionEvent event) {
    	String endDate = "";
		Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
		endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		if(!txtAccount.getText().isEmpty())
		{
    	ResponseEntity<List<ReceiptInvoice>> respentity  = RestCaller.getReceiptHdrByCustomerBetweenDate(custId,strtDate,endDate);
    	ReceiptInvoiceTable = FXCollections.observableArrayList(respentity.getBody());
		}
		else
		{
			ResponseEntity<List<ReceiptInvoice>> respentity  = RestCaller.getReceiptHdrBetweenDate(strtDate,endDate);
	    	ReceiptInvoiceTable = FXCollections.observableArrayList(respentity.getBody());
		}
    	fillTable();

    }
    @FXML
    void loadAccountsOnClick(MouseEvent event) {
    	loadCustomerPopup();

    }
    private void fillTable()
    {
    	tbReceipts.setItems(ReceiptInvoiceTable);
    	clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
    	clReceiptVoucher.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
    	clAccount.setCellValueFactory(cellData -> cellData.getValue().getacountNameProperty());
    }
    @FXML
	private void initialize() {
		eventBus.register(this);
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		tbReceipts.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					receiptHdr = new ReceiptHdr();
					receiptHdr.setId(newSelection.getId());
					ResponseEntity<List<ReceiptInvoiceDtl>> receiptInDtlSaved = RestCaller.getReceiptInvDtlById(receiptHdr);
					receiptInvoiceDtlList = FXCollections.observableArrayList(receiptInDtlSaved.getBody());
					fillInvoiceDetailTable();
					ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller.getOwnAccountDtlsByReceiptHdrId(receiptHdr.getId());
		    		ownAccList = FXCollections.observableArrayList(ownAccSaved.getBody());
		    		fillOwnAcc();
				}
			}
		});
	}
    @FXML
    void loadCustomer(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();

		}
    }
    private void fillOwnAcc()
    {
    	for (OwnAccount owc : ownAccList)
    	{
    		receiptInvDtl = new ReceiptInvoiceDtl();
    		receiptInvDtl.setInvoiceNumber("OWN ACCOUNT");
    		receiptInvDtl.setRecievedAmount(owc.getCreditAmount());
    		receiptInvoiceDtlList.add(receiptInvDtl);
    	}
    	fillInvoiceDetailTable();
    //	clInvDtlInvNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
    	//clInvDtlPaidAmt.setCellValueFactory(cellData -> cellData.getValue().getreceivedAmountProperty());
    }
    private void fillInvoiceDetailTable()
    {
    	tbInvoiceDtl.setItems(receiptInvoiceDtlList);
    	clInvDtlInvNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
    	clInvDtlPaidAmt.setCellValueFactory(cellData -> cellData.getValue().getreceivedAmountProperty());
    }
    private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			btnShow.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

    @FXML
    void PrintData(ActionEvent event) {
    	String endDate = "";
		Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
		endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		if(!txtAccount.getText().isEmpty())
		{
try {
			
			JasperPdfReportService.ReceiptReportByCustomerBetweenDate(custId,strtDate,endDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		}
		else
		{
			try {
						
						JasperPdfReportService.PaymentReportByBetweenDate(strtDate,endDate);
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					}
    }
	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnShow.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(customerEvent.getCustomerName());
			custId = customerEvent.getCustId();

		}

}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }
}
