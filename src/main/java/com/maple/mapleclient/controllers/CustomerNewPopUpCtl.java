package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.events.CustomerNewPopUpEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class CustomerNewPopUpCtl {

	
	  @FXML
	    private TableView<AccountHeads> tblCustomerNewPupup;

	    @FXML
	    private TableColumn<AccountHeads, String> clmnPartyName;

	    @FXML
	    private TableColumn<AccountHeads, String> clmnAddress;

	    @FXML
	    private TextField searchBox;

	    @FXML
	    private Button btnOk;

	    @FXML
	    private Button btnCancel;
	    
	    private EventBus eventBus = EventBusFactory.getEventBus();
	    public static String resultEventName="";
	    
	    StringProperty SearchString = new SimpleStringProperty();
	    
	    CustomerNewPopUpEvent customerNewPopUpEvent;
	    
	    private ObservableList<AccountHeads> accountHeads = FXCollections.observableArrayList();
	    
	    @FXML
		private void initialize() {
	    	searchBox.textProperty().bindBidirectional(SearchString);
	    	

			SearchString.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

					LoadCustomerNewPopupBySearch((String) newValue);
				}
			});
			
			
	    	customerNewPopUpEvent = new CustomerNewPopUpEvent();
	    	
	    	LoadCustomerNewPopupBySearch("");
	    	tblCustomerNewPupup.setItems(accountHeads);
	    	
	    	clmnPartyName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
	    	clmnAddress.setCellValueFactory(cellData -> cellData.getValue().getCustomerAddressProperty());
	    	
	    	tblCustomerNewPupup.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {

						if (null != newSelection.getAccountName()) {
							customerNewPopUpEvent.setCustomerName(newSelection.getAccountName());
						}
						if (null != newSelection.getPartyAddress1()) {
							customerNewPopUpEvent.setCustomerAddress(newSelection.getPartyAddress1());
						}
						if (null != newSelection.getPartyGst()) {
							customerNewPopUpEvent.setCustomerGst(newSelection.getPartyGst());
						}
						if (null != newSelection.getId()) {
							customerNewPopUpEvent.setCustomerId(newSelection.getId());
						}
						if (null != newSelection.getCreditPeriod()) {
							customerNewPopUpEvent.setCreditPeriod(newSelection.getCreditPeriod());
						}
//						if (null != newSelection.getUnitId()) {
//							itemPopupEvent.setUnitId(newSelection.getUnitId());
//						}
//						if (null != newSelection.getMrp()) {
//							itemPopupEvent.setMrp(newSelection.getMrp());
//						}
//						if (null != newSelection.getUnitname()) {
//							itemPopupEvent.setUnitName(newSelection.getUnitname());
//						}
//						System.out.println("itemPopupEventitemPopupEvent" + itemPopupEvent);
				}
			});
	    	
	    }

	    private void LoadCustomerNewPopupBySearch(String searchData) {
	    	/*
			 * This method populate the instance variable popUpItemList , which the source
			 * of data for the Table.
			 */
			ArrayList account = new ArrayList();
			/*
			 * Clear the Table before calling the Rest
			 */

			accountHeads.clear();

			account = RestCaller.SearchAccountByNameForNewPopUp(searchData);
			Iterator itr = account.iterator();
			System.out.println("accaccaccacc22");
			while (itr.hasNext()) {
				
				 LinkedHashMap element = (LinkedHashMap) itr.next();
				 System.out.println("accaccaccacc33"+element.get("id"));
				 Object accountName = (String) element.get("accountName");
				 Object address = (String) element.get("partyAddress1");
				 Object id = (String) element.get("id");
				 Object parentId = (String) element.get("parentId");
				 Object groupOnly = (String) element.get("groupOnly");
				 Object machineId = (String) element.get("machineId");
				 Object taxId = (String) element.get("taxId");
				 Object gst =(String) element.get("partyGst");
				
				if (null != id) {
					System.out.println("accaccaccacc44");
					AccountHeads acc = new AccountHeads();
					acc.setAccountName((String)accountName);
					acc.setId((String)id);
					acc.setGroupOnly((String)groupOnly);
					acc.setMachineId((String)machineId);
					acc.setParentId((String)parentId);
					acc.setTaxId((String)taxId);
					acc.setPartyAddress1((String)address);
					acc.setPartyGst((String)gst);
					System.out.println("accaccaccacc"+acc);
					accountHeads.add(acc);

				}
			}

			return;

			
		}

		@FXML
	    void ActionCancel(ActionEvent event) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
	    }

	    @FXML
	    void ActionOk(ActionEvent event) {
	    	Stage stage = (Stage) btnOk.getScene().getWindow();
	    	  
			eventBus.post(customerNewPopUpEvent); 
		
		
		resultEventName="";
		stage.close();
	    }

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tblCustomerNewPupup.requestFocus();
				tblCustomerNewPupup.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnOk.getScene().getWindow();
				stage.close();
			}

	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnOk.getScene().getWindow();
				
				
					eventBus.post(customerNewPopUpEvent); 
				
				
				resultEventName="";
				
				stage.close();
			}  else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

			} else {
				searchBox.requestFocus();
			}
	    }
}
