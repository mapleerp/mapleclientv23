package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.PurchaseSchemeMst;
import com.maple.mapleclient.entity.UrsMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class UserRegisteredStockCtl {
	
	String taskid;
	String processInstanceId;
	
	private ObservableList<UrsMst> userRegisteredStockList = FXCollections.observableArrayList();

	ArrayList<String > hdrids = new ArrayList<String>();
	EventBus eventBus = EventBusFactory.getEventBus();

	UrsMst ursMst =null;
    @FXML
    private TableView<UrsMst> tbUserStock;

    @FXML
    private TableColumn<UrsMst, String > clItemName;

    @FXML
    private TableColumn<UrsMst, Number> clCurrentStcok;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;
    @FXML
    private Button btnShow;
    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtStock;

    @FXML
    private Button btnClear;
    @FXML
	private void initialize() {
		eventBus.register(this);
		tbUserStock.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					ursMst = new UrsMst();
					ursMst.setId(newSelection.getId());
					txtItemName.setText(newSelection.getItemName());
					txtStock.setText(Double.toString(newSelection.getEnteredStock()));
				}
			}
		});
	}

    @FXML
    void actionClear(ActionEvent event) {
    	clearFields();
    }

    @FXML
    void actionDelete(ActionEvent event) 
    {
    	if(null == ursMst)
    	{
    		return;
    	}
    	if(null == ursMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteUrsMst(ursMst.getId());
    	clearFields();
    	showById();
    	//showByDate();
    }

    @FXML
    void actionSave(ActionEvent event) {
     addItem();	
    } 
    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

				Image img = new Image("done.png");
				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();
				notificationBuilder.show();
			}
    private void addItem()
    {
    	if(txtItemName.getText().trim().isEmpty())
    	{
    		notifyMessage(3,"Please Select Item");
    		txtItemName.requestFocus();
    		return;
    	}
    	if(txtStock.getText().trim().isEmpty())
    	{
    		notifyMessage(3,"Please Enter Stock");
    		txtStock.requestFocus();
    		return;
    	}
    	ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText()); 
    	
    	String vdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(),"yyyy-MM-dd");

    	ResponseEntity<UrsMst> getUrsMst = RestCaller.getUrsMstByDateAndItemId(getItem.getBody().getId(),vdate);
    	if(null == getUrsMst.getBody())
    	{
    	ursMst = new UrsMst();
    	}
    	else
    	{
    		ursMst = getUrsMst.getBody();
    	}
    	ursMst.setUserId(SystemSetting.getUser().getId());
    	ursMst.setEnteredStock(Double.parseDouble(txtStock.getText()));
    	ursMst.setItemId(getItem.getBody().getId());
    	ursMst.setBranchCode(SystemSetting.systemBranch);
    	ursMst.setTransDate(SystemSetting.getApplicationDate());
		ResponseEntity<List<StockReport>> itemStockReport=RestCaller.ItemStockReport(vdate,getItem.getBody().getId(),SystemSetting.systemBranch);

		if(itemStockReport.getBody().size()>0)
		{
			ursMst.setSystemStock(itemStockReport.getBody().get(0).getQty());
		}
		else
		{
			ursMst.setSystemStock(0.0);
		}
		if(null == getUrsMst.getBody())
    	{
		ResponseEntity<UrsMst> respentity =RestCaller.saveUrsMst(ursMst);
		ursMst= respentity.getBody();
		hdrids.add(ursMst.getId());
		userRegisteredStockList.add(ursMst);
		fillTable();
		ursMst = null;
		clearFields();
		fillTable();
    	}
		else
		{
			hdrids.add(ursMst.getId());
			RestCaller.updateUrsMst(ursMst);
			showById();
			clearFields();
			ursMst = null;
		}
		
		
    
		txtItemName.requestFocus();
    }
    @FXML
    void actionShow(ActionEvent event) {

    	showById();
    	//showByDate();
    }
    private void showById()
    {
    	userRegisteredStockList.clear();
//    	Date udate = SystemSetting.getApplicationDate();
//    	String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
//    	ResponseEntity<List<UrsMst>> getAllUrs = RestCaller.getUrsMstDate(sdate);
//    	userRegisteredStockList = FXCollections.observableArrayList(getAllUrs.getBody());
    	for(int i = 0;i<hdrids.size();i++)
    	{
    		ResponseEntity<UrsMst> ursMstById = RestCaller.getUrsMstById(hdrids.get(i));
    		if(null != ursMstById.getBody())
    		{
    			//userRegisteredStockList = FXCollections.observableArrayList(ursMstById.getBody());
    			ursMst = ursMstById.getBody();
    			userRegisteredStockList.add(ursMst);
    		}
    	}
    	
    	fillTable();
    }
    
    private void fillTable()
    {
    	for(UrsMst usrmst:userRegisteredStockList)
    	{
    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(usrmst.getItemId());
    		usrmst.setItemName(getItem.getBody().getItemName());
    	}
    	tbUserStock.setItems(userRegisteredStockList);
		clCurrentStcok.setCellValueFactory(cellData -> cellData.getValue().getenteredStockProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProprty());


    }
    private void clearFields()
    {
    	txtItemName.clear();
		txtStock.clear();
		
    }
    @FXML
    void itemNameOnEnter(KeyEvent event) {

    	loadItemPopUp();
    }
    @FXML
    void stockOnEnter(KeyEvent event) {

    	if(event.getCode()==KeyCode.ENTER)
    	{
    		btnSave.requestFocus();
    	}
    }
    @FXML
    void saveOnEnter(KeyEvent event) {

    	if(event.getCode() == KeyCode.ENTER)
    	{
    		addItem();
    	}
    }
	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnClear.getScene().getWindow();
		if (stage.isShowing()) {
			txtItemName.setText(itemPopupEvent.getItemName());
		}
	}
    private void loadItemPopUp()
    {

    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtStock.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
    }
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}
    
    

}
