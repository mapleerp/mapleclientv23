package com.maple.mapleclient.controllers;

import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.Iterator;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;

import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.ProductMst;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.events.ProductEvent;
import com.maple.mapleclient.events.SaleOrderHdrSearchEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class SaleOredreSearchPopupCtl {
	
	String taskid;
	String processInstanceId;


	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<SalesOrderTransHdr> saleOrderList = FXCollections.observableArrayList();
	SaleOrderHdrSearchEvent saleOrderHdrSearchEvent = null;

	@FXML
	private TextField txtProductName;

	@FXML
	private Button btnOk;

	@FXML
	private Button btnCancel;

	@FXML
	private TableView<SalesOrderTransHdr> tblSaleOrder;

	@FXML
	private TableColumn<SalesOrderTransHdr, String> clSaleOrderHdrId;

	@FXML
	private TableColumn<SalesOrderTransHdr, String> clCustomer;
	StringProperty SearchString = new SimpleStringProperty();

	@FXML
	private void initialize() {

		txtProductName.textProperty().bindBidirectional(SearchString);

		LoadSaleOrderPopupBySearch("");
		eventBus.register(this);
		saleOrderHdrSearchEvent = new SaleOrderHdrSearchEvent();
		tblSaleOrder.setItems(saleOrderList);
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clSaleOrderHdrId.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());

		tblSaleOrder.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId() && newSelection.getCustomerName().length() > 0) {
					saleOrderHdrSearchEvent = new SaleOrderHdrSearchEvent();
					saleOrderHdrSearchEvent.setId(newSelection.getId());
					saleOrderHdrSearchEvent.setVoucherNo(newSelection.getVoucherNumber());
					eventBus.post(saleOrderHdrSearchEvent);

				}
			}
		});

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadSaleOrderPopupBySearch((String) newValue);

			}
		});
	}

	@FXML
	void actionCancel(ActionEvent event) {
		Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
	}

	@FXML
	void actionOk(ActionEvent event) {
		if (null != saleOrderHdrSearchEvent.getId()) {
			eventBus.post(saleOrderHdrSearchEvent);
		} else {
//    		saleOrderHdrSearchEvent.setvProductName(txtProductName.getText());
//    		eventBus.post(productEvent);
		}
		Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
	}

	@FXML
	void onKeyPresstxt(KeyEvent event) {
		Stage stage = (Stage) btnOk.getScene().getWindow();
		if (event.getCode() == KeyCode.ENTER)

		{
			if (null != saleOrderHdrSearchEvent.getId()) {
				eventBus.post(saleOrderHdrSearchEvent);
				stage.close();
			} else {
//    		productEvent.setProductName(txtProductName.getText());
//    		eventBus.post(productEvent);
//    		stage.close();
			}
		}
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblSaleOrder.requestFocus();
			tblSaleOrder.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {

			stage.close();
		}
	}

	@FXML
	void tbOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			Stage stage = (Stage) btnOk.getScene().getWindow();

			if (null != saleOrderHdrSearchEvent.getId()) {
				eventBus.post(saleOrderHdrSearchEvent);
				stage.close();
			} else {
//        		productEvent.setProductName(txtProductName.getText());
//        		eventBus.post(productEvent);
//        		stage.close();
			}
		}
	}

	private void LoadSaleOrderPopupBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ArrayList saleOrders = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		saleOrderList.clear();

		saleOrders = RestCaller.SearchSaleOrederByVoucherNoCustomerName(searchData);
		String id;
		String voucherNo;
		String customerId;
		String localCustName;
		Iterator itr = saleOrders.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			voucherNo = (String) element.get(1);
			id = (String) element.get(0);
			customerId = (String) element.get(2);
			localCustName = (String) element.get(3);
			
			SalesOrderTransHdr salesOrderTransHdr = new SalesOrderTransHdr();
			salesOrderTransHdr.setId(id);
			salesOrderTransHdr.setVoucherNumber(voucherNo);
			
			salesOrderTransHdr.setCustomerName(localCustName);
			saleOrderList.add(salesOrderTransHdr);
			
			/*if (null != id) {
				SalesOrderTransHdr salesOrderTransHdr = new SalesOrderTransHdr();
				salesOrderTransHdr.setId(id);
				salesOrderTransHdr.setVoucherNumber(voucherNo);
				ResponseEntity<CustomerMst> custmer = RestCaller.getCustomerById(customerId);
				CustomerMst customerMst = custmer.getBody();
				if (null != customerMst) {
					salesOrderTransHdr.setCustomerId(customerMst);

				}
				ResponseEntity<LocalCustomerMst> custResp = RestCaller.getLocalCustomerbyId(localCustName);
				LocalCustomerMst localCustomerMst = custResp.getBody();
				if (null != localCustomerMst) {
					salesOrderTransHdr.setCustomerName(localCustomerMst.getLocalcustomerName());
				}

				saleOrderList.add(salesOrderTransHdr);
			}*/
			
		}
		return;
	}
	
	
	

	public void SaleOrderPopupBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ArrayList saleOrders = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		saleOrderList.clear();

		saleOrders = RestCaller.SearchSaleOrederByVoucherNoCustomerName(searchData);
		String id;
		String voucherNo;
		String customerId;
		String localCustName;
		Iterator itr = saleOrders.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			voucherNo = (String) element.get(1);
			id = (String) element.get(0);
			customerId = (String) element.get(2);
			localCustName = (String) element.get(4);
			
			SalesOrderTransHdr salesOrderTransHdr = new SalesOrderTransHdr();
			salesOrderTransHdr.setId(id);
			salesOrderTransHdr.setVoucherNumber(voucherNo);
			
			salesOrderTransHdr.setCustomerName(localCustName);
			saleOrderList.add(salesOrderTransHdr);
			
		}
		return;
	}
	
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   private void PageReload(String hdrId) {
	   	
	   }

}
