package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;



import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;

import com.maple.mapleclient.events.AccountHeadsPopupEvent;

import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class AccountHeadPopupCtl {
	
	String taskid;
	String processInstanceId;

	boolean initializedCalled = false;

	AccountHeadsPopupEvent accountHeadsPopupEvent;

	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<AccountHeads> accountHeadsList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();
	
	  @FXML
	    private TextField txtname;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TableView<AccountHeads> tablePopupView;

	    @FXML
	    private TableColumn<AccountHeads, String> partyName;

	    @FXML
	    private TableColumn<AccountHeads, String> partyAddress;

	    @FXML
	    private Button btnCancel;

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    
	    	

			if (event.getCode() == KeyCode.ENTER) {
				eventBus.post(accountHeadsPopupEvent);
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP
					|| event.getCode() == KeyCode.KP_UP) {

			} else {
				txtname.requestFocus();
			}

			initializedCalled = false;

	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	
	    	

			if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tablePopupView.requestFocus();
				tablePopupView.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {

				accountHeadsPopupEvent.setPartyName(null);
				eventBus.post(accountHeadsPopupEvent);
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			}
			if (event.getCode() == KeyCode.BACK_SPACE && txtname.getText().length() == 0) {
				accountHeadsPopupEvent.setPartyName(null);
				eventBus.post(accountHeadsPopupEvent);
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			}
			
			
			
			
	    }

	    @FXML
	    void onCancel(ActionEvent event) {

			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			accountHeadsPopupEvent.setPartyName(null);
			eventBus.post(accountHeadsPopupEvent);
			stage.close();
	    	
			

	    }

	    @FXML
	    void submit(ActionEvent event) {
	    	
	    	

	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			eventBus.post(accountHeadsPopupEvent);
			stage.close();


	    }
	    
	    @FXML
		private void initialize() {
	    	
	    	LoadAccountHeadsrBySearch("");

			txtname.textProperty().bindBidirectional(SearchString);

			accountHeadsPopupEvent = new AccountHeadsPopupEvent();
			eventBus.register(this);
			btnSubmit.setDefaultButton(true);
			tablePopupView.setItems(accountHeadsList);

			btnCancel.setCache(true);

			partyName.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
			partyAddress.setCellValueFactory(cellData -> cellData.getValue().getCustomerAddressProperty());
			

		
			tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getAccountName() && newSelection.getAccountName().length() > 0) {
						accountHeadsPopupEvent.setPartyAddress(newSelection.getPartyAddress1());
						accountHeadsPopupEvent.setPartyContact(newSelection.getCustomerContact());
						accountHeadsPopupEvent.setPartyGst(newSelection.getPartyGst());
						accountHeadsPopupEvent.setPartyMail(newSelection.getPartyMail());
						accountHeadsPopupEvent.setPartyName(newSelection.getAccountName());
					
						accountHeadsPopupEvent.setId(newSelection.getId());
						accountHeadsPopupEvent.setCreditPeriod(newSelection.getCreditPeriod());
						
						System.out.println(accountHeadsPopupEvent);
						eventBus.post(accountHeadsPopupEvent);
					}
				}
			});
	    
			SearchString.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

					LoadAccountHeadsrBySearch((String) newValue);
				}
			});
			
	    }
	    
	   
	    private void LoadAccountHeadsrBySearch(String searchData) {

			/*
			 * This method populate the instance variable popUpItemList , which the source
			 * of data for the Table.
			 */
	    	accountHeadsList.clear();
		    
			AccountHeads accountHeads =new AccountHeads();
			//List<AccountHeads> accountHeadsList =  new ArrayList<AccountHeads>();
			ArrayList accountHeadsList2 = new ArrayList();
			//RestTemplate restTemplate = new RestTemplate();

//			accountHeadsList2 = RestCaller.SearchAccountByTypeAndBoth(searchData,"SUPPLIER", "BOTH");
			accountHeadsList2 = RestCaller.SearchAccountByNameForNewPopUp(searchData);
//			customer = RestCaller.SearchCustomerByName();
			Iterator itr = accountHeadsList2.iterator();
			while (itr.hasNext()) {
				
			
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				// Iterator itr2 = lm.keySet().iterator();
				// while(itr2.hasNext()) {

				Object accountName = lm.get("accountName");
				Object partyAddress1 = lm.get("partyAddress1");
				Object customerContact = lm.get("customerContact");
				Object partyMail = lm.get("partyMail");
				Object partyGst = lm.get("partyGst");
				Object creditPeriod = lm.get("creditPeriod");
				Object id = lm.get("id");
				if (null != id) {
					accountHeads = new AccountHeads();
					accountHeads.setAccountName((String) accountName);
					accountHeads.setId((String)id);
					accountHeads.setPartyAddress1((String)partyAddress1);
					accountHeads.setPartyContact((String) customerContact);
				
					accountHeads.setPartyGst((String) partyGst);
					accountHeads.setPartyMail((String) partyMail);
					accountHeads.setCreditPeriod((Integer) creditPeriod);

				
					
					//-------ret branch
					

					System.out.println(lm);

					accountHeadsList.add(accountHeads);
				}

			}
			
			

			return;

		}	    
	    
	    
	    @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
