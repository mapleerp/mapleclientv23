package com.maple.mapleclient.controllers;

import java.awt.image.RescaleOp;
import java.sql.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.PurchaseReportExportToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PurchaseReportCtl {

	String taskid;
	String processInstanceId;

	PurchaseReportExportToExcel purchaseReportExportToExcel = new PurchaseReportExportToExcel();
	double totalamount = 0.0;
	String voucher;
	String vDate;
	Double grandTotal = 0.0;
	private ObservableList<PurchaseHdr> purchaseList = FXCollections.observableArrayList();
	private ObservableList<PurchaseDtl> purchaseDtlLists = FXCollections.observableArrayList();
	@FXML
	private DatePicker dpFromDate;
	@FXML
	private DatePicker dpToDate;

	PurchaseHdr purchaseHdr = null;

	@FXML
	private TableView<PurchaseHdr> tbReport;

	@FXML
	private TableColumn<PurchaseHdr, LocalDate> clVoucherNo;

	@FXML
	private TableColumn<PurchaseHdr, String> clSupplier;

	@FXML
	private TableColumn<PurchaseHdr, String> clamt;

	@FXML
	private TableColumn<PurchaseHdr, Number> Amnt;

	@FXML
	private TableColumn<PurchaseHdr, String> clSupInvNumbr;

	@FXML
	private TableColumn<PurchaseHdr, LocalDate> clSupInvDate;

	@FXML
	private TextField txtGrandTotal;
	@FXML
	private Button btnExcelExport;

	@FXML
	private Button btnShow;

	@FXML
	private TableView<PurchaseDtl> tbreports;

	@FXML
	private TableColumn<PurchaseDtl, String> clBranch;

	@FXML
	private TableColumn<PurchaseDtl, String> clItemName;
	
	  @FXML
	    private TableColumn<PurchaseDtl, String> clBatch;

	@FXML
	private TableColumn<PurchaseDtl, Number> clQty;

	@FXML
	private TableColumn<PurchaseDtl, Number> clRate;

	@FXML
	private TableColumn<PurchaseDtl, Number> clTaxRate;

	@FXML
	private TableColumn<PurchaseDtl, Number> clCessRate;

	@FXML
	private TableColumn<PurchaseDtl, String> clUnit;

	@FXML
	private TableColumn<PurchaseDtl, Number> clAmount;

	@FXML
	private ComboBox<String> branchselect;

	@FXML
	private Button ok;

	@FXML
	private Button btnPrint;

	@FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		setBranches();

		tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {
					System.out.println("getSelectionModel--getVoucherNumber");
					voucher = newSelection.getVoucherNumber();
					System.out.println(voucher);
					String voucherNumber = newSelection.getVoucherNumber();
					String purid = newSelection.getId();

					purchaseHdr = new PurchaseHdr();
					purchaseHdr.setId(purid);

					vDate = SystemSetting.UtilDateToString(newSelection.getourVoucherDate(), "yyyy-MM-dd");
					System.out.println(vDate);
					System.out.println("........" + voucherNumber);
					String branchName = branchselect.getValue();
					ResponseEntity<List<PurchaseDtl>> dailysaless = RestCaller.getPurchaseReportDtl(purid);
					purchaseDtlLists = FXCollections.observableArrayList(dailysaless.getBody());

					FillTables();

				}

				// FillTable();
			}
		});
	}

	@FXML
	void onClickOk(ActionEvent event) {

	}

	@FXML
	void reportPrint(ActionEvent event) {

		if (null == purchaseHdr) {
			return;
		}
		System.out.println(voucher);
		System.out.println(vDate);

		try {

			ResponseEntity<PurchaseHdr> purchaseHdrResp = RestCaller.getPurchaseHdr(purchaseHdr.getId());

			purchaseHdr = purchaseHdrResp.getBody();

			if (null == purchaseHdr) {
				return;
			}

			
			if (null != purchaseHdr.getCurrency() && !purchaseHdr.getCurrency().trim().isEmpty()) {

				if ((purchaseHdr.getCurrency()
						.equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName())
						|| purchaseHdr.getCurrency().equalsIgnoreCase(""))) {
					JasperPdfReportService.PharmmacyPurchaseInvoiceReport(purchaseHdr.getId());
				} else {

					JasperPdfReportService.importPurchaseInvoice(purchaseHdr.getId());

				}
			} else {

				JasperPdfReportService.PharmmacyPurchaseInvoiceReport(purchaseHdr.getId());

			}

//				 JasperPdfReportService.PharmmacyPurchaseInvoiceReport(purchaseHdr.getId());

//			JasperPdfReportService.importPurchaseInvoice(purchaseHdr.getId());
//			
//			JasperPdfReportService.PharmmacyPurchaseInvoiceReport(purchaseHdr.getId());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void show(ActionEvent event) {
		totalamount = 0.0;
		grandTotal = 0.0;

		if (null == branchselect.getValue()) {
			notifyMessage(5, "Please select branch", false);
			return;
		}

		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "Please select from date", false);
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(5, "Please select to date", false);
			return;
		}
		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		ResponseEntity<List<PurchaseHdr>> purchaseHdrRep = RestCaller.getAllPurchaseHdrByDate(startDate, endDate,
				branchselect.getValue());
		purchaseList = FXCollections.observableArrayList(purchaseHdrRep.getBody());

		for (PurchaseHdr purchase : purchaseList) {
			grandTotal = grandTotal + purchase.getInvoiceTotal();
		}

		FillTable();

	}

	private void setBranches() {

		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();

		for (BranchMst branchMst : branchMstList) {
			branchselect.getItems().add(branchMst.getBranchCode());

		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	private void FillTable() {
		tbReport.setItems(purchaseList);
		for (PurchaseHdr purchase : purchaseList) {
			if (null != purchase.getSupplierId()) {
				ResponseEntity<AccountHeads> sup = RestCaller.getAccountHeadsById(purchase.getSupplierId());
				purchase.setSupplierName(sup.getBody().getAccountName());
				clamt.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());

			}
		}

		clSupplier.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		Amnt.setCellValueFactory(cellData -> cellData.getValue().getInvoiceTotalProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clSupInvNumbr.setCellValueFactory(cellData -> cellData.getValue().getSupplierInvNoProperty());
		clSupInvDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
		BigDecimal newrate = new BigDecimal(grandTotal);
		newrate = newrate.setScale(2, BigDecimal.ROUND_CEILING);

		txtGrandTotal.setText(newrate.toString());

	}

	private void FillTables() {
		for (PurchaseDtl purchase : purchaseDtlLists) {
			ResponseEntity<UnitMst> unit = RestCaller.getunitMst(purchase.getUnitId());
			purchase.setUnitName(unit.getBody().getUnitName());
		}
		try {
			tbreports.setItems(purchaseDtlLists);
			java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			// clBranch.setCellValueFactory(cellData ->
			// cellData.getValue().getBarcodeProperty());
			clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
			clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
			clRate.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
			clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
			clCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());

			clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
			clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@FXML
	void exportToExcel(ActionEvent event) {

		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String strfromDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strToDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		ArrayList purchaseImport = new ArrayList();
		if (branchselect.getSelectionModel().isEmpty()) {
			return;
		}
		purchaseImport = RestCaller.getPurchasReportImport(branchselect.getSelectionModel().getSelectedItem(),
				strfromDate, strToDate);
		purchaseReportExportToExcel.exportToExcel("PurchaseToTally" + branchselect.getSelectionModel().getSelectedItem()
				+ strfromDate + strToDate + ".xls", purchaseImport);

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
