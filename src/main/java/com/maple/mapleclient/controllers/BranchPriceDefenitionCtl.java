package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchCategoryMst;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.BranchPriceDefenitionMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.SupplierPriceMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class BranchPriceDefenitionCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<BranchPriceDefenitionMst> branchPriceDefenitionMstTable = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();
	
	BranchPriceDefenitionMst branchPriceDefenitionMst;
	
    @FXML
    private TableView<BranchPriceDefenitionMst> tblBranchPrice;
   

    @FXML
    private TableColumn<BranchPriceDefenitionMst, String> clBranchCode;

    @FXML
    private TableColumn<BranchPriceDefenitionMst, String> clMRP;

    @FXML
    private TableColumn<BranchPriceDefenitionMst, String> clCostPrice;

    @FXML
    private TableColumn<BranchPriceDefenitionMst, String> id;

    @FXML
    private TextField txtMRP;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnDelete;

    @FXML
    private TextField txtCostPrice;

    @FXML
    private ComboBox<String> cmbBranchCode;

    @FXML
    void AddItem(ActionEvent event) {
    	save();
    }

    @FXML
    void DeleteItem(ActionEvent event) {

        if(null != branchPriceDefenitionMst)
        {
 	
 		if(null != branchPriceDefenitionMst.getId())
 		{
 			RestCaller.deleteBranchPriceDefenitionMst(branchPriceDefenitionMst.getId());
 			
 			showAll();
 			notifyMessage(5, " Item deleted", false);
				return;
 		}
        }
        
    }

    @FXML
    void ShowAllItems(ActionEvent event) {
    	showAll();
    }

    
    

	 
	    


	    

		public void showAll() {

			ResponseEntity<List<BranchPriceDefenitionMst>> respentity = RestCaller.getBranchPriceDefenition();
			branchPriceDefenitionMstTable = FXCollections.observableArrayList(respentity.getBody());
            
            System.out.print(branchPriceDefenitionMstTable.size()+"observable list size issssssssssssssssssssss");
            fillTable();
			
		}
    
    
    
		
		@FXML
		private void initialize() {
			
			
			eventBus.register(this);
			
			setCmbBranchCode();

			tblBranchPrice.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {

						branchPriceDefenitionMst = new BranchPriceDefenitionMst();
						branchPriceDefenitionMst.setId(newSelection.getId());
					}
				}
			});
		}
    
    
		private void setCmbBranchCode() {

			ResponseEntity<List<BranchMst>> branchListResp = RestCaller.getBranchMst();
			List<BranchMst> branchList = branchListResp.getBody();
			
			for(BranchMst branch
					 : branchList)
			{
				cmbBranchCode.setValue(branch.getBranchName());
			}
		}

		public void save() {

			BranchPriceDefenitionMst	branchPriceDefenitionMst=new BranchPriceDefenitionMst();
				if (null == cmbBranchCode.getValue()) {
					notifyMessage(5, "Please select Branch code", false);
					return;
				}
				
				if(txtMRP.getText().trim().isEmpty())
				{
					notifyMessage(5, "Please select MRP", false);
					return;
				}
				
				
		/*
		 * ////Doubt area ResponseEntity<BranchMst> getItem =
		 * RestCaller.getBranchMstByName(cmbBranchCode.getSelectionModel().
		 * getSelectedItem().toString()); BranchMst branchMst = getItem.getBody(); if
		 * (null == branchMst) { notifyMessage(5, "Item not found", false); return; }
		 */
				branchPriceDefenitionMst.setBranchCode(cmbBranchCode.getSelectionModel().getSelectedItem());
			
				branchPriceDefenitionMst.setMrp(txtMRP.getText());
				
				branchPriceDefenitionMst.setCostPrice(txtCostPrice.getText());
				
				
		

				ResponseEntity<BranchPriceDefenitionMst> respEntity = RestCaller.createBranchPriceDefenitionMst(branchPriceDefenitionMst);
				
				
				{
					notifyMessage(5, "Saved ", true);
					txtCostPrice.clear();
					txtMRP.clear();
				
					
					
					
					showAll();
					return;
				}
				
		
			}
		public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}

    
    @FXML
    void addOnEnter(KeyEvent event) {

    }

    @FXML
    void branchcodeOnEnter(InputMethodEvent event) {

    }

    @FXML
    void costPriceOnEnter(KeyEvent event) {

    }

    @FXML
    void mrpOnEnter(KeyEvent event) {

    }

    @FXML
    void saveOnKey(KeyEvent event) {

    	
    	
    }
    
    
    
    
    
    

	
	

private void fillTable() {
	
	  tblBranchPrice.setItems(branchPriceDefenitionMstTable);
	  
	
	  clBranchCode.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
	  clCostPrice.setCellValueFactory(cellData -> cellData.getValue().getCostPriceProperty());
	  clMRP.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());


	  
	  id.setCellValueFactory(cellData ->
	  cellData.getValue().getIdProperty());
	 }
@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


   private void PageReload() {
   	
}

}
