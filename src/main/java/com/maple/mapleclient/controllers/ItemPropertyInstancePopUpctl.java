package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.GoodReceiveNoteDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPropertyConfig;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.events.ItemPropertyInstance;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ItemPropertyInstancePopUpctl {
	String taskid;
	String processInstanceId;
	private ObservableList<ItemPropertyInstance> itemPropertyInstanceList = FXCollections.observableArrayList();
	private ObservableList<ItemPropertyConfig> ItemPropertyConfigList = FXCollections.observableArrayList();

	private EventBus eventBus = EventBusFactory.getEventBus();
	ItemPropertyInstance itemPropertyInstance = null;
	ItemPropertyConfig itempropertyConfig=null;
//	static String itemId =null;
	static PurchaseDtl purchaseDtl;
	static GoodReceiveNoteDtl goodReceiveNoteDtl;
    @FXML
    private TableView<ItemPropertyInstance> tblPropertyInstance;

    @FXML
    private TableColumn<ItemPropertyInstance,String> clProperty;

    @FXML
    private TableColumn<ItemPropertyInstance,String> clValue;

    @FXML
    private TableView<ItemPropertyConfig> tbPropertyConfig;

    @FXML
    private TableColumn<ItemPropertyConfig,String> clPropertyConfig;
    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtBatch;

    @FXML
    private TextField txtProperty;

    @FXML
    private TextField txtValue;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnClose;
    @FXML
 	private void initialize() {
 		// btnAdd.setVisible(false);
 		eventBus.register(this);
 		
 		tblPropertyInstance.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getId()) {
					itemPropertyInstance = new ItemPropertyInstance();
					itemPropertyInstance.setId(newSelection.getId());
					txtProperty.setText(newSelection.getPropertyName());
				}

			}

		});
 		
 		tbPropertyConfig.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {

				if (null != newSelection.getId()) {
					itempropertyConfig = new ItemPropertyConfig();
					txtProperty.setText(newSelection.getPropertyName());
				}

			}

		});
    }
    @FXML
    void actionClose(ActionEvent event) {
    	ResponseEntity<ItemMst> getItems = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	/*
    	 * Check if all property has set before closing the window
    	 */
    	
    	Boolean check = RestCaller.checkForAllPropertyInItemInstance(getItems.getBody().getId(),purchaseDtl.getId());
    	if(check)
    	{
    	Stage stage = (Stage) btnClose.getScene().getWindow();
		stage.close();
    	}
    	else
    	{
    		notifyMessage(3,"Please Save All Properties", true);
    	}
    }

    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == itemPropertyInstance)
    	{
    		return;
    	}
    	if(null == itemPropertyInstance.getId())
    	{
    		return;
    	}
    	RestCaller.delteItemPropertyInstance(itemPropertyInstance.getId());
    	ResponseEntity<List<ItemPropertyInstance>> itemPropertyInstance = RestCaller.getItemPropertyInstanceByPurchaseDtl(purchaseDtl.getId());
    	itemPropertyInstanceList = FXCollections.observableArrayList(itemPropertyInstance.getBody());
    	fillInstanceTable() ;
    }
    @FXML
    void actionSave(ActionEvent event) {
    	itemPropertyInstance = new ItemPropertyInstance();
    	itemPropertyInstance.setBatch(txtBatch.getText());
    	itemPropertyInstance.setPropertyName(txtProperty.getText());
    	itemPropertyInstance.setPropertyValue(txtValue.getText());
    	itemPropertyInstance.setPurhcaseDtl(purchaseDtl);
    	itemPropertyInstance.setBranchCode(SystemSetting.getUser().getBranchCode());
		ResponseEntity<ItemMst> getItems = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	itemPropertyInstance.setItemId(getItems.getBody().getId());
    	/*
    	 * Check if the property already saved
    	 */
    	ResponseEntity<ItemPropertyInstance> itemproperty = RestCaller.getitemPropertyInstanceByPurchaseDtlAndPropertyName(purchaseDtl.getId(),txtProperty.getText());
    	if(null == itemproperty.getBody())
    	{
    	ResponseEntity<ItemPropertyInstance> respentity = RestCaller.saveItemPropertyInstance(itemPropertyInstance);
    	itemPropertyInstance = respentity.getBody();
    	itemPropertyInstanceList.add(itemPropertyInstance);
    	fillInstanceTable();
    	}
    	else
    	{
    		notifyMessage(3,"Already Saved", true);
    	}
    	txtProperty.clear();
    	txtValue.clear();
    	itemPropertyInstance= null;
    }
	private void fillInstanceTable() {
		tblPropertyInstance.setItems(itemPropertyInstanceList);
		clProperty.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getPropertyValueProperty());

	}
	public void setItemAndBBatch(PurchaseDtl purchasedtl2) {
		purchaseDtl=purchasedtl2;
		txtItemName.setText(purchasedtl2.getItemName());
		txtBatch.setText(purchasedtl2.getBatch());
		ResponseEntity<List<ItemPropertyConfig>> itempropertylistList = RestCaller.getItemPropertyConfigByItemId(purchasedtl2.getItemId());
		ItemPropertyConfigList = FXCollections.observableArrayList(itempropertylistList.getBody());
		fillTable();
	}
	private void fillTable()
	{
		tbPropertyConfig.setItems(ItemPropertyConfigList);
		clPropertyConfig.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
	}
	
	public void setItemAndBBatch(GoodReceiveNoteDtl goodReceiveNoteDtl2) {
		goodReceiveNoteDtl=goodReceiveNoteDtl2;
		txtItemName.setText(goodReceiveNoteDtl2.getItemName());
		txtBatch.setText(goodReceiveNoteDtl2.getBatch());
		ResponseEntity<List<ItemPropertyConfig>> itempropertylistList = RestCaller.getItemPropertyConfigByItemId(goodReceiveNoteDtl2.getItemId());
		ItemPropertyConfigList = FXCollections.observableArrayList(itempropertylistList.getBody());
		fillTablegoodReceiveNoteDtl();
	}
	private void fillTablegoodReceiveNoteDtl()
	{
		tbPropertyConfig.setItems(ItemPropertyConfigList);
		clPropertyConfig.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
	}
	
	
	
	
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	public void getPreviousPropertyInstance(PurchaseDtl purchasedtl2,
			ResponseEntity<List<ItemPropertyInstance>> itemPropertyInstanceList2) {
		purchaseDtl =purchasedtl2;
		for(ItemPropertyInstance itempro :itemPropertyInstanceList2.getBody() )
		{
			itemPropertyInstance = new ItemPropertyInstance();
	    	itemPropertyInstance.setBatch(itempro.getBatch());
	    	itemPropertyInstance.setPropertyName(itempro.getPropertyName());
	    	itemPropertyInstance.setPropertyValue(itempro.getPropertyValue());
	    	itemPropertyInstance.setPurhcaseDtl(purchasedtl2);
	    	itemPropertyInstance.setBranchCode(SystemSetting.getUser().getBranchCode());
	    	itemPropertyInstance.setItemId(itempro.getId());
	    	{
	        	ResponseEntity<ItemPropertyInstance> respentity = RestCaller.saveItemPropertyInstance(itemPropertyInstance);
	        	itemPropertyInstance = respentity.getBody();
	        	itemPropertyInstanceList.add(itemPropertyInstance);
	        	
	    	}
		
		}
		fillInstanceTable();
		txtItemName.setText(purchasedtl2.getItemName());
		txtBatch.setText(purchasedtl2.getBatch());
		ResponseEntity<List<ItemPropertyConfig>> itempropertylistList = RestCaller.getItemPropertyConfigByItemId(purchasedtl2.getItemId());
		ItemPropertyConfigList = FXCollections.observableArrayList(itempropertylistList.getBody());
		fillTable();
	}
	
	public void getPreviousPropertyInstance(GoodReceiveNoteDtl goodReceiveNoteDtl2,
			ResponseEntity<List<ItemPropertyInstance>> itemPropertyInstanceList2) {
		goodReceiveNoteDtl =goodReceiveNoteDtl2;
		for(ItemPropertyInstance itempro :itemPropertyInstanceList2.getBody() )
		{
			itemPropertyInstance = new ItemPropertyInstance();
	    	itemPropertyInstance.setBatch(itempro.getBatch());
	    	itemPropertyInstance.setPropertyName(itempro.getPropertyName());
	    	itemPropertyInstance.setPropertyValue(itempro.getPropertyValue());
	    	itemPropertyInstance.setGoodReceiveNoteDtl(goodReceiveNoteDtl2);
	    	itemPropertyInstance.setBranchCode(SystemSetting.getUser().getBranchCode());
	    	itemPropertyInstance.setItemId(itempro.getId());
	    	{
	        	ResponseEntity<ItemPropertyInstance> respentity = RestCaller.saveItemPropertyInstance(itemPropertyInstance);
	        	itemPropertyInstance = respentity.getBody();
	        	itemPropertyInstanceList.add(itemPropertyInstance);
	        	
	    	}
		
		}
		fillInstanceTable();
		txtItemName.setText(goodReceiveNoteDtl2.getItemName());
		txtBatch.setText(goodReceiveNoteDtl2.getBatch());
		ResponseEntity<List<ItemPropertyConfig>> itempropertylistList = RestCaller.getItemPropertyConfigByItemId(goodReceiveNoteDtl2.getItemId());
		ItemPropertyConfigList = FXCollections.observableArrayList(itempropertylistList.getBody());
		fillTablegoodReceiveNoteDtl();
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
}
