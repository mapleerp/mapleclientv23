package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.entity.UserCreatedTask;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.UserPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
@RestController
public class UserCreatedTaskCtl {

	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<UserCreatedTask> userCreatedTaskTable = FXCollections.observableArrayList();

	UserCreatedTask userCreatedTask;

	@FXML
	private TextField txtUserName;

	@FXML
	private DatePicker dpTaskDueDate;

	@FXML
	private TextField txtRemark;

	@FXML
	private TextField txtStatus;

	@FXML
	private DatePicker dpTaskCreateDate;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnShowAll;

	@FXML
	private TableView<UserCreatedTask> tblUserCreatedTask;

	@FXML
	private TableColumn<UserCreatedTask, String> clUserID;

	@FXML
	private TableColumn<UserCreatedTask, String> clDueDate;

	@FXML
	private TableColumn<UserCreatedTask, String> clCreateDate;

	@FXML
	private TableColumn<UserCreatedTask, String> clStatus;
	
	
	@FXML
	private void initialize() {
		dpTaskCreateDate = SystemSetting.datePickerFormat(dpTaskCreateDate, "dd/MMM/yyyy");
		dpTaskDueDate = SystemSetting.datePickerFormat(dpTaskDueDate, "dd/MMM/yyyy");
		eventBus.register(this);
	}

	@FXML
	void AddItem(ActionEvent event) {
		save();
	}

	@FXML
	void ShowAllItems(ActionEvent event) {

	}

	@FXML
	void actionDateOnEnter(KeyEvent event) {

	}

	@FXML
	void addOnEnter(KeyEvent event) {

	}

	@FXML
	void commentOnEnter(KeyEvent event) {

	}

	@FXML
	void userPopup(MouseEvent event) {

		showUserPopup();

	}

	private void showUserPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/UserPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtRemark.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void saveOnKey(KeyEvent event) {

	}

	@FXML
	void taskIDOnEnter(KeyEvent event) {

	}

	public void save() {

		UserCreatedTask userCreatedTask = new UserCreatedTask();
		if (null == txtUserName.getText()) {
			notifyMessage(5, "Please select User name", false);
			return;
		}

		if (txtRemark.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select remark ", false);
			return;
		}

		ResponseEntity<UserMst> userMstResp = RestCaller.getUserByName(txtUserName.getText());
		UserMst userMst = userMstResp.getBody();

		if (null == userMst) {
			notifyMessage(5, "Invalid user name...!", false);
			return;
		}
		userCreatedTask.setUserId(userMst.getId());

		userCreatedTask.setRemark(txtRemark.getText());

		Date createDate = SystemSetting.localToUtilDate(dpTaskCreateDate.getValue());
		userCreatedTask.setCreateDate(createDate);
//		Date completeDate = SystemSetting.localToUtilDate(dpTaskCompleteDate.getValue());
//		userCreatedTask.setCompleteDate(completeDate);

		Date taskDueDate = SystemSetting.localToUtilDate(dpTaskDueDate.getValue());
		userCreatedTask.setTaskDueDate(taskDueDate);

		ResponseEntity<UserCreatedTask> userCreatedTaskSaved = RestCaller.createUserCreatedTask(userCreatedTask);

		userCreatedTaskTable.add(userCreatedTaskSaved.getBody());
		fillTable();

		notifyMessage(5, "Saved ", true);
		txtStatus.clear();
		txtRemark.clear();
		txtUserName.clear();
		dpTaskDueDate.setValue(null);
		dpTaskCreateDate.setValue(null);
		return;

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	private void fillTable() {

		tblUserCreatedTask.setItems(userCreatedTaskTable);

		clUserID.setCellValueFactory(cellData -> cellData.getValue().getUserIdProperty());
		clCreateDate.setCellValueFactory(cellData -> cellData.getValue().getCreateDateProperty());

		clDueDate.setCellValueFactory(cellData -> cellData.getValue().getTaskDueDateProperty());

	}

	@Subscribe
	public void userEventListener(UserPopupEvent userPopupEvent) {
		

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			
			txtUserName.setText(userPopupEvent.getUserName());
			
		}
	
	}
}
