package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.components.table.fill.FillTable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class ExceptionListReportCtl {
	
	String taskid;
	String processInstanceId;
	
	private ObservableList<ItemMst> exceptionList = FXCollections.observableArrayList();
	
	String itemId = null;


    @FXML
    private Button btnGenerateReport;

    @FXML
    private TableView<ItemMst> tblItemReport;

    @FXML
    private TableColumn<ItemMst, String> clItemName;
    
    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtCostPrice;

    @FXML
    private Button btnSaveCostPrice;
    
    @FXML
	private void initialize() {
    	
    	tblItemReport.getSelectionModel().selectedItemProperty()
		.addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					
					txtItemName.setText(newSelection.getItemName());
					itemId = newSelection.getId();
				}
			}
		});
    }

    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	generateReport();

    }
    
    private void generateReport() {
    	
    	tblItemReport.getItems().clear();

    	ResponseEntity<List<ItemMst>> itemMst = RestCaller.getAllExceptionItemReport();
    	exceptionList = FXCollections.observableArrayList(itemMst.getBody());
    	
    	FillTable();
	}

	@FXML
    void SaveCostPrice(ActionEvent event) {
    	
    	if(null == itemId)
    	{
    		notifyMessage(3, "please select item", false);
    		return;
    	}
    	if(null == txtCostPrice.getText())
    	{
    		notifyMessage(3, "please enter cost price", false);
    		return;
    	}
    	
    	ResponseEntity<ItemMst> itemMstSaved = RestCaller.getitemMst(itemId);
    	ItemMst itemMst = itemMstSaved.getBody();
    	
    	ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if (null != priceDefenitionMst) {
    	
    	PriceDefinition price = new PriceDefinition();
		price.setItemId(itemMst.getId());
		price.setUnitId(itemMst.getUnitId());
		price.setPriceId(priceDefenitionMst.getId());
		price.setAmount(Double.parseDouble(txtCostPrice.getText()));
		String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
		price.setStartDate(SystemSetting.StringToSqlDateSlash(sdate, "yyyy-MM-dd"));
		price.setBranchCode(SystemSetting.systemBranch);
		
		ResponseEntity<PriceDefinition> respentity = RestCaller.saveExceptionPriceDefenition(price);
		PriceDefinition priceDefinition = respentity.getBody();
		}
		
//		generateReport();
		
		txtCostPrice.clear();
		txtItemName.clear();
		itemId = null;

    }

	private void FillTable() {
		
		tblItemReport.setItems(exceptionList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

		
	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
