package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BatchPriceDefinition;

import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.InvoiceEditEnableMst;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.OrderTakerMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SaleOrderEdit;
import com.maple.mapleclient.entity.SaleOrderReceipt;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesOrderDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.SummarySalesOderDtl;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.SaleOrderHdrSearchEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
import javafx.scene.control.DatePicker;

public class SaleOrderEditCtl {
	
	String taskid;
	String processInstanceId;

	LocalCustomerMst localCustomerMst = null;

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");
	SalesOrderTransHdr salesOrderTransHdr = null;
	Double cardAmount = 0.0;
	private ObservableList<SaleOrderReceipt> salesReceiptsList = FXCollections.observableArrayList();
	SalesOrderDtl saleOrderDelete = null;
	private ObservableList<SalesOrderDtl> saleListTable = FXCollections.observableArrayList();

	// ----------------------version 6.4 surya
	private ObservableList<SaleOrderReceipt> salesReceiptsList2 = FXCollections.observableArrayList();

	// ----------------------version 6.4 surya end
	EventBus eventBus = EventBusFactory.getEventBus();
	SaleOrderReceipt saleOrderReceipt = new SaleOrderReceipt();
	List<SaleOrderReceipt> saleOrderReceiptTable = null;
	SalesOrderDtl salesOrderDtl = null;
	String salesReceiptVoucherNo = null;
	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();

	String strDate = null;
	String voucherDate = null;
	// --------------------new version 1.10 surya
	private ObservableList<PriceDefinition> priceDefenitionList = FXCollections.observableArrayList();
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();

	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty barcodeProperty = new SimpleStringProperty("");

	// --------------------new version 1.10 surya end

	@FXML
	private TextField txtLocalCustomerName;

	@FXML
	private TextField txtLocalCustomerAddress;

	@FXML
	private TextField txtLocalCustomerPhone;

	@FXML
	private TextField txtGstCustomer;

	@FXML
	private Button btnFetchData;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TableView<SalesOrderDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnQty;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnRate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesOrderDtl, String> columnExpiryDate;

	@FXML
	private TableColumn<SalesOrderDtl, Number> columnAmount;

	@FXML
	private ComboBox<String> cmbCardType;

	@FXML
	private TextField txtCrAmount;

	@FXML
	private Button AddCardAmount;

	@FXML
	private Button btnDeleteCardAmount;

	@FXML
	private TableView<SaleOrderReceipt> tblCardAmountDetails;

	@FXML
	private TableColumn<SaleOrderReceipt, String> clCardTye;

	@FXML
	private TableColumn<SaleOrderReceipt, Number> clCardAmount;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtCashPaidamount;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField txtCashtopay1;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnCardSale;

	@FXML
	private Label lblCardType;

	@FXML
	private Label lblCardAmount;

	@FXML
	private TextField txtSaleOrderHdrId;

	@FXML
	private TextField txtAdvanceAmount;

	// -------------------version 6.1 surya
	@FXML
	private DatePicker dpDueDate;

	@FXML
	private TextField txtDueTime;

	@FXML
	private ComboBox<String> cmbAmOrPm;
	// -----------------version 6.1 surya end

	// ----------------version 6.4 surya

	@FXML
	private TextField txtChangeAdvncAmount;

	@FXML
	private Label lblChngAdvncAmount;

	@FXML
	private Label lblTotalCardAmount;
	// ---------------version 6.4 surya end

	// ---------------new version 1.10 surya
	@FXML
	private ComboBox<String> cmbUnit;

	// ---------------new version 1.10 surya end

	// -------------------new version 2.0 surya

	@FXML
	private TextField txtCancelationReason;

	@FXML
	private Button btnCancelOrder;
	
	//------------------new version 2.1 surya 

    @FXML
    private TextField txtMessage;
    
	//------------------new version 2.1 surya end


	@FXML
	void CancelOrder(ActionEvent event) {

		if (null == salesOrderTransHdr.getId()) {
			notifyMessage(2, "Please select saleorder...! ", false);
			return;
		}

		ResponseEntity<SalesOrderTransHdr> salesOrderHdrResponse = RestCaller
				.getSalesOrderTransHdrByVoucherNo(salesOrderTransHdr.getVoucherNumber());

		salesOrderTransHdr = salesOrderHdrResponse.getBody();

		String vdate = SystemSetting.UtilDateToString(salesOrderTransHdr.getVoucherDate(), "yyyy-MM-dd");

		ResponseEntity<List<InvoiceEditEnableMst>> invoiceEditEnableMstResp = RestCaller.FindSaleOrderInvoiceEditByUser(
				salesOrderTransHdr.getVoucherNumber(), vdate, SystemSetting.getUser().getId());

		List<InvoiceEditEnableMst> invoiceEditEnableMstList = invoiceEditEnableMstResp.getBody();

		if (invoiceEditEnableMstList.size() == 0) {
			notifyMessage(2, "This Order is not allowed to cancel...!", false);
			return;
		}

		Alert a = new Alert(AlertType.CONFIRMATION);
		a.setHeaderText("Cancel Saleorder...");
		a.setContentText("The details will be deleted");
		a.showAndWait().ifPresent((btnType) -> {
			if (btnType == ButtonType.OK) {

				ResponseEntity<List<FinancialYearMst>> getFinancialYear = RestCaller.getAllFinancialYear();
				if (getFinancialYear.getBody().size() == 0) {
					notifyMessage(3, "Please Add Financial Year In the Configuration Menu", false);
					return;
				}
				int count = RestCaller.getFinancialYearCount();
				if (count == 0) {
					notifyMessage(3, "Please Add Financial Year In the Configuration Menu", false);
					return;
				}
				// version2.0ends
				ResponseEntity<ParamValueConfig> getParamValue = RestCaller
						.getParamValueConfig("DAY_END_LOCKED_IN_WHOLESALE");
				if (null != getParamValue.getBody()) {
					if (getParamValue.getBody().getValue().equalsIgnoreCase("YES")) {
						ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
						DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
						System.out.println("Sys Date before add" + SystemSetting.systemDate);
						if (null != dayEndClosureHdr) {
							if (null != dayEndClosureHdr.getDayEndStatus()) {
								String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(),
										"yyyy-MM-dd");
								String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
								java.sql.Date prDate = java.sql.Date.valueOf(process_date);
								Date sDate = java.sql.Date.valueOf(sysdate);
								int i = prDate.compareTo(sDate);
								if (i > 0 || i == 0) {
									notifyMessage(1, " Day End Already Done for this Date !!!!", false);
									return;
								}
							}
						}

					}
					Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

					if (!dayendtodone) {
						notifyMessage(1, "Day End should be done before changing the date !!!!", false);
						return;
					}
					System.out.println("Sys Date before add" + SystemSetting.systemDate);
				}

				if (txtSaleOrderHdrId.getText().trim().isEmpty()) {
					notifyMessage(3, "Please select order", false);
					return;
				}
				if (null == salesOrderTransHdr) {
					notifyMessage(3, "Please fetch order details", false);
					return;
				}

				if (txtCancelationReason.getText().trim().isEmpty()) {
					notifyMessage(2, "Please type cancelation reason", false);
					txtCancelationReason.requestFocus();
					return;
				}
				salesOrderTransHdr.setCancelationReason(txtCancelationReason.getText());

				salesOrderTransHdr.setOrderStatus("CANCELED");
				RestCaller.updateSalesOrderTranshdr(salesOrderTransHdr);

				txtcardAmount.clear();
				txtCashtopay1.clear();
				txtAdvanceAmount.clear();
				txtCashPaidamount.clear();
				txtChangeamount.clear();
				itemDetailTable.getItems().clear();
				txtcardAmount.clear();
				tblCardAmountDetails.getItems().clear();
				salesOrderTransHdr = null;
				salesOrderDtl = null;
				salesReceiptVoucherNo = null;
				cardAmount = 0.0;
				saleOrderReceipt = null;
				cmbCardType.setVisible(false);
				lblCardType.setVisible(false);
				lblCardAmount.setVisible(false);
				txtCrAmount.setVisible(false);
				AddCardAmount.setVisible(false);
				btnDeleteCardAmount.setVisible(false);
				tblCardAmountDetails.setVisible(false);
				txtcardAmount.setVisible(false);

				txtLocalCustomerName.clear();
				txtLocalCustomerAddress.clear();
				txtLocalCustomerPhone.clear();
				txtGstCustomer.clear();
				txtMessage.clear();

				txtSaleOrderHdrId.clear();
				cmbAmOrPm.getSelectionModel().clearSelection();
				txtDueTime.clear();
				dpDueDate.setValue(null);
				txtChangeAdvncAmount.setVisible(false);
				lblChngAdvncAmount.setVisible(false);

				txtCancelationReason.clear();

			} else if (btnType == ButtonType.CANCEL) {

				return;

			}
		});

	}

	// -----------------new version 2.0 surya

	@FXML
	void SaleOrderHdrIdPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			SaleOrderPopUp();
		}

	}

	@FXML
	void saleOrderOnOnClick(MouseEvent event) {
		SaleOrderPopUp();
	}

	private void SaleOrderPopUp() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderSearchPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			btnFetchData.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupSaleOrderListner(SaleOrderHdrSearchEvent saleOrderHdrSearchEvent) {

		Stage stage = (Stage) txtSaleOrderHdrId.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtcardAmount.clear();
					txtCashtopay1.clear();
					txtCashPaidamount.clear();
					txtChangeamount.clear();
					itemDetailTable.getItems().clear();
					txtcardAmount.clear();
					txtMessage.clear();

					tblCardAmountDetails.getItems().clear();
					salesOrderTransHdr = null;
					salesOrderDtl = null;
					salesReceiptVoucherNo = null;
					cardAmount = 0.0;
					saleOrderReceipt = null;

//					ResponseEntity<SalesOrderTransHdr> salesOrderHdrResponse = RestCaller
//							.getSalesOrderTransHdrByVoucherNo(saleOrderHdrSearchEvent.getVoucherNo());
//
//					salesOrderTransHdr = salesOrderHdrResponse.getBody();
//					
//					String vdate = SystemSetting.UtilDateToString(salesOrderTransHdr.getVoucherDate(), "yyyy-MM-dd");
//					
//					ResponseEntity<List<InvoiceEditEnableMst>>  invoiceEditEnableMstResp = RestCaller.
//							FindSaleOrderInvoiceEditByUser(salesOrderTransHdr.getVoucherNumber(),vdate, SystemSetting.getUser().getId());
//					
//					List<InvoiceEditEnableMst>  invoiceEditEnableMstList = invoiceEditEnableMstResp.getBody();
//					
//					if(invoiceEditEnableMstList.size() == 0)
//					{
//						notifyMessage(2, "This Order is not allowed to edit...!", false);
//						return;
//					}

					cmbCardType.setVisible(false);
					lblCardType.setVisible(false);
					lblCardAmount.setVisible(false);
					txtCrAmount.setVisible(false);
					AddCardAmount.setVisible(false);
					btnDeleteCardAmount.setVisible(false);
					tblCardAmountDetails.setVisible(false);
					txtcardAmount.setVisible(false);
					lblTotalCardAmount.setVisible(false);

					txtSaleOrderHdrId.setText(saleOrderHdrSearchEvent.getVoucherNo());
				}
			});
		}

	}

	@FXML
	void VisibleCardSale(ActionEvent event) {

		setCardSaleVisible();

	}

	private void setCardSaleVisible() {

		cmbCardType.setVisible(true);
		lblCardType.setVisible(true);
		lblCardAmount.setVisible(true);
		txtCrAmount.setVisible(true);
		tblCardAmountDetails.setVisible(true);
		txtcardAmount.setVisible(true);
		lblTotalCardAmount.setVisible(true);

	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();
		}
	}

	@FXML
	private void initialize() {
		dpDueDate = SystemSetting.datePickerFormat(dpDueDate, "dd/MMM/yyyy");
		eventBus.register(this);
		salesOrderDtl = new SalesOrderDtl();

		setCardType();

		txtCashPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtCashPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashPaidamount.setText(oldValue);
				}
			}
		});

		txtCrAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCrAmount.setText(oldValue);
				}
			}
		});

		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;

				Double advance = 0.0;
				if (!txtAdvanceAmount.getText().trim().isEmpty()) {
					advance = Double.parseDouble(txtAdvanceAmount.getText());
				}
				if ((txtCashPaidamount.getText().length() > 0)) {

					double paidAmount = Double.parseDouble(txtCashPaidamount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay1.getText());

					double cardAmt = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt + advance) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay1.getText());
					double cardAmount = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((cardAmount + advance) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				Double advance = 0.0;
				if (!txtAdvanceAmount.getText().trim().isEmpty()) {
					advance = Double.parseDouble(txtAdvanceAmount.getText());
				}
				if ((txtcardAmount.getText().length() > 0)) {

					double cardAmt = Double.parseDouble(txtcardAmount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay1.getText());

					double paidAmount = Double.parseDouble((String) newValue);

					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt + advance) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
						changeAmtProperty.set(newrate.toPlainString());
					}
				} else {

					double cashToPay = 0.0;
					double paidAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay1.getText());
					} catch (Exception e) {

					}
					try {
						paidAmt = Double.parseDouble((String) newValue);
					} catch (Exception e) {

					}

					BigDecimal newrate = new BigDecimal((paidAmt + advance) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

					if (newrate.doubleValue() < 0) {
						newrate = newrate.abs();

					}

					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesOrderDtl = new SalesOrderDtl();

					ResponseEntity<SalesOrderDtl> salesOrderDtlResp = RestCaller
							.getSalesOrderDtlById(newSelection.getId());
					salesOrderDtl = salesOrderDtlResp.getBody();
					
					if(null != salesOrderDtl.getOrderMsg()) {
						txtMessage.setText(salesOrderDtl.getOrderMsg());
					}

					ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(salesOrderDtl.getItemId());
					ItemMst itemMst = itemResp.getBody();

					txtItemname.setText(itemMst.getItemName());
					txtQty.setText(Double.toString(salesOrderDtl.getQty()));
					txtBarcode.setText(itemMst.getBarCode());
					txtRate.setText(Double.toString(salesOrderDtl.getMrp()));
					
					

					ResponseEntity<UnitMst> unitResp = RestCaller.getunitMst(salesOrderDtl.getUnitId());
					UnitMst unitMst = unitResp.getBody();

					cmbUnit.getSelectionModel().select(unitMst.getUnitName());

				}
			}
		});

		// -------------version 6.1 surya
		cmbAmOrPm.getItems().add("am");
		cmbAmOrPm.getItems().add("pm");

		// -------------version 6.1 surya end

		// ----------------new version 1.10 surya
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBarcode.textProperty().bindBidirectional(barcodeProperty);

		// ----------------new version 1.10 surya end

	}

	@FXML
	void DeleteTblItem(ActionEvent event) {

		if (null != salesOrderDtl) {
			if (null != salesOrderDtl.getId()) {
				RestCaller.deleteSalesOrderDtls(salesOrderDtl.getId());
				notifyMessage(5, "Item Deleted", true);
				itemDetailTable.getItems().clear();
				ResponseEntity<List<SalesOrderDtl>> saleorderDtls = RestCaller
						.getsaleOrderDtlByHdrId(salesOrderTransHdr.getId());
				saleListTable = FXCollections.observableArrayList(saleorderDtls.getBody());
				itemDetailTable.setItems(saleListTable);
				
				txtItemname.setText("");
				txtBarcode.setText("");
				txtQty.setText("");
				txtRate.setText("");
				txtMessage.clear();

				cmbUnit.getSelectionModel().clearSelection();
				txtBarcode.requestFocus();
				salesOrderDtl = new SalesOrderDtl();

			}
		}

	}

	@FXML
	void DeleteCardAmount(ActionEvent event) {
		if (null != saleOrderReceipt) {
			if (null != saleOrderReceipt.getId()) {
				RestCaller.deleteSalesOrderReceipts(saleOrderReceipt.getId());
				tblCardAmountDetails.getItems().clear();
				ResponseEntity<List<SaleOrderReceipt>> salesreceiptResp = RestCaller
						.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());
				salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

				filltableCardAmount();

			}
		}

	}

	@FXML
	void FetchData(ActionEvent event) {

		txtChangeAdvncAmount.setVisible(false);
		lblChngAdvncAmount.setVisible(false);

		if (!txtSaleOrderHdrId.getText().trim().isEmpty()) {

			txtLocalCustomerName.clear();
			txtLocalCustomerAddress.clear();
			txtLocalCustomerPhone.clear();
			txtGstCustomer.clear();
			tblCardAmountDetails.getItems().clear();
			txtCashPaidamount.clear();
			txtcardAmount.clear();
			salesReceiptsList.clear();
			txtCashPaidamount.clear();

			ResponseEntity<SalesOrderTransHdr> salesOrderHdrResponse = RestCaller
					.getSalesOrderTransHdrByVoucherNo(txtSaleOrderHdrId.getText());

			salesOrderTransHdr = salesOrderHdrResponse.getBody();

			txtLocalCustomerName.setText(salesOrderTransHdr.getLocalCustomerId().getLocalcustomerName());
			txtLocalCustomerAddress.setText(salesOrderTransHdr.getLocalCustomerId().getAddress());
			txtLocalCustomerPhone.setText(salesOrderTransHdr.getLocalCustomerId().getPhoneNo());

			txtGstCustomer.setText(salesOrderTransHdr.getAccountHeads().getAccountName());
			txtDueTime.setText(salesOrderTransHdr.getOrderDueTime());
			cmbAmOrPm.getSelectionModel().select(salesOrderTransHdr.getOrderTimeMode());
			dpDueDate.setValue(SystemSetting.utilToLocaDate(salesOrderTransHdr.getOrderDue()));

			if (null != salesOrderTransHdr) {

				ResponseEntity<List<SalesOrderDtl>> saleOrderList = RestCaller
						.getsaleOrderDtlByHdrId(salesOrderTransHdr.getId());

				saleListTable.clear();
				saleListTable = FXCollections.observableArrayList(saleOrderList.getBody());
				FillTable();

			}

			ResponseEntity<List<SaleOrderReceipt>> salesreceiptResp = RestCaller
					.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());
			salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

			List<SaleOrderReceipt> salesReceiptsTemp = salesreceiptResp.getBody();
			Double cashPayment = 0.0;
			Double cardPayment = 0.0;

			int salesReceiptsSize = salesReceiptsList.size();
			for (int i = 0; i < salesReceiptsTemp.size(); i++) {
				if (salesReceiptsTemp.get(i).getReceiptMode().equalsIgnoreCase("CASH")) {
					cashPayment = cashPayment + salesReceiptsTemp.get(i).getReceiptAmount();
					salesReceiptsList.remove(i);
				} else {
					cardPayment = cardPayment + salesReceiptsTemp.get(i).getReceiptAmount();
				}
			}

			txtAdvanceAmount.setText(cashPayment + "");
			txtcardAmount.setText(cardPayment + "");

			if (salesReceiptsList.size() > 0) {
				filltableCardAmount();
			}

			txtItemname.requestFocus();
		}

	}

	private void SaveSaleOrderEdit() {

		if (!txtSaleOrderHdrId.getText().trim().isEmpty()) {

			ResponseEntity<SalesOrderTransHdr> salesOrderHdrResponse = RestCaller
					.getSalesOrderTransHdrByVoucherNo(txtSaleOrderHdrId.getText());

			salesOrderTransHdr = salesOrderHdrResponse.getBody();

			if (null != salesOrderTransHdr) {

				ResponseEntity<List<SalesOrderDtl>> saleOrderList = RestCaller
						.getsaleOrderDtlByHdrId(salesOrderTransHdr.getId());

				saleListTable.clear();
				saleListTable = FXCollections.observableArrayList(saleOrderList.getBody());
				FillTable();

			}

		}
	}

	private void filltableCardAmount() {

		setCardSaleVisible();

		tblCardAmountDetails.setItems(salesReceiptsList);

		clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
		clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

	}

	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		columnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		Summary summary = null;

		if (null == salesOrderTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesOrderTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}
		if (salesOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			summary = RestCaller.getSalesWindowOrderSummaryDiscount(salesOrderTransHdr.getId());
		} else {
			summary = RestCaller.getSalesOrderSummary(salesOrderTransHdr.getId());
		}
		if (null != summary.getTotalAmount()) {
			salesOrderTransHdr.setInvoiceAmount(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtCashtopay1.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay1.setText("");
		}

	}

	@FXML
	void FocusOnCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCrAmount.requestFocus();
		}

	}

	@FXML
	void KeyPressAddCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			addCardAmount();
		}

	}

	@FXML
	void KeyPressDeleteCardAmount(KeyEvent event) {

	}

	@FXML
	void KeyPressFocusAddBtn(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if (txtCrAmount.getText().length() == 0 && null == cmbCardType.getValue()) {
				btnSave.requestFocus();
			} else {

				AddCardAmount.requestFocus();
			}
		}

	}

	@FXML
	void OnKeyPressFetchData(KeyEvent event) {

	}

	@FXML
	void addCardAmount(ActionEvent event) {
		addCardAmount();

	}

	private void addItem() {

		if (null == dpDueDate.getValue()) {
			notifyMessage(5, "Please select Due date", false);
			return;
		} else if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select item", false);
			return;
		} else if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Qty", false);
			return;
		} else if (txtDueTime.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter order time", false);
			return;
		} else if (txtLocalCustomerName.getText().trim().isEmpty() && txtGstCustomer.getText().trim().isEmpty()) {
			notifyMessage(5, " Please enter customer name", false);
			return;
		} else if (txtLocalCustomerName.getText().trim().isEmpty()) {

			notifyMessage(5, " Please select localcustomer", false);
			return;
		} else if (!txtLocalCustomerName.getText().contains(txtLocalCustomerPhone.getText())) {

			notifyMessage(5, "Please add customer 1st phone number to the end of customer name", false);
			return;
		} else {

			if (null == salesOrderTransHdr) {
				notifyMessage(5, "Please select sale order", false);
				return;
			}

			ResponseEntity<UnitMst> getUnitBYItem = RestCaller
					.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
			
			if(!txtBarcode.getText().trim().isEmpty())
			{
				if(!txtBarcode.getText().equalsIgnoreCase(getItem.getBody().getBarCode()))
				{
					notifyMessage(1, "Sorry... invalid item barcode!!!",false);
					txtBarcode.requestFocus();
					return;
				}
				
			}

			ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId());
			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				if (null == getmulti.getBody()) {
					notifyMessage(5, "Please Add the item in Multi Unit", false);
					return;
				}
			}

			String itemId = null;

			if (null == salesOrderTransHdr) {

				return;
			}

			if (null == salesOrderDtl) {
				salesOrderDtl = new SalesOrderDtl();
			}
			if (null != salesOrderDtl.getId()) {

				RestCaller.deleteSalesOrderDtls(salesOrderDtl.getId());

			}

			salesOrderDtl.setSalesOrderTransHdr(salesOrderTransHdr);
			salesOrderDtl.setItemName(txtItemname.getText());
			salesOrderDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());
			Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());

			salesOrderDtl.setQty(qty);

			ResponseEntity<List<SalesOrderDtl>> getSalesDtl = RestCaller
					.SalesOrderDtlByItemAndBatch(salesOrderTransHdr.getId(), getItem.getBody().getId());

			List<SalesOrderDtl> salesQtyCheck = getSalesDtl.getBody();

			if (salesQtyCheck.size() > 0) {

				for (SalesOrderDtl sales : salesQtyCheck) {
					qty = qty + sales.getQty();
				}

				salesOrderDtl.setQty(qty);
			}

			Double mrpRateIncludingTax = 00.0;
			mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
			salesOrderDtl.setMrp(mrpRateIncludingTax);

			Double taxRate = 00.0;

			ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesOrderDtl.getItemName()); // itemmst
																														// =
			ItemMst item = respsentity.getBody();

			salesOrderDtl.setItemCode(item.getItemCode());
			salesOrderDtl.setBarcode(item.getBarCode());
			salesOrderDtl.setStandardPrice(item.getStandardPrice());
			salesOrderDtl.setTaxRate(0.0);
			salesOrderDtl.setItemId(item.getId());

//			if (!txtMessage.getText().trim().isEmpty()) {
//				salesOrderDtl.setOrderMsg(txtMessage.getText());
//			}
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
			salesOrderDtl.setUnitId(getUnit.getBody().getId());
			salesOrderDtl.setUnitName(getUnit.getBody().getUnitName());

			String logdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
			
			if(!txtMessage.getText().trim().isEmpty())
			{
				salesOrderDtl.setOrderMsg(txtMessage.getText());
			}
			
			
			ResponseEntity<SalesOrderDtl> respentity = RestCaller.saveSalesOrderDtlWithLoginDate(salesOrderDtl,
					logdate);
			salesOrderDtl = respentity.getBody();

			ResponseEntity<List<SalesOrderDtl>> saleOrderList = RestCaller
					.getsaleOrderDtlByHdrId(salesOrderDtl.getSalesOrderTransHdr().getId());

			saleListTable.clear();
			saleListTable = FXCollections.observableArrayList(saleOrderList.getBody());
			FillTable();

			txtItemname.setText("");
			txtBarcode.setText("");
			txtQty.setText("");
			txtRate.setText("");
			cmbUnit.getSelectionModel().clearSelection();
			txtBarcode.requestFocus();
			txtMessage.clear();

			salesOrderDtl = new SalesOrderDtl();

		}

	}

	@FXML
	void addItemButtonClick(ActionEvent event) {

		addItem();

	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		showPopup();

	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {

		showPopup();
	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (txtBarcode.getText().length() <= 0) {
				txtCashPaidamount.requestFocus();
			}

		}

	}

	@FXML
	void OnKeyFinalSave(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			FinalSave();
		}

	}

	@FXML
	void FocusOnCardType(KeyEvent event) {

		if (event.getCode() == KeyCode.LEFT) {

			setCardSaleVisible();
			cmbCardType.requestFocus();
		}

	}

	@FXML
	void save(ActionEvent event) {

		FinalSave();

	}

	@FXML
	void setCardType(MouseEvent event) {
		setCardType();

	}

	private void setCardType() {
		cmbCardType.getItems().clear();

		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());

		for (ReceiptModeMst receiptModeMst : receiptModeList) {
			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
				cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
			}
		}
	}

//	private void getAllSalesOrder() {
//		
//		cmbVoucherNo.getItems().clear();
//
//		ArrayList sales = new ArrayList();
//		sales = RestCaller.getAllSalesOrder();
//		Iterator itr = sales.iterator();
//		while (itr.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr.next();
//			Object id = lm.get("id");
//			Object voucherNumber = lm.get("voucherNumber");
//			Object orderTimeMode = lm.get("orderTimeMode");
//			Object orderDueTime = lm.get("orderDueTime");
//			Object orderDueDate = lm.get("orderDue");
//			if (id != null) {
//				Integer Timeflag = 0;
//				 if(null != orderDueDate)
//				 {
//				Date udate = SystemSetting.localToUtilDate(LocalDate.now());
//
//				System.out.println(udate);
//				String dueDate = (String) orderDueDate;
//
//				Date dueDateUtil = SystemSetting.StringToUtilDate(dueDate, "yyyy-MM-dd");
//				Timeflag = udate.compareTo(dueDateUtil);
//				 }
//				if (Timeflag >= 0 ||SystemSetting.timeverificationbeforeorderedit.equalsIgnoreCase("FALSE")) {
//					cmbVoucherNo.getItems().add((String) voucherNumber);
//				}
//
//			}
//
//		}
//
//	}

//	private void showPopup() {
//		try {
//			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
//			// fxmlLoader.setController(itemStockPopupCtl);
//			Parent root1;
//
//			root1 = (Parent) fxmlLoader.load();
//			Stage stage = new Stage();
//
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.initStyle(StageStyle.UNDECORATED);
//			stage.setTitle("Stock Item");
//			stage.initModality(Modality.APPLICATION_MODAL);
//			stage.setScene(new Scene(root1));
//			stage.show();
//			txtQty.requestFocus();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//	}

	private void showPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		double applicableDiscount = 0.0;
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					cmbUnit.getItems().clear();
					itemNameProperty.set(itemPopupEvent.getItemName());
					cmbUnit.getItems().add(itemPopupEvent.getUnitName());
					cmbUnit.setValue(itemPopupEvent.getUnitName());
					// salesDtl.setItemName(itemNameProperty.get());
					/*
					 * itemNameProperty.set(itemPopupEvent.getItemName());
					 * 
					 * txtBarcode.setText(itemPopupEvent.getBarCode());
					 * batchProperty.set(itemPopupEvent.getBatch());
					 * 
					 * txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
					 * txtTax.setText(Double.toString(itemPopupEvent.getTaxRate()));
					 * 
					 */

//					batchProperty.set(itemPopupEvent.getBatch());
					salesOrderDtl = new SalesOrderDtl();
					salesOrderDtl.setAddCessRate(itemPopupEvent.getAddCessRate());
//					salesOrderDtl.setBatchCode(batchProperty.get());

					barcodeProperty.set(itemPopupEvent.getBarCode());

					salesOrderDtl.setBarcode(barcodeProperty.get());
					if (null != itemPopupEvent.getCess()) {
						salesOrderDtl.setCessRate(itemPopupEvent.getCess());
					} else {
						salesOrderDtl.setCessRate(0.0);
					}
					if (null != itemPopupEvent.getCgstTaxRate()) {
						salesOrderDtl.setCgstTaxRate(itemPopupEvent.getCgstTaxRate());
					} else {
						salesOrderDtl.setCgstTaxRate(0.0);
					}

					if (null != itemPopupEvent.getExpiryDate()) {
						salesOrderDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
					}

					salesOrderDtl.setItemCode(itemPopupEvent.getItemCode());

					salesOrderDtl.setItemTaxaxId(itemPopupEvent.getItemTaxaxId());

					mrpProperty.set(itemPopupEvent.getMrp() + "");
					salesOrderDtl.setMrp(Double.parseDouble(mrpProperty.get()));
					salesOrderDtl.setSgstTaxRate(itemPopupEvent.getSgstTaxRate());

//					taxRateProperty.set(itemPopupEvent.getTaxRate() + "");
//					salesOrderDtl.setTaxRate(Double.parseDouble(taxRateProperty.get()));
					salesOrderDtl.setUnitName(itemPopupEvent.getUnitName());

					System.out.println(itemPopupEvent.toString());
					if (txtGstCustomer.getText().trim().isEmpty()) {
						ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller
								.getMultiUnitByItemId(itemPopupEvent.getItemId());

						multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
						if (!multiUnitList.isEmpty()) {

							for (MultiUnitMst multiUniMst : multiUnitList) {

								ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
								cmbUnit.getItems().add(getUnit.getBody().getUnitName());
							}
							System.out.println(itemPopupEvent.toString());
						}
					}
					ResponseEntity<AccountHeads> getCustomer = RestCaller.getAccountHeadsByName(txtGstCustomer.getText());
					java.util.Date udate = SystemSetting.getApplicationDate();
					String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

					ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
							itemPopupEvent.getItemId(), getCustomer.getBody().getPriceTypeId(),
							itemPopupEvent.getUnitId(), itemPopupEvent.getBatch(), sdate);
					if (null != batchPriceDef.getBody()) {
						txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
					} else {
						ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
								itemPopupEvent.getItemId(), getCustomer.getBody().getPriceTypeId(),
								itemPopupEvent.getUnitId(), sdate);

						if (null != pricebyItem.getBody()) {
							txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));

						}
					}
//			ResponseEntity<List<BatchPriceDefinition>> batchPrice = RestCaller.getBatchPriceDefinitionByItemId(itemPopupEvent.getItemId(), getCustomer.getBody().getPriceTypeId(),txtBatch.getText());
//
//			BatchpriceDefenitionList = FXCollections.observableArrayList(batchPrice.getBody());
//			if(BatchpriceDefenitionList.size()>0)
//			{
//				cmbUnit.getItems().clear();
//				for (BatchPriceDefinition priceDef : BatchpriceDefenitionList) {
//					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
//					if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
//						cmbUnit.getItems().add(getUnit.getBody().getUnitName());
//						cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
//					}
//					else
//					{
//						cmbUnit.getItems().add(itemPopupEvent.getUnitName());
//						cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
//					}
//				}
//			}
//	
//			else
//			{
					ResponseEntity<List<PriceDefinition>> price = RestCaller
							.getPriceByItemId(itemPopupEvent.getItemId(), getCustomer.getBody().getPriceTypeId());
					priceDefenitionList = FXCollections.observableArrayList(price.getBody());

					if (null != price.getBody()) {
//				cmbUnit.getItems().clear();
						for (PriceDefinition priceDef : priceDefenitionList) {
							ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
							if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
								cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
							} else {
								cmbUnit.getItems().add(itemPopupEvent.getUnitName());
								cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
							}
						}
					}
//		}
//		if(getCustomer.getBody().getCustomerDiscount()>0.0)
//		{
//			double discount;
//			discount =Double.parseDouble(txtRate.getText())*(getCustomer.getBody().getCustomerDiscount()/100);
//			applicableDiscount = Double.parseDouble(txtRate.getText())-discount;
//			txtRate.setText(Double.toString(applicableDiscount));
//		}
				}
			});
		}
	}

	private void addCardAmount() {

		if (null != salesOrderTransHdr) {

			if (null == cmbCardType.getValue()) {

				notifyMessage(5, "Please select card type!!!!", false);
				cmbCardType.requestFocus();
				return;

			} else if (txtCrAmount.getText().trim().isEmpty()) {

				notifyMessage(5, "Please enter amount!!!!", false);
				txtCrAmount.requestFocus();
				return;

			} else {

				saleOrderReceipt = new SaleOrderReceipt();
				saleOrderReceipt.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());
				saleOrderReceipt.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
				saleOrderReceipt.setUserId(SystemSetting.getUser().getId());
				saleOrderReceipt.setBranchCode(SystemSetting.systemBranch);
				if (null == salesOrderTransHdr.getSaleOrderReceiptVoucherNumber()) {
					if (null == salesReceiptVoucherNo) {
						salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
						saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
					} else {
						saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
					}
				} else {
					salesReceiptVoucherNo = salesOrderTransHdr.getSaleOrderReceiptVoucherNumber();
					saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
				}
				// add acoount id
				ResponseEntity<AccountHeads> accountHeads = RestCaller
						.getAccountHeadByName(cmbCardType.getSelectionModel().getSelectedItem());
				saleOrderReceipt.setAccountId(accountHeads.getBody().getId());
				LocalDate date = LocalDate.now();
				java.util.Date udate = SystemSetting.localToUtilDate(date);
				saleOrderReceipt.setRereceiptDate(udate);
				saleOrderReceipt.setSalesOrderTransHdr(salesOrderTransHdr);
				System.out.println(saleOrderReceipt);
				ResponseEntity<SaleOrderReceipt> respEntity = RestCaller.saveSaleOrderReceipts(saleOrderReceipt);
				saleOrderReceipt = respEntity.getBody();
				salesReceiptsList.add(saleOrderReceipt);
				System.out.println("salesReceiptsList---" + salesReceiptsList.size());

				if (null != saleOrderReceipt) {
					if (null != saleOrderReceipt.getId()) {
						notifyMessage(5, "Successfully added cardAmount", true);

						tblCardAmountDetails.getItems().clear();
						txtCashPaidamount.clear();
						txtcardAmount.clear();
						ResponseEntity<List<SaleOrderReceipt>> salesreceiptResp = RestCaller
								.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());
//						salesReceiptsList2 = FXCollections.observableArrayList(salesreceiptResp.getBody());
						List<SaleOrderReceipt> salesReceiptsTemp = salesreceiptResp.getBody();
						Double cashPayment = 0.0;
						Double cardPayment = 0.0;
//						System.out.println(salesReceiptsList2.size());
//						int salesReceiptsSize = salesReceiptsList2.size();
						for (int i = 0; i < salesReceiptsTemp.size(); i++) {
							if (salesReceiptsTemp.get(i).getReceiptMode().equalsIgnoreCase("CASH")) {
								cashPayment = cashPayment + salesReceiptsTemp.get(i).getReceiptAmount();
//								salesReceiptsList2.remove(i);
							} else {
								cardPayment = cardPayment + salesReceiptsTemp.get(i).getReceiptAmount();
							}
						}
						txtAdvanceAmount.setText(cashPayment + "");
						txtcardAmount.setText(cardPayment + "");
//						//---------------------version 6.4 surya
//						if (salesReceiptsList.size() > 0) {
//							filltableCardAmount();
//						}
//						//---------------------version 6.4 surya end

					}
				}

				// ---------------------version 6.4 surya
				if (salesReceiptsList.size() > 0) {
					filltableCardAmount();
				}
				// ---------------------version 6.4 surya end

				txtCrAmount.clear();
				cmbCardType.getSelectionModel().clearSelection();
				cmbCardType.requestFocus();
				saleOrderReceipt = null;

			}
		} else {
			notifyMessage(5, "Please select Item!!!!", false);
			return;
		}
	}

	private void FinalSave() {

		if (null != salesOrderTransHdr) {

			Double cashToPay = 0.0;
			Double advanceCash = 0.0;
			Double advanceCard = 0.0;

			if (!txtCashtopay1.getText().trim().isEmpty()) {
				cashToPay = Double.parseDouble(txtCashtopay1.getText());
			}

			if (!txtAdvanceAmount.getText().trim().isEmpty()) {
				advanceCash = Double.parseDouble(txtAdvanceAmount.getText());
			}

			if (!txtcardAmount.getText().trim().isEmpty()) {
				advanceCard = Double.parseDouble(txtcardAmount.getText());

			}

			if (cashToPay < (advanceCash + advanceCard)) {
				notifyMessage(3, "Please check Amount...! total invoice amoubt must be greater than advance amount",
						false);
				return;
			}

			if (!txtLocalCustomerName.getText().trim().isEmpty()) {
				ResponseEntity<LocalCustomerMst> resplocal = RestCaller
						.getLocalCustomerbyName(txtLocalCustomerName.getText());
				if (null != resplocal.getBody()) {
					System.out.println(resplocal.getBody());
					salesOrderTransHdr.setLocalCustomerId(resplocal.getBody());
//					custId = resplocal.getBody().getId();
				}
			}
			if (txtGstCustomer.getText().trim().isEmpty()) {
				ResponseEntity<AccountHeads> resplocalgstcust = RestCaller.getAccountHeadsByNameAndBranchCode("POS",
						SystemSetting.systemBranch);
				salesOrderTransHdr.setAccountHeads(resplocalgstcust.getBody());
			} else {

				ResponseEntity<AccountHeads> resplocalgstcust = RestCaller
						.getAccountHeadsByNameAndBranchCode(txtGstCustomer.getText(), SystemSetting.systemBranch);

				AccountHeads customerMst = resplocalgstcust.getBody();
				if (null == customerMst) {
					ResponseEntity<AccountHeads> customerMstResp = RestCaller
							.getAccountHeadsByName(txtGstCustomer.getText());
					customerMst = customerMstResp.getBody();
				}
				salesOrderTransHdr.setAccountHeads(customerMst);
			}

			Double cashtoPay = Double.parseDouble(txtCashtopay1.getText());
			salesOrderTransHdr.setInvoiceAmount(cashtoPay);
			salesOrderTransHdr.setOrderDue(SystemSetting.localToUtilDate(dpDueDate.getValue()));
			salesOrderTransHdr.setOrderDueTime(txtDueTime.getText());
			salesOrderTransHdr.setOrderTimeMode(cmbAmOrPm.getSelectionModel().getSelectedItem().toString());

//			if (!txtCashPaidamount.getText().trim().isEmpty()) {
//				cashPaidAmount = Double.parseDouble(txtCashPaidamount.getText());
//				if (!txtcardAmount.getText().trim().isEmpty()) {
//					Double cardSale = 0.0;
//					Double totalSale = 0.0;
//					cardSale = Double.parseDouble(txtcardAmount.getText());
//					totalSale = cashPaidAmount + cardSale;
//					if (Double.parseDouble(txtCashtopay.getText()) < totalSale) {
//						cashPaidAmount = Double.parseDouble(txtCashtopay.getText()) - cardSale;
//					}
//				} else if (cashPaidAmount > Double.parseDouble(txtCashtopay.getText())) {
//					cashPaidAmount = Double.parseDouble(txtCashtopay.getText());
//
//				}
//				salesOrderTransHdr.setCashPay(cashPaidAmount);
//
//				saleOrderReceipt = new SaleOrderReceipt();
//
//				if (null == salesReceiptVoucherNo) {
//					salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
//					saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
//				} else {
//					saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
//				}
//
//				saleOrderReceipt.setReceiptMode("CASH");
//				saleOrderReceipt.setReceiptAmount(cashPaidAmount);
//				saleOrderReceipt.setUserId(SystemSetting.getUser().getId());
//				saleOrderReceipt.setBranchCode(SystemSetting.systemBranch);
//				// set accout id
//
//				String account = SystemSetting.systemBranch + "-CASH";
//				ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(account);
//				saleOrderReceipt.setAccountId(accountHeads.getBody().getId());
////	    		
////	    		LocalDate date = LocalDate.now();
////	    		java.util.Date udate = SystemSetting.localToUtilDate(date);
//				saleOrderReceipt.setRereceiptDate(SystemSetting.getApplicationDate());
//				saleOrderReceipt.setSalesOrderTransHdr(salesOrderTransHdr);
//				System.out.println(saleOrderReceipt);
//				ResponseEntity<SaleOrderReceipt> respEntity = RestCaller.saveSaleOrderReceipts(saleOrderReceipt);
//				saleOrderReceipt = respEntity.getBody();
//			}

			salesOrderTransHdr.setOrderStatus("Orderd");

			eventBus.post(salesOrderTransHdr);

			if (txtGstCustomer.getText().trim().isEmpty() || !txtLocalCustomerName.getText().trim().isEmpty()) {
				if (txtLocalCustomerName.getText().trim().isEmpty()) {
					notifyMessage(5, " Please enter customer name", false);
					return;
				} else if (txtLocalCustomerName.getText().trim().isEmpty()) {

					notifyMessage(5, " Please enter customer address", false);
					return;
				} else if (txtLocalCustomerAddress.getText().trim().isEmpty()) {

					notifyMessage(5, " Please enter customer phone no.", false);
					return;
				}

				if (txtLocalCustomerPhone.getText().trim().length() < 10) {
					notifyMessage(5, " Invalid Phone no.", false);
					return;
				} else if (!txtLocalCustomerName.getText().contains(txtLocalCustomerPhone.getText())) {

					notifyMessage(5, "Please add customer 1st phone number to the end of customer name", false);
					return;
				} else {
					localCustomerMst = new LocalCustomerMst();
					localCustomerMst.setId(salesOrderTransHdr.getLocalCustomerId().getId());
					localCustomerMst.setLocalcustomerName(txtLocalCustomerName.getText());
					localCustomerMst.setAddress(txtLocalCustomerAddress.getText());
					localCustomerMst.setPhoneNo(txtLocalCustomerPhone.getText());
//					localCustomerMst.setPhoneNO2(txtCustPhoneNo2.getText());
					RestCaller.updateLocalCustomer(localCustomerMst);

				}
			}
			ResponseEntity<List<SalesOrderDtl>> saledtlSaved = RestCaller.getSalesOrderDtl(salesOrderTransHdr);
			if (saledtlSaved.getBody().size() == 0) {
				return;
			}

			salesOrderTransHdr.setSalesMode("SALEORDER");
			salesOrderTransHdr.setOrderStatus("Orderd");
//			salesOrderTransHdr.setVoucherNumber(vNo);
			if (!txtCashtopay1.getText().trim().isEmpty()) {
				cashtoPay = Double.parseDouble(txtCashtopay1.getText());
				salesOrderTransHdr.setInvoiceAmount(cashtoPay);

			}

			System.out.println("Voucher Date " + salesOrderTransHdr.getVoucherDate());
			System.out.println("Voucher Date " + SystemSetting.systemDate);

			RestCaller.updateSalesOrderTranshdr(salesOrderTransHdr);
			notifyMessage(5, " Order Saved", true);

			ResponseEntity<SalesOrderTransHdr> salesOrderTransHdrSaves = RestCaller
					.getSaleOrderTransHdrById(salesOrderTransHdr.getId());
			salesOrderTransHdr = salesOrderTransHdrSaves.getBody();

			System.out.println("Voucher Date" + salesOrderTransHdr.getVoucherDate());

//			ResponseEntity<List<SaleOrderReceipt>> saleOrderReceiptList = RestCaller
//					.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());
//
//			List<SaleOrderReceipt> saleOrderReceipts = saleOrderReceiptList.getBody();
//			for (SaleOrderReceipt receipts : saleOrderReceipts) {
//				DayBook dayBook = new DayBook();
//				dayBook.setBranchCode(SystemSetting.systemBranch);
//				ResponseEntity<AccountHeads> accountHead1 = RestCaller
//						.getAccountById(salesOrderTransHdr.getCustomerId().getId());
//				AccountHeads accountHeads1 = accountHead1.getBody();
//				dayBook.setDrAccountName(accountHeads1.getAccountName());
//				dayBook.setDrAmount(receipts.getReceiptAmount());
//				dayBook.setSourceVoucheNumber(salesOrderTransHdr.getVoucherNumber());
//				if (receipts.getReceiptMode().contains("CASH")) {
//
//					String account = SystemSetting.systemBranch + "-CASH";
//					ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(account);
//					dayBook.setDrAccountName(accountHeads.getBody().getAccountName());
//
//					dayBook.setNarration("SALESORDER CASH ADVANCE");
//					dayBook.setCrAmount(0.0);
//					dayBook.setCrAccountName(salesOrderTransHdr.getCustomerId().getCustomerName());
//
//				}
//				dayBook.setSourceVoucherType("SALESORDER RECEIPTS");
//
//				LocalDate rdate = SystemSetting.utilToLocaDate(salesOrderTransHdr.getVoucherDate());
//				dayBook.setsourceVoucherDate(rdate);
//				ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
//			}

//			
//			DayBook dayBook = new DayBook();
//			dayBook.setBranchCode(SystemSetting.systemBranch);
//			
//			dayBook.setCrAccountName(salesOrderTransHdr.getCustomerId().getCustomerName());
//			dayBook.setCrAmount(salesOrderTransHdr.getAdvanceAmount());
//			dayBook.setNarration("SALEORDER CASH ADVANCE");
//			dayBook.setSourceVoucheNumber(salesOrderTransHdr.getVoucherNumber());
//			dayBook.setSourceVoucherType("SALEORDER RECEIPT");
//			dayBook.setDrAccountName(salesOrderTransHdr.getCustomerId().getCustomerName());
//			dayBook.setDrAmount(salesOrderTransHdr.getAdvanceAmount());
//
//			LocalDate rdate = SystemSetting.utilToLocaDate(salesOrderTransHdr.getVoucherDate());
//			dayBook.setsourceVoucherDate(java.sql.Date.valueOf(rdate));
//			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			Format formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			String vdate = formatter.format(salesOrderTransHdr.getVoucherDate());

			// Version 1.6 Starts
			txtcardAmount.setText("");
			txtCashtopay1.setText("");
			txtCashPaidamount.setText("");
			txtLocalCustomerPhone.clear();
			txtLocalCustomerName.setText("");
			txtLocalCustomerAddress.setText("");
			txtGstCustomer.setText("");
			dpDueDate.setValue(null);
			txtChangeamount.clear();
			saleListTable.clear();
			txtDueTime.clear();
			tblCardAmountDetails.getItems().clear();

			salesOrderDtl = null;

			saleOrderDelete = null;

			// Version 1.6 Ends

			try {
				JasperPdfReportService.SaleOrderReport(salesOrderTransHdr.getVoucherNumber(), vdate);
			} catch (JRException e) {
				e.printStackTrace();
			}

		}
		salesOrderTransHdr = null;

		txtSaleOrderHdrId.clear();
		cmbAmOrPm.getSelectionModel().select("pm");
		lblCardType.setVisible(false);
		cmbCardType.setVisible(false);
		txtCrAmount.setVisible(false);
		AddCardAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		tblCardAmountDetails.setVisible(false);
		txtcardAmount.setVisible(false);
		txtMessage.clear();


	}

	// --------------------------version 6.4 surya
	private void changeAdvanceAmount(SalesOrderTransHdr salesOrderTransHdr) {

		ResponseEntity<List<SaleOrderReceipt>> saleOrderCashReciept = RestCaller
				.SaleOrderReceiptByRecieptMode(salesOrderTransHdr.getId(), "CASH");

		List<SaleOrderReceipt> receiptList = saleOrderCashReciept.getBody();
		for (SaleOrderReceipt reciept : receiptList) {
			RestCaller.deleteSalesOrderReceipts(reciept.getId());

		}

		saleOrderReceipt = new SaleOrderReceipt();
		saleOrderReceipt.setReceiptMode("CASH");
		saleOrderReceipt.setReceiptAmount(Double.parseDouble(txtChangeAdvncAmount.getText()));
		saleOrderReceipt.setUserId(SystemSetting.getUser().getId());
		saleOrderReceipt.setBranchCode(SystemSetting.systemBranch);

		salesReceiptVoucherNo = salesOrderTransHdr.getSaleOrderReceiptVoucherNumber();
		saleOrderReceipt.setVoucherNumber(salesReceiptVoucherNo);
		// account id

		String account = SystemSetting.systemBranch + "-CASH";
		ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(account);
		saleOrderReceipt.setAccountId(accountHeads.getBody().getId());
		LocalDate date = LocalDate.now();
		java.util.Date udate = SystemSetting.localToUtilDate(date);
		saleOrderReceipt.setRereceiptDate(udate);
		saleOrderReceipt.setSalesOrderTransHdr(salesOrderTransHdr);
		System.out.println(saleOrderReceipt);
		ResponseEntity<SaleOrderReceipt> respEntity = RestCaller.saveSaleOrderReceipts(saleOrderReceipt);
		saleOrderReceipt = respEntity.getBody();

	}

	private void calcDiscountOnBasePrice(SalesOrderTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(rateBefrTax.doubleValue());
//			salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesOrder/Dtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesOrderDtl.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);

	}

	private void calcDiscountOnMRP(SalesOrderTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * mrpRateIncludingTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRate(BnewrateBeforeTax.doubleValue());
			// BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			// rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
			// salesDtl.setMrp(BrateAfterDiscount.doubleValue());
//			salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * mrpRateIncludingTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(mrpRateIncludingTax);
//					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesOrderDtl.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);

	}

	private void ambrossiaDiscount(SalesOrderTransHdr saleOrderTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {

		if (saleOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discoutAmount = (rateBeforeTax * saleOrderTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(discoutAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			double newRate = rateBeforeTax - discoutAmount;
			salesOrderDtl.setDiscount(discoutAmount);
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesOrderDtl.setRate(BnewRate.doubleValue());
			BigDecimal BrateBeforeTax = new BigDecimal(rateBeforeTax);
			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
//		salesOrderDtl.sets(BrateBeforeTax.doubleValue());
		} else {
			salesOrderDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (saleOrderTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (saleOrderTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + saleOrderTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesOrderDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesOrderDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesOrderDtl.setRate(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesOrderDtl.getQty() * salesOrderDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesOrderDtl.setCessRate(cessRate);
		salesOrderDtl.setCessAmount(cessAmount);

	}
	// --------------------------version 6.4 surya end

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }

}
