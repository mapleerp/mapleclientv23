package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.entity.GSTOutputDetailReport;
import com.maple.mapleclient.entity.PharmacyGroupWiseSalesReport;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.CustomerNewPopUpEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PharmacyNewCustomerWiseSaleReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacyNewCustomerWiseSalesReportCtl {

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnPrintReport;
    
 
    
    @FXML
    private TextField txtCustomer;

    @FXML
    private TableView<PharmacyNewCustomerWiseSaleReport> tblSalesReport;

    @FXML
    private TableColumn<PharmacyNewCustomerWiseSaleReport, String> clmnInvoiceDate;

    @FXML
    private TableColumn<PharmacyNewCustomerWiseSaleReport, String> clmnVoucherNumber;

    @FXML
    private TableColumn<PharmacyNewCustomerWiseSaleReport, String> clmnItemName;

    @FXML
    private TableColumn<PharmacyNewCustomerWiseSaleReport, String> clmnBatch;

    @FXML
    private TableColumn<PharmacyNewCustomerWiseSaleReport, Number> clmnQty;

    @FXML
    private TableColumn<PharmacyNewCustomerWiseSaleReport, Number> clmnAmount;

    @FXML
    private TableColumn<PharmacyNewCustomerWiseSaleReport, Number> clmnTax;

    @FXML
    private ListView<String> lstCustomerList;

    @FXML
    private Button btnAdd;

    
    @FXML
    private Button btnClear;
   
    EventBus eventBus = EventBusFactory.getEventBus();
    
    
    private ObservableList<PharmacyNewCustomerWiseSaleReport> pharmacyNewCustomerWiseSaleReport = FXCollections.observableArrayList();
    
    @FXML
    void initialize() {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	
    }
    @FXML
    void generateReport(ActionEvent event) {

    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select from date", false);
			return;
    	}
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, "Please select to date", false);
    		return;
    	}
    	
    	
    	if(null==lstCustomerList.getSelectionModel().getSelectedItem()) {
    		notifyMessage(5, "plz add Customer",false);
    		return;
    	}
    	
        
    	
  List<String> selectedCustomer = lstCustomerList.getSelectionModel().getSelectedItems();
        
    	String cust="";
    	for(String s:selectedCustomer)
    	{
    		s = s.concat(";");
    		cust= cust.concat(s);
    	} 
    	
    	
    	Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		
		ResponseEntity<List<PharmacyNewCustomerWiseSaleReport>> customerWiseSale=RestCaller.getAMDCCustomerWiseSaleReport(sfdate,stdate,cust);
		pharmacyNewCustomerWiseSaleReport = FXCollections.observableArrayList(customerWiseSale.getBody());
		    tblSalesReport.setItems(pharmacyNewCustomerWiseSaleReport);
			filltable();
		
    }

    private void filltable() {
    	
    	clmnVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
    	clmnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clmnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
    	clmnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clmnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clmnTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());
		
	}
	@FXML
    void printReport(ActionEvent event) {
    	Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		
		
		List<String> selectedCustomer = lstCustomerList.getSelectionModel().getSelectedItems();
        
    	String cust="";
    	for(String s:selectedCustomer)
    	{
    		s = s.concat(";");
    		cust= cust.concat(s);
    	} 

		  try { JasperPdfReportService.PharmacyNewCustomerWiseSaleReport(sfdate,
				  stdate,cust); } catch (JRException e) { // TODO Auto-gesnerated catch block
		  e.printStackTrace(); }
    	
    		
    	
		
    
    }
    
    
    @FXML
    void addCustomer(ActionEvent event) {
    	lstCustomerList.getItems().add(txtCustomer.getText());
    	lstCustomerList.getSelectionModel().select(txtCustomer.getText());
    	txtCustomer.clear();
    }
    
    @FXML
    void onEnterCustomer(ActionEvent event) {
    	loadCustomerPopup();
    }
    
    
    private void loadCustomerPopup() {
    	try {

//		txtLocalCustomer.clear();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CusstomerNewPopUp.fxml"));
		Parent root1;

		root1 = (Parent) fxmlLoader.load();
		Stage stage = new Stage();

		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setTitle("ABC");
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setScene(new Scene(root1));
		stage.show();


	} catch (IOException e) {
		//
		e.printStackTrace();
	}
    
    }
    
    
    @Subscribe
	public void popupCustomerlistner(CustomerNewPopUpEvent customerNewPopUpEvent) {

		ResponseEntity<AccountHeads> getCustomer = RestCaller.getAccountHeadsById(customerNewPopUpEvent.getCustomerId());
		AccountHeads custmrMst = new AccountHeads();
		custmrMst = getCustomer.getBody();

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			txtCustomer.setText(customerNewPopUpEvent.getCustomerName());
		}
	}
    
    
    
    private void notifyMessage(int i, String string, boolean b) {
		Image img;
		if (b) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

		
	}
    
    @FXML
    void clear(ActionEvent event) {
    	txtCustomer.clear();
    	dpFromDate.setValue(null);
    	dpToDate.setValue(null);
    	lstCustomerList.getItems().clear();
    }

}