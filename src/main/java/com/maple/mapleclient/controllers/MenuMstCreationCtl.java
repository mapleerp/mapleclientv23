package com.maple.mapleclient.controllers;



import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.InvoiceFormatMst;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.MenuMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.ProcessPermissionMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.MenuEvent;
import com.maple.mapleclient.events.ParentMenuEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PharmacyDailySalesTransactionReport;
import com.maple.report.entity.PharmacyPurchaseReturnDetailReport;
import com.maple.report.entity.SalesInvoiceReport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class MenuMstCreationCtl {
	String taskid;
	String processInstanceId;


	private ObservableList<MenuMst> menuList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();
	MenuMst menuMst = null;

	@FXML
	private Button btnUpdate;

	@FXML
	private ComboBox<String> cmbPriceId;
	@FXML
	private ComboBox<String> cmbJasperFormat;
	@FXML
	private TextField txtMenuName;

	@FXML
	private TextField txtParentMenu;
	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowAll;

	@FXML
	private TableView<MenuMst> tblMenuReport;

	@FXML
	private TableColumn<MenuMst, String> clMenu;

	@FXML
	private TableColumn<MenuMst, String> clParentMenu;

	@FXML
	private Button tbnDefaultMenu;

	@FXML
	private Button btnInitialize;
	@FXML
	private Button btnClear;
	
	
	
	
	
	
	
	//---------------------------
	
    @FXML
    private Button btnTest;
	
    

    @FXML
    void Test(ActionEvent event) {
    	
    	SalesTransHdr salesTrans = new SalesTransHdr();
    	
    	salesTrans.setSalesManId("WAITER1");
    	salesTrans.setServingTableName("TABLE1");
    	salesTrans.setKotNumber("1");
    	salesTrans.setBranchCode(SystemSetting.getSystemBranch());
    	salesTrans.setSalesMode("KOT");
    	
    	List<SalesDtl> salesDtlList = new ArrayList<SalesDtl>();
    	
    	SalesDtl salesDtl = new SalesDtl();
    	
    	salesDtl.setItemId("SAS1I000001SAS1");
    	salesDtl.setQty(1.0);
    	salesDtl.setSalesTransHdr(salesTrans);
    	salesDtl.setKotDescription("With out sugar");
    	
    	
    	salesDtlList.add(salesDtl);
    	
    	ResponseEntity<String> saveKot = RestCaller.saveKotOrder(salesDtlList);
    	

    }

	
	
	//--------------
	
	
	
	
	
	

	@FXML
	void Initialize(ActionEvent event) {

		try {
			InitializeMenuConfig();
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	private void InitializeMenuConfig() {

		ResponseEntity<List<MenuConfigMst>> menuConfigMstResp = RestCaller.MenuConfigMstByMenuName("KOT POS");
		List<MenuConfigMst> menuConfigMstList = menuConfigMstResp.getBody();

		if (menuConfigMstList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("KOT POS");
			menuConfigMst.setMenuFxml("kotpos1000x600.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		
		ResponseEntity<List<MenuConfigMst>> menuConfigMstResp2 = RestCaller.MenuConfigMstByMenuName("REPORTS");
		List<MenuConfigMst> menuConfigMstList2 = menuConfigMstResp2.getBody();

		if (menuConfigMstList2.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("REPORTS");
			menuConfigMst.setMenuFxml("reportTreeWindow.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstResp3 = RestCaller.MenuConfigMstByMenuName("INVENTORY");
		List<MenuConfigMst> menuConfigMstList3 = menuConfigMstResp3.getBody();

		if (menuConfigMstList3.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INVENTORY");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		

		ResponseEntity<List<MenuConfigMst>> menuConfigMstResp4 = RestCaller
				.MenuConfigMstByMenuName("MONTHLY STOCK REPORT");
		List<MenuConfigMst> menuConfigMstList4 = menuConfigMstResp4.getBody();

		if (menuConfigMstList4.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MONTHLY STOCK REPORT");
			menuConfigMst.setMenuFxml("stockReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		


		ResponseEntity<List<MenuConfigMst>> menuConfigMstRespacceptStoclWhithReturn = RestCaller
				.MenuConfigMstByMenuName("ACCEPT STOCK WHITH RETURN");
		List<MenuConfigMst> menuConfigMstLitAcceptStockWhithReturn = menuConfigMstRespacceptStoclWhithReturn.getBody();

		if (menuConfigMstLitAcceptStockWhithReturn.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCEPT STOCK WHITH RETURN");
			menuConfigMst.setMenuFxml("AcceptStockWhithReturn.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
	


		ResponseEntity<List<MenuConfigMst>> menuConfigMstServiceResp = RestCaller.MenuConfigMstByMenuName("SERVICE");
		List<MenuConfigMst> menuConfigMstServiceList = menuConfigMstServiceResp.getBody();

		if (menuConfigMstServiceList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SERVICE");
			menuConfigMst.setMenuFxml("ServiceTree.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstServiceReportResp = RestCaller
				.MenuConfigMstByMenuName("SERVICE REPORT");
		List<MenuConfigMst> menuConfigMstServiceReportList = menuConfigMstServiceResp.getBody();

		if (menuConfigMstServiceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SERVICE REPORT");
			menuConfigMst.setMenuFxml("ServiceReportTree.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstConfigurationResp = RestCaller
				.MenuConfigMstByMenuName("CONFIGURATIONS");
		List<MenuConfigMst> menuConfigMstConfigurationList = menuConfigMstServiceResp.getBody();

		if (menuConfigMstConfigurationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CONFIGURATIONS");
			menuConfigMst.setMenuFxml("ConfigTree.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstOrgResp = RestCaller.MenuConfigMstByMenuName("ORGANIZATION");
		List<MenuConfigMst> menuConfigMstOrgList = menuConfigMstOrgResp.getBody();

		if (menuConfigMstOrgList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ORGANIZATION");
			menuConfigMst.setMenuFxml("OrgTree.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstAccountResp = RestCaller.MenuConfigMstByMenuName("ACCOUNTS");
		List<MenuConfigMst> menuConfigMstAccountList = menuConfigMstAccountResp.getBody();

		if (menuConfigMstAccountList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCOUNTS");
			menuConfigMst.setMenuFxml("AccountsTree.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstAdvanceFeatureResp = RestCaller
				.MenuConfigMstByMenuName("ADVANCED FEATURES");
		List<MenuConfigMst> menuConfigMstAdvanceFeatureList = menuConfigMstAdvanceFeatureResp.getBody();

		if (menuConfigMstAdvanceFeatureList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ADVANCED FEATURES");
			menuConfigMst.setMenuFxml("AdvancedFeatures.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPharmacyResp = RestCaller.MenuConfigMstByMenuName("PHARMACY");
		List<MenuConfigMst> menuConfigMstPharmacyList = menuConfigMstPharmacyResp.getBody();

		if (menuConfigMstPharmacyList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyTree.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstConsumptionResp = RestCaller
				.MenuConfigMstByMenuName("CONSUMPTION");
		List<MenuConfigMst> menuConfigMstConsumptionList = menuConfigMstConsumptionResp.getBody();

		if (menuConfigMstConsumptionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CONSUMPTION");
			menuConfigMst.setMenuFxml("Consumption.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstOccupiedTblResp = RestCaller
				.MenuConfigMstByMenuName("OCCUPIED TABLES");
		List<MenuConfigMst> menuConfigMstOccupiedTblList = menuConfigMstOccupiedTblResp.getBody();

		if (menuConfigMstOccupiedTblList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OCCUPIED TABLES");
			menuConfigMst.setMenuFxml("TableOccupied.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemImageResp = RestCaller
				.MenuConfigMstByMenuName("SET ITEM IMAGE");
		List<MenuConfigMst> menuConfigMstItemImageList = menuConfigMstItemImageResp.getBody();

		if (menuConfigMstItemImageList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SET ITEM IMAGE");
			menuConfigMst.setMenuFxml("ItemImage.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstDamageEntryResp = RestCaller
				.MenuConfigMstByMenuName("DAMAGE ENTRY");
		List<MenuConfigMst> menuConfigMstDamageEntryList = menuConfigMstDamageEntryResp.getBody();

		if (menuConfigMstDamageEntryList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAMAGE ENTRY");
			menuConfigMst.setMenuFxml("DamageEntry.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstWholeSaleResp = RestCaller
				.MenuConfigMstByMenuName("WHOLESALE");
		List<MenuConfigMst> menuConfigMstWholeSaleList = menuConfigMstWholeSaleResp.getBody();

		if (menuConfigMstWholeSaleList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("WHOLESALE");
			menuConfigMst.setMenuFxml("wholeSaleWindow.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstRetailResp = RestCaller.MenuConfigMstByMenuName("RETAIL");
		List<MenuConfigMst> menuConfigMstRetailList = menuConfigMstRetailResp.getBody();

		if (menuConfigMstRetailList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RETAIL");
			menuConfigMst.setMenuFxml("NewWholesaleWindow.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPosResp = RestCaller.MenuConfigMstByMenuName("POS WINDOW");
		List<MenuConfigMst> menuConfigMstPosList = menuConfigMstPosResp.getBody();

		if (menuConfigMstPosList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("POS WINDOW");
			menuConfigMst.setMenuFxml("poswindow.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstTakeOrderResp = RestCaller
				.MenuConfigMstByMenuName("TAKE ORDER");
		List<MenuConfigMst> menuConfigMstTakeOrderList = menuConfigMstTakeOrderResp.getBody();

		if (menuConfigMstTakeOrderList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TAKE ORDER");
			menuConfigMst.setMenuFxml("TakeOrder.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstOnlineSaleResp = RestCaller
				.MenuConfigMstByMenuName("ONLINE SALES");
		List<MenuConfigMst> menuConfigMstOnlineSaleList = menuConfigMstOnlineSaleResp.getBody();

		if (menuConfigMstOnlineSaleList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ONLINE SALES");
			menuConfigMst.setMenuFxml("onlineSales.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstReOrderResp = RestCaller.MenuConfigMstByMenuName("ReORDER");
		List<MenuConfigMst> menuConfigMstReOrderList = menuConfigMstReOrderResp.getBody();

		if (menuConfigMstReOrderList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ReORDER");
			menuConfigMst.setMenuFxml("Reorder2.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstProductionResp = RestCaller
				.MenuConfigMstByMenuName("PRODUCTION");
		List<MenuConfigMst> menuConfigMstProductionList = menuConfigMstProductionResp.getBody();

		if (menuConfigMstProductionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCTION");
			menuConfigMst.setMenuFxml("productionTree.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSaleOrderResp = RestCaller
				.MenuConfigMstByMenuName("SALEORDER");
		List<MenuConfigMst> menuConfigMstSaleOrderList = menuConfigMstSaleOrderResp.getBody();

		if (menuConfigMstSaleOrderList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALEORDER");
			menuConfigMst.setMenuFxml("SaleOrderWindow.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemMasterResp = RestCaller
				.MenuConfigMstByMenuName("ITEM MASTER");
		List<MenuConfigMst> menuConfigMstItemMasterList = menuConfigMstItemMasterResp.getBody();

		if (menuConfigMstItemMasterList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM MASTER");
			menuConfigMst.setMenuFxml("itemcreate.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstAccountHeadsResp = RestCaller
				.MenuConfigMstByMenuName("ACCOUNT HEADS");
		List<MenuConfigMst> menuConfigMstAccountHeadsList = menuConfigMstAccountHeadsResp.getBody();

		if (menuConfigMstAccountHeadsList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCOUNT HEADS");
			menuConfigMst.setMenuFxml("AccountHead.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstProdudctConvertionResp = RestCaller
				.MenuConfigMstByMenuName("PRODUCT CONVERSION");
		List<MenuConfigMst> menuConfigMstProductConvertionList = menuConfigMstProdudctConvertionResp.getBody();

		if (menuConfigMstProductConvertionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCT CONVERSION");
			menuConfigMst.setMenuFxml("ProductionConversion.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPurchaseResp = RestCaller.MenuConfigMstByMenuName("PURCHASE");
		List<MenuConfigMst> menuConfigMstPurchaseList = menuConfigMstPurchaseResp.getBody();

		if (menuConfigMstPurchaseList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE");
			menuConfigMst.setMenuFxml("purchase.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

//		ResponseEntity<List<MenuConfigMst>> menuConfigMstCustomerResp = RestCaller.MenuConfigMstByMenuName("CUSTOMER");
//		List<MenuConfigMst> menuConfigMstCustomerList = menuConfigMstCustomerResp.getBody();
//
//		if (menuConfigMstPurchaseList.size() == 0) {
//			MenuConfigMst menuConfigMst = new MenuConfigMst();
//			menuConfigMst.setMenuName("CUSTOMER");
//			menuConfigMst.setMenuFxml("CustRegistration.fxml");
//			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
//
//			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
//		}

		
		ResponseEntity<List<MenuConfigMst>> menuConfigMstPartycreationResp = RestCaller.MenuConfigMstByMenuName("PARTY CREATION");
		List<MenuConfigMst> menuConfigMstPartycreationList = menuConfigMstPartycreationResp.getBody();

		if (menuConfigMstPartycreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PARTY CREATION");
			menuConfigMst.setMenuFxml("PartyCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		
		
		
		ResponseEntity<List<MenuConfigMst>> menuConfigMstAcceptStockResp = RestCaller
				.MenuConfigMstByMenuName("ACCEPT STOCK");
		List<MenuConfigMst> menuConfigMstAcceptStockList = menuConfigMstAcceptStockResp.getBody();

		if (menuConfigMstAcceptStockList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCEPT STOCK");
			menuConfigMst.setMenuFxml("AcceptStock.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstStockTransferResp = RestCaller
				.MenuConfigMstByMenuName("STOCK TRANSFER");
		List<MenuConfigMst> menuConfigMstStockTransferList = menuConfigMstStockTransferResp.getBody();

		if (menuConfigMstStockTransferList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK TRANSFER");
			menuConfigMst.setMenuFxml("StockTransfer.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstDayEndClosureResp = RestCaller
				.MenuConfigMstByMenuName("DAY END CLOSURE");
		List<MenuConfigMst> menuConfigMstClosureList = menuConfigMstDayEndClosureResp.getBody();

		if (menuConfigMstClosureList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAY END CLOSURE");
			menuConfigMst.setMenuFxml("DayEndReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstInvoiceEditResp = RestCaller
				.MenuConfigMstByMenuName("INVOICE EDIT");
		List<MenuConfigMst> menuConfigMstInvoiceEditList = menuConfigMstInvoiceEditResp.getBody();

		if (menuConfigMstInvoiceEditList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INVOICE EDIT");
			menuConfigMst.setMenuFxml("WholeSaleInvoiceEdit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSOEditResp = RestCaller
				.MenuConfigMstByMenuName("SALES ORDER EDIT");
		List<MenuConfigMst> menuConfigMstSOEditList = menuConfigMstSOEditResp.getBody();

		if (menuConfigMstSOEditList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES ORDER EDIT");
			menuConfigMst.setMenuFxml("SaleOrderEdit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstWMDisplayResp = RestCaller
				.MenuConfigMstByMenuName("WEIGHING MACHINE");
		List<MenuConfigMst> menuConfigMstWMDisplayList = menuConfigMstWMDisplayResp.getBody();

		if (menuConfigMstWMDisplayList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("WEIGHING MACHINE");
			menuConfigMst.setMenuFxml("WeighingMachineDisplay.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstMsterResp = RestCaller.MenuConfigMstByMenuName("MASTERS");
		List<MenuConfigMst> menuConfigMstMasterList = menuConfigMstMsterResp.getBody();

		if (menuConfigMstMasterList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MASTERS");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstAccountingReportResp = RestCaller
				.MenuConfigMstByMenuName("ACCOUNTING REPORTS");
		List<MenuConfigMst> menuConfigMstAccountingReportList = menuConfigMstAccountingReportResp.getBody();

		if (menuConfigMstAccountingReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCOUNTING REPORTS");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

//		ResponseEntity<List<MenuConfigMst>> menuConfigMstSalesReportResp = RestCaller
//				.MenuConfigMstByMenuName("SALES REPORT");
//		List<MenuConfigMst> menuConfigMstSalesReportList = menuConfigMstSalesReportResp.getBody();
//
//		if (menuConfigMstSalesReportList.size() == 0) {
//			MenuConfigMst menuConfigMst = new MenuConfigMst();
//			menuConfigMst.setMenuName("SALES REPORT");
//			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");
//
//			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
//		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSalesReturnReportResp = RestCaller
				.MenuConfigMstByMenuName("SALES RETURN REPORT");
		List<MenuConfigMst> menuConfigMstSalesReturnReportList = menuConfigMstSalesReturnReportResp.getBody();

		if (menuConfigMstSalesReturnReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES RETURN REPORT");
			menuConfigMst.setMenuFxml("SalesReturnReport.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPharnacySaleReturnReportResp = RestCaller
				.MenuConfigMstByMenuName("SALES RETURN REPORT PHARMACY");
		List<MenuConfigMst> menuConfigMstPharmacySalesReturnReportList = menuConfigMstPharnacySaleReturnReportResp
				.getBody();

		if (menuConfigMstPharmacySalesReturnReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES RETURN REPORT PHARMACY");
			menuConfigMst.setMenuFxml("SalesReturnReport.fxml");

			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

//		ResponseEntity<List<MenuConfigMst>> menuConfigMstSOReportResp = RestCaller
//				.MenuConfigMstByMenuName("SALEORDER REPORT");
//		List<MenuConfigMst> menuConfigMstSOReportList = menuConfigMstSOReportResp.getBody();
//
//		if (menuConfigMstSOReportList.size() == 0) {
//			MenuConfigMst menuConfigMst = new MenuConfigMst();
//			menuConfigMst.setMenuName("SALEORDER REPORT");
//			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");
//
//			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
//		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSOSummaryReportResp = RestCaller
				.MenuConfigMstByMenuName("SALEORDER REPORT");
		List<MenuConfigMst> menuConfigMstSOSummaryReportList = menuConfigMstSOSummaryReportResp.getBody();

		if (menuConfigMstSOSummaryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALEORDER REPORT");
			menuConfigMst.setMenuFxml("SaleorderReport.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSTReportResp = RestCaller
				.MenuConfigMstByMenuName("STOCK TRANSFER REPORT");
		List<MenuConfigMst> menuConfigMstSTReportList = menuConfigMstSTReportResp.getBody();

		if (menuConfigMstSTReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK TRANSFER REPORT");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstReprintResp = RestCaller.MenuConfigMstByMenuName("REPRINT");
		List<MenuConfigMst> menuConfigMstReprintList = menuConfigMstReprintResp.getBody();

		if (menuConfigMstReprintList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("REPRINT");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstReceiptReportResp = RestCaller
				.MenuConfigMstByMenuName("RECEIPT REPORT");
		List<MenuConfigMst> menuConfigMstReceiptReportList = menuConfigMstReceiptReportResp.getBody();

		if (menuConfigMstReceiptReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RECEIPT REPORT");
			menuConfigMst.setMenuFxml("ReceiptReport.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPaymentReportResp = RestCaller
				.MenuConfigMstByMenuName("PAYMENT REPORTS");
		List<MenuConfigMst> menuConfigMstPaymentReportList = menuConfigMstPaymentReportResp.getBody();

		if (menuConfigMstPaymentReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PAYMENT REPORTS");
			menuConfigMst.setMenuFxml("PaymentReport.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		/*
		 * ResponseEntity<List<MenuConfigMst>> menuConfigMstJournalReportResp =
		 * RestCaller .MenuConfigMstByMenuName("JOURNAL REPORTS"); List<MenuConfigMst>
		 * menuConfigMstJournalReportList = menuConfigMstJournalReportResp.getBody();
		 * 
		 * if (menuConfigMstJournalReportList.size() == 0) { MenuConfigMst menuConfigMst
		 * = new MenuConfigMst(); menuConfigMst.setMenuName("JOURNAL REPORTS");
		 * 
		 * menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");
		 * 
		 * ResponseEntity<MenuConfigMst> menuConfigMstSaved =
		 * RestCaller.saveMenuConfigMst(menuConfigMst); }
		 */

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPurchaseReportResp = RestCaller
				.MenuConfigMstByMenuName("PURCHASE REPORT");
		List<MenuConfigMst> menuConfigMstPurchaseReportList = menuConfigMstPurchaseReportResp.getBody();

		if (menuConfigMstPurchaseReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE REPORT");
			menuConfigMst.setMenuFxml("purchaseReport.fxml");

			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstProductionReportResp = RestCaller
				.MenuConfigMstByMenuName("PRODUCTION REPORT");
		List<MenuConfigMst> menuConfigMstProductionReportList = menuConfigMstProductionReportResp.getBody();

		if (menuConfigMstProductionReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCTION REPORT");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstBatchInventoryReportResp = RestCaller
				.MenuConfigMstByMenuName("BATCHWISE INVENTORY REPORT");
		List<MenuConfigMst> menuConfigMstBatchInventoryReportList = menuConfigMstBatchInventoryReportResp.getBody();

		if (menuConfigMstBatchInventoryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BATCHWISE INVENTORY REPORT");
			
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstConsumptionReportResp = RestCaller
				.MenuConfigMstByMenuName("CONSUMPTION REPORT");
		List<MenuConfigMst> menuConfigMstConsumptionReportList = menuConfigMstConsumptionReportResp.getBody();

		if (menuConfigMstBatchInventoryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CONSUMPTION REPORT");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		/*
		 * ResponseEntity<List<MenuConfigMst>> menuConfigMstReasonConsumptionReportResp
		 * = RestCaller .MenuConfigMstByMenuName("REASON WISE CONSUMPTION REPORT");
		 * List<MenuConfigMst> menuConfigMstResonConsumptionReportList =
		 * menuConfigMstReasonConsumptionReportResp .getBody();
		 * 
		 * if (menuConfigMstResonConsumptionReportList.size() == 0) { MenuConfigMst
		 * menuConfigMst = new MenuConfigMst();
		 * menuConfigMst.setMenuName("REASON WISE CONSUMPTION REPORT");
		 * 
		 * menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");
		 * 
		 * ResponseEntity<MenuConfigMst> menuConfigMstSaved =
		 * RestCaller.saveMenuConfigMst(menuConfigMst); }
		 */

		ResponseEntity<List<MenuConfigMst>> menuConfigMstExceptionnReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEM EXCEPTION REPORTS");
		List<MenuConfigMst> menuConfigMstExceptionReportList = menuConfigMstExceptionnReportResp.getBody();

		if (menuConfigMstExceptionReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM EXCEPTION REPORTS");

			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstWMReportResp = RestCaller
				.MenuConfigMstByMenuName("WEIGHING MACHINE REPORT");
		List<MenuConfigMst> menuConfigMstWMReportList = menuConfigMstWMReportResp.getBody();

		if (menuConfigMstWMReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("WEIGHING MACHINE REPORT");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstStockEnquiryReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEM STOCK ENQUIRY");
		List<MenuConfigMst> menuConfigMstStockEnquiryReportList = menuConfigMstStockEnquiryReportResp.getBody();

		if (menuConfigMstStockEnquiryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM STOCK ENQUIRY");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPhysicalStockReportResp = RestCaller
				.MenuConfigMstByMenuName("PHYSICAL STOCK REPORT");
		List<MenuConfigMst> menuConfigMstPhysicalStockReportList = menuConfigMstPhysicalStockReportResp.getBody();

		if (menuConfigMstPhysicalStockReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PHYSICAL STOCK REPORT");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstIntentReportResp = RestCaller
				.MenuConfigMstByMenuName("INTENT REPORT");
		List<MenuConfigMst> menuConfigIntentReportList = menuConfigMstIntentReportResp.getBody();

		if (menuConfigIntentReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INTENT REPORT");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstIncomeAndExpenseReportResp = RestCaller
				.MenuConfigMstByMenuName("INCOME AND EXPENCE REPORT");
		List<MenuConfigMst> menuConfigIncomeAndExpenseReportList = menuConfigMstIncomeAndExpenseReportResp.getBody();

		if (menuConfigIncomeAndExpenseReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INCOME AND EXPENCE REPORT");
			menuConfigMst.setMenuFxml("financialYearStatement.fxml");
			menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCustReportResp = RestCaller
				.MenuConfigMstByMenuName("KIT REPORT");
		List<MenuConfigMst> menuConfigMstCustReportList = menuConfigMstCustReportResp.getBody();

		if (menuConfigMstCustReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("KIT REPORT");
			menuConfigMst.setMenuFxml("KitReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstStockReportResp = RestCaller
				.MenuConfigMstByMenuName("STOCK REPORT");
		List<MenuConfigMst> menuConfigMstStockReportList = menuConfigMstStockReportResp.getBody();

		if (menuConfigMstStockReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK REPORT");
			menuConfigMst.setMenuFxml("stockReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstMobileStockReportResp = RestCaller
				.MenuConfigMstByMenuName("MOBILE STOCK REPORT");
		List<MenuConfigMst> menuConfigMstMobileStockReportList = menuConfigMstMobileStockReportResp.getBody();

		if (menuConfigMstMobileStockReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MOBILE STOCK REPORT");
			menuConfigMst.setMenuFxml("stockReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemStockReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEM STOCK REPORT");
		List<MenuConfigMst> menuConfigMstItemStockReportList = menuConfigMstItemStockReportResp.getBody();

		if (menuConfigMstItemStockReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM STOCK REPORT");
			menuConfigMst.setMenuFxml("ItemWiseStockReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemStockMovementReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEMWISE STOCK MOVEMENT REPORT");
		List<MenuConfigMst> menuConfigMstItemStockMovementReportList = menuConfigMstItemStockMovementReportResp
				.getBody();

		if (menuConfigMstItemStockMovementReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEMWISE STOCK MOVEMENT REPORT");
			menuConfigMst.setMenuFxml("StockMovementReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCategoryStockMovementReportResp = RestCaller
				.MenuConfigMstByMenuName("CATEGORYWISE STOCK REPORT");
		List<MenuConfigMst> menuConfigMstCategoryStockMovementReportList = menuConfigMstCategoryStockMovementReportResp
				.getBody();

		if (menuConfigMstCategoryStockMovementReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CATEGORYWISE STOCK REPORT");
			menuConfigMst.setMenuFxml("CategoryWiseMovement.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstInventoryReorderReportResp = RestCaller
				.MenuConfigMstByMenuName("INVENTORY REORDER STATUS REPORT");
		List<MenuConfigMst> menuConfigMstInventoryReorderReportList = menuConfigMstInventoryReorderReportResp.getBody();

		if (menuConfigMstInventoryReorderReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INVENTORY REORDER STATUS REPORT");
			menuConfigMst.setMenuFxml("InventoryReorderStatus.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstStockValueReportResp = RestCaller
				.MenuConfigMstByMenuName("STOCK VALUE REPORT");
		List<MenuConfigMst> menuConfigMstStockValueReportList = menuConfigMstStockValueReportResp.getBody();

		if (menuConfigMstStockValueReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK VALUE REPORT");
			menuConfigMst.setMenuFxml("StockValueReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemLocationReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEM LOCATION REPORT");
		List<MenuConfigMst> menuConfigMstItemLocationReportList = menuConfigMstItemLocationReportResp.getBody();

		if (menuConfigMstItemLocationReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM LOCATION REPORT");
			menuConfigMst.setMenuFxml("ItemLocationReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstFastMovingItemReportResp = RestCaller
				.MenuConfigMstByMenuName("FAST MOVING ITEMS REPORT");
		List<MenuConfigMst> menuConfigMstFastMovingItemReportList = menuConfigMstFastMovingItemReportResp.getBody();

		if (menuConfigMstFastMovingItemReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("FAST MOVING ITEMS REPORT");
			menuConfigMst.setMenuFxml("FastMovingItemsReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstFastMovingItemReportSalesModeResp = RestCaller
				.MenuConfigMstByMenuName("FAST MOVING ITEMS REPORT BY SALES MODE");
		List<MenuConfigMst> menuConfigMstFastMovingItemReportBySalesModeList = menuConfigMstFastMovingItemReportSalesModeResp
				.getBody();

		if (menuConfigMstFastMovingItemReportBySalesModeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("FAST MOVING ITEMS REPORT BY SALES MODE");
			menuConfigMst.setMenuFxml("FastMovingItemReportBySalesMode.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPharmacyItemOrCategoryResp = RestCaller
				.MenuConfigMstByMenuName("ITEM OR CATEGORY MOVEMENT PHARMACY");
		List<MenuConfigMst> menuConfigMstPharmacyItemOrCategoryList = menuConfigMstPharmacyItemOrCategoryResp.getBody();

		if (menuConfigMstPharmacyItemOrCategoryList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM OR CATEGORY MOVEMENT PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyItemStockMovementReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstInhouseConsumptionReportResp = RestCaller
				.MenuConfigMstByMenuName("INHOUSE CONSUMPTION REPORT");
		List<MenuConfigMst> menuConfigMstInhouseConsumptionReportList = menuConfigMstInhouseConsumptionReportResp
				.getBody();

		if (menuConfigMstInhouseConsumptionReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INHOUSE CONSUMPTION REPORT");
			menuConfigMst.setMenuFxml("InhouseConsumptionReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstStatementOfAccountReportResp = RestCaller
				.MenuConfigMstByMenuName("STATEMENTS OF ACCOUNT");
		List<MenuConfigMst> menuConfigMstStatementOfAccountReportList = menuConfigMstStatementOfAccountReportResp
				.getBody();

		if (menuConfigMstStatementOfAccountReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STATEMENTS OF ACCOUNT");
			menuConfigMst.setMenuFxml("AccountBalance.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCustomerBillBalanceReportResp = RestCaller
				.MenuConfigMstByMenuName("ALL CUSTOMER BILL WISE BALANCE REPORT");
		List<MenuConfigMst> menuConfigMstCustomerBillBalanceReportList = menuConfigMstCustomerBillBalanceReportResp
				.getBody();

		if (menuConfigMstCustomerBillBalanceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ALL CUSTOMER BILL WISE BALANCE REPORT");
			menuConfigMst.setMenuFxml("AllCustomerBalanceReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCustomerBillWiseBalanceReportResp = RestCaller
				.MenuConfigMstByMenuName("CUSTOMER BILL WISE BALANCE REPORT");
		List<MenuConfigMst> menuConfigMstCustomerBillWiseBalanceReportList = menuConfigMstCustomerBillWiseBalanceReportResp
				.getBody();

		if (menuConfigMstCustomerBillWiseBalanceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOMER BILL WISE BALANCE REPORT");
			menuConfigMst.setMenuFxml("CustomerBalanceReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSupplierLedgerReportResp = RestCaller
				.MenuConfigMstByMenuName("SUPPLIER LEDGER REPORT");
		List<MenuConfigMst> menuConfigMstSupplierLedgerReportList = menuConfigMstSupplierLedgerReportResp.getBody();

		if (menuConfigMstSupplierLedgerReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUPPLIER LEDGER REPORT");
			menuConfigMst.setMenuFxml("SupplierLedgerReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}


		ResponseEntity<List<MenuConfigMst>> menuConfigMstBranchPriceDefenitionResp = RestCaller
				.MenuConfigMstByMenuName("BRANCH PRICE DEFENITION");
		List<MenuConfigMst> menuConfigMstBranchPriceDefenitionList = menuConfigMstBranchPriceDefenitionResp.getBody();

		if (menuConfigMstBranchPriceDefenitionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BRANCH PRICE DEFENITION");
			menuConfigMst.setMenuFxml("BranchPriceDefenitionMst.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		
		

		ResponseEntity<List<MenuConfigMst>> menuConfigMstTaskApproveResp = RestCaller
				.MenuConfigMstByMenuName("TASK APPROVE");
		List<MenuConfigMst> menuConfigMstTaskApproveList = menuConfigMstTaskApproveResp.getBody();

		if (menuConfigMstTaskApproveList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TASK APPROVE");
			menuConfigMst.setMenuFxml("TaskApprove.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		
		
		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemNutritionResp = RestCaller
				.MenuConfigMstByMenuName("ITEM NUTRITION");
		List<MenuConfigMst> menuConfigMstItemNutritionList = menuConfigMstItemNutritionResp.getBody();

		if (menuConfigMstItemNutritionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM NUTRITION");
			menuConfigMst.setMenuFxml("ItemNutrition.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		
		
		
		ResponseEntity<List<MenuConfigMst>> menuConfigMstReceiptInvoiceReportResp = RestCaller
				.MenuConfigMstByMenuName("RECEIPT INVOICE DETAIL REPORT");
		List<MenuConfigMst> menuConfigMstReceiptInvoiceReportList = menuConfigMstReceiptInvoiceReportResp.getBody();

		if (menuConfigMstReceiptInvoiceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RECEIPT INVOICE DETAIL REPORT");
			menuConfigMst.setMenuFxml("ReceiptInvoiceDtlReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> menuConfigMstOrgReceiptInvoiceReportResp = RestCaller
				.MenuConfigMstByMenuName("ORG PAYMENT INVOICE DETAIL REPORT");
		List<MenuConfigMst> menuConfigMstOrgReceiptInvoiceReportList = menuConfigMstOrgReceiptInvoiceReportResp.getBody();

		if (menuConfigMstOrgReceiptInvoiceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ORG PAYMENT INVOICE DETAIL REPORT");
			menuConfigMst.setMenuFxml("orgReceiptReceiptInvoiceDtlReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemBatchImageResp = RestCaller
				.MenuConfigMstByMenuName("ITEM BATCH IMAGE");
		List<MenuConfigMst> menuConfigMstItemBatchImageReportList = menuConfigMstItemBatchImageResp.getBody();

		if (menuConfigMstItemBatchImageReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM BATCH IMAGE");
			menuConfigMst.setMenuFxml("ItemBatchImage.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		
		ResponseEntity<List<MenuConfigMst>> menuConfigMstPurchaseReturnResp = RestCaller
				.MenuConfigMstByMenuName("PURCHASE RETURN");
		List<MenuConfigMst> menuConfigMstPurchaseReturnList = menuConfigMstPurchaseReturnResp.getBody();

		if (menuConfigMstPurchaseReturnList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE RETURN");
			menuConfigMst.setMenuFxml("PurchaseReturn.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPurchaseAcknowledgeResp = RestCaller
				.MenuConfigMstByMenuName("PURCHASE ACKNOWLEDEGE");
		List<MenuConfigMst> menuConfigMstPurchaseAcknowledgeList = menuConfigMstPurchaseAcknowledgeResp.getBody();

		if (menuConfigMstPurchaseAcknowledgeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE ACKNOWLEDEGE");
			menuConfigMst.setMenuFxml("purchaseAcknowledge.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		
		

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPaymentInvoiceDetailReportResp = RestCaller
				.MenuConfigMstByMenuName("PAYMENT INVOICE DETAIL REPORT");
		List<MenuConfigMst> menuConfigMstPaymentInvoiceDetailReportList = menuConfigMstPaymentInvoiceDetailReportResp
				.getBody();

		if (menuConfigMstPaymentInvoiceDetailReportList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PAYMENT INVOICE DETAIL REPORT");
			menuConfigMst.setMenuFxml("PaymentInvoiceReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstTrialBalanceReportResp = RestCaller
				.MenuConfigMstByMenuName("TRIAL BALANCE");
		List<MenuConfigMst> menuConfigMstTrialBalanceReportList = menuConfigMstTrialBalanceReportResp.getBody();

		if (menuConfigMstTrialBalanceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TRIAL BALANCE");
			menuConfigMst.setMenuFxml("TrialBalanceByDate.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

//		ResponseEntity<List<MenuConfigMst>> menuConfigMstBalanceSheetReportResp = RestCaller
//				.MenuConfigMstByMenuName("BALANCE SHEET");
//		List<MenuConfigMst> menuConfigMstBalanceSheetReportList = menuConfigMstBalanceSheetReportResp.getBody();
//
//		if (menuConfigMstBalanceSheetReportList.size() == 0) {
//			MenuConfigMst menuConfigMst = new MenuConfigMst();
//			menuConfigMst.setMenuName("BALANCE SHEET");
//			menuConfigMst.setMenuFxml("BalanceSheet.fxml");
//			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
//
//			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
//		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSupplierDueReportResp = RestCaller
				.MenuConfigMstByMenuName("SUPPLIER DUE REPORT");
		List<MenuConfigMst> menuConfigMstSupplierDueReportList = menuConfigMstSupplierDueReportResp.getBody();

		if (menuConfigMstSupplierDueReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUPPLIER DUE REPORT");
			menuConfigMst.setMenuFxml("supplierDueReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCardReconcileReportResp = RestCaller
				.MenuConfigMstByMenuName("CARD RECONCILE REPORT");
		List<MenuConfigMst> menuConfigMstCardReconcileReportList = menuConfigMstCardReconcileReportResp.getBody();

		if (menuConfigMstCardReconcileReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CARD RECONCILE REPORT");
			menuConfigMst.setMenuFxml("CardReconcileReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSundryDebtorReportResp = RestCaller
				.MenuConfigMstByMenuName("SUNDRY DEBTOR BALANCE");
		List<MenuConfigMst> menuConfigMstSundryDebtorReportList = menuConfigMstSundryDebtorReportResp.getBody();

		if (menuConfigMstSundryDebtorReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUNDRY DEBTOR BALANCE");
			menuConfigMst.setMenuFxml("SundryDebtorBalanceReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstTaxReportResp = RestCaller
				.MenuConfigMstByMenuName("TAX REPORT");
		List<MenuConfigMst> menuConfigMstTaxReportList = menuConfigMstTaxReportResp.getBody();

		if (menuConfigMstTaxReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TAX REPORT");
			menuConfigMst.setMenuFxml("TaxReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstAccountClosingBalanceReportResp = RestCaller
				.MenuConfigMstByMenuName("ACCOUNT CLOSING BALANCE");
		List<MenuConfigMst> menuConfigMstAccountClosingBalanceReportList = menuConfigMstAccountClosingBalanceReportResp
				.getBody();

		if (menuConfigMstAccountClosingBalanceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCOUNT CLOSING BALANCE");
			menuConfigMst.setMenuFxml("AccountBalanceReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstDailySalesReportResp = RestCaller
				.MenuConfigMstByMenuName("DAILY SALES REPORT");
		List<MenuConfigMst> menuConfigMstDailySalesReportList = menuConfigMstDailySalesReportResp.getBody();

		if (menuConfigMstDailySalesReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAILY SALES REPORT");
			menuConfigMst.setMenuFxml("DailySalesReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPosMonthlySummaryReportResp = RestCaller
				.MenuConfigMstByMenuName("MONTHLY SALES REPORT");
		List<MenuConfigMst> menuConfigMstPosMonthlySummaryReportList = menuConfigMstPosMonthlySummaryReportResp
				.getBody();

		if (menuConfigMstPosMonthlySummaryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MONTHLY SALES REPORT");
			menuConfigMst.setMenuFxml("MonthlySalesReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		// Advanced Features
		ResponseEntity<List<MenuConfigMst>> menuConfigMstINTENT = RestCaller.MenuConfigMstByMenuName("INTENT");
		List<MenuConfigMst> menuConfigMstIntenteList = menuConfigMstINTENT.getBody();

		if (menuConfigMstIntenteList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INTENT");
			menuConfigMst.setMenuFxml("RequestIntent-1000x600.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstACCOUNTMERGE = RestCaller
				.MenuConfigMstByMenuName("ACCOUNT MERGE");
		List<MenuConfigMst> menuConfigMstACCOUNTMERGEList = menuConfigMstACCOUNTMERGE.getBody();

		if (menuConfigMstACCOUNTMERGEList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCOUNT MERGE");
			menuConfigMst.setMenuFxml("accountMerge.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCONSUMPTIONREASON = RestCaller
				.MenuConfigMstByMenuName("CONSUMPTION REASON");
		List<MenuConfigMst> menuConfigMstCONSUMPTIONREASONList = menuConfigMstCONSUMPTIONREASON.getBody();

		if (menuConfigMstCONSUMPTIONREASONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CONSUMPTION REASON");
			menuConfigMst.setMenuFxml("ConsumptionReason.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstDAMAGEENTRY = RestCaller
				.MenuConfigMstByMenuName("DAMAGE ENTRY");
		List<MenuConfigMst> menuConfigMstDAMAGEENTRYList = menuConfigMstDAMAGEENTRY.getBody();

		if (menuConfigMstDAMAGEENTRYList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAMAGE ENTRY");
			menuConfigMst.setMenuFxml("DamageEntry.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPHYSICALSTOCK = RestCaller
				.MenuConfigMstByMenuName("PHYSICAL STOCK");
		List<MenuConfigMst> menuConfigMstPHYSICALSTOCKList = menuConfigMstPHYSICALSTOCK.getBody();

		if (menuConfigMstPHYSICALSTOCKList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PHYSICAL STOCK");
			menuConfigMst.setMenuFxml("physicalStock.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPURCHASEORDER = RestCaller
				.MenuConfigMstByMenuName("PURCHASE ORDER");
		List<MenuConfigMst> menuConfigMstPURCHASEORDERList = menuConfigMstPURCHASEORDER.getBody();

		if (menuConfigMstPURCHASEORDERList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE ORDER");
			menuConfigMst.setMenuFxml("PurchaseOrder.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPURCHASEORDERMANAGEMENT = RestCaller
				.MenuConfigMstByMenuName("PURCHASE ORDER MANAGEMENT");
		List<MenuConfigMst> menuConfigMstPURCHASEORDERMANAGEMENTList = menuConfigMstPURCHASEORDERMANAGEMENT.getBody();

		if (menuConfigMstPURCHASEORDERMANAGEMENTList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE ORDER MANAGEMENT");
			menuConfigMst.setMenuFxml("PurchaseOrderManagement.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstOTHERBRANCHSALE = RestCaller
				.MenuConfigMstByMenuName("OTHER BRANCH SALE");
		List<MenuConfigMst> menuConfigMstOTHERBRANCHSALEList = menuConfigMstOTHERBRANCHSALE.getBody();

		if (menuConfigMstOTHERBRANCHSALEList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OTHER BRANCH SALE");
			menuConfigMst.setMenuFxml("OtherBranchSalePage.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstUSERREGISTEREDSTOCK = RestCaller
				.MenuConfigMstByMenuName("USER REGISTERED STOCK");
		List<MenuConfigMst> menuConfigMstUSERREGISTEREDSTOCKList = menuConfigMstUSERREGISTEREDSTOCK.getBody();

		if (menuConfigMstUSERREGISTEREDSTOCKList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("USER REGISTERED STOCK");
			menuConfigMst.setMenuFxml("UserRegisteredStock.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSTORECHANGE = RestCaller
				.MenuConfigMstByMenuName("STORE CHANGE");
		List<MenuConfigMst> menuConfigMstSTORECHANGEList = menuConfigMstSTORECHANGE.getBody();

		if (menuConfigMstSTORECHANGEList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STORE CHANGE");
			menuConfigMst.setMenuFxml("StoreChange.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstITEMLOCATION = RestCaller
				.MenuConfigMstByMenuName("ITEM LOCATION");
		List<MenuConfigMst> menuConfigMstITEMLOCATIONList = menuConfigMstITEMLOCATION.getBody();

		if (menuConfigMstITEMLOCATIONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM LOCATION");
			menuConfigMst.setMenuFxml("ItemLocation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSALESRETURN = RestCaller
				.MenuConfigMstByMenuName("SALES RETURN");
		List<MenuConfigMst> menuConfigMstSALESRETURNList = menuConfigMstSALESRETURN.getBody();

		if (menuConfigMstSALESRETURNList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES RETURN");
			menuConfigMst.setMenuFxml("SalesReturn.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSALESFROMORDER = RestCaller
				.MenuConfigMstByMenuName("SALES FROM ORDER");
		List<MenuConfigMst> menuConfigMstSALESFROMORDERList = menuConfigMstSALESFROMORDER.getBody();

		if (menuConfigMstSALESFROMORDERList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES FROM ORDER");
			menuConfigMst.setMenuFxml("SalesFromOrder.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigMstSTOCKVERIFICATION = RestCaller
				.MenuConfigMstByMenuName("STOCK VERIFICATION");
		List<MenuConfigMst> menuConfigMstSTOCKVERIFICATIONList = menuConfigMstSTOCKVERIFICATION.getBody();

		if (menuConfigMstSTOCKVERIFICATIONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK VERIFICATION");
			menuConfigMst.setMenuFxml("StockVerification.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPURCHASESCHEME = RestCaller
				.MenuConfigMstByMenuName("PURCHASE SCHEME");
		List<MenuConfigMst> menuConfigMstPURCHASESCHEMEList = menuConfigMstPURCHASESCHEME.getBody();

		if (menuConfigMstPURCHASESCHEMEList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE SCHEME");
			menuConfigMst.setMenuFxml("PurchaseScheme.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSALESPROPERTIESCONFIGURATION = RestCaller
				.MenuConfigMstByMenuName("SALES PROPERTIES CONFIGURATION");
		List<MenuConfigMst> menuConfigMstSALESPROPERTIESCONFIGURATIONList = menuConfigMstSALESPROPERTIESCONFIGURATION
				.getBody();

		if (menuConfigMstSALESPROPERTIESCONFIGURATIONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES PROPERTIES CONFIGURATION");
			menuConfigMst.setMenuFxml("SalesProperties.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstINTENTTOSTOCKTRANSFER = RestCaller
				.MenuConfigMstByMenuName("INTENT TO STOCK TRANSFER");
		List<MenuConfigMst> menuConfigMstINTENTTOSTOCKTRANSFERList = menuConfigMstINTENTTOSTOCKTRANSFER.getBody();

		if (menuConfigMstINTENTTOSTOCKTRANSFERList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INTENT TO STOCK TRANSFER");
			menuConfigMst.setMenuFxml("IntentStockTransfer.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstLMS_QUEUEPENDINGTASK = RestCaller
				.MenuConfigMstByMenuName("LMS_QUEUE PENDING TASK");
		List<MenuConfigMst> menuConfigMstLMS_QUEUEPENDINGTASKList = menuConfigMstLMS_QUEUEPENDINGTASK.getBody();

		if (menuConfigMstLMS_QUEUEPENDINGTASKList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("LMS_QUEUE PENDING TASK");
			menuConfigMst.setMenuFxml("LmsQueuePendingPopUp.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSESSIONEND = RestCaller.MenuConfigMstByMenuName("SESSION END");
		List<MenuConfigMst> menuConfigMstSESSIONENDList = menuConfigMstSESSIONEND.getBody();

		if (menuConfigMstSESSIONENDList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SESSION END");
			menuConfigMst.setMenuFxml("SessionEnd.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPRODUCTCONVERSION = RestCaller
				.MenuConfigMstByMenuName("PRODUCT CONVERSION");
		List<MenuConfigMst> menuConfigMstPRODUCTCONVERSIONList = menuConfigMstPRODUCTCONVERSION.getBody();

		if (menuConfigMstPRODUCTCONVERSIONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCT CONVERSION");
			menuConfigMst.setMenuFxml("ProductionConversion.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstLANGUAGEMST = RestCaller
				.MenuConfigMstByMenuName("LANGUAGE MST");
		List<MenuConfigMst> menuConfigMstLANGUAGEMSTList = menuConfigMstLANGUAGEMST.getBody();

		if (menuConfigMstLANGUAGEMSTList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("LANGUAGE MST");
			menuConfigMst.setMenuFxml("LanguageMstCtl.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstITEMINLANGUAGE = RestCaller
				.MenuConfigMstByMenuName("ITEM IN LANGUAGE");
		List<MenuConfigMst> menuConfigMstITEMINLANGUAGEList = menuConfigMstITEMINLANGUAGE.getBody();

		if (menuConfigMstITEMINLANGUAGEList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM IN LANGUAGE");
			menuConfigMst.setMenuFxml("ItemLangEntry.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstBARGRAPH = RestCaller.MenuConfigMstByMenuName("BAR GRAPH");
		List<MenuConfigMst> menuConfigMstBARGRAPHList = menuConfigMstBARGRAPH.getBody();

		if (menuConfigMstBARGRAPHList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BAR GRAPH");
			menuConfigMst.setMenuFxml("sampleBarGraph.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPRODUCTIONVALUEGRAPH = RestCaller
				.MenuConfigMstByMenuName("PRODUCTION VALUE GRAPH");
		List<MenuConfigMst> menuConfigMstPRODUCTIONVALUEGRAPHList = menuConfigMstPRODUCTIONVALUEGRAPH.getBody();

		if (menuConfigMstPRODUCTIONVALUEGRAPHList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCTION VALUE GRAPH");
			menuConfigMst.setMenuFxml("ProductionBarGraph.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPRODUCTIONQTYGRAPH = RestCaller
				.MenuConfigMstByMenuName("PRODUCTION QTY GRAPH");
		List<MenuConfigMst> menuConfigMstPRODUCTIONQTYGRAPHList = menuConfigMstPRODUCTIONQTYGRAPH.getBody();

		if (menuConfigMstPRODUCTIONQTYGRAPHList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCTION QTY GRAPH");
			menuConfigMst.setMenuFxml("ProductionQtyGraph.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSETANIMAGE = RestCaller
				.MenuConfigMstByMenuName("SET AN IMAGE");
		List<MenuConfigMst> menuConfigMstSETANIMAGEList = menuConfigMstSETANIMAGE.getBody();

		if (menuConfigMstSETANIMAGEList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SET AN IMAGE");
			menuConfigMst.setMenuFxml("ItemImage.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstTALLYINTEGRATION = RestCaller
				.MenuConfigMstByMenuName("TALLY INTEGRATION");
		List<MenuConfigMst> menuConfigMstTALLYINTEGRATIONList = menuConfigMstTALLYINTEGRATION.getBody();

		if (menuConfigMstTALLYINTEGRATIONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TALLY INTEGRATION");
			menuConfigMst.setMenuFxml("TallyIntegration.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSTOCKCORRECTION = RestCaller
				.MenuConfigMstByMenuName("STOCK CORRECTION");
		List<MenuConfigMst> menuConfigMstSTOCKCORRECTIONList = menuConfigMstSTOCKCORRECTION.getBody();

		if (menuConfigMstSTOCKCORRECTIONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK CORRECTION");
			menuConfigMst.setMenuFxml("StockCorrection.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstRETRY = RestCaller.MenuConfigMstByMenuName("RETRY");
		List<MenuConfigMst> menuConfigMstRETRYList = menuConfigMstRETRY.getBody();

		if (menuConfigMstRETRYList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RETRY");
			menuConfigMst.setMenuFxml("PostToServerRetry.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstVOUCHERDATEEDIT = RestCaller
				.MenuConfigMstByMenuName("VOUCHER DATE EDIT");
		List<MenuConfigMst> menuConfigMstVOUCHERDATEEDITList = menuConfigMstVOUCHERDATEEDIT.getBody();

		if (menuConfigMstVOUCHERDATEEDITList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("VOUCHER DATE EDIT");
			menuConfigMst.setMenuFxml("VoucherDateEdit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSUBBRANCHSALE = RestCaller
				.MenuConfigMstByMenuName("SUB BRANCH SALE");
		List<MenuConfigMst> menuConfigMstSUBBRANCHSALEList = menuConfigMstSUBBRANCHSALE.getBody();

		if (menuConfigMstSUBBRANCHSALEList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUB BRANCH SALE");
			menuConfigMst.setMenuFxml("SubBranchSale.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSSALESMANCREATION = RestCaller
				.MenuConfigMstByMenuName("SALES MAN CREATION");
		List<MenuConfigMst> menuConfigMstSSALESMANCREATIONList = menuConfigMstSSALESMANCREATION.getBody();

		if (menuConfigMstSSALESMANCREATIONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES MAN CREATION");
			menuConfigMst.setMenuFxml("SalesManCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		// Accounts Tree

		ResponseEntity<List<MenuConfigMst>> menuConfigMstOPENINGBALANCECONFIGURATIONS = RestCaller
				.MenuConfigMstByMenuName("OPENING BALANCE CONFIGURATIONS");
		List<MenuConfigMst> menuConfigMstOPENINGBALANCECONFIGURATIONSList = menuConfigMstOPENINGBALANCECONFIGURATIONS
				.getBody();

		if (menuConfigMstOPENINGBALANCECONFIGURATIONSList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OPENING BALANCE CONFIGURATIONS");
			menuConfigMst.setMenuFxml("OpeningBalanceConfig.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCUSTOMERBILLWISEADJUSTMENT = RestCaller
				.MenuConfigMstByMenuName("CUSTOMER BILLWISE ADJUSTMENT");
		List<MenuConfigMst> menuConfigMstCUSTOMERBILLWISEADJUSTMENTList = menuConfigMstCUSTOMERBILLWISEADJUSTMENT
				.getBody();

		if (menuConfigMstCUSTOMERBILLWISEADJUSTMENTList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOMER BILLWISE ADJUSTMENT");
			menuConfigMst.setMenuFxml("Receiptwindow.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPETTYCASHRECIEPTS = RestCaller
				.MenuConfigMstByMenuName("PETTY-CASH RECIEPTS");
		List<MenuConfigMst> menuConfigMstPETTYCASHRECIEPTSList = menuConfigMstPETTYCASHRECIEPTS.getBody();

		if (menuConfigMstPETTYCASHRECIEPTSList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PETTY-CASH RECIEPTS");
			menuConfigMst.setMenuFxml("PettyCashReciept.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstRECIEPTS = RestCaller
				.MenuConfigMstByMenuName("RECEIPT WINDOW");
		List<MenuConfigMst> menuConfigMstRECIEPTSList = menuConfigMstRECIEPTS.getBody();

		if (menuConfigMstRECIEPTSList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RECEIPT WINDOW");
			menuConfigMst.setMenuFxml("OtherReciepts.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstJOURNAL = RestCaller.MenuConfigMstByMenuName("JOURNAL");
		List<MenuConfigMst> menuConfigMstJOURNALList = menuConfigMstJOURNAL.getBody();

		if (menuConfigMstJOURNALList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("JOURNAL");
			menuConfigMst.setMenuFxml("Journal.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSUPPLIERBILLWISEADJUSTMENT = RestCaller
				.MenuConfigMstByMenuName("SUPPLIER PAYMENTS");
		List<MenuConfigMst> menuConfigMstSUPPLIERBILLWISEADJUSTMENTList = menuConfigMstSUPPLIERBILLWISEADJUSTMENT
				.getBody();

		if (menuConfigMstSUPPLIERBILLWISEADJUSTMENTList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUPPLIER PAYMENTS");
			menuConfigMst.setMenuFxml("SupplierPaymentWindowNew.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPETTYCASHPAYMENTS = RestCaller
				.MenuConfigMstByMenuName("PETTY-CASH PAYMENTS");
		List<MenuConfigMst> menuConfigMstPETTYCASHPAYMENTSList = menuConfigMstPETTYCASHPAYMENTS.getBody();

		if (menuConfigMstPETTYCASHPAYMENTSList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PETTY-CASH PAYMENTS");
			menuConfigMst.setMenuFxml("PettyCashPayment.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPAYMENTS = RestCaller.MenuConfigMstByMenuName("PAYMENT");
		List<MenuConfigMst> menuConfigMstPAYMENTSList = menuConfigMstPAYMENTS.getBody();

		if (menuConfigMstPAYMENTSList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PAYMENT");
			menuConfigMst.setMenuFxml("OtherPayments.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstMultiUnitRep = RestCaller
				.MenuConfigMstByMenuName("MULTI UNIT");
		List<MenuConfigMst> menuConfigMstMultiUnitList = menuConfigMstMultiUnitRep.getBody();

		if (menuConfigMstMultiUnitList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MULTI UNIT");
			menuConfigMst.setMenuFxml("MultiUnit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuOWNACCOUNTSETTLEMENT = RestCaller
				.MenuConfigMstByMenuName("OWN ACCOUNT SETTLEMENT");
		List<MenuConfigMst> mmenuOWNACCOUNTSETTLEMENTList = menuOWNACCOUNTSETTLEMENT.getBody();

		if (mmenuOWNACCOUNTSETTLEMENTList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OWN ACCOUNT SETTLEMENT");
			menuConfigMst.setMenuFxml("OwnAccountSettlement.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPDCRECEIPT = RestCaller.MenuConfigMstByMenuName("PDC RECEIPT");
		List<MenuConfigMst> menuPDCRECEIPTList = menuPDCRECEIPT.getBody();

		if (menuPDCRECEIPTList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PDC RECEIPT");
			menuConfigMst.setMenuFxml("PDCReceipts.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPDCPAYMENT = RestCaller.MenuConfigMstByMenuName("PDC PAYMENT");
		List<MenuConfigMst> menuPDCPAYMENTList = menuPDCPAYMENT.getBody();

		if (menuPDCPAYMENTList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PDC PAYMENT");
			menuConfigMst.setMenuFxml("PDCPayment.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPDCRECONCILE = RestCaller.MenuConfigMstByMenuName("PDC RECONCILE");
		List<MenuConfigMst> menuPDCRECONCILEList = menuPDCRECONCILE.getBody();

		if (menuPDCRECONCILEList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PDC RECONCILE");
			menuConfigMst.setMenuFxml("PDCReconsile.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuRECEIPTEDIT = RestCaller.MenuConfigMstByMenuName("RECEIPT EDIT");
		List<MenuConfigMst> menuRECEIPTEDITList = menuRECEIPTEDIT.getBody();

		if (menuRECEIPTEDITList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RECEIPT EDIT");
			menuConfigMst.setMenuFxml("ReceiptEdit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPAYMENTEDIT = RestCaller.MenuConfigMstByMenuName("PAYMENT EDIT");
		List<MenuConfigMst> menuPAYMENTEDITList = menuPAYMENTEDIT.getBody();

		if (menuPAYMENTEDITList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PAYMENT EDIT");
			menuConfigMst.setMenuFxml("PaymentEdit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuJOURNALEDIT = RestCaller.MenuConfigMstByMenuName("JOURNAL EDIT");
		List<MenuConfigMst> menuJOURNALEDITList = menuJOURNALEDIT.getBody();

		if (menuJOURNALEDITList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("JOURNAL EDIT");
			menuConfigMst.setMenuFxml("JournalEdit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuOPENINGBALANCEENTRY = RestCaller
				.MenuConfigMstByMenuName("OPENING BALANCE ENTRY");
		List<MenuConfigMst> menuOPENINGBALANCEENTRYList = menuOPENINGBALANCEENTRY.getBody();

		if (menuOPENINGBALANCEENTRYList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OPENING BALANCE ENTRY");
			menuConfigMst.setMenuFxml("OpeningBalanceCtl.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuACCOUNTsLUPDATION = RestCaller
				.MenuConfigMstByMenuName("ACCOUNT SL.NO UPDATION");
		List<MenuConfigMst> menuACCOUNTsLUPDATIONList = menuACCOUNTsLUPDATION.getBody();

		if (menuACCOUNTsLUPDATIONList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCOUNT SL.NO UPDATION");
			menuConfigMst.setMenuFxml("AccountSerialNumberUpdation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuDayBookResp = RestCaller.MenuConfigMstByMenuName("DAY BOOK REPORT");
		List<MenuConfigMst> menuDayBookList = menuDayBookResp.getBody();

		if (menuDayBookList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAY BOOK REPORT");
			menuConfigMst.setMenuFxml("DayBookReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuHsnCodeSaleReportResp = RestCaller
				.MenuConfigMstByMenuName("HSN CODE SALE REPORT");
		List<MenuConfigMst> menuHsnCodeSaleReportList = menuHsnCodeSaleReportResp.getBody();

		if (menuHsnCodeSaleReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("HSN CODE SALE REPORT");
			menuConfigMst.setMenuFxml("HSNCodeSaleReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuReceiptModeWiseReportResp = RestCaller
				.MenuConfigMstByMenuName("RECEIPT MODE WISE REPORT");
		List<MenuConfigMst> menuReceiptModeWiseReportList = menuReceiptModeWiseReportResp.getBody();

		if (menuReceiptModeWiseReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RECEIPT MODE WISE REPORT");
			menuConfigMst.setMenuFxml("ReceiptModeWiseReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuGroupWiseSalesReportResp = RestCaller
				.MenuConfigMstByMenuName("GROUP WISE SALES REPORT PHARMACY");
		List<MenuConfigMst> menuGroupWiseSalesReportList = menuGroupWiseSalesReportResp.getBody();

		if (menuGroupWiseSalesReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("GROUP WISE SALES REPORT PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyGroupWiseSales.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuGroupWiseSalesDetailReportResp = RestCaller
				.MenuConfigMstByMenuName("GROUP WISE SALES DETAIL REPORT PHARMACY");
		List<MenuConfigMst> menuGroupWiseSalesDetailReportList = menuGroupWiseSalesDetailReportResp.getBody();

		if (menuGroupWiseSalesDetailReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("GROUP WISE SALES DETAIL REPORT PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyGropWiseSalesDetailReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuItemOrCategorySalesReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEM OR CATEGORY SALE REPORT");
		List<MenuConfigMst> menuItemOrCategoryReportList = menuItemOrCategorySalesReportResp.getBody();

		if (menuItemOrCategoryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM OR CATEGORY SALE REPORT");
			menuConfigMst.setMenuFxml("ItemOrCategoryViseReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuSalesSummaryReportResp = RestCaller
				.MenuConfigMstByMenuName("SALE SUMMARY REPORT");
		List<MenuConfigMst> menuSalesSummaryReportList = menuSalesSummaryReportResp.getBody();

		if (menuSalesSummaryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALE SUMMARY REPORT");
			menuConfigMst.setMenuFxml("SaleSummary.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuSalesModeWiseSummaryReportResp = RestCaller
				.MenuConfigMstByMenuName("SALES MODEWISE SUMMARY REPORT");
		List<MenuConfigMst> menuSalesModeWiseSummaryReportList = menuSalesModeWiseSummaryReportResp.getBody();

		if (menuSalesModeWiseSummaryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES MODEWISE SUMMARY REPORT");
			menuConfigMst.setMenuFxml("SalesModeWiseSummaryReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuB2CSalesSummaryReportResp = RestCaller
				.MenuConfigMstByMenuName("B2C SALES REPORT");
		List<MenuConfigMst> menuB2CSalesSummaryReportList = menuB2CSalesSummaryReportResp.getBody();

		if (menuB2CSalesSummaryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("B2C SALES REPORT");
			menuConfigMst.setMenuFxml("salesreport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuB2BSalesSummaryReportResp = RestCaller
				.MenuConfigMstByMenuName("B2B SALES REPORT");
		List<MenuConfigMst> menuB2BSalesSummaryReportList = menuB2BSalesSummaryReportResp.getBody();

		if (menuB2BSalesSummaryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("B2B SALES REPORT");
			menuConfigMst.setMenuFxml("B2BSalesReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuDamageEntryReportResp = RestCaller
				.MenuConfigMstByMenuName("DAMAGE ENTRY REPORT");
		List<MenuConfigMst> menuDamageEntryReportList = menuDamageEntryReportResp.getBody();

		if (menuDamageEntryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAMAGE ENTRY REPORT");
			menuConfigMst.setMenuFxml("DamageEntryReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuDamegeWiseProfitResp = RestCaller
				.MenuConfigMstByMenuName("BRANCH WISE PROFIT");
		List<MenuConfigMst> menuDamegeWiseProfitList = menuDamegeWiseProfitResp.getBody();

		if (menuDamegeWiseProfitList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BRANCH WISE PROFIT");
			menuConfigMst.setMenuFxml("BranchWiseProfit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

//		ResponseEntity<List<MenuConfigMst>> menuDamegeWiseProfitResp= RestCaller.MenuConfigMstByMenuName("BRANCH WISE PROFIT");
//		List<MenuConfigMst> menuDamegeWiseProfitList =  menuDamegeWiseProfitResp.getBody();
//		
//		if(menuDamegeWiseProfitList.size() == 0) 
//		{
//			MenuConfigMst menuConfigMst = new MenuConfigMst();
//			menuConfigMst.setMenuName("BRANCH WISE PROFIT");
//			menuConfigMst.setMenuFxml("BranchWiseProfit.fxml");
//			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
//			
//			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
//		}

		ResponseEntity<List<MenuConfigMst>> menuConfigPrinter = RestCaller.MenuConfigMstByMenuName("PRINTER");
		List<MenuConfigMst> menuConfigPrinterList = menuConfigPrinter.getBody();

		if (menuConfigPrinterList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRINTER");
			menuConfigMst.setMenuFxml("PrinterCtl.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigChangePassword = RestCaller
				.MenuConfigMstByMenuName("CHANGE PASSWORD");
		List<MenuConfigMst> menuConfigChangePasswordList = menuConfigChangePassword.getBody();

		if (menuConfigChangePasswordList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CHANGE PASSWORD");
			menuConfigMst.setMenuFxml("passwordchange.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigUseGroupPermission = RestCaller
				.MenuConfigMstByMenuName("USER GROUP PERMISSION");
		List<MenuConfigMst> menuConfigUseGroupPermissionList = menuConfigUseGroupPermission.getBody();

		if (menuConfigUseGroupPermissionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CHANGE PASSWORD");
			menuConfigMst.setMenuFxml("usergrouppermission.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigGroupPermission = RestCaller
				.MenuConfigMstByMenuName("GROUP PERMISSION");
		List<MenuConfigMst> menuConfigGroupPermissionList = menuConfigGroupPermission.getBody();

		if (menuConfigChangePasswordList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("GROUP PERMISSION");
			menuConfigMst.setMenuFxml("grouppermission.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigGroupCreation = RestCaller
				.MenuConfigMstByMenuName("GROUP CREATION");
		List<MenuConfigMst> menuConfigGroupCreationList = menuConfigGroupCreation.getBody();

		if (menuConfigGroupCreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("GROUP CREATION");
			menuConfigMst.setMenuFxml("groupcreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}

		ResponseEntity<List<MenuConfigMst>> menuConfigProcessPermission = RestCaller
				.MenuConfigMstByMenuName("PROCESS PERMISSION");
		List<MenuConfigMst> menuConfigProcessPermissionList = menuConfigProcessPermission.getBody();

		if (menuConfigProcessPermissionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PROCESS PERMISSION");
			menuConfigMst.setMenuFxml("processpermission.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}

		ResponseEntity<List<MenuConfigMst>> menuConfigProcessCreation = RestCaller
				.MenuConfigMstByMenuName("PROCESS CREATION");
		List<MenuConfigMst> menuConfigProcessCreationList = menuConfigProcessCreation.getBody();

		if (menuConfigProcessCreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PROCESS CREATION");
			menuConfigMst.setMenuFxml("processCreationNew.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMultyCategoryUpdation = RestCaller
				.MenuConfigMstByMenuName("MULTI CATEGORY UPDATION");
		List<MenuConfigMst> menuConfigMultyCategoryUpdationList = menuConfigMultyCategoryUpdation.getBody();

		if (menuConfigMultyCategoryUpdationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MULTY CATEGORY UPDATION");
			menuConfigMst.setMenuFxml("MultiCategoryUpdation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigNachineResource = RestCaller
				.MenuConfigMstByMenuName("MACHINE RESOUCE");
		List<MenuConfigMst> menuConfigNachineResourceList = menuConfigNachineResource.getBody();

		if (menuConfigNachineResourceList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MACHINE RESOUCE");
			menuConfigMst.setMenuFxml("MachineResource.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigResourceCategory = RestCaller
				.MenuConfigMstByMenuName("RESOUCE CATEGORY");
		List<MenuConfigMst> menuConfigResourceCategoryList = menuConfigResourceCategory.getBody();

		if (menuConfigResourceCategoryList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RESOUCE CATEGORY");
			menuConfigMst.setMenuFxml("SubCategory.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigResourceCategoryLink = RestCaller
				.MenuConfigMstByMenuName("RESOUCE CATEGORY LINK");
		List<MenuConfigMst> menuConfigResourceCategoryLinkList = menuConfigResourceCategoryLink.getBody();

		if (menuConfigResourceCategoryLinkList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RESOUCE CATEGORY CATEGORY");
			menuConfigMst.setMenuFxml("ResourceCatItemLink.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}

		ResponseEntity<List<MenuConfigMst>> menuConfigUserRegistration = RestCaller
				.MenuConfigMstByMenuName("USER REGISTRATION");
		List<MenuConfigMst> menuConfigUserRegistrationList = menuConfigUserRegistration.getBody();

		if (menuConfigUserRegistrationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("USER REGISTRATION");
			menuConfigMst.setMenuFxml("userregistration.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}

		ResponseEntity<List<MenuConfigMst>> menuConfigAddAccountHeads = RestCaller
				.MenuConfigMstByMenuName("ADD ACCOUNT HEADS");
		List<MenuConfigMst> menuConfigAddAccountHeadsList = menuConfigAddAccountHeads.getBody();

		if (menuConfigAddAccountHeadsList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ADD ACCOUNT HEADS");
			menuConfigMst.setMenuFxml("AccountHead.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigBranchCreation = RestCaller
				.MenuConfigMstByMenuName("BRANCH CREATION");
		List<MenuConfigMst> menuConfigBranchCreationList = menuConfigBranchCreation.getBody();

		if (menuConfigBranchCreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BRANCH CREATION");
			menuConfigMst.setMenuFxml("branchmst.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		

		ResponseEntity<List<MenuConfigMst>> menuConfigFinancialYear = RestCaller
				.MenuConfigMstByMenuName("FINANCIAL YEAR");
		List<MenuConfigMst> menuConfigFinancialYearList = menuConfigFinancialYear.getBody();

		if (menuConfigFinancialYearList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("FINANCIAL YEAR");
			menuConfigMst.setMenuFxml("FinancialYear.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigCompanyCreation = RestCaller
				.MenuConfigMstByMenuName("COMPANY CREATION");
		List<MenuConfigMst> menuConfigCompanyCreationList = menuConfigCompanyCreation.getBody();

		if (menuConfigFinancialYearList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("COMPANY CREATION");
			menuConfigMst.setMenuFxml("CompanyCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}

		ResponseEntity<List<MenuConfigMst>> menuConfigDbInitialize = RestCaller
				.MenuConfigMstByMenuName("DB INITIALIZE");
		List<MenuConfigMst> menuConfigDbInitializeList = menuConfigDbInitialize.getBody();

		if (menuConfigFinancialYearList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DB INITIALIZE");
			menuConfigMst.setMenuFxml("DBinitialization.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigeExecutesql = RestCaller.MenuConfigMstByMenuName("EXECUTE SQL");
		List<MenuConfigMst> menuConfigeExecutesqlList = menuConfigeExecutesql.getBody();

		if (menuConfigeExecutesqlList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("EXECUTE SQL");
			menuConfigMst.setMenuFxml("SqlExecutionWindow.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemMaster = RestCaller.MenuConfigMstByMenuName("ITEM MASTER");
		List<MenuConfigMst> menuConfigItemMstList = menuConfigMstItemMaster.getBody();

		if (menuConfigItemMstList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM MASTER");
			menuConfigMst.setMenuFxml("itemcreate1000x600.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemEdit = RestCaller.MenuConfigMstByMenuName("ITEM EDIT");
		List<MenuConfigMst> menuConfigMstItemList = menuConfigMstItemEdit.getBody();

		if (menuConfigMstItemList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM EDIT");
			menuConfigMst.setMenuFxml("itemEdit1000x600.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCategoryMngt = RestCaller
				.MenuConfigMstByMenuName("CATEGORY MANAGEMENT");
		List<MenuConfigMst> menuConfigMstCategoryMngtList = menuConfigMstCategoryMngt.getBody();

		if (menuConfigMstCategoryMngtList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CATEGORY MANAGEMENT");
			menuConfigMst.setMenuFxml("CategoryManagement.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstCheckPrint = RestCaller.MenuConfigMstByMenuName("CHECK PRINT");
		List<MenuConfigMst> menuConfigMstCheckPrintList = menuConfigMstCheckPrint.getBody();

		if (menuConfigMstCheckPrintList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CHECK PRINT");
			menuConfigMst.setMenuFxml("CheckPrint.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstTaxcreation = RestCaller
				.MenuConfigMstByMenuName("TAX CREATION");
		List<MenuConfigMst> menuConfigMstTaxcreationList = menuConfigMstTaxcreation.getBody();

		if (menuConfigMstTaxcreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TAX CREATION");
			menuConfigMst.setMenuFxml("tax-1000x600.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemWiseTaxCreation = RestCaller
				.MenuConfigMstByMenuName("ITEMWISE TAX CREATION");
		List<MenuConfigMst> menuConfigMstItemWiseTaxCreationList = menuConfigMstItemWiseTaxCreation.getBody();

		if (menuConfigMstItemWiseTaxCreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEMWISE TAX CREATION");
			menuConfigMst.setMenuFxml("ItemWiseTaxCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstPublishMessage = RestCaller
				.MenuConfigMstByMenuName("PUBLISH MESSAGE");
		List<MenuConfigMst> menuConfigMstPublishMessageList = menuConfigMstPublishMessage.getBody();

		if (menuConfigMstPublishMessageList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PUBLISH MESSAGE");
			menuConfigMst.setMenuFxml("publishMessage.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstKitDefinition = RestCaller
				.MenuConfigMstByMenuName("KIT DEFINITION");
		List<MenuConfigMst> menuConfigMstKitDefinitionList = menuConfigMstKitDefinition.getBody();

		if (menuConfigMstKitDefinitionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("KIT DEFINITION");
			menuConfigMst.setMenuFxml("KitDefinition.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstKotManager = RestCaller.MenuConfigMstByMenuName("KOT MANAGER");
		List<MenuConfigMst> menuConfigMstKotManagerList = menuConfigMstKotManager.getBody();

		if (menuConfigMstKotManagerList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("KOT MANAGER");
			menuConfigMst.setMenuFxml("kotmanagement.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstStoreCreation = RestCaller
				.MenuConfigMstByMenuName("STORE CREATION");
		List<MenuConfigMst> menuConfigMstStoreCreationList = menuConfigMstStoreCreation.getBody();

		if (menuConfigMstStoreCreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STORE CREATION");
			menuConfigMst.setMenuFxml("StoreCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstDayEnd = RestCaller.MenuConfigMstByMenuName("DAY END");
		List<MenuConfigMst> menuConfigMstDayEndList = menuConfigMstDayEnd.getBody();

		if (menuConfigMstDayEndList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAY END");
			menuConfigMst.setMenuFxml("DayEnd.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstSiteCreation = RestCaller
				.MenuConfigMstByMenuName("SITE CREATION");
		List<MenuConfigMst> menuConfigMstSiteCreationList = menuConfigMstSiteCreation.getBody();

		if (menuConfigMstSiteCreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SITE CREATION");
			menuConfigMst.setMenuFxml("SiteCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigMstFinanceCreation = RestCaller
				.MenuConfigMstByMenuName("FINANCE CREATION");
		List<MenuConfigMst> menuConfigMstFinanceCreationList = menuConfigMstFinanceCreation.getBody();

		if (menuConfigMstFinanceCreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("FINANCE CREATION");
			menuConfigMst.setMenuFxml("financeCmpReg.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigMstBarCodePrinting = RestCaller
				.MenuConfigMstByMenuName("BARCODE PRINTING");
		List<MenuConfigMst> menuConfigMstBarCodePrintingList = menuConfigMstBarCodePrinting.getBody();

		if (menuConfigMstBarCodePrintingList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BARCODE PRINTING");
			menuConfigMst.setMenuFxml("BarcodePrint.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstBarCodeConfiguration = RestCaller
				.MenuConfigMstByMenuName("BARCODE CONFIGURATION");
		List<MenuConfigMst> menuConfigMstBarCodeConfigurationList = menuConfigMstBarCodeConfiguration.getBody();

		if (menuConfigMstBarCodeConfigurationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BARCODE CONFIGURATION");
			menuConfigMst.setMenuFxml("BarcodeConfig.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemMerge = RestCaller.MenuConfigMstByMenuName("ITEM MERGE");
		List<MenuConfigMst> menuConfigMstItemMergeList = menuConfigMstItemMerge.getBody();

		if (menuConfigMstItemMergeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM MERGE");
			menuConfigMst.setMenuFxml("ItemMerge.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigMstSaleOrderStatus = RestCaller
				.MenuConfigMstByMenuName("SALE ORDER STATUS");
		List<MenuConfigMst> menuConfigMstSaleOrderStatusList = menuConfigMstSaleOrderStatus.getBody();

		if (menuConfigMstSaleOrderStatusList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALE ORDER STATUS");
			menuConfigMst.setMenuFxml("SaleOrderStatusCheck.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstReorder = RestCaller.MenuConfigMstByMenuName("RE-ORDER");
		List<MenuConfigMst> menuConfigMstReorderList = menuConfigMstReorder.getBody();

		if (menuConfigMstReorderList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RE-ORDER");
			menuConfigMst.setMenuFxml("Reorder.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigMstItemBranch = RestCaller.MenuConfigMstByMenuName("ITEM BRANCH");
		List<MenuConfigMst> menuConfigMstItemBranchList = menuConfigMstItemBranch.getBody();

		if (menuConfigMstItemBranchList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM BRANCH");
			menuConfigMst.setMenuFxml("ItemBranch.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigMstMultyUnit = RestCaller.MenuConfigMstByMenuName("MULTY UNIT");
		List<MenuConfigMst> menuConfigMstMultyUnitList = menuConfigMstMultyUnit.getBody();

		if (menuConfigMstMultyUnitList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MULTY UNIT");
			menuConfigMst.setMenuFxml("MultiUnit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigProductConversionConfiguration = RestCaller
				.MenuConfigMstByMenuName("PRODUCT CONVERSION CONFIGURATION");
		List<MenuConfigMst> menuConfigProductConversionConfigurationList = menuConfigProductConversionConfiguration
				.getBody();

		if (menuConfigProductConversionConfigurationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCT CONVERSION CONFIGURATION");
			menuConfigMst.setMenuFxml("ProductConversionConfiguration.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigCategoryException = RestCaller
				.MenuConfigMstByMenuName("CATEGORY EXCEPTION LIST");
		List<MenuConfigMst> menuConfigCategoryExceptionList = menuConfigCategoryException.getBody();

		if (menuConfigCategoryExceptionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CATEGORY EXCEPTION LIST");
			menuConfigMst.setMenuFxml("CategoryExceptionList.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigNutritionFacts = RestCaller
				.MenuConfigMstByMenuName("NUTRITION FACTS");
		List<MenuConfigMst> menuConfigNutritionFactsList = menuConfigNutritionFacts.getBody();

		if (menuConfigNutritionFactsList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("NUTRITION FACTS");
			menuConfigMst.setMenuFxml("NutritionFacts.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigNutritionPrint = RestCaller
				.MenuConfigMstByMenuName("NUTRITION PRINT");
		List<MenuConfigMst> menuConfigNutritionPrintList = menuConfigNutritionPrint.getBody();

		if (menuConfigNutritionPrintList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("NUTRITION PRINT");
			menuConfigMst.setMenuFxml("NutritionValue.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigBarcodeNutritionCombined = RestCaller
				.MenuConfigMstByMenuName("BARCODE NUTRRITION COMBINED");
		List<MenuConfigMst> menuConfigBarcodeNutritionCombinedList = menuConfigBarcodeNutritionCombined.getBody();

		if (menuConfigBarcodeNutritionCombinedList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BARCODE NUTRRITION COMBINED");
			menuConfigMst.setMenuFxml("BarcodeNutritionCombined.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigSchemeResp = RestCaller.MenuConfigMstByMenuName("SCHEME");
		List<MenuConfigMst> menuConfigSchemeList = menuConfigSchemeResp.getBody();

		if (menuConfigSchemeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SCHEME");
			menuConfigMst.setMenuFxml("SchemeMaster.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigSchemeDetails = RestCaller
				.MenuConfigMstByMenuName("SCHEME DETAILS");
		List<MenuConfigMst> menuConfigSchemeDetailsList = menuConfigSchemeDetails.getBody();

		if (menuConfigSchemeDetailsList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SCHEME DETAILS");
			menuConfigMst.setMenuFxml("SchemeDetails.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigSaleOrderEdit = RestCaller
				.MenuConfigMstByMenuName("SALEORDER EDIT");
		List<MenuConfigMst> menuConfigSaleOrderEditList = menuConfigSaleOrderEdit.getBody();

		if (menuConfigSaleOrderEditList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALEORDER EDIT");
			menuConfigMst.setMenuFxml("SaleOrderEdit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigTaxInvoiceByPriceType = RestCaller
				.MenuConfigMstByMenuName("TAX INVOICE BY PRICE TYPE");
		List<MenuConfigMst> menuConfigTaxInvoiceByPriceTypeList = menuConfigTaxInvoiceByPriceType.getBody();

		if (menuConfigTaxInvoiceByPriceTypeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TAX INVOICE BY PRICE TYPE");
			menuConfigMst.setMenuFxml("taxInvoiceByPriceType.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigAddSupplier = RestCaller.MenuConfigMstByMenuName("ADD SUPPLIER");
		List<MenuConfigMst> menuConfigAddSupplierList = menuConfigAddSupplier.getBody();

		if (menuConfigAddSupplierList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ADD SUPPLIER");
			menuConfigMst.setMenuFxml("Supplier-1000x600.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigBatchPriceDefinition = RestCaller
				.MenuConfigMstByMenuName("BATCH PRICE DEFINITION");
		List<MenuConfigMst> menuConfigBatchPriceDefinitionList = menuConfigBatchPriceDefinition.getBody();

		if (menuConfigBatchPriceDefinitionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BATCH PRICE DEFINITION");
			menuConfigMst.setMenuFxml("BatchPriceDefenition.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigPriceDefinition = RestCaller
				.MenuConfigMstByMenuName("PRICE DEFINITION");
		List<MenuConfigMst> menuConfigPriceDefinitionList = menuConfigPriceDefinition.getBody();

		if (menuConfigPriceDefinitionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRICE DEFINITION");
			menuConfigMst.setMenuFxml("PriceDefenition.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menuConfigProcessIntent = RestCaller
				.MenuConfigMstByMenuName("PROCESS INTENT");
		List<MenuConfigMst> menuConfigProcessIntentList = menuConfigProcessIntent.getBody();

		if (menuConfigProcessIntentList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PROCESS INTENT");
			menuConfigMst.setMenuFxml("ProcessIntent.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigVoucherType = RestCaller.MenuConfigMstByMenuName("VOUCHER TYPE");
		List<MenuConfigMst> menuConfigVoucherTypeList = menuConfigVoucherType.getBody();

		if (menuConfigVoucherTypeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("VOUCHER TYPE");
			menuConfigMst.setMenuFxml("saleTye.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigReceiptMode = RestCaller.MenuConfigMstByMenuName("RECEIPT MODE");
		List<MenuConfigMst> menuConfigReceiptModeList = menuConfigReceiptMode.getBody();

		if (menuConfigReceiptModeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RECEIPT MODE");
			menuConfigMst.setMenuFxml("ReceiptModeWindow.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigKitchenCategoryyDtl = RestCaller
				.MenuConfigMstByMenuName("KITCHEN CATEGORY DTL");
		List<MenuConfigMst> menuConfigKitchenCategoryyDtlList = menuConfigKitchenCategoryyDtl.getBody();

		if (menuConfigKitchenCategoryyDtlList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("KITCHEN CATEGORY DTL");
			menuConfigMst.setMenuFxml("KitchenCategoryDtl.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConfigKotCategoryPrinter = RestCaller
				.MenuConfigMstByMenuName("KOT CATEGORY PRINTER");
		List<MenuConfigMst> menuConfigKotCategoryPrinterList = menuConfigKotCategoryPrinter.getBody();

		if (menuConfigKotCategoryPrinterList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("KOT CATEGORY PRINTER");
			menuConfigMst.setMenuFxml("CategoryPrinter.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPendingItenetReportResp = RestCaller
				.MenuConfigMstByMenuName("PENDING INTENT REPORT");
		List<MenuConfigMst> menuPendingItenetReportList = menuPendingItenetReportResp.getBody();

		if (menuPendingItenetReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PENDING INTENT REPORT");
			menuConfigMst.setMenuFxml("intentReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuOtherBranchSalesReportResp = RestCaller
				.MenuConfigMstByMenuName("OTHER BRANCH SALES REPORT");
		List<MenuConfigMst> menuOtherBranchSalesReportList = menuOtherBranchSalesReportResp.getBody();

		if (menuOtherBranchSalesReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OTHER BRANCH SALES REPORT");
			menuConfigMst.setMenuFxml("BranchSalesReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuCustomerWiseSalesReportResp = RestCaller
				.MenuConfigMstByMenuName("CUSTOMERWISE SALES REPORT PHARMACY");
		List<MenuConfigMst> menuCustomerWiseSalesReportList = menuCustomerWiseSalesReportResp.getBody();

		if (menuCustomerWiseSalesReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOMERWISE SALES REPORT PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyCustomerwiseSalesReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuProductConversionReportResp = RestCaller
				.MenuConfigMstByMenuName("PRODUCT CONVERSION REPORT");
		List<MenuConfigMst> menuProductConversionReportList = menuProductConversionReportResp.getBody();

		if (menuProductConversionReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCT CONVERSION REPORT");
			menuConfigMst.setMenuFxml("ProductConversionReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuCustomerSalesReportResp = RestCaller
				.MenuConfigMstByMenuName("CUSTOMER SALES REPORT");
		List<MenuConfigMst> menuCustomerSalesReportList = menuCustomerSalesReportResp.getBody();

		if (menuCustomerSalesReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOMER SALES REPORT");
			menuConfigMst.setMenuFxml("CustomerInvoiceReports.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuCustomerBillWiseDueDayReportResp = RestCaller
				.MenuConfigMstByMenuName("CUSTOMER BILLWISE BY DUE DAYS");
		List<MenuConfigMst> menuCustomerBillWiseDueDayReportList = menuCustomerBillWiseDueDayReportResp.getBody();

		if (menuCustomerBillWiseDueDayReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOMER BILLWISE BY DUE DAYS");
			menuConfigMst.setMenuFxml("CustomerBillWiseByDueDays.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuItemWiseShortExpiryReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEMWISE SHORT EXPIRY REPORT");
		List<MenuConfigMst> menuItemWiseShortExpiryReportList = menuItemWiseShortExpiryReportResp.getBody();

		if (menuItemWiseShortExpiryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEMWISE SHORT EXPIRY REPORT");
			menuConfigMst.setMenuFxml("ItemWiseShortExpiryReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuItemOrServiceWiseProfitResp = RestCaller
				.MenuConfigMstByMenuName("ITEM OR SERVICE WISE PROFIT");
		List<MenuConfigMst> menuItemOrServiceWiseProfitList = menuItemOrServiceWiseProfitResp.getBody();

		if (menuItemOrServiceWiseProfitList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM OR SERVICE WISE PROFIT");
			menuConfigMst.setMenuFxml("ItemWiseProfit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuSalesReporyByPriceTypeResp = RestCaller
				.MenuConfigMstByMenuName("SALES REPORT BY PRICE TYPE");
		List<MenuConfigMst> menuSalesReporyByPriceTypeList = menuSalesReporyByPriceTypeResp.getBody();

		if (menuSalesReporyByPriceTypeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES REPORT BY PRICE TYPE");
			menuConfigMst.setMenuFxml("SalesReportByCustPriceType.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuInsuranceCompanySalesReportResp = RestCaller
				.MenuConfigMstByMenuName("INSURANCE COMPANY SALES REPORT");
		List<MenuConfigMst> menuInsuranceCompanySalesReportList = menuInsuranceCompanySalesReportResp.getBody();

		if (menuInsuranceCompanySalesReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INSURANCE COMPANY SALES REPORT");
			menuConfigMst.setMenuFxml("InsuranceCompanySalesSumaryReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuDailyOrderDetailReportResp = RestCaller
				.MenuConfigMstByMenuName("DAILY ORDER DETAILS REPORT");
		List<MenuConfigMst> menuDailyOrderDetailReportList = menuDailyOrderDetailReportResp.getBody();

		if (menuDailyOrderDetailReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAILY ORDER DETAILS REPORT");
			menuConfigMst.setMenuFxml("DailyOrderDetailsReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuSaleorderBalanceAdvanceReportResp = RestCaller
				.MenuConfigMstByMenuName("SALEORDER BALANCE ADVANCE REPORT");
		List<MenuConfigMst> menuSaleorderBalanceAdvanceReportList = menuSaleorderBalanceAdvanceReportResp.getBody();

		if (menuSaleorderBalanceAdvanceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALEORDER BALANCE ADVANCE REPORT");
			menuConfigMst.setMenuFxml("SaleOrderBalanceAdvanceReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuDeliveryboySalesorderReportResp = RestCaller
				.MenuConfigMstByMenuName("DELIVERYBOY SALEORDER REPORT");
		List<MenuConfigMst> menuDeliveryboySalesorderReportList = menuDeliveryboySalesorderReportResp.getBody();

		if (menuDeliveryboySalesorderReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DELIVERYBOY SALEORDER REPORT");
			menuConfigMst.setMenuFxml("DeliveryBoySaleOrder.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuDeliveryOrTakeOrderReportResp = RestCaller
				.MenuConfigMstByMenuName("HOMEDELIVERY OR TAKE AWAY REPORT");
		List<MenuConfigMst> menuDeliveryOrTakeOrderReportList = menuDeliveryOrTakeOrderReportResp.getBody();

		if (menuDeliveryOrTakeOrderReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("HOMEDELIVERY OR TAKE AWAY REPORT");
			menuConfigMst.setMenuFxml("HomeDeliveryReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuSaleOrderPendingReportResp = RestCaller
				.MenuConfigMstByMenuName("SALEORDER PENDING REPORT");
		List<MenuConfigMst> menuSaleOrderPendingReportList = menuSaleOrderPendingReportResp.getBody();

		if (menuSaleOrderPendingReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALEORDER PENDING REPORT");
			menuConfigMst.setMenuFxml("SaleorderPendingReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuDeliveryBoyPendingReportResp = RestCaller
				.MenuConfigMstByMenuName("DELIVERY BOY PENDING REPORT");
		List<MenuConfigMst> menuDeliveryBoyPendingReportList = menuDeliveryBoyPendingReportResp.getBody();

		if (menuDeliveryBoyPendingReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DELIVERY BOY PENDING REPORT");
			menuConfigMst.setMenuFxml("DeliveryBoyPendingReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuDailyCakeSettingResp = RestCaller
				.MenuConfigMstByMenuName("DAILY CAKE SETTING");
		List<MenuConfigMst> menuDailyCakeSettingList = menuDailyCakeSettingResp.getBody();

		if (menuDailyCakeSettingList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAILY CAKE SETTING");
			menuConfigMst.setMenuFxml("DailyCakeSetting.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuHomeDeliveryReportResp = RestCaller
				.MenuConfigMstByMenuName("HOMEDELIVERY REPORT");
		List<MenuConfigMst> menuHomeDeliveryReportList = menuHomeDeliveryReportResp.getBody();

		if (menuHomeDeliveryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("HOMEDELIVERY REPORT");
			menuConfigMst.setMenuFxml("HomeDeliveryReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuHomeDailyOrderReportResp = RestCaller
				.MenuConfigMstByMenuName("DAILY ORDER REPORT");
		List<MenuConfigMst> menuHomeDailyOrderReportList = menuHomeDailyOrderReportResp.getBody();

		if (menuHomeDailyOrderReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAILY ORDER REPORT");
			menuConfigMst.setMenuFxml("dailyOrderSummary.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuStockTransferOutReportResp = RestCaller
				.MenuConfigMstByMenuName("STOCK TRANSFER OUT REPORT");
		List<MenuConfigMst> menuStockTransferOutReportList = menuStockTransferOutReportResp.getBody();

		if (menuStockTransferOutReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK TRANSFER OUT REPORT");
			menuConfigMst.setMenuFxml("StockTransferOutReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuStockTransferOutDetailReportResp = RestCaller
				.MenuConfigMstByMenuName("STOCK TRANSFER OUT DETAIL REPORT");
		List<MenuConfigMst> menuStockTransferOutDetailReportList = menuStockTransferOutDetailReportResp.getBody();

		if (menuStockTransferOutDetailReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK TRANSFER OUT DETAIL REPORT");
			menuConfigMst.setMenuFxml("StockTransferOutDetailReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuStockTransferInReportResp = RestCaller
				.MenuConfigMstByMenuName("STOCK TRANSFER IN REPORT");
		List<MenuConfigMst> menuStockTransferInReportList = menuStockTransferInReportResp.getBody();

		if (menuStockTransferInReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK TRANSFER IN REPORT");
			menuConfigMst.setMenuFxml("AcceptStockReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuStockTransferInDetailReportResp = RestCaller
				.MenuConfigMstByMenuName("STOCK TRANSFER IN DETAIL REPORT");
		List<MenuConfigMst> menuStockTransferInDetailReportList = menuStockTransferInDetailReportResp.getBody();

		if (menuStockTransferInDetailReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK TRANSFER IN DETAIL REPORT");
			menuConfigMst.setMenuFxml("StockTransferInDetailReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuStockTransferDetailDeletedReportResp = RestCaller
				.MenuConfigMstByMenuName("STOCK TRANSFER DETAIL DELETED REPORT");
		List<MenuConfigMst> menuStockTransferDetailDeletedReportList = menuStockTransferDetailDeletedReportResp
				.getBody();

		if (menuStockTransferDetailDeletedReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK TRANSFER DETAIL DELETED REPORT");
			menuConfigMst.setMenuFxml("StockTransferDeletedDetails.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuVoucherReprintReportResp = RestCaller
				.MenuConfigMstByMenuName("VOUCHER REPRINTS");
		List<MenuConfigMst> menuVoucherReprintReportList = menuVoucherReprintReportResp.getBody();

		if (menuVoucherReprintReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("VOUCHER REPRINTS");
			menuConfigMst.setMenuFxml("VoucherReprint.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuReceiptReportResp = RestCaller
				.MenuConfigMstByMenuName("RECEIPT REPORTS");
		List<MenuConfigMst> menuReceiptReportList = menuReceiptReportResp.getBody();

		if (menuReceiptReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RECEIPT REPORTS");
			menuConfigMst.setMenuFxml("ReceiptReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPaymentReportResp = RestCaller
				.MenuConfigMstByMenuName("PAYMENT REPORT");
		List<MenuConfigMst> menuPaymentReportList = menuPaymentReportResp.getBody();

		if (menuPaymentReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("");
			menuConfigMst.setMenuFxml("PaymentReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuSaleOrderLowResolutionResp = RestCaller
				.MenuConfigMstByMenuName("SALE ORDER LOW RESOLUTION");
		List<MenuConfigMst> menuSaleOrderLowResolutionList = menuSaleOrderLowResolutionResp.getBody();

		if (menuPaymentReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALE ORDER LOW RESOLUTION");
			menuConfigMst.setMenuFxml("SaleOrderWindowLowResolution.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> menuCreditCardReconcileResp = RestCaller
				.MenuConfigMstByMenuName("CREDIT CARD RECONCILE");
		List<MenuConfigMst> menuCreditCardReconcileList = menuCreditCardReconcileResp.getBody();

		if (menuCreditCardReconcileList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CREDIT CARD RECONCILE");
			menuConfigMst.setMenuFxml("CreditCradReconcile.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuJournalReportResp = RestCaller
				.MenuConfigMstByMenuName("JOURNAL REPORTS");
		List<MenuConfigMst> menuJournalReportList = menuJournalReportResp.getBody();

		if (menuJournalReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("JOURNAL REPORTS");
			menuConfigMst.setMenuFxml("journalReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPurchaseSummaryReportResp = RestCaller
				.MenuConfigMstByMenuName("PURCHASE SUMMARY REPORT");
		List<MenuConfigMst> menuPurchaseSummaryReportList = menuPurchaseSummaryReportResp.getBody();

		if (menuPurchaseSummaryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE SUMMARY REPORT");
			menuConfigMst.setMenuFxml("purchaseReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuHsnCodeWisePurchaseReportResp = RestCaller
				.MenuConfigMstByMenuName("HSNCODE WISE PURCHASE REPORT");
		List<MenuConfigMst> menuHsnCodeWisePurchaseReportList = menuHsnCodeWisePurchaseReportResp.getBody();

		if (menuHsnCodeWisePurchaseReportList.size() == 0) { 
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("HSNCODE WISE PURCHASE REPORT");
			menuConfigMst.setMenuFxml("HsnCodePurchaseReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuSupplierMonthlySummaryResp = RestCaller
				.MenuConfigMstByMenuName("SUPPLIER MONTHLY SUMMARY");
		List<MenuConfigMst> menuSupplierMonthlySummaryList = menuSupplierMonthlySummaryResp.getBody();

		if (menuSupplierMonthlySummaryList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUPPLIER MONTHLY SUMMARY");
			menuConfigMst.setMenuFxml("MonthlySupplierSummary.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuSupplierBillwiseByDueDaysResp = RestCaller
				.MenuConfigMstByMenuName("SUPPLIER BILLWISE BY DUE DAYS");
		List<MenuConfigMst> menuSupplierBillwiseByDueDaysList = menuSupplierBillwiseByDueDaysResp.getBody();

		if (menuSupplierBillwiseByDueDaysList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUPPLIER BILLWISE BY DUE DAYS");
			menuConfigMst.setMenuFxml("SupplierBillWiseByDueDays.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPharmacyPurchaseReportResp = RestCaller
				.MenuConfigMstByMenuName("PURCHASE REPORT PHARMACY");
		List<MenuConfigMst> menuPharmacyPurchaseReportList = menuPharmacyPurchaseReportResp.getBody();

		if (menuPharmacyPurchaseReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PURCHASE REPORT PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyPurchaseReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuProductionPlanningReportResp = RestCaller
				.MenuConfigMstByMenuName("PRODUCTION PLANNING REPORT");
		List<MenuConfigMst> menuProductionPlanningReportList = menuProductionPlanningReportResp.getBody();

		if (menuProductionPlanningReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCTION PLANNING REPORT");
			menuConfigMst.setMenuFxml("ProductionPlanningReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuRawMaterialRequiredReportResp = RestCaller
				.MenuConfigMstByMenuName("RAW MATERIAL REQUIRED REPORT");
		List<MenuConfigMst> menuRawMaterialRequiredReportList = menuRawMaterialRequiredReportResp.getBody();

		if (menuRawMaterialRequiredReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RAW MATERIAL REQUIRED REPORT");
			menuConfigMst.setMenuFxml("RawMaterialRequiredReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuRawMaterialIssueReportResp = RestCaller
				.MenuConfigMstByMenuName("RAW MATERIAL ISSUE REPORT");
		List<MenuConfigMst> menuRawMaterialIssueReportList = menuRawMaterialIssueReportResp.getBody();

		if (menuRawMaterialIssueReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RAW MATERIAL ISSUE REPORT");
			menuConfigMst.setMenuFxml("RawMaterialIssueReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuActualProductionReportResp = RestCaller
				.MenuConfigMstByMenuName("ACTUAL PRODUCTION REPORT");
		List<MenuConfigMst> menuActualProductionReportList = menuActualProductionReportResp.getBody();

		if (menuActualProductionReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACTUAL PRODUCTION REPORT");
			menuConfigMst.setMenuFxml("ActualProductionReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuActualProductionAndPlanningReportResp = RestCaller
				.MenuConfigMstByMenuName("ACTUAL PRODUCTION AND PLANING SUMMARY");
		List<MenuConfigMst> menuActualProductionAndPlanningReportList = menuActualProductionAndPlanningReportResp
				.getBody();

		if (menuActualProductionAndPlanningReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACTUAL PRODUCTION AND PLANING SUMMARY");
			menuConfigMst.setMenuFxml("ProductionPlaningAndActualReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuConsumptionReportResp = RestCaller
				.MenuConfigMstByMenuName("CONSUMPTION REPORT");
		List<MenuConfigMst> menuConsumptionReportList = menuConsumptionReportResp.getBody();

		if (menuConsumptionReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CONSUMPTION REPORT");
			menuConfigMst.setMenuFxml("consumptionreport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

//		ResponseEntity<List<MenuConfigMst>> menuConsumptionReportResp= RestCaller.MenuConfigMstByMenuName("CONSUMPTION REPORT");
//		List<MenuConfigMst> menuConsumptionReportList =  menuConsumptionReportResp.getBody();
//		
//		if(menuConsumptionReportList.size() == 0) 
//		{
//			MenuConfigMst menuConfigMst = new MenuConfigMst();
//			menuConfigMst.setMenuName("CONSUMPTION REPORT");
//			menuConfigMst.setMenuFxml("consumptionreport.fxml");
//			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
//			
//			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
//		}
		ResponseEntity<List<MenuConfigMst>> menuSupplierWisePurchaseReportResp = RestCaller
				.MenuConfigMstByMenuName("SUPPLIERWISE PURCHASE REPORT");
		List<MenuConfigMst> menuSupplierWisePurchaseReportList = menuSupplierWisePurchaseReportResp.getBody();

		if (menuSupplierWisePurchaseReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUPPLIERWISE PURCHASE REPORT");
			menuConfigMst.setMenuFxml("SupplierwisePurchaseReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

//		ResponseEntity<List<MenuConfigMst>> menuConsumptionReportResp= RestCaller.MenuConfigMstByMenuName("CONSUMPTION REPORT");
//		List<MenuConfigMst> menuConsumptionReportList =  menuConsumptionReportResp.getBody();
//		
//		if(menuConsumptionReportList.size() == 0) 
//		{
//			MenuConfigMst menuConfigMst = new MenuConfigMst();
//			menuConfigMst.setMenuName("CONSUMPTION REPORT");
//			menuConfigMst.setMenuFxml("consumptionreport.fxml");
//			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
//			
//			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
//		}

		ResponseEntity<List<MenuConfigMst>> menuReasonWiseConsumptionReportResp = RestCaller
				.MenuConfigMstByMenuName("REASON WISE CONSUMPTION REPORT");
		List<MenuConfigMst> menuReasonWiseConsumptionReportList = menuReasonWiseConsumptionReportResp.getBody();

		if (menuReasonWiseConsumptionReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("REASON WISE CONSUMPTION REPORT");
			menuConfigMst.setMenuFxml("ReasonWiseConsumptionReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuRawMaterialItemWiseReportResp = RestCaller
				.MenuConfigMstByMenuName("RAW MATERIAL ITEMWISE");
		List<MenuConfigMst> menuRawMaterialItemWiseReportList = menuRawMaterialItemWiseReportResp.getBody();

		if (menuRawMaterialItemWiseReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RAW MATERIAL ITEMWISE");
			menuConfigMst.setMenuFxml("ProductionRawMaterialItemWise.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuShortExpiryReportResp = RestCaller
				.MenuConfigMstByMenuName("SHORT EXPIRY REPORT");
		List<MenuConfigMst> menuShortExpiryReportList = menuShortExpiryReportResp.getBody();

		if (menuShortExpiryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SHORT EXPIRY REPORT");
			menuConfigMst.setMenuFxml("ShortExpiryReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuCategoryWiseShortExpiryReportResp = RestCaller
				.MenuConfigMstByMenuName("CATEGORYWISE SHORT EXPIRY REPORT");
		List<MenuConfigMst> menuCategoryWiseShortExpiryReportList = menuCategoryWiseShortExpiryReportResp.getBody();

		if (menuCategoryWiseShortExpiryReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CATEGORYWISE SHORT EXPIRY REPORT");
			menuConfigMst.setMenuFxml("CategoryWiseShortExpiry.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuItemExceptionReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEM EXCEPTION REPORT");
		List<MenuConfigMst> menuItemExceptionReportList = menuItemExceptionReportResp.getBody();

		if (menuItemExceptionReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM EXCEPTION REPORT");
			menuConfigMst.setMenuFxml("ExceptionListReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuWeighingMachineReportResp = RestCaller
				.MenuConfigMstByMenuName("WEIGHING MACHINE REPORT");
		List<MenuConfigMst> menuWeighingMachineReportList = menuWeighingMachineReportResp.getBody();

		if (menuWeighingMachineReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("WEIGHING MACHINE REPORT");
			menuConfigMst.setMenuFxml("weighBridgeDailyReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuItemStockEnquiryReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEM STOCK ENQUIRY");
		List<MenuConfigMst> menuItemStockEnquiryReportList = menuItemStockEnquiryReportResp.getBody();

		if (menuWeighingMachineReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM STOCK ENQUIRY");
			menuConfigMst.setMenuFxml("ItemStockEnquiry.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuPhysicalStockReportResp = RestCaller
				.MenuConfigMstByMenuName("PHYSICAL STOCK REPORT");
		List<MenuConfigMst> menuPhysicalStockReportList = menuPhysicalStockReportResp.getBody();

		if (menuPhysicalStockReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PHYSICAL STOCK REPORT");
			menuConfigMst.setMenuFxml("PhysicalStockReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuItemWiseIntentReportResp = RestCaller
				.MenuConfigMstByMenuName("ITEM WISE INTENT REPORT SUMMARY");
		List<MenuConfigMst> menuItemWiseIntentReportList = menuItemWiseIntentReportResp.getBody();

		if (menuItemWiseIntentReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM WISE INTENT REPORT SUMMARY");
			menuConfigMst.setMenuFxml("ItemWiseIntentSummaryReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuIncomeAndExpenceReportResp = RestCaller
				.MenuConfigMstByMenuName("INCOME AND EXPENCE REPORT");
		List<MenuConfigMst> menuIncomeAndExpenceReportList = menuIncomeAndExpenceReportResp.getBody();

		if (menuIncomeAndExpenceReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INCOME AND EXPENCE REPORT");
			menuConfigMst.setMenuFxml("financialYearStatement.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuProductionPrePlanningResp = RestCaller
				.MenuConfigMstByMenuName("PRODUCTION PRE PLANNING");
		List<MenuConfigMst> menuProductionPrePlanningList = menuProductionPrePlanningResp.getBody();

		if (menuProductionPrePlanningList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCTION PRE PLANNING");
			menuConfigMst.setMenuFxml("productionPrePlaning.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuProductionPlanningResp = RestCaller
				.MenuConfigMstByMenuName("PRODUCTION PLANNING");
		List<MenuConfigMst> menuProductionPlanningList = menuProductionPlanningResp.getBody();

		if (menuProductionPlanningList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PRODUCTION PLANNING");
			menuConfigMst.setMenuFxml("ProductionPlanning.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuActualProductionResp = RestCaller
				.MenuConfigMstByMenuName("ACTUAL PRODUCTION");
		List<MenuConfigMst> menuActualProductionList = menuActualProductionResp.getBody();

		if (menuActualProductionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACTUAL PRODUCTION");
			menuConfigMst.setMenuFxml("ActualProduction.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> menucustomSalesMdenResp = RestCaller
				.MenuConfigMstByMenuName("CUSTOM SALES MODE");
		List<MenuConfigMst> menucustomSalesMdenRespList = menucustomSalesMdenResp.getBody();

		if (menucustomSalesMdenRespList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOM SALES MODE");
			menuConfigMst.setMenuFxml("CustomiseSalesModeConfigInMenu.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> customInvoiceformat = RestCaller
				.MenuConfigMstByMenuName("CUSTOM INVOICE FORMAT");
		List<MenuConfigMst> customInvoiceformatList = customInvoiceformat.getBody();

		if (customInvoiceformatList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOM INVOICE FORMAT");
			menuConfigMst.setMenuFxml("CustomInvoiceFormat.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuRawMaterialIssueResp = RestCaller
				.MenuConfigMstByMenuName("RAW MATERIAL ISSUE");
		List<MenuConfigMst> menuRawMaterialIssueList = menuRawMaterialIssueResp.getBody();

		if (menuRawMaterialIssueList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RAW MATERIAL ISSUE");
			menuConfigMst.setMenuFxml("RawMaterialIssue.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuRawMaterialReturnResp = RestCaller
				.MenuConfigMstByMenuName("RAW MATERIAL RETURN");
		List<MenuConfigMst> menuRawMaterialReturnList = menuRawMaterialReturnResp.getBody();

		if (menuRawMaterialReturnList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RAW MATERIAL RETURN");
			menuConfigMst.setMenuFxml("RawMaterialReturn.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuTaskListResp = RestCaller.MenuConfigMstByMenuName("TASK LIST");
		List<MenuConfigMst> menuTaskList = menuTaskListResp.getBody();

		if (menuTaskList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TASK LIST");
			menuConfigMst.setMenuFxml("tasklist-1000x600.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuTallyUrlCreationResp = RestCaller
				.MenuConfigMstByMenuName("TALLY URL CREATION");
		List<MenuConfigMst> menuTallyUrlCreationList = menuTallyUrlCreationResp.getBody();

		if (menuTallyUrlCreationList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TALLY URL CREATION");
			menuConfigMst.setMenuFxml("TallyIntegrationMst.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> menuOtherBranchPurchaseResp = RestCaller
				.MenuConfigMstByMenuName("OTHER BRANCH PURCHASE");
		List<MenuConfigMst> menuOtherBranchPurchaseList = menuOtherBranchPurchaseResp.getBody();

		if (menuOtherBranchPurchaseList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OTHER BRANCH PURCHASE");
			menuConfigMst.setMenuFxml("OtherBranchPurchase.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> paramValueConfigResp = RestCaller
				.MenuConfigMstByMenuName("PARAM VALUE CONFIG");
		List<MenuConfigMst> paramValueConfigList = paramValueConfigResp.getBody();

		if (paramValueConfigList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PARAM VALUE CONFIG");
			menuConfigMst.setMenuFxml("ParamValueConfig.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> otherBranchSalesEditResp = RestCaller
				.MenuConfigMstByMenuName("OTHER BRANCH SALES EDIT");
		List<MenuConfigMst> otherBranchSalesEditList = otherBranchSalesEditResp.getBody();

		if (otherBranchSalesEditList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OTHER BRANCH SALES EDIT");
			menuConfigMst.setMenuFxml("OtherBranchSaleEdit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> chequeprint = RestCaller.MenuConfigMstByMenuName("CHEQUE PRINT PROPERTIES");
		List<MenuConfigMst> chequeprintList = otherBranchSalesEditResp.getBody();

		if (chequeprintList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CHEQUE PRINT PROPERTIES");
			menuConfigMst.setMenuFxml("ChequePrint.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> onlineSalesWIndowAtlier = RestCaller
				.MenuConfigMstByMenuName("ONLINE ATLIER WINDOW");
		List<MenuConfigMst> onlineSalesWIndowAtlierList = onlineSalesWIndowAtlier.getBody();

		if (onlineSalesWIndowAtlierList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ONLINE ATLIER WINDOW");
			menuConfigMst.setMenuFxml("onlineAtlierSales.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> salesProfitReport = RestCaller
				.MenuConfigMstByMenuName("SALES PROFIT REPORT");
		List<MenuConfigMst> salesProfitReportList = salesProfitReport.getBody();

		if (salesProfitReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES PROFIT REPORT");
			menuConfigMst.setMenuFxml("SalesProfitReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> acceptIntent = RestCaller.MenuConfigMstByMenuName("ACCEPT INTENT");
		List<MenuConfigMst> acceptIntenttList = salesProfitReport.getBody();

		if (acceptIntenttList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCEPT INTENT");
			menuConfigMst.setMenuFxml("AcceptIntent.fxml");
			menuConfigMst.setMenuDescription("accept intent");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> barcodeFormatConfig = RestCaller
				.MenuConfigMstByMenuName("BARCODE FORMAT CONFIGURATION");
		List<MenuConfigMst> barcodeFormatConfigList = barcodeFormatConfig.getBody();

		if (barcodeFormatConfigList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BARCODE FORMAT CONFIGURATION");
			menuConfigMst.setMenuFxml("BarcodeFormatConfiguration.fxml");
			menuConfigMst.setMenuDescription("BARCODE FORMAT CONFIGURATION");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> acceptIntentResp = RestCaller.MenuConfigMstByMenuName("ACCEPT INTENT");
		List<MenuConfigMst> acceptIntentList = acceptIntentResp.getBody();

		if (acceptIntentList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ACCEPT INTENT");
			menuConfigMst.setMenuFxml("AcceptIntent.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		
		ResponseEntity<List<MenuConfigMst>> organisationcreationResp= RestCaller.MenuConfigMstByMenuName("ORGANISATION CREATION");
		List<MenuConfigMst> organisationcreationList =  organisationcreationResp.getBody();
		
		if(organisationcreationList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ORGANISATION CREATION");
			menuConfigMst.setMenuFxml("Organisation.fxml");
			menuConfigMst.setMenuDescription("ORGANISATION CREATION");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> orgPaymentResp= RestCaller.MenuConfigMstByMenuName("ORG PAYMENTS");
		List<MenuConfigMst> orgpaymentList =  orgPaymentResp.getBody();
		
		if(orgpaymentList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ORG PAYMENTS");
			menuConfigMst.setMenuFxml("OrgPaymemt.fxml");
			menuConfigMst.setMenuDescription("ORG PAYMENTS");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> familyCreatiionResp= RestCaller.MenuConfigMstByMenuName("FAMILY CREATION");
		List<MenuConfigMst> familyCreationList =  familyCreatiionResp.getBody();
		
		if(familyCreationList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("FAMILY CREATION");
			menuConfigMst.setMenuFxml("FamilyCreation.fxml");
			menuConfigMst.setMenuDescription("FAMILY CREATION");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		

		ResponseEntity<List<MenuConfigMst>> memberMstResp= RestCaller.MenuConfigMstByMenuName("MEMBER CREATION");
		List<MenuConfigMst> memberMstList =  memberMstResp.getBody();
		
		if(memberMstList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MEMBER CREATION");
			menuConfigMst.setMenuFxml("MemberMst.fxml");
			menuConfigMst.setMenuDescription("FAMILY CREATION");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> subGroupMemberResp= RestCaller.MenuConfigMstByMenuName("SUB GROUP MEMBER");
		List<MenuConfigMst> subGroupMemberList =  subGroupMemberResp.getBody();
		
		if(subGroupMemberList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUB GROUP MEMBER");
			menuConfigMst.setMenuFxml("SubGroupMember.fxml");
			menuConfigMst.setMenuDescription("SUB GROUP MEMBER");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> eventCReationResp= RestCaller.MenuConfigMstByMenuName("EVENT CRTEATION");
		List<MenuConfigMst> eventCreationList =  eventCReationResp.getBody();
		
		if(eventCreationList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("EVENT CRTEATION");
			menuConfigMst.setMenuFxml("EventCreation.fxml");
			menuConfigMst.setMenuDescription("EVENT CRTEATION");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> pasterMstResp= RestCaller.MenuConfigMstByMenuName("PASTER MST");
		List<MenuConfigMst> pasterMstList =  pasterMstResp.getBody();
		
		if(pasterMstList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PASTER MST");
			menuConfigMst.setMenuFxml("PasterMst.fxml");
			menuConfigMst.setMenuDescription("PASTER MST");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		

		ResponseEntity<List<MenuConfigMst>> meetingMstResp= RestCaller.MenuConfigMstByMenuName("MEETING MST");
		List<MenuConfigMst> meetingMstList =  meetingMstResp.getBody();
		
		if(meetingMstList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MEETING MST");
			menuConfigMst.setMenuFxml("meetingmsts.fxml");
			menuConfigMst.setMenuDescription("MEETING MST");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		ResponseEntity<List<MenuConfigMst>> meetingMemberResp= RestCaller.MenuConfigMstByMenuName("MEETING MEMBER");
		List<MenuConfigMst> meetingMemberList =  meetingMemberResp.getBody();
		
		if(meetingMemberList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MEETING MEMBER");
			menuConfigMst.setMenuFxml("MeetingMemberDetail.fxml");
			menuConfigMst.setMenuDescription("MEETING MEMBER");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		

		ResponseEntity<List<MenuConfigMst>> meetingMenutesResp= RestCaller.MenuConfigMstByMenuName("MEETING MINUTES");
		List<MenuConfigMst> meetingMenutesList =  meetingMenutesResp.getBody();
		
		if(meetingMenutesList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MEETING MINUTES");
			menuConfigMst.setMenuFxml("MeetingMinutesDtl.fxml");
			menuConfigMst.setMenuDescription("MEETING MINUTES");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> orgReceiptResp= RestCaller.MenuConfigMstByMenuName("RECEIPTS");
		List<MenuConfigMst> orgReceiptList =  orgReceiptResp.getBody();
		
		if(orgReceiptList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RECEIPTS");
			menuConfigMst.setMenuFxml("OrgReceiptwindow.fxml");
			menuConfigMst.setMenuDescription("RECEIPTS");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> monthlyAcccountReceiptReportResp= RestCaller.MenuConfigMstByMenuName("MONTHLY ACCOUNT RECEIPT REPORT");
		List<MenuConfigMst> monthlyAcccountReceiptList =  monthlyAcccountReceiptReportResp.getBody();
		
		if(monthlyAcccountReceiptList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MONTHLY ACCOUNT RECEIPT REPORT");
			menuConfigMst.setMenuFxml("OrgAccountBalance.fxml");
			menuConfigMst.setMenuDescription("MONTHLY ACCOUNT RECEIPT REPORT");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> orgMonthlyAccountPayResp= RestCaller.MenuConfigMstByMenuName("MONTHLY ACCOUNT PAYMENT REPORT");
		List<MenuConfigMst> orgMonthlyAccountPayList =  orgMonthlyAccountPayResp.getBody();
		
		if(orgMonthlyAccountPayList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MONTHLY ACCOUNT PAYMENT REPORT");
			menuConfigMst.setMenuFxml("OrgMonthlyAccPay.fxml");
			menuConfigMst.setMenuDescription("MONTHLY ACCOUNT PAYMENT REPORT");
			
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		

		ResponseEntity<List<MenuConfigMst>> memberWiseReportResourceResp= RestCaller.MenuConfigMstByMenuName("MEMBERWISE RECEIPT REPORT");
		List<MenuConfigMst> memberWiseReportList =  memberWiseReportResourceResp.getBody();
		
		if(memberWiseReportList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("MEMBERWISE RECEIPT REPORT");
			menuConfigMst.setMenuFxml("MemberWiseReceiptReport.fxml");
			menuConfigMst.setMenuDescription("MEMBERWISE RECEIPT REPORT");
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		

		ResponseEntity<List<MenuConfigMst>> pendingResp= RestCaller.MenuConfigMstByMenuName("DUE AMOUNT REPORT");
		List<MenuConfigMst> pendingReportList =  memberWiseReportResourceResp.getBody();
		
		if(pendingReportList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DUE AMOUNT REPORT");
			menuConfigMst.setMenuFxml("PendingReport.fxml");
			menuConfigMst.setMenuDescription("DUE AMOUNT REPORT");
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		

		ResponseEntity<List<MenuConfigMst>> allMemberWiseReportResp= RestCaller.MenuConfigMstByMenuName("ALL MEMBERWISE RECEIPT REPORT");
		List<MenuConfigMst> allMemberwiseReportList =  allMemberWiseReportResp.getBody();
		
		if(allMemberwiseReportList.size() == 0) 
		{
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ALL MEMBERWISE RECEIPT REPORT");
			menuConfigMst.setMenuFxml("AllMemberWiseReceiptRerport.fxml");
			menuConfigMst.setMenuDescription("ALL MEMBERWISE RECEIPT REPORT");
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
	//.fxml
		


		ResponseEntity<List<MenuConfigMst>> storewiseStockResp = RestCaller.MenuConfigMstByMenuName("STOREWISE STOCK REPORT");
		List<MenuConfigMst> storewiseStockList = storewiseStockResp.getBody();


		if (storewiseStockList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOREWISE STOCK REPORT");
			menuConfigMst.setMenuFxml("StoreWiseStockSummary.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> userWiseSalesReportResp = RestCaller.MenuConfigMstByMenuName("USERWISE SALES REPORT");
		List<MenuConfigMst> userWiseSalesReportList = userWiseSalesReportResp.getBody();

		if (userWiseSalesReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("USERWISE SALES REPORT");
			menuConfigMst.setMenuFxml("UserWiseSalesReport.fxml");
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		
		ResponseEntity<List<MenuConfigMst>> storechangereport = RestCaller.MenuConfigMstByMenuName("STORE CHANGE REPORT");
		List<MenuConfigMst> storechangeLst = storechangereport.getBody();

		if (storechangeLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STORE CHANGE REPORT");
			menuConfigMst.setMenuFxml("StoreChangeReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> customerWiseProfitResp = RestCaller.MenuConfigMstByMenuName("CUSTOMERWISE PROFIT");
		List<MenuConfigMst> customerWiseProfitList = customerWiseProfitResp.getBody();

		if (customerWiseProfitList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOMERWISE PROFIT");
			menuConfigMst.setMenuFxml("CustomerWiseProfit.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}

		

		ResponseEntity<List<MenuConfigMst>> balancesheet = RestCaller.MenuConfigMstByMenuName("BALANCE SHEET");
		List<MenuConfigMst> balancesheetLst = balancesheet.getBody();

		if (balancesheetLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BALANCE SHEET");
			menuConfigMst.setMenuFxml("BalanceSheetHorizontal.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> pharmacyWholesale = RestCaller.MenuConfigMstByMenuName("WHOLESALE PHARMACY");
		List<MenuConfigMst> pharmacyWholesaleLst = pharmacyWholesale.getBody();

		if (pharmacyWholesaleLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("WHOLESALE PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyWholeSaleWindow.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> pharmacyCorporateSales= RestCaller.MenuConfigMstByMenuName("CORPORATE SALES PHARMACY");
		List<MenuConfigMst> pharmacyCorporateSalesLst = pharmacyCorporateSales.getBody();

		if (pharmacyCorporateSalesLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CORPORATE SALES PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyCorporateSales.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> pharmacyCustomerCreatiom= RestCaller.MenuConfigMstByMenuName("CUSTOMER PHARMACY");
		List<MenuConfigMst> pharmacyCustomerCreatiomLst = pharmacyCustomerCreatiom.getBody();

		if (pharmacyCustomerCreatiomLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CUSTOMER PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyCustomerCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> pharmacyRetail= RestCaller.MenuConfigMstByMenuName("RETAIL PHARMACY");
		List<MenuConfigMst> pharmacyRetailLst = pharmacyRetail.getBody();

		if (pharmacyRetailLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("RETAIL PHARMACY");
			menuConfigMst.setMenuFxml("PharmacyRetail.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

		}
		ResponseEntity<List<MenuConfigMst>> loyalityCustomer = RestCaller.MenuConfigMstByMenuName("LOYALITY CUSTOMER");
		List<MenuConfigMst> loyalityCustomerList = loyalityCustomer.getBody();

		if (loyalityCustomerList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("LOYALITY CUSTOMER");
			menuConfigMst.setMenuFxml("LoyalityCustomer.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> pharmacySalesReturn= RestCaller.MenuConfigMstByMenuName("SALES RETURN PHARMACY");
		List<MenuConfigMst> pharmacySalesReturnLst = pharmacySalesReturn.getBody();

		if (pharmacySalesReturnLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALES RETURN PHARMACY");
			menuConfigMst.setMenuFxml("PharmacySalesReturnReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> patient= RestCaller.MenuConfigMstByMenuName("PATIENT");
		List<MenuConfigMst> patientLst = patient.getBody();

		if (patientLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PATIENT");
			menuConfigMst.setMenuFxml("PatientCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> insurance= RestCaller.MenuConfigMstByMenuName("INSURANCE CREATION");
		List<MenuConfigMst> insuranceLst = insurance.getBody();

		if (insuranceLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("INSURANCE CREATION");
			menuConfigMst.setMenuFxml("InsuranceCreation.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> importPurchase= RestCaller.MenuConfigMstByMenuName("IMPORT PURCHASE");
		List<MenuConfigMst> importPurchaseLst = importPurchase.getBody();

		if (importPurchaseLst.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("IMPORT PURCHASE");
			menuConfigMst.setMenuFxml("ImportPurchase.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> termsAndConditionResp= RestCaller.MenuConfigMstByMenuName("TERMS AND CONDITION");
		List<MenuConfigMst> termsAndConditionList = termsAndConditionResp.getBody();

		if (termsAndConditionList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TERMS AND CONDITION");
			menuConfigMst.setMenuFxml("TermsAndConditionMst.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> pharmacySaleOrderResp= RestCaller.MenuConfigMstByMenuName("SALEORDER PHARMACY");
		List<MenuConfigMst> pharmacySaleOrderList = pharmacySaleOrderResp.getBody();

		if (pharmacySaleOrderList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALEORDER PHARMACY");
			menuConfigMst.setMenuFxml("PharmacySaleOrder.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> hsnpurchaseDtlreport= RestCaller.MenuConfigMstByMenuName("HSNWISE PURCHASE DETAIL REPORT");
		List<MenuConfigMst> hsnpurchaseDtlreportList = hsnpurchaseDtlreport.getBody();

		if (pharmacySaleOrderList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("HSNWISE PURCHASE DETAIL REPORT");
			menuConfigMst.setMenuFxml("HsnWisePurchaseDtlReport.fxml");
		}
		ResponseEntity<List<MenuConfigMst>> otherBranchPurchaseReportResp = RestCaller.MenuConfigMstByMenuName("OTHER BRANCH PURCHASE REPORT");
		List<MenuConfigMst> otherBranchPurchaseReportList = otherBranchPurchaseReportResp.getBody();

		if (otherBranchPurchaseReportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("OTHER BRANCH PURCHASE REPORT");
			menuConfigMst.setMenuFxml("OtherBranchPurchaseReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> hsnSalesDtlreport = RestCaller.MenuConfigMstByMenuName("HSN SALES DETAL REPORT");
		List<MenuConfigMst> hsnSalesDtlreportList = hsnSalesDtlreport.getBody();

		if (hsnSalesDtlreportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("HSN SALES DETAL REPORT");
			menuConfigMst.setMenuFxml("HsnWiseSalesDtlReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> gstsalesreport = RestCaller.MenuConfigMstByMenuName("GST SALES REPORT");
		List<MenuConfigMst> gstsalesreportList = gstsalesreport.getBody();

		if (gstsalesreportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("GST SALES REPORT");
			menuConfigMst.setMenuFxml("GstSalesReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> itemdevice = RestCaller.MenuConfigMstByMenuName("ITEM DEVICE");
		List<MenuConfigMst> itemdeviceList = itemdevice.getBody();

		if (itemdeviceList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ITEM DEVICE");
			menuConfigMst.setMenuFxml("ItemDevice.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		ResponseEntity<List<MenuConfigMst>> loyalcustreport = RestCaller.MenuConfigMstByMenuName("LOYALTY CUSTOMER REPORT");
		List<MenuConfigMst> loyalcustreportList = loyalcustreport.getBody();

		if (loyalcustreportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("LOYALTY CUSTOMER REPORT");
			menuConfigMst.setMenuFxml("LoyaltyCustomerReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> orgReceiptreport = RestCaller.MenuConfigMstByMenuName("ORG RECEIPT REPORT");
		List<MenuConfigMst> orgReceiptreportList = orgReceiptreport.getBody();

		if (orgReceiptreportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("ORG RECEIPT REPORT");
			menuConfigMst.setMenuFxml("orgReceiptReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> saleordercancelledreport = RestCaller.MenuConfigMstByMenuName("CANCELLED SALE ORDER REPORT");
		List<MenuConfigMst> saleordercancelledreportList = saleordercancelledreport.getBody();

		if (saleordercancelledreportList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CANCELLED SALE ORDER REPORT");
			menuConfigMst.setMenuFxml("CanceledSaleOrderReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> weighbridge = RestCaller.MenuConfigMstByMenuName("WEIGH BRIDGE");
		List<MenuConfigMst> weighbridgeList = weighbridge.getBody();

		if (weighbridgeList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("WEIGH BRIDGE");
			menuConfigMst.setMenuFxml("WeighingMachineDisplayNew.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		ResponseEntity<List<MenuConfigMst>> convertedso = RestCaller.MenuConfigMstByMenuName("SALE ORDER CONVERTED REPORT");
		List<MenuConfigMst> convertedsoList = convertedso.getBody();

		if (convertedsoList.size() == 0) {
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SALE ORDER CONVERTED REPORT");
			menuConfigMst.setMenuFxml("SOConvertedReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> wholeSalesReportResp = RestCaller.MenuConfigMstByMenuName("WHOLESALE REPORT");
		List<MenuConfigMst> wholeSalesReportList = wholeSalesReportResp.getBody();

		if (wholeSalesReportList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("WHOLESALE REPORT");
			menuConfigMst.setMenuFxml("WholeSalesReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		

		ResponseEntity<List<MenuConfigMst>> dynamicproduction = RestCaller.MenuConfigMstByMenuName("DYNAMIC PRODUCTION");
		List<MenuConfigMst>dynamicproductionList = dynamicproduction.getBody();

		if (dynamicproductionList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DYNAMIC PRODUCTION");
			menuConfigMst.setMenuFxml("DynamicProduction.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> dayendReceiptMode = RestCaller.MenuConfigMstByMenuName("DAYEND RECEIPTMODE");
		List<MenuConfigMst>dayendReceiptModeList = dayendReceiptMode.getBody();

		if (dayendReceiptModeList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DAYEND RECEIPTMODE");
			menuConfigMst.setMenuFxml("DayEndReceiptMode.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		

		ResponseEntity<List<MenuConfigMst>> currencyConvertion = RestCaller.MenuConfigMstByMenuName("CURRENCY CONVERSION");
		List<MenuConfigMst>currencyConvertionList = currencyConvertion.getBody();

		if (currencyConvertionList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CURRENCY CONVERSION");
			menuConfigMst.setMenuFxml("CurrenyConverter.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> branchCategoryResp = RestCaller.MenuConfigMstByMenuName("BRANCH CATEGORY CREATION");
		List<MenuConfigMst> branchCategoryList = branchCategoryResp.getBody();

		if (branchCategoryList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("BRANCH CATEGORY CREATION");
			menuConfigMst.setMenuFxml("BranchCategory.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		
		
		ResponseEntity<List<MenuConfigMst>> supplierPriceMstResp = RestCaller.MenuConfigMstByMenuName("SUPPLIER PRICE CREATION");
		List<MenuConfigMst> supplierPricemstList = supplierPriceMstResp.getBody();

		if (supplierPricemstList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("SUPPLIER PRICE CREATION");
			menuConfigMst.setMenuFxml("SupplierPriceMst.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		
		

		ResponseEntity<List<MenuConfigMst>> userCreatedTaskResp = RestCaller.MenuConfigMstByMenuName("USER CREATED TASK");
		List<MenuConfigMst> userCreatedTaskList = userCreatedTaskResp.getBody();

		if (userCreatedTaskList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("USER CREATED TASK");
			menuConfigMst.setMenuFxml("UserCreatedTask.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		
		ResponseEntity<List<MenuConfigMst>> menuConfigMstStockTransferInExceptionReport = RestCaller
				.MenuConfigMstByMenuName("STOCK TRANSFER IN EXCEPTION REPORT");
		List<MenuConfigMst> menuConfigMstStockTransferInExceptionReportList = menuConfigMstStockTransferInExceptionReport.getBody();

		if (menuConfigMstStockTransferInExceptionReportList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("STOCK TRANSFER IN EXCEPTION REPORT");
			menuConfigMst.setMenuFxml("StockTransferInExceptionReport.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
System.out.println("inside the menu creation isssssssssssssssss##############################################################################################################");
			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
		}
		//----------------S------DEBIT NOTE---------------------------------//

		ResponseEntity<List<MenuConfigMst>> debitNoteResp = RestCaller.MenuConfigMstByMenuName("DEBIT NOTE");
		List<MenuConfigMst> debitNoteList = debitNoteResp.getBody();

		if (debitNoteList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DEBIT NOTE");
			menuConfigMst.setMenuFxml("DebitNote.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		//----------------S------CREDIT NOTE---------------------------------//
		ResponseEntity<List<MenuConfigMst>> creditNoteResp = RestCaller.MenuConfigMstByMenuName("CREDIT NOTE");
		List<MenuConfigMst> creditNoteList = creditNoteResp.getBody();

		if (creditNoteList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("CREDIT NOTE");
			menuConfigMst.setMenuFxml("CreditNote.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		
		
		//-----------------S----BarcodeEdit Window---------------------------------//

				ResponseEntity<List<MenuConfigMst>> BarcodeEditWindowResp = RestCaller.MenuConfigMstByMenuName("BARCODE EDIT WINDOW");
				List<MenuConfigMst> barcodeEditWindowList = BarcodeEditWindowResp.getBody();

				if (barcodeEditWindowList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("BARCODE EDIT WINDOW");
					menuConfigMst.setMenuFxml("BarcodeEditWindow.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
		
		//-----------S----------Good Receive Note---------------------------------//

		ResponseEntity<List<MenuConfigMst>> GoodReceiveNoteResp = RestCaller.MenuConfigMstByMenuName("GOOD RECEIVE NOTE");
		List<MenuConfigMst> goodReceiveNoteList = GoodReceiveNoteResp.getBody();

		if (goodReceiveNoteList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("GOOD RECEIVE NOTE");
			menuConfigMst.setMenuFxml("GoodReceiveNote.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		//---------------------S-------------Other Branch Acknowledgement------------------------//
		
		ResponseEntity<List<MenuConfigMst>> otherBranchSalesAckResp = RestCaller.MenuConfigMstByMenuName("OTHER BRANCH ACKNOWLEDGEMENT");
		List<MenuConfigMst> otherBranchSalesAckList = otherBranchSalesAckResp.getBody();

		if (otherBranchSalesAckList.size() == 0) {
			
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			
			menuConfigMst.setMenuName("OTHER BRANCH ACKNOWLEDGEMENT");
			menuConfigMst.setMenuFxml("OtherBranchAcknowledgement.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		
		//---------------------Sibi-------------Tally Retry------------------------//
		
		
		ResponseEntity<List<MenuConfigMst>> TallyRetryResp = RestCaller.MenuConfigMstByMenuName("TALLY RETRY");
		List<MenuConfigMst> TallyRetryList = TallyRetryResp.getBody();

		if (TallyRetryList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TALLY RETRY");
			menuConfigMst.setMenuFxml("TallyRetry.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		
		//*********************sharon***************AMBC Doctor Details*******************
		
		ResponseEntity<List<MenuConfigMst>> newdoctorMstResp = RestCaller.MenuConfigMstByMenuName("DOCTOR LIST");
		List<MenuConfigMst> newdoctorMstList = newdoctorMstResp.getBody();

		if (newdoctorMstList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("DOCTOR LIST");
			menuConfigMst.setMenuFxml("DoctorDetails.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		//*********************sharon***************AMBC Doctor Details END*******************

		//*********************sharon***************TaxLedger*******************
		
		ResponseEntity<List<MenuConfigMst>> taxLedgerResp = RestCaller.MenuConfigMstByMenuName("TAX LEDGER");
		List<MenuConfigMst> taxLedgerList = taxLedgerResp.getBody();

		if (taxLedgerList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("TAX LEDGER");
			menuConfigMst.setMenuFxml("TaxLedger.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		//*********************sharon***************TaxLedger END*******************
		ResponseEntity<List<MenuConfigMst>> profitAndLossConfigMstResp = RestCaller.MenuConfigMstByMenuName("PROFITANDLOSSCONFIG");
		List<MenuConfigMst> profitAndLossConfigMstList = profitAndLossConfigMstResp.getBody();

		if (profitAndLossConfigMstList.size() == 0) {
			
			MenuConfigMst menuConfigMst = new MenuConfigMst();
			menuConfigMst.setMenuName("PROFIT AND LOSS CONFIG");
			menuConfigMst.setMenuFxml("ProfitAndLoss.fxml");
			menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

			ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

		}
		//************************************BalanceSheetConfig*******************
				ResponseEntity<List<MenuConfigMst>> balanceSheetConfigResp = RestCaller.MenuConfigMstByMenuName("BALANCE SHEET CONFIG");
				List<MenuConfigMst> balanceSheetConfigList = balanceSheetConfigResp.getBody();

				if (balanceSheetConfigList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("BALANCE SHEET CONFIG");
					menuConfigMst.setMenuFxml("BalanceSheetConfig.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				//*********************Sibi***************PharmacyRetail*******************
				ResponseEntity<List<MenuConfigMst>>  pharmacyRetailResp = RestCaller.MenuConfigMstByMenuName("PHARMACY RETAIL");
				List<MenuConfigMst> pharmacyRetailList = pharmacyRetailResp.getBody();

				if (pharmacyRetailList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("PHARMACY RETAIL");
					menuConfigMst.setMenuFxml("PharmacyRetail.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}

				//*********************Sibi***************ImportPurchaseSummaryReport*******************
				ResponseEntity<List<MenuConfigMst>>  importPurchaseSummaryResp = RestCaller.MenuConfigMstByMenuName("IMPORT PURCHASE SUMMARY REPORT PHARMACY");
				List<MenuConfigMst> importPurchaseSummaryList = importPurchaseSummaryResp.getBody();

				if (importPurchaseSummaryList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("IMPORT PURCHASE SUMMARY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("ImportPurchaseSummaryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
		

				
				//=======================GSTReport===========anandu===================

		
				ResponseEntity<List<MenuConfigMst>>  gstOutputDetailReportResp = RestCaller.MenuConfigMstByMenuName("GST OUTPUT DETAIL REPORT PHARMACY");
				List<MenuConfigMst> gstReportList = gstOutputDetailReportResp.getBody();

				if (gstReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("GST OUTPUT DETAIL REPORT PHARMACY");
					menuConfigMst.setMenuFxml("GSTOutputDetailReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				

				ResponseEntity<List<MenuConfigMst>>  gstOutputSummaryReportResp = RestCaller.MenuConfigMstByMenuName("GST OUTPUT SUMMARY REPORT PHARMACY");
				List<MenuConfigMst> gstOutputSummaryReportList = gstOutputSummaryReportResp.getBody();

				if (gstOutputSummaryReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("GST OUTPUT SUMMARY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("GSTOutputSummaryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				ResponseEntity<List<MenuConfigMst>>  gstInputDetailReport = RestCaller.MenuConfigMstByMenuName("GST INPUT DETAIL REPORT PHARMACY");
				List<MenuConfigMst> gstInputDetailReportList = gstInputDetailReport.getBody();

				if (gstInputDetailReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("GST INPUT DETAIL REPORT PHARMACY");
					menuConfigMst.setMenuFxml("GSTInputDetailReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				ResponseEntity<List<MenuConfigMst>>  gstInputSummaryReport = RestCaller.MenuConfigMstByMenuName("GST INPUT SUMMARY REPORT PHARMACY");
				List<MenuConfigMst> gstInputSummaryReportList = gstInputSummaryReport.getBody();

				if (gstInputSummaryReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("GST INPUT SUMMARY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("GSTInputSummaryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				//======================end gst report=================
				
				//==============================due to conflict============anandu===========
				//ResponseEntity<List<MenuConfigMst>>  serverReportResp = RestCaller.MenuConfigMstByMenuName("SERVER REPORTS");
				
				
				ResponseEntity<List<MenuConfigMst>>  serverReportResp = RestCaller.MenuConfigMstByMenuName("CONSOLIDATED REPORTS");
				List<MenuConfigMst> serverReportList = serverReportResp.getBody();

				if (serverReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("CONSOLIDATED REPORTS");
					menuConfigMst.setMenuFxml("ServerReports.fxml");
					menuConfigMst.setMenuDescription("SUBACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				ResponseEntity<List<MenuConfigMst>>  trailbalanceServerResp = RestCaller.MenuConfigMstByMenuName("TRIALBALANCE CONSOLIDATED");
				List<MenuConfigMst> trailbalanceServerReportList = trailbalanceServerResp.getBody();

				if (trailbalanceServerReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("TRIALBALANCE CONSOLIDATED");
					menuConfigMst.setMenuFxml("TrailBalance-Server.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				ResponseEntity<List<MenuConfigMst>> menuConfigMstStatementOfAccountReporServertResp = RestCaller
						.MenuConfigMstByMenuName("STATEMENTS OF ACCOUNT CONSOLIDATED");
				List<MenuConfigMst> menuConfigMstStatementOfAccountReporServertList = menuConfigMstStatementOfAccountReporServertResp
						.getBody();

				if (menuConfigMstStatementOfAccountReporServertList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("STATEMENTS OF ACCOUNT CONSOLIDATED");
					menuConfigMst.setMenuFxml("AccountBalanceServer.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}

				
				

				ResponseEntity<List<MenuConfigMst>>  purchasePriceDefinitiionResp = RestCaller.MenuConfigMstByMenuName("PURCHASE PRICE DEFINITION");
				List<MenuConfigMst> purchasePriceDefinitiionList = purchasePriceDefinitiionResp.getBody();

				if (purchasePriceDefinitiionList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("PURCHASE PRICE DEFINITION");
					menuConfigMst.setMenuFxml("PurchasePriceDefinition.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				ResponseEntity<List<MenuConfigMst>>  batchWiseStockEvaluationResp = RestCaller.MenuConfigMstByMenuName("BATCH WISE STOCK EVALUATION");
				List<MenuConfigMst> batchWiseStockEvaluationList = batchWiseStockEvaluationResp.getBody();

				if (batchWiseStockEvaluationList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("BATCH WISE STOCK EVALUATION");
					menuConfigMst.setMenuFxml("BranchWiseStockEvaluation.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				
				}
				ResponseEntity<List<MenuConfigMst>>  usergrouppermissionresp = RestCaller.MenuConfigMstByMenuName("USER GROUP ASSIGNING");
				List<MenuConfigMst> usergrouppermissionList = usergrouppermissionresp.getBody();

				if (usergrouppermissionList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("USER GROUP ASSIGNING");
					menuConfigMst.setMenuFxml("UserGroupAssigning.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				ResponseEntity<List<MenuConfigMst>>  insurancewisesalesresp = RestCaller.MenuConfigMstByMenuName("INSURANCE WISE SALES REPORT");
				List<MenuConfigMst> insurancewisesalesList = insurancewisesalesresp.getBody();

				if (insurancewisesalesList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("INSURANCE WISE SALES REPORT");
					menuConfigMst.setMenuFxml("InsuranceWisesalesreport.fxml");
				}

				ResponseEntity<List<MenuConfigMst>>  wholeSaleDetailReport = RestCaller.MenuConfigMstByMenuName("WHOLE SALE DETAIL REPORT PHARMACY");
				List<MenuConfigMst> wholeSaleDetailReportList = wholeSaleDetailReport.getBody();
				

				if (wholeSaleDetailReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("WHOLE SALE DETAIL REPORT PHARMACY");
					menuConfigMst.setMenuFxml("WholeSalesDetailReportOrginal.fxml");
					

					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				
				ResponseEntity<List<MenuConfigMst>>  wholeSaleSummaryReport = RestCaller.MenuConfigMstByMenuName("WHOLE SALE SUMMARY REPORT PHARMACY");
				List<MenuConfigMst> wholeSaleSummaryReportList = wholeSaleSummaryReport.getBody();

				if (wholeSaleSummaryReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("WHOLE SALE SUMMARY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("WholeSaleSummaryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				
				ResponseEntity<List<MenuConfigMst>>  branchStockReport = RestCaller.MenuConfigMstByMenuName("BRANCH STOCK REPORT PHARMACY");
				List<MenuConfigMst> branchStockReportList = branchStockReport.getBody();

				if (branchStockReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("BRANCH STOCK REPORT PHARMACY");
					menuConfigMst.setMenuFxml("BranchStockReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				ResponseEntity<List<MenuConfigMst>>  SubscriptionValidityResp = RestCaller.MenuConfigMstByMenuName("SUBSCRIPTION VALIDITY");
				List<MenuConfigMst> SubscriptionValidityList = SubscriptionValidityResp.getBody();

				if (SubscriptionValidityList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("SUBSCRIPTION VALIDITY");
					menuConfigMst.setMenuFxml("SubscriptionValidity.fxml");

					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}

				ResponseEntity<List<MenuConfigMst>>  ConsumptionReportResp = RestCaller.MenuConfigMstByMenuName("CONSUMTPTION SUMMARY REPORT");
				List<MenuConfigMst> ConsumptionReportList = ConsumptionReportResp.getBody();

				if (ConsumptionReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("CONSUMTPTION SUMMARY REPORT");
					menuConfigMst.setMenuFxml("ConsumptionReports.fxml");

					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				
				ResponseEntity<List<MenuConfigMst>>  stockSummaryReport = RestCaller.MenuConfigMstByMenuName("PRICE WISE STOCK SUMMARY REPORT");
				List<MenuConfigMst> stockSummaryReportList = stockSummaryReport.getBody();

				if (stockSummaryReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("PRICE WISE STOCK SUMMARY REPORT");
					menuConfigMst.setMenuFxml("PricetypeWiseStockSummaryReport.fxml");

					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				ResponseEntity<List<MenuConfigMst>>  AMDCShortExpiryReport = RestCaller.MenuConfigMstByMenuName("SHORT EXPIRY REPORT PHARMACY");
				List<MenuConfigMst> AMDCShortExpiryReportList = AMDCShortExpiryReport.getBody();

				if (AMDCShortExpiryReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("SHORT EXPIRY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("ShortExpiryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				
				
				ResponseEntity<List<MenuConfigMst>>  PoliceReport = RestCaller.MenuConfigMstByMenuName("POLICE REPORT PHARMACY");
				List<MenuConfigMst> PoliceReportList = PoliceReport.getBody();

				if (PoliceReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("POLICE REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PoliceReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				ResponseEntity<List<MenuConfigMst>>  Stockmigration = RestCaller.MenuConfigMstByMenuName("STOCK MIGRATION");
				List<MenuConfigMst> StockmigrationtList = Stockmigration.getBody();

				if (StockmigrationtList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("STOCK MIGRATION");
					menuConfigMst.setMenuFxml("StockMigrationNew.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				
				ResponseEntity<List<MenuConfigMst>>  ProfitAndLossReportResp = RestCaller.MenuConfigMstByMenuName("PROFIT AND LOSS  REPORT");
				List<MenuConfigMst> ProfitAndLossReportList = ProfitAndLossReportResp.getBody();

				if (ProfitAndLossReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("PROFIT AND LOSS REPORT");
					menuConfigMst.setMenuFxml("ProfitAndLossReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
//				ResponseEntity<List<MenuConfigMst>>  DomainInitialize = RestCaller.MenuConfigMstByMenuName("DOMAIN INITIALIZE");
//				List<MenuConfigMst> DomainInitializeList = DomainInitialize.getBody();
//
//				if (DomainInitializeList.size() == 0) {
//					
//					MenuConfigMst menuConfigMst = new MenuConfigMst();
//					menuConfigMst.setMenuName("DOMAIN INITIALIZE");
//					menuConfigMst.setMenuFxml("DomainInitialize.fxml");
//					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
//
//					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
//
//				}
				
				ResponseEntity<List<MenuConfigMst>>  balanceSheet = RestCaller.MenuConfigMstByMenuName("BALANCE SHEET CONFIG REPORT");
				List<MenuConfigMst> balanceSheetList = balanceSheet.getBody();

				if (balanceSheetList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("BALANCE SHEET CONFIG REPORT");
					menuConfigMst.setMenuFxml("BalanceSheetConfigAssetAndLiabilityReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				

				ResponseEntity<List<MenuConfigMst>>  AMDCCustomerWiseSaleReport = RestCaller.MenuConfigMstByMenuName("CUSTOMER WISE SALES REPORT PHARMACY");
				List<MenuConfigMst> AMDCCustomerWiseSaleReportList = AMDCCustomerWiseSaleReport.getBody();

				if (AMDCCustomerWiseSaleReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("CUSTOMER WISE SALES REPORT PHARMACY");
					menuConfigMst.setMenuFxml("AMDCCustomerWiseSalesReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				ResponseEntity<List<MenuConfigMst>>  dailySalesSummaryReport = RestCaller.MenuConfigMstByMenuName("DAILY SALES SUMMARY REPORT PHARMACY");
				List<MenuConfigMst> dailySalesSummaryReportList = dailySalesSummaryReport.getBody();

				if (dailySalesSummaryReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("DAILY SALES SUMMARY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("DailySalesSummaryReport.fxml");

					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}

				ResponseEntity<List<MenuConfigMst>>  MasterReportSqlWindowReport = RestCaller.MenuConfigMstByMenuName("CLOUD DEMO");
				List<MenuConfigMst> MasterReportSqlWindowtList = MasterReportSqlWindowReport.getBody();

				if (MasterReportSqlWindowtList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("CLOUD DEMO");
					menuConfigMst.setMenuFxml("MasterReportSQLWindow.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}
				
				ResponseEntity<List<MenuConfigMst>>  LocalPurchaseSummaryDtlReport = RestCaller.MenuConfigMstByMenuName("LOCAL PURCHASE SUMMARY REPORT");
				List<MenuConfigMst> LocalPurchaseSummaryDtlReportList = LocalPurchaseSummaryDtlReport.getBody();

				if (LocalPurchaseSummaryDtlReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("LOCAL PURCHASE SUMMARY REPORT");
					menuConfigMst.setMenuFxml("LocalPurchaseSummaryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");
					

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

					
				}
				
				ResponseEntity<List<MenuConfigMst>>  LocalPurchaseDetailsReport = RestCaller.MenuConfigMstByMenuName("LOCAL PURCHASE DETAILS REPORT PHARMACY");
				List<MenuConfigMst> LocalPurchaseDetailsReportList = LocalPurchaseDetailsReport.getBody();

				if (LocalPurchaseDetailsReportList.size() == 0) {
					
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("LOCAL PURCHASE DETAILS REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacyLocalPurchaseDetailsReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);

				}		



				
				
				ResponseEntity<List<MenuConfigMst>> writeOffDetailReport = RestCaller
						.MenuConfigMstByMenuName("STOCK WRITE OFF DETAIL REPORT PHARMACY");
				List<MenuConfigMst> writeOffDetailReportList = writeOffDetailReport.getBody();

				if (writeOffDetailReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("STOCK WRITE OFF DETAIL REPORT PHARMACY");
					menuConfigMst.setMenuFxml("WriteOffReportDetail.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				
				ResponseEntity<List<MenuConfigMst>> writeOffSummaryReport = RestCaller
						.MenuConfigMstByMenuName("STOCK WRITE OFF SUMMARY REPORT PHARMACY");
				List<MenuConfigMst> writeOffSummaryReportList = writeOffSummaryReport.getBody();

				if (writeOffSummaryReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("STOCK WRITE OFF SUMMARY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("WriteOffReportSummary.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				ResponseEntity<List<MenuConfigMst>> PharmacyDayClosingReport = RestCaller
						.MenuConfigMstByMenuName("DAY CLOSING REPORT PHARMACY");
				List<MenuConfigMst> PharmacyDayClosingReportList = PharmacyDayClosingReport.getBody();

				if (PharmacyDayClosingReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("DAY CLOSING REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacyDayClosing.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}

				ResponseEntity<List<MenuConfigMst>> PharmacyBrachStockMarginReport = RestCaller
						.MenuConfigMstByMenuName("BRANCH STOCK MARGIN REPORT PHARMACY");
				List<MenuConfigMst> PharmacyBrachStockMarginReportList = PharmacyDayClosingReport.getBody();

				if (PharmacyBrachStockMarginReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("BRANCH STOCK MARGIN REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacyBrachStockMarginReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}

				
				ResponseEntity<List<MenuConfigMst>> KitDefinitionPublishWindow = RestCaller
						.MenuConfigMstByMenuName("KIT PUBLISH WINDOW");
				List<MenuConfigMst> KitDefinitionPublishWindowList = KitDefinitionPublishWindow.getBody();

				if (KitDefinitionPublishWindowList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("KIT PUBLISH WINDOW");
					menuConfigMst.setMenuFxml("KitDefinitionPublishWindow.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				ResponseEntity<List<MenuConfigMst>> PharmacyDailySalesTransactionReport = RestCaller
						.MenuConfigMstByMenuName("DAILY SALES TRANSACTION REPORT PHARMACY");
				List<MenuConfigMst> PharmacyDailySalesTransactionReportList = PharmacyDailySalesTransactionReport.getBody();

				if (PharmacyDailySalesTransactionReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("DAILY SALES TRANSACTION REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacyDailySalesTransactionReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				ResponseEntity<List<MenuConfigMst>> PharmacyItemWiseSalesReport = RestCaller
						.MenuConfigMstByMenuName("ITEMWISE SALES REPORT PHARMACY");
				List<MenuConfigMst> PharmacyItemWiseSalesReportList = PharmacyItemWiseSalesReport.getBody();

				if (PharmacyItemWiseSalesReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("ITEMWISE SALES REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacyItemWiseSalesReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				ResponseEntity<List<MenuConfigMst>> PharmacyPurchaseReturnDetailReport = RestCaller
						.MenuConfigMstByMenuName("PURCHASE RETURN DETAIL REPORT PHARMACY");
				List<MenuConfigMst> PharmacyPurchaseReturnDetailReportList = PharmacyPurchaseReturnDetailReport.getBody();

				if (PharmacyPurchaseReturnDetailReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("PURCHASE RETURN DETAIL REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacyPurchaseReturnDetailReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				ResponseEntity<List<MenuConfigMst>> PharmacyPhysicalStockVarianceReport = RestCaller
						.MenuConfigMstByMenuName("PHYSICAL STOCK VARIANCE REPORT PHARMACY");
				List<MenuConfigMst> PharmacyPhysicalStockVarianceReportList = PharmacyPhysicalStockVarianceReport.getBody();

				if (PharmacyPhysicalStockVarianceReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("PHYSICAL STOCK VARIANCE REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacyPhysicalStockVarianceReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				
				ResponseEntity<List<MenuConfigMst>> RetailSalesDetailReport = RestCaller
						.MenuConfigMstByMenuName("RETAIL SALES DETAIL REPORT");
				List<MenuConfigMst> RetailSalesDetailReportList = RetailSalesDetailReport.getBody();

				if (RetailSalesDetailReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("RETAIL SALES DETAIL REPORT");
					menuConfigMst.setMenuFxml("RetailSalesDetailReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				ResponseEntity<List<MenuConfigMst>> RetailSalesSummaryReport = RestCaller
						.MenuConfigMstByMenuName("RETAIL SALES SUMMARY REPORT");
				List<MenuConfigMst> RetailSalesSummaryReportList = RetailSalesSummaryReport.getBody();

				if (RetailSalesSummaryReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("RETAIL SALES SUMMARY REPORT");
					menuConfigMst.setMenuFxml("RetailSalesSummaryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				ResponseEntity<List<MenuConfigMst>> PurchaseReturnSummaryReport = RestCaller
						.MenuConfigMstByMenuName("PURCHASE RETURN SUMMARY REPORT");
				List<MenuConfigMst> PurchaseReturnSummaryReporttList = PurchaseReturnSummaryReport.getBody();

				if (PurchaseReturnSummaryReporttList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("PURCHASE RETURN SUMMARY REPORT");
					menuConfigMst.setMenuFxml("PurchaseReturnSummaryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				
				ResponseEntity<List<MenuConfigMst>> clientTrialBalance = RestCaller
						.MenuConfigMstByMenuName("CLIENT TRIAL BALANCE");
				List<MenuConfigMst> clientTrialBalancetList = clientTrialBalance.getBody();

				if (clientTrialBalancetList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("CLIENT TRIAL BALANCE");
					menuConfigMst.setMenuFxml("ClientTrialBalance.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				

				ResponseEntity<List<MenuConfigMst>> CategorywisestockReport = RestCaller
						.MenuConfigMstByMenuName("CATEGORYWISE STOCK MOVEMENT REPORT PHARMACY");
				List<MenuConfigMst> CategorywisestockReportList = CategorywisestockReport.getBody();

				if (CategorywisestockReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("CATEGORYWISE STOCK MOVEMENT REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacyCategoryWiseStockReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}

				
				ResponseEntity<List<MenuConfigMst>> salesReturnDetailReport = RestCaller
						.MenuConfigMstByMenuName("SALES RETURN DETAIL REPORT PHARMACY");
				List<MenuConfigMst> salesReturnDetailReportList = salesReturnDetailReport.getBody();

				if (salesReturnDetailReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("SALES RETURN DETAIL REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacySalesReturnDetailReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				ResponseEntity<List<MenuConfigMst>> salesReturnSummaryReport = RestCaller
						.MenuConfigMstByMenuName("SALES RETURN SUMMARY REPORT PHARMACY");
				List<MenuConfigMst> salesReturnSummaryReportList = salesReturnSummaryReport.getBody();

				if (salesReturnSummaryReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("SALES RETURN SUMMARY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("PharmacySalesReturnReport.fxml");

					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				

				
				
				ResponseEntity<List<MenuConfigMst>> consumptionDetailReport = RestCaller
						.MenuConfigMstByMenuName("CONSUMPTION DETAIL REPORT PHARMACY");
				List<MenuConfigMst> consumptionDetailReportList = consumptionDetailReport.getBody();

				if (consumptionDetailReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("CONSUMPTION DETAIL REPORT PHARMACY");
					menuConfigMst.setMenuFxml("ConsumptionDetailReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				ResponseEntity<List<MenuConfigMst>> consumptionSummaryReport = RestCaller
						.MenuConfigMstByMenuName("CONSUMPTION SUMMARY REPORT PHARMACY");
				List<MenuConfigMst> consumptionSummaryReportList = consumptionSummaryReport.getBody();

				if (consumptionSummaryReportList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("CONSUMPTION SUMMARY REPORT PHARMACY");
					menuConfigMst.setMenuFxml("ConsumptionSummaryReport.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				ResponseEntity<List<MenuConfigMst>> itemCorrection = RestCaller
						.MenuConfigMstByMenuName("ITEM CORRECTION");
				List<MenuConfigMst> itemCorrectionList = itemCorrection.getBody();

				if (itemCorrectionList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("ITEM CORRECTION");
					menuConfigMst.setMenuFxml("ItemCorrection.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				ResponseEntity<List<MenuConfigMst>> masterReportResp = RestCaller
						.MenuConfigMstByMenuName("MASTER REPORTS");
				List<MenuConfigMst> masterReport = masterReportResp.getBody();

				if (masterReport.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("MASTER REPORTS");
					menuConfigMst.setMenuFxml("ReportSqlCommand.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				
	//======================MAP-279====================anandu======================
				ResponseEntity<List<MenuConfigMst>> LinkedAccounts = RestCaller
						.MenuConfigMstByMenuName("LINKED ACCOUNTS");
				List<MenuConfigMst> LinkedAccountsList = LinkedAccounts.getBody();

				if (LinkedAccountsList.size() == 0) {
					MenuConfigMst menuConfigMst = new MenuConfigMst();
					menuConfigMst.setMenuName("LINKED ACCOUNTS");
					menuConfigMst.setMenuFxml("LinkedAccounts.fxml");
					menuConfigMst.setMenuDescription("ACTIONABLE MENU ITEM");

					ResponseEntity<MenuConfigMst> menuConfigMstSaved = RestCaller.saveMenuConfigMst(menuConfigMst);
				}
				
				
				

		notifyMessage(5, "Initailized...!", true);
		
		
		
		

	}
	
	
	
	
	
	
	
	

	@FXML
	private void initialize() {
		
		ResponseEntity<List<InvoiceFormatMst>> jasperResp = RestCaller.getAllJasperByPriceType();
		for (int i = 0; i < jasperResp.getBody().size(); i++) {
			cmbJasperFormat.getItems().add(jasperResp.getBody().get(i).getReportName());
		}

		cmbPriceDefenition();
		getMenuList();
		
		getMenuParentList();

 	
		
		eventBus.register(this);

		tblMenuReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					menuMst = new MenuMst();
					menuMst.setId(newSelection.getId());
					

				}
			}
		});
	
		

	}

	@FXML
	void DefaultMenu(ActionEvent event) {

		ResponseEntity<String> defaultMenuCreation = RestCaller.DefaultMenuCreation();
		showAllMenuMst();
		MapleclientApplication.mainFrameController.initialize(null, null);


	}

	private void getMenuParentList() {

		/*
		 * cmbParentMenu.getItems().clear(); ResponseEntity<List<MenuConfigMst>>
		 * menuConfigMstParentResp =
		 * RestCaller.MenuConfigMstByMenuDescription("SUBACTIONABLE MENU ITEM");
		 * List<MenuConfigMst> menuConfigMstParentList =
		 * menuConfigMstParentResp.getBody();
		 * 
		 * for(MenuConfigMst menuConfig : menuConfigMstParentList) {
		 * cmbParentMenu.getItems().add(menuConfig.getMenuName()); }
		 */
	}

	private void getMenuList() {
		/*
		 * cmbMenu.getItems().clear();
		 * 
		 * ResponseEntity<List<MenuConfigMst>> menuConfigMstResp =
		 * RestCaller.getAllMenuConfigMst(); List<MenuConfigMst> menuConfigMstList =
		 * menuConfigMstResp.getBody();
		 * 
		 * for(MenuConfigMst menuConfig : menuConfigMstList) {
		 * cmbMenu.getItems().add(menuConfig.getMenuName()); }
		 */
	}

	@FXML
	void setMenuList(MouseEvent event) {

		getMenuList();

	}

	@FXML
	void setMenuParentList(MouseEvent event) {

		getMenuParentList();

	}

	@FXML
	void DELETE(ActionEvent event) {

		if (null != menuMst) {
			if (null != menuMst.getId()) {
				ResponseEntity<MenuMst> menuList = RestCaller.getMenuMstById( menuMst.getId());
				ResponseEntity<MenuConfigMst> menuConfigMst = RestCaller.MenuConfigMstById(menuList.getBody().getMenuId());
				ResponseEntity<List<ProcessPermissionMst>> process = RestCaller.getprocesspermissionMstByProcessName(menuConfigMst.getBody().getMenuName());
				for(int i =0;i<process.getBody().size();i++)
				{
					RestCaller.processPermissionMstDelete(process.getBody().get(i).getId());
				}
				RestCaller.DeleteMenuMstById(menuMst.getId());
				//showAllMenuMst();

			}
		}
		//MapleclientApplication.mainFrameController.initialize(null, null);

	}

	@FXML
	void SAVE(ActionEvent event) {

		if (txtMenuName.getText().isEmpty()) {
			notifyMessage(3, "Please select menu", false);
			txtMenuName.requestFocus();
			return;
		}

		menuMst = new MenuMst();

		ResponseEntity<List<MenuConfigMst>> menuConfig = RestCaller.MenuConfigMstByMenuName(txtMenuName.getText());
		List<MenuConfigMst> menuConfigList = menuConfig.getBody();

		if (menuConfigList.size() == 0) {
			notifyMessage(3, "Menu not found", false);
			return;
		}

		menuMst.setMenuId(menuConfigList.get(0).getId());

		if (!txtParentMenu.getText().isEmpty()) {
			ResponseEntity<List<MenuConfigMst>> menuConfigMst = RestCaller
					.MenuConfigMstByMenuName(txtParentMenu.getText());
			List<MenuConfigMst> menuConfigMstList = menuConfigMst.getBody();

			if (menuConfigMstList.size() == 0) {
				notifyMessage(3, "Parent Menu not found", false);
				return;
			}

			menuMst.setParentId(menuConfigMstList.get(0).getId());

		}

		menuMst.setBranchCode(SystemSetting.systemBranch);
		ResponseEntity<MenuMst> menuMstResp = RestCaller.saveMenuMst(menuMst);
		menuMst = menuMstResp.getBody();

		txtMenuName.clear();
		txtParentMenu.clear();

		menuMst = null;


		//showAllMenuMst();
		//MapleclientApplication.mainFrameController.initialize(null, null);


		//showAllMenuMst();
		//MapleclientApplication.mainFrameController.initialize(null, null);
//		showAllMenuMst();
		MapleclientApplication.mainFrameController.initialize(null, null);

	}

	private void showAllMenuMst() {

		ResponseEntity<List<MenuMst>> menuMstResp = RestCaller.getAllMenuMst();
		menuList = FXCollections.observableArrayList(menuMstResp.getBody());
		for (MenuMst menu : menuList) {
			ResponseEntity<MenuConfigMst> menuConfig = RestCaller.MenuConfigMstById(menu.getMenuId());
			MenuConfigMst menuConfigMst = menuConfig.getBody();

			if(null != menuConfigMst)
			{
				menu.setMenuName(menuConfigMst.getMenuName());

			}
			

			if (null != menu.getParentId()) {
				ResponseEntity<MenuConfigMst> menuConfigResp = RestCaller.MenuConfigMstById(menu.getParentId());
				MenuConfigMst menuConfigMstParent = menuConfigResp.getBody();

				if(null != menuConfigMstParent)
				{
					menu.setParentName(menuConfigMstParent.getMenuName());

				}

			}

		}

		tblMenuReport.setItems(menuList);

		clMenu.setCellValueFactory(cellData -> cellData.getValue().getMenuNameProperty());
		clParentMenu.setCellValueFactory(cellData -> cellData.getValue().getParentNameProperty());

	}

	@FXML
	void SHOWALL(ActionEvent event) {

		showAllMenuMst();

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	private void loadMenuPopup() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/menuPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupMenulistner(MenuEvent menuEvent) {

		System.out.print("inside the menu event subscribe");
		Stage stage = (Stage) btnDelete.getScene().getWindow();
		if (stage.isShowing()) {

			txtMenuName.setText(menuEvent.getMenuName());

			System.out.print(txtMenuName.getText());
		}
	}

	@FXML
	void onEnterMenuName(ActionEvent event) {
		loadMenuPopup();
	}

	private void loadParentMenuPopup() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/parentMenu.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void onEnterParentMenu(ActionEvent event) {
		loadParentMenuPopup();
	}

	@Subscribe
	public void popupParentMenulistner(ParentMenuEvent parentMenuEvent) {

		System.out.print("inside the menu event subscribe");
		Stage stage = (Stage) btnDelete.getScene().getWindow();
		if (stage.isShowing()) {

			txtParentMenu.setText(parentMenuEvent.getMenuName());

			System.out.print(txtParentMenu.getText());
		}
	}

	@FXML
	void actionUpdateJsperFormat(ActionEvent event) {

		String priceTypeId;
		if (null != cmbJasperFormat.getSelectionModel().getSelectedItem()
				&& null != cmbPriceId.getSelectionModel().getSelectedItem()) {
			ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
					.getPriceDefenitionByName(cmbPriceId.getSelectionModel().getSelectedItem().toString());

			if (priceDefenitionSaved.getBody().size() > 0) {
				priceTypeId = priceDefenitionSaved.getBody().get(0).getId();
			} else {
				notifyMessage(3, "Please select price type", false);
				return;
			}
			String msg = RestCaller.updateCustomerMstByPriceTypeAndReportName(
					cmbJasperFormat.getSelectionModel().getSelectedItem(), priceTypeId);
		} else if (null != cmbJasperFormat.getSelectionModel().getSelectedItem()
				&& null == cmbPriceId.getSelectionModel().getSelectedItem()) {
			String msg = RestCaller
					.updateCustomerMstByReportName(cmbJasperFormat.getSelectionModel().getSelectedItem());

		}
		notifyMessage(3, "UPDATED", true);

	}

	private void cmbPriceDefenition() {

		cmbPriceId.getItems().clear();

		System.out.println("=====PriceTypeOnKEyPress===");
		ArrayList priceType = new ArrayList();

		priceType = RestCaller.getPriceDefinition();
		Iterator itr = priceType.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object priceLevelName = lm.get("priceLevelName");
			Object id = lm.get("id");
			if (id != null) {
				cmbPriceId.getItems().add((String) priceLevelName);

			}

		}

	}

	@FXML
	void actionClear(ActionEvent event) {

		cmbJasperFormat.getSelectionModel().clearSelection();
		cmbPriceId.getSelectionModel().clearSelection();
	}
	 @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }

}
