package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.JobCardEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class jobCardPopupCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<JobCardHdr> jobCardList = FXCollections.observableArrayList();
	boolean initializedCalled = false;
	JobCardEvent jobCardEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();


    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<JobCardHdr> tblJocCard;

    @FXML
    private TableColumn<JobCardHdr, String> clCustName;

    @FXML
    private TableColumn<JobCardHdr, String> clVoucherNo;

    @FXML
    private TableColumn<JobCardHdr, String> clItemName;

    @FXML
    private Button btnCancel;

    @FXML
    void OnKeyPress(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}  else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		}

    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblJocCard.requestFocus();
			tblJocCard.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();

    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    
    @FXML
	private void initialize() {
		if (initializedCalled)
			return;

		initializedCalled = true;

		jobCardEvent = new JobCardEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);

		btnCancel.setCache(true);
		
		LoadItemPopupBySearch();
		

		tblJocCard.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					
					jobCardEvent = new JobCardEvent();
					jobCardEvent.setCustomerName(newSelection.getCustomerName());
					jobCardEvent.setItemName(newSelection.getItemName());
					jobCardEvent.setJobCardId(newSelection.getId());
					jobCardEvent.setServiceNo(newSelection.getServiceNo());
					jobCardEvent.setCustomerId(newSelection.getCustomerId());
					
					eventBus.post(jobCardEvent);
					
				}
			}

		});

	}

	private void LoadItemPopupBySearch() {
		
		ResponseEntity<List<JobCardHdr>> jobCardDtl = RestCaller.JobCardHdrByComanyAndStatus();
		jobCardList = FXCollections.observableArrayList(jobCardDtl.getBody()); 
		
		fillTable();
	}

	private void fillTable() {
		
		for(JobCardHdr jobcard : jobCardList)
		{
			jobcard.setServiceNo(jobcard.getServiceInDtl().getServiceInHdr().getVoucherNumber());
			jobcard.setItemName(jobcard.getServiceInDtl().getItemName());
			ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadsById(jobcard.getCustomerId());
			AccountHeads accountHeads = accountHeadsResp.getBody();
			if(null == accountHeads)
			{
				return;
			}
			jobcard.setCustomerName(accountHeads.getAccountName());
		}
		
		tblJocCard.setItems(jobCardList);
		clCustName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getServiceNoProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

		
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
