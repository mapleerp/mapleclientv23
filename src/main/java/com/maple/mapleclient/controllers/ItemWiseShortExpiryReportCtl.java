package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ShortExpiryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRException;

public class ItemWiseShortExpiryReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<ShortExpiryReport> shortExpiryList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();

    @FXML
    private TextField txtItemName;

    @FXML
    private ListView<String> lstItem;

    @FXML
    private Button btnShowReport;

    @FXML
    private Button btPrintReport;

    @FXML
    private Button btnAdd;

    @FXML
    private TableView<ShortExpiryReport> tblShortExpiry;

    @FXML
    private TableColumn<ShortExpiryReport, String> clItemName;

    @FXML
    private TableColumn<ShortExpiryReport, String> clBatch;

    @FXML
    private TableColumn<ShortExpiryReport, LocalDate> clExpiryDate;

    @FXML
    private TableColumn<ShortExpiryReport, LocalDate> clupdatedDate;

    @FXML
    private TableColumn<ShortExpiryReport, Number> clQty;

    @FXML
    private Button btnClear;
    @FXML
	private void initialize() {
		eventBus.register(this);
		 lstItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    }
    
    @FXML
    void printReport(ActionEvent event) {
	List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	try {
			JasperPdfReportService.CategoryWiseShortExpiryReport(cat,SystemSetting.systemBranch,SystemSetting.UtilDateToString(SystemSetting.getSystemDate()));
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    
    
    @FXML
    void clear(ActionEvent event) {

    	
    	txtItemName.clear();
    	tblShortExpiry.getItems().clear();
    	lstItem.getItems().clear();
    }
    
    private void fillTable()
	{
		tblShortExpiry.setItems(shortExpiryList);
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
    	clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getexpiryDateProperty());
    	clupdatedDate.setCellValueFactory(cellData -> cellData.getValue().getupdatedDateProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
	}
    @FXML
    void showReport(ActionEvent event) {
    	tblShortExpiry.getItems().clear();
    	List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	ResponseEntity<List<ShortExpiryReport>> getSavedItems = RestCaller.getItemWiseExpiry(cat);
    	shortExpiryList = FXCollections.observableArrayList(getSavedItems.getBody());
    	fillTable();
    }

    @FXML
    void showPopUp(MouseEvent event) {
    	showStockitemPopup();
    }

    @FXML
    void AddItem(ActionEvent event) {
    	
    	lstItem.getItems().add(txtItemName.getText());
    	lstItem.getSelectionModel().select(txtItemName.getText());
    	txtItemName.clear();
    }

	private void showStockitemPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtItemName.setText(itemPopupEvent.getItemName());
		
			// txtQty.setText(Integer.toString(itemPopupEvent.getQty()));

		}

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
