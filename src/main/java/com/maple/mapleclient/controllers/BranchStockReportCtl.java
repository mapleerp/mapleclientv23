package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.BranchStockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;




public class BranchStockReportCtl {
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<BranchStockReport> branchStockReportList = FXCollections.observableArrayList();
    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnPrintReport;

    @FXML
    private Button btnGenerate;
    
    @FXML
    private ComboBox<String> cmbBranch;
    @FXML
    private Button btnClear;
    
    @FXML
    private ListView<String> lstItem;



    @FXML
    private TableView<BranchStockReport> tblbranchstockreport;

    @FXML
    private TableColumn<BranchStockReport, String> clgroupName;

    @FXML
    private TableColumn<BranchStockReport, String> clitemName;

    @FXML
    private TableColumn<BranchStockReport, String> clbatchCode;

    @FXML
    private TableColumn<BranchStockReport, String> clitemCode;

    @FXML
    private TableColumn<BranchStockReport, String> clexpiryDate;

    @FXML
    private TableColumn<BranchStockReport, Number> clqty;

    @FXML
    private TableColumn<BranchStockReport, Number> clrate;

    @FXML
    private TableColumn<BranchStockReport, Number> clamount;
    
    @FXML
    private TextField txtCategoryName;
    
    @FXML
    private Button btnAdd;
    
    @FXML
    void clear(ActionEvent event) {
    	
    	dpDate.setValue(null);
		
		  cmbBranch.setValue(null);
		  lstItem.getItems().clear();
		  tblbranchstockreport.getItems().clear();
		  

    }
    
    @FXML
    void AddItem(ActionEvent event) {
    	
    	lstItem.getItems().add(txtCategoryName.getText());
    	lstItem.getSelectionModel().select(txtCategoryName.getText());
    	txtCategoryName.clear();
    

    }

    

    @FXML
    void onEnterGrop(ActionEvent event) {

    	 loadCategoryPopup();
    }

    private void loadCategoryPopup() {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@FXML
    void GenerateReport(ActionEvent event) {

List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
        
    	String cat="";
    	String temp="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	 java.util.Date uDate = Date.valueOf(dpDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

	if(null==dpDate.getValue())	
	{
		notifyMessage(3, "please select date", false);
		dpDate.requestFocus();
		return;
	}
	if(null==cmbBranch.getValue()) {
		notifyMessage(5, "select Branch",false);
		return;
	}
		
	if(cat.equalsIgnoreCase(temp))
	{
		ResponseEntity<List<BranchStockReport>> branchStockReportResp=RestCaller.getPharmacyStockReportBranchWise(startDate,cmbBranch.getSelectionModel().getSelectedItem());
		branchStockReportList = FXCollections.observableArrayList(branchStockReportResp.getBody());

		fillTable();
	}
	if(cat!=temp)
	{
		ResponseEntity<List<BranchStockReport>> branchStockReportResp=RestCaller.getPharmacyStockReportBranchWiseAndItems(startDate,cmbBranch.getSelectionModel().getSelectedItem(),cat);
		branchStockReportList = FXCollections.observableArrayList(branchStockReportResp.getBody());

		fillTable();
	}
		
		
		
    }

    private void fillTable() {
    	
    		tblbranchstockreport.setItems(branchStockReportList);

    		
    		clgroupName.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
    		clitemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    		clbatchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
    		clitemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
    		clexpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
    		clqty.setCellValueFactory(cellData -> cellData.getValue().getQuantityProperty());
    		clrate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
    		clamount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    		
    	
		
	}
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    
    

	@FXML
	private void initialize() {
		
    	
    	 lstItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		 setBranches(); eventBus.register(this);
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		tblbranchstockreport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getItemName()) {

					
					fillTable();


				}
			}
		});

	}
	@FXML
    void PrintReport(ActionEvent event) {
		
List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();
        
    	String cat="";
    	String temp="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	 java.util.Date uDate = Date.valueOf(dpDate.getValue());
			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

	if(null==dpDate.getValue())	
	{
		notifyMessage(3, "please select date", false);
		dpDate.requestFocus();
		return;
	}
	if(null==cmbBranch.getValue()) {
		notifyMessage(5, "select Branch",false);
		return;
	}
		
	if(cat.equalsIgnoreCase(temp))
	{
		
          try {
			
			JasperPdfReportService.PharmacyStockReportBranchWise(startDate,cmbBranch.getSelectionModel().getSelectedItem());
			
		} 
		   catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		
	}
	if(cat!=temp)
	{
		
           try {
			
			JasperPdfReportService.PharmacyStockReportBranchWiseAndItems(startDate,cmbBranch.getSelectionModel().getSelectedItem(),cat);
			
		} 
		   catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	
		
	}
		
		
  }
	
	
	private void setBranches() {
   		
   		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
   		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
   		branchMstList = branchMstRep.getBody();
   		
   		for(BranchMst branchMst : branchMstList)
   		{
   			cmbBranch.getItems().add(branchMst.getBranchCode());
   		
   			
   		}
   		 
   	}
	
	 @Subscribe
		public void popupOrglistner(CategoryEvent CategoryEvent) {

			Stage stage = (Stage) btnGenerate.getScene().getWindow();
			if (stage.isShowing()) {

				
				txtCategoryName.setText(CategoryEvent.getCategoryName());
		
			

			}
}

}
