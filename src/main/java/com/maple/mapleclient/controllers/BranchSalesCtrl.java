package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class BranchSalesCtrl {
	
	String taskid;
	String processInstanceId;
	
	
	
	 @FXML
	    private CheckBox chkBranchsales;

	    @FXML
	    private TextField txtCustomer;

	    @FXML
	    private TextField txtAddress;

	    @FXML
	    private TextField txtGstnumber;

	    @FXML
	    private TextField txtDrivehicle;

	    @FXML
	    private Button btnCreatecustomer;

	    @FXML
	    private TextField txtItemname;

	    @FXML
	    private TextField txtRate;

	    @FXML
	    private TextField txtBarcode;

	    @FXML
	    private TextField txtBatch;

	    @FXML
	    private TextField txtQty;

	    @FXML
	    private ComboBox<?> cmbPricelevel;

	    @FXML
	    private ComboBox<?> cmbUnit;

	    @FXML
	    private TextField txtTax;

	    @FXML
	    private Button btnDeleterow;

	    @FXML
	    private Button btnAdditem;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TextField txtInvoicediscount;

	    @FXML
	    private TextField txtItemwisediscount;

	    @FXML
	    private TextField txtInvoiceamount;

	    @FXML
	    void additem(ActionEvent event) {

	    }

	    @FXML
	    void createcustomer(ActionEvent event) {

	    }

	    @FXML
	    void deleterow(ActionEvent event) {

	    }

	    @FXML
	    void submit(ActionEvent event) {

	    }
	    @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	       private void PageReload() {
	       	
	 }

}
