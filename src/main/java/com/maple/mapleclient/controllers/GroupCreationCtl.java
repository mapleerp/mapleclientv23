package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
 import com.maple.mapleclient.entity.BranchMst;
  import com.maple.mapleclient.entity.GroupMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
 import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
@Component
public class GroupCreationCtl 
{
	String taskid;
	String processInstanceId;
	EventBus eventBus = EventBusFactory.getEventBus();
 

	private ObservableList< GroupMst> groupCreationList = FXCollections.observableArrayList();
	private ObservableList< GroupMst> groupCreationList1 = FXCollections.observableArrayList();
  	GroupMst groupcreation= null;
 	private ObservableList< BranchMst> branchList1 = FXCollections.observableArrayList();
	GroupMst groupcreation1= null;
 	
	 @FXML
	    private Button btnsave;

	    @FXML
	    private Button btndelete;

 
	    @FXML
	    private ComboBox<String> branchcode;
 
	    @FXML
	    private Button showall;


	    @FXML
	    private TextField groupname;

	    @FXML
	    private TableView<GroupMst> grouptbl;

	    @FXML
	    private TableColumn<GroupMst, String> GC1;

	    @FXML
	    private TableColumn<GroupMst, String> GC2;
	    
	    	@FXML
		private void initialize() {
			
			showBranchHeadGroup();
			grouptbl.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getGroupName() && newSelection.getGroupName().length() > 0) {
 		    		
						if (null != newSelection.getId()) {
							
							groupcreation = new GroupMst();
							groupcreation.setId(newSelection.getId());


						}

					}
				}
			});
			
		}
		  private void showBranchHeadGroup()
		    {
		    	ArrayList cat = new ArrayList();
				RestTemplate restTemplate1 = new RestTemplate();
				cat = RestCaller.SearchBranchs();
				Iterator itr1 = cat.iterator();
				while (itr1.hasNext()) {
					LinkedHashMap lm = (LinkedHashMap) itr1.next();
					Object unitName = lm.get("branchCode");
					Object unitId = lm.get("id");
					if (unitId != null) {
						branchcode.getItems().add((String) unitName);
						BranchMst newUnitMst = new BranchMst();
						
						newUnitMst.setId((String) unitId);
						branchList1.add(newUnitMst);
					}
				}
		    }

	    @FXML
	    void showall(ActionEvent event) {
	    	
	    	groupCreationList.clear();
 	    	ResponseEntity<List<GroupMst>> respentitygroup = RestCaller.returnValueOfGroupmst();
 	 	    
 	    	groupCreationList1 = FXCollections.observableArrayList(respentitygroup.getBody());
 	    		if(null != groupCreationList1)
 	    		{
 	    			for(GroupMst groupMst : groupCreationList1 )
 	    			{
 	    				groupCreationList.add(groupMst);
 	    			}
 	    			grouptbl.setItems(groupCreationList);
 	    			filltable();
 	    		}
 	        }

	    
	    @FXML
	    void delete(ActionEvent event) {
	    	
 
				
				
  			 
 				if (null != groupcreation) {
					if(null != groupcreation.getId())
					{

					RestCaller.deleteGroupMst(groupcreation.getId());
 					notifyMessage(5,"Deleted!!!");

 				 	ResponseEntity<List<GroupMst>> respentitygroup = RestCaller.getGroupMst();
				 

 				 	groupCreationList = FXCollections.observableArrayList(respentitygroup.getBody());
				 	if(null != groupCreationList)
		    		{
				 		grouptbl.setItems(groupCreationList);
		    		}
					grouptbl.getItems().clear();
 					}
 
				}
		 

 
	    }
	    public void notifyMessage(int duration, String msg) {

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}
	    @FXML
	    void save(ActionEvent event) 
	    {
	    	groupcreation=new GroupMst();
	    	groupcreation.setGroupName(groupname.getText());
	    	groupcreation.setBranchCode(branchcode.getValue());
	    	String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"GR");
	    	groupcreation.setId(vNo+SystemSetting.getUser().getCompanyMst().getId());
 	    	ResponseEntity< GroupMst> respentity = RestCaller.groupCreation(groupcreation);
	    	
	    	groupcreation = respentity.getBody();
	    	groupCreationList.add(groupcreation);
	    	grouptbl.setItems(groupCreationList);
	    	filltable();
	    	groupname.setText("");
	    	branchcode.setValue("");
 

	    }
      public void filltable() {
    		
 	    GC1.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
	    GC2.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
    
    	}
      @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

  
	}
	
	
	
	

