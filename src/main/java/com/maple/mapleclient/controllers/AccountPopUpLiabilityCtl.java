package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountPopUpAssetAndLiability;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.events.AccountPopUpLiabilityEvent;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class AccountPopUpLiabilityCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<AccountPopUpAssetAndLiability> accountList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

	private EventBus eventBus = EventBusFactory.getEventBus();
	
	AccountPopUpLiabilityEvent accountPopUpLiabilityEvent;
	
	   @FXML
	    private TextField accountSearchBox;

	    @FXML
	    private TableView<AccountPopUpAssetAndLiability> tblAccountAssetOrLiability;

	    @FXML
	    private TableColumn<AccountPopUpAssetAndLiability, String> clmnAccount;

	    @FXML
	    private Button btnOk;

	    @FXML
	    private Button btnCancel;

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnOk.getScene().getWindow();
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB) {

			} else {
				accountSearchBox.requestFocus();
			}
	    }
	    @FXML
	    void OnKeyPresstxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tblAccountAssetOrLiability.requestFocus();
				tblAccountAssetOrLiability.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnOk.getScene().getWindow();
				stage.close();
			}
	    }
	    @FXML
	    void actionCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
	    }

	    @FXML
	    void actionOK(ActionEvent event) {
	    	Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
	    }
	    @FXML
		private void initialize() {
	    	LoadAccountNameBySearch("");
			
			accountSearchBox.textProperty().bindBidirectional(SearchString);
			eventBus.register(this);
			btnOk.setDefaultButton(true);
			btnCancel.setCache(true);
			
			tblAccountAssetOrLiability.setItems(accountList);
			
			clmnAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
			
			tblAccountAssetOrLiability.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {
					
						accountPopUpLiabilityEvent = new AccountPopUpLiabilityEvent();
						accountPopUpLiabilityEvent.setAccountId(newSelection.getId());
						accountPopUpLiabilityEvent.setAccountName(newSelection.getAccountName());
						eventBus.post(accountPopUpLiabilityEvent);
					}
				}
			});


			SearchString.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

					LoadAccountNameBySearch((String) newValue);
				}
			});

	    }
	    
	    
	    private void LoadAccountNameBySearch(String searchData) {

			accountList.clear();
			
			ResponseEntity<AccountHeads> accountId=RestCaller.getAccountHeadByName("LIABLITY");
			String assetId=accountId.getBody().getId();
			ResponseEntity<List<AccountHeads>> accountDedtails = RestCaller.SearchAccountByNameByParentId(assetId, searchData);
			
			List<AccountHeads> accountListResp = accountDedtails.getBody();
			
			for(AccountHeads accountHeads : accountListResp) {
				AccountPopUpAssetAndLiability account = new AccountPopUpAssetAndLiability();
				account.setAccountName(accountHeads.getAccountName());
				account.setId(accountHeads.getId());
				accountList.add(account);
			}
		
			
			

			return;

		}
	    
	    
	    @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	       private void PageReload() {
	       	
	 }
	
}
