package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.PharmacySalesReturnReport;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ReceiptModeReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PharmacySalesReturnSummaryReportCtl {
	String taskid;
	String processInstanceId;

	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PharmacySalesReturnReport> pharmacySalesReturnReport = FXCollections.observableArrayList();
	@FXML
	private ComboBox<String> cmbGroup;

	@FXML
	private Button btnShowReport;

	@FXML
	private Button btPrintReport;

	@FXML
	private Button btnAdd;

	@FXML
	private ListView<String> lstItem;

	@FXML
	private TextField txtCategoryName;

	@FXML
	private Button btnClear;

	@FXML
	private DatePicker dpFromDate;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private ComboBox<String> cmbBranch;

	@FXML
	private TableView<PharmacySalesReturnReport> tblPharmacySalesReturn;
	@FXML
	private TableColumn<PharmacySalesReturnReport, String> clInvoiceDate;
	
	@FXML
    private TableColumn<PharmacySalesReturnReport, String> clmnSalesVoucherNumber;

	@FXML
	private TableColumn<PharmacySalesReturnReport, String> clCustName;

	@FXML
	private TableColumn<PharmacySalesReturnReport, String> clVoucherNumber;

	@FXML
	private TableColumn<PharmacySalesReturnReport, String> clCurrency;
	
	   @FXML
	    private TableColumn<PharmacySalesReturnReport, String> clmnSalesMan;

	    @FXML
	    private TableColumn<PharmacySalesReturnReport, Number> clmnAmountBeforeGst;

	    @FXML
	    private TableColumn<PharmacySalesReturnReport, Number> clmnGST;

	    @FXML
	    private TableColumn<PharmacySalesReturnReport, Number> clmnAmount;

	    @FXML
	    private TableColumn<PharmacySalesReturnReport, String> clmnUser;

	@FXML
	void AddItem(ActionEvent event) {

		lstItem.getItems().add(txtCategoryName.getText());
		lstItem.getSelectionModel().select(txtCategoryName.getText());
		txtCategoryName.clear();

	}

	@FXML
	void clear(ActionEvent event) {
		lstItem.getItems().clear();
		clear();
	}

	@FXML
	void printReport(ActionEvent event) {
		if (null == cmbBranch.getValue()) {
			notifyMessage(5, "Please select branch", false);
			return;
		}

		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "Please select from date", false);
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(5, "Please select to date", false);
			return;
		}
//		if (null == txtCategoryName.getText()) {
//			notifyMessage(5, "Please select Category", false);
//			return;
//		}

		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String fdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date t1Date = Date.valueOf(dpToDate.getValue());
		String tDate = SystemSetting.UtilDateToString(t1Date, "yyy-MM-dd");

		List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();

		String cat = "";
		for (String s : selectedItems) {
			s = s.concat(";");
			cat = cat.concat(s);
		}
		String branchCode =SystemSetting.systemBranch;
		
		if(cat.equalsIgnoreCase("")) {
		try {

			JasperPdfReportService.pharmacySalesReturnSummaryReportWithoutCategory(fdate, tDate, cmbBranch.getValue());
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		}else {
			try {

				JasperPdfReportService.pharmacySalesReturnSummaryReport(fdate, tDate, cmbBranch.getValue(), cat);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		
//		clear();
	}

	@FXML
	void showPopUp(MouseEvent event) {

	}

	@FXML
	void showReport(ActionEvent event) {

		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "select from date", false);
			return;
		}
		if (null == dpToDate.getValue()) {
			notifyMessage(5, "select to date", false);
			return;
		}
		if (null == cmbBranch.getValue()) {
			notifyMessage(5, "select Branch", false);
			return;
		}
//		if (null == lstItem.getSelectionModel().getSelectedItem()) {
//			notifyMessage(5, "plz add Grouph", false);
//			return;
//		}

		List<String> selectedItems = lstItem.getSelectionModel().getSelectedItems();

		String cat = "";
		for (String s : selectedItems) {
			s = s.concat(";");
			cat = cat.concat(s);
		}
		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		
		
		
		if(cat.equalsIgnoreCase("")) {
			ResponseEntity<List<PharmacySalesReturnReport>> resp = RestCaller.getPharmacySalesReturnWithoutCategory(startDate, endDate,
				cmbBranch.getSelectionModel().getSelectedItem());
			pharmacySalesReturnReport = FXCollections.observableArrayList(resp.getBody());
		}else {
			ResponseEntity<List<PharmacySalesReturnReport>> resp = RestCaller.getPharmacySalesReturn(startDate, endDate,
					cmbBranch.getSelectionModel().getSelectedItem(), cat);
			pharmacySalesReturnReport = FXCollections.observableArrayList(resp.getBody());
		}
		tblPharmacySalesReturn.setItems(pharmacySalesReturnReport);
		filltable();
		

	}

	@FXML
	private void initialize() {
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		setBranches();
		eventBus.register(this);
		lstItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
	}

	private void setBranches() {

		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();

		for (BranchMst branchMst : branchMstList) {
			cmbBranch.getItems().add(branchMst.getBranchCode());

		}

	}

	private void loadCategoryPopup() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupOrglistner(CategoryEvent CategoryEvent) {

		Stage stage = (Stage) btPrintReport.getScene().getWindow();
		if (stage.isShowing()) {

			txtCategoryName.setText(CategoryEvent.getCategoryName());

		}

	}

	public void clear() {

		dpFromDate.setValue(null);
		dpToDate.setValue(null);
		cmbBranch.setValue(null);
		lstItem.getItems().clear();
		tblPharmacySalesReturn.getItems().clear();

	}

	@FXML
	void onEnterGrop(ActionEvent event) {

		loadCategoryPopup();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	private void filltable() {
		clInvoiceDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
		clmnSalesVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getSalesVoucherNumberProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clCustName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clmnSalesMan.setCellValueFactory(cellData -> cellData.getValue().getSalesManProperty());
		clmnAmountBeforeGst.setCellValueFactory(cellData -> cellData.getValue().getAmountBeforeGstProperty());
		clmnGST.setCellValueFactory(cellData -> cellData.getValue().getGstProperty());
		clmnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clmnUser.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
		

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

}
