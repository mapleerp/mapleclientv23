package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class LocalCustPopupCtl {
	
	String taskid;
	String processInstanceId;
	StringProperty SearchString = new SimpleStringProperty();
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<LocalCustomerMst> custList = FXCollections.observableArrayList();
	
	
	LocalCustomerEvent customerEvent;
    @FXML
    private TextField txtname;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<LocalCustomerMst> tablePopupView;

    @FXML
    private TableColumn<LocalCustomerMst, String> custName;

    @FXML
    private TableColumn<LocalCustomerMst, String> custAddress;
    

    @FXML
    private TableColumn<LocalCustomerMst, String> custPhoneNo;

    @FXML
    private Button btnCancel;
    
	@FXML
	private void initialize() {

		LoadCustomerBySearch("");

		txtname.textProperty().bindBidirectional(SearchString);

		customerEvent = new LocalCustomerEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		tablePopupView.setItems(custList);

		btnCancel.setCache(true);

		custName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		custAddress.setCellValueFactory(cellData -> cellData.getValue().getAddressProperty());
		custPhoneNo.setCellValueFactory(cellData -> cellData.getValue().getPhoneNoProperty());

	
		

		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getLocalcustomerName() && newSelection.getLocalcustomerName().length() > 0) {
					customerEvent.setLocalCustAddress(newSelection.getAddress());
					customerEvent.setLocalCustid(newSelection.getId());
					customerEvent.setLocalCustName(newSelection.getLocalcustomerName());
					customerEvent.setLocalCustPhone2(newSelection.getPhoneNO2());
					customerEvent.setLocalCustPhoneNo(newSelection.getPhoneNo());
					customerEvent.setDateOfBirth(newSelection.getDateOfBirth());
					customerEvent.setWeddingDate(newSelection.getWeddingDate());
					eventBus.post(customerEvent);
				}
			}
		});

		/*
		 * Below code will do the searching based on typed string
		 */

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadCustomerBySearch((String) newValue);
			}
		});

	}

    @FXML
    void OnKeyPress(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		if(null == customerEvent.getLocalCustName())
    		{
    			customerEvent.setLocalCustName(txtname.getText());
				eventBus.post(customerEvent);
    		}
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtname.requestFocus();
		}

    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}

    }

    @FXML
    void onCancel(ActionEvent event) {
    	
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();

    }

    @FXML
    void submit(ActionEvent event) {
    	
    	if(null == customerEvent.getLocalCustName())
		{
			customerEvent.setLocalCustName(txtname.getText());
			eventBus.post(customerEvent);
		}
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();

    }
    
	private void LoadCustomerBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		custList.clear();
		ArrayList customer = new ArrayList();
		LocalCustomerMst cust = new LocalCustomerMst();
		RestTemplate restTemplate = new RestTemplate();

		customer = RestCaller.SearchLocalCustomer(searchData);
//		customer = RestCaller.SearchCustomerByName();
		Iterator itr = customer.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			// Iterator itr2 = lm.keySet().iterator();
			// while(itr2.hasNext()) {

			Object localCustomerName = lm.get("localcustomerName");
			Object Address = lm.get("address");
			Object PhoneNo = lm.get("phoneNo");
			Object PhoneNO2 = lm.get("phoneNO2");
			Object id = lm.get("id");
			Object dob = lm.get("dateOfBirth");
			Object weddigdate = lm.get("weddingDate");

			if (null != id) {
				cust = new LocalCustomerMst();
				cust.setAddress((String)Address);
				cust.setLocalcustomerName((String)localCustomerName);
				cust.setPhoneNo((String)PhoneNo);
				cust.setPhoneNO2((String)PhoneNO2);
				cust.setId((String)id);
				if(null != dob)
				{
					cust.setDateOfBirth(SystemSetting.StringToUtilDate((String)dob, "yyyy-MM-dd"));

				}
				if(null != weddigdate)
				{
					cust.setWeddingDate(SystemSetting.StringToUtilDate((String)weddigdate, "yyyy-MM-dd"));

				}

				System.out.println(lm);

				custList.add(cust);
			}

		}
		
		

		return;

	}
	
	public void localCustomerPopupByCustomerId(String customerId) {
		
		custList.clear();
		ArrayList customer = new ArrayList();
		LocalCustomerMst cust = new LocalCustomerMst();
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<LocalCustomerMst>> localCustomer = RestCaller.getLocalCustomerByCustomerId(customerId);
		custList = FXCollections.observableArrayList(localCustomer.getBody());
		FillTable();
//		return;
	}
	private void FillTable() {
		tablePopupView.setItems(custList);

		custName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		custAddress.setCellValueFactory(cellData -> cellData.getValue().getAddressProperty());
		custPhoneNo.setCellValueFactory(cellData -> cellData.getValue().getPhoneNoProperty());

		
	}
	@Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }


}

