package com.maple.mapleclient.controllers;



import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.MachineResourceMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
public class MachineResourceCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<UnitMst> unitList = FXCollections.observableArrayList();
	private ObservableList<MachineResourceMst> machineResourceList = FXCollections.observableArrayList();
	
	
	  @FXML
	    private TextField txtMachineResourceName;

	    @FXML
	    private TextField txtCapacity;

	    @FXML
	    private Button btnSave;

	    @FXML
	    private Button btnEdit;

	    @FXML
	    private Button btnShowAll;

	    @FXML
	    private ComboBox<String> cmbUnit;

	    @FXML
	    private TableView<MachineResourceMst> tblMachineResource;

	    @FXML
	    private TableColumn<MachineResourceMst, String> clMachineResourceName;

	    @FXML
	    private TableColumn<MachineResourceMst, Number> clCapacity;

	    @FXML
	    private TableColumn<MachineResourceMst, String> clUnit;

	    @FXML
	    void SaveOnEnter(KeyEvent event) {

	    }

	    @FXML
	    void btnEditAction(ActionEvent event) {

	    }

	    @FXML
	    void capacityOnEnter(KeyEvent event) {

	    }

	    @FXML
	    void machineResourceNameOnEnter(KeyEvent event) {

	    }

	    @FXML
	    void tableToText(MouseEvent event) {

	    }

	    @FXML
	    void unitOnEnter(KeyEvent event) {

	    }

	    
	    
		@FXML
		private void initialize() {
		UnitMst unitMst = new UnitMst();
		// set unit
		ArrayList unit = new ArrayList();

		unit = RestCaller.SearchUnit();
		Iterator itr = unit.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object unitName = lm.get("unitName");
			Object unitId = lm.get("id");
			if (unitId != null) {
				cmbUnit.getItems().add((String) unitName);
				UnitMst newUnitMst = new UnitMst();
				newUnitMst.setUnitName((String) unitName);
				newUnitMst.setId((String) unitId);
				unitList.add(newUnitMst);
			}
		}
	
		
}

	    @FXML
	    void btnSaveAction(ActionEvent event) {
		
		  if(txtMachineResourceName.getText().trim().isEmpty()) {
		  
		  notifyMessage(5,"Please fill Machine Name!!!");
		  txtMachineResourceName.requestFocus();
		  
		  } else if(txtCapacity.getText().trim().isEmpty()) {
		  
		  
		  notifyMessage(5,"Please fill Capacity!!!"); 
		  txtCapacity.requestFocus();
		  
		  } else if(cmbUnit.getValue().trim().isEmpty()) {
		  
		  notifyMessage(5,"Please fill Unit !!!");
		  
		  cmbUnit.requestFocus();
		  
		  }
		 MachineResourceMst machineResource =new MachineResourceMst();
	 			System.out.print(txtCapacity.getText()+"CAPACITY ISSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
	 			System.out.print(txtMachineResourceName.getText()+"Machine resource name");
	 			
		    	machineResource.setMachineResourceName(txtMachineResourceName.getText());
		    	if(null!=txtCapacity.getText()) {
		    	machineResource.setCapcity(Double.valueOf(txtCapacity.getText()));
		    	}
				String unitName = cmbUnit.getSelectionModel().getSelectedItem().toString();
				
				ResponseEntity<UnitMst> respUnitMst=RestCaller.getUnitByName( unitName);
				 UnitMst uMst=respUnitMst.getBody();
						machineResource.setUnitId(uMst.getId());

		 		ResponseEntity<MachineResourceMst> respentity = RestCaller.saveMachineResource(machineResource);
		 		clearfileds();
	 		
		 		  ResponseEntity<List<MachineResourceMst>> responseEntity=RestCaller.machineDetails();
		 		    machineResourceList = FXCollections.observableArrayList(responseEntity.getBody());
		 		    tblMachineResource.setItems(machineResourceList);
		 		    
		 		    System.out.print(machineResourceList.size()+"observable list size isssssssssssss"); 
		 		    
		 		    filltable();
	    	
	    }

		
		public void notifyMessage(int duration, String msg) {
				System.out.println("OK Event Receid");

						Image img = new Image("done.png");
						Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
								.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
									@Override
									public void handle(ActionEvent event) {
										System.out.println("clicked on notification");
									}
								});
						notificationBuilder.darkStyle();
						notificationBuilder.show();
					}
		
		
		  
	    private void clearfileds()
	    {
	    	
	    	txtMachineResourceName.setText("");
	    	txtCapacity.setText("");
	    	cmbUnit.getSelectionModel().clearSelection();
	    }
		
	    

	    @FXML
	    void showAll(ActionEvent event) {
	    	
	    ResponseEntity<List<MachineResourceMst>> responseEntity=RestCaller.machineDetails();
	    machineResourceList = FXCollections.observableArrayList(responseEntity.getBody());
	    tblMachineResource.setItems(machineResourceList);
	    
	    System.out.print(machineResourceList.size()+"observable list size isssssssssssss"); 
	    
	    filltable();
	    }

	    
	    private void filltable()
		{
	    	clMachineResourceName.setCellValueFactory(cellData -> cellData.getValue().getMachineResourceNameProperty());
	    	clCapacity.setCellValueFactory(cellData -> cellData.getValue().getCapcityProperty());
	    	clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitIdProperty());
			
							
		}
	    @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }
}
