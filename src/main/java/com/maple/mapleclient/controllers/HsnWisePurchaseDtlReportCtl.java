package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportHsnWIsePurchaseDtlToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.HsnWisePurchaseDtlReport;
import com.maple.report.entity.SalesInvoiceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRException;

public class HsnWisePurchaseDtlReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<HsnWisePurchaseDtlReport> reportList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();

	ExportHsnWIsePurchaseDtlToExcel exportHsnWisePurchase = new ExportHsnWIsePurchaseDtlToExcel();
    @FXML
    private Button btnAdd;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private ListView<String> lsGroup;

    @FXML
    private DatePicker dpfromDate;

    @FXML
    private DatePicker dpTodate;

    @FXML
    private TextField txtGroup;
    @FXML
    private Button btnClear;

    @FXML
    private Button btnExport;
    @FXML
    private TableView<HsnWisePurchaseDtlReport> tbHsnPurchaseReport;
    @FXML
    private TableColumn<HsnWisePurchaseDtlReport, String> clDate;

    @FXML
    private TableColumn<HsnWisePurchaseDtlReport, String> clItemName;

    @FXML
    private TableColumn<HsnWisePurchaseDtlReport, String> clHsn;

    @FXML
    private TableColumn<HsnWisePurchaseDtlReport, String> clUnit;

    @FXML
    private TableColumn<HsnWisePurchaseDtlReport, Number> clQty;

    @FXML
    private TableColumn<HsnWisePurchaseDtlReport, Number> clValue;

    @FXML
    private TableColumn<HsnWisePurchaseDtlReport, Number> clRate;
    
    @FXML
    private TableColumn<HsnWisePurchaseDtlReport, Number> clGst;
    
    @FXML
   	private void initialize() {
    	dpfromDate = SystemSetting.datePickerFormat(dpfromDate, "dd/MMM/yyyy");
    	dpTodate = SystemSetting.datePickerFormat(dpTodate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	 lsGroup.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }
    @Subscribe
	public void popupOrglistner(CategoryEvent CategoryEvent) {

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtGroup.setText(CategoryEvent.getCategoryName());
		}
}
    private void loadCategoryPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    @FXML
    void onEnter(KeyEvent event) {

    	if(event.getCode()==KeyCode.ENTER)
    	{
    		loadCategoryPopup() ;
    	}
    }
    @FXML
    void actionAdd(ActionEvent event) {
    	lsGroup.getItems().add(txtGroup.getText());
    	lsGroup.getSelectionModel().select(txtGroup.getText());
    	txtGroup.clear();
    }
    @FXML
    void actionClear(ActionEvent event) {
    	lsGroup.getItems().clear();
    	dpfromDate.setValue(null);
    	dpTodate.setValue(null);
    	txtGroup.getText();
    	tbHsnPurchaseReport.getItems().clear();
    	reportList.clear();
    }
    @FXML
    void actionExport(ActionEvent event) {
    	  
      	java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpTodate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	if(selectedItems.size()==0)
    	{
    		cat="";
    	}
    	ResponseEntity<List<HsnWisePurchaseDtlReport>> hsn =RestCaller.getHsnPurchaseDtl(startDate,endDate,cat);
    	reportList = FXCollections.observableArrayList(hsn.getBody());
    	exportHsnWisePurchase.exportToExcel("SalesToTallyHsnPurchase"+startDate+".xls", reportList);

    }

    @FXML
    void actionPrint(ActionEvent event) {
    	
    	java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpTodate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	if(selectedItems.size()==0)
    	{
    		cat="";
    	}
    	try {
    		JasperPdfReportService.hsnCodeWisePurchaseDtlReport(startDate, endDate, cat);
    	} catch (JRException e) {
    		e.printStackTrace();
    	}
    }

    @FXML
    void actionShow(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpTodate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	if(selectedItems.size()==0)
    	{
    		cat="";
    	}
    	ResponseEntity<List<HsnWisePurchaseDtlReport>> hsn =RestCaller.getHsnPurchaseDtl(startDate,endDate,cat);
    	reportList = FXCollections.observableArrayList(hsn.getBody());
    	fillTable();
    }
    private void fillTable()
    {
    	tbHsnPurchaseReport.setItems(reportList);
		clGst.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clHsn.setCellValueFactory(cellData -> cellData.getValue().getHsnCodeProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
