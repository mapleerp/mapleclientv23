package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.SalesPropertiesConfigMst;
import com.maple.mapleclient.entity.SalesPropertyTransHdr;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SalesPropertyTransHdrCtl {
	
	String taskid;
	String processInstanceId;
	
	SalesPropertyTransHdr salesPropertyTransHdr = null;
	public static String salesTransHdrId=null;
	private ObservableList<SalesPropertyTransHdr> reporList = FXCollections.observableArrayList();

	private EventBus eventBus = EventBusFactory.getEventBus();

    @FXML
    private TableView<SalesPropertyTransHdr> tbProperty;

    @FXML
    private TableColumn<SalesPropertyTransHdr, String> clProperty;

    @FXML
    private TableColumn<SalesPropertyTransHdr, String> clValue;

    @FXML
    private ComboBox<String> cmbProperty;

    @FXML
    private TextField txtValue;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnDelete;
    @FXML
	private void initialize() {
    	eventBus.register(this);
    	tbProperty.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					salesPropertyTransHdr = new SalesPropertyTransHdr();
					salesPropertyTransHdr.setId(newSelection.getId());
				}
			}
		});
    	
    	ResponseEntity<List<SalesPropertyTransHdr> > salesPropertyTransHdrList = RestCaller.getSalesPropertyTransHdrBySalesHdrId(salesTransHdrId);
    	reporList = FXCollections.observableArrayList(salesPropertyTransHdrList.getBody());
    	fillTable();
    	
		ResponseEntity<List<SalesPropertiesConfigMst> > salesPropertyList = RestCaller.getAllSalesPropertyConfigMst();
		for(int i=0;i<salesPropertyList.getBody().size();i++)
		{
			cmbProperty.getItems().add(salesPropertyList.getBody().get(i).getPropertyName());
		}
    }
    @FXML
    void actionCancel(ActionEvent event) {
    	Stage stage = (Stage) btnCancel.getScene().getWindow();
    	salesTransHdrId="";
		stage.close();
    }

    @FXML
    void actionDelete(ActionEvent event) {
    	if(null == salesPropertyTransHdr)
    	{
    		return;
    	}
    	if(null ==salesPropertyTransHdr.getId())
    	{
    		return;
    	}
    	RestCaller.deleteSalesPropertyTransHdr(salesPropertyTransHdr.getId());

    	ResponseEntity<List<SalesPropertyTransHdr> > salesPropertyTransHdrList = RestCaller.getSalesPropertyTransHdrBySalesHdrId(salesTransHdrId);
    	reporList = FXCollections.observableArrayList(salesPropertyTransHdrList.getBody());
    	fillTable();
    }

    @FXML
    void actionSave(ActionEvent event) {
    	ResponseEntity<List<SalesPropertyTransHdr> > salesPropertyTransHdrList = RestCaller.getSalesPropertyTransHdrBySalesHdrId(salesTransHdrId);
    	if(salesPropertyTransHdrList.getBody().size()>0)
    	{
    	reporList = FXCollections.observableArrayList(salesPropertyTransHdrList.getBody());
    	fillTable();
    	}
    	salesPropertyTransHdr = new SalesPropertyTransHdr();
    	salesPropertyTransHdr.setValue(txtValue.getText());
    	ResponseEntity<List<SalesPropertiesConfigMst>> salesPro = RestCaller.getsalesPropertyConfigMstByPropertyName(cmbProperty.getSelectionModel().getSelectedItem());
    	if(salesPro.getBody().size()>0)
    	{
    		salesPropertyTransHdr.setPropertyId(salesPro.getBody().get(0).getId());
    	}
    	SalesTransHdr salesTrns = RestCaller.getSalesTransHdr(salesTransHdrId);
    	salesPropertyTransHdr.setSalesTransHdr(salesTrns);
    	salesPropertyTransHdr.setPropertyName(cmbProperty.getSelectionModel().getSelectedItem());
    	ResponseEntity<SalesPropertyTransHdr> resp = RestCaller.saveSalesPropertyTransHdr(salesPropertyTransHdr);
    	salesPropertyTransHdr = resp.getBody();
    	reporList.add(salesPropertyTransHdr);
    	fillTable();
    
    	txtValue.clear();
    	salesPropertyTransHdr = null;
    }
    private void fillTable()
    {
    	for(SalesPropertyTransHdr salesPro:reporList)
    	{
    		ResponseEntity<SalesPropertiesConfigMst> salesProConfig = RestCaller.getsalespropertConfigMstById(salesPro.getPropertyId());
    		salesPro.setPropertyName(salesProConfig.getBody().getPropertyName());
    	}
    	tbProperty.setItems(reporList);
		clProperty.setCellValueFactory(cellData -> cellData.getValue().getpropertyNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getvalueProperty());

    }
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


private void PageReload(String hdrId) {
	
}

}
