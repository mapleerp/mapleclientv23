package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class SalesDtlCtl {
	
	String taskid;
	String processInstanceId;

    @FXML
    private Button btnAddcustomer;

    @FXML
    private ChoiceBox<?> choiceSalestype;

    @FXML
    private TextField txtItemname;

    @FXML
    private TextField txtExpiry;

    @FXML
    private TextField txtBarcode;

    @FXML
    private TextField txtQty;

    @FXML
    private TextField txtBatch;

    @FXML
    private ChoiceBox<?> choiceUnit;

    @FXML
    private TextField txtRate;

    @FXML
    private TextField txtTax;

    @FXML
    private Button btnAdditem;

    @FXML
    private Button btnUnhold;

    @FXML
    private Button btnHold;

    @FXML
    private Button btnDeleterow;

    @FXML
    private TableView<?> tableItemDtls;

    @FXML
    private TableColumn<?, ?> colSrl;

    @FXML
    private TableColumn<?, ?> colItemName;

    @FXML
    private TableColumn<?, ?> colBarcode;

    @FXML
    private TableColumn<?, ?> colQty;

    @FXML
    private TableColumn<?, ?> colTaxRate;

    @FXML
    private TableColumn<?, ?> colUnitRate;

    @FXML
    private TableColumn<?, ?> colAmount;

    @FXML
    private TableColumn<?, ?> colCess;

    @FXML
    private TableColumn<?, ?> colExpiryDate;

    @FXML
    private ChoiceBox<?> choiceCardtype;

    @FXML
    private TextField txtCardno;

    @FXML
    private TextField txtcardAmount;

    @FXML
    private Button btnSave;

    @FXML
    private TextField txtPaidamount;

    @FXML
    private TextField txtCashtopay;

    @FXML
    private TextField txtChangeamount;

    @FXML
    private RadioButton radioCash;

    @FXML
    private RadioButton radioCredit;

    @FXML
    void addCustomer(ActionEvent event) {

    }

    @FXML
    void addItem(ActionEvent event) {

    }

    @FXML
    void cashRadio(ActionEvent event) {

    }

    @FXML
    void creditRadio(ActionEvent event) {

    }

    @FXML
    void deleteRow(ActionEvent event) {

    }

    @FXML
    void hold(ActionEvent event) {

    }

    @FXML
    void save(ActionEvent event) {

    }

    @FXML
    void unHold(ActionEvent event) {

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }

}
