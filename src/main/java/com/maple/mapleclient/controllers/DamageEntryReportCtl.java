package com.maple.mapleclient.controllers;

import java.util.Date;

import org.controlsfx.control.Notifications;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class DamageEntryReportCtl {
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpEndDate;

    @FXML
    private DatePicker dpStartDate;

    @FXML
    private Button btnGenerateReport;
    @FXML
   	private void initialize() {
       	dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
       	dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
    }
    @FXML
    void ActionGenerateReport(ActionEvent event) {
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(5, "Please select start date", false);
    		return;
    		
    	} else if (null == dpEndDate.getValue()) {
    		notifyMessage(5, "Please select end date", false);
    		return;
		} else {
			
		
    	
    	String endDate = "";
		Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
		
		
		Date edate = SystemSetting.localToUtilDate(dpEndDate.getValue());
		endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		
//		String reason = "DAMAGED_ITEMS";

    	try {
			JasperPdfReportService.DamageEntriesReport(strtDate,endDate,SystemSetting.systemBranch);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
    }

    @FXML
    void focusEndDate(KeyEvent event) {

    }
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
