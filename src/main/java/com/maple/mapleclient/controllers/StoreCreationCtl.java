package com.maple.mapleclient.controllers;

import java.util.List;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SalesModeWiseBillReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class StoreCreationCtl {
	
	String taskid;
	String processInstanceId;
	
	StoreMst storeMst = null;
	
	private ObservableList<StoreMst> storeMstList = FXCollections.observableArrayList();


    @FXML
    private TextField txtName;

    @FXML
    private TextField txtShortCode;

    @FXML
    private CheckBox chkMobile;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnDelete;

    @FXML
    private TableView<StoreMst> tblStore;

    @FXML
    private TableColumn<StoreMst, String> clName;

    @FXML
    private TableColumn<StoreMst, String> clShortCode;

    @FXML
    private TableColumn<StoreMst,String> clMobile;
    
    @FXML
	private void initialize() {
    	
    	
    	tblStore.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					storeMst = new StoreMst();
					storeMst.setId(newSelection.getId());
				}
			}
		});
    	
    }

    @FXML
    void Delete(ActionEvent event) {

    	if(null != storeMst)
    	{
    		if(null != storeMst.getId())
    		{
    			RestCaller.deleteStoreMst(storeMst.getId());
    			storeMstList.clear();
    			ResponseEntity<List<StoreMst>> storeMstSaved = RestCaller.getAllStoreMst();
    			storeMstList = FXCollections.observableArrayList(storeMstSaved.getBody());
    			FillTable();
    		}
    	}
    }

    @FXML
    void MobileOnPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		btnSave.requestFocus();
    		}
    }

    @FXML
    void Save(ActionEvent event) {
    	
    	addItem();

    }

    private void addItem() {
		if(txtName.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please enter store name", false);
			txtName.requestFocus();
			return;
		}
		
		if(txtShortCode.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please enter short code", false);
			txtShortCode.requestFocus();
			return;
		}
		
		storeMst = new StoreMst(); 
		storeMst.setName(txtName.getText());
		storeMst.setBranchCode(SystemSetting.systemBranch);
		storeMst.setShortCode(txtShortCode.getText());
		
		if(chkMobile.isSelected())
		{
			storeMst.setMobile("YES");
		} else {
			storeMst.setMobile("NO");
		}
		
		String id = RestCaller.getVoucherNumber(SystemSetting.systemBranch+"STR");
		storeMst.setId(id);
		
		storeMstList.clear();
		ResponseEntity<StoreMst> storeMstSaved = RestCaller.saveStoreMst(storeMst);
		storeMst = storeMstSaved.getBody();
		
		if(null != storeMst)
		{
			
			storeMstList.add(storeMst);
			FillTable();
			notifyMessage(5, "Successfully saved", true);
			storeMst = null;
			txtName.clear();
			txtShortCode.clear();
			chkMobile.setSelected(false);
		}
	}

	private void FillTable() {
		
		tblStore.setItems(storeMstList);
    	clName.setCellValueFactory(cellData -> cellData.getValue().getNameProperty());
    	clMobile.setCellValueFactory(cellData -> cellData.getValue().getMobileProperty());
    	clShortCode.setCellValueFactory(cellData -> cellData.getValue().getShortCodeProperty());

		
		
	}

	@FXML
    void SaveOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		addItem();
		}
    }

    @FXML
    void ShortCodeOnPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		chkMobile.requestFocus();
    		}
    }

    @FXML
    void ShowAll(ActionEvent event) {
    	storeMstList.clear();
		ResponseEntity<List<StoreMst>> storeMstSaved = RestCaller.getAllStoreMst();
		storeMstList = FXCollections.observableArrayList(storeMstSaved.getBody());
		FillTable();

    }

    @FXML
    void nameOnPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		txtShortCode.requestFocus();
    		}
    }

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}
}
