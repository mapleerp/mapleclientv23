package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.InsuranceCompanyDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class KitDefinitionPublishWindowClt {
	String taskid;
	String processInstanceId;

	private ObservableList<KitDefinitionDtl> kitDefinitionDtlList = FXCollections.observableArrayList();

	List<String> branchList = new ArrayList<String>();
	private ObservableList<BranchMst> branchList1 = FXCollections.observableArrayList();

	private ObservableList<KitDefinitionMst> kitDefinitionPublishWindowList = FXCollections.observableArrayList();

	private static final Logger logger = LoggerFactory.getLogger(MasterReportSqlWindowCtl.class);

	private EventBus eventBus = EventBusFactory.getEventBus();

	ItemMst itemmst = new ItemMst();
	String itemid =null;
	KitDefinitionMst kitDefinitionPublishWindow = null;
	@FXML
	private ListView<String> lstBranch;

	@FXML
	private TextField txtKitName;

	@FXML
	private Button btnPublish;
	String kitId = "";
	@FXML
	private void initialize() {

		eventBus.register(this);
		logger.info(" =============== INITIALIZATION STARTED");

		showBranchGroup();
		
		
		
		logger.info(" =============== INITIALIZATION COMPLETED");
	}

	@FXML
	void showItemPopUp(MouseEvent event) {
		showItemMst();
	}

	private void showBranchGroup() {
		ArrayList cat = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		cat = RestCaller.SearchBranchs();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object unitName = lm.get("branchCode");
			Object unitId = lm.get("id");
			if (unitId != null) {
				lstBranch.getItems().add((String) unitName);
				// allow multi selection
				lstBranch.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
				BranchMst newUnitMst = new BranchMst();

				newUnitMst.setId((String) unitId);
				branchList1.add(newUnitMst);
			}
		}
	}

	@FXML
	void Publish(ActionEvent event) {
		save();

	}

	private void save() {

		KitDefinitionMst kitDefinitionPublishWindow = new KitDefinitionMst();
		logger.info("======STARTED  SAVE");
		if (null == lstBranch.getSelectionModel().getSelectedItems()) {
			notifyMessage(3, "Please select Branch", false);
			return;
		}
		if (txtKitName.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Select Kit name!!!");
			txtKitName.requestFocus();
		}
		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtKitName.getText());
		ItemMst itemMst = getItem.getBody();
		
		if (null == itemMst) {
			notifyMessage(5, "Item not found", false);
			return;
		}

		kitDefinitionPublishWindow.setBranchCode(lstBranch.getSelectionModel().getSelectedItem());
		kitDefinitionPublishWindow.setKitName(itemMst.getId());

		List<String> selectedItems = lstBranch.getSelectionModel().getSelectedItems();

		String branch = "";
		for (String b : selectedItems) {
			b= b.concat(";");
			branch = branch.concat(b);
		}
		
		ResponseEntity<List<KitDefinitionMst>> kitDtl = RestCaller
				.getKitDefinitionByItemId(itemMst.getId());
		
		
		List<KitDefinitionMst> kitdmstList = kitDtl.getBody();
		
		
		
		
		ResponseEntity<KitDefinitionMst> saveKitDefinitionPublishWindow = RestCaller
				.publishAllKitDefinition(branch,kitdmstList.get(0).getId());
		{
			notifyMessage(5, "Saved ", true);
			txtKitName.clear();

			lstBranch.getSelectionModel().clearSelection();

			return;
		}
	}

	private void showItemMst() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			KitPopupCtl popupctl = fxmlLoader.getController();
			popupctl.kitNamePopupByItemId("");
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtKitName.requestFocus();
	}

	@Subscribe
	public void popupItemlistner(KitPopupEvent kitPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnPublish.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					ItemMst itemMstTo = new ItemMst();
					txtKitName.setText(kitPopupEvent.getItemName());
					itemMstTo.setId(kitPopupEvent.getItemId());

				}
			});
		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}
}
