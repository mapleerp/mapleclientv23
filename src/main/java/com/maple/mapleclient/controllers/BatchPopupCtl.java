package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.ItemBatchEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
public class BatchPopupCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<ItemBatchMst> itemBatchList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();

	StringProperty SearchString = new SimpleStringProperty();
	ItemBatchEvent itemBatchEvent;

    @FXML
    private TextField txtname;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<ItemBatchMst> tablePopupView;

    @FXML
    private TableColumn<ItemBatchMst, String> batchCode;

    @FXML
    private Button btnCancel;

    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtname.requestFocus();
		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
     	Stage stage = (Stage) btnSubmit.getScene().getWindow();
    		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {

    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    
    
    
    
    @FXML
   	private void initialize() {
     
    	getItemBatchMstByBatch("");
		
		txtname.textProperty().bindBidirectional(SearchString);
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		btnCancel.setCache(true);
		
		tablePopupView.setItems(itemBatchList);
		
		batchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		
		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
				
					itemBatchEvent = new ItemBatchEvent();
					itemBatchEvent.setItemId(newSelection.getId());
					itemBatchEvent.setBatchCode(newSelection.getBatch());;
					eventBus.post(itemBatchEvent);
				}
			}
		});


		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				getItemBatchMstByBatch((String) newValue);
			}
		});
     }
     
 	public void getItemBatchMstByBatch(String itemName) {
 		
 		itemBatchList.clear();
		ArrayList batchArray = new ArrayList();
		if(itemName.length()>0)
		{
		batchArray = RestCaller.searchBatch(itemName);
		
//		System.out.print(batchArray.size()+"batch array size isssssssssssssssssssssssssss");
		Iterator itr = batchArray.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object categoryName = lm.get("batch");
			Object id = lm.get("id");
			if (null != id) {
				ItemBatchMst itemBatchMst = new ItemBatchMst();
				itemBatchMst.setBatch((String) categoryName);;
				itemBatchMst.setId((String) id);
				itemBatchList.add(itemBatchMst);
			}

		}
		}
		return;
 	}
 	private void FillTable() {
 		tablePopupView.setItems(itemBatchList);

 		batchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

 		
 	}
 	 @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


       private void PageReload() {
       	
 }

}
