package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.base.Optional;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.GroupMst;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.UserGroupMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.events.UserPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javafx.util.Duration;

public class UserGroupMstCtl {

	UserGroupMst userGroupMst = null;

	String taskid;
	String processInstanceId;
	private ObservableList<UserGroupMst> userGroupMstList = FXCollections.observableArrayList();

	private EventBus eventBus = EventBusFactory.getEventBus();

	@FXML
	private TextField txtusername;

	@FXML
	private ComboBox<String> cmbgrp;

	@FXML
	private Button btnSave;

	@FXML
	private Button btndelete;

	@FXML
	private Button btnshowAll;

	@FXML
	private TableView<UserGroupMst> tbluserDetails;

	@FXML
	private TableColumn<UserGroupMst, String> clUsername;

	@FXML
	private TableColumn<UserGroupMst, String> clgroup;

	@FXML
	void actionDelete(ActionEvent event) {

		if (null != userGroupMst) {

			if (null != userGroupMst.getId()) {
				RestCaller.userGroupPermissionDelete(userGroupMst.getId());

				showAll();
				notifyMessage(5, "Deleted......", false);
				return;
			}
		}

	}

	private void showAll() {
		ResponseEntity<List<UserGroupMst>> getAlluserName = RestCaller.getUserGroupMst();

		userGroupMstList = FXCollections.observableArrayList(getAlluserName.getBody());
		fillTable();
	}

	@FXML
	void actionShowAll(ActionEvent event) {
		showAll();
		fillTable();

	}

	@FXML
	void btnSaveAction(ActionEvent event) {

		additem();
	}

	@FXML
	void userNameOnEnter(KeyEvent event) {

	}

	@FXML
	void userpopup(MouseEvent event) {

		showUserPopup();

	}

	@FXML
	private void initialize() {
		eventBus.register(this);

		setCmbGroup();

		tbluserDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					userGroupMst = new UserGroupMst();
					userGroupMst.setId(newSelection.getId());
				}
			}
		});
	}

	private void setCmbGroup() {

		ResponseEntity<List<GroupMst>> userGroupMstListResp = RestCaller.getGroupMst();
		List<GroupMst> userGroupMstList = userGroupMstListResp.getBody();

		for (GroupMst userGroup : userGroupMstList) {
			cmbgrp.getItems().add(userGroup.getGroupName());

		}

	}

	private void showUserPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/UserPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtusername.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void userNamePopUp(MouseEvent event) {
		showUserPopup();
	}

	private void fillTable() {

		tbluserDetails.setItems(userGroupMstList);

//			for  userGroupMstList
//			{
//			1  get user by user id 
//			2  then set user name
//			
//			1 get group by id
//			2 set group name
//			}

		for (UserGroupMst usr : userGroupMstList) {
			ResponseEntity<UserMst> userMstresp = RestCaller.getUserNameById(usr.getUserId());
			UserMst userGroupMstList = userMstresp.getBody();
			if (null != userGroupMstList) {
				usr.setUserName(userGroupMstList.getUserName());
			}

			ResponseEntity<GroupMst> usergrpidresp = RestCaller.getGroupNameById(usr.getGroupId());
			GroupMst groupMstList = usergrpidresp.getBody();

			if (null != groupMstList) {
				usr.setGroupName(groupMstList.getGroupName());
			}

		}
		clgroup.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
		clUsername.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());

	}

	private void additem() {

		UserGroupMst userGroupMst = new UserGroupMst();

		if (txtusername.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select user name", false);
			return;
		}
		ResponseEntity<UserMst> userMstResp = RestCaller.getUserByName(txtusername.getText());
		UserMst userMst = userMstResp.getBody();

		if (null == userMst) {
			notifyMessage(5, "Invalid user name...!", false);
			return;
		}
		userGroupMst.setUserId(userMst.getId());

//			find group by name == id
//					
//			set group id to the object

		ResponseEntity<GroupMst> grpmstresp = RestCaller.getGroupDtls(cmbgrp.getSelectionModel().getSelectedItem());

		GroupMst groupMstList = grpmstresp.getBody();

		userGroupMst.setGroupId(groupMstList.getId());

		userGroupMst.setGroupName(cmbgrp.getSelectionModel().getSelectedItem());

		ResponseEntity<UserGroupMst> respEntity = RestCaller.saveUserGroupPermissionMst(userGroupMst);

//			fillTable();

		notifyMessage(5, "Saved ", true);
		txtusername.clear();

		showAll();
		return;

	}

	private void clearFileds() {

		txtusername.clear();
		cmbgrp.getSelectionModel().clearSelection();

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@Subscribe
	public void userEventListener(UserPopupEvent userPopupEvent) {

		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			txtusername.setText(userPopupEvent.getUserName());

		}

	}

}
