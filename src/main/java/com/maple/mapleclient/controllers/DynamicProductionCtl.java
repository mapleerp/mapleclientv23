package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.TSCBarcode;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ConsumptionDtl;
import com.maple.mapleclient.entity.DamageDtl;
import com.maple.mapleclient.entity.DynamicProductionDtl;
import com.maple.mapleclient.entity.DynamicProductionHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class DynamicProductionCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<DynamicProductionDtl> dynamicProdDtlList = FXCollections.observableArrayList();

	DynamicProductionHdr dynamicproductionHdr = null;
	DynamicProductionDtl dynamicProductionDtl = null;
    @FXML
    private DatePicker dpDate;

    @FXML
    private TextField txtUnit;
    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtBatch;

    @FXML
    private TextField txtQty;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnPrintBarcode;

    @FXML
    private TableView<DynamicProductionDtl> tbDynamicProduction;

    @FXML
    private TableColumn<DynamicProductionDtl, String> clItemName;

    @FXML
    private TableColumn<DynamicProductionDtl,String> clBatch;

    @FXML
    private TableColumn<DynamicProductionDtl,Number> clQty;

    @FXML
    private Button btnSave;
	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		eventBus.register(this);
		dpDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		dpDate.setEditable(false);
		
		
		tbDynamicProduction.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					dynamicProductionDtl = new DynamicProductionDtl();
					dynamicProductionDtl.setId(newSelection.getId());
					ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(newSelection.getItemId());
					txtItemName.setText(getItem.getBody().getItemName());
					txtBatch.setText(newSelection.getBatch());
					txtQty.setText(Double.toString(newSelection.getQty()));
					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(newSelection.getUnitId());
					txtUnit.setText(getUnit.getBody().getUnitName());
				}
			}
		});
	}
    @FXML
    void actionAdd(ActionEvent event) {
    	addItem();
    }
    private void clearFields()
    {
    	txtBatch.clear();
    	txtItemName.clear();
    	txtQty.clear();
    	txtUnit.clear();
    }
    private void fillTable()
    {
    	for(DynamicProductionDtl dynamicprod:dynamicProdDtlList)
    	{
    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(dynamicprod.getItemId());
    		dynamicprod.setItemName(getItem.getBody().getItemName());
    		ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(dynamicprod.getUnitId());
    		dynamicprod.setUnitName(getUnit.getBody().getUnitName());
    	}
    	tbDynamicProduction.setItems(dynamicProdDtlList);
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameproperty());
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	
    	if(null == dynamicProductionDtl)
    	{
    		return;
    	}
    	if(null == dynamicProductionDtl.getId())
    	{
    		return;
    	}
    	RestCaller.deleteDynamicProductionDtl(dynamicProductionDtl.getId());
		ResponseEntity<List<DynamicProductionDtl>> dynamicProdDtllist = RestCaller.getDynamicProductionHdr(dynamicproductionHdr);
		dynamicProdDtlList = FXCollections.observableArrayList(dynamicProdDtllist.getBody());
		fillTable();
    	notifyMessage(3, "Deleted");
    	dynamicProductionDtl = null;
    	clearFields();
    	txtItemName.requestFocus();
    }

    @FXML
    void actionPrintBarcode(ActionEvent event) {

    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @FXML
    void actionSave(ActionEvent event) {

    	if(null != dynamicproductionHdr)
    	{
    		ResponseEntity<List<DynamicProductionDtl>> dynamicProdDtllist = RestCaller.getDynamicProductionHdr(dynamicproductionHdr);
    		if(dynamicProdDtllist.getBody().size()==0)
    		{
    			return;
    		}
    		String vno = RestCaller.getVoucherNumber("DYNAMIC");
    		dynamicproductionHdr.setVoucherNumber(vno);
    		RestCaller.updateDynamicProductionHdr(dynamicproductionHdr);
    		dynamicproductionHdr = null;
    		dynamicProductionDtl = null;
    		dynamicProdDtlList.clear();
    		tbDynamicProduction.getItems().clear();
    		printBarcode(dynamicProdDtllist.getBody());
    		
    		
    		
    	}
    }
    private void printBarcode(List<DynamicProductionDtl> dynamicList) {
    	int i=0;
    		for(i =0;i<dynamicList.size();i++)
    		{
    			ResponseEntity<ItemMst>  itemmst = RestCaller.getitemMst(dynamicList.get(i).getItemId());
    			LocalDate ldate = dpDate.getValue();
    			java.util.Date uProdDate = SystemSetting.localToUtilDate(ldate);
    			String sProdDate = SystemSetting.UtilDateToString(uProdDate, "dd-MM-yyyy");
    			
    			String ManufactureDate =  sProdDate;
    			LocalDate expdate = ldate.plusDays(itemmst.getBody().getBestBefore());
    			java.util.Date uexpdate = SystemSetting.localToUtilDate(expdate);
    			String ExpiryDate = SystemSetting.UtilDateToString(uexpdate, "dd-MM-yyyy");
    			String ItemName = itemmst.getBody().getItemName();
    			String Barcode =itemmst.getBody().getBarCode()+"-"+dynamicList.get(i).getBatch();
    			String NumberOfCopies = dynamicList.get(i).getQty().toString();
    			String Mrp = itemmst.getBody().getStandardPrice().toString();
    			String IngLine1 =  itemmst.getBody().getBarCodeLine1();
    			String IngLine2 = itemmst.getBody().getBarCodeLine2();
    			String PrinterNAme = SystemSetting.printer_name;
    			String netWt = itemmst.getBody().getNetWeight();
    			String batch =  dynamicList.get(i).getBatch();
    			 TSCBarcode.manualPrint(NumberOfCopies, ManufactureDate, ExpiryDate, ItemName,
    					  Barcode, Mrp, IngLine1, IngLine2, PrinterNAme,netWt,batch);

    	}
	}
    @FXML
    void qtyOnEnter(KeyEvent event) {
    	if(event.getCode()==KeyCode.ENTER)
    	{
    		addItem();
    	}
    }
	private void addItem() {


    	if(null == dynamicproductionHdr)
    	{
    		dynamicproductionHdr = new DynamicProductionHdr();
    		dynamicproductionHdr.setBranchCode(SystemSetting.getUser().getBranchCode());
    		dynamicproductionHdr.setVoucherDate(SystemSetting.getApplicationDate());
    		ResponseEntity<DynamicProductionHdr> respenity = RestCaller.saveDynamicProductionHdr(dynamicproductionHdr);
    		dynamicproductionHdr = respenity.getBody();
    	}
    	dynamicProductionDtl = new DynamicProductionDtl();
    	ResponseEntity<ItemMst> getitemmst = RestCaller.getItemByNameRequestParam(txtItemName.getText());
    	dynamicProductionDtl.setBatch(txtBatch.getText());
    	dynamicProductionDtl.setItemId(getitemmst.getBody().getId());
    	dynamicProductionDtl.setQty(Double.parseDouble(txtQty.getText()));
    	dynamicProductionDtl.setDynamicProductionHdr(dynamicproductionHdr);
    	ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(txtUnit.getText());
    	dynamicProductionDtl.setUnitId(getUnit.getBody().getId());
    	ResponseEntity<DynamicProductionDtl> saveDynamicProdDtl = RestCaller.saveDynamicProductionDtl(dynamicProductionDtl);
    	dynamicProductionDtl = saveDynamicProdDtl.getBody();
    	dynamicProdDtlList.add(dynamicProductionDtl);
    	fillTable();
    	clearFields();
    	dynamicProductionDtl = null;
    	txtItemName.requestFocus();
		
	}
	@FXML
    void loadPopUp(KeyEvent event) {

    	if(event.getCode()==KeyCode.ENTER)
    	{
    		showpopup();
    		
    	}
    }
    @Subscribe
	public void popupItemlistner(ItemPopupEvent itempopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		txtBatch.setText(dpDate.getValue().toString());
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtItemName.setText(itempopupEvent.getItemName());
		//	productionDtl.setItemId(itempopupEvent.getItemId());
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
			txtBatch.setText(txtBatch.getText()+getItem.getBody().getItemCode());
			ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
			txtUnit.setText(getUnit.getBody().getUnitName());
//		//	txtBatch.requestFocus();
//			txtBatch.setText(dpProductionDate.getValue().toString());
				}
			});
		}
	}
    
    
	private void showpopup() {

    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
		
	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
