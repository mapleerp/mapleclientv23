package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SalesInvoiceReport;
import com.maple.report.entity.WholeSaleDetailReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class WholeSaleDetailReportCtl {
	private ObservableList<WholeSaleDetailReport> wholeSaleDetailReportList = FXCollections.observableArrayList();

	@FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnPrintReport;

    @FXML
    private TableView<WholeSaleDetailReport> tblWholeSaleDetail;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnInvoice;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnVoucher;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnCustomer;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnItemName;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnGroupName;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnBatch;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnItemCode;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnExpiryDate;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnQuantity;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnRate;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnTaxRate;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnSaleValue;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnCost;

    @FXML
    private TableColumn<WholeSaleDetailReport, Number> clmnCostValue;

    @FXML
    private TableColumn<WholeSaleDetailReport, String> clmnSalesMan;

	@FXML
	void generateReport(ActionEvent event) {
		if (null == dpFromDate.getValue()) {

			notifyMessage(3, "please select start date", false);
			dpFromDate.requestFocus();
			return;
		}

		if (null == dpToDate.getValue()) {

			notifyMessage(3, "please select start date", false);
			dpToDate.requestFocus();

			return;
		}

		Date uDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String fromDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		Date eDate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String toDate = SystemSetting.UtilDateToString(eDate, "yyy-MM-dd");
		
		ResponseEntity<List<WholeSaleDetailReport>> wholeSaleDetailReportResp = RestCaller
				.getWholeSaleDetailReport(fromDate, toDate);

		wholeSaleDetailReportList = FXCollections.observableArrayList(wholeSaleDetailReportResp.getBody());

		fillReportTable();
		
		
		
	}

	private void fillReportTable() {
		tblWholeSaleDetail.setItems(wholeSaleDetailReportList);

		clmnInvoice.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
		clmnVoucher.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clmnCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clmnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clmnGroupName.setCellValueFactory(cellData -> cellData.getValue().getGroupNameProperty());
		clmnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		//clmnItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
		clmnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clmnQuantity.setCellValueFactory(cellData -> cellData.getValue().getQuantityProperty());
		clmnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clmnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		//clmnSaleValue.setCellValueFactory(cellData -> cellData.getValue().getSaleValueProperty());
		//clmnCost.setCellValueFactory(cellData -> cellData.getValue().getCostProperty());
		//clmnCostValue.setCellValueFactory(cellData -> cellData.getValue().getCostValueProperty());
		clmnSalesMan.setCellValueFactory(cellData -> cellData.getValue().getSalesManProperty());
		
	}
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		tblWholeSaleDetail.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {

					
					fillReportTable();


				}
			}
		});

	}
	@FXML
	void printReport(ActionEvent event) {
		
		Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sfdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
		Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String stdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		


		try {
			JasperPdfReportService.WholeSaleDetailReport(sfdate,stdate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}