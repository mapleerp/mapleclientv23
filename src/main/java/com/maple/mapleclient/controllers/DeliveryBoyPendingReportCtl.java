package com.maple.mapleclient.controllers;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SaleOrderReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.scene.control.DatePicker;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;

public class DeliveryBoyPendingReportCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<SaleOrderReport> deliveryBoyPendingReport = FXCollections.observableArrayList();



    @FXML
    private DatePicker dpDate;

    @FXML
    private ChoiceBox<String> cmbDeliveryBoy;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private TableView<SaleOrderReport> tblReport;

    @FXML
    private TableColumn<SaleOrderReport, String> clVoucherNo;

    @FXML
    private TableColumn<SaleOrderReport, String> clAccount;

    @FXML
    private TableColumn<SaleOrderReport, String> clCustName;

    @FXML
    private TableColumn<SaleOrderReport, String> clCustAdd;

    @FXML
    private TableColumn<SaleOrderReport, String> clCustPhn;

    @FXML
    private TableColumn<SaleOrderReport, String> clDueDate;

    @FXML
    private TableColumn<SaleOrderReport, String> clDueTime;

    @FXML
    private TableColumn<SaleOrderReport, Number> clAmount;

    @FXML
    private TextField txtTotal;
    
    @FXML
	private void initialize() {
	
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    ResponseEntity<List<DeliveryBoyMst>> respdelivery = RestCaller.getDeliveryBoyMstByStatus();
	for (DeliveryBoyMst deliverymst : respdelivery.getBody()) {
		cmbDeliveryBoy.getItems().add(deliverymst.getDeliveryBoyName());
	}
	
    }

    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpDate.getValue())
    	{
    		notifyMessage(3, "Please select date", false);
    		return;
    	}

    	if(null == cmbDeliveryBoy.getValue())
    	{
    		notifyMessage(3, "Please select delivery boy", false);
    		return;
    	}
    	Date date = SystemSetting.localToUtilDate(dpDate.getValue());
    	String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
    	
    	ResponseEntity<DeliveryBoyMst> deliveryBoyMstResp = RestCaller.getDelivaryBoyByName(cmbDeliveryBoy.getValue().toString());
    	DeliveryBoyMst deliveryBoyMst = deliveryBoyMstResp.getBody();
    	
    	ResponseEntity<List<SaleOrderReport>> saleOrderReportResp = RestCaller.DeliveryBoyPendingReport(sdate,deliveryBoyMst.getId());

        deliveryBoyPendingReport = FXCollections.observableArrayList(saleOrderReportResp.getBody());
        
        FillTable();
    }
    
	private void FillTable() {

		tblReport.setItems(deliveryBoyPendingReport);
		Double total = 0.0;
		for(SaleOrderReport report : deliveryBoyPendingReport)
		{
			total = total+report.getInvoiceAmount();
		}
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clCustAdd.setCellValueFactory(cellData -> cellData.getValue().getCustAddProperty());
		clCustName.setCellValueFactory(cellData -> cellData.getValue().getCustNameProperty());
		clCustPhn.setCellValueFactory(cellData -> cellData.getValue().getCustPhnProperty());
		clDueDate.setCellValueFactory(cellData -> cellData.getValue().getDueDateProperty());
		clDueTime.setCellValueFactory(cellData -> cellData.getValue().getDueTimeProperty());
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());

		BigDecimal bdCashToPay = new BigDecimal(total);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);

		txtTotal.setText(bdCashToPay.toPlainString());
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
