package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMergeMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ItemCorrectionReport;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class ItemCorrectionCtl {

	String taskid;
	String processInstanceId;
	ItemCorrectionReport itemCorrectionReport = null;

	private ObservableList<ItemCorrectionReport> ItemCorrectionReportList = FXCollections.observableArrayList();

	@FXML
	private TableView<ItemCorrectionReport> tblItemReport;

	@FXML
	private TableColumn<ItemCorrectionReport, Number> clPurchaseCount;

	@FXML
	private TableColumn<ItemCorrectionReport, Number> clStock;
	@FXML
	private TableColumn<ItemCorrectionReport, String> clItemName;

	@FXML
	private TableColumn<ItemCorrectionReport, Number> clSalesCount;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowAll;

	@FXML
	private Button btnMerge;

	@FXML
	private TextField txtItemName;

	@FXML
	private DatePicker dpDate;

	@FXML
	private TextField txtMergeto;

	@FXML
	private Button btnCompleMerger;

	@FXML
	private Button btnClear;
	
	@FXML
    private Button btnPrintReport;

	@FXML
	private void initialize() {

		btnDelete.setDisable(false);

		btnCompleMerger.setDisable(false);
		txtItemName.setDisable(false);
		txtMergeto.setDisable(false);
		dpDate.setDisable(false);

		tblItemReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getItemId()) {

					btnDelete.setDisable(false);

					itemCorrectionReport = new ItemCorrectionReport();
					itemCorrectionReport.setItemId(newSelection.getItemId());

					if (newSelection.getPurchaseCount() > 0) {
						btnDelete.setDisable(true);
					}
					if (newSelection.getSalesCount() > 0) {
						btnDelete.setDisable(true);
					}
					if (newSelection.getStockCount() > 0) {
						btnDelete.setDisable(true);
					}
					
					if(!txtItemName.isDisable())
					{
						
						if(txtItemName.getText().trim().isEmpty())
						{
							txtItemName.setText(newSelection.getItemName());
						} else if (txtMergeto.getText().trim().isEmpty()) {
							txtMergeto.setText(newSelection.getItemName());
						}
					}
					
					

				}
			}
		});
	}

	@FXML
	void CompleteMerge(ActionEvent event) {
		
		//final save

		if (dpDate.getValue() == null) {
			notifyMessage(5, " Please enter date!!!");
			return;
		} else if (txtItemName.getText().isEmpty()) {
			notifyMessage(5, " Please enter fromitem...!!!");
			return;
		} else if (txtMergeto.getText().isEmpty()) {
			notifyMessage(5, " Please enter toitem...!!!");
			return;
		}

		ItemMergeMst itemMergeMst = new ItemMergeMst();
		
		ResponseEntity<ItemMst> itemMstResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		ItemMst itemMst = itemMstResp.getBody();
		itemMergeMst.setFromItemName(itemMst.getItemName());
		itemMergeMst.setFromItemId(itemMst.getId());
		itemMergeMst.setFromItemBarCode(itemMst.getBarCode());

		ResponseEntity<ItemMst> itemMstMergetoResp = RestCaller.getItemByNameRequestParam(txtMergeto.getText());
		ItemMst itemMstMergeTo = itemMstMergetoResp.getBody();

		
		itemMergeMst.setToItemName(itemMstMergeTo.getItemName());
		itemMergeMst.setToItemId(itemMstMergeTo.getId());
		itemMergeMst.setUserId(SystemSetting.getUser().getUserName());

		itemMergeMst.setBranchCode(SystemSetting.getSystemBranch());
		itemMergeMst.setToItemBarCode(itemMstMergeTo.getBarCode());
		
		Date local = Date.valueOf(dpDate.getValue());
		itemMergeMst.setVoucherDate(local);
		ResponseEntity<ItemMergeMst> respentity = RestCaller.ItemMergeInCloud(itemMergeMst);
		
		ResponseEntity<ItemMergeMst> respentity1 =RestCaller.ItemMergeInRestServer(itemMergeMst);
		
		
//	RestCaller.deleteItemForCorrectionbyId(itemMstMergeTo.getId());

		
		ShowAllItemList();

	}

	@FXML
	void clearFields(ActionEvent event) {
		
		txtItemName.clear();
		txtMergeto.clear();
	}

	@FXML
	void showAll(ActionEvent event) {

		btnDelete.setDisable(false);

		ShowAllItemList();
		
		

	}
	
	

	private void ShowAllItemList() {

		ResponseEntity<List<ItemCorrectionReport>> itemCorrectionReps = RestCaller.getAllItemsFromCloud();

		ItemCorrectionReportList = FXCollections.observableArrayList(itemCorrectionReps.getBody());
		FillTable();
	}

	private void FillTable() {

		tblItemReport.setItems(ItemCorrectionReportList);
		clPurchaseCount.setCellValueFactory(cellData -> cellData.getValue().getPurchaseCountProperty());
		clStock.setCellValueFactory(cellData -> cellData.getValue().getStockCountProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clSalesCount.setCellValueFactory(cellData -> cellData.getValue().getSalesCountProperty());

	}

	@FXML
	void deleteAction(ActionEvent event) {

		if (null != itemCorrectionReport) {

			if (null != itemCorrectionReport.getItemId()) {
				RestCaller.deleteItemForCorrectionbyId(itemCorrectionReport.getItemId());
				RestCaller.deleteItemForCorrectionbyIdFromCloud(itemCorrectionReport.getItemId());

				tblItemReport.getItems().clear();
				notifyMessage(5, "Deleted!!!");

				ShowAllItemList();
				FillTable();
			}
		}

	}

	@FXML
	void mergeAction(ActionEvent event) {

		notifyMessage(3, "Select Item ");
		txtItemName.setDisable(false);
		btnCompleMerger.setDisable(false);
		dpDate.setDisable(false);
		txtMergeto.setDisable(false);

	}
	
	
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

}
