package com.maple.mapleclient.controllers;

import java.time.LocalDate;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class ProductionBarGraphCtl {
	
	String taskid;
	String processInstanceId;
    private ObservableList<String> curDate = FXCollections.observableArrayList();
    private ObservableList<Double> values = FXCollections.observableArrayList();
	XYChart.Series<String, Double> series;
    @FXML
    private DatePicker dpToDate;
   
    @FXML
    private DatePicker dpFromDate;
    @FXML
    private CategoryAxis xAxis;

    @FXML
    private NumberAxis yAxis;

    @FXML
    private ListView<String> lstBranch;

    @FXML
    private BarChart<String,Double> prGraph;

    @FXML
    private ComboBox<String> cmbValue;
    @FXML
    private Button btnShow;
    @FXML
    private Button btnClear;

    @FXML
    void actionClear(ActionEvent event) {

    	cmbValue.getSelectionModel().clearSelection();
    	lstBranch.getSelectionModel().clearSelection();
    	prGraph.getData().clear();
    	 curDate.clear();
    	 values.clear();
    }

    @FXML
    private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	cmbValue.getItems().add("FINISHED PRODCUTS VALUE");
    	cmbValue.getItems().add("RAW MATERIALS VALUE");
    	ResponseEntity<List<BranchMst>> getAllBranches = RestCaller.getBranchDtl();
    	for(BranchMst branchMst:getAllBranches.getBody())
    	{
    		lstBranch.getItems().add(branchMst.getBranchCode());
    	}
    }
    public void notifyMessage(int duration, String msg, boolean success) {
  		Image img;
  		if (success) {
  			img = new Image("done.png");

  		} else {
  			img = new Image("failed.png");
  		}

  		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
  				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
  				.onAction(new EventHandler<ActionEvent>() {
  					@Override
  					public void handle(ActionEvent event) {
  						System.out.println("clicked on notification");
  					}
  				});
  		notificationBuilder.darkStyle();
  		notificationBuilder.show();

  	}
    @FXML
    void actionShow(ActionEvent event) {
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5,"Select From Date",false);
    		dpFromDate.requestFocus();
    		return;
    	}
    	if(null == dpToDate.getValue())
    	{

    		notifyMessage(5,"Select To Date",false);
    		dpToDate.requestFocus();
    		return;
    	}
    	if(cmbValue.getSelectionModel().isEmpty())
    	{
    		notifyMessage(2,"Select Graph Type",false);
    		return;
    	}
    	if(lstBranch.getSelectionModel().isEmpty())
    	{
    		notifyMessage(2,"Select Branch", false);
    		return;
    	}
   	 series= new XYChart.Series<String, Double>();
 	LocalDate dpDate1 = dpFromDate.getValue();
	java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
	 String  strtDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
	
	 LocalDate dpDate2= dpToDate.getValue();
		java.util.Date tDate  = SystemSetting.localToUtilDate(dpDate2);
		 String  strfDate = SystemSetting.UtilDateToString(tDate, "yyyy-MM-dd");
		 
		if(cmbValue.getSelectionModel().getSelectedItem().equalsIgnoreCase("FINISHED PRODCUTS VALUE"))
		{
		 
		 List<HashMap<String, Double>> map = new  ArrayList<HashMap<String,Double>>();
		 map = RestCaller.getProductionValue(lstBranch.getSelectionModel().getSelectedItem(),strtDate,strfDate);
		 System.out.println(map);
		 Iterator itr = map.iterator();
			while (itr.hasNext()) {
				
				HashMap<String, Double> mapValue = new HashMap<String, Double>();
				mapValue = (HashMap<String, Double>) itr.next();
			
				for(Map.Entry<String, Double> entry : mapValue.entrySet())
				{
					curDate.add(entry.getKey());
					values.add(entry.getValue());
				}
			}
			series.setName("FINISHED PRODUCTS");
		}
		if(cmbValue.getSelectionModel().getSelectedItem().equalsIgnoreCase("RAW MATERIALS VALUE"))
		{
			 
			 List<HashMap<String, Double>> map = new  ArrayList<HashMap<String,Double>>();
			 map = RestCaller.getRawMaterialValue(lstBranch.getSelectionModel().getSelectedItem(),strtDate,strfDate);
			 System.out.println(map);
			 Iterator itr = map.iterator();
				while (itr.hasNext()) {
					
					HashMap<String, Double> mapValue = new HashMap<String, Double>();
					mapValue = (HashMap<String, Double>) itr.next();
				
					for(Map.Entry<String, Double> entry : mapValue.entrySet())
					{
						curDate.add(entry.getKey());
						values.add(entry.getValue());
					}
				}
				series.setName("RAW MATERIALS");
		}
			createBarGraph(curDate,values);
    }
    public void createBarGraph(ObservableList<String> xaxis,ObservableList<Double> yaxis)
    {
    	
    	
    	for(int i = 0;i<xaxis.size();i++)
    	{
    	
    			 series.getData().add(new XYChart.Data<>(xaxis.get(i),yaxis.get(i)));
    		
    		
    	}
    	//series = new XYChart.Series<String, Double>();
    	prGraph.getData().add(series);
//    	}
    	
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
