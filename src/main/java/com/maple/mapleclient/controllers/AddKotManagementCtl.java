 
	package com.maple.mapleclient.controllers;

	import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AddKotTable;
import com.maple.mapleclient.entity.AddKotWaiter;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.OrderTakerMst;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TableOccupiedEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
	import javafx.scene.control.TableColumn;
	import javafx.scene.control.TableView;
	import javafx.scene.control.TextField;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

	public class AddKotManagementCtl {
		String taskid;
		String processInstanceId;
		TableOccupiedEvent tableOccupiedEvent= null;
		AddKotTable addKotTable;
		AddKotWaiter addKotWaiter;
		DeliveryBoyMst deliveryBoyMst;
		OrderTakerMst orderTakerMst;

		EventBus eventBus = EventBusFactory.getEventBus();
		private ObservableList<AddKotTable> addKotTableList = FXCollections.observableArrayList();
		private ObservableList<AddKotTable> addKotTableList1 = FXCollections.observableArrayList();

 		private ObservableList<AddKotWaiter> addKotWaiterList = FXCollections.observableArrayList();
 		private ObservableList<AddKotWaiter> addKotWaiterList1 = FXCollections.observableArrayList();
 		private ObservableList<OrderTakerMst> orderTakerList = FXCollections.observableArrayList();
		private ObservableList<DeliveryBoyMst> DeliveryBoyList = FXCollections.observableArrayList();

 		@FXML
 	    private TextField txtDeliveryBoy;

 	    @FXML
 	    private Button btnDeliveryShow;

 	    @FXML
 	    private Button btnDeliveryDelete;

 	    @FXML
 	    private Button btnDeliveryAdd;

 	    @FXML
 	    private TableView<DeliveryBoyMst> tblDeliveryBoy;

 	    @FXML
 	    private TableColumn<DeliveryBoyMst, String> clDeliveryName;

 	    @FXML
 	    private TableColumn<DeliveryBoyMst, String> clDeliveryStatus;

 	    @FXML
 	    private TextField txtOrderTaker;

 	    @FXML
 	    private Button btnShowAllOrderTaker;

 	    @FXML
 	    private Button btnDeleteOrderTaker;

 	    @FXML
 	    private Button btnOrederTakerAdd;

 	    @FXML
 	    private TableView<OrderTakerMst> tblOrderTaker;

 	    @FXML
 	    private TableColumn<OrderTakerMst, String> clOrder;

 	    @FXML
 	    private TableColumn<OrderTakerMst, String> clOrderStatus;

 	    @FXML
 	    private Button btnDeleteTable;

 	    @FXML
 	    private Button bbtnDeleteWaiter;
 		
	    @FXML
	    private TextField texttbl;

	    @FXML
	    private TextField textwaiter;

	    @FXML
	    private Button addTables;
	    @FXML
	    private TableColumn<AddKotTable, String> clTableStatus;
	    @FXML
	    private TableColumn<AddKotWaiter, String> clWaiterStatus;
	    @FXML
	    private Button addWaiters;
	    @FXML
	    private Button btnShowAllTAbles;

	    @FXML
	    private Button btnShowAllWaiters;


	    @FXML
	    private TableView<AddKotWaiter> tblkot1;

	    @FXML
	    private TableColumn<AddKotWaiter, String> wn1;
 
	    @FXML
	    private TableView<AddKotTable> tblkot;
 
	    @FXML
	    private TableColumn<AddKotTable,String> tn;
	    @FXML
	    void ShowAllWaiters(ActionEvent event) {
	    	 
	 	    
	    	showAllWaiter();
	 	    }

	    

	    @FXML
	    void ShowTable(ActionEvent event) {
	    
	    	showAllTable();
	    }
	    @FXML
	    void DedliveryDelete(ActionEvent event) {
	    	if(null != deliveryBoyMst)
	    	{
	    	if(null != deliveryBoyMst.getId())
	    	{
	    		deliveryBoyMst.setStatus("DELETED");
	    		RestCaller.updateDelivery(deliveryBoyMst);
	    		notifyMessage(5, "Deleted");
	    		tblDeliveryBoy.getItems().clear();
	    		ResponseEntity<List<DeliveryBoyMst>> deliverySaved = RestCaller.getDeliveryBoyMst();
		    	DeliveryBoyList = FXCollections.observableArrayList(deliverySaved.getBody());
		    	tblDeliveryBoy.setItems(DeliveryBoyList);
		    	
	    	}
	    	else
	    		notifyMessage(5, "Select from table to delete");
	    	return;
	    	}

	    }

	    @FXML
	    void DeliveryAdd(ActionEvent event) {

	    	if(txtDeliveryBoy.getText().trim().isEmpty())
	    	{
	    		notifyMessage(5, "Type Delivery Boy!!");
	    		return;
	    	}
	    	else
	    	{
	    		deliveryBoyMst = new DeliveryBoyMst();
	    		deliveryBoyMst.setBranchCode(SystemSetting.getSystemBranch());
	    		deliveryBoyMst.setDeliveryBoyName(txtDeliveryBoy.getText());
	    		deliveryBoyMst.setStatus("ACTIVE");
	    		ResponseEntity<DeliveryBoyMst> savedDelivery = RestCaller.getdeliveryBoyId(txtDeliveryBoy.getText());
	    		
	    		if(null!=savedDelivery.getBody())
	    		{
	    			deliveryBoyMst = savedDelivery.getBody();
	    			deliveryBoyMst.setStatus("ACTIVE");
		    		RestCaller.updateDelivery(deliveryBoyMst);
		    		showAllDeliveryBoy();
		    		txtDeliveryBoy.clear();
	    		}
	    		else
	    		{
	    		ResponseEntity<DeliveryBoyMst> respentity = RestCaller.saveDeliveryBoyMst(deliveryBoyMst);
	    		deliveryBoyMst = respentity.getBody();
	    		
	    		notifyMessage(5,"Saved");
	    		DeliveryBoyList.add(deliveryBoyMst);
	    		tblDeliveryBoy.setItems(DeliveryBoyList);
	    		clDeliveryName.setCellValueFactory(cellData -> cellData.getValue().getdeliveryBoyNameProperty());
	    		clDeliveryStatus.setCellValueFactory(cellData -> cellData.getValue().getstatusProperty());
				deliveryBoyMst = null;
	    		txtDeliveryBoy.clear();
	    		}
	    		
	    	}
	    }

	    @FXML
	    void DeliveryShowAll(ActionEvent event) {
	    	showAllDeliveryBoy();
	    }

	    @FXML
	    void OrderAdd(ActionEvent event) {
	    	
	    	if(txtOrderTaker.getText().trim().isEmpty())
	    	{
	    		notifyMessage(5,"Please Type Order Taker Name");
	    		return;
	    	}
	    	else
	    	{
	    		orderTakerMst = new OrderTakerMst();
	    		orderTakerMst.setOrderTakerName(txtOrderTaker.getText());
	    		orderTakerMst.setStatus("ACTIVE");
	    		orderTakerMst.setBranchCode(SystemSetting.getSystemBranch());
	    		
	    		
	    		// checking if its already saved or not
	    		ResponseEntity<OrderTakerMst> orderTakerSaved = RestCaller.getOrderTakerId(txtOrderTaker.getText());
	    		if(null!= orderTakerSaved.getBody())
	    		{
	    			orderTakerMst = orderTakerSaved.getBody();
	    			orderTakerMst.setStatus("ACTIVE");
		    		RestCaller.updateOrdertaker(orderTakerMst);
		    		showAllOrderTaker();
	    		}
	    		else
	    		{
	    		ResponseEntity<OrderTakerMst> respentity = RestCaller.saveOrderTakerMst(orderTakerMst);
	    		orderTakerMst = respentity.getBody();
	    		notifyMessage(5,"Saved");
	    		orderTakerList.add(orderTakerMst);
	    		tblOrderTaker.setItems(orderTakerList);
	    		clOrder.setCellValueFactory(cellData -> cellData.getValue().getorderTakerNameProperty());
	    		clOrderStatus.setCellValueFactory(cellData -> cellData.getValue().getstatusProperty());
	    		txtOrderTaker.clear();
	    		orderTakerMst = null;
	    	}
	    	}

	    }

	    @FXML
	    void OrderDelete(ActionEvent event) {

	    	if(null != orderTakerMst)
	    	{
	    		if (null!= orderTakerMst.getId())
	    		{
	    			orderTakerMst.setStatus("DELETED");
		    		RestCaller.updateOrdertaker(orderTakerMst);
		    		notifyMessage(5, "Deleted");
		    		tblOrderTaker.getItems().clear();
		    		ResponseEntity<List<OrderTakerMst>> orderSaved = RestCaller.getOrderTakerMst();
			    	orderTakerList = FXCollections.observableArrayList(orderSaved.getBody());
			    	tblOrderTaker.setItems(orderTakerList);
			    	
	    		}
	    	
	    	}
	    	
	    }

	    @FXML
	    void OrderShowAll(ActionEvent event) {
	    	showAllOrderTaker();
	    }
	    @FXML
	    void tableAdd(ActionEvent event) {
	    	if(texttbl.getText().trim().isEmpty())
	    	{
	    		notifyMessage(5,"Type Table Name");
	    		texttbl.requestFocus();
	    	}
	    	else
	    	{
	    	try
	    	{
	    	
	        addKotTable = new AddKotTable();
	    	addKotTable.setTableName(texttbl.getText());
	    	addKotTable.setStatus("ACTIVE");
	    	addKotTable.setBranchCode(SystemSetting.systemBranch);
	        String vNo= RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"KMT");
	        addKotTable.setId(vNo);
	        ResponseEntity<AddKotTable> savedTable = RestCaller.getTablebyName(texttbl.getText());
	        if(null != savedTable.getBody())
	        {
	        	addKotTable = savedTable.getBody();
	        	addKotTable.setStatus("ACTIVE");
	        	ResponseEntity<AddKotTable> reponse  = RestCaller.saveAddKotTable(addKotTable);
	        	addKotTable = reponse.getBody();
	        	showAllTable();
	        	tableOccupiedEvent.setTableId(addKotTable.getId());
	        	eventBus.post(tableOccupiedEvent);
	        	        	
	        }
	        else {
			ResponseEntity<AddKotTable> reponse  = RestCaller.saveAddKotTable(addKotTable);	
			//addKotTable.setId(((AddKotTable)reponse.getBody()).getId());
			addKotTable = reponse.getBody();
			tableOccupiedEvent.setTableId(addKotTable.getId());
			eventBus.post(tableOccupiedEvent);
			//RestCaller.saveAddKotTable( addKotTable);
			eventBus.post(addKotTable);
			addKotTableList.add(addKotTable);
			tblkot.setItems(addKotTableList);
			tn.setCellValueFactory(cellData -> cellData.getValue().gettableNameProperty());
			clTableStatus.setCellValueFactory(cellData -> cellData.getValue().getstatusProperty());
			texttbl.clear();
			notifyMessage(5,"Item Added");
	        }
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println(e);
	    		return;
	    	}
	    	}
	    }

	    @FXML
	    void tableWaiter(ActionEvent event) {
	    	if(textwaiter.getText().trim().isEmpty())
	    	{
	    		notifyMessage(5,"Type Waiter Name");
	    		textwaiter.requestFocus();
	    	}
	    	else
	    	{
	    	try
	    	{
	    	addKotWaiter = new AddKotWaiter();
	    	addKotWaiter.setWaiterName(textwaiter.getText());
	    	addKotWaiter.setStatus("ACTIVE");
	    	addKotWaiter.setBranchCode(SystemSetting.systemBranch);
	    	String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"KMW");
	    	addKotWaiter.setId(vNo);
	    	ResponseEntity<AddKotWaiter> waiterSaved = RestCaller.getWaiterbyName(textwaiter.getText());
	    	if(null != waiterSaved.getBody())
	    	{
	    		addKotWaiter = waiterSaved.getBody();
	    		addKotWaiter.setStatus("ACTIVE");
	    		ResponseEntity<AddKotWaiter> reponse  = RestCaller.saveAddKotWaiter(addKotWaiter);
	    		showAllWaiter();
	    	}
	    	else {
			ResponseEntity<AddKotWaiter> reponse  = RestCaller.saveAddKotWaiter(addKotWaiter);
			addKotWaiter = reponse.getBody();
			//addKotWaiter.setId(((AddKotWaiter)reponse.getBody()).getId());
			//RestCaller.saveAddKotWaiter( addKotWaiter);
			eventBus.post(addKotWaiter);
		
		  addKotWaiterList.add(addKotWaiter); 
		  tblkot1.setItems(addKotWaiterList);
		  wn1.setCellValueFactory(cellData ->cellData.getValue().getWaiterNameProperty());
		  clWaiterStatus.setCellValueFactory(cellData ->cellData.getValue().getstatusProperty());
		 textwaiter.clear();
			notifyMessage(5,"Item Added");
			showAllWaiter();
	    	}
	    	}
	    	catch(Exception e)
	    	{
	    		return;
	    	}
	    	}
	    	
	    }
	    
		@FXML
		private void initialize() {
			addKotTable = new AddKotTable();
			addKotWaiter = new AddKotWaiter();
			tableOccupiedEvent = new TableOccupiedEvent();
			eventBus.register(this);
			tblkot.setItems( addKotTableList);
			tblkot1.setItems( addKotWaiterList);
			tblDeliveryBoy.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					System.out.println("getSelectionModel");
					if (null != newSelection.getId()) {

						deliveryBoyMst = new DeliveryBoyMst();
						deliveryBoyMst.setId(newSelection.getId());
						
					}
				}
			});
			tblOrderTaker.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					System.out.println("getSelectionModel");
					if (null != newSelection.getId()) {

						orderTakerMst = new OrderTakerMst();
						orderTakerMst.setId(newSelection.getId());
						
					}
				}
			});
		}

	    @FXML
	    void bbtnDeleteWaiter(ActionEvent event) {
	    	if(null != addKotWaiter)
	    	{
	    		if(null != addKotWaiter.getId())
	    		{
	    	addKotWaiter.setStatus("DELETED");
	    	ResponseEntity<AddKotWaiter> reponse  = RestCaller.saveAddKotWaiter(addKotWaiter);
	    	addKotWaiterList.clear();
	    	tblkot1.setItems(null);
	    	showAllWaiter();
	    		}
	    	}
	    }

	    @FXML
	    void btnDeleteTable(ActionEvent event) {
	    	
	    	if(null != addKotTable)
	    	{
	    		if(null != addKotTable.getId())
	    		{
	    	addKotTable.setStatus("DELETED");
	    	ResponseEntity<AddKotTable> reponse  = RestCaller.saveAddKotTable(addKotTable);
	    	addKotTableList.clear();
	    	tblkot.setItems(null);
	    	showAllTable();
	    		}
	    	}
	    }

	    @FXML
	    void tableClicked(MouseEvent event) {
	    	//btnDelete.setVisible(true);
	    	if (tblkot.getSelectionModel().getSelectedItem() != null) {
				TableViewSelectionModel selectionModel = tblkot.getSelectionModel();
				addKotTableList1 = selectionModel.getSelectedItems();
				for (AddKotTable adddkot : addKotTableList1) {
					addKotTable.setId(adddkot.getId());
					addKotTable.setTableName(adddkot.getTableName());
				}
	    	}

	    }
	    @FXML
	    void waiterClicked(MouseEvent event) {
	    	if (tblkot1.getSelectionModel().getSelectedItem() != null) {
				TableViewSelectionModel selectionModel = tblkot1.getSelectionModel();
				addKotWaiterList1 = selectionModel.getSelectedItems();
				for (AddKotWaiter addwaiter : addKotWaiterList1) {
					addKotWaiter.setId(addwaiter.getId());
					addKotWaiter.setWaiterName(addwaiter.getWaiterName());
				}
	    	}
	    }
	    private void showAllWaiter()
	    {
	    	ResponseEntity<List<AddKotWaiter>> waiterSaved = RestCaller.getWaiter();
 			addKotWaiterList = FXCollections.observableArrayList(waiterSaved.getBody());
 			tblkot1.setItems(addKotWaiterList);
			wn1.setCellValueFactory(cellData -> cellData.getValue().getWaiterNameProperty());
			clWaiterStatus.setCellValueFactory(cellData -> cellData.getValue().getstatusProperty());
	    }
	    private void showAllTable()
	    {
	    	ResponseEntity<List<AddKotTable>> tableSaved = RestCaller.getTable();	
			addKotTableList = FXCollections.observableArrayList(tableSaved.getBody());
			tblkot.setItems(addKotTableList);
			tn.setCellValueFactory(cellData -> cellData.getValue().gettableNameProperty());
			clTableStatus.setCellValueFactory(cellData -> cellData.getValue().getstatusProperty());
	    }
	    private void showAllDeliveryBoy()
	    {
	    	tblDeliveryBoy.getItems().clear();
	    	ResponseEntity<List<DeliveryBoyMst>> deliverySaved = RestCaller.getDeliveryBoyMst();
	    	DeliveryBoyList = FXCollections.observableArrayList(deliverySaved.getBody());
	    	tblDeliveryBoy.setItems(DeliveryBoyList);
	    	clDeliveryName.setCellValueFactory(cellData -> cellData.getValue().getdeliveryBoyNameProperty());
    		clDeliveryStatus.setCellValueFactory(cellData -> cellData.getValue().getstatusProperty());
			
	    }
	    private void showAllOrderTaker()
	    {
	    	tblDeliveryBoy.getItems().clear();
	    	
	    	ResponseEntity<List<OrderTakerMst>> orderSaved = RestCaller.getOrderTakerMst();
	    	orderTakerList = FXCollections.observableArrayList(orderSaved.getBody());
	    	tblOrderTaker.setItems(orderTakerList);
	    	clOrder.setCellValueFactory(cellData -> cellData.getValue().getorderTakerNameProperty());
    		clOrderStatus.setCellValueFactory(cellData -> cellData.getValue().getstatusProperty());
    	
	    }
	    
	    public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");

					Image img = new Image("done.png");
					Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
							.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
								@Override
								public void handle(ActionEvent event) {
									System.out.println("clicked on notification");
								}
							});
					notificationBuilder.darkStyle();
					notificationBuilder.show();
				}
	    @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	      private void PageReload() {
	      	
	      	
	  		
	  	}
}
	
	

 
	
	

