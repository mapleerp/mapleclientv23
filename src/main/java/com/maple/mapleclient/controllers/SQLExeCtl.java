package com.maple.mapleclient.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.controlsfx.control.Notifications;
import org.springframework.web.client.RestTemplate;

import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.ExecuteSql;
import com.maple.mapleclient.restService.RestCaller;

import ch.qos.logback.classic.pattern.Util;
import javafx.application.HostServices;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class SQLExeCtl {
	String taskid;
	String processInstanceId;

	@FXML
	private ListView listResult;

	@FXML
	private TextArea txtSqlStatement;

	@FXML
	private Button btnOk;

	@FXML
	private Button btnCommit;

	@FXML
	private Button btnRollBack;

	@FXML
	private Button btnClear;

	@FXML
	private TextArea txtSqlResult;

//=======================2021-5-22 surya ===============================

	@FXML
	private Button txtExportToExcel;

	@FXML
	void ExportToExcel(ActionEvent event) {

		if (txtSqlStatement.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Enter Sql Statement...!!!", false);
			return;
		} else {

			String sqlStatement = txtSqlStatement.getText();

		}

		String resutlString = "";
		RestTemplate restTemplate = new RestTemplate();
		txtSqlStatement.setText(txtSqlStatement.getText().replaceAll("/", "%21"));

		ExecuteSql executeSql = new ExecuteSql();

		executeSql.setSqlToExecute(txtSqlStatement.getText());
		List<Map<String, Object>> sqlResult = RestCaller.executeSql(executeSql);

		int rownum = 0;
		int cellnum = 0;

		if (sqlResult.size() > 0) {
			ExportToExcel(sqlResult);
		}

	}

//=========================2021-5-22 surya ==============================

	private void ExportToExcel(List<Map<String, Object>> sqlResult) {

		Set<String> keySet = sqlResult.get(0).keySet();

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Ssql Result");

		int rownum = 0;
		int cellnum = -1;
		
		Iterator itr = keySet.iterator();
		while (itr.hasNext()) {
			rownum = 0;
			cellnum++;
			String element = (String) itr.next();

			Row row = sheet.getRow(rownum);
			if(null==row) {
				row = sheet.createRow(rownum);
			}
			Cell cell = row.createCell(cellnum);
			cell.setCellValue(element);
			
			for(int i=0; i<sqlResult.size(); i++)
			{
				Object valueObject =  sqlResult.get(i).get(element);
				System.out.println(valueObject);
				if(null != valueObject)
				{
					String values = valueObject.toString();
					 rownum++;
					 Row row2 = sheet.getRow(rownum);
					 
					 if(null==row2) {
						 row2 = sheet.createRow(rownum);
						}
					 

					 Cell cell2 = row2.createCell(cellnum);

					cell2.setCellValue(valueObject.toString());


				}else {
					rownum++;
				}

			}
			

		}
		
		try {
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Random rand = new Random();
			int rndFile = rand.nextInt(10000);
					
			String FileName =rndFile+".xls";
		    FileOutputStream out = 
		            new FileOutputStream(new File(FileName));
		    workbook.write(out);
		    out.close();
		    System.out.println("Excel written successfully..");
		    
		    
			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(FileName);
			
			//Desktop desktop = Desktop.getDesktop();
			//File file = new File(FileName);
			//desktop.open(file);
			
		     
		} catch (FileNotFoundException e1) {
		   System.out.println(e1.toString());
		} catch (IOException e2) {
			 System.out.println(e2.toString());
		}

	}

	@FXML
	void ClearTxt(ActionEvent event) {
		txtSqlResult.setText("");

		listResult.getItems().clear();
	}

	@FXML
	void CommitStatement(ActionEvent event) {

	}

	@FXML
	private void initialize() {
		listResult.setEditable(true);
		txtSqlResult.setEditable(true);

	}

	@FXML
	void ExecuteStatement(ActionEvent event) {

		if (txtSqlStatement.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Enter Sql Statement...!!!", false);
			return;
		} else {

			String sqlStatement = txtSqlStatement.getText();

		}

		String resutlString = "";
		RestTemplate restTemplate = new RestTemplate();
//		searchData = "cust";
		txtSqlStatement.setText(txtSqlStatement.getText().replaceAll("/", "%21"));

		ExecuteSql executeSql = new ExecuteSql();

		executeSql.setSqlToExecute(txtSqlStatement.getText());
		List<Map<String, Object>> sqlResult = RestCaller.executeSql(executeSql);
		for (int i = 0; i < sqlResult.size(); i++) {
			final int finalIdx = i;
			Map oneRow = sqlResult.get(i);
			// txtSqlResult.setText(oneRow.toString());
			listResult.getItems().add(oneRow.toString());
			resutlString = resutlString + oneRow.toString() + "\n";
			txtSqlResult.setText(resutlString);

		}

		/*
		 * TableView<ObservableList<String>> tableView = new TableView<>(); List<String>
		 * columnNames = new ArrayList(); columnNames.add("AAAA");
		 * columnNames.add("BBBB");
		 * 
		 * for (int i = 0; i < columnNames.size(); i++) { final int finalIdx = i;
		 * TableColumn<ObservableList<String>, String> column = new TableColumn<>(
		 * columnNames.get(i) ); column.setCellValueFactory(param -> new
		 * ReadOnlyObjectWrapper<>(param.getValue().get(finalIdx)) );
		 * tableView.getColumns().add(column); }
		 * 
		 * 
		 * 
		 * tableView.refresh();
		 */

	}

	@FXML
	void RollBack(ActionEvent event) {

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

	}

}
