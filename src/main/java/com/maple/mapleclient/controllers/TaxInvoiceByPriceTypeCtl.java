package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;

import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.InvoiceFormatMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.RadioButton;

public class TaxInvoiceByPriceTypeCtl {
	
	String taskid;
	String processInstanceId;
	
	private ObservableList<InvoiceFormatMst> reportListTable = FXCollections.observableArrayList();
	InvoiceFormatMst jasperByPriceType = null;

    @FXML
    private ComboBox<String> cmbPriceType;

    @FXML
    private ComboBox<String> cmbJasper;

    @FXML
    private Button tbnSave;

    @FXML
    private Button tbnDelete;

    @FXML
    private TableView<InvoiceFormatMst> tblReport;

    @FXML
    private TableColumn<InvoiceFormatMst, String> clPriceType;

    @FXML
    private TableColumn<InvoiceFormatMst, String> clJasperName;
    
    @FXML
    private TableColumn<InvoiceFormatMst, String> clReportName;

    @FXML
    private TableColumn<InvoiceFormatMst, String> clGst;

    @FXML
    private TableColumn<InvoiceFormatMst, String> clBatch;

    @FXML
    private TableColumn<InvoiceFormatMst, String> clMrp;

    @FXML
    private TableColumn<InvoiceFormatMst, String> clDiscount;
    
    @FXML
    private TableColumn<InvoiceFormatMst, String> clReportType;
 


    @FXML
    private ComboBox<String> cmbReportName;

    @FXML
    private RadioButton rdGst;

    @FXML
    private RadioButton rdBatch;

    @FXML
    private RadioButton rdMrp;

    @FXML
    private RadioButton rdDiscount;


    @FXML
    private ComboBox<String> cmbReportType;
   
    @FXML
    private RadioButton rdDefaultStatus;
    
    @FXML
   	private void initialize() {
       	cmbPriceDefenition();
       	
       	cmbJasper.getItems().add("HWTaxInvoiceWithOutMrp");
       	cmbJasper.getItems().add("HWTaxInvoice");
       	
       	cmbJasper.getItems().add("HWTaxInvoiceWithGst");
       	cmbJasper.getItems().add("discountTaxInvoice");
       	cmbJasper.getItems().add("taxinvoicee");
       	cmbJasper.getItems().add("TaxinvoiceeWithbatch");
    	cmbJasper.getItems().add("WeighBridgeTaxInvoice");
    	cmbJasper.getItems().add("taxInvoiceGstNoDiscount");
    	cmbJasper.getItems().add("discountTaxListPriceInvoice");
    	cmbJasper.getItems().add("PharmacyWholeSaleInvoice");


    	
//       	
//       	cmbJasper.getItems().add("taxinvoiceehw2");
//       	cmbJasper.getItems().add("taxinvoiceehw3");
//       	cmbJasper.getItems().add("taxinvoiceehw4");
//       	cmbJasper.getItems().add("taxinvoiceehw5");
//       	cmbJasper.getItems().add("taxinvoiceehw6");
//       	cmbJasper.getItems().add("taxinvoiceehw7");
//       	cmbJasper.getItems().add("taxinvoiceehw8");
//

   

       	cmbReportName.getItems().add("TAX INVOICE");
       	
       	cmbReportType.getItems().add("HARDWARE");
       	cmbReportType.getItems().add("BAKERY");


       	FillTable();
       	
		tblReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					
					jasperByPriceType = new InvoiceFormatMst();
					jasperByPriceType.setId(newSelection.getId());
				}
			}
		});
		
       	
    }

    @FXML
    void Delete(ActionEvent event) {
    	
    	if(null != jasperByPriceType)
    	{
    		if(null != jasperByPriceType.getId())
    		{
    			RestCaller.DeleteJasperByPriceType(jasperByPriceType.getId());
    			jasperByPriceType = null;
    			FillTable();

    		}
    	}

    }

    @FXML
    void Save(ActionEvent event) {
    	
//    	if(null == cmbReportName.getValue())
//    	{
//    		notifyMessage(3, "Please select report name", false);
//    		return;
//    	}
//    	

    	if(null == cmbJasper.getValue())
    	{
    		notifyMessage(3, "Please select jasper name", false);
    		return;
    	}
    	
//    	if(null == cmbReportType.getValue())
//    	{
//    		notifyMessage(3, "Please select report type", false);
//    		return;
//    	}
    	
		jasperByPriceType= new InvoiceFormatMst();

    	String priceTypeId;
    	if(null != cmbPriceType.getValue())
    	{
    	ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
				.getPriceDefenitionByName(cmbPriceType.getSelectionModel().getSelectedItem().toString());

		if(priceDefenitionSaved.getBody().size()>0)
		{
			priceTypeId = priceDefenitionSaved.getBody().get(0).getId();
		} else {
			notifyMessage(3, "Please select price type", false);
    		return;
		}
		jasperByPriceType.setPriceTypeId(priceTypeId);

    	}
		String gst,Batch,Discount;
//		jasperByPriceType.setReportName(cmbReportName.getSelectionModel().getSelectedItem().toString());
		jasperByPriceType.setJasperName(cmbJasper.getSelectionModel().getSelectedItem().toString());
		if(rdBatch.isSelected())
		{
			jasperByPriceType.setBatch("YES");
			Batch="YES";
		} else {
			jasperByPriceType.setBatch("NO");
			Batch="NO";

		}
		
		if(rdDiscount.isSelected())
		{
			jasperByPriceType.setDiscount("YES");
			Discount = "YES";
		} else {
			jasperByPriceType.setDiscount("NO");
			Discount = "NO";
		}
		
		if(rdGst.isSelected())
		{
			jasperByPriceType.setGst("YES");
			gst="YES";
		} else {
			jasperByPriceType.setGst("NO");
			gst="NO";
		}
		
		if(rdDefaultStatus.isSelected())
		{
			jasperByPriceType.setStatus("DEFAULT");
		}
		
		String reportName=generateInvoiceName(gst,Batch,Discount);
		jasperByPriceType.setReportName(reportName);
		ResponseEntity<InvoiceFormatMst> jasperByPriceTypeSaved = RestCaller.saveJasperByPriceType(jasperByPriceType);
		FillTable();
		
		jasperByPriceType = null;
		
		cmbJasper.getSelectionModel().clearSelection();
		cmbPriceType.getSelectionModel().clearSelection();
		cmbReportName.getSelectionModel().clearSelection();
		cmbReportType.getSelectionModel().clearSelection();
		rdBatch.setSelected(false);
		rdDefaultStatus.setSelected(false);
		rdDiscount.setSelected(false);
		rdGst.setSelected(false);
		rdMrp.setSelected(false);

    }
    
	private String generateInvoiceName(String gst, String batch, String discount) {
		String reportname="";
		reportname = "GST"+gst+"Batch"+batch+"Discount"+discount;

		return reportname;
	}



	private void FillTable() {

		ResponseEntity<List<InvoiceFormatMst>> jasperResp = RestCaller.getAllJasperByPriceType();
		reportListTable = FXCollections.observableArrayList(jasperResp.getBody());
		
		
		
		tblReport.setItems(reportListTable);
		
		for(InvoiceFormatMst jasper : reportListTable)
		{
			if(null != jasper.getPriceTypeId()) 
			{
				ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller.getPriceNameById(jasper.getPriceTypeId());
				jasper.setPriceName(priceDefenitionMstResp.getBody().getPriceLevelName());
			}
		}
		clPriceType.setCellValueFactory(cellData -> cellData.getValue().getPriceTypeProperty());
		clJasperName.setCellValueFactory(cellData -> cellData.getValue().getJasperNameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clDiscount.setCellValueFactory(cellData -> cellData.getValue().getDiscountProperty());
		clGst.setCellValueFactory(cellData -> cellData.getValue().getGstProperty());
		clReportName.setCellValueFactory(cellData -> cellData.getValue().getReportNameProperty());



	}

	private void cmbPriceDefenition() {

		cmbPriceType.getItems().clear();

		System.out.println("=====PriceTypeOnKEyPress===");
		ArrayList priceType = new ArrayList();

		priceType = RestCaller.getPriceDefinition();
		Iterator itr = priceType.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object priceLevelName = lm.get("priceLevelName");
			Object id = lm.get("id");
			if (id != null) {
				cmbPriceType.getItems().add((String) priceLevelName);

			}

		}

	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}


}
