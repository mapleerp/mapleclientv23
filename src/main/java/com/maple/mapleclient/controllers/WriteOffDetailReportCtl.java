package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.WriteOffDetailAndSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class WriteOffDetailReportCtl {

	
	
		private ObservableList<WriteOffDetailAndSummaryReport> writeOffDetailReportList = FXCollections.observableArrayList();
	 	
		@FXML
	    private DatePicker dpFrom;

	    @FXML
	    private DatePicker dpTo;

	    @FXML
	    private Button btnGenerate;

	    @FXML
	    private Button btnPrint;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private TableView<WriteOffDetailAndSummaryReport> tblWriteOffDetail;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnVoucher;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnDate;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnUser;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnItem;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnItemCode;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnItemGroup;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnBatch;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, String> clmnExpiry;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, Number> clmnQty;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, Number> clmnRate;

	    @FXML
	    private TableColumn<WriteOffDetailAndSummaryReport, Number> clmnAmount;
	
	    
	    @FXML
	   	private void initialize() {
	       	dpFrom = SystemSetting.datePickerFormat(dpFrom, "dd/MMM/yyyy");
	       	dpTo = SystemSetting.datePickerFormat(dpTo, "dd/MMM/yyyy");
	    }
	    
	    @FXML
	    void clear(ActionEvent event) {
	    	clearField();
	    }

	    private void clearField() {
	    	dpFrom.setValue(null);
	    	dpTo.setValue(null);
	    	tblWriteOffDetail.getItems().clear();
		}

		@FXML
	    void generateReport(ActionEvent event) {
			if(null == dpFrom.getValue())
	    	{
	    		notifyMessage(5, "Please select From date", false);
	    		return;
	    		
	    	} else if (null == dpTo.getValue()) {
	    		notifyMessage(5, "Please select To date", false);
	    		return;
			} else {
						    	
			Date fromDate = SystemSetting.localToUtilDate(dpFrom.getValue());
			String fDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
			
			
			Date toDate = SystemSetting.localToUtilDate(dpTo.getValue());
			String tDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
			
			String reason = "Write Off";
			
			ResponseEntity<List<WriteOffDetailAndSummaryReport>> damageEntryReport = RestCaller.getWriteOffSummary(fDate, tDate,
					SystemSetting.systemBranch,reason);

			writeOffDetailReportList = FXCollections.observableArrayList(damageEntryReport.getBody());
			
			fillTable();
			}
	    	
	    }
		
		private void fillTable() {
			tblWriteOffDetail.setItems(writeOffDetailReportList);	
			
			clmnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
			clmnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
			clmnDate.setCellValueFactory(cellData -> cellData.getValue().getDateProperty());
			clmnExpiry.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
			clmnItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			clmnItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
			clmnItemGroup.setCellValueFactory(cellData -> cellData.getValue().getItemGroupProperty());
			clmnQty.setCellValueFactory(cellData -> cellData.getValue().getQuantityProperty());
			clmnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
			clmnUser.setCellValueFactory(cellData -> cellData.getValue().getUserProperty());
			clmnVoucher.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		}

	    private void notifyMessage(int duration, String msg, boolean success) {
	    	Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}

		@FXML
	    void printReport(ActionEvent event) {
			if(null == dpFrom.getValue())
	    	{
	    		notifyMessage(5, "Please select From date", false);
	    		return;
	    		
	    	} else if (null == dpTo.getValue()) {
	    		notifyMessage(5, "Please select To date", false);
	    		return;
			} else {
						    	
			Date fromDate = SystemSetting.localToUtilDate(dpFrom.getValue());
			String fDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
			
			
			Date toDate = SystemSetting.localToUtilDate(dpTo.getValue());
			String tDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
			

	    	try {
	    		String reason = "Write off";
				JasperPdfReportService.writeOffDetailReport(fDate,tDate,SystemSetting.systemBranch,reason);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
	    }
}
