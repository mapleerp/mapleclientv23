package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class CustomerBalanceReportCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<AccountBalanceReport>AccountBalanceReportTable = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpStartDate;
    @FXML
    private TableView<AccountBalanceReport> tbCustomerReport;
    @FXML
    private TableColumn<AccountBalanceReport, String> clCustomerName;

    @FXML
    private TableColumn<AccountBalanceReport, Number> clDebit;

    @FXML
    private TableColumn<AccountBalanceReport, Number> clCredit;

    @FXML
    private Button btnGenerateReport;
    @FXML
    private Button btnShowReport;
    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null == dpStartDate.getValue()) {
			notifyMessage(5, "Please select start date", false);
			return;
		} 
		 else {

			generateReport();
		}
    }
    @FXML
    void actionShowReport(ActionEvent event) {
    	Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

    	ResponseEntity<List<AccountBalanceReport>> CustomerBalanceReportResponse = RestCaller
				.getAllCustomerBillBalanceBetweenDate(strtDate, strtDate);
    	AccountBalanceReportTable = FXCollections.observableArrayList(CustomerBalanceReportResponse.getBody());
    	fillTable();
    }
    private void  fillTable()
    {
    	tbCustomerReport.setItems(AccountBalanceReportTable);
		clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getAccountHeadsProperty());
		clCredit.setCellValueFactory(cellData -> cellData.getValue().getCreditProperty());
		clDebit.setCellValueFactory(cellData -> cellData.getValue().getDebitProperty());

    }

    private void generateReport() {

		
		Date sdate = SystemSetting.localToUtilDate(dpStartDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		
		
		  try { JasperPdfReportService.AllCustomerBillWiseBalanceReport(strtDate,
				  strtDate); } catch (JRException e) { // TODO Auto-gesnerated catch block
		  e.printStackTrace(); }
		
		
		 


	}
    @FXML
	private void initialize() {
    	
    	dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		System.out.println("sssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		eventBus.register(this);
		dpStartDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
	}
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

    @FXML
    void focusButton(KeyEvent event) {

    }

    @FXML
    void focusEndDate(KeyEvent event) {

    }
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


  private void PageReload() {
  	
}

}
