package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.POSThermalPrintFactory;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class OnlineAtlierSalesWindowCtl {
	String taskid;
	String processInstanceId;

	
	POSThermalPrintABS printingSupport ;
	POSThermalPrintABS printingSupportKOT ;
	 
	private ObservableList<SalesReceipts> salesReceiptsList = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();

	//String invoiceNumberPrefix = SystemSetting.ONLINE_SALES_PREFIX;
	
	// ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();

	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
	
	SalesDtl salesDtl =null;
	SalesTransHdr salesTransHdr = null;

	double qtyTotal = 0;
	double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	String custId = "";
	
	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");

	StringProperty barcodeProperty = new SimpleStringProperty("");

	StringProperty taxRateProperty = new SimpleStringProperty("");

	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");
	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;
    @FXML
    private ComboBox<String> cmbUnit;
	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnMrp;

	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

    @FXML
    private TableColumn<SalesDtl, Number> clAmount;

    @FXML
    private Button btnPrintKot;
	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;



	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField txtSBICard;

	@FXML
	private TextField txtSodexoCard;

	@FXML
	private TextField txtYesCard;
	@FXML
    private TextField custname;
    @FXML
	private TextField custAdress;
    @FXML
	private TextField gstNo;
    
    @FXML
    private ImageView imgUber;

    @FXML
    private ImageView imgZomato;

    @FXML
    private ImageView imdfood;

    @FXML
    private ImageView imgswiggy;

    @FXML
    private ImageView imgambrosia;

    @FXML
    private ComboBox<String> saletype;
    
   
    @FXML
    private ComboBox<String> deliveryboy;
    
    @FXML
    void SaleTypeKeyPress(KeyEvent event) {

    	if (event.getCode() == KeyCode.ENTER) {
    		txtItemname.requestFocus();
    		
    	}
    }
    
    @FXML
    void onEnterCustPopup(KeyEvent event) 
    {
  
    	if (event.getCode() == KeyCode.ENTER) {
    		loadCustomerPopup();
    		
    	}
    }

    @FXML
    void CustomerPopUp(MouseEvent event) {

    	loadCustomerPopup();
    }
	
	
    @FXML
    void PrintKot(ActionEvent event) {
    	
    	
    	
    	ResponseEntity<List<SalesTransHdr>> kotPrint = RestCaller.PrintKotBySalesTransID(salesTransHdr.getId());
    }

		
	
	@FXML
	void mouseClickOnItemName(MouseEvent event) {
		showPopup();
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	@FXML
	private void initialize() {
		//printingSupportKOT = new ImperialKitchenPrint2();
//		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("ONLINESALES RATE EDIT");
//		if(null != getParamValue.getBody())
//		{
//			if(getParamValue.getBody().getValue().equalsIgnoreCase("NO"))
//			{
//				txtRate.setEditable(false);
//			}
//			else
//			{
//				txtRate.setEditable(true);
//			}
//		}
		printingSupport =  POSThermalPrintFactory.getPOSThermalPrintABS(); 
		try {
			printingSupport.setupLogo();
		 
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		

		try {
			printingSupport.setupBottomline(SystemSetting.getPosInvoiceBottomLine());
		
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	
		//ResponseEntity<BranchMst> respentityBranch = RestCaller.SearchMyBranch();
 
		imgUber.setVisible(false);
		imgswiggy.setVisible(false);
		imgZomato.setVisible(false);
		imdfood.setVisible(false);
		//imgambrosia.setVisible(false);
		saletype.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				try {
					
			
				if (newValue.matches("UBER SALES")) {
					imgUber.setVisible(true);
					imgswiggy.setVisible(false);
					imgZomato.setVisible(false);
					imdfood.setVisible(false);
					imgambrosia.setVisible(false);
				}}
				catch (Exception e) {
				
				}
				 if(newValue.matches("SWIGGY"))
						{imgswiggy.setVisible(true);
						imgZomato.setVisible(false);
						imdfood.setVisible(false);
						imgUber.setVisible(false);
						imgambrosia.setVisible(false);
				}
				
					else if(newValue.matches("ZOMATO"))
					{
						imgZomato.setVisible(true);
						imgUber.setVisible(false);
						imgswiggy.setVisible(false);
						imdfood.setVisible(false);
						imgambrosia.setVisible(false);
					}
					else if(newValue.matches("FOOD PANDA"))
					{
						imdfood.setVisible(true);
						imgUber.setVisible(false);
						imgswiggy.setVisible(false);
						imgZomato.setVisible(false);
						imgambrosia.setVisible(false);
					}
					else 
					{
						imdfood.setVisible(false);
						imgUber.setVisible(false);
						imgswiggy.setVisible(false);
						imgZomato.setVisible(false);
						imgambrosia.setVisible(true);
					}
					
			}

		});
 		
		/*
		 * Add list box items to salesTransHdr
		 */
		saletype.getItems().add("UBER SALES");
		saletype.getItems().add("SWIGGY");
		saletype.getItems().add("ZOMATO");
		saletype.getItems().add("FOOD PANDA");
		saletype.getItems().add("ONLINE");
	
		/*deliveryboy.getItems().add("DELIVERY BOY -001");
		deliveryboy.getItems().add("DELIVERY BOY -002");
		deliveryboy.getItems().add("DELIVERY BOY -003");
		deliveryboy.getItems().add("DELIVERY BOY -004");
		deliveryboy.getItems().add("DELIVERY BOY -005");*/
		
		/*
		 * Create an instance of SalesDtl. SalesTransHdr entity will be refreshed after
		 * final submit. SalesDtl will be added on AddItem Function
		 * 
		 */

		//salesDtl = new SalesDtl();
		/*
		 * Fieds with Entity property
		 */
		
		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);
		
		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")|| ((Double.parseDouble(newValue))>5000)) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

	
		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPaidamount.setText(oldValue);
				}
			}
		});
		txtChangeamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtChangeamount.setText(oldValue);
				}
			}
		});
		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashtopay.setText(oldValue);
				}
			}
		});
		txtSBICard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSBICard.setText(oldValue);
				}
			}
		});
		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
				ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
//				cmbUnit.setValue(cmbUnit.getSelectionModel().getSelectedItem());
				ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(), getUnit.getBody().getId());
				if(null != getMultiUnit.getBody() )
				{
				txtRate.setText(Double.toString(getMultiUnit.getBody().getPrice()));
				}
				else
					txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));
			}
		});
		txtYesCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtYesCard.setText(oldValue);
				}
			}
		});

		txtSodexoCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSodexoCard.setText(oldValue);
				}
			}
		});
		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});
		
		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtPaidamount.getText().length() > 0)) {

					double paidAmount = Double.parseDouble(txtPaidamount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmt = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt )-cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
						changeAmtProperty.set(newrate.toPlainString());

					} 
				}else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmount = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((cardAmount )-cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
					changeAmtProperty.set(newrate.toPlainString());
					
				}
			}
		});
		
		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtcardAmount.getText().length() > 0)) {

					double cardAmt = Double.parseDouble(txtcardAmount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmount = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt )-cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
						changeAmtProperty.set(newrate.toPlainString());

					} 
				}else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmt = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((paidAmt )-cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
					changeAmtProperty.set(newrate.toPlainString());
					
				}
			}
		});
	
		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {


					salesDtl.setId(newSelection.getId());
					txtBarcode.setText(newSelection.getBarcode());
					txtItemname.setText(newSelection.getItemName());
					txtBatch.setText(newSelection.getBatchCode());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));
					}
			}
		});

		eventBus.register(this);

		//btnSave.setDisable(true);
		itemDetailTable.setItems(saleListTable);

		txtBarcode.requestFocus();
		////////////////////
		

	}

	@FXML
	void deleteRow(ActionEvent event) {
		try {

			if (null != salesDtl) {
				if (null != salesDtl.getId()) {
					System.out.println("toDeleteSale.getId()" + salesDtl.getId());
					RestCaller.deleteSalesDtl(salesDtl.getId());

					ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
					saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
					FillTable();
					salesDtl = new SalesDtl();
					
					notifyMessage(5, " Item Deleted Successfully");
					txtItemname.clear();
					txtBarcode.clear();
					txtBatch.clear();
					txtRate.clear();
					txtQty.clear();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void hold(ActionEvent event) {

	}

	@FXML
	void save(ActionEvent event) {
		
		finalSave();
		
	}
	private void finalSave()
	{
		/*
		 * Final Save
		 * Finalsave
		 */
		try {

		txtChangeamount.setText("00.0");
		Double paidAmount = 0.0;
		Double cardAmount = 0.0;
		String card = "";
		if (!txtSBICard.getText().trim().isEmpty()) {
			card = txtSBICard.getText();
		} else if (!txtYesCard.getText().trim().isEmpty()) {
			card = txtYesCard.getText();
		} else if (!txtSodexoCard.getText().trim().isEmpty()) {
			card = txtSodexoCard.getText();
		}
		salesTransHdr.setCardNo(card);

		Double cashToPay = Double.parseDouble(txtCashtopay.getText());
		//salesTransHdr.setCashPay(cashToPay);
		salesTransHdr.setInvoiceAmount(cashToPay);
		
		
		if (!txtPaidamount.getText().trim().isEmpty()) {

			paidAmount = Double.parseDouble(txtPaidamount.getText());
			//salesTransHdr.setPaidAmount(paidAmount);
		}

		if (!txtcardAmount.getText().trim().isEmpty()) {

			cardAmount = Double.parseDouble(txtcardAmount.getText());
			//salesTransHdr.setPaidAmount(cardAmount);
		}
		if (!txtChangeamount.getText().trim().isEmpty()) {
		Double changeAmount = Double.parseDouble(txtChangeamount.getText());
		salesTransHdr.setChangeAmount(changeAmount);
		}
		eventBus.post(salesTransHdr);

		// Double
		// change=Double.parseDouble(txtPaidamount.getText())-Double.parseDouble(txtCashtopay.getText());
		// txtChangeamount.setText(Double.toString(change));
		// btnSave.setDisable(false);
		// btnSave.setDisable(true);

		

		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		if (saledtlSaved.getBody().size() == 0) {
			return;
		}

		//String financialYear = SystemSetting.getFinancialYear();
		//String vNo = RestCaller.getVoucherNumber(financialYear + "POS");
		
		
		//String nValue = vNo.substring(12);
		//salesTransHdr.setNumericVoucherNumber(Long.parseLong(nValue));
		//salesTransHdr.setVoucherNumber(vNo);
		
		
		//if((Double.parseDouble(txtCashtopay.getText()))<=(paidAmount+cardAmount))
		//{

		String salesMode =saletype.getSelectionModel().getSelectedItem().toString();
		
	//	salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
		
		salesTransHdr.setSalesMode(salesMode);
		salesTransHdr.setCreditOrCash("CREDIT");
			 
		
		/*
		 * new url for getting account heads instead of customer mst=========05/0/2022
		 */
//		ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerByNameAndBarchCode(salesMode,SystemSetting.systemBranch);
		ResponseEntity<AccountHeads> accountHeadsResponse = RestCaller.getAccountHeadsByNameAndBranchCode(salesMode,SystemSetting.systemBranch);
				
		salesTransHdr.setAccountHeads(accountHeadsResponse.getBody());
		salesTransHdr.setSalesMode("ONLINE");
		ResponseEntity<AccountReceivable> accountReceivableResp = RestCaller
				.getAccountReceivableBySalesTransHdrId(salesTransHdr.getId());
		AccountReceivable accountReceivable = null;
		if (null != accountReceivableResp.getBody()) {
			accountReceivable = accountReceivableResp.getBody();
		} else {
			accountReceivable = new AccountReceivable();
		}

		accountReceivable.setAccountHeads(accountHeadsResponse.getBody());

		
			accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
			accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
			accountReceivable.setBalanceAmount(Double.parseDouble(txtCashtopay.getText()));
		accountReceivable.setAccountId(accountHeadsResponse.getBody().getId());
		LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		LocalDate dueDate = due.plusDays(accountHeadsResponse.getBody().getCreditPeriod());
		accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
		accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
		accountReceivable.setSalesTransHdr(salesTransHdr);
		accountReceivable.setRemark("OnlineSales");
		LocalDate due1 = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		accountReceivable.setVoucherDate(java.sql.Date.valueOf(due1));
		accountReceivable.setPaidAmount(0.0);
		ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
		accountReceivable = respentity.getBody();
		
		
		// Delete any sales receiots previously saved by mistake
		
		ResponseEntity<List<SalesReceipts>> srList = RestCaller.getSalesReceiptsByTransHdrId(salesTransHdr.getId());
		if(srList.getBody().size()>0) {
		 
		 
			RestCaller.deleteSalesReceiptsByTransHdr(salesTransHdr);
		}
		  
		 SalesReceipts salesReceipts = new SalesReceipts();
		  
		  ResponseEntity<AccountHeads> accountsResp = RestCaller.getAccountHeadByName(salesMode);
			 
			salesReceipts.setReceiptMode("CREDIT");
			 salesReceipts.setReceiptAmount(salesTransHdr.getInvoiceAmount());
			 salesReceipts.setSalesTransHdr(salesTransHdr);
			 salesReceipts.setAccountId(accountsResp.getBody().getId());
			 salesReceipts.setBranchCode(salesTransHdr.getBranchCode());
			 
			 RestCaller.saveSalesReceipts(salesReceipts);
			 
		  
			 salesTransHdr.setVoucherDate(SystemSetting.systemDate);
		RestCaller.updateSalesTranshdr(salesTransHdr);
		salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
		
		
	
		
		DayBook dayBook = new DayBook();
		dayBook.setBranchCode(salesTransHdr.getBranchCode());
		dayBook.setDrAccountName(salesTransHdr.getAccountHeads().getAccountName());
		dayBook.setDrAmount(salesTransHdr.getInvoiceAmount());
		dayBook.setNarration(salesTransHdr.getAccountHeads().getAccountName()+salesTransHdr.getVoucherNumber());
		dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
		dayBook.setSourceVoucherType("ONLINE");
		dayBook.setCrAccountName("BANK ACCOUNT");
		dayBook.setCrAmount(salesTransHdr.getInvoiceAmount());
	
		LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
		dayBook.setsourceVoucherDate(Date.valueOf(rdate));
		ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		
		
		
		
		notifyMessage(5,"Saved");

		  salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
		  
		  printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId()) ;
		 
		salesTransHdr = null;
		salesDtl = null;
		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtPaidamount.setText("");
		txtSBICard.setText("");
		txtSodexoCard.setText("");
		txtYesCard.setText("");
		custname.setText("");
		custAdress.setText("");
		saletype.setValue(null);
		deliveryboy.setValue(null);
		saleListTable.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
	}
    @FXML
    void SaveOnKey(KeyEvent event) {
    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
    		finalSave();
    	}
    }
    @FXML
    void saveKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
    		finalSave();
    	}
    }

	@FXML
	void EnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			// if(event.getSource());

			txtQty.requestFocus();
		}

	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtItemname.requestFocus();
			showPopup();
		}
		if (event.getCode() == KeyCode.ENTER) {
		
			btnSave.requestFocus();
		}

	}

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnSave.setDisable(false);

			Double change = Double.parseDouble(txtPaidamount.getText()) - Double.parseDouble(txtCashtopay.getText());
			txtChangeamount.setText(Double.toString(change));

		}

	}

	@FXML
	void calcPaid(KeyEvent event) {
		/*
		 * 
		 * if (event.getCode() == KeyCode.ENTER) { if (null == salesTransHdr) {
		 * salesTransHdr = new SalesTransHdr(); salesTransHdr.setInvoiceAmount(0.0);
		 * salesTransHdr.getId(); ResponseEntity<SalesTransHdr> respentity =
		 * RestCaller.saveSalesHdr(salesTransHdr); salesTransHdr = respentity.getBody();
		 * 
		 * } ItemMst itemmst = new ItemMst();
		 * 
		 * salesDtl.setItemName(txtItemname.getText());
		 * salesDtl.setBarCode(txtBarcode.getText());
		 * itemmst.setBarCode(txtBarcode.getText()); ResponseEntity<List<ItemMst>>
		 * respsentity = RestCaller.getSalesbarcode(itemmst); // itemmst =
		 * respenstity.getBody();
		 * 
		 * // salesDtl.setRate(Double.parseDouble(txtRate.getText())); Double qty =
		 * Double.parseDouble(txtQty.getText()); salesDtl.setQty(qty);
		 * 
		 * // salesDtl.setUnit(choiceUnit.getStyle());
		 * salesDtl.setItemCode(txtItemcode.getText()); Double eRate = 00.0; eRate =
		 * Double.parseDouble(txtRate.getText()); salesDtl.setRate(eRate); Double TaRate
		 * = 00.0; TaRate = Double.parseDouble(txtTax.getText());
		 * salesDtl.setTaxRate(TaRate);
		 * 
		 * salesDtl.setSalesTransHdr(salesTransHdr);
		 * 
		 * ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		 * salesDtl = respentity.getBody(); Double amount =
		 * Double.parseDouble(txtQty.getText()) * Double.parseDouble(txtRate.getText())
		 * + Double.parseDouble(txtTax.getText());
		 * salesTransHdr.setCashPaidSale(amount); // salesDtl.setTempAmount(amount);
		 * saleListTable.add(salesDtl);
		 * 
		 * FillTable();
		 * 
		 * amountTotal = amountTotal + salesTransHdr.getCashPaidSale();
		 * txtCashtopay.setText(Double.toString(amountTotal)); txtItemname.setText("");
		 * txtBarcode.setText(""); txtQty.setText(""); txtTax.setText("");
		 * txtRate.setText("");
		 * 
		 * txtItemcode.setText("");
		 * 
		 * }
		 * 
		 * if (event.getCode() == KeyCode.BACK_SPACE) {
		 * 
		 * txtBarcode.requestFocus();
		 * 
		 * }
		 * 
		 */}

	@FXML
	void unHold(ActionEvent event) {

	}

	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		/*
		 * Call Rest to get the summary and set that to the display fields
		 */
		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());

//		salesTransHdr.setCashPaidSale(amount);
//			salespos.setTempAmount(amount);
		// saleListTable.add(salespos);
		if (null != summary.getTotalAmount()) {

			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
	}
	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();
	}

	private void addItem() {

		if(saletype.getSelectionModel().isEmpty())
		{
			notifyMessage(5, "Please Select Sale Type");
			saletype.requestFocus();
		}
		 
	
		else if(txtItemname.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please Select Item");
			txtItemname.requestFocus();
		}
		else if(txtQty.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please type Quantity");
			txtQty.requestFocus();
		} else if (txtBatch.getText().trim().isEmpty()) {
			notifyMessage(1, "Item Batch is not present!!!");
			txtQty.requestFocus();
			return;
		}
		else
		{
			
		try {
			
			if (SystemSetting.systemBranch.equalsIgnoreCase(null)) {
				notifyMessage(5, "Incorrect Branch");
				return;
			}
			ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
			DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
			System.out.println("Sys Date before add" + SystemSetting.systemDate);
			if (null != dayEndClosureHdr) {
				if (null != dayEndClosureHdr.getDayEndStatus()) {
					String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
					String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
					java.sql.Date prDate = Date.valueOf(process_date);
					Date sDate = Date.valueOf(sysdate);
					int i = prDate.compareTo(sDate);
					if (i > 0 || i == 0) {
						notifyMessage(1, " Day End Already Done for this Date !!!!");
						return;
					}
				}
			}
			
			Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);
			
			if(!dayendtodone)
			{
				notifyMessage(1, "Day End should be done before changing the date !!!!");
				return;
			}
			ResponseEntity<UnitMst> getUnitBYItem = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
			
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
		
		
			ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),getUnitBYItem.getBody().getId());
			if(!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId()))
			{
			if(null == getmulti.getBody())
			{
				notifyMessage(5,"Please Add the item in Multi Unit");
				return;
			}
			}
			ArrayList items = new ArrayList();
			
			items = RestCaller.getSingleStockItemByName(txtItemname.getText(),txtBatch.getText());
			Double chkQty = 0.0;
			//chkQty = RestCaller.getQtyFromItemBatchDtlDailyByItemIdAndQty(getItem.getBody().getId(), txtBatch.getText());
			
			//----------------Here   stock checking  from ItemBatchMst--------------------------------sharon --------28/06/2021----------------------
			chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(), txtBatch.getText());
			
			
			String itemId= null;
			Iterator itr = items.iterator();
			while (itr.hasNext()) {
				List element = (List) itr.next();
//				chkQty = (Double) element.get(4);
				itemId = (String) element.get(7);
			}
			ResponseEntity<KitDefinitionMst> getKitMst = RestCaller.getKitByItemId(getItem.getBody().getId());
			if(null == getKitMst.getBody())
			{
			if(!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId()))
			{
				Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(), getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),Double.parseDouble(txtQty.getText()));
				System.out.println(conversionQty);
				if(chkQty < conversionQty)
				{
					notifyMessage(1, "Not in Stock!!!");
					txtQty.clear();
					txtItemname.clear();
					txtBarcode.clear();
					txtBatch.clear();
					txtRate.clear();
					txtBarcode.requestFocus();
					return;
				}
			}
			else if(chkQty < Double.parseDouble(txtQty.getText()))
			{
				notifyMessage(5, "Not in Stock!!!");
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
			
			}
		if (null == salesTransHdr) {
			salesTransHdr = new SalesTransHdr();
			salesTransHdr.setIsBranchSales("N");
			salesTransHdr.setInvoiceAmount(0.0);
			salesTransHdr.getId();
			
			salesTransHdr.setUserId(SystemSetting.getUserId());
		//	salesTransHdr.setBranchCode(SystemSetting.systemBranch);
			
			String salesMode =saletype.getSelectionModel().getSelectedItem().toString();
			/*
			 * new url for getting account heads instead of customer mst=========05/0/2022
			 */
//			ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerByNameAndBarchCode(salesMode,SystemSetting.systemBranch);
			ResponseEntity<AccountHeads> accountHeadsResponse = RestCaller.getAccountHeadsByNameAndBranchCode(salesMode,SystemSetting.systemBranch);
			salesTransHdr.setCustomerId(accountHeadsResponse.getBody().getId());
			salesTransHdr.setAccountHeads(accountHeadsResponse.getBody());
			
			salesTransHdr.setCreditOrCash("CREDIT");
			salesTransHdr.setBranchCode(SystemSetting.systemBranch);
			
			
			
			ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
			salesTransHdr = respentity.getBody();

		}
		if (null == salesDtl) {
			salesDtl = new SalesDtl();
		}
				if (null != salesDtl.getId()) {
			System.out.println("toDeleteSale.getId()" + salesDtl.getId());
			
			RestCaller.deleteSalesDtl(salesDtl.getId());
		}	
				if(null == getKitMst.getBody() )
				{
				Double itemsqty = 0.0;
				ResponseEntity<List<SalesDtl>> getSalesDtl =RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),getItem.getBody().getId(),txtBatch.getText());
				saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
				if(saleListItemTable.size()>1)
				{
					Double PrevQty=0.0;
					for(SalesDtl saleDtl: saleListItemTable)
					{
						if(!saleDtl.getUnitId().equalsIgnoreCase(getItem.getBody().getUnitId()))
						{
							 PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(),saleDtl.getUnitId(),getItem.getBody().getUnitId(),saleDtl.getQty());
						}
						else
						{
							PrevQty= saleDtl.getQty();
						}
						itemsqty =itemsqty + PrevQty;
					}
				}
				else
				{
					itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(),itemId,txtBatch.getText());
				}
				if(!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId()))
				{
					Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(), getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),Double.parseDouble(txtQty.getText()));
					System.out.println(conversionQty);
					if(chkQty < itemsqty + conversionQty)
					{
						notifyMessage(1, "Not in Stock!!!");
						txtQty.clear();
						txtItemname.clear();
						txtBarcode.clear();
						txtBatch.clear();
						txtRate.clear();
						txtBarcode.requestFocus();
						return;
					}
				}
		
				else if(chkQty  < itemsqty + Double.parseDouble(txtQty.getText()))
		{
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();
			notifyMessage(5,"No Stock!!");
			return;
		}
				}
				salesDtl = new SalesDtl();
		salesDtl.setSalesTransHdr(salesTransHdr);
		
		salesDtl.setItemName(txtItemname.getText());
		salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());
		
		String batch = txtBatch.getText().length()==0 ? "NOBATCH" : txtBatch.getText();
		System.out.println(batch);
		salesDtl.setBatchCode(batch);
		Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());
		salesDtl.setQty(qty);

		Double mrpRateIncludingTax = 00.0;
		mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
		salesDtl.setMrp(mrpRateIncludingTax);

		Double taxRate = 00.0;
		
		
		ResponseEntity<ItemMst>  respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
		 
		salesDtl.setBarcode(item.getBarCode());
		salesDtl.setStandardPrice(item.getStandardPrice());
		
		salesDtl.setItemCode(item.getItemCode());	
	
		salesDtl.setItemId(item.getId());
		if(null == cmbUnit.getValue())
		{
		ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
		salesDtl.setUnitId(getUnit.getBody().getId());
		salesDtl.setUnitName(getUnit.getBody().getUnitName());
		}
		else
		{
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
					salesDtl.setUnitId(getUnit.getBody().getId());
					salesDtl.setUnitName(getUnit.getBody().getUnitName());
		}
		
		
		
		
		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
		
		if(getTaxMst.getBody().size()>0)
		{
			for(TaxMst taxMst:getTaxMst.getBody())
			{
				if(taxMst.getTaxId().equalsIgnoreCase("IGST"))
				{
					salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//					BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setIgstAmount(igstAmount.doubleValue());
					salesDtl.setTaxRate(taxMst.getTaxRate());
				}
				if(taxMst.getTaxId().equalsIgnoreCase("CGST"))
				{
					salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//					BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setCgstAmount(CgstAmount.doubleValue());
				}
				if(taxMst.getTaxId().equalsIgnoreCase("SGST"))
				{
					salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//					BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setSgstAmount(SgstAmount.doubleValue());
				}
				if(taxMst.getTaxId().equalsIgnoreCase("KFC"))
				{
					salesDtl.setCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setCessAmount(cessAmount.doubleValue());
				}
				if(taxMst.getTaxId().equalsIgnoreCase("AC"))
				{
					salesDtl.setAddCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//					salesDtl.setAddCessAmount(cessAmount.doubleValue());
				}
				
			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + salesDtl.getIgstTaxRate()+salesDtl.getCessRate()+salesDtl.getAddCessRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue()* salesDtl.getQty());
			BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue()* salesDtl.getQty());
			BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(),rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue()* salesDtl.getQty());
			BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue()* salesDtl.getQty());
			BigDecimal adcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(adcessAmount.doubleValue()* salesDtl.getQty());
		}
		else
		{
			if(null != item.getTaxRate())
			{
				taxRate = item.getTaxRate();
				salesDtl.setTaxRate(taxRate);

			} else {
				taxRate = 0.0;
				salesDtl.setTaxRate(taxRate);

			}
		Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
		salesDtl.setRate(rateBeforeTax);
		
		double cessRate = item.getCess();
		double cessAmount = 0.0;
		if (cessRate > 0) {

			rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
			salesDtl.setRate(rateBeforeTax);
			cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;
		}

		salesDtl.setCessRate(cessRate);

		salesDtl.setCessAmount(cessAmount);
		
		

		 
		salesDtl.setSgstTaxRate(taxRate/2);
		
		salesDtl.setCgstTaxRate(taxRate/2);
		
		salesDtl.setCgstAmount(salesDtl.getCgstTaxRate()*salesDtl.getQty()*salesDtl.getRate()/100);
		
		salesDtl.setSgstAmount(salesDtl.getSgstTaxRate()*salesDtl.getQty()*salesDtl.getRate()/100);
		}
		salesDtl.setAmount(Double.parseDouble(txtQty.getText())* Double.parseDouble(txtRate.getText()));
		
		
		
		
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if(null != priceDefenitionMst)
		{
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(),sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setCostPrice(priceDefinition.getAmount());
			}
		}
		

		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		
		
		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());
		
		List<SalesDtl>salesDtlList = respentityList.getBody();

		/*
		 * Call Rest to get the summary and set that to the display fields
		 */

		// salesDtl.setTempAmount(amount);
		saleListTable.clear();
		saleListTable.setAll(salesDtlList);
		
		
		
		

		//ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		//salesDtl = respentity.getBody();

	
		// salesDtl.setTempAmount(amount);
		//saleListTable.add(salesDtl);

		FillTable();

		
		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");
	
		txtBatch.setText("");
		txtBarcode.requestFocus();
		salesDtl = new SalesDtl();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	}


	private void showPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
		ItemMst item = new ItemMst();
		item = getItem.getBody();

//		item.setRank(item.getRank()+1);
//		RestCaller.updateRankItemMst(item);
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			
			itemNameProperty.set(itemPopupEvent.getItemName());
			//salesDtl.setItemName(itemNameProperty.get());
			txtBarcode.setText(itemPopupEvent.getBarCode());
			batchProperty.set(itemPopupEvent.getBatch());
			cmbUnit.getItems().add(itemPopupEvent.getUnitName());
			cmbUnit.setValue(itemPopupEvent.getUnitName());
			txtRate.setText(Double.toString(itemPopupEvent.getMrp()));
			ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller.getMultiUnitByItemId(itemPopupEvent.getItemId());

			multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
			if(!multiUnitList.isEmpty())
			{
				
			for(MultiUnitMst multiUniMst:multiUnitList)
			{
				
			
				ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
				cmbUnit.getItems().add(getUnit.getBody().getUnitName());
			}
			System.out.println(itemPopupEvent.toString());
			}
			
			System.out.println(itemPopupEvent.toString());
			ResponseEntity<TaxMst> taxMst = RestCaller.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(),"IGST");
			if(null != taxMst.getBody())
			{
			}
				}
			});
		}
	}
	
	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			custname.setText(customerEvent.getCustomerName());
			custAdress.setText(customerEvent.getCustomerAddress());
			gstNo.setText(customerEvent.getCustomerGst());
			custId = customerEvent.getCustId();
			System.out.println("custIdcustIdcustId"+custId);
				}
			});	
		}
	
		}
	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

				Image img = new Image("done.png");
				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();
				notificationBuilder.show();
			}
	  @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }
}
