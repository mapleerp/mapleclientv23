package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.OtherBranchSalesTransHdr;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.StockTransferOutHdr;
import com.maple.mapleclient.events.OtherBranchSalesEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class OtherBranchSalesPopupCtl {
	

	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	OtherBranchSalesEvent 	otherBranchSalesEvent;
	
	StringProperty SearchString = new SimpleStringProperty();
    private ObservableList<OtherBranchSalesTransHdr> voucherNoList = FXCollections.observableArrayList();


    @FXML
    private TextField txtname;

    @FXML
    private TableView<OtherBranchSalesTransHdr> tblItemPopupView;
    
    @FXML
    private TableColumn<OtherBranchSalesTransHdr, String> clVoucherNumber;

    @FXML
    private TableColumn<OtherBranchSalesTransHdr, String> clVoucherDate;

    @FXML
    private TableColumn<OtherBranchSalesTransHdr, String> clFromBranch;

    @FXML
    private Button btnSubmit;

    @FXML
    private Button btnCancel;
    
    @FXML
    private void initialize()
    {
    	eventBus.register(this);
    	
    	txtname.textProperty().bindBidirectional(SearchString);
    	otherBranchSalesEvent = new OtherBranchSalesEvent();
    	LoadVoucherNoPopupBySearch("");
    	
    	
    	tblItemPopupView.setItems(voucherNoList);
    	clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
    	clFromBranch.setCellValueFactory(cellData -> cellData.getValue().getFromBranchProperty());
    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
    	
    	tblItemPopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	    	   if (newSelection != null) {
			    	if(null!=newSelection.getId()&& newSelection.getVoucherNumber().length()>0) {
			    		
			    		otherBranchSalesEvent = new OtherBranchSalesEvent();
			    		otherBranchSalesEvent.setVoucherDate(newSelection.getVoucherDate());
			    		otherBranchSalesEvent.setFromBranchCode(newSelection.getSourceBranchCode());
			    		otherBranchSalesEvent.setVoucherNumber(newSelection.getVoucherNumber());
			    		otherBranchSalesEvent.setOtherBranchSalesHdrId(newSelection.getId());
			    		
			    		eventBus.post(otherBranchSalesEvent);
			    		
			    	}
			    }
			});
	    	 SearchString.addListener(new ChangeListener(){
					@Override
					public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					 					
						LoadVoucherNoPopupBySearch((String)newValue);
					}
		        });

    	
    }

   

	@FXML
    void OnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			eventBus.post(otherBranchSalesEvent);
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtname.requestFocus();
		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblItemPopupView.requestFocus();
			tblItemPopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnCancel.getScene().getWindow();
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
    	eventBus.post(otherBranchSalesEvent);
		stage.close();
    }
    
    protected void LoadVoucherNoPopupBySearch(String voucherno) {

    	
    	
    	
    	ArrayList stockin = new ArrayList();
    	voucherNoList.clear();
    	
    	stockin = RestCaller.SearchOtherBranchSaleVoucherNo(voucherno);
    	Iterator itr = stockin.iterator();
    	while (itr.hasNext()) {
    		 LinkedHashMap element = (LinkedHashMap) itr.next();
    		// Object intentNumber = (String) element.get("intentNumber");
    		 Object id = (String) element.get("id");
    		 Object voucherNumber = (String) element.get("voucherNumber");
    		 Object fromBranch = (String)element.get("sourceBranchCode");
    		 Object voucherDate=(String)element.get("voucherDate");
    		 if (null != id) {
    			 OtherBranchSalesTransHdr stk = new OtherBranchSalesTransHdr();
    			 stk.setVoucherNumber((String)voucherNumber);
    			 stk.setId((String) id);
    			 stk.setSourceBranchCode((String)fromBranch);
    			 stk.setVoucherDate(SystemSetting.StringToUtilDate((String) voucherDate, "yyyy-MM-dd"));
    			 voucherNoList.add(stk);
    		 }
    	}
    	return;
    
	}

}
