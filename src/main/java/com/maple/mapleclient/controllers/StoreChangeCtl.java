package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.StockVerificationDtl;
import com.maple.mapleclient.entity.StoreChangeDtl;
import com.maple.mapleclient.entity.StoreChangeMst;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;

public class StoreChangeCtl {

	private ObservableList<StoreChangeDtl> storeChangeDtlList = FXCollections.observableArrayList();
	private ObservableList<StoreMst> storeMstList = FXCollections.observableArrayList();

	private EventBus eventBus = EventBusFactory.getEventBus();

	boolean multi = false;

	StoreChangeMst storeChangeMst = null;
	StoreChangeDtl storeChangeDtl = null;
	
	String taskid;
	String processInstanceId;
	
	

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtUnit;

	@FXML
	private TextField txtBatchCode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtRate;

	@FXML
	private DatePicker dpExpiryDate;

	@FXML
	private Button btnstockTransferAdd;

	@FXML
	private Button btnDelete;

	@FXML
	private TextField txtMRP;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TableView<StoreChangeDtl> tblStockTrasfer;

	@FXML
	private TableColumn<StoreChangeDtl, String> clStktransferItemName;

	@FXML
	private TableColumn<StoreChangeDtl, String> clStktransferBarcode;

	@FXML
	private TableColumn<StoreChangeDtl, String> clStktransferBatch;

	@FXML
	private TableColumn<StoreChangeDtl, Number> clStkTransferRate;

	@FXML
	private TableColumn<StoreChangeDtl, Number> clStkTransferQty;

	@FXML
	private TableColumn<StoreChangeDtl, Number> clStkMRP;

	@FXML
	private TableColumn<StoreChangeDtl, LocalDate> clStkTransferExpiryDate;

	@FXML
	private TableColumn<StoreChangeDtl, Number> clStkTransferAmount;

	@FXML
	private TextField txtTotalAmt;

	@FXML
	private Button btnSubmit;

	@FXML
	private DatePicker dpStkTrasferDate;

	@FXML
	private ComboBox<String> cmbStore;
	

    @FXML
    private ComboBox<String> cmbToStore;

	@FXML
	private void initialize() {
		dpExpiryDate = SystemSetting.datePickerFormat(dpExpiryDate, "dd/MMM/yyyy");
		dpStkTrasferDate = SystemSetting.datePickerFormat(dpStkTrasferDate, "dd/MMM/yyyy");
		eventBus.register(this);
		
		storeMstList.clear();
		ResponseEntity<List<StoreMst>> storeMstSaved = RestCaller.getAllStoreMst();
		storeMstList = FXCollections.observableArrayList(storeMstSaved.getBody());
		
		for(StoreMst store : storeMstList)
		{
			cmbStore.getItems().add(store.getShortCode());
			cmbToStore.getItems().add(store.getShortCode());

		}
		
		
		tblStockTrasfer.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					storeChangeDtl = new StoreChangeDtl();
					storeChangeDtl.setId(newSelection.getId());
				}
			}
		});
	}

	@FXML
	void add(ActionEvent event) {
		addItem();
	}

	@FXML
	void addOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void barcodekeyPress(KeyEvent event) {

	}

	@FXML
	void batchCodeOnEnter(KeyEvent event) {

	}

	@FXML
	void StoreOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtItemName.requestFocus();
		}

	}

	@FXML
	void btnDelete(ActionEvent event) {
		
		if(null != storeChangeDtl)
		{
			if(null != storeChangeDtl.getId())
			{
				RestCaller.deleteStoreChangeDtl(storeChangeDtl.getId());
				ResponseEntity<List<StoreChangeDtl>> savedStoreDtl = RestCaller.findStoreChangeByHdrId(storeChangeMst.getId());
				storeChangeDtlList = FXCollections.observableArrayList(savedStoreDtl.getBody());
				FillTable();
			}
		}

	}

	@FXML
	void expirydateOnEnter(KeyEvent event) {

	}

	@FXML
	void itempopup(MouseEvent event) {

	}

	@FXML
	void mrpOnEnter(KeyEvent event) {

	}
	

    @FXML
    void ToStoreOnEnter(KeyEvent event) {

    }

	@FXML
	void onEnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showStockitemPopup();

		}
	}

	@FXML
	void qtyOnEnter(KeyEvent event) {

	}

	@FXML
	void rateOnEnter(KeyEvent event) {

	}

	@FXML
	void stkTransferDate(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			cmbStore.requestFocus();
		}

	}

	@FXML
	void submit(ActionEvent event) {
		
		if(null == storeChangeMst)
		{
			return;
		}
		
		ResponseEntity<List<StoreChangeDtl>> savedStoreDtl = RestCaller.findStoreChangeByHdrId(storeChangeMst.getId());
		storeChangeDtlList = FXCollections.observableArrayList(savedStoreDtl.getBody());

		if(storeChangeDtlList.size() == 0)
		{
			return;
		}
	
		String vno = RestCaller.getVoucherNumber(SystemSetting.systemBranch+"SC");
		storeChangeMst.setVoucherNumber(vno);
		storeChangeMst.setVoucherDate(SystemSetting.localToUtilDate(dpStkTrasferDate.getValue()));
		storeChangeMst.setStore(cmbStore.getSelectionModel().getSelectedItem().toString());
		storeChangeMst.setToStore(cmbToStore.getSelectionModel().getSelectedItem().toString());

		RestCaller.updateStoreChangeMst(storeChangeMst);
		storeChangeMst = null;
		storeChangeDtl = null;
		storeChangeDtlList.clear();
		dpStkTrasferDate.setValue(null);
		cmbStore.getSelectionModel().clearSelection();
		cmbToStore.getSelectionModel().clearSelection();
		tblStockTrasfer.getItems().clear();
	}

	@FXML
	void tableToTextBox(MouseEvent event) {

	}

	private void showStockitemPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtQty.requestFocus();
	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnstockTransferAdd.getScene().getWindow();
		if (stage.isShowing()) {
			if (!multi) {
				txtItemName.setText(itemPopupEvent.getItemName());
//			stockTransferOutDtl.setItemId(itemPopupEvent.getItemId());
//			stockTransferOutDtl.setUnitId(itemPopupEvent.getUnitId());
				txtBarcode.setText(itemPopupEvent.getBarCode());
				txtUnit.setText(itemPopupEvent.getUnitName());
				txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));
				txtBatchCode.setText(itemPopupEvent.getBatch());
				txtRate.setText(txtMRP.getText());
				java.util.Date udate = SystemSetting.getApplicationDate();
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
			
				ResponseEntity<PriceDefenitionMst> getpriceDefMst = RestCaller
						.getPriceDefenitionMstByName("STOCK TRANSFER");
				if (null != getpriceDefMst.getBody()) {
					/*
					 * Check for Batch price definition
					 */
					ResponseEntity<BatchPriceDefinition> batchPrice = RestCaller.getBatchPriceDefinition(itemPopupEvent.getItemId(),
							getpriceDefMst.getBody().getId(), itemPopupEvent.getUnitId(),itemPopupEvent.getBatch(), sdate);
					if(null != batchPrice.getBody())
					{
						txtRate.setText(batchPrice.getBody().getAmount()+"");
					}
					else
					{
					ResponseEntity<PriceDefinition> getPrice = RestCaller.getPriceDefenitionByItemIdAndUnit(
							itemPopupEvent.getItemId(), getpriceDefMst.getBody().getId(),
							itemPopupEvent.getUnitId(),sdate);
					if (null != getPrice.getBody()) {
						txtRate.setText(Double.toString(getPrice.getBody().getAmount()));
					}
					}
				}

				txtBatchCode.setText(itemPopupEvent.getBatch());
				if (!(txtBatchCode.getText().trim().isEmpty())) {
					List<ItemBatchMst> itemBatchMstList = RestCaller.getBatchExpDate(txtBatchCode.getText());

					ItemBatchMst itemBatchMst = null;
					if (itemBatchMstList.size() > 0) {
						itemBatchMst = itemBatchMstList.get(0);
					}

					try {
						if (null != itemBatchMst) {
							Date expdaten = (Date) itemBatchMst.getExpDate();

							dpExpiryDate.setValue(expdaten.toLocalDate());
						}
					} catch (Exception e) {
						System.out.print(e.toString());
					}
					multi = false;
				}

				txtQty.requestFocus();
			} else if (multi) {
				txtItemName.setText(itemPopupEvent.getItemName());
//				stockTransferOutDtl.setItemId(itemPopupEvent.getItemId());
//				stockTransferOutDtl.setUnitId(itemPopupEvent.getUnitId());
				txtBarcode.setText(itemPopupEvent.getBarCode());
				txtUnit.setText(itemPopupEvent.getUnitName());
				txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));
				java.util.Date udate = SystemSetting.getApplicationDate();
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				ResponseEntity<PriceDefenitionMst> getpriceDefMst = RestCaller
						.getPriceDefenitionMstByName("STOCK TRANSFER");
				if (null != getpriceDefMst.getBody()) {
					/*
					 * Check for Batch price definition
					 */
					ResponseEntity<BatchPriceDefinition> batchPrice = RestCaller.getBatchPriceDefinition(itemPopupEvent.getItemId(),
							getpriceDefMst.getBody().getId(), itemPopupEvent.getUnitId(),itemPopupEvent.getBatch(), sdate);
					if(null != batchPrice.getBody())
					{
						txtRate.setText(batchPrice.getBody().getAmount()+"");
					}
					else
					{
					ResponseEntity<PriceDefinition> getPrice = RestCaller.getPriceDefenitionByItemIdAndUnit(
							itemPopupEvent.getItemId(), getpriceDefMst.getBody().getId(), 
							itemPopupEvent.getUnitId(),sdate);
					if (null != getPrice.getBody()) {
						txtRate.setText(Double.toString(getPrice.getBody().getAmount()));
					}
					}
				} else {
					txtRate.setText(txtMRP.getText());
				}

				txtBatchCode.setText(itemPopupEvent.getBatch());

				txtQty.setText("1");
				addItem();
				multi = false;
			}
		}

	}

	private void addItem() {

		if (null == dpStkTrasferDate.getValue()) {
			notifyMessage(5, "Please Select trnsfer date", false);
			dpStkTrasferDate.requestFocus();
			return;
		}

		if (null == cmbStore.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please Select From Store", false);
			cmbStore.requestFocus();
			return;
		}
		if (null == cmbToStore.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please Select To Store", false);
			cmbToStore.requestFocus();
			return;
		}
		
		if(cmbStore.getSelectionModel().getSelectedItem().equalsIgnoreCase(cmbToStore.getSelectionModel().getSelectedItem()))
		{
			notifyMessage(5, "Stock Change "+cmbStore.getSelectionModel().getSelectedItem()+" to "+
					cmbStore.getSelectionModel().getSelectedItem()+"not possible", false);
			cmbToStore.requestFocus();
			return;
		}

		if(txtItemName.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please Select Item name", false);
			txtItemName.requestFocus();
			return;
		}
		
		if(txtQty.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please Enter Qty", false);
			txtQty.requestFocus();
			return;
		}
		
	
		if (null == storeChangeMst) {
			storeChangeMst = new StoreChangeMst();
			storeChangeMst.setFromBranch(SystemSetting.systemBranch);
			storeChangeMst.setStore(cmbStore.getSelectionModel().getSelectedItem().toString());
			storeChangeMst.setToStore(cmbToStore.getSelectionModel().getSelectedItem().toString());

			storeChangeMst.setTransDate(SystemSetting.localToUtilDate(dpStkTrasferDate.getValue()));

			ResponseEntity<StoreChangeMst> saved = RestCaller.SaveStoreChange(storeChangeMst);
			storeChangeMst = saved.getBody();
		}
		
		if(null == storeChangeMst)
		{
			return;
		}

		storeChangeDtl = new StoreChangeDtl();
		storeChangeDtl.setStoreChangeMst(storeChangeMst);
		storeChangeDtl.setBarcode(txtBarcode.getText());
		storeChangeDtl.setBatch(txtBatchCode.getText());
		
		ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		ItemMst itemMst = itemResp.getBody();
		if(null !=itemMst.getItemCode()) {
		storeChangeDtl.setItemCode(itemMst.getItemCode());
		}
		storeChangeDtl.setQty(Double.parseDouble(txtQty.getText()));
		storeChangeDtl.setItemId(itemMst.getId());
		storeChangeDtl.setRate(Double.parseDouble(txtRate.getText()));
		storeChangeDtl.setMrp(Double.parseDouble(txtMRP.getText()));
		storeChangeDtl.setItemName(itemMst.getItemName());
		Double amount = Double.parseDouble(txtRate.getText()) * Double.parseDouble(txtQty.getText());
		storeChangeDtl.setAmount(amount);
		ResponseEntity<UnitMst> unitResp = RestCaller.getUnitByName(txtUnit.getText());
		UnitMst unitMst = unitResp.getBody();
		storeChangeDtl.setUnitId(unitMst.getId());
		ResponseEntity<StoreChangeDtl> storeChangeDtlSaved = RestCaller.SaveStoreChangeDtl(storeChangeDtl);
		storeChangeDtl = storeChangeDtlSaved.getBody();
		if(null != storeChangeDtl)
		{
			notifyMessage(5, "Successfully added", true);
			storeChangeDtlList.add(storeChangeDtl);
			FillTable();
			txtBarcode.clear();
			txtBatchCode.clear();
			txtItemName.clear();
			txtMRP.clear();
			txtQty.clear();
			txtRate.clear();
			txtTotalAmt.clear();
			txtUnit.clear();
			dpExpiryDate.setValue(null);
		}
	}

	private void FillTable() {
		
		for(StoreChangeDtl dtl :  storeChangeDtlList)
		{
			ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(dtl.getItemId());
			ItemMst itemMst = itemResp.getBody();
			dtl.setItemName(itemMst.getItemName());
			
			ResponseEntity<UnitMst> unitResp = RestCaller.getunitMst(dtl.getUnitId());
			UnitMst unitMst = unitResp.getBody();
			dtl.setUnitName(unitMst.getUnitName());
		}
		
		tblStockTrasfer.setItems(storeChangeDtlList);
		

		clStkMRP.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clStkTransferAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clStktransferBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarCodeProperty());
		clStktransferBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clStktransferItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clStkTransferQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clStkTransferRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

		
		
	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}


}
