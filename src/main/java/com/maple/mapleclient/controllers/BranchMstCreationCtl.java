package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CompanyMst;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class BranchMstCreationCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<BranchMst> branchCreationList = FXCollections.observableArrayList();
	private ObservableList<BranchMst> branchCreationList1 = FXCollections.observableArrayList();
	CompanyMst companyMst = null;
	BranchMst branchcreation = null;

	@FXML
	private Button btnBranchAsSupplier;

	@FXML
	private Button btnBranchAsCustomer;

	@FXML
	private TextField branchname;

	@FXML
	private ComboBox<String> cmbState;
	@FXML
	private TextField branchcode;
	@FXML
	private Button showall;
	@FXML
	private TextField branchgst;

	@FXML
	private TextField bankname;

	@FXML
	private TextField bankbranch;

	@FXML
	private TextField ifsccode;

	@FXML
	private TextField bankaccno;

	@FXML
	private Button btnInitalize;
	@FXML
	private CheckBox chMyBranch;
	@FXML
	private Button btnsave;

	@FXML
	private Button btnsubmit;

	@FXML
	private Button btndelete;

	@FXML
	private TextField branchEmail;

	@FXML
	private TextField branchWbsite;

	@FXML
	private TableView<BranchMst> tblbranch;

	@FXML
	private TextField branchPhoneNo;

	@FXML
	private TextField branchAddress1;

	@FXML
	private TextField branchAddress2;

	@FXML
	private TableColumn<BranchMst, String> bm1;

	@FXML
	private TableColumn<BranchMst, String> bm2;

	@FXML
	private TableColumn<BranchMst, String> bm3;

	@FXML
	private TableColumn<BranchMst, String> bm4;

	@FXML
	private void initialize() {

		ResponseEntity<List<CompanyMst>> companysaved = RestCaller.returnValueOfCompanymst();
		companyMst = companysaved.getBody().get(0);

		tblbranch.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getBranchName() && newSelection.getBranchName().length() > 0) {
// 			    		
					if (null != newSelection.getId()) {

						branchcreation = new BranchMst();
						branchcreation.setId(newSelection.getId());
						branchAddress1.setText(newSelection.getBranchAddress1());
						branchAddress2.setText(newSelection.getBranchAddress2());
						bankaccno.setText(newSelection.getAccountNumber());
						bankbranch.setText(newSelection.getBankBranch());
						branchcode.setText(newSelection.getBranchCode());
						branchEmail.setText(newSelection.getBranchEmail());
						branchgst.setText(newSelection.getBranchGst());
						branchname.setText(newSelection.getBranchName());
						branchPhoneNo.setText(newSelection.getBranchTelNo());
						branchWbsite.setText(newSelection.getBranchWebsite());
						cmbState.getSelectionModel().select(newSelection.getBranchState());
						if(newSelection.getMyBranch().equalsIgnoreCase("Y"))
						{
							chMyBranch.setSelected(true);							
						}
						else
						{
							chMyBranch.setSelected(false);
						}

					}

				}
			}
		});
//		cmbState.getItems().add("KERALA");
//		cmbState.getItems().add("ANDHRA PRADESH");
//		cmbState.getItems().add("ARUNACHAL PRADESH");
//		cmbState.getItems().add("ASSAM");
//		cmbState.getItems().add("BIHAR");
//		cmbState.getItems().add("CHHATTISGARH");
//		cmbState.getItems().add("GOA");
//		cmbState.getItems().add("GUJARAT");
//		cmbState.getItems().add("HARYANA");
//		cmbState.getItems().add("HIMACHAL PRADESH");
//		cmbState.getItems().add("JAMMU AND  KASHMIR");
//		cmbState.getItems().add("JHARKHAND");
//		cmbState.getItems().add("KARNATAKA");
//		cmbState.getItems().add("MADHYA PRADESH");
//		cmbState.getItems().add("MAHARASHTRA");
//		cmbState.getItems().add("MANIPUR");
//		cmbState.getItems().add("MEGHALAYA");
//		cmbState.getItems().add("MIZORAM");
//		cmbState.getItems().add("NAGALAND");
//		cmbState.getItems().add("ODISHA");
//		cmbState.getItems().add("PUNJAB");
//		cmbState.getItems().add("RAJASTHAN");
//		cmbState.getItems().add("TAMIL NADU");
//		cmbState.getItems().add("TELANGANA");
//		cmbState.getItems().add("TRIPURA");
//		cmbState.getItems().add("UTTAR PRADESH");
//		cmbState.getItems().add("UTTARAKHAND");
//		cmbState.getItems().add("WEST BENGAL");
		
		ResponseEntity<List<String>> stateList = RestCaller.getStateListByCountry(companyMst.getCountry());
    	for(int i = 0;i<stateList.getBody().size();i++)
    	{
    		cmbState.getItems().add(stateList.getBody().get(i));
    	}
		btnInitalize.setVisible(false);

	}

	@FXML
	void InitailizeData(ActionEvent event) {

		String msg = RestCaller.initailizeData(SystemSetting.getSystemBranch());
		notifyMessage(5, msg);
	}

	@FXML
	void showall(ActionEvent event) {

		showAllBranches();
	}

	private void showAllBranches() {
		branchCreationList.clear();
		ResponseEntity<List<BranchMst>> respentitygroup = RestCaller.returnValueOfBranchmst();

		branchCreationList1 = FXCollections.observableArrayList(respentitygroup.getBody());
		if (null != branchCreationList1) {
			for (BranchMst branchMst : branchCreationList1) {
				branchCreationList.add(branchMst);
			}
			tblbranch.setItems(branchCreationList);
			filltable();
		}

	}

	@FXML
	void branchNameOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchcode.requestFocus();
		}
	}

	@FXML
	void branchCodeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchcode.setText(branchcode.getText().toUpperCase());
			branchgst.requestFocus();
		}
	}

	@FXML
	void branchGstOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbState.requestFocus();
		}
	}

	@FXML
	void branchStateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchEmail.requestFocus();
		}
	}

	@FXML
	void branchEmailOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchPhoneNo.requestFocus();
		}

	}

	@FXML
	void branchPhoneOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchWbsite.requestFocus();
		}
	}

	@FXML
	void branchWebsiteOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchAddress1.requestFocus();
		}
	}

	@FXML
	void branchAddress1OnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchAddress2.requestFocus();
		}
	}

	@FXML
	void branchAddress2OnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			bankaccno.requestFocus();
		}
	}

	@FXML
	void branchAccountNoOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			bankname.requestFocus();
		}
	}

	@FXML
	void branchBanckNameOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			bankbranch.requestFocus();
		}
	}

	@FXML
	void branchBankBranchOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			ifsccode.requestFocus();
		}
	}

	@FXML
	void branchIfscOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			chMyBranch.requestFocus();
		}
	}

	@FXML
	void FocusOnSaveBtn(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnsave.requestFocus();
		}
	}

	@FXML
	void delete(ActionEvent event) {

		deleteBranch();

	}

	private void deleteBranch() {
		if (null != branchcreation) {
			if (null != branchcreation.getId()) {

				RestCaller.deleteBranchMst(branchcreation.getId());
				notifyMessage(5, "Deleted!!!");
				ResponseEntity<List<BranchMst>> respentitygroup = RestCaller.returnValueOfBranchmst();
				branchCreationList = FXCollections.observableArrayList(respentitygroup.getBody());
				if (null != branchCreationList) {
					tblbranch.setItems(branchCreationList);
				}
				tblbranch.getItems().clear();
				// filltable();
			}

		}
	}

	@FXML
	void finalsubmit(ActionEvent event) {

	}

	@FXML
	void SaveOnEnterKey(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			saveBranch();
		} else if (event.getCode() == KeyCode.RIGHT) {
			btndelete.requestFocus();
		}
	}

	@FXML
	void DeleteOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			deleteBranch();
		} else if (event.getCode() == KeyCode.RIGHT) {
			showall.requestFocus();
		} else if (event.getCode() == KeyCode.LEFT) {
			btnsave.requestFocus();
		}
	}

	@FXML
	void OnBranchAsCustomerEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			branchAsCustomer();
		} else if (event.getCode() == KeyCode.UP) {
			chMyBranch.requestFocus();
		} else if (event.getCode() == KeyCode.DOWN) {
			btnBranchAsSupplier.requestFocus();
		}
	}
	

    @FXML
    void BranchAsSupplier(ActionEvent event) {

    	branchAsSupplier();
    }
    

    @FXML
    void OnPressBranchAsSupplier(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    		branchAsSupplier();
		} else if (event.getCode() == KeyCode.UP) {
			btnBranchAsCustomer.requestFocus();
		} else if (event.getCode() == KeyCode.LEFT) {
			showall.requestFocus();
		}

    }


	private void branchAsSupplier() {
		List<AccountHeads> supplier = new ArrayList<AccountHeads>();
		List<BranchMst> otherBranch = new ArrayList<BranchMst>();
		ResponseEntity<List<BranchMst>> otherBrancgesRespentity = RestCaller.getOtherBranchesDtl();
		otherBranch = otherBrancgesRespentity.getBody();
		for (BranchMst brch : otherBranch) {
//			Supplier sup = new Supplier();
//			sup.setSupplierName(brch.getBranchName());
//			sup.setBranchCode(SystemSetting.systemBranch);
//			sup.setSupGST(brch.getBranchGst());
//			sup.setState(brch.getBranchState());
//			sup.setId(brch.getId());
//			sup.setAddress(brch.getBranchAddress1());
//			sup.setEmailid(brch.getBranchEmail());
//			sup.setPhoneNo(brch.getBranchTelNo());
//			sup.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
			
			

//			ResponseEntity<Supplier> respentity = RestCaller.saveSupplier(sup);
//			supplier.add(respentity.getBody());
			AccountHeads accountHeads = new AccountHeads();
	 		accountHeads.setAccountName(brch.getBranchName());
	 		accountHeads.setGroupOnly("N");
	 		accountHeads.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
	 		accountHeads.setCustomerContact(brch.getBranchTelNo());
	 		accountHeads.setPartyMail(brch.getBranchEmail());
	 		accountHeads.setPartyAddress1(brch.getBranchAddress1());
	 		accountHeads.setCustomerState(brch.getBranchState());
	 		accountHeads.setPartyGst(brch.getBranchGst());
	 		accountHeads.setBranchName(SystemSetting.systemBranch);
	 		//String vNo1 = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"AH");
	 		
	 		
	    	accountHeads.setId(brch.getId());
	    	ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadByName(accountHeads.getAccountName());
			if(null  != getAccountHeads.getBody())
			{
				notifyMessage(5,"Supplier Already Registered");
				return;
			}
			ResponseEntity<AccountHeads> reponse  = RestCaller.saveAccountHeads(accountHeads);	
		}
		try {
			if (!supplier.isEmpty() || supplier.size() > 0)
				notifyMessage(5, "Successfully created!!!");
		} catch (Exception e) {
			return;
		}
	}
	
	@FXML
	void save(ActionEvent event) {
		saveBranch();
	}

	@FXML
	void OnEnterShowAll(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showAllBranches();
		} else if (event.getCode() == KeyCode.RIGHT) {
			btnBranchAsCustomer.requestFocus();
		} else if (event.getCode() == KeyCode.LEFT) {
			showall.requestFocus();
		}
	}

	
	private void saveBranch() {

		if (branchname.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter branch name");
			return;
		} else if (branchcode.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter branch code");
			return;
		} else if (branchPhoneNo.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter branch phone number");
			return;
		} else if (branchgst.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter branch gst");
			return;
		} else if (null == cmbState.getValue()) {
			notifyMessage(5, "Please select state");
			return;
		} else if (branchEmail.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter branch email_id");
			return;
		} else if (branchWbsite.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter branch website");
			return;
		} else if (branchAddress1.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter branch address");
			return;
		} else {
			if(null != branchcreation)
			{
				if(null != branchcreation.getId())
				{
					ResponseEntity<BranchMst> branchMstRep = RestCaller.getBranchMstById(branchcreation.getId());
					branchcreation = branchMstRep.getBody();
					branchcreation.setBranchName(branchname.getText());
					branchcreation.setBranchCode(branchcode.getText());
					branchcreation.setBranchEmail(branchEmail.getText());
					branchcreation.setBranchWebsite(branchWbsite.getText());
					branchcreation.setBranchTelNo(branchPhoneNo.getText());
					branchcreation.setBranchGst(branchgst.getText());
					branchcreation.setBranchAddress1(branchAddress1.getText());
					branchcreation.setAccountNumber(bankaccno.getText());
					branchcreation.setBankName(bankname.getText());
					branchcreation.setBankBranch(bankbranch.getText());
					branchcreation.setIfsc(ifsccode.getText());

					if (!branchAddress2.getText().trim().isEmpty()) {
						branchcreation.setBranchAddress2(branchAddress2.getText());
					}
					branchcreation.setBranchState(cmbState.getSelectionModel().getSelectedItem());
					if (chMyBranch.isSelected())
						branchcreation.setMyBranch("Y");
					else
						branchcreation.setMyBranch("N");
					RestCaller.updateBranchMst(branchcreation);
					ResponseEntity<List<BranchMst>> respentitygroup = RestCaller.returnValueOfBranchmst();
					branchCreationList = FXCollections.observableArrayList(respentitygroup.getBody());
					if (null != branchCreationList) {
						tblbranch.setItems(branchCreationList);
					}
					branchname.setText("");
					branchcode.setText("");
					branchgst.setText("");
					branchEmail.clear();
					branchWbsite.clear();
					branchPhoneNo.clear();
					bankname.setText("");
					bankbranch.setText("");
					ifsccode.setText("");
					bankaccno.setText("");
					branchAddress2.setText("");
					branchAddress1.clear();
					chMyBranch.setSelected(false);
					cmbState.getSelectionModel().clearSelection();
					//tblbranch.getItems().clear();
				}
			}
			
			else
			{

			branchcreation = new BranchMst();
			String vNo = RestCaller.getVoucherNumberBranch(companyMst.getId(), SystemSetting.getSystemBranch() + "BR");

			branchcreation.setId(vNo + companyMst.getId());
			branchcreation.setBranchName(branchname.getText());
			branchcreation.setBranchCode(branchcode.getText());
			branchcreation.setBranchEmail(branchEmail.getText());
			branchcreation.setBranchWebsite(branchWbsite.getText());
			branchcreation.setBranchTelNo(branchPhoneNo.getText());
			branchcreation.setBranchGst(branchgst.getText());
			branchcreation.setBranchAddress1(branchAddress1.getText());
			branchcreation.setAccountNumber(bankaccno.getText());
			branchcreation.setBankName(bankname.getText());
			branchcreation.setBankBranch(bankbranch.getText());
			branchcreation.setIfsc(ifsccode.getText());

			if (!branchAddress2.getText().trim().isEmpty()) {
				branchcreation.setBranchAddress2(branchAddress2.getText());
			}
			branchcreation.setBranchState(cmbState.getSelectionModel().getSelectedItem());
			if (chMyBranch.isSelected())
				branchcreation.setMyBranch("Y");
			else
				branchcreation.setMyBranch("N");
			ResponseEntity<BranchMst> respentity = RestCaller.SavebranchCreation(branchcreation);

			
			branchcreation = respentity.getBody();
			branchCreationList.add(branchcreation);
			tblbranch.setItems(branchCreationList);
			filltable();
			branchname.setText("");
			branchcode.setText("");
			branchgst.setText("");
			branchEmail.clear();
			branchWbsite.clear();
			branchPhoneNo.clear();
			bankname.setText("");
			bankbranch.setText("");
			ifsccode.setText("");
			bankaccno.setText("");
			branchAddress2.setText("");
			branchAddress1.clear();
			chMyBranch.setSelected(false);
			cmbState.getSelectionModel().clearSelection();
		}
			//version4.3 
		}
	    //version4.3 end
		branchcreation = null;
	}

	@FXML
	void BranchAsCustomer(ActionEvent event) {
		branchAsCustomer();

	}
	
	

	private void branchAsCustomer() {

		AccountHeads accountHeads = new AccountHeads();
		List<BranchMst> otherBranch = new ArrayList<BranchMst>();
		ResponseEntity<List<BranchMst>> otherBrancgesRespentity = RestCaller.getOtherBranchesDtl();
		otherBranch = otherBrancgesRespentity.getBody();
		for (BranchMst brch : otherBranch) {
			AccountHeads account = new AccountHeads();
			account.setAccountName(brch.getBranchName());
//			cust.setId(brch.getBranchCode());
			account.setPartyGst(brch.getBranchGst());
			account.setCustomerState(brch.getBranchState());
			account.setId(brch.getId());
			account.setCustomerContact(brch.getBranchTelNo());
			account.setPartyAddress1(brch.getBranchAddress1());
			account.setPartyMail(brch.getBranchEmail());

			account.setCustomerRank(0);
			
			ResponseEntity<AccountHeads> respentity = RestCaller.partyRegistration(account);
			accountHeads = respentity.getBody();

		}
		try {
			if (null != accountHeads.getId())
				notifyMessage(5, "Successfully created!!!");
		} catch (Exception e) {
			return;
		}

	}

	public void filltable() {

		// grouptbl.setItems(groupCreationList);
		bm1.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
		bm2.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
		bm3.setCellValueFactory(cellData -> cellData.getValue().getBranchGSTProperty());
		bm4.setCellValueFactory(cellData -> cellData.getValue().getBranchStateProperty());

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	
    @FXML
    void GetAllBranches(ActionEvent event) {
    	String msg = RestCaller.getallMastrs("BranchMst");
    			

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


       private void PageReload() {
       	
 }
}
