package com.maple.mapleclient.controllers;

import java.sql.Date;
import javafx.scene.input.KeyEvent;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ConsumptionReasonMst;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ConsumptionReport;
import com.maple.report.entity.PurchaseReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;

public class InhouseConsumptionReportCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ConsumptionReport> reportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtItemGroup;

    @FXML
    private ListView<String> lsGroup;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnAdd;

    @FXML
    private TextField txtItemName;
    @FXML
    private ComboBox<String> cmbDepartment;
    

    @FXML
    private TableView<ConsumptionReport> tbConsumption;

    @FXML
    private TableColumn<ConsumptionReport,String> clVoucherDate;

    @FXML
    private TableColumn<ConsumptionReport,String> clDepartment;

    @FXML
    private TableColumn<ConsumptionReport,String> clItemGroup;

    @FXML
    private TableColumn<ConsumptionReport,String> clItemName;

    @FXML
    private TableColumn<ConsumptionReport,String> clBatch;

    @FXML
    private TableColumn<ConsumptionReport,Number> clQty;

    @FXML
    private TableColumn<ConsumptionReport,String> clMrp;

    @FXML
    private TableColumn<ConsumptionReport,Number> clAmount;
    @FXML
 	private void initialize() 
    {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	ResponseEntity<List<ConsumptionReasonMst>> getAll = RestCaller.getAllConsumptionReason();

    	for(ConsumptionReasonMst consmMst:getAll.getBody())
    	{
    		cmbDepartment.getItems().add(consmMst.getReason());
    	}
    }

    @FXML
    void actionAdd(ActionEvent event) {
    	lsGroup.getItems().add(txtItemGroup.getText());
    	lsGroup.getSelectionModel().select(txtItemGroup.getText());
    	txtItemGroup.clear();
    }

    @FXML
    void actionClear(ActionEvent event) {
    	lsGroup.getItems().clear();
    	dpFromDate.setValue(null);
    	dpToDate.setValue(null);
    	cmbDepartment.getSelectionModel().clearSelection();
    	tbConsumption.getItems().clear();
    	reportList.clear();
    }

    @FXML
    void actionPrint(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
   
	try {
		JasperPdfReportService.InhouseConsumptionReport(startDate, endDate, cat, cmbDepartment.getSelectionModel().getSelectedItem());
	} catch (JRException e) {
		e.printStackTrace();
	}
    }

    @FXML
    void actionShow(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	ResponseEntity<List<ConsumptionReport>> department = RestCaller.getdepartmentWiseConsumption(startDate,endDate,cat,cmbDepartment.getSelectionModel().getSelectedItem());
    	reportList = FXCollections.observableArrayList(department.getBody());
    	fillTable();
    }
    private void fillTable()
    {
    	tbConsumption.setItems(reportList);
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clDepartment.setCellValueFactory(cellData -> cellData.getValue().getReasonProperty());
    	clItemGroup.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());

    	
    }
	  @Subscribe
		public void popupOrglistner(CategoryEvent CategoryEvent) {

			Stage stage = (Stage) btnAdd.getScene().getWindow();
			if (stage.isShowing()) {
				txtItemGroup.setText(CategoryEvent.getCategoryName());
			}
		}
    @FXML
    void loadCategoryPopUp(MouseEvent event) {

    	try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			btnAdd.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
    
    }
    
	private void showPopup() {
		
	}
    
    @FXML
    void onEnterItemPopup(KeyEvent event) {

    	
    	
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
