package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BarcodeFormatMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

public class BarcodeEditWindowCtl {

	private EventBus eventBus = EventBusFactory.getEventBus();
//	private ObservableList<BarcodeEditWindow> barcodeEditWindowTable = FXCollections.observableArrayList();
	String itemId;
	
	private ObservableList<BarcodeFormatMst> barcodeFormatList = FXCollections.observableArrayList();
	
	
	@FXML
	private Button btnEdit;

	@FXML
	private Button btnShowAll;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TableView<?> tblBarcodeEdit;

	@FXML
	private TableColumn<?, String> clItemName;

	@FXML
	private TableColumn<?, String> clBarcode;

	@FXML
	void ShowAllItems(ActionEvent event) {
		showAll();
	}

	
	//---------anandu show all button start 02-05-2021-----------
	
	private void showAll() {
		//tbBarcodeFormat.getItems().clear();
    	//barcodeFormatList.clear();
    	ResponseEntity<List<BarcodeFormatMst>> barcodeList = RestCaller.showAllBarcodeFormat();
    	
    	barcodeFormatList = FXCollections.observableArrayList(barcodeList.getBody());
    	filltable();
	}
	
	
	//------------------end---------------------
	

	@FXML
	void barcode(ActionEvent event) {

	}

	@FXML
	void barcodeOnEnter(KeyEvent event) {

	}

	@FXML
	void EditBarcode(ActionEvent event) {

		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select Item name...!", false);
			txtItemName.requestFocus();
			return;
		}

		if (txtBarcode.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select barcode...!", false);
			txtBarcode.requestFocus();
			return;
		}

		ResponseEntity<ItemMst> itemMstResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());

		ItemMst itemMst = itemMstResp.getBody();
		if (null == itemMst) {
			notifyMessage(5, "Item not found", false);
			return;
		}

		itemMst.setBarCode(txtBarcode.getText());

		RestCaller.updateItemMst(itemMst);
		notifyMessage(5, "Saved ", true);

		txtItemName.clear();
		txtBarcode.clear();

	//	showAll();
		return;

	}

	//---------anandu fill table start 02-05-2021-----------
	
	 private void filltable()
	    {
//	    	tblItems.setItems(itemBranchList);
//	    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
//	    	clBranch.setCellValueFactory(cellData -> cellData.getValue().getbranchProperty());
//	    	
	    }
	 
	 
	 //--------------end--------------------
	 
	@FXML
	void addOnEnter(KeyEvent event) {

	}

	@FXML
	void itemOnEnter(KeyEvent event) {

	}

	@FXML
	void saveOnKey(KeyEvent event) {

	}

	private void showItemPopUp() {
		try {
			System.out.println("STOCK POPUP");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));

			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));

			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void ShowPopupOnClick(MouseEvent event) {

		showItemPopUp();
	}

	@FXML
	private void initialize() {

		eventBus.register(this);
//
//		tblBarcodeEdit.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//			if (newSelection != null) {
//				if (null != newSelection.getId()) {
//
//					barcodeEditWindow = new BarcodeEditWindow();
//					barcodeEditWindow.setId(newSelection.getId());
//				}
//			}
//		});
	}

	@Subscribe
	public void popupOrglistner(ItemPopupEvent itemPopupEvent) {

		Stage stage = (Stage) btnEdit.getScene().getWindow();
		if (stage.isShowing()) {

			Platform.runLater(() -> {
				txtItemName.setText(itemPopupEvent.getItemName());
				txtBarcode.setText(itemPopupEvent.getBarCode());
				itemId = itemPopupEvent.getItemId();
			});

		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

}
