package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BillSummaryReport;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.DailySalesSummary;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class DayEndCtl {
	String taskid;
	String processInstanceId;

	DailySalesSummary dailySalesSummary = null;
	BillSummaryReport billSummaryReport = null;
	BranchMst branch = null;
	private ObservableList<DailySalesSummary> dailySalesSummaryList = FXCollections.observableArrayList();

	private ObservableList<BillSummaryReport> billSummaryReportList = FXCollections.observableArrayList();

	@FXML
	private TableView<BillSummaryReport> tblDayEnd;

	@FXML
	private TableColumn<BillSummaryReport, String> ClSlNo;

	@FXML
	private TableColumn<BillSummaryReport, String> ClParticulars;

	@FXML
	private TableColumn<BillSummaryReport, String> ClAmount;

	@FXML
	private DatePicker dpDate;

	@FXML
	private Button ClearTable;

	@FXML
	void ClearTableAction(ActionEvent event) {

		dpDate.setValue(null);
		tblDayEnd.getItems().clear();

	}

	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		System.out.println("---branchRest---Su==");

		tblDayEnd.setItems(billSummaryReportList);

		ClAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		ClParticulars.setCellValueFactory(cellData -> cellData.getValue().getDescriptionProperty());
		ClSlNo.setCellValueFactory(cellData -> cellData.getValue().getSlNoPropertyProperty());

		tblDayEnd.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				System.out.println("getSelectionModel" + newSelection);
				if (null != newSelection.getSlNo()) {
					if (newSelection.getSlNo().equals("9")) {
//						CashSalesCall();
					}
					if (newSelection.getSlNo().equals("10")) {
//						OnlineSalesCall();
					}
					if (newSelection.getSlNo().equals("11")) {
//						DebitCardSalesCall();
					}
					if (newSelection.getSlNo().equals("16")) {
//						AdvanceReceivedFromOrderCall();
					}

				}
			}
		});

		// dpDate Event mangement for show report for different date

		dpDate.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.print("Date Changed");
				getDayEnd();

			}
		});

	}

	@FXML
	void getDailySalesReport(ActionEvent event) {

		getDayEnd();

	}

	public void getDayEnd() {
		tblDayEnd.getItems().clear();

		billSummaryReport = new BillSummaryReport();

		branch = new BranchMst();
		ResponseEntity<BranchMst> respentity = RestCaller.SearchMyBranch();
		branch = respentity.getBody();

		java.util.Date uDate = Date.valueOf(dpDate.getValue());

		String strDate = SystemSetting.UtilDateToString(uDate);

		ResponseEntity<List<Object>> billSummaryReportDtls = RestCaller.getDailySalesReport(branch.getBranchCode(),
				strDate);

		Iterator itr1 = billSummaryReportDtls.getBody().iterator();
		while (itr1.hasNext()) {
			List element = (List) itr1.next();
			System.out.println("------billSummaryReportDtls--------" + element);
			Object slNo = element.get(0);
			Object description = element.get(1);
			Object amount = element.get(2);
			if (slNo != null) {
				billSummaryReport = new BillSummaryReport();

				billSummaryReport.setAmount(amount + "");
				billSummaryReport.setDescription((String) description);
				billSummaryReport.setSlNo(slNo + "");

				billSummaryReportList.add(billSummaryReport);
			}
			tblDayEnd.setItems(billSummaryReportList);
		}

	}

	
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void CashSalesCall() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CashSalesReport.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			tblDayEnd.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void OnlineSalesCall() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/OnlineSalesReport.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			tblDayEnd.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void DebitCardSalesCall() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/OnlineSalesReport.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			tblDayEnd.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void AdvanceReceivedFromOrderCall() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/OnlineSalesReport.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			tblDayEnd.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
}
