package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.JTextField;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;

import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.entity.ReceiptInvoiceDtl;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.ReorderMst;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountHeadsPopupEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.CustomerNewPopUpEvent;
import com.maple.mapleclient.events.PaymentPopUpEvent;
import com.maple.mapleclient.events.ReceiptInvoicePopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;


public class ReceiptWindowCtl {

	String processInstanceId;
	

	Integer rowCounter = 1;
	String taskid = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	ReceiptDtl receipt;
	Double totalamount;
	ReceiptHdr receipthdr = null;
	AccountHeads customerRegistration = null;
	ReceiptInvoiceDtl receiptInvoiceDtl = null;
	AccountHeads accountHeads = null;
	AccountReceivable accountReceivable = null;
	ReceiptInvoiceDtl receiptInvoiceDtlDelete = null;
	String bankId;
	OwnAccount ownAccount = null;
	private ObservableList<ReceiptDtl> receiptList = FXCollections.observableArrayList();
	private ObservableList<OwnAccount> ownAccList = FXCollections.observableArrayList();
	private ObservableList<ReceiptDtl> receiptList1 = FXCollections.observableArrayList();
	private ObservableList<ReceiptHdr> receipthdrList = FXCollections.observableArrayList();
	private ObservableList<ReceiptInvoiceDtl> receiptInvoiceDtlList = FXCollections.observableArrayList();
	java.util.Date invoiceDate = null;
	@FXML
	private Button btnInvDelete;

	@FXML
	private TextField txtTotalBalance;
	@FXML
	private TextField txtAccount;

	@FXML
	private ComboBox<String> cmbModeofReceipt;

	@FXML
	private TextField txtPaidAmount;

	@FXML
	private Button btnAdd;
	@FXML
	private TextField txtBalance;
	@FXML
	private TextField txtRemark;

	@FXML
	private TextField txtInvoiceNumber;

	@FXML
	private Button btnClear;

	@FXML
	private TextField txtAmount;

	@FXML
	private TextField txtInstrumentNumber;

	@FXML
	private TextField txtVoucherNumber;

	@FXML
	private ComboBox<String> cmbDepositBank;
	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnFinalSubmit;

	@FXML
	private Button btnShowAll;

	@FXML
	private DatePicker dpTransDate;

	@FXML
	private DatePicker dpInstrumentDate;

	@FXML
	private TextField txtTotal;

	@FXML
	private TextField txtDate;

	@FXML
	private TableView<ReceiptDtl> tblReceiptWindow;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC1;

	@FXML
	private TableColumn<ReceiptDtl, LocalDate> RWC2;

	@FXML

	private TableColumn<ReceiptDtl, String> RWC4;

	@FXML
	private TableColumn<ReceiptDtl, Number> RWC5;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC6;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC7;

	@FXML
	private TableColumn<ReceiptDtl, LocalDate> RWC8;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC9;

	@FXML
	private TableColumn<ReceiptDtl, String> RWC11;
	@FXML
	private TableView<ReceiptInvoiceDtl> tbRecieptInvoiceDtl;

	@FXML
	private TableColumn<ReceiptInvoiceDtl, String> clReceiptInvReceiptNo;
	@FXML
	private TableColumn<ReceiptInvoiceDtl, LocalDate> clReceiptInvReciptDate;

	@FXML
	private TableColumn<ReceiptInvoiceDtl, String> clReceiptInvInvoiceNo;

	@FXML
	private TableColumn<ReceiptInvoiceDtl, LocalDate> clReceiptInvInoviceDate;

	@FXML
	private TableColumn<ReceiptInvoiceDtl, Number> clReceiptInvReceivedAmt;
	@FXML
	private TableView<OwnAccount> tbOwnAcc;

	@FXML
	private TableColumn<OwnAccount, Number> clOwnAccAmt;

	@FXML
	private void initialize() {
		dpInstrumentDate = SystemSetting.datePickerFormat(dpInstrumentDate, "dd/MMM/yyyy");
		dpTransDate = SystemSetting.datePickerFormat(dpTransDate, "dd/MMM/yyyy");
		try {
			btnFinalSubmit.setVisible(false);
		} catch (Exception e) {

		}
//		dpTransDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		txtDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd"));
		eventBus.register(this);
		receiptList.add(receipt);
		// cmbModeofReceipt.getItems().add("CASH");

		// -----------new vesrion 1.9 surya
//		ResponseEntity<AccountHeads> accountHead = RestCaller
//				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
//		AccountHeads accountHeads = accountHead.getBody();
//		accountHeads.setId(accountHead.getBody().getId());
//		cmbModeofReceipt.getItems().add("TRASNFER");
//		cmbModeofReceipt.getItems().add("CHEQUE");
//		cmbModeofReceipt.getItems().add("DD");
//		cmbModeofReceipt.getItems().add("MOBILE PAY");
//		cmbModeofReceipt.getItems().add("CARD");
//
//		cmbModeofReceipt.getItems().add(accountHead.getBody().getAccountName());

		ResponseEntity<List<ReceiptModeMst>> receiptModeMstListResp = RestCaller.getAllReceiptMode();
		List<ReceiptModeMst> receiptModeMstList = receiptModeMstListResp.getBody();

		for (ReceiptModeMst receiptModeMst : receiptModeMstList) {
			if (!receiptModeMst.getReceiptMode().equalsIgnoreCase("CASH")) {
				cmbModeofReceipt.getItems().add(receiptModeMst.getReceiptMode());
			}
		}

		// -----------new vesrion 1.9 surya end

		rowCounter = 0;

		cmbModeofReceipt.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				// ------------------new version 1.9 surya

				ResponseEntity<List<ReceiptModeMst>> receiptmodeList = RestCaller.getReceiptModeMstByName(newValue);
				ReceiptModeMst receiptModeMst = receiptmodeList.getBody().get(0);
				// ------------------new version 1.9 surya end

				if (newValue.matches(SystemSetting.getSystemBranch() + "-CASH")) {
					cmbDepositBank.setDisable(true);
					txtInstrumentNumber.setDisable(true);
					dpInstrumentDate.setDisable(true);
				}
				// ------------------new version 1.9 surya

//				else if (newValue.matches("CARD")) {
//					cmbDepositBank.setDisable(false);
//					txtInstrumentNumber.setDisable(true);
//					dpInstrumentDate.setDisable(true);
//
//				} 

				else if (receiptModeMst.getCreditCardStatus().equalsIgnoreCase("YES")) {

					cmbDepositBank.setDisable(false);
					txtInstrumentNumber.setDisable(true);
					dpInstrumentDate.setDisable(true);

				}
				// ------------------new version 1.9 surya end

				else {
					cmbDepositBank.setDisable(false);
					txtInstrumentNumber.setDisable(false);
					dpInstrumentDate.setDisable(false);
				}

			}

		});
		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
		ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid = lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbDepositBank.getItems().add((String) bankName);

				// accountHeads.getAccountName);
			}
		}
		tbRecieptInvoiceDtl.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						System.out.println("getSelectionModel");
						if (null != newSelection.getId()) {

							receiptInvoiceDtlDelete = new ReceiptInvoiceDtl();
							receiptInvoiceDtlDelete.setId(newSelection.getId());
						}
					}
				});
		tbOwnAcc.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					ownAccount = new OwnAccount();
					ownAccount.setId(newSelection.getId());
				}
			}
		});
	}

	@FXML
	void InvDelete(ActionEvent event) {

		if (null != receiptInvoiceDtlDelete) {
			if (null != receiptInvoiceDtlDelete.getId()) {
				RestCaller.deleteReceiptInvDtl(receiptInvoiceDtlDelete.getId());
				notifyMessage(4, "Deleted!!!");
				receiptInvoiceDtlDelete = null;
				tbRecieptInvoiceDtl.getItems().clear();
				ResponseEntity<List<ReceiptInvoiceDtl>> receiptInDtlSaved = RestCaller.getReceiptInvDtlById(receipthdr);
				receiptInvoiceDtlList = FXCollections.observableArrayList(receiptInDtlSaved.getBody());
				fillreciepttable();
			}
		} else if (null != ownAccount.getId()) {
			RestCaller.deleteOwnAcc(ownAccount.getId());
			notifyMessage(4, "Deleted!!!");
			ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller
					.getOwnAccountDtlsByReceiptHdrId(receipthdr.getId());
			ownAccList = FXCollections.observableArrayList(ownAccSaved.getBody());
			fillOwnAcc();
			double total = 0.0, totalBalance = 0.0;
			total = RestCaller.getSummaryOwnAccByReceiptHdr(receipthdr.getId());
			if (null != txtTotalBalance) {
				double invTotal = RestCaller.getSummaryReceiptInvTotal(receipthdr.getId());
				txtTotalBalance.setText(Double.toString(invTotal));

				totalBalance = Double.parseDouble(txtTotalBalance.getText());
				txtTotalBalance.setText(Double.toString(total + totalBalance));

			}
			txtBalance.setText(
					Double.toString((Double.valueOf(txtTotal.getText()) - Double.valueOf(txtTotalBalance.getText()))));
		}

	}

	@FXML
	void clear(ActionEvent event) {
		clearfields();
		txtTotal.clear();
	}

	private void clearfields() {
		// txtAccount.clear();
		txtAmount.clear();
		cmbDepositBank.getSelectionModel().clearSelection();
		txtInstrumentNumber.clear();
		txtInvoiceNumber.clear();
		txtRemark.clear();
		// txtVoucherNumber.clear();
		cmbModeofReceipt.getSelectionModel().clearSelection();
		dpInstrumentDate.setValue(null);

		// dpTransDate.setValue(null);
	}

	@FXML
	void delete(ActionEvent event) {
		RestCaller.deleteReceiptDtl(receipt.getId());
		notifyMessage(5, "Deleted");
		clearfields();
		tblReceiptWindow.getItems().clear();
		ResponseEntity<List<ReceiptDtl>> receiptdtlSaved = RestCaller.getReceiptDtl(receipthdr);
		receiptList1 = FXCollections.observableArrayList(receiptdtlSaved.getBody());
		for (ReceiptDtl recptdtl : receiptList1) {
			ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountById(recptdtl.getAccount());
			recptdtl.setAccountName(accSaved.getBody().getAccountName());
		}
		tblReceiptWindow.setItems(receiptList1);
		txtTotal.clear();

		txtPaidAmount.clear();

		setTotal();
		if (txtTotal.getText().trim().length() > 0)
			txtBalance.setText(Double
					.toString(Double.parseDouble(txtTotal.getText()) - Double.parseDouble(txtTotalBalance.getText())));

	}

	@FXML
	void finalSubmit(ActionEvent event) {

	}

	@FXML
	void save(ActionEvent event) {
		addItem();
	}

	public java.util.Date convertToDateViaInstant(LocalDate dateToConvert) {
		return java.util.Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	@FXML
	void loadpopup(MouseEvent event) {
		showPopup();
	}

	@FXML
	void OnEnterClick(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtAccount.getText().trim().isEmpty())
				txtInvoiceNumber.requestFocus();
			else
				showPopup();

		}
	}

	@FXML
	void amountKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtInstrumentNumber.isDisable()) {
				btnSave.requestFocus();
			} else
				txtInstrumentNumber.requestFocus();
		}
	}

	@FXML
	void instrumnetOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbDepositBank.requestFocus();
		}
	}

	@FXML
	void bankOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpInstrumentDate.requestFocus();
		}
	}

	@FXML
	void SaveOnKeyPrss(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void voucherDateKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();

		}
	}

	@FXML
	void invoiceKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
//			txt.requestFocus();
			showInvoicePopup();

		}
	}

	@FXML
	void onInvoiceClick(MouseEvent event) {
		showInvoicePopup();

	}

	@FXML
	void instrumentDate(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}
	}

	@FXML
	void RemarkKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtInvoiceNumber.requestFocus();

		}
	}

	@FXML
	void modeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtRemark.requestFocus();

		}
	}

	@FXML
	void tabletoDelete(MouseEvent event) {

		if (tblReceiptWindow.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tblReceiptWindow.getSelectionModel();
			receiptList = selectionModel.getSelectedItems();
			for (ReceiptDtl redtl : receiptList) {
				receipt.setId(redtl.getId());
			}
		}

	}

	private void fillreciepttable() {
		tbRecieptInvoiceDtl.setItems(receiptInvoiceDtlList);
		clReceiptInvInoviceDate.setCellValueFactory(cellData -> cellData.getValue().getinvoiceDateProperty());
		clReceiptInvInvoiceNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
		clReceiptInvReceivedAmt.setCellValueFactory(cellData -> cellData.getValue().getreceivedAmountProperty());
		clReceiptInvReciptDate.setCellValueFactory(cellData -> cellData.getValue().getreceiptDateProperty());

		double invTotal = RestCaller.getSummaryReceiptInvTotal(receipthdr.getId());
		txtTotalBalance.setText(Double.toString(invTotal));
	}

	private void filltable() {
		tblReceiptWindow.setItems(receiptList1);
		RWC1.setCellValueFactory(cellData -> cellData.getValue().getaccountProperty());
		RWC11.setCellValueFactory(cellData -> cellData.getValue().getremarkProperty());
		RWC5.setCellValueFactory(cellData -> cellData.getValue().getamountPropery());
		RWC6.setCellValueFactory(cellData -> cellData.getValue().getdepositBankProperty());
		RWC2.setCellValueFactory(cellData -> cellData.getValue().gettransDateProperty());
		RWC8.setCellValueFactory(cellData -> cellData.getValue().getInstrumentDateProperty());
		RWC4.setCellValueFactory(cellData -> cellData.getValue().getmodeOfPaymentProperty());
		RWC7.setCellValueFactory(cellData -> cellData.getValue().getinstnumberProperty());
		RWC9.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
	}

	private void setTotal() {

		totalamount = RestCaller.getSummaryReceiptTotal(receipthdr.getId());
		txtTotal.setText(Double.toString(totalamount));
	}

	@FXML
	void AddReceiptInvDtl(ActionEvent event) {
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}

			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}
//		if (null == dpTransDate.getValue()) {
//			notifyMessage(5, "Please Select Date!!!!");
//			dpTransDate.requestFocus();
//			return;
//		}
		if (txtAccount.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Customer");
			txtAccount.requestFocus();
			return;
		}
		if (txtInvoiceNumber.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Invoice Number");
			txtInvoiceNumber.requestFocus();
			return;
		}
		if (txtPaidAmount.getText().trim().isEmpty()) {
			notifyMessage(3, "Type paid amount");
			txtPaidAmount.requestFocus();
			return;
		} else {
			if (null == receipthdr) {
				receipthdr = new ReceiptHdr();

				receipthdr.setTransdate(SystemSetting.systemDate);

				receipthdr.setVoucherDate(SystemSetting.systemDate);
				receipthdr.setBranchCode(SystemSetting.systemBranch);

				ResponseEntity<ReceiptHdr> respentity = RestCaller.saveReceipthdr(receipthdr);
				receipthdr = respentity.getBody();
			}

			receiptInvoiceDtl = new ReceiptInvoiceDtl();
			java.sql.Date uDate = Date.valueOf(LocalDate.now());
			receiptInvoiceDtl.setreceiptDate(uDate);
			receiptInvoiceDtl.setinvoiceDate(Date.valueOf(SystemSetting.utilToLocaDate(invoiceDate)));
			String inVNo = txtInvoiceNumber.getText() == null ? "" : txtInvoiceNumber.getText();
			if (inVNo.trim().length() > 0) {
				receiptInvoiceDtl.setInvoiceNumber(txtInvoiceNumber.getText());

				receiptInvoiceDtl.setRecievedAmount(Double.parseDouble(txtPaidAmount.getText()));
				receiptInvoiceDtl.setReceiptHdr(receipthdr);
				receiptInvoiceDtl.setAccountHeads(customerRegistration);
				receiptInvoiceDtl.setReceiptNumber(receipthdr.getId());
				ResponseEntity<ReceiptInvoiceDtl> respentity1 = RestCaller.saveReceiptInvoiceDtl(receiptInvoiceDtl);
				receiptInvoiceDtl = respentity1.getBody();

				receiptInvoiceDtlList.add(receiptInvoiceDtl);
				fillreciepttable();
			} else {
				ResponseEntity<AccountHeads> custSaved = RestCaller.getAccountHeadsById(receipt.getAccount());
				if (null != custSaved.getBody()) {
					ownAccount = new OwnAccount();
					ownAccount.setAccountId(receipt.getAccount());
					ownAccount.setAccountType("CUSTOMER");
					ownAccount.setCreditAmount(Double.parseDouble(txtPaidAmount.getText()));
					ownAccount.setDebitAmount(0.0);
					ownAccount.setOwnAccountStatus("PENDING");
					ownAccount.setRealizedAmount(0.0);
					ownAccount.setVoucherDate(SystemSetting.systemDate);
					ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
					ownAccount.setReceiptHdr(receipthdr);
					ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
					ownAccount = respentity1.getBody();
					ownAccList.add(ownAccount);
					fillOwnAcc();
					if (!txtTotalBalance.getText().trim().isEmpty()) {
						double total = 0.0;
						total = Double.valueOf(txtTotalBalance.getText()) + respentity1.getBody().getCreditAmount();
						txtTotalBalance.setText(Double.toString(total));
					} else
						txtTotalBalance.setText(Double.toString(respentity1.getBody().getCreditAmount()));
				}
				notifyMessage(6, "Amount will set to Own Account");
			}
			if (txtTotal.getText().trim().length() > 0)
				txtBalance.setText(Double.toString(
						Double.parseDouble(txtTotal.getText()) - Double.parseDouble(txtTotalBalance.getText())));

//    	if(!txtTotalBalance.getText().trim().isEmpty())
//    	{
//    	txtBalance.setText(Double.toString((Double.valueOf(txtTotal.getText())-Double.valueOf(txtTotalBalance.getText()))));
//    	}

		}
		txtInvoiceNumber.clear();
		txtPaidAmount.clear();
	}

	@Subscribe
	public void popuplistner(ReceiptInvoicePopupEvent receiptInvoicePopupEvent) {

		receiptInvoiceDtl = new ReceiptInvoiceDtl();
		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnClear.getScene().getWindow();
		if (stage.isShowing()) {

//			receiptInvoiceDtl.setinvoiceDate(Date.valueOf(receiptInvoicePopupEvent.getInvoiceDate().toString()));
//			// sales trans hdr id

			invoiceDate = receiptInvoicePopupEvent.getInvoiceDate();
			txtInvoiceNumber.setText(receiptInvoicePopupEvent.getInvoiceNumber());
			txtPaidAmount.setText(String.valueOf(receiptInvoicePopupEvent.getBalanceAmount()));
			// txtAccount.setText(accountEvent.getAccountName());
			// String accountId = accountEvent.getAccountId();
		}

	}

	private void showInvoicePopup() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ReceiptInvPopUp.fxml"));
			Parent root = loader.load();
			ReceiptInvoicepopupCtl popupctl = loader.getController();
			popupctl.LoadAccountReceivable(customerRegistration.getId());

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showPopup() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CusstomerNewPopUp.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupCustomerlistner(CustomerNewPopUpEvent customerNewPopUpEvent) {

		customerRegistration = new AccountHeads();
//		customerRegistration.setCustomerName(txtAccount.getText());

		txtAccount.setText(customerNewPopUpEvent.getCustomerName());
		try {
			ResponseEntity<AccountHeads> custsaved = RestCaller.getAccountHeadsByName(txtAccount.getText());
			if (null != custsaved.getBody())
				customerRegistration = custsaved.getBody();
		} catch (Exception e) {
			return;
		}

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}

	private void addItem() {
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}

			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}
//		if(null == dpTransDate.getValue())
//		{
//			notifyMessage(3,"Select Voucher Date");
//			dpTransDate.requestFocus();
//			return;
//		}

		if (txtAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "Select Account Number!!!!");
			txtAccount.requestFocus();
			return;
		}
		if (cmbModeofReceipt.getSelectionModel().isEmpty()) {
			notifyMessage(5, "Select Mode Of Receipt!!!!");
			cmbModeofReceipt.requestFocus();
			return;
		}
		if (txtAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "Type Amount!!!!");
			txtAmount.requestFocus();
			return;
		}

		if (null == receipthdr) {
			receipthdr = new ReceiptHdr();

			receipthdr.setTransdate(SystemSetting.systemDate);

			receipthdr.setVoucherDate(SystemSetting.systemDate);
			receipthdr.setBranchCode(SystemSetting.systemBranch);

			ResponseEntity<ReceiptHdr> respentity = RestCaller.saveReceipthdr(receipthdr);
			receipthdr = respentity.getBody();
		}
		receipt = new ReceiptDtl();
		ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountHeadByName(txtAccount.getText());

		receipt.setAccount(accSaved.getBody().getId());
		receipt.setRemark(receipt.getRemark());
		receipt.setAmount(Double.parseDouble(txtAmount.getText()));
		receipt.setInvoiceDate(invoiceDate);

		// -----------------new version 1.9 surya
		ResponseEntity<List<ReceiptModeMst>> receiptmodeList = RestCaller
				.getReceiptModeMstByName(cmbModeofReceipt.getSelectionModel().getSelectedItem().toString());

		ReceiptModeMst receiptModeMst = receiptmodeList.getBody().get(0);
		// -----------------new version 1.9 surya end

		if (cmbModeofReceipt.getSelectionModel().getSelectedItem().toString()
				.equalsIgnoreCase(SystemSetting.getSystemBranch() + "-CASH")) {
			receipt.setInstrumentDate(null);
			receipt.setRemark(txtRemark.getText());

			receipt.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
			receipt.setInvoiceNumber(txtInvoiceNumber.getText());
			receipt.setModeOfpayment(cmbModeofReceipt.getSelectionModel().getSelectedItem());
			receipt.setReceipthdr(receipthdr);

			ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
					.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");

			if (null == accountHeadsResp.getBody()) {

				notifyMessage(5, "Branch Cash account not set");
				return;
			}
			AccountHeads accountHeads = accountHeadsResp.getBody();

			receipt.setDebitAccountId(accountHeads.getId());
			ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);
			receipt = respentity.getBody();
			receipt.setAccountName(txtAccount.getText());

			setTotal();
			receiptList1.add(receipt);
			filltable();
			if (txtTotalBalance.getText().trim().length() > 0)
				txtBalance.setText(Double.toString(
						Double.parseDouble(txtTotal.getText()) - Double.parseDouble(txtTotalBalance.getText())));

			clearfields();
		} else if (
		// ---------------new version 1.9 surya
//				cmbModeofReceipt.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase("CARD")
		receiptModeMst.getCreditCardStatus().equalsIgnoreCase("YES")
		// ---------------new version 1.9 surya end

		) {

			if (cmbDepositBank.getSelectionModel().isEmpty()) {
				notifyMessage(5, "Type Deposit Bank!!!!");
				cmbDepositBank.setDisable(false);

				cmbDepositBank.requestFocus();
				return;
			}

//					receipt.setInstrumentDate(Date.valueOf(dpInstrumentDate.getValue()));
//					receipt.setInstnumber(txtInstrumentNumber.getText());
			receipt.setDepositBank(cmbDepositBank.getSelectionModel().getSelectedItem());
			receipt.setRemark(txtRemark.getText());

			ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadByName(receipt.getDepositBank());

			if (null == accountHeadsResp.getBody()) {

				notifyMessage(5, " Bank account not set");
				return;
			}

			receipt.setDebitAccountId(accountHeadsResp.getBody().getId());

			receipt.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
			receipt.setInvoiceNumber(txtInvoiceNumber.getText());
			receipt.setModeOfpayment(cmbModeofReceipt.getSelectionModel().getSelectedItem());
			receipt.setReceipthdr(receipthdr);

			receipt.setAccountName(txtAccount.getText());
			ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);
			receipt = respentity.getBody();
			setTotal();
			receiptList1.add(receipt);
			filltable();

			if (txtTotalBalance.getText().trim().length() > 0)
				txtBalance.setText(Double.toString(
						Double.parseDouble(txtTotal.getText()) - Double.parseDouble(txtTotalBalance.getText())));

		} else {
			if (txtInstrumentNumber.getText().trim().isEmpty()) {
				notifyMessage(5, "Type Instrument Number!!!!");
				txtInstrumentNumber.setDisable(false);
				txtInstrumentNumber.requestFocus();
				return;
			}
			if (cmbDepositBank.getSelectionModel().isEmpty()) {
				notifyMessage(5, "Type Deposit Bank!!!!");
				cmbDepositBank.setDisable(false);

				cmbDepositBank.requestFocus();
				return;
			}
			if (null == dpInstrumentDate.getValue()) {
				notifyMessage(5, "Select Instrument Date!!!!");
				dpInstrumentDate.setDisable(false);
				dpInstrumentDate.requestFocus();
				return;
			}

			receipt.setInstrumentDate(Date.valueOf(dpInstrumentDate.getValue()));
			receipt.setInstnumber(txtInstrumentNumber.getText());
			receipt.setDepositBank(cmbDepositBank.getSelectionModel().getSelectedItem());
			receipt.setRemark(txtRemark.getText());

			ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadByName(receipt.getDepositBank());

			if (null == accountHeadsResp.getBody()) {

				notifyMessage(5, " Bank account not set");
				return;
			}

			receipt.setDebitAccountId(accountHeadsResp.getBody().getId());

			receipt.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
			receipt.setInvoiceNumber(txtInvoiceNumber.getText());
			receipt.setModeOfpayment(cmbModeofReceipt.getSelectionModel().getSelectedItem());
			receipt.setReceipthdr(receipthdr);

			receipt.setAccountName(txtAccount.getText());
			ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);
			receipt = respentity.getBody();
			setTotal();
			receiptList1.add(receipt);
			filltable();

			if (txtTotalBalance.getText().trim().length() > 0)
				txtBalance.setText(Double.toString(
						Double.parseDouble(txtTotal.getText()) - Double.parseDouble(txtTotalBalance.getText())));
		}
		clearfields();

		doFinalSubmit();

	}

	private void doFinalSubmit() {

		// if(txtTotal.getText().equalsIgnoreCase(txtTotalBalance.getText()))
		// {

		ResponseEntity<List<ReceiptDtl>> receiptdtlSaved = RestCaller.getReceiptDtl(receipthdr);
		if (receiptdtlSaved.getBody().size() == 0) {
			return;
		}

		if (txtTotalBalance.getText().length() > 0) {
			if ((Double.parseDouble(txtTotal.getText()) > (Double.parseDouble(txtTotalBalance.getText())))) {
				ownAccount = new OwnAccount();
				ownAccount.setAccountId(receipt.getAccount());
				ownAccount.setAccountType("CUSTOMER");
				ownAccount.setCreditAmount(
						Double.parseDouble(txtTotal.getText()) - Double.parseDouble(txtTotalBalance.getText()));
				ownAccount.setDebitAmount(0.0);
				ownAccount.setOwnAccountStatus("PENDING");
				ownAccount.setRealizedAmount(0.0);
				ownAccount.setVoucherDate(SystemSetting.systemDate);
				ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
				ownAccount.setReceiptHdr(receipthdr);
				ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
				ownAccount = respentity1.getBody();
				ownAccList.add(ownAccount);
				fillOwnAcc();
//				if(!txtTotalBalance.getText().trim().isEmpty())
//				{
//				double total = 0.0;
//				total = Double.valueOf(txtTotalBalance.getText())+respentity1.getBody().getCreditAmount();
//				txtTotalBalance.setText(Double.toString(total));
//				}
//				else
//					txtTotalBalance.setText(Double.toString(respentity1.getBody().getCreditAmount()));
//				}
				notifyMessage(6, "Amount will set to Own Account");
			}
			// version2.7 -- COMMENTS
			/*
			 * if((Double.parseDouble(txtTotal.getText())<(Double.parseDouble(
			 * txtTotalBalance.getText())))) {
			 * notifyMessage(5,"Please Enter Correct Amount!!!"); return; }
			 */
			// version2.7 ends
		} else {
			ownAccount = new OwnAccount();
			ownAccount.setAccountId(receipt.getAccount());
			ownAccount.setAccountType("CUSTOMER");
			ownAccount.setCreditAmount(Double.parseDouble(txtTotal.getText()));
			ownAccount.setDebitAmount(0.0);
			ownAccount.setOwnAccountStatus("PENDING");
			ownAccount.setRealizedAmount(0.0);
			ownAccount.setVoucherDate(SystemSetting.systemDate);
			ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
			ownAccount.setReceiptHdr(receipthdr);
			ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
			ownAccount = respentity1.getBody();
			ownAccList.add(ownAccount);
		}
		String financialYear = SystemSetting.getFinancialYear();
		String vNo = RestCaller.getVoucherNumber(financialYear + "RECEIPT");
		receipthdr.setVoucherNumber(vNo);
//		  Date date = Date.valueOf(LocalDate.now());
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
//			receipthdr.setVoucherDate(date);
		RestCaller.updateReceipthdr(receipthdr);

		ResponseEntity<List<ReceiptInvoiceDtl>> receiptInDtlSaved = RestCaller.getReceiptInvDtlById(receipthdr);
		receiptInvoiceDtlList = FXCollections.observableArrayList(receiptInDtlSaved.getBody());
		for (ReceiptInvoiceDtl reptDtl : receiptInvoiceDtlList) {
//			s
			accountReceivable = new AccountReceivable();
			
			ResponseEntity<AccountReceivable> savedAccounts = RestCaller
					.getAccountRecByVoucherIdAndDate(reptDtl.getInvoiceNumber(),
							SystemSetting.UtilDateToString(reptDtl.getinvoiceDate(),"yyyy-MM-dd"));
			
			accountReceivable = savedAccounts.getBody();
			if (null != accountReceivable) {
				accountReceivable.setPaidAmount(accountReceivable.getPaidAmount() + reptDtl.getRecievedAmount());
				RestCaller.updateAccountReceivablebyId(accountReceivable);
			}

		}
		for (ReceiptDtl rcpt : receiptList1) {
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(receipthdr.getBranchCode());
			ResponseEntity<AccountHeads> accountHead = RestCaller
					.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
			AccountHeads accountHeads = accountHead.getBody();
			if (rcpt.getModeOfpayment().equalsIgnoreCase(accountHeads.getAccountName())) {
				dayBook.setDrAccountName(SystemSetting.systemBranch + "-CASH");
				dayBook.setCrAmount(0.0);
			}

			else {
				dayBook.setDrAccountName(rcpt.getDepositBank());
				dayBook.setCrAmount(rcpt.getAmount());
			}
			dayBook.setDrAmount(rcpt.getAmount());
			ResponseEntity<AccountHeads> accountHead1 = RestCaller.getAccountById(rcpt.getAccount());
			AccountHeads accountHeads1 = accountHead1.getBody();
			dayBook.setNarration(accountHeads1.getAccountName() + receipthdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(receipthdr.getVoucherNumber());
			dayBook.setSourceVoucherType("RECEIPT");
			dayBook.setCrAccountName(accountHeads1.getAccountName());

			LocalDate ldate = SystemSetting.utilToLocaDate(receipthdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(ldate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}

		// version2.7
		ownAccount = null;
		// version2.7 ends

		// version1.8
		clearfields();
		txtBalance.clear();
		tbOwnAcc.getItems().clear();
		receiptInvoiceDtlList.clear();
		txtPaidAmount.clear();
		txtInvoiceNumber.clear();
		tbRecieptInvoiceDtl.getItems().clear();
		txtTotal.clear();
		notifyMessage(5, "Saved Successfully!!!!");
		tblReceiptWindow.getItems().clear();

		txtTotalBalance.clear();
		txtAccount.clear();

		String strDate = formatter.format(receipthdr.getVoucherDate());
		if (null != taskid) {
			RestCaller.completeTask(taskid);
		}
		// version1.8 ends...
		try {

			JasperPdfReportService.ReceiptInvoice(receipthdr.getVoucherNumber(), strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// version1.6 Comments ...

		/*
		 * clearfields();
		 * 
		 * txtBalance.clear(); tbOwnAcc.getItems().clear();
		 * receiptInvoiceDtlList.clear(); txtPaidAmount.clear();
		 * txtInvoiceNumber.clear(); tbRecieptInvoiceDtl.getItems().clear();
		 * txtTotal.clear(); notifyMessage(5, "Saved Successfully!!!!");
		 * tblReceiptWindow.getItems().clear(); receipthdr = null;
		 * txtTotalBalance.clear(); txtAccount.clear();
		 * 
		 */
		// version1.6 Comments ends...

		receipthdr = null;

		// }
		// else
//		{
//			notifyMessage(5, "Amount not Correct!!!");
//			return;
//		}
		// receiptList.clear();

	}

	private void fillOwnAcc() {
		double total = 0.0, totalBalance;
		tbOwnAcc.setItems(ownAccList);
		clOwnAccAmt.setCellValueFactory(cellData -> cellData.getValue().getcreditAmountProperty());

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		Stage stage = (Stage) btnClear.getScene().getWindow();
		if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			customerRegistration = new AccountHeads();
//				customerRegistration.setCustomerName(txtAccount.getText());
			ResponseEntity<AccountHeads> accountHead = RestCaller.getAccountById(taskWindowDataEvent.getAccountId());
			txtAccount.setText(accountHead.getBody().getAccountName());
			try {
				ResponseEntity<AccountHeads> custsaved = RestCaller.getAccountHeadsByName(txtAccount.getText());
				if (null != custsaved.getBody())
					customerRegistration = custsaved.getBody();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

}
