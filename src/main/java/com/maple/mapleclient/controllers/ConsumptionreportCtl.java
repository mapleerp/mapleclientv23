package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ConsumptionReport;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class ConsumptionreportCtl {
	String taskid;
	String processInstanceId;

	
    @FXML
    private ComboBox<String> branchselect;
	
    
    @FXML
   private DatePicker dpdate;
   

    @FXML
    private DatePicker dpToDate;
    
    
    
    @FXML
    private Button btnShow;
    @FXML
    private Button btnPrint;

    @FXML
    private TextField txtGrandTotal;
    
    @FXML
    private Button btnPrintReport;
    
    
    @FXML
    private TableView<ConsumptionReport> tbReport;

    @FXML
    private TableColumn<ConsumptionReport,String> tblitemName;

    @FXML
    private TableColumn<ConsumptionReport, String> tblunit;

    @FXML
    private TableColumn<ConsumptionReport, String> tblreason;

    @FXML
    private TableColumn<ConsumptionReport, Number> tblqty;

    @FXML
    private TableColumn<ConsumptionReport, String> tblmrp;

    @FXML
    private TableColumn<ConsumptionReport, Number> tblamount;
    
    private ObservableList<ConsumptionReport> consumptioReportList = FXCollections.observableArrayList();
    double totalamount = 0.0;
    @FXML
 	private void initialize() {

    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");
     	setBranches();
     	
    	FillTable();
     	}

    

@FXML
void show(ActionEvent event) {
	 totalamount = 0.0;
	java.util.Date uDate = Date.valueOf(dpdate.getValue());
	String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
	java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
	String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
	//uDate = SystemSetting.StringToUtilDate(strDate,);
	ResponseEntity<List<ConsumptionReport>> dailyconsumption = RestCaller.getConsumptionReport(branchselect.getSelectionModel().getSelectedItem(), strDate,strDate1);
	consumptioReportList = FXCollections.observableArrayList(dailyconsumption.getBody());
	tbReport.setItems(consumptioReportList);
	tblitemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	tblunit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
	tblqty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
	tblamount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	
	tblreason.setCellValueFactory(cellData -> cellData.getValue().getReasonProperty());
	tblmrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
	
	for(ConsumptionReport dailySaleReportDtl:consumptioReportList)
	{
		if(null != dailySaleReportDtl.getAmount())
		totalamount = totalamount + dailySaleReportDtl.getAmount();
	}
	BigDecimal settoamount = new BigDecimal(totalamount);
	settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
	txtGrandTotal.setText(settoamount.toString());

}
    
private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			branchselect.getItems().add(branchMst.getBranchCode());
		
			
		}
}

private void FillTable()
{
	
	tblitemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	tblunit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
	tblqty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
	tblamount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	tblreason.setCellValueFactory(cellData -> cellData.getValue().getReasonProperty());
	tblmrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
	
	
}

@FXML
void onClickOk(ActionEvent event) {

}
@FXML
void reportPrint(ActionEvent event) {

}
@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


  private void PageReload() {
  	
}

}
