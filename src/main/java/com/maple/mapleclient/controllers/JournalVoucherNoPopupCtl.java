package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.JobCardInHdr;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.JournalHdr;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

public class JournalVoucherNoPopupCtl {
	String taskid;
	String processInstanceId;

	StringProperty SearchString = new SimpleStringProperty();
	VoucherNoEvent voucherNoEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<JournalHdr> journalInList = FXCollections.observableArrayList();

	@FXML
	private TextField txtname;

	@FXML
	private Button btnSubmit;

	@FXML
	private TableView<JournalHdr> tablePopupView;

	@FXML
	private TableColumn<JournalHdr, String> voucherNo;

	
	@FXML
	private Button btnCancel;

	@FXML
	private void initialize() {
		txtname.textProperty().bindBidirectional(SearchString);
		LoadBrandBySearch("");
		voucherNoEvent = new VoucherNoEvent();
		
		tablePopupView.setItems(journalInList);
		voucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());

		
		tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getVoucherNumber()) {
					voucherNoEvent.setVoucherNumber(newSelection.getVoucherNumber());
					voucherNoEvent.setId(newSelection.getId());
					eventBus.post(voucherNoEvent);

				}
			}
		});

		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadBrandBySearch((String) newValue);

			}
		});
	}

	private void LoadBrandBySearch(String searchData) {

		journalInList.clear();

		ResponseEntity<List<JournalHdr>> getJournalDtlJobs = RestCaller.getJournalVoucherNumbers();
		journalInList = FXCollections.observableArrayList(getJournalDtlJobs.getBody());
		// tblJobCard.setItems(jobcardInList);
		return;
	}

	@FXML
	void OnKeyPress(KeyEvent event) {

	}

	@FXML
	void OnKeyPressTxt(KeyEvent event) {
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();

		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
	}

	@FXML
	void onCancel(ActionEvent event) {
		Stage stage = (Stage) btnCancel.getScene().getWindow();
		stage.close();
	}

	@FXML
	void submit(ActionEvent event) {
		eventBus.post(voucherNoEvent);

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
	}

	 @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }
}
