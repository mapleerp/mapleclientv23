package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportHsnWIsePurchaseDtlToExcel;
import com.maple.maple.util.ExportHsnWIseSalesDtlToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.HsnWisePurchaseDtlReport;
import com.maple.report.entity.HsnWiseSalesDtlReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRException;

public class HsnWiseSalesDtlReportCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<HsnWiseSalesDtlReport> reportList = FXCollections.observableArrayList();
	ExportHsnWIseSalesDtlToExcel exportHsnWIseSalesDtlToExcel = new ExportHsnWIseSalesDtlToExcel();

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private ListView<String> lsGroup;

    @FXML
    private DatePicker dpfromDate;

    @FXML
    private DatePicker dpTodate;

    @FXML
    private TextField txtGroup;

    @FXML
    private Button btnExport;

    @FXML
    private Button btnClear;

    @FXML
    private TableView<HsnWiseSalesDtlReport> tbHsnSalesReport;



    @FXML
    private TableColumn<HsnWiseSalesDtlReport,String> clItemName;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,String> clHsn;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,String> clUnit;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,Number> clQty;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,Number> clRate;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,Number> clValue;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,Number> clCgst;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,Number> clsgst;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,Number> clGst;

    @FXML
    private TableColumn<HsnWiseSalesDtlReport,Number> clcess;
    @FXML
   	private void initialize() {
    	dpfromDate = SystemSetting.datePickerFormat(dpfromDate, "dd/MMM/yyyy");
    	dpTodate = SystemSetting.datePickerFormat(dpTodate, "dd/MMM/yyyy");
    	eventBus.register(this);
    	 lsGroup.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }
    @Subscribe
	public void popupOrglistner(CategoryEvent CategoryEvent) {

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtGroup.setText(CategoryEvent.getCategoryName());
		}
    }
    private void loadCategoryPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    @FXML
    void actionAdd(ActionEvent event) {

    	lsGroup.getItems().add(txtGroup.getText());
    	lsGroup.getSelectionModel().select(txtGroup.getText());
    	txtGroup.clear();
    
    }

    @FXML
    void actionClear(ActionEvent event) {

    	lsGroup.getItems().clear();
    	dpfromDate.setValue(null);
    	dpTodate.setValue(null);
    	txtGroup.getText();
    	tbHsnSalesReport.getItems().clear();
    	reportList.clear();
    
    }

    @FXML
    void actionExport(ActionEvent event) {
     	java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpTodate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	if(selectedItems.size()==0)
    	{
    		cat="";
    	}
     	ResponseEntity<List<HsnWiseSalesDtlReport>> hsn =RestCaller.getHsnSalesDtl(startDate,endDate,cat);
    	reportList = FXCollections.observableArrayList(hsn.getBody());
    	for(HsnWiseSalesDtlReport hsnsales:reportList)
    	{
    		BigDecimal bdRate = new BigDecimal(hsnsales.getRate());
    		bdRate = bdRate.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setRate(bdRate.doubleValue());
    		
    		BigDecimal bdsgst = new BigDecimal(hsnsales.getSgstAmount());
    		bdsgst = bdsgst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setSgstAmount(bdsgst.doubleValue());
    		
    		BigDecimal bdcgst = new BigDecimal(hsnsales.getCgstAmount());
    		bdcgst = bdcgst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setCgstAmount(bdcgst.doubleValue());
    		
    		BigDecimal bdvalue = new BigDecimal(hsnsales.getValue());
    		bdvalue = bdvalue.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setValue(bdvalue.doubleValue());
    		
    		BigDecimal bdcess = new BigDecimal(hsnsales.getCessRate());
    		bdcess = bdcess.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setCessRate(bdcess.doubleValue());
    		
    		BigDecimal bdqty = new BigDecimal(hsnsales.getQty());
    		bdqty = bdqty.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setQty(bdqty.doubleValue());
    	}
    	exportHsnWIseSalesDtlToExcel.exportToExcel("HsnSalesDtl"+startDate+".xls", reportList);

    }

    @FXML
    void actionPrint(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpTodate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	if(selectedItems.size()==0)
    	{
    		cat="";
    	}
     	ResponseEntity<List<HsnWiseSalesDtlReport>> hsn =RestCaller.getHsnSalesDtl(startDate,endDate,cat);
    	reportList = FXCollections.observableArrayList(hsn.getBody());
    	try {
    		JasperPdfReportService.hsnCodeWiseSalesDtlReport(startDate, endDate, cat);
    	} catch (JRException e) {
    		e.printStackTrace();
    	}
    }

    @FXML
    void actionShow(ActionEvent event) {

    	java.util.Date uDate = Date.valueOf(dpfromDate.getValue());
    	String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpTodate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
    	List<String> selectedItems = lsGroup.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
    	if(selectedItems.size()==0)
    	{
    		cat="";
    	}
    	ResponseEntity<List<HsnWiseSalesDtlReport>> hsn =RestCaller.getHsnSalesDtl(startDate,endDate,cat);
    	reportList = FXCollections.observableArrayList(hsn.getBody());
    	fillTable();
    
    }
    private void fillTable()
    {
    	for(HsnWiseSalesDtlReport hsnsales:reportList)
    	{
    		BigDecimal bdRate = new BigDecimal(hsnsales.getRate());
    		bdRate = bdRate.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setRate(bdRate.doubleValue());
    		
    		BigDecimal bdsgst = new BigDecimal(hsnsales.getSgstAmount());
    		bdsgst = bdsgst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setSgstAmount(bdsgst.doubleValue());
    		
    		BigDecimal bdcgst = new BigDecimal(hsnsales.getCgstAmount());
    		bdcgst = bdcgst.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setCgstAmount(bdcgst.doubleValue());
    		
    		BigDecimal bdvalue = new BigDecimal(hsnsales.getValue());
    		bdvalue = bdvalue.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setValue(bdvalue.doubleValue());
    		
    		BigDecimal bdcess = new BigDecimal(hsnsales.getCessRate());
    		bdcess = bdcess.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setCessRate(bdcess.doubleValue());
    		
    		BigDecimal bdqty = new BigDecimal(hsnsales.getQty());
    		bdqty = bdqty.setScale(2,BigDecimal.ROUND_HALF_EVEN);
    		hsnsales.setQty(bdqty.doubleValue());
    	}
    	tbHsnSalesReport.setItems(reportList);
		clGst.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		clHsn.setCellValueFactory(cellData -> cellData.getValue().getHsnCodeProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());
		clcess.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
		clCgst.setCellValueFactory(cellData -> cellData.getValue().getCgstAmountProperty());
		clsgst.setCellValueFactory(cellData -> cellData.getValue().getSgstAmountProperty());


    }
    @FXML
    void onEnter(KeyEvent event) {
    	if(event.getCode()==KeyCode.ENTER)
    	{
    		loadCategoryPopup() ;
    	}
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
