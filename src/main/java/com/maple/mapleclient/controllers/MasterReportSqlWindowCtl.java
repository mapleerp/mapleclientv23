package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPropertyConfig;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.PurchaseOrderDtl;
import com.maple.mapleclient.entity.PurchaseOrderHdr;
import com.maple.mapleclient.entity.ReportSqlDtl;
import com.maple.mapleclient.entity.ReportSqlHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class MasterReportSqlWindowCtl {

	ReportSqlHdr reportSqlHdr = null;

	private ObservableList<ReportSqlDtl> reportSqlDtlList = FXCollections.observableArrayList();
	private ObservableList<ReportSqlHdr> reportSqlHdrTable = FXCollections.observableArrayList();

	private ObservableList<ReportSqlHdr> reportSqlHdrList = FXCollections.observableArrayList();
	
	private ObservableList<ReportSqlDtl> reportSqlDtlListTable = FXCollections.observableArrayList();
	
	
	private static final Logger logger = LoggerFactory.getLogger(MasterReportSqlWindowCtl.class);
	private EventBus eventBus = EventBusFactory.getEventBus();
	ReportSqlDtl reportSqlDtl = null;

	@FXML
	private TextField txtreportType;

	@FXML
	private TextArea txtsql;

	@FXML
	private Button btnAddItem;

	@FXML
	private Button btnDeleteItem;

	@FXML
	private Button btnClear;
	@FXML
	private Button btnfinalsave;
	@FXML
	private ComboBox<String> cmbparamName;

	@FXML
	private TextField txtparamSequence;
	@FXML
	private TableView<ReportSqlDtl> tblsql;
	@FXML
	private TableColumn<ReportSqlDtl, String> clreportType;

	@FXML
	private TableColumn<ReportSqlDtl, String> clparamName;

	@FXML
	private TableColumn<ReportSqlDtl, String> clparamsequence;

	@FXML
	private TableColumn<ReportSqlDtl, String> clsql;

	String reportSqlDtlId = null;
//	String reportSqlHdrId=null;

	@FXML
	private void initialize() {

		eventBus.register(this);
		logger.info(" =============== INITIALIZATION STARTED");
		// ==========table selection
		tblsql.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//			if (newSelection != null) {
//				if (null != newSelection.getId()) {
//
//					ReportSqlDtl reportSqlDtl = new ReportSqlDtl();
//					reportSqlDtl.setId(newSelection.getId());
//					reportSqlDtlId = newSelection.getId();
//					cmbparamName.getSelectionModel().select(newSelection.getParamName());
//					txtparamSequence.setText(newSelection.getParamSequence());
//					if (null != newSelection.getSqlString()) {
//
//						txtsql.setText(newSelection.getSqlString());
//						txtreportType.setText(newSelection.getReportName());
//						if(null != txtreportType.getText()) {
//						ResponseEntity<List<ReportSqlHdr>> reportSqlHdr1 = RestCaller
//								.getReportSqlHdrByReportName(txtreportType.getText());
//						reportSqlHdr=reportSqlHdr1.getBody().get(0);
//						}
//					}
//				}
//			}
			
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					reportSqlDtl = new ReportSqlDtl();
					cmbparamName.getSelectionModel().select(newSelection.getParamName());
					txtparamSequence.setText(newSelection.getParamSequence());
					if (null != newSelection.getSqlString()) {
						txtsql.setText(newSelection.getSqlString());
						txtreportType.setText(newSelection.getReportName());
					}
					if(null != txtreportType.getText()) {
						ResponseEntity<List<ReportSqlHdr>> reportSqlHdr1 = RestCaller
								.getReportSqlHdrByReportName(txtreportType.getText());
						reportSqlHdr=reportSqlHdr1.getBody().get(0);
						}
					reportSqlDtl.setId(newSelection.getId());


				}
			}
		});

		cmbparamName.getItems().add("COMPANYID");
		cmbparamName.getItems().add("BRANCHCODE");
		cmbparamName.getItems().add("FROMDATE");
		cmbparamName.getItems().add("TODATE");
		cmbparamName.getItems().add("ITEMID");

		logger.info(" =============== INITIALIZATION COMPLETED");
	}

	@FXML
	void ShowAll(ActionEvent event) {
		showAll();

	}

	private void showAll() {
		ResponseEntity<List<ReportSqlDtl>> respentity = RestCaller.getReportSqlDtl();
		reportSqlDtlListTable = FXCollections.observableArrayList(respentity.getBody());

		fillTable();

	}

	private void fillTable() {

		if(null != reportSqlHdr.getId()) {
		ResponseEntity<List<ReportSqlDtl>> reportSqlDtlListResp = RestCaller.getReportSqlDtlByHdrId(reportSqlHdr.getId());
		
		reportSqlDtlListTable = FXCollections.observableArrayList(reportSqlDtlListResp.getBody());
		}
		tblsql.setItems(reportSqlDtlListTable);
		for (ReportSqlDtl reportSqlDtl : reportSqlDtlListTable) {

			if (null != reportSqlDtl.getReportSqlHdrId()) {
				ResponseEntity<ReportSqlHdr> reportSqlHdr = RestCaller
						.getReportSqlHdrById(reportSqlDtl.getReportSqlHdrId());
				reportSqlDtl.setReportName(reportSqlHdr.getBody().getReportName());
				reportSqlDtl.setSqlString(reportSqlHdr.getBody().getSqlString());

			}

		}
		clsql.setCellValueFactory(cellData -> cellData.getValue().getSqlStringProperty());
		clreportType.setCellValueFactory(cellData -> cellData.getValue().getReportNameProperty());
		clparamName.setCellValueFactory(cellData -> cellData.getValue().getParamNameProperty());
		clparamsequence.setCellValueFactory(cellData -> cellData.getValue().getParamSequenceProperty());

	}

	@FXML
	void AddItem(ActionEvent event) {
		save();
	}

	private void save() {
		
		if (null == reportSqlHdr) {
			
			logger.info("======STARTED PARTIALY SAVE");
			if (txtreportType.getText().trim().isEmpty())

			{
				notifyMessage(5, "Please select report type", false);
				txtreportType.requestFocus();
				return;
			}
			if (txtsql.getText().trim().isEmpty()) {
				notifyMessage(5, "Please enter SQL Query", false);
				return;
			}
			if (null == cmbparamName.getValue()) {
				notifyMessage(5, "Please select Param Name", false);
				return;
			}

			if (txtparamSequence.getText().trim().isEmpty()) {
				notifyMessage(5, "Please enter param sequence", false);
				return;
			}
			
			reportSqlHdr = new ReportSqlHdr();
			reportSqlHdr.setReportName(txtreportType.getText());
			reportSqlHdr.setSqlString(txtsql.getText());
			ResponseEntity<ReportSqlHdr> reportSqlHdrSaved = RestCaller.saveReportSqlHdr(reportSqlHdr);
			reportSqlHdr=reportSqlHdrSaved.getBody();
		
			}
		if (null != reportSqlHdr) {
			
			reportSqlDtl = new ReportSqlDtl();

			if (null != reportSqlHdr.getId()) {


				reportSqlDtl.setReportSqlHdrId(reportSqlHdr.getId());

				reportSqlDtl.setParamName(cmbparamName.getSelectionModel().getSelectedItem());
				reportSqlDtl.setParamSequence(txtparamSequence.getText());
						
				ResponseEntity<ReportSqlDtl> respentity = RestCaller.saveReportSqlDtl(reportSqlDtl);
				reportSqlDtl = respentity.getBody();

				reportSqlDtlListTable.add(reportSqlDtl);
				
				fillTable();
				clearFields();
			} else {
				return;
			}
		} else {
			return;
		}
		reportSqlDtl = new ReportSqlDtl();
		
		
		
		
//		ResponseEntity<List<ReportSqlDtl>> reportSqlDtlSaved = RestCaller.getReportSqlDtlByHdrId(reportSqlHdr.getId());
//		if (reportSqlDtlSaved.getBody().size() == 0) {
//			return;
//		}


	}

	@FXML
	void finalSave(ActionEvent event) {
		
		if (txtreportType.getText().trim().isEmpty())

		{
			notifyMessage(5, "Please select report type", false);
			txtreportType.requestFocus();
			return;
		}
		if (txtsql.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter SQL Query", false);
			return;
		}
		if (null == cmbparamName.getValue()) {
			notifyMessage(5, "Please select Param Name", false);
			return;
		}

		if (txtparamSequence.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter param sequence", false);
			return;
		}
		
		ResponseEntity<List<ReportSqlDtl>> reportSqlDtlSaved = RestCaller.getReportSqlDtlByHdrId(reportSqlHdr.getId());
		if (reportSqlDtlSaved.getBody().size() == 0) {
			return;
		}

		// version1.7
		reportSqlHdr.setReportName(txtreportType.getText());
		reportSqlHdr.setSqlString(txtsql.getText());
		RestCaller.updateReportSqlHdr(reportSqlHdr);
		
		reportSqlHdr = null;
		reportSqlDtl = null;
		clearFields();
		notifyMessage(5, "Saved ", true);
		logger.info(" ============COMPLETED FINAL SAVE");
	}

	@FXML
	void DeleteItem(ActionEvent event) {
		logger.info(" =============== DELETE ITEM STARTED");
		if (null != reportSqlDtl.getId()) {

//			 if (null != reportSqlDtl.getId()) {

			RestCaller.deletereportSqlDtlById(reportSqlDtl.getId());

			showAll();
			txtparamSequence.clear();
			txtreportType.clear();
			txtsql.clear();
			cmbparamName.setValue(null);
			notifyMessage(5, "deleted", false);
			return;
//			 }
		}

		logger.info(" =============== DELETE ITME COMPLETED");
	}

	@FXML
	void addItem(KeyEvent event) {
		save();
	}

	@FXML
	void clearItems(ActionEvent event) {
		clearFields();
		tblsql.getItems().clear();
	}

	private void clearFields() {

		txtsql.clear();
		cmbparamName.setValue(null);
		txtparamSequence.clear();
		txtreportType.clear();
		reportSqlDtl = null;
		reportSqlHdr = null;
		reportSqlDtlId=null;
//		tblsql.getItems().clear();
	}

	@FXML
	void paramsequenceOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbparamName.requestFocus();
		}
	}

	@FXML
	void reportTypeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtsql.requestFocus();
		}
	}

	@FXML
	void sqlOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtparamSequence.requestFocus();
		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

}