package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.entity.ReceiptInvoiceDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PaymentVoucher;
import com.maple.report.entity.ReceiptInvoice;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRException;

public class OrgPaymentInvoiceDtlReportCtl {

	String voucherNumber;
	
	String taskid;
	String processInstanceId;

	ReceiptInvoice receiptInvoice= null;
	ReceiptHdr receiptHdr = null;
	ReceiptInvoiceDtl receiptInvDtl = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PaymentVoucher> ReceiptInvoiceTable = FXCollections.observableArrayList();
	private ObservableList<ReceiptInvoiceDtl> receiptInvoiceDtlList = FXCollections.observableArrayList();
	private ObservableList<OwnAccount> ownAccList = FXCollections.observableArrayList();
	String accountId = "";
    @FXML
    private DatePicker dpFromDate;
    @FXML
    private Button btnPrint;
    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtAccount;
    
    @FXML
    private TextField txtTotal;

    @FXML
    private Button btnShow;

    
    
    @FXML
    private Button btnDelete;
    

    @FXML
    private TableView<PaymentVoucher> tbReceipts;

    @FXML
    private TableColumn<PaymentVoucher, LocalDate> clDate;

    @FXML
    private TableColumn<PaymentVoucher, String> clReceiptVoucher;

    @FXML
    private TableColumn<PaymentVoucher,String> clAccount;
    @FXML
    private TableColumn<PaymentVoucher, Number> clAmount;
    @FXML
    private TableView<ReceiptInvoiceDtl> tbInvoiceDtl;
    @FXML
    private TableColumn<ReceiptInvoiceDtl, String> clInvDtlInvNo;

    @FXML
    private TableColumn<ReceiptInvoiceDtl, Number> clInvDtlPaidAmt;

    @FXML
    void showData(ActionEvent event) {
    	String endDate = "";
		Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
		endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		if(!txtAccount.getText().isEmpty())
		{
    	ResponseEntity<List<PaymentVoucher>> respentity  = RestCaller.getPaymentByCustomerBetweenDate(accountId,strtDate,endDate);
    	ReceiptInvoiceTable = FXCollections.observableArrayList(respentity.getBody());
    	Double total=0.0;
    	for(int i=0;i<ReceiptInvoiceTable.size();i++) {
    		
    		total+=ReceiptInvoiceTable.get(i).getAmount();
    	}
    	
    	txtTotal.setText(String.valueOf(total));
		}
		else
		{
			ResponseEntity<List<PaymentVoucher>> respentity  = RestCaller.getpaymentHdrBetweenDate(strtDate,endDate);
	    	ReceiptInvoiceTable = FXCollections.observableArrayList(respentity.getBody());
	    	Double total=0.0;
	    	for(int i=0;i<ReceiptInvoiceTable.size();i++) {
	    		
	    		total+=ReceiptInvoiceTable.get(i).getAmount();
	    	}
	    	
	    	txtTotal.setText(String.valueOf(total));
		}
    	fillTable();

    }
    @FXML
    void loadAccountsOnClick(MouseEvent event) {
    	showPopup();

    }
    private void fillTable()
    {
    	tbReceipts.setItems(ReceiptInvoiceTable);
    	clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
    	clReceiptVoucher.setCellValueFactory(cellData -> cellData.getValue().getvoucherNoProperty());
    	clAccount.setCellValueFactory(cellData -> cellData.getValue().getaccountNameProperty());
    }
    @FXML
	private void initialize() {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		eventBus.register(this);
		
		tbReceipts.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					receiptHdr = new ReceiptHdr();
					receiptHdr.setId(newSelection.getId());
					voucherNumber=newSelection.getVoucherNO();
					
					ResponseEntity<List<ReceiptInvoiceDtl>> receiptInDtlSaved = RestCaller.getReceiptInvDtlById(receiptHdr);
					receiptInvoiceDtlList = FXCollections.observableArrayList(receiptInDtlSaved.getBody());
					fillInvoiceDetailTable();
					ResponseEntity<List<OwnAccount>> ownAccSaved = RestCaller.getOwnAccountDtlsByReceiptHdrId(receiptHdr.getId());
		    		ownAccList = FXCollections.observableArrayList(ownAccSaved.getBody());
		    		fillOwnAcc();
				}
			}
		});
		
		tbInvoiceDtl.setVisible(false);
	}
    @FXML
    void loadCustomer(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		showPopup();

		}
    }
    private void fillOwnAcc()
    {
    	for (OwnAccount owc : ownAccList)
    	{
    		receiptInvDtl = new ReceiptInvoiceDtl();
    		receiptInvDtl.setInvoiceNumber("OWN ACCOUNT");
    		receiptInvDtl.setRecievedAmount(owc.getCreditAmount());
    		receiptInvoiceDtlList.add(receiptInvDtl);
    	}
    	fillInvoiceDetailTable();
    //	clInvDtlInvNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
    	//clInvDtlPaidAmt.setCellValueFactory(cellData -> cellData.getValue().getreceivedAmountProperty());
    }
    private void fillInvoiceDetailTable()
    {
    	tbInvoiceDtl.setItems(receiptInvoiceDtlList);
    	clInvDtlInvNo.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
    	clInvDtlPaidAmt.setCellValueFactory(cellData -> cellData.getValue().getreceivedAmountProperty());
    }
    private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			btnShow.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

    @FXML
    void PrintData(ActionEvent event) {
    	String endDate = "";
		Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
		endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		if(!txtAccount.getText().isEmpty())
		{
try {
	JasperPdfReportService.PaymentReportByBetweenDate(strtDate,endDate);
		//	JasperPdfReportService.getPaymentByCustomerBetweenDate(accountId,strtDate,endDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		}
		else
		{
			try {
						
						JasperPdfReportService.PaymentReportByBetweenDate(strtDate,endDate);
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					}
    }
	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnShow.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(customerEvent.getCustomerName());
			accountId = customerEvent.getCustId();

		}

}
	
	public void show() {
		
		String endDate = "";
		Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
		endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
		if(!txtAccount.getText().isEmpty())
		{
		//	txtTotal.clear();
    	ResponseEntity<List<PaymentVoucher>> respentity  = RestCaller.getPaymentByCustomerBetweenDate(accountId,strtDate,endDate);
    	ReceiptInvoiceTable = FXCollections.observableArrayList(respentity.getBody());
    	
    	
       	Double total=0.0;
    	for(int i=0;i<ReceiptInvoiceTable.size();i++) {
    		
    		total=+ReceiptInvoiceTable.get(i).getAmount();
    	}
    	
    	txtTotal.setText(String.valueOf(total));
		}
		else
		{
			//txtTotal.clear();
			ResponseEntity<List<PaymentVoucher>> respentity  = RestCaller.getpaymentHdrBetweenDate(strtDate,endDate);
	    	ReceiptInvoiceTable = FXCollections.observableArrayList(respentity.getBody());
	    	
	    	Double total=0.0;
	    	for(int i=0;i<ReceiptInvoiceTable.size();i++) {
	    		
	    		total=+ReceiptInvoiceTable.get(i).getAmount();
	    	}
	    	
	    	txtTotal.setText(String.valueOf(total));
		}
    	fillTable();
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }
	   
	   
	   public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}
		}
	   
	   private void showPopup() {

			try {
				System.out.println("inside the popup");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
				Parent root1;
				root1 = (Parent) loader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Accounts");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
			//	cmbModeOfPayment.requestFocus();
//					dpSupplierInvDate.requestFocus();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@Subscribe
		public void popuplistner(AccountEvent accountEvent) {

			System.out.println("------AccountEvent-------popuplistner-------------");
			Stage stage = (Stage) btnShow.getScene().getWindow();
			if (stage.isShowing()) {

				txtAccount.setText(accountEvent.getAccountName());
				accountId=accountEvent.getAccountId();

			}

		}


	    @FXML
	    void deleteData(ActionEvent event) {

	    	

			if (null != voucherNumber) {
				if (null != voucherNumber) {
					
					RestCaller.orgPaymentDelete(voucherNumber);
					notifyMessage(3, "receipt deleted", false);
					show();
				}
			}else {
				notifyMessage(3, "plz select recept entry", false);
			}
	    	
	    	
	    }
}
