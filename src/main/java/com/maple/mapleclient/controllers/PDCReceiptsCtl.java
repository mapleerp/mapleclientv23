package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.PDCReceipts;
import com.maple.mapleclient.entity.PDCReconcileHdr;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.HoldedCustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PDCReceiptsCtl {
	String taskid;
	String processInstanceId;
	
	String accountId = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	PDCReceipts pdcReceipts = null;
	
	private ObservableList<PDCReceipts> receiptList = FXCollections.observableArrayList();


	@FXML
	private TextField txtAccount;

	@FXML
	private TextField txtRemark;

	@FXML
	private DatePicker dpTransDate;

	@FXML
	private ComboBox<String> cmbBank;

	@FXML
	private TextField txtInstNo;

	@FXML
	private DatePicker dpInstDate;

	@FXML
	private TextField txtInstBankName;

	@FXML
	private TextField txtAmount;

	@FXML
	private TableView<PDCReceipts> tblPDC;

	@FXML
	private TableColumn<PDCReceipts, String> clAccount;

	@FXML
	private TableColumn<PDCReceipts, String> clRemark;

	@FXML
	private TableColumn<PDCReceipts, LocalDate> clTransactionDate;

	@FXML
	private TableColumn<PDCReceipts, String> clBank;

	@FXML
	private TableColumn<PDCReceipts, String> clInstNo;

	@FXML
	private TableColumn<PDCReceipts, LocalDate> clInstDate;

	@FXML
	private TableColumn<PDCReceipts, String> clInstBank;

	@FXML
	private TableColumn<PDCReceipts, Number> clAmount;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnShowAll;

	@FXML
	private Button btnDelete;

	@FXML
	private void initialize() {
		eventBus.register(this);
		dpInstDate = SystemSetting.datePickerFormat(dpInstDate, "dd/MMM/yyyy");
		dpTransDate = SystemSetting.datePickerFormat(dpTransDate, "dd/MMM/yyyy");
		txtAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
		
		dpTransDate.setValue(LocalDate.now());
		
		ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid= lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbBank.getItems().add((String) bankName);
				
						    //accountHeads.getAccountName);
			}
		}
		
		
		tblPDC.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					pdcReceipts = new PDCReceipts();
					pdcReceipts.setId(newSelection.getId()); 
					
				}
			}
		});
	}

	@FXML
	void Delete(ActionEvent event) {
		
		if(null != pdcReceipts)
		{
			if(null != pdcReceipts.getId())
			{
				RestCaller.deletePDCReceipts(pdcReceipts.getId());
				notifyMessage(5, "PDC Deleted Successfully...!", true);
				pdcReceipts = null;
				
				showAllReceipts();
			}
				
		}

	}

	@FXML
	void FocusAmount(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();
		}

	}

	@FXML
	void FocusBank(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbBank.requestFocus();
		}
	}

	@FXML
	void FocusInstBank(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtInstBankName.requestFocus();
		}
	}

	@FXML
	void FocusInstDate(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			dpInstDate.requestFocus();
		}

	}

	@FXML
	void FocusInstNo(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtInstNo.requestFocus();
		}

	}

	@FXML
	void FocusRemark(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtRemark.requestFocus();
		}

	}

	@FXML
	void FocusSave(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}

	}

	@FXML
	void FocusTransDate(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			dpTransDate.requestFocus();
		}

	}

	@FXML
	void OnPressDelete(KeyEvent event) {

	}

	@FXML
	void OnPressSave(KeyEvent event) {
		
		if (event.getCode() == KeyCode.ENTER) {
			SavePDCReceipts();
		}


	}

	@FXML
	void SOnPressShowAll(KeyEvent event) {
		
		if (event.getCode() == KeyCode.ENTER) {
			showAllReceipts();
		}
	}

	@FXML
	void Save(ActionEvent event) {
		
		SavePDCReceipts();
	}
	
	private void SavePDCReceipts() {
		
		if (txtAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "please select account...!", false);
			return;
		}

		if (null == dpTransDate.getValue()) {
			notifyMessage(5, "please select transaction date...!", false);
			return;
		}

		if (null == cmbBank.getValue()) {
			notifyMessage(5, "please select banck account...!", false);
			return;
		}

		if (txtInstNo.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter Instrument Number...!", false);
			return;
		}

		if (null == dpInstDate.getValue()) {
			notifyMessage(5, "please select Instrument date", false);
			return;
		}

		if (txtInstBankName.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter Instrument bank...!", false);
			return;
		}

		if (txtAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter Amount...!", false);
			return;
		}

		pdcReceipts = new PDCReceipts();
		
		pdcReceipts.setAccount(accountId);
		
		if(!txtRemark.getText().trim().isEmpty())
		{
			pdcReceipts.setRemark(txtRemark.getText());
		}
		pdcReceipts.setBranchCode(SystemSetting.systemBranch);
		pdcReceipts.setTransDate(Date.valueOf(dpTransDate.getValue()));
		pdcReceipts.setDepositBank(cmbBank.getSelectionModel().getSelectedItem().toString());
		pdcReceipts.setInstrumentnumber(txtInstNo.getText());
		pdcReceipts.setInstrumentDate(Date.valueOf(dpInstDate.getValue()));
		pdcReceipts.setInstrumentBank(txtInstBankName.getText());
		pdcReceipts.setAmount(Double.parseDouble(txtAmount.getText()));
		pdcReceipts.setStatus("PENDING");
		tblPDC.getItems().clear();
		ResponseEntity<PDCReceipts> savedPdcReceipts = RestCaller.SavePDCReceipts(pdcReceipts);
		pdcReceipts = savedPdcReceipts.getBody();
		receiptList.add(pdcReceipts);
		FillTable();
		try {
			
			JasperPdfReportService.PDCReceiptInvoice(receiptList);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		notifyMessage(5, "PDC Added Successfully...!", true);
		
		pdcReceipts = null;
		txtAccount.clear();
		txtAmount.clear();
		txtInstBankName.clear();
		txtInstNo.clear();
		txtRemark.clear();
		dpInstDate.setValue(null);
		dpTransDate.setValue(LocalDate.now());
		cmbBank.getSelectionModel().clearSelection();

		
	}

	private void FillTable() {
		

		for(PDCReceipts pdc : receiptList)
		{
			if(null != pdc.getAccount())
			{
				ResponseEntity<AccountHeads> accountHeadsresp = RestCaller.getAccountHeadsById(pdc.getAccount());
				AccountHeads accountHeads = accountHeadsresp.getBody();
				
				pdc.setAccountName(accountHeads.getAccountName());

			}
		}
		
		tblPDC.setItems(receiptList);
		
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

		clRemark.setCellValueFactory(cellData -> cellData.getValue().getRemarkeProperty());
		clTransactionDate.setCellValueFactory(cellData -> cellData.getValue().getTransDateProperty());
		clBank.setCellValueFactory(cellData -> cellData.getValue().getBankProperty());
		clInstNo.setCellValueFactory(cellData -> cellData.getValue().getInstNoProperty());
		clInstDate.setCellValueFactory(cellData -> cellData.getValue().getInstDateProperty());
		clInstBank.setCellValueFactory(cellData -> cellData.getValue().getInstBankProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		
	}

	@FXML
	void ShowAll(ActionEvent event) {
		
		showAllReceipts();

	}
	
	private void showAllReceipts() {
		
		tblPDC.getItems().clear();
		if(null == dpTransDate.getValue())
		{
			notifyMessage(5, "please select transaction date...!", false);
			return;
		}
		
		tblPDC.getItems().clear();
		String date = SystemSetting.SqlDateTostring(Date.valueOf(dpTransDate.getValue()));
		ResponseEntity<List<PDCReceipts>> pdcReceiptsList = RestCaller.getPDCReceiptsByDate(date);
		receiptList = FXCollections.observableArrayList(pdcReceiptsList.getBody());
		
		FillTable();
	}

	@FXML
	void AccountPopup(MouseEvent event) {

		showPopup();

	}

	private void showPopup() {

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtRemark.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(customerEvent.getCustomerName());
			accountId = customerEvent.getCustId();

		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }

}
