package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ParamMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.events.ParamValueEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ParamValueConfigCtl {
	 String paramId;
	String taskid;
	String processInstanceId;
	ParamValueConfig paramValueConfig = null;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ParamValueConfig> paramValueConfigList = FXCollections.observableArrayList();

	@FXML
	private TextField txtParameter;

	@FXML
	private TextField txtValue;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnShowAll;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnInitialize;

	@FXML
	private TableView<ParamValueConfig> tblParamValue;

	@FXML
	private TableColumn<ParamValueConfig, String> clParamete;

	@FXML
	private TableColumn<ParamValueConfig, String> clValue;

	// -------------------2021-05-1 Anandhu
	 @FXML
	    void paramOnAction(ActionEvent event) {
		 LoadParamPopup();
	    }
	 private void LoadParamPopup()
		{
			System.out.println("-------------showPopup-------------");

			try {
				System.out.println("inside the popup");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ParamNamePopup.fxml"));
				Parent root = loader.load();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
			

			} catch (Exception e) {
				e.printStackTrace();
			}
			txtParameter.requestFocus();
		
			
		}
	@FXML
	void initializeOnAction(ActionEvent event) {
		ParamMstInitailize();
		
		
	}

	private void ParamMstInitailize() {

		ParamMst paramMst = new ParamMst();
		ResponseEntity<ParamMst> paramMstReap = RestCaller.ParamMstByName("WHOLESALE_RATE_EDIT");

		paramMst = paramMstReap.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("WHOLESALE_RATE_EDIT");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}

		ResponseEntity<ParamMst> paramMstReap1 = RestCaller.ParamMstByName("HODACCOUNT");

		paramMst = paramMstReap1.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("HODACCOUNT");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap2 = RestCaller.ParamMstByName("PosDiscount");

		paramMst = paramMstReap2.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("PosDiscount");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap3 = RestCaller.ParamMstByName("BARCODEFORMAT");

		paramMst = paramMstReap3.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("BARCODEFORMAT");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap4 = RestCaller.ParamMstByName("BARCODE-IN-EXCEL");

		paramMst = paramMstReap4.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("BARCODE-IN-EXCEL");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap5 = RestCaller.ParamMstByName("DAY_END_LOCKED_IN_WHOLESALE");

		paramMst = paramMstReap5.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("DAY_END_LOCKED_IN_WHOLESALE");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap6 = RestCaller.ParamMstByName("CUSTOMER_EDIT");

		paramMst = paramMstReap6.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("CUSTOMER_EDIT");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap7 = RestCaller.ParamMstByName("DAY_END_LOCKED_IN_WHOLESALE");

		paramMst = paramMstReap7.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("DAY_END_LOCKED_IN_WHOLESALE");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap8 = RestCaller.ParamMstByName("DAY END");

		paramMst = paramMstReap8.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("DAY END");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap9 = RestCaller.ParamMstByName("WHOLESALE_RATE_EDIT");

		paramMst = paramMstReap9.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("WHOLESALE_RATE_EDIT");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap10 = RestCaller.ParamMstByName("RETAIL_RATE_EDIT");

		paramMst = paramMstReap10.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("RETAIL_RATE_EDIT");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap11 = RestCaller.ParamMstByName("MAPLE_LOYALITY_CUSTOMER");

		paramMst = paramMstReap11.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("MAPLE_LOYALITY_CUSTOMER");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap12 = RestCaller.ParamMstByName("LOYALTYENABLED");

		paramMst = paramMstReap12.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("LOYALTYENABLED");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap13 = RestCaller.ParamMstByName("PRINTBARCODEINPURCHASE");

		paramMst = paramMstReap13.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("PRINTBARCODEINPURCHASE");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap14 = RestCaller.ParamMstByName("TakeOrderPrintCopies");

		paramMst = paramMstReap14.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("TakeOrderPrintCopies");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap15 = RestCaller.ParamMstByName("API-Key");

		paramMst = paramMstReap15.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("API-Key");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap16 = RestCaller.ParamMstByName("Hash-Key");

		paramMst = paramMstReap16.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("Hash-Key");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		ResponseEntity<ParamMst> paramMstReap17 = RestCaller.ParamMstByName("KOTPRINTSPLIT");

		paramMst = paramMstReap17.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("KOTPRINTSPLIT");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		
		ResponseEntity<ParamMst> paramMstReap18 = RestCaller.ParamMstByName("BARCODE_DEFAULT_QTY");

		paramMst = paramMstReap18.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("BARCODE_DEFAULT_QTY");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		ResponseEntity<ParamMst> paramMstReap19 = RestCaller.ParamMstByName("GODOWN_MANAGE");

		paramMst = paramMstReap19.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("GODOWN_MANAGE");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		//====== for the purpose of giving permissioin to offline item creation =====by anandu=====01-07-2021====
		ResponseEntity<ParamMst> paramMstReap20 = RestCaller.ParamMstByName("OFFLINE_ITEM_CREATION");

		paramMst = paramMstReap20.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("OFFLINE_ITEM_CREATION");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		//====== for the purpose of giving permissioin to offline customer creation =====by anandu=====01-07-2021====
		ResponseEntity<ParamMst> paramMstReap21 = RestCaller.ParamMstByName("OFFLINE_CUSTOMER_CREATION");

		paramMst = paramMstReap21.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("OFFLINE_CUSTOMER_CREATION");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		//====== for the purpose of giving permissioin to offline supplier creation =====by anandu=====01-07-2021====
		ResponseEntity<ParamMst> paramMstReap22 = RestCaller.ParamMstByName("OFFLINE_SUPPLIER_CREATION");

		paramMst = paramMstReap22.getBody();

		if (null == paramMst) {
			paramMst = new ParamMst();
			paramMst.setParamName("OFFLINE_SUPPLIER_CREATION");

			ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
		}
		
		//====== for the purpose of giving permissioin to offline kit creation =====by anandu=====01-07-2021====
				ResponseEntity<ParamMst> paramMstReap23 = RestCaller.ParamMstByName("OFFLINE_KIT_CREATION");

				paramMst = paramMstReap23.getBody();

				if (null == paramMst) {
					paramMst = new ParamMst();
					paramMst.setParamName("OFFLINE_KIT_CREATION");

					ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
				}
		
				
		//====== for the purpose of giving permissioin to offline category creation =====by anandu=====01-07-2021====
				ResponseEntity<ParamMst> paramMstReap24 = RestCaller.ParamMstByName("OFFLINE_CATEGORY_CREATION");

				paramMst = paramMstReap24.getBody();

				if (null == paramMst) {
					paramMst = new ParamMst();
					paramMst.setParamName("OFFLINE_CATEGORY_CREATION");

					ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
				}	
				
				
		//====== for the purpose of giving permissioin to offline unit creation =====by anandu=====01-07-2021====
				ResponseEntity<ParamMst> paramMstReap25 = RestCaller.ParamMstByName("OFFLINE_UNIT_CREATION");

				paramMst = paramMstReap25.getBody();

				if (null == paramMst) {
					paramMst = new ParamMst();
					paramMst.setParamName("OFFLINE_UNIT_CREATION");

					ResponseEntity<ParamMst> paramSaved = RestCaller.createParamMst(paramMst);
				}	
				
				
				
		
		notifyMessage(3, "successfully Initialized", true);
		
	}


	@FXML
	private void initialize() {
		eventBus.register(this);
		FillTable();

		tblParamValue.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					paramValueConfig = new ParamValueConfig();
					paramValueConfig.setId(newSelection.getId());
				}
			}
		});

	}

	@FXML
	void Delete(ActionEvent event) {

		if (null != paramValueConfig) {
			if (null != paramValueConfig.getId()) {
				RestCaller.deleteParamValueConfigById(paramValueConfig.getId());
				FillTable();
			}
		}

	}

	@FXML
	void Save(ActionEvent event) {

		if (txtParameter.getText().isEmpty()) {
			notifyMessage(3, "Please Enter Parameter ", false);
			txtParameter.requestFocus();
			return;
		}

		if (txtValue.getText().isEmpty()) {
			notifyMessage(3, "Please Enter Value ", false);
			txtValue.requestFocus();
			return;
		}

		paramValueConfig = new ParamValueConfig();

		String id = RestCaller.getVoucherNumber(SystemSetting.getUser().getBranchCode());

		paramValueConfig.setId(id);
		paramValueConfig.setBranchCode(SystemSetting.getUser().getBranchCode());
		paramValueConfig.setParam(txtParameter.getText());
		paramValueConfig.setValue(txtValue.getText());

		ResponseEntity<ParamValueConfig> paramValueConfigResp = RestCaller.saveParamValueConfig(paramValueConfig);
		paramValueConfig = paramValueConfigResp.getBody();

		FillTable();
		paramValueConfig = null;
		txtParameter.clear();
		txtValue.clear();
	}

	private void FillTable() {

		ResponseEntity<List<ParamValueConfig>> paramValueListResp = RestCaller.getAllParamValueConfig();
		paramValueConfigList = FXCollections.observableArrayList(paramValueListResp.getBody());

		tblParamValue.setItems(paramValueConfigList);

		clParamete.setCellValueFactory(cellData -> cellData.getValue().getParamProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());

	}

	@FXML
	void ShowAll(ActionEvent event) {

		FillTable();

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
	
	
	@Subscribe
	public void paramPopuplistner(ParamValueEvent paramValueEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			
			
					Platform.runLater(() -> {
						txtParameter.setText(paramValueEvent.getParamName()); 
						paramId=paramValueEvent.getParamId();
		            });
		
			
				}
			});
			
			
		}
	}

	
    @FXML
    void paramPressed(KeyEvent event) {
    	LoadParamPopup();
    }
	    @FXML
	    void parameterClicked(MouseEvent event) {
	    	
				LoadParamPopup();

			
	    }
}
