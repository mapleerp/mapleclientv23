package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountPayable;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.events.PaymentInvPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class PaymentInvcDetailCtl {
	String taskid;
	String processInstanceId;


	 private ObservableList<AccountPayable> invoiceList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();
	PaymentInvPopupEvent paymentInvPopupEvent;
	 

    @FXML
    private TextField txtOwnAccount;
    @FXML
    private TableView<AccountPayable> tbInvPopUp;

    @FXML
    private TableColumn<AccountPayable, String> clInvNo;

    @FXML
    private TableColumn<AccountPayable, Number> clDueAmt;

    @FXML
    private TableColumn<AccountPayable, Number> clPaidAmt;

    @FXML
    private TableColumn<AccountPayable, Number> clBalanceAmt;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;

    @FXML
    void Cancel(ActionEvent event) {
    	
    	Stage stage = (Stage) btnCancel.getScene().getWindow();
		stage.close();

    }

    @FXML
    void Ok(ActionEvent event) {
    	
    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	
    	eventBus.post(paymentInvPopupEvent);
		stage.close();

    }
    
    @FXML
   	private void initialize() {
    	
    	paymentInvPopupEvent = new PaymentInvPopupEvent();
    	eventBus.register(this);
    	tbInvPopUp.setItems(invoiceList);
    	clDueAmt.setCellValueFactory(cellData -> cellData.getValue().getDueAmountProperty());
    	clInvNo.setCellValueFactory(cellData -> cellData.getValue().getInvoiceNumberProperty());
    	clPaidAmt.setCellValueFactory(cellData -> cellData.getValue().getPaidAmountProperty());
    	clBalanceAmt.setCellValueFactory(cellData -> cellData.getValue().getBalanceAmountProperty());
    	
    	tbInvPopUp.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
   		 if (newSelection != null) {
			    	if(null!=newSelection.getId() ){
			    		
			    		paymentInvPopupEvent.setBalanceAmount(newSelection.getBalanceAmount());
			    		paymentInvPopupEvent.setDueAmount(newSelection.getDueAmount());
			    		paymentInvPopupEvent.setInvoiceNumber(newSelection.getVoucherNumber());
			    		paymentInvPopupEvent.setPaidAmount(newSelection.getPaidAmount());;
//			    		paymentInvPopupEvent.setInvoiceDate(newSelection.getVoucherDate());
			    		eventBus.post(paymentInvPopupEvent);
			    		
			    	}
			    }
			});

    }
    
    public void LoadAccountPayable(String supid) {
    
    	
    	ArrayList stockin = new ArrayList();
    	invoiceList.clear();
    	tbInvPopUp.getItems().clear();
    	stockin = RestCaller.getAccountPayable(supid);
    	double ownAccAmt = 0.0;
    	ResponseEntity<AccountHeads> accountHeadsSaved = RestCaller.getAccountHeadsById(supid);
		
    	ownAccAmt = RestCaller.getOwnAccountBySupId(accountHeadsSaved.getBody().getId());
    	
    	txtOwnAccount.setText(Double.toString(ownAccAmt));
    	
    	
    	Iterator itr = stockin.iterator();
    	while (itr.hasNext()) {

    		 LinkedHashMap element = (LinkedHashMap) itr.next();
    		 Object id = (String) element.get("id");
    		 Object voucherNumber = (String) element.get("voucherNumber");
//    		 Object voucherDate = (String)element.get("voucherDate");
//    		 Object dueDate=(String)element.get("dueDate");
    		 Object dueAmount=(Double)element.get("dueAmount");
      		 Object paidAmount=(Double)element.get("paidAmount");
      		
    		 if (null != id) {
    			 
    			AccountPayable ack = new AccountPayable();
    			ack.setId((String) id);
    			ack.setVoucherNumber((String)voucherNumber);
//    			ack.setVoucherDate((Date)voucherDate);
//    			ack.setDueDate((Date)dueDate);
    			ack.setDueAmount((Double)dueAmount);
    			ack.setPaidAmount((Double)paidAmount);
    			ack.setBalanceAmount(ack.getDueAmount() - ack.getPaidAmount());
    			 invoiceList.add(ack);
    			 tbInvPopUp.setItems(invoiceList);
    		 }
    	}
    	return;
    
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }
}
