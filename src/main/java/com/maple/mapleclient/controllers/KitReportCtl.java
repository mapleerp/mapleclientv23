package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;
import javafx.scene.control.TextField;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
public class KitReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<KitDefinitionMst>kitTable = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	KitDefinitionMst kitDefMst = null;
    @FXML
    private Button btnShowAll;

    @FXML
    private TextField txtItemName;
    @FXML
    private TableView<KitDefinitionMst> tbKitMst;

    @FXML
    private TableColumn<KitDefinitionMst, String> clKitName;

    @FXML
    private TableColumn<KitDefinitionMst, String> clUnit;

    @FXML
    private TableColumn<KitDefinitionMst, Number> clMinQty;

    @FXML
    private TableColumn<KitDefinitionMst, Number> clCost;
    

    @FXML
    private Button btnDeleteKit;

    @FXML
    void actionDeleteKit(ActionEvent event) {

    	if(null == kitDefMst)
    	{
    		return;
    	}
    	if(null == kitDefMst.getId())
    	{
    		return;
    	}
    	RestCaller.deletekiTDefinitionMstById(kitDefMst.getId());
    	ResponseEntity<List<KitDefinitionMst>>  getKit = RestCaller.getAllKit();
    	kitTable =  FXCollections.observableArrayList(getKit.getBody());
    	fillTable();
    	
    }
    @FXML
	private void initialize() {
		txtItemName.textProperty().bindBidirectional(SearchString);
		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				System.out.println("Supplier Search changed");
				LoadItemBySearch((String) newValue);
			}
		});
		
		tbKitMst.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					kitDefMst = new KitDefinitionMst();
					kitDefMst.setId(newSelection.getId());
				}
			}
		});
    }
    private void LoadItemBySearch(String itemName)
    {
    	ResponseEntity<List<KitDefinitionMst>>  getKit = RestCaller.getAllKitByName(itemName);
    	kitTable =  FXCollections.observableArrayList(getKit.getBody());
    	fillTable();
    }
    @FXML
    void actionShowAll(ActionEvent event) {

    	ResponseEntity<List<KitDefinitionMst>>  getKit = RestCaller.getAllKit();
    	kitTable =  FXCollections.observableArrayList(getKit.getBody());
    	fillTable();
    }

    private void fillTable()
    {
    	for(KitDefinitionMst kit : kitTable)
    	{
    		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(kit.getItemId());
    		if(null != getItem.getBody())
    		kit.setKitName(getItem.getBody().getItemName());
    		try
    		{
    		ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(kit.getUnitId());
    		kit.setUnitName(getUnit.getBody().getUnitName());
    		}
    		catch (Exception e) {
				// TODO: handle exception
			}
    	}
    	tbKitMst.setItems(kitTable);
		clKitName.setCellValueFactory(cellData -> cellData.getValue().getkitnameProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
		clMinQty.setCellValueFactory(cellData -> cellData.getValue().getminQtyProperty());
		clCost.setCellValueFactory(cellData -> cellData.getValue().getprodCostProperty());




    }
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
