package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.PatientMst;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.events.PatientEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class PatientPopUpCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PatientMst> popUpItemList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	boolean initializedCalled = false;
	PatientEvent patientEvent =null;
    @FXML
    private TextField txtPatientName;

    @FXML
    private TableView<PatientMst> tbPatient;

    @FXML
    private TableColumn<PatientMst, String> clPatient;

    @FXML
    private TableColumn<PatientMst, String> clAddress;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
    @FXML
	private void initialize() {
		if (initializedCalled)
			return;

		initializedCalled = true;
		patientEvent = new PatientEvent();
		btnOk.setDefaultButton(true);

		btnCancel.setCache(true);
		txtPatientName.textProperty().bindBidirectional(SearchString);
		LoadItemPopupBySearch("");

		tbPatient.setItems(popUpItemList);
		clAddress.setCellValueFactory(cellData -> cellData.getValue().getaddressProperty());
		clPatient.setCellValueFactory(cellData -> cellData.getValue().getpatientNameProperty());
		tbPatient.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId() && newSelection.getPatientName().length() > 0) {
					if (null != newSelection.getId()) {
						patientEvent.setId(newSelection.getId());
						}
					if(null != newSelection.getAddress1())
					{
						patientEvent.setAddress1(newSelection.getAddress1());
					}
					if(null != newSelection.getAddress2())
					{
						patientEvent.setAddress2(newSelection.getAddress2());
					}
					if(null != newSelection.getContactPerson())
					{
						patientEvent.setContactPerson(newSelection.getContactPerson());
					}
					if(null != newSelection.getDateOfBirth())
					{
						patientEvent.setDateOfBirth(newSelection.getDateOfBirth());
					}
					if(null != newSelection.getGender())
					{
						patientEvent.setGender(newSelection.getGender());
					}
					if(null!= newSelection.getPolicyType())
					{
						patientEvent.setPolicyType(newSelection.getPolicyType());
					}
					if(null != newSelection.getPhoneNumber())
					{
						patientEvent.setPhoneNumber(newSelection.getPhoneNumber());
					}
					if(null != newSelection.getPercentageCoverage())
					{
						patientEvent.setPercentageCoverage(newSelection.getPercentageCoverage());
					}
					if(null != newSelection.getPatientName())
					{
						patientEvent.setPatientName(newSelection.getPatientName());
					}
					if(null != newSelection.getNationality())
					{
						patientEvent.setNationality(newSelection.getNationality());
					}
					if(null != newSelection.getNationalId())
					{
						patientEvent.setNationalId(newSelection.getNationalId());
					}
					if(null != newSelection.getInsuranceCompanyName())
					{
						patientEvent.setInsuranceCompanyName(newSelection.getInsuranceCompanyName());
					}
					if(null != newSelection.getInsuranceCompanyId())
					{
						patientEvent.setInsuranceCompanyId(newSelection.getInsuranceCompanyId());
					}
					if(null != newSelection.getInsuranceCardExpiry())
					{
						patientEvent.setInsuranceCardExpiry(newSelection.getInsuranceCardExpiry());
					}
					if(null != newSelection.getInsuranceCard())
					{
						patientEvent.setInsuranceCard(newSelection.getInsuranceCard());
					}
					
				}
				}
			});
		
//		SearchString.addListener(new ChangeListener() {
//
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
//				LoadItemPopupBySearch((String) newValue);
//			}
//		});
    }
    @FXML
    void OnKeyPress(KeyEvent event) {


		if (event.getCode() == KeyCode.ENTER) {
			eventBus.post(patientEvent);
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		} else {
			txtPatientName.requestFocus();
		}

		initializedCalled = false;

	
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {


		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tbPatient.requestFocus();
			tbPatient.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			patientEvent.setPatientName(null);
			eventBus.post(patientEvent);
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
			
		}
		if(event.getCode() == KeyCode.BACK_SPACE && txtPatientName.getText().length()==0)
		{
			patientEvent.setPatientName(null);
			eventBus.post(patientEvent);
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
			
		}
	

    }
    @FXML
    void tableOnClick(MouseEvent event) {
    	if (tbPatient.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tbPatient.getSelectionModel();
			popUpItemList = selectionModel.getSelectedItems();
			for (PatientMst stk : popUpItemList) {
				txtPatientName.setText(stk.getPatientName());
			}
    	}
    	
    

    }
	@FXML
    void actionCancel(ActionEvent event) {


		Stage stage = (Stage) btnOk.getScene().getWindow();
		patientEvent.setPatientName(null);
		eventBus.post(patientEvent);
		stage.close();
    }

    @FXML
    void actionOk(ActionEvent event) {

		Stage stage = (Stage) btnOk.getScene().getWindow();
		eventBus.post(patientEvent);
		stage.close();
    }
    private void LoadItemPopupBySearch(String searchData) {

		tbPatient.getItems().clear();
		popUpItemList.clear();

		ResponseEntity<List<PatientMst>> patient = RestCaller.searchPatientByName(searchData);
		popUpItemList = FXCollections.observableArrayList(patient.getBody());
		tbPatient.setItems(popUpItemList);
  		
  	}
    
    
	public void PatientPopUpWithCustomer(String customerid) {

		popUpItemList.clear();
		ArrayList patientArray = new ArrayList();
		patientArray = RestCaller.getPatientWithCustomer(customerid);
		Iterator itr = patientArray.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object patient = lm.get("patientName");
			Object id = lm.get("id");
			if (null != id) {
				PatientMst patientmst = new PatientMst();
				
				patientmst.setPatientName((String) patient);
				patientmst.setId((String) id);
				popUpItemList.add(patientmst);
			}

		}
		
	}
    
    
    
    
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }
}
