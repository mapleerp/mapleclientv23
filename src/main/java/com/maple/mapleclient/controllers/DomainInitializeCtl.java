package com.maple.mapleclient.controllers;

import org.controlsfx.control.Notifications;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CompanyMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class DomainInitializeCtl {
	
	  @FXML
	    private Button btnResturant;

	    @FXML
	    private Button btnPharmacy;

	    @FXML
	    private Button btnHardware;

	    @FXML
	    void HardwareInitialize(ActionEvent event) {

	    }
	    
	    @FXML
	    void PhaarmacyInitialize(ActionEvent event) {
	    	
	    	ResponseEntity<CompanyMst> companyMstResp = RestCaller.getCompanyMst(SystemSetting.myCompany);
	    	CompanyMst companyMst = companyMstResp.getBody();
	    	
			ResponseEntity<AccountHeads> accountHeadsResponse8 = RestCaller.getAccountHeadsByNameAndBranchCode("PHARMACYRETAIL",
					SystemSetting.getSystemBranch());
			boolean createCustomerRetailer = true;
			if (null != accountHeadsResponse8) {
				AccountHeads accountHeads = accountHeadsResponse8.getBody();

				if (null != accountHeads) {
					createCustomerRetailer = false;
				} else {
					createCustomerRetailer = true;
				}
			} else {
				createCustomerRetailer = true;
			}

			if (createCustomerRetailer) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setPartyAddress1("SYSTEM");
				accountHeads.setAccountName("PHARMACYRETAIL");
				accountHeads.setId("SY00K8" + SystemSetting.getUser().getCompanyMst().getCompanyName()
						+ SystemSetting.systemBranch);
				accountHeads.setCustomerRank(0);
				accountHeads.setCustomerState(companyMst.getState());
				RestCaller.partyRegistration(accountHeads);

			}
			
			ResponseEntity<String> insuranceCompanyMst = RestCaller.getInitializeInsuranceCompanyMst();
			
			ResponseEntity<String> localCustomer=RestCaller.InitializeLocalCustomer();
			ResponseEntity<String> corporateSale=RestCaller.InitializeCorporateSale(SystemSetting.getSystemBranch());
	    	
			notifyMessage(5, "Pharmacy Initailized!!");
	    	
	    

	    }

	    @FXML
	    void ResturantInitialize(ActionEvent event) {
	    	
			ResponseEntity<AccountHeads> accountHeadsResponse = RestCaller.getAccountHeadsByNameAndBranchCode("POS",
					SystemSetting.getSystemBranch());
			boolean createCustomer = true;
			if (null != accountHeadsResponse) {
				AccountHeads accountHeads = accountHeadsResponse.getBody();

				if (null != accountHeads) {
					createCustomer = false;
				} else {
					createCustomer = true;
				}
			} else {
				createCustomer = true;
			}

			if (createCustomer) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setPartyAddress1("SYSTEM");
				accountHeads.setAccountName("POS"+"_"+SystemSetting.systemBranch);
				accountHeads.setId("SY014" + SystemSetting.getUser().getCompanyMst().getCompanyName()
						+ SystemSetting.systemBranch);
				accountHeads.setCustomerRank(0);

				RestCaller.partyRegistration(accountHeads);

			}

			ResponseEntity<AccountHeads> accountHeadsResponse2 = RestCaller.getAccountHeadsByNameAndBranchCode("UBER SALES",
					SystemSetting.getSystemBranch());
			boolean createCustomerUber = true;
			if (null != accountHeadsResponse2) {
				AccountHeads accountHeads = accountHeadsResponse2.getBody();

				if (null != accountHeads) {
					createCustomerUber = false;
				} else {
					createCustomerUber = true;
				}
			} else {
				createCustomerUber = true;
			}

			if (createCustomerUber) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setPartyAddress1("SYSTEM");
				accountHeads.setAccountName("UBER SALES");
				accountHeads.setId("SY012" + SystemSetting.getUser().getCompanyMst().getCompanyName()
						+ SystemSetting.systemBranch);
				accountHeads.setCustomerRank(0);
				RestCaller.partyRegistration(accountHeads);

				
			}
			
			ResponseEntity<AccountHeads> accountHeadsResponse3 = RestCaller.getAccountHeadsByNameAndBranchCode("SWIGGY",
					SystemSetting.getSystemBranch());
			boolean createCustomerSwiggy = true;
			if (null != accountHeadsResponse3) {
				AccountHeads accountHeads = accountHeadsResponse3.getBody();

				if (null != accountHeads) {
					createCustomerSwiggy = false;
				} else {
					createCustomerSwiggy = true;
				}
			} else {
				createCustomerSwiggy = true;
			}

			if (createCustomerSwiggy) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setPartyAddress1("SYSTEM");
				accountHeads.setAccountName("SWIGGY");
				accountHeads.setId("SY008" + SystemSetting.getUser().getCompanyMst().getCompanyName()
						+ SystemSetting.systemBranch);
				accountHeads.setCustomerRank(0);
				RestCaller.partyRegistration(accountHeads);

			}

			ResponseEntity<AccountHeads> accountHeadsResponse4 = RestCaller.getAccountHeadsByNameAndBranchCode("ZOMATO",
					SystemSetting.getSystemBranch());
			boolean createCustomerZomato = true;
			if (null != accountHeadsResponse4) {
				AccountHeads accountHeads = accountHeadsResponse4.getBody();

				if (null != accountHeads) {
					createCustomerZomato = false;
				} else {
					createCustomerZomato = true;
				}
			} else {
				createCustomerZomato = true;
			}

			if (createCustomerZomato) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setPartyAddress1("SYSTEM");
				accountHeads.setAccountName("ZOMATO");
				accountHeads.setId("SY017" + SystemSetting.getUser().getCompanyMst().getCompanyName()
						+ SystemSetting.systemBranch);
				accountHeads.setCustomerRank(0);
				RestCaller.partyRegistration(accountHeads);

			}
			

			ResponseEntity<AccountHeads> accountHeadsResponse5 = RestCaller.getAccountHeadsByNameAndBranchCode("FOOD PANDA",
					SystemSetting.getSystemBranch());
			boolean createCustomerFood = true;
			if (null != accountHeadsResponse5) {
				AccountHeads accountHeads = accountHeadsResponse5.getBody();

				if (null != accountHeads) {
					createCustomerFood = false;
				} else {
					createCustomerFood = true;
				}
			} else {
				createCustomerFood = true;
			}

			if (createCustomerFood) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setPartyAddress1("SYSTEM");
				accountHeads.setAccountName("FOOD PANDA");
				accountHeads.setId("SY013" + SystemSetting.getUser().getCompanyMst().getCompanyName()
						+ SystemSetting.systemBranch);
				accountHeads.setCustomerRank(0);
				RestCaller.partyRegistration(accountHeads);

			}
			

			ResponseEntity<AccountHeads> accountHeadsResponse6 = RestCaller.getAccountHeadsByNameAndBranchCode("ONLINE",
					SystemSetting.getSystemBranch());
			boolean createCustomerOnline = true;
			if (null != accountHeadsResponse6) {
				AccountHeads accountHeads = accountHeadsResponse6.getBody();

				if (null != accountHeads) {
					createCustomerOnline = false;
				} else {
					createCustomerOnline = true;
				}
			} else {
				createCustomerOnline = true;
			}

			if (createCustomerOnline) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setPartyAddress1("SYSTEM");
				accountHeads.setAccountName("ONLINE");
				accountHeads.setId("SY007" + SystemSetting.getUser().getCompanyMst().getCompanyName()
						+ SystemSetting.systemBranch);
				accountHeads.setCustomerRank(0);
				RestCaller.partyRegistration(accountHeads);

			}
			
			ResponseEntity<AccountHeads> accountHeadsResponse7 = RestCaller.getAccountHeadsByNameAndBranchCode("KOT",
					SystemSetting.getSystemBranch());
			boolean createCustomerKOT = true;
			if (null != accountHeadsResponse7) {
				AccountHeads accountHeads = accountHeadsResponse7.getBody();

				if (null != accountHeads) {
					createCustomerKOT = false;
				} else {
					createCustomerKOT = true;
				}
			} else {
				createCustomerKOT = true;
			}

			if (createCustomerKOT) {
				AccountHeads accountHeads = new AccountHeads();
				accountHeads.setPartyAddress1("SYSTEM");
				accountHeads.setAccountName("KOT");
				accountHeads.setId("SY00K7" + SystemSetting.getUser().getCompanyMst().getCompanyName()
						+ SystemSetting.systemBranch);
				accountHeads.setCustomerRank(0);
				RestCaller.partyRegistration(accountHeads);

			}
	    	
			notifyMessage(5, "Resturant Initailized!!");

	    }
	    
	    
	    public void notifyMessage(int duration, String msg) {
			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();

			notificationBuilder.show();
		}


}
