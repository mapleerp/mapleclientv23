package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BranchMst;

import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.SalesTypeMst;
import com.maple.mapleclient.entity.SubBranchSalesDtl;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.SubBranchSaleEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;
import javafx.scene.control.DatePicker;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;
import net.sf.jasperreports.engine.JRException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class SubBranchSaleCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<SalesDtl> salesDtlTableList = FXCollections.observableArrayList();

	private ObservableList<SalesTypeMst> saleTypeTable = FXCollections.observableArrayList();

	SalesDtl salesDtl = null;
	SalesTransHdr salesTransHdr = null;
	UnitMst unitMst = null;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnRate;
	@FXML
	private TableColumn<SalesDtl, String> columnMrp;
	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TextField txtSBICard;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private TextField txtSodexoCard;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtYesCard;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField txtDiscount;

	@FXML
	private TextField txtDiscountAmt;

	@FXML
	private TextField txtAmtAftrDiscount;

	@FXML
	private TextField custname;

	@FXML
	private TextField custAdress;

	@FXML
	private TextField gstNo;

	@FXML
	private TextField txtPriceType;

	@FXML
	private ComboBox<String> cmbSaleType;

	@FXML
	private TextField txtLoginDate;

	@FXML
	private TextField txtLocalCustomer;

	@FXML
	private TextField txtSalesType;

	@FXML
	private Label lblAmount;

	@FXML
	private Button btnFetchItems;
	

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

	@FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		eventBus.register(this);

		ResponseEntity<List<SalesTypeMst>> salesTypeSaved = RestCaller.getSalesTypeMst();
		if (null == salesTypeSaved.getBody()) {
			notifyMessage(5, "Please Add Voucher Type", false);
			return;
		}
		saleTypeTable = FXCollections.observableArrayList(salesTypeSaved.getBody());

		for (SalesTypeMst salesType : saleTypeTable) {
			cmbSaleType.getItems().add(salesType.getSalesType());

		}
	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

	}

	@FXML
	void FetchItems(ActionEvent event) {

		if (null == custname.getText()) {
			notifyMessage(3, "Please select spencer", false);
			return;
		}
		
		if(null == dpToDate.getValue())
		{
			notifyMessage(3, "Please select to date", false);
			return;
		}
		
		if(null == dpFromDate.getValue())
		{
			notifyMessage(3, "Please select from date", false);
			return;
		}
		ResponseEntity<BranchMst> CustResp = RestCaller.getBranchMstByName(custname.getText());
		BranchMst branch = CustResp.getBody();
		
		Date udate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String edate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		
		Date ufdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String sdate = SystemSetting.UtilDateToString(ufdate, "yyyy-MM-dd");
		
		ResponseEntity<List<SubBranchSalesDtl>> spancerDtlListResp = RestCaller
				.findSpneserDtlBySpenserIdAndDate(branch.getBranchCode(), sdate, edate);

		List<SubBranchSalesDtl> spancerDtlList = spancerDtlListResp.getBody();

		if (null == salesTransHdr) {

			salesTransHdr = new SalesTransHdr();
			/*
			 * new url for getting account heads instead of customer mst=========05/0/2022
			 */
//			ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerById(branch.getId());
			ResponseEntity<AccountHeads> accountHeadsResponse=RestCaller.getAccountHeadsById(branch.getId());
			AccountHeads accountHeads = accountHeadsResponse.getBody();
			System.out.println("Customer"+accountHeads);
			salesTransHdr.setInvoiceAmount(0.0);
			salesTransHdr.getId();
			salesTransHdr.setVoucherType(cmbSaleType.getSelectionModel().getSelectedItem());
			salesTransHdr.setCustomerId(accountHeads.getId());
			if (null == accountHeads) {
				return;
			}
			salesTransHdr.setAccountHeads(accountHeads);

			if (null != accountHeads.getCustomerDiscount()) {
				salesTransHdr.setDiscount((accountHeads.getCustomerDiscount().toString()));
			} else {
				salesTransHdr.setDiscount("0");
			}

			if (null == accountHeads.getPartyGst() || accountHeads.getPartyGst().length() < 13) {
				salesTransHdr.setSalesMode("B2C");
			} else {
				salesTransHdr.setSalesMode("B2B");
			}

			salesTransHdr.setCreditOrCash("CREDIT");
			salesTransHdr.setUserId(SystemSetting.getUser().getId());
			salesTransHdr.setBranchCode(SystemSetting.systemBranch);
			salesTransHdr.setIsBranchSales("Y");

			ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
			salesTransHdr = respentity.getBody();
		}

		for (SubBranchSalesDtl spencer : spancerDtlList) {

			salesDtl = new SalesDtl();
			salesDtl.setSalesTransHdr(salesTransHdr);
			salesDtl.setBatchCode(spencer.getBatch());
			salesDtl.setQty(spencer.getQty());
			salesDtl.setItemId(spencer.getItemId());
			salesDtl.setUnitId(spencer.getUnitId());
			ResponseEntity<ItemMst> respsentity = RestCaller.getitemMst(spencer.getItemId());
			ItemMst itemMst = respsentity.getBody();
			salesDtl.setBarcode(itemMst.getBarCode());
			salesDtl.setStandardPrice(itemMst.getStandardPrice());
			salesDtl.setMrp(itemMst.getStandardPrice());
			Double mrpRateIncludingTax = itemMst.getStandardPrice();
			Double taxRate = itemMst.getTaxRate();
			ResponseEntity<UnitMst> unitResp = RestCaller.getunitMst(spencer.getUnitId());
			UnitMst unitMst = unitResp.getBody();
			salesDtl.setItemName(itemMst.getItemName());
			salesDtl.setUnitName(unitMst.getUnitName());
			ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
			if (getTaxMst.getBody().size() > 0) {
				for (TaxMst taxMst : getTaxMst.getBody()) {

					String companyState = SystemSetting.getUser().getCompanyMst().getState();
					String customerState = "KERALA";
					try {
						customerState = salesTransHdr.getAccountHeads().getCustomerState();
					} catch (Exception e) {
						System.out.println(e);
					}

					if (null == customerState) {
						customerState = "KERALA";
					}

					if (null == companyState) {
						companyState = "KERALA";
					}

					if (customerState.equalsIgnoreCase(companyState)) {
						if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
							salesDtl.setCgstTaxRate(taxMst.getTaxRate());
						}
						if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
							salesDtl.setSgstTaxRate(taxMst.getTaxRate());
						}
						salesDtl.setIgstTaxRate(0.0);
						salesDtl.setIgstAmount(0.0);
						ResponseEntity<TaxMst> taxMst1 = RestCaller.getTaxMstByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
						if (null != taxMst1.getBody()) {
							
							salesDtl.setTaxRate(taxMst1.getBody().getTaxRate());
						}
					} 
					
					else {
						if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
							salesDtl.setCgstTaxRate(0.0);
							salesDtl.setCgstAmount(0.0);
							salesDtl.setSgstTaxRate(0.0);
							salesDtl.setSgstAmount(0.0);
							salesDtl.setTaxRate(taxMst.getTaxRate());
							salesDtl.setIgstTaxRate(taxMst.getTaxRate());
						}
					}
					if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
						if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
							salesDtl.setCessRate(taxMst.getTaxRate());
						}
					}
					if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
						salesDtl.setAddCessRate(taxMst.getTaxRate());
				}
				Double rateBeforeTax = (100 * mrpRateIncludingTax)
						/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate()
								+ salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
				salesDtl.setRate(rateBeforeTax);
				BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
				salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
				BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
				salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
				BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
				salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
				BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
				salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
				BigDecimal addcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
				salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
			}
			}else {
							if (unitResp.getBody().getId().equalsIgnoreCase(itemMst.getUnitId())) {
								salesDtl.setStandardPrice(itemMst.getStandardPrice());
							} else {
					
								ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(itemMst.getId(),
										unitResp.getBody().getId());
								MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
								salesDtl.setStandardPrice(multiUnitMst.getPrice());
					
							}
							if(null != itemMst.getTaxRate())
								{
									salesDtl.setTaxRate(itemMst.getTaxRate());
									taxRate = itemMst.getTaxRate();
						
								} else {
									salesDtl.setTaxRate(0.0);
								}
							Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
							// Calculate discount on base price
							if(null != salesTransHdr.getAccountHeads().getDiscountProperty())
							{
								
							
							if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE"))
							{
								calcDiscountOnBasePrice(salesTransHdr,rateBeforeTax,itemMst,mrpRateIncludingTax,taxRate);
								
							}
							if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP"))
							{
//								salesDtl.setTaxRate(0.0);
								calcDiscountOnMRP(salesTransHdr,rateBeforeTax,itemMst,mrpRateIncludingTax,taxRate);
							}
							if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX"))
							{
							ambrossiaDiscount(salesTransHdr,rateBeforeTax,itemMst,mrpRateIncludingTax,taxRate);
							}
							}
							else
							{
								
								double cessAmount = 0.0;
								double cessRate = 0.0;
								salesDtl.setRate(rateBeforeTax);
								if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C"))
								{
									if (itemMst.getCess() > 0) {
										cessRate = itemMst.getCess();
										rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + itemMst.getCess());
									
									salesDtl.setRate(rateBeforeTax);
									cessAmount = salesDtl.getQty() * salesDtl.getRate() * itemMst.getCess() / 100;
									}
									else {
										cessAmount = 0.0;
										cessRate = 0.0;
									}
									salesDtl.setRate(rateBeforeTax);
									salesDtl.setCessRate(cessRate);
									salesDtl.setCessAmount(cessAmount);
								}
								
								double sgstTaxRate = taxRate / 2;
								double cgstTaxRate = taxRate / 2;
								salesDtl.setCgstTaxRate(cgstTaxRate);

								salesDtl.setSgstTaxRate(sgstTaxRate);
								String companyState = SystemSetting.getUser().getCompanyMst().getState();
								String customerState = "KERALA";
								try {
									customerState = salesTransHdr.getAccountHeads().getCustomerState();
								} catch (Exception e) {
									System.out.println(e);

								}

								if (null == customerState) {
									customerState = "KERALA";
								}

								if (null == companyState) {
									companyState = "KERALA";
								}

								if (customerState.equalsIgnoreCase(companyState)) {
									salesDtl.setSgstTaxRate(taxRate / 2);

									salesDtl.setCgstTaxRate(taxRate / 2);

									Double cgstAmt = 0.0,sgstAmt = 0.0;
									cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
									BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
									bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
											
									salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
									sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
									BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
									bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
									
									salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

									salesDtl.setIgstTaxRate(0.0);
									salesDtl.setIgstAmount(0.0);
								

								} else {
									salesDtl.setSgstTaxRate(0.0);

									salesDtl.setCgstTaxRate(0.0);

									salesDtl.setCgstAmount(0.0);

									salesDtl.setSgstAmount(0.0);

									salesDtl.setIgstTaxRate(taxRate);
									salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

								}
							}
							
			}
			
			String companyState = SystemSetting.getUser().getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {
				System.out.println(e);

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				salesDtl.setSgstTaxRate(taxRate / 2);

				salesDtl.setCgstTaxRate(taxRate / 2);

				Double cgstAmt =0.0,sgstAmt =0.0;
				cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
				bdcgstAmt = bdcgstAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setCgstAmount(bdcgstAmt.doubleValue());
				sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
				bdsgstAmt = bdsgstAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setSgstAmount(bdsgstAmt.doubleValue());
				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setIgstAmount(0.0);

			} else {
				salesDtl.setSgstTaxRate(0.0);
				salesDtl.setCgstTaxRate(0.0);
				salesDtl.setCgstAmount(0.0);
				salesDtl.setSgstAmount(0.0);
				salesDtl.setIgstTaxRate(taxRate);
				salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

			}
		

		BigDecimal settoamount = new BigDecimal(
				(spencer.getQty() *salesDtl.getRate() ));
		
		double includingTax= (settoamount.doubleValue()*salesDtl.getTaxRate())/100;
		double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(setamount.doubleValue());
		salesDtl.setAddCessRate(0.0);
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp1 = RestCaller
				.getPriceDefenitionMstByName("MRP");
		PriceDefenitionMst priceDefenitionMst1 = priceDefenitionMstResp1.getBody();
		if(null != priceDefenitionMst1)
		{
//			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(),edate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setMrp(priceDefinition.getAmount());
			}
		}
		
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if(null != priceDefenitionMst)
		{
//			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(),edate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setCostPrice(priceDefinition.getAmount());
			}
		}
		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		salesDtl = respentity.getBody();

		}

		ResponseEntity<List<SalesDtl>> salesDtlList = RestCaller.getSalesDtl(salesTransHdr);
		salesDtlTableList = FXCollections.observableArrayList(salesDtlList.getBody());
		FillTable();
	}

	private void FillTable() {

		itemDetailTable.setItems(salesDtlTableList);

		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Summary summary = null;

		if (null == salesTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			summary = RestCaller.getSalesWindowSummaryDiscount(salesTransHdr.getId());
		} else {
			summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
		}
		if (null != summary.getTotalAmount()) {
			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
		}

	}

	@FXML
	void FinalSaveOnPress(KeyEvent event) {

	}

	@FXML
	void ShowLocalCustomerPopup(ActionEvent event) {

	}

	@FXML
	void discountAmntOnEnter(KeyEvent event) {

	}

	@FXML
	void discountOnEnter(KeyEvent event) {

	}

	@FXML
	void hold(ActionEvent event) {

	}

	@FXML
	void onEnterCustPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();
		}
	}

	private void loadCustomerPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/SubBranchPopUp.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			btnFetchItems.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void save(ActionEvent event) {
		finalSave();

	}

	@FXML
	void saveOnCtl(KeyEvent event) {

    	if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}

	}

	private void finalSave() {
		


		/*
		 * FinalSave Final
		 */

		salesTransHdr.setCustomerId(salesTransHdr.getCustomerId());

		/*
		 * new url for getting account heads instead of customer mst=========05/0/2022
		 */
//		ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerById(salesTransHdr.getCustomerId());
		ResponseEntity<AccountHeads> accountHeadsResp=RestCaller.getAccountHeadsById(salesTransHdr.getCustomerId());
		if (null != accountHeadsResp.getBody()) {
			salesTransHdr.setAccountHeads(accountHeadsResp.getBody());
			
			if (null == accountHeadsResp.getBody().getPartyGst() || accountHeadsResp.getBody().getPartyGst().length() < 13) {
				salesTransHdr.setSalesMode("B2C");
			} else {
				salesTransHdr.setSalesMode("B2B");
			}
		}

		txtChangeamount.setText("00.0");
		Double paidAmount = 0.0;
		Double cardAmount = 0.0;
		String card = "";
		if (!txtSBICard.getText().trim().isEmpty()) {
			card = txtSBICard.getText();
		} else if (!txtYesCard.getText().trim().isEmpty()) {
			card = txtYesCard.getText();
		} else if (!txtSodexoCard.getText().trim().isEmpty()) {
			card = txtSodexoCard.getText();
		}

//		if (!txtLocalCustomer.getText().trim().isEmpty()) {
//			ResponseEntity<LocalCustomerMst> LocalCustomerMstResp = RestCaller.getLocalCustomerById(localCustId);
//			LocalCustomerMst localCustomerMst = LocalCustomerMstResp.getBody();
//			if (null != localCustomerMst) {
//				salesTransHdr.setLocalCustomerMst(localCustomerMst);
//			}
//
//		}
		salesTransHdr.setCardNo(card);

		Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
		salesTransHdr.setInvoiceAmount(invoiceAmount);

		if (txtDiscountAmt.getText().length() > 0)
			salesTransHdr.setInvoiceDiscount(Double.parseDouble(txtDiscountAmt.getText()));
		else
			salesTransHdr.setInvoiceDiscount(0.0);
//		salesTransHdr.setDiscount(txtDiscount.getText());
		if (!txtPaidamount.getText().trim().isEmpty()) {

			paidAmount = Double.parseDouble(txtPaidamount.getText());
			salesTransHdr.setCashPay(paidAmount);
		} else {
			salesTransHdr.setCashPay(0.0);
		}

		if (!txtcardAmount.getText().trim().isEmpty()) {

			cardAmount = Double.parseDouble(txtcardAmount.getText());
			salesTransHdr.setCardamount(cardAmount);
		} else {
			salesTransHdr.setCardamount(0.0);
		}

		salesTransHdr.setPaidAmount(salesTransHdr.getCashPay() + salesTransHdr.getCardamount());

		if (!txtChangeamount.getText().trim().isEmpty()) {
			Double changeAmount = Double.parseDouble(txtChangeamount.getText());
			salesTransHdr.setChangeAmount(changeAmount);
		}
		eventBus.post(salesTransHdr);

		// Double
		// change=Double.parseDouble(txtPaidamount.getText())-Double.parseDouble(txtCashtopay.getText());
		// txtChangeamount.setText(Double.toString(change));
		// btnSave.setDisable(false);
		// btnSave.setDisable(true);

		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		if (saledtlSaved.getBody().size() == 0) {
			return;
		}

		ResponseEntity<BranchMst> branchMst = RestCaller.getBranchMstById(salesTransHdr.getCustomerId());

		BranchMst branch = new BranchMst();
		branch = branchMst.getBody();

		if (null != branch) {
			salesTransHdr.setSalesMode("BRANCH_SALES");
		}


		ResponseEntity<SalesTypeMst> getsalesType = RestCaller
				.getSaleTypeByname(cmbSaleType.getSelectionModel().getSelectedItem());
		salesTransHdr.setInvoiceNumberPrefix(getsalesType.getBody().getSalesPrefix());


//				Date date = Date.valueOf(LocalDate.now());
		LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		Date date = SystemSetting.systemDate;
		salesTransHdr.setVoucherDate(date);
		// Call Rest to find if customer is Branch

			salesTransHdr.setIsBranchSales("Y");


		ResponseEntity<AccountHeads> custentity = RestCaller.getAccountHeadsById(salesTransHdr.getCustomerId());
		ResponseEntity<AccountReceivable> accountReceivableResp = RestCaller
				.getAccountReceivableBySalesTransHdrId(salesTransHdr.getId());
		AccountReceivable accountReceivable = null;
		if (null != accountReceivableResp.getBody()) {
			accountReceivable = accountReceivableResp.getBody();
		} else {
			accountReceivable = new AccountReceivable();
		}

		accountReceivable.setAccountId(salesTransHdr.getCustomerId());
		accountReceivable.setAccountHeads(custentity.getBody());

		if (txtAmtAftrDiscount.getText().length() > 0) {
			accountReceivable.setDueAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
			accountReceivable.setDueAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
			accountReceivable.setBalanceAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
		} else {
			accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
			accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
			accountReceivable.setBalanceAmount(Double.parseDouble(txtCashtopay.getText()));
		}
		LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		LocalDate dueDate = due.plusDays(custentity.getBody().getCreditPeriod());
		accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
		accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
		accountReceivable.setSalesTransHdr(salesTransHdr);
		accountReceivable.setRemark("Wholesale");
		LocalDate due1 = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		accountReceivable.setVoucherDate(java.sql.Date.valueOf(due1));
		accountReceivable.setPaidAmount(0.0);
		ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
		accountReceivable = respentity.getBody();
		
	// Delete any sales receiots previously saved by mistake
		
		ResponseEntity<List<SalesReceipts>> srList = RestCaller.getSalesReceiptsByTransHdrId(salesTransHdr.getId());
		if(srList.getBody().size()>00) {
		 
		 
			RestCaller.deleteSalesReceiptsByTransHdr(salesTransHdr);
		}
		  
		
		
		
		SalesReceipts salesReceipts = new SalesReceipts();
		salesReceipts.setReceiptMode("CREDIT");
		salesReceipts.setReceiptAmount(invoiceAmount);
		salesReceipts.setSalesTransHdr(salesTransHdr);
		salesReceipts.setAccountId(custentity.getBody().getId());
		salesReceipts.setBranchCode(salesTransHdr.getBranchCode());

		RestCaller.saveSalesReceipts(salesReceipts);


		if (null == salesTransHdr.getVoucherNumber()) {
			RestCaller.updateSpencerSalesTranshdr(salesTransHdr);
		}
		salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());

		notifyMessage(5, "Sales Saved",true);
		DayBook dayBook = new DayBook();
		dayBook.setBranchCode(salesTransHdr.getBranchCode());
		dayBook.setDrAccountName(custname.getText());
		dayBook.setDrAmount(salesTransHdr.getInvoiceAmount());
		dayBook.setNarration(custname.getText() + salesTransHdr.getVoucherNumber());
		dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
		dayBook.setSourceVoucherType("SALES");
		dayBook.setCrAccountName("SALES ACCOUNT");
		dayBook.setCrAmount(salesTransHdr.getInvoiceAmount());

		LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
		dayBook.setsourceVoucherDate(java.sql.Date.valueOf(rdate));
		ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(salesTransHdr.getVoucherDate());

		
		
		//Version 1.6
		
		
		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtPaidamount.setText("");
		txtSBICard.setText("");
		txtSodexoCard.setText("");
		txtYesCard.setText("");
		custname.setText("");
		custAdress.setText("");
		gstNo.setText("");

		cmbSaleType.getSelectionModel().clearSelection();
		txtPriceType.clear();
		txtAmtAftrDiscount.clear();
		txtDiscount.clear();
		txtDiscountAmt.clear();

		txtLocalCustomer.clear();
		txtLocalCustomer.setDisable(true);
		itemDetailTable.getItems().clear();
		// Version 1.6 ends
		
		try {
			NewJasperPdfReportService.TaxInvoiceReport(salesTransHdr.getVoucherNumber(), strDate);
		} catch (JRException e) {
			System.out.println(e);
		}
		
		salesTransHdr = null;
		salesDtl = null;

	}

	@FXML
	void toPrintChange(KeyEvent event) {

	}

	@FXML
	void unHold(ActionEvent event) {

	}

	@Subscribe
	public void popupCustomerlistner(SubBranchSaleEvent spencerEvent) {

		custname.setText(spencerEvent.getCustomerName());
		custAdress.setText(spencerEvent.getCustomerAddress());
		gstNo.setText(spencerEvent.getCustomerGst());

	}
	
	private void calcDiscountOnBasePrice(SalesTransHdr salesTransHdr,Double rateBeforeTax,
			ItemMst item,Double mrpRateIncludingTax,double taxRate)
	{
		if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
		{
			Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			//salesDtl.setMrp(rateBefrTax.doubleValue());
			salesDtl.setStandardPrice(rateBefrTax.doubleValue());
		}
		else
		{
		salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);
				
				if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
				{
					Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					//salesDtl.setMrp(rateBefrTax.doubleValue());
					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				}
				else
				{
				salesDtl.setRate(rateBeforeTax);
				}
				//salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);

		
	}
	
	private void calcDiscountOnMRP(SalesTransHdr salesTransHdr,Double rateBeforeTax,
			ItemMst item,Double mrpRateIncludingTax,double taxRate)
	{
		if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
		{
			Double rateAfterDiscount = (100 * mrpRateIncludingTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);
			
			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BnewrateBeforeTax.doubleValue());
			//BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
		//	rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
			//salesDtl.setMrp(BrateAfterDiscount.doubleValue());
			salesDtl.setStandardPrice(taxRate);
		}
		else
		{
		salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);
				
				if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
				{
					Double rateAfterDiscount = (100 * mrpRateIncludingTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					//salesDtl.setMrp(mrpRateIncludingTax);
					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				}
				else
				{
				salesDtl.setRate(rateBeforeTax);
				}
				//salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
			
		
	}
	
	

	private void ambrossiaDiscount(SalesTransHdr salesTransHdr,Double rateBeforeTax,
			ItemMst item,Double mrpRateIncludingTax,double taxRate)
	{
	if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
	{
	double discoutAmount = (rateBeforeTax * salesTransHdr.getAccountHeads().getCustomerDiscount())/100;
	BigDecimal BrateAfterDiscount = new BigDecimal(discoutAmount);
	
	BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	double newRate = rateBeforeTax - discoutAmount;
	salesDtl.setDiscount(discoutAmount);
	BigDecimal BnewRate = new BigDecimal(newRate);
	BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	salesDtl.setRate(BnewRate.doubleValue());
	BigDecimal BrateBeforeTax = new BigDecimal(rateBeforeTax);
	BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	//salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
	salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());
	}
	else
	{
	salesDtl.setRate(rateBeforeTax);
	}
	double cessAmount = 0.0;
	double cessRate = 0.0;
	
	if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
		if (item.getCess() > 0) {
			cessRate = item.getCess();

			rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

			System.out.println("rateBeforeTax---------" + rateBeforeTax);
			
			if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
			{
				Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
				BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setRate(BrateAfterDiscount.doubleValue());
				BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
				rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				//salesDtl.setMrp(rateBefrTax.doubleValue());
				salesDtl.setStandardPrice(rateBefrTax.doubleValue());
			}
			else
			{
			salesDtl.setRate(rateBeforeTax);
			}
			//salesDtl.setRate(rateBeforeTax);

			cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

			/*
			 * Recalculate RateBefore Tax if Cess is applied
			 */

		}
	} else {
		cessAmount = 0.0;
		cessRate = 0.0;
	}

	salesDtl.setCessRate(cessRate);
	salesDtl.setCessAmount(cessAmount);

}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
