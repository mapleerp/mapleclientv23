package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountPopUpAssetAndLiability;
import com.maple.mapleclient.events.AccountPopUpLiabilityEvent;
import com.maple.mapleclient.events.AcountPopUpAssetEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class AccountPopUpAssetCtl {

	String taskid;
	String processInstanceId;

	private ObservableList<AccountPopUpAssetAndLiability> accountList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

	AcountPopUpAssetEvent acountPopUpAssetEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	
	@FXML
    private TextField txtSearchBox;

    @FXML
    private TableView<AccountPopUpAssetAndLiability> tblAssetAccount;

    @FXML
    private TableColumn<AccountPopUpAssetAndLiability, String> clmnAssetAccount;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;

    @FXML
    void actionCancel(ActionEvent event) {
    	Stage stage = (Stage) btnCancel.getScene().getWindow();
		stage.close();
    }

    @FXML
    void actionOk(ActionEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
    void tblKeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtSearchBox.requestFocus();
		}
    }

    @FXML
    void txtKeyPressed(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblAssetAccount.requestFocus();
			tblAssetAccount.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
		}
    }
    
    @FXML
	private void initialize() {
    	LoadAccountNameBySearch("");
		
		txtSearchBox.textProperty().bindBidirectional(SearchString);
		eventBus.register(this);
		btnOk.setDefaultButton(true);
		btnCancel.setCache(true);
		
		tblAssetAccount.setItems(accountList);
		
		clmnAssetAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		
		tblAssetAccount.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
				
					acountPopUpAssetEvent = new AcountPopUpAssetEvent();
					acountPopUpAssetEvent.setAccountId(newSelection.getId());
					acountPopUpAssetEvent.setAccountName(newSelection.getAccountName());
					eventBus.post(acountPopUpAssetEvent);
				}
			}
		});


		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadAccountNameBySearch((String) newValue);
			}
		});

    }
    
    
    private void LoadAccountNameBySearch(String searchData) {

		accountList.clear();
		
		ResponseEntity<AccountHeads> accountId=RestCaller.getAccountHeadByName("ASSETS");
		String assetId=accountId.getBody().getId();
		ResponseEntity<List<AccountHeads>> accountDedtails = RestCaller.SearchAccountByNameByParentId(assetId, searchData);
		
		List<AccountHeads> accountListResp = accountDedtails.getBody();
		
		for(AccountHeads accountHeads : accountListResp) {
			AccountPopUpAssetAndLiability account = new AccountPopUpAssetAndLiability();
			account.setAccountName(accountHeads.getAccountName());
			account.setId(accountHeads.getId());
			accountList.add(account);
		}
		return;
    }
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


       private void PageReload() {
       	
 }
}
