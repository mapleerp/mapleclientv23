package com.maple.mapleclient.controllers;


import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Observable;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AddKotTable;
import com.maple.mapleclient.entity.AddKotWaiter;
import com.maple.mapleclient.entity.TableOccupiedMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TableOccupiedEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class TableOccupiedCtl {
	
	String taskid;
	String processInstanceId;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<Button> button = FXCollections.observableArrayList();
	 Button button1 = new Button();
    @FXML
    private GridPane gridpane;

    @FXML
    private ScrollPane scpane;
    
    @FXML
    private Button btnRefresh;

    @FXML
    void actionRefresh(ActionEvent event) {

    	int j =1;
    	int k =0;
    	int rowNo =0;
     	ResponseEntity<List<AddKotTable>> getTable = RestCaller.getTable();
     	for(int i =0;i<getTable.getBody().size();i++)
     	{
     		k=k+1;
     		//int colNo = getTable.getBody().size()/5;
     		if(k==8)
     		{
     			k=1;
     			j=j+3;
     			rowNo = 0;
     		}
     		String s =getTable.getBody().get(i).getTableName();
    	  button1 = new Button(getTable.getBody().get(i).getTableName()); 
    	  gridpane.setMinSize(400, 200); 
     	 button1.setMinHeight(50);
     	 button1.setPrefWidth(200);
     	 button1.setMaxHeight(75);
     	 button1.setMinHeight(75);
     	 button1.setMinWidth(75);
    	 gridpane.setPadding(new Insets(10, 10, 10, 10)); 
    	 gridpane.setVgap(5); 
    	 gridpane.setHgap(5);       
    	 gridpane.setAlignment(Pos.CENTER); 
        
    	 gridpane.add(button1,rowNo++,j); 
         button1.setOnMouseClicked((new EventHandler<MouseEvent>() { 
             public void handle(MouseEvent event) { 

                button1=(Button) event.getSource();
                TableOccupiedMst tableOccupiedMst = new TableOccupiedMst();
                tableOccupiedMst.setTableId(button1.getText());
                 Date date = new Date();
                long time =date.getTime();
                Timestamp ts= new Timestamp(time);
                tableOccupiedMst.setOccupiedTime(ts);
                ResponseEntity<TableOccupiedMst> respentity = RestCaller.saveTableOccupiedMst(tableOccupiedMst);
                tableOccupiedMst= respentity.getBody();
                button1.setText(button1.getText()+"/"+tableOccupiedMst.getOccupiedTime().toString());
                button1.setWrapText(true);
                String s = button1.getStyle() ;
                if(s.equalsIgnoreCase("-fx-background-color: red; -fx-text-fill: white;"))
                {
                	 button1.setStyle("-fx-background-color: violet; -fx-text-fill: white;");
                }
                else if(s.contentEquals("-fx-background-color: darkslateblue; -fx-text-fill: white;"))
                {
                button1.setStyle("-fx-background-color: red; -fx-text-fill: white;"); 
                }
                else if(s.equalsIgnoreCase("-fx-background-color: violet; -fx-text-fill: white;"))
                {
                	//tableOccupiedMst.setTableId(button1.getText());
                	  button1.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;"); 
                }
             } 
          }));
        
         
         button1.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;"); 
          
    
     	
    	}
    }
	@Subscribe
	public void popupStockItemlistner(TableOccupiedEvent tableOccupiedEvent) {
		try {
			
			String table =tableOccupiedEvent.getTableId();

			ResponseEntity<List<AddKotTable>> getTable = RestCaller.getTable();
	     	for(int i =0;i<getTable.getBody().size();i++)
	     	{
	     		button1 = (Button) gridpane.getChildren().get(i);
	     		if(button1.getText().contains(table))
	     		{
	     			button1.setText(button1.getText()+"/"+tableOccupiedEvent.getWaiterId()+"/"+tableOccupiedEvent.getOrderedTime());
	     			button1.setWrapText(true);
	     			button1.setStyle("-fx-background-color: violet; -fx-text-fill: white;");
	     		}
	     	}
		
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	@FXML
	private void initialize() {
		scpane.setContent(gridpane);
		eventBus.register(this);
		int j =1;
    	int k =0;
    	int rowNo =0;
     	ResponseEntity<List<AddKotTable>> getTable = RestCaller.getTable();
     	for(int i =0;i<getTable.getBody().size();i++)
     	{
     		k=k+1;
     		//int colNo = getTable.getBody().size()/5;
     		if(k==8)
     		{
     			k=1;
     			j=j+3;
     			rowNo = 0;
     		}
     		String s =getTable.getBody().get(i).getTableName();
    	  button1 = new Button(getTable.getBody().get(i).getTableName()); 
    	  gridpane.setMinSize(400, 200); 
    	 button1.setMinHeight(50);
    	 button1.setPrefWidth(200);
    	 button1.setMaxHeight(75);
    	 button1.setMinHeight(75);
    	 button1.setMinWidth(75);
    	 
    	 gridpane.setPadding(new Insets(10, 10, 10, 10)); 
    	 gridpane.setVgap(5); 
    	 gridpane.setHgap(5);       
    	 gridpane.setAlignment(Pos.CENTER); 
        
    	 gridpane.add(button1,rowNo++,j); 
         button1.setOnMouseClicked((new EventHandler<MouseEvent>() { 
             public void handle(MouseEvent event) { 

                button1=(Button) event.getSource();
                TableOccupiedMst tableOccupiedMst = new TableOccupiedMst();
                tableOccupiedMst.setTableId(button1.getText());
                tableOccupiedMst.setTableId(button1.getText());
                 Date date = new Date();
                long time =date.getTime();
                Timestamp ts= new Timestamp(time);
                tableOccupiedMst.setOccupiedTime(ts);
             //   tableOccupiedMst.setOccupiedTime(LocalTime.now());
                ResponseEntity<TableOccupiedMst> respentity = RestCaller.saveTableOccupiedMst(tableOccupiedMst);
                tableOccupiedMst= respentity.getBody();
                button1.setText(button1.getText()+"/"+tableOccupiedMst.getOccupiedTime().toString());
                button1.setWrapText(true);
                String s = button1.getStyle() ;
                if(s.equalsIgnoreCase("-fx-background-color: red; -fx-text-fill: white;"))
                {
                	 button1.setStyle("-fx-background-color: violet; -fx-text-fill: white;");
                }
                else if(s.contentEquals("-fx-background-color: darkslateblue; -fx-text-fill: white;"))
                {
                button1.setStyle("-fx-background-color: red; -fx-text-fill: white;"); 
                }
                else
                {
                	  button1.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;"); 
                }
             } 
          }));
        
         
         button1.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;"); 
          
    
     	
    	}
		
	
	}
	public final void setOnAction(EventHandler<ActionEvent> value)
    {
    	button1.setOnMouseClicked((new EventHandler<MouseEvent>() { 
            public void handle(MouseEvent event) { 
               System.out.println("Hello World"); 
               button1.setStyle("-fx-background-color: red; -fx-text-fill: white;"); 
            } 
         }));
    }
	
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}
	
}
