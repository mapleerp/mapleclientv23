package com.maple.mapleclient.controllers;

import java.io.File;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.bouncycastle.asn1.tsp.TSTInfo;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemNameInLanguage;
import com.maple.mapleclient.entity.LanguageMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

public class ItemLangCtl {
	String taskid;
	String processInstanceId;

	ItemNameInLanguage itemInLang = new ItemNameInLanguage();
	private ObservableList<LanguageMst> languageMstList = FXCollections.observableArrayList();
	private ObservableList<ItemNameInLanguage> itemInLngList = FXCollections.observableArrayList();


	EventBus eventBus = EventBusFactory.getEventBus();
	
	String itemId = null;

	@FXML
	private TextField txtItem;

	@FXML
	private TextArea txtItemLang;

	@FXML
	private ComboBox<String> cmbLang;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowAll;

	@FXML
	private TableView<ItemNameInLanguage> tblItemLang;

	@FXML
	private TableColumn<ItemNameInLanguage, String> clItemName;

	@FXML
	private TableColumn<ItemNameInLanguage, String> clItemInLang;

	@FXML
	private TableColumn<ItemNameInLanguage, String> clLanguage;

	@FXML
	private void initialize() {

		eventBus.register(this);
		
		tblItemLang.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					itemInLang = new ItemNameInLanguage();
					itemInLang.setId(newSelection.getId());
				

				}
			}
		});
		
	 
		
		
	}
	
    @FXML
    void setLanguages(MouseEvent event) {
    	
    	setCmbLanguages();

    }

	private void setCmbLanguages() {
		cmbLang.getItems().clear();
		ResponseEntity<List<LanguageMst>> alngResp = RestCaller.getAllLangues();
    	languageMstList =  FXCollections.observableArrayList(alngResp.getBody());
    	
    	for(LanguageMst lang : languageMstList)
    	{
    		cmbLang.getItems().add((String) lang.getLanguageName());
    	}
		
	}

	@FXML
	void Delete(ActionEvent event) {
		
		if(null != itemInLang)
		{
			if(null != itemInLang.getId())
			{
				RestCaller.deleteItemNameInLanguageById(itemInLang.getId());
    			ShowAll();
			}
		}

	}

	@FXML
	void Save(ActionEvent event) {

		if (txtItem.getText().trim().isEmpty()) {
			notifyMessage(5, "please select Item", false);
			txtItem.requestFocus();
			return;
		}
		if (txtItemLang.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter item name", false);
			txtItemLang.requestFocus();
			return;
		}
		if (null == cmbLang.getValue()) {
			notifyMessage(5, "please select Language", false);
			cmbLang.requestFocus();
			return;
		}

		itemInLang = new ItemNameInLanguage();
		ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(itemId);
		ItemMst itemMst = itemResp.getBody();
		if(null == itemMst)
		{
			notifyMessage(5, "Invalid Item", false);
			txtItem.requestFocus();
			return;
		}
		String langName = cmbLang.getSelectionModel().getSelectedItem();
		ResponseEntity<LanguageMst> langMstResp = RestCaller.getLanguageMstByLangName(langName);
		LanguageMst languageMst = langMstResp.getBody();
		if(null == languageMst)
		{
			notifyMessage(5, "Invalid Language name", false);
			cmbLang.requestFocus();
			return;
		}
		itemInLang.setLangName(languageMst.getLanguageName());
		itemInLang.setItemId(itemMst.getId());
//		Font font = new Font(languageMst.getLanguageFontType(),100);
		
		
//		String itemNameInLang = txtItemLang.getText();
	
		System.out.println("------------------------"+txtItemLang.getText());
		
		
		
		itemInLang.setItemNameInLang(txtItemLang.getText());
		
		ResponseEntity<ItemNameInLanguage> itemInLangResp = RestCaller.saveItemNameInLanguage(itemInLang);
		itemInLang = itemInLangResp.getBody();
		if(null != itemInLang)
		{
			notifyMessage(5, "Successfully Added", true);
			itemInLngList.add(itemInLang);
			FillTable();

		}
		itemInLang = null;

	}

	private void FillTable() {
		tblItemLang.setItems(itemInLngList);
		
		for(ItemNameInLanguage item : itemInLngList)
		{
			ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(item.getItemId());
			ItemMst itemMst = itemResp.getBody();
			if(null != itemMst)
			{
				item.setItemName(itemMst.getItemName());
		    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

			}
		}
    	clItemInLang.setCellValueFactory(cellData -> cellData.getValue().getItemNameInLangProperty());

    	clLanguage.setCellValueFactory(cellData -> cellData.getValue().getLangNameProperty());

		
	}

	@FXML
	void ShowAll(ActionEvent event) {
		
		ShowAll();

	}

	private void ShowAll() {
		ResponseEntity<List<ItemNameInLanguage>> alngResp = RestCaller.getAllItemNameInLanguage();;
		itemInLngList =  FXCollections.observableArrayList(alngResp.getBody());
    	FillTable();
	}

	@FXML
	void TxtItemKeyPress(KeyEvent event) {
		
		showPopup();

	}

	@FXML
	void cmbLangKeyPress(KeyEvent event) {

	}

	@FXML
	void txtItemLangKeyPress(KeyEvent event) {

	}

	private void showPopup() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtItemLang.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			txtItem.setText(itemPopupEvent.getItemName());
			itemId = itemPopupEvent.getItemId();
		

		}
	}
	
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
