package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.entity.AdditionalExpense;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
public class ExpenseReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<PaymentDtl> expensesList = FXCollections.observableArrayList();
	PaymentDtl paymentDtl = new PaymentDtl();
	String OrderDate;
    @FXML
    private TableView<PaymentDtl> tbExpenses;

    @FXML
    private TableColumn<PaymentDtl, String> clExpenseHead;

    @FXML
    private TableColumn<PaymentDtl, Number> clAmount;

    @FXML
    private TextField txtTotalAmt;
    
    @FXML
	private void initialize() {
    	expensesList.add(paymentDtl);
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clExpenseHead.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());
    
    	loadPettyExpense(OrderDate);
    }
    
    private void loadPettyExpense(String OrderDate)
    {

    	//ResponseEntity<List<PaymentDtl>> additionalExpense = RestCaller.getPettyExpensesByDate(OrderDate);
    	//expensesList = FXCollections.observableArrayList(additionalExpense.getBody());
    	//tbExpenses.setItems(expensesList);
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }


}
