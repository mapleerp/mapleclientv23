package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SaleOrderReport;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import net.sf.jasperreports.engine.JRException;

public class SOConvertedReportCtl {
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpVoucherDate;

    @FXML
    private Button btnGenerateReport;
    @FXML
   	private void initialize() {
    	dpVoucherDate = SystemSetting.datePickerFormat(dpVoucherDate, "dd/MMM/yyyy");
   		
       }
    @FXML
    void actionGenerateReport(ActionEvent event) {
    	java.util.Date uDate  = SystemSetting.localToUtilDate(dpVoucherDate.getValue());
		 String  strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
    	ResponseEntity<List<SaleOrderReport>> saleOrderreport = RestCaller.saleOrderConvertedReport(strDate);
    	try {
			JasperPdfReportService.soconvertedReport(saleOrderreport.getBody(), strDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}


}
