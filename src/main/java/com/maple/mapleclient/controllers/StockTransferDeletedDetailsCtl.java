package com.maple.mapleclient.controllers;

import java.util.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.entity.StockTransferOutDltdDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class StockTransferDeletedDetailsCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<StockTransferOutDltdDtl> stockTransferHdrReportList = FXCollections.observableArrayList();

	private ObservableList<StockTransferOutDltdDtl> stockTransferDtlReportList = FXCollections.observableArrayList();


    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnGenerate;

    @FXML
    private TableView<StockTransferOutDltdDtl> tblReportHdr;

    @FXML
    private TableColumn<StockTransferOutDltdDtl, String> clToBranch;

    @FXML
    private TableColumn<StockTransferOutDltdDtl, String> clVoucherNumber;

    @FXML
    private TableView<StockTransferOutDltdDtl> tblReportDtl;

    @FXML
    private TableColumn<StockTransferOutDltdDtl, String> clItem;

    @FXML
    private TableColumn<StockTransferOutDltdDtl, String> clUnit;

    @FXML
    private TableColumn<StockTransferOutDltdDtl, Number> clQty;

    @FXML
    private TableColumn<StockTransferOutDltdDtl, Number> clMrp;

    @FXML
    private TextField txtTotalAmount;
    

	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		tblReportHdr.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getStockTransferOutHdr()) {
					
					ResponseEntity<List<StockTransferOutDltdDtl>> DltdDtlList =
							RestCaller.getStockTransferDltdDtlList(newSelection.getStockTransferOutHdr().getId());
					
					stockTransferDtlReportList = FXCollections.observableArrayList(DltdDtlList.getBody());
					
					DtlReportFillTable();
					
				}
			}
		});
		
		
	}

    private void DtlReportFillTable() {

    	tblReportDtl.setItems(stockTransferDtlReportList);
    	
    	for(StockTransferOutDltdDtl dltdHdr : stockTransferDtlReportList)
    	{
    		ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(dltdHdr.getItemId());
    		ItemMst itemMst = itemResp.getBody();
    		
    		dltdHdr.setItemName(itemMst.getItemName());
    		
    		ResponseEntity<UnitMst> unitResp = RestCaller.getunitMst(dltdHdr.getUnitId());
    		UnitMst unitMst = unitResp.getBody();
    		
    		dltdHdr.setUnitName(unitMst.getUnitName());

    	}
    	
    	clItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
    	clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());


	}

	@FXML
    void GenerateReaport(ActionEvent event) {
    	
    	if(null == dpDate.getValue())
    	{
    		notifyMessage(3, "Please select date", false);
    		return;
    	}
    	
    	Date sdate = SystemSetting.localToUtilDate(dpDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");
		
		ResponseEntity<List<StockTransferOutDltdDtl>> stockTransferHdrDtl = RestCaller.getStockTransferDltdDtlSummary(strtDate);
		
		List<StockTransferOutDltdDtl> stockTransferHdrDtlList = stockTransferHdrDtl.getBody();
		
		stockTransferHdrReportList = FXCollections.observableArrayList(stockTransferHdrDtlList);
		
		HdrReportFillTable();


    }
    
    private void HdrReportFillTable() {
    	
    	tblReportHdr.setItems(stockTransferHdrReportList);
    	
    	for(StockTransferOutDltdDtl dltdHdr : stockTransferHdrReportList)
    	{
    		dltdHdr.setToBranch(dltdHdr.getStockTransferOutHdr().getToBranch());
    		dltdHdr.setVoucherNumber(dltdHdr.getStockTransferOutHdr().getVoucherNumber());

    	}
    	
    	clToBranch.setCellValueFactory(cellData -> cellData.getValue().getToBranchProperty());
    	clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());

    	
    	
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}


}
