package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MixCatItemLinkMst;
import com.maple.mapleclient.entity.MixCategoryMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class MixCatItemLinkCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<MixCatItemLinkMst> resourceCatItemLinkMstList = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();
	
	@FXML
    private TableView<MixCatItemLinkMst> tblResourceCateItemLink;
String  itemId;
Object resCatId;
    @FXML
    private TableColumn<MixCatItemLinkMst, String> clItem;

    @FXML
    private TableColumn<MixCatItemLinkMst, String	> clResourceCat;

    @FXML
    private TextField txtItem;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnFinalSubmit;

    @FXML
    private Button btnDelete;

    @FXML
    private ComboBox<String> cmbResourceCat;


    @FXML
    void Delete(ActionEvent event) {

    }

    @FXML
    void FinalSubmit(ActionEvent event) {
        ResponseEntity<List<MixCatItemLinkMst>> responseEntity=RestCaller.resourceCategoryItemLinkFetch();
        resourceCatItemLinkMstList = FXCollections.observableArrayList(responseEntity.getBody());
        tblResourceCateItemLink.setItems(resourceCatItemLinkMstList);
        
        System.out.print(resourceCatItemLinkMstList.size()+"observable list size isssssssssssss"); 
        filltable();
    }

    @FXML
    void addOnEnter(KeyEvent event) {

    }

    @FXML
    void itemOnEnter(KeyEvent event) {

    }

    @FXML
    void itempopup(MouseEvent event) {
    	 showPopups();
    }

    @FXML
    void saveOnKey(KeyEvent event) {

    }

    @FXML
	private void initialize() {
    	try {
    	eventBus.register(this);
    	
    	UnitMst unitMst = new UnitMst();
		// set unit
		ArrayList unit = new ArrayList();

		unit = RestCaller.SearcResourceCat();
		
		System.out.print(unit.size()+"unit list size isssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		Iterator itr = unit.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object unitName = lm.get("mixName");
			Object resCatId = lm.get("id");
			if (resCatId != null) {
				cmbResourceCat.getItems().add((String) unitName);
				UnitMst newUnitMst = new UnitMst();
				newUnitMst.setUnitName((String) unitName);
				
			}
		}}catch (Exception e) {
			System.out.print(e);
		}
    }
    private void showPopups() {
  		try {
  			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
  			// fxmlLoader.setController(itemStockPopupCtl);
  			Parent root1;

  			root1 = (Parent) fxmlLoader.load();
  			Stage stage = new Stage();

  			stage.initModality(Modality.APPLICATION_MODAL);
  			stage.initStyle(StageStyle.UNDECORATED);
  			stage.setTitle("Stock Item");
  			stage.initModality(Modality.APPLICATION_MODAL);
  			stage.setScene(new Scene(root1));
  			stage.show();

  		} catch (IOException e) {

  			e.printStackTrace();
  		}

  	}
	
@Subscribe
public void popupItemlistner(KitPopupEvent kitPopupEvent) {

	System.out.println("-------------popupItemlistner-------------");

	Stage stage = (Stage) btnAdd.getScene().getWindow();
	
	if (stage.isShowing()) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				txtItem.setText(kitPopupEvent.getItemName());
				itemId=kitPopupEvent.getItemId();
			}
		});
	}
}


@FXML
void AddItem(ActionEvent event) {
	
	 if(null==cmbResourceCat.getValue()) {
		 notifyMessage(5,"Pleace Fill ResourceCategory!!!");
	 }else if (null==txtItem.getText()) {
		 notifyMessage(5,"Pleace Fill Item Name!!!");
	}else {
	
	 
	MixCatItemLinkMst resourceCatItemLinkMst =new MixCatItemLinkMst();
	resourceCatItemLinkMst.setItemId(itemId);
    ResponseEntity<MixCategoryMst> resp=RestCaller.findByResoureCatName(cmbResourceCat.getValue()); 
    MixCategoryMst resourceCategoryMst=resp.getBody();
    resourceCatItemLinkMst.setResourceCatId(String.valueOf(resourceCategoryMst.getId()));
    ResponseEntity<List<MixCategoryMst>> response=RestCaller.findByResoureCatNameAndItem(itemId,resourceCategoryMst.getId());
    if(response.getBody().size()==0) {
	ResponseEntity<MixCatItemLinkMst> respentity = RestCaller.saveResourceCatItemLinkMst(resourceCatItemLinkMst);
	

    ResponseEntity<List<MixCatItemLinkMst>> responseEntity=RestCaller.resourceCategoryItemLinkFetch();
    resourceCatItemLinkMstList = FXCollections.observableArrayList(responseEntity.getBody());
    tblResourceCateItemLink.setItems(resourceCatItemLinkMstList);
    
    System.out.print(resourceCatItemLinkMstList.size()+"observable list size isssssssssssss"); 
    filltable();
    }else {
    	  notifyMessage(5,"Duplicate Entry!!!");
    
    }}
}
public void notifyMessage(int duration, String msg) {
	System.out.println("OK Event Receid");

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}

private void filltable()
{
	
	clItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
	
	clResourceCat.setCellValueFactory(cellData -> cellData.getValue().getResourceCatNameProperty());
	
					
}
@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload();
		}


private void PageReload() {
	
}
}
