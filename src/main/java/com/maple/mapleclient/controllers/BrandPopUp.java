package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BrandMst;
import com.maple.mapleclient.entity.ProductMst;
import com.maple.mapleclient.events.BrandEvent;
import com.maple.mapleclient.events.ProductEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class BrandPopUp {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
    private ObservableList<BrandMst> brandList = FXCollections.observableArrayList();
    BrandEvent brandEvent ;
    @FXML
    private TextField txtBrandName;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;

    @FXML
    private TableView<BrandMst> tbBrand;
    @FXML
    private TableColumn<BrandMst,String> clBrandName;
	StringProperty SearchString = new SimpleStringProperty();
	 @FXML
		private void initialize() {
		 txtBrandName.textProperty().bindBidirectional(SearchString);
		 LoadBrandBySearch("");
	    	brandEvent = new BrandEvent();
	    	tbBrand.setItems(brandList);
	    	clBrandName.setCellValueFactory(cellData -> cellData.getValue().getbrandNameProperty());
	    	tbBrand.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				    if (newSelection != null) {
				    	if(null!=newSelection.getId()&& newSelection.getBrandName().length()>0) {
				    		brandEvent.setId(newSelection.getId());
				    		brandEvent.setBrandName(newSelection.getBrandName());
				    	}
				    }
				});
	    	
	    	 
	   	 SearchString.addListener(new ChangeListener(){
					@Override
					public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					 					
						LoadBrandBySearch((String)newValue);
						
					}
		        });
	    }
    @FXML
    void actionCance(ActionEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
    void actionOk(ActionEvent event) {
     	if(null != brandEvent.getId())
    	{
    	eventBus.post(brandEvent); 
    	}
    	else
    	{
    		brandEvent.setBrandName(txtBrandName.getText());
    		eventBus.post(brandEvent);
    	}
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
    void txtKeyPress(KeyEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	if (event.getCode() == KeyCode.ENTER)
    
    	{
    	if(null != brandEvent.getId())
    	{
    	eventBus.post(brandEvent); 
    	stage.close();
    	}
    	else
    	{
    		brandEvent.setBrandName(txtBrandName.getText());
    		eventBus.post(brandEvent);
    		stage.close();
    	}
    	}
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tbBrand.requestFocus();
    		tbBrand.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			stage.close();
		}
    }

    @FXML
    void tblOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			
    		Stage stage = (Stage) btnOk.getScene().getWindow();
      
        	if(null != brandEvent.getId())
        	{
        	eventBus.post(brandEvent); 
        	stage.close();
        	}
        	else
        	{
        		brandEvent.setBrandName(txtBrandName.getText());
        		eventBus.post(brandEvent);
        		stage.close();
        	}
		}
    
    }
    private void LoadBrandBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ArrayList brand = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		brandList.clear();

		brand = RestCaller.SearchBrandByName(searchData);
		String brandName;
		String id;
		Iterator itr = brand.iterator();
		System.out.println("accaccaccacc22");
		while (itr.hasNext()) {
			
			 List element = (List) itr.next();
			  brandName = (String) element.get(1);
			  id = (String) element.get(0);
			 if (null != id) {
					System.out.println("accaccaccacc44");
					BrandMst brandMst = new BrandMst();
					brandMst.setId(id);
					brandMst.setBrandName(brandName);
					brandList.add(brandMst);
		}
    }
		return;
}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


       private void PageReload() {
       	
 }

}