package com.maple.mapleclient.controllers;

import java.util.Date;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class TallyRetryCtl {

	PurchaseHdr purchaseHdr;

	@FXML
	private Button btnRetry;

	@FXML
	private DatePicker dpStartingDate;

	

	@FXML
	private TextField txtTallyIp;

	@FXML
	private ChoiceBox<String> cmbVoucherType;

	@FXML
	private void initialize() {
		dpStartingDate = SystemSetting.datePickerFormat(dpStartingDate, "dd/MMM/yyyy");
		cmbVoucherType.getItems().add("SALES");
		cmbVoucherType.getItems().add("RECEIPT");
		cmbVoucherType.getItems().add("PAYMENT");
		cmbVoucherType.getItems().add("JOURNAL");
		cmbVoucherType.getItems().add("PURCHASE");

		txtTallyIp.setText(SystemSetting.tallyServer);

	}

	@FXML
	void Retry(ActionEvent event) {

		if (null == dpStartingDate.getValue()) {

			notifyMessage(5, "Please select starting date", false);
			dpStartingDate.requestFocus();
			return;

		}
		if (null == cmbVoucherType.getValue())

		{

			notifyMessage(5, "Please select voucher type", false);
			cmbVoucherType.requestFocus();
			return;

		}

		Date dpStartDate = SystemSetting.localToUtilDate(dpStartingDate.getValue());
		String startDate = SystemSetting.UtilDateToString(dpStartDate, "yyyy-MM-dd");

		String voucherType = cmbVoucherType.getSelectionModel().getSelectedItem().toString();

		
			if (voucherType.equalsIgnoreCase("RECEIPT")) {
				ReceiptToTally(startDate);
			}
    		else if(voucherType.equalsIgnoreCase("PAYMENT"))
    		{	
    			PaymentToTally(startDate);
			}
    		else if(cmbVoucherType.getValue() == "SALES")
    		{	
    				salesToTallyRetry(voucherType,startDate);
    		}
    		else if(cmbVoucherType.getValue() == "JOURNAL")
    			{	
	    			journalToTallyRetry(voucherType,startDate);
				}
	    		else if(cmbVoucherType.getValue() == "PURCHASE")
	    		{	
	    			purchaseToTallyRetry(voucherType,startDate);
				}
	    		
	    		
		    		
	    		

		}
	



	private void purchaseToTallyRetry(String voucherType, String startDate) {
		ResponseEntity<String> purchaseToTallyResp = RestCaller.purchaseToTally(startDate);
		
	}

	private void journalToTallyRetry(String voucherType, String startDate) {
		ResponseEntity<String> journalToTallyResp = RestCaller.journalToTally(startDate);
		
	}

	private void salesToTallyRetry(String voucherType, String startDate) {
		ResponseEntity<String> salesToTallyResp = RestCaller.salesToTally(startDate);
		
	}

	private void PaymentToTally(String startDate) {
		ResponseEntity<String> paymentToTallyResp = RestCaller.PaymentToTally(startDate);
		
	}

	private void ReceiptToTally(String startDate) {

		ResponseEntity<String> receiptToTallyResp = RestCaller.ReceiptToTally(startDate);
//		String receiptToTallyStatus = receiptToTallyResp.getBody();

	}

	

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
}
