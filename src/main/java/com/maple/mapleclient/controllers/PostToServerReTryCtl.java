package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.LmsQueueMst;
import com.maple.mapleclient.entity.LmsQueueTallyMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;

public class PostToServerReTryCtl {
	
	String taskid;
	String processInstanceId;

	
	private ObservableList<LmsQueueMst> lmsqList = FXCollections.observableArrayList();
	//--------------------- version 1.1	

    @FXML
    private DatePicker dpStartDate;

    @FXML
    private DatePicker dpEndDate;

    @FXML
    private Button btnFetchData;

    @FXML
    private ComboBox<String> cmbVoucherType;

    @FXML
    private Button btnReTry;
    
 

    @FXML
    private TableView<LmsQueueMst> tblReport;

    @FXML
    private TableColumn<LmsQueueMst, String> clVoucherNo;

    @FXML
    private TableColumn<LmsQueueMst, String> clVoucherDate;

    @FXML
    private TableColumn<LmsQueueMst, String> clBranch;

    @FXML
    private TableColumn<LmsQueueMst, String> clPostToServer;
    
    @FXML
   	private void initialize() {
    	
    	dpEndDate = SystemSetting.datePickerFormat(dpEndDate, "dd/MMM/yyyy");
    	dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
    	//cmbVoucherType.getItems().add("forwardAccountHeads");
    	//cmbVoucherType.getItems().add("forwardAccountPayable");
    	//cmbVoucherType.getItems().add("forwardCustomer");
    	cmbVoucherType.getItems().add("forwardDamageEntry");
    	cmbVoucherType.getItems().add("forwardDayEndReportStore");
    	cmbVoucherType.getItems().add("forwardItemBatchDtl");
    	cmbVoucherType.getItems().add("forwardOtherBranchSales");
    	//cmbVoucherType.getItems().add("forwardItemMerge");
    	//cmbVoucherType.getItems().add("forwardJournal");
    	//cmbVoucherType.getItems().add("forwardKitDefinition");
    	cmbVoucherType.getItems().add("forwardPayment");
    	//cmbVoucherType.getItems().add("forwardPaymentAccounting");
    	//cmbVoucherType.getItems().add("forwardPriceDefinition");
    	cmbVoucherType.getItems().add("forwardProduction");
    	cmbVoucherType.getItems().add("forwardPurchase");
    	//cmbVoucherType.getItems().add("forwardPurchaseAccounting");
    	cmbVoucherType.getItems().add("forwardReceipt");
    	//cmbVoucherType.getItems().add("forwardReceiptAccounting");
       	cmbVoucherType.getItems().add("forwardSales");
       ///	cmbVoucherType.getItems().add("forwardSalesAccounting");
       	cmbVoucherType.getItems().add("forwardSaleOrder");
       //	cmbVoucherType.getItems().add("forwardSupplier");
       //	cmbVoucherType.getItems().add("itemsave");
       	cmbVoucherType.getItems().add("productcoversion");
    	cmbVoucherType.getItems().add("productionprocess");
    	
    	tblReport.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


       }


    @FXML
    void FetchData(ActionEvent event) {
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(5, "Please select start date", false);
    		dpStartDate.requestFocus();
    		return;
    	}
    	
    	if(null == dpEndDate.getValue())
    	{
    		notifyMessage(5, "Please select end date", false);
    		dpEndDate.requestFocus();
    		return;
    	}

    	if(null == cmbVoucherType.getSelectionModel().getSelectedItem())
    	{
    		cmbVoucherType.requestFocus();
    		notifyMessage(2, "Please select voucher type", false);
    	}
    	
    	Date date =SystemSetting.localToUtilDate(dpStartDate.getValue());
    	String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
     	
    	Date edate =SystemSetting.localToUtilDate(dpEndDate.getValue());
    	String enddate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");
    	
    	tblReport.getItems().clear();
    	{
    		ResponseEntity<List<LmsQueueMst>> lmssQResp = RestCaller.findAllLmsQueueMstByVoucherTypeBetweenDate(
        			sdate,enddate,cmbVoucherType.getSelectionModel().getSelectedItem().toString());
        	
        	lmsqList = FXCollections.observableArrayList(lmssQResp.getBody());
    	}
    	
    	FillTable();

    }

    private void FillTable() {
    	
    	tblReport.setItems(lmsqList);
		clBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());
		clPostToServer.setCellValueFactory(cellData -> cellData.getValue().getPostedToServerProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());

	}


	@FXML
    void ReTry(ActionEvent event) {
		
		List<String> ids = new ArrayList<String>();
		java.util.Date sDate = java.sql.Date.valueOf(dpStartDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sDate, "yyy-MM-dd");
		
		java.util.Date eDate = java.sql.Date.valueOf(dpEndDate.getValue());
		String endtDate = SystemSetting.UtilDateToString(eDate, "yyy-MM-dd");
    	
    	int selesctedItemsNo = tblReport.getSelectionModel().getSelectedItems().size();

		for(int i=0;i<selesctedItemsNo;i++)
		{

			String voucherNo = tblReport.getSelectionModel().getSelectedItems().get(i).getId();
			ids.add(voucherNo);
		}
		
		for(String str : ids)
		{
			ResponseEntity<LmsQueueMst> lmsQResp = RestCaller.findLmsQueueMstById(str);
			LmsQueueMst lmsQueueMst = lmsQResp.getBody();
			if(null != lmsQueueMst)
			{
				
				

		    	ResponseEntity<String> getPedingItems = RestCaller.
		    			getLmsQueueMstRetry(lmsQResp.getBody().getVoucherType(),lmsQResp.getBody().getPostedToServer(),startDate,endtDate);
//				lmsQueueMst.setPostedToServer("NO");
//				RestCaller.updateLmsQueueMst(lmsQueueMst);
			}
		}
		
		tblReport.getItems().clear();
		dpStartDate.setValue(null);
		dpEndDate.setValue(null);
		

    }
    
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }

}
