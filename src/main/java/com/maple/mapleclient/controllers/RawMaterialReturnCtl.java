package com.maple.mapleclient.controllers;

import java.io.IOException;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.RawMaterialIssueDtl;
import com.maple.mapleclient.entity.RawMaterialReturnDtl;
import com.maple.mapleclient.entity.RawMaterialReturnHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class RawMaterialReturnCtl {
	
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<RawMaterialReturnDtl> rawMaterilaListTable = FXCollections.observableArrayList();
	RawMaterialReturnHdr rawMaterialReturnHdr = null;
	RawMaterialReturnDtl rawMaterialReturnDtl = null;
    @FXML
    private DatePicker dpDate;

    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtBatch;

    @FXML
    private TextField txtQty;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnFinalSave;

    @FXML
    private TextField txtUnit;

    @FXML
    private TableView<RawMaterialReturnDtl> tblItems;

    @FXML
    private TableColumn<RawMaterialReturnDtl, String> clItemName;

    @FXML
    private TableColumn<RawMaterialReturnDtl, String> clBatch;

    @FXML
    private TableColumn<RawMaterialReturnDtl, Number> clQty;

    @FXML
    private TableColumn<RawMaterialReturnDtl, String> clUnit;
    @FXML
   	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
   		eventBus.register(this);
   		tblItems.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					
					rawMaterialReturnDtl = new RawMaterialReturnDtl();
					rawMaterialReturnDtl.setId(newSelection.getId());
				}

			}
		});
		
    }
    @FXML
    void addItems(ActionEvent event) {

    	addItems();
    }
    
    private void fillTable()
    {
    	for(RawMaterialReturnDtl rawmatDtl :rawMaterilaListTable)
    	{
    		ResponseEntity<ItemMst> items = RestCaller.getitemMst(rawmatDtl.getItemId());
    		rawmatDtl.setItemName(items.getBody().getItemName());
    		ResponseEntity<UnitMst> unit = RestCaller.getunitMst(rawmatDtl.getUnitId());
    		rawmatDtl.setUnitName(unit.getBody().getUnitName());
    	}
    	tblItems.setItems(rawMaterilaListTable);
    	clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
    	clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
    }
    @Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtItemName.setText(itemPopupEvent.getItemName());
		//	itemmst.setId(itemPopupEvent.getItemId());
			txtBatch.setText(itemPopupEvent.getBatch());
			txtUnit.setText(itemPopupEvent.getUnitName());
				}
			});
		}
		txtQty.requestFocus(); // ===to automaticaly focus on quantity after popup dtls entered//
	}
    private void loadItemPopup() {
		/*
		 * Function to display popup window and show list of items to select.
		 */
		System.out.println("ssssssss=====loadItemPopup=====sssssssssss");

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    @FXML
    void deleteItems(ActionEvent event) {
    	if(null != rawMaterialReturnDtl)
    	{
    		if(null != rawMaterialReturnDtl.getId())
    		{
    			RestCaller.deleteRawmaterialReturnDtl(rawMaterialReturnDtl.getId());
    			ResponseEntity<List<RawMaterialReturnDtl>> rawMaterialReturnSaved = RestCaller.getRawMaterialReturnDtl(rawMaterialReturnHdr.getId());
    			rawMaterilaListTable = FXCollections.observableArrayList(rawMaterialReturnSaved.getBody());
    			fillTable();
    			notifyMessage(5, "Deleted!!!");
    		}
    	}
    	
    }

    @FXML
    void finalSave(ActionEvent event) {
    	finalSave();
    
    }
    @FXML
    void saveOnKey(KeyEvent event) {
    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
    		finalSave();
    		}
    }
    private void finalSave()
    {
    	
    	ResponseEntity<List<RawMaterialReturnDtl>> rawMaterialReturnSaved = RestCaller.getRawMaterialReturnDtl(rawMaterialReturnHdr.getId());
    	if(null ==rawMaterialReturnSaved.getBody())
    	{
    		return;
    	}
    	
    		String vNo = RestCaller.getVoucherNumber("RAWMATRETURN");
    		rawMaterialReturnHdr.setVoucherNumber(vNo);
    		RestCaller.updateRawMaterialReturnHdr(rawMaterialReturnHdr);
    		notifyMessage(5, "Saved!!!");
    		String rdate =SystemSetting.UtilDateToString(rawMaterialReturnHdr.getVoucherDate(), "yyyy-MM-dd") ;
        	
    		
    		try {
    			
				JasperPdfReportService.RawMaterialReturn(rawMaterialReturnHdr.getVoucherNumber(),rdate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
    
    		rawMaterialReturnDtl = null;
    		rawMaterialReturnHdr = null;
    		tblItems.getItems().clear();
    		rawMaterilaListTable.clear();
    }
    @FXML
    void qtyOnenter(KeyEvent event) {

    	if(event.getCode() == KeyCode.ENTER)
    	{
    		
    		addItems();
    	}
    }
    private void addItems()
    {
    	if(txtItemName.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Type ItemName");
    		txtItemName.requestFocus();
    		return;
    	}
    	else if(txtQty.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Type Qty");
    		txtQty.requestFocus();
    		return;
    	}

    	else if(null == rawMaterialReturnHdr)
    	{
    		rawMaterialReturnHdr = new RawMaterialReturnHdr();
    		
    		rawMaterialReturnHdr.setVoucherDate(Date.valueOf(dpDate.getValue()));
    		ResponseEntity<RawMaterialReturnHdr> respentity = RestCaller.saveRawMaterialReturnHdr(rawMaterialReturnHdr);
    		rawMaterialReturnHdr = respentity.getBody();
    	}
    	else
    	{
    	rawMaterialReturnDtl = new RawMaterialReturnDtl();
    	rawMaterialReturnDtl.setBatch(txtBatch.getText());
    	rawMaterialReturnDtl.setItemName(txtItemName.getText());
		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(rawMaterialReturnDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
    	rawMaterialReturnDtl.setItemId(item.getId());
    	rawMaterialReturnDtl.setUnitName(txtUnit.getText());
    	rawMaterialReturnDtl.setQty(Double.parseDouble(txtQty.getText()));
    	ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());
    	rawMaterialReturnDtl.setUnitId(unitMst.getBody().getId());
    	rawMaterialReturnDtl.setRawMaterialReturnHdr(rawMaterialReturnHdr);
    	ResponseEntity<RawMaterialReturnDtl> respentity = RestCaller.saveRawMaterialReturnDtl(rawMaterialReturnDtl);
    	rawMaterialReturnDtl = respentity.getBody();
    	rawMaterilaListTable.add(rawMaterialReturnDtl);
    	fillTable();
    	txtBatch.clear();
    	txtItemName.clear();
    	txtQty.clear();
    	txtUnit.clear();
    	txtItemName.requestFocus();
    	}
    }
    @FXML
    void itemOnEnter(KeyEvent event) {
    	if(event.getCode() == KeyCode.ENTER)
    	{
    		loadItemPopup();
    	}
    }
    @FXML
    void itemOnClick(MouseEvent event) {

    	loadItemPopup();
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }
}
