package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;


public class BranchWiseProfitNew {
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerate;

    @FXML
    private TableView<?> tblBranchWiseProfit;

    @FXML
    private TableColumn<?, ?> clBranchName;

    @FXML
    private TableColumn<?, ?> clProfit;

    @FXML
    void BackOnKeyPress(KeyEvent event) {

    }

    @FXML
    void GenerateReport(ActionEvent event) {

    }
    @FXML
   	private void initialize() {
       	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
       	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


       private void PageReload() {
       	
 }

}
