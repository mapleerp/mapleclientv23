package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.DayEndClosureDtl;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.StockVerificationMst;
import com.maple.mapleclient.entity.SysDateMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;
import net.sf.jasperreports.engine.JRException;

public class DayEndClosureCtl {
	String taskid;
	String processInstanceId;

	DayEndClosureHdr dayEndClosure = null;
	DayEndClosureDtl dayEndClosureDtl = null;
	DayEndClosureDtl dayEndClosureDtlToDelete = null;
	private ObservableList<DayEndClosureDtl> dayEndClosureListTable = FXCollections.observableArrayList();

	Integer stock_count = SystemSetting.STOCK_VERIFICATION_COUNT;
	@FXML
	private TextField txtCashSale;

	@FXML
	private TextField txtCardSale;

	@FXML
	private TextField txtPhysicalCash;
	@FXML
	private TextField txtChangeInCash;
	@FXML
	private Button btnSave;

	@FXML
	private TextField txtDenomination;

	@FXML
	private TextField txtCount;

	@FXML
	private Button btnAdd;

	@FXML
	private TableView<DayEndClosureDtl> tblDayEnd;

	@FXML
	private TableColumn<DayEndClosureDtl, Number> clDenomination;

	@FXML
	private TableColumn<DayEndClosureDtl, Number> clCount;

	@FXML
	private DatePicker dpProcessDate;

	@FXML
	private Button btnFetch;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnClear;

	@FXML
	private Button report;

	@FXML
	void ShowReport(ActionEvent event) {

		if (null != dpProcessDate.getValue()) {

			LocalDate dpDate = dpProcessDate.getValue();

			java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
			String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

//			try {
//				JasperPdfReportService.DayEndReportNew(strDate);
//			} catch (JRException e) {
			// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} else {

//			notifyMessage(5, " Please select date...!!!");
//			return;
			// }
		}
	}

	@FXML
	private void initialize() {

		tblDayEnd.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					dayEndClosureDtlToDelete = new DayEndClosureDtl();

					dayEndClosureDtlToDelete.setId(newSelection.getId());

				}
			}
		});

	}

	@FXML
	void AddDayEndDtl(ActionEvent event) {

		addDayEndClosure();

	}

	private void addDayEndClosure() {

		if (null == dpProcessDate.getValue()) {
			notifyMessage(5, " Please select process date...!!!");
			dpProcessDate.requestFocus();
			return;
		}
		if (txtDenomination.getText().trim().isEmpty()) {
			notifyMessage(5, " Please enter Denomination...!!!");
			txtDenomination.requestFocus();
			return;
		}
		if (txtCount.getText().trim().isEmpty()) {
			notifyMessage(5, " Please enter Count...!!!");
			txtCount.requestFocus();
			return;
		}
		LocalDate dpDate = dpProcessDate.getValue();
		java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
		if (null != dayEndClosuredtlSaved.getBody()) {
			if (null != dayEndClosuredtlSaved.getBody().getDayEndStatus()) {
				notifyMessage(5, "Day End Already Done");
				txtDenomination.clear();
				txtCount.clear();

				return;
			}
		}

		ResponseEntity<DayEndClosureHdr> dayEndresp = RestCaller.getDayEndClosureByDate(strDate);
		dayEndClosure = dayEndresp.getBody();
		if (null != dayEndClosure) {
			// btnAdd.setDisable(true);
			// btnDelete.setDisable(true);
			// btnSave.setDisable(true);
			BigDecimal cardSale = new BigDecimal(dayEndClosure.getCardSale());
			cardSale = cardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtCardSale.setText(cardSale.toPlainString());
			// txtCardSale2.setText(Double.toString(dayEndClosure.getCardSale2()));
			BigDecimal cashSale = new BigDecimal(dayEndClosure.getCashSale());
			cashSale = cashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			txtCashSale.setText(cashSale.toPlainString());
			// txtPhysicalCash.setText(Double.toString(dayEndClosure.getPhysicalCash()));
			getDayEndDtlByHdrId();

		}
		if (null == dayEndClosure) {

//			ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
//			if (null != dayEndClosuredtlSaved.getBody()) {
//				if (null != dayEndClosuredtlSaved.getBody().getDayEndStatus()) {
//					notifyMessage(5, "Day End Already Done");
//					txtDenomination.clear();
//					txtCount.clear();
//
//					return;
//				}
//			}
			dayEndClosure = new DayEndClosureHdr();

			Date date = Date.valueOf(dpProcessDate.getValue());
			dayEndClosure.setBranchCode(SystemSetting.getSystemBranch());
			dayEndClosure.setUserId(SystemSetting.getUser().getId());
			dayEndClosure.setProcessDate(date);

			ResponseEntity<DayEndClosureHdr> respentity = RestCaller.saveDayEndClosureHdr(dayEndClosure);
			dayEndClosure = respentity.getBody();
		}

		dayEndClosureDtl = new DayEndClosureDtl();
		dayEndClosureDtl.setDayEndClosure(dayEndClosure);
		dayEndClosureDtl.setDenomination(Double.parseDouble(txtDenomination.getText()));
		dayEndClosureDtl.setCount(Double.parseDouble(txtCount.getText()));
		dayEndClosure.setChangeInCash(Double.parseDouble(txtChangeInCash.getText()));
		ResponseEntity<DayEndClosureDtl> respentity = RestCaller.saveDayEndClosureDtl(dayEndClosureDtl);
		dayEndClosureDtl = respentity.getBody();

		dayEndClosureListTable.add(dayEndClosureDtl);
		FillTable();
		// dayEndClosureDtl = null;
		txtDenomination.clear();
		txtCount.clear();
		txtDenomination.requestFocus();
		notifyMessage(5, "Saved!!");

	}

	@FXML
	void FetchData(ActionEvent event) {

		LocalDate dpDate = dpProcessDate.getValue();
		System.out.println(dpProcessDate.getValue()
				+ "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
		java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
		String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
		dayEndClosure = dayEndClosuredtlSaved.getBody();
		if (null != dayEndClosure) {
			// btnAdd.setDisable(true);
			// btnDelete.setDisable(true);
			// btnSave.setDisable(true);
			
			 if(null !=dayEndClosure.getDayEndStatus()) {
				 BigDecimal cardSale = new BigDecimal(dayEndClosure.getCardSale());
					cardSale = cardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
					txtCardSale.setText(cardSale.toPlainString());
					// txtCardSale2.setText(Double.toString(dayEndClosure.getCardSale2()));
					BigDecimal cashSale = new BigDecimal(dayEndClosure.getCashSale());
					cashSale = cashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
					txtCashSale.setText(cashSale.toPlainString());
					// txtPhysicalCash.setText(Double.toString(dayEndClosure.getPhysicalCash()));
					
			 }
			 getDayEndDtlByHdrId(); 
			
			

		} else {

			tblDayEnd.getItems().clear();
			notifyMessage(5, " No data to show...!!!");
			txtCardSale.clear();

			txtCashSale.clear();
			txtCount.clear();
			txtDenomination.clear();
			txtPhysicalCash.clear();
		}

		/*
		 * Call Jasper Report for Day end here
		 */

	}

	@FXML
	void FinalSave(ActionEvent event) {
		if (stock_count > 0) {
			LocalDate dpDate = dpProcessDate.getValue();
			java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
			String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

			ResponseEntity<StockVerificationMst> stockMst = RestCaller.getStockVerificationMst(strDate);
			if (null != stockMst.getBody()) {
				notifyMessage(5, "Please Enter the Stock Verification Details");
				return;
			}
		}

		if (null != dayEndClosure) {

			LocalDate dpDate = dpProcessDate.getValue();
			java.util.Date uDate = SystemSetting.localToUtilDate(dpDate);
			String strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

			ResponseEntity<DayEndClosureHdr> totalCashAndCardSale = RestCaller.getTotalCashAndCardSale(strDate);
			DayEndClosureHdr dayEndClosureHdr = new DayEndClosureHdr();
			dayEndClosureHdr = totalCashAndCardSale.getBody();
			if (null != dayEndClosureHdr) {
				dayEndClosure.setCardSale(dayEndClosureHdr.getCardSale());
				dayEndClosure.setCardSale2(dayEndClosureHdr.getCardSale2());
				dayEndClosure.setCashSale(dayEndClosureHdr.getCashSale());
			}

			dayEndClosure.setPhysicalCash(Double.parseDouble(txtPhysicalCash.getText()));

//			dayEndClosure.setCardSale(Double.parseDouble(txtCashSale.getText()));
//			dayEndClosure.setCardSale(Double.parseDouble(txtCardSale.getText()));

			dayEndClosure.setChangeInCash(Double.parseDouble(txtChangeInCash.getText()));
			dayEndClosure.setDayEndStatus("DONE");
			
			
			// Delete salestranshdr with voucher number null when day end final save
						RestCaller.deleteSalesTransHdrVoucherNull();
			
						RestCaller.updateDayEndClosure(dayEndClosure);

			ResponseEntity<SysDateMst> respetnity = RestCaller.getSysDate(SystemSetting.systemBranch);
//			if(null == respetnity.getBody())
//			{
//				SysDateMst sysDateMst = new SysDateMst();
//				if(null != sysDateMst.getAplicationDate())
//				{
//				LocalDate ldate =SystemSetting.utilToLocaDate(sysDateMst.getAplicationDate());
//				
//				sysDateMst.setAplicationDate(SystemSetting.localToUtilDate(ldate.plusDays(1)));
//				}
//				else
//				{
//					sysDateMst.setAplicationDate(Date.valueOf(LocalDate.now().plusDays(1)));
//				}
//				sysDateMst.setBranchCode(SystemSetting.systemBranch);
//				sysDateMst.setSystemDate(Date.valueOf(LocalDate.now()));
//				ResponseEntity<SysDateMst> respentity = RestCaller.saveSysDateMst(sysDateMst);
//				
//			}
//			else
//			{
			SysDateMst sysDateMst = new SysDateMst();
			sysDateMst = respetnity.getBody();
//				LocalDate ldate =SystemSetting.utilToLocaDate(dpProcessDate.getValue());
			sysDateMst.setAplicationDate(SystemSetting.localToUtilDate(dpProcessDate.getValue().plusDays(1)));
			sysDateMst.setSystemDate(Date.valueOf(LocalDate.now()));
			sysDateMst.setBranchCode(SystemSetting.systemBranch);
			RestCaller.updateSysDate(sysDateMst);
//			}
			dayEndClosure = null;
			dpProcessDate.setValue(null);
			tblDayEnd.getItems().clear();
			txtPhysicalCash.clear();

			
			

		}
	}

	@FXML
	void DeleteDayEndDtl(ActionEvent event) {

		if (null != dayEndClosureDtlToDelete) {
			RestCaller.dayEndClosureDtlDelete(dayEndClosureDtlToDelete.getId());

			getDayEndDtlByHdrId();

		}

	}

	private void getDayEndDtlByHdrId() {

		tblDayEnd.getItems().clear();

		ArrayList pur = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		pur = RestCaller.getDayEndDtlByHdrId(dayEndClosure.getId());
		Iterator itr = pur.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object denomination = lm.get("denomination");
			Object count = lm.get("count");
			Object id = lm.get("id");
			if (id != null) {

				dayEndClosureDtl = new DayEndClosureDtl();
				dayEndClosureDtl.setId((String) id);
				dayEndClosureDtl.setDenomination((Double) denomination);
				dayEndClosureDtl.setCount((Double) count);

				dayEndClosureListTable.add(dayEndClosureDtl);

			}
		}
		FillTable();

	}

	@FXML
	void FocusOnAddButton(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnAdd.requestFocus();
		}

	}

	@FXML
	void FocusOnCount(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtCount.requestFocus();
		}

	}

	@FXML
	void FocusOnDenomination(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtDenomination.requestFocus();
		}

	}

	@FXML
	void addItem(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addDayEndClosure();
		}
	}

	@FXML
	void ClearAll(ActionEvent event) {

		tblDayEnd.getItems().clear();
		btnAdd.setDisable(false);
		btnDelete.setDisable(false);
		btnSave.setDisable(false);
		txtCardSale.clear();
		txtPhysicalCash.clear();

		txtCashSale.clear();
		txtCount.clear();
		txtDenomination.clear();
		dpProcessDate.setValue(null);
		dayEndClosure = null;
		dayEndClosureDtl = null;

	}

	private void FillTable() {

		tblDayEnd.setItems(dayEndClosureListTable);
		clDenomination.setCellValueFactory(cellData -> cellData.getValue().getDenominationProperty());
		clCount.setCellValueFactory(cellData -> cellData.getValue().getCountProperty());

		Double sum = RestCaller.getSummaryDayEndClosure(dayEndClosure.getId());
		if (null != sum) {
			txtPhysicalCash.setText(Double.toString(sum));
		} else {
			txtPhysicalCash.setText("");
		}

	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
}
