package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.HsnCodeSaleReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class HsnCodePurchaseReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<HsnCodeSaleReport> HsnCodeSaleReportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private TableView<HsnCodeSaleReport> tblHSn;

    @FXML
    private TableColumn<HsnCodeSaleReport, String> clvoucherDate;

    @FXML
    private TableColumn<HsnCodeSaleReport, String> clHsnCode;

    @FXML
    private TableColumn<HsnCodeSaleReport, Number> clTotalQty;

    @FXML
    private TableColumn<HsnCodeSaleReport, Number> clCgst;

    @FXML
    private TableColumn<HsnCodeSaleReport, Number> clValue;

    @FXML
    void actionPrint(ActionEvent event) {
    	LocalDate dpFDate = dpFromDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpFDate);
		 String  strfDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 
		 
		 LocalDate dpTDate = dpToDate.getValue();
			java.util.Date utDate  = SystemSetting.localToUtilDate(dpTDate);
			 String  strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
			 try {
					JasperPdfReportService.HsnCodePurchaseReport(strfDate,strtDate);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    }

    @FXML
    void actionShow(ActionEvent event) {
    	LocalDate dpFDate = dpFromDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpFDate);
		 String  strfDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 
		 
		 LocalDate dpTDate = dpToDate.getValue();
			java.util.Date utDate  = SystemSetting.localToUtilDate(dpTDate);
			 String  strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
		ResponseEntity<List<HsnCodeSaleReport>> gstHsnCodeSaleReport = RestCaller.getHsnCodePuchaseReport(strfDate,strtDate);
		HsnCodeSaleReportList = FXCollections.observableArrayList(gstHsnCodeSaleReport.getBody());
		fillTable();
    }
    private void fillTable()
    {
    	tblHSn.setItems(HsnCodeSaleReportList);
    	for(HsnCodeSaleReport hsnReport:HsnCodeSaleReportList)
    	{
    		
    		BigDecimal cgst = new BigDecimal(hsnReport.getCgst());
    		cgst = cgst.setScale(0, BigDecimal.ROUND_HALF_EVEN);
    		hsnReport.setCgst(cgst.doubleValue());
    		
    		BigDecimal qty = new BigDecimal(hsnReport.getQty());
    		qty = qty.setScale(0, BigDecimal.ROUND_HALF_EVEN);
    		hsnReport.setQty(qty.doubleValue());
    		
    		BigDecimal value = new BigDecimal(hsnReport.getValue());
    		value = value.setScale(0, BigDecimal.ROUND_HALF_EVEN);
    		hsnReport.setValue(value.doubleValue());
    	}
		clCgst.setCellValueFactory(cellData -> cellData.getValue().getcgstProperty());
		clHsnCode.setCellValueFactory(cellData -> cellData.getValue().gethsnCodeProperty());
		clTotalQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getvalueProperty());
		clvoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());


    }
    @FXML
   	private void initialize() {
       	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
       	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }


}
