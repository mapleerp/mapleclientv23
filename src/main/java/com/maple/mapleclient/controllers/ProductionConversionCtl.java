package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.IntentDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.ProductConversionConfigMst;
import com.maple.mapleclient.entity.ProductConversionDtl;
import com.maple.mapleclient.entity.ProductConversionMst;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class ProductionConversionCtl {
	
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ProductConversionDtl> productionDtlList = FXCollections.observableArrayList();

	ItemMst itemMstfrom = null;
	ItemMst itemMstTo = null;
	ProductConversionMst productConversionMst = null;
	ProductConversionDtl productConversionDtl = null;
	ProductConversionDtl productdtldelete = null;
    @FXML
    private DatePicker dpConversionDate;

    @FXML
    private TextField txtFromItem;

    @FXML
    private TextField txtFromQty;

    @FXML
    private TextField txtToItem;

    @FXML
    private TextField txtToQty;

    @FXML
    private Button btnSave;


    @FXML
    private TextField txtBatch;
    
    @FXML
    private Button btnDelete;
    
    

    @FXML
    private TableView<ProductConversionDtl> tbConversion;

    @FXML
    private TableColumn<ProductConversionDtl, String> clFromItem;

    @FXML
    private TableColumn<ProductConversionDtl, Number> clQty;

    @FXML
    private TableColumn<ProductConversionDtl, String> clToItem;


    @FXML
    private TableColumn<ProductConversionDtl, Number> clToQty;
    
    @FXML
    private TableColumn<ProductConversionDtl, String> clBatch;
    
    @FXML
    private Button btnFinalSave;
    
    @FXML
    void dateOnKeyPress(KeyEvent event) {
    	
    	
    	if (event.getCode() == KeyCode.ENTER) {
    	
    	txtFromItem.requestFocus();
    	}

    }

    @FXML
    void DeleteProducts(ActionEvent event) {
    	if(null != productdtldelete)
    	{
    	if(null!=productdtldelete.getId())
    	{
    		RestCaller.deleteProductConversionDtl(productdtldelete.getId());
    		notifyMessage(5,"Item Deleted!!!");
    		tbConversion.getItems().clear();
    		ResponseEntity<List<ProductConversionDtl>> dtlSaved = RestCaller.getProductConversionDtl(productConversionMst);
    		productionDtlList = FXCollections.observableArrayList(dtlSaved.getBody());
    		for (ProductConversionDtl prod :   productionDtlList) {
				
				ResponseEntity<ItemMst> respentityfrom = RestCaller.getitemMst(prod.getFromItem());
				prod.setFromitemName(respentityfrom.getBody().getItemName());
				ResponseEntity<ItemMst> respentityto = RestCaller.getitemMst(prod.getToItem());
				prod.setToItemName(respentityto.getBody().getItemName());
			}	
    		tbConversion.setItems(productionDtlList);
    		clearfields();
    		productdtldelete = null;
    	}
    	}
    	else
    	{
    		notifyMessage(5,"Select From Table!!!");
    	}
    		
    }

    @FXML
    void FinalSave(ActionEvent event) {
    
    	finalSave();

    }

    @FXML
    void ItemPopup(MouseEvent event) {

    	loadItemPopup();
    }
    @FXML
    void onEnterSave(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		savedata();

		}
    }
    @FXML
    void SaveProducts(ActionEvent event) {
    	 savedata();
    }

    @FXML
    void itemOnEnter(KeyEvent event) {
    	
    	if((txtFromItem.getText().trim().isEmpty()) && (event.getCode() == KeyCode.ENTER))
    	{
    		loadItemPopup();
    	}
    	else if(event.getCode()== KeyCode.ESCAPE)
    	{
    		btnFinalSave.requestFocus();
    	}
    	
    }

    @FXML
    void qtyOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtToItem.requestFocus();
			showItemMst();

		}
    }

    @FXML
    void toItemOnEnter(KeyEvent event) {
    	if(!(txtToItem.getText().trim().isEmpty()))
    	{
    	if (event.getCode() == KeyCode.ENTER) {
			txtToQty.requestFocus();

		}
    	}
    	else
    	{
    		showItemMst();
    	}
    	
    }
	@FXML
	private void initialize() {
		// btnAdd.setVisible(false);
		dpConversionDate = SystemSetting.datePickerFormat(dpConversionDate, "dd/MMM/yyyy");
		dpConversionDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		dpConversionDate.setEditable(false);		
		eventBus.register(this);
		tbConversion.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					productdtldelete = new ProductConversionDtl();
					productdtldelete.setId(newSelection.getId());
					}
			}
		});
	}
    @FXML
    void toQtyOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();

		}
    }
    @FXML
    void toItemclick(MouseEvent event) {
    	showItemMst();
    }
    @FXML
    void finalSaveOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER)
    	{
    		finalSave();
    	}
    }
    private void showItemMst() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		txtToQty.requestFocus();
	}
    private void loadItemPopup()
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	txtFromQty.requestFocus();

    }
	@Subscribe
	public void popupItemlistner(KitPopupEvent kitPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnDelete.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			itemMstTo = new ItemMst();
			txtToItem.setText(kitPopupEvent.getItemName());
			itemMstTo.setId(kitPopupEvent.getItemId());

		}
			});
	}
	}
	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnDelete.getScene().getWindow();
		if (stage.isShowing()) {

			itemMstfrom = new ItemMst();
			txtFromItem.setText(itemPopupEvent.getItemName());
			itemMstfrom.setId(itemPopupEvent.getItemId());
			txtBatch.setText(itemPopupEvent.getBatch());
		}

	}
	private void clearfields()
	{
		txtFromItem.clear();
		txtFromQty.clear();
		txtToItem.clear();
		txtToQty.clear();
		txtBatch.clear();
	}
	private void filltable()
	{
		tbConversion.setItems(productionDtlList);
		clFromItem.setCellValueFactory(cellData -> cellData.getValue().getfromItemProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getfromQtyProperty());
		clToItem.setCellValueFactory(cellData -> cellData.getValue().gettoItemProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchCodeProperty());
		clToQty.setCellValueFactory(cellData -> cellData.getValue().gettoQtyProperty());
	}
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void getitemName()
	{
		ResponseEntity<ItemMst> respentityfromItem = RestCaller.getitemMst(productConversionDtl.getFromItem());
    	productConversionDtl.setFromitemName(respentityfromItem.getBody().getItemName());
    	ResponseEntity<ItemMst> respentityToItem = RestCaller.getitemMst(productConversionDtl.getToItem());
    	productConversionDtl.setToItemName(respentityToItem.getBody().getItemName());
    	productionDtlList.add(productConversionDtl);
	}
	private void savedata()
	{
		if(txtFromItem.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Type Item!");
    		txtFromItem.requestFocus();
    	}
    	else if(txtFromQty.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Type Qty!");
    		txtFromQty.requestFocus();
    	}
    	else if(txtToItem.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Type Item!");
    		txtToItem.requestFocus();
    	}
    	else if(txtToQty.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"Please Type Qty!");
    		txtToQty.requestFocus();
    	}

    	else 
    	{
    		ArrayList items = new ArrayList();

			items = RestCaller.getSingleStockItemByName(txtFromItem.getText(), txtBatch.getText());
			Double chkQty = 0.0;
			String itemId = null;
			Iterator itr = items.iterator();
			while (itr.hasNext()) {
				List element = (List) itr.next();
				chkQty = (Double) element.get(4);
				itemId = (String) element.get(7);
			}
			if (chkQty < Double.parseDouble(txtFromQty.getText())) {
				notifyMessage(1, "Not in Stock!!!");
				txtFromQty.clear();
				txtFromItem.clear();
				txtFromItem.requestFocus();
				return;
			}

    	if (null ==productConversionMst )
    	{
    		productConversionMst = new ProductConversionMst();
    		productConversionMst.setConversionDate(Date.valueOf(dpConversionDate.getValue()));
    		productConversionMst.setBranchCode(SystemSetting.systemBranch);
    		ResponseEntity<ProductConversionMst > respentity = RestCaller.saveProductConversionMst(productConversionMst);
    		productConversionMst = respentity.getBody();
    		
    	}
    	
    	Double itemsqty = 0.0;
		itemsqty = RestCaller.ProdcutConversionItemQty(productConversionMst.getId(),itemMstfrom.getId(),txtBatch.getText());
		if(chkQty < itemsqty + Double.parseDouble(txtFromQty.getText()))
		{
			notifyMessage(1, "Not in Stock!!!");
			txtFromQty.clear();
			txtFromItem.clear();
			txtFromItem.requestFocus();
			return;
		}
    	productConversionDtl = new ProductConversionDtl();
    	productConversionDtl.setFromItem(itemMstfrom.getId());
    	productConversionDtl.setProductConversionMst(productConversionMst);
    	productConversionDtl.setFromQty(Double.parseDouble(txtFromQty.getText()));
    	productConversionDtl.setToItem(itemMstTo.getId());
    	productConversionDtl.setToQty(Double.parseDouble(txtToQty.getText()));
    	if(!txtBatch.getText().trim().isEmpty())
    	{
    		productConversionDtl.setBatchCode(txtBatch.getText());
    	}
    	else
    		productConversionDtl.setBatchCode("NOBATCH");
    	
    	ResponseEntity<ProductConversionConfigMst> getSavedProduct = RestCaller.getProductConversionConfigMstWithItemIds(itemMstfrom.getId(),itemMstTo.getId());
    	if(null != getSavedProduct.getBody())
    	{
    		ResponseEntity<ProductConversionDtl > respentity = RestCaller.saveProductConversionDtl(productConversionDtl);
        	productConversionDtl = respentity.getBody();
    	}
    	else
    	{
    		notifyMessage(5,"Cannot Convert the Product");
    		clearfields();
    		return;
    	}
    	
    	getitemName();
    	clearfields();
    	notifyMessage(5,"Item Added!!!");
       	filltable();
       	txtFromItem.requestFocus();
//       	loadItemPopup();
    	}
	}
	private void finalSave()
	{
		boolean batch = false;
		String stockVerification = RestCaller.StockVerificationByProductConversion(productConversionMst.getId());
		if(null != stockVerification)
		{
			notifyMessage(5, stockVerification);
			
			//return;

		} 
		else 
		{
		if(null!= productConversionMst)
		{
		ResponseEntity<List<ProductConversionDtl>> dtlSaved = RestCaller.getProductConversionDtl(productConversionMst);
		productionDtlList = FXCollections.observableArrayList(dtlSaved.getBody());
		if (dtlSaved.getBody().size() == 0) {
			notifyMessage(5,"Add Details!!!");
			return;
		}
		for(ProductConversionDtl prod:productionDtlList)
		{
			if(!prod.getBatchCode().equalsIgnoreCase("NOBATCH"))
			{
				batch = true;
				break;
			}
				
		}
		
			String financialYear = SystemSetting.getFinancialYear();
    		String sNo = RestCaller.getVoucherNumber(financialYear + "PRODCONVSION");
    		productConversionMst.setVoucherNo(sNo);
//    		if(!batch)
//    		{
//    		//RestCaller.saveProductConversionMst(productConversionMst);
//    		}
//    		else
//    		{
    			RestCaller.saveProductConversionMstWithBatch(productConversionMst);
//    		}
    		notifyMessage(5,"Product Conversion Details Saved!!!");
    		productConversionMst = null;
    		productConversionDtl = null;
    		productionDtlList.clear();
    		batch = false;
    		clearfields();
    		tbConversion.getItems().clear();
		
	}
		}
	}
	  @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }
}