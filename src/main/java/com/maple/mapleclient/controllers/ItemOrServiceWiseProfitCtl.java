package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.StockValueReports;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class ItemOrServiceWiseProfitCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<StockValueReports> itemOrServiceValueList = FXCollections.observableArrayList();


    @FXML
    private ComboBox<String> cmbType;

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnGenerate;

    @FXML
    private TableView<StockValueReports> tblItems;

    @FXML
    private TableColumn<StockValueReports, String> clItemName;

    @FXML
    private TableColumn<StockValueReports, Number> clQty;

    @FXML
    private TableColumn<StockValueReports, Number> clValues;
    
    @FXML
    private TableColumn<StockValueReports, Number> clCostPrice;

    @FXML
    private TableColumn<StockValueReports, Number> clMrp;

    @FXML
    private TableColumn<StockValueReports, Number> clDiscount;


    @FXML
    private TextField txtTotal;
    
    @FXML
   	private void initialize() {
    	
    	cmbType.getSelectionModel().select("ITEM MST");

    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	cmbType.getItems().add("SERVICE ITEM");
    	cmbType.getItems().add("ITEM MST");

    }

    @FXML
    void DenerateReport(ActionEvent event) {
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(5, "Please select form date", false);
    		return;
    	}

    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(5, "Please select form date", false);
    		return;
    	}
    	if(null == cmbType.getSelectionModel().getSelectedItem())
    	{
    		notifyMessage(5, "Please select type", false);
    		return;
    	}
    	
    	Date fDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
    	String fromDate = SystemSetting.UtilDateToString(fDate, "yyyy-MM-dd");
    	
    	Date tDate = SystemSetting.localToUtilDate(dpToDate.getValue());
    	String toDate = SystemSetting.UtilDateToString(tDate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<StockValueReports>> itemValueReport = RestCaller.getItemOrServiceProfit(fromDate,toDate,cmbType.getSelectionModel().getSelectedItem());
    	itemOrServiceValueList = FXCollections.observableArrayList(itemValueReport.getBody());    	
    	
    	Double total = 0.0;
    	for(StockValueReports profit : itemOrServiceValueList)
    	{
    		
    		if(null != profit.getClosingValue())
    		{
    		total = total+profit.getClosingValue();
    		
    		BigDecimal bdCashToPay = new BigDecimal(profit.getClosingValue());
    		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
    		profit.setClosingValue(bdCashToPay.doubleValue());	
    		}
    		
    			
    	}
    	BigDecimal bdCashToPay = new BigDecimal(total);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
    	txtTotal.setText(bdCashToPay.toString());
    	tblItems.setItems(itemOrServiceValueList);
    	
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getClosingQtyProperty());
		clValues.setCellValueFactory(cellData -> cellData.getValue().getClosingValueProperty());
		clCostPrice.setCellValueFactory(cellData -> cellData.getValue().getCostPriceProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clDiscount.setCellValueFactory(cellData -> cellData.getValue().getDiscountProperty());

   
    }
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }


}
