package com.maple.mapleclient.controllers;

import java.math.BigDecimal;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockReport;
import com.maple.report.entity.StockSummaryCategoryWiseReport;
import com.maple.report.entity.StockSummaryDtlReport;
import com.maple.report.entity.StockSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;

public class StockSummaryCtl {
	
	String taskid;
	String processInstanceId;
	
	
	
	private ObservableList<StockSummaryReport> stockReportList = FXCollections.observableArrayList();
	private ObservableList<StockSummaryCategoryWiseReport> stockSummaryCategoryWiseReportList = FXCollections.observableArrayList();
	@FXML
	private DatePicker dpStartDate;
	@FXML
	private DatePicker dpToDate;

	@FXML
	private Button btnGenerateReport;

	@FXML
	private TableView<StockSummaryReport> tblCategpryDetails;

	@FXML
	private TableColumn<StockSummaryReport, String> clCategory;
	
	@FXML
	private TableColumn<StockSummaryReport, Number> clCategoryValue;

	@FXML
	private TableView<StockSummaryCategoryWiseReport> tblItemDetails;

	@FXML
	private TableColumn<StockSummaryCategoryWiseReport, String> clItem;

	@FXML
	private TableColumn<StockSummaryCategoryWiseReport, Number> clItemQty;

	@FXML
	private TableColumn<StockSummaryCategoryWiseReport, Number> clItemRate;

	@FXML
	private TableColumn<StockSummaryCategoryWiseReport, Number> clItemValue;
	
    @FXML
    private TableColumn<StockSummaryCategoryWiseReport, LocalDate> clTransDate;
	@FXML
	private void initialize() {
		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		tblCategpryDetails.getSelectionModel().selectedItemProperty()
		.addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					Date uSDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
					String sDate = SystemSetting.UtilDateToString(uSDate, "yyyy-MM-dd");
					Date uTDate = SystemSetting.localToUtilDate(dpToDate.getValue());
					String tDate = SystemSetting.UtilDateToString(uTDate, "yyyy-MM-dd");
					ResponseEntity<List<StockSummaryCategoryWiseReport>> stockReportResp = RestCaller.getItemwiseStockSummary(newSelection.getId(),SystemSetting.systemBranch,sDate,tDate);
					stockSummaryCategoryWiseReportList = FXCollections.observableArrayList(stockReportResp.getBody());
					tblItemDetails.setItems(stockSummaryCategoryWiseReportList);
					fillItemTable();
				
				}
			}
		});
	
	}
    @FXML
    void showItems(MouseEvent event) {
//    	if (tblCategpryDetails.getSelectionModel().getSelectedItem() != null) {
//			TableViewSelectionModel selectionModel = tblCategpryDetails.getSelectionModel();
//			stockReportList = selectionModel.getSelectedItems();
//			for (StockSummaryDtlReport stk : stockReportList) {
//    	
//				
//			}
//    	}
    }

	@FXML
	void FocusOnBtn(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnGenerateReport.requestFocus();
		}
	}
	@FXML
	void FocusOnToDate(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			
			dpToDate.requestFocus();

		}
	}
	@FXML
	void GenerateReport(ActionEvent event) {
		generateReport() ;
	}

	private void generateReport() {

		if (null == dpStartDate.getValue()) {
			notifyMessage(5, "please select from Date", false);
			return;

		} else if (null == dpToDate.getValue()) {
			
			notifyMessage(5, "please select to Date", false);
			return;

		} else {
			
			Date uSDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
			String sDate = SystemSetting.UtilDateToString(uSDate, "yyyy-MM-dd");
			Date uTDate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String tDate = SystemSetting.UtilDateToString(uTDate, "yyyy-MM-dd");
			System.out.println("--------inside report ............" );
			ResponseEntity<List<StockSummaryReport>> stockReportResp = RestCaller.getStockSummary(SystemSetting.systemBranch,sDate,tDate);
			System.out.println("--------inside report333333333333333 ............" );
			stockReportList = FXCollections.observableArrayList(stockReportResp.getBody());
			
			System.out.println(stockReportList);
			FillTable();
		}
	}

	@FXML
	void KeyPressGenerateReport(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

		}

	}
	
	private void FillTable() {
		System.out.println("-------------FillTable-" + stockReportList);
		tblCategpryDetails.setItems(stockReportList);
		for(StockSummaryReport ssr : stockReportList)
		{
			BigDecimal value = new BigDecimal(ssr.getValue());
			value = value.setScale(2, BigDecimal.ROUND_CEILING);
			ssr.setValue(value.doubleValue());
		}
		clCategory.setCellValueFactory(cellData -> cellData.getValue().getparticularsProperty());
		clCategoryValue.setCellValueFactory(cellData -> cellData.getValue().getvalueProperty());
		
	}
	
	private void fillItemTable() {
		System.out.println("-------------FillTable-" + stockSummaryCategoryWiseReportList);
		tblItemDetails.setItems(stockSummaryCategoryWiseReportList);
		
		for(StockSummaryCategoryWiseReport sscr : stockSummaryCategoryWiseReportList)
		{
			BigDecimal value = new BigDecimal(sscr.getValue());
			value = value.setScale(2, BigDecimal.ROUND_CEILING);
			sscr.setValue(value.doubleValue());
			
			
		}
		clItem.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clItemQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		clItemRate.setCellValueFactory(cellData -> cellData.getValue().getrateProperty());
		clItemValue.setCellValueFactory(cellData -> cellData.getValue().getvalueProperty());
		clTransDate.setCellValueFactory(cellData -> cellData.getValue().getTransDateProperty());
//		
	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
