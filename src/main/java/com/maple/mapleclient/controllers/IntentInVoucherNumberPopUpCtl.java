package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.IntentInHdr;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.events.IntentInVoucherNumberEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class IntentInVoucherNumberPopUpCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
    private ObservableList<IntentInHdr> voucherNoList = FXCollections.observableArrayList();
    IntentInVoucherNumberEvent voucherNoEvent = null;

    @FXML
    private TableView<IntentInHdr> tbIntentInVoucher;

    @FXML
    private TableColumn<IntentInHdr, String> clIntentInVoucher;

    @FXML
    private TableColumn<IntentInHdr, String> clFromBranch;

    @FXML
    private TableColumn<IntentInHdr, String> clVoucherDate;
    StringProperty SearchString = new SimpleStringProperty();
    @FXML
    private TextField txtInVoucherNumber;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
    @FXML
	private void initialize() {
    	txtInVoucherNumber.textProperty().bindBidirectional(SearchString);
    	voucherNoEvent = new IntentInVoucherNumberEvent();
    	eventBus.register(this);
    	LoadVoucherNoPopupBySearch("");
    	tbIntentInVoucher.setItems(voucherNoList);
    	clIntentInVoucher.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
    	clFromBranch.setCellValueFactory(cellData -> cellData.getValue().gefromBranchProperty());
    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
     	tbIntentInVoucher.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			
	    	   if (newSelection != null) {
			    	if(null!=newSelection.getId()&& newSelection.getInVoucherNumber().length()>0) {
			    		voucherNoEvent.setFromBranch(newSelection.getFromBranch());
			    		voucherNoEvent.setId(newSelection.getId());
			    		voucherNoEvent.setIntentInVoucherNumber(newSelection.getInVoucherNumber());
			    		voucherNoEvent.setId(newSelection.getId());
			    		voucherNoEvent.setInVoucherDate(newSelection.getInVoucherDate());
			    		System.out.println("Voucher================"+voucherNoEvent);
//			    		eventBus.post(voucherNoEvent);
			    		
			    	}
			    }
			});
	    	 SearchString.addListener(new ChangeListener(){
					@Override
					public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					 					
						LoadVoucherNoPopupBySearch((String)newValue);
					}
		        });
 	 
    }
    private void LoadVoucherNoPopupBySearch(String searchData) {
    	ArrayList stockin = new ArrayList();
    	voucherNoList.clear();
    	stockin = RestCaller.SearchPenidngIntentInVoucherNo(searchData);
    	Iterator itr = stockin.iterator();
    	while (itr.hasNext()) {
    		 LinkedHashMap element = (LinkedHashMap) itr.next();
    		// Object intentNumber = (String) element.get("intentNumber");
    		 Object id = (String) element.get("id");
    		 Object inVoucherNumber = (String) element.get("inVoucherNumber");
    		 Object fromBranch = (String)element.get("fromBranch");
    		 Object inVoucherDate=(String)element.get("inVoucherDate");
    		 if (null != id) {
    			 IntentInHdr inhdr = new IntentInHdr();
    			 inhdr.setInVoucherNumber((String)inVoucherNumber);
    			 inhdr.setId((String) id);
    			 inhdr.setFromBranch((String)fromBranch);
    			 inhdr.setInVoucherDate(SystemSetting.StringToUtilDate(inVoucherDate.toString(), "yyyy-MM-dd"));
    			 voucherNoList.add(inhdr);
    		 }
    	}
    	return;
    
		
	}
    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			eventBus.post(voucherNoEvent);
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtInVoucherNumber.requestFocus();
		}
    

    }
    @FXML
    void OnKeyPressTxt(KeyEvent event) {

    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tbIntentInVoucher.requestFocus();
			tbIntentInVoucher.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnOk.getScene().getWindow();
			stage.close();
		}
    
    }

	@FXML
    void actionCancel(ActionEvent event) {

    	Stage stage = (Stage) btnCancel.getScene().getWindow();
		stage.close();
    
    }

    @FXML
    void actionOk(ActionEvent event) {

    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	eventBus.post(voucherNoEvent);
		stage.close();
    
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
