package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.javapos.print.WeighingMachinePrint;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.WeighingMachine;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.WeighBridgeMaterialMst;
import com.maple.mapleclient.entity.WeighBridgeTareWeight;
import com.maple.mapleclient.entity.WeighBridgeVehicleMst;
import com.maple.mapleclient.entity.WeighBridgeWeights;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.events.WeighingMachineDataEvent;
import com.maple.mapleclient.events.WeighingMachineFirstWtEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;



public class WeighingMachineDisplayNewCtl {
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<WeighBridgeWeights> weighBridgeWtsList = FXCollections.observableArrayList();
	WeighBridgeWeights weighBridgeWeights = null;

	private static WeighBridgeWeights firstWeight;
	StringProperty weightfromMachine = new SimpleStringProperty("");
	StringProperty vehicleno = new SimpleStringProperty("");

    @FXML
    private TableView<WeighBridgeWeights> tbWeighingMachine;

    @FXML
    private TableColumn<WeighBridgeWeights,String> cldate;

    @FXML
    private TableColumn<WeighBridgeWeights,String> clVehicleNumber;

    @FXML
    private TableColumn<WeighBridgeWeights, Number> clFirstWt;

    @FXML
    private TableColumn<WeighBridgeWeights,Number> clSecondWt;

    @FXML
    private Button btnDelete;
    @FXML
    private DatePicker dpDate;

    @FXML
    private TextField txtVehicleNumber;

    @FXML
    private TextField txtFirstWt;

    @FXML
    private TextField txtwtFromMachine;


    @FXML
    private TextField txtNumberOfCopies;

    @FXML
    private ComboBox<String> cmbVehicleType;

    @FXML
    private ComboBox<String> cmbMaterial;

    @FXML
    private TextField txtRate;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnclear;
    @FXML
	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    	dpDate.setValue(LocalDate.now());
    	setMaterialList();
    	setVehicleType();
		eventBus.register(this);
		txtwtFromMachine.textProperty().bindBidirectional(weightfromMachine);
		txtVehicleNumber.textProperty().bindBidirectional(vehicleno);
		vehicleno.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadItemPopupBySearch((String) newValue);
			}
		});
		Thread t = new Thread(new WeighingMachine(SystemSetting.WEIGHBRIDGEPORT, SystemSetting.WEIGHBRIDGEBAUDRATE,
				SystemSetting.WEIGHBRIDGEDATABITS, SystemSetting.WEIGHBRIDGESTOPBIT, SystemSetting.WEIGHBRIDGEPARITY));
		t.start();
		
		
		
		ResponseEntity<List<WeighBridgeWeights>> getallWts = RestCaller.getWeighBridgeWtsWithNullSecondWt();
		weighBridgeWtsList = FXCollections.observableArrayList(getallWts.getBody());
		fillTable();
		tbWeighingMachine.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					weighBridgeWeights = new WeighBridgeWeights();
					weighBridgeWeights.setId(newSelection.getId());
					txtFirstWt.setText(newSelection.getPreviousweight()+"");
					if(null != newSelection.getFirstweightdate())
					{
					java.util.Date udate = SystemSetting.StringToUtilDate(newSelection.getFirstweightdate(),"yyyy-MM-dd");
					dpDate.setValue(SystemSetting.utilToLocaDate(udate));
					}
					txtRate.setText(newSelection.getRate()+"");
					txtVehicleNumber.setText(newSelection.getVehicleno());
					
					ResponseEntity<WeighBridgeMaterialMst> getmaterial = RestCaller.getWeighBridgeMaterialMstById(newSelection.getMaterialtypeid());
					if(null != getmaterial.getBody())
					{
					cmbMaterial.getSelectionModel().select(getmaterial.getBody().getMaterial());
					}
					
					ResponseEntity<WeighBridgeVehicleMst> getVehicle = RestCaller.getWeighBridgeVehicleById(newSelection.getVehicletypeid());
					if(null != getVehicle.getBody())
					{
						cmbVehicleType.getSelectionModel().select(getVehicle.getBody().getVehicletype());
					}
				}
			}
		});
		
    }
    private void LoadItemPopupBySearch(String searchData) {
    	ResponseEntity<List<WeighBridgeWeights>> getAllWeight = RestCaller.searchWeightByVehicleNoWithSecondWtNull(searchData);
    	weighBridgeWtsList = FXCollections.observableArrayList(getAllWeight.getBody());
    	fillTable();

    }
    private void fillTable()
    {
    	tbWeighingMachine.setItems(weighBridgeWtsList);
    	cldate.setCellValueFactory(cellData -> cellData.getValue().getFirstwtDateProperty());
    	clFirstWt.setCellValueFactory(cellData -> cellData.getValue().getpreviousWtPropertyProperty());
    	clSecondWt.setCellValueFactory(cellData -> cellData.getValue().getNextwtproperty());
    	clVehicleNumber.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());

    }
    @FXML
    void actionAdd(ActionEvent event) {
    	if(null == weighBridgeWeights )
    	{
    	BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getSystemBranch());

		ResponseEntity<WeighBridgeVehicleMst> vehicleType = RestCaller
				.getWeighBridgeVehicleMstByVehicleType(cmbVehicleType.getSelectionModel().getSelectedItem().toString());
		WeighBridgeVehicleMst weighBridgeVehicleMst = vehicleType.getBody();

		if (null == weighBridgeVehicleMst) {
			WeighBridgeVehicleMst weighBridgeVehicleMstToSave = new WeighBridgeVehicleMst();
			weighBridgeVehicleMstToSave.setBranchMst(branchMst);
			weighBridgeVehicleMstToSave.setCompanyMst(SystemSetting.getUser().getCompanyMst());
			weighBridgeVehicleMstToSave.setRate(Integer.parseInt(txtRate.getText()));
			weighBridgeVehicleMstToSave.setVehicletype(cmbVehicleType.getSelectionModel().getSelectedItem());
			ResponseEntity<WeighBridgeVehicleMst> vehicleTypeSaved = RestCaller
					.SaveWeighBridgeVehicleMst(weighBridgeVehicleMstToSave);
			weighBridgeVehicleMst = vehicleTypeSaved.getBody();
		}

		ResponseEntity<WeighBridgeMaterialMst> materilaSavedResp = RestCaller
				.getWeighBridgeMaterialMstByMaterial(cmbMaterial.getSelectionModel().getSelectedItem().toString());

		WeighBridgeMaterialMst weighBridgeMaterialMst = materilaSavedResp.getBody();

		if (null == weighBridgeMaterialMst) {
			WeighBridgeMaterialMst weighBridgeMaterialMstSaved = new WeighBridgeMaterialMst();
			weighBridgeMaterialMstSaved.setBranchMst(branchMst);
			weighBridgeMaterialMstSaved.setCompanyMst(SystemSetting.getUser().getCompanyMst());
			weighBridgeMaterialMstSaved.setMaterial(cmbMaterial.getSelectionModel().getSelectedItem());

			ResponseEntity<WeighBridgeMaterialMst> materialSaved = RestCaller
					.SaveWeighBridgeMaterialMst(weighBridgeMaterialMstSaved);

			weighBridgeMaterialMst = materialSaved.getBody();

		}
		weighBridgeWeights = new WeighBridgeWeights();

		weighBridgeWeights.setBranchMst(branchMst);
		weighBridgeWeights.setCompanyMst(SystemSetting.getUser().getCompanyMst());

		weighBridgeWeights.setMaterialtypeid(weighBridgeMaterialMst.getId());
		weighBridgeWeights.setVehicletypeid(weighBridgeVehicleMst.getId());

		weighBridgeWeights.setRate(weighBridgeVehicleMst.getRate());

		LocalDateTime today = LocalDateTime.now();

		weighBridgeWeights.setVoucherDate(today);

		String weightFromMachine = txtwtFromMachine.getText();
		weightFromMachine = weightFromMachine.replaceAll("KG", "");

		Integer machineWeight = Integer.parseInt(weightFromMachine.trim());

		Integer firstWeightValue = Integer.parseInt(txtwtFromMachine.getText());
		weighBridgeWeights.setPreviousweight(firstWeightValue);
//		if(null != firstWeight)
//			weighBridgeWeights.setPreviousweightid();
		weighBridgeWeights.setVehicleno(txtVehicleNumber.getText());

		weighBridgeWeights.setMachineweight(machineWeight);

		weighBridgeWeights.setFirstweightdate(dpDate.getValue()+"");
		String vNo = RestCaller.getVoucherNumber("WB");
		weighBridgeWeights.setVoucherNumber(vNo);

		ResponseEntity<WeighBridgeWeights> weighBridgeWeightsSaved = RestCaller
				.SaveWeighBridgeWeights(weighBridgeWeights);
		if (null != firstWeight) {
			firstWeight.setNextweight(machineWeight);
			firstWeight.setNextweightid(weighBridgeWeightsSaved.getBody().getId());
			ResponseEntity<WeighBridgeWeights> firstWeightSaved = RestCaller.SaveWeighBridgeWeights(firstWeight);

		}

		firstWeight = null;

		WeighingMachinePrint wprint = new WeighingMachinePrint();

		String wtToPrint = "";
		if (null == txtwtFromMachine.getText()) {
			wtToPrint = "00000";
		} else {
			wtToPrint = txtwtFromMachine.getText();
		}

		try {

			Integer firstWtValue = Integer.parseInt(txtwtFromMachine.getText());

			String weightFromMachine2 = wtToPrint;
			weightFromMachine2 = weightFromMachine2.replaceAll("KG", "");

			Integer machineWtValue = Integer.parseInt(weightFromMachine2.trim());
			Integer noOfCopies = 1;
			if(!txtNumberOfCopies.getText().trim().isEmpty())
			{
				noOfCopies =Integer.parseInt(txtNumberOfCopies.getText());
			}
			
			
			if (firstWtValue > machineWtValue) {
				int netValue = firstWtValue;
				while(noOfCopies>0)
				{
				wprint.PrintInvoiceThermalPrinter(firstWtValue + "", machineWtValue + "", netValue + "",
						txtVehicleNumber.getText(), txtRate.getText(), getCurrentTimeStamp(), vNo);
				noOfCopies= noOfCopies-1;
				}
			} else {
				int netValue = machineWtValue ;
				while(noOfCopies>0)
				{
				wprint.PrintInvoiceThermalPrinter(machineWtValue + "", firstWtValue + "", netValue + "",
						txtVehicleNumber.getText(), txtRate.getText(), getCurrentTimeStamp(), vNo);
				noOfCopies= noOfCopies-1;
				}
			}
    	}
    	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	}
    	else
    	{
    		ResponseEntity<WeighBridgeWeights> weighBridge = RestCaller.getWeighBridgeWeightById(weighBridgeWeights.getId());
    		if(null != weighBridge)
    		{
    	
    			weighBridgeWeights = weighBridge.getBody();
    			weighBridgeWeights.setNextweight(Integer.parseInt(txtwtFromMachine.getText()));
    			RestCaller.updateWeighBridgeWeights(weighBridgeWeights);
    			firstWeight = null;

    			WeighingMachinePrint wprint = new WeighingMachinePrint();

    			String wtToPrint = "";
    			if (null == txtwtFromMachine.getText()) {
    				wtToPrint = "00000";
    			} else {
    				wtToPrint = txtwtFromMachine.getText();
    			}

    			try {

    				Integer firstWtValue = weighBridgeWeights.getPreviousweight();

    				String weightFromMachine2 = wtToPrint;
    				weightFromMachine2 = weightFromMachine2.replaceAll("KG", "");

    				Integer machineWtValue = Integer.parseInt(weightFromMachine2.trim());
    				Integer noOfCopies = 1;
    				if(!txtNumberOfCopies.getText().trim().isEmpty())
    				{
    					noOfCopies =Integer.parseInt(txtNumberOfCopies.getText());
    				}
    				
    				
    				if (firstWtValue > machineWtValue) {
    					int netValue = firstWtValue - machineWtValue;
    					while(noOfCopies>0)
    					{
    					wprint.PrintInvoiceThermalPrinter(firstWtValue + "", machineWtValue + "", netValue + "",
    							txtVehicleNumber.getText(), txtRate.getText(), getCurrentTimeStamp(), weighBridgeWeights.getVoucherNumber());
    					noOfCopies= noOfCopies-1;
    					}
    				} else {
    					int netValue = machineWtValue - firstWtValue;
    					while(noOfCopies>0)
    					{
    					wprint.PrintInvoiceThermalPrinter(machineWtValue + "", firstWtValue + "", netValue + "",
    							txtVehicleNumber.getText(), txtRate.getText(), getCurrentTimeStamp(), weighBridgeWeights.getVoucherNumber());
    					noOfCopies= noOfCopies-1;
    					}
    				}
    	    	}
    	    	catch (SQLException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			
    		}
    	}
    	ResponseEntity<List<WeighBridgeWeights>> getallWts = RestCaller.getWeighBridgeWtsWithNullSecondWt();
		weighBridgeWtsList = FXCollections.observableArrayList(getallWts.getBody());
		fillTable();
		

		setMaterialList();
		setVehicleType() ;
    	weighBridgeWeights = null;
    	clearFields();
    }

    	public static String getCurrentTimeStamp() {
    		SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm");// dd/MM/yyyy
    		java.util.Date now = new java.util.Date();
    		String strDate = sdfDate.format(now);
    		return strDate;
    	}
    private void clearFields()
    {
    	txtFirstWt.clear();
		cmbMaterial.getSelectionModel().clearSelection();
		txtRate.clear();
		cmbVehicleType.getSelectionModel().clearSelection();
		txtwtFromMachine.clear();
		txtVehicleNumber.clear();
		weighBridgeWeights = null;
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == weighBridgeWeights)
    	{
    		return;
    	}
    	if(null == weighBridgeWeights.getId())
    	{
    		return;
    	}
    	RestCaller.deleteWeighBridgeWeights(weighBridgeWeights.getId());
    	ResponseEntity<List<WeighBridgeWeights>> getallWts = RestCaller.getWeighBridgeWtsWithNullSecondWt();
		weighBridgeWtsList = FXCollections.observableArrayList(getallWts.getBody());
		fillTable();
		clearFields();
    }
    @FXML
    void actionClear(ActionEvent event) {
    	 clearFields();
    }
    @FXML
    void firstWtOnEnter(KeyEvent event) {

    	if(event.getCode() == KeyCode.ENTER)
    	{
    		 loadFirstWeight();
    	}
    }

	@Subscribe
	public void popuplistner(WeighingMachineFirstWtEvent weighingMachineFirstWtEvent) {

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			txtFirstWt.setText(weighingMachineFirstWtEvent.getFirstWeight().toString());
			txtVehicleNumber.setText(weighingMachineFirstWtEvent.getVehicleNo());
			if(null != weighingMachineFirstWtEvent.getVoucherDate())
			{
			java.util.Date udate = SystemSetting.StringToUtilDate(weighingMachineFirstWtEvent.getVoucherDate().toString(),"yyyy-MM-dd");
			dpDate.setValue(SystemSetting.utilToLocaDate(udate));
			}
		}

	}

    private void loadFirstWeight() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WeighingMachineFirstWtPopUp.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    @FXML
    void actionPrint(ActionEvent event) {

		/*
		 * Function to display popup window and show list of items to select.
		 */
		System.out.println("ssssssss=====loadItemPopup=====sssssssssss");

		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/WeighinMachineReprintPopUp.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    private void setMaterialList() {

		cmbMaterial.getItems().clear();

		ResponseEntity<List<WeighBridgeMaterialMst>> materialListResp = RestCaller.getAllWeighBridgeMaterialMst();

		List<WeighBridgeMaterialMst> materialList = materialListResp.getBody();
		for (WeighBridgeMaterialMst material : materialList) {
			cmbMaterial.getItems().add(material.getMaterial());

		}
	}
    
    
	private void setVehicleType() {

		cmbVehicleType.getItems().clear();

		ResponseEntity<List<WeighBridgeVehicleMst>> vehicleListResp = RestCaller.getAllWeighBridgeVehicleMst();

		List<WeighBridgeVehicleMst> vehicleList = vehicleListResp.getBody();
		for (WeighBridgeVehicleMst vehicle : vehicleList) {
			cmbVehicleType.getItems().add(vehicle.getVehicletype());
		}

	}
	  @FXML
	    void onactionvehicle(ActionEvent event) {

			if (null != event.getSource()
					&& (null != ((ComboBox) event.getSource()).getSelectionModel().getSelectedItem())) {

				String selectedITem = ((ComboBox) event.getSource()).getSelectionModel().getSelectedItem().toString();

				ResponseEntity<WeighBridgeVehicleMst> vehicleType = RestCaller
						.getWeighBridgeVehicleMstByVehicleType(selectedITem);
				WeighBridgeVehicleMst weighBridgeVehicleMst = vehicleType.getBody();
				if (null != weighBridgeVehicleMst) {
					txtRate.setText(weighBridgeVehicleMst.getRate() + "");

					txtRate.setEditable(false);
				} else {
					txtRate.setEditable(true);

				}
				System.out.println(selectedITem);
			}
		
	    }

	

	@Subscribe
	synchronized public void popuplistner(WeighingMachineDataEvent voucherNoEvent) {

		String wData = voucherNoEvent.getWeightData().trim();

		
		if (wData.toUpperCase().contains("KG")) {
			
			Platform.runLater(() -> {
				weightfromMachine.set( voucherNoEvent.getWeightData().trim());
            });
			

			
		}
  

	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}

}
