package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.CategoryExceptionList;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class CategoryExceptionListCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<CategoryMst> categoryMstList = FXCollections.observableArrayList();
	private ObservableList<ProcessMst> processMstList = FXCollections.observableArrayList();
	List<String> categoryList = new ArrayList<String>();
	private ObservableList<CategoryExceptionList> categoryExceptionTableList = FXCollections.observableArrayList();

	CategoryExceptionList categoryExceptionList = new CategoryExceptionList();

	@FXML
	private ListView<String> lstCategory;

	@FXML
	private ChoiceBox<String> cmbReportName;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShow;

	@FXML
	private TableView<CategoryExceptionList> tblReport;

	@FXML
	private TableColumn<CategoryExceptionList, String> clCategoryName;

	@FXML
	private TableColumn<CategoryExceptionList, String> clReportName;

	@FXML
	private void initialize() {

		lstCategory.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		setCategory();
		setProcessName();
		
		tblReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					categoryExceptionList = new CategoryExceptionList();
					categoryExceptionList.setId(newSelection.getId());
					
				}
			}
		});
	}

	private void setProcessName() {

		ResponseEntity<List<ProcessMst>> processList = RestCaller.getprocessMstByProcessType("REPORT");
		processMstList = FXCollections.observableArrayList(processList.getBody());

		for (ProcessMst processMst : processMstList) {
			cmbReportName.getItems().add(processMst.getProcessName());
		}

	}

	private void setCategory() {

		ResponseEntity<List<CategoryMst>> categoryMstResp = RestCaller.getCategoryMsts();
		categoryMstList = FXCollections.observableArrayList(categoryMstResp.getBody());
		for (CategoryMst categoryMst : categoryMstList) {
			lstCategory.getItems().add(categoryMst.getCategoryName());

		}
	}

	@FXML
	void Delete(ActionEvent event) {
		
		if(null != categoryExceptionList)
		{
			if(null != categoryExceptionList.getId())
			{
				RestCaller.deleteCategoryExceptionList(categoryExceptionList.getId());
				
				categoryExceptionList = null;
				ShowFillTable();
			}
		}
		


	}

	@FXML
	void Save(ActionEvent event) {

		if (null == lstCategory.getSelectionModel().getSelectedItems()) {
			notifyMessage(3, "Please select Category", false);
			return;
		}
		if (null == cmbReportName.getValue()) {
			notifyMessage(3, "Please select Report Name", false);
			return;
		}
		
		String reportName = cmbReportName.getSelectionModel().getSelectedItem().toString();

		categoryList = SelectedCategory();
		for(String category : categoryList)
		{
			ResponseEntity<CategoryMst> categoryMstResp = RestCaller.getCategoryByName(category);
			CategoryMst categoryMst = categoryMstResp.getBody();
			CategoryExceptionList categoryExceptionList = new CategoryExceptionList();
			categoryExceptionList.setCategoryId(categoryMst.getId());
			categoryExceptionList.setReportName(reportName);
			
			ResponseEntity<CategoryExceptionList> saveCategoryExceptionList = RestCaller.saveCategoryExceptionList(categoryExceptionList);
		
			categoryExceptionTableList.add(saveCategoryExceptionList.getBody());
			
		}

		FillTable();
		
		cmbReportName.getSelectionModel().clearSelection();
		lstCategory.getSelectionModel().clearSelection();
		categoryExceptionList = null;

		
	}

	private void FillTable() {

		tblReport.setItems(categoryExceptionTableList);
		for(CategoryExceptionList exceptionList : categoryExceptionTableList)
		{
			if(null != exceptionList.getCategoryId())
			{
				ResponseEntity<CategoryMst> categoryMstResp = RestCaller.getCategoryById(exceptionList.getCategoryId());
				CategoryMst categoryMst = categoryMstResp.getBody(); 
				
				exceptionList.setCategoryName(categoryMst.getCategoryName());
				
			}
		}
		
		clCategoryName.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
		clReportName.setCellValueFactory(cellData -> cellData.getValue().getReportNameProperty());

	}

	private List<String> SelectedCategory() {

		List<String> itemIds = new ArrayList<String>();

		int selesctedItemsNo = lstCategory.getSelectionModel().getSelectedItems().size();

		for (int i = 0; i < selesctedItemsNo; i++) {

			String itemId = lstCategory.getSelectionModel().getSelectedItems().get(i);
			itemIds.add(itemId);
		}
		return itemIds;
	}

	@FXML
	void Show(ActionEvent event) {
		
		ShowFillTable();
	}

	private void ShowFillTable() {

		ResponseEntity<List<CategoryExceptionList>> exceptionList = RestCaller.getAllCategoryExceptionList();
		categoryExceptionTableList =  FXCollections.observableArrayList(exceptionList.getBody());
		FillTable();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	       private void PageReload() {
	       	
	 }


}
