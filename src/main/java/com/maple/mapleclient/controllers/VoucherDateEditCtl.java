package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.maple.util.ExportCustomerSalesToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.DamageDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class VoucherDateEditCtl {
	
	
	String taskid;
	String processInstanceId;

	

	
	ExportCustomerSalesToExcel exportCustomerSalesToExcel = new ExportCustomerSalesToExcel();
	SalesTransHdr salesTransHdr=null;
	POSThermalPrintABS printingSupport ;
	private ObservableList<DailySalesReportDtl> dailySalesList = FXCollections.observableArrayList();
	private ObservableList<VoucherReprintDtl> dailySalesLists = FXCollections.observableArrayList();
	double totalamount = 0.0;
	String customer = null;	String voucher = null;
	String vDate = null;
	 @FXML
	 private Button ok;
     @FXML
    private DatePicker dpdate;
    

  

    @FXML
    private Button btnExportToExcel;
    @FXML
    private ComboBox<String> branchselect;

    DailySalesReportDtl dailySalesReportDtl=null;
    @FXML
    private TableView<DailySalesReportDtl> tbReport;

    @FXML
    private TableColumn<DailySalesReportDtl, String> clVoucherNo;

    @FXML
    private TableColumn<DailySalesReportDtl, LocalDate> clDate;
    @FXML
    private TableColumn<DailySalesReportDtl,String> clCustomer;

    @FXML
    private TableColumn<DailySalesReportDtl, Number> clamt;


    @FXML
    private TextField txtGrandTotal;
    @FXML
    private Button btnShow;
    @FXML
    private Button btnPrint;
    @FXML
    private TableColumn<VoucherReprintDtl,String> voucherNo;

    @FXML
    private TableColumn<VoucherReprintDtl, String> Customername;

    @FXML
    private TableColumn<VoucherReprintDtl, String> itemName;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> qty;
    

    @FXML
    private TableColumn<VoucherReprintDtl, Number> clRate;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> taxRate;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> cessRate;

    
    @FXML
    private Button btnStartVoucherNumber;

    @FXML
    private Button btnEndVouchNumber;

    @FXML
    private TextField txtStartingVoucherNumber;

    @FXML
    private TextField txtEndingVoucherNumber;

    @FXML
    private TextField txtVoucherSuffix;

    @FXML
    private TableColumn<VoucherReprintDtl, String> unit;

    @FXML
    private TableColumn<VoucherReprintDtl, Number> amount;
    
    @FXML
    
    private Button btnPrintReport;
    @FXML

    DatePicker dpConvertedToDate;
  

    @FXML
	private void initialize() {
    	dpConvertedToDate = SystemSetting.datePickerFormat(dpConvertedToDate, "dd/MMM/yyyy");
    	dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");
    	setBranches();
    	 
		  tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	    		if (newSelection != null) {
	    			System.out.println("getSelectionModel");
	    			if (null != newSelection.getVoucherNumber()) {
	    			
	    				String voucherNumber=newSelection.getVoucherNumber();
	    				customer = newSelection.getCustomerName();
	    				voucher = newSelection.getVoucherNumber();
	    				java.sql.Date uDate = newSelection.getvoucherDate();
	    	    		 vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
	    		    
	    				 String branchName=branchselect.getValue();
	    				ResponseEntity<List<VoucherReprintDtl>> dailysaless = RestCaller.getVoucherReprintDtl(newSelection.getVoucherNumber(),branchName);
	    		    	dailySalesLists = FXCollections.observableArrayList(dailysaless.getBody());
	    		
	
	    			}
	    		}
	    	});
		  btnPrint.setVisible(false);
		  btnPrintReport.setVisible(false);
		  btnExportToExcel.setVisible(false);
		  txtEndingVoucherNumber.setEditable(false);
		  txtStartingVoucherNumber.setEditable(false);
    }
    @FXML
    void show(ActionEvent event) {
    	txtStartingVoucherNumber.clear();
    	txtVoucherSuffix.clear();
    	dailySalesList.clear();
    	txtEndingVoucherNumber.clear();
    	
    	 totalamount = 0.0;
    	java.util.Date uDate = Date.valueOf(dpdate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		
		//uDate = SystemSetting.StringToUtilDate(strDate,);
    	ResponseEntity<List<DailySalesReportDtl>> dailysales = RestCaller.getSalesReport(branchselect.getSelectionModel().getSelectedItem(), strDate);
    	dailySalesList = FXCollections.observableArrayList(dailysales.getBody());
    	tbReport.setItems(dailySalesList);
    	clamt.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
    	clCustomer.setCellValueFactory(cellData -> cellData.getValue().getcustomerNameProperty());
    	clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
    	clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
    	
    	for(DailySalesReportDtl dailySaleReportDtl:dailySalesList)
    	{
    		if(null != dailySaleReportDtl.getAmount())
    		totalamount = totalamount + dailySaleReportDtl.getAmount();
    	}
    	BigDecimal settoamount = new BigDecimal(totalamount);
		settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
    	txtGrandTotal.setText(settoamount.toString());

    }
  
	private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
    	
    
		for(BranchMst branchMst : branchMstList)
		{
			branchselect.getItems().add(branchMst.getBranchCode());
		
			
		}
		 
	}
	  @FXML
	    void PrintReport(ActionEvent event) {
		/*
		 * java.util.Date uDate = Date.valueOf(dpdate.getValue()); String strDate =
		 * SystemSetting.UtilDateToString(uDate, "yyy-MM-dd"); java.util.Date uDate1 =
		 * Date.valueOf(dpToDate.getValue()); String strDate1 =
		 * SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd"); try {
		 * JasperPdfReportService.SalesReportBetweenDate(
		 * branchselect.getSelectionModel().getSelectedItem(),strDate); } catch
		 * (JRException e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */
	    }

		public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");
			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}
	    @FXML
	    void actionExcelExport(ActionEvent event) {
		/*
		 * java.util.Date uDate = Date.valueOf(dpdate.getValue()); String strDate =
		 * SystemSetting.UtilDateToString(uDate, "yyy-MM-dd"); java.util.Date uDate1 =
		 * Date.valueOf(dpToDate.getValue()); String strDate1 =
		 * SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd"); ArrayList tallyImport =
		 * new ArrayList(); if( branchselect.getSelectionModel().isEmpty()) { return; }
		 * tallyImport =
		 * RestCaller.getTallyImport(branchselect.getSelectionModel().getSelectedItem(),
		 * strDate, strDate1);
		 * exportCustomerSalesToExcel.exportToExcel("SalesToTally"+branchselect.
		 * getSelectionModel().getSelectedItem()+strDate+strDate1+".xls", tallyImport);
		 */
	    }

	  @FXML
	    void onClickOk(ActionEvent event) {
		 
		  
		  

	    }
	    @FXML
	    void reportPrint(ActionEvent event) {
	    	
//	    	java.util.Date uDate = Date.valueOf(dpdate.getValue());
//    		String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
	    
	    	if(customer.equalsIgnoreCase("POS") || customer.equalsIgnoreCase("KOT"))
	    	{
	    		 salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucher,vDate) ;
	    		 //salesTransHdr = Re
	 			if ( dailySalesList.size()>0) {
					for(DailySalesReportDtl dailySaleReportDtl:dailySalesList)
			    	{
			    		
					txtStartingVoucherNumber.setText(dailySaleReportDtl.getVoucherNumber());
			    	}
				}else {
					notifyMessage(5, "Please select the Item!!!");
				}RestCaller.getSalesTransHdr(salesTransHdr.getId());
	    		if(null!=salesTransHdr) {
	    		try {
	    			System.out.println(salesTransHdr.getId());
	    			printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	}
	    	else
	    	{
		    		try {
		    			NewJasperPdfReportService.TaxInvoiceReport(voucher, vDate);
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    	}
	    	}
	    
	    

	    @FXML
	    void setEndVoucherNumber(ActionEvent event) {

	    	txtEndingVoucherNumber.clear();
			if ( dailySalesList.size()>0) {
				
				List<DailySalesReportDtl> DailySalesReportDtlList=	tbReport.getSelectionModel().getSelectedItems();
				
					
		    		
				txtEndingVoucherNumber.setText(DailySalesReportDtlList.get(0).getVoucherNumber());
		    	
			}else {
				notifyMessage(5, "Please select the Item!!!");
			}
	    }

	    @FXML
	    void setStartVoucherNumber(ActionEvent event) {

	    	txtStartingVoucherNumber.clear();
			if ( dailySalesList.size()>0) {
				List<DailySalesReportDtl> DailySalesReportDtlList=	tbReport.getSelectionModel().getSelectedItems();
				
				
	    		
				txtStartingVoucherNumber.setText(DailySalesReportDtlList.get(0).getVoucherNumber());
		    	
			}else {
				notifyMessage(5, "Please select the Item!!!");
			}
	  
	    }
    

	    @FXML
	    void editVOucherDate(ActionEvent event) {

	    	if(null==dpConvertedToDate.getValue()) {
	    		notifyMessage(5, "Please select Date To CONVERT!!");
	    	}
	    	if(txtVoucherSuffix.getText().isEmpty()) {
	    		notifyMessage(5, "Please ENter Voucher Suffix!!");
	    	
	    		if(txtEndingVoucherNumber.getText().isEmpty()) {
		    		notifyMessage(5, "Please select ENDING VOUCHER NUMBER!!");
		    		if(txtStartingVoucherNumber.getText().isEmpty()) {
			    		notifyMessage(5, "Please select Starting VOUCHER NUMBER!!");
			    	}else {
			    	
			    	}
	    	}
	    	}

	    	String branchCode=SystemSetting.getSystemBranch();
	    	ResponseEntity<String> voucherDateUpdate = RestCaller.getVoucherDateUpdate(txtStartingVoucherNumber.getText(),
	    			txtEndingVoucherNumber.getText(),dpConvertedToDate.getValue().toString(),txtVoucherSuffix.getText(),
	    			branchCode,dpdate.getValue().toString());
	    	
	    	notifyMessage(5, "UPDATED !!!");
	    	}
	    
	    @Subscribe
  	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
  	   		//if (stage.isShowing()) {
  	   			taskid = taskWindowDataEvent.getId();
  	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  	   			
  	   		 
  	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  	   			System.out.println("Business Process ID = " + hdrId);
  	   			
  	   			 PageReload(hdrId);
  	   		}


  	   	private void PageReload(String hdrId) {

  	   	}
	    	
}
