package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.CategoryWiseStockMovement;
import com.maple.report.entity.StockSummaryDtlReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ItemStockMovementByDateCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<StockSummaryDtlReport> itemLsit = FXCollections.observableArrayList();

    @FXML
    private TextField txtItemName;

    @FXML
    private TextField txtDate;

    @FXML
    private TextField txtOpeningStock;

    @FXML
    private TextField txtClosingStock;
    @FXML
    private TableView<StockSummaryDtlReport> tblStock;

    @FXML
    private TableColumn<StockSummaryDtlReport, String> clParticulars;



    @FXML
    private TableColumn<StockSummaryDtlReport, Number> clInwardQty;

    @FXML
    private TableColumn<StockSummaryDtlReport, Number> clInwardValue;
    
    @FXML
    private TableColumn<StockSummaryDtlReport, Number> clValue;

    @FXML
    private TableColumn<StockSummaryDtlReport,Number> clOutwardQty;
    
    @FXML
    private TableColumn<StockSummaryDtlReport, Number> clOutwardValue;

    @FXML
    private TableColumn<StockSummaryDtlReport, Number> clClosingQty;
    

    @FXML
    private TableColumn<StockSummaryDtlReport, Number> clclosingValue;

    
    @FXML
   	private void initialize() {
   		
   		tblStock.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
   			if (newSelection != null) {
   				System.out.println("getSelectionModel");
   				System.out.println("getSelectionModel" + newSelection);
   				if (null != newSelection.getItemName()) {
   					
   					
   					if(newSelection.getParticulars().contains("Purchase "))
   						
   					{
   						purchase(newSelection.getVoucherNumber());
   					}
   					else
   					{
   					wholesale(newSelection.getVoucherNumber());
   					}
   					
   				}
   			}
   		});
   	}
    
    private void purchase(String voucherno)
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/purchase.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			PurchaseCtl purchaseCtl =fxmlLoader.getController();
			purchaseCtl.getPurchaseNonEditable(voucherno);
   			Stage stage = new Stage();
   			stage.setScene(new Scene(root1));
   			stage.initModality(Modality.APPLICATION_MODAL);
   			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
		
	}
    private void wholesale(String voucherno)
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/WholeSaleInvoiceEdit.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			WholeSaleInvoiceEdit WholeSaleInvoiceEdit =fxmlLoader.getController();
			WholeSaleInvoiceEdit.getSalesByVoucherNo(voucherno);
   			Stage stage = new Stage();
   			stage.setScene(new Scene(root1));
   			stage.initModality(Modality.APPLICATION_MODAL);
   			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
		
	}

	public void stockSummaryDtls(String itemName,String itemid, Date voucherDate) {
		

		txtItemName.setText(itemName);
		
	String fromDate = SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");
	txtDate.setText(fromDate);
	ResponseEntity<List<StockSummaryDtlReport>> getStockSummary = RestCaller.getStockSummaryDtlReport(fromDate,fromDate,itemid,SystemSetting.systemBranch);
	itemLsit = FXCollections.observableArrayList(getStockSummary.getBody());
		fillTable();
		
	}
    
    private void fillTable()
    {
    	for (StockSummaryDtlReport cat:itemLsit)
    	{
    		
    		
    		txtOpeningStock.setText(Double.toString(cat.getOpeningStock()));
    		txtClosingStock.setText(Double.toString(cat.getClosingQty()));
    		if(null != cat.getClosingQty())
    		{
    		BigDecimal closing = new BigDecimal(cat.getClosingQty());
    		closing = closing.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        	cat.setClosingQty(closing.doubleValue());
    		}
    		else
    		{
    			cat.setClosingQty(0.0);
    		}
    		if(null != cat.getOpeningStock())
			{
        	BigDecimal opening = new BigDecimal(cat.getOpeningStock());
        	opening = opening.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        	cat.setOpeningStock(opening.doubleValue());
			}
    		else
    		{
    			cat.setOpeningStock(0.0);
    		}
    		if(null != cat.getInwardQty())
			{
        	BigDecimal inward = new BigDecimal(cat.getInwardQty());
        	inward = inward.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        	cat.setInwardQty(inward.doubleValue());
			}
    		else
    		{
    			cat.setInwardQty(0.0);
    		}
    		if(null != cat.getOutwardQty())
			{
        	BigDecimal outward = new BigDecimal(cat.getOutwardQty());
        	outward = outward.setScale(3, BigDecimal.ROUND_HALF_EVEN);
        	cat.setOutwardQty(outward.doubleValue());
			}else
			{
				cat.setOutwardQty(0.0);
			}
    	}
    	tblStock.setItems(itemLsit);
		clClosingQty.setCellValueFactory(cellData -> cellData.getValue().getclosingQtyProperty());
		clInwardQty.setCellValueFactory(cellData -> cellData.getValue().getinwardQtyProperty());
		clOutwardQty.setCellValueFactory(cellData -> cellData.getValue().getoutWardQtyProperty());
		clParticulars.setCellValueFactory(cellData -> cellData.getValue().getparticularsProperty());
		clclosingValue.setCellValueFactory(cellData -> cellData.getValue().getclosingValueProperty());
		clInwardValue.setCellValueFactory(cellData -> cellData.getValue().getinwardValueProperty());
		clOutwardValue.setCellValueFactory(cellData -> cellData.getValue().getoutWardValueProperty());

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }
    

}
