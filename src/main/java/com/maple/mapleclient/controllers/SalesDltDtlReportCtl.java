package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.SalesDltdDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class SalesDltDtlReportCtl {
	
	String taskid;
	String processInstanceId;


	// Not Needed
	private ObservableList<SalesDltdDtl> SalesDltdDtlList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpDate;

    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private TableView<SalesDltdDtl> itemDetailTable;

    @FXML
    private TableColumn<SalesDltdDtl, String> columnItemName;

    @FXML
    private TableColumn<SalesDltdDtl, String> columnBarCode;

    @FXML
    private TableColumn<SalesDltdDtl, Number> columnQty;

    @FXML
    private TableColumn<SalesDltdDtl, Number> columnTaxRate;

    @FXML
    private TableColumn<SalesDltdDtl, Number> columnRate;

    @FXML
    private TableColumn<SalesDltdDtl, Number> columnMrp;

    @FXML
    private TableColumn<SalesDltdDtl, Number> clAmount;

    @FXML
    private TableColumn<SalesDltdDtl, String> columnBatch;

    @FXML
    private TableColumn<SalesDltdDtl, String> columnUnitName;
    @FXML
   	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
   		
       }
    @FXML
    void actionPrint(ActionEvent event) {

    	LocalDate dpDate1 = dpDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
    }

    @FXML
    void actionShow(ActionEvent event) {
    	LocalDate dpDate1 = dpDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 
		 ResponseEntity<List<SalesDltdDtl>> getSalesDeletDtl = RestCaller.getSalesDltdDtl(strDate);
		 SalesDltdDtlList = FXCollections.observableArrayList(getSalesDeletDtl.getBody());
		 fillTable();
    }

    private void fillTable()
    {
    	itemDetailTable.setItems(SalesDltdDtlList);
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getbarcodeProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getmrpProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getrateProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().gettaxRateProperty());
		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());


    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }

}
