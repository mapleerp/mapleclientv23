package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.sun.glass.events.KeyEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

public class BalanceSheet {
	String taskid;
	String processInstanceId;
	


    @FXML
    private AnchorPane clFromItemName;

    @FXML
    private Button btnAdd;

    @FXML
    private DatePicker date;

    @FXML
    private ComboBox<?> comboBoxBranch;

    @FXML
    private TableView<?> tblItems;

    @FXML
    private TableColumn<?, ?> clAssets;

    @FXML
    private TableColumn<?, ?> clLiabilities;

    @FXML
    void actionDate(KeyEvent event) {

    }

    @FXML
    void addItems(ActionEvent event) {

    }

    @FXML
    void addOnEnter(KeyEvent event) {

    }

    @FXML
    void saveOnKey(KeyEvent event) {

    }
    
    @FXML
   	private void initialize() {
    	date = SystemSetting.datePickerFormat(date, "dd/MMM/yyyy");
       
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


      private void PageReload() {
      	

}
      }
