package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class FastMovingItemBySalesModeCtl {
	String taskid;
	String processInstanceId;

	static final String windowName = "FAST MOVING ITEMS REPORT BY SALES MODE";

	private ObservableList<StockReport> stockList = FXCollections.observableArrayList();

	@FXML
	private DatePicker dpFromDate;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private Button btnShow;

	@FXML
	private Button btnPrint;

	@FXML
	private ComboBox<String> cmbSalesMode;
	
	   @FXML
	    private ComboBox<String> cmbCategory;

	@FXML
	private TableView<StockReport> tbItems;

	@FXML
	private TableColumn<StockReport, String> clItemName;

	@FXML
	private TableColumn<StockReport, Number> clQty;

	@FXML
	private void initialize() {
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		ResponseEntity<List<String>> salesModes = RestCaller.getSalesModes();
		List<String> salesModesList = salesModes.getBody();
		for (String str : salesModesList) {
			cmbSalesMode.getItems().add(str);
		}
		
		ResponseEntity<List<CategoryMst>> categoryMstResp = RestCaller.getCategoryMsts();
		List<CategoryMst> categoryMstList = categoryMstResp.getBody();
		for(CategoryMst category : categoryMstList) {
			cmbCategory.getItems().add(category.getCategoryName());
		}

	}

	@FXML
	void actionPrint(ActionEvent event) {

	}

	@FXML
	void actionshow(ActionEvent event) {
		
		if(null == dpFromDate.getValue())
		{
			notifyMessage(2, "Please select From Date", false);
			return;
		}
		
		if(null == dpToDate.getValue())
		{
			notifyMessage(2, "Please select To Date", false);
			return;
		}

		if(null == cmbSalesMode.getSelectionModel().getSelectedItem())
		{
			notifyMessage(2, "Please select To Date", false);
			return;
		}

		tbItems.getItems().clear();

		LocalDate lfdate = dpFromDate.getValue();
		Date ufDate = SystemSetting.localToUtilDate(lfdate);
		String sfDate = SystemSetting.UtilDateToString(ufDate, "yyyy-MM-dd");
		LocalDate ltdate = dpToDate.getValue();

		Date utDate = SystemSetting.localToUtilDate(ltdate);
		String stDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
		
		if(null == cmbCategory.getSelectionModel().getSelectedItem())
		{
			ResponseEntity<List<StockReport>> stockReport = RestCaller.
					getFastMovingItemsBySalesMode(sfDate, stDate,cmbSalesMode.getSelectionModel().getSelectedItem().toString() ,windowName);
			stockList = FXCollections.observableArrayList(stockReport.getBody());
			fillTable();
		} else {
			
			ResponseEntity<CategoryMst> categoryResp = RestCaller.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem().toString());
			CategoryMst categoryMst = categoryResp.getBody();
			
			ResponseEntity<List<StockReport>> stockReport = RestCaller.
					getFastMovingItemsBySalesModeAndCategory(sfDate, stDate,cmbSalesMode.getSelectionModel().getSelectedItem().toString() ,
							categoryMst.getId(),windowName);
			stockList = FXCollections.observableArrayList(stockReport.getBody());
			fillTable();
		}

		cmbCategory.getSelectionModel().clearSelection();
		cmbSalesMode.getSelectionModel().clearSelection();
		dpFromDate.setValue(null);
		dpToDate.setValue(null);

	}

	private void fillTable() {
		tbItems.setItems(stockList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());

	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
