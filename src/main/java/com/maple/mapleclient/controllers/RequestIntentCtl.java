 package com.maple.mapleclient.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportReorderToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.IntentDtl;
import com.maple.mapleclient.entity.IntentHdr;
import com.maple.mapleclient.entity.IntentItemBranchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPropertyConfig;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.ItemPropertyInstance;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ItemPropertyIntent;

import java.io.IOException;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

@Controller
public class RequestIntentCtl {
	
	String taskid;
	String processInstanceId;

	
	ExportReorderToExcel exportReorderToExcel = new ExportReorderToExcel();
	// IntentDtl itemToDelete;
	// IntentDtl intentDtls = new IntentDtl();
	private EventBus eventBus = EventBusFactory.getEventBus();
	IntentHdr intentHdr = null;
	IntentDtl intentDtl = null;
	ItemMst itemmst = new ItemMst();
	IntentDtl toDelete;
	private ObservableList<IntentDtl> intentListTable = FXCollections.observableArrayList();
	private ObservableList<Object> intentListTable1 = FXCollections.observableArrayList();
	
	List<ItemPropertyIntent> propertyinstanceList = null;

	
	boolean subscribeItemProperty = false;
	@FXML
    private Button btnExportToExcel;
	@FXML
	private TextField txtVoucherNo;

	@FXML
	private ComboBox<String> cmbToBranch;
	@FXML
	private DatePicker dpDate;
	@FXML
	private Button btnGenerate;

	@FXML
	private TextField txtItemId;

	@FXML
	private TextField txtQty;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnDelete;

	@FXML
	private TextField txtUnitName;

	@FXML
	private TableView<IntentDtl> tblIntent;

	@FXML
	private TableColumn<IntentDtl, String> clItemId;

	@FXML
	private TableColumn<IntentDtl, Number> clQty;

	@FXML
	private TableColumn<IntentDtl, String> clUnitName;
	

    @FXML
    private TableColumn<IntentDtl, String> clToBranch;
    
    @FXML
    private TableColumn<IntentDtl, String> clRemark;
    
    @FXML
    private TextField txtRemark;
	
	@FXML
	 void onBranchkey(KeyEvent event) {
		  if (event.getCode() == KeyCode.ENTER) {
				dpDate.requestFocus();
			}
	    }
	   @FXML
	    void ExportToExcel(ActionEvent event) {
	    	
	    	if(null == dpDate.getValue())
	    	{
				notifyMessage(5, "Please select date");
				return;

	    	}
	    	
	    	 java.util.Date uDate = Date.valueOf(dpDate.getValue());
				String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
				ArrayList tallyImport = new ArrayList();
				
				tallyImport = RestCaller.getReorderExportToExcel(strDate);	
				exportReorderToExcel.exportToExcel("IntentReorder"+SystemSetting.getUser().getBranchCode()+strDate+".xls", tallyImport);
	 	

	    }
//	@FXML
//    void onDateKey(KeyEvent event) {
//		if (event.getCode() == KeyCode.ENTER) {
//			txtItemId.requestFocus();
//		}
//    }
	

    @FXML
    void onItemkeypress(KeyEvent event) {

    	  if (event.getCode() == KeyCode.ENTER) {
    		  loadItemPopup();
			}
    }
	
	@FXML
	void OnActionAdd(ActionEvent event) {
		addItem();
			

			
			
			
	}
    @FXML
    void addItem(KeyEvent event) {
    	 if (event.getCode() == KeyCode.ENTER) {
    		 addItem();
			}
    }
	

	private void FillTable() {
		System.out.println("-------------FillTable-" + intentListTable);
		tblIntent.setItems(intentListTable);
		for(IntentDtl intent : intentListTable)
		{
			if(null != intent.getItemId())
			{
			ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(intent.getItemId());
			intent.setItemName(respentity.getBody().getItemName());
			clItemId.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			}
			if(null != intent.getUnitId())
			{
				ResponseEntity<UnitMst> respunit = RestCaller.getunitMst(intent.getUnitId());
				intent.setUnitName(respunit.getBody().getUnitName());
				clUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
			}
		}
			
			
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clToBranch.setCellValueFactory(cellData -> cellData.getValue().gettoBranchProperty());
		clRemark.setCellValueFactory(cellData -> cellData.getValue().getRemarkProperty());


	}

	@FXML
	void OnActionDelete(ActionEvent event) {
		if (null != intentDtl.getId()) {
			RestCaller.deleteIntentDtl(intentDtl.getId());
			notifyMessage(5, " Deleted Successfully");
			tblIntent.getItems().clear();
			ResponseEntity<List<IntentDtl>> intentdtlSaved = RestCaller.getIntentDtl(intentHdr);
			intentListTable = FXCollections.observableArrayList(intentdtlSaved.getBody());
	//==========to fill table again after delete===========//	
			if (intentListTable != null) {
				
				for (IntentDtl inte : intentListTable) {
					
					ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(inte.getItemId());
					inte.setItemName(respentity.getBody().getItemName());
				}	
			} 
			tblIntent.setItems(intentListTable);	
			}
			else
			{
				notifyMessage(5,"Please select the Item!!!");
			}		
		}

    @FXML
    void qtyOnEnter(KeyEvent event) {
    	
    	  if (event.getCode() == KeyCode.ENTER) {

// 			 ResponseEntity<IntentItemBranchMst> getIntentItemBranch = RestCaller.getIntentItemBranchMstByItemId(itemmst.getId());
// 			 if(null != getIntentItemBranch.getBody())
// 			 {
// 				 //cmbToBranch.setSelectionModel(value);
// 				 cmbToBranch.setPromptText(getIntentItemBranch.getBody().getBranchCode());
// 			 }
				btnAdd.requestFocus();
			}
	    }
    
	
	@FXML
	void OnActionGenerate(ActionEvent event) {
	
	
		Map<String, Object> map = new HashMap<String, Object>();
		map = RestCaller.generateIntentHdr();
	    map.get("intentHdrList");
		ArrayList<IntentDtl> intent = new ArrayList<IntentDtl>();
		intent = (ArrayList<IntentDtl>)map.get("intentHdrList");
		Iterator itr = intent.iterator();
		while (itr.hasNext()) {
			
			 LinkedHashMap element = (LinkedHashMap) itr.next();
			 Object id = (String) element.get("id");
//			 Object itemId = (String) element.get("itemId");
//			 Object qty = (Double) element.get("qty");
			 
			 if (null != id) {
				 
			 
			IntentHdr intentHd = new IntentHdr();
			intentHd.setId((String)id);
			ResponseEntity<List<IntentDtl>> getIntentDtl = RestCaller.getIntentDtl(intentHd);
			for(IntentDtl intnt:getIntentDtl.getBody())
			{
				ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(intnt.getItemId());
				intnt.setUnitId(getItem.getBody().getUnitId());
				
			}
			intentListTable = FXCollections.observableArrayList(getIntentDtl.getBody());
			 ResponseEntity<IntentHdr> intentHdrREsp = RestCaller.getIntentHdrById(intentHd.getId());
			 intentHdr = intentHdrREsp.getBody();
			 }
			
		}
			
			FillTable();
			//intentListTable.clear();
			
	}
	
	

	@FXML
	void OnMouseClickedItem(MouseEvent event) {
		loadItemPopup();
	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itempopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnAdd.getScene().getWindow();
	
		if (stage.isShowing()) {
			txtItemId.setText(itempopupEvent.getItemName());
			txtUnitName.setText(itempopupEvent.getUnitName());
			itemmst.setId(itempopupEvent.getItemId());
			itemmst.setUnitId(itempopupEvent.getUnitId());
			
			 ResponseEntity<IntentItemBranchMst> getIntentItemBranch = RestCaller.getIntentItemBranchMstByItemId(itemmst.getId());
			 if(null != getIntentItemBranch.getBody())
			 {
				 cmbToBranch.getSelectionModel().select(getIntentItemBranch.getBody().getBranchCode());
				// cmbToBranch.setPromptText(getIntentItemBranch.getBody().getBranchCode());
			 }

		}
		txtQty.requestFocus();  //===to automaticaly focus on quantity after popup dtls entered//
	}

	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		ResponseEntity<List<BranchMst>> branchRestListResp = RestCaller.getBranchDtl();
		 
		
		
		List branchRestList =branchRestListResp.getBody();
		Iterator itr1  = branchRestList.iterator();
		
		while (itr1.hasNext() ) {
			
			BranchMst lm = (BranchMst) itr1.next();
			if(!lm.getBranchCode().equalsIgnoreCase(SystemSetting.systemBranch))
			cmbToBranch.getItems().add(lm.getBranchCode());
		}
		eventBus.register(this);


		// ---------validating for not able to enter string in no. txt field---//
		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});
	}
	@FXML
	void OnActionSave(ActionEvent event) {
		/*
		 * Final Save
		 */
		System.out.println("inside save.........................");
		
		
		ResponseEntity<List<IntentDtl>> intentdtlSaved = RestCaller.getIntentDtl(intentHdr);
		if (intentdtlSaved.getBody().size() == 0) {
			return;
		} 
		else 
		{
		String financialYear = SystemSetting.getFinancialYear();
		String vNo = RestCaller.getVoucherNumber(financialYear + "INT"+SystemSetting.systemBranch);
		intentHdr.setVoucherNumber(vNo);
		RestCaller.updateIntenthdr(intentHdr);
		notifyMessage(5,"Details Saved Successfully!!!!");
		tblIntent.getItems().clear();
		cmbToBranch.getSelectionModel().clearSelection();
//		dpDate.setValue(null);
		intentHdr = null;
//		Format formatter;
//		formatter = new SimpleDateFormat("yyyy-MM-dd");
//		String strDate = formatter.format(intentHdr.getVoucherDate());
//		
//		try {
//			JasperPdfReportService.IntentVoucherReport(vNo,strDate);
//		} catch (JRException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		
		txtItemId.setText("");
		txtUnitName.setText("");
		txtQty.setText("");
		}
	}

	@FXML
	void tableToDelete(MouseEvent event) {
//========= table to textbox========//
		if (tblIntent.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tblIntent.getSelectionModel();
			intentListTable = selectionModel.getSelectedItems();
			for (IntentDtl idt : intentListTable) {
				intentDtl = new IntentDtl();
				intentDtl.setId(idt.getId());
				intentDtl.setUnitId(idt.getUnitId());
				//txtItemId.setText(idt.getItemId());
				txtUnitName.setText(idt.getUnitName());
				txtQty.setText(Double.toString(idt.getQty()));
				ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(idt.getItemId());
				txtItemId.setText(respentity.getBody().getItemName());//==set itemname to display ==========//
			}
		}
	}

	private void loadItemPopup() {
		/*
		 * Function to display popup window and show list of items to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtQty.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	private void addItem()
	{
		

		if (null==dpDate.getValue()) {
			notifyMessage(5, " Please enter Date...!!!");
		} 
		 if (null == cmbToBranch.getValue()) {
			notifyMessage(5, " Please select branch...!!!");
			return;
		}
		else
		{
			if (null == intentHdr) {
				intentHdr = new IntentHdr();
			//	String toBranch = RestCaller.getBranchCode(cmbToBranch.getSelectionModel().getSelectedItem());
								
			    intentHdr.setToBranch(cmbToBranch.getSelectionModel().getSelectedItem());
			
		intentHdr.setBranchCode(SystemSetting.getSystemBranch());
		intentHdr.setCompanyId(0);
		
//		intentHdr.setUserId("userId");
//		intentHdr.setMachineId("machineId");
		intentHdr.setFromBranch(SystemSetting.getSystemBranch());
//		intentHdr.setVoucherType("voucherType");
		Date dateIntent = Date.valueOf(dpDate.getValue());
		intentHdr.setVoucherDate(dateIntent);
		intentHdr.setVoucherType("INTENT REQUESTS");
		ResponseEntity<IntentHdr> respentity = RestCaller.saveIntentHdr(intentHdr);
		intentHdr = respentity.getBody();
		System.out.println("added intent hdr" + intentHdr);
			}
	
			
	
	if(null != intentHdr.getId())
	{
		if(null != intentDtl)
		{
		if(null != intentDtl.getId())
		{
			RestCaller.deleteIntentDtl(intentDtl.getId());
			ResponseEntity<List<IntentDtl>> getIntent = RestCaller.getIntentDtl(intentHdr);
			intentListTable = FXCollections.observableArrayList(getIntent.getBody());
		}
		}
	intentDtl = new IntentDtl();
	intentDtl.setIntentHdr(intentHdr);
	
	
	if (txtItemId.getText().trim().isEmpty()) {
		notifyMessage(5, " Please enter Item Name...!!!");

	}
	 else if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Please enter quantity...!!!");
		}
	
	 else {
	
		 ResponseEntity<ItemMst> itemmst = RestCaller.getItemByNameRequestParam(txtItemId.getText());
		 intentDtl.setItemId(itemmst.getBody().getId());//here we get id of item from itempopup to show .for that we create jzt aitem name in entity and set and get.bt it will not save jzt show in fill table.in fill table clitemid is set to show item name//
		 intentDtl.setUnitId(itemmst.getBody().getUnitId());
		 intentDtl.setQty(Double.parseDouble(txtQty.getText()));
		 intentDtl.setRate(00.0);
	
		 if(null != txtRemark.getText())
		 {
			 intentDtl.setRemark(txtRemark.getText());
		 }
		 
		 intentDtl.setToBranch(cmbToBranch.getSelectionModel().getSelectedItem());
		 ResponseEntity<IntentItemBranchMst> getIntentItemBranch = RestCaller.getIntentItemBranchMstByItemId(itemmst.getBody().getId());
		 if(null == getIntentItemBranch.getBody())
		 {
			 IntentItemBranchMst intentItemBranchMst = new IntentItemBranchMst();
			 intentItemBranchMst.setBranchCode(cmbToBranch.getSelectionModel().getSelectedItem());
			 intentItemBranchMst.setItemId(itemmst.getBody().getId());
			 ResponseEntity<IntentItemBranchMst> saveintentItem = RestCaller.saveIntentItemBranchMst(intentItemBranchMst);
		 }
		 else if (null != getIntentItemBranch.getBody())
		 {
			 ResponseEntity<IntentItemBranchMst> getIntentItemBranchByBranch = RestCaller.getIntentItemBranchMstByItemIdByBranch(itemmst.getBody().getId(),cmbToBranch.getValue());
			
			 if(null == getIntentItemBranchByBranch.getBody())
			 {
				 IntentItemBranchMst intentItemBranchMst = new IntentItemBranchMst();
				 intentItemBranchMst.setItemId(itemmst.getBody().getId());
				 intentItemBranchMst.setBranchCode(cmbToBranch.getSelectionModel().getSelectedItem());
				 RestCaller.updateIntentItemBranch(intentItemBranchMst);
			 }
			 
		 }
		 
		 ResponseEntity<List<ItemPropertyConfig>> itempropertylist = RestCaller.getItemPropertyConfigByItemId(intentDtl.getItemId());
			if(itempropertylist.getBody().size()>0)
			{
				/*
				 * Generate pop up for itemproperty
				 */
				loadItemPropertyPopup(intentDtl.getItemId());
			}
			
			
			
			
		
	 } 
	
	}
		
	}
	}
	private void loadItemPropertyPopup(String itemId) {
		


		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/itemPropetyIntet.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			ItemPropertyIntentCtl itempropertyPopup = loader.getController();
			
			itempropertyPopup.setItemProperties(itemId);
		
//			 root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.WINDOW_MODAL);
			stage.show();
//			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
	

	
	}
	
	@Subscribe
	public void popupItemPropertyListner(List<ItemPropertyIntent> itemPropertyIntent) {


		Stage stage = (Stage) btnAdd.getScene().getWindow();
	
		if (stage.isShowing()) {
			//String jsonStr = Obj.writeValueAsString(org);
			
			ObjectMapper Obj = new ObjectMapper();
			String jsonPropertString;
			try {
				jsonPropertString = Obj.writeValueAsString(itemPropertyIntent);
				intentDtl.setItemPropertyAsJson(jsonPropertString);
				
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			 ResponseEntity<IntentDtl> respentity1 = RestCaller.saveIntentDtl(intentDtl);
			 intentDtl = respentity1.getBody();
		
			
			 ResponseEntity<List<IntentDtl>> intentDtlList = RestCaller.getIntentDtl(intentDtl.getIntentHdr());
			 
			 intentListTable = FXCollections.observableArrayList(intentDtlList.getBody());
			 FillTable();

			 txtItemId.setText("");
			 txtUnitName.setText("");
			 txtRemark.clear();
			 txtQty.setText("");
			 cmbToBranch.getSelectionModel().clearSelection();
			 intentDtl = null;
			 cmbToBranch.getSelectionModel().clearSelection();

		}
	}
	@Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }
	
}
