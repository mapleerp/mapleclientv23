package com.maple.mapleclient.controllers;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;

public class ServiceReportTreeCtl {
	
	String taskid;
	String processInstanceId;

    String selectedText = null;

    @FXML
    private TreeView<String> rootTree;
    

    
    @FXML
    void initialize() {
    	
    	TreeItem rootItem = new TreeItem("SERVICE REPORT");
		//TreeItem masterItem = new TreeItem("MASTERS");
		
	HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
		
		
		
		ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("SERVICE REPORT");
		
		List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
		
		MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
		
		String parentId =aMenuConfigMst.getId();
		
		ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
		List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

		
		for(MenuConfigMst aMenuConfig : menuConfigMstList)
		{
			
			//masterItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
			
			if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
				rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
				}
			
			
		}
		
		//rootItem.getChildren().add(masterItem);

		rootTree.setRoot(rootItem);
		
		rootTree.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text 12345 : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();
				
				
				ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller.MenuConfigMstByMenuName(selectedText);
				
				List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();
		
				MenuConfigMst aMenuConfig = menuConfigMstList.get(0);
				
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != selectedText)
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(selectedText, dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
			
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
				  
				
			/*	if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.kotManagement);

				}
				
				*/
				
				

			}

		});
//    	TreeItem rootItem = new TreeItem("Service Reports");
//    	
//
//    	TreeItem serviceInReport = new TreeItem("SERVICE IN REPORT");
//		if (SystemSetting.UserHasRole("SERVICE IN REPORT")) {
//			rootItem.getChildren().add(serviceInReport);
//    	}
//		TreeItem jobcardReport = new TreeItem("JOBCARD REPORT");
//		if (SystemSetting.UserHasRole("JOBCARD REPORT")) {
//			rootItem.getChildren().add(jobcardReport);
//    	}
//		TreeItem activeJobcardReport = new TreeItem("PENDING JOBCARDS REPORTS");
//		if (SystemSetting.UserHasRole("PENDING JOBCARDS REPORTS")) {
//			rootItem.getChildren().add(activeJobcardReport);
//    	}
//	
//		
//		TreeItem SalesAnalysis = new TreeItem("SERVICE ANALYSIS REPORT");
//		if (SystemSetting.UserHasRole("SERVICE ANALYSIS REPORT")) {
//			rootItem.getChildren().add(SalesAnalysis);
//    	}
//		
//		
//		rootTree.setRoot(rootItem);
//		
//		rootTree.getSelectionModel().selectedItemProperty().addListener( new ChangeListener() {
//  		   @Override
//             public void changed(ObservableValue observable, Object oldValue,
//                     Object newValue) {
//
//                 TreeItem<String> selectedItem = (TreeItem<String>) newValue;
//                 System.out.println("Selected Text : " + selectedItem.getValue());
//                 selectedText = selectedItem.getValue();
//                 
//                 
//                 if(selectedItem.getValue().equalsIgnoreCase("SERVICE IN REPORT")) {
//                    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//                    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ServiceInReport);
//                    
//                    }
//                 if(selectedItem.getValue().equalsIgnoreCase("SERVICE ANALYSIS REPORT")) {
//                 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//                 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.SalesAnalysis);
//                 
//                 }
//                 
//                 if(selectedItem.getValue().equalsIgnoreCase("JOBCARD REPORT")) {
//                 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//                 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.JobcardReport);
//                 
//                 }
//                 
//                 if(selectedItem.getValue().equalsIgnoreCase("PENDING JOBCARDS REPORTS")) {
//                  	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                  		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//                  	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ActiveJobCardReport);
//                  
//                  }
//                 if(selectedItem.getValue().equalsIgnoreCase("ITEM OR SERVICE WISE PROFIT")) {
//                   	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//                   		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//                   	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.serviceOrItemProfit);
//                   
//                   }
//                 
//  		   }   
//   		 
//		  });
//

    }

    @FXML
    void serviceOnClick(MouseEvent event) {
    	

    	if (null != selectedText) {	//1. Fetch command and fx from table based on the clicked item text.
			//2. load the fx similar way we did in the main frame
			
		 
			HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
			
			
			
			ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName(selectedText);
			
			List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
			
			MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
			
			
			ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(aMenuConfigMst.getId());
			List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();
	
			
			for(MenuConfigMst aMenuConfig : menuConfigMstList)
			{
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != aMenuConfig.getMenuFxml())
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(aMenuConfig.getMenuName(), dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
	 
				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						/*MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
						 
						*/
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
			}
    	}
//    	
//    	if(null != selectedText)
//    	{
//    		  
//            if(selectedText.equalsIgnoreCase("SERVICE IN REPORT")) {
//               	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//               		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//               	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ServiceInReport);
//               
//               }
//            
//            if(selectedText.equalsIgnoreCase("JOBCARD REPORT")) {
//            	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//            		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//            	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.JobcardReport);
//            
//            }
//            
//            if(selectedText.equalsIgnoreCase("PENDING JOBCARDS REPORTS")) {
//             	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//             		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//             	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ActiveJobCardReport);
//             
//             }
//          
//            
//    	}

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}


}
