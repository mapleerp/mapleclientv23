package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.BranchSaleReport;
import com.maple.report.entity.PhysicalStockReport;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class BranchSaleReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<BranchSaleReport> branchSaleReportListTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> branchSaleDtlReportListTable = FXCollections.observableArrayList();

	SalesTransHdr salesTransHdr = null;
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnReport;

    @FXML
    private TableView<BranchSaleReport> tblHdrReport;

    @FXML
    private TableColumn<BranchSaleReport, String> clVoucherDate;

    @FXML
    private TableColumn<BranchSaleReport, String> clVoucherNumber;

    @FXML
    private TableColumn<BranchSaleReport, String> clCustomer;

    @FXML
    private TableColumn<BranchSaleReport, String> clToBranch;

    @FXML
    private TableColumn<BranchSaleReport, Number> clTotal;
    
    @FXML
    private TableColumn<BranchSaleReport, String> clBranchVoucherNo;

    @FXML
    private Button btnRetry;
    @FXML
    private TextField txtTotal;

    @FXML
    private TableView<SalesDtl> tblDtlReport;

    @FXML
    private TableColumn<SalesDtl, String> clItemName;

    @FXML
    private TableColumn<SalesDtl, String> clUnit;

    @FXML
    private TableColumn<SalesDtl, String> clQty;

    @FXML
    private TableColumn<SalesDtl, String> clBatch;

    @FXML
    private TableColumn<SalesDtl, Number> clAmount;
    
    
    
    @FXML
    private Button btnRePrint;
    
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblHdrReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	    		if (newSelection != null) {
	    			System.out.println("getSelectionModel");
	    			if (null != newSelection.getVoucherNumber()) {
	    				
	    				 salesTransHdr = RestCaller.
	    						getSalesTransHdrByVoucherAndDate(newSelection.getVoucherNumber(), SystemSetting.UtilDateToString(newSelection.getVoucherDate(), "yyyy-MM-dd"));
	    			
	    				ResponseEntity<List<SalesDtl>> salesDtlResp = RestCaller.getSalesDtl(salesTransHdr);
	    				branchSaleDtlReportListTable  = FXCollections.observableArrayList(salesDtlResp.getBody());
	    		    	FillDtlTable();
	    			}
	    		}
	    	});
    	
    }
    
    @FXML
    void RePrint(ActionEvent event) {
    	
    	if(null != salesTransHdr)
    	{
    		String msg = RestCaller.resendOtherBranchSales(salesTransHdr.getId());
    	}

    }

    private void FillDtlTable() {
    	
    	tblDtlReport.setItems(branchSaleDtlReportListTable);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	}

	@FXML
    void GenerateReport(ActionEvent event) {
    	
      	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(3, "Please Select From Date", false);
    		return;
    	}
    	
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(3, "Please Select To Date", false);
    		return;
    	}
    	
    	java.util.Date uFromDate = Date.valueOf(dpFromDate.getValue());
    	String strDate = SystemSetting.UtilDateToString(uFromDate, "yyyy-MM-dd");
    	
    	java.util.Date uToDate = Date.valueOf(dpToDate.getValue());
    	String toDate = SystemSetting.UtilDateToString(uToDate, "yyyy-MM-dd");

    	ResponseEntity<List<BranchSaleReport>> branchSaleReportResp = RestCaller.getBranchSaleReportSummary(strDate,toDate);
    	branchSaleReportListTable = FXCollections.observableArrayList(branchSaleReportResp.getBody());
    	
    	FillTable();

    }
    

    @FXML
    void actionRetry(ActionEvent event) {
    	String msg = RestCaller.resendOtherBranchSales(salesTransHdr.getId());

    }
	private void FillTable() {

		tblHdrReport.setItems(branchSaleReportListTable);
		Double total = 0.0;
		for(BranchSaleReport saleReport : branchSaleReportListTable)
		{
			total = total + saleReport.getInvoiceTotal();
		}
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
		clToBranch.setCellValueFactory(cellData -> cellData.getValue().getToBranchNameProperty());
		clTotal.setCellValueFactory(cellData -> cellData.getValue().getInvoiceTotalProperty());
		clBranchVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getBranchVoucherNoProperty());
		
		BigDecimal bdCashToPay = new BigDecimal(total);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		txtTotal.setText(bdCashToPay.toPlainString());

	}


	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	       private void PageReload() {
	       	
	 }

}
