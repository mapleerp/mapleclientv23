package com.maple.mapleclient.controllers;

import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ProductMst;
import com.maple.mapleclient.entity.WatchStrapMst;
import com.maple.mapleclient.events.ProductEvent;
import com.maple.mapleclient.events.WatchStrapEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class WatchStrapPopupCtl {
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
    private ObservableList<WatchStrapMst> strapList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

	WatchStrapEvent watchStrapEvent = null;


    @FXML
    private TextField txtProductName;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;

    @FXML
    private TableView<WatchStrapMst> tbProducts;

    @FXML
    private TableColumn<WatchStrapMst, String> clProductName;

    @FXML
    void actionCancel(ActionEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
	private void initialize() {
    	LoadAccountPopupBySearch("");
    	txtProductName.textProperty().bindBidirectional(SearchString);

    	eventBus.register(this);
    	watchStrapEvent = new WatchStrapEvent();
    	tbProducts.setItems(strapList);
    	clProductName.setCellValueFactory(cellData -> cellData.getValue().getStrapNameProperty());
    	tbProducts.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			    if (newSelection != null) {
			    	if(null!=newSelection.getId()&& newSelection.getStrapName().length()>0) {
			    		watchStrapEvent.setStrapId(newSelection.getId());
			    		watchStrapEvent.setStrapName(newSelection.getStrapName());
			    	}
			    }
			});
    	
    	 
   	 SearchString.addListener(new ChangeListener(){
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				 					
					LoadAccountPopupBySearch((String)newValue);
					
				}
	        });
    }
    @FXML
    void actionOk(ActionEvent event) {
    	if(null != watchStrapEvent.getStrapId())
    	{
    	eventBus.post(watchStrapEvent); 
    	}
    	else
    	{
    		watchStrapEvent.setStrapName(txtProductName.getText());
    		eventBus.post(watchStrapEvent);
    	}
    	Stage stage = (Stage) btnOk.getScene().getWindow();
		stage.close();
    }

    @FXML
    void onKeyPresstxt(KeyEvent event) {
    	Stage stage = (Stage) btnOk.getScene().getWindow();
    	if (event.getCode() == KeyCode.ENTER)
    
    	{
    	if(null != watchStrapEvent.getStrapId())
    	{
    	eventBus.post(watchStrapEvent); 
    	stage.close();
    	}
    	else
    	{
    		watchStrapEvent.setStrapName(txtProductName.getText());
    		eventBus.post(watchStrapEvent);
    		stage.close();
    	}
    	}
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tbProducts.requestFocus();
    		tbProducts.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			stage.close();
		}
    }

    @FXML
    void tbOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			
    		Stage stage = (Stage) btnOk.getScene().getWindow();
      
        	if(null != watchStrapEvent.getStrapId())
        	{
        	eventBus.post(watchStrapEvent); 
        	stage.close();
        	}
        	else
        	{
        		watchStrapEvent.setStrapName(txtProductName.getText());
        		eventBus.post(watchStrapEvent);
        		stage.close();
        	}
		}
    }

    
    private void LoadAccountPopupBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ArrayList strapArray = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		strapList.clear();

		strapArray = RestCaller.SearchWatchStrapByName(searchData);
		String id;
		String strapName;
		Iterator itr = strapArray.iterator();
		System.out.println("accaccaccacc22");
		while (itr.hasNext()) {
			
			List element = (List) itr.next();
			strapName = (String) element.get(1);
			  id = (String) element.get(0);
			 if (null != id) {
				 WatchStrapMst watchStrapMst = new WatchStrapMst();
				 watchStrapMst.setId(id);
				 watchStrapMst.setStrapName(strapName);
				 strapList.add(watchStrapMst);
		}
    }
		return;
}
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}
}
