
package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JobCardDtl;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.LocationMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.SalesTypeMst;
import com.maple.mapleclient.entity.ServiceInDtl;
import com.maple.mapleclient.entity.ServiceInReceipt;
import com.maple.mapleclient.entity.ServiceItemMst;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.JobCardEvent;
import com.maple.mapleclient.events.LocationEvent;
import com.maple.mapleclient.events.ServiceItemEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

public class JobCardPreparationCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<JobCardDtl> jobCardDtlList = FXCollections.observableArrayList();

	String batch = null;
	JobCardHdr jobCardHdr;

	JobCardDtl jobCardDtl;

	SalesTransHdr salesTransHdr;

	SalesDtl salesDtl = null;

	String location = null;

	EventBus eventBus = EventBusFactory.getEventBus();

	SalesReceipts salesReceipts = null;
	String salesReceiptVoucherNo = null;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtServiceItem;

	@FXML
	private TextField txtItemQty;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnFinalSave;

	@FXML
	private Button btnHold;

	@FXML
	private TextField txtServiceQty;

	@FXML
	private Button btnReady;

	@FXML
	private TextField txtCustomer;

	@FXML
	private TextField txtServiceInItem;

	@FXML
	private TextField txtLocation;

	@FXML
	private TableView<JobCardDtl> tblJobCard;

	@FXML
	private TableColumn<JobCardDtl, String> clItemName;

	@FXML
	private TableColumn<JobCardDtl, Number> clQty;

	@FXML
	private TableColumn<JobCardDtl, Number> clMrp;

	@FXML
	private TableColumn<JobCardDtl, Number> clTax;

	@FXML
	private TableColumn<JobCardDtl, Number> clCess;

	@FXML
	private TableColumn<JobCardDtl, Number> clAmount;

	@FXML
	private TextField txtAdvanceAmount;

	@FXML
	private TextField txtCashToPay;

	@FXML
	private TextField txtBalance;

	@FXML
	void Ready(ActionEvent event) {

		if (null != jobCardHdr) {
			jobCardHdr.setStatus("READY FOR DELIVERY");
//    		String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
//			java.sql.Date date = java.sql.Date.valueOf(sdate);
//			jobCardHdr.setVoucherDate(date);
			RestCaller.updateJobCardHdr(jobCardHdr);
			clearField();
		}

	}

	@FXML
	private void initialize() {

		eventBus.register(this);

		txtItemQty.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtItemQty.setText(oldValue);
				}
			}
		});

		txtServiceQty.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtServiceQty.setText(oldValue);
				}
			}
		});

		tblJobCard.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					jobCardDtl = new JobCardDtl();
					jobCardDtl.setId(newSelection.getId());

				}
			}

		});

	}

	@FXML
	void LocationPopupOnClick(MouseEvent event) {
		showLocationPopup();
	}

	@FXML
	void AddOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void AddItems(ActionEvent event) {
		addItem();

	}

	private void addItem() {

		if (txtServiceInItem.getText().trim().isEmpty() && txtCustomer.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Select Item And Customer ", false);
			return;
		}

		if (!txtItemName.getText().trim().isEmpty() && txtServiceItem.getText().trim().isEmpty()) {

			if (txtItemQty.getText().trim().isEmpty()) {
				notifyMessage(5, "Please Enter Qty ", false);
				txtItemQty.requestFocus();
				return;
			}

		}

		if (txtItemName.getText().trim().isEmpty() && !txtServiceItem.getText().trim().isEmpty()) {

			if (txtServiceQty.getText().trim().isEmpty()) {
				notifyMessage(5, "Please Enter Qty ", false);
				txtServiceQty.requestFocus();
				return;
			}

		}
		
		if(txtItemName.getText().trim().isEmpty() && 
				txtServiceItem.getText().trim().isEmpty() && 
				txtLocation.getText().trim().isEmpty())
		{
			return;
		}
		
		if (null != jobCardHdr) {
	//---------------------------------------------------------------------------------
		if(!txtItemName.getText().trim().isEmpty() && !txtItemQty.getText().trim().isEmpty())
		{
		ArrayList items = new ArrayList();
		
		items = RestCaller.getSingleStockItemByName(txtItemName.getText(), batch);
		Double chkQty = 0.0;
		String itemId = null;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			List element = (List) itr.next();
			chkQty = (Double) element.get(4);
			itemId = (String) element.get(7);
		}
		 if (chkQty < Double.parseDouble(txtItemQty.getText())) {
			notifyMessage(1, "Not in Stock!!!",false);
			txtItemName.clear();
			txtServiceItem.clear();
			txtItemQty.clear();
			txtServiceQty.clear();
			return;
		}
		 
		 ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		 ItemMst itemMst = itemResp.getBody();
		 Double itemsqty = 0.0;
			itemsqty = RestCaller.JobCardDtlItemQty(jobCardHdr.getId(), itemMst.getId());
			Double qty = 0.0;
			qty = Double.parseDouble(txtItemQty.getText());
		 if (chkQty < (itemsqty +qty)) {
			 notifyMessage(1, "Not in Stock!!!",false);
				txtItemName.clear();
				txtServiceItem.clear();
				txtItemQty.clear();
				txtServiceQty.clear();
			return;
		}
		
		}
//-------------------------------------------------
		

	
		
		
		
//--------------------------------------------------------------------------------------		

			jobCardDtl = new JobCardDtl();

			if (!txtItemName.getText().isEmpty()) {
				ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
				ItemMst itemMst = itemResp.getBody();
				jobCardDtl.setItemId(itemMst.getId());

				jobCardDtl.setQty(Double.parseDouble(txtItemQty.getText()));

			}

			if (!txtServiceItem.getText().isEmpty()) {
				ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(txtServiceItem.getText());
				ItemMst itemMst = itemResp.getBody();
				jobCardDtl.setItemId(itemMst.getId());

				jobCardDtl.setQty(Double.parseDouble(txtServiceQty.getText()));

			}
			if (txtItemName.getText().isEmpty() && txtServiceItem.getText().isEmpty()) {
				jobCardDtl.setQty(0.0);
			}
			jobCardDtl.setJobCardHdr(jobCardHdr);
			jobCardDtl.setLocationId(location);
			
			ResponseEntity<JobCardDtl> jobCardDtlResp = RestCaller.saveJobCardDtl(jobCardDtl);
			jobCardDtl = jobCardDtlResp.getBody();
			
			ResponseEntity<List<JobCardDtl>> jobCardDtl = RestCaller.getJobCardDtlByHdrId(jobCardHdr.getId());
			jobCardDtlList = FXCollections.observableArrayList(jobCardDtl.getBody());

			txtItemName.clear();
			txtItemQty.clear();
			txtLocation.clear();
			txtServiceItem.clear();
			txtServiceQty.clear();
			batch = null;

			jobCardDtl = null;
			fillTable();
		}

	}

	private void fillTable() {

		for (JobCardDtl jobCard : jobCardDtlList) {
			if (null != jobCard.getItemId()) {
				ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(jobCard.getItemId());
				ItemMst itemMst = itemResp.getBody();
				jobCard.setItemName(itemMst.getItemName());
				jobCard.setMrp(itemMst.getStandardPrice());
				jobCard.setCess(itemMst.getCess());
				jobCard.setTax(itemMst.getTaxRate());
				jobCard.setAmount(itemMst.getStandardPrice() * jobCard.getQty());
			}
			if (null != jobCard.getLocationId()) {
				ResponseEntity<LocationMst> locationResp = RestCaller.findLocationMstById(jobCard.getLocationId());
				LocationMst locationMst = new LocationMst();

				locationMst = locationResp.getBody();
				if (null != locationMst) {
					jobCard.setItemName(locationMst.getLocation());
					jobCard.setQty(0.0);
					jobCard.setMrp(0.0);
					jobCard.setCess(0.0);
					jobCard.setTax(0.0);
					jobCard.setAmount(0.0);
				}
			}

			if (null == jobCard.getQty()) {
				jobCard.setQty(0.0);
				jobCard.setMrp(0.0);
				jobCard.setCess(0.0);
				jobCard.setTax(0.0);
				jobCard.setAmount(0.0);
			}
		}

		tblJobCard.setItems(jobCardDtlList);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clCess.setCellValueFactory(cellData -> cellData.getValue().getCessProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		if(null != jobCardHdr.getServiceInDtl().getServiceInHdr().getAdvanceAmount())
		{
		BigDecimal bdCashToPay1 = new BigDecimal(jobCardHdr.getServiceInDtl().getServiceInHdr().getAdvanceAmount());
		bdCashToPay1 = bdCashToPay1.setScale(2, BigDecimal.ROUND_CEILING);
		txtAdvanceAmount.setText(bdCashToPay1.toString());
		} else {
			jobCardHdr.getServiceInDtl().getServiceInHdr().setAdvanceAmount(0.0);
			txtAdvanceAmount.setText("0.0");
		}
		

		Double invoiceTotal = RestCaller.getInvoiceAmountByJobCardHdrId(jobCardHdr.getId());

		BigDecimal bdCashToPay = new BigDecimal(invoiceTotal);
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);

		txtCashToPay.setText(bdCashToPay.toString());

		Double balance = invoiceTotal - jobCardHdr.getServiceInDtl().getServiceInHdr().getAdvanceAmount();

		BigDecimal bdCashToPay2 = new BigDecimal(balance);
		bdCashToPay2 = bdCashToPay2.setScale(2, BigDecimal.ROUND_CEILING);

		txtBalance.setText(bdCashToPay2.toString());

	}

	@FXML
	void LocationPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			showLocationPopup();
		}

	}

	@FXML
	void DeleteItems(ActionEvent event) {

		if (null != jobCardDtl) {
			if (null != jobCardDtl.getId()) {
				RestCaller.deleteJobCardDtlById(jobCardDtl.getId());
				ResponseEntity<List<JobCardDtl>> jobCardDtl = RestCaller.getJobCardDtlByHdrId(jobCardHdr.getId());
				jobCardDtlList = FXCollections.observableArrayList(jobCardDtl.getBody());
				fillTable();
			}
			jobCardDtl = null;
		}

	}

	@FXML
	void ItemPopupOnClick(MouseEvent event) {

		showPopup();

	}

	@FXML
	void ItemPopupOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			showPopup();

		}

	}

	@FXML
	void ServicePopupOnClick(MouseEvent event) {

		showServiceItemPopup();

	}

	@FXML
	void ServicePopupOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			showServiceItemPopup();

		}

	}

	@FXML
	void finalSave(ActionEvent event) {

		if (null != jobCardHdr) {
			if (!jobCardHdr.getStatus().equalsIgnoreCase("READY FOR DELIVERY")) {
				notifyMessage(5, "Please set jobcard ready for delivery ", false);
				return;
			}
			ResponseEntity<List<JobCardDtl>> jobCardDtl = RestCaller.getJobCardDtlByHdrId(jobCardHdr.getId());
			List<JobCardDtl> jobCardDtlList = jobCardDtl.getBody();

			if (jobCardDtlList.size() == 0) {
				return;
			}

			String financialYear = SystemSetting.getFinancialYear();
			String voucheNo = RestCaller.getVoucherNumber(financialYear + "JC");
			jobCardHdr.setVoucherNumber(voucheNo);

			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			java.sql.Date date = java.sql.Date.valueOf(sdate);

			jobCardHdr.setVoucherDate(date);

			jobCardHdr.setStatus("CLOSED");

			SalesTransHdr salesTransHdr = SaveSalesTransHdr(jobCardHdr);

			if (null == salesTransHdr) {
				return;
			}

			boolean addItem = addSalesTransHdrDtl(jobCardHdr);

			if (addItem == false) {
				return;
			}

			SalesTransHdrFinalSave();

			jobCardHdr.setSalesTransHdrId(salesTransHdr.getId());

			RestCaller.updateJobCardHdr(jobCardHdr);

//			String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
//			try {
//				JasperPdfReportService.JobCardInvoice(voucheNo, sdate, jobCardHdr);
//			} catch (JRException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}

		}

		clearField();

		txtAdvanceAmount.clear();
		txtBalance.clear();
		txtCashToPay.clear();
		batch = null;
	}

	private void SalesTransHdrFinalSave() {
		Double invoiceAmount = 0.0;

		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
		if (null != summary.getTotalAmount()) {
			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
			invoiceAmount = bdCashToPay.doubleValue();
		}

		salesTransHdr.setInvoiceAmount(invoiceAmount);

		salesTransHdr.setInvoiceDiscount(0.0);

		salesTransHdr.setCashPay(0.0);

		salesTransHdr.setCardamount(0.0);

		salesTransHdr.setChangeAmount(0.0);
		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		if (saledtlSaved.getBody().size() == 0) {
			return;
		}

		ResponseEntity<List<SalesTypeMst>> salesTypeResp = RestCaller.getSalesTypeMst();
		List<SalesTypeMst> salesTypeList = salesTypeResp.getBody();
		if (salesTypeList.size() == 0) {
			notifyMessage(5, "Please add Voucher Type", false);
			return;
		}
		salesTransHdr.setInvoiceNumberPrefix(salesTypeList.get(0).getSalesPrefix());

		LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		Date date = SystemSetting.systemDate;
		salesTransHdr.setVoucherDate(date);

		salesTransHdr.setIsBranchSales("N");

		ResponseEntity<AccountHeads> custentity = RestCaller.getAccountHeadsById(salesTransHdr.getCustomerId());
		ResponseEntity<AccountReceivable> accountReceivableResp = RestCaller
				.getAccountReceivableBySalesTransHdrId(salesTransHdr.getId());
		AccountReceivable accountReceivable = null;
		if (null != accountReceivableResp.getBody()) {
			accountReceivable = accountReceivableResp.getBody();
		} else {
			accountReceivable = new AccountReceivable();
		}

		accountReceivable.setAccountId(salesTransHdr.getCustomerId());
		accountReceivable.setAccountHeads(custentity.getBody());

		accountReceivable.setDueAmount(invoiceAmount - salesTransHdr.getPaidAmount());
		accountReceivable.setDueAmount(invoiceAmount - salesTransHdr.getPaidAmount());
		accountReceivable.setBalanceAmount(invoiceAmount - salesTransHdr.getPaidAmount());

		LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		LocalDate dueDate = due.plusDays(custentity.getBody().getCreditPeriod());
		accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
		accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
		accountReceivable.setSalesTransHdr(salesTransHdr);
		accountReceivable.setRemark("Wholesale");
		LocalDate due1 = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		accountReceivable.setVoucherDate(java.sql.Date.valueOf(due1));

		accountReceivable.setPaidAmount(salesTransHdr.getPaidAmount());
		ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
		accountReceivable = respentity.getBody();

		SalesReceipts salesReceipts = new SalesReceipts();
		salesReceipts.setReceiptMode("CREDIT");
		salesReceipts.setReceiptAmount(invoiceAmount - salesTransHdr.getPaidAmount());
		salesReceipts.setSalesTransHdr(salesTransHdr);
		salesReceipts.setAccountId(custentity.getBody().getId());
		salesReceipts.setBranchCode(salesTransHdr.getBranchCode());

		RestCaller.saveSalesReceipts(salesReceipts);

		if (null == salesTransHdr.getVoucherNumber()) {
			RestCaller.updateSalesTranshdr(salesTransHdr);
		}

		salesTransHdr.setSalesMode("JOBCARD CONVERTION");
		salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());

		notifyMessage(5, "Sales Saved", true);
		DayBook dayBook = new DayBook();
		dayBook.setBranchCode(salesTransHdr.getBranchCode());
		dayBook.setDrAccountName(custentity.getBody().getAccountName());
		dayBook.setDrAmount(salesTransHdr.getInvoiceAmount());
		dayBook.setNarration(custentity.getBody().getAccountName() + salesTransHdr.getVoucherNumber());
		dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
		dayBook.setSourceVoucherType("SALES");
		dayBook.setCrAccountName("SALES ACCOUNT");
		dayBook.setCrAmount(salesTransHdr.getInvoiceAmount());

		LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
		dayBook.setsourceVoucherDate(java.sql.Date.valueOf(rdate));
		ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(salesTransHdr.getVoucherDate());

		try {
			JasperPdfReportService.TaxInvoiceReport(salesTransHdr.getVoucherNumber(), strDate);
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	private boolean addSalesTransHdrDtl(JobCardHdr jobCardHdr) {
		ResponseEntity<List<JobCardDtl>> jobCardDtl = RestCaller.getJobCardDtlByHdrId(jobCardHdr.getId());
		List<JobCardDtl> jobCardDtlList = jobCardDtl.getBody();

		if (jobCardDtlList.size() == 0) {
			return false;
		}

		for (JobCardDtl jobCard : jobCardDtlList) {
			if (null != jobCard.getItemId()) {
				salesDtl = new SalesDtl();
				Double mrpRateIncludingTax = 00.0;
				Double taxRate = 0.0;
				double cessRate = 0.0;

				if (null != jobCard.getItemId()) {
					ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(jobCard.getItemId());
					ItemMst itemMst = itemResp.getBody();

					if (null == itemMst) {
						return false;
					}

					salesDtl.setItemName(itemMst.getItemName());
					salesDtl.setBarcode(itemMst.getBarCode());
					salesDtl.setItemCode(itemMst.getItemCode());
					salesDtl.setItemId(itemMst.getId());
					cessRate = itemMst.getCess();

					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(itemMst.getUnitId());
					salesDtl.setUnitId(getUnit.getBody().getId());
					salesDtl.setUnitName(getUnit.getBody().getUnitName());

					mrpRateIncludingTax = itemMst.getStandardPrice();
					taxRate = itemMst.getTaxRate();

				}
				salesDtl.setSalesTransHdr(salesTransHdr);
				salesDtl.setMrp(mrpRateIncludingTax);
				salesDtl.setBatchCode("NOBATCH");
				salesDtl.setQty(jobCard.getQty());
				salesDtl.setTaxRate(taxRate);

				// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());

				Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
				salesDtl.setRate(rateBeforeTax);

				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesDtl.setCgstTaxRate(cgstTaxRate);

				salesDtl.setSgstTaxRate(sgstTaxRate);
				double cessAmount = 0.0;

				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (cessRate > 0) {

						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + cessRate);

						System.out.println("rateBeforeTax---------" + rateBeforeTax);
						salesDtl.setRate(rateBeforeTax);

						cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;

						/*
						 * Recalculate RateBefore Tax if Cess is applied
						 */

					}
				} else {
					cessAmount = 0.0;
					cessRate = 0.0;
				}

				salesDtl.setCessRate(cessRate);
				salesDtl.setCessAmount(cessAmount);

				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					salesDtl.setSgstTaxRate(taxRate / 2);

					salesDtl.setCgstTaxRate(taxRate / 2);

					salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

					salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);

				} else {
					salesDtl.setSgstTaxRate(0.0);

					salesDtl.setCgstTaxRate(0.0);

					salesDtl.setCgstAmount(0.0);

					salesDtl.setSgstAmount(0.0);

					salesDtl.setIgstTaxRate(taxRate);
					salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				}

				BigDecimal settoamount = new BigDecimal(salesDtl.getQty() * mrpRateIncludingTax);
				settoamount = settoamount.setScale(2, BigDecimal.ROUND_CEILING);
				salesDtl.setAmount(settoamount.doubleValue());
				salesDtl.setStandardPrice(mrpRateIncludingTax);
				salesDtl.setAddCessRate(0.0);
				ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
						.getPriceDefenitionMstByName("COST PRICE");
				PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
				if (null != priceDefenitionMst) {
					String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
					ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
							salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);

					PriceDefinition priceDefinition = priceDefenitionResp.getBody();
					if (null != priceDefinition) {
						salesDtl.setCostPrice(priceDefinition.getAmount());
					}
				}
				ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
				salesDtl = respentity.getBody();
			}
		}

		return true;
	}

	private SalesTransHdr SaveSalesTransHdr(JobCardHdr jobCardHdr) {

		salesTransHdr = new SalesTransHdr();
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.getId();

		ResponseEntity<List<SalesTypeMst>> salesTypeResp = RestCaller.getSalesTypeMst();
		List<SalesTypeMst> salesTypeList = salesTypeResp.getBody();
		if (salesTypeList.size() == 0) {
			notifyMessage(5, "Please add Voucher Type", false);
			return null;
		}

		salesTransHdr.setVoucherType(salesTypeList.get(0).getSalesPrefix());
		salesTransHdr.setCustomerId(jobCardHdr.getCustomerId());

		/*
		 * new url for getting account heads instead of customer mst=========05/0/2022
		 */
//		ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerById(jobCardHdr.getCustomerId());
		ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadsById(jobCardHdr.getCustomerId());
		AccountHeads accountHeads = accountHeadsResp.getBody();
		if (null == accountHeads) {
			notifyMessage(5, "Please check customer detials", false);

			return null;
		}
		salesTransHdr.setAccountHeads(accountHeads);

		/*
		 * If Customer Has a valid GST , then B2B Invoice , otherwise its B2C
		 */
		if (null == accountHeads.getPartyGst() || accountHeads.getPartyGst().length() < 13) {
			salesTransHdr.setSalesMode("B2C");
		} else {
			salesTransHdr.setSalesMode("B2B");
		}

		salesTransHdr.setCreditOrCash("CREDIT");
		salesTransHdr.setUserId(SystemSetting.getUser().getId());
		salesTransHdr.setBranchCode(SystemSetting.systemBranch);

		salesTransHdr.setIsBranchSales("N");

		ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
		salesTransHdr = respentity.getBody();

		saveSalesReceipts(salesTransHdr, jobCardHdr.getServiceInDtl());
		Double paidAmpunt = 0.0;
		paidAmpunt = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());

		if (null == paidAmpunt) {
			paidAmpunt = 0.0;
		}
		salesTransHdr.setPaidAmount(paidAmpunt);

		return salesTransHdr;
	}

	private void saveSalesReceipts(SalesTransHdr salesTransHdr2, ServiceInDtl serviceInDtl) {

		ResponseEntity<List<ServiceInReceipt>> savedServiceInReceipt = RestCaller
				.getAllServiceInReceiptByHdrId(serviceInDtl.getServiceInHdr().getId());

		List<ServiceInReceipt> serviceInReceiptList = savedServiceInReceipt.getBody();
		for (ServiceInReceipt serviceInReceipt : serviceInReceiptList) {
			salesReceipts = new SalesReceipts();
			salesReceipts.setReceiptMode(serviceInReceipt.getReceiptMode());
			salesReceipts.setReceiptAmount(serviceInReceipt.getReceiptAmount());
			salesReceipts.setUserId(SystemSetting.getUser().getId());
			salesReceipts.setBranchCode(SystemSetting.systemBranch);
			if (null == salesReceiptVoucherNo) {
				salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
				salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
			} else {
				salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
			}

			salesReceipts.setAccountId(serviceInReceipt.getAccountId());
			LocalDate date = LocalDate.now();
			java.util.Date udate = SystemSetting.localToUtilDate(date);
			salesReceipts.setReceiptDate(udate);
			salesReceipts.setSalesTransHdr(salesTransHdr);

			ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
			salesReceipts = respEntity.getBody();
		}

	}

	@FXML
	void hold(ActionEvent event) {

		clearField();

	}

	private void clearField() {
		txtCustomer.clear();
		txtItemName.clear();
		txtItemQty.clear();
		txtLocation.clear();
		txtServiceInItem.clear();
		txtServiceItem.clear();
		txtServiceQty.clear();
		tblJobCard.getItems().clear();
		jobCardDtlList.clear();

		jobCardHdr = null;
		jobCardDtl = null;
		salesTransHdr = null;
		salesDtl = null;

	}

	@FXML
	void ShowJobCardPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			jobCardPopup();
		}

	}

	@FXML
	void ItemQtyOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtItemQty.getText().trim().isEmpty() && !txtItemName.getText().trim().isEmpty()) {
				btnAdd.requestFocus();
			} else {
				txtServiceItem.requestFocus();
			}
		}

	}

	@FXML
	void serviceQtyOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtServiceItem.getText().trim().isEmpty() && !txtServiceQty.getText().trim().isEmpty()) {
				btnAdd.requestFocus();
			} else {
				txtLocation.requestFocus();
			}
		}

	}

	@FXML
	void ShowJobCardPopupOnClick(MouseEvent event) {

		jobCardPopup();

	}

	private void showServiceItemPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ServiceItemPopup.fxml"));
			Parent root1;

			String cst = null;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtServiceQty.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void showLocationPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/LocationPopup.fxml"));
			Parent root1;

			String cst = null;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			btnAdd.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void jobCardPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/jobCardPopup.fxml"));
			Parent root1;

			String cst = null;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtItemName.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void showPopup() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
			Parent root1;

			String cst = null;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtItemQty.requestFocus();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		try {
			Stage stage = (Stage) txtItemName.getScene().getWindow();
			if (stage.isShowing()) {

				txtItemName.setText(itemPopupEvent.getItemName());
				batch = itemPopupEvent.getBatch();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Subscribe
	public void popupServiceItemlistner(ServiceItemEvent serviceItemEvent) {

		try {
			Stage stage = (Stage) txtItemName.getScene().getWindow();
			if (stage.isShowing()) {

				txtServiceItem.setText(serviceItemEvent.getServiceItemName());

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Subscribe
	public void popupLocationListner(LocationEvent locationEvent) {

		try {
			Stage stage = (Stage) txtLocation.getScene().getWindow();
			if (stage.isShowing()) {

				txtLocation.setText(locationEvent.getLocationName());
				location = locationEvent.getLocationId();

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Subscribe
	public void popupJobCardListner(JobCardEvent jobCardEvent) {

		try {
			Stage stage = (Stage) txtItemName.getScene().getWindow();
			if (stage.isShowing()) {

				txtServiceInItem.setText(jobCardEvent.getItemName());
				txtCustomer.setText(jobCardEvent.getCustomerName());

				ResponseEntity<JobCardHdr> jobCardHdrResp = RestCaller.JobCardHdrById(jobCardEvent.getJobCardId());
				jobCardHdr = jobCardHdrResp.getBody();

				ResponseEntity<List<JobCardDtl>> jobCardDtl = RestCaller.getJobCardDtlByHdrId(jobCardHdr.getId());
				jobCardDtlList = FXCollections.observableArrayList(jobCardDtl.getBody());
				fillTable();

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
