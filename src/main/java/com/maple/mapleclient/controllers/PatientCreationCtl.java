package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Logger;

import org.bouncycastle.crypto.tls.NewSessionTicket;
import org.controlsfx.control.Notifications;
import org.jfree.util.Log;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.InsuranceCompanyDtl;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.entity.PatientMst;
import com.maple.mapleclient.entity.PharmacyPatientType;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import sun.util.logging.resources.logging;

public class PatientCreationCtl {
	AccountHeads accountHeads =null;
	String taskid;
	String processInstanceId;
	private ObservableList<InsuranceCompanyDtl> insuranceCompanyList = FXCollections.observableArrayList();
	private ObservableList<PatientMst> patientMstList = FXCollections.observableArrayList();
	
	private ObservableList<AccountHeads> customerResponse = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	private EventBus eventBus = EventBusFactory.getEventBus();
	
	String custId = "";
	
	@FXML
	private Button btnClose;
	PatientMst patientMst = null;
	@FXML
	private Button btnDelete;
	@FXML
	private TextField txtNationalId;
	@FXML
	private Button btnSave;

	@FXML
	private Button btnShowAll;

	@FXML
	private TextField txtPatientName;

	@FXML
	private TextField txtAddress1;

	@FXML
	private TextField txtAddress2;

	@FXML
	private TextField txtHospitalId;

	@FXML
	private TextField txtContactPerson;
	
	 @FXML
	private TextField txtCorporateCust;

	@FXML
	private ComboBox<String> cmbInsuranceCompany;

	@FXML
	private TextField txtInsuranceCard;

	@FXML
	private ComboBox<String> cmbGender;

	@FXML
	private TextField txtPercentageCoverage;

	@FXML
	private ComboBox<String> cmbNationality;

	@FXML
	private TextField txtContactNo;
	@FXML
	private Button txtClear;
	@FXML
	private DatePicker dpDob;
	@FXML
    private ComboBox<String> cmbPolicyType;

	@FXML
	private DatePicker dpInsCardExpiry;

	@FXML
	private TableView<PatientMst> tblPatient;
	@FXML
	private TableColumn<PatientMst, String> clPatientName;

	@FXML
	private TableColumn<PatientMst, String> clAddress;

	@FXML
	private TableColumn<PatientMst, String> clHospitalId;

	@FXML
	private TableColumn<PatientMst, String> clContactPerson;

	@FXML
	private TableColumn<PatientMst, String> clContactNo;

//	@FXML
//	private ComboBox<String> cmbPatientType;

	@FXML
	private TableColumn<PatientMst, String> clInsuraceCompany;
	
    @FXML
    private TableColumn<PatientMst, String> clmnCustomerName;

	
	
	@FXML
    void onClickCustomerPopUp(MouseEvent event) {
		loadCustomerPopup();
    }
	
	

    private void loadCustomerPopup() {


		if (patientMst != null) {

			Alert a = new Alert(AlertType.CONFIRMATION);
			a.setHeaderText("Changing Customer...");
			a.setContentText("The details will be deleted");
			a.showAndWait().ifPresent((btnType) -> {
				if (btnType == ButtonType.OK) {

					//deleteAllSalesDtl();

					try {
						txtCorporateCust.clear();
						FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
						Parent root1;

						root1 = (Parent) fxmlLoader.load();
						Stage stage = new Stage();

						stage.initModality(Modality.APPLICATION_MODAL);
						stage.initStyle(StageStyle.UNDECORATED);
						stage.setTitle("ABC");
						stage.initModality(Modality.APPLICATION_MODAL);
						stage.setScene(new Scene(root1));
						stage.show();

						//txtItemname.requestFocus();

					} catch (IOException e) {
						//
						e.printStackTrace();
					}

				} else if (btnType == ButtonType.CANCEL) {

					return;

				}
			});
//			Boolean confirm = confirmMessage();

		} else {
			try {

				txtCorporateCust.clear();
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();


			} catch (IOException e) {
				//
				e.printStackTrace();
			}
		}
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */

	
		
	}



	@FXML
    void onEnterCustomerPopUp(KeyEvent event) {
		loadCustomerPopup();
    }
	
	@FXML
	private void initialize() {
		dpDob = SystemSetting.datePickerFormat(dpDob, "dd/MMM/yyyy");
		dpInsCardExpiry = SystemSetting.datePickerFormat(dpInsCardExpiry, "dd/MMM/yyyy");
		eventBus.register(this);
		ResponseEntity<List<InsuranceCompanyDtl>> savedInsuranceCompany = RestCaller.getAllInsuranceCompanyDtl();
		insuranceCompanyList = FXCollections.observableArrayList(savedInsuranceCompany.getBody());
		for (InsuranceCompanyDtl insuranceCompanyMst : insuranceCompanyList) {
			cmbPolicyType.getItems().add(insuranceCompanyMst.getPolicyType());
		}
		
		
		
		setCmbInsuranceCompany();

		//setcmbPatienttype();

		txtPatientName.textProperty().bindBidirectional(SearchString);
		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				System.out.println("Supplier Search changed");
				loadPatientsearch((String) newValue);
			}
		});
//    	ResponseEntity<List<InsuranceCompanyMst>> insuranceComp = RestCaller.getAllInsuranceCompanyMst();
//    	for(int i =0;i<insuranceComp.getBody().size();i++)
//    	{
//    		cmbInsuranceCompany.getItems().add(insuranceComp.getBody().get(i).getInsuranceCompanyName());
//    	}
		cmbGender.getItems().add("MALE");
		cmbGender.getItems().add("FEMALE");
		cmbNationality.getItems().add("MALDIVES");
		cmbNationality.getItems().add("INDIA");
		cmbInsuranceCompany.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					if (!newValue.equalsIgnoreCase(null)) {
						ResponseEntity<List<InsuranceCompanyMst>> insurance = RestCaller.getInsuranceCompanyByName(newValue);
						
						
						List<InsuranceCompanyMst> insuranceCompanyMst =  insurance.getBody();
						
						if(insuranceCompanyMst.size() > 0)
						{
							
							cmbPolicyType.getItems().clear();
							ResponseEntity<List<InsuranceCompanyDtl>> insuranceDtl = RestCaller.
									getInsuranceCompanyDtlByInsuranceCompanyMstId(insuranceCompanyMst.get(0).getId());
							
							List<InsuranceCompanyDtl> insurancePolicyList = insuranceDtl.getBody();
							
							for(InsuranceCompanyDtl dtls : insurancePolicyList)
							{
								cmbPolicyType.getItems().add(dtls.getPolicyType());
							}
						}
						
						//txtPercentageCoverage.setText(insurance.getBody().getPercentageCoverage());
					}

				}
			}
		});

		tblPatient.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					patientMst = new PatientMst();
					patientMst.setId(newSelection.getId());
					txtAddress1.setText(newSelection.getAddress1());
					txtAddress2.setText(newSelection.getAddress2());
					txtContactNo.setText(newSelection.getPhoneNumber());
					txtContactPerson.setText(newSelection.getContactPerson());
					txtHospitalId.setText(newSelection.getHospitalId());
					txtInsuranceCard.setText(newSelection.getInsuranceCard());
					txtNationalId.setText(newSelection.getNationalId());
					txtPatientName.setText(newSelection.getPatientName());
					txtPercentageCoverage.setText(newSelection.getPercentageCoverage());
					txtCorporateCust.setText(newSelection.getCustomerName());
					
					if (null != newSelection.getPolicyType()) {
						ResponseEntity<InsuranceCompanyDtl> inst = RestCaller
								.getinsuraCompanyDtlById(newSelection.getPolicyType());

						cmbPolicyType.getSelectionModel().select(inst.getBody().getPolicyType());
					}
					
				
					
					cmbGender.getSelectionModel().select(newSelection.getGender());
					if (null != newSelection.getInsuranceCompanyId()) {
						ResponseEntity<InsuranceCompanyMst> inst = RestCaller
								.getinsuraCompanyMstById(newSelection.getInsuranceCompanyId());

						cmbInsuranceCompany.getSelectionModel().select(inst.getBody().getInsuranceCompanyName());
					}

					
					// ================= no need of patient type==================
//					if (null != newSelection.getPatientType()) {
//						ResponseEntity<PharmacyPatientType> pharmacyPatientType = RestCaller
//								.getPatientTypeById(newSelection.getPatientType());
//
//						cmbPatientType.getSelectionModel().select(pharmacyPatientType.getBody().getPatientType());
//					}
					cmbNationality.getSelectionModel().select(newSelection.getNationality());

					dpDob.setValue(SystemSetting.utilToLocaDate(newSelection.getDateOfBirth()));
					dpInsCardExpiry.setValue(SystemSetting.utilToLocaDate(newSelection.getInsuranceCardExpiry()));
				}
			}
		});
		
		
		cmbPolicyType.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					ResponseEntity<InsuranceCompanyDtl> insurance = RestCaller
							.getInsuranceCompDtlByPolicyType(newValue);
					
					InsuranceCompanyDtl insuranceCompanyDtl = insurance.getBody();
					
					
						if(null != insuranceCompanyDtl)
						{
							txtPercentageCoverage.setText(insuranceCompanyDtl.getPercentageCoverage());
							
						}
					
				    }}
			});
		

		
		
		
		
	}
	

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		ResponseEntity<AccountHeads> getCustomer = RestCaller.getAccountHeadsById(customerEvent.getCustId());
		
		accountHeads = getCustomer.getBody();
		
		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			String customerSite = SystemSetting.customer_site_selection;
			if (customerSite.equalsIgnoreCase("TRUE")) {
				txtCorporateCust.setDisable(false);
				txtCorporateCust.clear();
			}

			String sdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
			txtCorporateCust.setText(customerEvent.getCustomerName());
			if(null != txtCorporateCust.getText()) {
				cmbInsuranceCompany.setDisable(true);
				cmbPolicyType.setDisable(true);
				txtInsuranceCard.setDisable(true);
				txtPercentageCoverage.setDisable(true);
				dpInsCardExpiry.setDisable(true);
			}
			
			
		}

	}

	@FXML
	void actionClear(ActionEvent event) {

		clearFileds();
		tblPatient.getItems().clear();
		patientMstList.clear();
		patientMst = null;
		txtPatientName.requestFocus();
		cmbInsuranceCompany.setDisable(false);
		cmbPolicyType.setDisable(false);
		txtInsuranceCard.setDisable(false);
		txtPercentageCoverage.setDisable(false);
		dpInsCardExpiry.setDisable(false);
		txtCorporateCust.clear();
		accountHeads=null;
	}

	private void loadPatientsearch(String searchData) {
		tblPatient.getItems().clear();
		patientMstList.clear();

		ResponseEntity<List<PatientMst>> patient = RestCaller.searchPatientByName(searchData);
		patientMstList = FXCollections.observableArrayList(patient.getBody());
		fillTable();
	}

	@FXML
	void actionDelete(ActionEvent event) {

		if (null == patientMst)

		{
			return;
		}
		if (null == patientMst.getId()) {
			return;
		}
		RestCaller.deletePatientMst(patientMst.getId());
		showAll();
		clearFileds();
		patientMst = null;
	}

	@FXML
	void actionClose(ActionEvent event) {
		Stage stage = (Stage) btnClose.getScene().getWindow();
		stage.close();

	}

	private void clearFileds() {
		txtAddress1.clear();
		txtAddress2.clear();
		txtContactNo.clear();
		txtContactPerson.clear();
		txtHospitalId.clear();
		txtInsuranceCard.clear();
		txtNationalId.clear();
		txtPatientName.clear();
		txtCorporateCust.clear();
		txtPercentageCoverage.clear();
		cmbPolicyType.getSelectionModel().clearSelection();
		cmbGender.getSelectionModel().clearSelection();
		cmbInsuranceCompany.getSelectionModel().clearSelection();
		cmbNationality.getSelectionModel().clearSelection();
		//cmbPatientType.getSelectionModel().clearSelection();
		dpDob.setValue(null);
		dpInsCardExpiry.setValue(null);

	}

	private void showAll() {
		ResponseEntity<List<PatientMst>> getAllPatient = RestCaller.getAllPatientMsts();
		patientMstList = FXCollections.observableArrayList(getAllPatient.getBody());
		fillTable();
	}

	@FXML
	void actionSave(ActionEvent event) {
		additem();
	}

	private void additem() {

		if (null != patientMst) {
			if (null != patientMst.getId()) {

				patientMst.setAddress1(txtAddress1.getText());
				patientMst.setAddress2(txtAddress2.getText());
				patientMst.setContactPerson(txtContactPerson.getText());
				if (null != dpDob.getValue()) {
					patientMst.setDateOfBirth(SystemSetting.localToUtilDate(dpDob.getValue()));
				}
				patientMst.setGender(cmbGender.getSelectionModel().getSelectedItem());
				patientMst.setHospitalId(txtHospitalId.getText());
				patientMst.setInsuranceCard(txtInsuranceCard.getText());
				if (null != dpInsCardExpiry.getValue()) {
					patientMst.setInsuranceCardExpiry(SystemSetting.localToUtilDate(dpInsCardExpiry.getValue()));
				}
				if (null != cmbInsuranceCompany.getSelectionModel().getSelectedItem()) {
					ResponseEntity<List<InsuranceCompanyMst>> insuranceComp = RestCaller
							.getInsuranceCompanyByName(cmbInsuranceCompany.getSelectionModel().getSelectedItem());
					
					List<InsuranceCompanyMst> insuranceCompanyMst = insuranceComp.getBody();
					
					if (insuranceCompanyMst.size() > 0) {
						patientMst.setInsuranceCompanyId(insuranceComp.getBody().get(0).getId());

					}
					
					
				}
				
				if (null != cmbPolicyType.getSelectionModel().getSelectedItem()) {
					ResponseEntity<InsuranceCompanyDtl> insurancePolicyComp = RestCaller
							.getInsuranceCompDtlByPolicyType(cmbPolicyType.getSelectionModel().getSelectedItem());
					InsuranceCompanyDtl insuranceCompanyDtl = insurancePolicyComp.getBody();
					if (null == insuranceCompanyDtl) {
						patientMst.setPolicyType(insurancePolicyComp.getBody().getId());

					}
				}
				
				
				
				
				
				
				
				
				patientMst.setNationalId(txtNationalId.getText());
				patientMst.setPolicyType(cmbPolicyType.getSelectionModel().getSelectedItem());

				patientMst.setPhoneNumber(txtContactNo.getText());
				patientMst.setPercentageCoverage(txtPercentageCoverage.getText());
				patientMst.setPatientName(txtPatientName.getText());
				patientMst.setNationality(cmbNationality.getSelectionModel().getSelectedItem());
				
				patientMst.setAccountHeads(accountHeads);
					//=======no need of patient type============== 
//				ResponseEntity<List<PharmacyPatientType>> pharmacyPatientType = RestCaller
//						.getPharmacyPatientTypeidByPatientType(
//								cmbPatientType.getSelectionModel().getSelectedItem());
//
//				List<PharmacyPatientType> PharmacyPatientType = pharmacyPatientType.getBody();
//
//				if (PharmacyPatientType.size() > 0) {
//					patientMst.setPatientType(pharmacyPatientType.getBody().get(0).getId());
//
//				}
//
				RestCaller.updatePatientMstById(patientMst);
			}
		} else {
			if (txtPatientName.getText().trim().isEmpty()) {
				notifyMessage(3, "Type Patient Name");
				txtPatientName.requestFocus();
				return;
			}
			if (txtAddress1.getText().trim().isEmpty()) {
				notifyMessage(3, "Type Address");
				txtAddress1.requestFocus();
				return;
			}
			if (!txtPatientName.getText().contains(txtContactNo.getText()))

			{
				notifyMessage(3, "Please add Patient phone number to the end of Patient name");
				txtPatientName.requestFocus();
				return;
			}
			patientMst = new PatientMst();
			patientMst.setAddress1(txtAddress1.getText());
			patientMst.setAddress2(txtAddress2.getText());
			patientMst.setContactPerson(txtContactPerson.getText());
			if (null != dpDob.getValue()) {
				patientMst.setDateOfBirth(SystemSetting.localToUtilDate(dpDob.getValue()));
			}
			patientMst.setGender(cmbGender.getSelectionModel().getSelectedItem());
			patientMst.setHospitalId(txtHospitalId.getText());
			patientMst.setInsuranceCard(txtInsuranceCard.getText());
			if (null != dpInsCardExpiry.getValue()) {
				patientMst.setInsuranceCardExpiry(SystemSetting.localToUtilDate(dpInsCardExpiry.getValue()));
			}
			if (null != cmbInsuranceCompany.getSelectionModel().getSelectedItem()) {
				ResponseEntity<List<InsuranceCompanyMst>> insuranceComp = RestCaller
						.getInsuranceCompanyByName(cmbInsuranceCompany.getSelectionModel().getSelectedItem());
				List<InsuranceCompanyMst> insuranceCompanyMst = insuranceComp.getBody();
				
				if (insuranceCompanyMst.size() > 0) {
					patientMst.setInsuranceCompanyId(insuranceComp.getBody().get(0).getId());

				}
					
			}
			String vno = RestCaller.getVoucherNumber("PNT");
			patientMst.setId(vno);
			patientMst.setNationalId(txtNationalId.getText());
			patientMst.setPolicyType(cmbPolicyType.getSelectionModel().getSelectedItem());
			patientMst.setPhoneNumber(txtContactNo.getText());
			patientMst.setPercentageCoverage(txtPercentageCoverage.getText());
			patientMst.setPatientName(txtPatientName.getText());
			patientMst.setNationality(cmbNationality.getSelectionModel().getSelectedItem());
			if(null != accountHeads) {
				
				patientMst.setAccountHeads(accountHeads);
				
				
			}
			
			// ====================no need of patient type====================
//			if (null != cmbPatientType.getSelectionModel().getSelectedItem()) {
//				ResponseEntity<List<PharmacyPatientType>> pharmacyPatientType = RestCaller
//						.getPharmacyPatientTypeidByPatientType(
//								cmbPatientType.getSelectionModel().getSelectedItem());
//
//				List<PharmacyPatientType> PharmacyPatientType = pharmacyPatientType.getBody();
//
//				if (PharmacyPatientType.size() > 0) {
//					patientMst.setPatientType(pharmacyPatientType.getBody().get(0).getId());
//
//				}
//			}

			ResponseEntity<PatientMst> getPatientByName = RestCaller.getPatientMstByName(patientMst.getPatientName());
			ResponseEntity<AccountHeads> getCustomerByName = RestCaller.getAccountHeadsByName(patientMst.getPatientName());

			if (null != getPatientByName.getBody()) {
				notifyMessage(3, "Patient Name Already Saved");
				patientMst = null;
				return;
			} else if (null != getCustomerByName.getBody()) {
				notifyMessage(3, "Customer Name Already Saved");
				patientMst = null;
				return;
			} else {
				ResponseEntity<PatientMst> respentity = RestCaller.savePatientMst(patientMst);
				patientMst = respentity.getBody();
				patientMstList.add(patientMst);
				fillTable();
			}
		}
		clearFileds();
		patientMst = null;
		accountHeads=null;
		cmbInsuranceCompany.setDisable(false);
		cmbPolicyType.setDisable(false);
		txtInsuranceCard.setDisable(false);
		txtPercentageCoverage.setDisable(false);
		dpInsCardExpiry.setDisable(false);

	}

	private void fillTable() {
		for (PatientMst patientmst : patientMstList) {
			if (null != patientmst.getInsuranceCompanyId()) {
				ResponseEntity<InsuranceCompanyMst> insurance = RestCaller
						.getinsuraCompanyMstById(patientmst.getInsuranceCompanyId());
				patientmst.setInsuranceCompanyName(insurance.getBody().getInsuranceCompanyName());
			}
		}
		tblPatient.setItems(patientMstList);
		clAddress.setCellValueFactory(cellData -> cellData.getValue().getaddressProperty());
		clContactNo.setCellValueFactory(cellData -> cellData.getValue().getcontactNoProperty());
		clContactPerson.setCellValueFactory(cellData -> cellData.getValue().getcontactPersonProperty());
		clHospitalId.setCellValueFactory(cellData -> cellData.getValue().gethospitalIdProperty());
		clInsuraceCompany.setCellValueFactory(cellData -> cellData.getValue().getinsuranceCompanyProperty());
		clPatientName.setCellValueFactory(cellData -> cellData.getValue().getpatientNameProperty());
		clmnCustomerName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
	}

	@FXML
	void actionShowAll(ActionEvent event) {
		showAll();
	}

	@FXML
	void address2OnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtHospitalId.requestFocus();
		}
	}

	@FXML
	void addressOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAddress2.requestFocus();
		}
	}

	@FXML
	void cmbInsuranceCompOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtInsuranceCard.requestFocus();
		}
	}

	@FXML
	void contactNoOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpDob.requestFocus();
		}
	}

	@FXML
	void contactPersonOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtContactNo.requestFocus();
		}
	}

	@FXML
	void dpDobOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtNationalId.requestFocus();
		}
	}

	@FXML
	void genderOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbNationality.requestFocus();
		}
	}

	@FXML
	void hospitalIdOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtContactPerson.requestFocus();
		}
	}

	@FXML
	void insCardExpOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}
	}

	@FXML
	void insuranceCardOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbGender.requestFocus();
		}
	}

	@FXML
	void nationalIdOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbInsuranceCompany.requestFocus();
		}
	}

	@FXML
	void nationalityOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtInsuranceCard.requestFocus();
		}
	}

	@FXML
	void cmbPatientTypeOnEnter(KeyEvent event) {

	}

	@FXML
	void patientNameOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtAddress1.requestFocus();
		}
	}

	@FXML
	void saveOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			additem();
		}
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

	private void setCmbInsuranceCompany() {

		cmbInsuranceCompany.getItems().clear();

		ResponseEntity<List<InsuranceCompanyMst>> insuranceCompanyMsttResp = RestCaller.getAllInsuranceCompanyMst();
		List<InsuranceCompanyMst> insuranceCompanyMstList = insuranceCompanyMsttResp.getBody();

		for (InsuranceCompanyMst insuranceCompanyMst : insuranceCompanyMstList) {
			cmbInsuranceCompany.getItems().add(insuranceCompanyMst.getInsuranceCompanyName());
		}

	}

	
	// ===========no need of patient type================
//	private void setcmbPatienttype() {
//
//		cmbPatientType.getItems().clear();
//
//		ResponseEntity<List<PharmacyPatientType>> pharmacyPatientTypeMstResp = RestCaller.getallpatienttype();
//		List<PharmacyPatientType> pharmacyPatientTypeMstList = pharmacyPatientTypeMstResp.getBody();
//
//		for (PharmacyPatientType pharmacyPatientType : pharmacyPatientTypeMstList) {
//			cmbPatientType.getItems().add(pharmacyPatientType.getPatientType());
//		}
//	}

}
