package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.LocalPurchaseSummaryReport;
import com.maple.report.entity.PharmacyLocalPurchaseDetailsReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class PharmacyLocalPurchaseDetailsReportCtl {
	private ObservableList<PharmacyLocalPurchaseDetailsReport> pharmacyLocalPurchaseDetailsReportList = FXCollections.observableArrayList();
	
	   @FXML
	    private DatePicker dpfromDate;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private Button btnShow;

	    @FXML
	    private Button btnGenerateReport;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private TableView<PharmacyLocalPurchaseDetailsReport> tblReport;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> cltransEntryDate;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clinvoiceDate;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clvoucherNumber;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clsupplierInvoice;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clsupplier;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> cldescription;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clitemCode;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clgroup;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clbatch;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clexpiryDate;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, String> clpoNumber;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, Number> clQty;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, Number> clpurchaseRate;

	    @FXML
	    private TableColumn<PharmacyLocalPurchaseDetailsReport, Number> clgst;

	    @FXML
	    void Clear(ActionEvent event) {
	    	
	    	dpfromDate.setValue(null);
	    	dpToDate.setValue(null);
	    	tblReport.getItems().clear();

	    }
	    

	    @FXML
	    void Report(ActionEvent event) {
	    	
	    	if(null==dpfromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    		
	    	}
	    	if (null==dpToDate.getValue()) {
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    		
	    	}
	    	
	    	Date fdate = SystemSetting.localToUtilDate(dpfromDate.getValue());
			String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

			Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String edate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
			
			try {
				JasperPdfReportService.PharmacyLocalPurchaseDetailsReports(sdate, edate);
			} catch (Exception e) {
				e.printStackTrace();
			}
			

	   }

	    @FXML
	    void Show(ActionEvent event) {
	    	Date fdate = SystemSetting.localToUtilDate(dpfromDate.getValue());
			String fromdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

			Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String todate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
	    	
	    	ResponseEntity<List<PharmacyLocalPurchaseDetailsReport>> pharmacyLocalPurchaseDetailsReport=RestCaller.getPharmacyLocalPurchaseDetailsReport(fromdate, todate);
	    	pharmacyLocalPurchaseDetailsReportList = FXCollections.observableArrayList(pharmacyLocalPurchaseDetailsReport.getBody());
	    	fillTable();


	    }
	    
		private void fillTable() {
			tblReport.setItems(pharmacyLocalPurchaseDetailsReportList);
	    
			cltransEntryDate.setCellValueFactory(cellData -> cellData.getValue().getTransEntryDateProperty());
			clinvoiceDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
			clvoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
			clsupplierInvoice.setCellValueFactory(cellData -> cellData.getValue().getSupplierInvoiceProperty());
			clsupplier.setCellValueFactory(cellData -> cellData.getValue().getSupplierProperty());
			cldescription.setCellValueFactory(cellData -> cellData.getValue().getDescriptionProperty());
			clitemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
			clgroup.setCellValueFactory(cellData -> cellData.getValue().getGroupProperty());
			clbatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
			clexpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
			clpoNumber.setCellValueFactory(cellData -> cellData.getValue().getPoNumberProperty());
			clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
			clpurchaseRate.setCellValueFactory(cellData -> cellData.getValue().getPurchaseRateProperty());
			clgst.setCellValueFactory(cellData -> cellData.getValue().getGstProperty());
		}


		public void notifyMessage(int duration, String msg, boolean success) {

 			Image img;
 			if (success) {
 				img = new Image("done.png");

 			} else {
 				img = new Image("failed.png");
 			}

 			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
 					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
 					.onAction(new EventHandler<ActionEvent>() {
 						@Override
 						public void handle(ActionEvent event) {
 							System.out.println("clicked on notification");
 						}
 					});
 			notificationBuilder.darkStyle();
 			notificationBuilder.show();

 	}
	

}
