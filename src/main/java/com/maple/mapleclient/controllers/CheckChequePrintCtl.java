package com.maple.mapleclient.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.google.common.eventbus.Subscribe;
import com.maple.javapos.print.ChequePrint;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class CheckChequePrintCtl {
	String taskid;
	String processInstanceId;


    @FXML
    private Button btnCheckPrint;

    @FXML
    void actionCheckPrint(ActionEvent event) {
    	ChequePrint chequePrint = new ChequePrint();
    	String accountName = "************************";
    	ArrayList bankList = new ArrayList();
    	String bankname = null;
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid= lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				bankname = ((String) bankName);
				
						    //accountHeads.getAccountName);
			}
		}

    	String amount = 1080929+"";
    	String amountinwords = "Ten Lakh eighty thousand 9 hundred and twenty nine";
    	String vDate = "2020-02-29";
    	try {
			chequePrint.PrintInvoiceThermalPrinter(bankname,accountName, amount, amountinwords, vDate);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	     private void PageReload() {
	     	
	}

}
