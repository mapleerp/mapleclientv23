package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class MultiCategoryUpdationCtl {
	
	String taskid;
	String processInstanceId;

	StringProperty SearchString = new SimpleStringProperty();
	private ObservableList<ItemMst> itemMstList = FXCollections.observableArrayList();
	private ObservableList<ItemMst> popUpItemList = FXCollections.observableArrayList();
//	ObservableList<TablePosition> selectedCells = FXCollections.observableArrayList();

	List<String> itemList = new ArrayList<String>();

	@FXML
	private TextField txtItemName;

	@FXML
	private TableView<ItemMst> tblItem;

	@FXML
	private TableColumn<ItemMst, String> clItem;

	@FXML
	private TableColumn<ItemMst, String> clItemCategory;

	@FXML
	private ChoiceBox<String> cmbCategory;

	@FXML
	private Button btnUpdate;

	@FXML
	private TableView<ItemMst> tblItemReport;

	@FXML
	private TableColumn<ItemMst, String> clItemName;

	@FXML
	private TableColumn<ItemMst, String> clCategory;

	@FXML
	private Button btlClear;

	@FXML
	private void initialize() {

		tblItem.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		tblItem.setItems(popUpItemList);
		clItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clItemCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());

//		tblItem.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//			if (newSelection != null) {
//				if (null != newSelection.getId()) {
//
////					if (itemList.size() == 0) {
////						itemList.add(newSelection.getId());
////					} else {
////						for (int i = 0; i < itemList.size(); i++) {
////							if (itemList.get(i).equalsIgnoreCase(newSelection.getId())) {
////								itemList.remove(i);
////							} else {
////								itemList.add(newSelection.getId());
////							}
////						}
////					}
//
//				
//				}
//			}
//		});

//		
//		tblItem.setOnMousePressed(new EventHandler<MouseEvent>() {
//			@Override
//			public void handle(MouseEvent event) {
//			
//				selectedCells.add(tblItem.getSelectionModel().getSelectedCells()
//						.get(tblItem.getSelectionModel().getSelectedCells().size() - 1));
//				for (TablePosition tp : selectedCells) {
//					tblItem.getSelectionModel().select(tp.getRow(), tp.getTableColumn());
//				}
//			}
//		});
		txtItemName.textProperty().bindBidirectional(SearchString);

//		LoadItemPopupBySearch("");

		ArrayList cat = new ArrayList();

		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				cmbCategory.getItems().add((String) categoryName);

			}

		}

		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadItemPopupBySearch((String) newValue);
			}
		});

	}

	@FXML
	void CategoryUpdation(ActionEvent event) {

		if (null == tblItem.getSelectionModel().getSelectedItems()) {
			notifyMessage(3, "Please select items", false);
			return;
		}

		if (null == cmbCategory.getValue()) {
			notifyMessage(3, "Please category", false);
			return;
		}
		
		itemList = SelectedItemId();

		System.out.println(itemList);
		ResponseEntity<CategoryMst> categoryMstResp = RestCaller
				.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem().toString());
		CategoryMst categoryMst = categoryMstResp.getBody();

		for (String itemId : itemList) {

			ResponseEntity<ItemMst> itemMstResp = RestCaller.getItemByNameRequestParam(itemId);
			ItemMst itemMst = itemMstResp.getBody();

			itemMst.setCategoryId(categoryMst.getId());
			RestCaller.updateItemMst(itemMst);

		}

		itemMstList.clear();
		for (String itemId : itemList) {

			ResponseEntity<ItemMst> itemMstResp = RestCaller.getItemByNameRequestParam(itemId);
			ItemMst itemMst = itemMstResp.getBody();

			itemMstList.add(itemMst);

		}

		FillTable();

		tblItem.getItems().clear();
		cmbCategory.getSelectionModel().clearSelection();

	}

	private List<String> SelectedItemId() {
		//I commented the below lines then no issues.........
		//popUpItemList.clear();
		List<String> itemIds = new ArrayList<String>();
		
    	int selesctedItemsNo = tblItem.getSelectionModel().getSelectedItems().size();



			for(int i=0;i<selesctedItemsNo;i++)
			{

				String itemId = tblItem.getSelectionModel().getSelectedItems().get(i).getItemName();
				itemIds.add(itemId);
			}
		return itemIds;
	}

	private void FillTable() {

		tblItemReport.setItems(itemMstList);

		for (ItemMst itemMst : itemMstList) {
			if (null != itemMst.getCategoryId()) {
				ResponseEntity<CategoryMst> categoryResp = RestCaller.getCategoryById(itemMst.getCategoryId());
				itemMst.setCategoryName(categoryResp.getBody().getCategoryName());
			}
		}

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clCategory.setCellValueFactory(cellData -> cellData.getValue().getCategoryNameProperty());
	}

	public void LoadItemPopupBySearch(String searchData) {

		tblItem.getItems().clear();
		popUpItemList.clear();

		ArrayList items = new ArrayList();
		items = RestCaller.SearchItemLikeName(searchData);

		String itemName = "";
		String categoryName = "";
		String itemId = "";
		Iterator itr = items.iterator();

		int i = 0;

		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			categoryName = (String) element.get(2);
			itemId = (String) element.get(1);

			if (null != itemId) {
				ItemMst itemMstNew = new ItemMst();
				itemMstNew.setId(itemId);
				itemMstNew.setItemName(itemName);
				itemMstNew.setCategoryName(categoryName);

				popUpItemList.add(itemMstNew);
			}

		}

		System.out.println(popUpItemList);

	}

	@FXML
	void Clear(ActionEvent event) {

		cmbCategory.getSelectionModel().clearSelection();
		tblItemReport.getItems().clear();
		txtItemName.clear();

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
