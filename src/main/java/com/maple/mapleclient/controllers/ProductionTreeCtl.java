package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;

public class ProductionTreeCtl {
	
	String taskid;
	String processInstanceId;
	
	
	String selectedText = null;
    @FXML
    private TreeView<String> treeview;
    
    @FXML
    void initialize() {
    	
    	TreeItem rootItem = new TreeItem("PRODUCTION");
		//TreeItem masterItem = new TreeItem("MASTERS");
		
	HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
		
		
		
		ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("PRODUCTION");
		
		List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
		
		MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
		
		String parentId =aMenuConfigMst.getId();
		
		ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
		List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

		
		for(MenuConfigMst aMenuConfig : menuConfigMstList)
		{
			
			//masterItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
			
			if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
				rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
				}
			
			
		}
		
		//rootItem.getChildren().add(masterItem);

		treeview.setRoot(rootItem);
		
		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text 12345 : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();
				
				
				ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller.MenuConfigMstByMenuName(selectedText);
				
				List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();
		
				MenuConfigMst aMenuConfig = menuConfigMstList.get(0);
				
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != selectedText)
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(selectedText, dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
			
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
				  
				
			/*	if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.kotManagement);

				}
				
				*/
				
				

			}

		});
				 
 
//    	TreeItem rootItem = new TreeItem("Production");
//    	
//
//    	TreeItem productionPrePlanning = new TreeItem("PRODUCTION PRE PLANNING");
//    	if (SystemSetting.UserHasRole("PRODUCTION PRE PLANNING")) {
//    		rootItem.getChildren().add(productionPrePlanning);
//    	}
//    	
//    	
//    	TreeItem productionPlanning = new TreeItem("PRODUCTION PLANNING");
//    	if (SystemSetting.UserHasRole("PRODUCTION PLANNING")) {
//    		rootItem.getChildren().add(productionPlanning);
//    	}
//    	
//    	TreeItem actualproduction = new TreeItem("ACTUAL PRODUCTION");
//    	if (SystemSetting.UserHasRole("ACTUAL PRODUCTION")) {
//    		rootItem.getChildren().add(actualproduction);
//    	}
//    	
//    	TreeItem rawMatIssue = new TreeItem("RAW MATERIAL ISSUE");
//    	if (SystemSetting.UserHasRole("PRODUCTION PLANNING")) {
//    		rootItem.getChildren().add(rawMatIssue);
//    	}
//    	
//    	TreeItem rawMatReturn = new TreeItem("RAW MATERIAL RETURN");
//    	if (SystemSetting.UserHasRole("PRODUCTION PLANNING")) {
//    		rootItem.getChildren().add(rawMatIssue);
//    	}
//    	treeview.setRoot(rootItem);
//    	treeview.getSelectionModel().selectedItemProperty().addListener( new ChangeListener() {
// 		   @Override
//            public void changed(ObservableValue observable, Object oldValue,
//                    Object newValue) {
//
//                TreeItem<String> selectedItem = (TreeItem<String>) newValue;
//                System.out.println("Selected Text : " + selectedItem.getValue());
//                selectedText = selectedItem.getValue();
// 	
// if(selectedItem.getValue().equalsIgnoreCase( "PRODUCTION PLANNING")) {
// 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
// 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
// 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.Production);
// 
// 	}
// if(selectedItem.getValue().equalsIgnoreCase( "ACTUAL PRODUCTION")) {
//	 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//	 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//	 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ActualProduction);
//	 
//	 	}
// 
// if(selectedItem.getValue().equalsIgnoreCase( "RAW MATERIAL ISSUE")) {
//	 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//	 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//	 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.rawMaterialIssue);
//	 
//	 	}
// 
// if(selectedItem.getValue().equalsIgnoreCase( "RAW MATERIAL RETURN")) {
//	 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//	 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//	 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.RawMaterialReturn);
//	 
//	 	}
//		   
// 		   }
// 		    
//  	  });
    }
    	
    @FXML
    void prodcutOnClick(MouseEvent event) {
    	
    	if (null != selectedText) {
			//1. Fetch command and fx from table based on the clicked item text.
			//2. load the fx similar way we did in the main frame
			
		 
			HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
			
			
			
			ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName(selectedText);
			
			List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
			
			MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
			
			
			ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(aMenuConfigMst.getId());
			List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();
	
			
			for(MenuConfigMst aMenuConfig : menuConfigMstList)
			{
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != aMenuConfig.getMenuFxml())
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(aMenuConfig.getMenuName(), dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
	 
				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						/*MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
						 
						*/
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
			}
//    	if(null != selectedText)
//    	{
//    		if(selectedText.equalsIgnoreCase( "RAW MATERIAL ISSUE")) {
//    		 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.rawMaterialIssue);
//    		 
//    		 	}
//    		if(selectedText.equalsIgnoreCase( "RAW MATERIAL RETURN")) {
//    		 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.RawMaterialReturn);
//    		 
//    		 	}
//    		 if(selectedText.equalsIgnoreCase( "PRODUCTION PRE PLANNING")) {
// 			 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
// 			 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
// 			 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ProductionPrePlaning);
// 			 
// 			 	}
//    		 if(selectedText.equalsIgnoreCase( "PRODUCTION PLANNING")) {
//    			 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    			 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    			 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.Production);
//    			 
//    			 	}
//    			 if(selectedText.equalsIgnoreCase( "ACTUAL PRODUCTION")) {
//    				 	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    				 		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    				 	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.ActualProduction);
//    				 
//    				 	}
//    	 
//    }
    }
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
