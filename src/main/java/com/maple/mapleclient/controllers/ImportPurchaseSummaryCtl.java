package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ImportPurchaseSummary;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class ImportPurchaseSummaryCtl {
	
	
	private ObservableList<ImportPurchaseSummary> ImportPurchaseSummaryList = FXCollections.observableArrayList();

	
	  @FXML
	    private DatePicker dpFromDate;

	    @FXML
	    private DatePicker dpToDate;

	    @FXML
	    private Button btnShow;

	    @FXML
	    private Button btnPrint;

	    @FXML
	    private Button btnClear;

	    @FXML
	    private TableView<ImportPurchaseSummary> tblItemDetails;

	   

	    @FXML
	    private TableColumn<ImportPurchaseSummary, String> clVoucherNum;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, String> clSuppName;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, String> clSuppInvo;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, Number> clItemTotal;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, Number> ClItemTotalfc;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, Number> clExpense;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, Number> clExpenseFc;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, Number> clNotIncExp;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, Number> clGrandTotal;

	    @FXML
	    private TableColumn<ImportPurchaseSummary, Number> clImportExpense;
	    @FXML
		private void initialize() {
	    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
	    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
	 }
	    @FXML
	    void actionClear(ActionEvent event) {
	    	
	    	dpFromDate.setValue(null);
	    	dpToDate.setValue(null);
	    	tblItemDetails.getItems().clear();
	    	
	    	

	    }

	    @FXML
	    void actionPrint(ActionEvent event) {
	    	
	    	if(null==dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    		
	    	}
	    	if (null==dpToDate.getValue()) {
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    		
	    	}
	    	
	    	Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
			String sdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");

			Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String edate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
			
			try {
				JasperPdfReportService.PharmacyImportPurchaseSummaryReport(sdate, edate);
			} catch (Exception e) {
				// TODO: handle exception
			}
			

	    }

	    @FXML
	    void actionShow(ActionEvent event) {
	    	
	    	if(null==dpFromDate.getValue())
	    	{
	    		notifyMessage(5, "Please select from date", false);
	    		return;
	    		
	    	}
	    	if (null==dpToDate.getValue()) {
	    		notifyMessage(5, "Please select to date", false);
	    		return;
	    		
	    	}
	    	
	    	java.util.Date usdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
	    	String ssdate = SystemSetting.UtilDateToString(usdate , "yyyy-MM-dd");
	    	java.util.Date uedate = SystemSetting.localToUtilDate(dpToDate.getValue());
	    	String sedate = SystemSetting.UtilDateToString(uedate, "yyyy-MM-dd");
	    	
	    	
	    	ResponseEntity<List<ImportPurchaseSummary>> importPurchaseSummaryReport =RestCaller.getImportPurchaseSummary(ssdate,sedate);
	    	ImportPurchaseSummaryList = FXCollections.observableArrayList(importPurchaseSummaryReport.getBody());
	    	
	    	
	    	
	    	
	    	FillTable();
	    	}


	    
	    @FXML
	    void FDateKeyPressed(KeyEvent event) {

	    }

	    @FXML
	    void TDateKeyPressed(KeyEvent event) {

	    }
	    
	    private void FillTable() {
	    	
	    	tblItemDetails.setItems(ImportPurchaseSummaryList);

	    	clVoucherNum.setCellValueFactory(cellData -> cellData.getValue().getInvoiceNumberProperty());
	    	clSuppName.setCellValueFactory(cellData -> cellData.getValue().getSupplierInvNoProperty());
			clSuppInvo.setCellValueFactory(cellData -> cellData.getValue().getSupplierInvNoProperty());
			clItemTotal.setCellValueFactory(cellData -> cellData.getValue().getItemTotalProperty());
			ClItemTotalfc.setCellValueFactory(cellData -> cellData.getValue().getItemTotalFcProperty());
			clExpense.setCellValueFactory(cellData -> cellData.getValue().getExpenseProperty());
			clExpenseFc.setCellValueFactory(cellData -> cellData.getValue().getFcExpenseProperty());
			clNotIncExp.setCellValueFactory(cellData -> cellData.getValue().getNotIncludedExpProperty());
			clGrandTotal.setCellValueFactory(cellData -> cellData.getValue().getGrandTotalExpProperty());
			clImportExpense.setCellValueFactory(cellData -> cellData.getValue().getImportDutyProperty());

		}
	    
	    public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

	}
}


