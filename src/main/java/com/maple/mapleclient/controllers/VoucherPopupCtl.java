package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class VoucherPopupCtl {
	
	
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	VoucherNoEvent 	voucherNoEvent;
	
	 	@FXML
	    private TextField txtname;

	    @FXML
	    private TableView<StockTransferInHdr> tblItemPopupView;
	    @FXML
	    private TableColumn<StockTransferInHdr, String> clVoucher;
	    

	    @FXML
	    private TableColumn<StockTransferInHdr, String> clid;

	    @FXML
	    private TableColumn<StockTransferInHdr, String> clVoucherDate;

	    @FXML
	    private TableColumn<StockTransferInHdr, String> clFromBranch;
	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private Button btnCancel;

	    private ObservableList<StockTransferInHdr> voucherNoList = FXCollections.observableArrayList();
		StringProperty SearchString = new SimpleStringProperty();
		
		 @FXML
			private void initialize() {
		    	
		    	txtname.textProperty().bindBidirectional(SearchString);
		    	voucherNoEvent = new VoucherNoEvent();
		    	eventBus.register(this);
		    	LoadVoucherNoPopupBySearch("");
		    	tblItemPopupView.setItems(voucherNoList);
		    	clVoucher.setCellValueFactory(cellData -> cellData.getValue().getInVoucherNumberProperty());
		    	clid.setCellValueFactory(cellData -> cellData.getValue().getRecIdProperty());
		    	clFromBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchProperty());
		    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());

		    	tblItemPopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
		    			
		    	   if (newSelection != null) {
				    	if(null!=newSelection.getId()&& newSelection.getInVoucherNumber().length()>0) {
				    		voucherNoEvent.setIntentNumber(newSelection.getIntentNumber());
				    		voucherNoEvent.setFromBranchCode(newSelection.getFromBranch());
				    		voucherNoEvent.setVoucherNumber(newSelection.getInVoucherNumber());
				    		voucherNoEvent.setId(newSelection.getId());
				    		voucherNoEvent.setVoucherDate(newSelection.getVoucherDate());
				    		System.out.println("Voucher================"+voucherNoEvent);
//				    		eventBus.post(voucherNoEvent);
				    		
				    	}
				    }
				});
		    	 SearchString.addListener(new ChangeListener(){
						@Override
						public void changed(ObservableValue observable, Object oldValue, Object newValue) {
						 					
							LoadVoucherNoPopupBySearch((String)newValue);
						}
			        });
	    	 
		 }

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				eventBus.post(voucherNoEvent);
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB) {

			} else {
				txtname.requestFocus();
			}
	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tblItemPopupView.requestFocus();
				tblItemPopupView.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			}
	    }
	    @FXML
	    void onCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
	    }

	    @FXML
	    void submit(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
	    	eventBus.post(voucherNoEvent);
			stage.close();
	    }

	    private void LoadVoucherNoPopupBySearch(String searchData) {
	    	
	    	
	    	ArrayList stockin = new ArrayList();
	    	voucherNoList.clear();
	    	stockin = RestCaller.SearchVoucherNo(searchData);
	    	Iterator itr = stockin.iterator();
	    	while (itr.hasNext()) {
	    		 LinkedHashMap element = (LinkedHashMap) itr.next();
	    		// Object intentNumber = (String) element.get("intentNumber");
	    		 Object id = (String) element.get("id");
	    		 Object inVoucherNumber = (String) element.get("inVoucherNumber");
	    		 Object fromBranch = (String)element.get("fromBranch");
	    		 Object intentNo=(String)element.get("intentNumber");
	    		 Object voucherDate=(String)element.get("voucherDate");
	    		 if (null != id) {
	    			 StockTransferInHdr stk = new StockTransferInHdr();
	    			 stk.setInVoucherNumber((String)inVoucherNumber);
	    			 stk.setId((String) id);
	    			 stk.setFromBranch((String)fromBranch);
	    			 stk.setIntentNumber((String)intentNo);
	    			 stk.setVoucherDate(SystemSetting.StringToUtilDate(voucherDate.toString(), "yyyy-MM-dd"));
	    			 voucherNoList.add(stk);
	    		 }
	    	}
	    	return;
	    }
	    
	    @Subscribe
  	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
  	   		//if (stage.isShowing()) {
  	   			taskid = taskWindowDataEvent.getId();
  	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  	   			
  	   		 
  	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  	   			System.out.println("Business Process ID = " + hdrId);
  	   			
  	   			 PageReload(hdrId);
  	   		}


  	   	private void PageReload(String hdrId) {

  	   	}
	    	

}
