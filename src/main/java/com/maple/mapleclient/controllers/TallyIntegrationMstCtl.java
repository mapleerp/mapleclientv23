package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.MenuMst;
import com.maple.mapleclient.entity.TallyIntegrationMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;

public class TallyIntegrationMstCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<TallyIntegrationMst> tallyIntegrationList = FXCollections.observableArrayList();

	@FXML
	private TextField txtTallyUrl;

	@FXML
	private Button tbnSave;

	@FXML
	private TableView<TallyIntegrationMst> tblTally;

	@FXML
	private TableColumn<TallyIntegrationMst, String> clUrl;

	@FXML
	private void initialize() {

		ResponseEntity<List<TallyIntegrationMst>> tallyIntegrationListResp = RestCaller.getlAllTallyIntegrationMst();
		tallyIntegrationList = FXCollections.observableArrayList(tallyIntegrationListResp.getBody());
		FillTable();

	}

	@FXML
	void SAVE(ActionEvent event) {

		if (txtTallyUrl.getText().isEmpty()) {
			notifyMessage(3, "Plese enter tally url", false);
			txtTallyUrl.requestFocus();
			return;
		}

		ResponseEntity<List<TallyIntegrationMst>> tallyIntegrationResp = RestCaller.getTallyIntegratonMsts();
		List<TallyIntegrationMst> tallyList = tallyIntegrationResp.getBody();
		if (null != tallyList) {
			RestCaller.deleteAllTallyIntegrationMst();
		}

		TallyIntegrationMst tallyIntegrationMst = new TallyIntegrationMst();
		tallyIntegrationMst.setTallyUrl(txtTallyUrl.getText());
		ResponseEntity<TallyIntegrationMst> tallyIntegration = RestCaller.saveTallyIntegrationMst(tallyIntegrationMst);

		tallyIntegrationMst = null;
		txtTallyUrl.clear();

		ResponseEntity<List<TallyIntegrationMst>> tallyIntegrationListResp = RestCaller.getlAllTallyIntegrationMst();
		tallyIntegrationList = FXCollections.observableArrayList(tallyIntegrationListResp.getBody());
		FillTable();

	}

	private void FillTable() {

		tblTally.setItems(tallyIntegrationList);
		clUrl.setCellValueFactory(cellData -> cellData.getValue().getTallyUrlProperty());

	}

	@FXML
	private DatePicker dpFromDate;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private ComboBox<String> cmbType;

	@FXML
	private Button tbnTallyForward;
	
    @FXML
    private Label lblTallyResponse;

	@FXML
	void TallyForward(ActionEvent event) {
		
		if(null == dpFromDate.getValue())
		{
			notifyMessage(3, "Please select from date", false);
			dpFromDate.requestFocus();
			return;
			
		}
		
		if(null == dpToDate.getValue())
		{
			notifyMessage(3, "Please select to date", false);
			dpToDate.requestFocus();
			return;
			
		}
		
		if(null == cmbType.getSelectionModel().getSelectedItem())
		{
			notifyMessage(3, "Please select type", false);
			cmbType.requestFocus();
			return;
		}
		
		Date udate =SystemSetting.localToUtilDate(dpFromDate.getValue());
    	String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
    	
    	Date date =SystemSetting.localToUtilDate(dpToDate.getValue());
    	String edate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
    	
    	String type = cmbType.getSelectionModel().getSelectedItem().toString();
    	

    	
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
