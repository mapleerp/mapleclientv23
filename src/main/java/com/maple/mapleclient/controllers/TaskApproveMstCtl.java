package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public class TaskApproveMstCtl {
	
	String taskid;
	String processInstanceId;

    @FXML
    private TextField txtVoucherNumber;

    @FXML
    private Button btnReturn;

    @FXML
    private Button btnObject;

    @FXML
    private Button btnApproved;

    @FXML
    private TextField txtReason;

    @FXML
    private DatePicker dpVoucherDate;

    @FXML
    private TextField txtComment;

    @FXML
    private TextField txtUserID;

    @FXML
    private TextField txtTaskID;

    @FXML
    private DatePicker dpActionDate;

    @FXML
    private ComboBox<?> cmbVoucherType;
    @FXML
   	private void initialize() {
   		dpActionDate = SystemSetting.datePickerFormat(dpActionDate, "dd/MMM/yyyy");
   		dpVoucherDate = SystemSetting.datePickerFormat(dpVoucherDate, "dd/MMM/yyyy");
       }
    @FXML
    void Approve(ActionEvent event) {

    }

    @FXML
    void Object(ActionEvent event) {

    }

    @FXML
    void Return(ActionEvent event) {

    }

    @FXML
    void actionDateOnEnter(KeyEvent event) {

    }

    @FXML
    void addOnEnter(KeyEvent event) {

    }

    @FXML
    void commentOnEnter(KeyEvent event) {

    }

    @FXML
    void reasonOnEnter(KeyEvent event) {

    }

    @FXML
    void saveOnKey(KeyEvent event) {

    }

    @FXML
    void taskIDOnEnter(KeyEvent event) {

    }

    @FXML
    void userIDOnEnter(KeyEvent event) {

    }

    @FXML
    void vouchernumberOnEnter(KeyEvent event) {

    }
    
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}

}
