package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JobCardDtl;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.entity.LocationMst;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.components.table.fill.FillTable;
import net.sf.jasperreports.engine.JRException;

public class JobCardRepoprtCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<JobCardHdr> jobCardHdrList = FXCollections.observableArrayList();
	private ObservableList<JobCardDtl> jobCardDtlList = FXCollections.observableArrayList();

	JobCardHdr jobCardHdr = null;
	@FXML
	private DatePicker dpFromDate;

	@FXML
	private TableView<JobCardHdr> tbReport;

	@FXML
	private TableColumn<JobCardHdr, String> clVoucherNo;

	@FXML
	private TableColumn<JobCardHdr, LocalDate> clVoucherDate;

	@FXML
	private TableColumn<JobCardHdr, String> clCustomer;

	@FXML
	private TableColumn<JobCardHdr, String> clItem;

	@FXML
	private TableColumn<JobCardHdr, String> clStatus;

	@FXML
	private Button btnShow;

	@FXML
	private TableView<JobCardDtl> tblItems;

	@FXML
	private TableColumn<JobCardDtl, String> Customername;

	@FXML
	private TableColumn<JobCardDtl, String> clItemName;

	@FXML
	private TableColumn<JobCardDtl, Number> clQty;

	@FXML
	private TableColumn<JobCardDtl, Number> clMrp;

	@FXML
	private TableColumn<JobCardDtl, Number> clTax;

	@FXML
	private TableColumn<JobCardDtl, Number> clCess;

	@FXML
	private Button ok;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private Button btnPrintReport;

	@FXML
	private Button btnPirntInvoice;

	@FXML
	private void initialize() {
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					jobCardHdr = new JobCardHdr();
					if (null != newSelection.getSalesTransHdrId()) {
						jobCardHdr.setSalesTransHdrId(newSelection.getSalesTransHdrId());
					}

					ResponseEntity<List<JobCardDtl>> jobCardDtl = RestCaller.getJobCardDtlByHdrId(newSelection.getId());
					jobCardDtlList = FXCollections.observableArrayList(jobCardDtl.getBody());
					fillTableDtl();
				}
			}
		});

	}

	private void fillTableDtl() {

		for (JobCardDtl jobCard : jobCardDtlList) {
			if (null != jobCard.getItemId()) {
				ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(jobCard.getItemId());
				ItemMst itemMst = itemResp.getBody();
				jobCard.setItemName(itemMst.getItemName());
				jobCard.setMrp(itemMst.getStandardPrice());
				jobCard.setCess(itemMst.getCess());
				jobCard.setTax(itemMst.getTaxRate());

			} else {
				
				ResponseEntity<LocationMst> locationMst = RestCaller.findLocationMstById(jobCard.getLocationId());
				jobCard.setItemName(locationMst.getBody().getLocation());
				jobCard.setQty(0.0);
				jobCard.setMrp(0.0);
				jobCard.setCess(0.0);
				jobCard.setTax(0.0);
			}
			if(null == jobCard.getQty())
			{
				jobCard.setQty(0.0);
				jobCard.setMrp(0.0);
				jobCard.setCess(0.0);
				jobCard.setTax(0.0);
			}
		}

		tblItems.setItems(jobCardDtlList);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clCess.setCellValueFactory(cellData -> cellData.getValue().getCessProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());

	}

	@FXML
	void PrintReport(ActionEvent event) {

		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "Please Select From Date ", false);
			dpFromDate.requestFocus();
			return;
		}
		if (null == dpToDate.getValue()) {
			notifyMessage(5, "Please Select To Date ", false);
			dpToDate.requestFocus();
			return;
		}

		Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");

		try {
			JasperPdfReportService.JobCardReport(startDate, endDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void onClickOk(ActionEvent event) {

	}

	@FXML
	void PrintInvoice(ActionEvent event) {

		if (null != jobCardHdr) {
			if (null != jobCardHdr.getSalesTransHdrId()) {
				SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(jobCardHdr.getSalesTransHdrId());

				String date = SystemSetting.UtilDateToString(salesTransHdr.getVoucherDate(), "yyyy-MM-dd");
				try {
					JasperPdfReportService.TaxInvoiceReport(salesTransHdr.getVoucherNumber(), date);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				jobCardHdr = null;
			}
		}

	}

	@FXML
	void show(ActionEvent event) {

		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "Please Select From Date ", false);
			dpFromDate.requestFocus();
			return;
		}
		if (null == dpToDate.getValue()) {
			notifyMessage(5, "Please Select To Date ", false);
			dpToDate.requestFocus();
			return;
		}

		Date sdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sdate, "yyyy-MM-dd");

		Date edate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(edate, "yyyy-MM-dd");

		ResponseEntity<List<JobCardHdr>> jobCardListResp = RestCaller.getJobCardHdrBetweenDate(startDate, endDate);
		jobCardHdrList = FXCollections.observableArrayList(jobCardListResp.getBody());
		FillTable();

	}

	private void FillTable() {

		for (JobCardHdr job : jobCardHdrList) {
			if (null != job.getCustomerId()) {
				ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadsById(job.getCustomerId());
				AccountHeads accountHeads = accountHeadsResp.getBody();
				if (null != accountHeads) {
					job.setCustomerName(accountHeads.getAccountName());
					job.setItemName(job.getServiceInDtl().getItemName());
				}

			}
		}

		tbReport.setItems(jobCardHdrList);
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());

		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNoProperty());
		clItem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clStatus.setCellValueFactory(cellData -> cellData.getValue().getStatusProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {
		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
