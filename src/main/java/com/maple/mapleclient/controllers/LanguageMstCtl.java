package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.LanguageMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.SchemeInstance;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class LanguageMstCtl {
	String taskid;
	String processInstanceId;

	
	private ObservableList<LanguageMst> languageMstList = FXCollections.observableArrayList();

	
	LanguageMst lang = null;

    @FXML
    private TextField txtLanguage;

    @FXML
    private TextField txtFontType;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;

    @FXML
    private TableView<LanguageMst> tblLanguage;

    @FXML
    private TableColumn<LanguageMst, String> clLanguage;

    @FXML
    private TableColumn<LanguageMst, String> clFontType;
    
    @FXML
	private void initialize() {
    	
    	tblLanguage.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					lang = new LanguageMst();
					lang.setId(newSelection.getId());
				

				}
			}
		});
    	
    }

    @FXML
    void Delete(ActionEvent event) {
    	
    	if(null != lang)
    	{
    		if(null != lang.getId())
    		{
    			RestCaller.deleteLanguageById(lang.getId());
    			ShowAll();
    		}
    		lang = null;
    	}
    	
    	
    }

    private void FillTable() {
		
    	tblLanguage.setItems(languageMstList);
    	
    	clLanguage.setCellValueFactory(cellData -> cellData.getValue().getLanguageNameProperty());
    	clFontType.setCellValueFactory(cellData -> cellData.getValue().getLanguageFontTypeProperty());


	}

	@FXML
    void Save(ActionEvent event) {

		if(txtLanguage.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"please enter language name", false);
    		txtLanguage.requestFocus();
    		return;
    		
    	}
    	if(txtFontType.getText().trim().isEmpty())
    	{
    		notifyMessage(5,"please enter font type", false);
    		txtFontType.requestFocus();
    		return;
    	}
    	
    	lang = new LanguageMst();
    	lang.setLanguageFontType(txtFontType.getText());
    	lang.setLanguageName(txtLanguage.getText());
    	ResponseEntity<LanguageMst> langResp = RestCaller.saveLanguageMst(lang);
    	lang = langResp.getBody();
    	if(null != lang)
    	{
    		notifyMessage(5,"Successfully Added", true);
    		languageMstList.add(lang);
    		FillTable();

    	}
    	lang = null;

    }

    @FXML
    void ShowAll(ActionEvent event) {
    	
    	ShowAll();

    }

    private void ShowAll() {
		
    	ResponseEntity<List<LanguageMst>> alngResp = RestCaller.getAllLangues();
    	languageMstList =  FXCollections.observableArrayList(alngResp.getBody());
    	FillTable();
	}

	@FXML
    void txtFontTypeKeyPress(KeyEvent event) {

    }

    @FXML
    void txtLangKeyPrsss(KeyEvent event) {

    }
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }
}
