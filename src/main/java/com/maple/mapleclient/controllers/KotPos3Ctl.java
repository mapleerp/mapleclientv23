//import com.maple.mapleclient.restService.RestCaller;

//package com.maple.mapleclient.controllers;
//
//import java.io.IOException;#
//import java.math.BigDecimal;
//import java.sql.Date;
//import java.sql.SQLException;
//import java.sql.Timestamp;
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//import org.controlsfx.control.Notifications;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.ResponseEntity;
//
//import com.google.common.eventbus.EventBus;
//import com.google.common.eventbus.Subscribe;
//import com.maple.javapos.print.POSThermalPrintABS;
//import com.maple.javapos.print.POSThermalPrintFactory;
//import com.maple.maple.util.SystemSetting;
//import com.maple.mapleclient.EventBusFactory;
//import com.maple.mapleclient.entity.AccountHeads;
//import com.maple.mapleclient.entity.AddKotTable;
//import com.maple.mapleclient.entity.AddKotWaiter;
//import com.maple.mapleclient.entity.CustomerMst;
//import com.maple.mapleclient.entity.DayEndClosureHdr;
//import com.maple.mapleclient.entity.ItemMst;
//import com.maple.mapleclient.entity.KitDefinitionMst;
//import com.maple.mapleclient.entity.MultiUnitMst;
//import com.maple.mapleclient.entity.ParamValueConfig;
//import com.maple.mapleclient.entity.PriceDefenitionMst;
//import com.maple.mapleclient.entity.PriceDefinition;
//import com.maple.mapleclient.entity.ReceiptModeMst;
//import com.maple.mapleclient.entity.SalesDtl;
//import com.maple.mapleclient.entity.SalesReceipts;
//import com.maple.mapleclient.entity.SalesTransHdr;
//import com.maple.mapleclient.entity.Summary;
//import com.maple.mapleclient.entity.TableOccupiedMst;
//import com.maple.mapleclient.entity.TaxMst;
//import com.maple.mapleclient.entity.UnitMst;
//import com.maple.mapleclient.entity.WeighingItemMst;
//import com.maple.mapleclient.events.ItemPopupEvent;
//import com.maple.mapleclient.events.TableOccupiedEvent;
//import com.maple.mapleclient.restService.RestCaller;
//import com.maple.report.entity.DayBook;
//import com.maple.report.entity.TableWaiterReport;
//
//import javafx.application.Platform;
//import javafx.beans.property.SimpleStringProperty;
//import javafx.beans.property.StringProperty;
//import javafx.beans.value.ChangeListener;
//import javafx.beans.value.ObservableValue;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.fxml.FXML;
//import javafx.fxml.FXMLLoader;
//import javafx.geometry.Pos;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.scene.control.Alert;
//import javafx.scene.control.Button;
//import javafx.scene.control.ButtonType;
//import javafx.scene.control.ComboBox;
//import javafx.scene.control.Label;
//import javafx.scene.control.ListView;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.scene.control.Alert.AlertType;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.KeyEvent;
//import javafx.scene.input.MouseEvent;
//import javafx.stage.Modality;
//import javafx.stage.Stage;
//import javafx.stage.StageStyle;
//import javafx.util.Duration;
//
////public class KotPos3Ctl {
////
////	private static final Logger logger = LoggerFactory.getLogger(KOTPosWindowCtl.class);
////	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();
////	SalesReceipts salesReceipts = new SalesReceipts();
////	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
////	private ObservableList<SalesReceipts> salesReceiptsList = FXCollections.observableArrayList();
////	private ObservableList<TableWaiterReport> tableWaiterList = FXCollections.observableArrayList();
////	UnitMst unitMst = null;
//////	String invoiceNumberPrefix = SystemSetting.POS_SALES_PREFIX;
////	String fixedQty = null;
////	POSThermalPrintABS printingSupport;
////	Double rateBeforeDiscount=0.0;
////	EventBus eventBus = EventBusFactory.getEventBus();
////	private ObservableList<AddKotTable> addKotTableList1 = FXCollections.observableArrayList();
////
////	private ObservableList<AddKotWaiter> addKotWaiterList = FXCollections.observableArrayList();
////
////	// ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();
////
////	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
////	private ObservableList<SalesDtl> saleDtlObservableList = FXCollections.observableArrayList();
////
////	SalesDtl salesDtl = null;
////	SalesTransHdr salesTransHdr = null;
////
////	boolean multi = false;
////	double qtyTotal = 0;
////	double amountTotal = 0;
////	double discountTotal = 0;
////	double taxTotal = 0;
////	double cessTotal = 0;
////	double discountBfTaxTotal = 0;
////	double grandTotal = 0;
////	double expenseTotal = 0;
////	String custId = "";
////	String salesReceiptVoucherNo = null;
////
////	double cardAmount = 0.0;
////
////	StringProperty cardAmountLis = new SimpleStringProperty("");
////	StringProperty paidAmtProperty = new SimpleStringProperty("");
////	StringProperty itemNameProperty = new SimpleStringProperty("");
////	StringProperty batchProperty = new SimpleStringProperty("");
////
////	StringProperty barcodeProperty = new SimpleStringProperty("");
////
////	StringProperty taxRateProperty = new SimpleStringProperty("");
////	StringProperty mrpProperty = new SimpleStringProperty("");
////	StringProperty unitNameProperty = new SimpleStringProperty("");
////	StringProperty cessRateProperty = new SimpleStringProperty("");
////	StringProperty changeAmtProperty = new SimpleStringProperty("");
////	@FXML
////	private TableView<SalesDtl> itemDetailTable;
////    @FXML
////    private Button btnClearSelection;
////	@FXML
////	private TableColumn<SalesDtl, String> columnItemName;
////
////	@FXML
////	private TableColumn<SalesDtl, String> columnBarCode;
////
////	@FXML
////	private TableColumn<SalesDtl, String> columnQty;
////
////	@FXML
////	private Label lblpaidAmount;
////    @FXML
////    private TextField txtposDiscount;
////
////    @FXML
////    private Label lblDiscount;
////	@FXML
////	private ComboBox<String> cmbUnit;
////	@FXML
////	private TableColumn<SalesDtl, String> columnTaxRate;
////
////	@FXML
////	private TableColumn<SalesDtl, String> columnMrp;
////
////	@FXML
////	private TableColumn<SalesDtl, String> columnBatch;
////
////	@FXML
////	private TableColumn<SalesDtl, String> columnCessRate;
////
////	@FXML
////	private TableColumn<SalesDtl, String> columnUnitName;
////
////	@FXML
////	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;
////
////	@FXML
////	private TableColumn<SalesDtl, Number> clAmount;
////	@FXML
////	private TableView<TableWaiterReport> tblTableWaiter;
////
////	@FXML
////	private TableColumn<TableWaiterReport, String> clTable;
////
////	@FXML
////	private TableColumn<TableWaiterReport, String> clWaiter;
////	@FXML
////	private TextField txtTable;
////
////	@FXML
////	private Button btnAddTable;
////	@FXML
////	private TextField txtItemname;
////
////	@FXML
////	private TextField txtRate;
////
////	@FXML
////	private Button btnAdditem;
////
////	@FXML
////	private TextField txtBarcode;
////
////	@FXML
////	private TextField txtQty;
////
////	@FXML
////	private TextField txtBatch;
////
////	@FXML
////	private Button btnUnhold;
////
////	@FXML
////	private Button btnHold;
////
////	@FXML
////	private Button btnDeleterow;
////
////	@FXML
////	private TextField txtcardAmount;
////
////	@FXML
////	private Button btnSave;
////
////	@FXML
////	private Button btnRefresh;
////
////	@FXML
////	private TextField txtPaidamount;
////
////	@FXML
////	private TextField txtCashtopay;
////
////	@FXML
////	private TextField txtChangeamount;
////
////	@FXML
////	private TextField custname;
////	@FXML
////	private TextField custAdress;
////	@FXML
////	private TextField gstNo;
////
////	@FXML
////	private Button btnPerformaInvoice;
////	@FXML
////	private ListView<String> servingtable;
////
////	@FXML
////	private ListView<String> salesman;
////
////	@FXML
////	private Button btnCardSale;
////
////	@FXML
////	private Label lblCardType;
////
////	@FXML
////	private Label lblCardAmount;
////
////	@FXML
////	void OnActionVisibleCardSale(ActionEvent event) {
////
////		visibleCardSale();
////
////	}
////
////	private void visibleCardSale() {
////
////		cmbCardType.setVisible(true);
////		txtCrAmount.setVisible(true);
////		AddCardAmount.setVisible(true);
////		btnDeleteCardAmount.setVisible(true);
////		tblCardAmountDetails.setVisible(true);
////		txtcardAmount.setVisible(true);
////		lblCardType.setVisible(true);
////		lblCardAmount.setVisible(true);
////
////	}
////
////	// ---------------------------------------------------------------------------------------------
////	@FXML
////	private ComboBox<String> cmbCardType;
////
////	@FXML
////	void FocusOnCardAmount(KeyEvent event) {
////		if (event.getCode() == KeyCode.ENTER) {
////			txtCrAmount.requestFocus();
////		}if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////		if(event.getCode() == KeyCode.RIGHT)
////		{
////			txtPaidamount.requestFocus();
////		}
////	}
////
////	@FXML
////	void setCardType(MouseEvent event) {
////
////		setCardType();
////	}
////
////	@FXML
////	private TextField txtCrAmount;
////
////	@FXML
////	void KeyPressFocusAddBtn(KeyEvent event) {
////
////		if (event.getCode() == KeyCode.ENTER) {
////
////			if (txtCrAmount.getText().length() == 0 && null == cmbCardType.getValue()) {
////				btnSave.requestFocus();
////			} else {
////
////				AddCardAmount.requestFocus();
////			}
////		}
////		if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////		if(event.getCode() == KeyCode.RIGHT)
////		{
////			txtPaidamount.requestFocus();
////		}
////	}
////
////	private void setCardType() {
////		cmbCardType.getItems().clear();
////
////		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
////		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());
////
////		for (ReceiptModeMst receiptModeMst : receiptModeList) {
////			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
////				cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
////			}
////		}
////	}
////
////	@FXML
////	private Button AddCardAmount;
////
////	@FXML
////	void KeyPressAddCardAmount(KeyEvent event) {
////		if (event.getCode() == KeyCode.ENTER) {
////
////			addCardAmount();
////		}
////		if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////		if(event.getCode()== KeyCode.RIGHT)
////		{
////			txtPaidamount.requestFocus();
////		}
////	}
////
////	@FXML
////	void addCardAmount(ActionEvent event) {
////
////		addCardAmount();
////
////	}
////
////	private void addCardAmount() {
////		logger.info("KOTPOS====== add Card Amount started===========");
////
////		if (null != salesTransHdr) {
////
////			if (null == cmbCardType.getValue()) {
////
////				notifyMessage(5, "Please select card type!!!!");
////				cmbCardType.requestFocus();
////				return;
////
////			} else if (txtCrAmount.getText().trim().isEmpty()) {
////
////				notifyMessage(1, "Please enter amount!!!!");
////				txtCrAmount.requestFocus();
////				return;
////
////			} else {
////
////				salesReceipts = new SalesReceipts();
////				Double prcardAmount =0.0;
////				 prcardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
////				Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
////				Double cardAmount = Double.parseDouble(txtCrAmount.getText());
////				if((prcardAmount+cardAmount)>invoiceAmount)
////				{
////					notifyFailureMessage(3,"Card Amount should be less than or equal to invoice Amount");
////					return;
////				}
////				ResponseEntity<AccountHeads> accountHeads = RestCaller
////						.getAccountHeadByName(cmbCardType.getSelectionModel().getSelectedItem());
////				salesReceipts.setAccountId(accountHeads.getBody().getId());
////
////				salesReceipts.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());
////				salesReceipts.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
////				salesTransHdr.setUserId(SystemSetting.getUserId());
////				salesReceipts.setBranchCode(SystemSetting.systemBranch);
////				if (null == salesReceiptVoucherNo) {
////					salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
////					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
////				} else {
////					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
////				}
//////				LocalDate date = LocalDate.now();
//////				java.util.Date udate = SystemSetting.localToUtilDate(date);
////				salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
////				salesReceipts.setSalesTransHdr(salesTransHdr);
////				System.out.println(salesReceipts);
////				ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
////				salesReceipts = respEntity.getBody();
////
////				if (null != salesReceipts) {
////					if (null != salesReceipts.getId()) {
////						notifyMessage(1, "Successfully added cardAmount");
//////	    				cardAmount = cardAmount+Double.parseDouble(txtCrAmount.getText());
////						// txtcardAmount.setText(Double.toString(txtPaidamount));
////						salesReceiptsList.add(salesReceipts);
////						filltableCardAmount();
////					}
////				}
////
////				txtCrAmount.clear();
////				cmbCardType.getSelectionModel().clearSelection();
////				cmbCardType.requestFocus();
////				salesReceipts = null;
////
////				logger.info("KOTPOS======= ADD ITEM FINISHED");
////			}
////		} else {
////			notifyMessage(1, "Please add Item!!!!");
////			return;
////		}
////	}
////
////	@FXML
////	private Button btnDeleteCardAmount;
////
////	@FXML
////	void DeleteCardAmount(ActionEvent event) {
////		logger.info("KOTPOS======== delete card amount");
////		if (null != salesReceipts) {
////			if (null != salesReceipts.getId()) {
////				RestCaller.deleteSalesReceipts(salesReceipts.getId());
////				tblCardAmountDetails.getItems().clear();
////				ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
////						.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
////				salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());
////
////				try {
////					cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
////				} catch (Exception e) {
////
////					txtcardAmount.setText("");
////				}
////
////				filltableCardAmount();
////
////			}
////		} else {
////
////		}
////		logger.info("KOTPOS========= delete card amount finished");
////
////	}
////
////	@FXML
////	void KeyPressDeleteCardAmount(KeyEvent event) {
////
////	}
////
////	@FXML
////	private TableView<SalesReceipts> tblCardAmountDetails;
////
////	@FXML
////	private TableColumn<SalesReceipts, String> clCardTye;
////
////	@FXML
////	private TableColumn<SalesReceipts, Number> clCardAmount;
////
////	private void filltableCardAmount() {
////
////		cmbCardType.setVisible(true);
////		txtCrAmount.setVisible(true);
////		AddCardAmount.setVisible(true);
////		btnDeleteCardAmount.setVisible(true);
////		tblCardAmountDetails.setVisible(true);
////		txtcardAmount.setVisible(true);
////		lblCardType.setVisible(true);
////		lblCardAmount.setVisible(true);
////
////		tblCardAmountDetails.setItems(salesReceiptsList);
////		clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
////		clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());
////
////		cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
////		txtcardAmount.setText(Double.toString(cardAmount));
////
////	}
////	// ---------------------------------------------------------------------------------------------
////
////	@FXML
////	void Refresh(ActionEvent event) {
////		addTableWaiter();
////	}
////
////	@FXML
////	void onEnterCustPopup(KeyEvent event) {
////
////		if (event.getCode() == KeyCode.ENTER) {
////			loadCustomerPopup();
////
////		}
////	}
////
////	@FXML
////	void CustomerPopUp(MouseEvent event) {
////
////		loadCustomerPopup();
////	}
////
////	@FXML
////	void printonlyinvoice(ActionEvent event) {
////		
////		printPerforma();
////	}
////
////	private void printPerforma()
////	{
////		logger.info("KOTPOS ======== print invoice started");
////		try {
////			printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
////			logger.info("KOTPOS ======== print invoice completed");
////		} catch (SQLException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////	}
////	@FXML
////	void mouseClickOnItemName(MouseEvent event) {
////		showPopup();
////	}
////
////	@FXML
////	void qtyKeyRelease(KeyEvent event) {
////		if (event.getCode() == KeyCode.ENTER) {
////			if (txtQty.getText().length() > 0)
////				addItem();
////			
////		}  if (event.getCode() == KeyCode.LEFT) 
////		{
////
////			txtBarcode.requestFocus();
////		} 
////		if (event.getCode() == KeyCode.BACK_SPACE && txtQty.getText().length() == 0)
////		{
////			txtBarcode.requestFocus();
////		}
////		if (event.getCode() == KeyCode.DOWN) 
////		{
////			
////
////			txtPaidamount.requestFocus();
////		} 
////		if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////	}
////
////	@FXML
////	void keyPressOnItemName(KeyEvent event) {
////		if (event.getCode() == KeyCode.ENTER) {
////			showPopup();
////		}
////		 if((event.getCode() == KeyCode.K && event.isControlDown()))
////			{
////				btnPrintKot.requestFocus();
////			}
////		 if(event.getCode()== KeyCode.ESCAPE)
////		 {
////			 txtPaidamount.requestFocus();
////		 }
////		 if(event.getCode() == KeyCode.K && event.isControlDown())
////			{
////				printKot();
////			}
////			if(event.getCode() == KeyCode.P && event.isControlDown())
////			{
////				printPerforma();
////			}
////	}
////	
////
////    @FXML
////    void keyPressOnPrintKot(KeyEvent event) {
////    	if (event.getCode() == KeyCode.ENTER) {
////    		printKot();
////		}
////    	if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////    }
////
////	@FXML
////	private Button btnPrintKot;
////
////	@FXML
////	void PrintKot(ActionEvent event) {
////		printKot();
////	}
////	private void printKot()
////	{
////
////
////		if (servingtable.getSelectionModel().isEmpty()) {
////			notifyMessage(1, "Please Select Table!!!");
////			return;
////
////		} else if (salesman.getSelectionModel().isEmpty()) {
////			notifyMessage(1, "Please Select Salesman!!!");
////			return;
////		}
////
////		String salesMan = salesman.getSelectionModel().getSelectedItem().toString();
////		String tableName = servingtable.getSelectionModel().getSelectedItem().toString();
////
////		ResponseEntity<List<SalesTransHdr>> kotPrint = RestCaller.PrintKot(salesMan, tableName);
////
////		servingtable.requestFocus();
////	
////	}
////
////	@FXML
////	private void initialize() {
////		tblTableWaiter.requestFocus();
////		logger.info("KOTPOS ======== initialization started");
////		txtposDiscount.setVisible(false);
////		lblDiscount.setVisible(false);
////		
////		ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("PosDiscount");
////		if(null == getParam.getBody())
////		{
////			txtposDiscount.setVisible(false);
////			lblDiscount.setVisible(false);
////		}
////		else 
////		{
////			if(getParam.getBody().getValue().equalsIgnoreCase("YES"))
////			{
////				txtposDiscount.setVisible(true);
////				lblDiscount.setVisible(true);
////			}
////			else
////			{
////				txtposDiscount.setVisible(false);
////				lblDiscount.setVisible(false);
////			}
////		}
////		lblpaidAmount.setVisible(false);
////		/*
////		 * Load logo to printing support once
////		 */
////		printingSupport = POSThermalPrintFactory.getPOSThermalPrintABS();
////		try {
////			printingSupport.setupLogo();
////
////		} catch (IOException e1) {
////			// TODO Auto-generated catch block
////			logger.info("KOTPOS ======== " + e1);
////			e1.printStackTrace();
////		}
////
////		try {
////			printingSupport.setupBottomline(SystemSetting.getPosInvoiceBottomLine());
////
////		} catch (IOException e1) {
////			e1.printStackTrace();
////		}
////
////		setCardType();
////		/*
////		 * servingtable.getItems().add("TABLE1"); servingtable.getItems().add("TABLE2");
////		 * servingtable.getItems().add("TABLE3"); servingtable.getItems().add("TABLE5");
////		 * servingtable.getItems().add("TABLE6"); servingtable.getItems().add("TABLE7");
////		 * servingtable.getItems().add("TABLE8"); servingtable.getItems().add("TABLE9");
////		 * servingtable.getItems().add("TABLE63");
////		 * servingtable.getItems().add("TABLE2"); servingtable.getItems().add("TABLE2");
////		 * servingtable.getItems().add("TABLE34");
////		 * servingtable.getItems().add("TABLE34");
////		 * servingtable.getItems().add("TABLE34");
////		 * servingtable.getItems().add("TABLE34");
////		 * servingtable.getItems().add("TABLE42");
////		 * servingtable.getItems().add("TABLE41");
////		 * 
////		 */
////
////		/*
////		 * servingtable.getItems().add("TABLE1"); servingtable.getItems().add("TABLE2");
////		 * 
////		 * servingtable.getItems().add("TABLE3"); servingtable.getItems().add("TABLE5");
////		 * servingtable.getItems().add("TABLE6"); servingtable.getItems().add("TABLE7");
////		 * servingtable.getItems().add("TABLE8"); servingtable.getItems().add("TABLE9");
////		 * servingtable.getItems().add("TABLE63");
////		 * servingtable.getItems().add("TABLE2"); servingtable.getItems().add("TABLE2");
////		 * servingtable.getItems().add("TABLE34");
////		 * servingtable.getItems().add("TABLE34");
////		 * servingtable.getItems().add("TABLE34");
////		 * servingtable.getItems().add("TABLE34");
////		 * servingtable.getItems().add("TABLE42");
////		 * servingtable.getItems().add("TABLE41");
////		 */
////
////		// saletype.getItems().add("ONLINE");
////
////		// salesman.getItems().add("WAITER-001"); salesman.getItems().add("WAITER-002");
////		// salesman.getItems().add("WAITER-003"); salesman.getItems().add("WAITER-004");
////		// salesman.getItems().add("WAITER-005");
////
////		/*
////		 * Create an instance of SalesDtl. SalesTransHdr entity will be refreshed after
////		 * final submit. SalesDtl will be added on AddItem Function
////		 * 
////		 */
////		addTableWaiter();
////		salesDtl = new SalesDtl();
////		/*
////		 * Fieds with Entity property
////		 */
////		txtCashtopay.setStyle("-fx-text-inner-color: red;-fx-font-size: 24px;");
////
////		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
////		txtItemname.textProperty().bindBidirectional(itemNameProperty);
////		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
////		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
////		txtBarcode.textProperty().bindBidirectional(barcodeProperty);
////
////		txtRate.textProperty().bindBidirectional(mrpProperty);
////
////		txtBatch.textProperty().bindBidirectional(batchProperty);
//////		txtTax.textProperty().bindBidirectional(taxRateProperty);
////
////		// getInProgressKOT
////
////		salesman.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
////			public void changed(ObservableValue<? extends String> ov, final String oldvalue, final String newvalue) {
////
////				if (!itemDetailTable.getItems().isEmpty())
////					itemDetailTable.getItems().clear();
////
////				if ((null != newvalue) && null != salesTransHdr)
////					salesTransHdr.setSalesManId(newvalue);
////				String tableName = "";
////
////				try {
////					tableName = servingtable.getSelectionModel().getSelectedItem().toString();
////				} catch (Exception e) {
////					System.out.println(e.toString());
////				}
////				String salesMan = newvalue;
////				//
////
////				try {
////					ResponseEntity<List<SalesTransHdr>> salesTransHdrResponse = RestCaller.getInProgressKOT(salesMan,
////							tableName);
////					List<SalesTransHdr> salesTransHdrKOT = salesTransHdrResponse.getBody();
////					System.out.println(salesTransHdrResponse.toString());
////
////					int hdrSize = salesTransHdrKOT.size();
////					if (salesTransHdrKOT.size() == 0) {
////						// Start new KOT
////						startNewKOT();
////
////					} else {
////						RefreshTable(salesTransHdrKOT.get(0));
////					}
////					FillTable();
////				} catch (Exception e) {
////					System.out.println(e.toString());
////					e.printStackTrace();
////					System.out.println(e.getStackTrace());
////
////				}
////			}
////		});
////
////		// --------
////
////		servingtable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
////			public void changed(ObservableValue<? extends String> ov, final String oldvalue, final String t1) {
////				/*
////				 * Call clear observable list only if it is not empty
////				 */
////				if (!saleDtlObservableList.isEmpty())
////					saleDtlObservableList.clear();
////
////				if ((null != t1) && null != salesTransHdr)
////					salesTransHdr.setServingTableName(t1);
////				String salesMan = "";
////				try {
////					salesMan = salesman.getSelectionModel().getSelectedItem().toString();
////				} catch (Exception e) {
////
////				}
////				String tableName = t1;
////				//
////				try {
////					ResponseEntity<List<SalesTransHdr>> salesTransHdrResponse = RestCaller.getInProgressKOT(salesMan,
////							tableName);
////					List<SalesTransHdr> salesTransHdrKOT = salesTransHdrResponse.getBody();
////					System.out.println(salesTransHdrResponse.toString());
////
////					if (salesTransHdrKOT.size() == 0) {
////						// Start new KOT
////						startNewKOT();
////
////					} else {
////						RefreshTable(salesTransHdrKOT.get(0));
////						getAllCardAmount();
////					}
////					FillTable();
////				} catch (Exception e) {
////					System.out.println(e.toString());
////					e.printStackTrace();
////					System.out.println(e.getStackTrace());
////				}
////
////			}
////		});
////
////		txtQty.textProperty().addListener(new ChangeListener<String>() {
////			@Override
////			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
////				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")|| ((Double.parseDouble(newValue))>5000)) {
////					txtQty.setText(oldValue);
////				}
////			}
////		});
////
////		txtRate.textProperty().addListener(new ChangeListener<String>() {
////
////			@Override
////			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
////				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
////					txtRate.setText(oldValue);
////				}
////			}
////		});
////
//////		txtTax.textProperty().addListener(new ChangeListener<String>() {
//////
//////			@Override
//////			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//////				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
//////					txtTax.setText(oldValue);
//////				}
//////			}
//////		});
////		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {
////
////			@Override
////			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
////				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
////					txtPaidamount.setText(oldValue);
////				}
////			}
////		});
////
////		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {
////
////			@Override
////			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
////				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
////					txtCashtopay.setText(oldValue);
////				}
////			}
////		});
////
////		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {
////
////			@Override
////			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
////				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
////					txtcardAmount.setText(oldValue);
////				}
////			}
////		});
////
////		cardAmountLis.addListener(new ChangeListener() {
////
////			@Override
////			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
////
////				if ((txtcardAmount.getText().length() >= 0)) {
////
////					double paidAmount = 0.0;
////					double cashToPay = 0.0;
////					double cardPaid = 0.0;
////					double sodexoAmt = 0.0;
////
////					try {
////						cashToPay = Double.parseDouble(txtCashtopay.getText());
////					} catch (Exception e) {
////						cashToPay = 0.0;
////
////					}
////
////					try {
////						paidAmount = Double.parseDouble(txtPaidamount.getText());
////					} catch (Exception e) {
////						paidAmount = 0.0;
////					}
////
////					try {
////						cardPaid = Double.parseDouble((String) newValue);
////					} catch (Exception e) {
////						cardPaid = 0.0;
////					}
////
////					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
////					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
////					changeAmtProperty.set(newrate.toPlainString());
////					if (newrate.doubleValue() < 0) {
////
////						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
////					} else {
////
////						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
////					}
////
////				}
////			}
////		});
////
////		paidAmtProperty.addListener(new ChangeListener() {
////
////			@Override
////			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
////
////				if ((txtPaidamount.getText().length() >= 0)) {
////
////					double paidAmount = 0.0;
////					double cashToPay = 0.0;
////					double cardPaid = 0.0;
////
////					try {
////						cashToPay = Double.parseDouble(txtCashtopay.getText());
////					} catch (Exception e) {
////						cashToPay = 0.0;
////					}
////
////					try {
////						cardPaid = Double.parseDouble(txtcardAmount.getText());
////					} catch (Exception e) {
////						cardPaid = 0.0;
////					}
////
////					try {
////						paidAmount = Double.parseDouble((String) newValue);
////					} catch (Exception e) {
////						paidAmount = 0.0;
////					}
////
////					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid) - cashToPay);
////					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
////					changeAmtProperty.set(newrate.toPlainString());
////
////					if (newrate.doubleValue() < 0) {
////
////						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
////					} else {
////
////						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
////					}
////
////				}
////
////				if (((String) newValue).length() == 0) {
////					double paidAmount = 0.0;
////					double cashToPay = 0.0;
////					double cardPaid = 0.0;
////					double sodexoAmt = 0.0;
////
////					try {
////						cashToPay = Double.parseDouble(txtCashtopay.getText());
////					} catch (Exception e) {
////						cashToPay = 0.0;
////					}
////
////					try {
////						cardPaid = Double.parseDouble(txtcardAmount.getText());
////					} catch (Exception e) {
////						cardPaid = 0.0;
////					}
////
////					try {
////						paidAmount = Double.parseDouble((String) newValue);
////					} catch (Exception e) {
////						paidAmount = 0.0;
////					}
////
////					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
////					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
////					changeAmtProperty.set(newrate.toPlainString());
////				}
////			}
////		});
////		tblTableWaiter.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
////			//---------------------version 6.6  surya 
////	    	salesDtl = null;
////	    	//---------------------version 6.6  surya end
////			if (newSelection != null) {
////				String tableName = newSelection.getTableName();
////				String Waiter = newSelection.getWaiterName();
////
////				for (int i = 0; i < salesman.getItems().size(); i++) {
////					if (Waiter.equalsIgnoreCase(salesman.getItems().get(i))) {
////						salesman.getSelectionModel().select(i);
////						salesman.getFocusModel().focus(i);
////						salesman.scrollTo(i);
////					}
////				}
////				for (int i = 0; i < servingtable.getItems().size(); i++) {
////					if (tableName.equalsIgnoreCase(servingtable.getItems().get(i))) {
////						servingtable.getSelectionModel().select(i);
////						servingtable.getFocusModel().focus(i);
////						servingtable.scrollTo(i);
////					}
//////				servingtable.getSelectionModel().select(Waiter);
////				}
////			}
////			ResponseEntity<List<SalesTransHdr>> salesTransHdrResponse = RestCaller
////					.getInProgressKOT(newSelection.getWaiterName(), newSelection.getTableName());
////			List<SalesTransHdr> salesTransHdrKOT = salesTransHdrResponse.getBody();
////			System.out.println(salesTransHdrResponse.toString());
////
////			if (salesTransHdrKOT.size() == 0) {
////				// Start new KOT
////				startNewKOT();
////
////			} else {
////				RefreshTable(salesTransHdrKOT.get(0));
////				getAllCardAmount();
////			}
////			FillTable();
////			
////
////		});
////		tblCardAmountDetails.getSelectionModel().selectedItemProperty()
////				.addListener((obs, oldSelection, newSelection) -> {
////					if (newSelection != null) {
////						if (null != newSelection.getId()) {
////
////							salesReceipts = new SalesReceipts();
////							salesReceipts.setId(newSelection.getId());
////						}
////					}
////				});
////		/*
////		 * cardAmountLis.addListener(new ChangeListener() {
////		 * 
////		 * @Override public void changed(ObservableValue observable, Object oldValue,
////		 * Object newValue) { if (((String) newValue).length() == 0) return; if
////		 * ((txtPaidamount.getText().length() > 0)) {
////		 * 
////		 * double paidAmount = Double.parseDouble(txtPaidamount.getText()); double
////		 * cashToPay = Double.parseDouble(txtCashtopay.getText()); double cardAmt =
////		 * Double.parseDouble((String) newValue); if (cardAmt > 0) { BigDecimal newrate
////		 * = new BigDecimal((paidAmount + cardAmt )-cashToPay); newrate =
////		 * newrate.setScale(3, BigDecimal.ROUND_CEILING);
////		 * changeAmtProperty.set(newrate.toPlainString());
////		 * 
////		 * } }else { double cashToPay = Double.parseDouble(txtCashtopay.getText());
////		 * double cardAmount = Double.parseDouble((String) newValue); BigDecimal newrate
////		 * = new BigDecimal((cardAmount )-cashToPay); newrate = newrate.setScale(3,
////		 * BigDecimal.ROUND_CEILING); changeAmtProperty.set(newrate.toPlainString());
////		 * 
////		 * } } });
////		 * 
////		 * paidAmtProperty.addListener(new ChangeListener() {
////		 * 
////		 * @Override public void changed(ObservableValue observable, Object oldValue,
////		 * Object newValue) { if (((String) newValue).length() == 0) return; if
////		 * ((txtcardAmount.getText().length() > 0)) {
////		 * 
////		 * double cardAmt = Double.parseDouble(txtcardAmount.getText()); double
////		 * cashToPay = Double.parseDouble(txtCashtopay.getText()); double paidAmount =
////		 * Double.parseDouble((String) newValue); if (cardAmt > 0) { BigDecimal newrate
////		 * = new BigDecimal((paidAmount + cardAmt )-cashToPay); newrate =
////		 * newrate.setScale(3, BigDecimal.ROUND_CEILING);
////		 * changeAmtProperty.set(newrate.toPlainString());
////		 * 
////		 * } }else { double cashToPay = Double.parseDouble(txtCashtopay.getText());
////		 * double paidAmt = Double.parseDouble((String) newValue); BigDecimal newrate =
////		 * new BigDecimal((paidAmt )-cashToPay); newrate = newrate.setScale(3,
////		 * BigDecimal.ROUND_CEILING); changeAmtProperty.set(newrate.toPlainString());
////		 * 
////		 * } } });
////		 */
////		eventBus.register(this);
////
////		// btnSave.setDisable(true);
////		itemDetailTable.setItems(saleDtlObservableList);
////
////		ResponseEntity<List<TableWaiterReport>> tablewaiter = RestCaller.getAllocatedTable(SystemSetting.systemBranch);
////		tableWaiterList = FXCollections.observableArrayList(tablewaiter.getBody());
////		tblTableWaiter.setItems(tableWaiterList);
////		clTable.setCellValueFactory(cellData -> cellData.getValue().gettableNameProperty());
////		clWaiter.setCellValueFactory(cellData -> cellData.getValue().getwaiterNameProperty());
////		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
////			if (newSelection != null) {
////				if (null != newSelection.getId()) {
////					salesDtl = new SalesDtl();
////					salesDtl.setId(newSelection.getId());
////					txtBarcode.setText(newSelection.getBarcode());
////					txtItemname.setText(newSelection.getItemName());
////					txtBatch.setText(newSelection.getBatchCode());
////					txtQty.setText(String.valueOf(newSelection.getQty()));
////					txtRate.setText(String.valueOf(newSelection.getMrp()));
//////					txtTax.setText(String.valueOf(newSelection.getTaxRate()));
////					cmbUnit.getItems().clear();
////					cmbUnit.getItems().add(newSelection.getUnitName());
////					cmbUnit.getSelectionModel().select(0);
////
////				}
////			}
////		});
////		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
////			@Override
////			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
////				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
////				ResponseEntity<UnitMst> getUnit = RestCaller
////						.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
//////				cmbUnit.setValue(cmbUnit.getSelectionModel().getSelectedItem());
////				ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller
////						.getMultiUnitbyprimaryunit(getItem.getBody().getId(), getUnit.getBody().getId());
////				if (null != getMultiUnit.getBody()) {
////					txtRate.setText(Double.toString(getMultiUnit.getBody().getPrice()));
////				} else
////					txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));
////			}
////		});
////		logger.info("KOTPOS ======== initialization completed");
//////		txtBarcode.requestFocus();
////	}
////
////	private void getAllCardAmount() {
////
////		tblCardAmountDetails.getItems().clear();
////		ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
////				.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
////		salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());
////
////		try {
////			cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
////
////		} catch (Exception e) {
////
////			txtcardAmount.setText("");
////		}
////
////		filltableCardAmount();
////	}
////	@FXML
////    void actionClearSelection(ActionEvent event) 
////	{
////		tblTableWaiter.getSelectionModel().clearSelection();
////		salesman.getSelectionModel().clearSelection();
////		servingtable.getSelectionModel().clearSelection();
////		itemDetailTable.getItems().clear();
////		salesTransHdr=null;
////		saleListItemTable.clear();
////		saleDtlObservableList.clear();
////    }
////	@FXML
////	void tableOnClick(MouseEvent event) {
////		//---------------------version 6.6  surya 
////    	salesDtl = null;
////    	//---------------------version 6.6  surya end
////		if (!saleDtlObservableList.isEmpty())
////			saleDtlObservableList.clear();
////
////		if ((null != servingtable.getSelectionModel().getSelectedItem()) && null != salesTransHdr)
////			salesTransHdr.setServingTableName(servingtable.getSelectionModel().getSelectedItem());
////		String salesMan = "";
////		try {
////			salesMan = salesman.getSelectionModel().getSelectedItem().toString();
////		} catch (Exception e) {
////
////		}
////		String tableName = servingtable.getSelectionModel().getSelectedItem();
////		//
////		try {
////			ResponseEntity<List<SalesTransHdr>> salesTransHdrResponse = RestCaller.getInProgressKOT(salesMan,
////					tableName);
////			List<SalesTransHdr> salesTransHdrKOT = salesTransHdrResponse.getBody();
////			System.out.println(salesTransHdrResponse.toString());
////
////			if (salesTransHdrKOT.size() == 0) {
////				// Start new KOT
////				startNewKOT();
////
////			} else {
////				RefreshTable(salesTransHdrKOT.get(0));
////				getAllCardAmount();
////			}
////			FillTable();
////		} catch (Exception e) {
////			System.out.println(e.toString());
////			e.printStackTrace();
////			System.out.println(e.getStackTrace());
////		}
////
////	}
////
////	@FXML
////	void waiterOnClick(MouseEvent event) {
////		//---------------------version 6.6  surya 
////    	salesDtl = null;
////    	//---------------------version 6.6  surya end
////		if (!itemDetailTable.getItems().isEmpty())
////			itemDetailTable.getItems().clear();
////
////		if ((null != salesman.getSelectionModel().getSelectedItem()) && null != salesTransHdr)
////			salesTransHdr.setSalesManId(salesman.getSelectionModel().getSelectedItem());
////		String tableName = "";
////
////		try {
////			tableName = servingtable.getSelectionModel().getSelectedItem().toString();
////		} catch (Exception e) {
////			System.out.println(e.toString());
////		}
////		String salesMan = salesman.getSelectionModel().getSelectedItem();
////		//
////
////		try {
////			ResponseEntity<List<SalesTransHdr>> salesTransHdrResponse = RestCaller.getInProgressKOT(salesMan,
////					tableName);
////			List<SalesTransHdr> salesTransHdrKOT = salesTransHdrResponse.getBody();
////			System.out.println(salesTransHdrResponse.toString());
////
////			int hdrSize = salesTransHdrKOT.size();
////			if (salesTransHdrKOT.size() == 0) {
////				// Start new KOT
////				startNewKOT();
////
////			} else {
////				RefreshTable(salesTransHdrKOT.get(0));
////				getAllCardAmount();
////			}
////			FillTable();
////		} catch (Exception e) {
////			System.out.println(e.toString());
////			e.printStackTrace();
////			System.out.println(e.getStackTrace());
////
////		}
////	}
////
////	@FXML
////	void deleteRow(ActionEvent event) {
////		logger.info("KOTPOS ========DELETE ITEM STARTED");
//////		Alert a = new Alert(AlertType.CONFIRMATION);
//////		a.setHeaderText("Delete Item");
//////		a.setContentText("Are you sure you want to Delete Item");
//////		a.showAndWait().ifPresent((btnType) -> {
//////			if (btnType == ButtonType.OK) {
////				
////			
////		if (null != salesDtl.getId()) {
////			RestCaller.deleteSalesDtl(salesDtl.getId());
////			salesDtl = new SalesDtl();
////			itemDetailTable.getItems().clear();
////			ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
////			saleDtlObservableList = FXCollections.observableArrayList(SalesDtlResponse.getBody());
//////			itemDetailTable.setItems(saleDtlObservableList);
////			notifyMessage(1, " Item Deleted Successfully");
////			ResponseEntity<List<TableWaiterReport>> tablewaiter = RestCaller
////					.getAllocatedTable(SystemSetting.systemBranch);
////			tableWaiterList = FXCollections.observableArrayList(tablewaiter.getBody());
////			tblTableWaiter.setItems(tableWaiterList);
////			txtItemname.clear();
////			txtBarcode.clear();
////			txtBatch.clear();
//////			txtTax.clear();
////			txtRate.clear();
////			txtQty.clear();
////			logger.info("KOTPOS ======== DELETE ITEM FINISHED");
//////			Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
//////
//////			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
//////			amountTotal = amountTotal + salesTransHdr.getCashPaidSale();
//////			txtCashtopay.setText(summary.getTotalAmount() + "");
//////			lblpaidAmount.setVisible(true);
//////			lblpaidAmount.setText(txtCashtopay.getText());
//////			 salesTransHdr.setCashPaidSale(1000.0);
//////
//////			if (null != salesDtl && null != saleDtlObservableList) {
//////				saleDtlObservableList.add(salesDtl);
//////			}
////
////			FillTable();
////		}
//////			} else if (btnType == ButtonType.CANCEL) {
//////
//////				return;
//////
//////			}
//////		});
//////		
////
////	}
////
////	@FXML
////	void hold(ActionEvent event) {
////
////	}
////
////	@FXML
////	void EnterItemName(KeyEvent event) {
////		if (event.getCode() == KeyCode.ENTER) {
////
////			txtQty.requestFocus();
////		}
////		if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////
////	}
////    @FXML
////    void onTableKeyPress(KeyEvent event) {
////    	//---------------------version 6.6  surya 
////    	salesDtl = null;
////    	//---------------------version 6.6  surya end
////		if (event.getCode() == KeyCode.ENTER) {
////
////			salesman.requestFocus();
////		}
////		if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////		
////    }
////    @FXML
////    void onWaiterOnEnter(KeyEvent event) {
////    	//---------------------version 6.6  surya 
////    	salesDtl = null;
////    	//---------------------version 6.6  surya end
////
////
////    	if (event.getCode() == KeyCode.ENTER) {
////
////			showPopup();
////		}
////    	if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////    }
////    @FXML
////    void tblOnEnter(KeyEvent event) {
////    	if (event.getCode() == KeyCode.ESCAPE) {
////
////			txtPaidamount.requestFocus();
////		}
////    	if (event.getCode() == KeyCode.ENTER) {
////
////			txtItemname.requestFocus();
////		}
////    	if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////    }
////
////	@FXML
////	void onClickBarcodeBack(KeyEvent event) {
////		if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////		if (event.getCode() == KeyCode.BACK_SPACE) {
////
////			txtItemname.requestFocus();
////			showPopup();
////		}
////
//////		else if (event.getCode() == KeyCode.LEFT) {
//////
//////			txtItemname.requestFocus();
//////			showPopup();
//////		}
////		if (event.getCode() == KeyCode.ENTER) {
////			if (txtBarcode.getText().trim().isEmpty()) {
////				txtPaidamount.requestFocus();
////			} else {
////
////				txtQty.setText("1");
////
////				if (null == txtBarcode) {
////					return;
////
////				}
////				if (null == txtBarcode.getText()) {
////					return;
////
////				}
////				String subBarcode = null;
////				if (txtBarcode.getText().trim().length() >= 12) {
////					String barcode1 = txtBarcode.getText();
////					subBarcode = barcode1.substring(0, 7);
////				}
////				ArrayList items = new ArrayList();
////
////				items = RestCaller.SearchStockItemByBarcode(txtBarcode.getText());
////				if (txtBarcode.getText().startsWith("21")) {
////					if (null == subBarcode) {
////						subBarcode = txtBarcode.getText();
////					}
////					ResponseEntity<WeighingItemMst> weighingItems = RestCaller.getWeighingItemMstByBarcode(subBarcode);
////					if (null != weighingItems.getBody()) {
////						items = RestCaller.SearchStockItemByBarcode(subBarcode);
////						if (txtBarcode.getText().trim().length() > 7) {
////							txtQty.setText(
////									txtBarcode.getText().substring(7, 9) + "." + txtBarcode.getText().substring(9, 12));
////						}
////						txtBarcode.setText(subBarcode);
////					}
////				}
////			
////				if (items.size() > 1) {
////					multi = true;
////					showPopupByBarcode();
////				} else {
////					multi = false;
////					String itemName = "";
////					String barcode = "";
////					Double mrp = 0.0;
////					String unitname = "";
//////					Double qty = 0.0;
//////					Double cess = 0.0;
////					Double tax = 0.0;
////					String itemId = "";
////					String unitId = "";
////					Iterator itr = items.iterator();
////					String batch = "";
////
////					while (itr.hasNext()) {
////
////						List element = (List) itr.next();
////						itemName = (String) element.get(0);
////						barcode = (String) element.get(1);
////						unitname = (String) element.get(2);
////						mrp = (Double) element.get(3);
//////						qty = (Double) element.get(4);
//////						cess = (Double) element.get(5);
////						tax = (Double) element.get(6);
////						itemId = (String) element.get(7);
////						unitId = (String) element.get(8);
////						batch = (String) element.get(9);
////
////						if (null != itemName) {
////							salesDtl.setBarcode(barcode);
////							salesDtl.setUnitId(unitId);
//////							salesDtl.setUnitName(unitname);
////							salesDtl.setMrp(mrp);
//////							salesDtl.setCessRate(cess);
////							salesDtl.setTaxRate(tax);
////							salesDtl.setItemId(itemId);
////							// salesDtl.setBatchCode(batch);
////							txtItemname.setText(itemName);
////							txtBatch.setText(batch);
////							txtRate.setText(Double.toString(mrp));
//////							txtTax.setText(Double.toString(tax));
////
////							cmbUnit.setValue(unitname);
////
////							addItem();
////						}
////
////					}
////				}
////
////				txtBatch.clear();
////				txtBarcode.requestFocus();
////
////			}
////		}
////
////		else if (event.getCode() == KeyCode.DOWN) {
////			itemDetailTable.requestFocus();
////		}
////	}
////
////	@FXML
////	void toPrintChange(KeyEvent event) {
////		if(event.getCode() == KeyCode.K && event.isControlDown())
////		{
////			printKot();
////		}
////		if(event.getCode() == KeyCode.P && event.isControlDown())
////		{
////			printPerforma();
////		}
////		if (event.getCode() == KeyCode.ENTER) {
////
////			
////			// btnSave.setDisable(false);
////
////			/// Double change = Double.parseDouble(txtPaidamount.getText()) -
////			/// Double.parseDouble(txtCashtopay.getText());
////			// txtChangeamount.setText(Double.toString(change));
////
////			try {
////				finalSave();
////			} catch (SQLException e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////				return;
////			}
////
////		} else if (event.getCode() == KeyCode.LEFT) {
////
////			visibleCardSale();
////
////			cmbCardType.requestFocus();
////
////		} else if (event.getCode() == KeyCode.UP) {
////			txtQty.requestFocus();
////		} else if (event.getCode() == KeyCode.BACK_SPACE) {
////			if (txtPaidamount.getText().length() == 0) {
////				txtBarcode.requestFocus();
////			}
////
////		} else if (event.getCode() == KeyCode.S && event.isControlDown()) {
////
////			try {
////				finalSave();
////			} catch (SQLException e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
////
////		}
////
////	}
////
////	@FXML
////	void calcPaid(KeyEvent event) {
////		/*
////		 * 
////		 * if (event.getCode() == KeyCode.ENTER) { if (null == salesTransHdr) {
////		 * salesTransHdr = new SalesTransHdr(); salesTransHdr.setInvoiceAmount(0.0);
////		 * salesTransHdr.getId(); ResponseEntity<SalesTransHdr> respentity =
////		 * RestCaller.saveSalesHdr(salesTransHdr); salesTransHdr = respentity.getBody();
////		 * 
////		 * } ItemMst itemmst = new ItemMst();
////		 * 
////		 * salesDtl.setItemName(txtItemname.getText());
////		 * salesDtl.setBarCode(txtBarcode.getText());
////		 * itemmst.setBarCode(txtBarcode.getText()); ResponseEntity<List<ItemMst>>
////		 * respsentity = RestCaller.getSalesbarcode(itemmst); // itemmst =
////		 * respenstity.getBody();
////		 * 
////		 * // salesDtl.setRate(Double.parseDouble(txtRate.getText())); Double qty =
////		 * Double.parseDouble(txtQty.getText()); salesDtl.setQty(qty);
////		 * 
////		 * // salesDtl.setUnit(choiceUnit.getStyle());
////		 * salesDtl.setItemCode(txtItemcode.getText()); Double eRate = 00.0; eRate =
////		 * Double.parseDouble(txtRate.getText()); salesDtl.setRate(eRate); Double TaRate
////		 * = 00.0; TaRate = Double.parseDouble(txtTax.getText());
////		 * salesDtl.setTaxRate(TaRate);
////		 * 
////		 * salesDtl.setSalesTransHdr(salesTransHdr);
////		 * 
////		 * ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
////		 * salesDtl = respentity.getBody(); Double amount =
////		 * Double.parseDouble(txtQty.getText()) * Double.parseDouble(txtRate.getText())
////		 * + Double.parseDouble(txtTax.getText());
////		 * salesTransHdr.setCashPaidSale(amount); // salesDtl.setTempAmount(amount);
////		 * saleListTable.add(salesDtl);
////		 * 
////		 * FillTable();
////		 * 
////		 * amountTotal = amountTotal + salesTransHdr.getCashPaidSale();
////		 * txtCashtopay.setText(Double.toString(amountTotal)); txtItemname.setText("");
////		 * txtBarcode.setText(""); txtQty.setText(""); txtTax.setText("");
////		 * txtRate.setText("");
////		 * 
////		 * txtItemcode.setText("");
////		 * 
////		 * }
////		 * 
////		 * if (event.getCode() == KeyCode.BACK_SPACE) {
////		 * 
////		 * txtBarcode.requestFocus();
////		 * 
////		 * }
////		 * 
////		 */}
////
////	@FXML
////	void unHold(ActionEvent event) {
////
////	}
////
////	private void FillTable() {
////
////		itemDetailTable.setItems(saleDtlObservableList);
////		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
////		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
////		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
////		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
////		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
////		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
////		columnCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
////		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
////		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
////		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
////		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
////
//////		salesTransHdr.setCashPaidSale(amount);
//////			salespos.setTempAmount(amount);
////		// saleListTable.add(salespos);
////		if (null != summary.getTotalAmount()) {
////
////			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
////			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
////			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
////
////			txtCashtopay.setText(bdCashToPay.toPlainString());
////		} else {
////			txtCashtopay.setText("");
////		}
////
////	}
////
////	@FXML
////	void addItemButtonClick(ActionEvent event) {
////		addItem();
////	}
////
////	private synchronized void startNewKOT() {
////		salesTransHdr = new SalesTransHdr();
////
////		ResponseEntity<CustomerMst> customerMstResponse = RestCaller.getCustomerByNameAndBarchCode("KOT",
////				SystemSetting.systemBranch);
////		CustomerMst customerMst = customerMstResponse.getBody();
////
////		// listAllocated.getItems().add(servingtable.getSelectionModel().getSelectedItem().toString());
////
////		salesTransHdr.setCustomerMst(customerMst);
////
////		salesTransHdr.setInvoiceAmount(0.0);
////		salesTransHdr.getId();
////		salesTransHdr.setBranchCode(SystemSetting.systemBranch);
////		salesTransHdr.setCustomerId(customerMst.getId());
////		salesTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
////		// salesTransHdr.setCustomerId(custId);
////		salesTransHdr.setCreditOrCash("CASH");
////		salesTransHdr.setIsBranchSales("N");
////		salesTransHdr.setSalesMode("ATLIER-KOT");
////		salesTransHdr.setUserId(SystemSetting.getUserId());
////		String servingTable = "";
////		try {
////			servingTable = servingtable.getSelectionModel().getSelectedItem().toString();
////		} catch (Exception e) {
////
////		}
////		if (servingTable.length() == 0) {
////			return;
////		}
////		salesTransHdr.setServingTableName(servingTable);
////
////		String salesmanId = "";
////		try {
////			salesmanId = salesman.getSelectionModel().getSelectedItem().toString();
////		} catch (Exception e) {
////
////		}
////		if (salesmanId.trim().length() == 0) {
////			return;
////		}
////		salesTransHdr.setSalesManId(salesmanId);
////
////		ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
////		salesTransHdr = respentity.getBody();
////		saveTableOccupiedMst(servingTable);
////	}
////
////	private void addItem() {
////		if (SystemSetting.systemBranch.equalsIgnoreCase(null)) {
////			notifyMessage(5, "Incorrect Branch");
////			return;
////		}
////		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
////		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
////		System.out.println("Sys Date before add" + SystemSetting.systemDate);
////		if (null != dayEndClosureHdr) {
////			if (null != dayEndClosureHdr.getDayEndStatus()) {
////				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
////				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
////				java.sql.Date prDate = Date.valueOf(process_date);
////				Date sDate = Date.valueOf(sysdate);
////				int i = prDate.compareTo(sDate);
////				if (i > 0 || i == 0) {
////					notifyMessage(1, " Day End Already Done for this Date !!!!");
////					return;
////				}
////			}
////		}
////
////		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);
////
////		if (!dayendtodone) {
////			notifyMessage(1, "Day End should be done before changing the date !!!!");
////			return;
////		}
////
////		if (servingtable.getSelectionModel().isEmpty()) {
////			notifyMessage(1, "Please Select Table!!!");
////			return;
////
////		} else if (salesman.getSelectionModel().isEmpty()) {
////			notifyMessage(1, "Please Select Salesman!!!");
////			return;
////		}
////
////		else if (txtItemname.getText().trim().isEmpty()) {
////			notifyMessage(1, "Please Select Item Name!!!");
////
////			txtItemname.requestFocus();
////			return;
////		} else if (txtQty.getText().trim().isEmpty()) {
////			notifyMessage(1, "Please Type Quantity!!!");
////			txtQty.requestFocus();
////			return;
////		} else if (txtBatch.getText().trim().isEmpty()) {
////			notifyMessage(1, "Item Batch is not present!!!");
////			txtQty.requestFocus();
////			return;
////		} else {
////			ResponseEntity<UnitMst> getUnitBYItem = RestCaller
////					.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
////
////			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
////
////			ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
////					getUnitBYItem.getBody().getId());
////			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
////				if (null == getmulti.getBody()) {
////					notifyMessage(5, "Please Add the item in Multi Unit");
////					return;
////				}
////			}
////
////			ArrayList items = new ArrayList();
////
////			items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());
////			Double chkQty = 0.0;
////			chkQty = RestCaller.getQtyFromItemBatchDtlDailyByItemIdAndQty(getItem.getBody().getId(), txtBatch.getText());
//////-----------------------------------sharon ------------------------------
////					chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(), txtBatch.getText());
////			String itemId = null;
////			Iterator itr = items.iterator();
////			while (itr.hasNext()) {
////				List element = (List) itr.next();
//////				chkQty = (Double) element.get(4);
////				itemId = (String) element.get(7);
////			}
////
////			ResponseEntity<KitDefinitionMst> getKitMst = RestCaller.getKitByItemId(getItem.getBody().getId());
////			if (null == getKitMst.getBody()) {
////				if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
////					Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
////							getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
////							Double.parseDouble(txtQty.getText()));
////					System.out.println(conversionQty);
////					if ((chkQty < conversionQty) && !SystemSetting.isNEGATIVEBILLING()) {
////						notifyMessage(1, "Not in Stock!!!");
////						txtQty.clear();
////						txtItemname.clear();
////						txtBarcode.clear();
////						txtBatch.clear();
////						txtRate.clear();
//////					txtTax.clear();
////						txtBarcode.requestFocus();
////						return;
////					}
////				}
////
////				else if ((chkQty < Double.parseDouble(txtQty.getText())) && !SystemSetting.isNEGATIVEBILLING()) {
////					notifyMessage(1, "Not in Stock!!!");
////					txtQty.clear();
////					txtItemname.clear();
////					txtBarcode.clear();
////					txtBatch.clear();
////					txtRate.clear();
//////				txtTax.clear();
////					txtBarcode.requestFocus();
////					return;
////				}
////			}
////			if (null == salesTransHdr || null == salesTransHdr.getId()) {
////				salesTransHdr = new SalesTransHdr();
////
////				ResponseEntity<CustomerMst> customerMstResponse = RestCaller.getCustomerByNameAndBarchCode("KOT",
////						SystemSetting.systemBranch);
////				CustomerMst customerMst = customerMstResponse.getBody();
////
////				salesTransHdr.setCustomerMst(customerMst);
////				salesTransHdr.setUserId(SystemSetting.getUserId());
////				salesTransHdr.setSalesMode("KOT");
////				salesTransHdr.setInvoiceAmount(0.0);
////				salesTransHdr.getId();
////				salesTransHdr.setUserId(SystemSetting.getUserId());
////				salesTransHdr.setBranchCode(SystemSetting.getSystemBranch());
////				salesTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
////				salesTransHdr.setCustomerId(customerMst.getId());
////				salesTransHdr.setCreditOrCash("CASH");
////				salesTransHdr.setIsBranchSales("N");
////				String servingTable = "";
////
////				servingTable = servingtable.getSelectionModel().getSelectedItem().toString();
////
////				if (servingTable.length() == 0) {
////					return;
////				}
////				salesTransHdr.setServingTableName(servingTable);
////				// listAllocated.getItems().add(servingtable.getSelectionModel().getSelectedItem().toString());
////
////				String salesmanId = "";
////
////				salesmanId = salesman.getSelectionModel().getSelectedItem().toString();
////
////				if (salesmanId.length() == 0) {
////					return;
////				}
////				salesTransHdr.setSalesManId(salesmanId);
////
////				ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
////				salesTransHdr = respentity.getBody();
////				saveTableOccupiedMst(servingTable);
////			}
////
////			if (null != salesDtl && null != salesDtl.getId()) {
////				System.out.println("toDeleteSale.getId()" + salesDtl.getId());
////				Alert a = new Alert(AlertType.CONFIRMATION);
////				a.setHeaderText("Delete Item");
////				a.setContentText("Are you sure you want to Delete Item");
////				a.showAndWait().ifPresent((btnType) -> {
////					if (btnType == ButtonType.OK) {
////				RestCaller.deleteSalesDtl(salesDtl.getId());
////					}
////				     else if (btnType == ButtonType.CANCEL) {
////
////						return;
////
////					}
////				});
////			}
////			if (null == getKitMst.getBody()) {
////				Double itemsqty = 0.0;
////				ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
////						getItem.getBody().getId(), txtBatch.getText());
////				saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
////				if (saleListItemTable.size() > 1) {
////					Double PrevQty = 0.0;
////					for (SalesDtl saleDtl : saleListItemTable) {
////						if (!saleDtl.getUnitId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
////							PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
////									getItem.getBody().getUnitId(), saleDtl.getQty());
////						} else {
////							PrevQty = saleDtl.getQty();
////						}
////						itemsqty = itemsqty + PrevQty;
////					}
////				} else {
////					itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId, txtBatch.getText());
////				}
////				if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
////					Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
////							getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
////							Double.parseDouble(txtQty.getText()));
////					System.out.println(conversionQty);
////					if ((chkQty < itemsqty + conversionQty) && !SystemSetting.isNEGATIVEBILLING()) {
////						notifyMessage(1, "Not in Stock!!!");
////						txtQty.clear();
////						txtItemname.clear();
////						txtBarcode.clear();
////						txtBatch.clear();
////						txtRate.clear();
//////					txtTax.clear();
////						txtBarcode.requestFocus();
////						return;
////					}
////				} else if ((chkQty < itemsqty + Double.parseDouble(txtQty.getText()))
////						&& !SystemSetting.isNEGATIVEBILLING()) {
////					txtQty.clear();
////					txtItemname.clear();
////					txtBarcode.clear();
////					txtBatch.clear();
////					txtRate.clear();
//////				txtTax.clear();
////					txtBarcode.requestFocus();
////					notifyMessage(1, "No Stock!!");
////					return;
////				}
////			}
////			salesDtl = new SalesDtl();
////			salesDtl.setSalesTransHdr(salesTransHdr);
////			salesDtl.setItemName(txtItemname.getText());
////			salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());
////
////			String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();
////			System.out.println(batch);
////			salesDtl.setBatchCode(batch);
////			Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());
////
////			salesDtl.setQty(qty);
////
////			// Double qty = txtQty.getText().length() == 0 ? 0 :
////			// Double.parseDouble(txtQty.getText());
////			// salesDtl.setQty(qty);
////
////			Double mrpRateIncludingTax = 00.0;
////			ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("PosDiscount");
////			if(null != getParam.getBody())
////			{
////				if(getParam.getBody().getValue().equalsIgnoreCase("YES"))
////				{
////					if(!txtposDiscount.getText().trim().isEmpty())
////					{
////						calculateDiscount();
////					}
////				}
////			}
////			mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
////			
////			
////			salesDtl.setMrp(mrpRateIncludingTax);
////			Double taxRate = 00.0;
//////				if(null != unitMst)
//////					salesDtl.setUnitName(unitMst.getUnitName());
////			ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst
////																												// =
////			ItemMst item = respsentity.getBody();
////			salesDtl.setItemCode(item.getItemCode());
////			salesDtl.setBarcode(item.getBarCode());
////			salesDtl.setStandardPrice(item.getStandardPrice());
////			// taxRate = Double.parseDouble(txtTax.getText());
////
////			salesDtl.setPrintKotStaus("N");
////			if (null != item.getTaxRate()) {
////				salesDtl.setTaxRate(item.getTaxRate());
////				taxRate = item.getTaxRate();
////
////			} else {
////				salesDtl.setTaxRate(0.0);
////
////			}
////			/*
////			 * Calculate Rate Before Tax
////			 */
////
////			salesDtl.setItemId(item.getId());
////			if (null == cmbUnit.getValue()) {
////				ResponseEntity<UnitMst> getUnit = RestCaller
////						.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
////				salesDtl.setUnitId(getUnit.getBody().getId());
////				salesDtl.setUnitName(getUnit.getBody().getUnitName());
////			} else {
////				ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
////				salesDtl.setUnitId(getUnit.getBody().getId());
////				salesDtl.setUnitName(getUnit.getBody().getUnitName());
////			}
////
////			ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
////
////			if (getTaxMst.getBody().size() > 0) {
////				for (TaxMst taxMst : getTaxMst.getBody()) {
////					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
////						salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//////							BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//////							salesDtl.setIgstAmount(igstAmount.doubleValue());
////						salesDtl.setTaxRate(taxMst.getTaxRate());
////					}
////					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
////						salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//////							BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//////							salesDtl.setCgstAmount(CgstAmount.doubleValue());
////					}
////					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
////						salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//////							BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//////							salesDtl.setSgstAmount(SgstAmount.doubleValue());
////					}
////					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
////						salesDtl.setCessRate(taxMst.getTaxRate());
//////							BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//////							salesDtl.setCessAmount(cessAmount.doubleValue());
////					}
////					if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
////						salesDtl.setAddCessRate(taxMst.getTaxRate());
//////							BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//////							salesDtl.setAddCessAmount(cessAmount.doubleValue());
////					}
////
////				}
////				Double rateBeforeTax = (100 * mrpRateIncludingTax)
////						/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate());
////				salesDtl.setRate(rateBeforeTax);
////				BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
////				salesDtl.setIgstAmount(igstAmount.doubleValue());
////				BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
////				salesDtl.setCgstAmount(CgstAmount.doubleValue());
////				BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
////				salesDtl.setSgstAmount(SgstAmount.doubleValue());
////				BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
////				salesDtl.setCessAmount(cessAmount.doubleValue());
////				BigDecimal adcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
////				salesDtl.setAddCessAmount(adcessAmount.doubleValue());
////			} else {
////				if (null != item.getTaxRate()) {
////					salesDtl.setTaxRate(item.getTaxRate());
////					taxRate = item.getTaxRate();
////				} else {
////					salesDtl.setTaxRate(0.0);
////				}
////				Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
////				salesDtl.setRate(rateBeforeTax);
////
////				double cessRate = item.getCess();
////				double cessAmount = 0.0;
////				if (cessRate > 0) {
////
////					rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
////					salesDtl.setRate(rateBeforeTax);
////
////					cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;
////
////				}
////
////				salesDtl.setCessRate(cessRate);
////
////				salesDtl.setCessAmount(cessAmount);
////
////				salesDtl.setSgstTaxRate(taxRate / 2);
////
////				salesDtl.setCgstTaxRate(taxRate / 2);
////
////				salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);
////
////				salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);
////			}
////
////			salesDtl.setAmount(qty * mrpRateIncludingTax);
////
////			ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
////					.getPriceDefenitionMstByName("COST PRICE");
////			PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
////			if (null != priceDefenitionMst) {
////				String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
////				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
////						salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);
////
////				PriceDefinition priceDefinition = priceDefenitionResp.getBody();
////				if (null != priceDefinition) {
////					salesDtl.setCostPrice(priceDefinition.getAmount());
////				}
////			}
////			ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
////			if(null == respentity.getBody())
////			{
////				return;
////			}
////			salesDtl = respentity.getBody();
////
////			ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());
////
////			List<SalesDtl> salesDtlList = respentityList.getBody();
////
////			/*
////			 * Call Rest to get the summary and set that to the display fields
////			 */
////
////			// salesDtl.setTempAmount(amount);
////
////			saleDtlObservableList.clear();
////
////			saleDtlObservableList.setAll(salesDtlList);
////
////			FillTable();
////			/*
////			 * Call Rest to get the summary and set that to the display fields
////			 */
////
////			// txtCashtopay.setText("100");
////
////			txtItemname.setText("");
////			txtBarcode.setText("");
////			txtQty.setText("");
//////			txtTax.setText("");
////			txtRate.setText("");
////
////			txtBatch.setText("");
////			txtBarcode.requestFocus();
////
////			ResponseEntity<List<TableWaiterReport>> tablewaiter = RestCaller
////					.getAllocatedTable(SystemSetting.systemBranch);
////			tableWaiterList = FXCollections.observableArrayList(tablewaiter.getBody());
////			tblTableWaiter.setItems(tableWaiterList);
////
////			salesDtl = new SalesDtl();
////		}
////		txtItemname.requestFocus();
////		txtQty.setText("");
//////		txtTax.setText("");
////		txtRate.setText("");
////
////		txtBatch.setText("");
////	}
////
////	private void calculateDiscount() {
////			
//////			txtRate.setText(rateBeforeDiscount+"");
////			Double discount = Double.parseDouble(txtposDiscount.getText());
////			Double rate = Double.parseDouble(txtRate.getText());
////			Double discountAmount = rate*(discount/100);
////			Double rateAfterDiscount = rate-discountAmount;
////			BigDecimal bdrateAfterDiscount = new BigDecimal(rateAfterDiscount);
////			bdrateAfterDiscount = bdrateAfterDiscount.setScale(2,BigDecimal.ROUND_HALF_DOWN);
////			txtRate.setText(bdrateAfterDiscount.toPlainString());		
////		
////	}
////		
////
////	@FXML
////	void tableToDelete(MouseEvent event) {
//////		if (itemDetailTable.getSelectionModel().getSelectedItem() != null) {
//////			TableViewSelectionModel selectionModel = itemDetailTable.getSelectionModel();
//////			saleDtlObservableList = selectionModel.getSelectedItems();
//////			for (SalesDtl sale : saleDtlObservableList) {
//////				salesDtl.setId(sale.getId());
//////				txtBarcode.setText(sale.getBarcode());
//////				txtItemname.setText(sale.getItemName());
//////				txtItemcode.setText(sale.getItemCode());
//////				txtQty.setText(String.valueOf(sale.getQty()));
//////				txtRate.setText(String.valueOf(sale.getMrp()));
//////				txtTax.setText(String.valueOf(sale.getTaxRate()));
//////			}
//////		}
////
////	}
////
////	@FXML
////	void addTableWaiter(MouseEvent event) {
////
////	}
////
////	@FXML
////	void save(ActionEvent event) {
////		/*
////		 * Final Save
////		 */
////
////		try {
////			finalSave();
////		} catch (SQLException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////
////	}
////
////	private void RefreshTable(SalesTransHdr hdr) {
////		// Get List of Items by HdrId.
////		// Create the Table.
////		// Populate the table
////		// Set the header
////
////		if (!saleDtlObservableList.isEmpty())
////			saleDtlObservableList.clear();
////		;
////
////		ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(hdr);
////
////		List<SalesDtl> saleDtlList = SalesDtlResponse.getBody();
////
////		boolean hasChildRecords = false;
////		for (int i = 0; i < saleDtlList.size(); i++) {
////			SalesDtl saledtl = saleDtlList.get(i);
////			saleDtlObservableList.add(saledtl);
////			hasChildRecords = true;
////
////		}
////
////		// itemDetailTable.setItems(saleListTable);
////
////		salesTransHdr = hdr;
////		if (hasChildRecords) {
////			setSummaryOfInvoice();
////			FillTable();
////		}
////
////	}
////
////	private void setSummaryOfInvoice() {
////		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
////
////		BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
////		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
////		if (summary.getTotalAmount() > 0) {
////			txtCashtopay.setText(bdCashToPay.toPlainString());
////		} else {
////			txtCashtopay.setText("0");
////		}
////
////	}
////
////	private void showPopup() {
////		try {
////
////			if (SystemSetting.isNEGATIVEBILLING()) {
////				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
////
////				// fxmlLoader.setController(itemStockPopupCtl);
////				Parent root1;
////
////				root1 = (Parent) fxmlLoader.load();
////				Stage stage = new Stage();
////
////				stage.initModality(Modality.APPLICATION_MODAL);
////				stage.initStyle(StageStyle.UNDECORATED);
////				stage.setTitle("Stock Item");
////				stage.initModality(Modality.APPLICATION_MODAL);
////				stage.setScene(new Scene(root1));
////				stage.show();
////				txtQty.requestFocus();
////			} else {
////
////				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
////				// fxmlLoader.setController(itemStockPopupCtl);
////				Parent root1;
////
////				root1 = (Parent) fxmlLoader.load();
////				Stage stage = new Stage();
////
////				stage.initModality(Modality.APPLICATION_MODAL);
////				stage.initStyle(StageStyle.UNDECORATED);
////				stage.setTitle("Stock Item");
////				stage.initModality(Modality.APPLICATION_MODAL);
////				stage.setScene(new Scene(root1));
////				stage.show();
////				txtQty.requestFocus();
////			}
////
////		} catch (IOException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////
////	}
////
////	private void loadCustomerPopup() {
////		/*
////		 * Function to display popup window and show list of suppliers to select.
////		 */
////		try {
////			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
////			Parent root1;
////
////			root1 = (Parent) fxmlLoader.load();
////			Stage stage = new Stage();
////
////			stage.initModality(Modality.APPLICATION_MODAL);
////			stage.initStyle(StageStyle.UNDECORATED);
////			stage.setTitle("ABC");
////			stage.initModality(Modality.APPLICATION_MODAL);
////			stage.setScene(new Scene(root1));
////			stage.show();
////
////		} catch (IOException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////
////	}
////
////	private void saveTableOccupiedMst(String servingTable) {
////		ResponseEntity<TableOccupiedMst> tableOccupied = RestCaller.getTableOccupiedMstbyTable(servingTable);
////		if (null != tableOccupied.getBody()) {
////			TableOccupiedMst tableOccupiedMst = tableOccupied.getBody();
////			tableOccupiedMst.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
////			java.util.Date date = new java.util.Date();
////			long timeoccupied = date.getTime();
////			Timestamp ts = new Timestamp(timeoccupied);
////			tableOccupiedMst.setOrderedTime(ts);
////			tableOccupiedMst.setSalesTransHdr(salesTransHdr);
////			RestCaller.updateTableOccupiedMst(tableOccupiedMst);
////			TableOccupiedEvent tableOccupiedEvent = new TableOccupiedEvent();
////			tableOccupiedEvent.setOrderedTime(tableOccupiedMst.getOrderedTime());
////			tableOccupiedEvent.setTableId(servingTable);
////			tableOccupiedEvent.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
////			eventBus.post(tableOccupiedEvent);
////		} else if (null == tableOccupied.getBody()) {
////			TableOccupiedMst tableOccupiedMst = new TableOccupiedMst();
////			tableOccupiedMst.setTableId(servingTable);
////			java.util.Date date = new java.util.Date();
////			long timeoccupied = date.getTime();
////			Timestamp ts = new Timestamp(timeoccupied);
////			tableOccupiedMst.setOccupiedTime(ts);
////			tableOccupiedMst.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
////			tableOccupiedMst.setSalesTransHdr(salesTransHdr);
////			tableOccupiedMst.setOrderedTime(ts);
////			ResponseEntity<TableOccupiedMst> respentity = RestCaller.saveTableOccupiedMst(tableOccupiedMst);
////			tableOccupiedMst = respentity.getBody();
////			TableOccupiedEvent tableOccupiedEvent = new TableOccupiedEvent();
////			tableOccupiedEvent.setOrderedTime(tableOccupiedMst.getOrderedTime());
////			tableOccupiedEvent.setTableId(servingTable);
////			tableOccupiedEvent.getOccupiedTime();
////			tableOccupiedEvent.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
////			eventBus.post(tableOccupiedEvent);
////		}
////	}
////
////	@Subscribe
////	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
////		try {
////			if (null == txtItemname) {
////				return;
////			}
////
////			if (null == txtItemname.getScene()) {
////				return;
////			}
////			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
////			ItemMst item = new ItemMst();
////			item = getItem.getBody();
////
////			item.setRank(item.getRank() + 1);
////			RestCaller.updateRankItemMst(item);
////			Stage stage = (Stage) txtItemname.getScene().getWindow();
////			if (stage.isShowing()) {
////
////				Platform.runLater(new Runnable() {
////					@Override
////					public void run() {
////						if (!multi) {
////
////							if (null == salesDtl) {
////								salesDtl = new SalesDtl();
////							}
////							itemNameProperty.set(itemPopupEvent.getItemName());
////							salesDtl.setItemName(itemNameProperty.get());
////
////							if (null == itemPopupEvent.getBatch()) {
////								batchProperty.set("NOBATCH");
////							} else {
////								batchProperty.set(itemPopupEvent.getBatch());
////								// batchProperty.set(itemPopupEvent.getBatch());
////							}
////
////							if (null != itemPopupEvent.getAddCessRate()) {
////								salesDtl.setAddCessRate(itemPopupEvent.getAddCessRate());
////							} else {
////								salesDtl.setAddCessRate(0.0);
////							}
////
////							salesDtl.setBatchCode(batchProperty.get());
////
////							barcodeProperty.set(itemPopupEvent.getBarCode());
////
////							salesDtl.setBarcode(barcodeProperty.get());
////
////							salesDtl.setCessRate(itemPopupEvent.getCess());
////							cmbUnit.getItems().add(itemPopupEvent.getUnitName());
////							cmbUnit.setValue(itemPopupEvent.getUnitName());
////							salesDtl.setCgstTaxRate(itemPopupEvent.getCgstTaxRate());
////
////							if (null != itemPopupEvent.getExpiryDate()) {
////								salesDtl.setExpiryDate(itemPopupEvent.getExpiryDate());
////							}
////
////							salesDtl.setItemCode(itemPopupEvent.getItemCode());
////
////							salesDtl.setItemTaxaxId(itemPopupEvent.getItemTaxaxId());
////
////							mrpProperty.set(itemPopupEvent.getMrp() + "");
////							salesDtl.setMrp(Double.parseDouble(mrpProperty.get()));
////							salesDtl.setSgstTaxRate(itemPopupEvent.getSgstTaxRate());
////
////							taxRateProperty.set(itemPopupEvent.getTaxRate() + "");
////							salesDtl.setTaxRate(Double.parseDouble(taxRateProperty.get()));
////							ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller
////									.getMultiUnitByItemId(itemPopupEvent.getItemId());
////
////							multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
////							if (!multiUnitList.isEmpty()) {
////
////								for (MultiUnitMst multiUniMst : multiUnitList) {
////
////									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
////									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
////								}
////							}
////							ResponseEntity<TaxMst> taxMst = RestCaller
////									.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
////							if (null != taxMst.getBody()) {
//////						txtTax.setText(Double.toString(taxMst.getBody().getTaxRate()));
////							}
////
//////				salesDtl.setUnitName(itemPopupEvent.getUnitName());
//////				unitMst = new UnitMst();
//////				unitMst.setUnitName(itemPopupEvent.getUnitName());
////							System.out.println(itemPopupEvent.toString());
////							multi = false;
////						}
////
////						else if (multi) {
////							itemNameProperty.set(itemPopupEvent.getItemName());
////							salesDtl.setItemName(itemNameProperty.get());
////
////							batchProperty.set(itemPopupEvent.getBatch());
////
////							if (null != itemPopupEvent.getAddCessRate()) {
////								salesDtl.setAddCessRate(itemPopupEvent.getAddCessRate());
////							} else {
////								salesDtl.setAddCessRate(0.0);
////							}
////
////							if (null == itemPopupEvent.getBatch()) {
////								batchProperty.set("NOBATCH");
////							} else {
////								batchProperty.set(itemPopupEvent.getBatch());
////								// batchProperty.set(itemPopupEvent.getBatch());
////							}
////							cmbUnit.getItems().add(itemPopupEvent.getUnitName());
////							cmbUnit.setValue(itemPopupEvent.getUnitName());
////							barcodeProperty.set(itemPopupEvent.getBarCode());
////
////							salesDtl.setBarcode(barcodeProperty.get());
////
////							salesDtl.setCessRate(itemPopupEvent.getCess());
////
////							salesDtl.setCgstTaxRate(itemPopupEvent.getCgstTaxRate());
////
////							if (null != itemPopupEvent.getExpiryDate()) {
////								salesDtl.setExpiryDate(itemPopupEvent.getExpiryDate());
////							}
////
////							salesDtl.setItemCode(itemPopupEvent.getItemCode());
////
////							salesDtl.setItemTaxaxId(itemPopupEvent.getItemTaxaxId());
////
////							mrpProperty.set(itemPopupEvent.getMrp() + "");
////							salesDtl.setMrp(Double.parseDouble(mrpProperty.get()));
////							salesDtl.setSgstTaxRate(itemPopupEvent.getSgstTaxRate());
////
////							taxRateProperty.set(itemPopupEvent.getTaxRate() + "");
////							salesDtl.setTaxRate(Double.parseDouble(taxRateProperty.get()));
////							ResponseEntity<List<MultiUnitMst>> multiUnit1 = RestCaller
////									.getMultiUnitByItemId(itemPopupEvent.getItemId());
////
////							multiUnitList = FXCollections.observableArrayList(multiUnit1.getBody());
////							if (!multiUnitList.isEmpty()) {
////
////								for (MultiUnitMst multiUniMst : multiUnitList) {
////
////									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
////									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
////								}
////							}
////							ResponseEntity<TaxMst> taxMst = RestCaller
////									.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
////							if (null != taxMst.getBody()) {
//////						txtTax.setText(Double.toString(taxMst.getBody().getTaxRate()));
////							}
////
//////				salesDtl.setUnitName(itemPopupEvent.getUnitName());
//////				unitMst = new UnitMst();
//////				unitMst.setUnitName(itemPopupEvent.getUnitName());
////							System.out.println(itemPopupEvent.toString());
////							txtQty.setText("1");
////							addItem();
////							multi = false;
////						}
////
////					}
////				});
////
////			}
////		}
////
////		catch (Exception e) {
////			e.printStackTrace();
////		}
////
////	}
////
////	public void notifyMessage(int duration, String msg) {
////		System.out.println("OK Event Receid");
////
////		Image img = new Image("done.png");
////		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
////				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_LEFT)
////				.onAction(new EventHandler<ActionEvent>() {
////					@Override
////					public void handle(ActionEvent event) {
////						System.out.println("clicked on notification");
////					}
////				});
////		notificationBuilder.darkStyle();
////		notificationBuilder.show();
////	}
////
////	public void addTableWaiter() {
////		servingtable.getItems().clear();
////		salesman.getItems().clear();
////
////		ResponseEntity<List<AddKotTable>> tableSaved = RestCaller.getTablebyStatus();
////		addKotTableList1 = FXCollections.observableArrayList(tableSaved.getBody());
////		for (AddKotTable addkot : addKotTableList1) {
////			servingtable.getItems().add(addkot.getTableName());
////		}
////		ResponseEntity<List<AddKotWaiter>> waiterSaved = RestCaller.getWaiterbyStatus();
////		addKotWaiterList = FXCollections.observableArrayList(waiterSaved.getBody());
////		for (AddKotWaiter addkotwaiter : addKotWaiterList) {
////			salesman.getItems().add(addkotwaiter.getWaiterName());
////		}
////	}
////
////	private synchronized void finalSave() throws SQLException {
////		
////		if(null != salesTransHdr)
////		{
////		
////		//-------------------verification 5.0 Surya
////		String stockVerification = RestCaller.StockVerificationBySalesTransHdr(salesTransHdr.getId());
////		if(null != stockVerification && !SystemSetting.isNEGATIVEBILLING())
////		{
////			notifyMessage(5, stockVerification);
////			
//////			return;
////
////		} else {
////			
////		
////		//-------------------verification 5.0 Surya end
////
////		/*
////		 * Save table occupiedmst
////		 */
//////		ResponseEntity<TableOccupiedMst> tableOccupiedMstResp = RestCaller
//////				.getTableOccupiedMstBySalesHdrId(salesTransHdr.getId());
//////		if (null != tableOccupiedMstResp.getBody()) {
//////			TableOccupiedMst tableOccupiedMst = tableOccupiedMstResp.getBody();
//////			java.util.Date date = new java.util.Date();
//////			long timeoccupied = date.getTime();
//////			Timestamp ts = new Timestamp(timeoccupied);
//////			tableOccupiedMst.setBillingTime(ts);
//////			tableOccupiedMst.setInvoiceAmount(Double.parseDouble(txtCashtopay.getText()));
//////			RestCaller.updateTableOccupiedMst(tableOccupiedMst);
//////			TableOccupiedEvent tableOccupiedEvent = new TableOccupiedEvent();
//////			tableOccupiedEvent.setBillingTime(ts);
//////			tableOccupiedEvent.setInvoiceAmount(Double.parseDouble(txtCashtopay.getText()));
//////			// tableOccupiedEvent.getOccupiedTime();
//////			// tableOccupiedEvent.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
//////			eventBus.post(tableOccupiedEvent);
//////		}
////		logger.info("KOTPOS==== Final Save Started");
////		Double cashPaid = 0.0;
////		Double cardAmount = 0.0;
////		Double cashToPay = 0.0;
////		Double sodexoAmount = 0.0;
////		Double changeAmount = 0.0;
////		SalesReceipts salesReceipts = new SalesReceipts();
////
//////			if ( (txtcardAmount.getText().trim().isEmpty())) {
//////				notifyMessage(1, "Please Enter Paid Amount Or Card Amount!!!");
//////				txtPaidamount.requestFocus();
//////			}
//////
//////			else {
////
////		if (!txtcardAmount.getText().trim().isEmpty()) {
////
////			cardAmount = Double.parseDouble(txtcardAmount.getText());
////
////		}
////		try {
////			cashToPay = Double.parseDouble(txtCashtopay.getText());
////
////		} catch (Exception e) {
////
////		}
////
////		if (!txtPaidamount.getText().trim().isEmpty()) {
////			cashPaid = Double.parseDouble(txtPaidamount.getText());
////			if (cashToPay < (cardAmount + cashPaid)) {
////				cashPaid = cashToPay - cardAmount;
////			}
////
////		}
////
//////			else {
//////				notifyMessage(1, "Type Paid Amount");
//////				return;
//////			}
////
////		if (!txtChangeamount.getText().trim().isEmpty()) {
////			changeAmount = Double.parseDouble(txtChangeamount.getText());
////
////		}
////
////		if ((cashToPay) > (cashPaid + cardAmount)) {
////
////			notifyMessage(1, "Please Enter Valid Amount!!!!");
////			return;
////		}
////
////		txtChangeamount.setText("00.0");
////
//////				if (cardAmount > 0 && cardType.trim().length() == 0) {
//////					notifyFailureMessage(5, "Please select Card Type");
//////					return;
//////
//////				}
////		if (SystemSetting.systemBranch.equalsIgnoreCase(null)) {
////			notifyFailureMessage(5, "User Branch is not set");
////			return;
////		}
////
////		salesTransHdr.setBranchCode(SystemSetting.systemBranch);
//////		if(cashPaid <= cashToPay)
//////		{
//////			salesTransHdr.setCashPay(cashPaid);
//////
//////		} else {
//////			salesTransHdr.setCashPay(cashToPay);
//////		}
////
////		salesTransHdr.setCardamount(cardAmount);
////		salesTransHdr.setCashPay(cashPaid);
////		salesTransHdr.setPaidAmount(cashPaid);
////		salesTransHdr.setChangeAmount(changeAmount);
////		salesTransHdr.setSodexoAmount(sodexoAmount);
////
////		salesTransHdr.setInvoiceAmount(cashToPay);
////
////		// eventBus.post(salesTransHdr);
////
////		salesReceipts.setReceiptAmount(cashToPay);
////
//////		Date date = Date.valueOf(LocalDate.now());
//////				salesReceipts.setReceiptDate(date);
////
//////		Double cashPay = Double.parseDouble(txtCashtopay.getText());
//////		salesTransHdr.setCashPay(cashPay);
//////
//////		if (!txtPaidamount.getText().trim().isEmpty()) {
//////
//////			paidAmount = Double.parseDouble(txtPaidamount.getText());
//////			salesTransHdr.setPaidAmount(paidAmount);
//////		}
//////		if (!txtcardAmount.getText().trim().isEmpty()) {
//////
//////			cardAmount = Double.parseDouble(txtcardAmount.getText());
//////			salesTransHdr.setPaidAmount(cardAmount);
//////		}
//////		if (!txtChangeamount.getText().trim().isEmpty()) {
//////		salesTransHdr.setChangeAmount(changeAmount);
//////		}
////		// eventBus.post(salesTransHdr);
////		// Double
////		// change=Double.parseDouble(txtPaidamount.getText())-Double.parseDouble(txtCashtopay.getText());
////		// txtChangeamount.setText(Double.toString(change));
////		// btnSave.setDisable(false);
////		// btnSave.setDisable(true);
////
////		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
////		if (saledtlSaved.getBody().size() == 0) {
////			return;
////		}
////		// String financialYear =SystemSetting.getFinancialYear();
////		// String vNo = RestCaller.getVoucherNumber(financialYear + "KOT");
////		// if((Double.parseDouble(txtCashtopay.getText())) > (paidAmount+cardAmount))
////		// {
////		// salesTransHdr.setVoucherDate(SystemSetting.systemDate);
////
////		if (null == SystemSetting.systemBranch) {
////
////			notifyMessage(5, "Pleace login whith proper user!");
////
////		}
////
////		else {
////
////			ResponseEntity<AccountHeads> accountHeads = RestCaller
////					.getAccountHeadByName(SystemSetting.systemBranch + "-" + "CASH");
////
////			salesReceipts.setAccountId(accountHeads.getBody().getId());
////			// salesReceipts.setReceiptMode("CASH");
////			salesReceipts.setReceiptMode(salesTransHdr.getBranchCode() + "-" + "CASH");
//////				salesReceipts.setReceiptAmount(cashPaid);
////
////			Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
////			Double AmountTenderd = 0.0;
////			Double CashPaid = 0.0;
////
////			if (!txtPaidamount.getText().isEmpty()) {
////				AmountTenderd = Double.parseDouble(txtPaidamount.getText());
////			}
////
////			Double CardAmount = 0.0;
////
////			if (!txtcardAmount.getText().isEmpty()) {
////				CardAmount = Double.parseDouble(txtcardAmount.getText());
////
////			}
////			Double totalAmountTenderd = AmountTenderd + CardAmount;
////			if (totalAmountTenderd >= invoiceAmount) {
////				CashPaid = invoiceAmount - CardAmount;
////
////			} else if (totalAmountTenderd < invoiceAmount) {
////
////				notifyMessage(5, "Please Enter Valid Amount!!!!");
////
////				return;
////			}
////
////			salesReceipts.setReceiptAmount(CashPaid);
////
////			/*
////			 * if(Double.parseDouble(txtCashtopay.getText()) <
////			 * Double.parseDouble(txtPaidamount.getText())) {
////			 * if(txtcardAmount.getText().isEmpty())
////			 * salesReceipts.setReceiptAmount(Double.parseDouble(txtCashtopay.getText()));
////			 * else
////			 * salesReceipts.setReceiptAmount(Double.parseDouble(txtCashtopay.getText())-
////			 * Double.parseDouble(txtcardAmount.getText())); } else
////			 * if(Double.parseDouble(txtCashtopay.getText()) >
////			 * Double.parseDouble(txtPaidamount.getText())) {
////			 * if(txtcardAmount.getText().isEmpty())
////			 * salesReceipts.setReceiptAmount(Double.parseDouble(txtPaidamount.getText()));
////			 * else
////			 * salesReceipts.setReceiptAmount(Double.parseDouble(txtCashtopay.getText())-
////			 * Double.parseDouble(txtcardAmount.getText())); }
////			 */
////
////			salesTransHdr.setUserId(SystemSetting.getUserId());
////			salesReceipts.setBranchCode(SystemSetting.systemBranch);
////
//////			java.util.Date udate = date;
////			salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
////			salesReceipts.setSalesTransHdr(salesTransHdr);
////			System.out.println(salesReceipts);
////			ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
////			salesReceipts = respEntity.getBody();
////		}
////
////		salesTransHdr.setSalesMode("KOT");
////		// salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
////
////		// salesTransHdr.setVoucherNumber(vNo);
////		salesTransHdr.setSalesReceiptsVoucherNumber(salesReceiptVoucherNo);
////		if (null == salesTransHdr.getVoucherNumber()) {
////			RestCaller.updateSalesTranshdr(salesTransHdr);
////			notifyMessage(1, " Saved Successfully");
////		}
////
////		salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
////		ResponseEntity<List<SalesReceipts>> salesreceipt = RestCaller
////				.getSalesReceiptsByTransHdrId(salesTransHdr.getId());
////		salesReceiptsList = FXCollections.observableArrayList(salesreceipt.getBody());
////
////		for (SalesReceipts salesRec : salesReceiptsList) {
////			DayBook dayBook = new DayBook();
////			dayBook.setBranchCode(salesTransHdr.getBranchCode());
////			ResponseEntity<AccountHeads> accountHead1 = RestCaller.getAccountById(salesRec.getAccountId());
////			AccountHeads accountHeads1 = accountHead1.getBody();
////			dayBook.setDrAccountName(accountHeads1.getAccountName());
////			dayBook.setDrAmount(salesRec.getReceiptAmount());
////			dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
////			dayBook.setSourceVoucherType("SALES RECEIPTS");
////			if (salesRec.getReceiptMode().contains("CASH")) {
////				dayBook.setNarration("KOT CASH SALE");
////				dayBook.setCrAmount(0.0);
////
////			} else {
////				dayBook.setNarration("KOT CARD SALE");
////				dayBook.setCrAmount(salesRec.getReceiptAmount());
////				dayBook.setCrAccountName("KOT");
////			}
////			LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
////			dayBook.setsourceVoucherDate(Date.valueOf(rdate));
////			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
////		}
////		DayBook dayBook = new DayBook();
////		dayBook.setBranchCode(salesTransHdr.getBranchCode());
////		dayBook.setDrAccountName("KOT");
////		dayBook.setDrAmount(salesTransHdr.getInvoiceAmount());
////		dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
////		dayBook.setSourceVoucherType("SALES");
////		dayBook.setNarration("KOT" + salesTransHdr.getVoucherNumber());
////		dayBook.setCrAccountName("KOT");
////		dayBook.setCrAmount(salesTransHdr.getInvoiceAmount());
////
////		LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
////		dayBook.setsourceVoucherDate(Date.valueOf(rdate));
////		ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
////
////		ResponseEntity<List<TableWaiterReport>> tablewaiter = RestCaller.getAllocatedTable(SystemSetting.systemBranch);
////		tableWaiterList = FXCollections.observableArrayList(tablewaiter.getBody());
////		tblTableWaiter.setItems(tableWaiterList);
////
////		salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
////
////		/*
////		 * Now Print Invoice
////		 */
////
////		printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
////
////		salesTransHdr = null;
////		salesDtl = new SalesDtl();
////		txtcardAmount.setText("");
////		txtCashtopay.setText("");
////		txtChangeamount.clear();
////		txtPaidamount.setText("");
////		saleDtlObservableList.clear();
////		tblCardAmountDetails.getItems().clear();
////		salesReceiptsList.clear();
////
////		txtItemname.clear();
////		txtBarcode.clear();
////		txtBatch.clear();
////		txtQty.clear();
////		txtRate.clear();
////		cmbUnit.getSelectionModel().clearSelection();
////		
//////			}
////
////		logger.info("KotPOs========= final Save completed");
////
////		cmbCardType.setVisible(false);
////		txtCrAmount.setVisible(false);
////		AddCardAmount.setVisible(false);
////		btnDeleteCardAmount.setVisible(false);
////		tblCardAmountDetails.setVisible(false);
////		txtcardAmount.setVisible(false);
////		lblCardType.setVisible(false);
////		lblCardAmount.setVisible(false);
////		servingtable.requestFocus();
////		//--------------version 5.0 surya 
////				}
////		}
////		
////				//--------------version 5.0 end
////	}
////
////	private void showPopupByBarcode() {
////		try {
////			
////			if (SystemSetting.isNEGATIVEBILLING()) {
////				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
////				Parent root1;
////				String cst = null;
////				root1 = (Parent) fxmlLoader.load();
////				Stage stage = new Stage();
////				ItemPopupCtl  popctl = fxmlLoader.getController();
////				popctl.LoadItemPopupBySearch(txtBarcode.getText());
////
////				stage.initModality(Modality.APPLICATION_MODAL);
////				stage.initStyle(StageStyle.UNDECORATED);
////				stage.setTitle("Stock Item");
////				stage.initModality(Modality.APPLICATION_MODAL);
////				stage.setScene(new Scene(root1));
////				stage.show();
////
////				
////			} 
////			else
////			{
////			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
////			// fxmlLoader.setController(itemStockPopupCtl);
////			Parent root1;
////
////			root1 = (Parent) fxmlLoader.load();
////			Stage stage = new Stage();
////
////			ItemStockPopupCtl popctl = fxmlLoader.getController();
////			popctl.LoadItemPopupByBarcode(txtBarcode.getText());
////
////			stage.initModality(Modality.APPLICATION_MODAL);
////			stage.initStyle(StageStyle.UNDECORATED);
////			stage.setTitle("Stock Item");
////			stage.initModality(Modality.APPLICATION_MODAL);
////			stage.setScene(new Scene(root1));
////			stage.show();
////			}
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
////
////	}
////
////	public void notifyFailureMessage(int duration, String msg) {
////
////		Image img = new Image("failed.png");
////		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
////				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_LEFT)
////				.onAction(new EventHandler<ActionEvent>() {
////					@Override
////					public void handle(ActionEvent event) {
////						System.out.println("clicked on notification");
////					}
////				});
////		notificationBuilder.darkStyle();
////		notificationBuilder.show();
////	}
////
////	@FXML
////	void addTable(ActionEvent event) {
////		AddKotTable addKotTable = new AddKotTable();
////		String table = txtTable.getText().trim();
////
////		table = table.replaceAll("/", "-");
////		table = table.replaceAll("\"", "-");
////		table = table.replaceAll(" ", "-");
////		addKotTable.setTableName(table);
////		addKotTable.setStatus("ACTIVE");
////		addKotTable.setBranchCode(SystemSetting.systemBranch);
////		String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "KMT");
////		addKotTable.setId(vNo);
////		ResponseEntity<AddKotTable> savedTable = RestCaller.getTablebyName(table);
////		if (null != savedTable.getBody()) {
////			addKotTable = savedTable.getBody();
////			addKotTable.setStatus("ACTIVE");
////			ResponseEntity<AddKotTable> reponse = RestCaller.saveAddKotTable(addKotTable);
////
////		} else {
////			ResponseEntity<AddKotTable> reponse = RestCaller.saveAddKotTable(addKotTable);
////			// addKotTable.setId(((AddKotTable)reponse.getBody()).getId());
////			addKotTable = reponse.getBody();
////		}
////		notifyMessage(5, "'/' will be replaced with '-'");
////		addTableWaiter();
////		txtTable.clear();
////	}
////
////
////
////
//}
