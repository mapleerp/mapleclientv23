package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DebitNote;
import com.maple.mapleclient.entity.SupplierPriceMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.events.UserPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

@RestController
public class DebitNoteCtl {
	
	
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<DebitNote> debitNoteTable = FXCollections.observableArrayList();
	DebitNote debitNote;
	
	
	
	 @FXML
	    private TextField txtUserName;

	    @FXML
	    private Button btnAdd;

	    @FXML
	    private Button btnShowAll;

	    @FXML
	    private Button btnDelete;

	    @FXML
	    private TextField txtDebitAccount;

	    @FXML
	    private TextField txtCreditAccount;

	    @FXML
	    private TextField txtAmount;

	    @FXML
	    private TextField txtRemark;

	    @FXML
	    private TextField txtVoucherNumber;

	    @FXML
	    private DatePicker dpVoucherDate;

	    @FXML
	    private ComboBox<String> cmbBranch;

	    @FXML
	    private TableView<DebitNote> tblDebitNoteTable;

	    @FXML
	    private TableColumn<DebitNote, String> clUserName;

	    @FXML
	    private TableColumn<DebitNote, String> clDebit;

	    @FXML
	    private TableColumn<DebitNote, String> clCredit;

	    @FXML
	    private TableColumn<DebitNote, String> clBranch;

	    @FXML
	    private TableColumn<DebitNote, String> clAmount;

	    @FXML
	    private TableColumn<DebitNote, String> clVoucherNumber;

	    @FXML
	    private TableColumn<DebitNote, String> clVoucherDate;

	    @FXML
	    void AddItem(ActionEvent event) {
	    	save();
	    }
	    private void showUserPopup() {

			try {
				System.out.println("inside the popup");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/UserPopup.fxml"));
				Parent root = loader.load();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				txtRemark.requestFocus();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	    @FXML
	    void DeleteItem(ActionEvent event) {

	           if(null != debitNote)
	           {
	    	
	    		if(null != debitNote.getId())
	    		{
	    			RestCaller.deleteDebitNote(debitNote.getId());
	    			
	    			showAll();
	    			notifyMessage(5, " field deleted", false);
					return;
	    		}
	           }
	    }
	    private void showAll() {
	    	ResponseEntity<List<DebitNote>> respentity = RestCaller.getDebitNote();
            debitNoteTable = FXCollections.observableArrayList(respentity.getBody());
            
            System.out.print(debitNoteTable.size()+"observable list size issssssssssssssssssssss");
            fillTable();
			
		}
		public void save() {

			DebitNote	debitNote=new DebitNote();
				if (null == txtDebitAccount.getText()) {
					notifyMessage(5, "Please select Debit account", false);
					return;
				}
				
				if(txtUserName.getText().trim().isEmpty())
				{
					notifyMessage(5, "Please select user name", false);
					return;
				}
				ResponseEntity<UserMst> userMstResp = RestCaller.getUserByName(txtUserName.getText());
				UserMst userMst = userMstResp.getBody();

				if (null == userMst) {
					notifyMessage(5, "Invalid user name...!", false);
					return;
				}
				debitNote.setUserId(userMst.getId());
				//debitNote.setUserName(userMst.getId());
			    debitNote.setVoucherNumber(txtVoucherNumber.getText());
			    debitNote.setDebitAccount(txtDebitAccount.getText());
			    debitNote.setCreditAccount(txtCreditAccount.getText());
			    debitNote.setRemark(txtRemark.getText());
			    debitNote.setAmount(Double.valueOf(txtAmount.getText()));
			    debitNote.setBranch(cmbBranch.getSelectionModel().getSelectedItem());
				Date dpVoucherDateDepit=SystemSetting.localToUtilDate(dpVoucherDate.getValue());
				debitNote.setVoucherDate(dpVoucherDateDepit);
		

				ResponseEntity<DebitNote> respEntity = RestCaller.createDebitNote(debitNote);
				
				fillTable();

				
					notifyMessage(5, "Saved ", true);
					txtAmount.clear();
					txtCreditAccount.clear();
					txtDebitAccount.clear();
					dpVoucherDate.setValue(null);
					
					showAll();
					return;
			
				
		
			}
		
		
		private void setCmbBranchCode() {

			ResponseEntity<List<BranchMst>> branchListResp = RestCaller.getBranchMst();
			List<BranchMst> branchList = branchListResp.getBody();
			
			for(BranchMst branch
					 : branchList)
			{
				cmbBranch.setValue(branch.getBranchName());
			}
		}

		@FXML
		private void initialize() {
			dpVoucherDate = SystemSetting.datePickerFormat(dpVoucherDate, "dd/MMM/yyyy");
			eventBus.register(this);
			setCmbBranchCode();
			

			tblDebitNoteTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {

						debitNote = new DebitNote();
						debitNote.setId(newSelection.getId());
					}
				}
			});
		}
			
		

		private void fillTable() {

			tblDebitNoteTable.setItems(debitNoteTable);
			 clBranch.setCellValueFactory(cellData ->
			  cellData.getValue().getBranchNameProperty());
			clUserName.setCellValueFactory(cellData -> cellData.getValue().getUserNameProperty());
		//    clAmount.setCellValueFactory(cellData ->cellData.getValue().getAmountProperty());
			 clCredit.setCellValueFactory(cellData ->
			  cellData.getValue().getCreditAccountProperty());
			clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
			clDebit.setCellValueFactory(cellData -> cellData.getValue().getDebitAccountProperty());
			clBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchNameProperty());
			clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());

		}
	    @FXML
	    void ShowAllItems(ActionEvent event) {
	    	showAll();
	    }

	    @FXML
	    void addOnEnter(KeyEvent event) {

	    }

	    @FXML
	    void saveOnKey(KeyEvent event) {

	    }

	    @FXML
	    void userNameOnEnter(KeyEvent event) {

	    }
	   

	    @FXML
	    void userpopup(MouseEvent event) {
	    	showUserPopup();
	    }
	    public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
	    
	    

		@Subscribe
		public void userEventListener(UserPopupEvent userPopupEvent) {
			

			Stage stage = (Stage) btnAdd.getScene().getWindow();
			if (stage.isShowing()) {
				
				txtUserName.setText(userPopupEvent.getUserName());
				
			}
		
		}
	}
	


