package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.MapleConstants;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.entity.InvoiceEditEnableMst;
import com.maple.mapleclient.entity.InvoiceFormatMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class PartyCreationCtl {
	
	
	String taskid;
	String processInstanceId;
	String CUSTOM_VOUCHER_PROPERTY;
	EventBus eventBus = EventBusFactory.getEventBus();

	String pricetype;
	private ObservableList<AccountHeads> partyRegistrationList = FXCollections.observableArrayList();

	AccountHeads partyRegistration = null;
	StringProperty SearchString = new SimpleStringProperty();


    @FXML
    private TextField txtName;

    @FXML
    private TextField txtCompanyName;

    @FXML
    private TextField txtAddress;

    @FXML
    private TextField txtPhoneNo;

    @FXML
    private TextField txtEmailid;

    @FXML
    private TextField txtGst;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnShowAll;

    @FXML
    private ComboBox<String> cmbState;

    @FXML
    private TextField txtCreditPeriod;

    @FXML
    private ComboBox<String> cmbCurrency;

    @FXML
    private ComboBox<String> cmbCountry;

    @FXML
    private CheckBox chkOnline;

    @FXML
    private ComboBox<String> cmbCustorsupplier;

    @FXML
    private TableView<AccountHeads> tblcust;

    @FXML
    private TableColumn<AccountHeads, String> clPartyType;

    @FXML
    private TableColumn<AccountHeads, String> CR1Name;

    @FXML
    private TableColumn<AccountHeads, String> CR2ADDRESS;

    @FXML
    private TableColumn<AccountHeads, String> CR5GSt;

    @FXML
    private TableColumn<AccountHeads, String> CR3Contact;

    @FXML
    private TableColumn<AccountHeads, String> CR4Email;

    @FXML
    private ComboBox<String> pricecombo;

    @FXML
    private ComboBox<String> cmbGroup;

    @FXML
    private TextField txtCustDiscount;

    @FXML
    private ComboBox<String> cmbDiscountProperty;

    @FXML
    private TextField txtBankName;

    @FXML
    private TextField txtBankAccountName;

    @FXML
    private TextField txtBankIfsc;

    @FXML
    private ComboBox<String> cmbInvoiceFormat;

    @FXML
    private Button btnBankDtl;
    @FXML
    private Label lblDiscount;

    @FXML
    private Label lblDiscountProperty;

    @FXML
    private Label lblBankName;

    @FXML
    private Label lblBankAccountName;

    @FXML
    private Label lblBankIfsc;

    @FXML
    void Clearfields(ActionEvent event) {
    	clear();
    	partyRegistration = null;
    }

	@FXML
	void bankDtls(ActionEvent event) {
		
		txtBankName.setVisible(true);
		txtBankAccountName.setVisible(true);
		txtBankIfsc.setVisible(true);
		lblBankName.setVisible(true);
		lblBankIfsc.setVisible(true);
		lblBankAccountName.setVisible(true);
	}
	
	@FXML
	private void initialize() {


		
		eventBus.register(this);
		showAccountHeadGroup();
		
		
		try {
		lblBankName.setVisible(false);
		lblBankIfsc.setVisible(false);
		txtBankAccountName.setVisible(false);
		txtBankName.setVisible(false);
		lblBankAccountName.setVisible(false);
		txtBankIfsc.setVisible(false);
		}catch(Exception e) {
			System.out.println(e.toString());
		}
		
		System.out.println("--LoadCustomerBySearch--initialize");
		cmbState.getItems().add("KERALA");
		cmbState.getItems().add("ANDHRA PRADESH");
		cmbState.getItems().add("ARUNACHAL PRADESH");
		cmbState.getItems().add("ASSAM");
		cmbState.getItems().add("BIHAR");
		cmbState.getItems().add("CHHATTISGARH");
		cmbState.getItems().add("GOA");
		cmbState.getItems().add("GUJARAT");
		cmbState.getItems().add("HARYANA");
		cmbState.getItems().add("HIMACHAL PRADESH");
		cmbState.getItems().add("JAMMU AND  KASHMIR");
		cmbState.getItems().add("JHARKHAND");
		cmbState.getItems().add("KARNATAKA");
		cmbState.getItems().add("MADHYA PRADESH");
		cmbState.getItems().add("MAHARASHTRA");
		cmbState.getItems().add("MANIPUR");
		cmbState.getItems().add("MEGHALAYA");
		cmbState.getItems().add("MIZORAM");
		cmbState.getItems().add("NAGALAND");
		cmbState.getItems().add("ODISHA");
		cmbState.getItems().add("PUNJAB");
		cmbState.getItems().add("RAJASTHAN");
		cmbState.getItems().add("TAMIL NADU");
		cmbState.getItems().add("TELANGANA");
		cmbState.getItems().add("TRIPURA");
		cmbState.getItems().add("UTTAR PRADESH");
		cmbState.getItems().add("UTTARAKHAND");
		cmbState.getItems().add("WEST BENGAL");
       //  country
		cmbCountry.getItems().add("INDIA");
		cmbCountry.getItems().add("PAKISTAN");
		cmbCountry.getItems().add("BANGLADESH");
		cmbCountry.getItems().add("SREELANKA");
		cmbCountry.getItems().add("CHINA");
		cmbCountry.getItems().add("NEPAL");
		cmbCountry.getItems().add("IRAN");
		cmbCountry.getItems().add("AFGANISTHAN");
		
		cmbCustorsupplier.getItems().add(MapleConstants.CUSTOMER);
		cmbCustorsupplier.getItems().add(MapleConstants.SUPPLIER);
		cmbCustorsupplier.getItems().add(MapleConstants.BOTH);
		
		cmbCustorsupplier.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					if (!newValue.equalsIgnoreCase(null)) {
						
//						if (cmbCustorsupplier.getSelectionModel().getSelectedItem().equalsIgnoreCase(MapleConstants.CUSTOMER)) {
//							cmbCurrency.setDisable(true);
//							pricecombo.setDisable(false);
//							cmbGroup.setDisable(false);
//							txtCustDiscount.setDisable(false);
//							cmbDiscountProperty.setDisable(false);
//							cmbInvoiceFormat.setDisable(false);
//							pricecombo.setDisable(true);
//						}else if(cmbCustorsupplier.getSelectionModel().getSelectedItem().equalsIgnoreCase(MapleConstants.SUPPLIER)) {
//							pricecombo.setDisable(true);
//							cmbGroup.setDisable(true);
//							txtCustDiscount.setDisable(true);
//							cmbDiscountProperty.setDisable(true);
//							cmbInvoiceFormat.setDisable(true);
//							pricecombo.setDisable(true);
//							cmbCurrency.setDisable(false);
//							
//						}else {
//							pricecombo.setDisable(false);
//							cmbGroup.setDisable(false);
//							txtCustDiscount.setDisable(false);
//							cmbDiscountProperty.setDisable(false);
//							cmbInvoiceFormat.setDisable(false);
//							pricecombo.setDisable(false);
//							cmbCurrency.setDisable(false);
//						}
					}

				}
			}
		});
		
		
		
	
		partyRegistration = new AccountHeads();
		txtName.textProperty().bindBidirectional(SearchString);

		cmbDiscountProperty.getItems().add(MapleConstants.ONBASISOFBASEPRICE);
		cmbDiscountProperty.getItems().add(MapleConstants.ONBASISOFMRP);
		cmbDiscountProperty.getItems().add(MapleConstants.ONBASISOFDISCOUNTINCLUDINGTAX);
		
		
	
		cmbPriceDefenition();
		
		//check box for deciding the process is done on either offline or online=======
		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig(MapleConstants.OFFLINECUSTOMERCREATION);
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase(MapleConstants.YES)) {
				chkOnline.setDisable(false);
			} else {
				chkOnline.setDisable(true);
			}
		}else {
			chkOnline.setDisable(true);

		}
		chkOnline.selectedProperty().addListener(
				(obs, oldSelection, newSelection) ->	{
					if(chkOnline.isSelected()) {
						chkOnline.setText(MapleConstants.OFFLINE);
					}else {
						chkOnline.setText(MapleConstants.ONLINE);
					}
				});
		//============================end===================================
		


		// ------------------------table binding
		ResponseEntity<List<InvoiceFormatMst>> jasperResp = RestCaller.getAllJasperByPriceType();
		for(int i=0;i<jasperResp.getBody().size();i++)
		{
			cmbInvoiceFormat.getItems().add(jasperResp.getBody().get(i).getReportName());
		}
		
		ResponseEntity<List<CurrencyMst>> currencyMst = RestCaller.getallCurrencyMst();
		for(int i=0;i<currencyMst.getBody().size();i++)
		{
			cmbCurrency.getItems().add(currencyMst.getBody().get(i).getCurrencyName());
		}
		SearchString.addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				System.out.println("item Search changed");
				LoadPartyBySearch((String) newValue);
			}
		});
		
		txtCustDiscount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCustDiscount.setText(oldValue);
				}
			}
		});
		
		
		tblcust.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					pricecombo.setPromptText("");
					cmbGroup.setPromptText("");
					partyRegistration = new AccountHeads();
				
					ResponseEntity<AccountHeads> getcust = RestCaller.getNameById(newSelection.getId());
					partyRegistration = getcust.getBody();
					ResponseEntity<PriceDefenitionMst> priceDef = RestCaller
							.getPriceNameById(getcust.getBody().getPriceTypeId());
					txtCreditPeriod.setText(Integer.toString(newSelection.getCreditPeriod()));
					txtName.setText(newSelection.getAccountName());
//					txtCompanyName.setText(newSelection.getCompanyName());
					cmbCustorsupplier.setPromptText(newSelection.getCustomerOrSupplier());
					cmbCustorsupplier.setValue(getcust.getBody().getCustomerOrSupplier());
					txtAddress.setText(newSelection.getPartyAddress1());
					txtPhoneNo.setText(newSelection.getCustomerContact());
					txtEmailid.setText(newSelection.getPartyMail());
					txtGst.setText(newSelection.getPartyGst());
					txtCustDiscount.setText(Double.toString(newSelection.getCustomerDiscount()));
					if (null != priceDef.getBody()) {
						pricecombo.setPromptText(priceDef.getBody().getPriceLevelName());
						pricecombo.setValue(priceDef.getBody().getPriceLevelName());
					} else
						pricecombo.getSelectionModel().clearSelection();

					cmbState.setPromptText(newSelection.getCustomerState());
					cmbState.setValue(getcust.getBody().getCustomerState());
					cmbCountry.setPromptText(newSelection.getCustomerCountry());
					cmbCountry.setValue(getcust.getBody().getCustomerCountry());
//					cmbCurrency.setPromptText(newSelection.getCurrencyId());
//					cmbCurrency.setValue(getcust.getBody().getCurrencyId());
					cmbGroup.setPromptText(getcust.getBody().getCustomerGroup());
					cmbGroup.setValue(getcust.getBody().getCustomerGroup());
					cmbDiscountProperty.setPromptText(getcust.getBody().getDiscountProperty());
					cmbDiscountProperty.setValue(getcust.getBody().getDiscountProperty());
				}
			}
		});

		
		 txtCreditPeriod.textProperty().addListener((observable, oldValue, newValue) -> {
		        if (!newValue.matches("\\d*\\.?\\d*")) {
		        	txtCreditPeriod.setText(newValue.replaceAll("[^\\d]", ""));
		        }
		    });
		 
		 txtCustDiscount.textProperty().addListener((observable, oldValue, newValue) -> {
			 if (!newValue.matches("\\d*\\.?\\d*")) {
		        	txtCreditPeriod.setText(newValue.replaceAll("[^\\d]", ""));
		        }
		    });
		 
		txtPhoneNo.textProperty().addListener((observable, oldValue, newValue) -> {
			 if (!newValue.matches("\\d*\\.?\\d*")) {
		        	txtCreditPeriod.setText(newValue.replaceAll("[^\\d]", ""));
		        }
		    });
	
	}
	

	@FXML
	void custAddressOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtPhoneNo.requestFocus();
		}
	}

	@FXML
	void nameOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAddress.requestFocus();
		}
	}

	@FXML
	void custContactOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtEmailid.requestFocus();
		}
	}

	@FXML
	void custEmailOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtGst.requestFocus();
		}
	}

	@FXML
	void custStateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		//	txtCreditPeriod.requestFocus();
		}
	}
	

	@FXML
	void custCountryOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCreditPeriod.requestFocus();
		}
	}

	@FXML
	void creditPeriodOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			pricecombo.requestFocus();
		}
	}

	@FXML
	void gstOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbState.requestFocus();
		}
	}

	@FXML
	void submitOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			submit();
		}
	}

	private void submit() {

		if (null != partyRegistration)

		{
			if (null != partyRegistration.getId()) {

				partyRegistration.setBankAccountName(txtBankAccountName.getText());
				partyRegistration.setBankIfsc(txtBankIfsc.getText());
				partyRegistration.setBankName(txtBankName.getText());
				if(null == cmbCountry.getSelectionModel())
				{
					partyRegistration.setCustomerCountry("INDIA");
				}
				else
				{
					partyRegistration.setCustomerCountry(cmbCountry.getSelectionModel().getSelectedItem());
				}
				partyRegistration.setPartyAddress1(txtAddress.getText());
				partyRegistration.setCustomerContact(txtPhoneNo.getText());
				partyRegistration.setPartyMail(txtEmailid.getText());
				partyRegistration.setCustomerOrSupplier(cmbCustorsupplier.getSelectionModel().getSelectedItem());
				partyRegistration.setPartyGst(txtGst.getText());
				partyRegistration.setCurrencyId(cmbCurrency.getSelectionModel().getSelectedItem());
				partyRegistration.setCustomerGroup(cmbGroup.getSelectionModel().getSelectedItem());
				partyRegistration.setCreditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
				if(null !=cmbInvoiceFormat.getSelectionModel().getSelectedItem() )
				{
				partyRegistration.setTaxInvoiceFormat(cmbInvoiceFormat.getSelectionModel().getSelectedItem());
				}
				if(null!=txtBankName.getText()) {
					partyRegistration.setBankName(txtBankName.getText());
					
				}
				if(null!=txtBankAccountName) {
					partyRegistration.setBankAccountName(txtBankAccountName.getText());
				}
				if(null!=txtBankIfsc) {
					partyRegistration.setBankIfsc(txtBankIfsc.getText());
				}
				if(null!=cmbCountry.getValue()) {
					partyRegistration.setCustomerCountry(cmbCountry.getValue());
				}
				String pricetype = pricecombo.getValue();
				ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
						.getPriceDefenitionByName(pricetype);

				if(priceDefenitionSaved.getBody().size()>0)
				{
				partyRegistration.setPriceTypeId(priceDefenitionSaved.getBody().get(0).getId());
				}
				else
				{
					notifyMessage(4,"Select Price Type");
					return;
				}
				if (null == cmbState.getValue()) {
					partyRegistration.setCustomerState("KERALA");

				} else {

					partyRegistration.setCustomerState(cmbState.getValue());
				}

				if(txtCustDiscount.getText().trim().isEmpty())
				{
					partyRegistration.setCustomerDiscount(0.0);
				}
				else
				{
				partyRegistration.setCustomerDiscount(Double.parseDouble(txtCustDiscount.getText()));
				}
				partyRegistration.setDiscountProperty(cmbDiscountProperty.getSelectionModel().getSelectedItem());

				if (txtGst.getText().length()>=13) 
				{
					partyRegistration.setCustomerType("REGULAR");
				}
				else
				{
					partyRegistration.setCustomerType("CONSUMER");
				}
				ResponseEntity<ParamValueConfig> custedit = RestCaller.getParamValueConfig("CUSTOMER_EDIT");
				if(null != custedit.getBody())
				{
					if(custedit.getBody().getValue().equalsIgnoreCase("NO"))
					{
					
						notifyMessage(3,"Cannot Update Customer");
						return;
					}
					else
					{
						ResponseEntity<List<InvoiceEditEnableMst>> getInvoice = RestCaller.getInvoiceEditEnableMstByCustomer(txtName.getText(),SystemSetting.getUser().getId());
						if(getInvoice.getBody().size()>0)
						{
							RestCaller.updatePartyRegistration(partyRegistration);
							notifyMessage(5, "Updated");
						}
						else
						{
							notifyMessage(5, "You are not Allowed to edit this user");
							return;
						}
					}
				}
				else
				{
					RestCaller.updatePartyRegistration(partyRegistration);
					notifyMessage(5, "Updated");
				}
				filltable();
				clear();
			}
		 else {
			try {
				if (txtName.getText().trim().isEmpty()) {
					notifyMessage(5, "Please Enter  Name...!!!");
					return;
				} else if (txtAddress.getText().trim().isEmpty()) {
					notifyMessage(5, "Please Enter Address...!!!");
					return;
				} else if (txtPhoneNo.getText().trim().isEmpty()) {
					notifyMessage(5, "Please Enter Contact...!!!");
					return;
				} else if (txtGst.getText().trim().isEmpty()) {
					notifyMessage(5, "Please Enter gst...!!!");
					return;
				} else if (cmbState.getSelectionModel().isEmpty()) {
					notifyMessage(5, "Please Select State...!!!");
					cmbState.requestFocus();
					return;
				} else if (cmbCustorsupplier.getSelectionModel().isEmpty()) {
					notifyMessage(5, "Please Select Customer or Supplier...!!!");
					cmbCustorsupplier.requestFocus();
					return;
				} 
				
				else if (null == pricecombo.getValue()) {

					notifyMessage(5, "Please Select Price type...!!!");
					pricecombo.requestFocus();
					return;
				} else if (txtCreditPeriod.getText().trim().isEmpty()) {

					notifyMessage(5, "Please Select credit period...!!!");
					return;
				} else if (!txtName.getText().contains(txtPhoneNo.getText())) {

					notifyMessage(5, "Please add  1st phone number to the end of  name");
					return;
				} else {
					
					if(txtGst.getText().length() >= 5)
					{
						ResponseEntity<List<AccountHeads>> customerGstList = RestCaller.AccountHeadsByGst(txtGst.getText());
						List<AccountHeads> gstList = customerGstList.getBody();
						if(gstList.size() > 0)
						{
							notifyMessage(2, "Gst is already registered");
							txtGst.requestFocus();
							return;
						}
					}
					tblcust.getItems().clear();
					partyRegistration = new AccountHeads();
					String custName1 = txtName.getText().trim();

					custName1 = custName1.replaceAll("'", "");
					custName1 = custName1.replaceAll("&", "");

					custName1 = custName1.replaceAll("/", "");
					custName1 = custName1.replaceAll("%", "");
					custName1 = custName1.replaceAll("\"", "");
					
					partyRegistration.setAccountName(custName1);
					String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "CU");
					partyRegistration.setId(vNo + SystemSetting.getUser().getCompanyMst().getId()
							+ SystemSetting.systemBranch);
					partyRegistration.setCustomerRank(0);
					partyRegistration.setPartyAddress1(txtAddress.getText());
					partyRegistration.setCustomerOrSupplier(cmbCustorsupplier.getSelectionModel().getSelectedItem());
					partyRegistration.setCustomerContact(txtPhoneNo.getText());
					partyRegistration.setPartyMail(txtEmailid.getText());
					partyRegistration.setPartyGst(txtGst.getText());
					partyRegistration.setCustomerGroup(cmbGroup.getSelectionModel().getSelectedItem());
					partyRegistration.setCreditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
					if(null !=cmbInvoiceFormat.getSelectionModel().getSelectedItem() )
					{
					partyRegistration.setTaxInvoiceFormat(cmbInvoiceFormat.getSelectionModel().getSelectedItem());
					}
					String pricetype = pricecombo.getValue();
					ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
							.getPriceDefenitionByName(pricetype);

					partyRegistration.setPriceTypeId(priceDefenitionSaved.getBody().get(0).getId());
					if (null == cmbState.getValue()) {
						partyRegistration.setCustomerState("KERALA");

					} else {

						partyRegistration.setCustomerState(cmbState.getValue());
					}
					if(null != cmbCurrency.getSelectionModel().getSelectedItem())
			 		{
			 			ResponseEntity<CurrencyMst> getcurrency = RestCaller.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem());
			 			if(null != getcurrency.getBody())
			 			{
			 				partyRegistration.setCurrencyId(getcurrency.getBody().getId());
			 			}
			 		}
					if (txtGst.getText().length()>=13) 
					{
						partyRegistration.setCustomerType("REGULAR");
					}
					else
					{
						partyRegistration.setCustomerType("CONSUMER");
					}
					ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadByName(txtName.getText());
					if (null != getAccountHeads.getBody()) {
						partyRegistration = new AccountHeads();
						notifyMessage(5, "Name Already Registered");
						clear();
						return;
					}
					if(txtCustDiscount.getText().trim().isEmpty())
					{
						partyRegistration.setCustomerDiscount(0.0);
					}
					else
					{
					partyRegistration.setCustomerDiscount(Double.parseDouble(txtCustDiscount.getText()));
					}
					partyRegistration.setDiscountProperty(cmbDiscountProperty.getSelectionModel().getSelectedItem());
					if(null!=txtBankName.getText()) {
						partyRegistration.setBankName(txtBankName.getText());
						
					}
					if(null!=txtBankAccountName) {
						partyRegistration.setBankAccountName(txtBankAccountName.getText());
					}
					if(null!=txtBankIfsc) {
						partyRegistration.setBankIfsc(txtBankIfsc.getText());
					}
					if(null!=cmbCountry.getValue()) {
						partyRegistration.setCustomerCountry(cmbCountry.getValue());
					}
					
					
					//===========code for identify whether the customer is saved on offline or online mode=======01-07-2021====
					String status = null;
					ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("OFFLINE_CUSTOMER_CREATION");
					ParamValueConfig paramValueConfig = getParamValue.getBody();
					if(chkOnline.isSelected() || null == paramValueConfig || paramValueConfig.getValue().equalsIgnoreCase("NO")) {
						ResponseEntity<AccountHeads> respentity = RestCaller.partyRegistration(partyRegistration);
						partyRegistration = respentity.getBody();
					}else {
					status = "ONLINE";
					ResponseEntity<AccountHeads> respentity = RestCaller.partyRegistration(partyRegistration,status);
					partyRegistration = respentity.getBody();
					}
					
					//=================code end for saving customer==============================
					
					
					if (null != partyRegistration) {
						partyRegistrationList.add(partyRegistration);
						tblcust.setItems(partyRegistrationList);
						notifyMessage(5, "Registration Successfully Completed");

						filltable();
						clear();

						eventBus.post(partyRegistration);
					} else {

						notifyMessage(5, "Please Fill Your Fields");
					}
				}
			} catch (Exception e) {
				partyRegistration = new AccountHeads();
				return;

			}
		}
		}
		partyRegistration = new AccountHeads();
	
		
	}

	@FXML
	void Submit(ActionEvent event) {
		submit();

	}

	@FXML
	void priceTypeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			cmbGroup.requestFocus();
		}
	}

	@FXML
	void groupOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCustDiscount.requestFocus();
		}
	}

    @FXML
    void custDiscountKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		btnSave.requestFocus();
		}
    }

    @FXML
    void Search(ActionEvent event) {
    	tblcust.getItems().clear();
    	LoadPartyBySearch("");
    }

    

    @FXML
    void addressOnEnter(KeyEvent event) {
     	if (event.getCode() == KeyCode.ENTER)
    	{
    		txtCreditPeriod.requestFocus();
    	}
    }

   

    @FXML
    void companyNameOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER)
    	{
    		txtAddress.requestFocus();
    	}
    }
    
    @FXML
    void creditPeriodOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER)
    	{
    		txtPhoneNo.requestFocus();
    	}
    }
  
    @FXML
    void phnoOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER)
    	{
    		txtEmailid.requestFocus();
    	}
    }
    @FXML
    void emailIdOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER)
    	{
    		txtGst.requestFocus();
    	}

    }
      @FXML
    void supGstOnEnter(KeyEvent event) {
    		if (event.getCode() == KeyCode.ENTER)
        	{
        		cmbState.requestFocus();
        	}
    }
      @FXML
      void stateOnEnter(KeyEvent event) {
    	  if (event.getCode() == KeyCode.ENTER)
      	{
      		btnSave.requestFocus();
      	}
      }

   
    @FXML
    void supNameOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER)
    	{
    		txtCompanyName.requestFocus();
    	}
    }
    public void clear() {
    	txtAddress.setText("");
		txtPhoneNo.setText("");
		txtEmailid.setText("");
		txtGst.setText("");
		txtName.setText("");
		txtCustDiscount.clear();
		txtBankName.clear();
		txtBankIfsc.clear();
		txtBankAccountName.clear();
		txtCreditPeriod.clear();
		cmbState.getSelectionModel().clearSelection();
		cmbState.setPromptText("");
		cmbDiscountProperty.getSelectionModel().clearSelection();
		cmbDiscountProperty.setPromptText("");
		pricecombo.getSelectionModel().clearSelection();
		pricecombo.setPromptText("");
		cmbGroup.getSelectionModel().clearSelection();
		cmbGroup.setPromptText("");
		cmbCurrency.getSelectionModel().clearSelection();
		txtCompanyName.clear();
		cmbCustorsupplier.getSelectionModel().clearSelection();
		cmbCountry.getSelectionModel().clearSelection();
		partyRegistration = new AccountHeads();
		try {
			lblBankName.setVisible(false);
			lblBankIfsc.setVisible(false);
			txtBankAccountName.setVisible(false);
			txtBankName.setVisible(false);
			lblBankAccountName.setVisible(false);
			txtBankIfsc.setVisible(false);

			}catch(Exception e) {
				System.out.println(e.toString());
			}
	}

	private void showAccountHeadGroup() {

		ResponseEntity<List<AccountHeads>> getAccHeads = RestCaller.getAssetAccounts();
		for (int i = 0; i < getAccHeads.getBody().size(); i++) {
			cmbGroup.getItems().add(getAccHeads.getBody().get(i).getAccountName());
		}

	}

	private void cmbPriceDefenition() {

		pricecombo.getItems().clear();

		System.out.println("=====PriceTypeOnKEyPress===");
		ArrayList priceType = new ArrayList();

		priceType = RestCaller.getPriceDefinition();
		Iterator itr = priceType.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object priceLevelName = lm.get("priceLevelName");
			Object id = lm.get("id");
			if (id != null) {
				pricecombo.getItems().add((String) priceLevelName);

			}

		}

	}
	
	// ----------show all btn---------------
	private void LoadPartyBySearch(String searchData) {

		tblcust.getItems().clear();
		ArrayList cat = new ArrayList();
		RestTemplate restTemplate = new RestTemplate();
		cat = RestCaller.SearchPartyByName(searchData);
		Iterator itr = cat.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object accountName = lm.get("accountName");
			Object PartyAddress1 = lm.get("partyAddress1");
			Object customerContact = lm.get("customerContact");
			Object customerMail = lm.get("partyMail");
			Object customerGst = lm.get("partyGst");
			Object id = lm.get("id");
			Object customerorsupplier = lm.get("customerOrSupplier");
			Object currency = lm.get("currencyId");
//			Object state = lm.get("customerState");
			Object customerDiscount =  lm.get("customerDiscount");
			
			
			if (id != null) {
				AccountHeads customer = new AccountHeads();

				
				try {
				customer.setPartyAddress1((String) PartyAddress1);
				customer.setCustomerContact((String) customerContact);
				customer.setPartyGst((String) customerGst);
				customer.setPartyMail((String) customerMail);
				customer.setAccountName((String) accountName);
				customer.setId((String) id);
				customer.setCurrencyId((String) currency);
				customer.setCustomerOrSupplier((String)customerorsupplier );
//				customer.setCustomerState((String) state);
				customer.setCustomerDiscount((Double)customerDiscount);
				
				
				partyRegistrationList.add(customer);
				tblcust.setItems(partyRegistrationList);
				}catch(Exception e) {
					System.out.println(e.toString());
				}
				
				filltable();
			}
		}
	}

	public void filltable() {
		clPartyType.setCellValueFactory(cellData -> cellData.getValue().getCustomerorSupplierProperty());
		CR1Name.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		CR2ADDRESS.setCellValueFactory(cellData -> cellData.getValue().getCustomerAddressProperty());
		CR3Contact.setCellValueFactory(cellData -> cellData.getValue().getCustomerContactProperty());
		CR4Email.setCellValueFactory(cellData -> cellData.getValue().getCustomerMailProperty());
		CR5GSt.setCellValueFactory(cellData -> cellData.getValue().getCustomerGstProperty());
		
	}
	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	
	 public String setCustomerVoucherProperty(String voucher) {
	    	this.CUSTOM_VOUCHER_PROPERTY = voucher;
	    	
	    	
	    	return this.CUSTOM_VOUCHER_PROPERTY ;
	    	
	    }
}