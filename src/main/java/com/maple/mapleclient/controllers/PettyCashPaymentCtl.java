package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PettyCashPaymentCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PaymentDtl> paymentList = FXCollections.observableArrayList();
	PaymentHdr paymenthdr = null;
	PaymentDtl paymentDtl = null;
	PaymentDtl paymentDelete = null;
	double totalamount = 0.0;
	String modeOfPay = null;
	@FXML
	private Button btnAdd;
	@FXML
	private TextField txtGrandTotal;

	@FXML
	private TextField txtDate;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnFinalSubmit;

	@FXML
	private TextField txtAccount;

	@FXML
	private TextField txtRemarks;

	@FXML
	private TextField txtAmount;

	@FXML
	private ComboBox<String> cmbModeOfPayment;

	@FXML
	private DatePicker dpTransaDate;

	@FXML
	private TableView<PaymentDtl> tbPayment;

	@FXML
	private TableColumn<PaymentDtl, String> clAccount;

	@FXML
	private TableColumn<PaymentDtl, String> clModeOfPay;

	@FXML
	private TableColumn<PaymentDtl, String> clRemarks;

	@FXML
	private TableColumn<PaymentDtl, LocalDate> clTransaDate;

	@FXML
	private TableColumn<PaymentDtl, Number> clAmount;

	@FXML
	private TableColumn<PaymentDtl, String> clInstrumentNo;

	@FXML
	private TableColumn<PaymentDtl, String> voucher;

	@FXML
	private TableColumn<PaymentDtl, String> clBankAccount;

	@FXML
	private TableColumn<PaymentDtl, LocalDate> clInstrumentDate;

	@FXML

	private void initialize() {
		dpTransaDate = SystemSetting.datePickerFormat(dpTransaDate, "dd/MMM/yyyy");
		txtDate.setText(SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd"));
		eventBus.register(this);
		
		//-----------------new version 1.9 surya
//		ResponseEntity<AccountHeads> accountHead = RestCaller
//				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-PETTY CASH");
//		cmbModeOfPayment.getItems().add(accountHead.getBody().getAccountName());
//		cmbModeOfPayment.setPromptText(accountHead.getBody().getAccountName());
//		cmbModeOfPayment.setValue(accountHead.getBody().getAccountName());
//		modeOfPay = accountHead.getBody().getAccountName();
		
		ResponseEntity<List<ReceiptModeMst>> receiptModeResp  = RestCaller.getReceiptModeMstByName(SystemSetting.getSystemBranch()+"-PETTY CASH");
		List<ReceiptModeMst> receiptMode = receiptModeResp.getBody();
		if(receiptMode.size() > 0)
		{
			cmbModeOfPayment.getItems().add(receiptMode.get(0).getReceiptMode());
			cmbModeOfPayment.setPromptText(receiptMode.get(0).getReceiptMode());
			modeOfPay = receiptMode.get(0).getReceiptMode();
			cmbModeOfPayment.setValue(receiptMode.get(0).getReceiptMode());

		}
		//-----------------new version 1.9 surya end
		
		
		tbPayment.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					paymentDelete = new PaymentDtl();
					paymentDelete.setId(newSelection.getId());
				}
			}
		});
		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
	}

	@FXML
	void AddItems(ActionEvent event) {
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}
			}
		}
		String account = txtAccount.getText();
		if (null != cmbModeOfPayment.getSelectionModel()) {
			String mode = cmbModeOfPayment.getSelectionModel().getSelectedItem();
			if (mode.equalsIgnoreCase(account)) {
				notifyMessage(4, "Mode of Payment and Account Heads should not be same");
				return;
			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}
		if (null == paymenthdr) {
			paymenthdr = new PaymentHdr();
			paymenthdr.setBranchCode(SystemSetting.systemBranch);
			paymenthdr.setCompanyId(SystemSetting.getUser().getCompanyMst().getId());

			paymenthdr.setVoucherDate((SystemSetting.systemDate));
			ResponseEntity<PaymentHdr> respentity = RestCaller.savePaymenthdr(paymenthdr);
			paymenthdr = respentity.getBody();
		}
		paymentDtl = new PaymentDtl();
		paymentDtl.setAccount(txtAccount.getText());
		ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountHeadByName(txtAccount.getText());
		paymentDtl.setAccountId(accSaved.getBody().getId());
		paymentDtl.setAmount(Double.parseDouble(txtAmount.getText()));
		paymentDtl.setRemark(txtRemarks.getText());
		paymentDtl.setModeOfPayment(modeOfPay);
		paymentDtl.setTransDate(Date.valueOf(SystemSetting.utilToLocaDate(SystemSetting.systemDate)));
		paymentDtl.setPaymenthdr(paymenthdr);
		if (null == cmbModeOfPayment.getSelectionModel().getSelectedItem()) {

			ResponseEntity<AccountHeads> creditAccount = RestCaller
					.getAccountHeadByName(cmbModeOfPayment.getItems().get(0));
			paymentDtl.setCreditAccountId(creditAccount.getBody().getId());
		} else {
			ResponseEntity<AccountHeads> creditAccount = RestCaller
					.getAccountHeadByName(cmbModeOfPayment.getSelectionModel().getSelectedItem());
			paymentDtl.setCreditAccountId(creditAccount.getBody().getId());

		}
		paymentDtl.setInstrumentDate(null);
		ResponseEntity<PaymentDtl> respentity = RestCaller.savePaymentdtls(paymentDtl);
		paymentDtl = respentity.getBody();

		setTotal();
		paymentDtl.setAccount(txtAccount.getText());
		paymentList.add(paymentDtl);
		fillTable();
		txtAccount.requestFocus();
		clearFilelds();

		cmbModeOfPayment.getSelectionModel().clearSelection();
		ResponseEntity<AccountHeads> accountHead = RestCaller
				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-PETTY CASH");
		cmbModeOfPayment.getItems().add(accountHead.getBody().getAccountName());
		cmbModeOfPayment.setPromptText(accountHead.getBody().getAccountName());
		cmbModeOfPayment.setValue(accountHead.getBody().getAccountName());
		btnAdd.setDisable(true);
	}

	private void clearFilelds()

	{
		txtAccount.clear();
		txtAmount.clear();
		txtRemarks.clear();
	}

	@FXML
	void DeleteItem(ActionEvent event) {
		btnAdd.setDisable(false);
		if (null != paymentDelete.getId()) {
			RestCaller.deletePaymentDtl(paymentDelete.getId());
			notifyMessage(5, "Deleted!!!");
			ResponseEntity<List<PaymentDtl>> paymentdtlSaved = RestCaller.getPaymentDtl(paymenthdr);
			paymentList = FXCollections.observableArrayList(paymentdtlSaved.getBody());
			if (paymentList != null) {
				for (PaymentDtl payment : paymentList) {
					ResponseEntity<AccountHeads> respentity = RestCaller.getAccountById(payment.getAccountId());
					payment.setAccount(respentity.getBody().getAccountName());
				}

			}
			tbPayment.setItems(paymentList);
		}
		setTotal();

	}

	@FXML
	void loadPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	@FXML
	void FinalSubmit(ActionEvent event) {
		ResponseEntity<List<PaymentDtl>> paymentdtlSaved = RestCaller.getPaymentDtl(paymenthdr);
		if (paymentdtlSaved.getBody().size() == 0) {
			return;
		}

		String financialYear = SystemSetting.getFinancialYear();
		String vNo = RestCaller.getVoucherNumber(financialYear + "PYN");

		paymenthdr.setVoucherNumber(vNo);
		RestCaller.updatePaymenthdr(paymenthdr);

		for (PaymentDtl paymnt : paymentList) {
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(paymenthdr.getBranchCode());
			ResponseEntity<AccountHeads> accountHead = RestCaller
					.getAccountHeadByName(SystemSetting.getSystemBranch() + "-PETTY CASH");
			AccountHeads accountHeads = accountHead.getBody();
			if (paymnt.getModeOfPayment().equalsIgnoreCase(accountHeads.getAccountName())) {
				dayBook.setCrAccountName(SystemSetting.systemBranch + "-PETTY CASH");
			}

			else {
				dayBook.setCrAccountName(paymnt.getBankAccountNumber());
			}
			dayBook.setCrAmount(paymnt.getAmount());
			ResponseEntity<AccountHeads> accountHead1 = RestCaller.getAccountById(paymnt.getAccountId());
			AccountHeads accountHeads1 = accountHead1.getBody();
			dayBook.setNarration(accountHeads1.getAccountName() + paymenthdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(paymenthdr.getVoucherNumber());
			dayBook.setSourceVoucherType("PAYMENTS");
			dayBook.setDrAccountName(accountHeads1.getAccountName());
			dayBook.setDrAmount(paymnt.getAmount());
			LocalDate ldate = SystemSetting.utilToLocaDate(paymenthdr.getVoucherDate());
			dayBook.setsourceVoucherDate(Date.valueOf(ldate));
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		}

		// version1.6
		clearFields();
		txtGrandTotal.clear();
		cmbModeOfPayment.getSelectionModel().clearSelection();
		ResponseEntity<AccountHeads> accountHead = RestCaller
				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-PETTY CASH");
		cmbModeOfPayment.getItems().add(accountHead.getBody().getAccountName());
		cmbModeOfPayment.setPromptText(accountHead.getBody().getAccountName());
		cmbModeOfPayment.setValue(accountHead.getBody().getAccountName());
		// version1.6 ends
		java.util.Date uDate = paymenthdr.getVoucherDate();
		paymenthdr = null;
		paymentList.clear();
		txtGrandTotal.clear();
		String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		try {
			JasperPdfReportService.PaymentReport(vNo, vDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btnAdd.setDisable(false);
		// version1.6 - > Coment two lines
		// clearFields();
		// txtGrandTotal.clear();
		// version1.6 ends
	}

	@FXML
	void remarkOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			txtAmount.requestFocus();
		}

	}

	@FXML
	void transDateOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			txtAmount.requestFocus();
		}
	}

	@FXML
	void amountOnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			btnAdd.requestFocus();
		}
	}

	@FXML
	void ShowAccountPopup(MouseEvent event) {
		showPopup();
	}

	private void clearFields() {
		txtAccount.clear();
		txtAmount.clear();
		txtRemarks.clear();
		cmbModeOfPayment.getSelectionModel().clearSelection();

	}

	private void fillTable() {
		tbPayment.setItems(paymentList);

		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountProperty());
		clRemarks.setCellValueFactory(cellData -> cellData.getValue().getRemarkProperty());
		clModeOfPay.setCellValueFactory(cellData -> cellData.getValue().getModeOfPaymentProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clTransaDate.setCellValueFactory(cellData -> cellData.getValue().gettransDateProperty());
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}

	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Accounts");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtRemarks.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			txtAccount.setText(accountEvent.getAccountName());
		}
	}

	private void setTotal() {

		totalamount = RestCaller.getPaymentTotal(paymenthdr.getId());
		txtGrandTotal.setText(Double.toString(totalamount));
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }


}
