package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ShortExpiryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.engine.JRException;

public class ShortExpiryReportCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<ShortExpiryReport> shortExpiryList = FXCollections.observableArrayList();


    @FXML
    private TableView<ShortExpiryReport> tblShortExpiry;
    
    @FXML
    private TableColumn<ShortExpiryReport, String> clmnGroup;
    
    @FXML
    private TableColumn<ShortExpiryReport, String> clItemName;
    
    @FXML
    private TableColumn<ShortExpiryReport, String> clmnItemCode;

    @FXML
    private TableColumn<ShortExpiryReport, String> clBatch;

    @FXML
    private TableColumn<ShortExpiryReport, LocalDate> clExpiryDate;

    @FXML
    private TableColumn<ShortExpiryReport, LocalDate> clUpdatedDate;

    @FXML
    private TableColumn<ShortExpiryReport, Number> clQty;
    
    @FXML
    private TableColumn<ShortExpiryReport, Number> clmnRate;

    @FXML
    private TableColumn<ShortExpiryReport, Number> clmnAmount;

    @FXML
    private TableColumn<ShortExpiryReport, String> clmnDaysToExpire;


    @FXML
    private Button btnShowReport;

    @FXML
    private Button btnPrintReport;

    @FXML
    void PrintReport(ActionEvent event) {
    	try {
    		Date curdate = SystemSetting.applicationDate;
        	String currentDate = SystemSetting.UtilDateToString(curdate, "yyyy-MM-dd");
			JasperPdfReportService.ShortExpiryReport(SystemSetting.systemBranch,SystemSetting.UtilDateToString(SystemSetting.getSystemDate()),currentDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    @FXML
    void shohReport(ActionEvent event) {
  	
    	Date curdate = SystemSetting.applicationDate;
    	String currentDate = SystemSetting.UtilDateToString(curdate, "yyyy-MM-dd");
    	ResponseEntity<List<ShortExpiryReport>> getDetails = RestCaller.getShortExpiryReport(currentDate);
    	shortExpiryList = FXCollections.observableArrayList(getDetails.getBody());
    	fillTable();
    	
    }

    private void fillTable()
    {
    	tblShortExpiry.setItems(shortExpiryList);
    	clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
    	clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getexpiryDateProperty());
    	clUpdatedDate.setCellValueFactory(cellData -> cellData.getValue().getupdatedDateProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
    	clmnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    	clmnDaysToExpire.setCellValueFactory(cellData -> cellData.getValue().getDaysToExpiryProperty());
    	clmnItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemCodeProperty());
    	clmnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
    	clmnGroup.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}

}
