package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;

public class ServiceTreeCtl {
	
	String taskid;
	String processInstanceId;

    @FXML
    private TreeView<String> rootTree;
    String selectedText = null;
    
    @FXML
    void initialize() {
    	
    	TreeItem rootItem = new TreeItem("SERVICE");
		//TreeItem masterItem = new TreeItem("MASTERS");
		
	HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
		
		
		
		ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("SERVICE");
		
		List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
		
		MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
		
		String parentId =aMenuConfigMst.getId();
		
		ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
		List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

		
		for(MenuConfigMst aMenuConfig : menuConfigMstList)
		{
			
			//masterItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
			
			if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
				rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
				}
			
			
		}
		
		//rootItem.getChildren().add(masterItem);

		rootTree.setRoot(rootItem);
		
		rootTree.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text 12345 : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();
				
				
				ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller.MenuConfigMstByMenuName(selectedText);
				
				List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();
		
				MenuConfigMst aMenuConfig = menuConfigMstList.get(0);
				
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != selectedText)
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(selectedText, dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
			
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
				  
				
			/*	if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.kotManagement);

				}
				
				*/
				
				

			}

		});
//    
//    	TreeItem rootItem = new TreeItem("Service");
//    	
//    	
//    	TreeItem locationMst = new TreeItem("LOCATION MASTER");
//		if (SystemSetting.UserHasRole("LOCATION MASTER")) {
//			rootItem.getChildren().add(locationMst);
//    	}
//    	
//    		TreeItem serviceIn = new TreeItem("SERVICE IN");
//    		if (SystemSetting.UserHasRole("SERVICE IN")) {
//    			rootItem.getChildren().add(serviceIn);
//        	}
//    		
//    		TreeItem serviceItemCreation = new TreeItem("ITEM CREATION");
//    		if (SystemSetting.UserHasRole("ITEM CREATION")) {
//    			rootItem.getChildren().add(serviceItemCreation);
//        	}
//    		TreeItem JobCardPreparation = new TreeItem("JOB CARD PREPARATION");
//    		if (SystemSetting.UserHasRole("JOB CARD PREPARATION")) {
//    			rootItem.getChildren().add(JobCardPreparation);
//        	}
//    		TreeItem serviceItemMst = new TreeItem("SERVICE ITEM MST CREATION");
//    		if (SystemSetting.UserHasRole("SERVICE ITEM MST CREATION")) {
//    			rootItem.getChildren().add(serviceItemMst);
//        	}
//    		TreeItem jobcardout = new TreeItem("JOBCARD TRANSFER OUT");
//    		if (SystemSetting.UserHasRole("JOBCARD TRANSFER OUT")) {
//    			rootItem.getChildren().add(jobcardout);
//        	}
//    		
//    		
//    		TreeItem jobCardIn = new TreeItem("JOB CARD IN");
//    		if (SystemSetting.UserHasRole("JOB CARD IN")) {
//    			rootItem.getChildren().add(jobCardIn);
//        	}
//    		
//    		rootTree.setRoot(rootItem);
//    		rootTree.getSelectionModel().selectedItemProperty().addListener( new ChangeListener() {
//     		   @Override
//                public void changed(ObservableValue observable, Object oldValue,
//                        Object newValue) {
//
//                    TreeItem<String> selectedItem = (TreeItem<String>) newValue;
//                    System.out.println("Selected Text : " + selectedItem.getValue());
//                    selectedText = selectedItem.getValue();
//     	
//     if(selectedItem.getValue().equalsIgnoreCase( "SERVICE IN")) {
//     	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//     		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//     	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.serviceIn);
//     
//     }
//     if(selectedItem.getValue().equalsIgnoreCase( "JOB CARD IN")) {
//      	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//      		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//      	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.jobCardIn);
//      
//      }
//     if(selectedItem.getValue().equalsIgnoreCase( "ITEM CREATION")) {
//      	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//      		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//      	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.serviceItemCreation);
//      
//      }
//     
//     if(selectedItem.getValue().equalsIgnoreCase( "JOB CARD PREPARATION")) {
//       	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//       		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//       	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.JobCardPreparation);
//       
//       }
//     
//     if(selectedItem.getValue().equalsIgnoreCase( "SERVICE ITEM MST CREATION")) {
//        	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//        		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//        	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.serviceItemMst);
//        
//        }
//     if(selectedItem.getValue().equalsIgnoreCase( "JOBCARD TRANSFER OUT")) {
//     	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//     		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//     	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.JobcardOut);
//     
//     }
//     
//     if(selectedItem.getValue().equalsIgnoreCase("LOCATION MASTER")) {
//       	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//       		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//       	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.locationMst);
//       
//       }
//    }   
//     		 
//	  });
//    
//    		
}
    @FXML
    void serviceOnClick(MouseEvent event) {
    	

    	if (null != selectedText) {	//1. Fetch command and fx from table based on the clicked item text.
			//2. load the fx similar way we did in the main frame
			
		 
			HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
			
			
			
			ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName(selectedText);
			
			List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
			
			MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
			
			
			ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(aMenuConfigMst.getId());
			List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();
	
			
			for(MenuConfigMst aMenuConfig : menuConfigMstList)
			{
				
				Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
				if(null==dynamicWindow) {
					if(null != aMenuConfig.getMenuFxml())
					{
						  try {
							dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
							 menuWindowQueue.put(aMenuConfig.getMenuName(), dynamicWindow);
						  
						  } catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
	 
				}
				  
				  try {
			
						// Clear screen before loading the Page.
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();
			
						MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
			
						/*MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
						MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					 	dynamicWindow.requestFocus();
						 
						*/
					} catch (Exception e) {
						e.printStackTrace();
					}
				  
				  
			}
    	}
//    	if(null != selectedText)
//    	{
//    		  if(selectedText.equalsIgnoreCase( "SERVICE IN")) {
//    		     	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		     		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		     	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.serviceIn);
//    		     
//    		     }
//    		     if(selectedText.equalsIgnoreCase( "JOB CARD IN")) {
//    		      	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		      		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		      	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.jobCardIn);
//    		      
//    		      }
//    		     if(selectedText.equalsIgnoreCase( "ITEM CREATION")) {
//    		      	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		      		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		      	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.serviceItemCreation);
//    		      
//    		      }
//    		     
//    		     if(selectedText.equalsIgnoreCase( "JOB CARD PREPARATION")) {
//    		       	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		       		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		       	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.JobCardPreparation);
//    		       
//    		       }
//    		     
//    		     if(selectedText.equalsIgnoreCase( "SERVICE ITEM MST CREATION")) {
//    		        	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		        		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		        	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.serviceItemMst);
//    		        
//    		        }
//    		     if(selectedText.equalsIgnoreCase( "JOBCARD TRANSFER OUT")) {
//    		     	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		     		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		     	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.JobcardOut);
//    		     
//    		     }
//    		     
//    		     if(selectedText.equalsIgnoreCase("LOCATION MASTER")) {
//    		       	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		       		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//
//    		       	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.locationMst);
//    		       
//    		       }
//    }
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}


}
