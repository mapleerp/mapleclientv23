package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;

import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseHdr;

import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesFinalSave;
import com.maple.mapleclient.entity.SalesManMst;
import com.maple.mapleclient.entity.SalesPropertiesConfigMst;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.SalesTypeMst;
import com.maple.mapleclient.entity.SchEligibilityAttribInst;
import com.maple.mapleclient.entity.SchOfferAttrInst;
import com.maple.mapleclient.entity.SchSelectionAttribInst;
import com.maple.mapleclient.entity.SchemeInstance;
import com.maple.mapleclient.entity.SiteMst;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CategorywiseItemSearchEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.CustomerNewPopUpEvent;
import com.maple.mapleclient.events.HoldedCustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.LocalCustomerEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.application.Platform;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

public class WholeSaleWindowCtl {

	String taskid;
	String processInstanceId;

	String localCustId = null;
	private ObservableList<SalesTypeMst> saleTypeTable = FXCollections.observableArrayList();
	private ObservableList<PriceDefinition> priceDefenitionList = FXCollections.observableArrayList();
	private ObservableList<BatchPriceDefinition> BatchpriceDefenitionList = FXCollections.observableArrayList();

	private ObservableList<SiteMst> siteMstList = FXCollections.observableArrayList();

	private static final Logger logger = LoggerFactory.getLogger(PurchaseCtl.class);
	String invoiceNumberPrefix = SystemSetting.WHOLE_SALES_PREFIX;
	String gstInvoicePrefix = SystemSetting.GST_INVOCE_PREFIX;
	String vanSalesPrefix = SystemSetting.GST_INVOCE_PREFIX;
	EventBus eventBus = EventBusFactory.getEventBus();
	// ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();

	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();

//	String store = "MAIN";

	String storeNameFromPopUp = null;
	@FXML
	private TextField txtDiscount;

	@FXML
	private Label lblAmount;
	@FXML
	private TextField txtDiscountAmt;

	@FXML
	private TextField txtPriceType;
	@FXML
	private TextField txtPreviousBalance;
	@FXML
	private Button btnSearch;
	@FXML
	private TextField txtLocalCustomer;

	@FXML
	private TextField txtSalesType;

	@FXML
	private TextField txtCreditPeriod;

	@FXML
	private Label lblCreditPeriod;

	@FXML
	private TextField txtWarranty;

	@FXML
	private Button btnClear;

	@FXML
	private TextField txtStore;

	@FXML
	void ShowLocalCustomerPopup(ActionEvent event) {

		showLocalCustPopup();
	}

	private void showLocalCustPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/LocalCustPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);

			Parent root = loader.load();
			LocalCustPopupCtl popupctl = loader.getController();
			popupctl.localCustomerPopupByCustomerId(custId);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

			gstNo.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupLocalCustomerlistner(LocalCustomerEvent localCustomerEvent) {
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			txtLocalCustomer.setText(localCustomerEvent.getLocalCustAddress());
			localCustId = localCustomerEvent.getLocalCustid();
			System.out.println("custIdcustIdcustId" + custId);

		}

	}

	@FXML
	private TextField txtAmtAftrDiscount;
	SalesDtl salesDtl = null;
	SalesTransHdr salesTransHdr = null;
	UnitMst unitMst = null;

	double qtyTotal = 0;
	double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	String custId = "";
	boolean customerIsBranch = false;

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty discount = new SimpleStringProperty("");
	StringProperty discountAmt = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");

	StringProperty barcodeProperty = new SimpleStringProperty("");

	StringProperty taxRateProperty = new SimpleStringProperty("");

	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnRate;
	@FXML
	private TableColumn<SalesDtl, String> columnMrp;
	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;
	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtItemcode;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private ComboBox<String> cmbSaleType;
	@FXML
	private Button btnSave;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField txtLoginDate;
	@FXML
	private TextField txtSBICard;

	@FXML
	private TextField txtSodexoCard;

	@FXML
	private TextField txtYesCard;
	@FXML
	private TextField custname;
	@FXML
	private TextField custAdress;
	@FXML
	private TextField gstNo;

	@FXML
	private ComboBox<String> cmbStore;

	@FXML
	private ComboBox<String> cmbSalesMan;

	@FXML
	void onEnterCustPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();
		}
	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

		loadCustomerPopup();
	}

	@FXML
	void discountOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (!txtDiscount.getText().isEmpty()) {
				txtDiscountAmt.clear();
				Double amtaftradiscount;
				Double discountamt = (Double.parseDouble(txtCashtopay.getText())
						* Double.parseDouble(txtDiscount.getText())) / 100;
				BigDecimal disamt = new BigDecimal(discountamt);
				disamt = disamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				txtDiscountAmt.setText(disamt.toPlainString());
				// txtDiscountAmt.setEditable(false);
				amtaftradiscount = Double.parseDouble(txtCashtopay.getText())
						- Double.parseDouble(txtDiscountAmt.getText());
				BigDecimal amtaftdiscount = new BigDecimal(amtaftradiscount);
				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
				txtAmtAftrDiscount.setEditable(false);
			}

		}
	}

	@FXML
	void discountAmntOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtDiscountAmt.getText().isEmpty()) {
				txtDiscount.clear();
				Double discountAftrAmt = 0.0, discount = 0.0;
				discount = (Double.parseDouble(txtDiscountAmt.getText()) / Double.parseDouble(txtCashtopay.getText()))
						* 100;
				BigDecimal disamt = new BigDecimal(discount);
				disamt = disamt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtDiscount.setText(disamt.toPlainString());
				discountAftrAmt = Double.parseDouble(txtCashtopay.getText())
						- Double.parseDouble(txtDiscountAmt.getText());
				BigDecimal amtaftdiscount = new BigDecimal(discountAftrAmt);
				amtaftdiscount = amtaftdiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				txtAmtAftrDiscount.setText(amtaftdiscount.toPlainString());
				txtAmtAftrDiscount.setEditable(false);
			}

		}
	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {
		// showPopup();
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
	}

	@FXML
	private void initialize() {

		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("WHOLESALE_RATE_EDIT");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("NO")) {
				txtRate.setEditable(false);
			} else {
				txtRate.setEditable(true);
			}
		}
		eventBus.register(this);

		cmbStore.setDisable(true);

		txtLoginDate.setText(SystemSetting.UtilDateToString(SystemSetting.applicationDate));
		txtDiscount.setVisible(false);
		txtAmtAftrDiscount.setVisible(false);
		txtDiscountAmt.setVisible(false);
//		cmbSaleType.getItems().add("VAN SALE");
//		cmbSaleType.getItems().add("COUNTER SALE");
		ResponseEntity<List<SalesTypeMst>> salesTypeSaved = RestCaller.getSalesTypeMst();
		if (null == salesTypeSaved.getBody()) {
			notifyMessage(5, "Please Add Voucher Type");
			return;
		}
		saleTypeTable = FXCollections.observableArrayList(salesTypeSaved.getBody());

		for (SalesTypeMst salesType : saleTypeTable) {
			cmbSaleType.getItems().add(salesType.getSalesType());

		}

		logger.info("========INITIALIZATION STARTED IN WHOLE SALE WINDOW ===============");
		/*
		 * Create an instance of SalesDtl. SalesTransHdr entity will be refreshed after
		 * final submit. SalesDtl will be added on AddItem Function
		 * 
		 */

		// salesDtl = new SalesDtl();
		/*
		 * Fieds with Entity property
		 */

		txtDiscountAmt.textProperty().bindBidirectional(discountAmt);
		txtDiscount.textProperty().bindBidirectional(discount);
		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);
		// txtItemcode.textProperty().bindBidirectional(salesDtl.getItemCodeProperty());
		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPaidamount.setText(oldValue);
				}
			}
		});
		txtChangeamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtChangeamount.setText(oldValue);
				}
			}
		});
		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashtopay.setText(oldValue);
				}
			}
		});
		txtSBICard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSBICard.setText(oldValue);
				}
			}
		});

		txtYesCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtYesCard.setText(oldValue);
				}
			}
		});

		txtSodexoCard.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtSodexoCard.setText(oldValue);
				}
			}
		});
		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});

		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtPaidamount.getText().length() > 0)) {

					double paidAmount = Double.parseDouble(txtPaidamount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmt = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmount = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((cardAmount) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtcardAmount.getText().length() > 0)) {

					double cardAmt = Double.parseDouble(txtcardAmount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmount = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double paidAmt = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((paidAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_HALF_EVEN);
					changeAmtProperty.set(newrate.toPlainString());
				}
			}
		});

		// btnSave.setDisable(true);
		itemDetailTable.setItems(saleListTable);

		txtBarcode.requestFocus();

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					salesDtl = new SalesDtl();
					txtBatch.setText(newSelection.getBatchCode());
					salesDtl = new SalesDtl();
					salesDtl.setId(newSelection.getId());
					if (null != newSelection.getOfferReferenceId()) {
						salesDtl.setOfferReferenceId(newSelection.getOfferReferenceId());
					}

					if (null != newSelection.getSchemeId()) {
						salesDtl.setSchemeId(newSelection.getSchemeId());
					}
					salesDtl.setItemId(newSelection.getItemId());
					salesDtl.setSalesTransHdr(newSelection.getSalesTransHdr());
					salesDtl.setQty(newSelection.getQty());
					salesDtl.setUnitId(newSelection.getUnitId());
					salesDtl.setUnitName(newSelection.getUnitName());
					txtBarcode.setText(newSelection.getBarcode());
					txtItemname.setText(newSelection.getItemName());
					txtItemcode.setText(newSelection.getItemCode());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));
					cmbUnit.setValue(newSelection.getUnitName());
				}
			}
		});
		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				// ----------------------------new version 2 surya

				if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO")) {
					return;
				}
				// ----------------------------new version 2 surya end

				/*
				 * new url for getting customer from account heads by Account name====05-01-2022
				 */
//				ResponseEntity<CustomerMst> getCust = RestCaller.getCustomerByName(custname.getText());
				ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadsByName(custname.getText());
				AccountHeads accountHeadss = getAccountHeads.getBody();

				// ResponseEntity<PriceDefinition> getpriceDef = RestCaller.get
				ResponseEntity<UnitMst> getUnit = RestCaller
						.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

				UnitMst unitMst = getUnit.getBody();

				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

				ItemMst item = getItem.getBody();

				if (null == item || null == item.getId()) {
					return;
				}

				String unitId = "";

				if (null == unitMst) {
					unitId = item.getUnitId();

				} else {
					unitId = unitMst.getId();
				}

				Date udate = SystemSetting.getApplicationDate();
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
				ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
						getItem.getBody().getId(), getAccountHeads.getBody().getPriceTypeId(),
						getUnit.getBody().getId(), txtBatch.getText(), sdate);
				if (null != batchPriceDef.getBody()) {
					txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
				} else {
					txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));

					ResponseEntity<PriceDefinition> priceDef = RestCaller.getPriceDefenitionByItemIdAndUnit(
							item.getId(), accountHeadss.getPriceTypeId(), unitMst.getId(), sdate);
					if (null != priceDef.getBody()) {
						txtRate.setText(Double.toString(priceDef.getBody().getAmount()));

					}

					else

					{
						ResponseEntity<List<PriceDefinition>> pricebyItem = RestCaller.getPriceByItemId(
								getItem.getBody().getId(), getAccountHeads.getBody().getPriceTypeId());
						priceDefenitionList = FXCollections.observableArrayList(pricebyItem.getBody());

						if (null != pricebyItem.getBody())

						{
							for (PriceDefinition price : priceDefenitionList) {
								if (null == price.getUnitId()) {
									txtRate.setText(Double.toString(price.getAmount()));

								}
							}
						}
					}

				}

			}
		});

		ResponseEntity<List<StoreMst>> storeMstListResp = RestCaller.getAllStoreMst();
		List<StoreMst> storeMstList = storeMstListResp.getBody();

		for (StoreMst store : storeMstList) {
			cmbStore.getItems().add(store.getName());
		}

		ResponseEntity<List<SalesManMst>> salesManMstResp = RestCaller.findAllSalesMan();
		List<SalesManMst> salesManMstList = salesManMstResp.getBody();

		for (SalesManMst salesMan : salesManMstList) {
			cmbSalesMan.getItems().add(salesMan.getSalesManName());
		}

		logger.info("======== WHOLE SALE WINDOW INITIALIZATION COMPLETED");

		txtCreditPeriod.setVisible(false);

	}

	@FXML
	void deleteRow(ActionEvent event) {

		try {

			if (null != salesDtl) {
				if (null != salesDtl.getId()) {

					if (null == salesDtl.getSchemeId()) {

						RestCaller.deleteSalesDtl(salesDtl.getId());

						System.out.println("toDeleteSale.getId()" + salesDtl.getId());
//						RestCaller.deleteSalesDtl(salesDtl.getId());
						txtItemname.clear();
						txtItemcode.clear();
						txtBarcode.clear();
						txtBatch.clear();
						txtRate.clear();
						txtQty.clear();
						ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
						saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
						FillTable();
						salesDtl = new SalesDtl();

						notifyMessage(1, " Item Deleted Successfully");

					} else {
						notifyMessage(1, " Offer can not be delete");
					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void hold(ActionEvent event) {
		txtAmtAftrDiscount.clear();
		txtBarcode.clear();
		txtBatch.clear();
		txtcardAmount.clear();
		txtCashtopay.clear();
		txtChangeamount.clear();
		txtDiscount.clear();
		txtYesCard.clear();
		txtSodexoCard.clear();
		txtSBICard.clear();
		txtRate.clear();
		txtQty.clear();
		txtPriceType.clear();
		txtItemcode.clear();
		txtItemname.clear();
		txtPaidamount.clear();
		custAdress.clear();
		custname.clear();
		custId = null;
		saleListTable.clear();
		saleTypeTable.clear();
		cmbSaleType.getSelectionModel().clearSelection();
		salesDtl = null;
		salesTransHdr = null;

		txtCreditPeriod.setVisible(false);

		notifyMessage(5, "Sales Holded");

	}

	@FXML
	void saveOnkey(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}
	}

	@FXML
	void saveOnCtl(KeyEvent event) {
//    	if (event.getCode() == KeyCode.S && event.isControlDown()  ) {
//    		finalSave();
//    	}
	}

	@FXML
	void FinalSaveOnPress(KeyEvent event) {

		logger.info("===== WHOLE SALE FINAL SAVE STARTED ============");
		if (event.getCode() == KeyCode.ENTER) {
			finalSave();
		}
	}

	private void finalSave() {

//		if (null != cmbStore.getSelectionModel().getSelectedItem()) {
//
//			store = cmbStore.getSelectionModel().getSelectedItem().toString();
//		}

		btnSave.setDisable(true);
		/*
		 * FinalSave Final
		 */
		// -------------------verification 5.0 Surya
		if (!SystemSetting.isNEGATIVEBILLING()) {
			String stockVerification = RestCaller.StockVerificationBySalesTransHdr(salesTransHdr.getId());
			if (null != stockVerification) {
				notifyMessage(5, stockVerification);

				// return;

			}
		}

		// Check for sales Properties
		ResponseEntity<List<SalesPropertiesConfigMst>> salesPropertyList = RestCaller.getAllSalesPropertyConfigMst();
		if (salesPropertyList.getBody().size() > 0) {
			saveSalesProperty();
		}
		// -------------------verification 5.0 Surya end

		salesTransHdr.setCustomerId(custId);

		logger.info("===========Whole Sale get customer by Id in Add item!!");

		/*
		 * new url for getting customer details from account heads by Account
		 * id====05-01-2022
		 */
//		ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerById(custId);
		ResponseEntity<AccountHeads> accountHeadsResponce = RestCaller.getAccountHeadsById(custId);
		if (null != accountHeadsResponce.getBody()) {
			salesTransHdr.setAccountHeads(accountHeadsResponce.getBody());

			if (null == accountHeadsResponce.getBody().getPartyGst()
					|| accountHeadsResponce.getBody().getPartyGst().length() < 13) {
				salesTransHdr.setSalesMode("B2C");
			} else {
				salesTransHdr.setSalesMode("B2B");
			}
		}

		txtChangeamount.setText("00.0");
		Double paidAmount = 0.0;
		Double cardAmount = 0.0;
		String card = "";
		if (!txtSBICard.getText().trim().isEmpty()) {
			card = txtSBICard.getText();
		} else if (!txtYesCard.getText().trim().isEmpty()) {
			card = txtYesCard.getText();
		} else if (!txtSodexoCard.getText().trim().isEmpty()) {
			card = txtSodexoCard.getText();
		}

		if (!txtLocalCustomer.getText().trim().isEmpty()) {
			ResponseEntity<LocalCustomerMst> LocalCustomerMstResp = RestCaller.getLocalCustomerById(localCustId);
			LocalCustomerMst localCustomerMst = LocalCustomerMstResp.getBody();
			if (null != localCustomerMst) {
				salesTransHdr.setLocalCustomerMst(localCustomerMst);
			}

		}
		salesTransHdr.setCardNo(card);

		Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
		salesTransHdr.setInvoiceAmount(invoiceAmount);

		if (txtDiscountAmt.getText().length() > 0)
			salesTransHdr.setInvoiceDiscount(Double.parseDouble(txtDiscountAmt.getText()));
		else
			salesTransHdr.setInvoiceDiscount(0.0);
		if (!txtPaidamount.getText().trim().isEmpty()) {

			paidAmount = Double.parseDouble(txtPaidamount.getText());
			salesTransHdr.setCashPay(paidAmount);
		} else {
			salesTransHdr.setCashPay(0.0);
		}

		if (!txtcardAmount.getText().trim().isEmpty()) {

			cardAmount = Double.parseDouble(txtcardAmount.getText());
			salesTransHdr.setCardamount(cardAmount);
		} else {
			salesTransHdr.setCardamount(0.0);
		}
		salesTransHdr.setPaidAmount(salesTransHdr.getCashPay() + salesTransHdr.getCardamount());

		if (!txtChangeamount.getText().trim().isEmpty()) {
			Double changeAmount = Double.parseDouble(txtChangeamount.getText());
			salesTransHdr.setChangeAmount(changeAmount);
		}
		logger.info("=====Whole Sale Card amount and cash amount set===========");
		eventBus.post(salesTransHdr);

		// Double
		// change=Double.parseDouble(txtPaidamount.getText())-Double.parseDouble(txtCashtopay.getText());
		// txtChangeamount.setText(Double.toString(change));
		// btnSave.setDisable(false);
		// btnSave.setDisable(true);

		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		if (saledtlSaved.getBody().size() == 0) {
			return;
		}
		logger.info("=====salesTransHdr.getCustomerId()======" + salesTransHdr.getCustomerId());

		ResponseEntity<BranchMst> branchMst = RestCaller.getBranchMstById(salesTransHdr.getCustomerId());

		BranchMst branch = new BranchMst();
		branch = branchMst.getBody();

		if (null != branch) {
			salesTransHdr.setSalesMode("BRANCH_SALES");
		}

		// String financialYear = SystemSetting.getFinancialYear();
		// String vNo = RestCaller.getVoucherNumber(financialYear + "CRD");
		// if((Double.parseDouble(txtCashtopay.getText())) > (paidAmount+cardAmount))
		// {

		// salesTransHdr.setVoucherNumber(vNo);
		logger.info("=======Whole Sale set invoice prefix started==========");

		ResponseEntity<SalesTypeMst> getsalesType = RestCaller
				.getSaleTypeByname(cmbSaleType.getSelectionModel().getSelectedItem());
		salesTransHdr.setInvoiceNumberPrefix(getsalesType.getBody().getSalesPrefix());

		logger.info("Whole Sale invoice prefix COMPLETED===============");

//				Date date = Date.valueOf(LocalDate.now());

		/*
		 * 4th March 2021. As per discussion with Ambrosia Vinod / Mukkadan / Geetha,
		 * System date is changed to Application Date.
		 */
		Date date = SystemSetting.applicationDate;
		salesTransHdr.setVoucherDate(date);
		// Call Rest to find if customer is Branch

		if (customerIsBranch) {
			salesTransHdr.setIsBranchSales("Y");
		} else {
			salesTransHdr.setIsBranchSales("N");
		}

		logger.info("Whole Sale STARTED TO SET ACCOUNT RECEIVABLE");

		/*
		 * new url for getting customer details from account heads by Account
		 * id====05-01-2022
		 */
//		ResponseEntity<CustomerMst> custentity = RestCaller.getCustomerById(custId);
		ResponseEntity<AccountHeads> accountHeadsentity = RestCaller.getAccountHeadsById(custId);
		ResponseEntity<AccountReceivable> accountReceivableResp = RestCaller
				.getAccountReceivableBySalesTransHdrId(salesTransHdr.getId());
		AccountReceivable accountReceivable = null;
		if (null != accountReceivableResp.getBody()) {
			accountReceivable = accountReceivableResp.getBody();
		} else {
			accountReceivable = new AccountReceivable();
		}

		accountReceivable.setAccountId(custId);
//		accountReceivable.setCustomerMst(custentity.getBody());
		accountReceivable.setAccountHeads(accountHeadsentity.getBody());

		if (txtAmtAftrDiscount.getText().length() > 0) {
			accountReceivable.setDueAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
			accountReceivable.setDueAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
			accountReceivable.setBalanceAmount(Double.parseDouble(txtAmtAftrDiscount.getText()));
		} else {
			accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
			accountReceivable.setDueAmount(Double.parseDouble(txtCashtopay.getText()));
			accountReceivable.setBalanceAmount(Double.parseDouble(txtCashtopay.getText()));
		}
		LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		LocalDate dueDate = due.plusDays(accountHeadsentity.getBody().getCreditPeriod());
		accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
		accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
		accountReceivable.setSalesTransHdr(salesTransHdr);
		accountReceivable.setRemark("Wholesale");
		LocalDate due1 = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		accountReceivable.setVoucherDate(java.sql.Date.valueOf(due1));
		accountReceivable.setPaidAmount(0.0);
//		ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
//		accountReceivable = respentity.getBody();

		logger.info("Whole Sale ACCOUNT RECEIVABLE SAVED !!");
		logger.info("Whole Sale STARTED TO SAVE SALE RECEIPTS!!");

		// Delete any sales receiots previously saved by mistake

		ResponseEntity<List<SalesReceipts>> srList = RestCaller.getSalesReceiptsByTransHdrId(salesTransHdr.getId());
		if (srList.getBody().size() > 00) {

			RestCaller.deleteSalesReceiptsByTransHdr(salesTransHdr);
		}

		SalesReceipts salesReceipts = new SalesReceipts();
		salesReceipts.setReceiptMode("CREDIT");
		salesReceipts.setReceiptAmount(invoiceAmount);
		salesReceipts.setSalesTransHdr(salesTransHdr);
		salesReceipts.setAccountId(accountHeadsentity.getBody().getId());
		salesReceipts.setBranchCode(salesTransHdr.getBranchCode());

//		RestCaller.saveSalesReceipts(salesReceipts);

		logger.info("Whole Sale SALE RECEIPTS SAVED!!");

		if (null == salesTransHdr.getVoucherNumber()) {
//			RestCaller.updateSalesTranshdr(salesTransHdr);

			/*
			 * new single url for common final save of sales including sales_trans_hdr,
			 * sales_receipts, account_receivable jan 14 2022
			 */
			DayBook dayBook = new DayBook();
			dayBook.setDrAccountName(custname.getText());

			SalesFinalSave salesFinalSave = new SalesFinalSave();
			salesFinalSave.setAccountReceivable(accountReceivable);
			salesFinalSave.setDayBook(dayBook);
			salesFinalSave.setSalesReceipts(salesReceipts);
			salesFinalSave.setSalesTransHdr(salesTransHdr);
			RestCaller.FinalSaveForSales(salesFinalSave);
		}
		salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());

		notifyMessage(5, "Sales Saved");
//		DayBook dayBook = new DayBook();
//		dayBook.setBranchCode(salesTransHdr.getBranchCode());
//		dayBook.setDrAccountName(custname.getText());
//		dayBook.setDrAmount(salesTransHdr.getInvoiceAmount());
//		dayBook.setNarration(custname.getText() + salesTransHdr.getVoucherNumber());
//		dayBook.setSourceVoucheNumber(salesTransHdr.getVoucherNumber());
//		dayBook.setSourceVoucherType("SALES");
//		dayBook.setCrAccountName("SALES ACCOUNT");
//		dayBook.setCrAmount(salesTransHdr.getInvoiceAmount());
//
//		LocalDate rdate = SystemSetting.utilToLocaDate(salesTransHdr.getVoucherDate());
//		dayBook.setsourceVoucherDate(java.sql.Date.valueOf(rdate));
//		ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
		logger.info("====Whole Sale FINAL SAVE COMPLETED STARTED TO PRINT JASPER!!======");
		Format formatter;
		logger.info("Whole Sale started jasper print!!");
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(salesTransHdr.getVoucherDate());

		// Version 1.6

		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtPaidamount.setText("");
		txtSBICard.setText("");
		txtSodexoCard.setText("");
		txtYesCard.setText("");
		custname.setText("");
		custAdress.setText("");
		gstNo.setText("");
		saleListTable.clear();
		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");

		custId = null;
		cmbSaleType.getSelectionModel().clearSelection();
		txtPriceType.clear();
		txtItemcode.setText("");
		txtBatch.setText("");
		txtBarcode.requestFocus();
		txtAmtAftrDiscount.clear();
		txtDiscount.clear();
		txtDiscountAmt.clear();
		txtLocalCustomer.clear();
		txtLocalCustomer.setDisable(true);
		localCustId = null;
		// Version 1.6 ends
		String vno = salesTransHdr.getVoucherNumber();
		salesTransHdr = null;
		salesDtl = null;
		try {
			NewJasperPdfReportService.TaxInvoiceReport(vno, strDate);
		} catch (JRException e) {
			e.printStackTrace();
			logger.info("Whole Sale " + e);
		}
		logger.info("===========Whole Sale jasper print completed!!====================");
		// ---------------version 5.0 surya
		txtPreviousBalance.clear();

		// ---------------version 5.0 surya end

		// Version 1.6 Comment states
		/*
		 * txtcardAmount.setText(""); txtCashtopay.setText("");
		 * txtPaidamount.setText(""); txtSBICard.setText(""); txtSodexoCard.setText("");
		 * txtYesCard.setText(""); custname.setText(""); custAdress.setText("");
		 * gstNo.setText(""); saleListTable.clear(); txtItemname.setText("");
		 * txtBarcode.setText(""); txtQty.setText(""); txtRate.setText("");
		 * 
		 * custId = null; cmbSaleType.getSelectionModel().clearSelection();
		 * txtPriceType.clear(); txtItemcode.setText(""); txtBatch.setText("");
		 * txtBarcode.requestFocus(); txtAmtAftrDiscount.clear(); txtDiscount.clear();
		 * txtDiscountAmt.clear();
		 * 
		 * txtLocalCustomer.clear(); txtLocalCustomer.setDisable(true); localCustId =
		 * null;
		 */
		// Version 1.6 Comment ends

		logger.info("==========Whole Sale jasper print completed!!=============");
		logger.info("Whole Sale EXIT FROM FINAL SAVE!!");

		txtCreditPeriod.setVisible(false);
		lblCreditPeriod.setVisible(false);
		btnSave.setDisable(false);
		cmbStore.getSelectionModel().clearSelection();

		txtCreditPeriod.setVisible(false);

	}

	private void saveSalesProperty() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SalesPropertyTransHdr.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			SalesPropertyTransHdrCtl itemStockPopupCtl = fxmlLoader.getController();
			itemStockPopupCtl.salesTransHdrId = salesTransHdr.getId();
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.WINDOW_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
//			stage.show();
			stage.showAndWait();
		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	@FXML
	void save(ActionEvent event) {

		finalSave();
		// salesTransHdr = null;
		// salesDtl = null;
	}

	@FXML
	void EnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			// if(event.getSource());

			txtQty.requestFocus();
		}

	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtItemname.requestFocus();
			showPopup();
		}
		if (event.getCode() == KeyCode.ENTER) {
			if (txtBarcode.getText().trim().isEmpty())
				btnSave.requestFocus();
		}
		if (event.getCode() == KeyCode.DOWN) {
			itemDetailTable.requestFocus();
		}
	}

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnSave.setDisable(false);

			Double change = Double.parseDouble(txtPaidamount.getText()) - Double.parseDouble(txtCashtopay.getText());
			txtChangeamount.setText(Double.toString(change));

		}

	}

	@FXML
	void calcPaid(KeyEvent event) {

	}

	@FXML
	void unHold(ActionEvent event) {
		loadHoldedSales();

		txtCreditPeriod.setVisible(true);

	}

	@Subscribe
	public void popupCustomerlistner(HoldedCustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			custname.setText(customerEvent.getCustomerName());
			custAdress.setText(customerEvent.getAddress());
			gstNo.setText(customerEvent.getCustGst());
			custId = customerEvent.getCustomerId();

			String customerSite = SystemSetting.customer_site_selection;
			if (customerSite.equalsIgnoreCase("TRUE")) {
				txtLocalCustomer.setDisable(false);
				txtLocalCustomer.clear();
			}

			ResponseEntity<PriceDefenitionMst> priceDef = RestCaller.getPriceNameById(customerEvent.getCustPriceTYpe());
			if (null != priceDef.getBody()) {
				txtPriceType.setText(priceDef.getBody().getPriceLevelName());
			}
			salesTransHdr = RestCaller.getSalesTransHdr(customerEvent.getHdrId());
			cmbSaleType.setValue(salesTransHdr.getVoucherType());
			cmbSaleType.getSelectionModel().select(salesTransHdr.getVoucherType());
			saleListTable.clear();
			ResponseEntity<List<SalesDtl>> respentity = RestCaller.getSalesDtl(salesTransHdr);
			saleListTable = FXCollections.observableArrayList(respentity.getBody());
			FillTable();
			cmbSaleType.getSelectionModel().select(salesTransHdr.getVoucherType().toString());

		}

	}

	private Boolean confirmMessage() {

		return null;
	}

	private void deleteAllSalesDtl() {

		ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
		List<SalesDtl> saledtlList = saledtlSaved.getBody();
		for (SalesDtl sales : saledtlList) {
			RestCaller.deleteSalesDtl(sales.getId());
		}

		ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
		saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
		if (saleListTable.size() == 0) {
			RestCaller.deleteSalesTransHdr(salesTransHdr.getId());
			salesTransHdr = null;
			saleListTable.clear();
			itemDetailTable.getItems().clear();
		}

	}

	private void loadHoldedSales() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/HoldedCustomer.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtBarcode.requestFocus();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	private void FillTable() {

		logger.info("=============Whole Sale started fill table!!=============");
		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		// columnCessRate.setCellValueFactory(cellData ->
		// cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Summary summary = null;

		if (null == salesTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			summary = RestCaller.getSalesWindowSummaryDiscount(salesTransHdr.getId());
		} else {
			summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
		}
		if (null != summary.getTotalAmount()) {
			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
		}
		logger.info("Whole Sale jasper fill table completed!!");
	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();
	}

	private void addItem() {

		// version2.0
		/// check financial year
		btnSave.setDisable(false);
		ResponseEntity<List<FinancialYearMst>> getFinancialYear = RestCaller.getAllFinancialYear();
		if (getFinancialYear.getBody().size() == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}
		int count = RestCaller.getFinancialYearCount();
		if (count == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}
		// version2.0ends
		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("DAY_END_LOCKED_IN_WHOLESALE");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("YES")) {
				ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
				DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
				System.out.println("Sys Date before add" + SystemSetting.applicationDate);
				if (null != dayEndClosureHdr) {
					if (null != dayEndClosureHdr.getDayEndStatus()) {
						String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(),
								"yyyy-MM-dd");
						String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
						java.sql.Date prDate = java.sql.Date.valueOf(process_date);
						Date sDate = java.sql.Date.valueOf(sysdate);
						int i = prDate.compareTo(sDate);
						if (i > 0 || i == 0) {
							notifyMessage(1, " Day End Already Done for this Date !!!!");
							return;
						}
					}
				}

			}
			Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.applicationDate);

			if (!dayendtodone) {
				notifyMessage(1, "Day End should be done before changing the date !!!!");
				return;
			}
			System.out.println("Sys Date before add" + SystemSetting.applicationDate);
		}

		logger.info("Inside Whole Sale add item");

		if (txtQty.getText().trim().isEmpty()) {

			notifyMessage(5, " Please Enter Quantity...!!!");
			return;

		}
		if (custname.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Enter Customer Name...!!!");
			custname.requestFocus();
			return;
		}
		if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Select Item Name...!!!");
			txtItemname.requestFocus();
			return;
		}
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Type Quantity...!!!");
			txtQty.requestFocus();
			return;
		}

		if (txtBatch.getText().trim().isEmpty() && SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("YES")) {

			notifyMessage(1, "Item Batch is not present!!!");
			txtBatch.requestFocus();
			return;

		}

		if (null == cmbSaleType.getValue()) {
			notifyMessage(5, " Please Select Sale Type...!!!");
			cmbSaleType.requestFocus();
			return;
		}

		if (SystemSetting.CUSTOMER_CDREDIT_PERIOD.equalsIgnoreCase("YES")) {
			if (txtCreditPeriod.getText().trim().isEmpty() || (txtCreditPeriod.getText()).equalsIgnoreCase("0")) {
				notifyMessage(5, " Please Enter Credit Period...!!!");
				cmbSaleType.requestFocus();
				return;
			}
		}

		ResponseEntity<UnitMst> getUnitBYItem = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

		if (!txtBarcode.getText().trim().isEmpty()) {
			if (!txtBarcode.getText().equalsIgnoreCase(getItem.getBody().getBarCode())) {
				notifyMessage(1, "Sorry... invalid item barcode!!!");
				txtBarcode.requestFocus();
				return;
			}

		}
		// ----------------------------new version 2 surya

		Double chkQty = 0.0;
		String itemId = getItem.getBody().getId();

		if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("YES") || !txtBatch.getText().trim().isEmpty()) {
			// ----------------------------new version 2 surya end

			ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId());
			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				if (null == getmulti.getBody()) {
					notifyMessage(5, "Please Add the item in Multi Unit");
					return;
				}
			}
			ArrayList items = new ArrayList();
			items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());

//			chkQty = RestCaller.getQtyFromItemBatchDtlDailyByItemIdAndQty(getItem.getBody().getId(),
//					txtBatch.getText());
			// -----------------------------------sharon ------------------------------

			chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(), txtBatch.getText(),
					storeNameFromPopUp);

			Iterator itr = items.iterator();
			while (itr.hasNext()) {
				List element = (List) itr.next();
//			chkQty = (Double) element.get(4);
				itemId = (String) element.get(7);

			}

			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
						getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
						Double.parseDouble(txtQty.getText()));
				if (chkQty < conversionQty && (!SystemSetting.isNEGATIVEBILLING())) {
					notifyMessage(1, "Not in Stock!!!");
					txtQty.clear();
					txtItemname.clear();
					txtBarcode.clear();
					txtBatch.clear();
					txtRate.clear();
					txtBarcode.requestFocus();
					return;
				}
			} else if (chkQty < Double.parseDouble(txtQty.getText()) && (!SystemSetting.isNEGATIVEBILLING())) {
				notifyMessage(5, "Not in Stock!!!");
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
			// ----------------------------new version 2 surya

		}
		// ----------------------------new version 2 surya end

		if (null == salesTransHdr) {
			createSalesTransHdr();
		}

		// ----------------------------new version 2 surya

		if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("NO") && txtBatch.getText().trim().isEmpty()
				&& (!SystemSetting.isNEGATIVEBILLING())) {

			ShowItemStockPopup(salesTransHdr, getItem.getBody().getId());

			ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

			List<SalesDtl> salesDtlList = respentityList.getBody();
			saleListTable.clear();
			saleListTable.setAll(salesDtlList);
			FillTable();

			txtItemname.setText("");
			txtBarcode.setText("");
			txtQty.setText("");
			txtRate.setText("");

			cmbUnit.getSelectionModel().clearSelection();
			txtItemcode.setText("");
			txtBatch.setText("");
			txtBarcode.requestFocus();
			txtWarranty.clear();
			salesDtl = new SalesDtl();
			return;
		}

		// ----------------------------new version 2 surya end

		String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();

		Double itemsqty = 0.0;
		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
				getItem.getBody().getId(), batch);
		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
		if (saleListItemTable.size() > 1) {
			Double PrevQty = 0.0;
			for (SalesDtl saleDtl : saleListItemTable) {
				if (!saleDtl.getUnitId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
							getItem.getBody().getUnitId(), saleDtl.getQty());
				} else {
					PrevQty = saleDtl.getQty();
				}
				itemsqty = itemsqty + PrevQty;
			}
		} else {
			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId, txtBatch.getText());
		}
		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if (chkQty < itemsqty + conversionQty && (!SystemSetting.isNEGATIVEBILLING())) {
				notifyMessage(1, "Not in Stock!!!");
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
		}

		else if (chkQty < itemsqty + Double.parseDouble(txtQty.getText()) && (!SystemSetting.isNEGATIVEBILLING())) {
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();
			notifyMessage(5, "No Stock!!");
			return;
		}
		if (null == salesDtl) {
			salesDtl = new SalesDtl();
		}
		if (null != salesDtl.getId()) {

			RestCaller.deleteSalesDtl(salesDtl.getId());

			logger.info("Whole Sale delete sales Dtl completed!!");

			if (null != salesDtl.getSchemeId()) {
				return;
			}

		}

		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(txtItemname.getText());
		salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());

		System.out.println(batch);
		salesDtl.setBatchCode(batch);

		logger.info("Whole Sale amount Calculation Started!!");
		Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());

		salesDtl.setQty(qty);

		salesDtl.setItemCode(txtItemcode.getText());
		Double mrpRateIncludingTax = 00.0;
		logger.info("Fetching tax");
		mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
		logger.info("Fetching tax finished");
		salesDtl.setMrp(mrpRateIncludingTax);

		Double taxRate = 00.0;

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
		salesDtl.setBarcode(item.getBarCode());
//		salesDtl.setUnitName(unitMst.getUnitName());
		salesDtl.setStandardPrice(item.getStandardPrice());

		salesDtl.setListPrice(mrpRateIncludingTax);

		if (null == salesTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double discount = 0.0;
			discount = mrpRateIncludingTax
					- (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			salesDtl.setListPrice(discount);
		}

//		if(null != item.getTaxRate())
//		{
//			salesDtl.setTaxRate(item.getTaxRate());
//			taxRate = item.getTaxRate();
//
//		} else {
//			salesDtl.setTaxRate(0.0);
//		}
		salesDtl.setItemId(item.getId());

		if (!txtBatch.getText().trim().isEmpty()) {
			ResponseEntity<List<ItemBatchExpiryDtl>> batchExpiryDtlResp = RestCaller
					.getItemBatchExpByItemAndBatch(salesDtl.getItemId(), salesDtl.getBatch());

			List<ItemBatchExpiryDtl> itemBatchExpiryDtlList = batchExpiryDtlResp.getBody();
			if (itemBatchExpiryDtlList.size() > 0 && null != itemBatchExpiryDtlList) {
				ItemBatchExpiryDtl itemBatchExpiryDtl = itemBatchExpiryDtlList.get(0);

				if (null != itemBatchExpiryDtl) {
					String expirydate = SystemSetting.UtilDateToString(itemBatchExpiryDtl.getExpiryDate(),
							"yyyy-MM-dd");
					salesDtl.setExpiryDate(java.sql.Date.valueOf(expirydate));
				}
			}
		}

		ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
		salesDtl.setUnitId(getUnit.getBody().getId());
		salesDtl.setUnitName(getUnit.getBody().getUnitName());

		// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());

		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.getBody().size() > 0) {
			for (TaxMst taxMst : getTaxMst.getBody()) {

				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {
					logger.info(e.toString());

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//						BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//						BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
					ResponseEntity<TaxMst> taxMst1 = RestCaller.getTaxMstByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
					if (null != taxMst1.getBody()) {

						salesDtl.setTaxRate(taxMst1.getBody().getTaxRate());
					}
				} else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesDtl.setCgstTaxRate(0.0);
						salesDtl.setCgstAmount(0.0);
						salesDtl.setSgstTaxRate(0.0);
						salesDtl.setSgstAmount(0.0);

						salesDtl.setTaxRate(taxMst.getTaxRate());
						salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//							BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setIgstAmount(igstAmount.doubleValue());
					}
				}
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesDtl.setCessRate(taxMst.getTaxRate());
//						BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCessAmount(cessAmount.doubleValue());
					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtl.setAddCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//							Double.valueOf(txtRate.getText()));
//					salesDtl.setAddCessAmount(cessAmount.doubleValue());
				}

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax)
					/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate()
							+ salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
			BigDecimal addcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
		}

		else {

// verificed

			if (getUnit.getBody().getId().equalsIgnoreCase(item.getUnitId())) {
				salesDtl.setStandardPrice(item.getStandardPrice());
			} else {

				ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),
						getUnit.getBody().getId());
				MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
				salesDtl.setStandardPrice(multiUnitMst.getPrice());

			}
			if (null != item.getTaxRate()) {
				salesDtl.setTaxRate(item.getTaxRate());
				taxRate = item.getTaxRate();

			} else {
				salesDtl.setTaxRate(0.0);
			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);

			// if Discount

			// Calculate discount on base price
			if (null != salesTransHdr.getAccountHeads().getDiscountProperty()) {

				if (salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE")) {
					calcDiscountOnBasePrice(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);

				}
				if (salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP")) {
//				salesDtl.setTaxRate(0.0);
					calcDiscountOnMRP(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);
				}
				if (salesTransHdr.getAccountHeads().getDiscountProperty()
						.equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX")) {
					ambrossiaDiscount(salesTransHdr, rateBeforeTax, item, mrpRateIncludingTax, taxRate);
				}
			} else {

				double cessAmount = 0.0;
				double cessRate = 0.0;
				salesDtl.setRate(rateBeforeTax);
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (item.getCess() > 0) {
						cessRate = item.getCess();
						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

						salesDtl.setRate(rateBeforeTax);
						cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;
					} else {
						cessAmount = 0.0;
						cessRate = 0.0;
					}
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setCessRate(cessRate);
					salesDtl.setCessAmount(cessAmount);
					salesDtl.setRateBeforeDiscount(rateBeforeTax);
				}

				// salesDtl.setStandardPrice(Double.parseDouble(txtRate.getText()));
				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesDtl.setCgstTaxRate(cgstTaxRate);

				salesDtl.setSgstTaxRate(sgstTaxRate);
				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {
					logger.info(e.toString());

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					salesDtl.setSgstTaxRate(taxRate / 2);

					salesDtl.setCgstTaxRate(taxRate / 2);

					Double cgstAmt = 0.0, sgstAmt = 0.0;
					cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
					bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
					sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
					bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);

					salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);

				} else {
					salesDtl.setSgstTaxRate(0.0);

					salesDtl.setCgstTaxRate(0.0);

					salesDtl.setCgstAmount(0.0);

					salesDtl.setSgstAmount(0.0);

					salesDtl.setIgstTaxRate(taxRate);
					salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				}
			}
		}

		String companyState = SystemSetting.getUser().getCompanyMst().getState();
		String customerState = "KERALA";
		try {
			customerState = salesTransHdr.getAccountHeads().getCustomerState();
		} catch (Exception e) {
			logger.info(e.toString());

		}

		if (null == customerState) {
			customerState = "KERALA";
		}

		if (null == companyState) {
			companyState = "KERALA";
		}
		String gstType = RestCaller.getGSTTypeByCustomerState(customerState);
		if (gstType.equalsIgnoreCase("GST")) {
			salesDtl.setSgstTaxRate(taxRate / 2);

			salesDtl.setCgstTaxRate(taxRate / 2);

			Double cgstAmt = 0.0, sgstAmt = 0.0;
			cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
			bdcgstAmt = bdcgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setCgstAmount(bdcgstAmt.doubleValue());
			sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
			BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
			bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

			salesDtl.setIgstTaxRate(0.0);
			salesDtl.setIgstAmount(0.0);

		} else {
			salesDtl.setSgstTaxRate(0.0);

			salesDtl.setCgstTaxRate(0.0);

			salesDtl.setCgstAmount(0.0);

			salesDtl.setSgstAmount(0.0);

			salesDtl.setIgstTaxRate(taxRate);
			salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		}

		BigDecimal settoamount = new BigDecimal((Double.parseDouble(txtQty.getText()) * salesDtl.getRate()));

		double includingTax = (settoamount.doubleValue() * salesDtl.getTaxRate()) / 100;
		double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(setamount.doubleValue());
//		}
//		else
//		{
//			BigDecimal settoamount = new BigDecimal(
//					Double.parseDouble(txtQty.getText()) *salesDtl.getMrp() );
//			settoamount = settoamount.setScale(2, BigDecimal.ROUND_CEILING);
//			salesDtl.setAmount(settoamount.doubleValue());
//		}
//		if (getUnit.getBody().getId().equalsIgnoreCase(item.getUnitId())) {
//			salesDtl.setStandardPrice(item.getStandardPrice());
//		} else {
//
//			ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),
//					getUnit.getBody().getId());
//			MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
//			salesDtl.setStandardPrice(multiUnitMst.getPrice());
//
//		}
		salesDtl.setAddCessRate(0.0);
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp1 = RestCaller.getPriceDefenitionMstByName("MRP");
		PriceDefenitionMst priceDefenitionMst1 = priceDefenitionMstResp1.getBody();
		if (null != priceDefenitionMst1) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			/*
			 * fetch for batchprice definition
			 */
			ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller.getBatchPriceDefinition(
					salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(), salesDtl.getBatch(),
					sdate);
			if (null != batchpriceDef.getBody()) {
				salesDtl.setMrp(batchpriceDef.getBody().getAmount());
			} else {
				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();
				if (null != priceDefinition) {
					salesDtl.setMrp(priceDefinition.getAmount());
				}
			}
		}

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if (null != priceDefenitionMst) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller.getBatchPriceDefinition(
					salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), salesDtl.getBatch(), sdate);
			if (null != batchpriceDef.getBody()) {
				salesDtl.setCostPrice(batchpriceDef.getBody().getAmount());
			} else {
				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();
				if (null != priceDefinition) {
					salesDtl.setCostPrice(priceDefinition.getAmount());
				}
			}
		}

		// -------------------new version 1.4

		if (!txtWarranty.getText().trim().isEmpty()) {
			salesDtl.setWarrantySerial(txtWarranty.getText());
		}

		// -------------------new version 1.4 end

		// ===============added by anandu for the purpose of storewise stock
		// updation=====29-07-2021====
		if (!txtStore.getText().trim().isEmpty()) {
			salesDtl.setStore(txtStore.getText());
		}
		// ==========end=================

		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		salesDtl = respentity.getBody();

//		//---------------offer---------------

		logger.info("Whole Sale Save sals Dtl COMPLETED!!");
		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());

		List<SalesDtl> salesDtlList = respentityList.getBody();

		/*
		 * Call Rest to get the summary and set that to the display fields
		 */

		// salesDtl.setTempAmount(amount);
		saleListTable.clear();
		saleListTable.setAll(salesDtlList);

		// ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		// salesDtl = respentity.getBody();

		// saleListTable.add(salesDtl);

		FillTable();

		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");

		txtStore.setText("");

		cmbUnit.getSelectionModel().clearSelection();
		txtItemcode.setText("");
		txtBatch.setText("");
		txtBarcode.requestFocus();
		txtWarranty.clear();
		salesDtl = new SalesDtl();
		logger.info("====================Whole Sale ADD ITEM FINISHED!!================");

	}

	// ----------------------------new version 2 surya

	private void ShowItemStockPopup(SalesTransHdr salesTransHdr, String itemid) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemWiseStockPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			ItemWiseStockPopupCtl itemWiseStockPopupCtl = fxmlLoader.getController();
			itemWiseStockPopupCtl.LoadItemPopupBySearch(itemid, salesTransHdr);

			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}
	// ----------------------------new version 2 surya end

	private void ambrossiaDiscount(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {

		System.out.print("mrpRateIncludingTax" + mrpRateIncludingTax);

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			double discountAmount = (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

//		 

			System.out.println("BrateAfterDiscount*****************" + BrateAfterDiscount);
			System.out.println("discountAmount*****************" + discountAmount);
//		
			salesDtl.setDiscount(discountAmount);

			salesDtl.setRateBeforeDiscount(rateBeforeTax);

			double newRate = BrateAfterDiscount.doubleValue();
			BigDecimal BnewRate = new BigDecimal(newRate);
			BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate));

			BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			salesDtl.setRate(BrateBeforeTax.doubleValue());

			// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
			// salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());
		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				double discountAmount = (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount())
						/ 100;
				BigDecimal BrateAfterDiscount = new BigDecimal(mrpRateIncludingTax - discountAmount);

				BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

//			
				salesDtl.setDiscount(discountAmount);

				double newRate = BrateAfterDiscount.doubleValue();
				BigDecimal BnewRate = new BigDecimal(newRate);
				BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				BigDecimal BrateBeforeTax = new BigDecimal((newRate * 100) / (100 + taxRate + cessRate));

				BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

				salesDtl.setRate(BrateBeforeTax.doubleValue());

				// salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
				// salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());

				System.out.println("rateBeforeTax---------" + BrateBeforeTax);

//				if (salesTransHdr.getCustomerMst().getCustomerDiscount() > 0) {
//					Double rateAfterDiscount = (100 * rateBeforeTax)
//							/ (100 + salesTransHdr.getCustomerMst().getCustomerDiscount());
//					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
//					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					
//					
//					salesDtl.setRate(BrateAfterDiscount.doubleValue());
//					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
//					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
//					// salesDtl.setMrp(rateBefrTax.doubleValue());
//					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
//				} else {
//					salesDtl.setRate(rateBeforeTax);
//				}

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

	}

	private void calcDiscountOnBasePrice(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * rateBeforeTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
			// salesDtl.setMrp(rateBefrTax.doubleValue());
			// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * rateBeforeTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(rateBefrTax.doubleValue());
					// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setRateBeforeDiscount(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

	}

	private void calcDiscountOnMRP(SalesTransHdr salesTransHdr, Double rateBeforeTax, ItemMst item,
			Double mrpRateIncludingTax, double taxRate) {
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double rateAfterDiscount = (100 * mrpRateIncludingTax)
					/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);

			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BnewrateBeforeTax.doubleValue());
			salesDtl.setRateBeforeDiscount(rateBeforeTax);

		} else {
			salesDtl.setRate(rateBeforeTax);
			salesDtl.setRateBeforeDiscount(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);

				if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
					Double rateAfterDiscount = (100 * mrpRateIncludingTax)
							/ (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					// salesDtl.setMrp(mrpRateIncludingTax);
					// salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				} else {
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setRateBeforeDiscount(rateBeforeTax);
				}
				// salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
		salesDtl.setRateBeforeDiscount(rateBeforeTax);

	}

	private void showPopup() {

		// ----------------------------new version 2 surya

		if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("YES")) {

			// ----------------------------new version 2 surya end

			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
				// fxmlLoader.setController(itemStockPopupCtl);
				Parent root1;
				ItemStockPopupCtl itemStockPopupCtl = fxmlLoader.getController();
				itemStockPopupCtl.windowName = "WHOLESALE";
				root1 = (Parent) fxmlLoader.load();
				if (!custname.getText().trim().isEmpty()) {
					ItemStockPopupCtl itemStockPopupCtl1 = fxmlLoader.getController();
					itemStockPopupCtl1.getCustomer(custname.getText());
				}

				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} catch (IOException e) {
				//
				e.printStackTrace();
			}
			// ----------------------------new version 2 surya

		} else {
			try {
				System.out.println("STOCK POPUP");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				// loader.setController(itemPopupCtl);
				Parent root = loader.load();
				Stage stage = new Stage();
				stage.setScene(new Scene(root));
				// stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				txtQty.requestFocus();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// ----------------------------new version 2 surya end

		txtQty.requestFocus();
	}

	private void loadCustomerPopup() {

		if (salesTransHdr != null) {

			Alert a = new Alert(AlertType.CONFIRMATION);
			a.setHeaderText("Changing Customer...");
			a.setContentText("The details will be deleted");
			a.showAndWait().ifPresent((btnType) -> {
				if (btnType == ButtonType.OK) {

					deleteAllSalesDtl();

					try {
						txtLocalCustomer.clear();
//						FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
						FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CusstomerNewPopUp.fxml"));
						Parent root1;

						root1 = (Parent) fxmlLoader.load();
						Stage stage = new Stage();

						stage.initModality(Modality.APPLICATION_MODAL);
						stage.initStyle(StageStyle.UNDECORATED);
						stage.setTitle("ABC");
						stage.initModality(Modality.APPLICATION_MODAL);
						stage.setScene(new Scene(root1));
						stage.show();

						txtItemname.requestFocus();

					} catch (IOException e) {
						//
						e.printStackTrace();
					}

				} else if (btnType == ButtonType.CANCEL) {

					return;

				}
			});
//			Boolean confirm = confirmMessage();

		} else {
			try {

				txtLocalCustomer.clear();
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CusstomerNewPopUp.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

				txtItemname.requestFocus();

			} catch (IOException e) {
				//
				e.printStackTrace();
			}
		}
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */

	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
		// ----------------------------new version 2 surya

		if (SystemSetting.SHOWSTOCKPOPUP.equalsIgnoreCase("YES")) {
			// ----------------------------new version 2 surya end

			double applicableDiscount = 0.0;
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
			ItemMst item = new ItemMst();
			item = getItem.getBody();

//			item.setRank(item.getRank() + 1);
//			RestCaller.updateRankItemMst(item);
			Stage stage = (Stage) btnAdditem.getScene().getWindow();

			if (stage.isShowing()) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						cmbUnit.getItems().clear();

						Platform.runLater(() -> {
							itemNameProperty.set(itemPopupEvent.getItemName());

						});

						Platform.runLater(() -> {
							txtBarcode.setText(itemPopupEvent.getBarCode());

						});
						Platform.runLater(() -> {
							if (null != itemPopupEvent.getStoreName()) {
								storeNameFromPopUp = itemPopupEvent.getStoreName();
								txtStore.setText(itemPopupEvent.getStoreName());
							} else {
								storeNameFromPopUp = "MAIN";
							}

						});
						Platform.runLater(() -> {
							txtRate.setText(Double.toString(itemPopupEvent.getMrp()));

						});
						Platform.runLater(() -> {
							cmbUnit.getItems().add(itemPopupEvent.getUnitName());
							cmbUnit.setValue(itemPopupEvent.getUnitName());
						});

						Platform.runLater(() -> {
							batchProperty.set(itemPopupEvent.getBatch());

						});

						/*
						 * new url for getting customer details from account heads by Account
						 * id====05-01-2022
						 */
//						ResponseEntity<CustomerMst> custMst = RestCaller.getCustomerById(custId);
						ResponseEntity<AccountHeads> accountHead = RestCaller.getAccountHeadsById(custId);

						Date udate = SystemSetting.getApplicationDate();
						String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

						ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
								itemPopupEvent.getItemId(), accountHead.getBody().getPriceTypeId(),
								itemPopupEvent.getUnitId(), itemPopupEvent.getBatch(), sdate);
						if (null != batchPriceDef.getBody()) {
							Platform.runLater(() -> {
								txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));

							});

						} else {
							ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
									itemPopupEvent.getItemId(), accountHead.getBody().getPriceTypeId(),
									itemPopupEvent.getUnitId(), sdate);

							if (null != pricebyItem.getBody()) {

								// version 2.3
								Platform.runLater(() -> {
									txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));

								});
								// version2.3ends

								ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp2 = RestCaller
										.getPriceDefenitionMstByName("MRP");
								if (null != priceDefenitionMstResp2.getBody()) {
									if (!pricebyItem.getBody().getPriceId()
											.equalsIgnoreCase(priceDefenitionMstResp2.getBody().getId()))
										Platform.runLater(() -> {
											txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));

										});

//						else
//						{
//							txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
//						}
								}
							}
						}
						ResponseEntity<List<BatchPriceDefinition>> batchPrice = RestCaller
								.getBatchPriceDefinitionByItemId(itemPopupEvent.getItemId(),
										accountHead.getBody().getPriceTypeId(), txtBatch.getText(), sdate);
						BatchpriceDefenitionList = FXCollections.observableArrayList(batchPrice.getBody());
						if (BatchpriceDefenitionList.size() > 0) {
							for (BatchPriceDefinition priceDef : BatchpriceDefenitionList) {
								ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
								if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {

									Platform.runLater(() -> {
										cmbUnit.getItems().add(getUnit.getBody().getUnitName());
										cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
									});

								}
							}
						}

						else {

							ResponseEntity<List<PriceDefinition>> price = RestCaller.getPriceByItemId(
									itemPopupEvent.getItemId(), accountHead.getBody().getPriceTypeId());
							priceDefenitionList = FXCollections.observableArrayList(price.getBody());

							if (priceDefenitionList.size() > 0) {
								// cmbUnit.getItems().clear();
								for (PriceDefinition priceDef : priceDefenitionList) {
									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
									if (!getUnit.getBody().getUnitName()
											.equalsIgnoreCase(itemPopupEvent.getUnitName())) {
										Platform.runLater(() -> {
											cmbUnit.getItems().add(getUnit.getBody().getUnitName());
											cmbUnit.getSelectionModel().select(itemPopupEvent.getUnitName());
										});

									}
								}
							}
						}

						ResponseEntity<TaxMst> taxMst = RestCaller.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(),
								"IGST");
						if (null != taxMst.getBody()) {
						}

						if (null != itemPopupEvent.getExpiryDate()) {
							// salesDtl.setexpiryDate(itemPopupEvent.getExpiryDate());
						}

//				if(custMst.getBody().getCustomerDiscount()>0)
//				{
//					double discount;
//					discount =Double.parseDouble(txtRate.getText())*(custMst.getBody().getCustomerDiscount()/100);
//					applicableDiscount = Double.parseDouble(txtRate.getText())-discount;
//					txtRate.setText(Double.toString(applicableDiscount));
//				}
//				
						if (null != itemPopupEvent.getItemPriceLock()) {
							if (itemPopupEvent.getItemPriceLock().equalsIgnoreCase("YES")) {

								Platform.runLater(() -> {
									txtRate.setEditable(false);

								});
							} else {

								Platform.runLater(() -> {
									txtRate.setEditable(true);

								});
							}
						}

						System.out.println(itemPopupEvent.toString());
					}
				});
			}
			// ----------------------------new version 2 surya

		} else {

			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
			ItemMst item = getItem.getBody();
			if (null != item.getItemName()) {
				Platform.runLater(() -> {
					txtItemname.setText(item.getItemName());

				});

			}

			if (null != item.getBarCode()) {
				Platform.runLater(() -> {
					txtBarcode.setText(item.getBarCode());

				});

			}

			if (null != itemPopupEvent.getStoreName()) {
				storeNameFromPopUp = itemPopupEvent.getStoreName();
				txtStore.setText(itemPopupEvent.getStoreName());
			} else {
				storeNameFromPopUp = "MAIN";
			}

			ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(item.getUnitId());
			UnitMst unitMst = unitMstResp.getBody();
			if (null != unitMst) {
				if (null != cmbUnit) {

					Platform.runLater(() -> {
						// weightfromMachine.set( voucherNoEvent.getWeightData().trim());
						cmbUnit.getItems().add(unitMst.getUnitName());
					});

					if (null != cmbUnit.getSelectionModel()) {
						if (null != unitMst.getUnitName()) {

							Platform.runLater(() -> {
								// weightfromMachine.set( voucherNoEvent.getWeightData().trim());
								cmbUnit.getSelectionModel().select(unitMst.getUnitName());
							});

						}
					}
				}
			}

			if (null != itemPopupEvent.getMrp()) {

				Platform.runLater(() -> {
					// weightfromMachine.set( voucherNoEvent.getWeightData().trim());
					txtRate.setText(item.getStandardPrice().toString());
				});

			}

			ArrayList itemBatch = new ArrayList();
			String logDate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
			itemBatch = RestCaller.SearchStockItemByBatchAndItemId(item.getId(), logDate);

			if (itemBatch.size() == 1) {
				Iterator itr = itemBatch.iterator();
				while (itr.hasNext()) {
					List element = (List) itr.next();

					Platform.runLater(() -> {
						String batch = (String) element.get(9);
						txtBatch.setText(batch);
					});

				}

			}

		}

		// ----------------------------new version 2 surya end

	}

	/*
	 * @Subscribe public void popupCustomerlistner(CustomerEvent customerEvent) {
	 * 
	 * ResponseEntity<AccountHeads> getCustomer =
	 * RestCaller.getAccountHeadsById(customerEvent.getCustId()); AccountHeads
	 * custmrMst = new AccountHeads(); custmrMst = getCustomer.getBody(); //
	 * custmrMst.setRank(custmrMst.getRank() + 1); //
	 * RestCaller.updateCustomerRank(custmrMst);
	 * 
	 * Stage stage = (Stage) btnAdditem.getScene().getWindow(); if
	 * (stage.isShowing()) {
	 * 
	 * String customerSite = SystemSetting.customer_site_selection; if
	 * (customerSite.equalsIgnoreCase("TRUE")) { txtLocalCustomer.setDisable(false);
	 * txtLocalCustomer.clear(); }
	 * 
	 * String sdate =
	 * SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(),
	 * "yyyy-MM-dd"); custname.setText(customerEvent.getCustomerName());
	 * ResponseEntity<BranchMst> branchMst =
	 * RestCaller.getBranchMstByName(customerEvent.getCustomerName());
	 * 
	 * if (null != branchMst.getBody()) { txtSalesType.setText("Branch Sales"); }
	 * else { txtSalesType.setText("Customer Sales"); }
	 * custAdress.setText(customerEvent.getCustomerAddress());
	 * gstNo.setText(customerEvent.getCustomerGst()); custId =
	 * customerEvent.getCustId();
	 * 
	 * if (SystemSetting.CUSTOMER_CDREDIT_PERIOD.equalsIgnoreCase("YES")) {
	 * txtCreditPeriod.setVisible(true); lblCreditPeriod.setVisible(true);
	 * txtCreditPeriod.setText(customerEvent.getCreditPeriod().toString()); }
	 * 
	 * 
	 * new url for getting customer details from account heads by Account
	 * id====05-01-2022
	 * 
	 * // ResponseEntity<CustomerMst> custMst = RestCaller.getCustomerById(custId);
	 * ResponseEntity<AccountHeads> accountHeadssss =
	 * RestCaller.getAccountHeadsById(custId); ResponseEntity<PriceDefenitionMst>
	 * priceDef = RestCaller
	 * .getPriceNameById(accountHeadssss.getBody().getPriceTypeId()); if (null !=
	 * priceDef.getBody()) {
	 * txtPriceType.setText(priceDef.getBody().getPriceLevelName());
	 * 
	 * } else { txtPriceType.clear();
	 * 
	 * } ResponseEntity<BranchMst> branchMstResp =
	 * RestCaller.getBranchMstByName(customerEvent.getCustomerName());
	 * 
	 * if (null != branchMstResp.getBody()) { customerIsBranch = true; }
	 * 
	 * // ResponseEntity<T> Double custBalance =
	 * RestCaller.getCustomerBalance(custId, sdate); BigDecimal bCustBalance = new
	 * BigDecimal(custBalance); bCustBalance = bCustBalance.setScale(2,
	 * BigDecimal.ROUND_HALF_EVEN);
	 * txtPreviousBalance.setText(bCustBalance.toString()); }
	 * 
	 * }
	 */

	@Subscribe
	public void popupNewCustomerlistner(CustomerNewPopUpEvent customerNewPopUpEvent) {
		System.out.println("7777777777777777777777777777777" + customerNewPopUpEvent);
//		ResponseEntity<CustomerMst> getCustomer = RestCaller.getCustomerById(customerNewPopUpEvent.getCustomerId());
//		CustomerMst custmrMst = new CustomerMst();
//		custmrMst = getCustomer.getBody();
//		custmrMst.setRank(custmrMst.getRank() + 1);
//		RestCaller.updateCustomerRank(custmrMst);

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			String customerSite = SystemSetting.customer_site_selection;
			if (customerSite.equalsIgnoreCase("TRUE")) {
				txtLocalCustomer.setDisable(false);
				txtLocalCustomer.clear();
			}

			String sdate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
			custname.setText(customerNewPopUpEvent.getCustomerName());
			ResponseEntity<BranchMst> branchMst = RestCaller
					.getBranchMstByName(customerNewPopUpEvent.getCustomerName());

			if (null != branchMst.getBody()) {
				txtSalesType.setText("Branch Sales");
			} else {
				txtSalesType.setText("Customer Sales");
			}
			custAdress.setText(customerNewPopUpEvent.getCustomerAddress());
			gstNo.setText(customerNewPopUpEvent.getCustomerGst());
			custId = customerNewPopUpEvent.getCustomerId();

			if (SystemSetting.CUSTOMER_CDREDIT_PERIOD.equalsIgnoreCase("YES")) {
				txtCreditPeriod.setVisible(true);
				lblCreditPeriod.setVisible(true);
				txtCreditPeriod.setText(customerNewPopUpEvent.getCreditPeriod().toString());
			}

			/*
			 * new url for getting customer details from account heads by Account
			 * id====05-01-2022
			 */
//			ResponseEntity<CustomerMst> custMst = RestCaller.getCustomerById(custId);
			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsById(custId);
			ResponseEntity<PriceDefenitionMst> priceDef = RestCaller
					.getPriceNameById(accountHeads.getBody().getPriceTypeId());
			if (null != priceDef.getBody()) {
				txtPriceType.setText(priceDef.getBody().getPriceLevelName());

			} else {
				txtPriceType.clear();

			}
			ResponseEntity<BranchMst> branchMstResp = RestCaller
					.getBranchMstByName(customerNewPopUpEvent.getCustomerName());

			if (null != branchMstResp.getBody()) {
				customerIsBranch = true;
			}

			// ResponseEntity<T>
			Double custBalance = RestCaller.getCustomerBalance(custId, sdate);
			BigDecimal bCustBalance = new BigDecimal(custBalance);
			bCustBalance = bCustBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			txtPreviousBalance.setText(bCustBalance.toString());
		}

	}

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private boolean isCustomerABranch(String customerName) {
		ArrayList branchRest = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		branchRest = RestCaller.SearchBranchLocalhost();
		Iterator itr1 = branchRest.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			System.out.println("---branchRest---" + lm);
			String branchName = (String) lm.get("branchName");
			if (customerName.equalsIgnoreCase(branchName)) {
				return true;
			}

		}

		return false;
	}

	@FXML
	void searchAction(ActionEvent event) {
	}

	@FXML
	void itemNameClick(MouseEvent event) {
		if (null == cmbSaleType.getSelectionModel() || null == cmbSaleType.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Select Voucher Type");
			cmbSaleType.requestFocus();
			return;
		}
		if (custname.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Customer");
			custname.requestFocus();
			return;
		}

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/CategorywiseItemSearch.fxml"));
			Parent root1;
			CategorywiseItemSearchCtl categorywiseItemSearchCtl = loader.getController();
			categorywiseItemSearchCtl.customerName = custname.getText();
			if (null != cmbSaleType.getSelectionModel()) {
				categorywiseItemSearchCtl.voucherType = cmbSaleType.getSelectionModel().getSelectedItem();
			}
			categorywiseItemSearchCtl.customerIsBranch = customerIsBranch;
			categorywiseItemSearchCtl.txtLocalCustomer = txtLocalCustomer.getText();
			categorywiseItemSearchCtl.localCustId = localCustId;
			categorywiseItemSearchCtl.windowType = "WHOLESALE";
			if (null == salesTransHdr) {
				createSalesTransHdr();
				categorywiseItemSearchCtl.salesTransHdr = salesTransHdr;
			} else {
				categorywiseItemSearchCtl.salesTransHdr = salesTransHdr;
			}
			root1 = (Parent) loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root1));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}

	}

	@Subscribe
	public void categoryItemSearchListener(CategorywiseItemSearchEvent categorywiseItemSearchEvent) {

		{
			salesTransHdr = categorywiseItemSearchEvent.getSalesTransHdrId();
			ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

			List<SalesDtl> salesDtlList = respentityList.getBody();

			/*
			 * Call Rest to get the summary and set that to the display fields
			 */

			// salesDtl.setTempAmount(amount);
			saleListTable.clear();
			saleListTable.setAll(salesDtlList);
			FillTable();
		}

	}

	private void createSalesTransHdr() {

		salesTransHdr = new SalesTransHdr();
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.getId();

		salesTransHdr.setVoucherType(cmbSaleType.getSelectionModel().getSelectedItem());
		salesTransHdr.setCustomerId(custId);

		logger.info("===========Whole Sale get customer by Id in Add item!!");

		/*
		 * new url for getting customer details from account heads by Account
		 * id====05-01-2022
		 */
//		ResponseEntity<CustomerMst> customerResponce = RestCaller.getCustomerById(custId);
		ResponseEntity<AccountHeads> accountHeadsResponce = RestCaller.getAccountHeadsById(custId);

		AccountHeads accountHeads = accountHeadsResponce.getBody();
		if (null == accountHeads) {
			return;
		}
//		salesTransHdr.setCustomerMst(customerMst);
		salesTransHdr.setAccountHeads(accountHeads);

		if (null != accountHeads.getCustomerDiscount()) {
			salesTransHdr.setDiscount((accountHeads.getCustomerDiscount().toString()));
		} else {
			salesTransHdr.setDiscount("0");
		}

		if (!txtLocalCustomer.getText().trim().isEmpty()) {
			ResponseEntity<LocalCustomerMst> LocalCustomerMstResp = RestCaller.getLocalCustomerById(localCustId);
			LocalCustomerMst localCustomerMst = LocalCustomerMstResp.getBody();
			if (null != localCustomerMst) {
				salesTransHdr.setLocalCustomerMst(localCustomerMst);
			}

		}

		/*
		 * If Customer Has a valid GST , then B2B Invoice , otherwise its B2C
		 */
		if (null == accountHeads.getPartyGst() || accountHeads.getPartyGst().length() < 13) {
			salesTransHdr.setSalesMode("B2C");
		} else {
			salesTransHdr.setSalesMode("B2B");
		}

		salesTransHdr.setCreditOrCash("CREDIT");
		salesTransHdr.setCustomiseSalesMode("WHOLESALEWINDOW");
		salesTransHdr.setUserId(SystemSetting.getUser().getId());
		salesTransHdr.setBranchCode(SystemSetting.systemBranch);
		LocalDate ldate = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
		Date date = SystemSetting.applicationDate;
		salesTransHdr.setVoucherDate(date);
		if (customerIsBranch) {
			salesTransHdr.setIsBranchSales("Y");
		} else {
			salesTransHdr.setIsBranchSales("N");
		}
		logger.info("==============Whole Sale save Sales trans Hdr started!!");
		String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

		if (!txtCreditPeriod.getText().trim().isEmpty()) {
			salesTransHdr.setCreditPeriod(Integer.parseInt(txtCreditPeriod.getText()));
		}

		if (null != cmbSalesMan.getSelectionModel().getSelectedItem()) {
			ResponseEntity<List<SalesManMst>> salesManMstResp = RestCaller
					.getSalesManMstByName(cmbSalesMan.getSelectionModel().getSelectedItem().toString());

			List<SalesManMst> salesManList = salesManMstResp.getBody();

			if (salesManList.size() > 0) {
				if (null != salesManList.get(0)) {
					if (null != salesManList.get(0).getId()) {
						salesTransHdr.setSalesManId(salesManList.get(0).getId());

					} else {
						notifyMessage(3, "Invalid salesman...!");
						return;
					}

				} else {
					notifyMessage(3, "Invalid salesman...!");
					return;
				}

			} else {
				notifyMessage(3, "Invalid salesman...!");
				return;

			}
		}

		ResponseEntity<SalesTransHdr> getsales = RestCaller
				.getNullSalesTransHdrByCustomer(salesTransHdr.getCustomerId(), sdate, "WHOLESALEWINDOW");
		if (null != getsales.getBody()) {
			salesTransHdr = getsales.getBody();
		} else {
			ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
			logger.info("Whole Sale save Sales trans Hdr completed!!");
			salesTransHdr = respentity.getBody();
		}

	}

	@Subscribe
	public void batchWiseSalesListner(String sales) {
		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {

			if (sales.equalsIgnoreCase("SALES")) {
				ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

				List<SalesDtl> salesDtlList = respentityList.getBody();
				saleListTable.clear();
				saleListTable.setAll(salesDtlList);
				FillTable();
				txtItemname.setText("");
				txtBarcode.setText("");
				txtQty.setText("");
				txtRate.setText("");
				cmbUnit.getSelectionModel().clearSelection();
				txtItemcode.setText("");
				txtBatch.setText("");
				txtBarcode.requestFocus();
				txtWarranty.clear();
				salesDtl = new SalesDtl();
			}

		}

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

	}

	@FXML
	void clearAll(ActionEvent event) {

		cmbSaleType.getSelectionModel().clearSelection();
		txtLoginDate.clear();
		txtSalesType.clear();
		txtPriceType.clear();
		gstNo.clear();
		custAdress.clear();
		custname.clear();
		txtCreditPeriod.clear();
		cmbStore.getItems().clear();
		cmbSalesMan.getItems().clear();
		txtPreviousBalance.clear();
		txtItemname.clear();
		txtBarcode.clear();
		txtQty.clear();
		cmbUnit.getItems().clear();
		txtBatch.clear();
		txtRate.clear();
		txtCashtopay.clear();
		txtDiscount.clear();
		txtDiscountAmt.clear();
		txtAmtAftrDiscount.clear();
		itemDetailTable.getItems().clear();
		;

		txtDiscount.clear();
		txtDiscountAmt.clear();
		txtPriceType.clear();
		txtPreviousBalance.clear();
		txtLocalCustomer.clear();
		txtSalesType.clear();
		txtCreditPeriod.clear();
		txtWarranty.clear();
		saleListItemTable.clear();
		saleListItemTable.clear();
		saleTypeTable.clear();
		priceDefenitionList.clear();
		BatchpriceDefenitionList.clear();
		localCustId = null;

		salesDtl = null;
		salesTransHdr = null;
		unitMst = null;

		qtyTotal = 0;
		amountTotal = 0;
		discountTotal = 0;
		taxTotal = 0;
		cessTotal = 0;
		discountBfTaxTotal = 0;
		grandTotal = 0;
		expenseTotal = 0;

		notifyMessage(3, "Cleared...!");

	}

}
