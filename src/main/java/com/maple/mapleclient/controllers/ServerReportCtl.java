package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;

public class ServerReportCtl {
	
	
	String taskid;
	String processInstanceId;

	 String selectedText = null;
	 
	 @FXML
	    private TreeView<String> treeview;

	    @FXML
	    void reprtclick(MouseEvent event) {
	    	
	    	if (null != selectedText) {	//1. Fetch command and fx from table based on the clicked item text.
				//2. load the fx similar way we did in the main frame
				
			 
				HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
				
				
				
				ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName(selectedText);
				
				List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
				
				MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
				
				
				ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(aMenuConfigMst.getId());
				List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();
		
				
				for(MenuConfigMst aMenuConfig : menuConfigMstList)
				{
					
					Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
					if(null==dynamicWindow) {
						if(null != aMenuConfig.getMenuFxml())
						{
							  try {
								dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
								 menuWindowQueue.put(aMenuConfig.getMenuName(), dynamicWindow);
							  
							  } catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
		 
					}
					  
					  try {
				
							// Clear screen before loading the Page.
							if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
								MapleclientApplication.mainWorkArea.getChildren().clear();
				
							MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
				
							/*MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
						 	dynamicWindow.requestFocus();
							 
							*/
						} catch (Exception e) {
							e.printStackTrace();
						}
					  
					  
				}
	    	}

	    }
	    
	    @FXML
	    void initialize() {
	    	
			TreeItem rootItem = new TreeItem("SERVER REPORTS");
			//TreeItem masterItem = new TreeItem("MASTERS");
			
		HashMap menuWindowQueue= MapleclientApplication.mainFrameController.getMenuWindowQueue();
			
			
			
			ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("SERVER REPORTS");
			
			List<MenuConfigMst> listMenuConfig =listMenuConficMstBody.getBody();
			
			MenuConfigMst aMenuConfigMst =listMenuConfig.get(0);
			
			String parentId =aMenuConfigMst.getId();
			
			ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
			List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

			
			for(MenuConfigMst aMenuConfig : menuConfigMstList)
			{
				
				//masterItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
				
				if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
					rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
					}
				
				
			}
			
			//rootItem.getChildren().add(masterItem);

			treeview.setRoot(rootItem);
			
			treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
	//
					TreeItem<String> selectedItem = (TreeItem<String>) newValue;
					System.out.println("Selected Text 12345 : " + selectedItem.getValue());
					selectedText = selectedItem.getValue();
					
					
					ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller.MenuConfigMstByMenuName(selectedText);
					
					List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();
			
					MenuConfigMst aMenuConfig = menuConfigMstList.get(0);
					
					
					Node dynamicWindow = 	(Node) menuWindowQueue.get(aMenuConfig.getMenuName());
					if(null==dynamicWindow) {
						if(null != selectedText)
						{
							  try {
								dynamicWindow =  FXMLLoader.load(getClass().getResource("/fxml/"+aMenuConfig.getMenuFxml()));
								 menuWindowQueue.put(selectedText, dynamicWindow);
							  
							  } catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					}
					  
					  try {
				
							// Clear screen before loading the Page.
							if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
								MapleclientApplication.mainWorkArea.getChildren().clear();
				
							MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);
				
							MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
						 	dynamicWindow.requestFocus();
				
						} catch (Exception e) {
							e.printStackTrace();
						}
					  
					  
					  
					
				/*	if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) {
						if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
							MapleclientApplication.mainWorkArea.getChildren().clear();

						MapleclientApplication.mainWorkArea.getChildren()
								.add(MapleclientApplication.mainFrameController.kotManagement);

					}
					
					*/
					
					

				}

			});
//	    	TreeItem rootItem = new TreeItem("ADVANCED FEATURES");
//	    	TreeItem physicalStock = new TreeItem("PHYSICAL STOCK");
//	    	if (SystemSetting.UserHasRole("PHYSICAL STOCK")) {
//	    		rootItem.getChildren().add(physicalStock);
//	    	}
//	      	//version4.4
	//    
//	    	
//	    	//version4.4 end
//	    	
//	    	//version1.7
//	    	
//	    	TreeItem purchaseOrder = new TreeItem("PURCHASE ORDER");
//	    	
//			rootItem.getChildren().add(purchaseOrder);
//	    	TreeItem purchaseOrderMgmt = new TreeItem("PURCHASE ORDER MANAGEMENT");
//	    	
//	    		rootItem.getChildren().add(purchaseOrderMgmt);
//	    	
//	    	//version1.7ends
//	    		
//	    		//version2.5
//	    		//TreeItem otherBrasales = new TreeItem("OTHER BRANCH SALES");
//	        	
//	    		//rootItem.getChildren().add(otherBrasales);
//	    	
//	    		//version2.5ends
//	    		//----------------version 4.20
//	        	TreeItem otherBranchSaleCreation = new TreeItem("OTHER BRANCH SALE");
//	        	if (SystemSetting.UserHasRole("OTHER BRANCH SALE")) {
//	        		rootItem.getChildren().add(otherBranchSaleCreation);
//	        	}
//	        	
//	        	
////	        	TreeItem categoryUpdation = new TreeItem("MULTI CATEGORY UPDATION");
////	        	if (SystemSetting.UserHasRole("MULTI CATEGORY UPDATION")) {
////	        		rootItem.getChildren().add(categoryUpdation);
////	        	}
//	        	
//	        	
//	        	TreeItem acountMerge = new TreeItem("ACCOUNT MERGE");
//	        	if (SystemSetting.UserHasRole("ACCOUNT MERGE")) {
//	        		rootItem.getChildren().add(acountMerge);
//	        	}
//	        	
//	        	//---------------version 4.20 end
//	        	
//	    	TreeItem damageEntry = new TreeItem("DAMAGE ENTRY");
//	    	if (SystemSetting.UserHasRole("DAMAGE ENTRY")) {
//	    		rootItem.getChildren().add(damageEntry);
//	    	}
//	    	rootItem.getChildren().add(new TreeItem("USER REGISTERED STOCK"));
//	    	TreeItem storeChange = new TreeItem("STORE CHANGE");
//	    	if (SystemSetting.UserHasRole("STORE CHANGE")) {
//	    		rootItem.getChildren().add(storeChange);
//	    	}
//	    	TreeItem itemLocation = new TreeItem("ITEM LOCATION");
//	    	if (SystemSetting.UserHasRole("ITEM LOCATION")) {
//	    		rootItem.getChildren().add(itemLocation);
//	    	}
//	    	TreeItem salesReturn = new TreeItem("SALES RETURN");
//	    	if (SystemSetting.UserHasRole("SALES RETURN")) {
//	    		rootItem.getChildren().add(salesReturn);
//	    	}
//	    	
//	    	TreeItem salesFromOrder = new TreeItem("SALES FROM ORDER");
//	    	if (SystemSetting.UserHasRole("SALES FROM ORDER")) {
//	    		rootItem.getChildren().add(salesFromOrder);
//	    	}
//	    	
//	    	TreeItem stockVerification = new TreeItem("STOCK VERIFICATION");
//	    	if (SystemSetting.UserHasRole("STOCK VERIFICATION")) {
//	    		rootItem.getChildren().add(stockVerification);
//	    	}
//	    	TreeItem purchaseScheme = new TreeItem("PURCHASE SCHEME");
//	    	if (SystemSetting.UserHasRole("PURCHASE SCHEME")) {
//	    		rootItem.getChildren().add(purchaseScheme);
//	    	}
//	    	rootItem.getChildren().add(new TreeItem("INTENT"));
//	    	rootItem.getChildren().add(new TreeItem("SALES PROPERTIES CONFIGURATION"));
	//
//	    	rootItem.getChildren().add(new TreeItem("INTENT TO STOCK TRANSFER"));
//	    	rootItem.getChildren().add(new TreeItem("LMS_QUEUE PENDING TASK"));
//	    	
//	    	
//	    	rootItem.getChildren().add(new TreeItem("SESSION END"));
//	    	rootItem.getChildren().add(new TreeItem("PRODUCT CONVERSION"));
//	    	
//	    	rootItem.getChildren().add(new TreeItem("LANGUAGE MST"));
//	    	rootItem.getChildren().add(new TreeItem("ITEM IN LANGUAGE"));
//	    	TreeItem bargraph = new TreeItem("BAR GRAPH");
//	    	if (SystemSetting.UserHasRole("BAR GRAPH")) {
//	    		rootItem.getChildren().add(bargraph);
//	    	}
//	    	TreeItem prodValue = new TreeItem("PRODUCTION VALUE GRAPH");
//	    	if (SystemSetting.UserHasRole("BAR GRAPH")) {
//	    		rootItem.getChildren().add(prodValue);
//	    	}
//	    	TreeItem prodQty = new TreeItem("PRODUCTION QTY GRAPH");
//	    	if (SystemSetting.UserHasRole("BAR GRAPH")) {
//	    		rootItem.getChildren().add(prodQty);
//	    	}
//	    	TreeItem image = new TreeItem("SET AN IMAGE");
//	    	if (SystemSetting.UserHasRole("SET AN IMAGE")) {
//	    		rootItem.getChildren().add(image);
//	    	}
//	    	
//	    	TreeItem tally = new TreeItem("TALLY INTEGRATION");
//	    	if (SystemSetting.UserHasRole("TALLY INTEGRATION")) {
//	    		rootItem.getChildren().add(tally);
//	    	}
//	    	TreeItem stockCorrection = new TreeItem("STOCK CORRECTION");
//	    	if (SystemSetting.UserHasRole("STOCK CORRECTION")) {
//	    		rootItem.getChildren().add(stockCorrection);
//	    	}
//	    	
//	    	TreeItem posttoserver = new TreeItem("RETRY");
//	    	if (SystemSetting.UserHasRole("RETRY")) {
//	    		rootItem.getChildren().add(posttoserver);
//	    	}
//	    	
//	    	TreeItem consumption = new TreeItem("CONSUMPTION REASON");
//	    	if (SystemSetting.UserHasRole("CONSUMPTION REASON")) {
//	    		rootItem.getChildren().add(consumption);
//	    	}
//	    	TreeItem voucherdateedit = new TreeItem("VOUCHER DATE EDIT");
//	    	if (SystemSetting.UserHasRole("VOUCHER DATE EDIT")) {
//	    		rootItem.getChildren().add(voucherdateedit);
//	    	}
//	    	//-------------version 4.13
//	    	TreeItem spenserSale = new TreeItem("SUB BRANCH SALE");
//	    	if (SystemSetting.UserHasRole("SUB BRANCH SALE")) {
//	    		rootItem.getChildren().add(spenserSale);
//	    	}
//	    	TreeItem salesManCreation = new TreeItem("SALES MAN CREATION");
//	    	if (SystemSetting.UserHasRole("SALES MAN CREATION")) {
//	    		rootItem.getChildren().add(salesManCreation);
//	    	}
////	    	TreeItem spenserCreation = new TreeItem("SPENCER CREATION");
////	    	if (SystemSetting.UserHasRole("SPENCER CREATION")) {
////	    		rootItem.getChildren().add(spenserCreation);
////	    	}
//	    	//-------------version 4.13 end
//	    	treeview.getSelectionModel().selectedItemProperty().addListener( new ChangeListener() {
	//
//	            @Override
//	            public void changed(ObservableValue observable, Object oldValue,
//	                    Object newValue) {
	//
//	                TreeItem<String> selectedItem = (TreeItem<String>) newValue;
//	                System.out.println("Selected Text : " + selectedItem.getValue());
//	                selectedText =  selectedItem.getValue();
//	            }
//	        });
//	    	
//	    	
//	    	
//	    	
//	    	treeview.setRoot(rootItem);
	    	
	    }
	    @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	      private void PageReload() {
	      	
	      	
	  		
	  	}
	    


}
