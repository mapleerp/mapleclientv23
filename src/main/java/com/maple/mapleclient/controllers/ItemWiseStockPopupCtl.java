package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPopUp;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ItemWiseStockPopupCtl {
	String taskid;
	String processInstanceId;

	SalesTransHdr salesTransHdr = null;
	SalesDtl salesDtl = null;

	ItemPopupEvent itemPopupEvent = null;
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();

	private ObservableList<BatchPriceDefinition> batchpriceDefenitionList = FXCollections.observableArrayList();
	private ObservableList<PriceDefinition> priceDefenitionList = FXCollections.observableArrayList();

	private ObservableList<ItemStockPopUp> popUpItemList = FXCollections.observableArrayList();

	@FXML
	private TextField txtname;

	@FXML
	private TableView<ItemStockPopUp> tblItemPopupView;

	@FXML
	private TableColumn<ItemStockPopUp, String> clItemName;

	@FXML
	private TableColumn<ItemStockPopUp, Number> clMrp;

	@FXML
	private TableColumn<ItemStockPopUp, Number> ClQty;

	@FXML
	private TableColumn<ItemStockPopUp, String> ClBarcode;

	@FXML
	private TableColumn<ItemStockPopUp, String> clBatch;

	@FXML
	private TableColumn<ItemStockPopUp, String> columnItemCode;

	@FXML
	private TableColumn<ItemStockPopUp, String> ClUnit;

	@FXML
	private TableColumn<ItemStockPopUp, Number> columnTax;

	@FXML
	private TableColumn<ItemStockPopUp, String> clItemCode;

	@FXML
	private TableColumn<ItemStockPopUp, LocalDate> clExpiryDate;

	@FXML
	private Button btnEnquiry;

	@FXML
	private TextField txtEnquiry;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtQty;

	@FXML
	private Button btnSubmit;

	@FXML
	private Button btnCancel;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnRate;

	@FXML
	private TableColumn<SalesDtl, String> columnMrp;

	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

	@FXML
	private Button btnaddItem;

	@FXML
	private Button btndeleteRaw;

	@FXML
	private void initialize() {

		LoadItemPopupBySearch("", null);
		
		eventBus.register(this);

		tblItemPopupView.setItems(popUpItemList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemcodeProperty());
		ClBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		ClUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitnameProperty());
		ClQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());
		clItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemcodeProperty());
		clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpDateProperty());

		tblItemPopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				itemPopupEvent = new ItemPopupEvent();
				itemPopupEvent.setItemId(newSelection.getItemId());
				itemPopupEvent.setBatch(newSelection.getBatch());
				itemPopupEvent.setUnitId(newSelection.getUnitId());
				itemPopupEvent.setUnitName(newSelection.getUnitname());
				txtRate.setText(newSelection.getMrp().toString());
				setCustomerRate(itemPopupEvent);
			}

		});
		
		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				
				txtRate.setText(newSelection.getRate().toString());
				txtQty.setText(newSelection.getQty().toString());

				salesDtl = new SalesDtl();
				salesDtl.setId(newSelection.getId());
				
			}

		});
		
		itemDetailTable.setOnKeyPressed((event) -> {
			if (event.getCode().isDigitKey()) {
				txtQty.requestFocus();
			} else if (event.getCode() == KeyCode.ENTER) {
				txtQty.requestFocus();
			} 
		});
	}

	private void setCustomerRate(ItemPopupEvent itemPopupEvent) {

		if (null == salesTransHdr) {
			return;
		}

		Date udate = SystemSetting.getApplicationDate();
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

		ResponseEntity<BatchPriceDefinition> batchPriceDef = RestCaller.getBatchPriceDefinition(
				itemPopupEvent.getItemId(), salesTransHdr.getAccountHeads().getPriceTypeId(), itemPopupEvent.getUnitId(),
				itemPopupEvent.getBatch(), sdate);
		if (null != batchPriceDef.getBody()) {
			txtRate.setText(Double.toString(batchPriceDef.getBody().getAmount()));
		} else {
			ResponseEntity<PriceDefinition> pricebyItem = RestCaller.getPriceDefenitionByItemIdAndUnit(
					itemPopupEvent.getItemId(), salesTransHdr.getAccountHeads().getPriceTypeId(),
					itemPopupEvent.getUnitId(), sdate);

			if (null != pricebyItem.getBody()) {

				// version 2.3
				txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
				// version2.3ends

				ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp2 = RestCaller
						.getPriceDefenitionMstByName("MRP");
				if (null != priceDefenitionMstResp2.getBody()) {
					if (!pricebyItem.getBody().getPriceId().equalsIgnoreCase(priceDefenitionMstResp2.getBody().getId()))
						txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));

//		else
//		{
//			txtRate.setText(Double.toString(pricebyItem.getBody().getAmount()));
//		}
				}
			}
		}
		ResponseEntity<List<BatchPriceDefinition>> batchPrice = RestCaller.getBatchPriceDefinitionByItemId(
				itemPopupEvent.getItemId(), salesTransHdr.getAccountHeads().getPriceTypeId(), itemPopupEvent.getBatch(),
				sdate);
		batchpriceDefenitionList = FXCollections.observableArrayList(batchPrice.getBody());
		if (batchpriceDefenitionList.size() > 0) {
			for (BatchPriceDefinition priceDef : batchpriceDefenitionList) {
				ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
				if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
					itemPopupEvent.setUnitId(getUnit.getBody().getId());
					itemPopupEvent.setUnitName(getUnit.getBody().getUnitName());
				}
			}
		}

		else {

			ResponseEntity<List<PriceDefinition>> price = RestCaller.getPriceByItemId(itemPopupEvent.getItemId(),
					salesTransHdr.getAccountHeads().getPriceTypeId());
			priceDefenitionList = FXCollections.observableArrayList(price.getBody());

			if (priceDefenitionList.size() > 0) {
				// cmbUnit.getItems().clear();
				for (PriceDefinition priceDef : priceDefenitionList) {
					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(priceDef.getUnitId());
					if (!getUnit.getBody().getUnitName().equalsIgnoreCase(itemPopupEvent.getUnitName())) {
						itemPopupEvent.setUnitId(getUnit.getBody().getId());
						itemPopupEvent.setUnitName(getUnit.getBody().getUnitName());

					}
				}
			}
		}

	}

	public void LoadItemPopupBySearch(String searchData, SalesTransHdr salesHdr) {

		salesTransHdr = salesHdr;
		System.out.println("@@@@@@##########@@@@@@@");

		System.out.println(salesTransHdr);

		ArrayList items = new ArrayList();
		popUpItemList.clear();

		String logDate = SystemSetting.UtilDateToString(SystemSetting.getApplicationDate(), "yyyy-MM-dd");
		items = RestCaller.SearchStockItemByBatchAndItemId(searchData, logDate);

		String itemName = "";
		String barcode = "";
		Double mrp = 0.0;
		String unitname = "";
		Double qty = 0.0;
		Double cess = 0.0;
		Double tax = 0.0;
		String itemId = "";
		String unitId = "";
		String itemCode = "";
		Iterator itr = items.iterator();
		String batch = "";
		String expdate;
		String itemPriceLock;

		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			barcode = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			qty = (Double) element.get(4);
			cess = (Double) element.get(5);
			tax = (Double) element.get(6);
			itemId = (String) element.get(7);
			unitId = (String) element.get(8);
			batch = (String) element.get(9);
			itemCode = (String) element.get(10);
			expdate = (String) element.get(11);

			itemPriceLock = (String) element.get(12);
			if (null != itemName) {
				ItemStockPopUp itemStockPopUp = new ItemStockPopUp();
				itemStockPopUp.setBarcode((String) barcode);
				itemStockPopUp.setCess(null == cess ? 0 : cess);
				itemStockPopUp.setItemId(null == itemId ? "" : itemId);
				itemStockPopUp.setItemName(null == itemName ? "" : itemName);
				itemStockPopUp.setTax(null == tax ? 0 : tax);
				itemStockPopUp.setUnitId(null == unitId ? "" : unitId);
				itemStockPopUp.setUnitname(null == unitname ? "" : unitname);
				itemStockPopUp.setQty(null == qty ? 0 : qty);
				itemStockPopUp.setBatch(null == batch ? "" : batch);
				itemStockPopUp.setMrp(null == mrp ? 0 : mrp);
				itemStockPopUp.setItemcode((String) itemCode);
				itemStockPopUp.setitemPriceLock(null == itemPriceLock ? "" : itemPriceLock);

				if (null != expdate) {

					itemStockPopUp.setexpiryDate(SystemSetting.StringToSqlDateSlash(expdate, "yyyy-MM-dd"));

				}

				popUpItemList.add(itemStockPopUp);

			}
		}

		if (items.size() == 1) {

			tblItemPopupView.getSelectionModel().selectFirst();
		}

		return;

	}

	@FXML
	void addItem(ActionEvent event) {

		addItem();

	}

	private void addItem() {
		
		if(null == itemPopupEvent)
		{
			notifyMessage(3, "Please select item");
			return;

		}

		if(null == salesTransHdr)
		{
			return;
		}

		ResponseEntity<List<FinancialYearMst>> getFinancialYear = RestCaller.getAllFinancialYear();
		if (getFinancialYear.getBody().size() == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}
		int count = RestCaller.getFinancialYearCount();
		if (count == 0) {
			notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
			return;
		}
		// version2.0ends
		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("DAY_END_LOCKED_IN_WHOLESALE");
		if (null != getParamValue.getBody()) {
			if (getParamValue.getBody().getValue().equalsIgnoreCase("YES")) {
				ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
				DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
				System.out.println("Sys Date before add" + SystemSetting.applicationDate);
				if (null != dayEndClosureHdr) {
					if (null != dayEndClosureHdr.getDayEndStatus()) {
						String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(),
								"yyyy-MM-dd");
						String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
						java.sql.Date prDate = java.sql.Date.valueOf(process_date);
						Date sDate = java.sql.Date.valueOf(sysdate);
						int i = prDate.compareTo(sDate);
						if (i > 0 || i == 0) {
							notifyMessage(1, " Day End Already Done for this Date !!!!");
							return;
						}
					}
				}

			}
			Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.applicationDate);

			if (!dayendtodone) {
				notifyMessage(1, "Day End should be done before changing the date !!!!");
				return;
			}
			System.out.println("Sys Date before add" + SystemSetting.applicationDate);
		}
	
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Type Quantity...!!!");
			txtQty.requestFocus();
			return;
		}


		ResponseEntity<UnitMst> getUnitBYItem = RestCaller.getUnitByName(itemPopupEvent.getUnitName());

		ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(itemPopupEvent.getItemId());

		Double chkQty = 0.0;
		String itemId = getItem.getBody().getId();

			
			ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId());
			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				if (null == getmulti.getBody()) {
					notifyMessage(5, "Please Add the item in Multi Unit");
					return;
				}
			}
			ArrayList items = new ArrayList();
			items = RestCaller.getSingleStockItemByName(getItem.getBody().getItemName(), itemPopupEvent.getBatch());

			//chkQty = RestCaller.getQtyFromItemBatchDtlDailyByItemIdAndQty(getItem.getBody().getId(),itemPopupEvent.getBatch());
          
			
			//--Here--stock checking  from ItemBatchMst--------------------------------sharon --------28/06/2021--------------------------------------
					chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(),itemPopupEvent.getBatch());
			
					Iterator itr = items.iterator();
			
			while (itr.hasNext()) {
				List element = (List) itr.next();
//			chkQty = (Double) element.get(4);
				itemId = (String) element.get(7);

			}

			if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
				Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
						getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
						Double.parseDouble(txtQty.getText()));
				System.out.println(conversionQty);
				if (chkQty < conversionQty) {
					notifyMessage(1, "Not in Stock!!!");
					txtQty.clear();
					txtRate.clear();
				
					return;
				}
			} else if (chkQty < Double.parseDouble(txtQty.getText())) {
				notifyMessage(5, "Not in Stock!!!");
				txtQty.clear();
				txtRate.clear();
				return;
			}


		Double itemsqty = 0.0;
		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
				getItem.getBody().getId(), itemPopupEvent.getBatch());
		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
		if (saleListItemTable.size() > 1) {
			Double PrevQty = 0.0;
			for (SalesDtl saleDtl : saleListItemTable) {
				if (!saleDtl.getUnitId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
							getItem.getBody().getUnitId(), saleDtl.getQty());
				} else {
					PrevQty = saleDtl.getQty();
				}
				itemsqty = itemsqty + PrevQty;
			}
		} else {
			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId, itemPopupEvent.getBatch());
		}
		if (!getUnitBYItem.getBody().getId().equalsIgnoreCase(getItem.getBody().getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(getItem.getBody().getId(),
					getUnitBYItem.getBody().getId(), getItem.getBody().getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if (chkQty < itemsqty + conversionQty) {
				txtQty.clear();
				txtRate.clear();
				return;
			}
		}

		else if (chkQty < itemsqty + Double.parseDouble(txtQty.getText())) {
			txtQty.clear();
	
			txtRate.clear();
			return;
		}
		if (null == salesDtl) {
			salesDtl = new SalesDtl();
		}
		if (null != salesDtl.getId()) {

			RestCaller.deleteSalesDtl(salesDtl.getId());

			if (null != salesDtl.getSchemeId()) {
				return;
			}

		}

		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(getItem.getBody().getItemName());
		salesDtl.setBarcode(getItem.getBody().getBarCode());

		String batch = itemPopupEvent.getBatch();
		System.out.println(batch);
		salesDtl.setBatchCode(batch);

		Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());

		salesDtl.setQty(qty);

		salesDtl.setItemCode(getItem.getBody().getItemCode());
		Double mrpRateIncludingTax = 00.0;
		mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());
		salesDtl.setMrp(mrpRateIncludingTax);

		Double taxRate = 00.0;

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
		salesDtl.setBarcode(item.getBarCode());
		salesDtl.setStandardPrice(item.getStandardPrice());
		salesDtl.setListPrice(mrpRateIncludingTax);

		if (null == salesTransHdr.getAccountHeads().getCustomerDiscount()) {
			salesTransHdr.getAccountHeads().setCustomerDiscount(0.0);
		}

		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			Double discount = 0.0;
			discount = mrpRateIncludingTax
					- (mrpRateIncludingTax * salesTransHdr.getAccountHeads().getCustomerDiscount()) / 100;
			salesDtl.setListPrice(discount);
		}


		salesDtl.setItemId(item.getId());

		if (null != itemPopupEvent.getBatch()) {
			ResponseEntity<List<ItemBatchExpiryDtl>> batchExpiryDtlResp = RestCaller
					.getItemBatchExpByItemAndBatch(salesDtl.getItemId(), salesDtl.getBatch());

			List<ItemBatchExpiryDtl> itemBatchExpiryDtlList = batchExpiryDtlResp.getBody();
			if (itemBatchExpiryDtlList.size() > 0 && null != itemBatchExpiryDtlList) {
				ItemBatchExpiryDtl itemBatchExpiryDtl = itemBatchExpiryDtlList.get(0);

				if (null != itemBatchExpiryDtl) {
					String expirydate = SystemSetting.UtilDateToString(itemBatchExpiryDtl.getExpiryDate(),
							"yyyy-MM-dd");
					salesDtl.setExpiryDate(java.sql.Date.valueOf(expirydate));
				}
			}
		}

		salesDtl.setUnitId(itemPopupEvent.getUnitId());
		salesDtl.setUnitName(itemPopupEvent.getUnitName());

		// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());

		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtlWithCalculation(salesDtl);
		salesDtl = respentity.getBody();
		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());

		List<SalesDtl> salesDtlList = respentityList.getBody();
		saleListTable.clear();
		saleListTable.setAll(salesDtlList);

		FillTable();

	
		txtQty.setText("");
		txtRate.setText("");
		salesDtl = new SalesDtl();
		
		eventBus.post("SALES");


	
	}

	@FXML
	void DeleteRaw(ActionEvent event) {
		try {

			if (null != salesDtl) {
				if (null != salesDtl.getId()) {

					if (null == salesDtl.getSchemeId()) {

						RestCaller.deleteSalesDtl(salesDtl.getId());

						System.out.println("toDeleteSale.getId()" + salesDtl.getId());
//						RestCaller.deleteSalesDtl(salesDtl.getId());

						txtRate.clear();
						txtQty.clear();
						ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
						saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
						FillTable();
						salesDtl = new SalesDtl();
						eventBus.post("SALES");


					}

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void FillTable() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		// columnCessRate.setCellValueFactory(cellData ->
		// cellData.getValue().getCessRateProperty());

		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

	}

	@FXML
	void OnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		
			txtQty.requestFocus();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP
				|| event.getCode() == KeyCode.KP_UP) {

		} else {
			txtname.requestFocus();
		}

	}

	@FXML
	void OnKeyPressTxt(KeyEvent event) {
		
		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblItemPopupView.requestFocus();
			tblItemPopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {

			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
		if (event.getCode() == KeyCode.BACK_SPACE && txtname.getText().length() == 0) {
			
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}

	}

	@FXML
	void enquiryOnAction(ActionEvent event) {

	}

	@FXML
	void onCancel(ActionEvent event) {
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		eventBus.post("SALES");
		stage.close();
	}

	@FXML
	void submit(ActionEvent event) {
		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		eventBus.post("SALES");
		stage.close();
	}

	@FXML
	void tableOnClick(MouseEvent event) {

	}
	
	
    @FXML
    void saveSalesDtls(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		}
    }
    
    
    @FXML
    void OnKeyPressQty(KeyEvent event) {

		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			itemDetailTable.requestFocus();
			itemDetailTable.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {

			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
		
		if(event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP)
		{
			tblItemPopupView.requestFocus();
			tblItemPopupView.getSelectionModel().selectFirst();
		}
		
		if (event.getCode() == KeyCode.ENTER) {

			if(txtQty.getText().trim().isEmpty())
			{
				btnSubmit.requestFocus();

			}
		}
    }
    
    
    @FXML
    void OnKeyPressOk(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		
    		eventBus.post("SALES");

    		Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
