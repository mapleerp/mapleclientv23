package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.CategoryWiseStockMovement;
import com.maple.report.entity.DayBook;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class CategoryWiseStockMovementCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<CategoryWiseStockMovement> categoryList = FXCollections.observableArrayList();

	String strfDate, strtDate;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private DatePicker dpFromDate;

	@FXML
	private Button btnShow;
	
	@FXML
    private Button btnPrintCat;

	@FXML
	private TableView<CategoryWiseStockMovement> tblCategory;

	@FXML
	private TableColumn<CategoryWiseStockMovement, String> clCategory;

	@FXML
	private TableColumn<CategoryWiseStockMovement, Number> clCatOpen;

	@FXML
	private TableColumn<CategoryWiseStockMovement, Number> clCatIn;

	@FXML
	private TableColumn<CategoryWiseStockMovement, Number> clCatOut;

	@FXML
	private TableColumn<CategoryWiseStockMovement, Number> clCatClosing;

	@FXML
	private TableColumn<CategoryWiseStockMovement, String> clCatVoucherDate;

	@FXML
	private void initialize() {
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		tblCategory.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				System.out.println("getSelectionModel" + newSelection);
				if (null != newSelection.getCategory()) {

					ResponseEntity<CategoryMst> getCategory = RestCaller.getCategoryByName(newSelection.getCategory());
					ItemWiseStockMovement(getCategory.getBody().getId());
				}
			}
		});
	}

	// -------------------new version 2.1 surya

	@FXML
	private Button btnPrintItemWiseMovement;

	@FXML
	void PrintItemWiseMovement(ActionEvent event) {

		if (null == dpFromDate.getValue()) {
			notifyMessage(3, "Please select from date", false);
			dpFromDate.requestFocus();
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(3, "Please select to date", false);
			dpToDate.requestFocus();
			return;
		}

		java.util.Date fDate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String strtDate = SystemSetting.UtilDateToString(fDate, "yyyy-MM-dd");
		
		java.util.Date tDate = SystemSetting.localToUtilDate(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(tDate, "yyyy-MM-dd");
		
		try {
			JasperPdfReportService.PrintItemWiseStockMovementReport(strtDate,endDate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	// -------------------new version 2.1 surya end

	private void ItemWiseStockMovement(String categoryId) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/itemCategoryStockMvmnt.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			ItemWIseStockMovementCtl itemWIseStockMovementCtl = fxmlLoader.getController();
			itemWIseStockMovementCtl.itemWiseStkMovement(categoryId, strfDate, strtDate);
			Stage stage = new Stage();
			stage.setScene(new Scene(root1));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (IOException e) {
			//
			e.printStackTrace();
		}
	}

	@FXML
	void actionShow(ActionEvent event) {

		LocalDate dpDate1 = dpFromDate.getValue();
		java.util.Date ufDate = SystemSetting.localToUtilDate(dpDate1);
		strfDate = SystemSetting.UtilDateToString(ufDate, "yyyy-MM-dd");

		LocalDate dpDate2 = dpToDate.getValue();
		java.util.Date utDate = SystemSetting.localToUtilDate(dpDate2);
		strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");

		ResponseEntity<List<CategoryWiseStockMovement>> getCategoryStkMvmnt = RestCaller
				.getCategoryWiseStockMovement(strfDate, strtDate);
		categoryList = FXCollections.observableArrayList(getCategoryStkMvmnt.getBody());
		fillTable();

	}

	private void fillTable() {

		for (CategoryWiseStockMovement cat : categoryList) {
			if (null != cat.getClosingStock()) {
				BigDecimal closing = new BigDecimal(cat.getClosingStock());
				closing = closing.setScale(3, BigDecimal.ROUND_HALF_EVEN);
				cat.setClosingStock(closing.doubleValue());
			} else {
				cat.setClosingStock(0.0);
			}
			if (null != cat.getOpeningStock()) {
				BigDecimal opening = new BigDecimal(cat.getOpeningStock());
				opening = opening.setScale(3, BigDecimal.ROUND_HALF_EVEN);
				cat.setOpeningStock(opening.doubleValue());
			} else {
				cat.setOpeningStock(0.0);
			}
			if (null != cat.getInWardQty()) {
				BigDecimal inward = new BigDecimal(cat.getInWardQty());
				inward = inward.setScale(3, BigDecimal.ROUND_HALF_EVEN);
				cat.setInWardQty(inward.doubleValue());
			} else {
				cat.setInWardQty(0.0);
			}
			if (null != cat.getOutWardQty()) {
				BigDecimal outward = new BigDecimal(cat.getOutWardQty());
				outward = outward.setScale(3, BigDecimal.ROUND_HALF_EVEN);
				cat.setOutWardQty(outward.doubleValue());
			} else {
				cat.setOutWardQty(0.0);
			}
		}
		tblCategory.setItems(categoryList);
		clCatClosing.setCellValueFactory(cellData -> cellData.getValue().getclosingStockProperty());
		clCategory.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
		clCatIn.setCellValueFactory(cellData -> cellData.getValue().getinWardQtyProperty());
		clCatOut.setCellValueFactory(cellData -> cellData.getValue().getoutWardQtyProperty());
		clCatOpen.setCellValueFactory(cellData -> cellData.getValue().getopeningStockProperty());
		clCatVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}

	
	 @FXML
	    void printCategoryWise(ActionEvent event) {
		 LocalDate dpDate1 = dpFromDate.getValue();
			java.util.Date ufDate = SystemSetting.localToUtilDate(dpDate1);
			strfDate = SystemSetting.UtilDateToString(ufDate, "yyyy-MM-dd");

			LocalDate dpDate2 = dpToDate.getValue();
			java.util.Date utDate = SystemSetting.localToUtilDate(dpDate2);
			strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
			
			try {
				JasperPdfReportService.PrintCategoryWiseStockMovementReport(strfDate,strtDate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 

	    }

     private void PageReload() {
     	
}

}
