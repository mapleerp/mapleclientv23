package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ConsumptionReport;
import com.maple.report.entity.InsuranceCompanySalesSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import net.sf.jasperreports.engine.JRException;

public class InsuranceCompanySalesSummary {
	String taskid;
	String processInstanceId;
	
	
	private ObservableList<InsuranceCompanySalesSummaryReport> reportList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;
    

    @FXML
    private ComboBox<String> cmbInsuranceCompany;

    @FXML
    private Button btnAdd;

    @FXML
    private ListView<String> lsInsurance;
    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private Button btnClear;

    @FXML
    private TableView<InsuranceCompanySalesSummaryReport> tbInsuranceSales;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,String> clVoucherDate;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,String> clVoucherNumber;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport, String> clPatientId;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,String> clInsuranceCompany;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,String> clInsuranceCard;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,String> clPolicyType;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,Number> clCrAmount;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,Number> clCashAmount;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,Number> clCardAmount;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,Number> clInsuranceAmount;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,Number> clInvoiceAmount;

    @FXML
    private TableColumn<InsuranceCompanySalesSummaryReport,String> clUser;
    @FXML
   	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	ResponseEntity<List<InsuranceCompanyMst>> insuranceComp = RestCaller.getAllInsuranceCompanyMst();
    	for(int i =0;i<insuranceComp.getBody().size();i++)
    	{
    		cmbInsuranceCompany.getItems().add(insuranceComp.getBody().get(i).getInsuranceCompanyName());
    	}
    }
    @FXML
    void actionClear(ActionEvent event) {
    	dpFromDate.setValue(null);
    	dpToDate.setValue(null);
    	cmbInsuranceCompany.getSelectionModel().clearSelection();
    	tbInsuranceSales.getItems().clear();
    	reportList.clear();
    	lsInsurance.getItems().clear();
    }
    @FXML
    void actionadd(ActionEvent event) {
    	lsInsurance.getItems().add(cmbInsuranceCompany.getSelectionModel().getSelectedItem());
    	lsInsurance.getSelectionModel().select(cmbInsuranceCompany.getSelectionModel().getSelectedItem());
    	cmbInsuranceCompany.getSelectionModel().clearSelection();
    }
    @FXML
    void actionPrint(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		List<String> selectedItems = lsInsurance.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	} 
		if(cat.equalsIgnoreCase(""))
		{
			
			
		try {
			JasperPdfReportService.InsuranceWiseSalesSummary(startDate, endDate, cat);
		} catch (JRException e) {
			e.printStackTrace();
		}
		}
		else
		{
			try {
				JasperPdfReportService.InsuranceWiseSalesSummary(startDate, endDate, "");
			} catch (JRException e) {
				e.printStackTrace();
			}
		}
    }
    

    @FXML
    void actionShow(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		List<String> selectedItems = lsInsurance.getItems();
        
    	String cat="";
    	for(String s:selectedItems)
    	{
    		s = s.concat(";");
    		cat= cat.concat(s);
    	}
		if(cat.equalsIgnoreCase(""))
		{
			ResponseEntity<List<InsuranceCompanySalesSummaryReport>> insurancelist = RestCaller.getInsuranceSalesSummary(startDate,endDate);
			reportList = FXCollections.observableArrayList(insurancelist.getBody());
		}
		else
		{
			
			
			
			ResponseEntity<List<InsuranceCompanySalesSummaryReport>> insurancelist = RestCaller.getInsuranceSalesSummaryWithInsuranceComp(startDate,endDate,cat);
			reportList = FXCollections.observableArrayList(insurancelist.getBody());
		}
		fillTable();
    }
    private void fillTable()
    {
    	tbInsuranceSales.setItems(reportList);
    	clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getcardPaidProperty());
    	clCashAmount.setCellValueFactory(cellData -> cellData.getValue().getcashPaidProperty());
    	clCrAmount.setCellValueFactory(cellData -> cellData.getValue().getcreditAmountProperty());
//    	clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getcustomerNameProperty());
    	clInsuranceAmount.setCellValueFactory(cellData -> cellData.getValue().getinsuranceAmountProperty());
    	clInsuranceCard.setCellValueFactory(cellData -> cellData.getValue().getinsuranceCardProperty());
    	clInsuranceCompany.setCellValueFactory(cellData -> cellData.getValue().getinsuranceCompanyNameProperty());
    	clInvoiceAmount.setCellValueFactory(cellData -> cellData.getValue().getinvoiceAmountProperty());
    	clPolicyType.setCellValueFactory(cellData -> cellData.getValue().getpolicyTypeProperty());
    	clUser.setCellValueFactory(cellData -> cellData.getValue().getuserNameProperty());
    	clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
    	clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
    	clPatientId.setCellValueFactory(cellData -> cellData.getValue().getPatientNameProperty());

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
