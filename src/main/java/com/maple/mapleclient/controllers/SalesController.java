package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.Sales;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class SalesController {
	
	String taskid;
	String processInstanceId;


    @FXML
    private TextField salescustomername;

    @FXML
    private DatePicker salesdate;

    @FXML
    private Button salescheck;

    @FXML
    private TextField salesitem;

    @FXML
    private TextField salesquantity;

    @FXML
    private TextField salesbarcode;

    @FXML
    private TextField salesbatch;

    @FXML
    private TextField salesrate;

    @FXML
    private TextField salesdiscount;

    @FXML
    private TextField salesamount;
    @FXML
   	private void initialize() {
    	salesdate = SystemSetting.datePickerFormat(salesdate, "dd/MMM/yyyy");
   		
       }
    @FXML
    void SalesCheck(ActionEvent event) {
    	try
    	{
    	Sales sales= new Sales();
    	sales.setItem(salesitem.getText());
    	sales.setCustomerName(salescustomername.getText());
    	//sales.setDate(salesdate.getValue());
    	sales.setBarcode(salesbarcode.getText());
    	sales.setBatch(salesbatch.getText());
    	sales.setDiscount(salesdiscount.getText());
    	sales.setQuantity(salesquantity.getText());
    	sales.setRate(salesrate.getText());
    	sales.setAmount(salesamount.getText());
    	} catch (Exception e) {
    		e.printStackTrace();
		}
    	
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   private void PageReload(String hdrId) {
   	
   }


}
