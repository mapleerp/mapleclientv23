package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.OwnAccount;
import com.maple.mapleclient.entity.PDCPayment;
import com.maple.mapleclient.entity.PDCReceipts;
import com.maple.mapleclient.entity.PDCReconcileDtl;
import com.maple.mapleclient.entity.PDCReconcileHdr;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentHdr;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class PDCReconcileCtl {

	String processInstanceId;

	private ObservableList<PDCPayment> paymentList = FXCollections.observableArrayList();
	private ObservableList<PDCReceipts> receiptList = FXCollections.observableArrayList();
	private ObservableList<PDCReconcileDtl> reconcileList = FXCollections.observableArrayList();
	String taskid=null;
	ReceiptHdr receipthdr = null;
	ReceiptDtl receipt;
	PaymentHdr paymenthdr = null;
	PaymentDtl payment = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	PDCPayment pdcPayment = null;
	PDCReconcileDtl pdcReconcile = null;
	PDCReconcileHdr pdcReconcileHdr = null;
	Date transDate = null;

	OwnAccount ownAccount = null;

	@FXML
	private DatePicker dpDate;

	@FXML
	private Button btnReceipts;

	@FXML
	private Button btnPayments;

	@FXML
	private ComboBox<String> cmbBank;

	@FXML
	private Button btnReconcile;

	@FXML
	private Button btnClear;

	@FXML
	private Button btn;

	@FXML
	private TableView<PDCPayment> tblPDC;

	@FXML
	private TableColumn<PDCPayment, String> clAccount;

	@FXML
	private TableColumn<PDCPayment, String> clInstNo;

	@FXML
	private TableColumn<PDCPayment, Number> clAmount;

	@FXML
	private TableColumn<PDCPayment, String> clRemark;

	@FXML
	private TableView<PDCReconcileDtl> tblReconcile;

	@FXML
	private TableColumn<PDCReconcileDtl, String> clReAccount;

	@FXML
	private TableColumn<PDCReconcileDtl, String> clReBank;

	@FXML
	private TableColumn<PDCReconcileDtl, String> clReTransDetils;

	@FXML
	private TableColumn<PDCReconcileDtl, Number> clReAmount;

	@FXML
	private TableColumn<PDCReconcileDtl, String> clReInstNo;
	@FXML
	private Button btnPDCRemove;

	@FXML
	private Button btnFinalSave;

	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		eventBus.register(this);
		dpDate.setValue(LocalDate.now());

		ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid = lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbBank.getItems().add((String) bankName);

			}
		}

		tblPDC.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					pdcPayment = new PDCPayment();
					pdcPayment.setId(newSelection.getId());
					pdcPayment.setAccount(newSelection.getAccount());
					pdcPayment.setAmount(newSelection.getAmount());
					pdcPayment.setBank(newSelection.getBank());
					pdcPayment.setInstrumentnumber(newSelection.getInstrumentnumber());
					pdcPayment.setRemark(newSelection.getRemark());

				}
			}
		});

		tblReconcile.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					pdcReconcile = new PDCReconcileDtl();
					pdcReconcile.setId(newSelection.getId());
					pdcReconcile.setPdcId(newSelection.getPdcId());

				}
			}
		});

	}

	@FXML
	void FetchPayments(ActionEvent event) {

		fetchPDCPayment();

	}

	private void fetchPDCPayment() {

		paymentList.clear();

		if (null == cmbBank.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please select bank...!", false);
		}
		String bank = cmbBank.getSelectionModel().getSelectedItem();
		ResponseEntity<List<PDCPayment>> paymentResp = RestCaller.getPDCPaymentByBankAndStatus(bank);
		paymentList = FXCollections.observableArrayList(paymentResp.getBody());

		FillTable();

	}

	private void FillTable() {

		for (PDCPayment pdc : paymentList) {
			if (null != pdc.getAccount()) {
				ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadsById(pdc.getAccount());
				AccountHeads accountHeads = accountHeadsResp.getBody();

				if (null != accountHeads) {
					pdc.setAccountName(accountHeads.getAccountName());
				} else {

					ResponseEntity<AccountHeads> customet = RestCaller.getAccountHeadsById(pdc.getAccount());
					AccountHeads customerMst = customet.getBody();
					if (null != customerMst) {
						pdc.setAccountName(customerMst.getAccountName());
					}
				}

			}
		}

		tblPDC.setItems(paymentList);

		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

		clRemark.setCellValueFactory(cellData -> cellData.getValue().getRemarkeProperty());
		clInstNo.setCellValueFactory(cellData -> cellData.getValue().getInstNoProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

	}

	@FXML
	void FinalSave(ActionEvent event) {

		if (null == pdcReconcileHdr) {
			notifyMessage(5, "Please add PDC to Reconcile table...!", false);
			return;

		}

		ResponseEntity<List<PDCReconcileDtl>> pdcReconcileDtlResp = RestCaller
				.getAllPDCReconcileDtl(pdcReconcileHdr.getId());
		reconcileList = FXCollections.observableArrayList(pdcReconcileDtlResp.getBody());

		for (PDCReconcileDtl reconcile : reconcileList) {
			ResponseEntity<PDCPayment> paymentResp = RestCaller.getPDCPaymentById(reconcile.getPdcId());
			PDCPayment pdcPaymentObj = new PDCPayment();
			pdcPaymentObj = paymentResp.getBody();

			if (null != pdcPaymentObj) {

				
				tblPDC.getItems().clear();

				// ------------------payment entry

				paymenthdr = new PaymentHdr();
				paymenthdr.setBranchCode(SystemSetting.getSystemBranch());
				paymenthdr.setVoucherDate(reconcile.getTransDate());
				ResponseEntity<PaymentHdr> respentity = RestCaller.savePaymenthdr(paymenthdr);
				paymenthdr = respentity.getBody();

				if (null != paymenthdr) {
					payment = new PaymentDtl();

					payment.setRemark(reconcile.getRemark());
					payment.setAccountId(reconcile.getAccount());
					ResponseEntity<AccountHeads> creditAccount = RestCaller.getAccountHeadByName(reconcile.getBank());
					payment.setCreditAccountId(creditAccount.getBody().getId());

					payment.setTransDate(reconcile.getTransDate());
					payment.setAmount(reconcile.getAmount());
					payment.setInstrumentNumber(reconcile.getIntrumentNo());
					payment.setModeOfPayment("CHEQUE");
					
					payment.setInstrumentDate(pdcPaymentObj.getInstrumentDate());
//						payment.setBankAccountNumber(bankAccount);
					payment.setPaymenthdr(paymenthdr);
					ResponseEntity<PaymentDtl> respentityPayment = RestCaller.savePaymentdtls(payment);

					ResponseEntity<AccountHeads> accountHeadsSaved = RestCaller.getAccountHeadsById(reconcile.getAccount());
					if (null != accountHeadsSaved.getBody()) {
						ownAccount = new OwnAccount();
						ownAccount.setAccountId(reconcile.getAccount());
						ownAccount.setPaymenthdr(paymenthdr);
						ownAccount.setAccountType("SUPPLIER");
						ownAccount.setCreditAmount(0.0);
						ownAccount.setDebitAmount(reconcile.getAmount());
						ownAccount.setOwnAccountStatus("PENDING");
						ownAccount.setRealizedAmount(0.0);
						
						String sinstDate = SystemSetting.SqlDateTostring(pdcPaymentObj.getInstrumentDate());
						java.util.Date uinstDate = SystemSetting.StringToUtilDate(sinstDate, "dd_MM-yyyy");
						ownAccount.setVoucherDate(uinstDate);
						ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
						ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
						ownAccount = respentity1.getBody();
					}

					String financialYear = SystemSetting.getFinancialYear();
					String vNo = RestCaller.getVoucherNumber(financialYear + "PYN");
					paymenthdr.setVoucherNumber(vNo);
					RestCaller.updatePaymenthdr(paymenthdr);
					pdcPaymentObj.setStatus("RECONCILED");
					RestCaller.updatePDCPayment(pdcPaymentObj);
				}

			} else {

				ResponseEntity<PDCReceipts> receiptsResp = RestCaller.getPDCReceiptsById(reconcile.getPdcId());
				PDCReceipts pdcReceipts = receiptsResp.getBody();

				if (null != pdcReceipts) {
					
					tblPDC.getItems().clear();

					// ------------------recipt entry

					receipthdr = new ReceiptHdr();

					receipthdr.setTransdate(reconcile.getTransDate());
					receipthdr.setVoucherDate(SystemSetting.systemDate);
					receipthdr.setBranchCode(SystemSetting.systemBranch);

					ResponseEntity<ReceiptHdr> respentity = RestCaller.saveReceipthdr(receipthdr);
					receipthdr = respentity.getBody();

					if (null != receipthdr) {
						receipt = new ReceiptDtl();

						receipt.setAccount(reconcile.getAccount());
						receipt.setRemark(reconcile.getRemark());
						receipt.setAmount(reconcile.getAmount());
						
						receipt.setInstrumentDate(pdcReceipts.getInstrumentDate());
						receipt.setInstnumber(reconcile.getIntrumentNo());
						receipt.setDepositBank(reconcile.getBank());

						ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
								.getAccountHeadByName(reconcile.getBank());
						AccountHeads accountHeads = accountHeadsResp.getBody();

						if (null != accountHeads) {
							receipt.setDebitAccountId(accountHeads.getId());
						}

						receipt.setTransDate(reconcile.getTransDate());
						receipt.setModeOfpayment("CHEQUE");
						receipt.setReceipthdr(receipthdr);
						ResponseEntity<AccountHeads> accountHeadsRespName = RestCaller
								.getAccountHeadsById(reconcile.getAccount());
						AccountHeads accountHeadsName = accountHeadsRespName.getBody();
						if(null == accountHeadsName)
						{
							return;
						}
						receipt.setAccountName(accountHeadsName.getAccountName());
						ResponseEntity<ReceiptDtl> respentityReceipt = RestCaller.saveReceiptDtl(receipt);

						ResponseEntity<AccountHeads> custSaved = RestCaller.getAccountHeadsById(receipt.getAccount());
						if (null != custSaved.getBody()) {
							ownAccount = new OwnAccount();
							ownAccount.setAccountId(receipt.getAccount());
							ownAccount.setAccountType("CUSTOMER");
							ownAccount.setCreditAmount(reconcile.getAmount());
							ownAccount.setDebitAmount(0.0);
							ownAccount.setOwnAccountStatus("PENDING");
							ownAccount.setRealizedAmount(0.0);
							
							String sinstDate = SystemSetting.SqlDateTostring(pdcReceipts.getInstrumentDate());
							java.util.Date uinstDate = SystemSetting.StringToUtilDate(sinstDate, "dd_MM-yyyy");
							ownAccount.setVoucherDate(uinstDate);
							ownAccount.setVoucherNo(RestCaller.getVoucherNumber("OWNACC"));
							ownAccount.setReceiptHdr(receipthdr);
							ResponseEntity<OwnAccount> respentity1 = RestCaller.saveOwnAccount(ownAccount);
						}

						String financialYear = SystemSetting.getFinancialYear();
						String vNo = RestCaller.getVoucherNumber(financialYear + "RECEIPT");
						receipthdr.setVoucherNumber(vNo);
						Format formatter;
						formatter = new SimpleDateFormat("yyyy-MM-dd");
						RestCaller.updateReceipthdr(receipthdr);
						pdcReceipts.setStatus("RECONCILED");
						RestCaller.updatePDCReceipts(pdcReceipts);
					}

				}
			}
		}

		String voucherNo = RestCaller.getVoucherNumber(SystemSetting.systemBranch);
		pdcReconcileHdr.setVoucherNumber(voucherNo);
		RestCaller.updatePDCReconcileHdr(pdcReconcileHdr);
		if(null != taskid)
		{
			RestCaller.completeTask(taskid);
		}
		taskid = null;
		paymenthdr = null;
		payment = null;
		ownAccount = null;
		receipthdr = null;
		receipt = null;
		
		paymentList.clear();
		receiptList.clear();
		reconcileList.clear();

		pdcReconcileHdr = null;
		pdcPayment = null;
		pdcReconcile = null;
		tblPDC.getItems().clear();
		tblReconcile.getItems().clear();
		cmbBank.getSelectionModel().clearSelection();

	}

	@FXML
	void FtechReceipts(ActionEvent event) {

		fetchPDCReceipts();

	}

	private void fetchPDCReceipts() {

		paymentList.clear();

		if (null == cmbBank.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please select bank...!", false);
		}
		String bank = cmbBank.getSelectionModel().getSelectedItem();
		ResponseEntity<List<PDCReceipts>> receiptsResp = RestCaller.getPDCReceiptsByBankAndStatus(bank);
		receiptList = FXCollections.observableArrayList(receiptsResp.getBody());

		for (PDCReceipts receipts : receiptList) {
			pdcPayment = new PDCPayment();
			pdcPayment.setId(receipts.getId());
			pdcPayment.setAccount(receipts.getAccount());
			pdcPayment.setBank(receipts.getDepositBank());
			pdcPayment.setInstrumentDate(receipts.getInstrumentDate());
			pdcPayment.setInstrumentnumber(receipts.getInstrumentnumber());
			pdcPayment.setAmount(receipts.getAmount());
			pdcPayment.setRemark(receipts.getRemark());

			paymentList.add(pdcPayment);

		}

		FillTable();

	}

	@FXML
	void PDCRemove(ActionEvent event) {

		if (null == pdcPayment) {
			notifyMessage(5, "Please select a row from PDC TABLE...!", false);
			return;
		}

		if (null != pdcPayment.getId()) {
			ResponseEntity<PDCPayment> paymentResp = RestCaller.getPDCPaymentById(pdcPayment.getId());
			PDCPayment pdcPaymentObj = new PDCPayment();
			pdcPaymentObj = paymentResp.getBody();

			if (null != pdcPaymentObj) {

				pdcPaymentObj.setStatus("BOUNCE");
				RestCaller.updatePDCPayment(pdcPaymentObj);
				notifyMessage(5, "Successfully removed...!", false);
				fetchPDCPayment();

			} else {

				ResponseEntity<PDCReceipts> receiptsResp = RestCaller.getPDCReceiptsById(pdcPayment.getId());
				PDCReceipts pdcReceipts = receiptsResp.getBody();

				if (null != pdcReceipts) {
					pdcReceipts.setStatus("BOUNCE");
					RestCaller.updatePDCReceipts(pdcReceipts);
					notifyMessage(5, "Successfully removed...!", false);

					fetchPDCReceipts();
				}
			}
		}

	}

	@FXML
	void Reconcile(ActionEvent event) {
		
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.systemDate);
		if (null != dayEndClosureHdr) {

			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!",false);
					return;
				}

			}
		}
		
		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.systemDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!",false);
			return;
		}

		if (null == pdcPayment) {
			notifyMessage(5, "Please select a row from PDC TABLE...!", false);
			return;
		}

		if (null != pdcPayment.getId()) {

			if (null == pdcReconcileHdr) {

				if (null == dpDate.getValue()) {
					notifyMessage(5, "Please select transaction date...!", false);
					return;
				}
				pdcReconcileHdr = new PDCReconcileHdr();
				pdcReconcileHdr.setBranchCode(SystemSetting.getSystemBranch());

				transDate = Date.valueOf(dpDate.getValue());
				pdcReconcileHdr.setTransDate(transDate);

				ResponseEntity<PDCReconcileHdr> pdcReconcileHdrResp = RestCaller.savePDCReconcileHdr(pdcReconcileHdr);
				pdcReconcileHdr = pdcReconcileHdrResp.getBody();

				if (null == pdcReconcileHdr) {
//					notifyMessage(5, "Save Unsuccessful...!", false);
					return;
				}
			}

			if (null == pdcReconcileHdr) {
				return;
			}

			pdcReconcile = new PDCReconcileDtl();
			pdcReconcile.setAccount(pdcPayment.getAccount());
			pdcReconcile.setAmount(pdcPayment.getAmount());
			pdcReconcile.setBank(cmbBank.getSelectionModel().getSelectedItem());
			pdcReconcile.setIntrumentNo(pdcPayment.getInstrumentnumber());
			pdcReconcile.setPdcId(pdcPayment.getId());
			pdcReconcile.setTransDate(transDate);
			pdcReconcile.setPdcReconcileHdr(pdcReconcileHdr);
			pdcReconcile.setRemark(pdcPayment.getRemark());

			ResponseEntity<PDCPayment> paymentResp = RestCaller.getPDCPaymentById(pdcPayment.getId());
			PDCPayment pdcPaymentObj = new PDCPayment();
			pdcPaymentObj = paymentResp.getBody();

			if (null != pdcPaymentObj) {

				pdcReconcile.setTransactionDetails("CREDIT");

			} else {

				ResponseEntity<PDCReceipts> receiptsResp = RestCaller.getPDCReceiptsById(pdcPayment.getId());
				PDCReceipts pdcReceipts = receiptsResp.getBody();

				if (null != pdcReceipts) {
					pdcReconcile.setTransactionDetails("DEBIT");

				}
			}

			ResponseEntity<PDCReconcileDtl> pdcReconcileResp = RestCaller.savePDCReconcile(pdcReconcile);
			pdcReconcile = pdcReconcileResp.getBody();

			if (null != pdcReconcile) {
				reconcileList.add(pdcReconcile);
				FillTableReconcile();

				pdcReconcile = null;
				pdcPayment = null;
			}

		}

	}

	private void FillTableReconcile() {

		for (PDCReconcileDtl pdc : reconcileList) {
			if (null != pdc.getAccount()) {
//				ResponseEntity<AccountHeads> supplierResp = RestCaller.getSupplier(pdc.getAccount());
//				AccountHeads accountHeads1 = supplierResp.getBody();

//				if (null != accountHeads) {
//					pdc.setAccountName(accountHeads.getSupplierName());
//				} else {

					ResponseEntity<AccountHeads> accountHeadsResp1 = RestCaller.getAccountHeadsById(pdc.getAccount());
					AccountHeads accountHeads = accountHeadsResp1.getBody();
					if (null != accountHeads) {
						pdc.setAccountName(accountHeads.getAccountName());
					}
//				}

			}
		}

		tblReconcile.setItems(reconcileList);

		clReAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		clReAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clReBank.setCellValueFactory(cellData -> cellData.getValue().getBankProperty());
		clReInstNo.setCellValueFactory(cellData -> cellData.getValue().getIntrumentNoProperty());
		clReTransDetils.setCellValueFactory(cellData -> cellData.getValue().getTransactionDetailsProperty());

	}

	@FXML
	void ReconcileDelete(ActionEvent event) {

		if (null == pdcReconcile) {
			notifyMessage(5, "Please select a row from RECONCILE TABLE...!", false);
			return;
		}

		if (null != pdcReconcile.getId()) {
			RestCaller.deletePDCReconcileDtl(pdcReconcile.getId());

			ResponseEntity<List<PDCReconcileDtl>> pdcReconcileDtlResp = RestCaller
					.getAllPDCReconcileDtl(pdcReconcileHdr.getId());
			reconcileList = FXCollections.observableArrayList(pdcReconcileDtlResp.getBody());

			FillTableReconcile();
		}

	}

	@FXML
	void ReconcileClear(ActionEvent event) {

		pdcReconcileHdr = null;
		pdcPayment = null;
		pdcReconcile = null;
		tblPDC.getItems().clear();
		tblReconcile.getItems().clear();
		cmbBank.getSelectionModel().clearSelection();
		dpDate.setValue(LocalDate.now());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		Stage stage = (Stage) btnPayments.getScene().getWindow();
		{
			taskid = taskWindowDataEvent.getId();
			String sdate = taskWindowDataEvent.getVoucherDate();
//			customerRegistration.setCustomerName(txtAccount.getText());
			ResponseEntity<List<PDCReceipts>> resp = RestCaller.getPDCReceiptByStatusBankAndAccountAndDate(taskWindowDataEvent.getVoucherNumber(),taskWindowDataEvent.getAccountId(),sdate);
			receiptList = FXCollections.observableArrayList(resp.getBody());

			for (PDCReceipts receipts : receiptList) {
				pdcPayment = new PDCPayment();
				pdcPayment.setId(receipts.getId());
				pdcPayment.setAccount(receipts.getAccount());
				pdcPayment.setBank(receipts.getDepositBank());
				pdcPayment.setInstrumentDate(receipts.getInstrumentDate());
				pdcPayment.setInstrumentnumber(receipts.getInstrumentnumber());
				pdcPayment.setAmount(receipts.getAmount());
				pdcPayment.setRemark(receipts.getRemark());

				paymentList.add(pdcPayment);

			}

			FillTable();
		}
 }
	 

}
