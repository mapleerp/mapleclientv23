package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.util.CurrencyAmount;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountPayable;
import com.maple.mapleclient.entity.AdditionalExpense;
import com.maple.mapleclient.entity.CurrencyConversionMst;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseAdditionalExpenseDtl;
import com.maple.mapleclient.entity.PurchaseAdditionalExpenseHdr;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.PurchaseOrderDtl;
import com.maple.mapleclient.entity.PurchaseOrderHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.PoNumberEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import javafx.util.converter.NumberStringConverter;
import net.sf.jasperreports.engine.JRException;

public class ImportPurchaseCtl {

	String taskid;
	String processInstanceId;
	String purchaseid;
	PurchaseHdr purchaseHdrDummy;
	String purchasehdrpreviewid;
	PurchaseHdr purchasehdrpreview;

	Double totalAdditionalExpense = 0.0;
	Double totalAmountLocalCurrency = 0.0;
	Double totalImportExpense = 0.0;

	PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr = null;
	private static final Logger logger = LoggerFactory.getLogger(PurchaseCtl.class);
	ItemPopupCtl itemPopupCtl = new ItemPopupCtl();
	PurchaseDtl purchaseToDelete;
	AccountPayable accountPayable = null;
	String voucherNo = null;
	PurchaseOrderHdr purchaseOrderHdr = null;
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();
	private ObservableList<PurchaseHdr> purchaseHdrListTable = FXCollections.observableArrayList();
	private ObservableList<PurchaseAdditionalExpenseDtl> purchaseAdditionalExpenseList = FXCollections
			.observableArrayList();
	private ObservableList<AdditionalExpense> additionalExpenseLists = FXCollections.observableArrayList();
	private ObservableList<PurchaseAdditionalExpenseDtl> purchaseAdditionalExpenseListToADD = FXCollections
			.observableArrayList();
	private ObservableList<AccountHeads> accountList = FXCollections.observableArrayList();
	String supplierID = null;
	String IMPORT_PURCHASE_COSTING_METHOD = SystemSetting.IMPORT_PURCHASE_COSTING_METHOD;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PurchaseHdr> PurchaseHdrList = FXCollections.observableArrayList();
	AdditionalExpense additionalExpense;
	private ObservableList<PurchaseDtl> purchaseDtlList = FXCollections.observableArrayList();
	PurchaseDtl purchaseDtl;
	private ObservableList<PurchaseDtl> purchaseListTable = FXCollections.observableArrayList();
	PurchaseHdr purchasehdr = null;
	private ObservableList<PurchaseDtl> purchaseListTable1 = FXCollections.observableArrayList();

	double qtyTotal = 0;
	double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	String supplierName = "";
	String itemId = null;
	String sdate = null;

	/*
	 * We need these properties to bind with text fied and listen to the changes
	 * made by user. so that automatic calculation works
	 */
	StringProperty amountProperty = new SimpleStringProperty("");
	StringProperty rateProperty = new SimpleStringProperty("");
	StringProperty quantityProperty = new SimpleStringProperty("");
	StringProperty taxTateProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	/*
	 * To manage Item Serial
	 */
	StringProperty itemSerialProperty = new SimpleStringProperty("");
	StringProperty cessAmtProperty = new SimpleStringProperty("");

	/*
	 * To Enable disable Expiry date / Manu Date based on Batch
	 */
	StringProperty batchProperty = new SimpleStringProperty("");
	StringProperty taxAmountProperty = new SimpleStringProperty("");

	/*
	 * To Manage Total Amount Property
	 */
	StringProperty totalAmountProperty = new SimpleStringProperty("");

	@FXML
	private TextField txtSupplierName;

	@FXML
	private TextField txtSupplierGst;

	@FXML
	private TextField txtNarration;

	@FXML
	private TextField txtPONum;

	@FXML
	private TextField txtAdditionalExpenseAmt;
	@FXML
	private TextField txtSupplierInvNo;

	@FXML
	private TextField txtOurVoucherNo;

	@FXML
	private ComboBox<String> cmbCurrency;
	@FXML
	private TextField txtInvoiceTotal;

	@FXML
	private Button btnDeletePartial;
	@FXML
	private DatePicker dpSupplierInvDate;

	@FXML
	private DatePicker dpOurVoucherDate;

	@FXML
	private TextField txtFreeQty;

	@FXML
	private Button btnRefresh;

	@FXML
	private Button btnImportExcel;

	@FXML
	private Button btnImportGRN;

	@FXML
	private Button btnLoadFromPO;

	@FXML
	private TextField txtTotalFCAmount;
	@FXML
	private ComboBox<String> cmbUnit;
	@FXML
	private TableView<PurchaseHdr> tblPartialySavedList;

	@FXML
	private TableColumn<PurchaseHdr, String> clSupplierName;

	@FXML
	private TableColumn<PurchaseHdr, String> clVoucherNumber;

	@FXML
	private TableColumn<PurchaseHdr, String> clSupplierInvoiceNo;

	@FXML
	private TableColumn<PurchaseHdr, LocalDate> clInvoiceDate;

	@FXML
	private TableColumn<PurchaseHdr, LocalDate> clVoucherDate;

	@FXML
	private TableColumn<PurchaseHdr, Number> clInvoiceTotoal;
	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtPurchseRate;

	@FXML
	private TextField txtAmount;

	@FXML
	private TextField txtBatch;

	@FXML
	private TextField txtItemSerial;

	@FXML
	private Button btnAddItem;

	@FXML
	private Button btnDeleteItem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtTaxAmt;

	@FXML
	private TextField txtCessAmt;

	@FXML
	private TextField txtPreviousMRP;

	@FXML
	private DatePicker dpExpiryDate;

	@FXML
	private CheckBox chkFreeQty;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtTaxRate;

	@FXML
	private TextField txtDiscount;

	@FXML
	private CheckBox chkChangePrice;

	@FXML
	private TextField txtMRP;

	@FXML
	private TextField txtCessRate;

	@FXML
	private DatePicker dpManufacture;

	@FXML
	private TextField txtQtyTotal;

	@FXML
	private TextField txtAmtTotal;

	@FXML
	private TextField txtDiscountTotal;

	@FXML
	private TextField txtTotal;

	@FXML
	private TextField txtTotalCess;

	@FXML
	private Button btnHolPurchase;

	@FXML
	private TextArea txtTaxSplit;

	@FXML
	private TextField txtTotalDisBeforeTax;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtAdditionalExpenseTotal;
	@FXML
	private Button btnCalculate;

	@FXML
	private TextField txtGrandTotal;

	@FXML
	private TableView<PurchaseDtl> tblItemDetails;

	@FXML
	private TableColumn<PurchaseDtl, String> clItemName;

	@FXML
	private TableColumn<PurchaseDtl, Number> clQty;

	@FXML
	private TableColumn<PurchaseDtl, Number> clPurRate;

	@FXML
	private TableColumn<PurchaseDtl, Number> clFCPurchaseRate;
	@FXML
	private TableColumn<PurchaseDtl, Number> clTaxRate;

	@FXML
	private TableColumn<PurchaseDtl, Number> clFCTaxRate;
	@FXML
	private TableColumn<PurchaseDtl, Number> clamt;
	@FXML
	private TableColumn<PurchaseDtl, Number> clFCAmount;

	@FXML
	private TableColumn<PurchaseDtl, Number> clMRP;
	@FXML
	private TableColumn<PurchaseDtl, Number> clFCMrp;
	@FXML
	private TableColumn<PurchaseDtl, Number> clNetCost;

	@FXML
	private TableColumn<PurchaseDtl, Number> clCess;

	@FXML
	private ComboBox<String> cmbExpenseHead;

	@FXML
	private ComboBox<String> cmbCurrency1;

	@FXML
	private ComboBox<String> cmbAccountHeadLbl;

	@FXML
	private TextField txtConversionRate;

	@FXML
	private TextField txtTotalAmount;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnDelete;

	@FXML
	private TableView<AdditionalExpense> tblAdditionalExpense;

	@FXML
	private TableColumn<AdditionalExpense, String> clExpenseHead;

	@FXML
	private TableColumn<AdditionalExpense, String> clAccountHead;

	@FXML
	private TableColumn<AdditionalExpense, String> clCurrency;

	@FXML
	private TableColumn<AdditionalExpense, Number> clConversionRate;

	@FXML
	private TableColumn<AdditionalExpense, Number> clAmount;
	
	@FXML
	private Label lblExpryDate;

	@FXML
	private Tab MenuItemDetails;

	@FXML
	private Label lblManufDate;

	@FXML
	private Button btnSelect;

	@FXML
	private DatePicker voucherDate;

	@FXML
	private Button btnFetchInvoiceNo;

	@FXML
	private TableView<PurchaseHdr> tblInvoice;

	@FXML
	private TableColumn<PurchaseHdr, String> clVoucherNo;

	@FXML
	private TableColumn<PurchaseHdr, String> clCustomerName;

	@FXML
	private Button btnChng2PartialySaved;
	
	@FXML
	private TableView<PurchaseDtl> tbImportPurchase;

	@FXML
	private TableColumn<PurchaseDtl, String> clPurchaseItemName;

	@FXML
	private TableColumn<PurchaseDtl, String> clPurchaseBatch;

	@FXML
	private TableColumn<PurchaseDtl, String> clImportPurchaseDtlId;
	
	@FXML
	private TextField txtImportExpenseItemName;

	@FXML
	private TextField txtImportExpenseBatch;

	@FXML
	private ComboBox<String> cmbImportExpense;

	@FXML
	private TextField txtImportExpenseAmount;

	@FXML
	private Button btnImportExpenseAdd;

	@FXML
	private Button btnImportExpenseDelete;

	@FXML
	private TableView<PurchaseAdditionalExpenseDtl> tbImportExpense;
	
	@FXML
	private TableColumn<PurchaseAdditionalExpenseDtl, String> clImportExpenseItemName;

	@FXML
	private TableColumn<PurchaseAdditionalExpenseDtl, String> clImportExpenseBatch;

	@FXML
	private TableColumn<PurchaseAdditionalExpenseDtl, String> clImportExpense;

	@FXML
	private TableColumn<PurchaseAdditionalExpenseDtl, Number> clImportExpenseAmount;

	@FXML
	private Button btnImportExpenseRefresh;

	@FXML
	private Button btnImportExpenseSave;

	@FXML
	private Button btnPreview;

	@FXML
	private Label lblExpenseTotalCrrency;

	// ==========================

	@FXML
	private Label lblPurchaseRate;

	// ==========================

	@FXML
	void ChangetoPartialySaved(ActionEvent event) {

		logger.info("PURCHASE ======STARTED PARTIALY SAVE");

		if (null == voucherNo) {
			notifyMessage(5, "Please voucher No...!!!", false);
			return;
		} else {

			ResponseEntity<List<PurchaseHdr>> purchaseHdrResp = RestCaller.getPurchaseHdrByVoucherNumber(voucherNo,
					sdate);

			if (purchaseHdrResp.getBody().size() == 0) {
				return;
			}
			purchasehdr = purchaseHdrResp.getBody().get(0);
			if (null != purchasehdr) {
				purchasehdr.setFinalSavedStatus("N");
				RestCaller.updatePurchasehdrToPArtialSave(purchasehdr);
				notifyMessage(5, "Successfull...!!!", true);
				fetchPurchaseBill();
			}

			logger.info("PURCHASE ======COMPLETED PARTIALY SAVE");
		}

	}

	@FXML
	void FetchInvoiceNo(ActionEvent event) {
		tblInvoice.getItems().clear();
		fetchPurchaseBill();
	}

	private void fetchPurchaseBill() {
		logger.info("PURCHASE ======FETCH PURCHASE BILL STARTED");
		purchaseHdrListTable.clear();
		if (null == voucherDate.getValue()) {
			notifyMessage(5, "Please select date...!!!", false);
			return;
		} else {

			java.util.Date date = SystemSetting.localToUtilDate(voucherDate.getValue());
			sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

			ArrayList invoice = new ArrayList();
			invoice = RestCaller.getPurchaseBillsByDate(sdate);

			String voucherNo = "";
			String supplierName = "";
			Iterator itr = invoice.iterator();

			while (itr.hasNext()) {
				List element = (List) itr.next();
				voucherNo = (String) element.get(1);
				supplierName = (String) element.get(0);

				if (!voucherNo.equals("") && !voucherNo.equals(null)) {

					PurchaseHdr purchaseHdr = new PurchaseHdr();
					purchaseHdr.setVoucherNumber(voucherNo);
					purchaseHdr.setSupplierName(supplierName);
					purchaseHdrListTable.add(purchaseHdr);
				}
			}
			logger.info("PURCHASE ======FETCH PURCHASE BILL COMPLETED");
			FillInvoiceTable();
		}

	}

	private void FillInvoiceTable() {

		tblInvoice.setItems(purchaseHdrListTable);
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clCustomerName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());

	}

	@FXML
	void SupplierOnKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtSupplierGst.requestFocus();
		}

	}

	@FXML
	void SupplierInvDateKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpOurVoucherDate.requestFocus();
		}
	}

	@FXML
	void OurVouchrDateOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtNarration.requestFocus();
		}

	}

	@FXML
	void NarrationOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtSupplierInvNo.requestFocus();
		}

	}

	@FXML
	void SupplierInvNoOnKeyPresssed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtInvoiceTotal.requestFocus();
		}
	}

	@FXML
	void OurVoucherNoOnKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtInvoiceTotal.requestFocus();
		}

	}

	@FXML
	void ItemNameOnkeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			txtItemName.clear();
			txtBarcode.clear();
			txtQty.clear();
			txtPurchseRate.clear();
			txtAmount.clear();
			txtDiscount.clear();
			txtCessRate.clear();
			txtTaxAmt.clear();
			txtCessAmt.clear();
			txtTaxRate.clear();
			txtMRP.clear();
			txtBatch.clear();
			if (null == cmbCurrency.getSelectionModel().getSelectedItem()) {
				notifyMessage(3, "Select Currency");
				cmbCurrency.requestFocus();
				return;
			}
			showPopup();
			txtQty.requestFocus();
			// txtBarcode.requestFocus();
		}
	}

	@FXML
	void InvcTotalOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtPONum.requestFocus();
		}

	}

	@FXML
	void PoNoOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtItemName.requestFocus();
		}
	}

	@FXML
	void PrvOutBlns(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpSupplierInvDate.requestFocus();
		}

	}

	@FXML
	void QtyOnKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtPurchseRate.requestFocus();
		}

	}

	@FXML
	void PurchaseRateOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtBatch.requestFocus();
		}
	}

	@FXML
	void batchActon(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (!txtBatch.getText().trim().isEmpty()) {
				dpManufacture.requestFocus();
			} else
				btnAddItem.requestFocus();
		}

	}

	@FXML
	void addItemWithNoBatch(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			if (null == txtBatch.getText()) {
				addItem();
			}
		}

	}

	@FXML
	void ManfDAteOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpExpiryDate.requestFocus();
		}
	}

	@FXML
	void ExpiryDateOnkeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnAddItem.requestFocus();
		}
	}

	// ==========================anandu===============invoice preview==============
	@FXML
	void previewInvoice(ActionEvent event) {

		try {
			JasperPdfReportService.importPurchaseInvoice(purchasehdrpreviewid);
		} catch (JRException e) {
		}

	}

	// ==========================anandu===============invoice preview==============

	// =================================end invoice===================

	@FXML
	private void initialize() {

		dpExpiryDate = SystemSetting.datePickerFormat(dpExpiryDate, "dd/MMM/yyyy");
		dpManufacture = SystemSetting.datePickerFormat(dpManufacture, "dd/MMM/yyyy");
		//dpOurVoucherDate = SystemSetting.datePickerFormat(dpOurVoucherDate, "dd/MMM/yyyy");
		dpSupplierInvDate = SystemSetting.datePickerFormat(dpSupplierInvDate, "dd/MMM/yyyy");

		// ========================================

		btnSave.setDisable(true);
		ResponseEntity<List<CurrencyMst>> currencyMst = RestCaller.getallCurrencyMst();
		for (int i = 0; i < currencyMst.getBody().size(); i++) {
			cmbCurrency.getItems().add(currencyMst.getBody().get(i).getCurrencyName());

		}

		ResponseEntity<List<AccountHeads>> accountList = RestCaller.getAccountHeadsByExpense();
		System.out.println(accountList.getBody() + "acount size");

		for (int i = 0; i < accountList.getBody().size(); i++) {
			cmbExpenseHead.getItems().add(accountList.getBody().get(i).getAccountName());
		}

		cmbAccountHeadLbl.getItems().add("INCLUDED IN BILL");
		cmbAccountHeadLbl.getItems().add("NOT IN BILL");
		dpOurVoucherDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.getApplicationDate()));
		dpOurVoucherDate.setEditable(false);
		logger.info("PURCHASE =============== INITIALIZATION STARTED");
		txtSupplierName.requestFocus();

		// -----new purchasedlt----------
		purchaseDtl = new PurchaseDtl();

		txtAmount.textProperty().bindBidirectional(amountProperty);
		txtQty.textProperty().bindBidirectional(quantityProperty);
		txtPurchseRate.textProperty().bindBidirectional(rateProperty);
		txtTaxAmt.textProperty().bindBidirectional(taxAmountProperty);
		txtTaxRate.textProperty().bindBidirectional(taxTateProperty);
		txtItemSerial.textProperty().bindBidirectional(itemSerialProperty);
		itemSerialProperty.set("1");
		txtCessRate.textProperty().bindBidirectional(cessRateProperty);
		txtCessAmt.textProperty().bindBidirectional(cessAmtProperty);
		txtBatch.textProperty().bindBidirectional(batchProperty);

		eventBus.register(this);
		txtInvoiceTotal.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtInvoiceTotal.setText(oldValue);
				}
			}
		});
		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});

		txtMRP.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtMRP.setText(oldValue);
				}
			}
		});

		txtPurchseRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPurchseRate.setText(oldValue);
				}
			}
		});

		txtItemSerial.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtItemSerial.setText(oldValue);
				}
			}
		});

		txtTaxRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtTaxRate.setText(oldValue);
				}
			}
		});
		txtCessRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCessRate.setText(oldValue);
				}
			}
		});
		txtDiscount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtDiscount.setText(oldValue);
				}
			}
		});

//		additionalExpense = new AdditionalExpense("Additional Expense");
//		AdditionalExpenseList.add(additionalExpense);
//
//		txtConversionRate.textProperty().bindBidirectional(AdditionalExpenseList.get(0).getConversionRateProperty(),
//				new NumberStringConverter());

		/*
		 * Listen the property change and calculate corresponding values and set to the
		 * text box
		 */
//		cmbCurrency.valueProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				
//				if(null == cmbCurrency.getSelectionModel() ||null == cmbCurrency.getSelectionModel().getSelectedItem())
//				{
//					if(null != SystemSetting.getUser().getCompanyMst().getCurrencyName())
//					{
//						if(!cmbCurrency.getSelectionModel().getSelectedItem().equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName()))
//						{
//							
//						}
//					}
//					else
//					{
//						notifyMessage(3,"Company Currency Not Set");
//						return;
//					}
//				}
//			}
//			
//		});

		amountProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0)) {

					BigDecimal qty = new BigDecimal(txtQty.getText());
					qty = qty.setScale(3, BigDecimal.ROUND_CEILING);

					BigDecimal amount = new BigDecimal((String) newValue);
					amount = amount.setScale(3, BigDecimal.ROUND_CEILING);

					if (qty.doubleValue() > 0) {
						BigDecimal newrate = amount.divide(qty);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						rateProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		quantityProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;

				if ((txtPurchseRate.getText().length() > 0)) {
					BigDecimal rate = new BigDecimal(txtPurchseRate.getText());
					rate = rate.setScale(3, BigDecimal.ROUND_CEILING);

					BigDecimal qty = new BigDecimal((String) newValue);
					qty = qty.setScale(3, BigDecimal.ROUND_CEILING);

					if (rate.doubleValue() > 0) {
						BigDecimal newAmount = qty.multiply(rate);
						newAmount = newAmount.setScale(3, BigDecimal.ROUND_CEILING);
						amountProperty.set(newAmount.toPlainString());

					}
				} else if ((txtAmount.getText().length() > 0)) {
					BigDecimal amount = new BigDecimal(txtAmount.getText());

					amount = amount.setScale(3, BigDecimal.ROUND_CEILING);

					BigDecimal qty = new BigDecimal((String) newValue);
					qty = qty.setScale(3, BigDecimal.ROUND_CEILING);

					if (amount.doubleValue() > 0) {
						BigDecimal newRate = amount.divide(qty);
						newRate = newRate.setScale(3, BigDecimal.ROUND_CEILING);
						rateProperty.set(newRate.toPlainString());

					}
				}

				if ((txtTaxRate.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					taxAmountProperty.set("0.0");
				}

				if ((txtPurchseRate.getText().length() > 0) && txtCessRate.getText().length() > 0) {

					cessAmtProperty.set("0.0");
				}
			}
		});

		rateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;

				if ((txtQty.getText().length() > 0)) {
					BigDecimal qty = new BigDecimal(txtQty.getText());
					qty = qty.setScale(3, BigDecimal.ROUND_CEILING);

					BigDecimal rate = new BigDecimal((String) newValue);
					rate = rate.setScale(3, BigDecimal.ROUND_CEILING);

					if (qty.doubleValue() > 0) {
						BigDecimal newAmount = qty.multiply(rate);
						newAmount = newAmount.setScale(3, BigDecimal.ROUND_CEILING);
						amountProperty.set(newAmount.toPlainString());

					}
				}

				if ((txtTaxRate.getText().length() > 0) && txtQty.getText().length() > 0) {

					taxAmountProperty.set("0.0");
				}

				if ((txtQty.getText().length() > 0) && txtCessRate.getText().length() > 0) {

					cessAmtProperty.set("0.0");
				}
			}
		});

		batchProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0) {
					dpExpiryDate.setDisable(true);
					dpManufacture.setDisable(true);
					lblExpryDate.setDisable(true);
					lblManufDate.setDisable(true);

				} else {
					dpExpiryDate.setDisable(false);
					dpManufacture.setDisable(false);
					lblExpryDate.setDisable(false);
					lblManufDate.setDisable(false);
				}

			}
		});

		// --------------tax amt
		taxTateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					taxAmountProperty.set("0.0");
				}
			}
		});

		cessRateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					cessAmtProperty.set("0.0");
				}
			}
		});

		// ==========table selection
		tblItemDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					purchaseDtl = new PurchaseDtl();
					txtItemName.setText(newSelection.getItemName());
					ResponseEntity<ItemMst> getItems = RestCaller.getItemByNameRequestParam(txtItemName.getText());

					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItems.getBody().getUnitId());
					txtBarcode.setText(getItems.getBody().getBarCode());
					cmbUnit.getSelectionModel().select(getUnit.getBody().getUnitName());
					cmbUnit.setValue(getUnit.getBody().getUnitName());
					txtQty.setText(Double.toString(newSelection.getQty()));
					txtAmount.setText(Double.toString(newSelection.getAmount()));
					txtPurchseRate.setText(Double.toString(newSelection.getPurchseRate()));
//					itemSerialProperty.set(newSelection.getItemSerial());
//					txtItemSerial.setText(newSelection.getItemSerial());
					txtMRP.setText(Double.toString(getItems.getBody().getStandardPrice()));
					txtCessRate.setText(Double.toString(newSelection.getCessRate()));
					txtDiscount.setText("0.0");

					System.out.println("Batchhhhhhhhhhhhhhhhhhhhhhhhhhh" + newSelection.getBatch());
					txtBatch.setText(newSelection.getBatch());
					dpExpiryDate.setValue(newSelection.getexpiryDate().toLocalDate());
					txtTaxRate.setText(Double.toString(newSelection.getTaxRate()));
					txtTaxAmt.setText(Double.toString(newSelection.getTaxAmt()));
					System.out.println("getSelectionModel--getId");

					System.out.println("DELETE24-" + newSelection.getId());

					purchaseDtl.setId(newSelection.getId());

					// version1.7
					purchaseDtl.setPurchaseOrderDtl(newSelection.getPurchaseOrderDtl());
					// version1.7 ends now bt continues.....

					System.out.println("DELETE--" + purchaseDtl.getId());
					if (!cmbCurrency.getSelectionModel().getSelectedItem()
							.equalsIgnoreCase(purchasehdr.getCompanyMst().getCurrencyName())) {
						txtAmount.setText(Double.toString(newSelection.getFcAmount()));
						txtPurchseRate.setText(Double.toString(newSelection.getFcPurchaseRate()));
						txtMRP.setText(Double.toString(newSelection.getFcMrp()));
						txtCessRate.setText(Double.toString(newSelection.getFcCessRate()));
						txtDiscount.setText("0.0");
						txtTaxRate.setText(Double.toString(newSelection.getFcTaxRate()));
						txtTaxAmt.setText(Double.toString(newSelection.getFcTaxAmt()));
					}

				}
			}
		});

		cmbCurrency1.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null != newValue) {
					ResponseEntity<CurrencyMst> currencyMst = RestCaller
							.getCurrencyMstByName(cmbCurrency1.getSelectionModel().getSelectedItem());
					if (null != currencyMst.getBody()) {
						ResponseEntity<CurrencyConversionMst> currencyConversionMst = RestCaller
								.getCurrencyConversionMstByCurrencyId(currencyMst.getBody().getId());
						if (null != currencyConversionMst.getBody()) {
							txtConversionRate
									.setText(Double.toString(currencyConversionMst.getBody().getConversionRate()));
						} else {
//							txtConversionRate.setText("0");
							notifyMessage(3, "Currency COnversion Not set");
							txtConversionRate.clear();
							return;
						}

					}
				}
			}

		});
		cmbImportExpense.getItems().add("IMPORT DUTY");
		tbImportPurchase.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					purchaseAdditionalExpenseHdr = new PurchaseAdditionalExpenseHdr();
					ResponseEntity<PurchaseDtl> purchaseDtl = RestCaller.getPurchaseDtlById(newSelection.getId());
					purchaseAdditionalExpenseHdr.setPurchaseDtl(purchaseDtl.getBody());
					txtImportExpenseItemName.setText(newSelection.getItemName());
					txtImportExpenseBatch.setText(newSelection.getBatch());
				}
			}
		});

		tblAdditionalExpense.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						System.out.println("getSelectionModel");
						if (null != newSelection.getId()) {
							additionalExpense = new AdditionalExpense();
							additionalExpense.setId(newSelection.getId());
							ResponseEntity<AccountHeads> accountHead = RestCaller
									.getAccountById(newSelection.getAccountId());
							cmbExpenseHead.getSelectionModel().select(accountHead.getBody().getAccountName());
							cmbAccountHeadLbl.getSelectionModel().select(newSelection.getExpenseHead());
							txtAdditionalExpenseAmt.setText(Double.toString(newSelection.getAmount()));
							ResponseEntity<CurrencyMst> currencyMstesp = RestCaller
									.getcurrencyMsyById(newSelection.getCurrencyId());
							{
								cmbCurrency1.getSelectionModel().select(currencyMstesp.getBody().getCurrencyName());
								ResponseEntity<CurrencyConversionMst> currencyConversionMst = RestCaller
										.getCurrencyConversionMstByCurrencyId(newSelection.getCurrencyId());
								txtConversionRate
										.setText(Double.toString(currencyConversionMst.getBody().getConversionRate()));
								txtAdditionalExpenseAmt.setText(Double.toString(newSelection.getFcAmount()));
							}
						}
					}
				});
		tblPartialySavedList.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						System.out.println("getSelectionModel");
						System.out.println("getSelectionModel" + newSelection);
						if (null != newSelection.getId()) {

							purchasehdr = new PurchaseHdr();
							purchasehdr.setId(newSelection.getId());
							purchasehdr.setVoucherNumber(newSelection.getVoucherNumber());
							purchasehdr.setSupplierId(newSelection.getSupplierId());
							purchasehdr.setNarration(newSelection.getNarration());
							purchasehdr.setInvoiceDate(newSelection.getInvoiceDate());
							purchasehdr.setourVoucherDate(newSelection.getourVoucherDate());
							purchasehdr.setSupplierInvNo(newSelection.getSupplierInvNo());
							purchasehdr.setInvoiceTotal(newSelection.getInvoiceTotal());
							purchasehdr.setpONum(newSelection.getpONum());
							purchasehdr.setCompanyMst(newSelection.getCompanyMst());
							supplierID = newSelection.getSupplierId();
							System.out.println("==purchasehdr==" + purchasehdr);
							// Get additional Expense
							ResponseEntity<List<AdditionalExpense>> additionalExpenseList = RestCaller
									.getallAdditionalExpenseByHdrId(purchasehdr.getId());
							if (additionalExpenseList.getBody().size() > 0) {
								additionalExpenseLists = FXCollections
										.observableArrayList(additionalExpenseList.getBody());
								fillAdditionalExpense();
							}

						}
					}
				});

		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.isEmpty()) {

					ResponseEntity<UnitMst> initResp = RestCaller.getUnitByName(newValue);
					UnitMst unitMst = initResp.getBody();

					if (null == unitMst || null == itemId) {
						return;
					}

					ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
							.getPriceDefenitionMstByName("COST PRICE");
					PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
					if (null != priceDefenitionMst) {

						ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
								.getPriceDefinitionByItemIdAndPriceId(itemId, priceDefenitionMst.getId(),
										unitMst.getId());

						PriceDefinition priceDefinition = priceDefenitionResp.getBody();

						if (null != priceDefinition) {
							txtPurchseRate.setText(Double.toString(priceDefinition.getAmount()));
						}

					}
				}

			}

		});

		cmbCurrency.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null != newValue) {

					System.out.println(newValue);
					System.out.println("0999999999");

					lblPurchaseRate.setText("Rate in " + newValue);
					cmbCurrency1.getItems().clear();
					cmbCurrency1.getItems().add(newValue);

					cmbCurrency1.getItems().add(SystemSetting.getUser().getCompanyMst().getCurrencyName());
					cmbCurrency1.getSelectionModel().select(newValue);
					lblExpenseTotalCrrency.setText("Expense in " + newValue);
				}
			}

		});

//		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
//			@Override
//			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//				ResponseEntity<ItemMst> getItems = RestCaller.getItemByName(txtItemName.getText());
//				//ResponseEntity<List<MultiUnitMst>> getMultiUnit = RestCaller.getMultiUnitByItemId(getItems.getBody().getId());
//				ResponseEntity<Uni>
//				for (MultiUnitMst multi : multiUnitList)
//				{
//					
//				}
//				if(null != getMultiUnit.getBody())
//				{
//				txtBarcode.setText(getMultiUnit.getBody().getBarCode());
//				}
//				else
//					txtBarcode.setText(getItems.getBody().getBarCode());
//			}
//			});
//-----------------refresh table-- get partialy saved purchase

		ResponseEntity<List<PurchaseHdr>> purchaseHdrSaved = RestCaller.getPartialyPurchaseHdr();

		purchaseHdrListTable = FXCollections.observableArrayList(purchaseHdrSaved.getBody());
		fillPartialySaveTable();

		tblInvoice.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getVoucherNumber()) {
					voucherNo = newSelection.getVoucherNumber();
				}
			}
		});

		logger.info("PURCHASE =============== INITIALIZATION COMPLETED");
	}

	@FXML
	void addItem(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void AddItem(ActionEvent event) {

		addItem();

	}

	@FXML
	void Calculate(ActionEvent event) {

	}

	@FXML
	void DeleteItem(ActionEvent event) {
		btnSave.setDisable(true);
		if (null != purchaseDtl) {
			if (null != purchaseDtl.getId()) {

				// version1.7
				ResponseEntity<PurchaseDtl> getPur = RestCaller.getPurchaseDtlById(purchaseDtl.getId());
				if (null != getPur.getBody().getPurchaseOrderDtl()) {
					if (!getPur.getBody().getPurchaseOrderDtl().equalsIgnoreCase(null))

					{
						PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
						ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller
								.getPurchaseOrderDtlById(getPur.getBody().getPurchaseOrderDtl());
						puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
						puchaseOrderDtl.setReceivedQty(0.0);
						puchaseOrderDtl.setStatus("OPEN");
						RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
						ResponseEntity<PurchaseOrderHdr> getPurOrdrHdr = RestCaller
								.getPurchaseOrderByDtlId(puchaseOrderDtl.getId());
						String vdate = SystemSetting.UtilDateToString(getPurOrdrHdr.getBody().getVoucherDate(),
								"yyyy-MM-dd");
						ResponseEntity<List<PurchaseOrderDtl>> getPurchaseDtl = RestCaller
								.getPurchaseOrderDtlByVoucherNoAndDate(getPurOrdrHdr.getBody().getVoucherNumber(),
										vdate);
//	        		if(getPurchaseDtl.getBody().size()==0)
//	        		{
						getPurOrdrHdr.getBody().setFinalSavedStatus("OPEN");
						RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());
						//
//	        		}
					}
				}
				/// version1.7end

				RestCaller.purchaseDtlDelete(purchaseDtl.getId());
				purchaseDtl = null;
				/////////////////////
				getPurchaseDtls();

				clearFields();

			}
		}

	}



	public void getPurchaseNonEditable(String voucherNo) {
		ResponseEntity<PurchaseHdr> getPurchase = RestCaller.getPurchaseHdrByVoucher(voucherNo);
		purchasehdr = getPurchase.getBody();
		ResponseEntity<AccountHeads> getAccountHeads = RestCaller.getAccountHeadsById(purchasehdr.getSupplierId());
		txtSupplierName.setText(getAccountHeads.getBody().getAccountName());
		txtSupplierInvNo.setText(purchasehdr.getSupplierInvNo());
		txtSupplierGst.setText(getAccountHeads.getBody().getPartyGst());
		txtSupplierGst.setEditable(false);
		txtSupplierInvNo.setEditable(false);
		txtSupplierName.setEditable(false);
		btnAddItem.setVisible(false);
		btnDeleteItem.setVisible(false);
		btnSave.setVisible(false);
		btnHolPurchase.setVisible(false);
		btnLoadFromPO.setVisible(false);
		getPurchaseDtls();
	}

	private void getPurchaseDtls() {

		purchaseListTable.clear();
		ArrayList pur = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		pur = RestCaller.SearchPurchaseDtls(purchasehdr.getId());
		Iterator itr = pur.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object itemName = lm.get("itemName");
			Object qty = lm.get("qty");
			Object purchseRate = lm.get("purchseRate");
			Object taxAmt = lm.get("taxAmt");
			Object cessAmt = lm.get("cessAmt");
			Object amount = lm.get("amount");
			Object mrp = lm.get("mrp");
			Object netCost = lm.get("netCost");
			Object batch = lm.get("batch");
			Object expiryDate = lm.get("expiryDate");
			Object id = lm.get("id");
			if (id != null) {
				PurchaseDtl purchaseDtl = new PurchaseDtl();
				purchaseDtl.setAmount((Double) amount);
				purchaseDtl.setItemName((String) itemName);
				purchaseDtl.setQty((Double) qty);
				purchaseDtl.setPurchseRate((Double) purchseRate);
				purchaseDtl.setTaxAmt((Double) taxAmt);
				purchaseDtl.setCessAmt((Double) cessAmt);
				purchaseDtl.setMrp((Double) mrp);
				purchaseDtl.setNetCost((Double) netCost);
				purchaseDtl.setId((String) id);
				purchaseDtl.setBatch((String) batch);
				if (null != expiryDate)
					purchaseDtl.setexpiryDate(Date.valueOf((String) expiryDate));
				purchaseListTable.add(purchaseDtl);

			}

		}
		for (PurchaseDtl purchasedtl : purchaseListTable) {
			ResponseEntity<PurchaseAdditionalExpenseHdr> purchaseAdditionalExpenseHdr = RestCaller
					.getPurchaseAdditionalExpenseHdrByPurchaseDtl(purchasedtl.getId());
			if (null != purchaseAdditionalExpenseHdr.getBody()) {
				ResponseEntity<List<PurchaseAdditionalExpenseDtl>> purchaseAddExpDtl = RestCaller
						.getpurchaseAdditionalExpenseDtlByHdrId(purchaseAdditionalExpenseHdr.getBody().getId());
				purchaseAdditionalExpenseListToADD = FXCollections.observableArrayList(purchaseAddExpDtl.getBody());
				purchaseAdditionalExpenseList.addAll(purchaseAdditionalExpenseListToADD);
				fillImportExpense();

			}
		}

		FillTable();

		setTotal();

	}

	@FXML
	void Holdthispurchase(ActionEvent event) {
		logger.info("PURCHASE =============== HOLD PURCHSE STARTED");
		try {
			logger.info("save===" + purchasehdr);
			ResponseEntity<List<PurchaseDtl>> purchasetdtlSaved = RestCaller.getPurchaseDtl(purchasehdr);
			if (purchasetdtlSaved.getBody().size() == 0) {
				return;
			}

			purchasehdr.setFinalSavedStatus("N");
			RestCaller.updatePurchasehdr(purchasehdr);
			logger.info("PURCHASE =============== purchase Hdr UPDATED");
			purchasehdr = null;
			purchaseDtl = null;
			purchaseListTable.clear();
			itemSerialProperty.set("1");
			clearFieldshdr();
			tblItemDetails.getItems().clear();
			tblAdditionalExpense.getItems().clear();
			additionalExpenseLists.clear();
			tbImportPurchase.getItems().clear();
			tbImportExpense.getItems().clear();

		} catch (Exception e) {
			logger.info("PURCHASE =============== " + e);
			e.printStackTrace();
		}

	}

	@FXML
	void ImportExcel(ActionEvent event) {

	}

	@FXML
	void ImportGRN(ActionEvent event) {

	}

	@FXML
	void LoadFromPO(ActionEvent event) {

	}

	@FXML
	void RefreshTable(ActionEvent event) {

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<PurchaseHdr>> purchaseHdrSaved = RestCaller.getPartialyPurchaseHdr();
		purchaseHdrListTable = FXCollections.observableArrayList(purchaseHdrSaved.getBody());
		System.out.println("purchaseHdrListTable--" + purchaseHdrListTable);
		fillPartialySaveTable();

		voucherDate.setValue(null);
		tblInvoice.getItems().clear();

	}

	@FXML
	void SaveOnKey(KeyEvent event) {
		if (event.getCode() == KeyCode.S && event.isControlDown()) {
			finalSave();
		}
	}

	@FXML
	void Save(ActionEvent event) {
		finalSave();
	}

	private void finalSave() {
		try {
			// version1.7

			if (null == dpSupplierInvDate.getValue()) {
				notifyMessage(3, "Select Supplier Invoice Date");
				dpSupplierInvDate.requestFocus();
				return;
			}
			if (txtSupplierInvNo.getText().trim().isEmpty()) {
				notifyMessage(3, "Type Supplier Invoice No ");
				txtSupplierInvNo.getInputMethodRequests();
				return;
			}

			// version1.7ends

			Double invoiceAmount = 0.0;
			logger.info("SAVE===" + purchasehdr);
			ResponseEntity<List<PurchaseDtl>> purchasetdtlSaved = RestCaller.getPurchaseDtl(purchasehdr);
			if (purchasetdtlSaved.getBody().size() == 0) {
				return;
			}

			updateCostPrice(purchasehdr);

			// version1.7
			purchasehdr.setSupplierInvNo(txtSupplierInvNo.getText());
			purchasehdr.setInvoiceDate(Date.valueOf(dpSupplierInvDate.getValue()));
			// version1.7ends

			ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadsByName(txtSupplierName.getText());
			AccountHeads accountHeads = accountHeadsResp.getBody();
			purchasehdr.setSupplierId(accountHeads.getId());

			System.out.println("voucherNo----" + purchasehdr.getVoucherNumber());
			String vouchernumber = purchasehdr.getVoucherNumber();
			logger.info("PURCHASE ============UPDATED ACCOUNT PAYABLE ");
			////////////////////////////////////
			purchasehdr.setFinalSavedStatus("Y");
			if (null == vouchernumber) {
				String financialYear = SystemSetting.getFinancialYear();
				String pNo = RestCaller.getVoucherNumber(financialYear + "PVN");

				pNo = SystemSetting.systemBranch + pNo;
				purchasehdr.setVoucherNumber(pNo);

				java.util.Date date = SystemSetting.localToUtilDate(dpOurVoucherDate.getValue());
				java.sql.Date sqlDate = new java.sql.Date(date.getTime());

				purchasehdr.setourVoucherDate(sqlDate);
			}
			Double expenseTotal = 0.0;
			Double amountTotal = 0.0;
			Double taxTotal = 0.0;
			if (!txtAdditionalExpenseTotal.getText().trim().isEmpty()) {
				expenseTotal = Double.parseDouble(txtAdditionalExpenseTotal.getText());
			}
			if (!txtTotal.getText().trim().isEmpty()) {
				taxTotal = Double.parseDouble(txtTotal.getText());
			}
			if (!txtAmtTotal.getText().trim().isEmpty()) {
				amountTotal = Double.parseDouble(txtAmtTotal.getText());
			}
			purchasehdr.setInvoiceTotal(expenseTotal + taxTotal + amountTotal);
			Double fcInvoiceTotal = 0.0;

			if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {

				purchasehdr.setCurrency(cmbCurrency.getSelectionModel().getSelectedItem());

				String toCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
				String fromCurrency = purchasehdr.getCompanyMst().getCurrencyName();

				fcInvoiceTotal = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
						Double.parseDouble(txtGrandTotal.getText()));

				purchasehdr.setFcInvoiceTotal(fcInvoiceTotal);

			}
			java.util.Date uDate = purchasehdr.getourVoucherDate();
			String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
			
			purchasehdr.setUserId(SystemSetting.getUser().getId());

//		if(Double.parseDouble(txtAmtTotal.getText()) >= invoiceAmount-2 && 
//				Double.parseDouble(txtAmtTotal.getText()) <= invoiceAmount+2)
//		{
			
			purchasehdr.setPurchaseType("Import Purchase");
			

			RestCaller.updatePurchasehdr(purchasehdr);
			boolean status = false;
			if (null != purchaseOrderHdr) {

				ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtl = RestCaller
						.getPurchaseOrderDtl(purchaseOrderHdr);
				ResponseEntity<List<PurchaseDtl>> getPurchaseDtl = RestCaller.getPurchaseDtl(purchasehdr);
				for (PurchaseOrderDtl purOdr : getPurchaseOrderDtl.getBody()) {
					for (PurchaseDtl purDtl : getPurchaseDtl.getBody()) {
						if (purOdr.getItemId().equalsIgnoreCase(purDtl.getItemId())) {
//							purOdr.setReceivedQty(purDtl.getQty());
							purOdr.setReceivedQty(purOdr.getReceivedQty() + purDtl.getQty());
							if (purOdr.getQty() - purOdr.getReceivedQty() <= 0) {

								purOdr.setStatus("CLOSED");
							} else {

								purOdr.setStatus("OPEN");
							}
							RestCaller.updatePurchaseOrderDtl(purOdr);
						}
					}
				}
				ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtlAftrUpdate = RestCaller
						.getPurchaseOrderDtl(purchaseOrderHdr);
				for (PurchaseOrderDtl purOdr : getPurchaseOrderDtlAftrUpdate.getBody()) {
					if (purOdr.getStatus().equalsIgnoreCase("OPEN")) {
						status = true;
						break;
					}
				}
				if (!status) {
					purchaseOrderHdr.setFinalSavedStatus("CLOSED");
					RestCaller.updatePurchaseOrderHdr(purchaseOrderHdr);

				}
			}

			// version1.7

			for (PurchaseDtl purDtl : purchaseDtlList) {
				if (null != purDtl.getPurchaseOrderDtl()) {
					if (!purDtl.getPurchaseOrderDtl().equalsIgnoreCase(null)) {
						PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
						ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller
								.getPurchaseOrderDtlById(purDtl.getPurchaseOrderDtl());
						puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
						// puchaseOrderDtl.setReceivedQty(purDtl.getQty());
						puchaseOrderDtl.setStatus("OPEN");
						RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
						ResponseEntity<PurchaseOrderHdr> getPurOrdrHdr = RestCaller
								.getPurchaseOrderByDtlId(puchaseOrderDtl.getId());
						ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtlAftrUpdate = RestCaller
								.getPurchaseOrderDtl(getPurOrdrHdr.getBody());
						for (PurchaseOrderDtl purOdr : getPurchaseOrderDtlAftrUpdate.getBody()) {
							if (purOdr.getStatus().equalsIgnoreCase("OPEN")) {
								status = true;
								break;
							}
						}
						if (!status) {
							purchaseOrderHdr.setFinalSavedStatus("CLOSED");
							RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());

						} else {
							purchaseOrderHdr.setFinalSavedStatus("OPEN");
							RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());

						}
					}
				}

			}

			/// version1.7ends

			status = false;
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(purchasehdr.getBranchCode());
			dayBook.setDrAccountName("PURCHASE ACCOUNT");
			dayBook.setDrAmount(purchasehdr.getInvoiceTotal());
			dayBook.setNarration(txtSupplierName.getText() + purchasehdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(purchasehdr.getVoucherNumber());
			dayBook.setSourceVoucherType("PURCHASE");
			dayBook.setCrAccountName(txtSupplierName.getText());
			dayBook.setCrAmount(purchasehdr.getInvoiceTotal());
			dayBook.setsourceVoucherDate(purchasehdr.getourVoucherDate());
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			notifyMessage(5, "saved", true);

			purchaseid = purchasehdr.getId();
			purchaseHdrDummy = purchasehdr;

			// version1.6
			String vNo = purchasehdr.getVoucherNumber();
			purchasehdr = null;
			purchaseOrderHdr = null;
			purchaseDtl = null;
			purchaseListTable.clear();
			itemSerialProperty.set("1");
			tblItemDetails.getItems().clear();
			// version1.6 ends

			clearFieldshdr();
			tbImportExpense.getItems().clear();
			tbImportPurchase.getItems().clear();
			purchaseAdditionalExpenseList.clear();
			txtImportExpenseItemName.clear();
			txtImportExpenseBatch.clear();
			cmbImportExpense.getSelectionModel().clearSelection();
			txtImportExpenseAmount.clear();
			cmbCurrency.getSelectionModel().clearSelection();
			txtAdditionalExpenseTotal.clear();
			tblAdditionalExpense.getItems().clear();
			additionalExpenseLists.clear();
			btnSave.setDisable(true);
			tbImportExpense.getItems().clear();
			purchaseAdditionalExpenseList.clear();
		
			try {
				// JasperPdfReportService.importPurchaseInvoice(purchaseid);
				JasperPdfReportService.importPurchaseInvoice(purchaseid);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			

		} catch (Exception e) {
			logger.info("PURCHASE ============ " + e);
			e.printStackTrace();
		}
		logger.info("PURCHASE ============COMPLETED FINAL SAVE");
	}

	private void updateCostPrice(PurchaseHdr purchasehdr2) {

		ResponseEntity<List<PurchaseDtl>> purchasetdtlSaved = RestCaller.getPurchaseDtl(purchasehdr2);
		List<PurchaseDtl> purchaseDtlList = purchasetdtlSaved.getBody();
		for (PurchaseDtl purchaseDtl : purchaseDtlList) {
			ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
					.getPriceDefenitionMstByName("COST PRICE");
			PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
			if (null != priceDefenitionMst) {

				String sdate = SystemSetting.UtilDateToString(purchasehdr2.getourVoucherDate(), "yyyy-MM-dd");

				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						purchaseDtl.getItemId(), priceDefenitionMst.getId(), purchaseDtl.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();

				if (null != priceDefinition) {

					if (purchaseDtl.getPurchseRate() != priceDefinition.getAmount()) {
						PriceDefinition price = new PriceDefinition();
						price.setItemId(purchaseDtl.getItemId());
						price.setUnitId(purchaseDtl.getUnitId());
						price.setPriceId(priceDefenitionMst.getId());
						price.setAmount(purchaseDtl.getPurchseRate());
						price.setStartDate(purchasehdr2.getourVoucherDate());
						price.setBranchCode(SystemSetting.systemBranch);

						ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(price);

					}
				} else {
					PriceDefinition price = new PriceDefinition();
					price.setItemId(purchaseDtl.getItemId());
					price.setUnitId(purchaseDtl.getUnitId());
					price.setPriceId(priceDefenitionMst.getId());
					price.setAmount(purchaseDtl.getPurchseRate());
					price.setStartDate(purchasehdr2.getourVoucherDate());
					price.setBranchCode(SystemSetting.systemBranch);

					ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(price);

				}

			}
		}
	}

	@FXML
	void btnAdd(ActionEvent event) {

		if (null != additionalExpense) {
			if (null != additionalExpense.getId()) {
				RestCaller.deleteAdditionalExpenseById(additionalExpense.getId());
				ResponseEntity<List<AdditionalExpense>> additionalExpenseList = RestCaller
						.getallAdditionalExpenseByHdrId(purchasehdr.getId());
				additionalExpenseLists = FXCollections.observableArrayList(additionalExpenseList.getBody());
				fillAdditionalExpense();
			}
		}
		if (null == cmbExpenseHead.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Select Expense Head");
			cmbExpenseHead.requestFocus();
			return;
		}
		if (null == cmbCurrency1.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Select Currency");
			cmbCurrency1.requestFocus();
			return;
		}

		additionalExpense = new AdditionalExpense();
		ResponseEntity<AccountHeads> accountHeadByName = RestCaller
				.getAccountHeadByName(cmbExpenseHead.getSelectionModel().getSelectedItem());

		additionalExpense.setAccountId(accountHeadByName.getBody().getId());
//	additionalExpense.setAmount(Double.parseDouble(txtAdditionalExpenseAmt.getText()));
		ResponseEntity<CurrencyMst> currencyMst = RestCaller
				.getCurrencyMstByName(cmbCurrency1.getSelectionModel().getSelectedItem());
		if (null != currencyMst.getBody()) {
			additionalExpense.setCurrencyId(currencyMst.getBody().getId());
		}
		if (!txtConversionRate.getText().trim().isEmpty()) {
			additionalExpense.setConversionRate(Double.parseDouble(txtConversionRate.getText()));
		}
		if (null != purchasehdr.getCompanyMst().getCurrencyName()) {
			if (!purchasehdr.getCompanyMst().getCurrencyName()
					.equalsIgnoreCase(cmbCurrency1.getSelectionModel().getSelectedItem())) {

				String fromCurrency = cmbCurrency1.getSelectionModel().getSelectedItem().toString();
				String toCurrency = purchasehdr.getCompanyMst().getCurrencyName();
				Double fromAmount = Double.parseDouble(txtAdditionalExpenseAmt.getText());

				Double amount = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency, fromAmount);

				if (null != amount) {
					additionalExpense.setFcAmount(Double.parseDouble(txtAdditionalExpenseAmt.getText()));
					additionalExpense.setAmount(amount);
				} else {
					notifyMessage(3, "Currency Conversion Not set");
					return;
				}

			} else {
				additionalExpense.setAmount(Double.parseDouble(txtAdditionalExpenseAmt.getText()));
				additionalExpense.setFcAmount(0.0);
			}

		} else {
			notifyMessage(3, "Company  CurrencyNot Set");
			return;
		}
		additionalExpense.setCalculatedStatus("NO");
		additionalExpense.setExpenseHead(cmbAccountHeadLbl.getSelectionModel().getSelectedItem());
		additionalExpense.setPurchaseHdr(purchasehdr);
		ResponseEntity<AdditionalExpense> respentity = RestCaller.savePurchaseAdditionalExpenseDtl(additionalExpense);
		additionalExpense = respentity.getBody();
		additionalExpenseLists.add(additionalExpense);
		fillAdditionalExpense();
		cmbExpenseHead.getSelectionModel().clearSelection();
		cmbAccountHeadLbl.getSelectionModel().clearSelection();
		txtAdditionalExpenseAmt.clear();
		cmbCurrency1.getSelectionModel().clearSelection();
		txtConversionRate.clear();
		additionalExpense = null;
	}

	private void fillAdditionalExpense() {

		for (AdditionalExpense additionalExp : additionalExpenseLists) {
			ResponseEntity<AccountHeads> accById = RestCaller.getAccountById(additionalExp.getAccountId());
			additionalExp.setAccountHead(accById.getBody().getAccountName());
			ResponseEntity<CurrencyMst> currencyById = RestCaller.getcurrencyMsyById(additionalExp.getCurrencyId());
			additionalExp.setCurrencyName(currencyById.getBody().getCurrencyName());

		}
		tblAdditionalExpense.setItems(additionalExpenseLists);
		clExpenseHead.setCellValueFactory(cellData -> cellData.getValue().getaccountHeadProperty());
		clAccountHead.setCellValueFactory(cellData -> cellData.getValue().getexpenseHeadProperty());
		clCurrency.setCellValueFactory(cellData -> cellData.getValue().getcurrencyProperty());
		clConversionRate.setCellValueFactory(cellData -> cellData.getValue().getfcAmountProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
		Double totalAmount = RestCaller.getSumOfAdditionalExpenseByPurchaseHdr(purchasehdr.getId());

		txtTotalAmount.setText(Double.toString(totalAmount));
		Double totalfcAmount = RestCaller.getSumOfFCAdditionalExpenseByPurchaseHdr(purchasehdr.getId());
		txtTotalFCAmount.setText(Double.toString(totalfcAmount));
//		Double totalFc = RestCaller.getFCRate(totalAmount, selectedItem)
	}

	@FXML
	void btnDelete(ActionEvent event) {

		if (null == additionalExpense) {
			return;
		}
		if (null == additionalExpense.getId()) {
			return;
		}
		RestCaller.deleteAdditionalExpenseById(additionalExpense.getId());
		notifyMessage(2, "Deleted");
		ResponseEntity<List<AdditionalExpense>> additionalExpenseList = RestCaller
				.getallAdditionalExpenseByHdrId(purchasehdr.getId());
		additionalExpenseLists = FXCollections.observableArrayList(additionalExpenseList.getBody());
		fillAdditionalExpense();
		cmbAccountHeadLbl.getSelectionModel().clearSelection();
		cmbExpenseHead.getSelectionModel().clearSelection();
		txtConversionRate.clear();
		txtAdditionalExpenseAmt.clear();
		cmbCurrency1.getSelectionModel().clearSelection();
		additionalExpense = null;
	}

	@FXML
	void ShowItemPopup(MouseEvent event) {

		if (null == cmbCurrency.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Select Currency");
			cmbCurrency.requestFocus();
			return;
		}
		showPopup();
	}

	@FXML
	void showPopup(MouseEvent event) {

		System.out.println("-------------showPopup-------------");
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/supplierPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(SupplierPopupEvent supplierEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtSupplierGst.setText(supplierEvent.getSupplierGST());
					txtSupplierName.setText(supplierEvent.getSupplierName());
					supplierName = supplierEvent.getSupplierName();
					// purchasehdr.setSupplierId(supplierEvent.getSupplierId());
					supplierID = supplierEvent.getSupplierId();
					/*
					 * for some reason id is null
					 */
					if (null == supplierID) {
						ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(supplierEvent.getSupplierName());
						supplierID = accountHeads.getBody().getId();
					}
				}
			});
			//============commented because of repition of adding content in to currency combo box=================
//			ResponseEntity<CurrencyMst> currencyMstById = RestCaller.getcurrencyMsyById(supplierEvent.getCurrencyId());
//			if (null != currencyMstById.getBody()) {
//				cmbCurrency.getItems().add(currencyMstById.getBody().getCurrencyName());
//			}
		}

	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			cmbUnit.getItems().clear();

			itemId = itemPopupEvent.getItemId();
			txtItemName.setText(itemPopupEvent.getItemName());
			txtBarcode.setText(itemPopupEvent.getBarCode());
			txtCessRate.setText(Double.toString(itemPopupEvent.getCess()));
			txtTaxRate.setText(Double.toString(itemPopupEvent.getTaxRate()));
			txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));
			txtDiscount.setText("0.0");
			if (!cmbCurrency.getSelectionModel().getSelectedItem()
					.equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName())) {
				Double fcTaxRate = RestCaller.getFCRate(itemPopupEvent.getTaxRate(),
						cmbCurrency.getSelectionModel().getSelectedItem());
				if (fcTaxRate > 0) {
					txtTaxRate.setText(fcTaxRate + "");
				}
			} else {
				txtTaxRate.setText(Double.toString(itemPopupEvent.getTaxRate()));
			}
			if (!cmbCurrency.getSelectionModel().getSelectedItem()
					.equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName())) {
				Double fcMrp = RestCaller.getFCRate(itemPopupEvent.getMrp(),
						cmbCurrency.getSelectionModel().getSelectedItem());
				if (fcMrp > 0) {
					txtMRP.setText(fcMrp + "");
				}
			} else {
				txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));
			}
			if (!cmbCurrency.getSelectionModel().getSelectedItem()
					.equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName())) {
				Double fcCess = RestCaller.getFCRate(itemPopupEvent.getCess(),
						cmbCurrency.getSelectionModel().getSelectedItem());
				if (fcCess > 0) {
					txtCessRate.setText(fcCess + "");
				}
			} else {
				txtCessRate.setText(Double.toString(itemPopupEvent.getCess()));
			}
			cmbUnit.getItems().add(itemPopupEvent.getUnitName());
			cmbUnit.setValue(itemPopupEvent.getUnitName());
			ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller.getMultiUnitByItemId(itemPopupEvent.getItemId());

			multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());

			if (!multiUnitList.isEmpty()) {

				for (MultiUnitMst multiUniMst : multiUnitList) {

					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
					cmbUnit.getItems().add(getUnit.getBody().getUnitName());
				}

			} else {
//				cmbUnit.getItems().clear();
//				cmbUnit.getItems().add(itemPopupEvent.getUnitName());
//				cmbUnit.setPromptText(itemPopupEvent.getUnitName());
				cmbUnit.setValue(itemPopupEvent.getUnitName());
			}

			ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
					.getPriceDefenitionMstByName("COST PRICE");
			PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
			if (null != priceDefenitionMst) {

				java.util.Date udate = SystemSetting.localToUtilDate(dpOurVoucherDate.getValue());
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						itemPopupEvent.getItemId(), priceDefenitionMst.getId(), itemPopupEvent.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();

				if (null != priceDefinition) {

					if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {
						if (null != purchasehdr.getCompanyMst().getCurrencyName()) {
							if (!purchasehdr.getCompanyMst().getCurrencyName()
									.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem()))
								;
							{
								Double purchaseRate = RestCaller.getFCRate(priceDefinition.getAmount(),
										cmbCurrency.getSelectionModel().getSelectedItem());
								if (purchaseRate > 0) {
									txtPurchseRate.setText(purchaseRate + "");
								}
							}
						}
					} else
						txtPurchseRate.setText(Double.toString(priceDefinition.getAmount()));

				}

			}
		}
	}

	private void FillTable() {

		tblItemDetails.setItems(purchaseListTable);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clamt.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clMRP.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clPurRate.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxAmtProperty());
		clCess.setCellValueFactory(cellData -> cellData.getValue().getCessAmtProperty());
		clNetCost.setCellValueFactory(cellData -> cellData.getValue().getNetCostProperty());
		clFCAmount.setCellValueFactory(cellData -> cellData.getValue().getfcAmountProperty());
		clFCMrp.setCellValueFactory(cellData -> cellData.getValue().getfcMrpProperty());
		clFCPurchaseRate.setCellValueFactory(cellData -> cellData.getValue().getfcpurchaseRateProperty());
		clFCTaxRate.setCellValueFactory(cellData -> cellData.getValue().getfcTaxAmountProperty());

		tbImportPurchase.setItems(purchaseListTable);
		clPurchaseItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clPurchaseBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
//		clImportPurchaseDtlId.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());

	}

	@FXML
	void calculateAmount(KeyEvent event) {

	}

	@FXML
	void calculatePurchaseRate(KeyEvent event) {

	}

	private void clearFields() {
		txtAmount.setText("");
		txtCessAmt.setText("");
		txtItemName.setText("");
		txtQty.setText("");
		txtBarcode.setText("");
		txtBatch.setText("");
		txtCessRate.setText("");
		txtDiscount.setText("");
		// txtFreeQty.setText("");
		txtPurchseRate.setText("");
		// txtItemSerial.setText("");
		txtTaxRate.setText("");
		// txtPreviousMRP.setText("");
		txtCessAmt.setText("");
		txtMRP.setText("");
		cmbUnit.getSelectionModel().clearSelection();
		txtTaxAmt.setText("");
		dpExpiryDate.setValue(null);
		dpManufacture.setValue(null);
	}

	private void clearFieldshdr() {
		clearFields();
		supplierID = null;
		txtSupplierGst.setText("");
		txtSupplierInvNo.setText("");
		txtSupplierName.setText("");
		txtOurVoucherNo.setText("");
		dpSupplierInvDate.setValue(null);
		txtInvoiceTotal.setText("");
		txtPONum.setText("");
		txtNarration.setText("");
		txtQtyTotal.setText("");
		txtAmtTotal.setText("");
		txtDiscountTotal.setText("");
		txtTotalCess.setText("");
		txtTotal.setText("");
		txtGrandTotal.clear();
		cmbCurrency.getSelectionModel().clearSelection();
	}

	@FXML
	void DeletePartialSavedItem(ActionEvent event) {
		if (null != purchasehdr.getId()) {

			RestCaller.deletePurchaseHdr(purchasehdr.getId());
			ResponseEntity<List<PurchaseHdr>> purchaseHdrSaved = RestCaller.getPartialyPurchaseHdr();
			purchaseHdrListTable = FXCollections.observableArrayList(purchaseHdrSaved.getBody());
			System.out.println("purchaseHdrListTable--" + purchaseHdrListTable);
			fillPartialySaveTable();
			purchasehdr = null;
			txtSupplierGst.clear();
			txtSupplierInvNo.clear();
			txtSupplierName.clear();
			dpSupplierInvDate.setValue(null);

		}

	}

	@Subscribe
	public void popupPolistner(PoNumberEvent poNumberEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {
			cmbUnit.getItems().clear();
			txtGrandTotal.clear();
			txtAmtTotal.clear();
			txtTotalCess.clear();
			txtTotal.clear();
			txtDiscountTotal.clear();
			txtQtyTotal.clear();
			txtInvoiceTotal.clear();

			txtTotalCess.clear();

			txtPONum.setText(poNumberEvent.getPoNumber());
			txtSupplierGst.setText(poNumberEvent.getSupGst());
			txtSupplierName.setText(poNumberEvent.getSupplierName());
			supplierName = poNumberEvent.getSupplierName();
			supplierID = poNumberEvent.getSupId();
			tblItemDetails.getItems().clear();
			purchaseDtlList.clear();
			ResponseEntity<PurchaseOrderHdr> getPurOrder = RestCaller
					.getPurchaseOrderHdrByVoucherNo(txtPONum.getText());
			purchaseOrderHdr = getPurOrder.getBody();
			ResponseEntity<List<PurchaseOrderDtl>> respentity = RestCaller
					.getPurchaseOrderDtlByVoucherNo(txtPONum.getText());

			if (null != respentity.getBody()) {

				int i = 1;
				for (PurchaseOrderDtl purOrderDtl : respentity.getBody()) {

					if (null == purchasehdr) {
						purchasehdr = new PurchaseHdr();

						if (null == dpSupplierInvDate.getValue()) {

							notifyMessage(5, "Please select Suppier Invoice date", false);
							purchasehdr = null;
							return;

						}
						if (null == dpOurVoucherDate.getValue()) {

							notifyMessage(5, "Please select voucher date", false);
							purchasehdr = null;
							return;
						}
						if (txtSupplierInvNo.getText().trim().isEmpty()) {
							notifyMessage(5, "Please enter Suppier Invoice Number", false);
							purchasehdr = null;
							return;
						}
//					purchasehdr.setFinalSavedStatus("N");
						purchasehdr.setSupplierId(supplierID);
						// purchasehdr.setPurchaseType(cmbPurchaseType.getSelectionModel().getSelectedItem());
						Date dateOurVouch = Date.valueOf(dpOurVoucherDate.getValue());
						purchasehdr.setpONum(txtPONum.getText());
						purchasehdr.setNarration(txtNarration.getText());
						purchasehdr.setourVoucherDate(dateOurVouch);
						purchasehdr.setSupplierInvNo(txtSupplierInvNo.getText());
						if (!txtInvoiceTotal.getText().trim().isEmpty()) {
							purchasehdr.setInvoiceTotal(Double.parseDouble(txtInvoiceTotal.getText()));
						}
						purchasehdr.setInvoiceDate(Date.valueOf(dpSupplierInvDate.getValue()));

						purchasehdr.setBranchCode(SystemSetting.getSystemBranch());

						purchasehdr.setVoucherType("PURCHASETYPE");

						logger.info("=====purchasehdr======" + purchasehdr);
						ResponseEntity<PurchaseHdr> respentity1 = RestCaller.savePurchaseHdr(purchasehdr);
						purchasehdr = respentity1.getBody();

					}
					purchaseDtl = new PurchaseDtl();

					purchaseDtl.setItemSerial(i);
					txtItemSerial.setText(++i + "");
					purchaseDtl.setAmount(
							(purOrderDtl.getQty() - purOrderDtl.getReceivedQty()) * purOrderDtl.getPurchseRate());
					purchaseDtl.setBarcode(purOrderDtl.getBarcode());
					purchaseDtl.setBatch(purOrderDtl.getBatch());
					purchaseDtl.setBinNo(purOrderDtl.getBinNo());
					purchaseDtl.setCessAmt(purOrderDtl.getCessAmt());
					purchaseDtl.setItemId(purOrderDtl.getItemId());
					purchaseDtl.setItemSerial(Integer.parseInt(purOrderDtl.getItemSerial()));
					purchaseDtl.setPurchseRate(purOrderDtl.getPurchseRate());
					purchaseDtl.setUnitId(purOrderDtl.getUnitId());
					purchaseDtl.setTaxRate(purOrderDtl.getTaxRate());
					purchaseDtl.setTaxAmt(purOrderDtl.getTaxAmt());
					purchaseDtl.setQty(purOrderDtl.getQty() - purOrderDtl.getReceivedQty());
					purchaseDtl.setPurchaseHdr(purchasehdr);
					purchaseDtl.setMrp(purOrderDtl.getMrp());
					purchaseDtl.setBatch("NOBATCH");

					ResponseEntity<ItemMst> resItem = RestCaller.getitemMst(purOrderDtl.getItemId());
					purchaseDtl.setItemName(resItem.getBody().getItemName());
					ResponseEntity<PurchaseDtl> respentity2 = RestCaller.savePurchaseDtl(purchaseDtl);
					purchaseDtl = respentity2.getBody();
					purchaseDtl.setItemName(resItem.getBody().getItemName());
					purchaseListTable.add(purchaseDtl);
					purchaseDtl = null;
					FillTable();
					txtGrandTotal.clear();
					txtAmtTotal.clear();
					txtTotalCess.clear();
					txtTotal.clear();
					txtDiscountTotal.clear();
					txtQtyTotal.clear();
					txtInvoiceTotal.clear();

					txtTotalCess.clear();
					setTotal();
				}
			}
		}
	}

	@FXML
	void actionLoadPo(ActionEvent event) {

		loadPOpopUp(supplierID);
	}

	private void loadPOpopUp(String supplierID) {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			logger.info("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PONumberPopUp.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			POPopupCtl popupctl = loader.getController();
			popupctl.LoadPoNumberBySearch(supplierID);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTotal() {

		Summary sumList = new Summary();
		sumList = RestCaller.getSummary(purchasehdr.getId());
		Double expenseTotal = 0.0;
		try {
			expenseTotal = Double.parseDouble(txtAdditionalExpenseTotal.getText());
		} catch (Exception e) {
			// TODO: handle exception
		}

		try {

			txtGrandTotal.setText(Double.toString(sumList.getTotalAmount() + sumList.getTotalTax()));
			txtQtyTotal.setText(Double.toString(sumList.getTotalQty()));
			txtDiscountTotal.setText(Double.toString(sumList.getTotalDiscount()));
			txtAmtTotal.setText(Double.toString(sumList.getTotalAmount()));
			logger.info("PURCHASE==== get tax total" + sumList.getTotalTax());
			txtTotal.setText(Double.toString(sumList.getTotalTax()));
			txtTotalCess.setText(Double.toString(sumList.getTotalCessAmt()));
		} catch (Exception e) {
			// TODO: handle exception
		}
//txtTaxSplit.setText(sumList);
	}

	private boolean validationHdr() {
		boolean flag = false;

		if (null == dpSupplierInvDate.getValue()) {

			notifyMessage(5, "Please select Suppier Invoice date", false);
			return flag;
			// Alert alert = new Alert(AlertType.INFORMATION);
			// alert.setTitle("WARNING");
			// alert.setContentText("Please select Suppier Invoice date");
			// alert.show();

		} else if (null == dpOurVoucherDate.getValue()) {

			notifyMessage(5, "Please select voucher date", false);
			return flag;
		} else if (txtSupplierInvNo.getText().trim().isEmpty()) {

			System.out.println("ssssssssssssuuuuuuuuuuuuuuu");
			notifyMessage(5, "Please enter Suppier Invoice Number", false);
			return flag;
		} else if (txtInvoiceTotal.getText().trim().isEmpty()) {
			System.out.println("ssssssssssssuuuuuuuuuuuuuuuffffffffffffff");
			notifyMessage(5, "Please enter Invoice totol", false);
			return flag;

		} else {

			flag = true;

			return flag;

		}

	}

	public boolean validationDtl() {

		boolean flag = false;
		if (txtCessRate.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter Cess Rate", false);
			return false;

		} else if (txtMRP.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter MRP", false);
			return false;
		} else if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Quantity", false);
			return false;
		} else if (txtTaxRate.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter TaxRate", false);
			return false;

		} else if (txtPurchseRate.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter purchase Rate", false);
			return false;

		} else {

			flag = true;

		}

		return flag;
	}

	private void fillPartialySaveTable() {

		tblPartialySavedList.setItems(purchaseHdrListTable);
		for (PurchaseHdr pur : purchaseHdrListTable) {
			if (null != pur.getSupplierId()) {
				System.out.println("==pur.getSupplierId()==" + pur.getSupplierId());
				ResponseEntity<AccountHeads> accountHeadsResponce = RestCaller.getAccountHeadsById(pur.getSupplierId());
				AccountHeads accountHeads = accountHeadsResponce.getBody();
				// System.out.println("Suuuuuu" + supplier.getAddress());
				if (null != accountHeads) {
					pur.setSupplierAddress(accountHeads.getPartyAddress1());
					pur.setSupplierName(accountHeads.getAccountName());
				}
				clSupplierName.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());
				clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
			}
			clInvoiceTotoal.setCellValueFactory(cellData -> cellData.getValue().getInvoiceTotalProperty());
			clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
			clInvoiceDate.setCellValueFactory(cellData -> cellData.getValue().getInvoiceDateProperty());
			clSupplierInvoiceNo.setCellValueFactory(cellData -> cellData.getValue().getSupplierInvNoProperty());
		}

	}

	@FXML
	void OnSelect(ActionEvent event) {
	

		clearFieldshdr();
		tblItemDetails.getItems().clear();
		tblAdditionalExpense.getItems().clear();
		additionalExpenseLists.clear();
		tbImportPurchase.getItems().clear();
		tbImportExpense.getItems().clear();

		System.out.println("==purchasehdr==" + purchasehdr);
		if (null != purchasehdr.getId()) {
			if (null != purchasehdr.getSupplierId()) {
				System.out.println("==pur.getSupplierId()==" + purchasehdr.getSupplierId());
				ResponseEntity<AccountHeads> accountHeadsResponce = RestCaller.getAccountHeadsById(purchasehdr.getSupplierId());
				AccountHeads accountHeads = accountHeadsResponce.getBody();
				txtSupplierName.setText(accountHeads.getAccountName());
				txtSupplierGst.setText(accountHeads.getPartyGst());

			}
			if (null != purchasehdr.getSupplierInvNo()) {
				txtSupplierInvNo.setText(purchasehdr.getSupplierInvNo());
			}
			if (null != purchasehdr.getInvoiceDate()) {
				dpSupplierInvDate.setValue(purchasehdr.getInvoiceDate().toLocalDate());
			}
			if (null != purchasehdr.getourVoucherDate()) {
				dpOurVoucherDate.setValue(purchasehdr.getourVoucherDate().toLocalDate());
			}
			if (null != purchasehdr.getNarration()) {
				txtNarration.setText(purchasehdr.getNarration());
			}
			if (null != purchasehdr.getInvoiceTotal()) {
				txtInvoiceTotal.setText(Double.toString(purchasehdr.getInvoiceTotal()));
			}
			if (null != purchasehdr.getpONum()) {
				txtPONum.setText(purchasehdr.getpONum());
			}

			getPurchaseDtls();
			
			ResponseEntity<List<AdditionalExpense>> additionalExpenseList = RestCaller
					.getallAdditionalExpenseByHdrId(purchasehdr.getId());
			if (additionalExpenseList.getBody().size() > 0) {
				additionalExpenseLists = FXCollections
						.observableArrayList(additionalExpenseList.getBody());
				fillAdditionalExpense();
			}
		}

	}

	@FXML
	void SelectPartialySaved(MouseEvent event) {

		tblPartialySavedList.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (null != newSelection) {
						System.out.println("getSelectionModel");
						System.out.println("getSelectionModel" + newSelection);
						if (null != newSelection.getId()) {

							// purchasehdr = new PurchaseHdr();
							purchasehdr.setId(newSelection.getId());
							purchasehdr.setSupplierId(newSelection.getSupplierId());
							purchasehdr.setNarration(newSelection.getNarration());
							purchasehdr.setInvoiceDate(newSelection.getInvoiceDate());
							purchasehdr.setourVoucherDate(newSelection.getourVoucherDate());
							purchasehdr.setSupplierInvNo(newSelection.getSupplierInvNo());
							purchasehdr.setInvoiceTotal(newSelection.getInvoiceTotal());
							purchasehdr.setpONum(newSelection.getpONum());
							purchasehdr.setPurchaseType(newSelection.getPurchaseType());
							purchasehdr.setBranchCode(newSelection.getBranchCode());
							purchasehdr.setCompanyMst(newSelection.getCompanyMst());

							// txtSupplierGst.setText(newSelection.getsu);
							txtSupplierName.setText(newSelection.getSupplierName());
							supplierName = newSelection.getSupplierName();
							// purchasehdr.setSupplierId(supplierEvent.getSupplierId());
							supplierID = newSelection.getSupplierId();

							dpOurVoucherDate.setValue(newSelection.getourVoucherDate().toLocalDate());
							txtPONum.setText(newSelection.getpONum());
							txtNarration.setText(newSelection.getNarration());
							txtSupplierInvNo.setText(newSelection.getSupplierInvNo());
							txtInvoiceTotal.setText(newSelection.getInvoiceTotal() + "");
							dpSupplierInvDate.setValue(newSelection.getInvoiceDate().toLocalDate());

							getPurchaseDtls();
							FillTable();

							System.out.println("==purchasehdr==" + purchasehdr);
						}
					}
				});

	}

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void showPopup() {
		try {
			logger.info("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addItem() {

		if (null == cmbCurrency.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Please Select Currency");
			cmbCurrency.requestFocus();
			return;
		}

		ResponseEntity<CurrencyMst> currencyResp = RestCaller
				.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem().toString());
		CurrencyMst currencyMst = currencyResp.getBody();

		if (null == currencyMst) {
			notifyMessage(2, "Invalid currency type", false);
			return;
		}

		if (!currencyMst.getCurrencyName()
				.equalsIgnoreCase(SystemSetting.getUser().getCompanyMst().getCurrencyName())) {
			ResponseEntity<CurrencyConversionMst> currencyConversionMstResp = RestCaller
					.getCurrencyConversionMstByCurrencyId(currencyMst.getId());
			CurrencyConversionMst currencyConversionMst = currencyConversionMstResp.getBody();

			if (null == currencyConversionMst) {
				notifyMessage(2, "currency conversion rate is not set", false);
				return;
			}
		}

		btnSave.setDisable(true);
		if (null == purchasehdr) {

			if (null == supplierID)

			{
				notifyMessage(5, "Please select Supplier", false);
				purchasehdr = null;
				return;
			}

			if (null == dpSupplierInvDate.getValue()) {

				notifyMessage(5, "Please select Suppier Invoice date", false);
				purchasehdr = null;
				return;

			}
			if (null == dpOurVoucherDate.getValue()) {

				notifyMessage(5, "Please select voucher date", false);
				purchasehdr = null;
				return;
			}
			if (txtSupplierInvNo.getText().trim().isEmpty()) {
				notifyMessage(5, "Please enter Suppier Invoice Number", false);
				purchasehdr = null;
				return;
			}

			purchasehdr = new PurchaseHdr();
			purchasehdr.setSupplierId(supplierID);
			purchasehdr.setPurchaseType("IMPORT PURCHASE");
			System.out.println("ddddd" + supplierID);

			purchasehdr.setFinalSavedStatus("N");
			Date dateOurVouch = Date.valueOf(dpOurVoucherDate.getValue());
			purchasehdr.setpONum(txtPONum.getText());
			purchasehdr.setNarration(txtNarration.getText());
			purchasehdr.setourVoucherDate(dateOurVouch);
			purchasehdr.setSupplierInvNo(txtSupplierInvNo.getText());
			if (!txtInvoiceTotal.getText().trim().isEmpty()) {
				purchasehdr.setInvoiceTotal(Double.parseDouble(txtInvoiceTotal.getText()));
			}
			purchasehdr.setInvoiceDate(Date.valueOf(dpSupplierInvDate.getValue()));

			purchasehdr.setBranchCode(SystemSetting.getSystemBranch());

			purchasehdr.setVoucherType("PURCHASETYPE");

			
			
			purchasehdr.setPurchaseType("Import Purchase");
			

			purchasehdr.setCurrency(cmbCurrency.getSelectionModel().getSelectedItem().toString());

			logger.info("=====purchasehdr======" + purchasehdr);
			ResponseEntity<PurchaseHdr> respentity = RestCaller.savePurchaseHdr(purchasehdr);
			purchasehdr = respentity.getBody();

			purchasehdrpreviewid = purchasehdr.getId();
			purchasehdrpreview = purchasehdr;

		}

		/*
		 * Set PurchaseHdr to PurchaseDtl.
		 */
		if (null != purchaseDtl) {
			if (null != purchaseDtl.getId()) {

				// version1.7
				ResponseEntity<PurchaseDtl> getPur = RestCaller.getPurchaseDtlById(purchaseDtl.getId());
				if (null != getPur.getBody().getPurchaseOrderDtl()) {
					if (!getPur.getBody().getPurchaseOrderDtl().equalsIgnoreCase(null))

					{
						PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
						ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller
								.getPurchaseOrderDtlById(getPur.getBody().getPurchaseOrderDtl());
						puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
						puchaseOrderDtl.setReceivedQty(
								Double.parseDouble(txtQty.getText()) + puchaseOrderDtl.getReceivedQty());
						if (puchaseOrderDtl.getQty() > puchaseOrderDtl.getReceivedQty()) {
							puchaseOrderDtl.setStatus("OPEN");
						} else {
							puchaseOrderDtl.setStatus("CLOSED");
						}
						RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
						ResponseEntity<PurchaseOrderHdr> getPurOrdrHdr = RestCaller
								.getPurchaseOrderByDtlId(puchaseOrderDtl.getId());
						String vdate = SystemSetting.UtilDateToString(getPurOrdrHdr.getBody().getVoucherDate(),
								"yyyy-MM-dd");
						ResponseEntity<List<PurchaseOrderDtl>> getPurchaseDtl = RestCaller
								.getPurchaseOrderDtlByVoucherNoAndDate(getPurOrdrHdr.getBody().getVoucherNumber(),
										vdate);
						getPurOrdrHdr.getBody().setFinalSavedStatus("OPEN");
						RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());
					}
				}
				RestCaller.purchaseDtlDelete(purchaseDtl.getId());
				purchaseDtl = null;
				getPurchaseDtls();
			}
		}

		if (null != purchasehdr) {

			purchaseDtl = new PurchaseDtl();

			if (txtItemName.getText().trim().isEmpty()) {
				notifyMessage(5, "Please select Item...!!!", false);
				return;
			}
			String ItemName = txtItemName.getText();
			ResponseEntity<ItemMst> resp = RestCaller.getItemByNameRequestParam(ItemName);
			ItemMst item = resp.getBody();

			if (null == item) {
				notifyMessage(5, "Please select Item...!!!", false);
				return;

			}
			System.out.println("-------------AddItem-------------");
			boolean valDtl = validationDtl();
			if (valDtl == false) {
				return;
			}

			boolean expiry = true;

			if (null != txtBatch) {
				String batch = txtBatch.getText() == null ? "NOBATCH" : txtBatch.getText();

				if (batch.trim().length() > 0) {
					purchaseDtl.setBatch(batch);
					expiry = false;
					purchaseDtl.setBatch(txtBatch.getText());
					if (null != dpExpiryDate.getValue()) {
						purchaseDtl.setexpiryDate(Date.valueOf(dpExpiryDate.getValue()));
						expiry = true;

					} else {
						notifyMessage(5, "Please select Expirydate...!!!");
						return;
					}

					if (null != dpManufacture.getValue()) {
						Date dateMfg = Date.valueOf(dpManufacture.getValue());

						purchaseDtl.setmanufactureDate(dateMfg);
					}
				} else {
					purchaseDtl.setBatch("NOBATCH");
					purchaseDtl.setexpiryDate(null);
				}
			}

			if (null != purchasehdr.getId()) {
				if (valDtl == true) {

					if (expiry == true) {

						purchaseDtl.setPurchaseHdr(purchasehdr);
						System.out.println("getItemIdgetItemId" + purchaseDtl.getItemId());

						purchaseDtl.setItemName(item.getItemName());
						purchaseDtl.setBarcode(item.getBarCode());
						if (!purchasehdr.getCompanyMst().getCurrencyName()
								.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {

							purchaseDtl.setFcTaxRate(item.getTaxRate());
							String fromCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
							String toCurrency = purchasehdr.getCompanyMst().getCurrencyName();

							Double taxRate = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
									item.getTaxRate());

							purchaseDtl.setTaxRate(taxRate);
						} else {
							purchaseDtl.setTaxRate(item.getTaxRate());
						}
						purchaseDtl.setDiscount(0.0);
						purchaseDtl.setItemId(item.getId());
						purchaseDtl.setTaxAmt(0.0);

						if (!txtPurchseRate.getText().trim().isEmpty()) {

							purchaseDtl.setNetCost(Double.parseDouble(txtPurchseRate.getText()));

						}
						if (!txtDiscount.getText().trim().isEmpty()) {
							if (!purchasehdr.getCompanyMst().getCurrencyName()
									.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
								purchaseDtl.setFcDiscount(Double.parseDouble(txtDiscount.getText()));

								String fromCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
								String toCurrency = purchasehdr.getCompanyMst().getCurrencyName();

								Double discount = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
										Double.parseDouble(txtDiscount.getText()));

								purchaseDtl.setDiscount(discount);
								purchaseDtl.setFcDiscount(0.0);
							}

							else {
								purchaseDtl.setDiscount(Double.parseDouble(txtDiscount.getText()));
							}
						}
						purchaseDtl.setCessAmt(0.0);

						if (!purchasehdr.getCompanyMst().getCurrencyName()
								.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
							purchaseDtl.setFcCessRate(Double.parseDouble(txtCessRate.getText()));

							String fromCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
							String toCurrency = purchasehdr.getCompanyMst().getCurrencyName();

							Double cessRate = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
									Double.parseDouble(txtCessRate.getText()));

							purchaseDtl.setCessRate(cessRate);
						} else {
							purchaseDtl.setFcCessRate(0.0);
							purchaseDtl.setCessRate(Double.parseDouble(txtCessRate.getText()));
						}
						if (!purchasehdr.getCompanyMst().getCurrencyName()
								.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
							purchaseDtl.setFcMrp(Double.parseDouble(txtMRP.getText()));

							String fromCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
							String toCurrency = purchasehdr.getCompanyMst().getCurrencyName();

							Double mrp = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
									Double.parseDouble(txtMRP.getText()));

							purchaseDtl.setMrp(mrp);
						} else {
							purchaseDtl.setFcMrp(0.0);
							purchaseDtl.setMrp(Double.parseDouble(txtMRP.getText()));
						}
						purchaseDtl.setQty(Double.parseDouble(txtQty.getText()));
						if (!purchasehdr.getCompanyMst().getCurrencyName()
								.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {
							purchaseDtl.setFcTaxRate(Double.parseDouble(txtTaxRate.getText()));

							String fromCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
							String toCurrency = purchasehdr.getCompanyMst().getCurrencyName();

							Double fcTaxrate = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
									Double.parseDouble(txtTaxRate.getText()));

							purchaseDtl.setTaxRate(fcTaxrate);
						} else {
							purchaseDtl.setFcTaxRate(0.0);
							purchaseDtl.setTaxRate(Double.parseDouble(txtTaxRate.getText()));
						}
						if (!purchasehdr.getCompanyMst().getCurrencyName()
								.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {

							purchaseDtl.setFcPurchaseRate(Double.parseDouble(txtPurchseRate.getText()));

							String fromCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
							String toCurrency = purchasehdr.getCompanyMst().getCurrencyName();

							Double purchaserate = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
									Double.parseDouble(txtPurchseRate.getText()));

							purchaseDtl.setPurchseRate(purchaserate);
						} else {
							purchaseDtl.setFcPurchaseRate(0.0);
							purchaseDtl.setPurchseRate(Double.parseDouble(txtPurchseRate.getText()));
						}
						if (!purchasehdr.getCompanyMst().getCurrencyName()
								.equalsIgnoreCase(cmbCurrency.getSelectionModel().getSelectedItem())) {

							purchaseDtl.setFcAmount(Double.parseDouble(txtAmount.getText()));

							String fromCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
							String toCurrency = purchasehdr.getCompanyMst().getCurrencyName();

							Double amount = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
									Double.parseDouble(txtAmount.getText()));

							purchaseDtl.setAmount(amount);
						} else {
							purchaseDtl.setFcAmount(0.0);
							purchaseDtl.setAmount(Double.parseDouble(txtAmount.getText()));
						}
						if (null == cmbUnit.getValue()) {

							ResponseEntity<UnitMst> unitId = RestCaller
									.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

							purchaseDtl.setUnitId(unitId.getBody().getId());
						} else {
							ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
							purchaseDtl.setUnitId(getUnit.getBody().getId());
						}

						ResponseEntity<PurchaseDtl> respentity = RestCaller.savePurchaseDtl(purchaseDtl);
						purchaseDtl = respentity.getBody();
						setTotal();
						purchaseListTable.add(purchaseDtl);
						itemSerialProperty.set(Integer.parseInt(itemSerialProperty.get()) + 1 + "");
						FillTable();
						clearFields();

						actionCalculate();

					}
				}
			} else {
//			purchasehdr = null;

				return;

			}
		} else {
			return;
		}
		logger.info("ADD ITEM  COMPLETED=================");

		purchaseDtl = new PurchaseDtl();

		txtItemName.requestFocus();

	}

	private PurchaseDtl saveFCFields(PurchaseDtl purchaseDtl2) {

		if (!cmbCurrency.getSelectionModel().getSelectedItem()
				.equalsIgnoreCase(purchasehdr.getCompanyMst().getCurrencyName())) {

			ResponseEntity<CurrencyMst> currencyMst = RestCaller
					.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem());
			ResponseEntity<CurrencyConversionMst> currencyConMst = RestCaller
					.getCurrencyConversionMstByCurrencyId(currencyMst.getBody().getId());

			String toCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
			String fromCurrency = purchasehdr.getCompanyMst().getCurrencyName();

			if (null != currencyConMst.getBody()) {

				Double fcAmount = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
						purchaseDtl2.getAmount());

				if (fcAmount > 0) {
					purchaseDtl2.setFcAmount(fcAmount);
				}
				if (null != purchaseDtl2.getDiscount()) {
					if (purchaseDtl2.getDiscount() > 0) {

						Double fcDiscount = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
								purchaseDtl2.getDiscount());

						if (fcDiscount > 0) {
							purchaseDtl2.setFcDiscount(fcDiscount);
						}
					}
				}

				Double fcNetCost = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
						purchaseDtl2.getNetCost());

				if (fcNetCost > 0) {
					purchaseDtl2.setFcNetCost(fcNetCost);
				}

				Double fcTaxAmt = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
						purchaseDtl2.getTaxAmt());

				if (fcTaxAmt > 0) {
					purchaseDtl2.setFcTaxAmt(fcTaxAmt);
				}

				Double fcTaxrate = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
						purchaseDtl2.getTaxRate());

				if (fcTaxrate > 0) {
					purchaseDtl2.setFcTaxRate(fcTaxrate);
				}
				Double fcPurchaseRate = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
						purchaseDtl2.getPurchseRate());

				if (fcPurchaseRate > 0) {
					purchaseDtl2.setFcPurchaseRate(fcPurchaseRate);
				}
				// RestCaller.updatePurchaseDtlFcFields(purchaseDtl2);

			} else {
				notifyMessage(3, "Currency Conversion Not Set");
			}
		}
		return purchaseDtl2;
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	void actionImportExpenseAdd(ActionEvent event) {

		ResponseEntity<PurchaseAdditionalExpenseHdr> purchaseAdditionalExpense = RestCaller
				.getPurchaseAdditionalExpenseHdrByPurchaseDtl(purchaseAdditionalExpenseHdr.getPurchaseDtl().getId());

		if (null == purchaseAdditionalExpense.getBody()) {
			ResponseEntity<ItemMst> getitem = RestCaller.getItemByNameRequestParam(txtImportExpenseItemName.getText());
			purchaseAdditionalExpenseHdr.setItemId(getitem.getBody().getId());
			ResponseEntity<PurchaseAdditionalExpenseHdr> respentity = RestCaller
					.savePurchaseAdditionalExpenseHdr(purchaseAdditionalExpenseHdr);
			purchaseAdditionalExpenseHdr = respentity.getBody();

		} else {

			purchaseAdditionalExpenseHdr = purchaseAdditionalExpense.getBody();
		}

		PurchaseAdditionalExpenseDtl purchaseAdditionalExpenseDtl = new PurchaseAdditionalExpenseDtl();
		purchaseAdditionalExpenseDtl.setAmount(Double.parseDouble(txtImportExpenseAmount.getText()));
		purchaseAdditionalExpenseDtl.setExpense(cmbImportExpense.getSelectionModel().getSelectedItem());
		purchaseAdditionalExpenseDtl.setPurchaseAdditionalExpenseHdr(purchaseAdditionalExpenseHdr);
		purchaseAdditionalExpenseDtl.setCalculated("NO");
		ResponseEntity<PurchaseAdditionalExpenseDtl> respentity = RestCaller
				.savePurchaseAdditionalExpenseDtl(purchaseAdditionalExpenseDtl);
		purchaseAdditionalExpenseDtl = respentity.getBody();
		purchaseAdditionalExpenseList.add(purchaseAdditionalExpenseDtl);
		fillImportExpense();
		txtImportExpenseItemName.clear();
		txtImportExpenseBatch.clear();
		txtImportExpenseAmount.clear();

	}

	private void fillImportExpense() {
		for (PurchaseAdditionalExpenseDtl pur : purchaseAdditionalExpenseList) {
			pur.setItemName(pur.getPurchaseAdditionalExpenseHdr().getPurchaseDtl().getItemName());
			pur.setBatch(pur.getPurchaseAdditionalExpenseHdr().getPurchaseDtl().getBatch());
		}
		tbImportExpense.setItems(purchaseAdditionalExpenseList);

		clImportExpense.setCellValueFactory(cellData -> cellData.getValue().getexpenseProperty());
		clImportExpenseAmount.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
		clImportExpenseBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clImportExpenseItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());

	}

	@FXML
	void actionImportExpenseDelete(ActionEvent event) {

	}

	@FXML
	void actionImportExpenseSave(ActionEvent event) {

		/*
		 * Calculate netcost
		 */
		ResponseEntity<List<PurchaseDtl>> purchaseDtlList = RestCaller.getPurchaseDtl(purchasehdr);
		for (int i = 0; i < purchaseDtlList.getBody().size(); i++) {
			PurchaseDtl purchase = purchaseDtlList.getBody().get(i);
			Double netCost = 0.0;
			ResponseEntity<PurchaseAdditionalExpenseHdr> purchaseAdditionalExpenseHdr = RestCaller
					.getPurchaseAdditionalExpenseHdrByPurchaseDtl(purchaseDtlList.getBody().get(i).getId());
			if (null != purchaseAdditionalExpenseHdr.getBody()) {
				ResponseEntity<List<PurchaseAdditionalExpenseDtl>> purchaseAddExpDtl = RestCaller
						.getpurchaseAdditionalExpenseDtlByHdrIdByCalculated(
								purchaseAdditionalExpenseHdr.getBody().getId());
				if (purchaseAddExpDtl.getBody().size() > 0) {
					for (int j = 0; j < purchaseAddExpDtl.getBody().size(); j++) {
						netCost = netCost + purchaseAddExpDtl.getBody().get(j).getAmount();
						PurchaseAdditionalExpenseDtl purchaseAddExpDtl1 = purchaseAddExpDtl.getBody().get(j);
						purchaseAddExpDtl1.setCalculated("YES");
						RestCaller.updatePurchaseAdditionalExpenseDtl(purchaseAddExpDtl1);
					}

				}
			}

			purchase.setNetCost(netCost + purchase.getNetCost());
			if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {

				String toCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
				String fromCurrency = purchasehdr.getCompanyMst().getCurrencyName();

				Double fcNetCost = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
						purchase.getNetCost());

				purchase.setFcNetCost(fcNetCost);
			}
			RestCaller.updatepurchaseNetCost(purchase);
		}
		ResponseEntity<List<PurchaseDtl>> purchaseDtlList1rep = RestCaller.getPurchaseDtl(purchasehdr);

		purchaseListTable = FXCollections.observableArrayList(purchaseDtlList1rep.getBody());
		FillTable();

	}

	@FXML
	void actionCalculate(ActionEvent event) {

		actionCalculate();
		btnSave.setDisable(false);

	}

	private void actionCalculate() {

		Double additionalExpenseTotal = RestCaller.getSumOfAdditionalExpenseByExpenseTye(purchasehdr.getId(),
				"INCLUDED IN BILL");

		if (cmbCurrency.getSelectionModel().getSelectedItem()
				.equalsIgnoreCase(purchasehdr.getCompanyMst().getCurrencyName())) {

			txtAdditionalExpenseTotal.setText(Double.toString(additionalExpenseTotal));

		} else {

			additionalExpenseTotal = RestCaller.getFCRate(additionalExpenseTotal,
					cmbCurrency.getSelectionModel().getSelectedItem());
			txtAdditionalExpenseTotal.setText(additionalExpenseTotal.toString());

		}

		Double grandTotal = Double.parseDouble(txtAmtTotal.getText()) + Double.parseDouble(txtTotal.getText());
		Double grandTotalPlusExpenseTotal = additionalExpenseTotal + grandTotal;
		txtGrandTotal.setText(Double.toString(grandTotalPlusExpenseTotal));

		if (IMPORT_PURCHASE_COSTING_METHOD.equalsIgnoreCase("QTY")) {

			Double qtyTotal = Double.parseDouble(txtQtyTotal.getText());

			Double totalAmount = RestCaller.getSumOfAdditionalExpenseByPurchaseHdr(purchasehdr.getId());
			Double netApproxcost = totalAmount / qtyTotal;
			BigDecimal bgNetApproxCost = new BigDecimal(netApproxcost);
			bgNetApproxCost = bgNetApproxCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			netApproxcost = bgNetApproxCost.doubleValue();
			ResponseEntity<List<PurchaseDtl>> pudtl = RestCaller.getPurchaseDtl(purchasehdr);
			for (int i = 0; i < pudtl.getBody().size(); i++) {
				PurchaseDtl purchase = pudtl.getBody().get(i);
				Double purchaseNetcost = purchase.getPurchseRate() + netApproxcost;
				BigDecimal bdpurchaseNetCost = new BigDecimal(purchaseNetcost);
				bdpurchaseNetCost = bdpurchaseNetCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				purchase.setNetCost(bdpurchaseNetCost.doubleValue());
				if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {
					if (!cmbCurrency.getSelectionModel().getSelectedItem()
							.equalsIgnoreCase(purchasehdr.getCompanyMst().getCurrencyName())) {

						String toCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
						String fromCurrency = purchasehdr.getCompanyMst().getCurrencyName();

						Double fcNetCost = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
								netApproxcost);

						if (fcNetCost > 0) {

							BigDecimal bgfcNetCost = new BigDecimal(fcNetCost);
							bgfcNetCost = bgfcNetCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
							fcNetCost = bgfcNetCost.doubleValue();

							purchase.setFcNetCost(purchase.getFcPurchaseRate() + fcNetCost);
						}
					}
				}
				RestCaller.updatepurchaseNetCost(purchase);

			}

		}
		if (IMPORT_PURCHASE_COSTING_METHOD.equalsIgnoreCase("AMOUNT")) {

			Double totalAdditionalExpAmount = RestCaller.getSumOfAdditionalExpenseByPurchaseHdr(purchasehdr.getId());

			Summary sumList = new Summary();
			sumList = RestCaller.getSummary(purchasehdr.getId());
			Double totalAmount = sumList.getTotalAmount();
			ResponseEntity<List<PurchaseDtl>> pudtl = RestCaller.getPurchaseDtl(purchasehdr);
			for (int i = 0; i < pudtl.getBody().size(); i++) {
				PurchaseDtl purchase = pudtl.getBody().get(i);
				Double netCost = 0.0;
				if (totalAmount > 0) {
					netCost = ((purchase.getAmount() / totalAmount) * totalAdditionalExpAmount) / purchase.getQty();

//	    			else {
//	    				return;
//	    			}
					BigDecimal bgnetCost = new BigDecimal(netCost);
					bgnetCost = bgnetCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					netCost = bgnetCost.doubleValue();
					Double purchaseNetcost = purchase.getPurchseRate() + netCost;
					BigDecimal bdpurchaseNetCost = new BigDecimal(purchaseNetcost);
					bdpurchaseNetCost = bdpurchaseNetCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					purchase.setNetCost(bdpurchaseNetCost.doubleValue());
				}
				if (null != cmbCurrency.getSelectionModel().getSelectedItem()) {
					if (!cmbCurrency.getSelectionModel().getSelectedItem()
							.equalsIgnoreCase(purchasehdr.getCompanyMst().getCurrencyName())) {

						String toCurrency = cmbCurrency.getSelectionModel().getSelectedItem().toString();
						String fromCurrency = purchasehdr.getCompanyMst().getCurrencyName();

						Double fcNetCost = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency, netCost);

						if (fcNetCost > 0) {

							BigDecimal bgfcNetCost = new BigDecimal(fcNetCost);
							bgfcNetCost = bgfcNetCost.setScale(2, BigDecimal.ROUND_HALF_EVEN);
							fcNetCost = bgfcNetCost.doubleValue();

							purchase.setFcNetCost(purchase.getFcPurchaseRate() + fcNetCost);
						}
					}
				}
				RestCaller.updatepurchaseNetCost(purchase);
			}
		}
//	    	RestCaller.updateAdditionalExpenseStatusWithPurchaseHdrId(purchasehdr.getId());
		ResponseEntity<List<PurchaseDtl>> pudtl1 = RestCaller.getPurchaseDtl(purchasehdr);
		purchaseListTable = FXCollections.observableArrayList(pudtl1.getBody());
		FillTable();

	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

	@FXML
	void actionImportExpenseRefresh(ActionEvent event) {

	}

}
