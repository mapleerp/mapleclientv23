package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.SalesPropertiesConfigMst;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class SalesPropertiesConfigurationCtl {
	
	String taskid;
	String processInstanceId;

	
	SalesPropertiesConfigMst salesPropertiesConfigMst = null;
	private ObservableList<SalesPropertiesConfigMst> reportList = FXCollections.observableArrayList();

    @FXML
    private TextField txtSalesProperty;

    @FXML
    private ComboBox<String> cmbPropertyType;
    @FXML
    private TableView<SalesPropertiesConfigMst> tbProperty;

    @FXML
    private TableColumn<SalesPropertiesConfigMst, String> clProperty;
    

    @FXML
    private TableColumn<SalesPropertiesConfigMst, String> clType;
    @FXML
    private Button btnSave;

    @FXML
    private Button btnShowAll;
    @FXML
    private Button btnDelete;
    @FXML
	private void initialize() {
    	cmbPropertyType.getItems().add("String");
    	cmbPropertyType.getItems().add("Double");
    	cmbPropertyType.getItems().add("Date");
		tbProperty.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					salesPropertiesConfigMst = new SalesPropertiesConfigMst();
					salesPropertiesConfigMst.setId(newSelection.getId());
				}
			}
		});
    }
    @FXML
    void actionSave(ActionEvent event) {

    	if(null == cmbPropertyType.getSelectionModel().getSelectedItem())
    	{
    		notifyMessage(3,"Select Property Type");
    		cmbPropertyType.requestFocus();
    		return;
    		
    	}
    	salesPropertiesConfigMst = new SalesPropertiesConfigMst();
    	salesPropertiesConfigMst.setPropertyName(txtSalesProperty.getText());
    	salesPropertiesConfigMst.setPropertyType(cmbPropertyType.getSelectionModel().getSelectedItem());
    	ResponseEntity<SalesPropertiesConfigMst> respentity = RestCaller.saveSalePropertiesConfigMst(salesPropertiesConfigMst);
    	salesPropertiesConfigMst = respentity.getBody();
    	reportList.add(salesPropertiesConfigMst);
    	fillTable();
    	txtSalesProperty.clear();
    	salesPropertiesConfigMst = null;
    }
    private void fillTable()
    {
    	tbProperty.setItems(reportList);
		clProperty.setCellValueFactory(cellData -> cellData.getValue().getpropertyNameproperty());
		clType.setCellValueFactory(cellData -> cellData.getValue().getpropertyTypeproperty());

    }
    @FXML
    void actionShowAll(ActionEvent event) {

    	ResponseEntity<List<SalesPropertiesConfigMst>> salesPropertyList = RestCaller.getAllSalesPropertyConfigMst();
    	reportList = FXCollections.observableArrayList(salesPropertyList.getBody());
    	fillTable();
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null== salesPropertiesConfigMst)
    	{
    		return;
    	}
    	if(null == salesPropertiesConfigMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteSalePropertyConfiguration(salesPropertiesConfigMst.getId());
    	ResponseEntity<List<SalesPropertiesConfigMst>> salesPropertyList = RestCaller.getAllSalesPropertyConfigMst();
    	reportList = FXCollections.observableArrayList(salesPropertyList.getBody());
    	fillTable();
    	txtSalesProperty.clear();
    	salesPropertiesConfigMst = null;

    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


private void PageReload(String hdrId) {
	
}

}
