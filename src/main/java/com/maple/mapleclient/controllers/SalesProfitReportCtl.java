package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.maple.util.ExportSalesProfitToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;


public class SalesProfitReportCtl {
	
	String taskid;
	String processInstanceId;

	
	ExportSalesProfitToExcel exportSalesProfitToExcel = new ExportSalesProfitToExcel();

	private ObservableList<SalesProfitReportHdr> salesProfitReportList = FXCollections.observableArrayList();
    @FXML
    private DatePicker dpdate;

    @FXML
    private TableView<SalesProfitReportHdr> tbReport;

    @FXML
    private TableColumn<SalesProfitReportHdr, String> clVoucherNo;

    @FXML
    private TableColumn<SalesProfitReportHdr, String> clDate;

    @FXML
    private TableColumn<SalesProfitReportHdr, String> clCustomer;

    @FXML
    private TableColumn<SalesProfitReportHdr, String> clBatch;
    
    @FXML
    private TableColumn<SalesProfitReportHdr, String> clreceiptmode;

    @FXML
    private TableColumn<SalesProfitReportHdr, Number> clamt;
    
    @FXML
    private TableColumn<SalesProfitReportHdr, Number>clProfitAmount;

	double totalamount = 0.0;
    @FXML
    private TextField txtGrandTotal;

    @FXML
    private Button btnShow;

    @FXML
    private ComboBox<String> branchselect;

    @FXML
    private Button ok;

    @FXML
    private Button btnPrint;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnPrintReport;

    @FXML
    private Button btnExportToExcel;

    @FXML
    private Button btnPrintSalesDtl;

    @FXML
    void PrintReport(ActionEvent event) {

    }

    @FXML
    void actionExcelExport(ActionEvent event) {
    	 java.util.Date uDate = Date.valueOf(dpdate.getValue());
			String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
			java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
			String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
			ArrayList excelExport = new ArrayList();
			if( branchselect.getSelectionModel().isEmpty())
			{
				return;
			}
			//ResponseEntity<List<SalesProfitReportHdr>> salesProfitReport = RestCaller.getSalesProfit(branchselect.getSelectionModel().getSelectedItem(), strDate,strDate1);
			excelExport = RestCaller.salesProfitExportToExcel(branchselect.getSelectionModel().getSelectedItem(),strDate, strDate1);	
			exportSalesProfitToExcel.exportToExcel("SalesProfit"+branchselect.getSelectionModel().getSelectedItem()+strDate+strDate1+".xls", excelExport);
 	
			
    }

    @FXML
    void actionPrintSalesDtl(ActionEvent event) {

    }

    @FXML
    void onClickOk(ActionEvent event) {

    }

    @FXML
    void reportPrint(ActionEvent event) {

    }

    @FXML
    void show(ActionEvent event) {

   	 totalamount = 0.0;
 	java.util.Date uDate = Date.valueOf(dpdate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		//uDate = SystemSetting.StringToUtilDate(strDate,);
 	ResponseEntity<List<SalesProfitReportHdr>> salesProfitReport = RestCaller.getSalesProfit(branchselect.getSelectionModel().getSelectedItem(), strDate,strDate1);
 	salesProfitReportList = FXCollections.observableArrayList(salesProfitReport.getBody());
 	for(SalesProfitReportHdr dtl :salesProfitReportList)
 	{
 		if(null!=dtl.getProfit()) {
 		BigDecimal bdCAmount= new BigDecimal(dtl.getProfit());
 		
 		bdCAmount = bdCAmount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
 		dtl.setProfit(bdCAmount.doubleValue());
 		}
 	}
 	tbReport.setItems(salesProfitReportList);
 	clamt.setCellValueFactory(cellData -> cellData.getValue().getInvoiceAmountProperty());
 	clCustomer.setCellValueFactory(cellData -> cellData.getValue().getCustomerProperty());
 	clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
 	clreceiptmode.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());
 	clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
 	clDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
 	clProfitAmount.setCellValueFactory(cellData -> cellData.getValue().getProfitProperty());
 	for(SalesProfitReportHdr salesProfit:salesProfitReportList)
 	{
 		if(null != salesProfit.getProfit())
 		totalamount = totalamount + salesProfit.getProfit();
 	}
 	BigDecimal settoamount = new BigDecimal(totalamount);
		settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
 	txtGrandTotal.setText(settoamount.toString());

 	
 	
 	
    }

    @FXML
	private void initialize() {
    	dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    setBranches();
    btnPrintReport.setVisible(false);
    btnPrintSalesDtl.setVisible(false);
    btnPrint.setVisible(false);
    }
    
	private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			branchselect.getItems().add(branchMst.getBranchCode());
		
			
		}
		 
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   private void PageReload(String hdrId) {
	   	
	   }

}
