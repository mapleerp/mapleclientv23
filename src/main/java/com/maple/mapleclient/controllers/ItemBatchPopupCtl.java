package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemBatchMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.events.ItemBatchEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ItemBatchPopupCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<ItemBatchMst> itemBatchList = FXCollections.observableArrayList();
	private EventBus eventBus = EventBusFactory.getEventBus();

	ItemBatchEvent itemBatchEvent;

    @FXML
    private TextField txtname;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<ItemBatchMst> tablePopupView;

    @FXML
    private TableColumn<ItemBatchMst, String> batchCode;

    @FXML
    private Button btnCancel;

    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtname.requestFocus();
		}
    }

    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }

    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }

    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    
    @FXML
  	private void initialize() {
    
    	tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					itemBatchEvent = new ItemBatchEvent();
					itemBatchEvent.setItemId(newSelection.getItemId());
					itemBatchEvent.setBatchCode(newSelection.getBatch());

					eventBus.post(itemBatchEvent);
				}
			}
		});
    }
    
	public void itemBatchMstPopupByItemId(String itemId) {
		
		itemBatchList.clear();
		ResponseEntity<List<ItemBatchMst>> itemBatchResp = RestCaller.getItemBatchMstByItemId(itemId);
		itemBatchList = FXCollections.observableArrayList(itemBatchResp.getBody());
		FillTable();
//		return;
	}
	private void FillTable() {
		tablePopupView.setItems(itemBatchList);

		batchCode.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());

		
	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }

}
