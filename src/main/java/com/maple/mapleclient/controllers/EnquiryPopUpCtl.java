package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.EnquiryPopUp;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.EnquiryPopUpEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class EnquiryPopUpCtl {
	
	String taskid;
	String processInstanceId;
//	private EventBus eventBus = EventBusFactory.getEventBus();
	public ObservableList<EnquiryPopUp> popUpItemList1 = FXCollections.observableArrayList();

	EnquiryPopUp enquiryPopUp = null;
//	EnquiryPopUpEvent enquiryPopUpEvent;

	@FXML
	private TableView<EnquiryPopUp> tblEnquiry;
	@FXML
	private TableColumn<EnquiryPopUp, String> clItemName;

	@FXML
	private TableColumn<EnquiryPopUp, String> clVoucherType;

	@FXML
	private TableColumn<EnquiryPopUp, String> clUnit;

	@FXML
	private TableColumn<EnquiryPopUp, Number> clMRP;

	@FXML
	private TableColumn<EnquiryPopUp, Number> clQty;

	@FXML
	private void initialize() {

//		enquiryPopUpEvent = new EnquiryPopUpEvent();
//		eventBus.register(this);

//		tbEnquiry.setItems(popUpItemList1);
//		fillTable();
	//	showEnquiry("ab");
		tblEnquiry.setItems(popUpItemList1);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clMRP.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clVoucherType.setCellValueFactory(cellData -> cellData.getValue().getVoucherTypeProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

	}

	public void showEnquiry(String searchData,String number) {

//    	popUpItemList1.clear();
		ArrayList items = new ArrayList();
		items = RestCaller.SearchLastFiveTransactions(searchData,number);

		String itemName = "";
		String voucherType = "";
		String itemId = "";

		String unitId = "";
		Double mrp = 0.0;
		String unitname = "";
		Double qtyIn = 0.0;
		Double qtyOut = 0.0;

		Iterator itr = items.iterator();
		String batch = "";

		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			voucherType = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			qtyIn = (Double) element.get(4);
			qtyOut = (Double) element.get(5);
			unitId = (String) element.get(7);
			itemId = (String) element.get(6);
			batch = (String) element.get(8);

			if (null != itemName) {

				enquiryPopUp = new EnquiryPopUp();

				enquiryPopUp.setItemName(null == itemName ? "" : itemName);
				enquiryPopUp.setUnitName(null == unitname ? "" : unitname);
				enquiryPopUp.setVoucherType(null == voucherType ? "" : voucherType);
				enquiryPopUp.setUnitId(null == unitId ? "" : unitId);
				enquiryPopUp.setItemId(null == itemId ? "" : itemId);
				if (qtyIn == 0) {
					enquiryPopUp.setQty(null == qtyOut ? 0 : qtyOut);
				} else {
					enquiryPopUp.setQty(null == qtyIn ? 0 : qtyIn);
				}
				enquiryPopUp.setBatch(null == batch ? "" : batch);
				enquiryPopUp.setMrp(null == mrp ? 0 : mrp);
				popUpItemList1.add(enquiryPopUp);
				System.out.println("Enquiry pop Up" + popUpItemList1);

			}

		}

		return;

	}
	public void showEnquiryWithCustomer(String searchData,String custId,String number) {

//    	popUpItemList1.clear();
		ArrayList items = new ArrayList();
		items = RestCaller.SearchLastFiveTransactionsWithCustomer(searchData,custId,number);

		String itemName = "";
		String voucherType = "";
		String itemId = "";

		String unitId = "";
		Double mrp = 0.0;
		String unitname = "";
		Double qtyIn = 0.0;
		Double qtyOut = 0.0;

		Iterator itr = items.iterator();
		String batch = "";

		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			voucherType = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			qtyIn = (Double) element.get(4);
			qtyOut = (Double) element.get(5);
			unitId = (String) element.get(7);
			itemId = (String) element.get(6);
			batch = (String) element.get(8);

			if (null != itemName) {

				enquiryPopUp = new EnquiryPopUp();

				enquiryPopUp.setItemName(null == itemName ? "" : itemName);
				enquiryPopUp.setUnitName(null == unitname ? "" : unitname);
				enquiryPopUp.setVoucherType(null == voucherType ? "" : voucherType);
				enquiryPopUp.setUnitId(null == unitId ? "" : unitId);
				enquiryPopUp.setItemId(null == itemId ? "" : itemId);
				if (qtyIn == 0) {
					enquiryPopUp.setQty(null == qtyOut ? 0 : qtyOut);
				} else {
					enquiryPopUp.setQty(null == qtyIn ? 0 : qtyIn);
				}
				enquiryPopUp.setBatch(null == batch ? "" : batch);
				enquiryPopUp.setMrp(null == mrp ? 0 : mrp);
				popUpItemList1.add(enquiryPopUp);
				System.out.println("Enquiry pop Up" + popUpItemList1);

			}

		}

		return;

	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
