package com.maple.mapleclient.controllers;




import java.sql.Date;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.text.Format;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.text.SimpleDateFormat;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.ProductionDtl;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.entity.ProductionMst;
import com.maple.mapleclient.entity.ProductionPreplanningDtlReport;
import com.maple.mapleclient.entity.ProductionPreplanningMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
public class ProductionPrePlaningCtl {
	
	String taskid;
	String processInstanceId;

	private EventBus eventBus = EventBusFactory.getEventBus();
	ItemMst itemmst = new ItemMst();
	ProductionPreplanningMst productionPreplanningMst = null;
	ProductionDtl productionDtl = null;
	ProductionDtlDtl productionDtlDtl = null;
	UnitMst unitmst = null;
	List<ProductionPreplanningDtlReport> productionPreplanningDtlList=new ArrayList<ProductionPreplanningDtlReport>();
	private ObservableList<ProductionPreplanningDtlReport> productionDtlList = FXCollections.observableArrayList();
	private ObservableList<ProductionDtlDtl> productionDtlDtlList = FXCollections.observableArrayList();
	
	private ObservableList<KitDefinitionDtl> kitDefinitionDtlList = FXCollections.observableArrayList();
	double Qty = 0.0;
	double minQty = 0.0;
	double reqQty = 0.0;

    @FXML
    private TextField txtKitName;

    @FXML
    private Button btnShowReport;

    @FXML
    private Button btnAllRawMaterialReq;

    @FXML
    private Button btnPrintReport;

    @FXML
    private TextField txtBatch;

    @FXML
    private TextField txtQty;

    @FXML
    private DatePicker dpProductionDate;
    
    @FXML
    private DatePicker dpProductionToDate;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnClear;

    @FXML
    private Button btnPrintRawMaterial;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSaleOrderToProduction;

    @FXML
    private TableView<ProductionPreplanningDtlReport> tblProduction1;

    @FXML
    private TableColumn<ProductionPreplanningDtlReport, String> clKitname;

    @FXML
    private TableColumn<ProductionPreplanningDtlReport, String> clBatch;

    @FXML
    private TableColumn<ProductionPreplanningDtlReport, Number> clQty;

    @FXML
    private TableColumn<ProductionPreplanningDtlReport, String> columnId;



    @FXML
    private Button btnSubmit;

    @FXML
    void BatchKeyPressed(KeyEvent event) {

    }

    

    @FXML
    void PrintRawMaterial(ActionEvent event) {

    }

    @FXML
	void addItem(ActionEvent event) {
		/*
		 * 
		 * 
		 * 
		 * if(txtKitName.getText().trim().isEmpty()) { notifyMessage(5,
		 * " Please Select Kit name!!!"); txtKitName.requestFocus(); } else
		 * if(txtQty.getText().trim().isEmpty()) { notifyMessage(5, " Type Quantity");
		 * txtQty.requestFocus(); } else { if (null == productionMst) { productionMst =
		 * new ProductionMst();
		 * productionMst.setVoucherDate(Date.valueOf(dpProductionDate.getValue()));
		 * 
		 * productionMst.setBranchCode(SystemSetting.getSystemBranch());
		 * ResponseEntity<ProductionMst> respentity =
		 * RestCaller.saveProductionMst(productionMst); productionMst =
		 * respentity.getBody(); }
		 * 
		 * productionDtl = new ProductionDtl(); productionDtl.setStatus("N");
		 * productionDtl.setProductionMst(productionMst);
		 * productionDtl.setBatch(txtBatch.getText());
		 * productionDtl.setQty(Double.parseDouble(txtQty.getText())); Qty =
		 * Double.parseDouble(txtQty.getText()); ResponseEntity<ItemMst> repitem =
		 * RestCaller.getItemByNameRequestParam(txtKitName.getText()); itemmst =
		 * repitem.getBody();
		 * 
		 * // ResponseEntity<ItemMst> repitemmst =
		 * RestCaller.getitemMst(txtKitName.getText()); // itemmst =
		 * repitemmst.getBody(); productionDtl.setItemId(itemmst.getId());
		 * ResponseEntity<KitDefinitionMst> respkit =
		 * RestCaller.getKitDefinitionMst(productionDtl.getItemId()); if (null !=
		 * respkit.getBody()) { KitDefinitionMst kitdefinitionMst = new
		 * KitDefinitionMst(); kitdefinitionMst = respkit.getBody(); minQty =
		 * kitdefinitionMst.getMinimumQty();
		 * List<ProductionPreplanningDtl> productionPreplanningDtlList=new ArrayList<ProductionPreplanningDtl>();
		 * ResponseEntity<ProductionDtl> respentity =
		 * RestCaller.saveProductionDtl(productionDtl); productionDtl =
		 * respentity.getBody();
		 * 
		 * ResponseEntity<List<KitDefinitionDtl>> kitdefinitiondtlSaved = RestCaller
		 * .getKidefinitionDtl(kitdefinitionMst); kitDefinitionDtlList =
		 * FXCollections.observableArrayList(kitdefinitiondtlSaved.getBody()); for
		 * (KitDefinitionDtl kit : kitDefinitionDtlList) { productionDtlDtl = new
		 * ProductionDtlDtl();
		 * 
		 * productionDtlDtl.setItemName(kit.getItemName());
		 * 
		 * // reqQty = (kit.getQty() / minQty); // // double qty = reqQty * Qty; double
		 * q =(kit.getQty()* Qty); BigDecimal bdQty1 = new BigDecimal(q); bdQty1 =
		 * bdQty1.setScale(3, BigDecimal.ROUND_CEILING); double qty
		 * =bdQty1.doubleValue()/minQty; BigDecimal bdQty = new BigDecimal(qty); bdQty =
		 * bdQty.setScale(3, BigDecimal.ROUND_CEILING); // qty =
		 * productionDtlDtl.setQty(bdQty.doubleValue());
		 * productionDtlDtl.setRawMaterialItemId(kit.getItemId());
		 * productionDtlDtl.setUnitId(kit.getUnitId());
		 * productionDtlDtl.setProductionDtl(productionDtl);
		 * System.out.println(productionDtlDtl.getProductionDtl().getId());
		 * 
		 * ResponseEntity<ProductionDtlDtl> respentitydtldtl = RestCaller
		 * .saveProductionDtlDtl(productionDtlDtl); productionDtlDtl =
		 * respentitydtldtl.getBody(); // productionDtlDtlList.add(productionDtlDtl);
		 * 
		 * }
		 * 
		 * productionDtl.setKitname(itemmst.getItemName());
		 * 
		 * productionDtlList.add(productionDtl); txtKitName.clear(); txtQty.clear();
		 * 
		 * filltable();
		 * 
		 * // filltable2();
		 * 
		 * } else
		 * 
		 * notifyMessage(5, " Please Select Valid Kit name!!!"); }
		 * 
		 * Qty = 0.0; minQty = 0.0;
		 * txtBatch.setText(dpProductionDate.getValue().toString());
		 * 
		 */}

    @FXML
    void allRawMaterialReq(ActionEvent event) {

    }

    @FXML
    void clearfileds(ActionEvent event) {

    }

    @FXML
    void deleteItem(ActionEvent event) {

    	dpProductionDate.getValue();
    	dpProductionToDate.getValue();
    	if(null!=dpProductionDate.getValue()||null!=dpProductionToDate.getValue()) {
    	java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
    	Format formatter;
    	formatter = new SimpleDateFormat("yyyy-MM-dd");
    	String prodFromDate = formatter.format(uDate);
    	java.util.Date toDate=Date.valueOf(dpProductionToDate.getValue());
    	String prodToDate=formatter.format(toDate);
    	
    	RestCaller.deleteProductionPrePlanning(prodFromDate, prodToDate); 
    	
    	notifyMessage(5,"Preplanning  Deleted");
    
    	tblProduction1.getItems().clear();
    
    //	showReport();
    	}else {
    		notifyMessage(5,"Select  Date");
    	}
		/*
		 * if(null == productionDtl) { return; } if(null == productionDtl.getId()) {
		 * notifyMessage(5,"Select Item to Delete"); return; }
		 * 
		 * if(productionDtl.getStatus().equalsIgnoreCase("Y")) {
		 * notifyMessage(5,"Can't Delete Item , Actual Production Done!!!"); return; }
		 * RestCaller.deleteProductionDtls(productionDtl.getId());
		 * notifyMessage(5,"Item Deleted");
		 */
//    		tblProduction1.getItems().clear();
//    		tblProduction2.getItems().clear();
//    		productionDtlList.clear();
//    		ResponseEntity<List<ProductionDtl>> productionDtlSaved = RestCaller.getProductionDtlsbyMstId(productionMst);
//			productionDtlList = FXCollections.observableArrayList(productionDtlSaved.getBody());
//			if(!productionDtlList.isEmpty())
//			{
//			for (ProductionDtl proddtl : productionDtlList) {
//				ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(proddtl.getItemId());
//				itemmst = respitem.getBody();
//				proddtl.setKitname(itemmst.getItemName());
//				// productionDtlDtl.setUnitName(unitmst.getUnitName());
//				// productionDtlDtlList.set(5,proddtldtl );
//			}
//			if (null != productionDtlList) {
//				tblProduction1.setItems(productionDtlList);
//			}
    		
		//	productionDtl = null;
    }

    @FXML
    void printReport(ActionEvent event) {

    }
    
    private void showItemMst() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();
			
			txtQty.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    private void showReport() {
    	java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
    	Format formatter;
    	formatter = new SimpleDateFormat("yyyy-MM-dd");
    	String prodFromDate = formatter.format(uDate);
    	java.util.Date toDate=Date.valueOf(dpProductionToDate.getValue());
    	String prodToDate=formatter.format(toDate);
    	String branchCode=SystemSetting.getSystemBranch();	
    	
    	ResponseEntity<List<ProductionPreplanningDtlReport>> productionDtl=RestCaller.fetchProductionPreplanningDtl(	branchCode,prodFromDate,prodToDate);
    	productionPreplanningDtlList=productionDtl.getBody();
    	productionDtlList=FXCollections.observableArrayList(productionDtl.getBody());
    	if(productionPreplanningDtlList.size()==0) {
    		notifyMessage(5, "No Data !!!!!!!!!!");
		
    	}else {
    	filltable();
    	}
		/*
		 * tblProduction1.getItems().clear(); // if(null != productionDtlList ) //
		 * productionDtlList.clear(); // if (null != productionDtlDtlList) //
		 * productionDtlDtlList.clear(); java.util.Date uDate =
		 * Date.valueOf(dpProductionDate.getValue());
		 * //actualProductionHdr.setVoucherDate(uDate);
		 * //ResponseEntity<ActualProductionHdr> respentityHdr =
		 * RestCaller.saveActualProductionHdr(actualProductionHdr);
		 * //actualProductionHdr = respentityHdr.getBody(); Format formatter; formatter
		 * = new SimpleDateFormat("yyyy-MM-dd"); String voucherDate =
		 * formatter.format(uDate);
		 * 
		 * ResponseEntity<List<ProductionDtl>> productionDtlsaved =
		 * RestCaller.getKItDtlsbyDate(voucherDate); productionDtlList =
		 * FXCollections.observableArrayList(productionDtlsaved.getBody());
		 * 
		 * if(!productionDtlList.isEmpty()) { for (ProductionDtl proddtl :
		 * productionDtlList) { ResponseEntity<ItemMst> respitem =
		 * RestCaller.getitemMst(proddtl.getItemId()); itemmst = respitem.getBody();
		 * proddtl.setKitname(itemmst.getItemName()); //
		 * productionDtlDtl.setUnitName(unitmst.getUnitName()); //
		 * productionDtlDtlList.set(5,proddtldtl ); } filltables(); //filltable();
		 * productionDtl = null; productionDtlDtl = null;
		 * 
		 * } else notifyMessage(5, "No item Found!!");
		 */
	}

    @FXML
    void salOrderToProduction(ActionEvent event) {
    	productionDtlList.clear();
    	String branchCode=SystemSetting.getSystemBranch();	
    	java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
    	Format formatter;
    	formatter = new SimpleDateFormat("yyyy-MM-dd");
    	String prodDate = formatter.format(uDate);
    	java.util.Date toDate=Date.valueOf(dpProductionToDate.getValue());
    	String prodToDate=formatter.format(toDate);
    	
   
    ResponseEntity<String> basicResponse=RestCaller.fetchSaleOrderProductionPrePlanning(branchCode,prodDate,prodToDate);
  
    	
    		notifyMessage(5,basicResponse.getBody());
    		
    	System.out.print(basicResponse.getBody()+"response message issssssssssss");
    //	ResponseEntity<ProductionPreplanningMst> response=RestCaller.fectchProductionPreplanningMst(branchCode,prodDate);
    //	productionPreplanningMst=response.getBody();
    	
    	if(basicResponse.getBody().equalsIgnoreCase("Sucesses")){
    	
    	
    	ResponseEntity<List<ProductionPreplanningDtlReport>> productionDtl=RestCaller.fetchProductionPreplanningDtl(	branchCode,prodDate,prodToDate);
    	productionPreplanningDtlList=productionDtl.getBody();
    	productionDtlList=FXCollections.observableArrayList(productionDtl.getBody());
    	if(productionPreplanningDtlList.size()==0) {
    		notifyMessage(5, "No Data !!!!!!!!!!");
		
    	}else {
    	filltable();
    	}
    	}
    }

    @FXML
    void showItemPopUp(MouseEvent event) {
    	showItemMst();
    }

    @FXML
    void showReport(ActionEvent event) {

    }

	
	@FXML
	private void initialize() {
		dpProductionDate = SystemSetting.datePickerFormat(dpProductionDate, "dd/MMM/yyyy");
		dpProductionToDate = SystemSetting.datePickerFormat(dpProductionToDate, "dd/MMM/yyyy");
		eventBus.register(this);
		
		  try { btnShowReport.setVisible(false); btnPrintRawMaterial.setVisible(false);
		  btnPrintRawMaterial.setVisible(false); btnPrintReport.setVisible(false);
		  }catch (Exception e) { System.out.print(e.getMessage()); }
		 
		dpProductionDate.setValue(LocalDate.now());
		dpProductionToDate.setValue(LocalDate.now());
		txtBatch.setText(dpProductionDate.getValue().toString());
		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});
		dpProductionDate.valueProperty().addListener(new ChangeListener<LocalDate>() {
			@Override
			public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
				txtBatch.setText(dpProductionDate.getValue().toString());

			}

		});
		tblProduction1.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					/*
					 * productionDtl = new ProductionDtl();
					 * productionDtl.setId(newSelection.getId());
					 * productionDtl.setStatus(newSelection.getStatus());
					 * clDtDtlUnit.setCellValueFactory(cellData ->
					 * cellData.getValue().getunitNameProperty());
					 * clDtlDtlMaterail.setCellValueFactory(cellData ->
					 * cellData.getValue().getitemNameProperty());
					 * 
					 * clDtlDtlQty.setCellValueFactory(cellData ->
					 * cellData.getValue().getqtyProperty()); ResponseEntity<List<ProductionDtlDtl>>
					 * productiondtldtlSaved =
					 * RestCaller.getProductionDtlDtl(productionDtl.getId()); productionDtlDtlList =
					 * FXCollections.observableArrayList(productiondtldtlSaved.getBody()); for
					 * (ProductionDtlDtl proddtldtl : productionDtlDtlList) { try { BigDecimal bdQty
					 * = new BigDecimal(proddtldtl.getQty()); bdQty = bdQty.setScale(3,
					 * BigDecimal.ROUND_CEILING); proddtldtl.setQty(bdQty.doubleValue());
					 * ResponseEntity<UnitMst> respunit =
					 * RestCaller.getunitMst(proddtldtl.getUnitId()); unitmst = respunit.getBody();
					 * } catch (Exception e) { return; }
					 * proddtldtl.setUnitName(unitmst.getUnitName()); //
					 * productionDtlDtl.setUnitName(unitmst.getUnitName()); //
					 * productionDtlDtlList.set(5,proddtldtl ); }
					 * tblProduction2.setItems(productionDtlDtlList); // productionDtl = new
					 * ProductionDtl(); // productionDtlDtl = new ProductionDtlDtl();
					 * 
					 * // filltable(); // productionDtl = null;
					 */				}
			}
		});
	}
	
	private void filltable() {
	
		tblProduction1.setItems(productionDtlList);
		clKitname.setCellValueFactory(cellData -> cellData.getValue().getKitnameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		
	}
	
	private void filltables() {
		
		/*
		 * tblProduction1.setItems(productionDtlList);
		 * clKitname.setCellValueFactory(cellData ->
		 * cellData.getValue().getKitnameProperty());
		 * clBatch.setCellValueFactory(cellData ->
		 * cellData.getValue().getbatchProperty()); clQty.setCellValueFactory(cellData
		 * -> cellData.getValue().getqtyProperty());
		 * columnId.setCellValueFactory(cellData ->
		 * cellData.getValue().getIdProperty());
		 */

	}

	
	@Subscribe
	public void popupItemlistner(ItemPopupEvent itempopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		txtBatch.setText(dpProductionDate.getValue().toString());
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
			txtKitName.setText(itempopupEvent.getItemName());
		//	productionDtl.setItemId(itempopupEvent.getItemId());
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtKitName.getText());
			txtBatch.setText(txtBatch.getText()+getItem.getBody().getItemCode());
//		//	txtBatch.requestFocus();
//			txtBatch.setText(dpProductionDate.getValue().toString());
				}
			});
		}
	}
	@FXML
    void FinalSubmit(ActionEvent event) {

		ProductionMst productionMst=new ProductionMst();
		productionMst.setBranchCode(SystemSetting.getSystemBranch());
		String financialYear = "2019-2020";
		String sNo = RestCaller.getVoucherNumber(financialYear + "STON");
		productionMst.setVoucherNumber(sNo);
		productionMst.setVoucherDate(Date.valueOf(dpProductionDate.getValue()));
 		ResponseEntity<ProductionMst> respentity = RestCaller.saveProductionMst(productionMst);
		productionMst = respentity.getBody();

		
          List<ProductionDtl> productionDtlList=new ArrayList<ProductionDtl>();
          
        System.out.print(productionPreplanningDtlList.size()+"production pre planing list size isssssssssssssssssssssssssss");  
		 for(int i=0;productionPreplanningDtlList.size()<0;i++ ) {
			ProductionDtl productionDtl =new ProductionDtl();
			productionDtl.setKitname(productionPreplanningDtlList.get(i).getItemName());
			productionDtl.setItemId(productionPreplanningDtlList.get(i).getItemId());
			productionDtl.setProductionMst(productionMst);
			productionDtl.setQty(productionPreplanningDtlList.get(i).getQty());
			productionDtl.setBatch(productionPreplanningDtlList.get(i).getBatch());
			productionDtl.setStatus("N");
			productionDtlList.add(productionDtl);
			
		}
		if( productionPreplanningDtlList.size()>0) {
		 ResponseEntity<String> ResponeMessage = RestCaller.saveProductionDtlList(productionPreplanningDtlList,productionMst.getId());
		 notifyMessage(5, ResponeMessage.getBody());
		 
		}else {
			notifyMessage(5, "No Data !!!!!!!!!!");
		}
		productionDtlDtlList.clear();
		productionDtlList.clear();
		tblProduction1.getItems().clear();
		
	
    }
	
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}
	  @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }
	
}
