package com.maple.mapleclient.controllers;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;
import com.maple.report.entity.BranchWiseProfitReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import net.sf.jasperreports.components.table.fill.FillTable;

public class BranchProfitCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<BranchWiseProfitReport> branchPrifitList = FXCollections.observableArrayList();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");


    @FXML
    private TableView<BranchWiseProfitReport> tblBranchProfit;

    @FXML
    private TableColumn<BranchWiseProfitReport, String> clItemName;

    @FXML
    private TableColumn<BranchWiseProfitReport, Number> clProfit;
    
    
    
    public void branchProfit(String startDate, String endDate, String branchCode) {
    	
    	ResponseEntity<List<BranchWiseProfitReport>> branchProfitResp = RestCaller.branchProfit(startDate,endDate,branchCode);
    	branchPrifitList = FXCollections.observableArrayList(branchProfitResp.getBody());
    	
    	FillTable();
    	
	}

	private void FillTable() {
		tblBranchProfit.setItems(branchPrifitList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clProfit.setCellValueFactory(cellData -> cellData.getValue().getProfitProperty());
		
		clProfit.setCellFactory(tc-> new TableCell<BranchWiseProfitReport, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});


	}
	
    @FXML
    void EscOnKeyPress(KeyEvent event) {

    	if (event.getCode() == KeyCode.ESCAPE) {
			  Stage stage = (Stage) tblBranchProfit.getScene().getWindow();
				stage.close();

			}
    }

    @FXML
    void EscOnKeyPress1(KeyEvent event) {

    	if (event.getCode() == KeyCode.ESCAPE) {
			  Stage stage = (Stage) tblBranchProfit.getScene().getWindow();
				stage.close();

			}
    }

    @FXML
    void EscOnKeyPress2(KeyEvent event) {
    	if (event.getCode() == KeyCode.ESCAPE) {
			  Stage stage = (Stage) tblBranchProfit.getScene().getWindow();
				stage.close();

			}

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


       private void PageReload() {
       	
 }

}
