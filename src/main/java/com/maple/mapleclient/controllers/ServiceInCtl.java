package com.maple.mapleclient.controllers;

import java.io.IOException;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BrandMst;

import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.entity.ProductMst;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.ServiceInDtl;
import com.maple.mapleclient.entity.ServiceInHdr;
import com.maple.mapleclient.entity.ServiceInReceipt;
import com.maple.mapleclient.entity.WatchComplaintMst;
import com.maple.mapleclient.entity.WatchObservationMst;
import com.maple.mapleclient.entity.WatchStrapMst;
import com.maple.mapleclient.events.BrandEvent;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.ProductEvent;
import com.maple.mapleclient.events.WatchCompliantEvent;
import com.maple.mapleclient.events.WatchObservationEvent;
import com.maple.mapleclient.events.WatchStrapEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class ServiceInCtl {
	
	String taskid;
	String processInstanceId;
	
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ServiceInDtl> serviceListTable = FXCollections.observableArrayList();
	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
	ServiceInReceipt serviceInReceipt = new ServiceInReceipt();
	String salesReceiptVoucherNo = null;
	private ObservableList<ServiceInReceipt> serviceInReceiptptsList = FXCollections.observableArrayList();
	double cardAmount = 0.0;

	ServiceInHdr serviceInHdr = null;
	ServiceInDtl serviceInDtl = null;
	String custId = null;
	
	
	StringProperty productProperty = new SimpleStringProperty("");
	StringProperty brandProperty = new SimpleStringProperty("");
	StringProperty modelProperty = new SimpleStringProperty("");
	
	@FXML
	private DatePicker dpDate;

	@FXML
	private TextField txtCustomer;

	@FXML
	private TextField txtModel;

	@FXML
	private TextField txtProduct;

	@FXML
	private TextField txtBrand;

	@FXML
	private TextField txtItemName;

	

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowlAll;
	@FXML
	private TextField txtSerialId;

	@FXML
	private TextField txtComplaint;

	@FXML
	private TableView<ServiceInDtl> tblService;

	@FXML
	private TableColumn<ServiceInDtl, String> clCustomer;

	@FXML
	private TableColumn<ServiceInDtl, String> clModel;

	@FXML
	private TableColumn<ServiceInDtl, String> clProduct;

	@FXML
	private TableColumn<ServiceInDtl, String> clBrand;

	@FXML
	private TableColumn<ServiceInDtl, Number> clQty;

	@FXML
	private TableColumn<ServiceInDtl, String> clItemName;

	@FXML
	private TableColumn<ServiceInDtl, String> clComplaint;
	@FXML
	private TableColumn<ServiceInDtl, String> clWarranty;

	@FXML
	private TableColumn<ServiceInDtl, String> clLadiesOrGents;

	@FXML
	private TableColumn<ServiceInDtl, String> clStrap;

	@FXML
	private TableColumn<ServiceInDtl, String> clBattery;

	@FXML
	private Button btnFinalSave;

	@FXML
	private Button btnPrint;

	@FXML
	private TextField txtObservation;

	@FXML
	private ChoiceBox<String> cmbWarranty;

	@FXML
	private RadioButton rbMale;

	@FXML
	private RadioButton rbFemale;

	@FXML
	private RadioButton rbMetal;

	@FXML
	private RadioButton rbStrap;

	@FXML
	private RadioButton rbBattery;
	
	  @FXML
	    private TextField txtAdvanceAmount;

	    @FXML
	    private TextField txtEstmatedCharge;

	    @FXML
	    private DatePicker dpEstmtdDeliveryDate;

	    @FXML
	    private TextField txtcardAmount;


	    @FXML
	    private Label lblCardType;

	    @FXML
	    private Label lblAmount;

	  
	    @FXML
	    private TextField txtStrap;
	    
	    @FXML
	    void StrapPopup(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    	StrapPopup();
	    	}
	    	
	    }
	    
	    
		private void StrapPopup() {
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/WatchStrapPopup.fxml"));
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				rbMale.requestFocus();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		// ---------------------------------------------------------------------------------------------
	

	    @FXML
	    void OnAdvanceKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				btnFinalSave.requestFocus();
			}
	    	
	    	if (event.getCode() == KeyCode.LEFT) {
	    		
	    		visibleCardPayment();
	    		
	    		
	    		cmbCardType.requestFocus();
			}
	    }
	    
	    @FXML
	    private Button btnVisibleCardPayment;
	    
	    @FXML
	    void VisibleCardPayment(ActionEvent event) {
	    	visibleCardPayment();
	    }
	    
		private void visibleCardPayment() {
			cmbCardType.setVisible(true);
			lblCardType.setVisible(true);
			lblAmount.setVisible(true);
			AddCardAmount.setVisible(true);
			txtCrAmount.setVisible(true);
			btnDeleteCardAmount.setVisible(true);
			tblCardAmountDetails.setVisible(true);
			txtcardAmount.setVisible(true);
			
		}

		@FXML
		private ComboBox<String> cmbCardType;

		@FXML
		void FocusOnCardAmount(KeyEvent event) {
			if (event.getCode() == KeyCode.ENTER) {
				txtCrAmount.requestFocus();
			}
		}

		@FXML
		void setCardType(MouseEvent event) {

			setCardType();
		}

		@FXML
		private TextField txtCrAmount;

		@FXML
		void KeyPressFocusAddBtn(KeyEvent event) {

			if (event.getCode() == KeyCode.ENTER) {

					AddCardAmount.requestFocus();
				}
		}

		private void setCardType() {
			cmbCardType.getItems().clear();

			ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
			receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());

			for (ReceiptModeMst receiptModeMst : receiptModeList) {
				if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
					cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
				}
			}
		}

		@FXML
		private Button AddCardAmount;

		@FXML
		void KeyPressAddCardAmount(KeyEvent event) {
			if (event.getCode() == KeyCode.ENTER) {

				addCardAmount();
			}

		}

		@FXML
		void addCardAmount(ActionEvent event) {

			addCardAmount();

		}

		private void addCardAmount() {

			if (null != serviceInHdr) {

				if (null == cmbCardType.getValue()) {

					notifyMessage(5, "Please select card type!!!!",false);
					cmbCardType.requestFocus();
					return;

				} else if (txtCrAmount.getText().trim().isEmpty()) {

					notifyMessage(1, "Please enter amount!!!!",false);
					txtCrAmount.requestFocus();
					return;

				} else {

					serviceInReceipt = new ServiceInReceipt();

					ResponseEntity<AccountHeads> accountHeads = RestCaller
							.getAccountHeadByName(cmbCardType.getSelectionModel().getSelectedItem());
					serviceInReceipt.setAccountId(accountHeads.getBody().getId());

					serviceInReceipt.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());
					serviceInReceipt.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
					serviceInReceipt.setUserId(SystemSetting.getUser().getId());
					serviceInReceipt.setBranchCode(SystemSetting.systemBranch);
					if (null == salesReceiptVoucherNo) {
						salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
						serviceInReceipt.setVoucherNumber(salesReceiptVoucherNo);
					} else {
						serviceInReceipt.setVoucherNumber(salesReceiptVoucherNo);
					}
					LocalDate date = LocalDate.now();
					java.util.Date udate = SystemSetting.localToUtilDate(date);
					serviceInReceipt.setRereceiptDate(udate);
					serviceInReceipt.setServiceInHdr(serviceInHdr);
					
					ResponseEntity<ServiceInReceipt> respEntity = RestCaller.saveServiceInReceipt(serviceInReceipt);
					serviceInReceipt = respEntity.getBody();

					if (null != serviceInReceipt) {
						if (null != serviceInReceipt.getId()) {
//		    				cardAmount = cardAmount+Double.parseDouble(txtCrAmount.getText());
							// txtcardAmount.setText(Double.toString(txtPaidamount));
							serviceInReceiptptsList.add(serviceInReceipt);
							filltableCardAmount();
						}
					}

					txtCrAmount.clear();
					cmbCardType.getSelectionModel().clearSelection();
					cmbCardType.requestFocus();
					serviceInReceipt = null;

				}
			} else {
				notifyMessage(1, "Please add Item!!!!",false);
				return;
			}
		}

		@FXML
		private Button btnDeleteCardAmount;

		@FXML
		void DeleteCardAmount(ActionEvent event) {
			if (null != serviceInReceipt) {
				if (null != serviceInReceipt.getId()) {
					RestCaller.deleteServiceInReceipt(serviceInReceipt.getId());
					
					tblCardAmountDetails.getItems().clear();
					ResponseEntity<List<ServiceInReceipt>> salesreceiptResp = RestCaller
							.getAllServiceInReceiptByHdrId(serviceInHdr.getId());
					serviceInReceiptptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

					filltableCardAmount();

				}
			} else {

				notifyMessage(1, "Please select  card payment details!!!!",false);			}

		}

		@FXML
		void KeyPressDeleteCardAmount(KeyEvent event) {

		}

		@FXML
		private TableView<ServiceInReceipt> tblCardAmountDetails;

		@FXML
		private TableColumn<ServiceInReceipt, String> clCardTye;

		@FXML
		private TableColumn<ServiceInReceipt, Number> clCardAmount;

		private void filltableCardAmount() {

			tblCardAmountDetails.setItems(serviceInReceiptptsList);
			clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
			clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

			Double cardAmount = RestCaller.findTotalAmountOfServiceInReceipts(serviceInHdr.getId());
			
			BigDecimal bdCashToPay = new BigDecimal(cardAmount);
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
			txtcardAmount.setText(bdCashToPay.toString());

		}


	@FXML
	void BatteryOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtComplaint.requestFocus();
		}
	}

	@FXML
	void StrapOnEnter(KeyEvent event) {

	}

	@FXML
	void MetalOnEnter(KeyEvent event) {

	}

	@FXML
	void MaleOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			rbFemale.requestFocus();
		}

	}

	@FXML
	void FemaleOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			rbBattery.requestFocus();
		}
	}

	@FXML
	void WarrantyOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtStrap.requestFocus();
		}

	}

	@FXML
	void BrandPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			
			if(!txtBrand.getText().trim().isEmpty())
			{
				txtProduct.requestFocus();
			} else {
				loadBrandPopUp();
			}
		}
	}

	@FXML
	void OnFemaleClick(ActionEvent event) {

		if (rbMale.isSelected()) {
			rbMale.setSelected(false);
			rbFemale.setSelected(true);
		}

	}

	@FXML
	void OnMaleClick(ActionEvent event) {
		if (rbFemale.isSelected()) {
			rbFemale.setSelected(false);
			rbMale.setSelected(true);
		}
	}



	@FXML
	private void initialize() {
		dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
		dpEstmtdDeliveryDate = SystemSetting.datePickerFormat(dpEstmtdDeliveryDate, "dd/MMM/yyyy");
		txtProduct.textProperty().bindBidirectional(productProperty);
		txtBrand.textProperty().bindBidirectional(brandProperty);
		txtModel.textProperty().bindBidirectional(modelProperty);
		
		eventBus.register(this);

		cmbWarranty.getItems().add("WARRANTY");
		cmbWarranty.getItems().add("SERVICE WARRANTY");
		cmbWarranty.getItems().add("NONE");
		

		tblService.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					serviceInDtl = new ServiceInDtl();
					serviceInDtl.setId(newSelection.getId());
				}
			}
		});
		
		tblCardAmountDetails.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					serviceInReceipt = new ServiceInReceipt();
					serviceInReceipt.setId(newSelection.getId());
				}
			}
		});
		
		productProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtBrand.getText().length() > 0) && (txtModel.getText().length() > 0)) {

					String brand = txtBrand.getText();
					String model = txtModel.getText();
					String product =(String) newValue;
					
					String itemName = brand+"-"+product+"-"+model;
					txtItemName.setText(itemName);
				} 
				
			}
		});
		
		brandProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtProduct.getText().length() > 0) && (txtModel.getText().length() > 0)) {

					String brand = (String) newValue;
					String model = txtModel.getText();
					String product = txtProduct.getText();
					
					String itemName = brand+"-"+product+"-"+model;
					txtItemName.setText(itemName);
				} 
				
			}
		});
		
		modelProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtProduct.getText().length() > 0) && (txtBrand.getText().length() > 0)) {

					String model = (String) newValue;
					String brand = txtBrand.getText();
					String product = txtProduct.getText();
					
					String itemName = brand+"-"+product+"-"+model;
					txtItemName.setText(itemName);
				} 
				
			}
		});
	}

	@FXML
	void CustomerPopup(MouseEvent event) {
		loadCustomerPopup();
	}

	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			txtBrand.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnDelete.getScene().getWindow();
		if (stage.isShowing()) {

			txtCustomer.setText(customerEvent.getCustomerName());
			custId = customerEvent.getCustId();
		}

	}

	@Subscribe
	public void popupWatchStraplistner(WatchStrapEvent watchStrapEvent) {

		Stage stage = (Stage) btnDelete.getScene().getWindow();
		if (stage.isShowing()) {

			txtStrap.setText(watchStrapEvent.getStrapName());
//			custId = customerEvent.getCustId();
		}

	}
	@FXML
	void FinalSave(ActionEvent event) {
		
		finalSave();
	}

	@FXML
	void deleteItem(ActionEvent event) {

		if (null == serviceInDtl) {
			return;
		}
		if (null == serviceInDtl.getId()) {
			return;
		}
		RestCaller.deleteServiceDtl(serviceInDtl.getId());
		ResponseEntity<List<ServiceInDtl>> service = RestCaller.getServiceByHdrId(serviceInHdr.getId());
		serviceListTable = FXCollections.observableArrayList(service.getBody());
		fillTable();
		notifyMessage(5, "Deleted Successfully", true);

	}

	@FXML
	void ItemPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

//			txtQty.requestFocus();

		}
	}

	@FXML
	void ModelPopup(KeyEvent event) {
		
		if (event.getCode() == KeyCode.ENTER) {
			txtSerialId.requestFocus();
		}

	}

	@FXML
	void OnFinalSave(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		finalSave();
		}

	}

	private void finalSave() {
		
		if(null == dpEstmtdDeliveryDate.getValue())
		{
			notifyMessage(5, "Please Estimated delivary date!!!!",false);
			dpEstmtdDeliveryDate.requestFocus();
			return;
		}

		if(txtEstmatedCharge.getText().trim().isEmpty())
		{
			notifyMessage(5, "Please enter estimated charge!!!!",false);
			txtEstmatedCharge.requestFocus();
			return;
		}
		Double cardAmount = 0.0;
		Double cashAmount = 0.0;
		
		
		ResponseEntity<List<ServiceInDtl>> service = RestCaller.getServiceByHdrId(serviceInHdr.getId());
		serviceListTable = FXCollections.observableArrayList(service.getBody());

		if (serviceListTable.isEmpty()) {
			return;
		}

		String financialYear = SystemSetting.getFinancialYear();
		String sNo = RestCaller.getVoucherNumber(financialYear + "SERIN");
		
		if(!txtAdvanceAmount.getText().trim().isEmpty())
		{
			cashAmount = Double.parseDouble(txtAdvanceAmount.getText());
			serviceInReceipt = new ServiceInReceipt();

			ResponseEntity<AccountHeads> accountHeads = RestCaller
					.getAccountHeadByName(SystemSetting.systemBranch+"-"+"CASH");
			serviceInReceipt.setAccountId(accountHeads.getBody().getId());

			serviceInReceipt.setReceiptMode(SystemSetting.systemBranch+"-"+"CASH");
			serviceInReceipt.setReceiptAmount(Double.parseDouble(txtAdvanceAmount.getText()));
			serviceInReceipt.setUserId(SystemSetting.getUser().getId());
			serviceInReceipt.setBranchCode(SystemSetting.systemBranch);
			if (null == salesReceiptVoucherNo) {
				salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
				serviceInReceipt.setVoucherNumber(salesReceiptVoucherNo);
			} else {
				serviceInReceipt.setVoucherNumber(salesReceiptVoucherNo);
			}
			LocalDate date = LocalDate.now();
			java.util.Date udate = SystemSetting.localToUtilDate(date);
			serviceInReceipt.setRereceiptDate(udate);
			serviceInReceipt.setServiceInHdr(serviceInHdr);
			
			ResponseEntity<ServiceInReceipt> respEntity = RestCaller.saveServiceInReceipt(serviceInReceipt);
			serviceInReceipt = respEntity.getBody();

		}
		
		
		serviceInHdr.setVoucherNumber(sNo);
		
		serviceInHdr.setEstimatedDeliveryDate(SystemSetting.localToUtilDate(dpEstmtdDeliveryDate.getValue()));
		if(txtEstmatedCharge.getText().trim().isEmpty())
		{
			serviceInHdr.setEstimatedCharge(0.0);
		} else {
			serviceInHdr.setEstimatedCharge(Double.parseDouble(txtEstmatedCharge.getText()));

		}
		
		if(!txtcardAmount.getText().trim().isEmpty())
		{
			cardAmount = Double.parseDouble(txtcardAmount.getText());
		}
		
		serviceInHdr.setAdvanceAmount(cardAmount+cashAmount);
		RestCaller.updateServiceInHdr(serviceInHdr);

		notifyMessage(3, "Saved Successfully", true);

		Alert a = new Alert(AlertType.CONFIRMATION);
		a.setHeaderText("Please Create Job Card...");
		a.showAndWait().ifPresent((btnType) -> {
			if (btnType == ButtonType.OK) {

				for (ServiceInDtl serviceIn : serviceListTable) {
					JobCardHdr jobCardHdr = new JobCardHdr();
					jobCardHdr.setCustomerId(serviceIn.getServiceInHdr().getAccountHeads().getId());
					jobCardHdr.setServiceInDtl(serviceIn);
					jobCardHdr.setStatus("ACTIVE");
					jobCardHdr.setBranchCode(SystemSetting.systemBranch);
					ResponseEntity<JobCardHdr> savedJobCard = RestCaller.saveJobCard(jobCardHdr);

				}
			} else if (btnType == ButtonType.CANCEL) {

				return;

			}
		});

		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(serviceInHdr.getVoucherDate());

		try {
			JasperPdfReportService.serviceInvoice(serviceInHdr,serviceInHdr.getVoucherNumber(), strDate);
		} catch (JRException e) {
			e.printStackTrace();
		}

		serviceInHdr = null;
		serviceInDtl = null;
		tblService.getItems().clear();
		serviceListTable.clear();
		txtCustomer.clear();
		txtStrap.clear();
		salesReceiptVoucherNo = null;
		tblCardAmountDetails.getItems().clear();
		serviceInReceiptptsList.clear();
		txtcardAmount.clear();
		dpEstmtdDeliveryDate.setValue(null);
		txtEstmatedCharge.clear();
		txtAdvanceAmount.clear();
		dpDate.setValue(null);
		serviceInReceipt = null;
		txtCustomer.clear();
		
		cmbCardType.setVisible(false);
		lblCardType.setVisible(false);
		lblAmount.setVisible(false);
		AddCardAmount.setVisible(false);
		txtCrAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		tblCardAmountDetails.setVisible(false);
		txtcardAmount.setVisible(false);
		
		dpDate.requestFocus();
	
		
	}


	public void notifyMessage(int duration, String msg, boolean success) {
		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@FXML
	void OnPrint(KeyEvent event) {

	}

	@FXML
	void OnShowAll(KeyEvent event) {

	}

	@FXML
	void Print(ActionEvent event) {

	}

	@FXML
	void ProductPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if(!txtProduct.getText().trim().isEmpty())
			{
				txtModel.requestFocus();
			} else {
				loadProdcutPopUp();
			}
		}
	}

	@FXML
	void Save(ActionEvent event) {
		saveserviceItem();
	}
	
	@FXML
    void DeliveryDateOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtEstmatedCharge.requestFocus();
		}

    }
	
	  @FXML
	    void EstimatedChargeOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtAdvanceAmount.requestFocus();
		}
	    }

	
	private void saveserviceItem() {
		
		if(null != serviceInHdr)
		{
			if(null != serviceInHdr.getId())
			{
				ResponseEntity<List<ServiceInDtl>> savedServiceInDtl = RestCaller.getServiceByHdrId(serviceInHdr.getId());
				List<ServiceInDtl> savedServiceInDtlList = savedServiceInDtl.getBody();
				if(savedServiceInDtlList.size() >= 1)
				{
					notifyMessage(5, "Please Add new item After finalSave", false);
					btnFinalSave.requestFocus();
					return;
				}
			}		
		}

		if (null == dpDate.getValue()) {
			notifyMessage(5, "Please Select Date", false);
			dpDate.requestFocus();
			return;
		}
		if (txtCustomer.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Select Customer", false);
			txtCustomer.requestFocus();
			return;
		}
		if (txtSerialId.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter Serial Id", false);
			txtSerialId.requestFocus();
			return;

		}
		if (txtProduct.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter Product Name", false);
			txtProduct.requestFocus();
			return;
		}
		if (txtBrand.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter Brand Name", false);
			txtBrand.requestFocus();
			return;
		}
		if (txtModel.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter Model", false);
			txtModel.requestFocus();
			return;
		}
		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter Item Name", false);
			txtItemName.requestFocus();
			return;
		}
		if (txtStrap.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter watch strap type ", false);
			txtStrap.requestFocus();
			return;
		}
		
		if (null == serviceInHdr) {
			serviceInHdr = new ServiceInHdr();
			ResponseEntity<AccountHeads> getCustomer = RestCaller.getAccountHeadsById(custId);
			serviceInHdr.setAccountHeads(getCustomer.getBody());
			serviceInHdr.setBranchCode(SystemSetting.systemBranch);
			serviceInHdr.setVoucherDate(SystemSetting.localToUtilDate(dpDate.getValue()));
			ResponseEntity<ServiceInHdr> respentity = RestCaller.saveServiceinhdr(serviceInHdr);
			serviceInHdr = respentity.getBody();
		}
		serviceInDtl = new ServiceInDtl();
		serviceInDtl.setModel(txtModel.getText());
		serviceInDtl.setServiceInHdr(serviceInHdr);
		serviceInDtl.setSerialId(txtSerialId.getText());
		serviceInDtl.setQty(1.0);
		serviceInDtl.setProductName(txtProduct.getText());
		
		ResponseEntity<WatchObservationMst> observationResp = RestCaller.SearchWatchObseravtionByName(txtObservation.getText());
		WatchObservationMst watchObservationMst = observationResp.getBody();
		if(null == watchObservationMst)
		{
			watchObservationMst = new WatchObservationMst();
			watchObservationMst.setObservation(txtObservation.getText());
			watchObservationMst.setBranchCode(SystemSetting.systemBranch);

			ResponseEntity<WatchObservationMst> savedComplaint = RestCaller.saveWatchObservation(watchObservationMst);
		}
		
		serviceInDtl.setObservation(txtObservation.getText());
		serviceInDtl.setItemName(txtItemName.getText());
		serviceInDtl.setBrandName(txtBrand.getText());
		ResponseEntity<WatchComplaintMst> complaintResp = RestCaller.SearchWatchComplaintByName(txtComplaint.getText());
		WatchComplaintMst watchComplaintMst = complaintResp.getBody();
		if(null == watchComplaintMst)
		{
			watchComplaintMst = new WatchComplaintMst();
			watchComplaintMst.setComplaint(txtComplaint.getText());
			watchComplaintMst.setBranchCode(SystemSetting.systemBranch);

			ResponseEntity<WatchComplaintMst> savedComplaint = RestCaller.saveWatchCompliant(watchComplaintMst);
		}
		serviceInDtl.setComplaints(txtComplaint.getText());

		if (rbBattery.isSelected()) {
			serviceInDtl.setBattery("YES");
		} else {
			serviceInDtl.setBattery("NO");
		}

		if (rbFemale.isSelected()) {
			serviceInDtl.setLadiesOrGents("LADIES");
		}
		if (rbMale.isSelected()) {
			serviceInDtl.setLadiesOrGents("GENTS");
		}
		
		serviceInDtl.setUnderWarranty(cmbWarranty.getSelectionModel().getSelectedItem().toString());

		ProductMst productMst;
		ResponseEntity<ProductMst> getProduct = RestCaller.findProductMstByName(txtProduct.getText());
		productMst = getProduct.getBody();
		if (null == productMst) {
			productMst = new ProductMst();
			productMst.setProductName(txtProduct.getText());
			productMst.setBranchCode(SystemSetting.systemBranch);
			ResponseEntity<ProductMst> respproduct = RestCaller.saveServiceProductMst(productMst);
			productMst = respproduct.getBody();
		}
		BrandMst brandMst;
		ResponseEntity<BrandMst> getBrand = RestCaller.findBrandMstByName(txtBrand.getText());
		brandMst = getBrand.getBody();
		if (null == brandMst) {
			brandMst = new BrandMst();
			brandMst.setBrandName(txtBrand.getText());
			brandMst.setBranchCode(SystemSetting.systemBranch);
			ResponseEntity<BrandMst> respbrand = RestCaller.saveServiceBrandMst(brandMst);
			brandMst = respbrand.getBody();
		}
		
		ResponseEntity<WatchStrapMst> watchStrapResp = RestCaller.findWatchStrapByName(txtStrap.getText());
		WatchStrapMst watchStrapMst = watchStrapResp.getBody();
		
		if(null == watchStrapMst)
		{
			watchStrapMst = new WatchStrapMst();
			watchStrapMst.setStrapName(txtStrap.getText());
			watchStrapMst.setBranchCode(SystemSetting.systemBranch);
		
			ResponseEntity<WatchStrapMst> watchStrapResp1 = RestCaller.SaveWatchStrapMst(watchStrapMst);
			watchStrapMst = watchStrapResp1.getBody();
		}
				
		serviceInDtl.setStrap(watchStrapMst.getStrapName());
		
		serviceInDtl.setBrandId(brandMst.getId());
		serviceInDtl.setItemName(txtItemName.getText());
		serviceInDtl.setProductId(productMst.getId());
		serviceInDtl.setObservation(txtObservation.getText());
		ResponseEntity<ServiceInDtl> respentity = RestCaller.saveServiceinDtl(serviceInDtl);
		serviceInDtl = respentity.getBody();
		serviceListTable.add(serviceInDtl);
		fillTable();
		clearfields();
		txtStrap.clear();
		serviceInDtl = null;
		dpEstmtdDeliveryDate.requestFocus();
	
		
	}

	private void clearfields() {
		txtBrand.clear();
		txtComplaint.clear();
		txtItemName.clear();
		txtModel.clear();
		txtObservation.clear();
		txtProduct.clear();
		txtSerialId.clear();
		txtSerialId.requestFocus();
		rbMale.setSelected(false);
		rbBattery.setSelected(false);
		rbFemale.setSelected(false);
		cmbWarranty.getSelectionModel().clearSelection();
//		dpDate.setValue(null);
//		txtCustomer.clear();

	}

	@FXML
	void serialIdOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			cmbWarranty.requestFocus();

		}
	}

	@FXML
	void modeOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			if (!txtModel.getText().trim().isEmpty()) {
				txtItemName.setText(txtBrand.getText() + txtProduct.getText() + txtModel.getText());
				txtComplaint.requestFocus();
			} else {
				txtModel.requestFocus();
			}

		}
	}

	@FXML
	void productOnEnter(KeyEvent event) {

	}

	private void fillTable() {
		for (ServiceInDtl ser : serviceListTable) {
			ResponseEntity<BrandMst> getBrand = RestCaller.brandById(ser.getBrandId());
			ser.setBrandName(getBrand.getBody().getBrandName());
			ResponseEntity<ProductMst> getProduct = RestCaller.productById(ser.getProductId());
			ser.setProductName(getProduct.getBody().getProductName());
		}
		tblService.setItems(serviceListTable);
		clWarranty.setCellValueFactory(cellData -> cellData.getValue().getWarrantyProperty());
		clLadiesOrGents.setCellValueFactory(cellData -> cellData.getValue().getGenderProperty());
		clBattery.setCellValueFactory(cellData -> cellData.getValue().getBatteryProperty());
		clStrap.setCellValueFactory(cellData -> cellData.getValue().getStrapProperty());

		clBrand.setCellValueFactory(cellData -> cellData.getValue().getbrandProperty());
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clModel.setCellValueFactory(cellData -> cellData.getValue().getModelProperty());
		clProduct.setCellValueFactory(cellData -> cellData.getValue().getproductProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		clComplaint.setCellValueFactory(cellData -> cellData.getValue().getcomplaintProperty());

	}

	@Subscribe
	public void popupItemlistner(ProductEvent productEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnDelete.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			txtProduct.setText(productEvent.getProductName());

		}
	}

	@Subscribe
	public void popupItemlistner(BrandEvent brandEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnDelete.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			txtBrand.setText(brandEvent.getBrandName());

		}
	}
	
	@Subscribe
	public void popupObservationListner(WatchObservationEvent watchObservationEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) txtObservation.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			txtObservation.setText(watchObservationEvent.getObservation());

		}
	}

	
	@Subscribe
	public void popupComplaintListner(WatchCompliantEvent watchCompliantEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) txtComplaint.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			txtComplaint.setText(watchCompliantEvent.getComplaint());

		}
	}
	private void loadBrandPopUp() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/BrandPopUp.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtProduct.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadProdcutPopUp() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ProductPopUp.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtModel.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadComplaintPopUp() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/WatchComplaintPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtObservation.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	private void loadObservationPopUp() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/WatchObservationPopup.fxml"));
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			btnSave.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@FXML
	void ShowAll(ActionEvent event) {

	}

	@FXML
	void focusOnDelete(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			saveserviceItem();
		}
		if (event.getCode() == KeyCode.RIGHT) {
			btnDelete.requestFocus();
		}
	}

	@FXML
	void focusOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if(!txtComplaint.getText().trim().isEmpty())
			{
				txtObservation.requestFocus();
			} else {
				loadComplaintPopUp();
			}
		}
	}

	@FXML
	void focusOnModel(KeyEvent event) {

	}

	@FXML
	void focusOnSave(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if(!txtObservation.getText().trim().isEmpty())
			{
				btnSave.requestFocus();
			} else {
				loadObservationPopUp();
			}
			
//			btnSave.requestFocus();
		}

	}

	@FXML
	void focusOnShowAll(KeyEvent event) {

	}


	@FXML
	void fucosOnCust(KeyEvent event) {

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload(hdrId);
			}


		private void PageReload(String hdrId) {

		}

}
