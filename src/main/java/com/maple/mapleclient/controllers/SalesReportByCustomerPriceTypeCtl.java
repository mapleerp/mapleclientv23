package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.BranchSaleReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SalesReportByCustomerPriceTypeCtl {
	
	String taskid;
	String processInstanceId;
	
	private ObservableList<DailySalesReportDtl> saleReportListTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleDtlReportListTable = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private ComboBox<String> cmbPriceType;

    @FXML
    private Button btnGenarateReport;

    @FXML
    private TableView<DailySalesReportDtl> tblSalesSummary;

    @FXML
    private TableColumn<DailySalesReportDtl, LocalDate> clVoucherDate;

    @FXML
    private TableColumn<DailySalesReportDtl, String> clVoucherNumber;

    @FXML
    private TableColumn<DailySalesReportDtl, String> clCustomer;
    
    @FXML
    private TableColumn<DailySalesReportDtl, Number> clInvoiceAmount;

  

    @FXML
    private TableView<SalesDtl> tblSalesDetails;

    @FXML
    private TableColumn<SalesDtl, String> clItemName;

    @FXML
    private TableColumn<SalesDtl, String> clQty;

    @FXML
    private TableColumn<SalesDtl, String> clUnit;

    @FXML
    private TableColumn<SalesDtl, String> clMrp;
    
    @FXML
    private TableColumn<SalesDtl, Number> clAmount;

    @FXML
    private TextField txtTotal;
    
    @FXML
	private void initialize() {
    	cmbPriceDefenition();
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblSalesSummary.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
    		if (newSelection != null) {
    			System.out.println("getSelectionModel");
    			if (null != newSelection.getVoucherNumber()) {
    				
    				SalesTransHdr salesTransHdr = RestCaller.
    						getSalesTransHdrByVoucherAndDate(newSelection.getVoucherNumber(), 
    								SystemSetting.UtilDateToString(newSelection.getvoucherDate(), "yyyy-MM-dd"));
    			
    				ResponseEntity<List<SalesDtl>> salesDtlResp = RestCaller.getSalesDtl(salesTransHdr);
    				saleDtlReportListTable  = FXCollections.observableArrayList(salesDtlResp.getBody());
    		    	FillDtlTable();
    			}
    		}
    	});
    }

    private void FillDtlTable() {

    	tblSalesDetails.setItems(saleDtlReportListTable);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	}

	@FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == cmbPriceType.getValue())
    	{
    		notifyMessage(3, "Please select Price Type", false);
    		return;
    	}
    	
    	if(null == dpFromDate.getValue())
    	{
    		notifyMessage(3, "Please select From Date", false);
    		return;
    	}
    	
    	if(null == dpToDate.getValue())
    	{
    		notifyMessage(3, "Please select From Date", false);
    		return;
    	}
    	
    	String priceTypeId = null;
    	
    	java.util.Date fDate = Date.valueOf(dpFromDate.getValue());
		String strDate = SystemSetting.UtilDateToString(fDate, "yyyy-MM-dd");
		
		java.util.Date tDate = Date.valueOf(dpFromDate.getValue());
		String toDate = SystemSetting.UtilDateToString(tDate, "yyyy-MM-dd");
		
		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
				.getPriceDefenitionByName(cmbPriceType.getSelectionModel().getSelectedItem().toString());

		if(priceDefenitionSaved.getBody().size()>0)
		{
			priceTypeId = priceDefenitionSaved.getBody().get(0).getId();
		}
		if(null == priceTypeId)
		{
			notifyMessage(3, "Price Type Not Available", false);
    		return;
		}
		
		ResponseEntity<List<DailySalesReportDtl>> dailySalesReportDtlResp = RestCaller.
				SalesReportByPriceType(strDate,toDate,priceTypeId);
		
		saleReportListTable = FXCollections.observableArrayList(dailySalesReportDtlResp.getBody());

    }
    
	private void cmbPriceDefenition() {

		cmbPriceType.getItems().clear();

		System.out.println("=====PriceTypeOnKEyPress===");
		ArrayList priceType = new ArrayList();

		priceType = RestCaller.getPriceDefinition();
		Iterator itr = priceType.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object priceLevelName = lm.get("priceLevelName");
			Object id = lm.get("id");
			if (id != null) {
				cmbPriceType.getItems().add((String) priceLevelName);

			}

		}

	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload(hdrId);
			}


	private void PageReload(String hdrId) {
		
	}

}
