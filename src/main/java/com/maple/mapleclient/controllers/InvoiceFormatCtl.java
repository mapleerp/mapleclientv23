package com.maple.mapleclient.controllers;

import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.entity.InvoiceFormatMst;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class InvoiceFormatCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<InvoiceFormatMst> reportList = FXCollections.observableArrayList();

	InvoiceFormatMst invoiceFormatMst=null;
    @FXML
    private ComboBox<String> cmbJasper;

    @FXML
    private ComboBox<String> cmbGst;

    @FXML
    private ComboBox<String> cmbDiscount;

    @FXML
    private ComboBox<String> cmbBatch;
    @FXML
    private ComboBox<String> cmbPriceType;
    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;

    @FXML
    private TableView<InvoiceFormatMst> tbnvoiceFormat;

    @FXML
    private TableColumn<InvoiceFormatMst,String> clJasperName;

    @FXML
    private TableColumn<InvoiceFormatMst,String> cGst;

    @FXML
    private TableColumn<InvoiceFormatMst,String> clDiscount;

    @FXML
    private TableColumn<InvoiceFormatMst,String> clBatch;
    

    @FXML
    private TableColumn<InvoiceFormatMst,String> clPriceType;
    @FXML
	private void initialize() {
    	cmbBatch.getItems().add("YES");
    	cmbBatch.getItems().add("NO");
    	cmbDiscount.getItems().add("YES");
    	cmbDiscount.getItems().add("NO");
    	cmbGst.getItems().add("YES");
    	cmbGst.getItems().add("NO");
    	cmbJasper.getItems().add("discountTaxInvoice");
    	cmbJasper.getItems().add("taxinvoicee");
    	cmbJasper.getItems().add("TaxinvoiceeWithbatch");
    	cmbJasper.getItems().add("HWTaxInvoice");
    	cmbJasper.getItems().add("HWTaxInvoiceWithOutMrp");
    	cmbJasper.getItems().add("HWTaxInvoiceWithGst");
    	cmbJasper.getItems().add("PharmacyWholeSaleInvoice");

    	cmbPriceType.getItems().add("yale_id");
    }
    @FXML
    void actionDelete(ActionEvent event) {

    }

    @FXML
    void actionSave(ActionEvent event) {
    	invoiceFormatMst = new InvoiceFormatMst();
    	invoiceFormatMst.setBatch(cmbBatch.getSelectionModel().getSelectedItem());
    	invoiceFormatMst.setDiscount(cmbDiscount.getSelectionModel().getSelectedItem());
    	invoiceFormatMst.setGst(cmbGst.getSelectionModel().getSelectedItem());
    	invoiceFormatMst.setJasperName(cmbJasper.getSelectionModel().getSelectedItem());
    	if(null != cmbPriceType.getSelectionModel().getSelectedItem())
    	{
    	invoiceFormatMst.setPriceTypeId(cmbPriceType.getSelectionModel().getSelectedItem());
    	}
    	ResponseEntity<InvoiceFormatMst> respentity = RestCaller.saveinvoiceFormatMst(invoiceFormatMst);
    	invoiceFormatMst = respentity.getBody();
    	reportList.add(invoiceFormatMst);
    	fillTable();
    	clearFields();
    }
    private void clearFields()
    {
    	cmbBatch.getSelectionModel().clearSelection();
    	cmbDiscount.getSelectionModel().clearSelection();
    	cmbGst.getSelectionModel().clearSelection();
    	cmbJasper.getSelectionModel().clearSelection();
    	cmbPriceType.getSelectionModel().clearSelection();

    }
    private void fillTable()
    {
    	for(InvoiceFormatMst inv : reportList)
    	{
    		inv.setPriceName(inv.getPriceName());
    	}
    	tbnvoiceFormat.setItems(reportList);
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		clDiscount.setCellValueFactory(cellData -> cellData.getValue().getDiscountProperty());
		clJasperName.setCellValueFactory(cellData -> cellData.getValue().getJasperNameProperty());
		cGst.setCellValueFactory(cellData -> cellData.getValue().getGstProperty());
		clPriceType.setCellValueFactory(cellData -> cellData.getValue().getPriceTypeProperty());


    }

    @FXML
    void actionShowAll(ActionEvent event) {

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }



}
