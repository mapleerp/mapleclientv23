package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CompanyMst;
import com.maple.mapleclient.entity.CurrencyConversionMst;
import com.maple.mapleclient.entity.CurrencyMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class CompanyMstCtl {
	
	
	@Value("${mycompany}")
	private String mycompany;
	
	
	String taskid;
	String processInstanceId;
	Boolean status = false;
	CompanyMst companyMst= null; 
	
	CurrencyConversionMst currencyConversionMst = null;
	
    @FXML
    private TextField txtCompanyName;
    @FXML
    private ComboBox<String> cmbCountry;
    @FXML
    private TextField txtCompanyGst;

    @FXML
    private ComboBox<String> cmbState;

    @FXML
    private ComboBox<String> cmbCurrency;
    @FXML
    private Button btnSave;
    
    @FXML
    private Button btnInstallation;

    
    @FXML
    private TextField txtUrlForInstallation;
    
   
    
    @FXML
	private void initialize() {
    	
    	ResponseEntity<List<String>> countryList = RestCaller.getCountryList();
    	for(int i=0;i<countryList.getBody().size();i++)
    	{
    		cmbCountry.getItems().add(countryList.getBody().get(i));
    	}
    	cmbCurrency.getItems().add("DOLLAR");
    	cmbCurrency.getItems().add("INR");
    	cmbCurrency.getItems().add("MVR");
    	

//    	txtUrlForInstallation.setText(SystemSetting.HOST2 + "/installationmodelclassresource/fileinstallation");
    	
    	cmbCountry.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				cmbState.getItems().clear();
				if(!newValue.equalsIgnoreCase(null))
				{
					ResponseEntity<List<String>> stateList = RestCaller.getStateListByCountry(cmbCountry.getSelectionModel().getSelectedItem());
			    	for(int i = 0;i<stateList.getBody().size();i++)
			    	{
			    		cmbState.getItems().add(stateList.getBody().get(i));
			    	}
				}
			}

		});
    	
//    	cmbState.getItems().add("KERALA");
//    	cmbState.getItems().add("ANDHRA PRADESH");
//    	cmbState.getItems().add("ARUNACHAL PRADESH");
//    	cmbState.getItems().add("ASSAM");
//    	cmbState.getItems().add("BIHAR");
//    	cmbState.getItems().add("CHHATTISGARH");
//		cmbState.getItems().add("GOA");
//		cmbState.getItems().add("GUJARAT");
//		cmbState.getItems().add("HARYANA");
//		cmbState.getItems().add("HIMACHAL PRADESH");
//		cmbState.getItems().add("JAMMU AND  KASHMIR");
//		cmbState.getItems().add("JHARKHAND");
//		cmbState.getItems().add("KARNATAKA");
//		cmbState.getItems().add("MADHYA PRADESH");
//		cmbState.getItems().add("MAHARASHTRA");
//		cmbState.getItems().add("MANIPUR");
//		cmbState.getItems().add("MEGHALAYA");
//		cmbState.getItems().add("MIZORAM");
//		cmbState.getItems().add("NAGALAND");
//		cmbState.getItems().add("ODISHA");
//		cmbState.getItems().add("PUNJAB");
//		cmbState.getItems().add("RAJASTHAN");
//		cmbState.getItems().add("TAMIL NADU");
//		cmbState.getItems().add("TELANGANA");
//		cmbState.getItems().add("TRIPURA");
//		cmbState.getItems().add("UTTAR PRADESH");
//		cmbState.getItems().add("UTTARAKHAND");
//		cmbState.getItems().add("WEST BENGAL");

    }
    @FXML
    void installationfile(ActionEvent event) {
    	
    	if(	null==companyMst.getId()){
    		return;	
    		}
    	
    	String url = txtUrlForInstallation.getText();
    	if(status == true) {
    		
    	    String file=RestCaller.installationFilecreation();
    		
    		String  resp= RestCaller.installationModelClass();
    		if(!resp.isEmpty()|| !file.isEmpty()) {
    			notifyMessage(3, "Installation Succesfully Completed");
    		}
    	}else {
    		notifyMessage(3, "Create company first");
    	}
    	
    	
    }
    @FXML
    void SaveAction(ActionEvent event) {

    	companyMst = new CompanyMst();
    	companyMst.setCompanyGst(txtCompanyGst.getText());
    	companyMst.setCompanyName(txtCompanyName.getText());
    	companyMst.setState(cmbState.getSelectionModel().getSelectedItem().toString());
    	companyMst.setCurrencyName(cmbCurrency.getSelectionModel().getSelectedItem());
    	companyMst.setCountry(cmbCountry.getSelectionModel().getSelectedItem());
    	ResponseEntity<CompanyMst> respentity = RestCaller.saveCompanyMst(companyMst);
    	
    	//=======================save to currency conversion entity=================
    	
//    	currencyConversionMst = new CurrencyConversionMst();
//    	ResponseEntity<CurrencyMst> currencyMstResp = RestCaller.getCurrencyMstByName(cmbCurrency.getSelectionModel().getSelectedItem().toString());
//    	CurrencyMst currencyMst = currencyMstResp.getBody();
//    	currencyConversionMst.setBranchCode(SystemSetting.getUser().getBranchCode());
//    	currencyConversionMst.setConversionRate(1.0);
//    	if(null == currencyMst)
//    	{
//    		notifyMessage(3, "Currency Type is not present");
//    		cmbCurrency.requestFocus();
//    		return;
//    	}
//    	
//    	currencyConversionMst.setCurrencyId(currencyMst.getId());
//    	ResponseEntity<CurrencyConversionMst> currencyConversionMstResp = RestCaller.saveCurrencyConversionMst(currencyConversionMst);
//    	if(null != currencyConversionMstResp)
//    	{
//    		notifyMessage(3, "Success");
//    		cmbCurrency.requestFocus();
//    		return;
//    	}
    	//=======================end=========================
    	
    	
    	
    	companyMst = respentity.getBody();
    	notifyMessage(5,"Company Details Saved!!!");
    	if(null != companyMst) {
    		status = true;
    	}
    	txtCompanyGst.clear();
    	txtCompanyName.clear();
    	
    }
    
    public void notifyMessage(int duration, String msg) {
				Image img = new Image("done.png");
				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();

				notificationBuilder.show();
			}
    @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


     private void PageReload() {
     	
}
}
