package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;
import org.springframework.http.ResponseEntity;

import com.ibm.icu.math.BigDecimal;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.VoucherReprintDtl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

public class UserWiseSalesReportCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<DailySalesReportDtl> dailySalesList = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();

	@FXML
	private ComboBox<String> cmbUser;

	@FXML
	private DatePicker dpFromDate;

	@FXML
	private DatePicker dpToDate;

	@FXML
	private Button BntGenarateReport;

	@FXML
	private Button btnPrint;

	@FXML
	private TableView<DailySalesReportDtl> tbReport;

	@FXML
	private TableColumn<DailySalesReportDtl, String> clVoucherNo;

	@FXML
	private TableColumn<DailySalesReportDtl, LocalDate> clDate;

	@FXML
	private TableColumn<DailySalesReportDtl, String> clCustomer;

	@FXML
	private TableColumn<DailySalesReportDtl, String> clreceiptmode;

	@FXML
	private TableColumn<DailySalesReportDtl, Number> clamt;

	@FXML
	private TextField txtGrandTotal;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnRate;
	@FXML
	private TableColumn<SalesDtl, String> columnMrp;
	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private Button btnReceiptSummary;

	@FXML
	private void initialize() {
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
		ResponseEntity<List<UserMst>> userMstListResp = RestCaller.getAllusers();
		List<UserMst> userList = userMstListResp.getBody();

		for (UserMst userMst : userList) {
			cmbUser.getItems().add(userMst.getUserName());
		}

		tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {

					salesDtlList(newSelection.getVoucherNumber(), newSelection.getvoucherDate());

				}
			}
		});
	}

	private void salesDtlList(String voucherNumber, Date voucherDate) {

		String date = SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");
		SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNumber, date);

		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesTransHdr);

		saleListTable = FXCollections.observableArrayList(respentityList.getBody());
		FillTableSalesDtl();

	}

	private void FillTableSalesDtl() {

		itemDetailTable.setItems(saleListTable);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	}

	@FXML
	void GenarateReport(ActionEvent event) {

		if (null == cmbUser.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Please select user...!", false);
			cmbUser.requestFocus();
			return;
		}
		if (null == dpFromDate.getValue()) {
			notifyMessage(3, "Please select from date...!", false);
			dpFromDate.requestFocus();
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(3, "Please select to date...!", false);
			dpToDate.requestFocus();
			return;
		}

		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");

		String username = cmbUser.getSelectionModel().getSelectedItem().toString();

		ResponseEntity<UserMst> userMstResp = RestCaller.getUserByName(username);
		UserMst userMst = userMstResp.getBody();

		String branchCode =SystemSetting.systemBranch;

		ResponseEntity<List<DailySalesReportDtl>> dailysales = RestCaller.getUserWiseSaleReportSummary(branchCode,
				userMst.getId(), strDate, endDate);
		dailySalesList = FXCollections.observableArrayList(dailysales.getBody());

		FillTable();
		itemDetailTable.getItems().clear();

	}

	private void FillTable() {

		tbReport.setItems(dailySalesList);
		clamt.setCellValueFactory(cellData -> cellData.getValue().getamountProperty());
		clCustomer.setCellValueFactory(cellData -> cellData.getValue().getcustomerNameProperty());
		clreceiptmode.setCellValueFactory(cellData -> cellData.getValue().getreceiptModeProperty());

		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());

		Double totalamount = 0.0;

		for (DailySalesReportDtl dailySaleReportDtl : dailySalesList) {
			if (null != dailySaleReportDtl.getAmount())
				totalamount = totalamount + dailySaleReportDtl.getAmount();
		}

		BigDecimal settoamount = new BigDecimal(totalamount);
		settoamount = settoamount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtGrandTotal.setText(settoamount.toString());

	}

	@FXML
	void PrintReport(ActionEvent event) {

		if (null == cmbUser.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Please select user...!", false);
			cmbUser.requestFocus();
			return;
		}
		if (null == dpFromDate.getValue()) {
			notifyMessage(3, "Please select from date...!", false);
			dpFromDate.requestFocus();
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(3, "Please select to date...!", false);
			dpToDate.requestFocus();
			return;
		}

		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");

		String username = cmbUser.getSelectionModel().getSelectedItem().toString();

		ResponseEntity<UserMst> userMstResp = RestCaller.getUserByName(username);
		UserMst userMst = userMstResp.getBody();

		String branchCode =SystemSetting.systemBranch;

		try {
			JasperPdfReportService.UserWiseSalesReport(strDate, endDate, userMst, branchCode);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void PrintReceiptSummary(ActionEvent event) {

		if (null == cmbUser.getSelectionModel().getSelectedItem()) {
			notifyMessage(3, "Please select user...!", false);
			cmbUser.requestFocus();
			return;
		}
		if (null == dpFromDate.getValue()) {
			notifyMessage(3, "Please select from date...!", false);
			dpFromDate.requestFocus();
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(3, "Please select to date...!", false);
			dpToDate.requestFocus();
			return;
		}

		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");

		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");

		String username = cmbUser.getSelectionModel().getSelectedItem().toString();

		ResponseEntity<UserMst> userMstResp = RestCaller.getUserByName(username);
		UserMst userMst = userMstResp.getBody();

		String branchCode =SystemSetting.systemBranch;
		

		try {
			JasperPdfReportService.UserWiseSalesReceiptSummaryReport(strDate, endDate, userMst, branchCode);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}
