package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;

import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class DailyCashFromOrderReportCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<SalesOrderTransHdr> salesorderlist = FXCollections.observableArrayList();
	SalesOrderTransHdr salesOrderTransHdr =new SalesOrderTransHdr();
	String cashDate= null;
    @FXML
    private TableView<SalesOrderTransHdr> tbcashFrmOrder;
    @FXML
    private TableColumn<SalesOrderTransHdr, String> clName;

    @FXML
    private TableColumn<SalesOrderTransHdr, Number> clAmount;

    @FXML
    private TextField txtTotal;
    
    @FXML
	private void initialize() {
    	
    	
    	
    	
    }
    @FXML
    private Button btnShow;

    @FXML
    void showRp(ActionEvent event) {
    	salesorderlist.add(salesOrderTransHdr);
    	
    	clAmount.setCellValueFactory(cellData -> cellData.getValue().getpaidAmtProperty());
    	clName.setCellValueFactory(cellData -> cellData.getValue().getCustomerNameProperty());
    	 cashFromOrder(cashDate);

    }
    private void cashFromOrder(String orderDate)
    {

    	//ResponseEntity<List<SalesOrderTransHdr>> salesTransferhdrSaved = RestCaller.getSalesOrderByDate();
		//salesorderlist = FXCollections.observableArrayList(salesTransferhdrSaved.getBody());
		tbcashFrmOrder.setItems(salesorderlist);


    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
