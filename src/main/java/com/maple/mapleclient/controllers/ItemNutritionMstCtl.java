package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemNutritionMst;
import com.maple.mapleclient.entity.NutritionMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

@RestController
public class ItemNutritionMstCtl {

	private ObservableList<NutritionMst> nutritionList = FXCollections.observableArrayList();
	private ObservableList<ItemNutritionMst> itemNutritionsTable = FXCollections.observableArrayList();

	private EventBus eventBus = EventBusFactory.getEventBus();

	String itemId;
	ItemNutritionMst itemNutritionMsts;
	@FXML
	private Button btnAddItem;

	@FXML
	private Button btnDeleteItem;

	@FXML
	private TextField txtvalue;

	@FXML
	private TextField txtItemName;

	@FXML
	private Button btnShow;

	@FXML
	private ComboBox<String> cmbNutritionFact;

	@FXML
	private TableView<ItemNutritionMst> tblItemNutritionDetails;

	@FXML
	private TableColumn<ItemNutritionMst, String> clItemName;

	@FXML
	private TableColumn<ItemNutritionMst, String> clValue;

	@FXML
	private TableColumn<ItemNutritionMst, String> clNutritionFactId;

	@FXML
	void AddItem(ActionEvent event) {
		save();
	}

	private void save() {
		ItemNutritionMst itemNutritionMsts = new ItemNutritionMst();
		if (null == txtItemName.getText()) {
			notifyMessage(5, "Please select Item name", false);
			return;
		}

		if (txtvalue.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select value", false);
			return;
		}
		if (null == cmbNutritionFact.getValue())

		{

			notifyMessage(5, "Please select  Nutrition facts", false);
			cmbNutritionFact.requestFocus();
			return;
		}
		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		ItemMst itemMst = getItem.getBody();
		if (null == itemMst) {
			notifyMessage(5, "Item not found", false);
			return;
		}
		itemNutritionMsts.setItemId(itemMst.getId());

		itemNutritionMsts.setValue(txtvalue.getText());

		itemNutritionMsts.setNutritionFact(cmbNutritionFact.getSelectionModel().getSelectedItem());

		ResponseEntity<ItemNutritionMst> respEntity = RestCaller.createItemNutritions(itemNutritionMsts);

		{
			notifyMessage(5, "Saved ", true);
			txtItemName.clear();
			txtvalue.clear();
			cmbNutritionFact.setValue(null);

			showAll();
			return;
		}

	}

	@FXML
	void DeleteItem(ActionEvent event) {
		if (null != itemNutritionMsts) {

			if (null != itemNutritionMsts.getId()) {
				RestCaller.deleteItemNutritions(itemNutritionMsts.getId());

				showAll();
				notifyMessage(5, " Item deleted", false);
				return;
			}
		}
	}

	@FXML
	void ItemNameOnkeyPressed(KeyEvent event) {
		try {
			System.out.println("STOCK POPUP");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));

			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));

			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void SaveOnKey(KeyEvent event) {

	}

	@FXML
	void ShowAll(ActionEvent event) {
		showAll();
	}

	private void showAll() {
		ResponseEntity<List<ItemNutritionMst>> respentity = RestCaller.getItemNutritions();
		itemNutritionsTable = FXCollections.observableArrayList(respentity.getBody());

		System.out.print(itemNutritionsTable.size() + "observable list size issssssssssssssssssssss");
		fillTable();

	}

	private void fillTable() {
		tblItemNutritionDetails.setItems(itemNutritionsTable);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());

		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());

		clNutritionFactId.setCellValueFactory(cellData -> cellData.getValue().getNutritionFactProperty());

	}

	@FXML
	void clearItems(ActionEvent event) {
		txtItemName.clear();
		txtvalue.clear();
		tblItemNutritionDetails.getItems().clear();
	}

	@FXML
	private void initialize() {

		eventBus.register(this);
		ResponseEntity<List<NutritionMst>> savedNutritions = RestCaller.getAllNutritions();
		nutritionList = FXCollections.observableArrayList(savedNutritions.getBody());
		for (NutritionMst nutrtionMst : nutritionList) {
			cmbNutritionFact.getItems().add(nutrtionMst.getNutrition());
		}

		tblItemNutritionDetails.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						if (null != newSelection.getId()) {

							itemNutritionMsts = new ItemNutritionMst();
							itemNutritionMsts.setId(newSelection.getId());
						}
					}
				});
	}

	@FXML
	void ShowItemPopup(MouseEvent event) {

	}

	@FXML
	void addItem(KeyEvent event) {

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	@Subscribe
	public void popupOrglistner(ItemPopupEvent itemPopupEvent) {

		Stage stage = (Stage) btnAddItem.getScene().getWindow();
		if (stage.isShowing()) {

			Platform.runLater(() -> {
				txtItemName.setText(itemPopupEvent.getItemName());
				itemId = itemPopupEvent.getItemId();
			});

		}
	}

	@FXML
	void nutritionOnEnter(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtvalue.requestFocus();
		}
	}

	@FXML
	void OnActionNutritionFact(ActionEvent event) {

	}

}
