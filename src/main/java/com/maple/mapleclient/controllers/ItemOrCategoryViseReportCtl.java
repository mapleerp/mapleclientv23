package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.CategoryWiseSalesReport;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemWiseDtlReport;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class ItemOrCategoryViseReportCtl {
	String taskid;
	String processInstanceId;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ItemWiseDtlReport> itemSalesList = FXCollections.observableArrayList();

String itemId = null;
String categoryId = null;
    @FXML
    private TextField txtItem;
    @FXML
    private TextField txtTotalAmount;
    @FXML
    private ComboBox<String> cmbCategory;
    @FXML
    private DatePicker dpFromDate;
    @FXML
    private Button btnClear;
    @FXML
    private Button btnPrintReport;
    @FXML
    private DatePicker dpToDate;
    @FXML
    private TableView<ItemWiseDtlReport> tblReport;

    @FXML
    private TableColumn<ItemWiseDtlReport, String> clItemName;

    @FXML
    private TableColumn<ItemWiseDtlReport, Number> clQty;
    @FXML
    private TableColumn<ItemWiseDtlReport, LocalDate> clDate;
    

    @FXML
    private TableColumn<ItemWiseDtlReport, Number> clValue;
    @FXML
    private Button btnGenerateReport;
   
    @FXML
    private Button btnCatDtlPrint;

    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	Date fromdate = Date.valueOf(dpFromDate.getValue());
    	Date toDate = Date.valueOf(dpToDate.getValue());
    	
    	Format formatter;
		
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(fromdate);
		String endDate = formatter.format(toDate);
//		if(!txtItem.getText().trim().isEmpty() && !cmbCategory.getSelectionModel().isEmpty())
//		{
//			notifyMessage(5,"Select any One");
//			return;
//		}
//		else
//		{
			String item = txtItem.getText();
//		if(null != strDate && null != endDate && item.isEmpty() && cmbCategory.getSelectionModel().isEmpty())
//		{
//			ResponseEntity<List<ItemWiseDtlReport>> itemListSaved = RestCaller.getMonthSales(SystemSetting.systemBranch,strDate,endDate);
//			itemSalesList = FXCollections.observableArrayList(itemListSaved.getBody());
//		}
		 if(!cmbCategory.getSelectionModel().isEmpty())
		{
			 txtItem.clear();
			ResponseEntity<CategoryMst>category = RestCaller.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem());
			ResponseEntity<List<ItemWiseDtlReport>> itemListSaved = RestCaller.getCategorybytwoDates(SystemSetting.systemBranch,strDate,endDate,category.getBody().getId());
			itemSalesList = FXCollections.observableArrayList(itemListSaved.getBody());
		}
		else if(!txtItem.getText().trim().isEmpty())
		{
			cmbCategory.getSelectionModel().clearSelection();
			ResponseEntity<ItemMst> itemsaved = RestCaller.getItemByNameRequestParam(txtItem.getText());
			
			ResponseEntity<List<ItemWiseDtlReport>> itemListSaved = RestCaller.getItembytwoDates(SystemSetting.systemBranch,strDate,endDate,itemsaved.getBody().getId());
		
		itemSalesList = FXCollections.observableArrayList(itemListSaved.getBody());
		}
//		}
		 double total = 0.0;
		 for(ItemWiseDtlReport itemssaleReport : itemSalesList)
		 {
			 total = total + itemssaleReport.getValue();
			 BigDecimal value = new BigDecimal(total);
			 value = value.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			 total = value.doubleValue();
		 }
		 txtTotalAmount.setText(Double.toString(total));
		 tblReport.setItems(itemSalesList);
		 for(ItemWiseDtlReport itemDtlR : itemSalesList)
		 {
			 BigDecimal value = new BigDecimal(itemDtlR.getValue());
			 value = value.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			 itemDtlR.setValue(value.doubleValue());
		 }
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getValueProperty());
		/*
		 * if(!txtItem.getText().trim().isEmpty()) { if(null != itemId) { //======jasper
		 * service caller by itemId System.out.println("itemIditemId"+itemId); try {
		 * JasperPdfReportService.ItemSalesReport(itemId,strDate); } catch (JRException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); } } } else if
		 * (null != cmbCategory.getValue()) {
		 * 
		 * ResponseEntity<CategoryMst> categoryMstRep =
		 * RestCaller.getCategoryByName(cmbCategory.getValue());
		 * 
		 * CategoryMst categoryMst= categoryMstRep.getBody();
		 * 
		 * //======jasper service caller by categoryId
		 * 
		 * 
		 * System.out.println("categoryIdcategoryId"+categoryId); try {
		 * JasperPdfReportService.CategorySaleReport(categoryMst.getId(),strDate); }
		 * catch (JRException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } }
		 */
    

    }
    @FXML
    void PrintReport(ActionEvent event) {

    	Date fromdate = Date.valueOf(dpFromDate.getValue());
    	Date toDate = Date.valueOf(dpToDate.getValue());
    	
    	Format formatter;
		
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(fromdate);
		String endDate = formatter.format(toDate);
//    	if(!txtItem.getText().trim().isEmpty() && !cmbCategory.getSelectionModel().isEmpty())
//		{
//			notifyMessage(5,"Select any One");
//			return;
//		}
//		else
//		{
//			String item = txtItem.getText();
//		if(null != strDate && null != endDate && item.isEmpty() && cmbCategory.getSelectionModel().isEmpty())
//		{
//			ResponseEntity<List<ItemWiseDtlReport>> itemListSaved = RestCaller.getMonthSales(SystemSetting.systemBranch,strDate,endDate);
//			itemSalesList = FXCollections.observableArrayList(itemListSaved.getBody());
//			
//			try {
//				JasperPdfReportService.DateBetweenSalesReport(strDate,endDate,SystemSetting.systemBranch);
//			} catch (JRException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//	    	
//			
//		}
		if(!cmbCategory.getSelectionModel().isEmpty())
		{
			txtItem.clear();
			ResponseEntity<CategoryMst>category = RestCaller.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem());
			try {
				JasperPdfReportService.CategoryWiseSalesReport(strDate,endDate,SystemSetting.systemBranch,category.getBody().getId());
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(!txtItem.getText().trim().isEmpty())
		{
			cmbCategory.getSelectionModel().clearSelection();
			ResponseEntity<ItemMst> itemsaved = RestCaller.getItemByNameRequestParam(txtItem.getText());
			
			try {
				JasperPdfReportService.ItemWiseSalesReport(strDate,endDate,SystemSetting.systemBranch,itemsaved.getBody().getId());
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}
    	
   

    @FXML
    void SetCategory(MouseEvent event) {
    	
    
    	
    	cmbCategory.getItems().clear();
    	ArrayList cat = new ArrayList();
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				cmbCategory.getItems().add((String) categoryName);
			}
		}

    }

    @FXML
    void clearAction(ActionEvent event) {

    	cmbCategory.getSelectionModel().clearSelection();
    	txtItem.clear();
    	tblReport.getItems().clear();
    }
    
	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnGenerateReport.getScene().getWindow();
		if (stage.isShowing()) {
			
			txtItem.setText(itemPopupEvent.getItemName());
			cmbCategory.getSelectionModel().clearSelection();
			itemId = itemPopupEvent.getItemId();

		}
	}
	

    @FXML
    void ShowItemPopup(MouseEvent event) {
    	
    	try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			btnGenerateReport.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}


    }
    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @FXML
	private void initialize() {
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	eventBus.register(this);
		
	}
    @FXML
    void printCatDtl(ActionEvent event) {

    	if(null==dpFromDate.getValue()||null==dpToDate.getValue()) {
    		notifyMessage(5,"Select proper Date");
			return;
    	}
    	
    	Date fromdate = Date.valueOf(dpFromDate.getValue());
    	Date toDate = Date.valueOf(dpToDate.getValue());
    	
    	Format formatter;
		
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = formatter.format(fromdate);
		String endDate = formatter.format(toDate);
			try {
				JasperPdfReportService.CategoryWiseSalesDtlReport(strDate,endDate,SystemSetting.systemBranch);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }
	
    
    
}
