package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.StockTransferInExceptionDtl;
import com.maple.mapleclient.entity.StockTransferInExceptionHdr;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;


@RestController
public class StockTransferInExceptionReportCtl {
	private ObservableList<StockTransferInExceptionHdr> stockTransferInExceptionList = FXCollections.observableArrayList();
    
	private ObservableList<StockTransferInExceptionDtl> stockTransferInExceptionDtlLists = FXCollections.observableArrayList();
	
	
	@FXML
    private DatePicker dpdate;

    @FXML
    private ComboBox<String> branchselect;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TableView<StockTransferInExceptionHdr> tbReport;

    @FXML
    private TableColumn<StockTransferInExceptionHdr, String> clVoucherNo;

    @FXML
    private TableColumn<StockTransferInExceptionHdr, String> clDate;

    @FXML
    private TableColumn<StockTransferInExceptionHdr, String> clBranch;

    @FXML
    private TableColumn<StockTransferInExceptionHdr, String> clIntentNumber;

    @FXML
    private TableColumn<StockTransferInExceptionHdr, String> clReason;

    @FXML
    private TableView<StockTransferInExceptionDtl> tbreports;

    @FXML
    private TableColumn<StockTransferInExceptionDtl, ?> Customername;

    @FXML
    private TableColumn<StockTransferInExceptionDtl, String> clitemName;

    @FXML
    private TableColumn<StockTransferInExceptionDtl, Double> clqty;

    @FXML
    private TableColumn<StockTransferInExceptionDtl, String> clbatch;

    @FXML
    private TableColumn<StockTransferInExceptionDtl, String> clRate;

    @FXML
    private TableColumn<StockTransferInExceptionDtl, String> clitem;

    @FXML
    private TableColumn<StockTransferInExceptionDtl, String> unit;

    @FXML
    private TableColumn<StockTransferInExceptionDtl,String> amount;

    @FXML
    private TableColumn<StockTransferInExceptionDtl,String> clReason1;

    @FXML
    private TextField txtGrandTotal;

    @FXML
    private Button btnShow;

    @FXML
    void show(ActionEvent event) {
    	showReport() ;
    	
    }
    
    
    @FXML
    public void initialize() {
    	
    	dpdate = SystemSetting.datePickerFormat(dpdate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	
    	tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {

					String voucherNumber = newSelection.getVoucherNumber();
					
					java.util.Date  voucherDate=newSelection.getInVoucherDate();
					String vDate=SystemSetting.UtilDateToString(voucherDate);
					ResponseEntity<List<StockTransferInExceptionDtl>> dailysaless = RestCaller
							.getExceptionReportDtl(newSelection.getVoucherNumber(), vDate);
					stockTransferInExceptionDtlLists = FXCollections.observableArrayList(dailysaless.getBody());
					tbreports.setItems(stockTransferInExceptionDtlLists);
					FillTables();
				}
			}
		});

    	
    	
    	
    	
    	setBranches();
    }


	private void setBranches() {

		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();

		for (BranchMst branchMst : branchMstList) {
			branchselect.getItems().add(branchMst.getBranchCode());

		}

	}
	public void showReport() {
		
		if(null==dpToDate.getValue()||null==dpdate.getValue()) {
			notifyMessage(5, "Please select Date", false);
			return;
		}
		if(null==branchselect.getSelectionModel().getSelectedItem()) {
			notifyMessage(5, "Please select Branch", false);
			return;
		}
		
		java.util.Date uDate = Date.valueOf(dpdate.getValue());
		String strDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
		String strDate1 = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
		
		ResponseEntity<List<StockTransferInExceptionHdr>> stockTransferInExceptionReportList=RestCaller
		.getStockTransferInExceptionReportHdr(branchselect.getSelectionModel().getSelectedItem(), strDate, strDate1);
		

		stockTransferInExceptionList=FXCollections.observableArrayList(stockTransferInExceptionReportList.getBody());
	
		tbReport.setItems(stockTransferInExceptionList); 
		FillTables();
	}

	
	private void FillTable() {

		clIntentNumber.setCellValueFactory(cellData -> cellData.getValue().getIntentVoucherNumberProperty());
	
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clBranch.setCellValueFactory(cellData -> cellData.getValue().getFromBranchProperty());
		clReason.setCellValueFactory(cellData -> cellData.getValue().getReasonProperty());
		clDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		
	   
		
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	

	private void FillTables() {

	
		clitemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		 clbatch.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		 clRate.setCellValueFactory(cellData -> cellData.getValue().getRatesProperty());
		 clitem.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		 unit.setCellValueFactory(cellData -> cellData.getValue().getUnitidProperty());
		 amount.setCellValueFactory(cellData -> cellData.getValue().getAmountsProperty());
		 clReason1.setCellValueFactory(cellData -> cellData.getValue().getReasonProperty());
	   
		
	}
}


