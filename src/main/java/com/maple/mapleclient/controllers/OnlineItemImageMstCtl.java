package com.maple.mapleclient.controllers;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import javax.imageio.ImageIO;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ItemImageMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PhysicalStockHdr;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.events.VoucherNoEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.HostServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

public class OnlineItemImageMstCtl {
	String taskid;
	String processInstanceId;

	EventBus eventBus = EventBusFactory.getEventBus();

	FileChooser fileChooser = new FileChooser();

	@FXML
	private ScrollPane scrollpane;

	@FXML
	private FlowPane flowpane;

	@FXML
	private TextField txtItemName;
	@FXML
	private AnchorPane clFromItemName;

	@FXML
	private Button btnAdd;

	@FXML
	private Button reset;

	@FXML
	private Button viewitemimage;

	@FXML
	private TextField txtMrp;

	@FXML
	private TextField txtIngredients;
	@FXML
	private ComboBox<String> cmbApplicableTime;

	@FXML
	private ComboBox<String> cmbCategory;
	@FXML
	private TextField txtSpecialOffer;

	@FXML
	void OnMouseClicked(MouseEvent event) {

		selectAndSaveImage();

	}

	@FXML
	void addItems(ActionEvent event) {

	}

	@FXML
	void addOnEnter(KeyEvent event) {

	}

	@FXML
	void reset(ActionEvent event) {
		resetImage();

	}

	@FXML
	void loadPopUp(MouseEvent event) {
		showItemMst();
	}

	private void showItemMst() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			btnAdd.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupItemlistner(KitPopupEvent kitPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			// kitDefenitionmst = new KitDefinitionMst();
			txtItemName.setText(kitPopupEvent.getItemName());
			txtMrp.clear();
			txtIngredients.clear();
			txtSpecialOffer.clear();
			cmbApplicableTime.getSelectionModel().clearSelection();
		}
	}

	@FXML
	void saveOnKey(KeyEvent event) {
		selectAndSaveImage();
	}

	@FXML
	void vewItemImage(ActionEvent event) {
		viewImage();
	}

	@FXML
	private void initialize() {
		eventBus.register(this);

		cmbApplicableTime.getItems().add("BREAK FAST");
		cmbApplicableTime.getItems().add("LUNCH");
		cmbApplicableTime.getItems().add("DINNER");
		cmbApplicableTime.getItems().add("ALL");
		cmbCategory.getItems().add("ARABIC");
		cmbCategory.getItems().add("CHINESE");
		cmbCategory.getItems().add("ITALIAN");
		cmbCategory.getItems().add("NORTH INDIAN");
		cmbCategory.getItems().add("SOUTH INDIAN");

	}

	private void viewImage() 
	{
		if (!txtItemName.getText().trim().isEmpty()) {
			ResponseEntity<ItemMst> item = RestCaller.getItemByNameRequestParam(txtItemName.getText());

			ResponseEntity<List<ItemImageMst>> itemImageList = RestCaller.getimagebyitem(item.getBody().getId());

			List<ItemImageMst> itemImageMstList = itemImageList.getBody();

			Iterator iter = itemImageMstList.iterator();

			while (iter.hasNext()) {
				ItemImageMst itemImageMst = (ItemImageMst) iter.next();

//
////				byte[] data = Base64.getUrlDecoder().decode( itemImageMst.getItemImage());
//			 
//				byte[] data =   itemImageMst.getItemImage();
// 
//				
//				
//				//byte[] data = itemImageMst.getItemImage();
//				
//				if(null==data) {
//					continue;
//				}
//
//				ByteArrayInputStream bis = new ByteArrayInputStream(data);
//				int i = 0;
//				i = i + 1;
//				BufferedImage bImage2 = null;
//
//				try {
//					bImage2 = ImageIO.read(bis);
//
//					Image image = SwingFXUtils.toFXImage(bImage2, null);
//
//					// imageView.setImage(image);
//
//					Node imagenode = new ImageView();
//
//					imagenode.autosize();
//
//					ImageView img = (ImageView) imagenode;
//
//					img.setFitHeight(300);
//					img.setFitWidth(200);
//					img.setImage(image);
//
//					flowpane.getChildren().add(imagenode);
//					// txtItemName.clear();
//
//				}
//
//				catch (IOException e) { // TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//
		} 
		}
			else {
//			notifyMessage(3, "Select Item", false);
//			txtItemName.requestFocus();
//			return;
		}
		// ItemImageMst itemImageMst = itemImageMstList.get(itemImageMstList.size() -
		// 1);

	}

	private void resetImage() {
		// imageView.setImage(null);

		flowpane.getChildren().clear();

	}

	private void selectAndSaveImage() {

		if (txtItemName.getText().trim().isEmpty()) {
			notifyMessage(3, "Select Item Name", false);
			txtItemName.requestFocus();
			return;
		}
		if (txtMrp.getText().trim().isEmpty()) {
			notifyMessage(3, "Type Mrp", false);
			txtMrp.requestFocus();
			return;
		}
		if (cmbApplicableTime.getSelectionModel().isEmpty()) {
			notifyMessage(3, "Select Applicable Time", false);
			cmbApplicableTime.requestFocus();
			return;
		}
		if (cmbCategory.getSelectionModel().isEmpty()) {
			notifyMessage(3, "Select Category", false);
			cmbCategory.requestFocus();
			return;
		}
		
		
		File selectedFile = fileChooser.showOpenDialog(btnAdd.getScene().getWindow());
		
		try {
		
			ResponseEntity<ItemMst> item = RestCaller.getItemByNameRequestParam(txtItemName.getText());
			
			ResponseEntity<String> responce = RestCaller.uploadImage(selectedFile, item.getBody());
			
			System.out.println(responce.getBody());
		
			
			ItemImageMst itemImageMst = new ItemImageMst();
			
			  itemImageMst.setApplicableTime(cmbApplicableTime.getSelectionModel().
			  getSelectedItem()); 
			  itemImageMst.setIngredients(txtIngredients.getText());
			  itemImageMst.setMrp(Double.parseDouble(txtMrp.getText()));
			  itemImageMst.setSpecialOfferDescription(txtSpecialOffer.getText());
			  itemImageMst.setCategory(cmbCategory.getSelectionModel().getSelectedItem());
			  itemImageMst.setItemMst(item.getBody());
			  itemImageMst.setItemImage(responce.getBody());
			  itemImageMst.setBranchCode(SystemSetting.systemBranch);
			  
			  ResponseEntity<ItemImageMst> respentity = RestCaller.saveItemIamgeMst(itemImageMst);
			 
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 

		Image image1 = new Image(selectedFile.toURI().toString());

	// imageView.setImage(image);

		Node imagenode = new ImageView();

		imagenode.autosize();

		ImageView img = (ImageView) imagenode;

		img.setFitHeight(300);
		img.setFitWidth(200);
		img.setImage(image1);

		flowpane.getChildren().add(imagenode);
		
	
		//itemImageMst.setItemImage(img.getImage().toString().getBytes());

		// BufferedImage bufferedImage = null;
		
		try {

			BufferedImage bImage = ImageIO.read(selectedFile);
			
		 

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ImageIO.write(bImage, "png", bos);
			bos.flush();
			byte[] imageInByte = bos.toByteArray();
			bos.close();
			
			
			
			
			//byte[] encoded = Base64.getUrlEncoder().encode(imageInByte);
			
			/*
			 * byte[] encoded = Base64.getEncoder().encode("Hello".getBytes());
println(new String(encoded));   // Outputs "SGVsbG8="

byte[] decoded = Base64.getDecoder().decode(encoded);
println(new String(decoded))    // Outputs
			 */

			//itemImageMst.setItemImage(imageInByte);
			 

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		

		// txtItemName.clear();
		notifyMessage(3, "Image Uploaded", true);

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	/*
	 * //save image into database File file = new
	 * File("C:\\mavan-hibernate-image-mysql.gif"); byte[] bFile = new byte[(int)
	 * file.length()];
	 * 
	 * try { FileInputStream fileInputStream = new FileInputStream(file); //convert
	 * file into array of bytes fileInputStream.read(bFile);
	 * fileInputStream.close(); } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * Avatar avatar = new Avatar(); avatar.setImage(bFile);
	 * 
	 * session.save(avatar);
	 * 
	 * //Get image from database Avatar avatar2 = (Avatar)session.get(Avatar.class,
	 * avatar.getAvatarId()); byte[] bAvatar = avatar2.getImage();
	 * 
	 * try{ FileOutputStream fos = new FileOutputStream("C:\\test.gif");
	 * fos.write(bAvatar); fos.close(); }catch(Exception e){ e.printStackTrace(); }
	 */
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
}
