package com.maple.mapleclient.controllers;
import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.SalesManMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;
public class SalesManCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<SalesManMst> salesManList = FXCollections.observableArrayList();

    @FXML
    private Button save;

    @FXML
    private TextField txtSalesManName;

    @FXML
    private Button Delete;

    @FXML
    private Button showAll;

    @FXML
    private TableView<SalesManMst> salesManTbl;

    @FXML
    private TableColumn<SalesManMst, String> tblSalesManName;

    @FXML
    private TableColumn<SalesManMst, String> tblContactNumber;

    @FXML
    private TableColumn<SalesManMst, String> tblBranch;

    @FXML
    private TextField txtContactNo;

    @FXML
    private ComboBox<String> cmbBranch;

    @FXML
    void OnClickDelete(ActionEvent event) {

    }

    @FXML
    void OnClickSave(ActionEvent event) {
    	saveSalesMan();
    }

    @FXML
    void OnClickShowall(ActionEvent event) {

    	
    	findAllSalesMan();
    }

    @FXML
    void onEnterCustName(KeyEvent event) {

    }

    @FXML
    void onEnterLocalCustName(KeyEvent event) {

    }
	
	private void findAllSalesMan() {
		salesManList.clear();
    	List<SalesManMst>salesManMst = new ArrayList();
    	ResponseEntity<List<SalesManMst>> response = RestCaller.findAllSalesMan();
    	List<SalesManMst>salesManMstLists=response.getBody();
    	for(int i=0;i<salesManMstLists.size();i++) {
    		SalesManMst salesManMsts=new SalesManMst();
    		
    		salesManMsts.setSalesManName(salesManMstLists.get(i).getSalesManName());
    		salesManMsts.setSalesManContactNo(salesManMstLists.get(i).getSalesManContactNo());
    		salesManMsts.setBranchCode(salesManMstLists.get(i).getBranchCode());
    		salesManList.add(salesManMsts);
    		
    	
    	}
    	
    	
    	salesManTbl.setItems(salesManList); 
    	 filltable();
	}
    
    private void saveSalesMan() {
    	
    	if(txtSalesManName.getText().isEmpty()) {
    	 	notifyMessage(5,"Enter name ");
    	 	return;
    	}
    	if(txtContactNo.getText().isEmpty()) {
    	 	notifyMessage(5,"Enter phone Number ");
    	 	
    	 	return;
    	}if(null==cmbBranch.getValue()) {
    	 	notifyMessage(5,"Select Branch ");
    	 	return;
    	}
    	
    	SalesManMst salesManMst=new SalesManMst();
    	salesManMst.setSalesManName(txtSalesManName.getText());
    	salesManMst.setSalesManContactNo(txtContactNo.getText());
    	salesManMst.setBranchCode(cmbBranch.getValue());
    
      ResponseEntity<SalesManMst> respentity = RestCaller.saveSalesMan(salesManMst);
      SalesManMst  salesManMsts=respentity.getBody();
      salesManList.add(salesManMsts);
      salesManTbl.setItems(salesManList);
      findAllSalesMan();
      clearFields();
      notifyMessage(5,"Sales Man Saved");
    }
    
    @FXML
	 	private void initialize() {
    	setBranches();
	    
    }
    
    public void clearFields() {
    	txtSalesManName.clear();
    	txtContactNo.clear();
    }
    private void setBranches() {
		
		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();
		
		for(BranchMst branchMst : branchMstList)
		{
			cmbBranch.getItems().add(branchMst.getBranchCode());
		
			
		}
    }
    

    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

				Image img = new Image("done.png");
				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();
				notificationBuilder.show();
			}
    
    

private void filltable()
{
	tblSalesManName.setCellValueFactory(cellData -> cellData.getValue().getSalesManNameProperty());
	tblContactNumber.setCellValueFactory(cellData -> cellData.getValue().getSalesManContactNoProperty());
	tblBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchCodeProperty());

	
}

@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		//Stage stage = (Stage) btnClear.getScene().getWindow();
		//if (stage.isShowing()) {
			taskid = taskWindowDataEvent.getId();
			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
			
		 
			String hdrId = taskWindowDataEvent.getBusinessProcessId();
			System.out.println("Business Process ID = " + hdrId);
			
			 PageReload(hdrId);
		}


private void PageReload(String hdrId) {
	
}
}
