package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.CategoryManagementMst;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.StockTransferInDtl;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class CategoryManagementCtl {
	String taskid;
	String processInstanceId;
	CategoryManagementMst categoryManagementMst=null;
	private ObservableList<CategoryManagementMst> categorMgmntList = FXCollections.observableArrayList();

    @FXML
    private ComboBox<String> cmbCategory;

    @FXML
    private ComboBox<String> cmbSubCategory;
    @FXML
    private TableView<CategoryManagementMst> tbCategoryManagement;

    @FXML
    private TableColumn<CategoryManagementMst,String> clCategory;

    @FXML
    private TableColumn<CategoryManagementMst, String> clSubCategory;
    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;
    
    @FXML
    private Button btnClear;

    @FXML
    void actionClear(ActionEvent event) {
    	cmbCategory.getSelectionModel().clearSelection();
     	cmbSubCategory.getSelectionModel().clearSelection();
    }
	@FXML
	private void initialize() {
		ArrayList cat = new ArrayList();

		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object id = lm.get("id");
			if (id != null) {
				cmbCategory.getItems().add((String) categoryName);
				cmbSubCategory.getItems().add((String) categoryName);
			}

		}
		tbCategoryManagement.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					categoryManagementMst = new CategoryManagementMst();
					categoryManagementMst.setId(newSelection.getId());
				}
			}
		});
	}
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == categoryManagementMst)
    	{
    		return;
    	}
    	if(null == categoryManagementMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteCategoryManagementById(categoryManagementMst.getId());
    	showAll();
    	cmbCategory.getSelectionModel().clearSelection();
    	cmbSubCategory.getSelectionModel().clearSelection();
    }

    @FXML
    void actionSave(ActionEvent event) {

//    	if(null == cmbCategory.getSelectionModel() || null == cmbCategory.getSelectionModel().getSelectedItem())
//    	{
//    		notifyMessage(3,"Select Category");
//    		return;
//    	}
    	if(null == cmbSubCategory.getSelectionModel() || null == cmbSubCategory.getSelectionModel().getSelectedItem())
    	{
    		notifyMessage(3,"Select Sub Category");
    		return;
    	}
    	 ResponseEntity<CategoryMst> catMst = null;
    	 categoryManagementMst = new CategoryManagementMst();
    	 if(null != cmbCategory.getSelectionModel().getSelectedItem())
    	 {
    	  catMst = RestCaller.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem());
    	 categoryManagementMst.setCategoryId(catMst.getBody().getId());
    	 }
    	 ResponseEntity<CategoryMst> subCatMst = RestCaller.getCategoryByName(cmbSubCategory.getSelectionModel().getSelectedItem());
    	 categoryManagementMst.setSubCategoryId(subCatMst.getBody().getId());
    	 if(null != catMst)
    	 {
    	 ResponseEntity<CategoryManagementMst> getcatByCatIdAndSubCatId = RestCaller.getcategoryManagementByCategoryIdAndSubCategoryId(catMst.getBody().getId(),subCatMst.getBody().getId());
    	 if(null != getcatByCatIdAndSubCatId.getBody())
    	 {
    		 notifyMessage(3,"Already Saved");
    		 cmbCategory.getSelectionModel().clearSelection();
    	     	cmbSubCategory.getSelectionModel().clearSelection();
    		 return;
    	 }
    	 else
    	 {
    	 ResponseEntity<CategoryManagementMst> respentity = RestCaller.saveCategoryManagementMst(categoryManagementMst);
    	 categoryManagementMst = respentity.getBody();
    	 categorMgmntList.add(categoryManagementMst);
    	 }
    	 }
    	 else
    	 {
    	 ResponseEntity<CategoryManagementMst> respentity = RestCaller.saveCategoryManagementMst(categoryManagementMst);
    	 categoryManagementMst = respentity.getBody();
    	 categorMgmntList.add(categoryManagementMst);
    	 }
    	
    	 fillTable();
    	 cmbCategory.getSelectionModel().clearSelection();
//     	cmbSubCategory.getSelectionModel().clearSelection();
    }
    private void fillTable()
    {
    	
    	for(CategoryManagementMst catmgmnt:categorMgmntList)
    	{
    		if(null != catmgmnt.getCategoryId())
    		{
    		ResponseEntity<CategoryMst> categoryMst = RestCaller.getCategoryById(catmgmnt.getCategoryId());
    		catmgmnt.setCategoryName(categoryMst.getBody().getCategoryName());
    		}
    		ResponseEntity<CategoryMst> subCategory = RestCaller.getCategoryById(catmgmnt.getSubCategoryId());
    		catmgmnt.setSubCategoryName(subCategory.getBody().getCategoryName());
    	}
    	tbCategoryManagement.setItems(categorMgmntList);
		clCategory.setCellValueFactory(cellData -> cellData.getValue().getcategoryNameProperty());
		clSubCategory.setCellValueFactory(cellData -> cellData.getValue().getsubCategoryNameProperty());

    	
    }
    @FXML
    void actionShowAll(ActionEvent event) {
    
    	showAll();
    }
    private void showAll()
    {
    	ResponseEntity<List<CategoryManagementMst>> categoryManagementmstList = RestCaller.getAlcategoryManagement();
    	categorMgmntList = FXCollections.observableArrayList(categoryManagementmstList.getBody());
    	fillTable();
    }

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	       private void PageReload() {
	       	
	 }

}
