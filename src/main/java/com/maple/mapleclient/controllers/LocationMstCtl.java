package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.LocationMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class LocationMstCtl {
	String taskid;
	String processInstanceId;
	
	LocationMst locationMst = new LocationMst();
	
	private ObservableList<LocationMst> locationMstList = FXCollections.observableArrayList();


	@FXML
	private TextField txtLocation;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowAll;

    @FXML
    private TableView<LocationMst> tblLocationMst;

    @FXML
    private TableColumn<LocationMst, String> clLocation;

	@FXML
	void Delete(ActionEvent event) {

	}

	@FXML
	void SaveOnPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) 
    	{
			save();
    	}

	}

	@FXML
	void ShowAll(ActionEvent event) {
		
		ResponseEntity<List<LocationMst>> locationList = RestCaller.findAllLocationMst();
		locationMstList = FXCollections.observableArrayList(locationList.getBody());

		FillTable();
	}

	@FXML
	void save(ActionEvent event) {

		save();
	}

	private void save() {

		if (txtLocation.getText().trim().isEmpty()) {
			notifyMessage(5, "Please Enter Location!!!", false);
			txtLocation.requestFocus();
			return;
		}

		locationMst = new LocationMst();

		String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "LOC");

		locationMst.setId(vNo + SystemSetting.getUser().getCompanyMst().getId());
		locationMst.setLocation(txtLocation.getText());
		locationMst.setBranchCode(SystemSetting.systemBranch);
		ResponseEntity<LocationMst> locationMstResp = RestCaller.saveLocationMst(locationMst);
		locationMst = locationMstResp.getBody();
		
		locationMstList.add(locationMst);
		FillTable();
		locationMst = null;
		txtLocation.clear();
	}

	private void FillTable() {
		
		tblLocationMst.setItems(locationMstList);
		clLocation.setCellValueFactory(cellData -> cellData.getValue().getLocationProperty());

		
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	@Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }


}
