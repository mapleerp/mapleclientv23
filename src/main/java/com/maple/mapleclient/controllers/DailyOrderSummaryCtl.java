package com.maple.mapleclient.controllers;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.SaleOrderReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class DailyOrderSummaryCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<SaleOrderReport> saleOrderListTable = FXCollections.observableArrayList();


    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private TableView<SaleOrderReport> tblReport;

    @FXML
    private TableColumn<SaleOrderReport, String> clItem;

    @FXML
    private TableColumn<SaleOrderReport, String> clUnit;

    @FXML
    private TableColumn<SaleOrderReport, Number> clQty;
    @FXML
   	private void initialize() {
       	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
       
    }
    @FXML
    void GenerateReport(ActionEvent event) {
    	
    	if(null == dpDate.getValue())
    	{
    		notifyMessage(3, "Please select date...!", false);
    		return;
    	}
    	
    	Date date = SystemSetting.localToUtilDate(dpDate.getValue());
    	String sdate = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");
    	ResponseEntity<List<SaleOrderReport>> saleOrderList = RestCaller.getOrderSummary(sdate);

    	saleOrderListTable = FXCollections.observableArrayList(saleOrderList.getBody());
    	FillTable();
    	
    	

    }
    
    private void FillTable() {
    	tblReport.setItems(saleOrderListTable);
		
		clItem.setCellValueFactory(cellData -> cellData.getValue().getItemProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
		
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	   @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }


}
