package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.TaxDtl;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class TaxCreationCtl {
	
	String taskid;
	String processInstanceId;
	
	TaxMst taxMst = null;
	TaxDtl taxDtl = null;
	
	private ObservableList<TaxMst> itemsListTable = FXCollections.observableArrayList();
	private ObservableList<TaxMst> itemsListTable1= FXCollections.observableArrayList();


    @FXML
    private ComboBox<String> cmbTaxType;
	
    @FXML
    private ComboBox<String> cmbCategory;
    @FXML
    private Button btnClear;
    @FXML
    private TextField txtCurrentTax;

    @FXML
    private Button btnFetchItems;

    @FXML
    private DatePicker dpStartDate;


    @FXML
    private TextField txtTaxRate;

    @FXML
    private Button btnSubmit;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnDelete;

    @FXML
    private TableView<TaxMst> tblTax;

    @FXML
    private TableColumn<TaxMst, String> clItemName;

    @FXML
    private TableColumn<TaxMst, Number> clItemTax;

    @FXML
    private TableColumn<TaxMst, String> clTaxType;

    @FXML
    private TableColumn<TaxMst, String> clItemUnit;
    @FXML
    private TableColumn<TaxMst, String> clStartDate;

    @FXML
    private TableColumn<TaxMst, String> clEndDate;
    @FXML
	private void initialize() {
    	dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
    	getCategory();
    	searchTaxType();
    
    	
    }

    @FXML
    void DeleteTax(ActionEvent event) {

    }

    @FXML
    void ShowAll(ActionEvent event) {

    	ResponseEntity<List<TaxMst>> getTaxs= RestCaller.getAllTaxs();
    	itemsListTable1   = FXCollections.observableArrayList(getTaxs.getBody());
    	for(TaxMst tax:itemsListTable1)
		{
			
			ResponseEntity<ItemMst> getItem= RestCaller.getitemMst(tax.getItemId());
			ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
			tax.setItemName(getItem.getBody().getItemName());
			tax.setUnitName(getUnit.getBody().getUnitName());
			
		}
    	fillTable();
    	
    	
    	
    }

    @FXML
    void actionClear(ActionEvent event) {

    	txtCurrentTax.clear();
    	txtTaxRate.clear();
    	cmbTaxType.getSelectionModel().clearSelection();
    	dpStartDate.setValue(null);
    	tblTax.getItems().clear();
    	
    	
    }
    @FXML
    void Submit(ActionEvent event) {
    	
    	if(txtTaxRate.getText().trim().isEmpty())
    	{
    		notifyMessage(5, "please enter tax rate",false);
    	} else if (null == itemsListTable1) {
    		
    		notifyMessage(5, "please select items",false);
			
		} 
    	else if(cmbTaxType.getSelectionModel().isEmpty())
    	{
    		notifyMessage(5, "please select Tax Type",false);
    	}
    	else {
			TaxMst tax = new TaxMst();
			for(TaxMst item : itemsListTable1)
			{
				
				taxMst = new TaxMst();
				taxMst.setItemId(item.getItemId());
				taxMst.setTaxId(cmbTaxType.getSelectionModel().getSelectedItem());
				taxMst.setEndDate(null);
				String vNo= RestCaller.getVoucherNumber(SystemSetting.getSystemBranch()+"TX");
				taxMst.setId(vNo);
				taxMst.setTaxRate(Double.parseDouble(txtTaxRate.getText()));
				java.util.Date startDate = Date.valueOf(dpStartDate.getValue());
				taxMst.setStartDate(startDate);
				
				ResponseEntity<TaxMst> respentity = RestCaller.saveTaxMst(taxMst);
				tax = respentity.getBody();
				
			}
			
			if(null != tax)
			{
				notifyMessage(5, "Successfully created",true);
			}
			
		}
    	

    }

    @FXML
    void getCategoryOnClick(MouseEvent event) {
    	getCategory();

    }
    
    
    @FXML
    void FetchItems(ActionEvent event) {
    	tblTax.getItems().clear();
    	itemsListTable1.clear();
    	if(null != cmbCategory.getValue() && txtCurrentTax.getText().trim().isEmpty())
    	{
    		
    		getItemsByCategory();
    		
    	} else if (!txtCurrentTax.getText().trim().isEmpty()) {
    		
    		getItemsByCategoryAndTax();
			
		}
    	
    	

    }
    
    private void searchTaxType()
    {
    	ArrayList allTaxType = new ArrayList();
    	allTaxType = RestCaller.SearchTaxTypes();
    	Iterator itr1 = allTaxType.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object taxName = lm.get("taxName");
			Object taxId = lm.get("taxId");
			if (taxId != null) {
				
				cmbTaxType.getItems().add((String)taxId);
	
			}
		}
    }
    
    
    private void getCategory() {
    	
    	cmbCategory.getItems().clear();

		ArrayList cat = new ArrayList();
		RestTemplate restTemplate1 = new RestTemplate();
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object parentId = lm.get("parentId");
			Object id = lm.get("id");
			if (id != null) {
				
				cmbCategory.getItems().add((String)categoryName);
	
			}
		}
	}
    
    private void getItemsByCategory() {
    	
    	if(null != cmbCategory.getValue())
    	{
    		ResponseEntity<CategoryMst> categorySaved = RestCaller.getCategoryByName(cmbCategory.getValue());
    		if(null != categorySaved.getBody())
    		{
    		
    			
    			ResponseEntity<List<ItemMst>> itemsSaved = RestCaller.getItemsByCategory(categorySaved.getBody());
    		
    		if(null != itemsSaved.getBody())
    		{
    		for(ItemMst items:itemsSaved.getBody())
    		{
    			taxMst = new TaxMst();
    			taxMst.setItemName(items.getItemName());
    			ResponseEntity<UnitMst> unit = RestCaller.getunitMst(items.getUnitId());
    			taxMst.setUnitName(unit.getBody().getUnitName());
    			taxMst.setTaxId("");
    			taxMst.setTaxRate(items.getTaxRate());
    			taxMst.setItemId(items.getId());
    			taxMst.setEndDate(null);
    			taxMst.setEndDate(null);
    			itemsListTable1.add(taxMst);
    		}
    		fillTable();
    		}
    		}
    	}
		
	}
    private void fillTable() {
	
    	tblTax.setItems(itemsListTable1);
    	
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
    	clItemTax.setCellValueFactory(cellData -> cellData.getValue().gettaxRateProperty());
    	clTaxType.setCellValueFactory(cellData -> cellData.getValue().gettaxIdProperty());
    	clItemUnit.setCellValueFactory(cellData -> cellData.getValue().getunitProperty());
    	clStartDate.setCellValueFactory(cellData -> cellData.getValue().getstartDateProperty());
    	clEndDate.setCellValueFactory(cellData -> cellData.getValue().getendDateProperty());

//    	for(ItemMst item : itemsListTable)
//    	{
//    		if(null != item.getUnitId())
//    		{
//    			ResponseEntity<UnitMst> itemsSaved = RestCaller.getunitMst(item.getUnitId());
//    			item.setUnitName(itemsSaved.getBody().getUnitName());
//    			clItemUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
//    		}
//    		
//    	}
		
	}

	private void getItemsByCategoryAndTax() {
		
		if(null != cmbCategory.getValue() && !txtCurrentTax.getText().trim().isEmpty() && null != cmbTaxType.getValue() && null != dpStartDate.getValue())
    	{
    		ResponseEntity<CategoryMst> categorySaved = RestCaller.getCategoryByName(cmbCategory.getValue());
    		if(null != categorySaved.getBody())
    		{
    			
    			LocalDate dpDate1 = dpStartDate.getValue();
    			java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
    			 String  strDate = SystemSetting.UtilDateToString(uDate, "dd-MM-yyyy");
    		ResponseEntity<List<ItemMst>> getitems = RestCaller.getItemsByCategory(categorySaved.getBody());
    		for(ItemMst items:getitems.getBody())
    		{
     		ResponseEntity<List<TaxMst>> tacSaved = RestCaller.getItemsByCategoryAndTaxAndTaxId(items.getId(),cmbTaxType.getSelectionModel().getSelectedItem(),Double.parseDouble(txtCurrentTax.getText()),strDate);
    		
    		if(tacSaved.getBody().size()>0)
    		{
    		itemsListTable = FXCollections.observableArrayList(tacSaved.getBody());
    		for(TaxMst tax:itemsListTable)
    		{
    			
    			ResponseEntity<ItemMst> getItem= RestCaller.getitemMst(tax.getItemId());
    			ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(getItem.getBody().getUnitId());
    			tax.setItemName(getItem.getBody().getItemName());
    			tax.setUnitName(getUnit.getBody().getUnitName());
    			itemsListTable1.add(tax);
    		}
    		
    		}
    		else
    		{
    			getItemsByCategory();
    		}
    		
    		}
    		fillTable();
    		}
    	}
		
	}
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	
	  @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}

}

