package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.ExportCategoryWiseStockReportToExcel;
import com.maple.maple.util.ExportItemWiseStockMovmntReportToExcel;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.StockSummaryDtlReport;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JRException;

public class StockReportCtl {
	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	ExportItemWiseStockMovmntReportToExcel exportItemwiseStockMvmntReportToExcel=new ExportItemWiseStockMovmntReportToExcel();
    @FXML
    private DatePicker dpFromDate;
    @FXML
    private DatePicker dpToDate;
    @FXML
    private Button btnExport;

    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnShowReport;
    
    @FXML
    private Button btnExportToExcel;
    
    @FXML
    private Button btnItemWIseStockReport;

    @FXML
    void ShowReport(ActionEvent event) {
    	java.util.Date fromDate =Date.valueOf(dpFromDate.getValue());
    	java.util.Date toDate =Date.valueOf(dpToDate.getValue());
    	
    	ResponseEntity<ItemMst> itemsaved = RestCaller.getItemByNameRequestParam(txtItemName.getText());
		
		String vFDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
		String vToDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
		
			try {
				JasperPdfReportService.StockSummaryDtslReport(vFDate,vToDate,itemsaved.getBody().getId(),SystemSetting.systemBranch);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
		
			
	    	
		

    }
    @FXML
    void loadPopUp(MouseEvent event) {
    	showItem();
    }
    
    @Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");

	
		Stage stage = (Stage) btnShowReport.getScene().getWindow();
		if (stage.isShowing()) {
	
			txtItemName.setText(itemPopupEvent.getItemName());
			
		}

	}
    private void showItem()
    {
    	try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// fxmlLoader.setController(itemStockPopupCtl);
			Parent root1;
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Stock Item");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	@FXML
	private void initialize() {
		eventBus.register(this);
		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
	
		
	}
	
	 @FXML
	    void exportToExcel(ActionEvent event) {
		
			/*
			 * java.util.Date fromDate =Date.valueOf(dpFromDate.getValue()); java.util.Date
			 * toDate =Date.valueOf(dpToDate.getValue());
			 * 
			 * ResponseEntity<ItemMst> itemsaved =
			 * RestCaller.getItemByNameRequestParam(txtItemName.getText());
			 * 
			 * String vFDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
			 * String vToDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
			 * ResponseEntity<List<StockSummaryDtlReport>> stockSummaryDtlReport =
			 * RestCaller .getStockSummaryDtlReport(vFDate, vToDate,
			 * itemsaved.getBody().getId(), SystemSetting.systemBranch);
			 * 
			 * 
			 * 
			 * exportItemwiseStockMvmntReportToExcel.exportToExcel("ItemStockMovmnt"+vFDate+
			 * ".xls", stockSummaryDtlReport.getBody());
			 */
	    }

	 @FXML
	    void actionExport(ActionEvent event) {
		 java.util.Date fromDate =Date.valueOf(dpFromDate.getValue()); java.util.Date
		  toDate =Date.valueOf(dpToDate.getValue());
		  
		  ResponseEntity<ItemMst> itemsaved =
		  RestCaller.getItemByNameRequestParam(txtItemName.getText());
		  
		  String vFDate = SystemSetting.UtilDateToString(fromDate, "yyyy-MM-dd");
		  String vToDate = SystemSetting.UtilDateToString(toDate, "yyyy-MM-dd");
			ResponseEntity<List<StockSummaryDtlReport>> stockSummaryDtlReport = RestCaller
					.getStockSummaryDtlReport(vFDate, vToDate, itemsaved.getBody().getId(), SystemSetting.systemBranch);

		  
		  
			exportItemwiseStockMvmntReportToExcel.exportToExcel("ItemStockMovmnt"+vFDate+".xls", stockSummaryDtlReport.getBody());
		 
	    }
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   	private void PageReload(String hdrId) {

	   	}


}
