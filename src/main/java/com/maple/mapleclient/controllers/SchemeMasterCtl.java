package com.maple.mapleclient.controllers;

import java.util.ArrayList;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.maple.javapos.print.ChequePrint;
import com.maple.maple.util.MapleConstants;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.SchEligibilityDef;
import com.maple.mapleclient.entity.SchOfferDef;
import com.maple.mapleclient.entity.SchSelectDef;
import com.maple.mapleclient.entity.SchemeInstance;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class SchemeMasterCtl {
	private static final Logger log = LoggerFactory.getLogger(SchemeMasterCtl.class);

	String taskid;
	String processInstanceId;
	
	String schemeName;
	
	private ObservableList<SchemeInstance> SchemeInstanceList = FXCollections.observableArrayList();
	SchemeInstance schemeInstance = null;
	
	SchOfferDef schofferDef = null;
	SchEligibilityDef schEligibilityDef = null;
	SchSelectDef schSelectDef = null;
	@FXML
	private ComboBox<String> cmbValidityPeriod;

	@FXML
	private TextField txtSchemeName;

	@FXML
	private ComboBox<String> cmbEligibility;

	@FXML
	private ComboBox<String> cmbOfferId;

	@FXML
	private ComboBox<String> cmbActive;

	@FXML
	private ComboBox<String> cmbWholesale;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnRefresh;

	@FXML
	private Button btnDelete;

	 @FXML
	 private Button btnPublish;
	
	@FXML
	private TableView<SchemeInstance> tbSchemeMaster;

	@FXML
	private TableColumn<SchemeInstance, String> clSchemeName;

	@FXML
	private TableColumn<SchemeInstance, String> clEligibilityCriteria;

	@FXML
	private TableColumn<SchemeInstance, String> clValidity;

	@FXML
	private TableColumn<SchemeInstance, String> clOfferId;

	@FXML
	private TableColumn<SchemeInstance, String> clActive;

	@FXML
	private TableColumn<SchemeInstance, String> clWholesale;

	@FXML
	private Button btnEdit;
	
	


    @FXML
    void EditScheme(ActionEvent event) {
    	
    	if (txtSchemeName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter scheme name!!!", false);
			return;
		}
		if (null == cmbEligibility.getValue()) {
			notifyMessage(5, "Please select Eligibility Criteria!!!", false);
			return;
		}
		if (null == cmbActive.getValue()) {
			notifyMessage(5, "Please select scheme status!!!", false);
			return;
		}
		if (null == cmbValidityPeriod.getValue()) {
			notifyMessage(5, "Please validity period!!!", false);
			return;
		}
		if (null == cmbOfferId.getValue()) {
			notifyMessage(5, "Please offereId!!!", false);
			return;
		}
		if (null == cmbWholesale.getValue()) {
			notifyMessage(5, "Please select Wholesale/ Retail!!!", false);
			return;
		}

		schemeName = txtSchemeName.getText();
		
//		schemeInstance = new SchemeInstance();
//		String vNo = RestCaller.getVoucherNumber("SCHI");
//		schemeInstance.setId(vNo);
		schemeInstance.setactiveProperty(cmbActive.getSelectionModel().getSelectedItem());
		schemeInstance.setSchemeWholesaleRetail(cmbWholesale.getSelectionModel().getSelectedItem());
		schemeInstance.setSchemeName(txtSchemeName.getText());
		ResponseEntity<SchOfferDef> schOfferDefsaved = RestCaller
				.getSchemeOfferDef(cmbOfferId.getSelectionModel().getSelectedItem());
		schofferDef = new SchOfferDef();
		schofferDef = schOfferDefsaved.getBody();
		schemeInstance.setOfferId(schofferDef.getId());
		ResponseEntity<SchEligibilityDef> schEligibilityDefsaved = RestCaller
				.getSchemeEligibilityDef(cmbEligibility.getSelectionModel().getSelectedItem());
		schEligibilityDef = new SchEligibilityDef();
		schEligibilityDef = schEligibilityDefsaved.getBody();
		schemeInstance.setEligibilityId(schEligibilityDef.getId());
		ResponseEntity<SchSelectDef> schSelectDefsaved = RestCaller
				.getSchSelectDefbyName(cmbValidityPeriod.getSelectionModel().getSelectedItem());
		schSelectDef = new SchSelectDef();
		schSelectDef = schSelectDefsaved.getBody();
		schemeInstance.setSelectionId(schSelectDef.getId());
		RestCaller.updateSchemeMst(schemeInstance);
		
		schemeInstance = null;
		clearFields();
		showAll();

    }

	@FXML
	private void initialize() {

		ArrayList selection = new ArrayList();

		selection = RestCaller.getAllSelectDef();
		Iterator itr = selection.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			System.out.println("selectionName-----" + lm);
			Object selectionName = lm.get("selectionName");
			Object id = lm.get("id");
			if (id != null) {
				cmbValidityPeriod.getItems().add((String) selectionName);
			}
		}
		ArrayList offers = new ArrayList();

		offers = RestCaller.getAllOffers();
		Iterator itr1 = offers.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			System.out.println("offerType-----" + lm);
			Object offerType = lm.get("offerType");
			Object id = lm.get("id");
			if (id != null) {
				cmbOfferId.getItems().add((String) offerType);
			}
		}
		ArrayList eligibility = new ArrayList();

		eligibility = RestCaller.getAllEligibility();
		Iterator itr2 = eligibility.iterator();
		while (itr2.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr2.next();
			System.out.println("eligibility-----" + lm);
			Object eligibilityName = lm.get("eligibilityName");
			Object id = lm.get("id");
			if (id != null) {
				cmbEligibility.getItems().add((String) eligibilityName);
			}
		}
		cmbActive.getItems().add("YES");
		cmbActive.getItems().add("NO");
		cmbWholesale.getItems().add("WholeSale");
		cmbWholesale.getItems().add("Retail");
		cmbWholesale.getItems().add("Both");

		showAll();
		tbSchemeMaster.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					schemeInstance = new SchemeInstance();
					schemeInstance.setId(newSelection.getId());
					if(null != newSelection.getSchemeName())
					txtSchemeName.setText(newSelection.getSchemeName());
					
					if(null != newSelection.getIsActive())
					cmbActive.getSelectionModel().select(newSelection.getIsActive());
						
					if(null != newSelection.getSchemeWholesaleRetail())
						cmbWholesale.getSelectionModel().select(newSelection.getSchemeWholesaleRetail());
						
					if(null != newSelection.getEligibilityId())
					{
						ResponseEntity<SchEligibilityDef> eligibResp = RestCaller.getSchemeEligibilityId(newSelection.getEligibilityId());
						SchEligibilityDef schEligibilityDef = eligibResp.getBody();
						if(null != schEligibilityDef)
						{
							cmbEligibility.getSelectionModel().select(schEligibilityDef.getEligibilityName());
							
						}
						
					}
						
					
					if(null != newSelection.getOfferId())
					{
						ResponseEntity<SchOfferDef> offerResp = RestCaller.getSchemeOfferDefbyId(newSelection.getOfferId());
						SchOfferDef schOfferDef = offerResp.getBody();
						if(null != schOfferDef)
						{
							cmbOfferId.getSelectionModel().select(schOfferDef.getOfferType());
							
						}
						
					}
					
					if(null != newSelection.getSelectionId())
					{
						ResponseEntity<SchSelectDef> selectResp = RestCaller.getSchSelectDefbyId(newSelection.getOfferId());
						SchSelectDef schSelectDef = selectResp.getBody();
						if(null != schSelectDef)
						{
							cmbValidityPeriod.getSelectionModel().select(schSelectDef.getSelectionName());
							
						}
						
					}

				}
			}
		});

	}

	@FXML
	void addItem(ActionEvent event) {

		if (txtSchemeName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter scheme name!!!", false);
			return;
		}
		if (null == cmbEligibility.getValue()) {
			notifyMessage(5, "Please select Eligibility Criteria!!!", false);
			return;
		}
		if (null == cmbActive.getValue()) {
			notifyMessage(5, "Please select scheme status!!!", false);
			return;
		}
		if (null == cmbValidityPeriod.getValue()) {
			notifyMessage(5, "Please validity period!!!", false);
			return;
		}
		if (null == cmbOfferId.getValue()) {
			notifyMessage(5, "Please offereId!!!", false);
			return;
		}
		if (null == cmbWholesale.getValue()) {
			notifyMessage(5, "Please select Wholesale/ Retail!!!", false);
			return;
		}
		
		schemeName = txtSchemeName.getText();
		
		String eligibilityCriteria = cmbEligibility.getSelectionModel().getSelectedItem();
		String validityPeriod =  cmbValidityPeriod.getSelectionModel().getSelectedItem();
		
		log.info("cmbEligibility = {}",cmbEligibility);
		log.info("cmbValidityPeriod = {}",cmbValidityPeriod);
		
		schemeInstance = new SchemeInstance();
		String vNo = RestCaller.getVoucherNumber("SCHI");
		schemeInstance.setId(vNo);
		schemeInstance.setBranchCode(SystemSetting.systemBranch);
		schemeInstance.setactiveProperty(cmbActive.getSelectionModel().getSelectedItem());
		schemeInstance.setSchemeWholesaleRetail(cmbWholesale.getSelectionModel().getSelectedItem());
		schemeInstance.setSchemeName(txtSchemeName.getText());
		ResponseEntity<SchOfferDef> schOfferDefsaved = RestCaller
				.getSchemeOfferDef(cmbOfferId.getSelectionModel().getSelectedItem());
		schofferDef = new SchOfferDef();
		schofferDef = schOfferDefsaved.getBody();
		schemeInstance.setOfferId(schofferDef.getId());
		ResponseEntity<SchEligibilityDef> schEligibilityDefsaved = RestCaller
				.getSchemeEligibilityDef(eligibilityCriteria);
		schEligibilityDef = new SchEligibilityDef();
		schEligibilityDef = schEligibilityDefsaved.getBody();
		schemeInstance.setEligibilityId(schEligibilityDef.getId());
		ResponseEntity<SchSelectDef> schSelectDefsaved = RestCaller
				.getSchSelectDefbyName(validityPeriod);
		schSelectDef = new SchSelectDef();
		schSelectDef = schSelectDefsaved.getBody();
		schemeInstance.setSelectionId(schSelectDef.getId());
		
		
		ResponseEntity<SchemeInstance> respentity = RestCaller.saveSchemeInstance(schemeInstance);
		schemeInstance = respentity.getBody();
		schemeInstance.setSelection(schSelectDef.getSelectionName());
		schemeInstance.setEligibility(schEligibilityDef.getEligibilityName());
		schemeInstance.setOffer(schofferDef.getOfferType());
		SchemeInstanceList.add(schemeInstance);
		filltable();
		notifyMessage(5, "Scheme Added!!!", true);
		schemeInstance = null;
		cmbActive.getSelectionModel().clearSelection();
		cmbEligibility.getSelectionModel().clearSelection();
		cmbOfferId.getSelectionModel().clearSelection();
		cmbValidityPeriod.getSelectionModel().clearSelection();
		cmbWholesale.getSelectionModel().clearSelection();
		txtSchemeName.clear();

	}

	private void clearFields() {
		cmbActive.getSelectionModel().clearSelection();
		cmbEligibility.getSelectionModel().clearSelection();
		cmbOfferId.getSelectionModel().clearSelection();
		cmbValidityPeriod.getSelectionModel().clearSelection();
		cmbWholesale.getSelectionModel().clearSelection();
		txtSchemeName.clear();
		tbSchemeMaster.getItems().clear();

	}

	@FXML
	void deleteAction(ActionEvent event) {
		if (null != schemeInstance) {
			if (null != schemeInstance.getId()) {
				RestCaller.deleteSchemeInstance(schemeInstance.getId());
				SchemeInstanceList.clear();
				notifyMessage(5, "Scheme Deleted!!!", true);
				schemeInstance = null;
				showAll();
				clearFields();
			}
		}

	}

	private void showAll() {
		tbSchemeMaster.getItems().clear();
		ResponseEntity<List<SchemeInstance>> schemeinstancesaved = RestCaller.getAllSchemeInstance();
		SchemeInstanceList = FXCollections.observableArrayList(schemeinstancesaved.getBody());
		for (SchemeInstance schi : SchemeInstanceList) {
			ResponseEntity<SchEligibilityDef> schEligibilityDefsaved = RestCaller
					.getSchemeEligibilityId(schi.getEligibilityId());
			if (null != schEligibilityDefsaved.getBody()) {
				schi.setEligibility(schEligibilityDefsaved.getBody().getEligibilityName());
			}
			ResponseEntity<SchOfferDef> schOfferDefsaved = RestCaller.getSchemeOfferDefbyId(schi.getOfferId());
			if (null != schOfferDefsaved.getBody()) {
				schi.setOffer(schOfferDefsaved.getBody().getOfferType());
			}
			ResponseEntity<SchSelectDef> schSelectDefsaved = RestCaller.getSchSelectDefbyId(schi.getSelectionId());
			if (null != schSelectDefsaved.getBody()) {
				schi.setSelection(schSelectDefsaved.getBody().getSelectionName());
			}
		}
		tbSchemeMaster.setItems(SchemeInstanceList);
		filltable();

	}

	@FXML
	void Refresh(ActionEvent event) {
	
		clearFields();
		showAll();

	}

	private void filltable() {
		tbSchemeMaster.setItems(SchemeInstanceList);
		clActive.setCellValueFactory(cellData -> cellData.getValue().getactiveProperty());
		clEligibilityCriteria.setCellValueFactory(cellData -> cellData.getValue().geteligiblityProperty());
		clOfferId.setCellValueFactory(cellData -> cellData.getValue().getofferProperty());
		clSchemeName.setCellValueFactory(cellData -> cellData.getValue().getschemeNameProperty());
		clValidity.setCellValueFactory(cellData -> cellData.getValue().getselectionProperty());
		clWholesale.setCellValueFactory(cellData -> cellData.getValue().getschemeWholesaleRetailProperty());

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
		public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
			//Stage stage = (Stage) btnClear.getScene().getWindow();
			//if (stage.isShowing()) {
				taskid = taskWindowDataEvent.getId();
				processInstanceId = taskWindowDataEvent.getProcessInstanceId();
				
			 
				String hdrId = taskWindowDataEvent.getBusinessProcessId();
				System.out.println("Business Process ID = " + hdrId);
				
				 PageReload(hdrId);
			}


		private void PageReload(String hdrId) {

		}
		
		@FXML
		void onPublish(ActionEvent event) {

			if(!schemeName.isEmpty()) {
				String result = RestCaller.publishScheme(schemeName);
				if(result.equalsIgnoreCase(MapleConstants.SUCCESS.toString())) {
					notifyMessage(5, "Scheme Published Succesfully!!!", true);
				}
				
				schemeName = "";
			}
			
		}
}
