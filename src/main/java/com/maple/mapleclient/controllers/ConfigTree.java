package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;

public class ConfigTree {
	String taskid;
	String processInstanceId;

	String selectedText = null;
	@FXML
	private TreeView<String> treeview;

	@FXML
	void initialize() {

		TreeItem rootItem = new TreeItem("Configuration");

		HashMap menuWindowQueue = MapleclientApplication.mainFrameController.getMenuWindowQueue();

		ResponseEntity<List<MenuConfigMst>> listMenuConficMstBody = RestCaller.MenuConfigMstByMenuName("Configuration");

		List<MenuConfigMst> listMenuConfig = listMenuConficMstBody.getBody();

		if (null != listMenuConfig && listMenuConfig.size() > 0) {
			MenuConfigMst aMenuConfigMst = listMenuConfig.get(0);

			String parentId = aMenuConfigMst.getId();

			ResponseEntity<List<MenuConfigMst>> menuConficMst = RestCaller.MenuConfigMstByParentId(parentId);
			List<MenuConfigMst> menuConfigMstList = menuConficMst.getBody();

			for (MenuConfigMst aMenuConfig : menuConfigMstList) {
				if (SystemSetting.UserHasRole(aMenuConfig.getMenuName())) {
					rootItem.getChildren().add(new TreeItem(aMenuConfig.getMenuName()));
				}

			}

			// rootItem.getChildren().add(masterItem);

			treeview.setRoot(rootItem);

			treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
					TreeItem<String> selectedItem = (TreeItem<String>) newValue;
					System.out.println("Selected Text 12345 : " + selectedItem.getValue());
					selectedText = selectedItem.getValue();

					ResponseEntity<List<MenuConfigMst>> menuConficMstListBody = RestCaller
							.MenuConfigMstByMenuName(selectedText);

					List<MenuConfigMst> menuConfigMstList = menuConficMstListBody.getBody();

					if (menuConfigMstList.size() > 0) {
						MenuConfigMst aMenuConfig = menuConfigMstList.get(0);

						Node dynamicWindow = (Node) menuWindowQueue.get(aMenuConfig.getMenuName());
						if (null == dynamicWindow) {
							if (null != selectedText) {
								try {
									dynamicWindow = FXMLLoader
											.load(getClass().getResource("/fxml/" + aMenuConfig.getMenuFxml()));
									menuWindowQueue.put(selectedText, dynamicWindow);

								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						}

						try {

							// Clear screen before loading the Page.
							if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
								MapleclientApplication.mainWorkArea.getChildren().clear();

							MapleclientApplication.mainWorkArea.getChildren().add(dynamicWindow);

							MapleclientApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
							MapleclientApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
							dynamicWindow.requestFocus();

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					/*
					 * if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) { if
					 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					 * MapleclientApplication.mainWorkArea.getChildren().clear();
					 * 
					 * MapleclientApplication.mainWorkArea.getChildren()
					 * .add(MapleclientApplication.mainFrameController.kotManagement);
					 * 
					 * }
					 * 
					 */

				}

			});

		}

		TreeItem rootItem2 = new TreeItem("Configuration");
//
//		TreeItem itemMasterItem = new TreeItem("ITEM MASTER");
//		if (SystemSetting.UserHasRole("ITEM MASTER")) {
//
//			rootItem2.getChildren().add(itemMasterItem);
//		}
//		TreeItem itemEdit = new TreeItem("ITEM EDIT");
//		if (SystemSetting.UserHasRole("ITEM EDIT")) {
//			rootItem2.getChildren().add(itemEdit);
//		}
//		TreeItem categoryManagement = new TreeItem("CATEGORY MANAGEMENT");
//		if (SystemSetting.UserHasRole("CATEGORY MANAGEMENT")) {
//			rootItem2.getChildren().add(categoryManagement);
//		}
//		TreeItem checkprint = new TreeItem("CHECK PRINT");
//		if (SystemSetting.UserHasRole("CHECK PRINT")) {
//			rootItem2.getChildren().add(checkprint);
//		}
//		TreeItem taxItem = new TreeItem("TAX CREATION");
//		if (SystemSetting.UserHasRole("TAX CREATION")) {
//			rootItem2.getChildren().add(taxItem);
//		}
//
//		TreeItem taxItemwise = new TreeItem("ITEMWISE TAX CREATION");
//		if (SystemSetting.UserHasRole("TAX CREATION")) {
//			rootItem2.getChildren().add(taxItemwise);
//		}
//
//		TreeItem pubItem = new TreeItem("PUBLISH MESSAGE");
//		if (SystemSetting.UserHasRole("PUBLISH MESSAGE")) {
//			rootItem2.getChildren().add(pubItem);
//		}
//
//		TreeItem kitedefItem = new TreeItem("KIT DEFINITION");
//		if (SystemSetting.getUser().getUserName().equalsIgnoreCase("test")) {
//			rootItem2.getChildren().add(kitedefItem);
//		}
//
//		TreeItem kotManagerItem = new TreeItem("KOT MANAGER");
//		if (SystemSetting.UserHasRole("KOT MANAGER")) {
//
//			rootItem2.getChildren().add(kotManagerItem);
//		}
//
//		TreeItem storeCreationItem = new TreeItem("STORE CREATION");
//		if (SystemSetting.UserHasRole("STORE CREATION")) {
//
//			rootItem2.getChildren().add(storeCreationItem);
//		}
//		TreeItem dayEndItem = new TreeItem("DAY END");
//		if (SystemSetting.UserHasRole("DAY END")) {
//
//			rootItem2.getChildren().add(dayEndItem);
//		}
//
//		TreeItem siteCreationItem = new TreeItem("SITE CREATION");
//		if (SystemSetting.UserHasRole("SITE CREATION")) {
//
//			rootItem2.getChildren().add(siteCreationItem);
//		}
//
//		TreeItem financeCreationItem = new TreeItem("FINANCE CREATION");
//		if (SystemSetting.UserHasRole("FINANCE CREATION")) {
//
//			rootItem2.getChildren().add(financeCreationItem);
//		}
//
//		TreeItem voucherReprint = new TreeItem("BARCODE PRINTING");
//		if (SystemSetting.UserHasRole("BARCODE PRINTING")) {
//
//			rootItem2.getChildren().add(voucherReprint);
//		}
//		TreeItem barcodeConfig = new TreeItem("BARCODE CONFIGURATION");
//		if (SystemSetting.UserHasRole("BARCODE CONFIGURATION")) {
//
//			rootItem2.getChildren().add(barcodeConfig);
//		}
//
//		TreeItem ItemMerge = new TreeItem("ITEM MERGE");
//		if (SystemSetting.UserHasRole("ITEM MERGE")) {
//
//			rootItem2.getChildren().add(ItemMerge);
//		}
//
//		TreeItem saleOrderStatus = new TreeItem("SALE ORDER STATUS");
//		rootItem2.getChildren().add(saleOrderStatus);
//
//		TreeItem ReordertItem = new TreeItem("RE-ORDER");
//		if (SystemSetting.UserHasRole("RE-ORDER")) {
//
//			rootItem2.getChildren().add(ReordertItem);
//		}
//		TreeItem ReordertItemBranch = new TreeItem("ITEM BRANCH");
//		if (SystemSetting.UserHasRole("ITEM BRANCH")) {
//
//			rootItem2.getChildren().add(ReordertItemBranch);
//		}
//
//		TreeItem multiunitItem = new TreeItem("MULTI UNIT");
//		if (SystemSetting.UserHasRole("MULTI UNIT")) {
//
//			rootItem2.getChildren().add(multiunitItem);
//		}
//
//		TreeItem productConversion = new TreeItem("PRODUCT CONVERSION CONFIGURATION");
//		if (SystemSetting.UserHasRole("PRODUCT CONVERSION CONFIGURATION")) {
//
//			rootItem2.getChildren().add(productConversion);
//		}
//
//		TreeItem categoryExceptionList = new TreeItem("CATEGORY EXCEPTION LIST");
//		if (SystemSetting.UserHasRole("CATEGORY EXCEPTION LIST")) {
//
//			rootItem2.getChildren().add(categoryExceptionList);
//		}
//
//		TreeItem nutritionFacts = new TreeItem("NUTRITION FACTS");
//		if (SystemSetting.UserHasRole("NUTRITION FACTS")) {
//
//			rootItem2.getChildren().add(nutritionFacts);
//		}
//
//		TreeItem nutritionValue = new TreeItem("NUTRITION PRINT");
//		if (SystemSetting.UserHasRole("NUTRITION PRINT")) {
//
//			rootItem2.getChildren().add(nutritionValue);
//		}
//		TreeItem barcodenutritioncombined = new TreeItem("BARCODE NUTRITION COMBINED");
//		if (SystemSetting.UserHasRole("BARCODE NUTRITION COMBINED")) {
//
//			rootItem2.getChildren().add(barcodenutritioncombined);
//		}
//
//		TreeItem scheme = new TreeItem("SCHEME");
//		if (SystemSetting.UserHasRole("SCHEME")) {
//
//			rootItem2.getChildren().add(scheme);
//
//		}
//		TreeItem schemeDetails = new TreeItem("SCHEME DETAILS");
//		if (SystemSetting.UserHasRole("SCHEME DETAILS")) {
//			rootItem2.getChildren().add(schemeDetails);
//		}
//
//		TreeItem saleOrderEdit = new TreeItem("SALEORDER EDIT");
//		if (SystemSetting.UserHasRole("SALEORDER EDIT")) {
//			rootItem2.getChildren().add(saleOrderEdit);
//		}
//
//		TreeItem TaxInvoiceJasper = new TreeItem("TAX INVOICE BY PRICE TYPE");
//		if (SystemSetting.UserHasRole("TAX INVOICE BY PRICE TYPE")) {
//			rootItem2.getChildren().add(TaxInvoiceJasper);
//		}
//
//		TreeItem addSupplierItem = new TreeItem("ADD SUPPLIER");
//		if (SystemSetting.UserHasRole("ADD SUPPLIER")) {
//
//			rootItem2.getChildren().add(addSupplierItem);
//		}
//
//		TreeItem pricedefBATCHItem = new TreeItem("BATCH PRICE DEFINITION");
//		if (SystemSetting.UserHasRole("PRICE DEFINITION")) {
//			rootItem2.getChildren().add(pricedefBATCHItem);
//		}
//		TreeItem pricedefItem = new TreeItem("PRICE DEFINITION");
//		if (SystemSetting.UserHasRole("PRICE DEFINITION")) {
//			rootItem2.getChildren().add(pricedefItem);
//		}
//
//		TreeItem addSalesType = new TreeItem("VOUCHER TYPE");
//		if (SystemSetting.UserHasRole("VOUCHER TYPE")) {
//
//			rootItem2.getChildren().add(addSalesType);
//		}
//		TreeItem receiptmode = new TreeItem("RECEIPT_MODE");
//		if (SystemSetting.UserHasRole("RECEIPT_MODE")) {
//			rootItem2.getChildren().add(receiptmode);
//		}
//
//		TreeItem kitchenCategoryDtl = new TreeItem("KITCHEN CATEGORY DTL");
//		if (SystemSetting.UserHasRole("KITCHEN CATEGORY DTL")) {
//			rootItem2.getChildren().add(kitchenCategoryDtl);
//		}
//
//		TreeItem kotCatprinter = new TreeItem("KOT CATEGORY PRINTER");
//		if (SystemSetting.UserHasRole("KOT CATEGORY PRINTER")) {
//			rootItem2.getChildren().add(kotCatprinter);
//		}
//
//		TreeItem printer = new TreeItem("PRINTER");
//		if (SystemSetting.UserHasRole("PRINTER")) {
//			rootItem2.getChildren().add(printer);
//		}
//
//		TreeItem changePasswordCreationItem = new TreeItem("CHANGE PASSWORD");
//
//		rootItem2.getChildren().add(changePasswordCreationItem);
//
//		TreeItem userGroupPermissionItem = new TreeItem("USER-GROUP PERMISSION");
//		if (SystemSetting.UserHasRole("USER-GROUP PERMISSION")) {
//
//			rootItem2.getChildren().add(userGroupPermissionItem);
//		}
//
//		TreeItem groupPermissionItem = new TreeItem("GROUP PERMISSION");
//		if (SystemSetting.UserHasRole("GROUP PERMISSION")) {
//
//			rootItem2.getChildren().add(groupPermissionItem);
//		}
//
//		TreeItem groupCreationItem = new TreeItem("GROUP CREATION");
//		if (SystemSetting.UserHasRole("GROUP CREATION")) {
//
//			rootItem2.getChildren().add(groupCreationItem);
//		}
//
//		TreeItem processPermissionItem = new TreeItem("PROCESS PERMISSION");
//		if (SystemSetting.UserHasRole("PROCESS PERMISSION")) {
//
//			rootItem2.getChildren().add(processPermissionItem);
//		}
//
//		TreeItem processCreationItem = new TreeItem("PROCESS CREATION");
//		if (SystemSetting.UserHasRole("PROCESS CREATION")) {
//
//			rootItem2.getChildren().add(processCreationItem);
//		}
//
//		TreeItem multiCategoryUpdation = new TreeItem("MULTI CATEGORY UPDATION");
//		if (SystemSetting.UserHasRole("MULTI CATEGORY UPDATION")) {
//
//			rootItem2.getChildren().add(multiCategoryUpdation);
//		}
//
//		TreeItem machineResource = new TreeItem("MACHINE RESOURCE");
//		if (SystemSetting.UserHasRole("MACHINE RESOURCE")) {
//
//			rootItem2.getChildren().add(machineResource);
//		}
//		
//		TreeItem setTallyUrl = new TreeItem("SET TALLY URL");
//		if (SystemSetting.UserHasRole("SET TALLY URL")) {
//
//			rootItem2.getChildren().add(setTallyUrl);
//		}
//		
//		TreeItem subCategory = new TreeItem("RESOURCE CATEGORY");
//		if (SystemSetting.UserHasRole("RESOURCE CATEGORY")) {
//
//			rootItem2.getChildren().add(subCategory);
//		}
//		TreeItem resourceCatItemLink = new TreeItem("RESOURCE CATEGORY LINK");
//		if (SystemSetting.UserHasRole("RESOURCE CATEGORY LINK")) {
//
//			rootItem2.getChildren().add(resourceCatItemLink);
//		}
		TreeItem menuCreationItem = new TreeItem("MENU CREATION");
		if (SystemSetting.UserHasRole("MENU CREATION")) {

			rootItem2.getChildren().add(menuCreationItem);
		}

		TreeItem menuConfiguration = new TreeItem("MENU CONFIGURATION");
		if (SystemSetting.UserHasRole("MENU CONFIGURATION")) {

			rootItem2.getChildren().add(menuConfiguration);
		}

		TreeItem userRegistrationItem = new TreeItem("USER REGISTRATION");
		if (SystemSetting.UserHasRole("USER REGISTRATION")) {

			rootItem2.getChildren().add(userRegistrationItem);
		}

		TreeItem accountHeadsItem = new TreeItem("ADD ACCOUNT HEADS");
		if (SystemSetting.UserHasRole("ADD ACCOUNT HEADS")) {

			rootItem2.getChildren().add(accountHeadsItem);
		}

		TreeItem branchCreationItem = new TreeItem("BRANCH CREATION");
		if (SystemSetting.UserHasRole("BRANCH CREATION")) {

			rootItem2.getChildren().add(branchCreationItem);
		}
		TreeItem subscriptionItem = new TreeItem("SUBSCRIPTION VALIDITY");
		if (SystemSetting.UserHasRole("SUBSCRIPTION VALIDITY")) {

			rootItem2.getChildren().add(subscriptionItem);
		}
		TreeItem domaininitializeItem = new TreeItem("DOMAIN INITIALIZE");
		if (SystemSetting.UserHasRole("DOMAIN INITIALIZE")) {

			rootItem2.getChildren().add(domaininitializeItem);
		}
		

		
//
//		// version2.0
		// TreeItem financialYear = new TreeItem("FINANCIAL YEAR");
		// if (SystemSetting.UserHasRole("FINANCIAL YEAR")) {

		// rootItem2.getChildren().add(financialYear);
		// }
//		// version2.0 ends

		TreeItem companyCreationItem = new TreeItem("COMPANY CREATION");
		if (SystemSetting.UserHasRole("COMPANY CREATION")) {

			rootItem2.getChildren().add(companyCreationItem);
		}

		// TreeItem dbInitailize = new TreeItem("DB INITIALIZE");
//
//		rootItem2.getChildren().add(dbInitailize);
//
//		TreeItem executeSqlItem = new TreeItem("EXECUTE SQL");
//		if (SystemSetting.UserHasRole("EXECUTE SQL")) {
//
//			rootItem2.getChildren().add(executeSqlItem);
//		}

//	TreeItem voucherReprintItem = new TreeItem("VOUCHER REPRINT");
//	if (SystemSetting.UserHasRole("VOUCHER REPRINT")) {
//
//	rootItem2.getChildren().add(voucherReprintItem);
//	}

		treeview.setRoot(rootItem2);

		treeview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
				TreeItem<String> selectedItem = (TreeItem<String>) newValue;
				System.out.println("Selected Text : " + selectedItem.getValue());
				selectedText = selectedItem.getValue();
//				if (selectedItem.getValue().equalsIgnoreCase("KOT MANAGER")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.kotManagement);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("STORE CREATION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.StoreCreation);
//
//				}
//
//				// version2.0
				/*
				 * if (selectedItem.getValue().equalsIgnoreCase("FINANCIAL YEAR")) { if
				 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
				 * MapleclientApplication.mainWorkArea.getChildren().clear();
				 * 
				 * MapleclientApplication.mainWorkArea.getChildren()
				 * .add(MapleclientApplication.mainFrameController.financialYear);
				 * 
				 * }
				 */

//
//				// version2.0ends
//
//				if (selectedItem.getValue().equalsIgnoreCase("ITEM BRANCH")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.ItemBranch);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("ITEM EDIT")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.itemEdit);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("CATEGORY MANAGEMENT")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.categoryManagement);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("PRINTER")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.printer);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("ITEMWISE TAX CREATION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.itemWiseTaxCreation);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("SCHEME")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.Scheme);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("KIT DEFINITION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.kitde);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("VOUCHER TYPE")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.SalesType);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("PRICE DEFINITION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.PriceDefenition);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("KOT CATEGORY PRINTER")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.KotCateogry);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("PUBLISH MESSAGE")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.publishMessages);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("SCHEME DETAILS")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.SchemeDetails);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("SALEORDER EDIT")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.SaleOrderEdit);
//
//				}
//
				if (selectedItem.getValue().equalsIgnoreCase("TAX INVOICE BY PRICE TYPE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();
					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.taxInvoiceByPriceType);

				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("RECEIPT_MODE")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.ReceiptMode);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("SALE ORDER STATUS")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.SaleOrderStatusChecking);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("PRODUCT CONVERSION CONFIGURATION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.productConversionConfig);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("CATEGORY EXCEPTION LIST")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.categoryExceptionList);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("TAX CREATION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.Tax);
//
//				}
//
				if (selectedItem.getValue().equalsIgnoreCase("PROCESS CREATION")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.Process);
				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("MULTI CATEGORY UPDATION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.categoryUpdation);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("RESOURCE CATEGORY")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.subCategory);
//
//				}

				if (selectedItem.getValue().equalsIgnoreCase("SET TALLY URL")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.TallyUrl);

				}
//				if (selectedItem.getValue().equalsIgnoreCase("CHECK PRINT")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.checkPrint);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("RESOURCE CATEGORY LINK")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.resourceCatItemLink);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("MACHINE RESOURCE")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.machineResource);
//
//				}
				if (selectedItem.getValue().equalsIgnoreCase("USER REGISTRATION")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.userreg);

				}

				if (selectedItem.getValue().equalsIgnoreCase("USER REGISTRATION")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.userreg);

				}
//				if (selectedItem.getValue().equalsIgnoreCase("CHANGE PASSWORD")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.changepassword);
//
//				}

				if (selectedItem.getValue().equalsIgnoreCase("MENU CREATION")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.menuCreations);

				}

				if (selectedItem.getValue().equalsIgnoreCase("MENU CONFIGURATION")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.MenuConfig);

				}

				if (selectedItem.getValue().equalsIgnoreCase("BRANCH CREATION")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.BranchCreation);

				}
				if (selectedItem.getValue().equalsIgnoreCase("SUBSCRIPTION VALIDITY")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.subscriptionValidity);

				}
				
				if (selectedItem.getValue().equalsIgnoreCase("DOMAIN INITIALIZE")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.domainInitialize);

				}
				
				
				
				
				
				
				
//				if (selectedItem.getValue().equalsIgnoreCase("ITEM MERGE")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.ItemMerge);
//
//				}

//    if(selectedItem.getValue().equalsIgnoreCase( "VOUCHER REPRINT")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.VoucherReprint);
//    
//    	
//    }   
//    

//				if (selectedItem.getValue().equalsIgnoreCase("PROCESS PERMISSION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.ProcessPermission);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("GROUP PERMISSION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.GroupPermission);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("GROUP CREATION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.groupcreation);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("USER-GROUP PERMISSION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.UserGroupPermission);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("BATCH PRICE DEFINITION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.BatchPriceDefinition);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("RE-ORDER")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.ReOrder);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("MULTI UNIT")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.Multiunit);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("ITEM MASTER")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.ItemMstr);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("DAY END")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.DayEnd);
//
//				}

				if (selectedItem.getValue().equalsIgnoreCase("ADD ACCOUNT HEADS")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.AccountHeads);

				}

//				if (selectedItem.getValue().equalsIgnoreCase("ADD SUPPLIER")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.AddSupplier);
//
//				}
				if (selectedItem.getValue().equalsIgnoreCase("COMPANY CREATION")) {
					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
						MapleclientApplication.mainWorkArea.getChildren().clear();

					MapleclientApplication.mainWorkArea.getChildren()
							.add(MapleclientApplication.mainFrameController.CompanyCreations);

				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("EXECUTE SQL")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.ExecuteSql);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("SITE CREATION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.SiteCreation);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("KITCHEN CATEGORY DTL")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.kitchenCategoryDtl);
//
//				}
//				if (selectedItem.getValue().equalsIgnoreCase("FINANCE CREATION")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.finance);
//
//				}
//
//				if (selectedItem.getValue().equalsIgnoreCase("DB INITIALIZE")) {
//					if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//						MapleclientApplication.mainWorkArea.getChildren().clear();
//
//					MapleclientApplication.mainWorkArea.getChildren()
//							.add(MapleclientApplication.mainFrameController.dbInitialization);
//
//				}

//    if(selectedItem.getValue().equalsIgnoreCase( "VOUCHER REPRINT")) {
//    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//    		MapleclientApplication.mainWorkArea.getChildren().clear();
//
//    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.VoucherReprint);
//    
//    	
//    }
			}

		});

	}

	@FXML
	void cofigOnClick(MouseEvent event) {
		if (null != selectedText) {
//			if (selectedText.equalsIgnoreCase("KOT MANAGER")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.kotManagement);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("PRODUCT CONVERSION CONFIGURATION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.productConversionConfig);
//
//			}
//			if (selectedText.equalsIgnoreCase("CATEGORY EXCEPTION LIST")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.categoryExceptionList);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("ITEM BRANCH")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.ItemBranch);
//
//			}
//			if (selectedText.equalsIgnoreCase("SCHEME")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.Scheme);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("KIT DEFINITION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.kitde);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("ITEMWISE TAX CREATION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.itemWiseTaxCreation);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("PRICE DEFINITION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.PriceDefenition);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("PUBLISH MESSAGE")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.publishMessages);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("SCHEME DETAILS")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.SchemeDetails);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("SALEORDER EDIT")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.SaleOrderEdit);
//
//			}
//
//			// version2.0
			/*
			 * if (selectedText.equalsIgnoreCase("FINANCIAL YEAR")) { if
			 * (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
			 * MapleclientApplication.mainWorkArea.getChildren().clear();
			 * 
			 * MapleclientApplication.mainWorkArea.getChildren()
			 * .add(MapleclientApplication.mainFrameController.financialYear);
			 * 
			 * }
			 */

//			// version2.0ends
//			if (selectedText.equalsIgnoreCase("RECEIPT_MODE")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.ReceiptMode);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("SALE ORDER STATUS")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.SaleOrderStatusChecking);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("TAX CREATION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.Tax);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("CHECK PRINT")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.checkPrint);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("KOT CATEGORY PRINTER")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.KotCateogry);
//
//			}
//			if (selectedText.equalsIgnoreCase("PROCESS CREATION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.Process);
//
//			}
//			if (selectedText.equalsIgnoreCase("RESOURCE CATEGORY")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.subCategory);
//
//			}
//			if (selectedText.equalsIgnoreCase("RESOURCE CATEGORY LINK")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.resourceCatItemLink);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("MACHINE RESOURCE")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.machineResource);
//
//			}
//
			if (selectedText.equalsIgnoreCase("USER REGISTRATION")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.userreg);

			}
//
//			if (selectedText.equalsIgnoreCase("USER REGISTRATION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.userreg);
//
//			}
//			if (selectedText.equalsIgnoreCase("CHANGE PASSWORD")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.changepassword);
//
//			}
//			if (selectedText.equalsIgnoreCase("BARCODE PRINTING")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.barcodePrinting);
//
//			}
//			if (selectedText.equalsIgnoreCase("BARCODE CONFIGURATION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.barcodeConfiguration);
//
//			}

			if (selectedText.equalsIgnoreCase("MENU CREATION")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.menuCreations);

			}
			if (selectedText.equalsIgnoreCase("BRANCH CREATION")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.BranchCreation);

			}
			
			if (selectedText.equalsIgnoreCase("SUBSCRIPTION VALIDITY")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.subscriptionValidity);

			}
			
			if (selectedText.equalsIgnoreCase("DOMAIN INITIALIZE")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.domainInitialize);

			}
			
		
			
			
			
//
////    	    if(selectedItem.getValue().equalsIgnoreCase( "VOUCHER REPRINT")) {
////    	    	if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
////    	    		MapleclientApplication.mainWorkArea.getChildren().clear();
//			//
////    	    	MapleclientApplication.mainWorkArea.getChildren().add(MapleclientApplication.mainFrameController.VoucherReprint);
//			//
////    	    	
////    	    }   
//			//
//
//			if (selectedText.equalsIgnoreCase("PROCESS PERMISSION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.ProcessPermission);
//
//			}
//			if (selectedText.equalsIgnoreCase("ITEM EDIT")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.itemEdit);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("GROUP PERMISSION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.GroupPermission);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("GROUP CREATION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.groupcreation);
//
//			}
//			if (selectedText.equalsIgnoreCase("USER-GROUP PERMISSION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.UserGroupPermission);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("RE-ORDER")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.ReOrder);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("MULTI UNIT")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.Multiunit);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("ITEM MASTER")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.ItemMstr);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("DAY END")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.DayEnd);
//
//			}

			if (selectedText.equalsIgnoreCase("ADD ACCOUNT HEADS")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.AccountHeads);

			}

//			if (selectedText.equalsIgnoreCase("ADD SUPPLIER")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.AddSupplier);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("PRINTER")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.printer);
//
//			}
//			if (selectedText.equalsIgnoreCase("NUTRITION PRINT")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.NutritionValue);
//
//			}
//			if (selectedText.equalsIgnoreCase("BARCODE NUTRITION COMBINED")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.barcodenutritioncombined);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("NUTRITION FACTS")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.NutritionFacts);
//
//			}
			if (selectedText.equalsIgnoreCase("COMPANY CREATION")) {
				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
					MapleclientApplication.mainWorkArea.getChildren().clear();

				MapleclientApplication.mainWorkArea.getChildren()
						.add(MapleclientApplication.mainFrameController.CompanyCreations);

			}
//
//			if (selectedText.equalsIgnoreCase("EXECUTE SQL")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.ExecuteSql);
//
//			}
//
//			if (selectedText.equalsIgnoreCase("SITE CREATION")) {
//				if (!MapleclientApplication.mainWorkArea.getChildren().isEmpty())
//					MapleclientApplication.mainWorkArea.getChildren().clear();
//
//				MapleclientApplication.mainWorkArea.getChildren()
//						.add(MapleclientApplication.mainFrameController.SiteCreation);
//
//			}
		}
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}
}
