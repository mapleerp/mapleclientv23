package com.maple.mapleclient.controllers;

import java.time.LocalDate;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.util.Date;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.RawMaterialIssueDtl;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.RawMaterialIssueReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import net.sf.jasperreports.components.table.fill.FillTable;
import net.sf.jasperreports.engine.JRException;

public class RawMaterialIssueReportCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<RawMaterialIssueReport> rawMaterilaListTable = FXCollections.observableArrayList();
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;

    @FXML
    private TableView<RawMaterialIssueReport> tbRawMaterials;
    @FXML
    private TableColumn<RawMaterialIssueReport,LocalDate> clDate;

    @FXML
    private TableColumn<RawMaterialIssueReport, String> clItemName;

    @FXML
    private TableColumn<RawMaterialIssueReport, String> clBatch;

    @FXML
    private TableColumn<RawMaterialIssueReport, Number> clQty;

    @FXML
    private TableColumn<RawMaterialIssueReport, String> clUnit;
    @FXML
   	private void initialize() {
   		dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
   		dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
       }
    @FXML
    void showTable(ActionEvent event) {
    	Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String fsdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
	   	Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String tsdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
    	
    	ResponseEntity<List<RawMaterialIssueReport>> respentity = RestCaller.getRawMaterialBetweenDate(fsdate,tsdate);
    	rawMaterilaListTable = FXCollections.observableArrayList(respentity.getBody());
    	FillTable();

    }
    private void FillTable()
    {
    	tbRawMaterials.setItems(rawMaterilaListTable);
    	clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
    	clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
    	clItemName.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());
    	clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
    	clUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
    }
    @FXML
    void printreport(ActionEvent event) {
    	Date fdate = SystemSetting.localToUtilDate(dpFromDate.getValue());
		String fsdate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
	   	Date tdate = SystemSetting.localToUtilDate(dpToDate.getValue());
			String tsdate = SystemSetting.UtilDateToString(tdate, "yyyy-MM-dd");
		try {
			
			JasperPdfReportService.RawMaterialIssueBetweenDate(fsdate,tsdate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

    	
    }
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


  private void PageReload() {
  	
  }
}
