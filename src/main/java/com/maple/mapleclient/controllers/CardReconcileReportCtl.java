package com.maple.mapleclient.controllers;

import java.awt.CardLayout;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.CardReconcileReport;
import com.maple.report.entity.DayBook;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class CardReconcileReportCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<CardReconcileReport> CardReconcileReportList = FXCollections.observableArrayList();
	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();

    @FXML
    private DatePicker dpFromDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;
    @FXML
    private TextField txtReceiptAmount;
    @FXML
    private ComboBox<String> cmbReceiptMode;
    @FXML
    private TableView<CardReconcileReport> tblReceiptMode;
    @FXML
    private TableColumn<CardReconcileReport,String> clVoucherDate;

    @FXML
    private TableColumn<CardReconcileReport, String> clVoucherNumber;

    @FXML
    private TableColumn<CardReconcileReport, String> clReceiptMode;

    @FXML
    private TableColumn<CardReconcileReport, Number> clReceiptAmount;

    @FXML
    void actionPrint(ActionEvent event) {
    	

    	LocalDate dpFDate = dpFromDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpFDate);
		 String  strfDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 
		 
		 LocalDate dpTDate = dpToDate.getValue();
			java.util.Date utDate  = SystemSetting.localToUtilDate(dpTDate);
			 String  strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
			 try {
					JasperPdfReportService.CardReconcileReport(strfDate,strtDate,cmbReceiptMode.getSelectionModel().getSelectedItem());
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    }
    @FXML
	private void initialize() {
    	 setCardType() ;
    }
    @FXML
    void actionShow(ActionEvent event) {

    	
    	LocalDate dpFDate = dpFromDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpFDate);
		 String  strfDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 
		 
		 LocalDate dpTDate = dpToDate.getValue();
			java.util.Date utDate  = SystemSetting.localToUtilDate(dpTDate);
			 String  strtDate = SystemSetting.UtilDateToString(utDate, "yyyy-MM-dd");
			 
			 if(null == cmbReceiptMode.getSelectionModel().getSelectedItem())
			 {
				 notifyMessage(3,"Select receipt Mode",false);
				 cmbReceiptMode.requestFocus();
				 return;
			 }
	
		ResponseEntity<List<CardReconcileReport>> gstCardReconcile = RestCaller.getCardReconcileReport(strfDate,strtDate,cmbReceiptMode.getSelectionModel().getSelectedItem());
		CardReconcileReportList =FXCollections.observableArrayList(gstCardReconcile.getBody());
		fillTable();
    }
    private void fillTable()
    {
    	double receiptAmt =0.0;
    	tblReceiptMode.setItems(CardReconcileReportList);
		clReceiptAmount.setCellValueFactory(cellData -> cellData.getValue().getreceiptAmountProperty());
		clReceiptMode.setCellValueFactory(cellData -> cellData.getValue().getreceiptModeProperty());
		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getvoucherNumberProperty());
		for(CardReconcileReport cardReconcileReport:CardReconcileReportList)
		{
			receiptAmt = receiptAmt + cardReconcileReport.getReceiptAmount();
		}
		BigDecimal totalreceiptAmt = new BigDecimal(receiptAmt);
		totalreceiptAmt = totalreceiptAmt.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtReceiptAmount.setText(totalreceiptAmt.toPlainString());
    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

    private void setCardType() {
    	cmbReceiptMode.getItems().clear();

		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());

		for (ReceiptModeMst receiptModeMst : receiptModeList) {
			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
				cmbReceiptMode.getItems().add(receiptModeMst.getReceiptMode());
			}
		}
	}
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


       private void PageReload() {
       	
 }

}
