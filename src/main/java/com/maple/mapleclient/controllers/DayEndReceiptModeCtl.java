package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.springframework.http.ResponseEntity;

import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayEndProcessing;
import com.maple.report.entity.DayEndReceiptMode;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import net.sf.jasperreports.engine.JRException;

public class DayEndReceiptModeCtl {
	String taskid;
	String processInstanceId;

    @FXML
    private DatePicker dpDate;

    @FXML
    private Button btnPrint;

    @FXML
	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
    	
 }
    @FXML
    void actionPrint(ActionEvent event) {

    	LocalDate dpDate1 = dpDate.getValue();
		java.util.Date uDate  = SystemSetting.localToUtilDate(dpDate1);
		 String  strDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		 ResponseEntity<List<DayEndProcessing>> dayendReceiptMode = RestCaller.getDayEndReceiptMode(strDate);
			try {
				JasperPdfReportService.DayEndReceiptMode(dayendReceiptMode,strDate);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

    
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
