package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;
import org.bouncycastle.crypto.tls.NewSessionTicket;
import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.POSThermalPrintFactory;
import com.maple.javapos.print.PosTaxLayoutPrint;
import com.maple.maple.util.MapleConstants;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AddKotTable;
import com.maple.mapleclient.entity.AddKotWaiter;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.DamageDtl;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KOTItems;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.KotItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PatientMst;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.TableOccupiedMst;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.entity.WeighingItemMst;
import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.KotDeleteEvent;
import com.maple.mapleclient.events.KotFetchEvent;
import com.maple.mapleclient.events.KotFinalSaveEvent;
import com.maple.mapleclient.events.KotScreenRefresh;
import com.maple.mapleclient.events.TableOccupiedEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;
import com.maple.report.entity.TableWaiterReport;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.application.Platform;

public class KOTPosWindowCtl {

	String taskid;
	String processInstanceId;
	private static final Logger logger = LoggerFactory.getLogger(KOTPosWindowCtl.class);
	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();
	SalesReceipts salesReceipts = new SalesReceipts();
	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
	private ObservableList<SalesReceipts> salesReceiptsList = FXCollections.observableArrayList();
	private ObservableList<TableWaiterReport> tableWaiterList = FXCollections.observableArrayList();
	UnitMst unitMst = null;
	String invoiceNumberPrefix = SystemSetting.KOT_SALES_PREFIX;
	String fixedQty = null;
	POSThermalPrintABS printingSupport;
	Double rateBeforeDiscount = 0.0;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<AddKotTable> addKotTableList1 = FXCollections.observableArrayList();
	private ObservableList<AddKotWaiter> addKotWaiterList = FXCollections.observableArrayList();

	private ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleDtlObservableList = FXCollections.observableArrayList();

	private ObservableList<KOTItems> kotitemsObservableList = FXCollections.observableArrayList();
	SalesDtl salesDtl = null;
	SalesTransHdr salesTransHdr = null;

	boolean multi = false;
	double qtyTotal = 0;
	double amountTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;
	String custId = "";
	String salesReceiptVoucherNo = null;

	double cardAmount = 0.0;

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");
	StringProperty barcodeProperty = new SimpleStringProperty("");
	StringProperty taxRateProperty = new SimpleStringProperty("");
	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");
	String storeNameFromPopUp = null;

	KotScreenRefresh kOTItemsFetch = null;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private Button btnClearSelection;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private Label lblpaidAmount;

	@FXML
	private TextField txtposDiscount;

	@FXML
	private Label lblDiscount;

	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnMrp;

	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnCessRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

	@FXML
	private TableColumn<SalesDtl, Number> clAmount;

	@FXML
	private TableView<KOTItems> tblTableWaiter;

	@FXML
	private TableColumn<KOTItems, String> clTable;

	@FXML
	private TableColumn<KOTItems, String> clWaiter;

	@FXML
	private TextField txtTable;

	@FXML
	private Button btnAddTable;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private Button btnDeleterow;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnRefresh;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField custname;

	@FXML
	private TextField custAdress;

	@FXML
	private TextField gstNo;

	@FXML
	private Button btnPerformaInvoice;

	@FXML
	private ListView<String> servingtable;

	@FXML
	private ListView<String> salesman;

	@FXML
	private Button btnCardSale;

	@FXML
	private Label lblCardType;

	@FXML
	private Label lblCardAmount;

	// ------------------------new version 1.11 surya
	@FXML
	private TextField txtDescription;
	// ------------------------new version 1.11 surya end
	@FXML
	private Button btnClearTab;

	@FXML
	private Button btnClear;

	@FXML
	private TextField txtStore;

	@FXML
	void clearFromTab(ActionEvent event) {

		if (servingtable.getSelectionModel().isEmpty()) {
			notifyMessage(1, "Please Select Table!!!");
			return;

		} else if (salesman.getSelectionModel().isEmpty()) {
			notifyMessage(1, "Please Select Salesman!!!");
			return;
		}

		String salesMan = salesman.getSelectionModel().getSelectedItem().toString();
		String tableName = servingtable.getSelectionModel().getSelectedItem().toString();

		/*
		 * This method is not fully implemented, A resource is created in class
		 * MobileTabKotClearResource.
		 * 
		 */

	}

	@FXML
	void OnActionVisibleCardSale(ActionEvent event) {

		visibleCardSale();

	}

	private void visibleCardSale() {

		cmbCardType.setVisible(true);
		txtCrAmount.setVisible(true);
		AddCardAmount.setVisible(true);
		btnDeleteCardAmount.setVisible(true);
		tblCardAmountDetails.setVisible(true);
		txtcardAmount.setVisible(true);
		lblCardType.setVisible(true);
		lblCardAmount.setVisible(true);

	}

	@FXML
	private ComboBox<String> cmbCardType;

	@FXML
	void FocusOnCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCrAmount.requestFocus();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
		if (event.getCode() == KeyCode.RIGHT) {
			txtPaidamount.requestFocus();
		}
	}

	@FXML
	void setCardType(MouseEvent event) {

		setCardType();
	}

	@FXML
	private TextField txtCrAmount;

	@FXML
	void KeyPressFocusAddBtn(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (txtCrAmount.getText().length() == 0 && null == cmbCardType.getValue()) {
				btnSave.requestFocus();
			} else {

				AddCardAmount.requestFocus();
			}
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
		if (event.getCode() == KeyCode.RIGHT) {
			txtPaidamount.requestFocus();
		}
	}

	private void setCardType() {
		cmbCardType.getItems().clear();

		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());

		for (ReceiptModeMst receiptModeMst : receiptModeList) {
			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
				cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
			}
		}
	}

	@FXML
	private Button AddCardAmount;

	@FXML
	void KeyPressAddCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			addCardAmount();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
		if (event.getCode() == KeyCode.RIGHT) {
			txtPaidamount.requestFocus();
		}
	}

	@FXML
	void addCardAmount(ActionEvent event) {

		addCardAmount();

	}

	private void addCardAmount() {
		logger.info("KOTPOS====== add Card Amount started===========");

		if (null != salesTransHdr) {

			if (null == cmbCardType.getValue()) {

				notifyMessage(5, "Please select card type!!!!");
				cmbCardType.requestFocus();
				return;

			} else if (txtCrAmount.getText().trim().isEmpty()) {

				notifyMessage(1, "Please enter amount!!!!");
				txtCrAmount.requestFocus();
				return;

			} else {

				salesReceipts = new SalesReceipts();
				Double prcardAmount = 0.0;
				prcardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
				Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
				Double cardAmount = Double.parseDouble(txtCrAmount.getText());
				if ((prcardAmount + cardAmount) > invoiceAmount) {
					notifyFailureMessage(3, "Card Amount should be less than or equal to invoice Amount");
					return;
				}
				ResponseEntity<AccountHeads> accountHeads = RestCaller
						.getAccountHeadByName(cmbCardType.getSelectionModel().getSelectedItem());
				salesReceipts.setAccountId(accountHeads.getBody().getId());

				salesReceipts.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());
				salesReceipts.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
				salesTransHdr.setUserId(SystemSetting.getUserId());
				salesReceipts.setBranchCode(SystemSetting.systemBranch);
				if (null == salesReceiptVoucherNo) {
					salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
				} else {
					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
				}

				salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
				salesReceipts.setSalesTransHdr(salesTransHdr);
				System.out.println(salesReceipts);
				ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
				salesReceipts = respEntity.getBody();

				if (null != salesReceipts) {
					if (null != salesReceipts.getId()) {
						notifyMessage(1, "Successfully added cardAmount");
						salesReceiptsList.add(salesReceipts);
						filltableCardAmount();
					}
				}

				txtCrAmount.clear();
				cmbCardType.getSelectionModel().clearSelection();
				cmbCardType.requestFocus();
				salesReceipts = null;

				logger.info("KOTPOS======= ADD ITEM FINISHED");
			}
		} else {
			notifyMessage(1, "Please add Item!!!!");
			return;
		}
	}

	@FXML
	private Button btnDeleteCardAmount;

	@FXML
	void DeleteCardAmount(ActionEvent event) {
		logger.info("KOTPOS======== delete card amount");
		if (null != salesReceipts) {
			if (null != salesReceipts.getId()) {
				RestCaller.deleteSalesReceipts(salesReceipts.getId());
				tblCardAmountDetails.getItems().clear();
				ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
						.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
				salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

				try {
					cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
				} catch (Exception e) {

					txtcardAmount.setText("");
				}

				filltableCardAmount();

			}
		} else {

		}
		logger.info("KOTPOS========= delete card amount finished");

	}

	@FXML
	void KeyPressDeleteCardAmount(KeyEvent event) {

	}

	@FXML
	private TableView<SalesReceipts> tblCardAmountDetails;

	@FXML
	private TableColumn<SalesReceipts, String> clCardTye;

	@FXML
	private TableColumn<SalesReceipts, Number> clCardAmount;

	private void filltableCardAmount() {

		cmbCardType.setVisible(true);
		txtCrAmount.setVisible(true);
		AddCardAmount.setVisible(true);
		btnDeleteCardAmount.setVisible(true);
		tblCardAmountDetails.setVisible(true);
		txtcardAmount.setVisible(true);
		lblCardType.setVisible(true);
		lblCardAmount.setVisible(true);

		tblCardAmountDetails.setItems(salesReceiptsList);
		clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
		clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

		cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
		txtcardAmount.setText(Double.toString(cardAmount));

	}

	@FXML
	void Refresh(ActionEvent event) {
//		addTableWaiter();
		
//		ResponseEntity<List<KOTItems>> respentityList = RestCaller.getkotdtls();
//		kotitemsObservableList.clear();
//		kotitemsObservableList = FXCollections.observableArrayList(respentityList.getBody());
//		tblTableWaiter.setItems(kotitemsObservableList);
		addTableWaiter();
		
	}

	@FXML
	void onEnterCustPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadCustomerPopup();

		}
	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

		loadCustomerPopup();
	}

	@FXML
	void printonlyinvoice(ActionEvent event) {

		printPerforma();
	}

	private void printPerforma() {
		logger.info("KOTPOS ======== print invoice started");
		try {
			printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
			logger.info("KOTPOS ======== print invoice completed");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {
		showPopup();
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				addItem();

		}
		if (event.getCode() == KeyCode.LEFT) {

			txtBarcode.requestFocus();
		}
		if (event.getCode() == KeyCode.BACK_SPACE && txtQty.getText().length() == 0) {
			txtBarcode.requestFocus();
		}
		if (event.getCode() == KeyCode.DOWN) {

			txtPaidamount.requestFocus();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			showPopup();
		}
		if ((event.getCode() == KeyCode.K && event.isControlDown())) {
			btnPrintKot.requestFocus();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			txtPaidamount.requestFocus();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
	}

	@FXML
	void keyPressOnPrintKot(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			printKot();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
	}

	@FXML
	private Button btnPrintKot;
	private String CUSTOME_SALES_MODE;

	@FXML
	void PrintKot(ActionEvent event) {
		printKot();
	}

	private void printKot() {

		if (servingtable.getSelectionModel().isEmpty()) {
			notifyMessage(1, "Please Select Table!!!");
			return;

		} else if (salesman.getSelectionModel().isEmpty()) {
			notifyMessage(1, "Please Select Salesman!!!");
			return;
		}

		String salesMan = salesman.getSelectionModel().getSelectedItem().toString();
		String tableName = servingtable.getSelectionModel().getSelectedItem().toString();

		ResponseEntity<List<SalesTransHdr>> kotPrint = RestCaller.PrintKot(salesMan, tableName);

		servingtable.requestFocus();

	}

	@FXML
	private void initialize() {

		// ----------kot screen refresh-----------

		kotItemListInitialize();

		tblTableWaiter.requestFocus();
		logger.info("KOTPOS ======== initialization started");
		txtposDiscount.setVisible(false);
		lblDiscount.setVisible(false);

		ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("PosDiscount");
		if (null == getParam.getBody()) {
			txtposDiscount.setVisible(false);
			lblDiscount.setVisible(false);
		} else {
			if (getParam.getBody().getValue().equalsIgnoreCase("YES")) {
				txtposDiscount.setVisible(true);
				lblDiscount.setVisible(true);
			} else {
				txtposDiscount.setVisible(false);
				lblDiscount.setVisible(false);
			}
		}
		lblpaidAmount.setVisible(false);
		/*
		 * Load logo to printing support once
		 */
		printingSupport = POSThermalPrintFactory.getPOSThermalPrintABS();
		try {
			printingSupport.setupLogo();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			logger.info("KOTPOS ======== " + e1);
			e1.printStackTrace();
		}

		try {
			printingSupport.setupBottomline(SystemSetting.getPosInvoiceBottomLine());

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		setCardType();

		/*
		 * Create an instance of SalesDtl. SalesTransHdr entity will be refreshed after
		 * final submit. SalesDtl will be added on AddItem Function
		 * 
		 */
		addTableWaiter();
		salesDtl = new SalesDtl();
		/*
		 * Fieds with Entity property
		 */
		txtCashtopay.setStyle("-fx-text-inner-color: red;-fx-font-size: 24px;");

		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);

		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);

		salesman.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> ov, final String oldvalue, final String newvalue) {

				if (null == newvalue) {
					return;
				}
				if (newvalue.length() == 0) {
					return;
				}

				if (!itemDetailTable.getItems().isEmpty())
					itemDetailTable.getItems().clear();

				if ((null != newvalue) && null != salesTransHdr)
					salesTransHdr.setSalesManId(newvalue);
				String tableName = "";

				try {
					tableName = servingtable.getSelectionModel().getSelectedItem().toString();

					// ----------MAP-125-kot-in-memory-scroll ------------
					if (null != tableName) {

						KotFetchEvent kOTItemsFetch = new KotFetchEvent();
						kOTItemsFetch.setTables(tableName);
						kOTItemsFetch.setWaiter(newvalue);
						System.out.println("inside  salesman selection ............ event post");
						System.out.println(kOTItemsFetch);

						eventBus.post(kOTItemsFetch);

					}

					FillTable();
					// ------------MAP-125-kot-in-memory-scroll-------------------

				} catch (Exception e) {
					System.out.println(e.toString());
				}
				String salesMan = newvalue;
			}
		});

		servingtable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> ov, final String oldvalue, final String t1) {

				if (null == t1) {
					return;
				}
				/*
				 * Call clear observable list only if it is not empty
				 */
				if (!saleDtlObservableList.isEmpty())
					saleDtlObservableList.clear();

				if ((null != t1) && null != salesTransHdr)
					salesTransHdr.setServingTableName(t1);
				String salesMan = "";
				try {
					salesMan = salesman.getSelectionModel().getSelectedItem().toString();

					// ----------MAP-125-kot-in-memory-scroll ------------

					if (null != salesMan) {
						KotFetchEvent kOTItemsFetch = new KotFetchEvent();
						kOTItemsFetch.setTables(t1);
						kOTItemsFetch.setWaiter(salesMan);
						System.out.println("Inside Table selection ...................... event post");
						System.out.println(kOTItemsFetch);

						eventBus.post(kOTItemsFetch);

					}

					FillTable();
					// ------------MAP-125-kot-in-memory-scroll-------------------

				} catch (Exception e) {

				}
				String tableName = t1;

			}
		});

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null == newValue) {
					return;
				}
				if (newValue.length() == 0) {
					return;
				}
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?") || ((Double.parseDouble(newValue)) > 5000)) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null == newValue) {
					return;
				}
				if (newValue.length() == 0) {
					return;
				}
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null == newValue) {
					return;
				}
				if (newValue.length() == 0) {
					return;
				}
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPaidamount.setText(oldValue);
				}
			}
		});

		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null == newValue) {
					return;
				}
				if (newValue.length() == 0) {
					return;
				}
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashtopay.setText(oldValue);
				}
			}
		});

		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null == newValue) {
					return;
				}
				if (newValue.length() == 0) {
					return;
				}
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});

		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if (null == newValue) {
					return;
				}

				if ((txtcardAmount.getText().length() >= 0)) {

					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;

					}

					try {
						paidAmount = Double.parseDouble(txtPaidamount.getText());
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					try {
						cardPaid = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
					changeAmtProperty.set(newrate.toPlainString());
					if (newrate.doubleValue() < 0) {

						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
					} else {

						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
					}

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				if (null == newValue) {
					return;
				}

				if ((txtPaidamount.getText().length() >= 0)) {

					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;
					}

					try {
						cardPaid = Double.parseDouble(txtcardAmount.getText());
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					try {
						paidAmount = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
					changeAmtProperty.set(newrate.toPlainString());

					if (newrate.doubleValue() < 0) {

						txtChangeamount.setStyle("-fx-text-inner-color: red;-fx-font-size: 20px;");
					} else {

						txtChangeamount.setStyle("-fx-text-inner-color: green;-fx-font-size: 20px;");
					}

				}

				if (((String) newValue).length() == 0) {
					double paidAmount = 0.0;
					double cashToPay = 0.0;
					double cardPaid = 0.0;
					double sodexoAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {
						cashToPay = 0.0;
					}

					try {
						cardPaid = Double.parseDouble(txtcardAmount.getText());
					} catch (Exception e) {
						cardPaid = 0.0;
					}

					try {
						paidAmount = Double.parseDouble((String) newValue);
					} catch (Exception e) {
						paidAmount = 0.0;
					}

					BigDecimal newrate = new BigDecimal((paidAmount + cardPaid + sodexoAmt) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
					changeAmtProperty.set(newrate.toPlainString());
				}
			}
		});
		tblTableWaiter.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			// ---------------------version 6.6 surya
			salesDtl = null;
			// ---------------------version 6.6 surya end
			if (newSelection != null) {
				String tableName = newSelection.getTables();
				String Waiter = newSelection.getWaiter();

				for (int i = 0; i < salesman.getItems().size(); i++) {
					if (Waiter.equalsIgnoreCase(salesman.getItems().get(i))) {
						salesman.getSelectionModel().select(i);
						salesman.getFocusModel().focus(i);
						salesman.scrollTo(i);
					}
				}
				for (int i = 0; i < servingtable.getItems().size(); i++) {
					if (tableName.equalsIgnoreCase(servingtable.getItems().get(i))) {
						servingtable.getSelectionModel().select(i);
						servingtable.getFocusModel().focus(i);
						servingtable.scrollTo(i);
					}
				}
			}

			ResponseEntity<List<SalesTransHdr>> salesTransHdrResponse = RestCaller
					.getInProgressKOT(newSelection.getWaiter(), newSelection.getTables(), CUSTOME_SALES_MODE);
			List<SalesTransHdr> salesTransHdrKOT = salesTransHdrResponse.getBody();
			System.out.println(salesTransHdrResponse.toString());

			if (salesTransHdrKOT.size() == 0) {
				// Start new KOT
				startNewKOT();

			} else {
				getAllCardAmount();
			}

		});
		tblCardAmountDetails.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {

					if (newSelection != null) {
						if (null != newSelection.getId()) {

							salesReceipts = new SalesReceipts();
							salesReceipts.setId(newSelection.getId());
						}
					}
				});
		eventBus.register(this);

		itemDetailTable.setItems(saleDtlObservableList);

		/*
		 * ResponseEntity<List<TableWaiterReport>> tablewaiter = RestCaller
		 * .getAllocatedTable(SystemSetting.getUser().getBranchCode(),
		 * CUSTOME_SALES_MODE); tableWaiterList =
		 * FXCollections.observableArrayList(tablewaiter.getBody());
		 * tblTableWaiter.setItems(tableWaiterList);
		 * clTable.setCellValueFactory(cellData ->
		 * cellData.getValue().gettableNameProperty());
		 * clWaiter.setCellValueFactory(cellData ->
		 * cellData.getValue().getwaiterNameProperty());
		 */
		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {
					salesDtl = new SalesDtl();
					salesDtl.setId(newSelection.getId());
					txtBarcode.setText(newSelection.getBarcode());
					txtItemname.setText(newSelection.getItemName());
					txtBatch.setText(newSelection.getBatchCode());
					txtQty.setText(String.valueOf(newSelection.getQty()));
					txtRate.setText(String.valueOf(newSelection.getMrp()));
					if (null == newSelection.getKotDescription()) {
						txtDescription.setText("");
					} else {
						txtDescription.setText(newSelection.getKotDescription());
					}
					cmbUnit.getItems().clear();
					cmbUnit.getItems().add(newSelection.getUnitName());
					cmbUnit.getSelectionModel().select(0);

				}
			}
		});
		cmbUnit.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (null == newValue) {
					return;
				}
				if (newValue.length() == 0) {
					return;
				}
				ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
				ResponseEntity<UnitMst> getUnit = RestCaller
						.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
				ResponseEntity<MultiUnitMst> getMultiUnit = RestCaller
						.getMultiUnitbyprimaryunit(getItem.getBody().getId(), getUnit.getBody().getId());
				if (null != getMultiUnit.getBody()) {
					txtRate.setText(Double.toString(getMultiUnit.getBody().getPrice()));
				} else
					txtRate.setText(Double.toString(getItem.getBody().getStandardPrice()));
			}
		});
		logger.info("KOTPOS ======== initialization completed");
	}

	private void getAllCardAmount() {

		tblCardAmountDetails.getItems().clear();
		ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
				.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
		salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

		try {
			cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());

		} catch (Exception e) {

			txtcardAmount.setText("");
		}

		filltableCardAmount();
	}

	@FXML
	void actionClearSelection(ActionEvent event) {
		tblTableWaiter.getSelectionModel().clearSelection();
		salesman.getSelectionModel().clearSelection();
		servingtable.getSelectionModel().clearSelection();
		itemDetailTable.getItems().clear();
		salesTransHdr = null;
		saleListItemTable.clear();
		saleDtlObservableList.clear();
	}

	@FXML
	void tableOnClick(MouseEvent event) {
		// ---------------------version 6.6 surya
		salesDtl = null;
		// ---------------------version 6.6 surya end
		if (!saleDtlObservableList.isEmpty())
			saleDtlObservableList.clear();

		if ((null != servingtable.getSelectionModel().getSelectedItem()) && null != salesTransHdr)
			salesTransHdr.setServingTableName(servingtable.getSelectionModel().getSelectedItem());
		String salesMan = "";
		try {
			salesMan = salesman.getSelectionModel().getSelectedItem().toString();
		} catch (Exception e) {

		}
		String tableName = servingtable.getSelectionModel().getSelectedItem();
		//
		try {
			if (null == salesMan || null == tableName) {
				return;
			}

			KotFetchEvent kOTItemsFetch = new KotFetchEvent();
			kOTItemsFetch.setTables(tableName);
			kOTItemsFetch.setWaiter(salesMan);
			System.out.println("inside table On Click");
			eventBus.post(kOTItemsFetch);

			FillTable();
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
			System.out.println(e.getStackTrace());
		}

	}

	@FXML
	void waiterOnClick(MouseEvent event) {
		// ---------------------version 6.6 surya
		salesDtl = null;
		// ---------------------version 6.6 surya end
		if (!itemDetailTable.getItems().isEmpty())
			itemDetailTable.getItems().clear();

		if ((null != salesman.getSelectionModel().getSelectedItem()) && null != salesTransHdr)
			salesTransHdr.setSalesManId(salesman.getSelectionModel().getSelectedItem());
		String tableName = "";

		try {
			tableName = servingtable.getSelectionModel().getSelectedItem().toString();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		String salesMan = salesman.getSelectionModel().getSelectedItem();
		//
		KotFetchEvent kOTItemsFetch = new KotFetchEvent();
		kOTItemsFetch.setTables(tableName);
		kOTItemsFetch.setWaiter(salesMan);

		eventBus.post(kOTItemsFetch);
		System.out.println("inside waiter on click...........");

		FillTable();

	}

	@FXML
	void deleteRow(ActionEvent event) {
		logger.info("KOTPOS ========DELETE ITEM STARTED");

		if (null != salesDtl.getId()) {
			RestCaller.deleteSalesDtl(salesDtl.getId());

			// -------------------MAP-125------------

			KotDeleteEvent kotDeleteEvent = new KotDeleteEvent();
			ArrayList<SalesDtl> salesdtslist = new ArrayList<SalesDtl>();
			salesdtslist.add(salesDtl);
			kotDeleteEvent.setArrayofsalesdtl(salesdtslist);
			kotDeleteEvent.setSalesTransHdr(salesTransHdr);

			System.out.println("----------INSIDE DELETE METHODS -----------------");
			System.out.println(kotDeleteEvent);

			eventBus.post(kotDeleteEvent);

			FillTable();

			// -------------------MAP-125------------

			salesDtl = new SalesDtl();
			itemDetailTable.getItems().clear();
			notifyMessage(1, " Item Deleted Successfully");
			/*
			 * ResponseEntity<List<TableWaiterReport>> tablewaiter = RestCaller
			 * .getAllocatedTable(SystemSetting.systemBranch, CUSTOME_SALES_MODE);
			 * tableWaiterList = FXCollections.observableArrayList(tablewaiter.getBody());
			 * tblTableWaiter.setItems(tableWaiterList);
			 */
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtQty.clear();
			logger.info("KOTPOS ======== DELETE ITEM FINISHED");
		}

	}

	@FXML
	void hold(ActionEvent event) {

	}

	@FXML
	void EnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			txtQty.requestFocus();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}

	}

	@FXML
	void onTableKeyPress(KeyEvent event) {
		// ---------------------version 6.6 surya
		salesDtl = null;
		// ---------------------version 6.6 surya end
		if (event.getCode() == KeyCode.ENTER) {

			salesman.requestFocus();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}

	}

	@FXML
	void onWaiterOnEnter(KeyEvent event) {
		// ---------------------version 6.6 surya
		salesDtl = null;
		// ---------------------version 6.6 surya end

		if (event.getCode() == KeyCode.ENTER) {

			showPopup();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
	}

	@FXML
	void tblOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ESCAPE) {

			txtPaidamount.requestFocus();
		}
		if (event.getCode() == KeyCode.ENTER) {

			txtItemname.requestFocus();
		}
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
		if (event.getCode() == KeyCode.BACK_SPACE) {

			txtItemname.requestFocus();
			showPopup();
		}

		if (event.getCode() == KeyCode.ENTER) {
			barcodeExec();
		}

		else if (event.getCode() == KeyCode.DOWN) {
			itemDetailTable.requestFocus();
		}
	}

	private void barcodeExec() {

		txtQty.setText("1");

		if (null == txtBarcode) {
			return;

		}
		if (null == txtBarcode.getText()) {
			return;

		}
		String subBarcode = null;
		if (txtBarcode.getText().trim().length() >= 12) {
			String barcode1 = txtBarcode.getText();
			subBarcode = barcode1.substring(0, 7);
		}
		ArrayList items = new ArrayList();

		items = RestCaller.SearchStockItemByBarcode(txtBarcode.getText());

		if (SystemSetting.isNEGATIVEBILLING()) {
			items = RestCaller.ItemSearchByBarcode(txtBarcode.getText());
		} else {
			items = RestCaller.SearchStockItemByBarcode(txtBarcode.getText());
		}

		/*
		 * Integrating Weighing machine
		 */

		if (txtBarcode.getText().startsWith("21")) {
			if (null == subBarcode) {
				subBarcode = txtBarcode.getText();
			}
			ResponseEntity<WeighingItemMst> weighingItems = RestCaller.getWeighingItemMstByBarcode(subBarcode);
			if (null != weighingItems.getBody()) {
				items = RestCaller.SearchStockItemByBarcode(subBarcode);
				if (txtBarcode.getText().trim().length() > 7) {
					txtQty.setText(txtBarcode.getText().substring(7, 9) + "." + txtBarcode.getText().substring(9, 12));
				}
				txtBarcode.setText(subBarcode);
			}
		}
		/*
		 * For Items with barcode including batch
		 */

		if (txtBarcode.getText().trim().contains("-")) {
			String[] barcodearr = txtBarcode.getText().split("-", 2);
			String barcode = barcodearr[0];
			String batch = barcodearr[1];
			items = RestCaller.SearchStockItemByBarcodeAndBatch(barcode, batch);

		}

		if (items.size() > 1) {
			multi = true;

			showPopupByBarcode();

		} else {
			multi = false;

			String itemName = "";
			String barcode = "";
			Double mrp = 0.0;
			String unitname = "";
			Double tax = 0.0;
			String itemId = "";
			String unitId = "";
			Iterator itr = items.iterator();
			String batch = "";

			while (itr.hasNext()) {

				List element = (List) itr.next();
				itemName = (String) element.get(0);
				barcode = (String) element.get(1);
				unitname = (String) element.get(2);
				mrp = (Double) element.get(3);
				tax = (Double) element.get(6);
				itemId = (String) element.get(7);
				unitId = (String) element.get(8);
				batch = (String) element.get(9);

				if (null != itemName) {
					salesDtl = new SalesDtl();
					salesDtl.setBarcode(barcode);
					salesDtl.setUnitId(unitId);
					salesDtl.setMrp(mrp);
					salesDtl.setTaxRate(tax);
					salesDtl.setItemId(itemId);
					txtItemname.setText(itemName);
					txtBatch.setText(batch);
					txtRate.setText(Double.toString(mrp));

					cmbUnit.setValue(unitname);

					addItem();
				}

			}
		}

		txtBatch.clear();
		txtBarcode.requestFocus();

	}

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.K && event.isControlDown()) {
			printKot();
		}
		if (event.getCode() == KeyCode.P && event.isControlDown()) {
			printPerforma();
		}
		if (event.getCode() == KeyCode.ENTER) {

			try {
				finalSave();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}

		} else if (event.getCode() == KeyCode.LEFT) {

			visibleCardSale();

			cmbCardType.requestFocus();

		} else if (event.getCode() == KeyCode.UP) {
			txtQty.requestFocus();
		} else if (event.getCode() == KeyCode.BACK_SPACE) {
			if (txtPaidamount.getText().length() == 0) {
				txtBarcode.requestFocus();
			}

		} else if (event.getCode() == KeyCode.S && event.isControlDown()) {

			try {
				finalSave();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	@FXML
	void calcPaid(KeyEvent event) {
	}

	@FXML
	void unHold(ActionEvent event) {

	}

	private void FillTable() {
		itemDetailTable.setItems(saleDtlObservableList);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		columnCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());
		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());

		if (null != summary.getTotalAmount()) {

			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);

			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("");
		}

	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
		addItem();
	}

	private synchronized void startNewKOT() {
		salesTransHdr = new SalesTransHdr();

		/*
		 * new url for getting account heads instead of customer mst=========05/0/2022
		 */
//		ResponseEntity<CustomerMst> customerMstResponse = RestCaller.getCustomerByNameAndBarchCode("KOT",
//				SystemSetting.getUser().getBranchCode());
		ResponseEntity<AccountHeads> accountHeadsResponse = RestCaller.getAccountHeadsByNameAndBranchCode("KOT",SystemSetting.getUser().getBranchCode());
				AccountHeads accountHeads = accountHeadsResponse.getBody();

		if (null == accountHeads) {
			ResponseEntity<AccountHeads>  accountHeadsResp1 = RestCaller.getAccountHeadsByName("KOT");
			accountHeads = accountHeadsResp1.getBody();
		}

		salesTransHdr.setAccountHeads(accountHeads);
		salesTransHdr.setCustomiseSalesMode(CUSTOME_SALES_MODE);
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.getId();
		salesTransHdr.setBranchCode(SystemSetting.systemBranch);
		salesTransHdr.setCustomerId(accountHeads.getId());
		salesTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
		salesTransHdr.setCreditOrCash("CASH");
		salesTransHdr.setIsBranchSales("N");
		salesTransHdr.setSalesMode("KOT");
		salesTransHdr.setUserId(SystemSetting.getUserId());
		String servingTable = "";
		try {
			servingTable = servingtable.getSelectionModel().getSelectedItem().toString();
		} catch (Exception e) {

		}
		if (servingTable.length() == 0) {
			return;
		}
		salesTransHdr.setServingTableName(servingTable);

		String salesmanId = "";
		try {
			salesmanId = salesman.getSelectionModel().getSelectedItem().toString();
		} catch (Exception e) {

		}
		if (salesmanId.trim().length() == 0) {
			return;
		}
		salesTransHdr.setSalesManId(salesmanId);

		ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
		salesTransHdr = respentity.getBody();
		saveTableOccupiedMst(servingTable);
	}

	private void addItem() {

		// valid day checking
		// validation checking
		// multiunit checking

		if (SystemSetting.systemBranch.equalsIgnoreCase(null)) {
			notifyMessage(5, "Incorrect Branch");
			return;
		}

		boolean validDate = checkingForValidDay();

		if (!validDate) {
			return;
		}

		boolean validationChecking = validationChecking();

		if (!validationChecking) {
			return;
		}

		ResponseEntity<UnitMst> getUnitBYItem = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());

		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());

		if (!txtBarcode.getText().trim().isEmpty()) {
			if (!txtBarcode.getText().equalsIgnoreCase(getItem.getBody().getBarCode())) {
				notifyMessage(1, "Sorry... invalid item barcode!!!");
				txtBarcode.requestFocus();
				return;
			}

		}

		boolean multiunitCheching = multiunitCheching(getUnitBYItem.getBody(), getItem.getBody());

		if (!multiunitCheching) {
			return;
		}

		ArrayList items = new ArrayList();

		items = RestCaller.getSingleStockItemByName(txtItemname.getText(), txtBatch.getText());
		Double chkQty = 0.0;

		chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(getItem.getBody().getId(), txtBatch.getText(),
				storeNameFromPopUp);

		String itemId = null;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			List element = (List) itr.next();
			itemId = (String) element.get(7);
		}

		ResponseEntity<KitDefinitionMst> getKitMst = RestCaller.getKitByItemId(getItem.getBody().getId());

		if (null == salesTransHdr || null == salesTransHdr.getId()) {

			String servingTable = servingtable.getSelectionModel().getSelectedItem().toString();
			String salesmanId = salesman.getSelectionModel().getSelectedItem().toString();

			salesTransHdr = createSalesTransHdr(servingTable, salesmanId);
		}

		if (null == salesTransHdr) {
			return;
		}
		if (null != salesDtl && null != salesDtl.getId()) {

			salesDtlDeletion();
		}

		if (null == getKitMst.getBody()) {
			stockChecking(getItem.getBody(), salesTransHdr, chkQty, getUnitBYItem.getBody());
		}

		salesDtl = saveSalesDtl();

		if (null == salesDtl) {
			return;
		}

//		saleDtlObservableList.clear();

		// --add--------MAP-125-kot-in-memory-scroll------------

		KotFetchEvent kotFetchEvent = new KotFetchEvent();
		ArrayList<SalesDtl> salesdtslist = new ArrayList<SalesDtl>();
		salesdtslist.add(salesDtl);

		kotFetchEvent.setTables(servingtable.getSelectionModel().getSelectedItem().toString());
		kotFetchEvent.setWaiter(salesman.getSelectionModel().getSelectedItem().toString());
		kotFetchEvent.setArrayofsalesdtl(salesdtslist);
		kotFetchEvent.setSalesTransHdr(salesTransHdr);

		System.out.println("----------INSIDE ADD item-----------------");
		System.out.println(kotFetchEvent);

		// ----------MAP-125-kot-in-memory-scroll----------

		txtItemname.setText("");
		txtBarcode.setText("");
		txtQty.setText("");
		txtRate.setText("");
		txtBatch.setText("");
		txtBarcode.requestFocus();

		/*
		 * ResponseEntity<List<TableWaiterReport>> tablewaiter =
		 * RestCaller.getAllocatedTable(SystemSetting.systemBranch, CUSTOME_SALES_MODE);
		 * tableWaiterList = FXCollections.observableArrayList(tablewaiter.getBody());
		 * tblTableWaiter.setItems(tableWaiterList);
		 */
		salesDtl = new SalesDtl();
		txtItemname.requestFocus();
		txtQty.setText("");
		txtRate.setText("");
		salesman.getSelectionModel().clearSelection();
		servingtable.getSelectionModel().clearSelection();
		txtBatch.setText("");
		txtDescription.clear();

		txtStore.setText("");

		eventBus.post(kotFetchEvent);

		FillTable();

	}

	private SalesDtl saveSalesDtl() {

		salesDtl = new SalesDtl();
		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(txtItemname.getText());
		ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtItemname.getText());
		salesDtl.setItemId(getItem.getBody().getId());
		salesDtl.setBarcode(txtBarcode.getText().length() == 0 ? "" : txtBarcode.getText());

		String batch = txtBatch.getText().length() == 0 ? "NOBATCH" : txtBatch.getText();
		System.out.println(batch);
		salesDtl.setBatchCode(batch);
		Double qty = txtQty.getText().length() == 0 ? 0 : Double.parseDouble(txtQty.getText());
		
//	-----------------	kot id creation-------------------------
		java.util.Date date = new java.util.Date(); 
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date sqldate =SystemSetting.StringToSqlDateSlash(dateFormat.format(date), "yyyy-MM-dd");
		
		
		
		String kotidformat=dateFormat.format(date) +salesDtl.getItemId();
		
		String kotId = RestCaller.getVoucherNumber(kotidformat);
		
		kotId = kotId.replace(kotidformat, "");
		 
	 
		System.out.println("kotId =" + kotId);

		
		salesDtl.setKotId(kotId);
		
		

		// -------------------------------new version 1.11 surya

		if (!txtDescription.getText().trim().isEmpty()) {
			salesDtl.setKotDescription(txtDescription.getText());
		}

		// -------------------------------new version 1.11 surya end

		salesDtl.setQty(qty);

		Double mrpRateIncludingTax = 00.0;
		ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("PosDiscount");
		if (null != getParam.getBody()) {
			if (getParam.getBody().getValue().equalsIgnoreCase("YES")) {
				if (!txtposDiscount.getText().trim().isEmpty()) {
					calculateDiscount();
				}
			}
		}
		mrpRateIncludingTax = txtRate.getText().length() == 0 ? 0.0 : Double.parseDouble(txtRate.getText());

		salesDtl.setMrp(mrpRateIncludingTax);
		Double taxRate = 00.0;
		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName());
		ItemMst item = respsentity.getBody();
		salesDtl.setItemCode(item.getItemCode());
		salesDtl.setBarcode(item.getBarCode());
		salesDtl.setStandardPrice(item.getStandardPrice());

		salesDtl.setPrintKotStaus("N");
		if (null != item.getTaxRate()) {
			salesDtl.setTaxRate(item.getTaxRate());
			taxRate = item.getTaxRate();

		} else {
			salesDtl.setTaxRate(0.0);

		}
		/*
		 * Calculate Rate Before Tax
		 */

		salesDtl.setItemId(item.getId());
		if (null == cmbUnit.getValue()) {
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem());
			salesDtl.setUnitId(getUnit.getBody().getId());
			salesDtl.setUnitName(getUnit.getBody().getUnitName());
		} else {
			ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue());
			salesDtl.setUnitId(getUnit.getBody().getId());
			salesDtl.setUnitName(getUnit.getBody().getUnitName());
		}

		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());

		if (getTaxMst.getBody().size() > 0) {

			salesDtl = calculationWithTaxMst(getTaxMst, salesDtl, mrpRateIncludingTax);

		} else {

			salesDtl = calculationWithOutTaxMst(salesDtl, mrpRateIncludingTax, item, taxRate);

		}

		salesDtl.setAmount(qty * mrpRateIncludingTax);

		salesDtl = setCostPrice(salesDtl);

		// ===============added by anandu for the purpose of storewise stock
		// updation=====29-07-2021====
		if (!txtStore.getText().trim().isEmpty()) {
			salesDtl.setStore(txtStore.getText());
		}
		// ==========end=================

		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);

		salesDtl = respentity.getBody();

		return salesDtl;
	}

	private SalesDtl setCostPrice(SalesDtl salesDtl2) {

		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if (null != priceDefenitionMst) {
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");

			ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller.getBatchPriceDefinition(
					salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), salesDtl.getBatch(), sdate);
			if (null != batchpriceDef.getBody()) {
				salesDtl.setCostPrice(batchpriceDef.getBody().getAmount());
			} else {
				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();
				if (null != priceDefinition) {
					salesDtl.setCostPrice(priceDefinition.getAmount());
				}
			}
		}

		return salesDtl;

	}

	private SalesDtl calculationWithOutTaxMst(SalesDtl salesDtl, Double mrpRateIncludingTax, ItemMst item,
			Double taxRate) {

		if (null != item.getTaxRate()) {
			salesDtl.setTaxRate(item.getTaxRate());
			taxRate = item.getTaxRate();
		} else {
			salesDtl.setTaxRate(0.0);
		}
		Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
		salesDtl.setRate(rateBeforeTax);

		double cessRate = item.getCess();
		double cessAmount = 0.0;
		if (cessRate > 0) {

			rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
			salesDtl.setRate(rateBeforeTax);

			cessAmount = salesDtl.getQty() * salesDtl.getRate() * cessRate / 100;

		}

		salesDtl.setCessRate(cessRate);

		salesDtl.setCessAmount(cessAmount);

		salesDtl.setSgstTaxRate(taxRate / 2);

		salesDtl.setCgstTaxRate(taxRate / 2);

		salesDtl.setCgstAmount(salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		salesDtl.setSgstAmount(salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

		return salesDtl;
	}

	private SalesDtl calculationWithTaxMst(ResponseEntity<List<TaxMst>> getTaxMst, SalesDtl salesDtl,
			Double mrpRateIncludingTax) {

		for (TaxMst taxMst : getTaxMst.getBody()) {
			if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
				salesDtl.setIgstTaxRate(taxMst.getTaxRate());
				salesDtl.setTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
				salesDtl.setCgstTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
				salesDtl.setSgstTaxRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
				salesDtl.setCessRate(taxMst.getTaxRate());
			}
			if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
				salesDtl.setAddCessRate(taxMst.getTaxRate());
			}

		}
		Double rateBeforeTax = (100 * mrpRateIncludingTax)
				/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate());
		salesDtl.setRate(rateBeforeTax);
		BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
		salesDtl.setIgstAmount(igstAmount.doubleValue());
		BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
		salesDtl.setCgstAmount(CgstAmount.doubleValue());
		BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
		salesDtl.setSgstAmount(SgstAmount.doubleValue());
		BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
		salesDtl.setCessAmount(cessAmount.doubleValue());
		BigDecimal adcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
		salesDtl.setAddCessAmount(adcessAmount.doubleValue());

		return salesDtl;

	}

	private void stockChecking(ItemMst itemMst, SalesTransHdr salesTransHdr, Double chkQty, UnitMst unitMst) {

		Double itemsqty = 0.0;
		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),
				itemMst.getId(), txtBatch.getText());
		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
		if (saleListItemTable.size() > 1) {
			Double PrevQty = 0.0;
			for (SalesDtl saleDtl : saleListItemTable) {
				if (!saleDtl.getUnitId().equalsIgnoreCase(itemMst.getUnitId())) {
					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(), itemMst.getUnitId(),
							saleDtl.getQty());
				} else {
					PrevQty = saleDtl.getQty();
				}
				itemsqty = itemsqty + PrevQty;
			}
		} else {
			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemMst.getId(), txtBatch.getText());
		}
		if (!unitMst.getId().equalsIgnoreCase(itemMst.getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(itemMst.getId(), unitMst.getId(), itemMst.getUnitId(),
					Double.parseDouble(txtQty.getText()));
			System.out.println(conversionQty);
			if ((chkQty < itemsqty + conversionQty) && !SystemSetting.isNEGATIVEBILLING()) {
				notifyMessage(1, "Not in Stock!!!");
				txtQty.clear();
				txtItemname.clear();
				txtBarcode.clear();
				txtBatch.clear();
				txtRate.clear();
				txtBarcode.requestFocus();
				return;
			}
		} else if ((chkQty < itemsqty + Double.parseDouble(txtQty.getText())) && !SystemSetting.isNEGATIVEBILLING()) {
			txtQty.clear();
			txtItemname.clear();
			txtBarcode.clear();
			txtBatch.clear();
			txtRate.clear();
			txtBarcode.requestFocus();
			notifyMessage(1, "No Stock!!");
			return;
		}
	}

	private void salesDtlDeletion() {

		System.out.println("toDeleteSale.getId()" + salesDtl.getId());
		Alert a = new Alert(AlertType.CONFIRMATION);
		a.setHeaderText("Delete Item");
		a.setContentText("Are you sure you want to Delete Item");
		a.showAndWait().ifPresent((btnType) -> {
			if (btnType == ButtonType.OK) {
				RestCaller.deleteSalesDtl(salesDtl.getId());
			} else if (btnType == ButtonType.CANCEL) {

				return;

			}
		});
	}

	private SalesTransHdr createSalesTransHdr(String servingTable, String salesmanId) {

		salesTransHdr = new SalesTransHdr();

		/*
		 * new url for getting account heads instead of customer mst=========05/0/2022
		 */
//		ResponseEntity<CustomerMst> customerMstResponse = RestCaller.getCustomerByNameAndBarchCode("KOT",
//				SystemSetting.systemBranch);
//		CustomerMst customerMst = customerMstResponse.getBody();
		
		ResponseEntity<AccountHeads> accountHeadsResponse = RestCaller.getAccountHeadsByNameAndBranchCode("KOT",SystemSetting.systemBranch);
				AccountHeads accountHeads = accountHeadsResponse.getBody();

		if (null == accountHeads) {
			ResponseEntity<AccountHeads> accountHeadsResp1 = RestCaller.getAccountHeadsByName("KOT");
			accountHeads = accountHeadsResp1.getBody();
		}

		salesTransHdr.setAccountHeads(accountHeads);
		salesTransHdr.setUserId(SystemSetting.getUserId());
		salesTransHdr.setSalesMode("KOT");
		salesTransHdr.setInvoiceAmount(0.0);
		salesTransHdr.getId();
		salesTransHdr.setUserId(SystemSetting.getUserId());
		salesTransHdr.setBranchCode(SystemSetting.getSystemBranch());
		salesTransHdr.setVoucherDate(SystemSetting.getApplicationDate());
		salesTransHdr.setCustomerId(accountHeads.getId());
		salesTransHdr.setCreditOrCash("CASH");
		salesTransHdr.setIsBranchSales("N");
		salesTransHdr.setCustomiseSalesMode(CUSTOME_SALES_MODE);

		String kotNumber = RestCaller.getVoucherNumber("KOT-");
		salesTransHdr.setKotNumber(kotNumber);

		salesTransHdr.setServingTableName(servingTable);
		salesTransHdr.setSalesManId(salesmanId);

		ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
		salesTransHdr = respentity.getBody();
		saveTableOccupiedMst(servingTable);

		return salesTransHdr;
	}

	private boolean multiunitCheching(UnitMst unitMst, ItemMst itemMst) {

		ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(itemMst.getId(), unitMst.getId());
		if (!unitMst.getId().equalsIgnoreCase(itemMst.getUnitId())) {
			if (null == getmulti.getBody()) {
				notifyMessage(5, "Please Add the item in Multi Unit");
				return false;
			}
		}

		return true;

	}

	private boolean validationChecking() {

		if (servingtable.getSelectionModel().isEmpty()) {
			notifyMessage(1, "Please Select Table!!!");
			return false;

		}
		if (salesman.getSelectionModel().isEmpty()) {
			notifyMessage(1, "Please Select Salesman!!!");
			return false;
		}

		if (txtItemname.getText().trim().isEmpty()) {
			notifyMessage(1, "Please Select Item Name!!!");
			txtItemname.requestFocus();
			return false;
		}
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(1, "Please Type Quantity!!!");
			txtQty.requestFocus();
			return false;
		}
		if (txtBatch.getText().trim().isEmpty()) {
			notifyMessage(1, "Item Batch is not present!!!");
			txtQty.requestFocus();
			return false;
		}

		return true;
	}

	private boolean checkingForValidDay() {

		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.applicationDate);
		if (null != dayEndClosureHdr) {
			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return false;
				}
			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.applicationDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return false;
		}

		return true;

	}

	private void calculateDiscount() {

		Double discount = Double.parseDouble(txtposDiscount.getText());
		Double rate = Double.parseDouble(txtRate.getText());
		Double discountAmount = rate * (discount / 100);
		Double rateAfterDiscount = rate - discountAmount;
		BigDecimal bdrateAfterDiscount = new BigDecimal(rateAfterDiscount);
		bdrateAfterDiscount = bdrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
		txtRate.setText(bdrateAfterDiscount.toPlainString());

	}

	@FXML
	void tableToDelete(MouseEvent event) {

	}

	@FXML
	void addTableWaiter(MouseEvent event) {

	}

	@FXML
	void save(ActionEvent event) {
		/*
		 * Final Save
		 */

		try {
			finalSave();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void RefreshTable1(SalesTransHdr hdr) {
		// Get List of Items by HdrId.
		// Create the Table.
		// Populate the table
		// Set the header

		boolean hasChildRecords = false;
		hasChildRecords = true;

		salesTransHdr = hdr;
		if (hasChildRecords) {
			setSummaryOfInvoice();
		}

	}

	private void setSummaryOfInvoice() {
		Summary summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());

		BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
		bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
		if (summary.getTotalAmount() > 0) {
			txtCashtopay.setText(bdCashToPay.toPlainString());
		} else {
			txtCashtopay.setText("0");
		}

	}

	private void showPopup() {
		try {

			if (SystemSetting.isNEGATIVEBILLING()) {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));

				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				txtQty.requestFocus();
			} else {

				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
				Parent root1;
				ItemStockPopupCtl itemStockPopupCtl = fxmlLoader.getController();
				itemStockPopupCtl.windowName = "KOT";
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				txtQty.requestFocus();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void saveTableOccupiedMst(String servingTable) {
		ResponseEntity<TableOccupiedMst> tableOccupied = RestCaller.getTableOccupiedMstbyTable(servingTable);
		if (null != tableOccupied.getBody()) {
			TableOccupiedMst tableOccupiedMst = tableOccupied.getBody();
			tableOccupiedMst.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
			java.util.Date date = new java.util.Date();
			long timeoccupied = date.getTime();
			Timestamp ts = new Timestamp(timeoccupied);
			tableOccupiedMst.setOrderedTime(ts);
			tableOccupiedMst.setSalesTransHdr(salesTransHdr);
			RestCaller.updateTableOccupiedMst(tableOccupiedMst);
			TableOccupiedEvent tableOccupiedEvent = new TableOccupiedEvent();
			tableOccupiedEvent.setOrderedTime(tableOccupiedMst.getOrderedTime());
			tableOccupiedEvent.setTableId(servingTable);
			tableOccupiedEvent.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
			eventBus.post(tableOccupiedEvent);
		} else if (null == tableOccupied.getBody()) {
			TableOccupiedMst tableOccupiedMst = new TableOccupiedMst();
			tableOccupiedMst.setTableId(servingTable);
			java.util.Date date = new java.util.Date();
			long timeoccupied = date.getTime();
			Timestamp ts = new Timestamp(timeoccupied);
			tableOccupiedMst.setOccupiedTime(ts);
			tableOccupiedMst.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
			tableOccupiedMst.setSalesTransHdr(salesTransHdr);
			tableOccupiedMst.setOrderedTime(ts);
			ResponseEntity<TableOccupiedMst> respentity = RestCaller.saveTableOccupiedMst(tableOccupiedMst);
			tableOccupiedMst = respentity.getBody();
			TableOccupiedEvent tableOccupiedEvent = new TableOccupiedEvent();
			tableOccupiedEvent.setOrderedTime(tableOccupiedMst.getOrderedTime());
			tableOccupiedEvent.setTableId(servingTable);
			tableOccupiedEvent.getOccupiedTime();
			tableOccupiedEvent.setWaiterId(salesman.getSelectionModel().getSelectedItem().toString());
			eventBus.post(tableOccupiedEvent);
		}
	}

	@Subscribe
	public void popupStockItemlistner(ItemPopupEvent itemPopupEvent) {
		try {
			if (null == txtItemname) {
				return;
			}

			if (null == txtItemname.getScene()) {
				return;
			}
			ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(itemPopupEvent.getItemName());
			ItemMst item = new ItemMst();
			item = getItem.getBody();

			Stage stage = (Stage) txtItemname.getScene().getWindow();
			if (stage.isShowing()) {

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (!multi) {

							if (null == salesDtl) {
								salesDtl = new SalesDtl();
							}
							itemNameProperty.set(itemPopupEvent.getItemName());
							salesDtl.setItemName(itemNameProperty.get());

							if (null == itemPopupEvent.getBatch()) {
								batchProperty.set("NOBATCH");
							} else {
								batchProperty.set(itemPopupEvent.getBatch());
							}

							if (null != itemPopupEvent.getStoreName()) {
								storeNameFromPopUp = itemPopupEvent.getStoreName();
								txtStore.setText(itemPopupEvent.getStoreName());
							} else {
								storeNameFromPopUp = "MAIN";
							}

							if (null != itemPopupEvent.getAddCessRate()) {
								salesDtl.setAddCessRate(itemPopupEvent.getAddCessRate());
							} else {
								salesDtl.setAddCessRate(0.0);
							}

							salesDtl.setBatchCode(batchProperty.get());

							barcodeProperty.set(itemPopupEvent.getBarCode());

							salesDtl.setBarcode(barcodeProperty.get());

							salesDtl.setCessRate(itemPopupEvent.getCess());
							cmbUnit.getItems().add(itemPopupEvent.getUnitName());
							cmbUnit.setValue(itemPopupEvent.getUnitName());
							salesDtl.setCgstTaxRate(itemPopupEvent.getCgstTaxRate());

							if (null != itemPopupEvent.getExpiryDate()) {
								salesDtl.setExpiryDate(itemPopupEvent.getExpiryDate());
							}

							salesDtl.setItemCode(itemPopupEvent.getItemCode());

							salesDtl.setItemTaxaxId(itemPopupEvent.getItemTaxaxId());

							mrpProperty.set(itemPopupEvent.getMrp() + "");
							if (null == mrpProperty) {
								return;
							}
							salesDtl.setMrp(Double.parseDouble(mrpProperty.get()));
							salesDtl.setSgstTaxRate(itemPopupEvent.getSgstTaxRate());

							taxRateProperty.set(itemPopupEvent.getTaxRate() + "");
							salesDtl.setTaxRate(Double.parseDouble(taxRateProperty.get()));
							ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller
									.getMultiUnitByItemId(itemPopupEvent.getItemId());

							multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());
							if (!multiUnitList.isEmpty()) {

								for (MultiUnitMst multiUniMst : multiUnitList) {

									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								}
							}
							ResponseEntity<TaxMst> taxMst = RestCaller
									.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
							if (null != taxMst.getBody()) {
							}

							System.out.println(itemPopupEvent.toString());
							multi = false;
						}

						else if (multi) {
							itemNameProperty.set(itemPopupEvent.getItemName());
							salesDtl.setItemName(itemNameProperty.get());

							batchProperty.set(itemPopupEvent.getBatch());

							if (null != itemPopupEvent.getAddCessRate()) {
								salesDtl.setAddCessRate(itemPopupEvent.getAddCessRate());
							} else {
								salesDtl.setAddCessRate(0.0);
							}

							if (null == itemPopupEvent.getBatch()) {
								batchProperty.set("NOBATCH");
							} else {
								batchProperty.set(itemPopupEvent.getBatch());
							}

							if (null != itemPopupEvent.getStoreName()) {
								storeNameFromPopUp = itemPopupEvent.getStoreName();
								txtStore.setText(itemPopupEvent.getStoreName());
							} else {
								storeNameFromPopUp = "MAIN";
							}

							cmbUnit.getItems().add(itemPopupEvent.getUnitName());
							cmbUnit.setValue(itemPopupEvent.getUnitName());
							barcodeProperty.set(itemPopupEvent.getBarCode());

							salesDtl.setBarcode(barcodeProperty.get());

							salesDtl.setCessRate(itemPopupEvent.getCess());

							salesDtl.setCgstTaxRate(itemPopupEvent.getCgstTaxRate());

							if (null != itemPopupEvent.getExpiryDate()) {
								salesDtl.setExpiryDate(itemPopupEvent.getExpiryDate());
							}

							salesDtl.setItemCode(itemPopupEvent.getItemCode());

							salesDtl.setItemTaxaxId(itemPopupEvent.getItemTaxaxId());

							mrpProperty.set(itemPopupEvent.getMrp() + "");
							salesDtl.setMrp(Double.parseDouble(mrpProperty.get()));
							salesDtl.setSgstTaxRate(itemPopupEvent.getSgstTaxRate());

							taxRateProperty.set(itemPopupEvent.getTaxRate() + "");
							salesDtl.setTaxRate(Double.parseDouble(taxRateProperty.get()));
							ResponseEntity<List<MultiUnitMst>> multiUnit1 = RestCaller
									.getMultiUnitByItemId(itemPopupEvent.getItemId());

							multiUnitList = FXCollections.observableArrayList(multiUnit1.getBody());
							if (!multiUnitList.isEmpty()) {

								for (MultiUnitMst multiUniMst : multiUnitList) {

									ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
									cmbUnit.getItems().add(getUnit.getBody().getUnitName());
								}
							}
							ResponseEntity<TaxMst> taxMst = RestCaller
									.getTaxMstByItemIdAndTaxId(itemPopupEvent.getItemId(), "IGST");
							if (null != taxMst.getBody()) {
							}

							System.out.println(itemPopupEvent.toString());
							txtQty.setText("1");
							addItem();
							multi = false;
						}

					}
				});

			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_LEFT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	public void addTableWaiter() {
		
		servingtable.getItems().clear();
		salesman.getItems().clear();

		ResponseEntity<List<AddKotTable>> tableSaved = RestCaller.getTablebyStatus();
		addKotTableList1 = FXCollections.observableArrayList(tableSaved.getBody());
		for (AddKotTable addkot : addKotTableList1) {
			servingtable.getItems().add(addkot.getTableName());
		}
		ResponseEntity<List<AddKotWaiter>> waiterSaved = RestCaller.getWaiterbyStatus();
		addKotWaiterList = FXCollections.observableArrayList(waiterSaved.getBody());
		for (AddKotWaiter addkotwaiter : addKotWaiterList) {
			salesman.getItems().add(addkotwaiter.getWaiterName());
		}
	
		
		
	}

	private synchronized void finalSave() throws SQLException {

		if (null != salesTransHdr) {

			if (null == SystemSetting.systemBranch) {
				notifyMessage(5, "Pleace login whith proper user!");
			}

			boolean finalStockStatus = finalStockVerification();

			if (!finalStockStatus) {
				return;
			}

			Double cashPaid = 0.0;
			Double cardAmount = 0.0;
			Double cashToPay = 0.0;
			Double sodexoAmount = 0.0;
			Double changeAmount = 0.0;

			try {
				cashToPay = Double.parseDouble(txtCashtopay.getText());

			} catch (Exception e) {

			}

			if (!txtPaidamount.getText().trim().isEmpty()) {
				cashPaid = Double.parseDouble(txtPaidamount.getText());
				if (cashToPay < (cardAmount + cashPaid)) {
					cashPaid = cashToPay - cardAmount;
				}

			}
			Double invoiceAmount = Double.parseDouble(txtCashtopay.getText());
			Double AmountTenderd = 0.0;
			Double CashPaid = 0.0;

			if (!txtPaidamount.getText().isEmpty()) {
				AmountTenderd = Double.parseDouble(txtPaidamount.getText());
			}

			Double CardAmount = 0.0;

			if (!txtcardAmount.getText().isEmpty()) {
				CardAmount = Double.parseDouble(txtcardAmount.getText());

			}
			Double totalAmountTenderd = AmountTenderd + CardAmount;
			if (totalAmountTenderd >= invoiceAmount) {
				CashPaid = invoiceAmount - CardAmount;

			} else if (totalAmountTenderd < invoiceAmount) {

				notifyMessage(5, "Please Enter Valid Amount!!!!");

				return;
			}

			salesTransHdr.setBranchCode(SystemSetting.systemBranch);
			salesTransHdr.setCardamount(cardAmount);
			salesTransHdr.setCashPay(cashPaid);
			salesTransHdr.setPaidAmount(cashPaid);
			salesTransHdr.setChangeAmount(changeAmount);
			salesTransHdr.setSodexoAmount(sodexoAmount);
			salesTransHdr.setInvoiceAmount(cashToPay);
			salesReceipts = saveCashSalesReceipts(salesTransHdr, cashToPay, CashPaid);
			salesTransHdr.setSalesMode("KOT");
			salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
			salesTransHdr.setSalesReceiptsVoucherNumber(salesReceiptVoucherNo);

			if (null == salesTransHdr.getVoucherNumber()) {
				RestCaller.updateSalesTranshdr(salesTransHdr);
				notifyMessage(1, " Saved Successfully");
			}

			/*
			 * ResponseEntity<List<TableWaiterReport>> tablewaiter = RestCaller
			 * .getAllocatedTable(SystemSetting.systemBranch, CUSTOME_SALES_MODE);
			 * tableWaiterList = FXCollections.observableArrayList(tablewaiter.getBody());
			 * tblTableWaiter.setItems(tableWaiterList);
			 */

			salesTransHdr = RestCaller.getSalesTransHdr(salesTransHdr.getId());
			String hdrId = salesTransHdr.getId();

			printingRoutine(hdrId);

			// -----------MAP-125-------------------

			KotFinalSaveEvent kotFinalSaveEvent = new KotFinalSaveEvent();
			kotFinalSaveEvent.setSalesTransHdr(salesTransHdr);
			System.out.println("----------inside finalsave kot details-----------------");
			System.out.println(kotFinalSaveEvent);
			eventBus.post(kotFinalSaveEvent);

			// -----------MAP-125-------------------

			saleDtlObservableList.clear();

			clearFields();

		}

	}

	private SalesReceipts saveCashSalesReceipts(SalesTransHdr salesTransHdr, Double cashToPay, Double cashPaid) {

		SalesReceipts salesReceipts = new SalesReceipts();

		salesReceipts.setReceiptAmount(cashToPay);

		ResponseEntity<AccountHeads> accountHeads = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-" + "CASH");

		salesReceipts.setAccountId(accountHeads.getBody().getId());
		salesReceipts.setReceiptMode(salesTransHdr.getBranchCode() + "-" + "CASH");

		salesReceipts.setReceiptAmount(cashPaid);

		salesTransHdr.setUserId(SystemSetting.getUserId());
		salesReceipts.setBranchCode(SystemSetting.systemBranch);

		salesReceipts.setReceiptDate(SystemSetting.getApplicationDate());
		salesReceipts.setSalesTransHdr(salesTransHdr);
		System.out.println(salesReceipts);
		ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
		salesReceipts = respEntity.getBody();

		return salesReceipts;

	}

	private void clearFields() {
		salesTransHdr = null;
		salesDtl = new SalesDtl();
		txtcardAmount.setText("");
		txtCashtopay.setText("");
		txtChangeamount.clear();
		txtPaidamount.setText("");
		saleDtlObservableList.clear();
		tblCardAmountDetails.getItems().clear();
		salesReceiptsList.clear();

		txtItemname.clear();
		txtBarcode.clear();
		txtBatch.clear();
		txtQty.clear();
		txtRate.clear();
		cmbUnit.getSelectionModel().clearSelection();
		cmbCardType.setVisible(false);
		txtCrAmount.setVisible(false);
		AddCardAmount.setVisible(false);
		btnDeleteCardAmount.setVisible(false);
		tblCardAmountDetails.setVisible(false);
		txtcardAmount.setVisible(false);
		lblCardType.setVisible(false);
		lblCardAmount.setVisible(false);
		servingtable.requestFocus();
		txtDescription.clear();
	}

	private void printingRoutine(String hdrId) {

		try {
			printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean finalStockVerification() {
		String stockVerification = RestCaller.StockVerificationBySalesTransHdr(salesTransHdr.getId());
		
		logger.info(".........Stock checking reply......" + stockVerification);
		if (!stockVerification.equalsIgnoreCase(MapleConstants.STOCKOK)) {
			logger.info(stockVerification + " not equal to " + MapleConstants.STOCKOK);
			notifyMessage(5, stockVerification);
			return false;
		} else {
			return true;
		}
	}

	private void showPopupByBarcode() {
		try {

			if (SystemSetting.isNEGATIVEBILLING()) {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
				Parent root1;
				String cst = null;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				ItemPopupCtl popctl = fxmlLoader.getController();
				popctl.LoadItemPopupBySearch(txtBarcode.getText());

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} else {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/stockItemPopup.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				ItemStockPopupCtl popctl = fxmlLoader.getController();
				popctl.LoadItemPopupByBarcode(txtBarcode.getText());

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Stock Item");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void notifyFailureMessage(int duration, String msg) {

		Image img = new Image("failed.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_LEFT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	@FXML
	void addTable(ActionEvent event) {
		AddKotTable addKotTable = new AddKotTable();
		String table = txtTable.getText().trim();

		table = table.replaceAll("/", "-");
		table = table.replaceAll("\"", "-");
		table = table.replaceAll(" ", "-");
		addKotTable.setTableName(table);
		addKotTable.setStatus("ACTIVE");
		addKotTable.setBranchCode(SystemSetting.systemBranch);
		String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "KMT");
		addKotTable.setId(vNo);
		ResponseEntity<AddKotTable> savedTable = RestCaller.getTablebyName(table);
		if (null != savedTable.getBody()) {
			addKotTable = savedTable.getBody();
			addKotTable.setStatus("ACTIVE");
			ResponseEntity<AddKotTable> reponse = RestCaller.saveAddKotTable(addKotTable);

		} else {
			ResponseEntity<AddKotTable> reponse = RestCaller.saveAddKotTable(addKotTable);
			addKotTable = reponse.getBody();
		}
		notifyMessage(5, "'/' will be replaced with '-'");
		addTableWaiter();
		txtTable.clear();
	}

	public String setCustomerVoucherProperty(String voucher) {
		this.CUSTOME_SALES_MODE = voucher;
		return this.CUSTOME_SALES_MODE;

	}

	public void loadInpProgressKot() {
		/*
		 * ResponseEntity<List<TableWaiterReport>> tablewaiter = RestCaller
		 * .getAllocatedTable(SystemSetting.getUser().getBranchCode(),
		 * CUSTOME_SALES_MODE); tableWaiterList =
		 * FXCollections.observableArrayList(tablewaiter.getBody());
		 * tblTableWaiter.setItems(tableWaiterList);
		 * clTable.setCellValueFactory(cellData ->
		 * cellData.getValue().gettableNameProperty());
		 * clWaiter.setCellValueFactory(cellData ->
		 * cellData.getValue().getwaiterNameProperty());
		 */
	}

	@Subscribe

	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

	@FXML
	void clearAll(ActionEvent event) {

		txtTable.clear();
		servingtable.getItems().clear();
		salesman.getItems().clear();
		tblTableWaiter.getItems().clear();
		txtItemname.clear();
		txtBarcode.clear();
		txtQty.clear();
		cmbUnit.getItems().clear();
		txtBatch.clear();
		txtDescription.clear();
		txtposDiscount.clear();
		itemDetailTable.getItems().clear();
		txtCashtopay.clear();
		txtPaidamount.clear();
		txtChangeamount.clear();
		tblCardAmountDetails.getItems().clear();
		txtCrAmount.clear();
		cmbCardType.getItems().clear();
		multiUnitList.clear();
		txtRate.clear();
		salesReceipts = null;
		receiptModeList = null;
		tableWaiterList = null;
		unitMst = null;
		fixedQty = null;
		rateBeforeDiscount = 0.0;
		addKotTableList1.clear();
		addKotWaiterList.clear();
		saleListItemTable.clear();
		saleDtlObservableList.clear();

		salesDtl = null;
		salesTransHdr = null;

		multi = false;
		qtyTotal = 0;
		amountTotal = 0;
		discountTotal = 0;
		taxTotal = 0;
		cessTotal = 0;
		discountBfTaxTotal = 0;
		grandTotal = 0;
		expenseTotal = 0;
		custId = "";
		salesReceiptVoucherNo = null;

		cardAmount = 0.0;

		notifyMessage(3, "Cleared...!");

	}

	// =========================get kotitems list at initial
	// stage========================
	public void kotItemListInitialize() {

		ResponseEntity<List<KOTItems>> respentityList = RestCaller.getkotdtls();
		kotitemsObservableList.clear();
		kotitemsObservableList = FXCollections.observableArrayList(respentityList.getBody());
		tblTableWaiter.setItems(kotitemsObservableList);
		clTable.setCellValueFactory(cellData -> cellData.getValue().getTablesProperty());
		clWaiter.setCellValueFactory(cellData -> cellData.getValue().getWaiterProperty());
		return;

	}

	@Subscribe
	public void addKotDetail(KotScreenRefresh kotScreenRefresh) {

		System.out.println("INSIDE addKotDetail**************************");

		System.out.println(kotScreenRefresh);

		// Get Sales trans hdr based on table waiter

		salesTransHdr = getSalesTransHdrByTableWaiter(kotScreenRefresh.getTables(), kotScreenRefresh.getWaiter());

		if (null == salesTransHdr) {
			salesTransHdr = createSalesTransHdr(kotScreenRefresh.getTables(), kotScreenRefresh.getWaiter());

		}

		saleDtlObservableList.clear();

		for (KOTItems kot : kotitemsObservableList) {

			if (kot.getSalesTransHdr().getId().equals(salesTransHdr.getId())) {

				ArrayList<SalesDtl> saleDtlArray = (kot.getArrayofsalesdtl());
				saleDtlObservableList.setAll(saleDtlArray);

			}

		}

	}

	@Subscribe
	public void fetchEventSubscriber(KotFetchEvent kotFetchEvent) {

		System.out.println(".......inside fetchEventSubscriber.........");

		System.out.println(kotFetchEvent);

		// Get Sales trans hdr based on table waiter
		salesTransHdr = getSalesTransHdrByTableWaiter(kotFetchEvent.getTables(), kotFetchEvent.getWaiter());

		if (null == salesTransHdr) {
			salesTransHdr = createSalesTransHdr(kotFetchEvent.getTables(), kotFetchEvent.getWaiter());

		}

		ResponseEntity<List<KOTItems>> respentityList = RestCaller.getkotdtls();
		kotitemsObservableList.clear();
		kotitemsObservableList = FXCollections.observableArrayList(respentityList.getBody());
		saleDtlObservableList.clear();

		for (KOTItems kot : kotitemsObservableList) {

			if (kot.getSalesTransHdr().getId().equals(salesTransHdr.getId())) {

				ArrayList<SalesDtl> saleDtlArray = (kot.getArrayofsalesdtl());
				saleDtlObservableList.setAll(saleDtlArray);

			}

		}

	}

	@Subscribe
	public void deleteKotDetails(KotDeleteEvent kotDeleteEvent) {

		System.out.println(".........inside deleteKotDetails..............");

		for (KOTItems kot : kotitemsObservableList) {
			if (kot.getSalesTransHdr().getId().equals(kotDeleteEvent.getSalesTransHdr().getId())) {

				ArrayList<SalesDtl> salesArray = kot.getArrayofsalesdtl();

				for (int i = 0; i < salesArray.size(); i++) {
					SalesDtl salesDtlToCheck = salesArray.get(i);

					if (null != kotDeleteEvent.getArrayofsalesdtl().get(i)) {

						if (kotDeleteEvent.getArrayofsalesdtl().get(i).getId()
								.equalsIgnoreCase(salesDtlToCheck.getId())) {
							salesArray.remove(i);
						}
					}

				}
				saleDtlObservableList.setAll(kot.getArrayofsalesdtl());

				FillTable();

			}
		}
	}

	@Subscribe
	public void finalsaveKotDetails(KotFinalSaveEvent kotFinalSaveEvent) {

		System.out.println(".................inside finalsaveKotDetails..................");
		for (KOTItems kot : kotitemsObservableList) {

			if (kot.getSalesTransHdr().getId().equals(kotFinalSaveEvent.getSalesTransHdr().getId())) {

				kotitemsObservableList.remove(kot);

			}

		}
	}

	private SalesTransHdr getSalesTransHdrByTableWaiter(String table, String Waiter) {
		SalesTransHdr salesTransHdr = null;
		for (KOTItems kot : kotitemsObservableList) {
			if (kot.getTables().equalsIgnoreCase(table) && kot.getWaiter().equalsIgnoreCase(Waiter)) {
				salesTransHdr = kot.getSalesTransHdr();

			}

		}
		return salesTransHdr;

	}

}