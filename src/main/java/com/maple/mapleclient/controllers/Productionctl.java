package com.maple.mapleclient.controllers;

import java.math.BigDecimal;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import java.sql.Date;
import java.text.Format;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.controlsfx.control.Notifications;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.ibm.icu.text.SimpleDateFormat;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.ProductionDtl;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.entity.ProductionMst;
import com.maple.mapleclient.entity.StockTransferInHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;
import javafx.scene.control.ComboBox;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.converter.LocalDateStringConverter;
import net.sf.jasperreports.engine.JRException;

public class Productionctl {

	String taskid;
	String processInstanceId;
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	ItemMst itemmst = new ItemMst();
	ProductionMst productionMst = null;
	ProductionDtl productionDtl = null;
	ProductionDtlDtl productionDtlDtl = null;
	UnitMst unitmst = null;
	private ObservableList<ProductionDtl> productionDtlList = FXCollections.observableArrayList();
	private ObservableList<ProductionDtlDtl> productionDtlDtlList = FXCollections.observableArrayList();

	private ObservableList<KitDefinitionDtl> kitDefinitionDtlList = FXCollections.observableArrayList();
	double Qty = 0.0;
	double minQty = 0.0;
	double reqQty = 0.0;
	
	@Value("${PRODUCTIONWITHOUTPLANNING}")
	private String PRODUCTIONWITHOUTPLANNING;
	
	@FXML
	private Button btnDelete;
	
	@FXML
	private TextField txtKitName;

	@FXML
	private Button btnPartiallySaved;

	@FXML
	private Button btnShowReport;

	@FXML
	private Button btnAllRawMaterialReq;

	@FXML
	private Button btnPrintReport;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnSaleOrderToProduction;
	
	@FXML
	private DatePicker dpProductionDate;
	
	@FXML
	private TextField txtQty;

	@FXML
	private TableView<ProductionDtl> tblProduction1;

	@FXML
	private TableColumn<ProductionDtl, String> clKitname;

	@FXML
	private TableColumn<ProductionDtl, String> clBatch;
	
	@FXML
	private Button btnPrintRawMaterial;
	
	@FXML
	private TableColumn<ProductionDtl, Number> clQty;

	@FXML
	private TableColumn<ProductionDtl, String> columnId;

	@FXML
	private TableView<ProductionDtlDtl> tblProduction2;
	
	@FXML
	private TableColumn<ProductionDtlDtl, String> clDtlDtlMaterail;

	@FXML
	private TableColumn<ProductionDtlDtl, Number> clDtlDtlQty;

	@FXML
	private TableColumn<ProductionDtlDtl, String> clDtDtlUnit;
	
	@FXML
	private Button btnAdd;

	@FXML
	private ComboBox<String> cmbStore;

	@FXML
	void BatchKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtQty.requestFocus();
		}
	}

	@FXML
	private void initialize() {
		dpProductionDate = SystemSetting.datePickerFormat(dpProductionDate, "dd/MMM/yyyy");
		eventBus.register(this);
		dpProductionDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.systemDate));
		dpProductionDate.setEditable(false);
//		txtBatch.setText(dpProductionDate.getValue().toString());
		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});
		dpProductionDate.valueProperty().addListener(new ChangeListener<LocalDate>() {
			@Override
			public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue,
					LocalDate newValue) {
				txtBatch.setText(dpProductionDate.getValue().toString());

			}

		});
		tblProduction1.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					productionDtl = new ProductionDtl();
					productionDtl.setId(newSelection.getId());
					productionDtl.setStatus(newSelection.getStatus());
					clDtDtlUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
					clDtlDtlMaterail.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());

					clDtlDtlQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
					ResponseEntity<List<ProductionDtlDtl>> productiondtldtlSaved = RestCaller
							.getProductionDtlDtl(productionDtl.getId());
					productionDtlDtlList = FXCollections.observableArrayList(productiondtldtlSaved.getBody());
					for (ProductionDtlDtl proddtldtl : productionDtlDtlList) {
						try {
							BigDecimal bdQty = new BigDecimal(proddtldtl.getQty());
							bdQty = bdQty.setScale(3, BigDecimal.ROUND_CEILING);
							proddtldtl.setQty(bdQty.doubleValue());
							ResponseEntity<UnitMst> respunit = RestCaller.getunitMst(proddtldtl.getUnitId());
							unitmst = respunit.getBody();
						} catch (Exception e) {
							return;
						}
						proddtldtl.setUnitName(unitmst.getUnitName());
						// productionDtlDtl.setUnitName(unitmst.getUnitName());
						// productionDtlDtlList.set(5,proddtldtl );
					}
					tblProduction2.setItems(productionDtlDtlList);
//							productionDtl = new ProductionDtl();
//							productionDtlDtl = new ProductionDtlDtl();

					// filltable();
//						productionDtl = null;
				}
			}
		});

		ResponseEntity<List<StoreMst>> storeMstListResp = RestCaller.getAllStoreMst();
		List<StoreMst> storeMstList = storeMstListResp.getBody();

		cmbStore.getSelectionModel().select("MAIN");

		for (StoreMst storeMst : storeMstList) {
			cmbStore.getItems().add(storeMst.getShortCode());
		}
	}

	@FXML
	void allRawMaterialReq(ActionEvent event) {

		java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
		// actualProductionHdr.setVoucherDate(uDate);
		// ResponseEntity<ActualProductionHdr> respentityHdr =
		// RestCaller.saveActualProductionHdr(actualProductionHdr);
		// actualProductionHdr = respentityHdr.getBody();
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);
		ResponseEntity<List<ProductionDtlDtl>> respentity = RestCaller.getProductionSummary(voucherDate);
		productionDtlDtlList = FXCollections.observableArrayList(respentity.getBody());
		clDtDtlUnit.setCellValueFactory(cellData -> cellData.getValue().getunitNameProperty());
		clDtlDtlMaterail.setCellValueFactory(cellData -> cellData.getValue().getitemNameProperty());

		clDtlDtlQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());

		for (ProductionDtlDtl proddtldtl : productionDtlDtlList) {
			try {

				ResponseEntity<UnitMst> respunit = RestCaller.getunitMst(proddtldtl.getUnitId());
				unitmst = respunit.getBody();
				proddtldtl.setUnitName(unitmst.getUnitName());
			} catch (Exception e) {
				return;
			}

			// productionDtlDtl.setUnitName(unitmst.getUnitName());
			// productionDtlDtlList.set(5,proddtldtl );

		}

		tblProduction2.setItems(productionDtlDtlList);

	}

	@FXML
	void printReport(ActionEvent event) {
		java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
		// actualProductionHdr.setVoucherDate(uDate);
		// ResponseEntity<ActualProductionHdr> respentityHdr =
		// RestCaller.saveActualProductionHdr(actualProductionHdr);
		// actualProductionHdr = respentityHdr.getBody();
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);
		try {
			JasperPdfReportService.ProductionDtlReport(voucherDate, SystemSetting.systemBranch);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void showReport(ActionEvent event) {

		showReport();
	}

	@FXML
	void actionPartiallySavedFetch(ActionEvent event) {
		tblProduction1.getItems().clear();
		ResponseEntity<ProductionMst> productionmstresp = RestCaller
				.getPartiallySavedProduction(dpProductionDate.getValue().toString());
		if (null != productionmstresp.getBody()) {
			productionMst = productionmstresp.getBody();
			ResponseEntity<List<ProductionDtl>> getProductionDtl = RestCaller.getProductionDtlsbyMstId(productionMst);

			productionDtlList = FXCollections.observableArrayList(getProductionDtl.getBody());

			if (!productionDtlList.isEmpty()) {
				for (ProductionDtl proddtl : productionDtlList) {
					ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(proddtl.getItemId());
					itemmst = respitem.getBody();
					proddtl.setKitname(itemmst.getItemName());
					// productionDtlDtl.setUnitName(unitmst.getUnitName());
					// productionDtlDtlList.set(5,proddtldtl );
				}

				filltable();
			}
		}
	}

	@FXML
	void deleteItem(ActionEvent event) {

		if (null == productionDtl) {
			return;
		}
		if (null == productionDtl.getId()) {
			notifyMessage(5, "Select Item to Delete");
			return;
		}

		if (productionDtl.getStatus().equalsIgnoreCase("Y")) {
			notifyMessage(5, "Can't Delete Item , Actual Production Done!!!");
			return;
		}
		RestCaller.deleteProductionDtls(productionDtl.getId());
		notifyMessage(5, "Item Deleted");
//	    		tblProduction1.getItems().clear();
//	    		tblProduction2.getItems().clear();
//	    		productionDtlList.clear();
//	    		ResponseEntity<List<ProductionDtl>> productionDtlSaved = RestCaller.getProductionDtlsbyMstId(productionMst);
//				productionDtlList = FXCollections.observableArrayList(productionDtlSaved.getBody());
//				if(!productionDtlList.isEmpty())
//				{
//				for (ProductionDtl proddtl : productionDtlList) {
//					ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(proddtl.getItemId());
//					itemmst = respitem.getBody();
//					proddtl.setKitname(itemmst.getItemName());
//					// productionDtlDtl.setUnitName(unitmst.getUnitName());
//					// productionDtlDtlList.set(5,proddtldtl );
//				}
//				if (null != productionDtlList) {
//					tblProduction1.setItems(productionDtlList);
//				}
		showReport();
		productionDtl = null;
	}

	@FXML
	void PrintRawMaterial(ActionEvent event) {
		LocalDate ldate = dpProductionDate.getValue();
		java.util.Date uDate = SystemSetting.localToUtilDate(ldate);
		String date = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");
		try {
			JasperPdfReportService.ProductionRawMaterialReport(date, SystemSetting.systemBranch);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void addItem(ActionEvent event) {

		if (txtKitName.getText().trim().isEmpty()) {
			notifyMessage(5, " Please Select Kit name!!!");
			txtKitName.requestFocus();
			return;
		}
		if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, " Type Quantity");
			txtQty.requestFocus();
			return;
		}
		if (null == cmbStore.getValue()) {
			notifyMessage(5, "Please select store");
			cmbStore.requestFocus();
			return;
		}
		
		
		ResponseEntity<ProductionMst> productionmstresp = RestCaller
				.getPartiallySavedProduction(dpProductionDate.getValue().toString());
		if (null != productionmstresp.getBody()) {
			productionMst = productionmstresp.getBody();
		}
		if (null == productionMst) {
			productionMst = new ProductionMst();
			productionMst.setVoucherDate(Date.valueOf(dpProductionDate.getValue()));

			productionMst.setBranchCode(SystemSetting.getSystemBranch());
			
			productionMst.setStore("MAIN");
			if(null != cmbStore.getValue())
			{
				productionMst.setStore(cmbStore.getSelectionModel().getSelectedItem().toString());
			}
			
			
			ResponseEntity<ProductionMst> respentity = RestCaller.saveProductionMst(productionMst);
			productionMst = respentity.getBody();
		}

		productionDtl = new ProductionDtl();
		
		productionDtl.setStore(cmbStore.getSelectionModel().getSelectedItem().toString());
		productionDtl.setStatus("N");
		productionDtl.setProductionMst(productionMst);
		productionDtl.setBatch(txtBatch.getText());
		productionDtl.setQty(Double.parseDouble(txtQty.getText()));
		Qty = Double.parseDouble(txtQty.getText());

		ResponseEntity<ItemMst> repitem = RestCaller.getItemByNameRequestParam(txtKitName.getText());
		itemmst = repitem.getBody();

//				ResponseEntity<ItemMst> repitemmst = RestCaller.getitemMst(txtKitName.getText());
//				itemmst = repitemmst.getBody();
		productionDtl.setItemId(itemmst.getId());
		ResponseEntity<KitDefinitionMst> respkit = RestCaller.getKitDefinitionMst(productionDtl.getItemId());
		if (null != respkit.getBody()) {
			KitDefinitionMst kitdefinitionMst = new KitDefinitionMst();
			kitdefinitionMst = respkit.getBody();
			minQty = kitdefinitionMst.getMinimumQty();

			ResponseEntity<ProductionDtl> respentity = RestCaller.saveProductionDtl(productionDtl);
			productionDtl = respentity.getBody();

			ResponseEntity<List<KitDefinitionDtl>> kitdefinitiondtlSaved = RestCaller
					.getKidefinitionDtl(kitdefinitionMst);
			kitDefinitionDtlList = FXCollections.observableArrayList(kitdefinitiondtlSaved.getBody());
			for (KitDefinitionDtl kit : kitDefinitionDtlList) {
				productionDtlDtl = new ProductionDtlDtl();

				productionDtlDtl.setItemName(kit.getItemName());

//						reqQty = (kit.getQty() / minQty);
//						
//						double qty = reqQty * Qty;
				double q = (kit.getQty() * Qty);
				BigDecimal bdQty1 = new BigDecimal(q);
				bdQty1 = bdQty1.setScale(3, BigDecimal.ROUND_CEILING);
				double qty = bdQty1.doubleValue() / minQty;
				BigDecimal bdQty = new BigDecimal(qty);
				bdQty = bdQty.setScale(3, BigDecimal.ROUND_CEILING);
				// qty =
				productionDtlDtl.setQty(bdQty.doubleValue());
				productionDtlDtl.setRawMaterialItemId(kit.getItemId());
				productionDtlDtl.setUnitId(kit.getUnitId());
				productionDtlDtl.setProductionDtl(productionDtl);
				System.out.println(productionDtlDtl.getProductionDtl().getId());

				ResponseEntity<ProductionDtlDtl> respentitydtldtl = RestCaller.saveProductionDtlDtl(productionDtlDtl);
				productionDtlDtl = respentitydtldtl.getBody();
				// productionDtlDtlList.add(productionDtlDtl);

			}

			productionDtl.setKitname(itemmst.getItemName());

			productionDtlList.add(productionDtl);
			txtKitName.clear();
			txtQty.clear();

			filltable();

			// filltable2();

		} else

			notifyMessage(5, " Please Select Valid Kit name!!!");

		Qty = 0.0;
		minQty = 0.0;
//		txtBatch.setText(dpProductionDate.getValue().toString());
	}

	@FXML
	void clearfileds(ActionEvent event) {
		tblProduction1.getItems().clear();
		tblProduction2.getItems().clear();
//		txtBatch.setText("");
		txtKitName.setText("");
		txtQty.setText("");

	}

	@FXML
	void FinalSubmit(ActionEvent event) {
		/*-Regy 22/08/2020.
		 * MFP could not complete a Plan SWEET BUN ROLL 40. Final save was failing.
		 * if window was opened just to do final submit,  how productionMst is set.
		 *this will result in null pointer.
		 *
		 */
		if (null != productionMst) {
			ResponseEntity<List<ProductionDtl>> productionDtlsaved = RestCaller.getProductionDtls();
			if (productionDtlsaved.getBody().size() == 0) {
				return;
			}
			String financialYear = SystemSetting.getFinancialYear();
			String sNo = RestCaller.getVoucherNumber(financialYear + "STON");
			productionMst.setVoucherNumber(sNo);

			try {
				RestCaller.updateProductionMst(productionMst);

			} catch (Exception e) {
				// if(PRODUCTIONWITHOUTPLANNING.equalsIgnoreCase("YES"))
				{
					notifyMessage(3, "Raw Materials Not In Stock");
					return;
				}
			}

//		for (ProductionDtl productionDtl : productionDtlList) {
//			String dtlId = productionDtl.getId();
//
//			Object obj = RestCaller.makeProductionByDtlId(dtlId);
//		}

			productionMst = null;
			productionDtl = null;
			productionDtlList.clear();
			productionDtlDtlList.clear();
			tblProduction1.getItems().clear();
//		txtBatch.setText("");
			txtQty.setText("");
//		txtBatch.setText("");
			tblProduction2.getItems().clear();
			notifyMessage(5, "Saved");
		} else {
			notifyMessage(3, "Fetch from partially saved");
		}
	}

	public String rawMaterialCheck(ProductionDtl prodDtl) {
		String items = null;
		ResponseEntity<List<ProductionDtlDtl>> getProdDtlDtl = RestCaller.getProductionDtlDtl(prodDtl.getId());
		for (ProductionDtlDtl prodDtlDtl : getProdDtlDtl.getBody()) {
			double sysQty = 0.0;
			Map<String, Double> map = new HashMap<String, Double>();
			map = RestCaller.getRequiredMaterial(prodDtlDtl.getRawMaterialItemId(), prodDtlDtl.getQty());
			if (map.size() == 0) {
				if (null == items) {
					items = "";
				}
				items = items + "," + prodDtlDtl.getItemName();
			} else {
				for (Map.Entry<String, Double> entry : map.entrySet()) {
					sysQty = sysQty + entry.getValue();
				}
				if (prodDtlDtl.getQty() > sysQty) {
					if (null == items) {
						items = "";
					}

					items = items + "," + prodDtlDtl.getItemName();

				}
			}

		}

		return items;
	}

	@FXML
	void showItemPopUp(MouseEvent event) {
		showItemMst();
	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itempopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
//		txtBatch.setText(dpProductionDate.getValue().toString());
		Stage stage = (Stage) btnAdd.getScene().getWindow();
		// Stage stage = (Stage) txtKitName.focusedProperty().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtKitName.setText(itempopupEvent.getItemName());
					// productionDtl.setItemId(itempopupEvent.getItemId());
					ResponseEntity<ItemMst> getItem = RestCaller.getItemByNameRequestParam(txtKitName.getText());
					txtBatch.setText(getItem.getBody().getItemCode());
//		//	txtBatch.requestFocus();
//			txtBatch.setText(dpProductionDate.getValue().toString());
				}
			});
		}
	}

	private void showItemMst() {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.show();

			txtQty.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void filltable() {
		tblProduction1.setItems(productionDtlList);
		clKitname.setCellValueFactory(cellData -> cellData.getValue().getKitnameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		columnId.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());

	}

	private void showReport() {

		tblProduction1.getItems().clear();
//		if(null != productionDtlList )
//		productionDtlList.clear();
//		if (null != productionDtlDtlList)
//		productionDtlDtlList.clear();
		java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
		// actualProductionHdr.setVoucherDate(uDate);
		// ResponseEntity<ActualProductionHdr> respentityHdr =
		// RestCaller.saveActualProductionHdr(actualProductionHdr);
		// actualProductionHdr = respentityHdr.getBody();
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String voucherDate = formatter.format(uDate);

		ResponseEntity<List<ProductionDtl>> productionDtlsaved = RestCaller.getKItDtlsbyDate(voucherDate);
		productionDtlList = FXCollections.observableArrayList(productionDtlsaved.getBody());

		if (!productionDtlList.isEmpty()) {
			for (ProductionDtl proddtl : productionDtlList) {
				ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(proddtl.getItemId());
				itemmst = respitem.getBody();
				proddtl.setKitname(itemmst.getItemName());
				// productionDtlDtl.setUnitName(unitmst.getUnitName());
				// productionDtlDtlList.set(5,proddtldtl );
			}

			filltable();
			productionDtl = null;
			productionDtlDtl = null;

		} else
			notifyMessage(5, "No item Found!!");
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}

	@FXML
	void salOrderToProduction(ActionEvent event) {
		String branchCode = SystemSetting.getSystemBranch();
		java.util.Date uDate = Date.valueOf(dpProductionDate.getValue());
		Format formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String prodDate = formatter.format(uDate);
		ResponseEntity<ProductionMst> response = RestCaller.saleOrderToProduction(branchCode, prodDate);
		productionMst = response.getBody();
//	ResponseEntity<List<ProductionDtl>> productionDtl=RestCaller.planedProductionDtl();

		ResponseEntity<List<ProductionDtl>> resp = RestCaller.prePlaningToProductionPlaning();

		productionDtlList = FXCollections.observableArrayList(resp.getBody());
		filltables();
	}

	private void filltables() {
		for (ProductionDtl prod : productionDtlList) {
			prod.setKitname(prod.getItemId());

		}
		tblProduction1.setItems(productionDtlList);
		clKitname.setCellValueFactory(cellData -> cellData.getValue().getKitnameProperty());
		clBatch.setCellValueFactory(cellData -> cellData.getValue().getbatchProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getqtyProperty());
		columnId.setCellValueFactory(cellData -> cellData.getValue().getIdProperty());

	}
//	  @Subscribe
//	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
//	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
//	  		//if (stage.isShowing()) {
//	  			taskid = taskWindowDataEvent.getId();
//	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
//	  			
//	  		 
//	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
//	  			System.out.println("Business Process ID = " + hdrId);
//	  			
//	  			 PageReload();
//	  		}
//
//
//	  private void PageReload() {
//	  	
//	
//=======

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload(hdrId);
	}

	private void PageReload(String hdrId) {

		ResponseEntity<ProductionMst> getProductionMst = RestCaller.getProductionMstbyHdrId(hdrId);

		ResponseEntity<List<ProductionDtl>> getProductionDtl = RestCaller.getProductionDtlsbyMstId(productionMst);

		productionDtlList = FXCollections.observableArrayList(getProductionDtl.getBody());

		if (!productionDtlList.isEmpty()) {
			for (ProductionDtl proddtl : productionDtlList) {
				ResponseEntity<ItemMst> respitem = RestCaller.getitemMst(proddtl.getItemId());
				itemmst = respitem.getBody();
				proddtl.setKitname(itemmst.getItemName());

			}

			filltable();
		}

	}
}
