package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;

import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class PriceDefenitionCtl {
	String taskid;
	String processInstanceId;


	PriceDefenitionMst priceDefenitionMst = null;
	PriceDefinition priceDefenition = null;
	String itemId = null;
	ItemMst itemMst = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<PriceDefinition> priceDefenitionListTable = FXCollections.observableArrayList();
	private ObservableList<PriceDefenitionMst> priceDefenitionMstTable = FXCollections.observableArrayList();
	

	@FXML
	private ComboBox<String> cmbCategory;

	@FXML
	private TextField txtPriceLevelName;

	@FXML
	private TextField txtUnitName;

	@FXML
	private DatePicker dpMultilevelStartDate;

	@FXML
	private ComboBox<String> cmbPriceCalculator;

	@FXML
	private Button btnAddPriceLevel;

	@FXML
	private Button btnLoadPriceLevel;

	@FXML
	private Button btnDelete;

	@FXML
	private TableView<PriceDefenitionMst> tblPriceDefenition;

	@FXML
	private TableColumn<PriceDefenitionMst, String> clPriceLevelName;

	@FXML
	private TableColumn<PriceDefenitionMst, String> clPriceCalculator;

	@FXML
	private TextField txtItemName;

	@FXML
	private ComboBox<String> cmbPriceType;

	@FXML
	private TextField txtAmount;

	@FXML
	private Button btnSave;

	@FXML
	private TableView<PriceDefinition> tblMultiLevelPriceEdit;

	@FXML
	private TableColumn<PriceDefinition, String> clMultiLevelItemName;

	@FXML
	private TableColumn<PriceDefinition, String> clMultiLevelPriceType;

	@FXML
	private TableColumn<PriceDefinition, Number> clMultiLevelAmount;

	@FXML
	private TableColumn<PriceDefinition, LocalDate> clMultiLevelStartDate;

	@FXML
	private TableColumn<PriceDefinition, LocalDate> clMultiLevelEndDate;

	@FXML
	private TableColumn<PriceDefinition, String> clUnit;
	@FXML
	private ComboBox<?> cmbPricePolicy;

	@FXML
	private TextField txtFixedAmount;

	@FXML
	private TextField txtBrand;

	@FXML
	private TextField txtCategory;

	@FXML
	private DatePicker dpStartDate;

	@FXML
	private TextField txtCommission;

	@FXML
	private TextField txtDelta;

	@FXML
	private Button btnReCalculato;

	@FXML
	private Button btnAddValue;

	@FXML
	private Button btnPublishAll;

	@FXML
	private Button btnShowAll;

	@FXML
	private TableView<?> tblConfigurePriceForItem;

	@FXML
	private Button btnMultiLevelShowAll;

	@FXML
	private void initialize() {
		dpMultilevelStartDate = SystemSetting.datePickerFormat(dpMultilevelStartDate, "dd/MMM/yyyy");
		dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
		setCategory();

		if (null != cmbCategory) {
			if (null != cmbCategory.getItems()) {
				cmbCategory.getItems().add("All Items");
			}
		}

		cmbDisplay();

		eventBus.register(this);
		cmbPriceCalculator.getItems().add("MRP_MINUS_FIXEDPERCENTAGE");
//		cmbPriceCalculator.getItems().add("PURCHASERATE_INCLUDING_TAX_PLUS_FIXEDPERCENTAGE");
		cmbPriceCalculator.getItems().add("FIXEDAMOUNT");
//		cmbPriceCalculator.getItems().add("ZERORATE");
		cmbPriceCalculator.getItems().add("COST_PRICE_PLUS_FIXEDPERCENTAGE");

		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});
		/*
		 * ArrayList unit = new ArrayList();
		 * 
		 * unit = RestCaller.SearchUnit(); Iterator itr = unit.iterator(); while
		 * (itr.hasNext()) { LinkedHashMap lm = (LinkedHashMap) itr.next(); Object
		 * unitName = lm.get("unitName"); Object unitId = lm.get("id"); if (unitId !=
		 * null) { cmbUnit.getItems().add((String) unitName);
		 * 
		 * }
		 * 
		 * }
		 */
		tblMultiLevelPriceEdit.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						if (null != newSelection.getId()) {

							priceDefenition = new PriceDefinition();
							priceDefenition.setId(newSelection.getId());
							
//							tblMultiLevelPriceEditIndex = obs.getValue().get

						}
					}

				});

		tblPriceDefenition.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					priceDefenitionMst = new PriceDefenitionMst();
					priceDefenitionMst.setId(newSelection.getId());

				}
			}

		});

	}

	private void setCategory() {
		ArrayList cat = new ArrayList();
		cat = RestCaller.SearchCategory();
		Iterator itr1 = cat.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object categoryName = lm.get("categoryName");
			Object parentId = lm.get("parentId");
			Object id = lm.get("id");
			if (id != null) {

				cmbCategory.getItems().add((String) categoryName);

			}

		}
	}

	@FXML
	void Save(ActionEvent event) {
		tblMultiLevelPriceEdit.getItems().clear();
		priceDefenition = new PriceDefinition();

		if (null != cmbCategory.getSelectionModel().getSelectedItem() && !txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, " Please either Item or Category...!!!");
			txtItemName.clear();
			cmbCategory.getSelectionModel().clearSelection();
			return;
		}
		if (null == cmbCategory.getSelectionModel().getSelectedItem() && txtItemName.getText().trim().isEmpty()) {
			notifyMessage(5, " Please either Item or Category...!!!");
			return;
		}
		if (null == cmbPriceType.getValue()) {

			notifyMessage(5, " Please select Price Type...!!!");
			return;

		}
		if (txtAmount.getText().isEmpty()) {
			notifyMessage(5, " Please Enter Amount...!!!");
			return;
		}
		if (null == dpMultilevelStartDate.getValue()) {
			notifyMessage(5, "Please Select Start Date...!!!");
			return;

		}
		if (!txtItemName.getText().trim().isEmpty()) {
			ResponseEntity<ItemMst> itemResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
			ItemMst itemMst = itemResp.getBody();

			priceDefenition.setItemId(itemMst.getId());
			priceDefenition.setBranchCode(SystemSetting.systemBranch);
			itemId = null;

			priceDefenition.setEndDate(null);
			priceDefenition.setStartDate(Date.valueOf(dpMultilevelStartDate.getValue()));

			String pricetype = cmbPriceType.getValue();
			ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
					.getPriceDefenitionByName(pricetype);
			PriceDefenitionMst price = new PriceDefenitionMst();
			price = priceDefenitionSaved.getBody().get(0);
			if (null != price) {
				priceDefenition.setPriceId(price.getId());
			}
//			priceDefenition.setAmount(Double.parseDouble(txtAmount.getText()));
			Double amount = 0.0;
			Double percentage = 0.0;
			percentage = Double.parseDouble(txtAmount.getText());

			if (price.getPriceCalculator().equalsIgnoreCase("MRP_MINUS_FIXEDPERCENTAGE")) {
				amount = itemMst.getStandardPrice() - ((percentage * itemMst.getStandardPrice()) / 100);
			} else if (price.getPriceCalculator().equalsIgnoreCase("COST_PRICE_PLUS_FIXEDPERCENTAGE")) {

				ResponseEntity<List<PriceDefenitionMst>> priceDefenitionMstResp = RestCaller
						.getPriceDefenitionByName("COST PRICE");
				List<PriceDefenitionMst> priceDefenitionMstList = priceDefenitionMstResp.getBody();

				if (priceDefenitionMstList.size() > 0) {
					
					LocalDate  dpDate =dpMultilevelStartDate.getValue();
					

					java.util.Date startDate = SystemSetting.localToUtilDate(dpDate);
					String date = SystemSetting.UtilDateToString(startDate, "yyyy-MM-dd");
					ResponseEntity<PriceDefinition> pricedefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
							itemMst.getId(), priceDefenitionMstList.get(0).getId(), itemMst.getUnitId(), date);
					PriceDefinition priceDefinition = pricedefenitionResp.getBody();

					if (null != priceDefinition) {
						Double costPrice = priceDefinition.getAmount();

						amount = costPrice + ((percentage * costPrice) / 100);

					} else {
						notifyMessage(5, "Please add cost price to the item " + itemMst.getItemName());
						return;
					}

				} else {
					notifyMessage(5, "Type Cost Price is not present...!!!");
					return;
				}
			} else {

				amount = Double.parseDouble(txtAmount.getText());
			}

			if (!txtUnitName.getText().isEmpty()) {
				ResponseEntity<UnitMst> unit = RestCaller.getUnitByName(txtUnitName.getText());
				priceDefenition.setUnitId(unit.getBody().getId());
			}
			priceDefenition.setAmount(amount);
			ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(priceDefenition);
			priceDefenition = respentity.getBody();
			priceDefenitionListTable.add(priceDefenition);
			fillTable();
			notifyMessage(5, "Successfully saved...!!!");
			clear();
		} else if (cmbCategory.getSelectionModel().getSelectedItem().equalsIgnoreCase("All Items")) {

			ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
					.getPriceDefenitionByName(cmbPriceType.getSelectionModel().getSelectedItem());
			PriceDefenitionMst price = priceDefenitionSaved.getBody().get(0);

//			if(!price.getPriceCalculator().equalsIgnoreCase("MRP_MINUS_FIXEDPERCENTAGE"))
//			{
//				return;
//			}

			ResponseEntity<List<ItemMst>> itemList = RestCaller.getAllItemMst();

			for (ItemMst itemMst : itemList.getBody()) {
				priceDefenition.setItemId(itemMst.getId());
				priceDefenition.setBranchCode(SystemSetting.systemBranch);
				itemId = null;

				priceDefenition.setEndDate(null);
				priceDefenition.setStartDate(Date.valueOf(dpMultilevelStartDate.getValue()));
				if (null != price) {
					priceDefenition.setPriceId(price.getId());
				}
				Double amount = 0.0;
				Double percentage = 0.0;
				percentage = Double.parseDouble(txtAmount.getText());

				if (price.getPriceCalculator().equalsIgnoreCase("MRP_MINUS_FIXEDPERCENTAGE")) {
					amount = itemMst.getStandardPrice() - ((percentage * itemMst.getStandardPrice()) / 100);
				} else if (price.getPriceCalculator().equalsIgnoreCase("COST_PRICE_PLUS_FIXEDPERCENTAGE")) {

					ResponseEntity<List<PriceDefenitionMst>> priceDefenitionMstResp = RestCaller
							.getPriceDefenitionByName("COST PRICE");
					List<PriceDefenitionMst> priceDefenitionMstList = priceDefenitionMstResp.getBody();

					if (priceDefenitionMstList.size() > 0) {
						String date = SystemSetting.UtilDateToString(SystemSetting.getSystemDate(), "yyyy-MM-dd");
						ResponseEntity<PriceDefinition> pricedefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
								itemMst.getId(), priceDefenitionMstList.get(0).getId(), itemMst.getUnitId(), date);
						PriceDefinition priceDefinition = pricedefenitionResp.getBody();

						if (null != priceDefinition) {
							Double costPrice = priceDefinition.getAmount();

							amount = costPrice + ((percentage * itemMst.getStandardPrice()) / 100);

						} else {
							notifyMessage(5, "Please add cost price...!!!");
							return;
						}

					} else {
						notifyMessage(5, "Type Cost Price is not present...!!!");
						return;
					}
				} else {

					amount = Double.parseDouble(txtAmount.getText());
				}
				priceDefenition.setAmount(amount);
				priceDefenition.setUnitId(itemMst.getUnitId());

				ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(priceDefenition);
				priceDefenition = respentity.getBody();
				priceDefenitionListTable.add(priceDefenition);
//				fillTable();
				priceDefenition = new PriceDefinition();

			}
			notifyMessage(5, "Successfully saved...!!!");
			LoadPriceDefenition();
			clear();
		} else if (null != cmbCategory.getSelectionModel().getSelectedItem()) {

			ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
					.getPriceDefenitionByName(cmbPriceType.getSelectionModel().getSelectedItem());
			PriceDefenitionMst price = priceDefenitionSaved.getBody().get(0);

			ResponseEntity<CategoryMst> categoryMstResp = RestCaller
					.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem());
			CategoryMst categoryMst = categoryMstResp.getBody();
			if (null != categoryMst) {
				ResponseEntity<List<ItemMst>> itemList = RestCaller.getItemsByCategory(categoryMst);
				for (ItemMst itemMst : itemList.getBody()) {
					priceDefenition.setItemId(itemMst.getId());
					priceDefenition.setBranchCode(SystemSetting.systemBranch);
					itemId = null;

					priceDefenition.setEndDate(null);
					priceDefenition.setStartDate(Date.valueOf(dpMultilevelStartDate.getValue()));

					if (null != price) {
						priceDefenition.setPriceId(price.getId());
					}
					Double amount = 0.0;
					Double percentage = 0.0;
					percentage = Double.parseDouble(txtAmount.getText());

					if (price.getPriceCalculator().equalsIgnoreCase("MRP_MINUS_FIXEDPERCENTAGE")) {
						amount = itemMst.getStandardPrice() - ((percentage * itemMst.getStandardPrice()) / 100);
					} else if (price.getPriceCalculator().equalsIgnoreCase("COST_PRICE_PLUS_FIXEDPERCENTAGE")) {

						ResponseEntity<List<PriceDefenitionMst>> priceDefenitionMstResp = RestCaller
								.getPriceDefenitionByName("COST PRICE");
						List<PriceDefenitionMst> priceDefenitionMstList = priceDefenitionMstResp.getBody();

						if (priceDefenitionMstList.size() > 0) {
							String date = SystemSetting.UtilDateToString(SystemSetting.getSystemDate(), "yyyy-MM-dd");
							ResponseEntity<PriceDefinition> pricedefenitionResp = RestCaller
									.getPriceDefenitionByCostPrice(itemMst.getId(),
											priceDefenitionMstList.get(0).getId(), itemMst.getUnitId(), date);
							PriceDefinition priceDefinition = pricedefenitionResp.getBody();

							if (null != priceDefinition) {
								Double costPrice = priceDefinition.getAmount();

								amount = costPrice + ((percentage * itemMst.getStandardPrice()) / 100);

							} else {
								notifyMessage(5, "Please add cost price...!!!");
								return;
							}

						} else {
							notifyMessage(5, "Type Cost Price is not present...!!!");
							return;
						}

					} else {

						amount = Double.parseDouble(txtAmount.getText());
					}

					priceDefenition.setAmount(amount);
					priceDefenition.setUnitId(itemMst.getUnitId());

					ResponseEntity<PriceDefinition> respentity = RestCaller.savePriceDefenition(priceDefenition);
					priceDefenition = respentity.getBody();
					priceDefenitionListTable.add(priceDefenition);
//					fillTable();
					priceDefenition = new PriceDefinition();

				}

				notifyMessage(5, "Successfully saved...!!!");
				LoadPriceDefenition();
				clear();
			}

		}

	}

	@FXML
	void actionAddPrice(ActionEvent event) {

		if (txtPriceLevelName.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter price name...!!!");
		} else if (null == cmbPriceCalculator.getValue()) {

			notifyMessage(5, "Please select price calculator...!!!");

		} else {

			priceDefenitionMst = new PriceDefenitionMst();
			priceDefenitionMst.setPriceCalculator(cmbPriceCalculator.getValue());
			priceDefenitionMst.setPriceLevelName(txtPriceLevelName.getText());
			String vNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch() + "PD");
			priceDefenitionMst.setId(vNo);
			priceDefenitionMstTable.clear();
			tblPriceDefenition.getItems().clear();
			ResponseEntity<PriceDefenitionMst> respentity = RestCaller.savePriceDefenitionMst(priceDefenitionMst);
			priceDefenitionMst = respentity.getBody();
			if (null != priceDefenitionMst) {
				notifyMessage(5, "Successfully saved...!!!");
				priceDefenitionMstTable.add(priceDefenitionMst);
				FillPriceMstDetails();
				txtPriceLevelName.clear();
				cmbPriceCalculator.getSelectionModel().clearSelection();
				priceDefenitionMst = null;
			}
		}

	}

	@FXML
	void actionAddValue(ActionEvent event) {

	}

	@FXML
	void PriceTypeOnKEyPress(MouseEvent event) {

		cmbDisplay();

	}

	private void cmbDisplay() {

		if (null == cmbPriceType) {
			return;
		}

		cmbPriceType.getItems().clear();

		System.out.println("=====PriceTypeOnKEyPress===");
		ArrayList priceType = new ArrayList();
		RestTemplate restTemplate = new RestTemplate();
		priceType = RestCaller.getPriceDefinition();
		Iterator itr = priceType.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object priceLevelName = lm.get("priceLevelName");
			Object id = lm.get("id");
			String priceName = (String) priceLevelName;
			if (id != null) {
				cmbPriceType.getItems().add((String) priceLevelName);

			}

		}

	}

	@FXML
	void actionDelete(ActionEvent event) {

		if (null != priceDefenitionMst) {
			System.out.println("===priceDefenitionMst====" + priceDefenitionMst);
			if (null != priceDefenitionMst.getId()) {
				RestCaller.deletePriceDefenitionMst(priceDefenitionMst.getId());

				showPriceDefenition();
			}

		}

	}

	@FXML
	void actionLoadPriceLevel(ActionEvent event) {

		if (!txtPriceLevelName.getText().trim().isEmpty()) {
			showPriceDefenitionByName();
		} else if (null != cmbPriceCalculator.getValue()) {

			showPriceDefenitionByPriceCalculator();
		} else {

			showPriceDefenition();
		}

	}

	@FXML
	void DeleteMultilevelPrice(ActionEvent event) {

		if (null != priceDefenition) {
			System.out.println("====priceDefenition===" + priceDefenition);
			if (null != priceDefenition.getId()) {
				RestCaller.deletePriceDefenition(priceDefenition.getId());
				
				removePriceDefenitionListTable(priceDefenition.getId());

//				LoadPriceDefenition();
//				priceDefenitionListTable.removeListener(priceDefenition);

			}
		}

	}

	private void removePriceDefenitionListTable(String id) {
		
		for(int i=0; i<priceDefenitionListTable.size(); i++)
		{
			if(priceDefenitionListTable.get(i).getId().equalsIgnoreCase(id))
			{
				priceDefenitionListTable.remove(i);
			}
		}
		
		fillTable();

	}

	@FXML
	void actionPublishAll(ActionEvent event) {

	}

	@FXML
	void actionPublishPrice(ActionEvent event) {

	}

	@FXML
	void actionReCalculation(ActionEvent event) {

	}

	@FXML
	void ShowItemPopup(MouseEvent event) {

		ItemPopup();
	}

	@FXML
	void ItemPopUpView(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			ItemPopup();
		}

	}

	private void ItemPopup() {

		System.out.println("inside the popup");
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			cmbPriceType.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	void AmountOnKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			dpMultilevelStartDate.requestFocus();
		}

	}

	@FXML
	void EndDateOnKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}

	}

	@FXML
	void StartDateOnKEyPressed(KeyEvent event) {

	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		Stage stage = (Stage) txtItemName.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtItemName.setText(itemPopupEvent.getItemName());

					itemMst = new ItemMst();
					itemMst.setId(itemPopupEvent.getItemId());
					txtUnitName.setText(itemPopupEvent.getUnitName());

				}
			});
		}
	}

	private void fillTable() {

		tblMultiLevelPriceEdit.setItems(priceDefenitionListTable);
		for (PriceDefinition p : priceDefenitionListTable) {
			if (null != p.getItemId()) {
				ResponseEntity<ItemMst> respentity = RestCaller.getitemMst(p.getItemId());
				if (null != respentity.getBody())
					p.setItemName(respentity.getBody().getItemName());
				clMultiLevelItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			}
			if (null != p.getPriceId()) {
				ResponseEntity<PriceDefenitionMst> respentity = RestCaller.getPriceNameById(p.getPriceId());
				if (null != respentity.getBody())
					p.setPriceType(respentity.getBody().getPriceLevelName());
				clMultiLevelPriceType.setCellValueFactory(cellData -> cellData.getValue().getPriceTypeProperty());
			}
			if (null != p.getUnitId()) {
				ResponseEntity<UnitMst> unit = RestCaller.getunitMst(p.getUnitId());
				if (null != unit.getBody())
					p.setUnitName(unit.getBody().getUnitName());
			}
		}
		clMultiLevelAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		clMultiLevelStartDate.setCellValueFactory(cellData -> cellData.getValue().getStartDateProperty());
		clMultiLevelEndDate.setCellValueFactory(cellData -> cellData.getValue().getEndDateProperty());
		clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());

	}

	@FXML
	void MultiLevelShowAll(ActionEvent event) {

		if (!txtItemName.getText().trim().isEmpty()) {

			ResponseEntity<ItemMst> itemMstResp = RestCaller.getItemByNameRequestParam(txtItemName.getText());
			ItemMst itemMst = itemMstResp.getBody();
			LoadPriceDefenitionByItemId(itemMst.getId());

		} else if (null != cmbPriceType.getValue()) {

			String pricetype = cmbPriceType.getValue();
			ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
					.getPriceDefenitionByName(pricetype);

			LoadPriceDefenitionByPriceType(priceDefenitionSaved.getBody().get(0).getId());

		} else if (null != dpMultilevelStartDate.getValue()) {

			java.util.Date uDate = Date.valueOf(dpMultilevelStartDate.getValue());

			Format formatter;

			formatter = new SimpleDateFormat("dd-MM-yyyy");
			String strDate = formatter.format(uDate);
			LoadPriceDefenitionByDate(strDate);

		} else if (null != cmbCategory.getSelectionModel().getSelectedItem()) {

			ResponseEntity<CategoryMst> categoryMstResp = RestCaller
					.getCategoryByName(cmbCategory.getSelectionModel().getSelectedItem().toString());
			CategoryMst categoryMst = categoryMstResp.getBody();
			LoadPriceDefenitionByCategoryId(categoryMst.getId());

		} else {

			LoadPriceDefenition();
		}

		txtItemName.clear();
		cmbCategory.getSelectionModel().clearSelection();
		cmbPriceType.getSelectionModel().clearSelection();

	}

	private void LoadPriceDefenitionByCategoryId(String id) {

		ResponseEntity<List<PriceDefinition>> priceDefinitionSaved = RestCaller.SearchPriceDefinitionByCategoryId(id);
		if (null != priceDefinitionSaved.getBody()) {
			priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
			fillTable();
		}
	}

	@FXML
	void actionShowAll(ActionEvent event) {

	}

	@FXML
	void ActionShowAll(ActionEvent event) {
		////////////////////
		System.out.println("Show All PriceDefenition");
		ResponseEntity<List<PriceDefenitionMst>> priceDefinitionShow = RestCaller.ShowAllPriceDefinition();
		System.out.println("Show All PriceDefenition1111222");
		if (null != priceDefinitionShow.getBody()) {
			priceDefenitionMstTable = FXCollections.observableArrayList(priceDefinitionShow.getBody());
			FillPriceMstDetails();
		}
	}

	private void LoadPriceDefenition() {
		System.out.println("LoadPriceDefenition");
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<PriceDefinition>> priceDefinitionSaved = RestCaller.SearchPriceDefinition();
		if (null != priceDefinitionSaved.getBody()) {
			priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
			fillTable();
		}
	}

	private void clear() {
		txtAmount.setText("");
		txtItemName.setText("");
		cmbPriceType.getSelectionModel().clearSelection();
		txtUnitName.clear();
		dpMultilevelStartDate.setValue(null);
		cmbCategory.getSelectionModel().clearSelection();
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	private void FillPriceMstDetails() {

		tblPriceDefenition.setItems(priceDefenitionMstTable);
		clPriceLevelName.setCellValueFactory(cellData -> cellData.getValue().getPriceLevelNameProperty());
		clPriceCalculator.setCellValueFactory(cellData -> cellData.getValue().getPriceCalculatorNameProperty());

		funClearFields();
	}

	private void showPriceDefenitionByName() {

		priceDefenitionMstTable.clear();
		tblPriceDefenition.getItems().clear();

		String name = txtPriceLevelName.getText();

		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller.getPriceDefenitionByName(name);
		if (null != priceDefenitionSaved.getBody()) {
			priceDefenitionMstTable = FXCollections.observableArrayList(priceDefenitionSaved.getBody());
			FillPriceMstDetails();
		}

	}

	private void showPriceDefenitionByPriceCalculator() {

		priceDefenitionMstTable.clear();
		tblPriceDefenition.getItems().clear();

		String pricecalculator = cmbPriceCalculator.getValue();
		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller
				.getPriceDefenitionByPricecalculator(pricecalculator);
		if (null != priceDefenitionSaved.getBody()) {
			priceDefenitionMstTable = FXCollections.observableArrayList(priceDefenitionSaved.getBody());
			FillPriceMstDetails();
		}
		txtPriceLevelName.clear();

	}

	private void showPriceDefenition() {

		priceDefenitionMstTable.clear();
		tblPriceDefenition.getItems().clear();

		ResponseEntity<List<PriceDefenitionMst>> priceDefenitionSaved = RestCaller.getAllPriceDefenition();
		if (null != priceDefenitionSaved.getBody()) {
			priceDefenitionMstTable = FXCollections.observableArrayList(priceDefenitionSaved.getBody());
			FillPriceMstDetails();
			cmbPriceCalculator.getSelectionModel().clearSelection();
		}

	}

	private void funClearFields() {
		txtPriceLevelName.clear();
		cmbPriceCalculator.getSelectionModel().clearSelection();

	}

	private void LoadPriceDefenitionByItemId(String id) {

		ResponseEntity<List<PriceDefinition>> priceDefinitionSaved = RestCaller.SearchPriceDefinitionByitemid(id);
		if (null != priceDefinitionSaved.getBody()) {
			priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
			fillTable();
		}

	}

	private void LoadPriceDefenitionByPriceType(String priceType) {

		ResponseEntity<List<PriceDefinition>> priceDefinitionSaved = RestCaller
				.SearchPriceDefinitionByPriceType(priceType);
		if (null != priceDefinitionSaved.getBody()) {
			priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
			fillTable();
		}

	}

	private void LoadPriceDefenitionByDate(String date) {
		System.out.println("===LoadPriceDefenitionByDate====");

		ResponseEntity<List<PriceDefinition>> priceDefinitionSaved = RestCaller.getPriceDefenitionByDate(date);
		if (null != priceDefinitionSaved.getBody()) {
			priceDefenitionListTable = FXCollections.observableArrayList(priceDefinitionSaved.getBody());
			fillTable();
		}

	}
	 @Subscribe
	  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	  		//Stage stage = (Stage) btnClear.getScene().getWindow();
	  		//if (stage.isShowing()) {
	  			taskid = taskWindowDataEvent.getId();
	  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	  			
	  		 
	  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	  			System.out.println("Business Process ID = " + hdrId);
	  			
	  			 PageReload();
	  		}


	  private void PageReload() {
	  	
	  }

}
