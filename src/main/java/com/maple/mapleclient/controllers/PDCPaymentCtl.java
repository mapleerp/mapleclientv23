package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.PDCPayment;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class PDCPaymentCtl {
	String taskid;
	String processInstanceId;
	

	
	String accountId = null;
	EventBus eventBus = EventBusFactory.getEventBus();
	PDCPayment pdcPayment = null;
	private ObservableList<PDCPayment> paymentList = FXCollections.observableArrayList();

   
    @FXML
    private TextField txtAccount;

    @FXML
    private TextField txtRemark;

    @FXML
    private DatePicker dpTransDate;

    @FXML
    private ComboBox<String> cmbBank;

    @FXML
    private TextField txtInstNo;

    @FXML
    private DatePicker dpInstDate;

    @FXML
    private TextField txtAmount;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnDelete;

    @FXML
    private TableView<PDCPayment> tblPDC;

    @FXML
    private TableColumn<PDCPayment, String> clAccount;

    @FXML
    private TableColumn<PDCPayment, String> clRemark;

    @FXML
    private TableColumn<PDCPayment, LocalDate> clTransactionDate;

    @FXML
    private TableColumn<PDCPayment, String> clBank;

    @FXML
    private TableColumn<PDCPayment, String> clInstNo;

    @FXML
    private TableColumn<PDCPayment, LocalDate> clInstDate;

    @FXML
    private TableColumn<PDCPayment, Number> clAmount;
    
    @FXML
	private void initialize() {
		eventBus.register(this);
		dpInstDate = SystemSetting.datePickerFormat(dpInstDate, "dd/MMM/yyyy");
		dpTransDate = SystemSetting.datePickerFormat(dpTransDate, "dd/MMM/yyyy");
		
		dpTransDate.setValue(LocalDate.now());
		
		ArrayList bankList = new ArrayList();
		bankList = RestCaller.getAllBankAccounts();
		Iterator itr1 = bankList.iterator();
		while (itr1.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr1.next();
			Object bankName = lm.get("accountName");
			Object bankid= lm.get("id");
			String bankId = (String) bankid;
			if (bankName != null) {
				cmbBank.getItems().add((String) bankName);
				
						    //accountHeads.getAccountName);
			}
		}
		
		
		tblPDC.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					pdcPayment = new PDCPayment();
					pdcPayment.setId(newSelection.getId()); 
					
				}
			}
		});
	}

    

    @FXML
    void AccountPopup(MouseEvent event) {
    	
    	showPopup();

    }

	
    @FXML
    void Delete(ActionEvent event) {
    	
    	if(null != pdcPayment)
		{
			if(null != pdcPayment.getId())
			{
				RestCaller.deletePDCPayment(pdcPayment.getId());
				notifyMessage(5, "PDC Deleted Successfully...!", false);
				pdcPayment = null;
				
		    	showAllPayments();
			}
				
		}


    }

    @FXML
    void FocusBank(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
			cmbBank.requestFocus();
		}

    }

    @FXML
    void FocusAmount(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();
		}


    }

    @FXML
    void FocusInstDate(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			dpInstDate.requestFocus();
		}


    }

    @FXML
    void FocusInstNo(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtInstNo.requestFocus();
		}


    }

    @FXML
    void FocusRemark(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtRemark.requestFocus();
		}


    }

    @FXML
    void FocusSave(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}


    }

    @FXML
    void FocusTransDate(KeyEvent event) {

    	if (event.getCode() == KeyCode.ENTER) {
			dpTransDate.requestFocus();
		}

    }

    @FXML
    void OnPressDelete(KeyEvent event) {

    }
    
    

    @FXML
    void OnPressSave(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
    	  	SavePDCPayment();
		}


    }

    @FXML
    void SOnPressShowAll(KeyEvent event) {
    	
    	if (event.getCode() == KeyCode.ENTER) {
        	showAllPayments();
		}

    }

    @FXML
    void Save(ActionEvent event) {
    	
    	SavePDCPayment();

    }
    
	private void SavePDCPayment() {
		
		if (txtAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "please select account...!", false);
			return;
		}

		if (null == dpTransDate.getValue()) {
			notifyMessage(5, "please select transaction date...!", false);
			return;
		}

		if (null == cmbBank.getValue()) {
			notifyMessage(5, "please select banck account...!", false);
			return;
		}

		if (txtInstNo.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter Instrument Number...!", false);
			return;
		}

		if (null == dpInstDate.getValue()) {
			notifyMessage(5, "please select Instrument date", false);
			return;
		}


		if (txtAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "please enter Amount...!", false);
			return;
		}

		pdcPayment = new PDCPayment();
		
		pdcPayment.setAccount(accountId);
		
		if(!txtRemark.getText().trim().isEmpty())
		{
			pdcPayment.setRemark(txtRemark.getText());
		}
		pdcPayment.setBranchCode(SystemSetting.systemBranch);
		pdcPayment.setTransDate(Date.valueOf(dpTransDate.getValue()));
		pdcPayment.setBank(cmbBank.getSelectionModel().getSelectedItem().toString());
		pdcPayment.setInstrumentnumber(txtInstNo.getText());
		pdcPayment.setInstrumentDate(Date.valueOf(dpInstDate.getValue()));
		pdcPayment.setAmount(Double.parseDouble(txtAmount.getText()));
		pdcPayment.setStatus("PENDING");
		
		
		
		ResponseEntity<PDCPayment> savedPdcPayment = RestCaller.SavePDCPaymen(pdcPayment);
		pdcPayment = savedPdcPayment.getBody();
		paymentList.add(pdcPayment);
		FillTable();
		
		notifyMessage(5, "PDC Added Successfully...!", true);
		try {
			
			JasperPdfReportService.PDCPaymentInvoice(paymentList);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		pdcPayment = null;
		txtAccount.clear();
		txtAmount.clear();
		txtInstNo.clear();
		txtRemark.clear();
		dpInstDate.setValue(null);
		dpTransDate.setValue(LocalDate.now());
		cmbBank.getSelectionModel().clearSelection();

		
	}
	

	private void FillTable() {
		

		for(PDCPayment pdc : paymentList)
		{
			if(null != pdc.getAccount())
			{
				ResponseEntity<AccountHeads> accountHeadsResp = RestCaller.getAccountHeadsById(pdc.getAccount());
				AccountHeads accountHeads = accountHeadsResp.getBody();
				
				pdc.setAccountName(accountHeads.getAccountName());

			}
		}
		
		tblPDC.setItems(paymentList);
		
		clAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());

		clRemark.setCellValueFactory(cellData -> cellData.getValue().getRemarkeProperty());
		clTransactionDate.setCellValueFactory(cellData -> cellData.getValue().getTransDateProperty());
		clBank.setCellValueFactory(cellData -> cellData.getValue().getBankProperty());
		clInstNo.setCellValueFactory(cellData -> cellData.getValue().getInstNoProperty());
		clInstDate.setCellValueFactory(cellData -> cellData.getValue().getInstDateProperty());
		clBank.setCellValueFactory(cellData -> cellData.getValue().getBankProperty());
		clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());

		
	}

    @FXML
    void ShowAll(ActionEvent event) {
    	
    	showAllPayments();

    }
    
	
	private void showAllPayments() {
		
		if(null == dpTransDate.getValue())
		{
			notifyMessage(5, "please select transaction date...!", false);
			return;
		}
		
	
		String date = SystemSetting.SqlDateTostring(Date.valueOf(dpTransDate.getValue()));
		ResponseEntity<List<PDCPayment>> pdcPaymentsList = RestCaller.getPDCPaymentsByDate(date);
		paymentList = FXCollections.observableArrayList(pdcPaymentsList.getBody());
		
		FillTable();
	}
    
	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtRemark.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Subscribe
	public void popuplistner(AccountPopupEvent accountPopupEvent) {
		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) txtAccount.getScene().getWindow();
		if (stage.isShowing()) {
		
			txtAccount.setText(accountPopupEvent.getAccountName());
			 accountId = accountPopupEvent.getAccountId();

		
		}
		
	}
	
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	   private void PageReload() {
	   	
	   }

}
