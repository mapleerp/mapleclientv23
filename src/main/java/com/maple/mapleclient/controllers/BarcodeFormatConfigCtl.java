package com.maple.mapleclient.controllers;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BarcodeFormatMst;
import com.maple.mapleclient.entity.ChequePrintMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class BarcodeFormatConfigCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<BarcodeFormatMst> barcodeFormatList = FXCollections.observableArrayList();

	BarcodeFormatMst barcodeFormatMst = null;
    @FXML
    private TextField txtFormatName;

    @FXML
    private ComboBox<String> cmbPropertyName;

    @FXML
    private TextField txtValue;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnShowAll;

    @FXML
    private TableView<BarcodeFormatMst> tbBarcodeFormat;

    @FXML
    private TableColumn<BarcodeFormatMst, String> clFormatName;

    @FXML
    private TableColumn<BarcodeFormatMst, String> clPropertyName;

    @FXML
    private TableColumn<BarcodeFormatMst,String> clPropertyValue;
    @FXML
    void initialize()
    {
    	

    	cmbPropertyName.getItems().add("NUTRITION_SIZE");
    	cmbPropertyName.getItems().add("NUTRITION_FORMAT");
    	cmbPropertyName.getItems().add("BARCODETITLE");
    	cmbPropertyName.getItems().add("BARCODE_FORMAT");
    	cmbPropertyName.getItems().add("BARCODE_SIZE");
    	cmbPropertyName.getItems().add("NUTRITIONTITLE");
    	cmbPropertyName.getItems().add("BARCODE_X");
    	cmbPropertyName.getItems().add("BARCODE_Y");
    	cmbPropertyName.getItems().add("MFGDATE_X");
    	cmbPropertyName.getItems().add("MFGDATE_Y");
    	cmbPropertyName.getItems().add("EXPDATE_X");
    	cmbPropertyName.getItems().add("EXPDATE_Y");
    	cmbPropertyName.getItems().add("ITEMNAME_X");
    	cmbPropertyName.getItems().add("ITEMNAME_Y");
    	cmbPropertyName.getItems().add("INGLINE1_X");
    	cmbPropertyName.getItems().add("INGLINE1_Y");
    	cmbPropertyName.getItems().add("INGLINE2_X");
    	cmbPropertyName.getItems().add("INGLINE2_Y");
    	cmbPropertyName.getItems().add("MRP_X");
    	cmbPropertyName.getItems().add("MRP_Y");
    	cmbPropertyName.getItems().add("NETWT_X");
    	cmbPropertyName.getItems().add("NETWT_Y");
    	cmbPropertyName.getItems().add("BATCH_X");
    	cmbPropertyName.getItems().add("BATCH_Y");
    	cmbPropertyName.getItems().add("NUTRITIONTITLE_X");
    	cmbPropertyName.getItems().add("NUTRITIONTITLE_Y");
    	cmbPropertyName.getItems().add("NUTRITIONFACT_X");
    	cmbPropertyName.getItems().add("NUTRITIONFACT_Y");
    	cmbPropertyName.getItems().add("BARCODETITLE_X");
    	cmbPropertyName.getItems().add("BARCODETITLE_Y");
    	
    	tbBarcodeFormat.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					barcodeFormatMst = new BarcodeFormatMst();
					barcodeFormatMst.setId(newSelection.getId());
					txtFormatName.setText(newSelection.getBarcodeFormatName());
//					txtBatch.setText(newSelection.getBatch());
//					txtMrp.setText(Double.toString(newSelection.getMrp()));
//					txtQty.setText(Double.toString(newSelection.getQty()));
				}
			}
		});
    }
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == barcodeFormatMst)
    	{
    		return;
    	}
    	if(null == barcodeFormatMst.getId())
    	{
    		return;
    	}
    	RestCaller.deleteBarcodeFormatMstById(barcodeFormatMst.getId());
    	tbBarcodeFormat.getItems().clear();
    	barcodeFormatList.clear();
    	ResponseEntity<List<BarcodeFormatMst>> barcodeList = RestCaller.showAllBarcodeFormat();
    	barcodeFormatList = FXCollections.observableArrayList(barcodeList.getBody());
    	fillTable();
    }

    @FXML
    void actionSave(ActionEvent event) {
    	barcodeFormatMst = new BarcodeFormatMst();
    	barcodeFormatMst.setBarcodeFormatName(txtFormatName.getText());
    	barcodeFormatMst.setPropertyName(cmbPropertyName.getSelectionModel().getSelectedItem());
    	barcodeFormatMst.setPropertyValue(txtValue.getText());
    	ResponseEntity<BarcodeFormatMst> respentity = RestCaller.saveBarcodeFormatMst(barcodeFormatMst);
    	barcodeFormatMst = respentity.getBody();
    	barcodeFormatList.add(barcodeFormatMst);
    	fillTable();
    	barcodeFormatMst = null;
    	cmbPropertyName.getSelectionModel().clearSelection();
    	txtValue.clear();
    }
    private void fillTable()
    {
    	tbBarcodeFormat.setItems(barcodeFormatList);
    	clFormatName.setCellValueFactory(cellData -> cellData.getValue().getBarcodeFormatNameProperty());
    	clPropertyName.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
    	clPropertyValue.setCellValueFactory(cellData -> cellData.getValue().getPropertyValueProperty());

    }

    @FXML
    void actionShowAll(ActionEvent event) {

    	tbBarcodeFormat.getItems().clear();
    	barcodeFormatList.clear();
    	ResponseEntity<List<BarcodeFormatMst>> barcodeList = RestCaller.showAllBarcodeFormat();
    	
    	barcodeFormatList = FXCollections.observableArrayList(barcodeList.getBody());
    	fillTable();

    }
    public void notifyMessage(int duration, String msg) {
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


      private void PageReload() {
      	
}
}
