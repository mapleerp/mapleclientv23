package com.maple.mapleclient.controllers;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.events.TaskWindowDataEvent;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class pendingRequestController {
	String taskid;
	String processInstanceId;

    @FXML
    private ComboBox<?> cmbPendingRequest;

    @FXML
    private Button btnRefresh;

    @FXML
    private TextField txtStatutoryVoucher;

    @FXML
    private Button btnCnvrtStockTran;

    @FXML
    private Button btnPrintStockTran;

    @FXML
    private TableView<?> tblPendingRequest;

    @FXML
    void convertToStockTransfer(ActionEvent event) {

    }

    @FXML
    void printStockTransfer(ActionEvent event) {

    }

    @FXML
    void refresh(ActionEvent event) {

    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


   private void PageReload() {
   	
   }


}
