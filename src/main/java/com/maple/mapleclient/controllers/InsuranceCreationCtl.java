package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.InsuranceCompanyDtl;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.entity.MenuMst;
import com.maple.mapleclient.entity.NutritionValueDtl;
import com.maple.mapleclient.entity.NutritionValueMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;

public class InsuranceCreationCtl {
	String taskid;
	String processInstanceId;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<InsuranceCompanyMst> insuranceCompanyMstList = FXCollections.observableArrayList();
	private ObservableList<InsuranceCompanyDtl> insuranceCompanyDtlList = FXCollections.observableArrayList();
	InsuranceCompanyMst insuranceCompanyMst;
	InsuranceCompanyDtl insuranceCompanyDtl;
	@FXML
	private ComboBox<String> cmbInsuranceCompany;

	@FXML
	private TextField txtPolicyType;
	@FXML
	private TextField txtPercentageCoverage;

	@FXML
	private TextField txtContactPerson;

	@FXML
	private TextField txtTelNo;

	@FXML
	private Button btnSave;

	@FXML
	private Button btnDelete;

	@FXML
	private Button btnShowall;

	@FXML
	private Button btnClear;

	@FXML
	private TableView<InsuranceCompanyDtl> tblInsuranceCompany;

	@FXML
	private TableColumn<InsuranceCompanyDtl, String> clInsuranceComp;
	@FXML
	private TableColumn<InsuranceCompanyDtl, String> clPolicyType;

	@FXML
	private TableColumn<InsuranceCompanyDtl, String> clPercentageConverage;

	@FXML
	private TableColumn<InsuranceCompanyDtl, String> clContactPerson;

	@FXML
	private TableColumn<InsuranceCompanyDtl, String> clTelephoneNo;

	@FXML
	void actionClear(ActionEvent event) {
		clearFields();
//		tblInsurenceCompanyDtl.getSelectionModel().clearSelection();
//		tblInsuranceCompany.getSelectionModel().clearSelection();
	}

    @FXML
    void insuranceClicked(MouseEvent event) {
    	cmbInsuranceCompany.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					ResponseEntity<List<InsuranceCompanyMst>> insurance = RestCaller
							.getInsuranceCompanyByName(newValue);
					
					List<InsuranceCompanyMst> insuranceCompLIst = insurance.getBody();
					if(insuranceCompLIst.size() > 0)
					{
						InsuranceCompanyMst insuranceCompanyMst = insuranceCompLIst.get(0);
						if(null != insuranceCompanyMst)
						{
							txtContactPerson.setText(insuranceCompanyMst.getContactPerson());
							txtTelNo.setText(insuranceCompanyMst.getPhoneNumber());

						}
					}
				}

			}
		});
    }
	@FXML
	private void initialize() {
		eventBus.register(this);
		ResponseEntity<List<InsuranceCompanyMst>> savedInsuranceCompanyMst = RestCaller.getAllInsuranceCompanyMst();
		insuranceCompanyMstList = FXCollections.observableArrayList(savedInsuranceCompanyMst.getBody());
		for (InsuranceCompanyMst insuranceCompanyMst : insuranceCompanyMstList) {
			cmbInsuranceCompany.getItems().add(insuranceCompanyMst.getInsuranceCompanyName());

		}


		tblInsuranceCompany.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						System.out.println("getSelectionModel");
						if (null != newSelection.getId()) {

							insuranceCompanyDtl = new InsuranceCompanyDtl();
							insuranceCompanyDtl.setId(newSelection.getId());
							txtContactPerson.setText(newSelection.getContactPerson());
							txtTelNo.setText(newSelection.getPhoneNumber());

						}
					}
				});

		cmbInsuranceCompany.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

				if (null != newValue) {
					ResponseEntity<List<InsuranceCompanyMst>> insurance = RestCaller
							.getInsuranceCompanyByName(newValue);
					
					List<InsuranceCompanyMst> insuranceCompLIst = insurance.getBody();
					if(insuranceCompLIst.size() > 0)
					{
						InsuranceCompanyMst insuranceCompanyMst = insuranceCompLIst.get(0);
						if(null != insuranceCompanyMst)
						{
							txtContactPerson.setText(insuranceCompanyMst.getContactPerson());
							txtTelNo.setText(insuranceCompanyMst.getPhoneNumber());

						}
					}
				}

			}
		});

	}

	@FXML
	void actionDelete(ActionEvent event) {

		if (null != insuranceCompanyDtl) {

			if (null != insuranceCompanyDtl.getId()) {
				
				
				RestCaller.deleteInsuranceCompanyDtlById(insuranceCompanyDtl.getId());
				
			
			showAll();
			clearFields();
			notifyMessage(5, "deleted", false);
			return;
			}
		}
			
		
	}

	private void addItem() {
		if (null == cmbInsuranceCompany.getValue())

		{
			notifyMessage(5, "Please select Insurance Company", false);
			cmbInsuranceCompany.requestFocus();
			return;
		}
		if (txtContactPerson.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select ContactPerson", false);
			return;
		}
		if (txtTelNo.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select value", false);
			return;
		}

		if (txtPolicyType.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select PolicyType", false);
			return;
		}
		if (txtPercentageCoverage.getText().trim().isEmpty()) {
			notifyMessage(5, "Please select PercentageCoverage", false);
			return;
		}

		ResponseEntity<List<InsuranceCompanyMst>> insuranceCompResp = RestCaller
				.getInsuranceCompanyByName(cmbInsuranceCompany.getSelectionModel().getSelectedItem().toString());

		List<InsuranceCompanyMst> insuranceCompanyMstList = insuranceCompResp.getBody();
		if (insuranceCompanyMstList.size() > 0) {
			insuranceCompanyMst = insuranceCompanyMstList.get(0);
		}

		if (null == insuranceCompanyMst) {
			insuranceCompanyMst = new InsuranceCompanyMst();

			String vno1 = RestCaller.getVoucherNumber("INS" + SystemSetting.getUser().getBranchCode());
			insuranceCompanyMst.setId(vno1);
			insuranceCompanyMst.setContactPerson(txtContactPerson.getText());
			insuranceCompanyMst.setInsuranceCompanyName(cmbInsuranceCompany.getSelectionModel().getSelectedItem());
			insuranceCompanyMst.setPhoneNumber(txtTelNo.getText());

			ResponseEntity<InsuranceCompanyMst> insuranceCompanyMstSaved = RestCaller
					.saveInsuranceCompanyMst(insuranceCompanyMst);
			insuranceCompanyMst = insuranceCompanyMstSaved.getBody();
		}

		if (null == insuranceCompanyMst) {
			notifyMessage(5, "Invalid Insurance Company ", false);
			return;
		}

		InsuranceCompanyDtl insuranceCompanyDtl = new InsuranceCompanyDtl();

		String vno = RestCaller.getVoucherNumber("INS" + SystemSetting.getUser().getBranchCode());
		insuranceCompanyDtl.setId(vno);
		insuranceCompanyDtl.setPolicyType(txtPolicyType.getText());
		insuranceCompanyDtl.setPercentageCoverage(txtPercentageCoverage.getText());
		insuranceCompanyDtl.setInsuranceCompanyMst(insuranceCompanyMst);

		ResponseEntity<InsuranceCompanyDtl> insurancecompdtlresp = RestCaller
				.createInsuranceCompanyDtl(insuranceCompanyDtl);

		//{
			notifyMessage(5, "Saved ", true);
			txtPolicyType.clear();
			txtPercentageCoverage.clear();
			txtContactPerson.clear();
			txtTelNo.clear();
			cmbInsuranceCompany.setValue(null);
			insuranceCompanyDtl=null;
			insuranceCompanyMst	=null;
			showAll();
			fillTable();

			return;
		//}
	}



	@FXML
	void actionSave(ActionEvent event) {
		addItem();
	}

	private void fillTable() {
		tblInsuranceCompany.setItems(insuranceCompanyDtlList);

		for (InsuranceCompanyDtl insurance : insuranceCompanyDtlList) {
			insurance.setInsuranceCompanyName(insurance.getInsuranceCompanyMst().getInsuranceCompanyName());
			insurance.setContactPerson(insurance.getInsuranceCompanyMst().getContactPerson());
			insurance.setPhoneNumber(insurance.getInsuranceCompanyMst().getPhoneNumber());
		}
		clContactPerson.setCellValueFactory(cellData -> cellData.getValue().getContactPersonProperty());
		clInsuranceComp.setCellValueFactory(cellData -> cellData.getValue().getInsuranceCompanyNameProperty());
		clTelephoneNo.setCellValueFactory(cellData -> cellData.getValue().getPhoneNumberProperty());
		clPolicyType.setCellValueFactory(cellData -> cellData.getValue().getPolicyTypeProperty());
		clPercentageConverage.setCellValueFactory(cellData -> cellData.getValue().getPercentageCoverageProperty());
	}

	private void clearFields() {
		txtContactPerson.clear();
		txtPolicyType.clear();
		cmbInsuranceCompany.setValue(null);
		txtPercentageCoverage.clear();
		txtTelNo.clear();
		insuranceCompanyDtl = null;
		insuranceCompanyMst = null;
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	@FXML
	void actionShowAll(ActionEvent event) {
		showAll();

	}

	private void showAll() {

		ResponseEntity<List<InsuranceCompanyDtl>> dtlresp = RestCaller.getInsuranceCompanyDtl();
		insuranceCompanyDtlList = FXCollections.observableArrayList(dtlresp.getBody());

		fillTable();

	}

	@FXML
	void contactPersonOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtTelNo.requestFocus();
		}

	}

	@FXML
	void policyTypeOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtPercentageCoverage.requestFocus();
		}
	}

	@FXML
	void insuraceCompanyOnKeypress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtContactPerson.requestFocus();
		}
	}

	@FXML
	void percentageCoverageOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}
	}

	@FXML
	void policyTypeOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtPercentageCoverage.requestFocus();
		}
	}

	@FXML
	void telNoOnKeyPress(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtPolicyType.requestFocus();
		}
	}

	@FXML
	void actionAddItem(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@Subscribe
	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
		// Stage stage = (Stage) btnClear.getScene().getWindow();
		// if (stage.isShowing()) {
		taskid = taskWindowDataEvent.getId();
		processInstanceId = taskWindowDataEvent.getProcessInstanceId();

		String hdrId = taskWindowDataEvent.getBusinessProcessId();
		System.out.println("Business Process ID = " + hdrId);

		PageReload();
	}

	private void PageReload() {

	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
}
