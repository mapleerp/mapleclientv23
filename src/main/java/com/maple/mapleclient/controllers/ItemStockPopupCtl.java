package com.maple.mapleclient.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.InvoiceEditEnableMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPopUp;
import com.maple.mapleclient.entity.ItemStockPopUp;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.StockTransferOutDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

/*
 * This popup Controller is used when u need the stock quantity along with Item details
 * Eg:- Sales Window, Stock Transfer Window uses this popup instead of ItemPopup to display items
 */

public class ItemStockPopupCtl {
	String taskid;
	String processInstanceId;

	String custName = null, CustId = null;
	boolean initializedCalled = false;

	public static String windowName = "";
	ItemPopupEvent itemPopupEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<ItemStockPopUp> popUpItemList1 = FXCollections.observableArrayList();

	private ObservableList<ItemStockPopUp> popUpItemList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	
	@FXML
	private TextField txtname;

	@FXML
	private Button btnSubmit;

	@FXML
	private Button btnEnquiry;

	@FXML
	private TextField txtEnquiry;
	@FXML
	private TableView<ItemStockPopUp> tblItemPopupView;

	@FXML
	private TableColumn<ItemStockPopUp, String> columnItemName;

	@FXML
	private TableColumn<ItemStockPopUp, String> columnItemCode;

	@FXML
	private TableColumn<ItemStockPopUp, String> ColumnBarcode;

	@FXML
	private TableColumn<ItemStockPopUp, String> ColumnUnit;

	@FXML
	private TableColumn<ItemStockPopUp, Number> ColumnQty;

	@FXML
	private TableColumn<ItemStockPopUp, String> columnBatch;

	@FXML
	private TableColumn<ItemStockPopUp, Number> columnMrp;

	@FXML
	private TableColumn<ItemStockPopUp, Number> columnTax;
	@FXML
	private TableColumn<ItemStockPopUp, String> clItemCode;

	@FXML
	private TableColumn<ItemStockPopUp, LocalDate> clExpiryDate;
	
    @FXML
    private TableColumn<ItemStockPopUp, String> clStore;

	@FXML
	private Button btnCancel;
   
	@FXML
	private void initialize() {
		if (initializedCalled)
			return;

		initializedCalled = true;

		itemPopupEvent = new ItemPopupEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);
		

		btnCancel.setCache(true);

		/*
		 * Bind SearchString with the Textbox so that lisat will be filtered as user
		 * type in item name
		 * 
		 */
		txtname.textProperty().bindBidirectional(SearchString);
		/*
		 * The instance variable 'popUpItemList' is managed inside the function
		 * LoadItemPopupBySearch. So just call LoadItemPopupBySearch and the the list
		 * will be filled with values. Search String is passed as parameter
		 */

		LoadItemPopupBySearch("");

		tblItemPopupView.setItems(popUpItemList);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemcodeProperty());
		ColumnBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		ColumnUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitnameProperty());
		ColumnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnTax.setCellValueFactory(cellData -> cellData.getValue().getTaxProperty());
		clItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemcodeProperty());
		clExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpDateProperty());
		clStore.setCellValueFactory(cellData -> cellData.getValue().getStoreNameProperty());
		
		tblItemPopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getItemName() && newSelection.getItemName().length() > 0) {
					if (null != newSelection.getItemName()) {
						itemPopupEvent.setItemName(newSelection.getItemName());
//						txtname.setText(newSelection.getItemName());
					}
//				    		itemPopupEvent.setItemCode(newSelection.getItemCode());
					if (null != newSelection.getBarcode()) {
						itemPopupEvent.setBarCode(newSelection.getBarcode());
					}
					if (null != newSelection.getTaxProperty()) {
						itemPopupEvent.setTaxRate(newSelection.getTax());
					}

					if (null != newSelection.getCess()) {
						itemPopupEvent.setCess(newSelection.getCess());
					}
					if (null != newSelection.getItemId()) {
						itemPopupEvent.setItemId(newSelection.getItemId());
					}
					if (null != newSelection.getUnitId()) {
						itemPopupEvent.setUnitId(newSelection.getUnitId());
					}
					if (null != newSelection.getMrp()) {
						itemPopupEvent.setMrp(newSelection.getMrp());
					}
					if (null != newSelection.getUnitname()) {
						itemPopupEvent.setUnitName(newSelection.getUnitname());
					}
					if (null != newSelection.getBatch()) {
						itemPopupEvent.setBatch(newSelection.getBatch());
					}
					if (null != newSelection.getTax()) {
						itemPopupEvent.setTaxRate(newSelection.getTax());
					}
					if (null != newSelection.getQty()) {
						itemPopupEvent.setQty(newSelection.getQty());
					}

					if (null != newSelection.getexpiryDate()) {
						java.sql.Date sqlDate = java.sql.Date
								.valueOf(SystemSetting.UtilDateToString(newSelection.getexpiryDate(), "yyyy-MM-dd"));
						itemPopupEvent.setExpiryDate(sqlDate);
					}
					if (null != newSelection.getitemPriceLock()) {
						itemPopupEvent.setItemPriceLock(newSelection.getitemPriceLock());
					}
					if (null != newSelection.getStoreName()) {
						itemPopupEvent.setStoreName(newSelection.getStoreName());
					}
//					eventBus.post(itemPopupEvent);
					// System.out.println("itemPopupEventitemPopupEvent" + itemPopupEvent);
				}
			}

		});

		/*
		 * Following Change listener will triggerfiltering. The moment user type
		 * something on the search textBox, this event will be triggered and table is
		 * filterd
		 */

		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadItemPopupBySearch((String) newValue);
			}
		});

	}

	@FXML
	void OnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			eventBus.post(itemPopupEvent);
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP
				|| event.getCode() == KeyCode.KP_UP) {

		} else {
			txtname.requestFocus();
		}

		initializedCalled = false;

	}

	@FXML
	void tableOnClick(MouseEvent event) {
		if (tblItemPopupView.getSelectionModel().getSelectedItem() != null) {
			TableViewSelectionModel selectionModel = tblItemPopupView.getSelectionModel();
			popUpItemList1 = selectionModel.getSelectedItems();
			for (ItemStockPopUp stk : popUpItemList1) {
				txtname.setText(stk.getItemName());
			}
		}

	}

	@FXML
	void OnKeyPressTxt(KeyEvent event) {

		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblItemPopupView.requestFocus();
			tblItemPopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {

			itemPopupEvent.setItemName(null);
			eventBus.post(itemPopupEvent);
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			windowName = "";
			stage.close();

		}
		if (event.getCode() == KeyCode.BACK_SPACE && txtname.getText().length() == 0) {
			itemPopupEvent.setItemName(null);
			eventBus.post(itemPopupEvent);
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			windowName = "";
			stage.close();

		}
	}

	@FXML
	void onCancel(ActionEvent event) {

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		itemPopupEvent.setItemName(null);
		eventBus.post(itemPopupEvent);
		windowName = "";
		stage.close();

	}

	@FXML
	void submit(ActionEvent event) {

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		eventBus.post(itemPopupEvent);
		windowName = "";
		stage.close();

	}

	@FXML
	void enquiryOnAction(ActionEvent event) {

		// getCustomer(custName);
		if (null == custName)
			showEnquiryPopUp();
		else
			showEnquiryPopUpWithCustomer();

	}

	public void getCustomer(String customer) {

		custName = customer;
		ResponseEntity<AccountHeads> getcustomer = RestCaller.getAccountHeadsByName(custName);
		if (null != getcustomer.getBody())
			CustId = getcustomer.getBody().getId();
		else
			return;

	}

	private void showEnquiryPopUp() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EnquiryPopup.fxml"));
			Parent root = loader.load();
			String searchData = txtname.getText();
			EnquiryPopUpCtl enquiryPopUpCtl = loader.getController();
			enquiryPopUpCtl.showEnquiry(searchData, txtEnquiry.getText());
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showEnquiryPopUpWithCustomer() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/EnquiryPopup.fxml"));
			Parent root = loader.load();
			String searchData = txtname.getText();
			EnquiryPopUpCtl enquiryPopUpCtl = loader.getController();
			enquiryPopUpCtl.showEnquiryWithCustomer(searchData, CustId, txtEnquiry.getText());
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//   				dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void LoadItemPopupByBarcode(String searchData) {
//		custName = null;
		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		/*
		 * java.util.LinkedHashMap items = new LinkedHashMap();
		 * 
		 * Clear the Table before calling the Rest
		 * 
		 * 
		 * 
		 * 
		 * popUpItemList.clear();
		 * 
		 * 
		 * ArrayList itemsList = new ArrayList();
		 * 
		 * Clear the Table before calling the Rest
		 * 
		 * 
		 * 
		 * 
		 * itemsList = RestCaller.SearchStockItemByBarcode(searchData);
		 * 
		 * String itemName = ""; String barcode = ""; Double mrp = 0.0; String unitname
		 * = ""; Double qty = 0.0; Double cess = 0.0; Double tax = 0.0; String itemId =
		 * ""; String unitId = "";
		 * 
		 * String batch ="";
		 * 
		 * Iterator itr = itemsList.iterator(); while (itr.hasNext()) {
		 * 
		 * HashMap element = (HashMap) itr.next();
		 * 
		 * 
		 * itemName = (String) element.get("itemName"); barcode = (String)
		 * element.get("barCode"); //unitname = (String) element.get(2); mrp = (Double)
		 * element.get("standardPrice"); // qty = (Double) element.get(); cess =
		 * (Double) element.get("cess"); tax = (Double) element.get("taxRate") ; itemId
		 * = (String) element.get("id"); unitId = (String) element.get("unitId"); //
		 * batch = (String) element.get(9);
		 * 
		 * 
		 * if (null != itemName) { ItemStockPopUp itemStockPopUp = new ItemStockPopUp();
		 * itemStockPopUp.setBarcode((String) barcode); itemStockPopUp.setCess(null ==
		 * cess ? 0 : cess); itemStockPopUp.setItemId(null == itemId ? "" : itemId);
		 * itemStockPopUp.setItemName(null == itemName ? "" : itemName);
		 * itemStockPopUp.setTax(null == tax ? 0 : tax); itemStockPopUp.setUnitId(null
		 * == unitId ? "" : unitId); itemStockPopUp.setUnitname(null == unitname ? "" :
		 * unitname); itemStockPopUp.setQty(null == qty ? 0 : qty );
		 * itemStockPopUp.setBatch(null== batch ? "" : batch);
		 * itemStockPopUp.setMrp(null== mrp ? 0 : mrp);
		 * popUpItemList.add(itemStockPopUp);
		 * 
		 * } } return;
		 */
		popUpItemList.clear();
		ArrayList items = new ArrayList();

	//	ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("BATCHWISE_STOCK_POPUP");

		/*ParamValueConfig paramValueConfig = getParamValue.getBody();
		if (null != paramValueConfig) {
			if (paramValueConfig.getValue().equalsIgnoreCase("YES")) {
				
				String logDate = SystemSetting.UtilDateToString(SystemSetting.getSystemDate(), "yyyy-MM-dd");

				items = RestCaller.SearchStockItemByBatch(searchData,logDate);

			} else {
				items = RestCaller.SearchStockItemByBarcode(searchData);

			}

		} else {

			items = RestCaller.SearchStockItemByBarcode(searchData);

		}*/
			

		items = RestCaller.SearchStockItemByBarcode(searchData);

		String itemName = "";
		String barcode = "";
		Double mrp = 0.0;
		String unitname = "";
		Double qty = 0.0;
		Double cess = 0.0;
		Double tax = 0.0;
		String itemId = "";
		String unitId = "";
		Iterator itr = items.iterator();
		String batch = "";
		String storeName = "";
		java.sql.Date startDate = null;
//		String itemPriceLock;

		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			barcode = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			qty = (Double) element.get(4);
			cess = (Double) element.get(5);
			tax = (Double) element.get(6);
			itemId = (String) element.get(7);
			unitId = (String) element.get(8);
			batch = (String) element.get(9);
			storeName = (String) element.get(10);
			if(null != element.get(11))
			{
				startDate = SystemSetting.StringToSqlDateSlash((String) element.get(11), "yyyy-MM-dd");

			}
//			itemPriceLock = (String) element.get(10);

			if (null != itemName) {
				ItemStockPopUp itemStockPopUp = new ItemStockPopUp();
				itemStockPopUp.setBarcode((String) barcode);
				itemStockPopUp.setCess(null == cess ? 0 : cess);
				itemStockPopUp.setItemId(null == itemId ? "" : itemId);
				itemStockPopUp.setItemName(null == itemName ? "" : itemName);
				itemStockPopUp.setTax(null == tax ? 0 : tax);
				itemStockPopUp.setUnitId(null == unitId ? "" : unitId);
				itemStockPopUp.setUnitname(null == unitname ? "" : unitname);
				itemStockPopUp.setQty(null == qty ? 0 : qty);
				itemStockPopUp.setBatch(null == batch ? "" : batch);
				itemStockPopUp.setMrp(null == mrp ? 0 : mrp);
				itemStockPopUp.setStoreName(null == storeName ? "" : storeName);
//				if (null != paramValueConfig) {
//					if (paramValueConfig.getValue().equalsIgnoreCase("YES")) {
				System.out.println("Date");
			
				itemStockPopUp.setexpiryDate(startDate);
				
//					}
//				}
//				itemStockPopUp.setitemPriceLock(null== itemPriceLock ? "" : itemPriceLock);

				popUpItemList.add(itemStockPopUp);

			}
		}

		if (items.size() == 1) {

			tblItemPopupView.getSelectionModel().selectFirst();
		}

		return;

	}

	private void LoadItemPopupBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ArrayList items = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		popUpItemList.clear();
/*
		ResponseEntity<ParamValueConfig> getParamValuePopup = RestCaller.getParamValueConfig("BATCHWISE_STOCK_POPUP");

		ParamValueConfig paramValueConfig = getParamValuePopup.getBody();
		if (null != paramValueConfig) {
			
			if (paramValueConfig.getValue().equalsIgnoreCase("YES")) {
				
				String logDate = SystemSetting.UtilDateToString(SystemSetting.getSystemDate(), "yyyy-MM-dd");
				
				items = RestCaller.SearchStockItemByBatch(searchData,logDate);
				
			}
			
		} else if (windowName.equalsIgnoreCase("WHOLESALE") || windowName.equalsIgnoreCase("POS")  
	|| windowName.equalsIgnoreCase("KOT")) {
			ResponseEntity<ParamValueConfig> getParamValue = RestCaller
					.getParamValueConfig("SHOW_ZERO_STOCK_FOR_BATCH");
			if (null != getParamValue.getBody()) {
				if (getParamValue.getBody().getValue().equalsIgnoreCase("NO")) {
					items = RestCaller.SearchStockItemByNameWithCompWithoutZero(searchData);
				} else {
					items = RestCaller.SearchStockItemByNameWithComp(searchData);
				}
			} else {
				items = RestCaller.SearchStockItemByNameWithComp(searchData);
			}
		} else {
			items = RestCaller.SearchStockItemByNameWithComp(searchData);
		}

*/

		String logDate = SystemSetting.UtilDateToString(SystemSetting.getSystemDate(), "yyyy-MM-dd");
		
		items = RestCaller.SearchStockItemByBatch(searchData,logDate);
		
		String itemName = "";
		String barcode = "";
		Double mrp = 0.0;
		String unitname = "";
		Double qty = 0.0;
		Double cess = 0.0;
		Double tax = 0.0;
		String itemId = "";
		String unitId = "";
		String itemCode = "";
		Iterator itr = items.iterator();
		String batch = "";
		String expdate;
		String itemPriceLock;
		String storeName = "";
		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			barcode = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			qty = (Double) element.get(4);
			cess = (Double) element.get(5);
			tax = (Double) element.get(6);
			itemId = (String) element.get(7);
			unitId = (String) element.get(8);
			batch = (String) element.get(9);
			itemCode = (String) element.get(10);
			expdate = (String) element.get(11);

			itemPriceLock = (String) element.get(12);
			storeName = (String) element.get(13);
			
			if (null != itemName) {
				ItemStockPopUp itemStockPopUp = new ItemStockPopUp();
				itemStockPopUp.setBarcode((String) barcode);
				itemStockPopUp.setCess(null == cess ? 0 : cess);
				itemStockPopUp.setItemId(null == itemId ? "" : itemId);
				itemStockPopUp.setItemName(null == itemName ? "" : itemName);
				itemStockPopUp.setTax(null == tax ? 0 : tax);
				itemStockPopUp.setUnitId(null == unitId ? "" : unitId);
				itemStockPopUp.setUnitname(null == unitname ? "" : unitname);
				itemStockPopUp.setQty(null == qty ? 0 : qty);
				itemStockPopUp.setBatch(null == batch ? "" : batch);
				itemStockPopUp.setMrp(null == mrp ? 0 : mrp);
				itemStockPopUp.setItemcode((String) itemCode);
				itemStockPopUp.setitemPriceLock(null == itemPriceLock ? "" : itemPriceLock);
				itemStockPopUp.setStoreName(null == storeName ? "" : storeName);
//				if (null != paramValueConfig) {
//					if (paramValueConfig.getValue().equalsIgnoreCase("YES")) {
				if (null != expdate) {
					
//					Date udate = SystemSetting.StringToUtilDate(expdate, "yyyy-MM-dd");
					itemStockPopUp.setexpiryDate(SystemSetting.StringToSqlDateSlash(expdate, "yyyy-MM-dd"));

				}
//					}
//				}

				popUpItemList.add(itemStockPopUp);

			}
		}

		if (items.size() == 1) {

			tblItemPopupView.getSelectionModel().selectFirst();
		}

		return;

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload();
	   		}


	     private void PageReload() {
	     	
	   }
	 	public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}
}
