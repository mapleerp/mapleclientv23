package com.maple.mapleclient.controllers;


import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.CategoryMst;
import com.maple.mapleclient.entity.StockValueReports;
import com.maple.mapleclient.events.CategoryEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class StockValueReport {
	
	String taskid;
	String processInstanceId;
	
	

	private EventBus eventBus = EventBusFactory.getEventBus();
	private final NumberFormat integerFormat = new DecimalFormat("#,###.##");

	private ObservableList<StockValueReports> StockValueList = FXCollections.observableArrayList();
    @FXML
    private AnchorPane clFromItemName;

    @FXML
    private Button btnGet;

  

    @FXML
    private DatePicker datePickerFromDate;

 
    @FXML
    private TableView<StockValueReports> tblItems;

    @FXML
    private TableColumn<StockValueReports, String> clItemName;

    @FXML
    private TableColumn<StockValueReports, Number> clStock;

    @FXML
    private TableColumn<StockValueReports, Number> clValue;

    @FXML
    private TextField txtTotalValue;
    


    @FXML
    private ComboBox<String> cmbPriceType;
    
    @FXML
   private TextField   txtCategoryName;
	@FXML
	private void initialize() {
		datePickerFromDate = SystemSetting.datePickerFormat(datePickerFromDate, "dd/MMM/yyyy");
    cmbPriceType.getItems().add("COST PRICE");
	cmbPriceType.getItems().add("STANDARD PRICE");
	
	eventBus.register(this);
//	setCategory();
	
	}

    @FXML
    void getReport(ActionEvent event) {
    	
    	if(null == datePickerFromDate.getValue())
    	{
    		notifyMessage(5, "please select date", false);
    		datePickerFromDate.requestFocus();
    		return;
    	}
    	
    	
    	if( txtCategoryName.getText().isEmpty())
    	{
    		notifyMessage(5, "please select Category", false);
    		txtCategoryName.requestFocus();
    		return;
    	}
    	
    	if(null == cmbPriceType.getValue())
    	{
    		notifyMessage(5, "please select Price Type", false);
    		cmbPriceType.requestFocus();
    		return;
    	}
    	
    	ResponseEntity<CategoryMst> categoryMstResp = RestCaller.getCategoryByName(txtCategoryName.getText());
    	CategoryMst categoryMst = categoryMstResp.getBody();
    	
    	Date udate = SystemSetting.localToUtilDate(datePickerFromDate.getValue());
    	
    	String date = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
    	
    	if(cmbPriceType.getValue().equalsIgnoreCase("COST PRICE"))
    	{
    		StockValueByCostPrice(categoryMst.getId(),date);
    	} else {
			StockValueByStandardPrice(categoryMst.getId(),date);
		}
    	
    	
    	
    	
    	
//    	txtTotalValue.clear();
//    	StockValueList.clear();
//    	LocalDate dpfDate = datePickerFromDate.getValue();
//		java.util.Date fDate  = SystemSetting.localToUtilDate(dpfDate);
//		String  strFDate = SystemSetting.UtilDateToString(fDate, "yyyy-MM-dd");
//        ResponseEntity <List<StockValueReports>>respentity = RestCaller.getStockValueReport(strFDate);
//		StockValueList = FXCollections.observableArrayList(respentity.getBody());
//    	fillTable();

    }

	private void StockValueByStandardPrice(String categoryId, String date) {
		ResponseEntity <List<StockValueReports>>respentity = RestCaller.getStockValueByStandardPrice(categoryId,date);
		StockValueList = FXCollections.observableArrayList(respentity.getBody());
    	fillTable();
	}

	private void StockValueByCostPrice(String categoryId, String date) {
		
    	
    	ResponseEntity <List<StockValueReports>>respentity = RestCaller.getStockValueByCostPrice(categoryId,date);
		StockValueList = FXCollections.observableArrayList(respentity.getBody());
    	fillTable();
	}



	 private void loadCategoryPopup() {
			
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CategoryPopup.fxml"));
				Parent root1;
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	private void fillTable() {
		
		
		Double valueTotal = 0.0;
		
		for(StockValueReports value : StockValueList)
		{
			valueTotal = valueTotal + value.getClosingValue();
			
		}
		tblItems.setItems(StockValueList);
		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		
		clStock.setCellValueFactory(cellData -> cellData.getValue().getClosingQtyProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getClosingValueProperty());
		BigDecimal valutTotal = new BigDecimal(valueTotal);
		valutTotal = valutTotal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		txtTotalValue.setText(valutTotal.toString());
		
		clStock.setCellFactory(tc-> new TableCell<StockValueReports, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});
		clValue.setCellFactory(tc-> new TableCell<StockValueReports, Number>(){
			@Override
			protected void updateItem(Number value, boolean empty)
			{
				if(value == null || empty) {
					setText("");
				} else {
					setText(integerFormat.format(value));
				}
			}
		});

	}
	/*
	 * private void setCategory() {
	 * 
	 * ArrayList cat = new ArrayList();
	 * 
	 * cat = RestCaller.SearchCategory(); Iterator itr1 = cat.iterator(); while
	 * (itr1.hasNext()) { LinkedHashMap lm = (LinkedHashMap) itr1.next(); Object
	 * categoryName = lm.get("categoryName"); Object id = lm.get("id"); if (id !=
	 * null) { cmbCategory.getItems().add((String) categoryName);
	 * 
	 * }
	 * 
	 * } }
	 */
	
	

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	

	  @Subscribe
		public void popupOrglistner(CategoryEvent CategoryEvent) {

			Stage stage = (Stage) btnGet.getScene().getWindow();
			if (stage.isShowing()) {

				
				txtCategoryName.setText(CategoryEvent.getCategoryName());
		
			
			}
			}
    @FXML
    void categoryPoup(MouseEvent event) {
    	loadCategoryPopup();
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload(hdrId);
   		}


   	private void PageReload(String hdrId) {

   	}


}
