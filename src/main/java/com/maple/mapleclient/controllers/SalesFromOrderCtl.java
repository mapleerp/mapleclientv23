package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.jasper.NewJasperPdfReportService;
import com.maple.javapos.print.POSThermalPrintABS;
import com.maple.javapos.print.POSThermalPrintFactory;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AccountReceivable;
import com.maple.mapleclient.entity.BatchPriceDefinition;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryMst;

import com.maple.mapleclient.entity.DayEndClosureDtl;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.DeliveryBoyMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SaleOrderRealizedMst;
import com.maple.mapleclient.entity.SaleOrderReceipt;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesOrderDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.CustomerEvent;
//import com.maple.mapleclient.events.CustomerEvent;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.SaleOrderHdrSearchEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.sun.prism.paint.Color;

import javafx.beans.binding.SetBinding;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.application.Platform;

public class SalesFromOrderCtl {
	
	String taskid;
	String processInstanceId;

	String accounId = null;
	String salOrderPrintFormat = SystemSetting.SALE_ORDER_PRINT_FORMAT;
	POSThermalPrintABS printingSupport;
	double cardAmount = 0.0;
	String selectedSaleOrderHdrId = null;

	private ObservableList<ReceiptModeMst> receiptModeList = FXCollections.observableArrayList();
	EventBus eventBus = EventBusFactory.getEventBus();
	SalesReceipts salesReceipts = null;
	SalesReceipts salesReceiptsCash = null;
	String salesReceiptVoucherNo = null;

	ItemStockPopupCtl itemStockPopupCtl = new ItemStockPopupCtl();
	private ObservableList<SalesReceipts> salesReceiptsList = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> salesDtlListTable = FXCollections.observableArrayList();
	private ObservableList<SalesDtl> saleListTable = FXCollections.observableArrayList();
	List<SalesOrderDtl> salesOrderDtlList = new ArrayList<SalesOrderDtl>();
	SalesOrderTransHdr salesOrderTransHdr = new SalesOrderTransHdr();
	SalesDtl salesDtl = null;
	SalesTransHdr salesTransHdr = null;
	String fixedQty = null;

	double qtyTotal = 0;
	double discountTotal = 0;
	double taxTotal = 0;
	double cessTotal = 0;
	double discountBfTaxTotal = 0;
	double grandTotal = 0;
	double expenseTotal = 0;

	String custId = "";

	StringProperty cardAmountLis = new SimpleStringProperty("");
	StringProperty paidAmtProperty = new SimpleStringProperty("");
	StringProperty sodexoAmtProperty = new SimpleStringProperty("");
	StringProperty itemNameProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");
	StringProperty barcodeProperty = new SimpleStringProperty("");
	StringProperty taxRateProperty = new SimpleStringProperty("");
	StringProperty mrpProperty = new SimpleStringProperty("");
	StringProperty unitNameProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty changeAmtProperty = new SimpleStringProperty("");

	SalesDtl toDeleteSale = null;

	@FXML
	private TextField txtCashPaid;

	@FXML
	private TextField txtSaleOrderHdrId;

	@FXML
	void SaleOrderHdrIdPopup(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			SaleOrderPopUp();
		}

	}

	@FXML
	private TextField txtCardPaid;

	@FXML
	private TableView<SalesDtl> itemDetailTable;

	@FXML
	private TableColumn<SalesDtl, String> columnItemName;

	@FXML
	private TableColumn<SalesDtl, String> columnBarCode;

	@FXML
	private TextField txtBalance;

	@FXML
	private CheckBox chkCreditSale;

	@FXML
	private TableColumn<SalesDtl, String> columnQty;

	@FXML
	private TableColumn<SalesDtl, String> columnTaxRate;

	@FXML
	private TableColumn<SalesDtl, String> columnMrp;

	@FXML
	private TableColumn<SalesDtl, String> columnBatch;

	@FXML
	private TableColumn<SalesDtl, String> columnRate;

	@FXML
	private TableColumn<SalesDtl, String> columnUnitName;

	@FXML
	private TableColumn<SalesDtl, Number> columnAmount;

	@FXML
	private TableColumn<SalesDtl, LocalDate> columnExpiryDate;

	@FXML
	private TextField txtItemname;

	@FXML
	private TextField txtRate;

	@FXML
	private TextField txtTax;

	@FXML
	private TextField txtItemcode;

	@FXML
	private Button btnAdditem;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtBatch;

	@FXML
	private Button btnUnhold;

	@FXML
	private Button btnHold;

	@FXML
	private TextField txtcardAmount;

	@FXML
	private Button btnSave;

	@FXML
	private TextField txtPaidamount;

	@FXML
	private TextField txtCashtopay;

	@FXML
	private TextField txtChangeamount;

	@FXML
	private TextField custname;

	@FXML
	private TextField custAdress;

	@FXML
	private TextField gstNo;

	@FXML
	private TextField txtSodexoPaid;

	@FXML
	private TextField txtCustomerName;

	@FXML
	private TextField txtCustomerPhoneNo;

	@FXML
	private Button btnFetchData;

	@FXML
	private TextField txtAccount;

	@FXML
	private TextField txtTotalPaidAmount;
	
	
    @FXML
    private ComboBox<String> cmbInvoicePrintType;

	// -------------------new version 2.2 surya

	@FXML
	private ComboBox<String> cmbDeliveryBoy;

	// -------------------new version 2.2 surya

//----------------------------------------------------------------------------------

	@FXML
	private ComboBox<String> cmbCardType;

	@FXML
	void FocusOnCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtCrAmount.requestFocus();
		}
	}

	@FXML
	void setCardType(MouseEvent event) {

		setCardType();
	}

	@FXML
	private TextField txtCrAmount;

	@FXML
	void KeyPressFocusAddBtn(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			if (txtCrAmount.getText().length() == 0 && null == cmbCardType.getValue()) {
				btnSave.requestFocus();
			} else {

				AddCardAmount.requestFocus();
			}
		}

	}

	private void setCardType() {
		cmbCardType.getItems().clear();

		ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptMode();
		receiptModeList = FXCollections.observableArrayList(receiptModeResp.getBody());

		for (ReceiptModeMst receiptModeMst : receiptModeList) {
			if (null != receiptModeMst.getReceiptMode() && !receiptModeMst.getReceiptMode().equals("CASH")) {
				cmbCardType.getItems().add(receiptModeMst.getReceiptMode());
			}
		}
	}

	@FXML
	private Button AddCardAmount;

	@FXML
	void KeyPressAddCardAmount(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			addCardAmount();
		}

	}

	@FXML
	void addCardAmount(ActionEvent event) {

		addCardAmount();

	}

	private void addCardAmount() {

		if (null != salesTransHdr) {

			if (null == cmbCardType.getValue()) {

				notifyMessage(5, "Please select card type!!!!");
				cmbCardType.requestFocus();
				return;

			} else if (txtCrAmount.getText().trim().isEmpty()) {

				notifyMessage(5, "Please enter amount!!!!");
				txtCrAmount.requestFocus();
				return;

			} else {

				salesReceipts = new SalesReceipts();
				salesReceipts.setReceiptMode(cmbCardType.getSelectionModel().getSelectedItem());
				if (Double.parseDouble(txtCrAmount.getText()) > Double.parseDouble(txtBalance.getText())) {
					notifyMessage(3, "Invalid Amount");
					txtCrAmount.clear();
					return;
				}
				salesReceipts.setReceiptAmount(Double.parseDouble(txtCrAmount.getText()));
				salesReceipts.setUserId(SystemSetting.getUser().getId());
				salesReceipts.setBranchCode(SystemSetting.systemBranch);

				if (null == salesReceiptVoucherNo) {
					salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
				} else {
					salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
				}

				// find by account name

				ResponseEntity<AccountHeads> accountHeads = RestCaller
						.getAccountHeadByName(cmbCardType.getSelectionModel().getSelectedItem());
				salesReceipts.setAccountId(accountHeads.getBody().getId());
				LocalDate date = LocalDate.now();
				java.util.Date udate = SystemSetting.localToUtilDate(date);
				salesReceipts.setReceiptDate(udate);
				salesReceipts.setSalesTransHdr(salesTransHdr);
				System.out.println(salesReceipts);
				ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
				salesReceipts = respEntity.getBody();

				if (null != salesReceipts) {
					if (null != salesReceipts.getId()) {
						notifyMessage(5, "Successfully added cardAmount");
//	    				cardAmount = cardAmount+Double.parseDouble(txtCrAmount.getText());
//	    				txtcardAmount.setText(Double.toString(cardAmount));
						salesReceiptsList.add(salesReceipts);
						filltableCardAmount();
					}
				}

				txtCrAmount.clear();
				cmbCardType.getSelectionModel().clearSelection();
				cmbCardType.requestFocus();
				salesReceipts = null;

			}
		} else {
			notifyMessage(5, "Please add Item!!!!");
			return;
		}
	}

	@FXML
	private Button btnDeleteCardAmount;

	@FXML
	void DeleteCardAmount(ActionEvent event) {

		if (null != salesReceipts) {
			if (null != salesReceipts.getId()) {
				RestCaller.deleteSalesReceipts(salesReceipts.getId());
				tblCardAmountDetails.getItems().clear();
				ResponseEntity<List<SalesReceipts>> salesreceiptResp = RestCaller
						.getAllSalesReceiptsBySalesTransHdr(salesTransHdr.getId());
				salesReceiptsList = FXCollections.observableArrayList(salesreceiptResp.getBody());

				filltableCardAmount();

			}
		}

	}

	@FXML
	void KeyPressDeleteCardAmount(KeyEvent event) {

	}

	@FXML
	private TableView<SalesReceipts> tblCardAmountDetails;

	@FXML
	private TableColumn<SalesReceipts, String> clCardTye;

	@FXML
	private TableColumn<SalesReceipts, Number> clCardAmount;

	private void filltableCardAmount() {

		setCardSaleVisible();

		tblCardAmountDetails.setItems(salesReceiptsList);
		clCardAmount.setCellValueFactory(cellData -> cellData.getValue().getReceiptAmountProperty());
		clCardTye.setCellValueFactory(cellData -> cellData.getValue().getReceiptModeProperty());

		cardAmount = RestCaller.getSumofSalesReceiptbySalestransHdrId(salesTransHdr.getId());
		if (cardAmount > 0) {
			txtcardAmount.setText(Double.toString(cardAmount));
		} else {
			txtcardAmount.setText("0.0");
		}

	}

//----------------------------------------------------------------------------------

	private void setCardSaleVisible() {

		cmbCardType.setVisible(true);
		cmbCardType.setVisible(true);
		tblCardAmountDetails.setVisible(true);
		txtCrAmount.setVisible(true);
		AddCardAmount.setVisible(true);
		btnDeleteCardAmount.setVisible(true);
		tblCardAmountDetails.setVisible(true);
		txtcardAmount.setVisible(true);
		txtCrAmount.setVisible(true);
	}

	@FXML
	void AccountPopup(MouseEvent event) {

		loadCustomerPopup();

	}

	private void loadCustomerPopup() {
		/*
		 * Function to display popup window and show list of suppliers to select.
		 */
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/custPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			gstNo.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void SaleOrderPopUp() {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SaleOrderSearchPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();

			gstNo.requestFocus();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Subscribe
	public void popupCustomerlistner(CustomerEvent customerEvent) {

		Stage stage = (Stage) btnAdditem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtAccount.setText(customerEvent.getCustomerName());
					accounId = customerEvent.getCustId();
				}
			});
		}

	}

	@Subscribe
	public void popupSaleOrderListner(SaleOrderHdrSearchEvent saleOrderHdrSearchEvent) {

		Stage stage = (Stage) txtSaleOrderHdrId.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					salesTransHdr = null;
					txtCashtopay.setText("");
					txtPaidamount.setText("");
					saleListTable.clear();

					salesDtl = new SalesDtl();
					txtCustomerPhoneNo.clear();
					txtCustomerName.clear();
					txtCardPaid.clear();
					txtCashPaid.clear();
					itemDetailTable.getItems().clear();
					txtBalance.clear();
					txtAccount.clear();
					txtcardAmount.clear();
					tblCardAmountDetails.getItems().clear();
					txtTotalPaidAmount.clear();
					salesReceiptVoucherNo = null;
					salesReceiptsCash = null;
					salesOrderTransHdr = null;

					cmbDeliveryBoy.getSelectionModel().clearSelection();

					qtyTotal = 0;
					// double amountTotal = 0;
					discountTotal = 0;
					taxTotal = 0;
					cessTotal = 0;
					discountBfTaxTotal = 0;
					grandTotal = 0;
					expenseTotal = 0;
					custId = "";
					cardAmount = 0.0;

					cmbCardType.setVisible(false);
					cmbCardType.setVisible(false);
					tblCardAmountDetails.setVisible(false);
					txtCrAmount.setVisible(false);
					AddCardAmount.setVisible(false);
					btnDeleteCardAmount.setVisible(false);
					tblCardAmountDetails.setVisible(false);
					txtcardAmount.setVisible(false);
					txtCrAmount.setVisible(false);

					txtSaleOrderHdrId.setText(saleOrderHdrSearchEvent.getVoucherNo());
					selectedSaleOrderHdrId = saleOrderHdrSearchEvent.getId();
				}
			});
		}

	}

	@FXML
	void FetchData(ActionEvent event) {

		salesReceiptsList.clear();
		txtcardAmount.clear();
		tblCardAmountDetails.getItems().clear();

		if (SystemSetting.systemBranch.equalsIgnoreCase(null)) {
			notifyMessage(5, "Incorrect Branch");
			return;
		}
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.applicationDate);
		if (null != dayEndClosureHdr) {
			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return;
				}
			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.applicationDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return;
		}

		salesTransHdr = null;

		itemDetailTable.getItems().clear();
		salesDtlListTable.clear();

		if (!txtSaleOrderHdrId.getText().trim().isEmpty()) {

			// --------------saleorder conversion
			boolean conversionSatatus = doCovertOrderToSalesService();
			if (!conversionSatatus) {
				return;
			}
			
			ResponseEntity<List<SalesTransHdr>> salesTransHdrResp = RestCaller.getSalesTranHdrByorderId(salesOrderTransHdr.getId());
			List<SalesTransHdr> salesTransHdrList = salesTransHdrResp.getBody();
			if(salesTransHdrList.size() > 0)
			{
				salesTransHdr = salesTransHdrList.get(0);
			} else {
				return;
			}
			if (null != salesTransHdr) {
				if (null != salesTransHdr.getId()) {
					AccountHeads cust = new AccountHeads();
					ResponseEntity<AccountHeads> respentityCust = RestCaller
							.getAccountHeadsById(salesTransHdr.getCustomerId());
					cust = respentityCust.getBody();
					txtAccount.setText(cust.getAccountName());

					ResponseEntity<LocalCustomerMst> resplocal = RestCaller
							.getLocalCustomerbyId(salesTransHdr.getLocalCustomerMst().getId());
					if (null != resplocal.getBody()) {
						txtCustomerName.setText(resplocal.getBody().getLocalcustomerName());
						txtCustomerPhoneNo.setText(resplocal.getBody().getPhoneNo());
					}

					ResponseEntity<List<SalesDtl>> respentity = RestCaller.getSalesDtl(salesTransHdr);
					salesDtlListTable = FXCollections.observableArrayList(respentity.getBody());

					FillTable();
					Double totalPaidAmount = 0.0;
					if (!txtTotalPaidAmount.getText().trim().isEmpty()) {
						totalPaidAmount = Double.parseDouble(txtTotalPaidAmount.getText());

					}
					Double cardamount = 0.0;
					if (!txtcardAmount.getText().trim().isEmpty()) {
						cardamount = Double.parseDouble(txtcardAmount.getText());
					}

					Double cashToPay = Double.parseDouble(txtCashtopay.getText());
					Double balance = cashToPay - (totalPaidAmount + cardamount);
					if (balance > 0) {

						BigDecimal bdCashToPay = new BigDecimal(balance);
						bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
						txtBalance.setText(bdCashToPay.toString());
					} else {
						txtBalance.setText("0.0");
					}
				}
			}
		}

	}

//	@FXML
//	void SelectOrder(ActionEvent event) {
//
////		cmbOrder.getItems().clear();
////
////		getAllSalesOrder();
//
//	}

//	@FXML
//	void SetSalesOrderTransHdrId(MouseEvent event) {
//
//		getAllSalesOrder();
//
//	}

	double amountTotal = 0;

	@FXML
	void onEnterCustPopup(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

		}
	}

	@FXML
	void CustomerPopUp(MouseEvent event) {

	}

	@FXML
	void mouseClickOnItemName(MouseEvent event) {
	}

	@FXML
	void qtyKeyRelease(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (txtQty.getText().length() > 0)
				doCovertOrderToSalesService();

		}
	}

	@FXML
	void keyPressOnItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
		}
	}

	@FXML
	private void initialize() {
		
		cmbInvoicePrintType.getItems().add("GST");
		cmbInvoicePrintType.getItems().add("LOCAl CUSTOMER");


		printingSupport = POSThermalPrintFactory.getPOSThermalPrintABS();
		try {
			printingSupport.setupLogo();

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			printingSupport.setupBottomline(SystemSetting.getPosInvoiceBottomLine());

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		/*
		 * final submit. SalesDtl will be added on AddItem Function
		 * 
		 */
		// salesDtl = new SalesDtl();
		// ============set order list to combobox

//		getAllSalesOrder();

		/*
		 * Fieds with Entity property
		 */
		salesDtl = new SalesDtl();

		txtPaidamount.textProperty().bindBidirectional(paidAmtProperty);
		txtItemname.textProperty().bindBidirectional(itemNameProperty);
		txtcardAmount.textProperty().bindBidirectional(cardAmountLis);
		txtChangeamount.textProperty().bindBidirectional(changeAmtProperty);
		txtBarcode.textProperty().bindBidirectional(barcodeProperty);

		txtRate.textProperty().bindBidirectional(mrpProperty);

		txtBatch.textProperty().bindBidirectional(batchProperty);
		txtTax.textProperty().bindBidirectional(taxRateProperty);

		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtRate.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtRate.setText(oldValue);
				}
			}
		});

		txtTax.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtTax.setText(oldValue);
				}
			}
		});
		txtPaidamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPaidamount.setText(oldValue);
				}
			}
		});
		txtChangeamount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtChangeamount.setText(oldValue);
				}
			}
		});
		txtCashtopay.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCashtopay.setText(oldValue);
				}
			}
		});

		txtcardAmount.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtcardAmount.setText(oldValue);
				}
			}
		});

		cardAmountLis.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				double advanceCash = 0.0;
				if (!txtTotalPaidAmount.getText().trim().isEmpty()) {
					advanceCash = Double.parseDouble(txtTotalPaidAmount.getText());
				}
				if ((txtPaidamount.getText().length() > 0)) {

					double paidAmount = Double.parseDouble(txtPaidamount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());

					double cardAmt = Double.parseDouble((String) newValue);
					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt + advanceCash) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {
					double cashToPay = Double.parseDouble(txtCashtopay.getText());
					double cardAmount = Double.parseDouble((String) newValue);
					BigDecimal newrate = new BigDecimal((cardAmount + advanceCash) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		paidAmtProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				double advanceCash = 0.0;
				if (!txtTotalPaidAmount.getText().trim().isEmpty()) {
					advanceCash = Double.parseDouble(txtTotalPaidAmount.getText());
				}
				if ((txtcardAmount.getText().length() > 0)) {

					double cardAmt = Double.parseDouble(txtcardAmount.getText());
					double cashToPay = Double.parseDouble(txtCashtopay.getText());

					double paidAmount = Double.parseDouble((String) newValue);

					if (cardAmt > 0) {
						BigDecimal newrate = new BigDecimal((paidAmount + cardAmt + advanceCash) - cashToPay);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);
						changeAmtProperty.set(newrate.toPlainString());

					}
				} else {

					double cashToPay = 0.0;
					double paidAmt = 0.0;

					try {
						cashToPay = Double.parseDouble(txtCashtopay.getText());
					} catch (Exception e) {

					}
					try {
						paidAmt = Double.parseDouble((String) newValue);
					} catch (Exception e) {

					}

					BigDecimal newrate = new BigDecimal((paidAmt + advanceCash) - cashToPay);
					newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

					if (newrate.doubleValue() < 0) {
						newrate = newrate.abs();

					}

					changeAmtProperty.set(newrate.toPlainString());

				}
			}
		});

		itemDetailTable.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					toDeleteSale = new SalesDtl();

					toDeleteSale.setId(newSelection.getId());
				}
			}
		});

		tblCardAmountDetails.getSelectionModel().selectedItemProperty()
				.addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						if (null != newSelection.getId()) {

							salesReceipts = new SalesReceipts();

							salesReceipts.setId(newSelection.getId());
						}
					}
				});
		// btnSave.setDisable(true);
//		itemDetailTable.setItems(saleListTable);

		txtBarcode.requestFocus();
		eventBus.register(this);

		ResponseEntity<List<DeliveryBoyMst>> deliveryBoyMstResp = RestCaller.getDeliveryBoyMst();
		List<DeliveryBoyMst> deliveryBoyMstList = deliveryBoyMstResp.getBody();

		for (DeliveryBoyMst deliveryBoyMst : deliveryBoyMstList) {
			cmbDeliveryBoy.getItems().add(deliveryBoyMst.getDeliveryBoyName());
		}

	}

	@FXML
	void saleOrderOnOnClick(MouseEvent event) {
		SaleOrderPopUp();
	}

	@FXML
	void deleteRow(ActionEvent event) {
		try {

			if (null != toDeleteSale) {
				if (null != toDeleteSale) {
					System.out.println("toDeleteSale.getId()" + toDeleteSale.getId());
					RestCaller.deleteSalesDtl(toDeleteSale.getId());

					ResponseEntity<List<SalesDtl>> SalesDtlResponse = RestCaller.getSalesDtl(salesTransHdr);
					saleListTable = FXCollections.observableArrayList(SalesDtlResponse.getBody());
					FillTable();
					notifyMessage(5, " Item Deleted Successfully");
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void hold(ActionEvent event) {

	}

	@FXML
	void save(ActionEvent event) {

		FinalSave();

	}

	@FXML
	void EnterItemName(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			// if(event.getSource());

			txtQty.requestFocus();
		}

	}

	@FXML
	void SaveButton(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			FinalSave();
		}

	}

	@FXML
	void onClickBarcodeBack(KeyEvent event) {

	}

	@FXML
	private Label lblCardType;

	@FXML
	private Label lblAmount;

	@FXML
	void toPrintChange(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {

			btnSave.setDisable(false);

			Double change = Double.parseDouble(txtPaidamount.getText()) - Double.parseDouble(txtCashtopay.getText());
			txtChangeamount.setText(Double.toString(change));
			btnSave.requestFocus();
		}
		if (event.getCode() == KeyCode.LEFT) {

			setCardSaleVisible();

		}

	}

	@FXML
	void calcPaid(KeyEvent event) {
	}

	@FXML
	void unHold(ActionEvent event) {

	}

	private void FillTable() {

		itemDetailTable.setItems(salesDtlListTable);

		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnBarCode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		columnQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		columnTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
		columnMrp.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		columnBatch.setCellValueFactory(cellData -> cellData.getValue().getBatchCodeProperty());
		columnRate.setCellValueFactory(cellData -> cellData.getValue().getRateProperty());
		columnUnitName.setCellValueFactory(cellData -> cellData.getValue().getUnitNameProperty());
		columnExpiryDate.setCellValueFactory(cellData -> cellData.getValue().getExpiryDateProperty());
		columnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		Summary summary = null;
		if (salesTransHdr.getAccountHeads().getCustomerDiscount() > 0) {
			summary = RestCaller.getSalesWindowSummaryDiscount(salesTransHdr.getId());
		} else {
			summary = RestCaller.getSalesWindowSummary(salesTransHdr.getId());
		}
//		salesTransHdr.setCashPaidSale(amount);
//			salespos.setTempAmount(amount);
		// saleListTable.add(salespos);
		if (null != summary.getTotalAmount()) {

			System.out.println("===============summary================" + summary);

			salesTransHdr.setCashPaidSale(summary.getTotalAmount());
			BigDecimal bdCashToPay = new BigDecimal(summary.getTotalAmount());
			bdCashToPay = bdCashToPay.setScale(2, BigDecimal.ROUND_CEILING);
			txtCashtopay.setText(Double.toString(salesOrderTransHdr.getInvoiceAmount()));

		} else {
			txtCashtopay.setText("");
		}

	}

	@FXML
	void addItemButtonClick(ActionEvent event) {
//		addItem();
	}
	private boolean doCovertOrderToSalesService() {
		
		ResponseEntity<SalesOrderTransHdr> salesOrderHdrResponse = RestCaller
				.getSalesOrderTransHdrByVoucherNo(txtSaleOrderHdrId.getText());

		salesOrderTransHdr = salesOrderHdrResponse.getBody();
		
		if(null == salesOrderTransHdr)
		{
			notifyFailureMessage(2, "Invalid Saleorder");
			return false;

		}
		
		ResponseEntity<String> result = RestCaller.converSalesOrderToSales(salesOrderTransHdr.getId(),   SystemSetting.getUserId() );
		
		if(result.getBody().equalsIgnoreCase("OK")) {
			return true;
		}
		return false;
	}

	private boolean doCovertOrderToSalesOld() {

		if (SystemSetting.systemBranch.equalsIgnoreCase(null)) {
			notifyMessage(5, "Incorrect Branch");
			return false;
		}
		ResponseEntity<DayEndClosureHdr> maxofDay = RestCaller.getMaxDayEndClosure();
		DayEndClosureHdr dayEndClosureHdr = maxofDay.getBody();
		System.out.println("Sys Date before add" + SystemSetting.applicationDate);
		if (null != dayEndClosureHdr) {
			if (null != dayEndClosureHdr.getDayEndStatus()) {
				String process_date = SystemSetting.UtilDateToString(dayEndClosureHdr.getProcessDate(), "yyyy-MM-dd");
				String sysdate = SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyy-MM-dd");
				java.sql.Date prDate = Date.valueOf(process_date);
				Date sDate = Date.valueOf(sysdate);
				int i = prDate.compareTo(sDate);
				if (i > 0 || i == 0) {
					notifyMessage(1, " Day End Already Done for this Date !!!!");
					return false;
				}
			}
		}

		Boolean dayendtodone = SystemSetting.DayEndHasToBeDone(SystemSetting.applicationDate);

		if (!dayendtodone) {
			notifyMessage(1, "Day End should be done before changing the date !!!!");
			return false;
		}
		salesTransHdr = null;
		if (!txtSaleOrderHdrId.getText().trim().isEmpty()) {
			ResponseEntity<SalesOrderTransHdr> salesOrderHdrResponse = RestCaller
					.getSalesOrderTransHdrByVoucherNo(txtSaleOrderHdrId.getText());

			salesOrderTransHdr = salesOrderHdrResponse.getBody();

			RestCaller.deleteSalesTransHdrBySalesOrderTransHdr(salesOrderTransHdr.getId());

			if (null != salesOrderTransHdr.getDeliveryBoyId()) {
				ResponseEntity<DeliveryBoyMst> deliveryBoyResp = RestCaller
						.getdeliveryBoyId(salesOrderTransHdr.getDeliveryBoyId());
				DeliveryBoyMst deliveryBoyMst = deliveryBoyResp.getBody();
				if (null != deliveryBoyMst) {
					cmbDeliveryBoy.getSelectionModel().select(deliveryBoyMst.getDeliveryBoyName());

				}

			}

			if (null != salesOrderTransHdr) {
				Double totalPaidAmount = 0.0;

				totalPaidAmount = salesOrderTransHdr.getPaidAmount();
				if (null != salesOrderTransHdr.getCashPay()) {
					txtTotalPaidAmount.setText(Double.toString(salesOrderTransHdr.getCashPay()));
				} else {
					txtTotalPaidAmount.setText("0.0");
				}

				if (null == salesTransHdr) {

					salesTransHdr = new SalesTransHdr();
					salesTransHdr.setSaleOrderHrdId(salesOrderTransHdr.getId());

					salesTransHdr.setPaidAmount(totalPaidAmount);
					salesTransHdr.setCustomerId(salesOrderTransHdr.getAccountHeads().getId());
					salesTransHdr.setSalesMode("ORDER-CONVERTION");
					salesTransHdr.setCreditOrCash("CASH");
					salesTransHdr.setIsBranchSales("N");
					salesTransHdr.setUserId(SystemSetting.getUserId());
					salesTransHdr.setBranchCode(SystemSetting.systemBranch);
					if (null != salesOrderTransHdr.getAccountHeads()) {
						/*
						 * new url for getting account heads instead of customer mst=========05/0/2022
						 */
//						CustomerMst customerMst = RestCaller.getCustomerById(salesOrderTransHdr.getCustomerId().getId())
//								.getBody();
						ResponseEntity<AccountHeads> accountHeads=RestCaller.getAccountHeadsById(salesOrderTransHdr.getAccountHeads().getId());
						salesTransHdr.setAccountHeads(accountHeads.getBody());
						if(accountHeads.getBody().getAccountName().equalsIgnoreCase("POS"))
						{
							salesTransHdr.setSalesMode("POS");

						} else {
							if (null == accountHeads.getBody().getPartyGst() || accountHeads.getBody().getPartyGst().length() < 13) {
								salesTransHdr.setSalesMode("B2C");
							} else {
								salesTransHdr.setSalesMode("B2B");
							}
						}
						
					}
					// salesTransHdr.setDiscount(Double.toString(d));
					if (null != salesOrderTransHdr.getLocalCustomerId()) {
						LocalCustomerMst localCustomerMst = RestCaller
								.getLocalCustomerById(salesOrderTransHdr.getLocalCustomerId().getId()).getBody();
						salesTransHdr.setLocalCustomerMst(localCustomerMst);
					}
					salesTransHdr.setDiscount(Double.toString(salesTransHdr.getAccountHeads().getCustomerDiscount()));
					ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
					salesTransHdr = respentity.getBody();
					System.out.println("=======salesTransHdr======" + salesTransHdr);

					ResponseEntity<List<SaleOrderReceipt>> saleOrderReceiptList = RestCaller
							.getAllSaleOrderReceiptsBySalesTranOrdersHdr(salesOrderTransHdr.getId());
					Double advanceAmount = 0.0;
					if (null != salesTransHdr) {
						for (SaleOrderReceipt saleOrderReceipt : saleOrderReceiptList.getBody()) {
							if (!saleOrderReceipt.getReceiptMode().equals("CASH")) {
								salesReceipts = new SalesReceipts();
								salesReceipts.setReceiptMode(saleOrderReceipt.getReceiptMode());
								salesReceipts.setReceiptAmount(saleOrderReceipt.getReceiptAmount());
								salesReceipts.setUserId(SystemSetting.getUser().getId());
								salesReceipts.setBranchCode(SystemSetting.systemBranch);
								salesReceipts.setSoStatus("CONVERTED");
								if (null == salesReceiptVoucherNo) {
									salesReceiptVoucherNo = RestCaller
											.getVoucherNumber(SystemSetting.getSystemBranch());
									salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
								} else {
									salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
								}

								salesReceipts.setAccountId(saleOrderReceipt.getAccountId());
								LocalDate date = LocalDate.now();
								java.util.Date udate = SystemSetting.localToUtilDate(date);
								salesReceipts.setReceiptDate(udate);
								salesReceipts.setSalesTransHdr(salesTransHdr);

								ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
								salesReceipts = respEntity.getBody();
								if (null != salesReceipts) {
									salesReceiptsList.add(salesReceipts);

								}
								filltableCardAmount();
							} else {

								if (null != saleOrderReceipt.getReceiptAmount()) {
									advanceAmount = advanceAmount + saleOrderReceipt.getReceiptAmount();

									txtTotalPaidAmount.setText(Double.toString(advanceAmount));
								} else {
									txtTotalPaidAmount.setText("0.0");
								}
							}
						}
					}

				}

				if (null != salesTransHdr) {
					if (null != salesTransHdr.getId()) {

						ResponseEntity<List<SalesOrderDtl>> respentity = RestCaller
								.getSalesOrderTransDtlById(salesOrderTransHdr.getId());

						salesOrderDtlList = respentity.getBody();
						System.out.println("salesOrderDtlList--" + salesOrderDtlList.size());

						Boolean notinstock = false;
						List<String> itemNotInStockList = new ArrayList<String>();
 

						int i = 0;
						int j = 0;
						for (SalesOrderDtl salesOrderDtl : salesOrderDtlList) {

							System.out.println("SalesOrderDtl =" + i);

							Map<String, Double> map = new HashMap<String, Double>();
							map = RestCaller.getRequiredMaterial(salesOrderDtl.getItemId(), salesOrderDtl.getQty());

							if (map.size() == 0 || null == map || map.isEmpty()) {
								ResponseEntity<KitDefinitionMst> getKitMst = RestCaller
										.getKitByItemId(salesOrderDtl.getItemId());
								if (null != getKitMst.getBody()) {
									map.put("NOBATCH", salesOrderDtl.getQty());
								} else {
									notinstock = true;
									itemNotInStockList
											.add(salesOrderDtl.getItemName() + "(" + salesOrderDtl.getQty() + ")");
								}
							}

							for (Map.Entry<String, Double> batchAndQty : map.entrySet()) {

								System.out.println("batchAndQty =" + j);
								j++;

								salesDtl = new SalesDtl();
								salesDtl.setSalesTransHdr(salesTransHdr);
								salesDtl.setItemId(salesOrderDtl.getItemId());
								salesDtl.setBarcode(salesOrderDtl.getBarcode());
								salesDtl.setBatchCode(batchAndQty.getKey());

								ResponseEntity<ItemMst> getItem = RestCaller.getitemMst(salesOrderDtl.getItemId());
								if (null != salesOrderDtl.getCessRate()) {
									salesDtl.setCessRate(salesOrderDtl.getCessRate());
								} else {
									salesDtl.setCessRate(0.0);
								}
								salesDtl.setExpiryDate(salesOrderDtl.getexpiryDate());
								salesDtl.setItemCode(salesOrderDtl.getItemCode());
								salesDtl.setItemName(salesOrderDtl.getItemName());
								java.util.Date udate = SystemSetting.getApplicationDate();
								String sdate1 = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

								ResponseEntity<PriceDefinition> pricebyItem = RestCaller
										.getPriceDefenitionByItemIdAndUnit(salesOrderDtl.getItemId(),
												salesTransHdr.getAccountHeads().getPriceTypeId(),
												salesOrderDtl.getUnitId(), sdate1);
								PriceDefinition priceDefinition = pricebyItem.getBody();
								if (null != priceDefinition) {

									salesDtl.setMrp(priceDefinition.getAmount());

									Double rate = (100 * priceDefinition.getAmount())
											/ (100 + salesOrderDtl.getTaxRate());
									salesDtl.setRate(rate);
								} else {
									salesDtl.setMrp(salesOrderDtl.getMrp());
									salesDtl.setRate(salesOrderDtl.getRate());
								}

								salesDtl.setQty(batchAndQty.getValue());
								salesDtl.setUnitId(salesOrderDtl.getUnitId());
								salesDtl.setUnitName(salesOrderDtl.getUnitName());
								if (null != salesOrderDtl.getSgstTaxRate()) {
									salesDtl.setSgstTaxRate(salesOrderDtl.getSgstTaxRate());
								} else {
									salesDtl.setSgstTaxRate(0.0);
								}
								if (null != salesOrderDtl.getCgstAmount()) {
									salesDtl.setCgstAmount(salesOrderDtl.getCgstAmount());
								} else {
									salesDtl.setCgstAmount(0.0);
								}
								if (null != salesOrderDtl.getSgstAmount()) {
									salesDtl.setSgstAmount(salesOrderDtl.getSgstAmount());
								} else {
									salesDtl.setSgstAmount(0.0);
								}
								if (null != salesOrderDtl.getCgstTaxRate()) {
									salesDtl.setCgstTaxRate(salesOrderDtl.getCgstTaxRate());
								} else {
									salesDtl.setCgstTaxRate(0.0);
								}

								salesDtl.setTaxRate(salesOrderDtl.getTaxRate());
								salesDtl.setCessRate(salesOrderDtl.getCessRate());
								salesDtl.setCessAmount(salesOrderDtl.getCessAmount());
								salesDtl.setMrp(salesOrderDtl.getMrp());
								salesDtl.setStandardPrice(salesOrderDtl.getMrp());
								salesDtl.setRate(salesOrderDtl.getRate());
								double cessAmount = 0.0;
								double cessRate = 0.0;
								double rateBeforeTax = 0.0;
 

								BigDecimal settoamount = new BigDecimal((salesDtl.getQty() * salesDtl.getRate()));

								double includingTax = (settoamount.doubleValue() * salesDtl.getTaxRate()) / 100;
								double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
								BigDecimal setamount = new BigDecimal(amount);
								setamount = setamount.setScale(2, BigDecimal.ROUND_CEILING);
								salesDtl.setAmount(setamount.doubleValue());

								ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
										.getPriceDefenitionMstByName("COST PRICE");
								PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
								if (null != priceDefenitionMst) {
									String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate,
											"yyyy-MM-dd");
									ResponseEntity<BatchPriceDefinition> batchpriceDef = RestCaller
											.getBatchPriceDefinition(salesDtl.getItemId(), priceDefenitionMst.getId(),
													salesDtl.getUnitId(), salesDtl.getBatch(), sdate);
									if (null != batchpriceDef.getBody()) {
										salesDtl.setCostPrice(batchpriceDef.getBody().getAmount());
									} else {
										ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
												.getPriceDefenitionByCostPrice(salesDtl.getItemId(),
														priceDefenitionMst.getId(), salesDtl.getUnitId(), sdate);

										PriceDefinition priceDefinition1 = priceDefenitionResp.getBody();
										if (null != priceDefinition1) {
											salesDtl.setCostPrice(priceDefinition1.getAmount());
										}
									}
								}

								ResponseEntity<SalesDtl> salesDtlrespentity = RestCaller.saveSalesDtl(salesDtl);
								salesDtl = salesDtlrespentity.getBody();
								System.out.println("=======salesDtl======" + salesDtl);
								System.out.println("=======salesDtl ID======" + salesDtl.getId());

							}

						}

						if (notinstock) {
							notifyMessage(5, "Not in Stock " + itemNotInStockList.toString() + "...!");
							txtCustomerName.clear();
							txtAccount.clear();
							txtCustomerPhoneNo.clear();
							txtSaleOrderHdrId.clear();
							txtTotalPaidAmount.clear();
							txtCashtopay.clear();
							txtBalance.clear();
							txtPaidamount.clear();
							txtChangeamount.clear();
							salesTransHdr = null;
							return false;
						}

					}

				}

			}

		}

		return true;
	}

	private void FinalSave() {
		// -------------------verification 5.0 Surya
//		String stockVerification = RestCaller.StockVerificationBySalesTransHdr(salesTransHdr.getId());
//		if (null != stockVerification) {
//			notifyMessage(5, stockVerification);
//
////			return;
//
//		} else {

			// -------------------verification 5.0 Surya end
			ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtl(salesTransHdr);
			for (SalesDtl salesDtl : getSalesDtl.getBody()) {
				ArrayList items = new ArrayList();

				ResponseEntity<ItemMst> itemMst = RestCaller.getitemMst(salesDtl.getItemId());

				items = RestCaller.getSingleStockItemByName(itemMst.getBody().getItemName(), salesDtl.getBatch());
				Double chkQty = 0.0;
				String itemId = null;
				
//				chkQty = RestCaller.getQtyFromItemBatchMstByItemIdAndQty(itemMst.getBody().getId(), txtBatch.getText(),storeNameFromPopUp);
				
				Iterator itr = items.iterator();
				while (itr.hasNext()) {
					List element = (List) itr.next();
					chkQty = (Double) element.get(4);
					itemId = (String) element.get(7);
				}
				ResponseEntity<KitDefinitionMst> getKitMst = RestCaller.getKitByItemId(itemMst.getBody().getId());
				if (null == getKitMst.getBody()) {
					if (!itemMst.getBody().getUnitId().equalsIgnoreCase(itemMst.getBody().getUnitId())) {
						Double conversionQty = RestCaller.getConversionQty(itemMst.getBody().getUnitId(),
								itemMst.getBody().getUnitId(), itemMst.getBody().getUnitId(), salesDtl.getQty());
						System.out.println(conversionQty);
						if (chkQty < conversionQty) {
							notifyMessage(1, "Not in Stock!!!" + itemMst.getBody().getItemName());
							return;
						}
					}
				}
			}
			Double cashPaid = 0.0;
//		Double cardAmount = 0.0;
			Double cashToPay = 0.0;
			Double advanceAmount = 0.0;
//		Double sodexo = 0.0;
			if (null != salesTransHdr) {

				if (!txtCashtopay.getText().trim().isEmpty()) {
					cashToPay = Double.parseDouble(txtCashtopay.getText());
					salesTransHdr.setInvoiceAmount(cashToPay);

				}

				if (!txtTotalPaidAmount.getText().trim().isEmpty()) {
					advanceAmount = Double.parseDouble(txtTotalPaidAmount.getText());
				}

				if (!txtcardAmount.getText().trim().isEmpty()) {
					cardAmount = Double.parseDouble(txtcardAmount.getText());
					/*
					 * if(cardAmount>Double.parseDouble(txtBalance.getText())) {
					 * notifyMessage(3,"Invalid Card Amount"); return; }
					 */
					// Test if Balance to Pay is less than total Card Paid
					if (null != cashToPay && ((cashToPay - cardAmount) < -2)) {
						notifyMessage(3, "Total Card Paid is Greater then Balance to Pay");
						return;
					}
					salesTransHdr.setCardamount(cardAmount);
				}
				txtChangeamount.setText("00.0");
				if (!txtPaidamount.getText().trim().isEmpty()) {
					cashPaid = Double.parseDouble(txtPaidamount.getText());

					if (cashToPay < (advanceAmount + cardAmount + cashPaid)) {
						cashPaid = cashToPay - (advanceAmount + cardAmount);
					}
				}

				salesTransHdr.setBranchCode(SystemSetting.systemBranch);

				if (!txtChangeamount.getText().trim().isEmpty()) {
					Double changeAmount = Double.parseDouble(txtChangeamount.getText());
					salesTransHdr.setChangeAmount(changeAmount);
				}

				if (cashToPay > (advanceAmount + cardAmount + cashPaid) && !chkCreditSale.isSelected()) {

					notifyMessage(5, "Please enter Amount");
					return;
				}

				if ((cashToPay - (advanceAmount + cardAmount + cashPaid) < -1)) {

					notifyMessage(5, "Please enter Amount");
					return;
				}

				if (chkCreditSale.isSelected() && txtAccount.getText().equalsIgnoreCase("POS")
						&& cashToPay > (advanceAmount + cardAmount + cashPaid)) {
					notifyMessage(5, "Please select valid account");
					return;
				}
				/*
				 * new url for getting account heads instead of customer mst=========05/0/2022
				 */
//				ResponseEntity<CustomerMst> customerResp = RestCaller
//						.getCustomerByNameAndBarchCode(txtAccount.getText(),SystemSetting.systemBranch);
				ResponseEntity<AccountHeads> accountHeadsResp=RestCaller.getAccountHeadsByNameAndBranchCode(txtAccount.getText(),SystemSetting.systemBranch);
				AccountHeads accountHeadsss = accountHeadsResp.getBody();

				if (null == accountHeadsss) {
					ResponseEntity<AccountHeads> accountHeadsResponse = RestCaller.getAccountHeadsByName(txtAccount.getText());
					accountHeadsss = accountHeadsResponse.getBody();
				}
				salesTransHdr.setCustomerId(accountHeadsss.getId());
				salesTransHdr.setAccountHeads(accountHeadsss);
				if (chkCreditSale.isSelected() && !txtAccount.getText().equalsIgnoreCase("POS")
						&& cashToPay > (advanceAmount + cardAmount + cashPaid)) {
					Double creditAmount = cashToPay - (advanceAmount + cardAmount + cashPaid);

//				ResponseEntity<CustomerMst> customerResp = RestCaller.getCustomerByName(txtAccount.getText());
//				CustomerMst customerMst = customerResp.getBody();
//				salesTransHdr.setCustomerId(customerMst.getId());
//				salesTransHdr.setCustomerMst(customerMst);
					ResponseEntity<AccountReceivable> accountReceivableResp = RestCaller
							.getAccountReceivableBySalesTransHdrId(salesTransHdr.getId());
					AccountReceivable accountReceivable = null;
					if (null != accountReceivableResp.getBody()) {
						accountReceivable = accountReceivableResp.getBody();
					} else {
						accountReceivable = new AccountReceivable();
					}

					accountReceivable.setAccountId(accountHeadsss.getId());
					accountReceivable.setAccountHeads(accountHeadsss);

					accountReceivable.setDueAmount(creditAmount);
					accountReceivable.setDueAmount(creditAmount);
					accountReceivable.setBalanceAmount(creditAmount);

					LocalDate due = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
					LocalDate dueDate = due.plusDays(accountHeadsss.getCreditPeriod());
					accountReceivable.setDueDate(java.sql.Date.valueOf(dueDate));
					accountReceivable.setVoucherNumber(salesTransHdr.getVoucherNumber());
					accountReceivable.setSalesTransHdr(salesTransHdr);
					accountReceivable.setRemark("ORDER CONVERTION");
					LocalDate due1 = SystemSetting.utilToLocaDate(SystemSetting.systemDate);
					accountReceivable.setVoucherDate(java.sql.Date.valueOf(due1));
					accountReceivable.setPaidAmount(0.0);
					ResponseEntity<AccountReceivable> respentity = RestCaller.saveAccountReceivable(accountReceivable);
					accountReceivable = respentity.getBody();

					SalesReceipts salesReceipts = new SalesReceipts();
					salesReceipts.setReceiptMode("CREDIT");
					salesReceipts.setReceiptAmount(creditAmount);
					salesReceipts.setSalesTransHdr(salesTransHdr);
					salesReceipts.setAccountId(accountHeadsss.getId());
					salesReceipts.setBranchCode(salesTransHdr.getBranchCode());

					RestCaller.saveSalesReceipts(salesReceipts);

				}

				salesReceipts = new SalesReceipts();
				Double getCashAmount = 0.0;
				if (null != txtPaidamount.getText()) {
					if (!txtPaidamount.getText().trim().isEmpty()) {
						getCashAmount = Double.parseDouble(txtPaidamount.getText());
					}
				}
				Double getCashAmount2 = 0.0;
				
				if(!txtTotalPaidAmount.getText().trim().isEmpty())
				{
					getCashAmount2 = Double.parseDouble(txtTotalPaidAmount.getText());
				}
						
						
				if (getCashAmount > 0 || getCashAmount2 > 0) {
//					if (!txtTotalPaidAmount.getText().trim().isEmpty()) {
//						cashPaid = Double.parseDouble(txtPaidamount.getText())
//								+ Double.parseDouble(txtTotalPaidAmount.getText());
//					} else {
//						cashPaid = Double.parseDouble(txtPaidamount.getText());
//					}
					if (cashPaid <= cashToPay) {
						cashPaid = cashPaid + advanceAmount;
					} else {
						cashPaid = cashToPay + advanceAmount;
					}
					// salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
					salesTransHdr.setCashPay(cashPaid);
					salesReceipts.setReceiptMode(SystemSetting.systemBranch + "-CASH");
					salesReceipts.setReceiptAmount(cashPaid);
					salesReceipts.setUserId(SystemSetting.getUser().getId());
					salesReceipts.setBranchCode(SystemSetting.systemBranch);
					String account = SystemSetting.systemBranch + "-CASH";
					ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadByName(account);
					salesReceipts.setAccountId(accountHeads.getBody().getId());
					if (null == salesReceiptVoucherNo) {
						salesReceiptVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
						salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
					} else {
						salesReceipts.setVoucherNumber(salesReceiptVoucherNo);
					}
					LocalDate date = LocalDate.now();
					java.util.Date udate = SystemSetting.localToUtilDate(date);
					salesReceipts.setReceiptDate(udate);
					salesReceipts.setSalesTransHdr(salesTransHdr);

					ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
					salesReceipts = respEntity.getBody();
					if (null != salesReceipts) {
						salesReceiptsList.add(salesReceipts);

					}

				} else {
					cashPaid = Double.parseDouble(txtCashtopay.getText());
					if (null != salesTransHdr.getCashPay()) {
						cashPaid = cashPaid + salesTransHdr.getCashPay();
					}
					salesTransHdr.setCashPay(cashPaid);

				}

				eventBus.post(salesTransHdr);

//			salesReceipts.setReceiptAmount(cashToPay);

				java.util.Date udate = SystemSetting.applicationDate;

//			salesReceipts.setRereceiptDate(udate);
				ResponseEntity<List<SalesDtl>> saledtlSaved = RestCaller.getSalesDtl(salesTransHdr);
				if (saledtlSaved.getBody().size() == 0) {
					return;
				}
				// salesTransHdr.setInvoiceNumberPrefix(invoiceNumberPrefix);
				salesTransHdr.setSalesReceiptsVoucherNumber(salesReceiptVoucherNo);
				salesTransHdr.setVoucherDate(udate);
				
			
				if(null != cmbInvoicePrintType.getSelectionModel().getSelectedItem())
				{
					salesTransHdr.setInvoicePrintType(cmbInvoicePrintType.getSelectionModel().getSelectedItem().toString());
				}

				RestCaller.updateSalesTranshdr(salesTransHdr);
				
				salesReceipts.setSalesTransHdr(salesTransHdr);
				salesReceipts.setReceiptMode("Nill");

				salesOrderTransHdr.setOrderStatus("Realized");
//			salesTransHdr.setSalesReceiptsVoucherNumber(salesReceiptVoucherNo);

				if (null != cmbDeliveryBoy.getValue()) {

					ResponseEntity<DeliveryBoyMst> deliveryBoyMst = RestCaller
							.getDelivaryBoyByName(cmbDeliveryBoy.getSelectionModel().getSelectedItem().toString());
					salesOrderTransHdr.setDeliveryBoyId(deliveryBoyMst.getBody().getId());
				}

				RestCaller.updateSalesOrderTranshdr(salesOrderTransHdr);

				SalesTransHdr salestranshrd = RestCaller.getSalesTransHdr(salesTransHdr.getId());

//------------new version 1.0 -22-2-21- surya
				
				saveSaleOrderRealizedMst(salestranshrd);

// ------------new version 1.0 -22-2-21- surya end

				Format formatter;
				formatter = new SimpleDateFormat("yyyy-MM-dd");
				String strDate = formatter.format(udate);

				// Version 1.6 Starts

				txtCashtopay.setText("");
				txtPaidamount.setText("");
				saleListTable.clear();

				salesDtl = new SalesDtl();
				txtCustomerPhoneNo.clear();
				txtCustomerName.clear();
				txtCardPaid.clear();
				txtCashPaid.clear();
				itemDetailTable.getItems().clear();
				txtBalance.clear();
				txtAccount.clear();
				txtcardAmount.clear();
				tblCardAmountDetails.getItems().clear();
				txtTotalPaidAmount.clear();
				salesReceiptVoucherNo = null;
				salesReceiptsCash = null;
				salesOrderTransHdr = null;

				qtyTotal = 0;
				// double amountTotal = 0;
				discountTotal = 0;
				taxTotal = 0;
				cessTotal = 0;
				discountBfTaxTotal = 0;
				grandTotal = 0;
				expenseTotal = 0;
				custId = "";
				cardAmount = 0.0;
				advanceAmount = 0.0;

				cmbCardType.setVisible(false);
				cmbCardType.setVisible(false);
				tblCardAmountDetails.setVisible(false);
				txtCrAmount.setVisible(false);
				AddCardAmount.setVisible(false);
				btnDeleteCardAmount.setVisible(false);
				tblCardAmountDetails.setVisible(false);
				txtcardAmount.setVisible(false);
				txtCrAmount.setVisible(false);
				cmbDeliveryBoy.getSelectionModel().clearSelection();
				cmbInvoicePrintType.getSelectionModel().clearSelection();

				txtSaleOrderHdrId.clear();
				if (!salOrderPrintFormat.equalsIgnoreCase("POS")) {
					// Version 1.6 Ends
					try {
						NewJasperPdfReportService.TaxInvoiceReport(salestranshrd.getVoucherNumber(), strDate);
					} catch (JRException e) {

						e.printStackTrace();
					}
				} else {
					try {
						printingSupport.PrintInvoiceThermalPrinter(salesTransHdr.getId());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

//			salesReceipts.setBranchCode(branch.getId());

				/*
				 * if (paidAmount > 0.0) { salesReceipts.setReceiptMode("CASH");
				 * ResponseEntity<SalesReceipts> respentity =
				 * RestCaller.saveSalesReceipts(salesReceipts); }
				 * 
				 * if (card.isEmpty()) { salesReceipts.setReceiptMode("CARD");
				 * ResponseEntity<SalesReceipts> respentity =
				 * RestCaller.saveSalesReceipts(salesReceipts); }
				 */

				// Version 1.6 Comment start
				/*
				 * salesTransHdr = null; txtCashtopay.setText(""); txtPaidamount.setText("");
				 * saleListTable.clear();
				 * 
				 * salesDtl = new SalesDtl(); txtCustomerPhoneNo.clear();
				 * txtCustomerName.clear(); txtCardPaid.clear(); txtCashPaid.clear();
				 * itemDetailTable.getItems().clear(); txtBalance.clear(); txtAccount.clear();
				 * txtcardAmount.clear(); tblCardAmountDetails.getItems().clear();
				 * txtTotalPaidAmount.clear(); salesReceiptVoucherNo = null; salesReceiptsCash =
				 * null; salesOrderTransHdr = null;
				 * 
				 * qtyTotal = 0; // double amountTotal = 0; discountTotal = 0; taxTotal = 0;
				 * cessTotal = 0; discountBfTaxTotal = 0; grandTotal = 0; expenseTotal = 0;
				 * custId = ""; cardAmount = 0.0; advanceAmount = 0.0;
				 * 
				 * cmbCardType.setVisible(false); cmbCardType.setVisible(false);
				 * tblCardAmountDetails.setVisible(false); txtCrAmount.setVisible(false);
				 * AddCardAmount.setVisible(false); btnDeleteCardAmount.setVisible(false);
				 * tblCardAmountDetails.setVisible(false); txtcardAmount.setVisible(false);
				 * txtCrAmount.setVisible(false);
				 * 
				 * txtSaleOrderHdrId.clear();
				 */
				// Version 1.6 Comment ends

				salesTransHdr = null;

			}
			// ----------------version 5.0 surya
//		}
		// ----------------version 5.0 end
	}

	// ------------new version 1.0 -22-2-21- surya end

	private void saveSaleOrderRealizedMst(SalesTransHdr salestranshrd) {
		
		SaleOrderRealizedMst saleOrderRealizedMst = new SaleOrderRealizedMst();
		
		saleOrderRealizedMst.setSalesTransHdr(salestranshrd);
		
		ResponseEntity<SalesOrderTransHdr> salesOrderTransHdrResp = RestCaller.getSaleOrderTransHdrById(salestranshrd.getSaleOrderHrdId());
		SalesOrderTransHdr salesOrderTransHdr = salesOrderTransHdrResp.getBody();
		
		saleOrderRealizedMst.setSalesOrderTransHdr(salesOrderTransHdr);
		
		Double totalPaidAmount = 0.0;
		if (!txtTotalPaidAmount.getText().trim().isEmpty()) {
			totalPaidAmount = Double.parseDouble(txtTotalPaidAmount.getText());

		}
		
		Double cardamount = 0.0;
		if (!txtcardAmount.getText().trim().isEmpty()) {
			cardamount = Double.parseDouble(txtcardAmount.getText());
		}
		
		Double totalAdvance = totalPaidAmount+cardamount;
		
		saleOrderRealizedMst.setAdvanceAmount(totalAdvance);
		
		if(!txtCashtopay.getText().trim().isEmpty())
		{
			saleOrderRealizedMst.setInvoiceTotal(Double.parseDouble(txtCashtopay.getText()));

		}
		
		if(!txtPaidamount.getText().trim().isEmpty())
		{
			Double totalPayment = Double.parseDouble(txtCashtopay.getText()) - totalAdvance;
			saleOrderRealizedMst.setRealizedAmount(totalPayment);

		}
		
		
		ResponseEntity<SaleOrderRealizedMst> savedSaleOrderRealizedMst = RestCaller.saveSaleOrderRealizedMst(saleOrderRealizedMst);
		
	}
	
	// ------------new version 1.0 -22-2-21- surya end


	public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	public void notifyFailureMessage(int duration, String msg) {

		Image img = new Image("failed.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

//	private void getAllSalesOrder() {
//
//		cmbOrder.getItems().clear();
//		ArrayList sales = new ArrayList();
//		sales = RestCaller.getAllSalesOrder();
//		Iterator itr = sales.iterator();
//		while (itr.hasNext()) {
//			LinkedHashMap lm = (LinkedHashMap) itr.next();
//			Object id = lm.get("id");
//			Object voucherNumber = lm.get("voucherNumber");
//			if (id != null) {
//
//				cmbOrder.getItems().add((String) voucherNumber);
//
//			}
//
//		}
//
//	}

	private void getSalesOrderTransDtl() {

//		itemDetailTable.getItems().clear();
//		ResponseEntity<List<SalesOrderDtl>> respentity = RestCaller
//				.getSalesOrderTransDtlById(salesOrderTransHdr.getId());
//		salesDtlListTable = FXCollections.observableArrayList(respentity.getBody());
		FillTable();

	}
	 @Subscribe
	   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	   		//Stage stage = (Stage) btnClear.getScene().getWindow();
	   		//if (stage.isShowing()) {
	   			taskid = taskWindowDataEvent.getId();
	   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	   			
	   		 
	   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	   			System.out.println("Business Process ID = " + hdrId);
	   			
	   			 PageReload(hdrId);
	   		}


	   private void PageReload(String hdrId) {
	   	
	   }

}
