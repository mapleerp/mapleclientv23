package com.maple.mapleclient.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BalanceSheetHorizontal;
import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class BalanceSheetHorizontalCtl {
	String taskid;
	String processInstanceId;

	private ObservableList<BalanceSheetHorizontal> balancesheetlist = FXCollections.observableArrayList();

	BalanceSheetHorizontal balanceSheetHorizontal = null;
	
    @FXML
    private DatePicker dpDate;

    @FXML
    private TableView<BalanceSheetHorizontal> tbBalanceSheet;

    @FXML
    private TableColumn<BalanceSheetHorizontal,String> clSlNo;

    @FXML
    private TableColumn<BalanceSheetHorizontal,String> clAssetAccnt;

    @FXML
    private TableColumn<BalanceSheetHorizontal,Number> clAssetAmt;

    @FXML
    private TableColumn<BalanceSheetHorizontal,String> clLiabilityaccnt;

    @FXML
    private TableColumn<BalanceSheetHorizontal,Number> clLiabiltyAmt;

    @FXML
    private Button btnShow;

    @FXML
    private Button btnPrint;
    
    @FXML
   	private void initialize() {
    	dpDate = SystemSetting.datePickerFormat(dpDate, "dd/MMM/yyyy");
       
    }
    

    @FXML
    void actionPrint(ActionEvent event) {
    	if(balancesheetlist.size()>0)
    	{
    	try {
			JasperPdfReportService.BalanceSheetHorizontal(balancesheetlist);
		} catch (JRException e) {
			e.printStackTrace();
		}
    	}
    	else
    	{
    		notifyMessage(3,"Firt Generate Report then Print");
    	}
    }

    @FXML
    void actionShow(ActionEvent event) {
    	Date udate = SystemSetting.localToUtilDate(dpDate.getValue());
		String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");
		String account = "";
		Double amount = 0.0;
     	ArrayList items = new ArrayList();
     	ResponseEntity<FinancialYearMst> finance = RestCaller.getFinancialYear(sdate);
     	Date enddat = finance.getBody().getEndDate();
		String uenddat = SystemSetting.UtilDateToString(enddat, "yyyy-MM-dd");

    	items = RestCaller.getTrialBalance(uenddat);
    	
    	Iterator itr = items.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			account = (String) element.get(0);
			amount = (Double) element.get(3);
    		String accHeads = RestCaller.getRootAccount(account);
    		ResponseEntity<AccountHeads> accountHeads =RestCaller.getAccountHeadByName(account);
    	
    		try
    		{
    		if(!accHeads.equalsIgnoreCase(null))
    		{
    			if(accHeads.equalsIgnoreCase("ASSETS"))
    			{
    				balanceSheetHorizontal = new BalanceSheetHorizontal();
    				
    				balanceSheetHorizontal.setAssetAccountId(accountHeads.getBody().getId());
    				balanceSheetHorizontal.setAssetAmount(amount);
    				balanceSheetHorizontal.setFinancialYear(finance.getBody().getFinancialYear());
    				ResponseEntity<BalanceSheetHorizontal> respentity = RestCaller.saveBalanceSheetHorizontal(balanceSheetHorizontal);
    				balanceSheetHorizontal = respentity.getBody();
    				balancesheetlist.add(balanceSheetHorizontal);    			
    				fillTable();
    			}
    			else if(accHeads.equalsIgnoreCase("LIABLITY"))
    			{
    				balanceSheetHorizontal = new BalanceSheetHorizontal();
    				balanceSheetHorizontal.setFinancialYear(finance.getBody().getFinancialYear());
    				balanceSheetHorizontal.setLiabilityAccountId(accountHeads.getBody().getId());
    				balanceSheetHorizontal.setLiabilityAmount(amount);
    				ResponseEntity<BalanceSheetHorizontal> respentity = RestCaller.saveBalanceSheetHorizontal(balanceSheetHorizontal);
    				balanceSheetHorizontal = respentity.getBody();
    				balancesheetlist.add(balanceSheetHorizontal);    			
    				fillTable();
    			}
    		}
    	}
    		catch (Exception e) {
    			notifyMessage(3, "Parent Not Set"+account);
    			return;
    		}
		}
		
    	
    }

    private void fillTable()
    {
    	for(BalanceSheetHorizontal bal:balancesheetlist)
    	{
    		if(null != bal.getAssetAccountId())
    		{
    		ResponseEntity<AccountHeads> assetacc = RestCaller.getAccountHeadsById(bal.getAssetAccountId());
    		bal.setAssetAccountName(assetacc.getBody().getAccountName());
    		}
    		if(null != bal.getLiabilityAccountId())
    		{
    			ResponseEntity<AccountHeads> assetacc = RestCaller.getAccountHeadsById( bal.getLiabilityAccountId());
        		bal.setLiabilityAccountName(assetacc.getBody().getAccountName());
    		}
    	}
    	tbBalanceSheet.setItems(balancesheetlist);
		clAssetAccnt.setCellValueFactory(cellData -> cellData.getValue().getAssetAccountNameProperty());
		clAssetAmt.setCellValueFactory(cellData -> cellData.getValue().getAssetAmountProperty());
		clLiabilityaccnt.setCellValueFactory(cellData -> cellData.getValue().getLiabilityAccountNameProperty());
		clLiabiltyAmt.setCellValueFactory(cellData -> cellData.getValue().getLiabilityAmountProperty());
		clSlNo.setCellValueFactory(cellData -> cellData.getValue().getSlNoProperty());
    }
    public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @Subscribe
  	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
  		//Stage stage = (Stage) btnClear.getScene().getWindow();
  		//if (stage.isShowing()) {
  			taskid = taskWindowDataEvent.getId();
  			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
  			
  		 
  			String hdrId = taskWindowDataEvent.getBusinessProcessId();
  			System.out.println("Business Process ID = " + hdrId);
  			
  			 PageReload();
  		}


      private void PageReload() {
      	
}
}
