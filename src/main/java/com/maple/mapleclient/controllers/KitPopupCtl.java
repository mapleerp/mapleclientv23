package com.maple.mapleclient.controllers;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.google.common.eventbus.Subscribe;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.InsuranceCompanyDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemPopUp;
import com.maple.mapleclient.entity.KitDefinitionDtl;
import com.maple.mapleclient.entity.KitDefinitionMst;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.ProductionDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.KitPopupEvent;

import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

public class KitPopupCtl {
	String taskid;
	String processInstanceId;

	boolean initializedCalled = false;

	KitPopupEvent kitPopupEvent;

	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<ItemPopUp> popUpItemList = FXCollections.observableArrayList();
	
	private ObservableList<ItemPopUp> KitDefinitionMstList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();
	@FXML
	private TextField txtname;

	@FXML
	private Button btnSubmit;

	@FXML
	private TableView<ItemPopUp> tblItemPopupView;

	@FXML
	private TableColumn<ItemPopUp, String> columnItemName;

	@FXML
	private TableColumn<ItemPopUp, String> columnItemCode;

	@FXML
	private TableColumn<ItemPopUp, String> ColumnBarcode;

	@FXML
	private TableColumn<ItemPopUp, String> ColumnUnit;

	@FXML
	private Button btnCancel;

	@FXML
	private void initialize() {
		if (initializedCalled)
			return;

		initializedCalled = true;

		kitPopupEvent = new KitPopupEvent();
		eventBus.register(this);
		btnSubmit.setDefaultButton(true);

		btnCancel.setCache(true);
		
		txtname.textProperty().bindBidirectional(SearchString);

		/*
		 * The instance variable 'popUpItemList' is managed inside the function
		 * LoadItemPopupBySearch. So just call LoadItemPopupBySearch and the the list
		 * will be filled with values. Search String is passed as parameter
		 */
		LoadItemPopupBySearch("");

		tblItemPopupView.setItems(popUpItemList);
		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		columnItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemcodeProperty());
		ColumnBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		ColumnUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitnameProperty());

		tblItemPopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getItemName() && newSelection.getItemName().length() > 0) {
					
					if (null != newSelection.getItemName()) {
						kitPopupEvent.setItemName(newSelection.getItemName());
					}
//				    		itemPopupEvent.setItemCode(newSelection.getItemCode());
					if (null != newSelection.getBarcode()) {
						kitPopupEvent.setBarCode(newSelection.getBarcode());
					}
					if (null != newSelection.getTaxProperty()) {
						kitPopupEvent.setTaxRate(newSelection.getTax());
					}

					if (null != newSelection.getCess()) {
						kitPopupEvent.setCess(newSelection.getCess());
					}
					if (null != newSelection.getItemId()) {
						kitPopupEvent.setItemId(newSelection.getItemId());
					}
					if (null != newSelection.getUnitId()) {
						kitPopupEvent.setUnitId(newSelection.getUnitId());
					}
					if (null != newSelection.getMrp()) {
						kitPopupEvent.setMrp(newSelection.getMrp());
					}
					if (null != newSelection.getUnitname()) {
						kitPopupEvent.setUnitName(newSelection.getUnitname());
					}
					eventBus.post(kitPopupEvent);
					//System.out.println("itemPopupEventitemPopupEvent" + itemPopupEvent);
				}
			}

		});

		/*
		 * Following Change listener will triggerfiltering. The moment user type
		 * something on the search textBox, this event will be triggered and table is
		 * filterd
		 */

		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadItemPopupBySearch((String) newValue);
			}
		});

	}

	@FXML
	void OnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {

			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtname.requestFocus();
		}

		initializedCalled = false;

	}

	@FXML
	void OnKeyPressTxt(KeyEvent event) {

		if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tblItemPopupView.requestFocus();
			tblItemPopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
	}

	@FXML
	void onCancel(ActionEvent event) {

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();

	}

	@FXML
	void submit(ActionEvent event) {

		Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();

	}

	private void LoadItemPopupBySearch(String searchData) {

		/*
		 * This method populate the instance variable popUpItemList , which the source
		 * of data for the Table.
		 */
		ArrayList items = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		popUpItemList.clear();

		items = RestCaller.SearchItemByName(searchData);

		String itemName = "";
		String barCode = "";
		Double mrp = 0.0;
		String unitname = "";
		Integer qty = 0;
		Double cess = 0.0;
		Double tax = 0.0;
		String itemId = "";
		String unitId = "";
		Iterator itr = items.iterator();

		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			barCode = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			//qty = (Integer) element.get(4);
			cess = (Double) element.get(4);
			tax = (Double) element.get(5);
			itemId = (String) element.get(6);
			unitId = (String) element.get(7);

			if (null != itemName) {
				ItemPopUp itemPopUp = new ItemPopUp();
				itemPopUp.setBarcode((String) barCode);
				itemPopUp.setCess(null == cess ? 0 : cess);
				itemPopUp.setItemId(null == itemId ? "" : itemId);
				itemPopUp.setItemName(null == itemName ? "" : itemName);
				itemPopUp.setTax(null == tax ? 0 : tax);
				itemPopUp.setUnitId(null == unitId ? "" : unitId);
				itemPopUp.setUnitname(null == unitname ? "" : unitname);
				itemPopUp.setMrp(null == mrp ? 0 : mrp);
				popUpItemList.add(itemPopUp);

			}
		}

		return;

	}
	
	public void kitNamePopupByItemId(String searchData) {
		
		//KitDefinitionMstList.clear();
		ArrayList items = new ArrayList();
		/*
		 * Clear the Table before calling the Rest
		 */

		popUpItemList.clear();

		items = RestCaller.SearchKitByNameForPopup(searchData);

		String itemName = "";
		String barCode = "";
		Double mrp = 0.0;
		String unitname = "";
		Integer qty = 0;
		Double cess = 0.0;
		Double tax = 0.0;
		String itemId = "";
		String unitId = "";
		Iterator itr = items.iterator();

		while (itr.hasNext()) {

			List element = (List) itr.next();
			itemName = (String) element.get(0);
			barCode = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			//qty = (Integer) element.get(4);
			cess = (Double) element.get(4);
			tax = (Double) element.get(5);
			itemId = (String) element.get(6);
			unitId = (String) element.get(7);

			if (null != itemName) {
				ItemPopUp itemPopUp = new ItemPopUp();
				itemPopUp.setBarcode((String) barCode);
				itemPopUp.setCess(null == cess ? 0 : cess);
				itemPopUp.setItemId(null == itemId ? "" : itemId);
				itemPopUp.setItemName(null == itemName ? "" : itemName);
				itemPopUp.setTax(null == tax ? 0 : tax);
				itemPopUp.setUnitId(null == unitId ? "" : unitId);
				itemPopUp.setUnitname(null == unitname ? "" : unitname);
				itemPopUp.setMrp(null == mrp ? 0 : mrp);
				popUpItemList.add(itemPopUp);

			}
		}

		return;
	}
	private void FillTable() {
		tblItemPopupView.setItems(KitDefinitionMstList);
		for (ItemPopUp Kit : popUpItemList) {
			
			Kit.setItemName(Kit.getKitDefinitionMst().getKitName());
			
		}

		columnItemName.setCellValueFactory(cellData -> cellData.getValue().getKitNameProperty());
		columnItemCode.setCellValueFactory(cellData -> cellData.getValue().getItemcodeProperty());
		ColumnBarcode.setCellValueFactory(cellData -> cellData.getValue().getBarcodeProperty());
		ColumnUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitnameProperty());
		
	}
	 @Subscribe
 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
 		//Stage stage = (Stage) btnClear.getScene().getWindow();
 		//if (stage.isShowing()) {
 			taskid = taskWindowDataEvent.getId();
 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
 			
 		 
 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
 			System.out.println("Business Process ID = " + hdrId);
 			
 			 PageReload();
 		}


   private void PageReload() {
   	
 }

}
