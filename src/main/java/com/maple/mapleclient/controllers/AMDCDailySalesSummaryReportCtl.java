package com.maple.mapleclient.controllers;

import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AMDCDailySalesSummaryReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class AMDCDailySalesSummaryReportCtl {

	
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	
	AMDCDailySalesSummaryReport aMDCDailySalesSummaryReport=null;
	private ObservableList<AMDCDailySalesSummaryReport> aMDCDailySalesSummaryReportList = FXCollections.observableArrayList(); 	
	
	 @FXML
	    private DatePicker dpDate;

	    @FXML
	    private Button btnGenerateReport;

	    @FXML
	    private Button btnPrintReport;

	    @FXML
	    private TableView<AMDCDailySalesSummaryReport> tblDailySalesSummary;

	    @FXML
	    private TableColumn<AMDCDailySalesSummaryReport, String> clmnBranch;

	    @FXML
	    private TableColumn<AMDCDailySalesSummaryReport, Number> clmnTotalCard;

	    @FXML
	    private TableColumn<AMDCDailySalesSummaryReport, Number> clmnTotalCredit;

	    @FXML
	    private TableColumn<AMDCDailySalesSummaryReport, Number> clmnTotalCash;

	    @FXML
	    private TableColumn<AMDCDailySalesSummaryReport, Number> clmnTotalBank;

	    @FXML
	    private TableColumn<AMDCDailySalesSummaryReport, Number> clmnTotalInsurance;

	    @FXML
	    private TableColumn<AMDCDailySalesSummaryReport, Number> clmnGrantTotal;

	    
	    @FXML
	   	private void initialize() {
	    	eventBus.register(this);
	    	
	    	tblDailySalesSummary.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getId()) {

						aMDCDailySalesSummaryReport = new AMDCDailySalesSummaryReport();
						aMDCDailySalesSummaryReport.setId(newSelection.getId());
						
					}
				}
	    	});
	    	
	    	
	    }
	    
	    
	    @FXML
	    void generateReport(ActionEvent event) {
	    	if(null == dpDate.getValue())
	    	{
	    		notifyMessage(5, "Please select date", false);
				return;
	    	}
	    	
	    	
    		Date fdate = SystemSetting.localToUtilDate(dpDate.getValue());
    		String currentDate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
    		
    		ResponseEntity<List<AMDCDailySalesSummaryReport>> aMDCDailySalesSummary=RestCaller.getAMDCDailySalesSummaryReport(currentDate);
    		aMDCDailySalesSummaryReportList = FXCollections.observableArrayList(aMDCDailySalesSummary.getBody());
    		
    		
    		
    		if (aMDCDailySalesSummaryReportList.size() > 0) {
    			
    			
    			fillTable();
    			dpDate.getEditor().clear();;
    			
    		}

	    }

	    private void fillTable() {
	    	tblDailySalesSummary.setItems(aMDCDailySalesSummaryReportList);
			clmnBranch.setCellValueFactory(cellData -> cellData.getValue().getBranchProperty());
			clmnTotalCard.setCellValueFactory(cellData -> cellData.getValue().getTotalCardSalesProperty());
			clmnTotalCredit.setCellValueFactory(cellData -> cellData.getValue().getTotalCreditSalesProperty());
			clmnTotalCash.setCellValueFactory(cellData -> cellData.getValue().getTotalCashSalesProperty());
			clmnTotalBank.setCellValueFactory(cellData -> cellData.getValue().getTotalBankSalesProperty());
			clmnTotalInsurance.setCellValueFactory(cellData -> cellData.getValue().getTotalInsuranceSalesProperty());
			clmnGrantTotal.setCellValueFactory(cellData -> cellData.getValue().getGrandTotalProperty());
			
			
		}


		@FXML
	    void printReport(ActionEvent event) {
			Date fdate = SystemSetting.localToUtilDate(dpDate.getValue());
			String currentDate = SystemSetting.UtilDateToString(fdate, "yyyy-MM-dd");
		
			  try { 
				  JasperPdfReportService.AMDCDailySalesSummaryReport(currentDate); 
			  } catch (JRException e) { // TODO Auto-generated catch block
			  e.printStackTrace(); }
	    }
	    
	    private void notifyMessage(int i, String string, boolean b) {
			
	    	Image img;
			if (b) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(string).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(i)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

	    	
		}

}