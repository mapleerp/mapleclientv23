package com.maple.mapleclient.controllers;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.ibm.icu.math.BigDecimal;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.OtherBranchPurchaseDtl;
import com.maple.mapleclient.entity.OtherBranchPurchaseHdr;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

public class OtherBranchPurchaseReportCtl {
	String taskid;
	String processInstanceId;
	
	private ObservableList<OtherBranchPurchaseHdr> purchaseList = FXCollections.observableArrayList();
	private ObservableList<OtherBranchPurchaseDtl> purchaseDtlLists = FXCollections.observableArrayList();
	
    @FXML
    private DatePicker dpFromDate;

    @FXML
    private TableView<OtherBranchPurchaseHdr> tbReport;

    @FXML
    private TableColumn<OtherBranchPurchaseHdr, LocalDate> clVoucherNo;

    @FXML
    private TableColumn<OtherBranchPurchaseHdr, String> clSupplier;

    @FXML
    private TableColumn<OtherBranchPurchaseHdr, String> clamt;

    @FXML
    private TableColumn<OtherBranchPurchaseHdr, Number> Amnt;

    @FXML
    private TextField txtGrandTotal;

    @FXML
    private Button btnShow;

	@FXML
	private TableView<OtherBranchPurchaseDtl> tbreports;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, String> clBranch;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, String> clItemName;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clQty;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clRate;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clTaxRate;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clCessRate;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, String> clUnit;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clAmount;

    @FXML
    private ComboBox<String> branchselect;

    @FXML
    private Button ok;

    @FXML
    private Button btnPrint;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private Button btnExcelExport;
    
    @FXML
	private void initialize() {
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	dpFromDate = SystemSetting.datePickerFormat(dpFromDate, "dd/MMM/yyyy");
    	ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();

		for (BranchMst branchMst : branchMstList) {
			branchselect.getItems().add(branchMst.getBranchCode());

		}
		
		

		tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {
					
					
					ResponseEntity<OtherBranchPurchaseHdr> otherBranchPurchaseHdr = RestCaller.OtherBranchPurchaseHdrById(newSelection.getId());
					
					ResponseEntity<List<OtherBranchPurchaseDtl>> dailysaless = RestCaller.getOtherBranchPurchaseDtlByHdr(otherBranchPurchaseHdr.getBody());
					purchaseDtlLists = FXCollections.observableArrayList(dailysaless.getBody());

					FillDtlTables();

				}

				// FillTable();
			}
		});
    	
    }

    private void FillDtlTables() {

		for (OtherBranchPurchaseDtl purchase : purchaseDtlLists) {
			ResponseEntity<UnitMst> unit = RestCaller.getunitMst(purchase.getUnitId());
			purchase.setUnitName(unit.getBody().getUnitName());
		}
		try {
			tbreports.setItems(purchaseDtlLists);
			java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
			// clBranch.setCellValueFactory(cellData ->
			// cellData.getValue().getBarcodeProperty());
			clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
			clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
			clRate.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
			clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxRateProperty());
			clCessRate.setCellValueFactory(cellData -> cellData.getValue().getCessRateProperty());

			clUnit.setCellValueFactory(cellData -> cellData.getValue().getUnitProperty());
			clAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		} catch (Exception e) {
			// TODO: handle exception
		}
			
	}

	@FXML
    void BackOnKeyPress(KeyEvent event) {

    }

    @FXML
    void exportToExcel(ActionEvent event) {

    }

    @FXML
    void onClickOk(ActionEvent event) {

    }

    @FXML
    void reportPrint(ActionEvent event) {

    }

    @FXML
    void show(ActionEvent event) {
    	

		Double totalamount = 0.0;
		Double grandTotal = 0.0;

		if (null == branchselect.getValue()) {
			notifyMessage(5, "Please select branch", false);
			return;
		}

		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "Please select from date", false);
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(5, "Please select to date", false);
			return;
		}
		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		
		ResponseEntity<List<OtherBranchPurchaseHdr>> purchaseHdrRep = RestCaller.getAllOtherBranchPurchaseHdrByDate(startDate, endDate,
				branchselect.getSelectionModel().getSelectedItem().toString());
		
		purchaseList = FXCollections.observableArrayList(purchaseHdrRep.getBody());

		
		FillTable();

	

    }
    
	private void FillTable() {

		tbReport.setItems(purchaseList);
		
		Double grandTotal = 0.0;
		for (OtherBranchPurchaseHdr purchase : purchaseList) {
			grandTotal = grandTotal + purchase.getInvoiceTotal();
		}

		for (OtherBranchPurchaseHdr purchase : purchaseList) {
			if (null != purchase.getSupplierId()) {
				ResponseEntity<AccountHeads> account = RestCaller.getAccountHeadsById(purchase.getSupplierId());
				purchase.setSupplierName(account.getBody().getAccountName());
				clamt.setCellValueFactory(cellData -> cellData.getValue().getSupplierNameProperty());

			}
		}

		clSupplier.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		Amnt.setCellValueFactory(cellData -> cellData.getValue().getInvoiceTotalProperty());
		clVoucherNo.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		BigDecimal newrate = new BigDecimal(grandTotal);
		newrate = newrate.setScale(2, BigDecimal.ROUND_CEILING);

		txtGrandTotal.setText(newrate.toString());
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }


}
