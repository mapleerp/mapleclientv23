package com.maple.mapleclient.controllers;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.OtherBranchPurchaseDtl;
import com.maple.mapleclient.entity.OtherBranchPurchaseHdr;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseOrderDtl;
import com.maple.mapleclient.entity.PurchaseOrderHdr;
import com.maple.mapleclient.entity.Summary;
import com.maple.mapleclient.entity.SummarySalesDtl;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.SupplierPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class OtherBranchPurchaseCtl {
	
	String taskid;
	String processInstanceId;

	String supplierID = null;
	String supplierName = "";
	String itemId = null;

	EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<MultiUnitMst> multiUnitList = FXCollections.observableArrayList();

	private ObservableList<OtherBranchPurchaseDtl> otherBranchPurchaseDtlListTable = FXCollections
			.observableArrayList();

	private ObservableList<OtherBranchPurchaseDtl> otherBranchPurchaseDtlList = FXCollections.observableArrayList();

	OtherBranchPurchaseHdr otherBranchPurchaseHdr = null;

	OtherBranchPurchaseDtl otherBranchPurchaseDtl = null;

	StringProperty itemSerialProperty = new SimpleStringProperty("");

	StringProperty amountProperty = new SimpleStringProperty("");
	StringProperty rateProperty = new SimpleStringProperty("");
	StringProperty quantityProperty = new SimpleStringProperty("");
	StringProperty taxTateProperty = new SimpleStringProperty("");
	StringProperty cessRateProperty = new SimpleStringProperty("");
	StringProperty cessAmtProperty = new SimpleStringProperty("");
	StringProperty batchProperty = new SimpleStringProperty("");
	StringProperty taxAmountProperty = new SimpleStringProperty("");
	StringProperty totalAmountProperty = new SimpleStringProperty("");

	@FXML
	private TextField txtSupplierName;

	@FXML
	private TextField txtSupplierGst;

	@FXML
	private DatePicker dpSupplierInvDate;

	@FXML
	private DatePicker dpOurVoucherDate;

	@FXML
	private TextField txtSupplierInvNo;

	@FXML
	private TextField txtOurVoucherNo;

	@FXML
	private TextField txtInvoiceTotal;

	@FXML
	private TextField txtNarration;

	@FXML
	private TextField txtPONum;

	@FXML
	private Button btnLoadFromPO;

	@FXML
	private TextField txtQtyTotal;

	@FXML
	private TextField txtAmtTotal;

	@FXML
	private TextField txtDiscountTotal;

	@FXML
	private TextField txtTotal;

	@FXML
	private TextField txtTotalCess;

	@FXML
	private TextField txtTotalDisBeforeTax;

	@FXML
	private TextField txtGrandTotal;

	@FXML
	private TextField txtExpenseTotal;

	@FXML
	private TextArea txtTaxSplit;

	@FXML
	private Button btnHolPurchase;

	@FXML
	private Button btnCalculate;

	@FXML
	private Button btnSave;

	@FXML
	private TableView<OtherBranchPurchaseDtl> tblItemDetails;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, String> clItemName;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clQty;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clPurRate;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clTaxRate;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clamt;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clMRP;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clNetCost;

	@FXML
	private TableColumn<OtherBranchPurchaseDtl, Number> clCess;

	@FXML
	private Button btnAddItem;

	@FXML
	private Button btnDeleteItem;

	@FXML
	private DatePicker dpExpiryDate;

	@FXML
	private Label lblExpryDate;

	@FXML
	private DatePicker dpManufacture;

	@FXML
	private Label lblManufDate;

	@FXML
	private TextField txtBatch;

	@FXML
	private TextField txtMRP;

	@FXML
	private TextField txtTaxAmt;

	@FXML
	private TextField txtItemName;

	@FXML
	private TextField txtBarcode;

	@FXML
	private TextField txtQty;

	@FXML
	private TextField txtPurchseRate;

	@FXML
	private TextField txtAmount;

	@FXML
	private TextField txtItemSerial;

	@FXML
	private TextField txtCessRate;

	@FXML
	private TextField txtDiscount;

	@FXML
	private TextField txtTaxRate;

	@FXML
	private TextField txtCessAmt;

	@FXML
	private ComboBox<String> cmbUnit;

	@FXML
	private ComboBox<String> cmbBranchCode;

	@FXML
	private void initialize() {
		dpExpiryDate = SystemSetting.datePickerFormat(dpExpiryDate, "dd/MMM/yyyy");
		dpManufacture = SystemSetting.datePickerFormat(dpManufacture, "dd/MMM/yyyy");
		dpOurVoucherDate = SystemSetting.datePickerFormat(dpOurVoucherDate, "dd/MMM/yyyy");
		dpSupplierInvDate = SystemSetting.datePickerFormat(dpSupplierInvDate, "dd/MMM/yyyy");
		txtItemSerial.textProperty().bindBidirectional(itemSerialProperty);
		itemSerialProperty.set("1");

		eventBus.register(this);

		dpOurVoucherDate.setValue(SystemSetting.utilToLocaDate(SystemSetting.getApplicationDate()));
		dpOurVoucherDate.setEditable(false);
		txtSupplierName.requestFocus();

		// -----new purchasedlt----------
		otherBranchPurchaseDtl = new OtherBranchPurchaseDtl();

		/*
		 * Bind below three fields to Property so that calculation of Amout or Rate will
		 * happen as the user types in
		 */
		// dpOurVoucherDate.setValue(LocalDate.now());
		txtAmount.textProperty().bindBidirectional(amountProperty);
		txtQty.textProperty().bindBidirectional(quantityProperty);
		txtPurchseRate.textProperty().bindBidirectional(rateProperty);
		txtTaxAmt.textProperty().bindBidirectional(taxAmountProperty);
		txtTaxRate.textProperty().bindBidirectional(taxTateProperty);
		txtItemSerial.textProperty().bindBidirectional(itemSerialProperty);
		itemSerialProperty.set("1");
		txtCessRate.textProperty().bindBidirectional(cessRateProperty);
		txtCessAmt.textProperty().bindBidirectional(cessAmtProperty);
		txtBatch.textProperty().bindBidirectional(batchProperty);

		txtInvoiceTotal.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtInvoiceTotal.setText(oldValue);
				}
			}
		});
		txtQty.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtQty.setText(oldValue);
				}
			}
		});

		txtAmount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtAmount.setText(oldValue);
				}
			}
		});

		txtMRP.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtMRP.setText(oldValue);
				}
			}
		});

		txtPurchseRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtPurchseRate.setText(oldValue);
				}
			}
		});

		txtItemSerial.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtItemSerial.setText(oldValue);
				}
			}
		});

		txtTaxRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtTaxRate.setText(oldValue);
				}
			}
		});
		txtCessRate.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtCessRate.setText(oldValue);
				}
			}
		});
		txtDiscount.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
					txtDiscount.setText(oldValue);
				}
			}
		});

		amountProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0)) {

					double qty = Double.parseDouble(txtQty.getText());
					double amount = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal(amount / qty);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						rateProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		quantityProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;

				if ((txtPurchseRate.getText().length() > 0)) {
					double rate = Double.parseDouble(txtPurchseRate.getText());

					double qty = Double.parseDouble((String) newValue);
					if (rate > 0) {
						BigDecimal newAmount = new BigDecimal(qty * rate);
						newAmount = newAmount.setScale(3, BigDecimal.ROUND_CEILING);
						amountProperty.set(newAmount.toPlainString());

					}
				} else if ((txtAmount.getText().length() > 0)) {
					double amount = Double.parseDouble(txtAmount.getText());

					double qty = Double.parseDouble((String) newValue);
					if (amount > 0) {
						BigDecimal newRate = new BigDecimal(amount / qty);
						newRate = newRate.setScale(3, BigDecimal.ROUND_CEILING);
						rateProperty.set(newRate.toPlainString());

					}
				}

				if ((txtTaxRate.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					double tax = Double.parseDouble(txtTaxRate.getText());
					double rate = Double.parseDouble(txtPurchseRate.getText());
					double qty = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * tax) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						taxAmountProperty.set(newrate.toPlainString());

					}
				}

				if ((txtPurchseRate.getText().length() > 0) && txtCessRate.getText().length() > 0) {

					double rate = Double.parseDouble(txtPurchseRate.getText());
					double cess = Double.parseDouble(txtCessRate.getText());
					double qty = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * cess) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						cessAmtProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		rateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;

				if ((txtQty.getText().length() > 0)) {
					double qty = Double.parseDouble(txtQty.getText());

					double rate = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newAmount = new BigDecimal(qty * rate);
						newAmount = newAmount.setScale(3, BigDecimal.ROUND_CEILING);
						amountProperty.set(newAmount.toPlainString());

					}
				}

				if ((txtTaxRate.getText().length() > 0) && txtQty.getText().length() > 0) {

					double tax = Double.parseDouble(txtTaxRate.getText());
					double qty = Double.parseDouble(txtQty.getText());
					double rate = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * tax) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						taxAmountProperty.set(newrate.toPlainString());

					}
				}

				if ((txtQty.getText().length() > 0) && txtCessRate.getText().length() > 0) {

					double qty = Double.parseDouble(txtQty.getText());
					double cess = Double.parseDouble(txtCessRate.getText());
					double rate = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * cess) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						cessAmtProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		batchProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0) {
					dpExpiryDate.setDisable(true);
					dpManufacture.setDisable(true);
					lblExpryDate.setDisable(true);
					lblManufDate.setDisable(true);

				} else {
					dpExpiryDate.setDisable(false);
					dpManufacture.setDisable(false);
					lblExpryDate.setDisable(false);
					lblManufDate.setDisable(false);
				}

			}
		});

		// --------------tax amt
		taxTateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					double qty = Double.parseDouble(txtQty.getText());
					double rate = Double.parseDouble(txtPurchseRate.getText());
					double tax = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * tax) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						taxAmountProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		cessRateProperty.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (((String) newValue).length() == 0)
					return;
				if ((txtQty.getText().length() > 0) && txtPurchseRate.getText().length() > 0) {

					double qty = Double.parseDouble(txtQty.getText());
					double rate = Double.parseDouble(txtPurchseRate.getText());
					double cess = Double.parseDouble((String) newValue);
					if (qty > 0) {
						BigDecimal newrate = new BigDecimal((rate * qty * cess) / 100);
						newrate = newrate.setScale(3, BigDecimal.ROUND_CEILING);

						cessAmtProperty.set(newrate.toPlainString());

					}
				}
			}
		});

		getAllBranchCode();

	}

	private void getAllBranchCode() {

		ResponseEntity<List<BranchMst>> brachMstResp = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = brachMstResp.getBody();

		for (BranchMst branch : branchMstList) {
			cmbBranchCode.getItems().add(branch.getBranchName());
		}
	}

	@FXML
	void AddItem(ActionEvent event) {
		addItem();
	}

	@FXML
	void DeleteItem(ActionEvent event) {

		if (null != otherBranchPurchaseDtl) {
			if (null != otherBranchPurchaseDtl.getId()) {

				// version1.7
				ResponseEntity<OtherBranchPurchaseDtl> getPur = RestCaller
						.getOtherBranchPurchaseDtlById(otherBranchPurchaseDtl.getId());
				if (null != getPur.getBody().getPurchaseOrderDtl()) {
					if (!getPur.getBody().getPurchaseOrderDtl().equalsIgnoreCase(null))

					{
						PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
						ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller
								.getPurchaseOrderDtlById(getPur.getBody().getPurchaseOrderDtl());
						puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
						puchaseOrderDtl.setReceivedQty(0.0);
						puchaseOrderDtl.setStatus("OPEN");
						RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
						ResponseEntity<PurchaseOrderHdr> getPurOrdrHdr = RestCaller
								.getPurchaseOrderByDtlId(puchaseOrderDtl.getId());
						String vdate = SystemSetting.UtilDateToString(getPurOrdrHdr.getBody().getVoucherDate(),
								"yyyy-MM-dd");
						ResponseEntity<List<PurchaseOrderDtl>> getPurchaseDtl = RestCaller
								.getPurchaseOrderDtlByVoucherNoAndDate(getPurOrdrHdr.getBody().getVoucherNumber(),
										vdate);
//	        		if(getPurchaseDtl.getBody().size()==0)
//	        		{
						getPurOrdrHdr.getBody().setFinalSavedStatus("OPEN");
						RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());
						//
//	        		}
					}
				}
				/// version1.7end

				RestCaller.purchaseDtlDelete(otherBranchPurchaseDtl.getId());
				otherBranchPurchaseDtl = null;
				/////////////////////
				getOtherBranchPurchaseDtl();

				clearFields();

			}
		}

	}

	private void getOtherBranchPurchaseDtl() {
		// TODO Auto-generated method stub

	}

	@FXML
	void ExpiryDateOnkeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			btnAddItem.requestFocus();
		}
	}

	@FXML
	void Holdthispurchase(ActionEvent event) {
		try {
			ResponseEntity<List<OtherBranchPurchaseDtl>> purchasetdtlSaved = RestCaller
					.getOtherBranchPurchaseDtlByHdr(otherBranchPurchaseHdr);
			if (purchasetdtlSaved.getBody().size() == 0) {
				return;
			}

			otherBranchPurchaseHdr.setFinalSavedStatus("N");
			RestCaller.updateOtherBranchPurchaseHdr(otherBranchPurchaseHdr);
			otherBranchPurchaseHdr = null;
			otherBranchPurchaseDtl = null;
			otherBranchPurchaseDtlListTable.clear();
			itemSerialProperty.set("1");
			clearFieldshdr();
			tblItemDetails.getItems().clear();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void clearFieldshdr() {
		clearFields();
		supplierID = null;
		txtSupplierGst.setText("");
		txtSupplierInvNo.setText("");
		txtSupplierName.setText("");
		txtOurVoucherNo.setText("");
		dpSupplierInvDate.setValue(null);
		txtInvoiceTotal.setText("");
		txtPONum.setText("");
		txtNarration.setText("");
		txtQtyTotal.setText("");
		txtAmtTotal.setText("");
		txtDiscountTotal.setText("");
		txtTotalCess.setText("");
		txtTotal.setText("");
		txtGrandTotal.clear();
	}

	@FXML
	void InvcTotalOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtPONum.requestFocus();
		}
	}

	@FXML
	void ItemNameOnkeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtItemName.clear();
			txtBarcode.clear();
			txtQty.clear();
			txtPurchseRate.clear();
			txtAmount.clear();
			txtDiscount.clear();
			txtCessRate.clear();
			txtTaxAmt.clear();
			txtCessAmt.clear();
			txtTaxRate.clear();
			txtMRP.clear();
			txtBatch.clear();
			showPopup();
			txtQty.requestFocus();
			// txtBarcode.requestFocus();
		}
	}

	@FXML
	void ManfDAteOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			dpExpiryDate.requestFocus();
		}
	}

	@FXML
	void NarrationOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtSupplierInvNo.requestFocus();
		}
	}

	@FXML
	void OurVoucherNoOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtInvoiceTotal.requestFocus();
		}
	}

	@FXML
	void OurVouchrDateOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtNarration.requestFocus();
		}
	}

	@FXML
	void PoNoOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtItemName.requestFocus();
		}
	}

	@FXML
	void PurchaseRateOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtBatch.requestFocus();
		}
	}

	@FXML
	void QtyOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtPurchseRate.requestFocus();
		}
	}

	@FXML
	void Save(ActionEvent event) {
		finalSave();

	}

	@FXML
	void SaveOnKey(KeyEvent event) {

	}

	@FXML
	void ShowItemPopup(MouseEvent event) {

		showPopup();

	}

	@FXML
	void SupplierInvDateKeyPressed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			dpOurVoucherDate.requestFocus();
		}

	}

	@FXML
	void SupplierInvNoOnKeyPresssed(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			txtInvoiceTotal.requestFocus();
		}

	}

	@FXML
	void SupplierOnKeyPressed(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			txtSupplierGst.requestFocus();
		}

	}

	@FXML
	void actionLoadPo(ActionEvent event) {
		loadPOpopUp(supplierID);

	}

	@FXML
	void addItem(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
	}

	@FXML
	void batchActon(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			if (!txtBatch.getText().trim().isEmpty()) {
				dpManufacture.requestFocus();
			} else
				btnAddItem.requestFocus();
		}
	}

	@FXML
	void showPopup(MouseEvent event) {
		System.out.println("-------------showPopup-------------");
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/supplierPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addItem() {

		if (null == otherBranchPurchaseHdr) {

			if (null == supplierID)

			{
				notifyMessage(5, "Please select Supplier", false);
				otherBranchPurchaseHdr = null;
				return;
			}

			if (null == dpSupplierInvDate.getValue()) {

				notifyMessage(5, "Please select Suppier Invoice date", false);
				otherBranchPurchaseHdr = null;
				return;

			}
			if (null == dpOurVoucherDate.getValue()) {

				notifyMessage(5, "Please select voucher date", false);
				otherBranchPurchaseHdr = null;
				return;
			}
			if (txtSupplierInvNo.getText().trim().isEmpty()) {
				notifyMessage(5, "Please enter Suppier Invoice Number", false);
				otherBranchPurchaseHdr = null;
				return;
			}
			
			if (null == cmbBranchCode.getSelectionModel().getSelectedItem()) {
				notifyMessage(5, "Please select branch", false);
				otherBranchPurchaseHdr = null;
				return;
			}

//				
//				else {
			otherBranchPurchaseHdr = new OtherBranchPurchaseHdr();
//					otherBranchPurchaseHdr = new otherBranchPurchaseHdr();
			// supplierID was stored from subscription
			otherBranchPurchaseHdr.setSupplierId(supplierID);
			System.out.println("ddddd" + supplierID);

//					if (!(txtPaymentDue.getText().trim().isEmpty())) {
////						otherBranchPurchaseHdr.set(txtPaymentDue.getText());
//					}
			otherBranchPurchaseHdr.setFinalSavedStatus("N");
			// otherBranchPurchaseHdr.setPurchaseType(cmbPurchaseType.getSelectionModel().getSelectedItem());
			Date dateOurVouch = Date.valueOf(dpOurVoucherDate.getValue());
			otherBranchPurchaseHdr.setpONum(txtPONum.getText());
			otherBranchPurchaseHdr.setNarration(txtNarration.getText());
			otherBranchPurchaseHdr.setourVoucherDate(dateOurVouch);
			otherBranchPurchaseHdr.setSupplierInvNo(txtSupplierInvNo.getText());
			if (!txtInvoiceTotal.getText().trim().isEmpty()) {
				otherBranchPurchaseHdr.setInvoiceTotal(Double.parseDouble(txtInvoiceTotal.getText()));
			}
			otherBranchPurchaseHdr.setInvoiceDate(Date.valueOf(dpSupplierInvDate.getValue()));
			
			ResponseEntity<BranchMst> branchMstResp = RestCaller.getBranchMstByName(cmbBranchCode.getSelectionModel().getSelectedItem().toString());
			BranchMst branchMst = branchMstResp.getBody();
			otherBranchPurchaseHdr.setBranchCode(branchMst.getBranchCode());

			otherBranchPurchaseHdr.setVoucherType("PURCHASETYPE");

			ResponseEntity<OtherBranchPurchaseHdr> respentity = RestCaller
					.saveOtherBranchPurchaseHdr(otherBranchPurchaseHdr);
			otherBranchPurchaseHdr = respentity.getBody();

//				}

		}

		/*
		 * Set otherBranchPurchaseHdr to otherBranchPurchaseDtl.
		 */
		if (null != otherBranchPurchaseDtl) {
			if (null != otherBranchPurchaseDtl.getId()) {

				// version1.7
				ResponseEntity<OtherBranchPurchaseDtl> getPur = RestCaller
						.getOtherBranchPurchaseDtlById(otherBranchPurchaseDtl.getId());
				if (null != getPur.getBody().getPurchaseOrderDtl()) {
					if (!getPur.getBody().getPurchaseOrderDtl().equalsIgnoreCase(null))

					{
						PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
						ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller
								.getPurchaseOrderDtlById(getPur.getBody().getPurchaseOrderDtl());
						puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
						puchaseOrderDtl.setReceivedQty(
								Double.parseDouble(txtQty.getText()) + puchaseOrderDtl.getReceivedQty());
						if (puchaseOrderDtl.getQty() > puchaseOrderDtl.getReceivedQty()) {
							puchaseOrderDtl.setStatus("OPEN");
						} else {
							puchaseOrderDtl.setStatus("CLOSED");
						}
						RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
						ResponseEntity<PurchaseOrderHdr> getPurOrdrHdr = RestCaller
								.getPurchaseOrderByDtlId(puchaseOrderDtl.getId());
						String vdate = SystemSetting.UtilDateToString(getPurOrdrHdr.getBody().getVoucherDate(),
								"yyyy-MM-dd");
						ResponseEntity<List<PurchaseOrderDtl>> getotherBranchPurchaseDtl = RestCaller
								.getPurchaseOrderDtlByVoucherNoAndDate(getPurOrdrHdr.getBody().getVoucherNumber(),
										vdate);
//	        		if(getotherBranchPurchaseDtl.getBody().size()==0)
//	        		{
						getPurOrdrHdr.getBody().setFinalSavedStatus("OPEN");
						RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());
						//
//	        		}
					}
				}
				// version1.7 ends
				RestCaller.OtherBranchPurchaseDtlDelete(otherBranchPurchaseDtl.getId());
				// txtItemSerial.setText(Integer.parseInt(txtItemSerial.getText())-1+"");
				otherBranchPurchaseDtl = null;
				getOtherBranchPurchaseDtls();
			}
		}
		if (null != otherBranchPurchaseHdr) {
			otherBranchPurchaseDtl = new OtherBranchPurchaseDtl();

			if (txtItemName.getText().trim().isEmpty()) {
				notifyMessage(5, "Please select Item...!!!", false);
				return;
			}
			String ItemName = txtItemName.getText();
			ResponseEntity<ItemMst> resp = RestCaller.getItemByNameRequestParam(ItemName);
			ItemMst item = resp.getBody();

			if (null == item) {
				notifyMessage(5, "Please select Item...!!!", false);
				return;

			}
			System.out.println("-------------AddItem-------------");
			boolean valDtl = validationDtl();
			if (valDtl == false) {
				return;
			}

			boolean expiry = true;

			if (null != txtBatch) {
				String batch = txtBatch.getText() == null ? "NOBATCH" : txtBatch.getText();

				if (batch.trim().length() > 0) {
					otherBranchPurchaseDtl.setBatch(batch);
					expiry = false;
					otherBranchPurchaseDtl.setBatch(txtBatch.getText());
					if (null != dpExpiryDate.getValue()) {
						otherBranchPurchaseDtl.setexpiryDate(Date.valueOf(dpExpiryDate.getValue()));
						expiry = true;

					} else {
						notifyMessage(5, "Please select Expirydate...!!!", false);
						return;
					}

					if (null != dpManufacture.getValue()) {
						Date dateMfg = Date.valueOf(dpManufacture.getValue());

						otherBranchPurchaseDtl.setmanufactureDate(dateMfg);
					}
				} else {
					otherBranchPurchaseDtl.setBatch("NOBATCH");
					otherBranchPurchaseDtl.setexpiryDate(null);
				}
			}

			if (null != otherBranchPurchaseHdr.getId()) {
				if (valDtl == true) {

					if (expiry == true) {
						otherBranchPurchaseDtl.setOtherBranchPurchaseHdr(otherBranchPurchaseHdr);

						System.out.println("getItemIdgetItemId" + otherBranchPurchaseDtl.getItemId());

						otherBranchPurchaseDtl.setItemName(item.getItemName());
						otherBranchPurchaseDtl.setBarcode(item.getBarCode());
						otherBranchPurchaseDtl.setTaxRate(item.getTaxRate());
						otherBranchPurchaseDtl.setDiscount(0.0);
						otherBranchPurchaseDtl.setItemId(item.getId());

//				if (null != otherBranchPurchaseDtl.getItemId()) {

						if (!txtTaxAmt.getText().trim().isEmpty()) {

							otherBranchPurchaseDtl.setTaxAmt(Double.parseDouble(txtTaxAmt.getText()));

						}
						if (!txtPurchseRate.getText().trim().isEmpty()) {

							otherBranchPurchaseDtl.setNetCost(Double.parseDouble(txtPurchseRate.getText()));

						}
						if (!txtDiscount.getText().trim().isEmpty()) {
							otherBranchPurchaseDtl.setDiscount(Double.parseDouble(txtDiscount.getText()));
						}
						otherBranchPurchaseDtl.setCessAmt(
								txtCessAmt.getText().length() > 0 ? Double.parseDouble(txtCessAmt.getText()) : 0);
						otherBranchPurchaseDtl.setCessRate(Double.parseDouble(txtCessRate.getText()));
						otherBranchPurchaseDtl.setMrp(Double.parseDouble(txtMRP.getText()));
						otherBranchPurchaseDtl.setQty(Double.parseDouble(txtQty.getText()));
						otherBranchPurchaseDtl.setTaxRate(Double.parseDouble(txtTaxRate.getText()));
						otherBranchPurchaseDtl.setPurchseRate(Double.parseDouble(txtPurchseRate.getText()));
						otherBranchPurchaseDtl.setAmount(Double.parseDouble(txtAmount.getText()));
						// otherBranchPurchaseDtl.setUnit(unit);
						if (null == cmbUnit.getValue()) {

							ResponseEntity<UnitMst> unitId = RestCaller
									.getUnitByName(cmbUnit.getSelectionModel().getSelectedItem().toString());

							otherBranchPurchaseDtl.setUnitId(unitId.getBody().getId());
						} else {
							ResponseEntity<UnitMst> getUnit = RestCaller.getUnitByName(cmbUnit.getValue().toString());
							otherBranchPurchaseDtl.setUnitId(getUnit.getBody().getId());
						}
						ResponseEntity<OtherBranchPurchaseDtl> respentity = RestCaller
								.saveOtherBranchPurchaseDtl(otherBranchPurchaseDtl);
						otherBranchPurchaseDtl = respentity.getBody();
						setTotal();
						otherBranchPurchaseDtlListTable.add(otherBranchPurchaseDtl);
						itemSerialProperty.set(Integer.parseInt(itemSerialProperty.get()) + 1 + "");
						FillTable();
						clearFields();
//				} else {
//					notifyMessage(5, "Please select Item...!!!",false);
//					return;
//
//				}
					}
				}
			} else {
//			otherBranchPurchaseHdr = null;

				return;

			}
		} else {
			return;
		}

		otherBranchPurchaseDtl = new OtherBranchPurchaseDtl();

		txtItemName.requestFocus();

	}

	private void clearFields() {

		txtAmount.setText("");
		txtCessAmt.setText("");
		txtItemName.setText("");
		txtQty.setText("");
		txtBarcode.setText("");
		txtBatch.setText("");
		txtCessRate.setText("");
		txtDiscount.setText("");
		// txtFreeQty.setText("");
		txtPurchseRate.setText("");
		// txtItemSerial.setText("");
		txtTaxRate.setText("");
		// txtPreviousMRP.setText("");
		txtCessAmt.setText("");
		txtMRP.setText("");
		cmbUnit.getSelectionModel().clearSelection();
		txtTaxAmt.setText("");
		dpExpiryDate.setValue(null);
		dpManufacture.setValue(null);

	}

	private void FillTable() {

		tblItemDetails.setItems(otherBranchPurchaseDtlListTable);

		clItemName.setCellValueFactory(cellData -> cellData.getValue().getItemNameProperty());
		clamt.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
		clMRP.setCellValueFactory(cellData -> cellData.getValue().getMrpProperty());
		clPurRate.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
		clQty.setCellValueFactory(cellData -> cellData.getValue().getQtyProperty());
		clTaxRate.setCellValueFactory(cellData -> cellData.getValue().getTaxAmtProperty());
		clCess.setCellValueFactory(cellData -> cellData.getValue().getCessAmtProperty());
		clNetCost.setCellValueFactory(cellData -> cellData.getValue().getPurchseRateProperty());
	}

	private void setTotal() {

		SummarySalesDtl sumList = new SummarySalesDtl();
		sumList = RestCaller.getOtherBranchPurchaseHdrSummary(otherBranchPurchaseHdr.getId());

		try {
			txtGrandTotal.setText(Double.toString(sumList.getTotalAmount() + sumList.getTotalTax()));
			txtQtyTotal.setText(Double.toString(sumList.getTotalQty()));
			txtDiscountTotal.setText(Double.toString(sumList.getTotalDiscount()));
			txtAmtTotal.setText(Double.toString(sumList.getTotalAmount()));
			txtTotal.setText(Double.toString(sumList.getTotalTax()));
			txtTotalCess.setText(Double.toString(sumList.getTotalCessAmt()));
		} catch (Exception e) {
			// TODO: handle exception
		}
//txtTaxSplit.setText(sumList);

	}

	private void getOtherBranchPurchaseDtls() {
		// TODO Auto-generated method stub

	}

	@Subscribe
	public void popuplistner(SupplierPopupEvent supplierEvent) {

		System.out.println("-------------popuplistner-------------");
		Stage stage = (Stage) btnAddItem.getScene().getWindow();
		if (stage.isShowing()) {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					txtSupplierGst.setText(supplierEvent.getSupplierGST());
					txtSupplierName.setText(supplierEvent.getSupplierName());
					supplierName = supplierEvent.getSupplierName();
					// purchasehdr.setSupplierId(supplierEvent.getSupplierId());
					supplierID = supplierEvent.getSupplierId();
					/*
					 * for some reason id is null
					 */
					if (null == supplierID) {
						ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsByName(supplierEvent.getSupplierName());
						supplierID = accountHeads.getBody().getId();
					}
				}
			});

		}

	}

	private void showPopup() {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ItemPopup.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadPOpopUp(String supplierID) {
		System.out.println("-------------ShowItemPopup-------------");

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PONumberPopUp.fxml"));
			// loader.setController(itemPopupCtl);
			Parent root = loader.load();
			POPopupCtl popupctl = loader.getController();
			popupctl.LoadPoNumberBySearch(supplierID);

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			// stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			txtQty.requestFocus();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean validationDtl() {

		boolean flag = false;
		if (txtCessRate.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter Cess Rate", false);
			return false;

		} else if (txtMRP.getText().trim().isEmpty()) {

			notifyMessage(5, "Please enter MRP", false);
			return false;
		} else if (txtQty.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter Quantity", false);
			return false;
		} else if (txtTaxRate.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter TaxRate", false);
			return false;

		} else if (txtPurchseRate.getText().trim().isEmpty()) {
			notifyMessage(5, "Please enter purchase Rate", false);
			return false;

		} else {

			flag = true;

		}

		return flag;
	}

	@Subscribe
	public void popupItemlistner(ItemPopupEvent itemPopupEvent) {

		System.out.println("-------------popupItemlistner-------------");
		Stage stage = (Stage) btnAddItem.getScene().getWindow();
		if (stage.isShowing()) {
			cmbUnit.getItems().clear();

			itemId = itemPopupEvent.getItemId();
			txtItemName.setText(itemPopupEvent.getItemName());
			txtBarcode.setText(itemPopupEvent.getBarCode());

			txtDiscount.setText("0.0");

			txtTaxRate.setText(Double.toString(itemPopupEvent.getTaxRate()));
			txtMRP.setText(Double.toString(itemPopupEvent.getMrp()));
			txtCessRate.setText(Double.toString(itemPopupEvent.getCess()));
			cmbUnit.getItems().add(itemPopupEvent.getUnitName());
			cmbUnit.setValue(itemPopupEvent.getUnitName());
			ResponseEntity<List<MultiUnitMst>> multiUnit = RestCaller.getMultiUnitByItemId(itemPopupEvent.getItemId());

			multiUnitList = FXCollections.observableArrayList(multiUnit.getBody());

			if (!multiUnitList.isEmpty()) {

				for (MultiUnitMst multiUniMst : multiUnitList) {

					ResponseEntity<UnitMst> getUnit = RestCaller.getunitMst(multiUniMst.getUnit1());
					cmbUnit.getItems().add(getUnit.getBody().getUnitName());
				}

			} else {
//				cmbUnit.getItems().clear();
//				cmbUnit.getItems().add(itemPopupEvent.getUnitName());
//				cmbUnit.setPromptText(itemPopupEvent.getUnitName());
				cmbUnit.setValue(itemPopupEvent.getUnitName());
			}

			ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
					.getPriceDefenitionMstByName("COST PRICE");
			PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
			if (null != priceDefenitionMst) {

				java.util.Date udate = SystemSetting.localToUtilDate(dpOurVoucherDate.getValue());
				String sdate = SystemSetting.UtilDateToString(udate, "yyyy-MM-dd");

				ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller.getPriceDefenitionByCostPrice(
						itemPopupEvent.getItemId(), priceDefenitionMst.getId(), itemPopupEvent.getUnitId(), sdate);

				PriceDefinition priceDefinition = priceDefenitionResp.getBody();

				if (null != priceDefinition) {
					txtPurchseRate.setText(Double.toString(priceDefinition.getAmount()));
				}

			}
		}
	}

	private void finalSave() {
		try {
			// version1.7

			if (null == dpSupplierInvDate.getValue()) {
				notifyMessage(3, "Select Supplier Invoice Date", false);
				dpSupplierInvDate.requestFocus();
				return;
			}
			if (txtSupplierInvNo.getText().trim().isEmpty()) {
				notifyMessage(3, "Type Supplier Invoice No ", false);
				txtSupplierInvNo.getInputMethodRequests();
				return;
			}

			// version1.7ends

			Double invoiceAmount = 0.0;
			ResponseEntity<List<OtherBranchPurchaseDtl>> otherBranchPurchaseDtlSaved = RestCaller
					.getOtherBranchPurchaseDtlByHdr(otherBranchPurchaseHdr);
			if (otherBranchPurchaseDtlSaved.getBody().size() == 0) {
				return;
			}

//			updateCostPrice(otherBranchPurchaseHdr);

			// version1.7
			otherBranchPurchaseHdr.setSupplierInvNo(txtSupplierInvNo.getText());
			otherBranchPurchaseHdr.setInvoiceDate(Date.valueOf(dpSupplierInvDate.getValue()));
			// version1.7ends

			otherBranchPurchaseHdr.setSupplierId(supplierID);

			ResponseEntity<AccountHeads> accountHeads = RestCaller.getAccountHeadsById(otherBranchPurchaseHdr.getSupplierId());
			String vouchernumber = otherBranchPurchaseHdr.getVoucherNumber();
			otherBranchPurchaseHdr.setFinalSavedStatus("Y");
			if (null == vouchernumber) {
				String financialYear = SystemSetting.getFinancialYear();
				String pNo = RestCaller.getVoucherNumber(financialYear + "PVN");

				pNo =SystemSetting.systemBranch + pNo;
				otherBranchPurchaseHdr.setVoucherNumber(pNo);

				java.util.Date date = SystemSetting.localToUtilDate(dpOurVoucherDate.getValue());
				java.sql.Date sqlDate = new java.sql.Date(date.getTime());

				otherBranchPurchaseHdr.setourVoucherDate(sqlDate);
			}
			otherBranchPurchaseHdr.setInvoiceTotal(Double.parseDouble(txtGrandTotal.getText()));
			java.util.Date uDate = otherBranchPurchaseHdr.getourVoucherDate();
			String vDate = SystemSetting.UtilDateToString(uDate, "yyyy-MM-dd");

			RestCaller.updateOtherBranchPurchaseHdr(otherBranchPurchaseHdr);
			boolean status = false;
//			if (null != otherBranchPurchaseHdr) {
//
//				ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtl = RestCaller
//						.getOtherBranchPurchaseDtlByHdr(otherBranchPurchaseHdr);
//				ResponseEntity<List<PurchaseDtl>> getPurchaseDtl = RestCaller.getPurchaseDtl(otherBranchPurchaseHdr);
//				for (PurchaseOrderDtl purOdr : getPurchaseOrderDtl.getBody()) {
//					for (PurchaseDtl purDtl : getPurchaseDtl.getBody()) {
//						if (purOdr.getItemId().equalsIgnoreCase(purDtl.getItemId())) {
////							purOdr.setReceivedQty(purDtl.getQty());
//							purOdr.setReceivedQty(purOdr.getReceivedQty() + purDtl.getQty());
//							if (purOdr.getQty() - purOdr.getReceivedQty() <= 0) {
//
//								purOdr.setStatus("CLOSED");
//							} else {
//
//								purOdr.setStatus("OPEN");
//							}
//							RestCaller.updatePurchaseOrderDtl(purOdr);
//						}
//					}
//				}
//				ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtlAftrUpdate = RestCaller
//						.getPurchaseOrderDtl(otherBranchPurchaseHdr);
//				for (PurchaseOrderDtl purOdr : getPurchaseOrderDtlAftrUpdate.getBody()) {
//					if (purOdr.getStatus().equalsIgnoreCase("OPEN")) {
//						status = true;
//						break;
//					}
//				}
//				if (!status) {
//					otherBranchPurchaseHdr.setFinalSavedStatus("CLOSED");
//					RestCaller.updatePurchaseOrderHdr(otherBranchPurchaseHdr);
//
//				}
//			}

			// version1.7

//			for(PurchaseDtl purDtl:otherBranchPurchaseDtlList)
//			{
//				if(null != purDtl.getPurchaseOrderDtl())
//				{
//					if(!purDtl.getPurchaseOrderDtl().equalsIgnoreCase(null))
//					{
//						PurchaseOrderDtl puchaseOrderDtl = new PurchaseOrderDtl();
//						ResponseEntity<PurchaseOrderDtl> getPurchaseOrdrDtl = RestCaller.getPurchaseOrderDtlById(purDtl.getPurchaseOrderDtl());
//		        		 puchaseOrderDtl = getPurchaseOrdrDtl.getBody();
//		        		// puchaseOrderDtl.setReceivedQty(purDtl.getQty());
//		        		puchaseOrderDtl.setStatus("OPEN");
//		        		RestCaller.updatePurchaseOrderDtl(puchaseOrderDtl);
//		        		ResponseEntity<PurchaseOrderHdr> getPurOrdrHdr = RestCaller.getPurchaseOrderByDtlId(puchaseOrderDtl.getId());
//		        		ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtlAftrUpdate = RestCaller
//								.getPurchaseOrderDtl(getPurOrdrHdr.getBody());
//						for (PurchaseOrderDtl purOdr : getPurchaseOrderDtlAftrUpdate.getBody()) {
//							if (purOdr.getStatus().equalsIgnoreCase("OPEN")) {
//								status = true;
//								break;
//							}
//						}
//						if (!status) {
//							otherBranchPurchaseHdr.setFinalSavedStatus("CLOSED");
//							RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());
//
//						}
//						else
//						{
//							otherBranchPurchaseHdr.setFinalSavedStatus("OPEN");
//							RestCaller.updatePurchaseOrderHdr(getPurOrdrHdr.getBody());
//
//						}
//					}
//				}
//				
//			}

			/// version1.7ends

			status = false;
			DayBook dayBook = new DayBook();
			dayBook.setBranchCode(otherBranchPurchaseHdr.getBranchCode());
			dayBook.setDrAccountName("PURCHASE ACCOUNT");
			dayBook.setDrAmount(otherBranchPurchaseHdr.getInvoiceTotal());
			dayBook.setNarration(txtSupplierName.getText() + otherBranchPurchaseHdr.getVoucherNumber());
			dayBook.setSourceVoucheNumber(otherBranchPurchaseHdr.getVoucherNumber());
			dayBook.setSourceVoucherType("PURCHASE");
			dayBook.setCrAccountName(txtSupplierName.getText());
			dayBook.setCrAmount(otherBranchPurchaseHdr.getInvoiceTotal());
			dayBook.setsourceVoucherDate(otherBranchPurchaseHdr.getourVoucherDate());
			ResponseEntity<DayBook> saveDaybook = RestCaller.savedayBook(dayBook);
			notifyMessage(5, "saved", true);

			// version1.6
			String vNo = otherBranchPurchaseHdr.getVoucherNumber();
			otherBranchPurchaseHdr = null;
			otherBranchPurchaseDtl = null;
//			purchaseListTable.clear();
			itemSerialProperty.set("1");
			tblItemDetails.getItems().clear();
			// version1.6 ends

			clearFieldshdr();
			

//			JasperPdfReportService.PurchaseInvoiceReport(vNo, vDate);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
	  @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }

}
