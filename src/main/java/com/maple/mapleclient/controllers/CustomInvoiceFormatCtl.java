package com.maple.mapleclient.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.entity.CustomInvoicePropertyFormat;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.MenuConfigMst;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class CustomInvoiceFormatCtl {
	String taskid;
	String processInstanceId;
	private ObservableList<CustomInvoicePropertyFormat> customInvoiceList = FXCollections.observableArrayList();

	CustomInvoicePropertyFormat customInvoicePropertyFormat = null;
    @FXML
    private TableView<CustomInvoicePropertyFormat> tblCustomSaleMode;

    @FXML
    private TableColumn<CustomInvoicePropertyFormat, String> clCustomSalesMode;

    @FXML
    private TableColumn<CustomInvoicePropertyFormat,String> clInvoiceFormatProperty;

    @FXML
    private TableColumn<CustomInvoicePropertyFormat,String> clValue;

    @FXML
    private ComboBox<String> cmbCustomSalesMode;

    @FXML
    private ComboBox<String> cmbInvoiceFormatProperty;

    @FXML
    private TextField txtValue;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnSave;
    @FXML
	private void initialize() {
		ResponseEntity<List<MenuConfigMst>> menuConfigMstListResp = RestCaller.getAllMenuConfigMst();
		for(int i=0;i<menuConfigMstListResp.getBody().size();i++)
		{
			if(null != menuConfigMstListResp.getBody().get(i).getCustomiseSalesMode())
			{
				if(!menuConfigMstListResp.getBody().get(i).getCustomiseSalesMode().equalsIgnoreCase(null)) {
					cmbCustomSalesMode.getItems().add(menuConfigMstListResp.getBody().get(i).getCustomiseSalesMode());
				}
			}
		}
		cmbInvoiceFormatProperty.getItems().add("print.logo_name");
		cmbInvoiceFormatProperty.getItems().add("print.posformat");
		cmbInvoiceFormatProperty.getItems().add("print.hascashdrawer");
		cmbInvoiceFormatProperty.getItems().add("logox");
		cmbInvoiceFormatProperty.getItems().add("logoy");
		cmbInvoiceFormatProperty.getItems().add("logow");
		cmbInvoiceFormatProperty.getItems().add("logoh");
		cmbInvoiceFormatProperty.getItems().add("posinvoicetitle1");
		cmbInvoiceFormatProperty.getItems().add("posinvoicetitle2");
		cmbInvoiceFormatProperty.getItems().add("posinvoicetitle3");
		cmbInvoiceFormatProperty.getItems().add("posinvoicetitle4");
		cmbInvoiceFormatProperty.getItems().add("posinvoicetitle5");
		cmbInvoiceFormatProperty.getItems().add("title1x");
		cmbInvoiceFormatProperty.getItems().add("title1y");
		cmbInvoiceFormatProperty.getItems().add("title2x");
		cmbInvoiceFormatProperty.getItems().add("title2y");
		cmbInvoiceFormatProperty.getItems().add("title3x");
		cmbInvoiceFormatProperty.getItems().add("title3y");
		cmbInvoiceFormatProperty.getItems().add("title4x");
		cmbInvoiceFormatProperty.getItems().add("title4y");
		cmbInvoiceFormatProperty.getItems().add("title5x");
		cmbInvoiceFormatProperty.getItems().add("title5y");
		
		tblCustomSaleMode.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getId()) {

					customInvoicePropertyFormat = new CustomInvoicePropertyFormat();
					customInvoicePropertyFormat.setId(newSelection.getId());
				}
			}
		});

    }
    @FXML
    void actionDelete(ActionEvent event) {

    	if(null == customInvoicePropertyFormat)
    	{
    		return;
    	}
    	if(null == customInvoicePropertyFormat.getId())
    	{
    		return;
    	}
    	RestCaller.delteCustomInvoiceFormat(customInvoicePropertyFormat.getId());
    	ResponseEntity<List<CustomInvoicePropertyFormat>> custPropertyList = RestCaller.getCustomInvoiceFormatByCustomSalesMode(cmbCustomSalesMode.getSelectionModel().getSelectedItem());
    	customInvoiceList = FXCollections.observableArrayList(custPropertyList.getBody());
    	fillTable();
    }

    @FXML
    void actionSave(ActionEvent event) {
    	customInvoicePropertyFormat = new CustomInvoicePropertyFormat();
    	customInvoicePropertyFormat.setCustomSalesMode(cmbCustomSalesMode.getSelectionModel().getSelectedItem());
    	customInvoicePropertyFormat.setPropertyName(cmbInvoiceFormatProperty.getSelectionModel().getSelectedItem());
    	customInvoicePropertyFormat.setPropertyValue(txtValue.getText());
    	ResponseEntity<CustomInvoicePropertyFormat> respentity = RestCaller.saveCustomInvoicePropertyFormat(customInvoicePropertyFormat);
    	customInvoicePropertyFormat = respentity.getBody();
    	customInvoiceList.add(customInvoicePropertyFormat);
    	fillTable();
    	cmbInvoiceFormatProperty.getSelectionModel().clearSelection();
    	txtValue.clear();
    }
    private void fillTable()
    {
    	tblCustomSaleMode.setItems(customInvoiceList);
		clCustomSalesMode.setCellValueFactory(cellData -> cellData.getValue().getCustomSalesModeProperty());
		clInvoiceFormatProperty.setCellValueFactory(cellData -> cellData.getValue().getPropertyNameProperty());
		clValue.setCellValueFactory(cellData -> cellData.getValue().getPropertyValueProperty());

    }

    @FXML
    void actionShowAll(ActionEvent event) {

    	ResponseEntity<List<CustomInvoicePropertyFormat>> custPropertyList = RestCaller.getCustomInvoiceFormatByCustomSalesMode(cmbCustomSalesMode.getSelectionModel().getSelectedItem());
    	customInvoiceList = FXCollections.observableArrayList(custPropertyList.getBody());
    	fillTable();
    }
    @Subscribe
   	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
   		//Stage stage = (Stage) btnClear.getScene().getWindow();
   		//if (stage.isShowing()) {
   			taskid = taskWindowDataEvent.getId();
   			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
   			
   		 
   			String hdrId = taskWindowDataEvent.getBusinessProcessId();
   			System.out.println("Business Process ID = " + hdrId);
   			
   			 PageReload();
   		}


     private void PageReload() {
     	
   }

}
