package com.maple.mapleclient.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MachineResourceMst;
import com.maple.mapleclient.entity.MixCategoryMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.events.KitPopupEvent;
import com.maple.mapleclient.events.TaskWindowDataEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
public class MixCategoryCtl {
	
	String taskid;
	String processInstanceId;
	private ObservableList<MixCategoryMst> resourceCatList = FXCollections.observableArrayList();
	
	  
		EventBus eventBus = EventBusFactory.getEventBus();
		
		
		String itemId;
 
    @FXML
    private TextField txtSubCatName;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnEdit;

    @FXML
    private Button btnShowAll;

    @FXML
    private ComboBox<String> cmbMechine;

    @FXML
    private TableView<MixCategoryMst> tblSubCategory;

    @FXML
    private TableColumn<MixCategoryMst, String> clSubCatName;



    @FXML
    private TableColumn<MixCategoryMst, String> clMachine;

    @FXML
    void SaveOnEnter(KeyEvent event) {

    }

    @FXML
    void btnEditAction(ActionEvent event) {

    }


    @FXML
    void itemNameOnEnter(KeyEvent event) {

    	if (event.getCode() == KeyCode.ENTER) {
    		showPopups();
    	}
    }

    @FXML
    void machineOnEnter(KeyEvent event) {

    }

  
    @FXML
    void subCatNameOnEnter(KeyEvent event) {

    }

    @FXML
    void tableToText(MouseEvent event) {

    }
	
    
    private void showPopups() {
  		try {
  			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/KitPopup.fxml"));
  			// fxmlLoader.setController(itemStockPopupCtl);
  			Parent root1;

  			root1 = (Parent) fxmlLoader.load();
  			Stage stage = new Stage();

  			stage.initModality(Modality.APPLICATION_MODAL);
  			stage.initStyle(StageStyle.UNDECORATED);
  			stage.setTitle("Stock Item");
  			stage.initModality(Modality.APPLICATION_MODAL);
  			stage.setScene(new Scene(root1));
  			stage.show();

  		} catch (IOException e) {

  			e.printStackTrace();
  		}

  	}
    
    
    
	@FXML
	private void initialize() {
		eventBus.register(this);
//	UnitMst unitMst = new UnitMst();
	// set unit
	
	ArrayList machine = new ArrayList();

	machine = RestCaller.SearchMachine();
	Iterator itr = machine.iterator();
	while (itr.hasNext()) {
		LinkedHashMap lm = (LinkedHashMap) itr.next();
		Object machineName = lm.get("machineResourceName");
		Object unitId = lm.get("id");
		if (unitId != null) {
			cmbMechine.getItems().add((String) machineName);
			
		}
	}
    
	
    
	}
	

    @FXML
    void btnSaveAction(ActionEvent event) {


		  if(txtSubCatName.getText().trim().isEmpty()) {
		  
		  notifyMessage(5,"Please fill Resource  subcategory Name!!!");
		  txtSubCatName.requestFocus();
		  
		   
		  
		  } else if(cmbMechine.getValue().trim().isEmpty()) {
		  
		  notifyMessage(5,"Please fill mechine !!!");
		  
		  cmbMechine.requestFocus();
		  
		  }
    	
        ResponseEntity<MachineResourceMst> resp=RestCaller.findByMachineResourecName(cmbMechine.getValue()); 
        MachineResourceMst machineResourceMst=resp.getBody();
        System.out.println(machineResourceMst.getId()+"machineresourcemst");
		
        
        
		  MixCategoryMst SubCategory =new MixCategoryMst();
		  SubCategory.setMixName(txtSubCatName.getText());
	
		  SubCategory.setMechineId(machineResourceMst.getId());
		  ResponseEntity<MixCategoryMst> respentity =
		  RestCaller.saveResourceCategory(SubCategory);
		  clearFields();

		    ResponseEntity<List<MixCategoryMst>> responseEntity=RestCaller.resourceCategory();
		    resourceCatList = FXCollections.observableArrayList(responseEntity.getBody());
		    tblSubCategory.setItems(resourceCatList);
		    
		    System.out.print(resourceCatList.size()+"observable list size isssssssssssss"); 
		    filltable();
    }
	
	public void clearFields() {
	
		txtSubCatName.setText("");
		cmbMechine.getSelectionModel().clearSelection();
	}
	
	  
    private void filltable()
	{
    	
    	clSubCatName.setCellValueFactory(cellData -> cellData.getValue().getSubCateNameProperty());
    	
    	clMachine.setCellValueFactory(cellData -> cellData.getValue().getMechineProperty());
		
						
	}
    @FXML
    void showAll(ActionEvent event) {
    	
	    ResponseEntity<List<MixCategoryMst>> responseEntity=RestCaller.resourceCategory();
	    resourceCatList = FXCollections.observableArrayList(responseEntity.getBody());
	    tblSubCategory.setItems(resourceCatList);
	    
	    System.out.print(resourceCatList.size()+"observable list size isssssssssssss"); 
	    filltable();
    }

	public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");

					Image img = new Image("done.png");
					Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
							.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT).onAction(new EventHandler<ActionEvent>() {
								@Override
								public void handle(ActionEvent event) {
									System.out.println("clicked on notification");
								}
							});
					notificationBuilder.darkStyle();
					notificationBuilder.show();
				}
	 @Subscribe
	 	public void TaskWindowEvent(TaskWindowDataEvent taskWindowDataEvent) {
	 		//Stage stage = (Stage) btnClear.getScene().getWindow();
	 		//if (stage.isShowing()) {
	 			taskid = taskWindowDataEvent.getId();
	 			processInstanceId = taskWindowDataEvent.getProcessInstanceId();
	 			
	 		 
	 			String hdrId = taskWindowDataEvent.getBusinessProcessId();
	 			System.out.println("Business Process ID = " + hdrId);
	 			
	 			 PageReload();
	 		}


	   private void PageReload() {
	   	
	 }
}
