package com.maple.mapleclient;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class ScheduleConfig {

	

	@Scheduled(fixedDelay = 6000)
	public void PerformanceTestAndGc() 
	{
//		System.out.println("Memory Usage Checking");
		
		long kb=1024000;
		
		Runtime runtime=Runtime.getRuntime();
		
		
	  
		long totalMemory =runtime.totalMemory()/kb;
		
		long freeMemory=runtime.freeMemory()/kb;
		
     
		long memoryUsage=totalMemory-freeMemory;
//        System.out.println("Total Memory="+totalMemory);
//        System.out.println("Free Memory="+freeMemory);
//        System.out.println("Memory Usage="+memoryUsage);
        
		double usedSpace=Double.longBitsToDouble(memoryUsage);
		double totalSpace=Double.longBitsToDouble(totalMemory);
		
		double consumption =usedSpace/totalSpace*100;
		
//		System.out.println("Memory Usage in Percentage="+consumption+"%");
		
		if (consumption>80)
		{
	     Runtime.getRuntime().gc();
	     
//	     System.out.println("Garbages Collected..!");
		}
	}
}
