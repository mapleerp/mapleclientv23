package com.maple.mapleclient.events;

public class AccountHeadsPopupEvent {
	
	String partyName;
    String id;
	String partyAddress;
	String partyContact;
	String partyMail;
	String partyGst;
	String partyId;
    String currencyId;
	private Integer creditPeriod;
	public String getPartyName() {
		return partyName;
	}
	public String getId() {
		return id;
	}
	public String getPartyAddress() {
		return partyAddress;
	}
	public String getPartyContact() {
		return partyContact;
	}
	public String getPartyMail() {
		return partyMail;
	}
	public String getPartyGst() {
		return partyGst;
	}
	public String getPartyId() {
		return partyId;
	}
	public String getCurrencyId() {
		return currencyId;
	}
	public Integer getCreditPeriod() {
		return creditPeriod;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setPartyAddress(String partyAddress) {
		this.partyAddress = partyAddress;
	}
	public void setPartyContact(String partyContact) {
		this.partyContact = partyContact;
	}
	public void setPartyMail(String partyMail) {
		this.partyMail = partyMail;
	}
	public void setPartyGst(String partyGst) {
		this.partyGst = partyGst;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}
	public void setCreditPeriod(Integer creditPeriod) {
		this.creditPeriod = creditPeriod;
	}
	@Override
	public String toString() {
		return "AccountHeadsPopupEvent [partyName=" + partyName + ", id=" + id + ", partyAddress=" + partyAddress
				+ ", partyContact=" + partyContact + ", partyMail=" + partyMail + ", partyGst=" + partyGst
				+ ", partyId=" + partyId + ", currencyId=" + currencyId + ", creditPeriod=" + creditPeriod + "]";
	}


}
