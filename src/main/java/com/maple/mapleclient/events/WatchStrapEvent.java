package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class WatchStrapEvent {
	String strapId;
	String strapName;
	public String getStrapId() {
		return strapId;
	}
	public void setStrapId(String strapId) {
		this.strapId = strapId;
	}
	public String getStrapName() {
		return strapName;
	}
	public void setStrapName(String strapName) {
		this.strapName = strapName;
	}
	@Override
	public String toString() {
		return "WatchStrapEvent [strapId=" + strapId + ", strapName=" + strapName + "]";
	}

	
	

}
