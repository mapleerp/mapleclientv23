package com.maple.mapleclient.events;

import java.sql.Date;

public class ProductBatchStockEvent {
	
	String itemName;
	String itemId;
	String batch;
	Date expiryDate;
	Double qty;
	Double allocatedQty;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAllocatedQty() {
		return allocatedQty;
	}
	public void setAllocatedQty(Double allocatedQty) {
		this.allocatedQty = allocatedQty;
	}
	@Override
	public String toString() {
		return "ProductBatchStockEvent [itemName=" + itemName + ", itemId=" + itemId + ", batch=" + batch
				+ ", expiryDate=" + expiryDate + ", qty=" + qty + ", allocatedQty=" + allocatedQty + "]";
	}

}
