package com.maple.mapleclient.events;

import java.sql.Date;

public class ItemPopupEvent {


	String itemPriceLock;
	String itemName;
	String itemCode;
	String barCode;
	Double taxRate;
	Double itemDiscount;
	String itemId;
	String unitId;
	Double mrp;
	Double qty;
	Double cess;
	String unitName;
	String batch;
	Double addCessRate;
	Double cgstTaxRate;
	String itemTaxaxId;
	Double sgstTaxRate;
	Date expiryDate;
	String barCodeLine1;
	String barCodeLine2;
	Integer bestBefore;
	
	String storeName;
	
	
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getItemDiscount() {
		return itemDiscount;
	}
	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getCess() {
		return cess;
	}
	public void setCess(Double cess) {
		this.cess = cess;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getAddCessRate() {
		return addCessRate;
	}
	public void setAddCessRate(Double addCessRate) {
		this.addCessRate = addCessRate;
	}
	public Double getCgstTaxRate() {
		return cgstTaxRate;
	}
	public void setCgstTaxRate(Double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}
	public String getItemTaxaxId() {
		return itemTaxaxId;
	}
	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}
	public Double getSgstTaxRate() {
		return sgstTaxRate;
	}
	public void setSgstTaxRate(Double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getBarCodeLine1() {
		return barCodeLine1;
	}
	public void setBarCodeLine1(String barCodeLine1) {
		this.barCodeLine1 = barCodeLine1;
	}
	public String getBarCodeLine2() {
		return barCodeLine2;
	}
	public void setBarCodeLine2(String barCodeLine2) {
		this.barCodeLine2 = barCodeLine2;
	}
	public Integer getBestBefore() {
		return bestBefore;
	}
	public void setBestBefore(Integer bestBefore) {
		this.bestBefore = bestBefore;
	}
	
	public String getItemPriceLock() {
		return itemPriceLock;
	}
	public void setItemPriceLock(String itemPriceLock) {
		this.itemPriceLock = itemPriceLock;
	}
	@Override
	public String toString() {
		return "ItemPopupEvent [itemPriceLock=" + itemPriceLock + ", itemName=" + itemName + ", itemCode=" + itemCode
				+ ", barCode=" + barCode + ", taxRate=" + taxRate + ", itemDiscount=" + itemDiscount + ", itemId="
				+ itemId + ", unitId=" + unitId + ", mrp=" + mrp + ", qty=" + qty + ", cess=" + cess + ", unitName="
				+ unitName + ", batch=" + batch + ", addCessRate=" + addCessRate + ", cgstTaxRate=" + cgstTaxRate
				+ ", itemTaxaxId=" + itemTaxaxId + ", sgstTaxRate=" + sgstTaxRate + ", expiryDate=" + expiryDate
				+ ", barCodeLine1=" + barCodeLine1 + ", barCodeLine2=" + barCodeLine2 + ", bestBefore=" + bestBefore
				+ ", storeName=" + storeName + "]";
	}
	
	
	
	
	

	
}
