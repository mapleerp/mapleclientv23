package com.maple.mapleclient.events;

public class WeighingMachineDataEvent {
	
	String weightData;

	@Override
	public String toString() {
		return "WeighingMachineDataEvent [weightData=" + weightData + "]";
	}

	public String getWeightData() {
		return weightData;
	}

	public void setWeightData(String weightData) {
		this.weightData = weightData;
	}
	 
	 
	

}
