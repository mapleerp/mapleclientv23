package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class WatchCompliantEvent {
	String complaintId;
	String complaint;
	public String getComplaintId() {
		return complaintId;
	}
	public void setComplaintId(String complaintId) {
		this.complaintId = complaintId;
	}
	public String getComplaint() {
		return complaint;
	}
	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}
	
	

}
