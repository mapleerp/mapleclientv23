package com.maple.mapleclient.events;

public class AcountPopUpAssetEvent {
	String accountId;
	String accountName;
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	@Override
	public String toString() {
		return "AcountPopUpAssetEvent [accountId=" + accountId + ", accountName=" + accountName + "]";
	}
	
}
