package com.maple.mapleclient.events;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.mapleclient.entity.GoodReceiveNoteDtl;
import com.maple.mapleclient.entity.PurchaseDtl;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class ItemPropertyInstance implements Serializable {
	private static final long serialVersionUID = 1L;
	
    String id;
	
 
	private String branchCode;
	
	
	
	private String itemId;
	private String propertyValue;
	private String propertyName;
	private String batch ;
	PurchaseDtl purhcaseDtl;
	GoodReceiveNoteDtl goodReceiveNoteDtl;
	@JsonIgnore
	private StringProperty propertyNameProperty;
	
	@JsonIgnore
	private StringProperty propertyValueProperty;
	
	
	public ItemPropertyInstance() {

		this.propertyNameProperty = new SimpleStringProperty();
		this.propertyValueProperty = new SimpleStringProperty();
	}



	public StringProperty getPropertyNameProperty() {
		propertyNameProperty.set(propertyName);
		return propertyNameProperty;
	}
	public void setPropertyNameProperty(String propertyName) {
		this.propertyName = propertyName;
	}



	public StringProperty getPropertyValueProperty() {
		propertyValueProperty.set(propertyValue);
		return propertyValueProperty;
	}



	public void setPropertyValueProperty(String propertyValue) {
		this.propertyValue = propertyValue;
	}



	
	



	public GoodReceiveNoteDtl getGoodReceiveNoteDtl() {
		return goodReceiveNoteDtl;
	}



	public void setGoodReceiveNoteDtl(GoodReceiveNoteDtl goodReceiveNoteDtl) {
		this.goodReceiveNoteDtl = goodReceiveNoteDtl;
	}



	public PurchaseDtl getPurhcaseDtl() {
		return purhcaseDtl;
	}



	public void setPurhcaseDtl(PurchaseDtl purhcaseDtl) {
		this.purhcaseDtl = purhcaseDtl;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}



	public String getItemId() {
		return itemId;
	}



	public void setItemId(String itemId) {
		this.itemId = itemId;
	}



	public String getPropertyValue() {
		return propertyValue;
	}



	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}



	public String getPropertyName() {
		return propertyName;
	}



	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}



	public String getBatch() {
		return batch;
	}



	public void setBatch(String batch) {
		this.batch = batch;
	}



	@Override
	public String toString() {
		return "ItemPropertyInstance [id=" + id + ", branchCode=" + branchCode + ", itemId=" + itemId
				+ ", propertyValue=" + propertyValue + ", propertyName=" + propertyName + ", batch=" + batch
				+ ", purhcaseDtl=" + purhcaseDtl + ", goodReceiveNoteDtl=" + goodReceiveNoteDtl
				+ ", propertyNameProperty=" + propertyNameProperty + ", propertyValueProperty=" + propertyValueProperty
				+ "]";
	}



	
	
	 
	
	
	
	

}
