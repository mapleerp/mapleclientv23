package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class FamilyEvent {
	
	
	String familyMstId;
	String familyName;
	String headOfFamily;
	String orgId;
	

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getFamilyMstId() {
		return familyMstId;
	}

	public void setFamilyMstId(String familyMstId) {
		this.familyMstId = familyMstId;
	}

	
	
	public String getHeadOfFamily() {
		return headOfFamily;
	}

	public void setHeadOfFamily(String headOfFamily) {
		this.headOfFamily = headOfFamily;
	}

	
	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	@Override
	public String toString() {
		return "FamilyEvent [familyMstId=" + familyMstId + ", familyName=" + familyName + "]";
	}

	



}
