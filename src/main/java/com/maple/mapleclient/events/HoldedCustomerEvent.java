package com.maple.mapleclient.events;

public class HoldedCustomerEvent {
	
	String HdrId;
	String customerId;
	String customerName;
	String address;
	String custGst;
	String custPriceTYpe;
	
	
	public String getCustPriceTYpe() {
		return custPriceTYpe;
	}
	public void setCustPriceTYpe(String custPriceTYpe) {
		this.custPriceTYpe = custPriceTYpe;
	}
	public String getCustGst() {
		return custGst;
	}
	public void setCustGst(String custGst) {
		this.custGst = custGst;
	}
	public String getHdrId() {
		return HdrId;
	}
	public void setHdrId(String hdrId) {
		HdrId = hdrId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "HoldedCustomerEvent [HdrId=" + HdrId + ", customerId=" + customerId + ", customerName=" + customerName
				+ ", address=" + address + "]";
	}
		

}
