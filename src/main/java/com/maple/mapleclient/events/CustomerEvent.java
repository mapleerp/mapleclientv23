package com.maple.mapleclient.events;

public class CustomerEvent {
	String CustomerName;
	String Address;
	String cutomerName;
	String customerAddress;
	String customerContact;
	String customerMail;
	String customerGst;
	String custId;
	private Integer creditPeriod;

	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getCutomerName() {
		return cutomerName;
	}
	public void setCutomerName(String cutomerName) {
		this.cutomerName = cutomerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerContact() {
		return customerContact;
	}
	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}
	public String getCustomerMail() {
		return customerMail;
	}
	public void setCustomerMail(String customerMail) {
		this.customerMail = customerMail;
	}
	public String getCustomerGst() {
		return customerGst;
	}
	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}
	
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	
	
	public Integer getCreditPeriod() {
		return creditPeriod;
	}
	public void setCreditPeriod(Integer creditPeriod) {
		this.creditPeriod = creditPeriod;
	}
	@Override
	public String toString() {
		return "CustomerEvent [CustomerName=" + CustomerName + ", Address=" + Address + ", cutomerName=" + cutomerName
				+ ", customerAddress=" + customerAddress + ", customerContact=" + customerContact + ", customerMail="
				+ customerMail + ", customerGst=" + customerGst + ", custId=" + custId + ", creditPeriod="
				+ creditPeriod + "]";
	}
	
	
	
	
	

}
