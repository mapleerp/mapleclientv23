package com.maple.mapleclient.events;

public class ReceiptModeEvent {
	String receiptModeId;
	String receiptMode;
	
	
	public ReceiptModeEvent() {
	}


	public String getReceiptModeId() {
		return receiptModeId;
	}


	public void setReceiptModeId(String receiptModeId) {
		this.receiptModeId = receiptModeId;
	}


	public String getReceiptMode() {
		return receiptMode;
	}


	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}


	@Override
	public String toString() {
		return "ReceiptModeEvent [receiptModeId=" + receiptModeId + ", receiptMode=" + receiptMode + "]";
	}


	
	
}
