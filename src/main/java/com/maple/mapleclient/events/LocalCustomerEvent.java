package com.maple.mapleclient.events;

import java.util.Date;

public class LocalCustomerEvent {
	
	String localCustid;
	String localCustName;
	String localCustAddress;
	String localCustPhoneNo;
	String localCustPhone2;
	
	Date dateOfBirth;
	Date weddingDate;

	
	public String getLocalCustid() {
		return localCustid;
	}
	public void setLocalCustid(String localCustid) {
		this.localCustid = localCustid;
	}
	public String getLocalCustName() {
		return localCustName;
	}
	public void setLocalCustName(String localCustName) {
		this.localCustName = localCustName;
	}
	public String getLocalCustAddress() {
		return localCustAddress;
	}
	public void setLocalCustAddress(String localCustAddress) {
		this.localCustAddress = localCustAddress;
	}
	public String getLocalCustPhoneNo() {
		return localCustPhoneNo;
	}
	public void setLocalCustPhoneNo(String localCustPhoneNo) {
		this.localCustPhoneNo = localCustPhoneNo;
	}
	public String getLocalCustPhone2() {
		return localCustPhone2;
	}
	public void setLocalCustPhone2(String localCustPhone2) {
		this.localCustPhone2 = localCustPhone2;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Date getWeddingDate() {
		return weddingDate;
	}
	public void setWeddingDate(Date weddingDate) {
		this.weddingDate = weddingDate;
	}

}
