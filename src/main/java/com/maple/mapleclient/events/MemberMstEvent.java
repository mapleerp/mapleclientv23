package com.maple.mapleclient.events;

public class MemberMstEvent {

	
	String memberId;
	String memberName;
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	@Override
	public String toString() {
		return "MemberMstEvent [memberId=" + memberId + ", memberName=" + memberName + "]";
	}
	
	
	
}
