package com.maple.mapleclient.events;

public class ProcessPermissionEvent {

	String id;
	String processName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getProcessName() {
		return processName;
	}
	public void setProcessName(String processName) {
		this.processName = processName;
	}
	@Override
	public String toString() {
		return "ProcessPermissionEvent [id=" + id + ", processName=" + processName + "]";
	}
	
}
