 
package com.maple.mapleclient.events;

import java.sql.Date;

public class LoginPopupEvent {

	String user;
	String password;
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "LoginPopupEvent [user=" + user + ", password=" + password + "]";
	}
	 
	 
	
	
	 
	
}
