package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class ProductEvent {
	String productId;
	String productName;
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	@Override
	public String toString() {
		return "ProductEvent [productId=" + productId + ", productName=" + productName + "]";
	}
	
	

}
