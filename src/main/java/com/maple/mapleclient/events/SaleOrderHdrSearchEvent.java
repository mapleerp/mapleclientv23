package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class SaleOrderHdrSearchEvent {
	
	String id;
	String voucherNo;
	String customerId;
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getVoucherNo() {
		return voucherNo;
	}


	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}


	public String getCustomerId() {
		return customerId;
	}


	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}


	@Override
	public String toString() {
		return "SaleOrderHdrSearchEvent [id=" + id + ", voucherNo=" + voucherNo + ", customerId=" + customerId + "]";
	}



	

}
