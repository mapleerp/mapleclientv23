package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class SupplierPopupEvent {

	String SupplierName ;
	String SupplierAddress;
	String SupplierPhone;
	String SupplierGST;
	String SupplierId;
	String currencyId;
	
	
	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getSupplierId() {
		return SupplierId;
	}

	public void setSupplierId(String supplierId) {
		SupplierId = supplierId;
	}

	
	
	
	public SupplierPopupEvent(){
		
	}
	
	public String getSupplierGST() {
		return SupplierGST;
	}

	public void setSupplierGST(String supplierGST) {
		SupplierGST = supplierGST;
	}

	public String getSupplierName() {
		return SupplierName;
	}
	public void setSupplierName(String supplierName) {
		SupplierName = supplierName;
	}
	public String getSupplierAddress() {
		return SupplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		SupplierAddress = supplierAddress;
	}
	public String getSupplierPhone() {
		return SupplierPhone;
	}
	public void setSupplierPhone(String supplierPhone) {
		SupplierPhone = supplierPhone;
	}
	
}
