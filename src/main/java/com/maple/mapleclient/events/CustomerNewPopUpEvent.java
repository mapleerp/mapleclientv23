package com.maple.mapleclient.events;

public class CustomerNewPopUpEvent {
	
	
	
	String customerId;
	String customerName;
	String customerAddress;
	String customerGst;
	private Integer creditPeriod;
	
	
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerGst() {
		return customerGst;
	}
	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}
	public Integer getCreditPeriod() {
		return creditPeriod;
	}
	public void setCreditPeriod(Integer creditPeriod) {
		this.creditPeriod = creditPeriod;
	}
	@Override
	public String toString() {
		return "CustomerNewPopUpEvent [customerId=" + customerId + ", customerName=" + customerName
				+ ", customerAddress=" + customerAddress + ", customerGst=" + customerGst + ", creditPeriod="
				+ creditPeriod + "]";
	}
	
	
	
	

	
	
}
