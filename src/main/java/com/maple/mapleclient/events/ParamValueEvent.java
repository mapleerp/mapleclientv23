package com.maple.mapleclient.events;

public class ParamValueEvent {
	String paramId;
	String paramName;
	
	
	public ParamValueEvent() {
	}
	
	public String getParamId() {
		return paramId;
	}
	public void setParamId(String paramId) {
		this.paramId = paramId;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	@Override
	public String toString() {
		return "ParamValueEvent [paramId=" + paramId + ", paramName=" + paramName + "]";
	}
	
}
