package com.maple.mapleclient.events;

import java.sql.Date;

import com.maple.mapleclient.entity.PurchaseHdr;

public class PaymentInvPopupEvent {
	
	String invoiceNumber;
	Double dueAmount;
	Double paidAmount;
	Double balanceAmount;
	Date invoiceDate;
	PurchaseHdr purchaseHdr;
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public PurchaseHdr getPurchaseHdr() {
		return purchaseHdr;
	}
	public void setPurchaseHdr(PurchaseHdr purchaseHdr) {
		this.purchaseHdr = purchaseHdr;
	}
	
	
	
	

}
