package com.maple.mapleclient.events;

public class ServiceItemEvent {
	
	String serviceItemName;
	String id;
	Double mrp;
	String unitId;
	String category;
	public String getServiceItemName() {
		return serviceItemName;
	}
	public void setServiceItemName(String serviceItemName) {
		this.serviceItemName = serviceItemName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "ServiceItemEvent [serviceItemName=" + serviceItemName + ", id=" + id + ", mrp=" + mrp + ", unitId="
				+ unitId + ", category=" + category + "]";
	}
	
	
	

}
