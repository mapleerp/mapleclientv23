package com.maple.mapleclient.events;

public class LocationEvent {
	
	String locationName;
	String locationId;
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	@Override
	public String toString() {
		return "LocationEvent [locationName=" + locationName + ", locationId=" + locationId + "]";
	}
	
	
	

}
