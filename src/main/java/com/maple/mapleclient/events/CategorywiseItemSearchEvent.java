package com.maple.mapleclient.events;

import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.SalesTransHdr;

public class CategorywiseItemSearchEvent {
	SalesOrderTransHdr salesOrderTransHdrId;
	SalesTransHdr salesTransHdrId;

	public SalesTransHdr getSalesTransHdrId() {
		return salesTransHdrId;
	}

	public void setSalesTransHdrId(SalesTransHdr salesTransHdrId) {
		this.salesTransHdrId = salesTransHdrId;
	}

	public SalesOrderTransHdr getSalesOrderTransHdrId() {
		return salesOrderTransHdrId;
	}

	public void setSalesOrderTransHdrId(SalesOrderTransHdr salesOrderTransHdrId) {
		this.salesOrderTransHdrId = salesOrderTransHdrId;
	}


	
}
