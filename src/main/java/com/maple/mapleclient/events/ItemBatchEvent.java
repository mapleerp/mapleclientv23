package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class ItemBatchEvent {
	
	String batchCode;
	String itemName;
	String itemId;
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	@Override
	public String toString() {
		return "ItemBatchEvent [batchCode=" + batchCode + ", itemName=" + itemName + ", itemId=" + itemId + "]";
	}
	
	

}
