package com.maple.mapleclient.events;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TaskWindowDataEvent {

	String id;
	String name;
	String assignee;
	Date created;
	Date due;
	String followUp;
	String delegationState;
	String description;
	String executionId;
	String owner;
	String parentTaskId;
	String priority;
	String processDefinitionId;
	String processInstanceId;
	String voucherNumber;
	String voucherDate;
	String entityId;
	String accountId;
	String itemId;
	String businessProcessId;
	Map<String, Object> businessVariables = new HashMap<String, Object>();
	
	String taskId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getDue() {
		return due;
	}
	public void setDue(Date due) {
		this.due = due;
	}
	public String getFollowUp() {
		return followUp;
	}
	public void setFollowUp(String followUp) {
		this.followUp = followUp;
	}
	public String getDelegationState() {
		return delegationState;
	}
	public void setDelegationState(String delegationState) {
		this.delegationState = delegationState;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getExecutionId() {
		return executionId;
	}
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getParentTaskId() {
		return parentTaskId;
	}
	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getEntityId() {
		return entityId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}


	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getBusinessProcessId() {
		return businessProcessId;
	}
	public void setBusinessProcessId(String businessProcessId) {
		this.businessProcessId = businessProcessId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	@Override
	public String toString() {
		return "TaskWindowDataEvent [id=" + id + ", name=" + name + ", assignee=" + assignee + ", created=" + created
				+ ", due=" + due + ", followUp=" + followUp + ", delegationState=" + delegationState + ", description="
				+ description + ", executionId=" + executionId + ", owner=" + owner + ", parentTaskId=" + parentTaskId
				+ ", priority=" + priority + ", processDefinitionId=" + processDefinitionId + ", processInstanceId="
				+ processInstanceId + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", entityId=" + entityId + ", accountId=" + accountId + ", itemId=" + itemId + ", businessProcessId="
				+ businessProcessId + ", taskId=" + taskId + "]";
	}
	public Map<String, Object> getBusinessVariables() {
		return businessVariables;
	}
	public void setBusinessVariables(Map<String, Object> businessVariables) {
		this.businessVariables = businessVariables;
	}
	
	
}
