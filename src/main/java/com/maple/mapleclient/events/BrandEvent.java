package com.maple.mapleclient.events;

public class BrandEvent {
	String id;
	String brandName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	@Override
	public String toString() {
		return "BrandEvent [id=" + id + ", brandName=" + brandName + "]";
	}
	

}
