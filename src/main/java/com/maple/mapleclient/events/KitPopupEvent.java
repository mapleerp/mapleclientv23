package com.maple.mapleclient.events;

import java.sql.Date;

public class KitPopupEvent {
	String itemName;
	String itemCode;
	String barCode;
	double taxRate;
	double itemDiscount;
	String itemId;
	String unitId;
 

	double mrp;
	Integer qty;
	double cess;
	String unitName;
	String batch;
	double addCessRate;
	double cgstTaxRate;
	String itemTaxaxId;
	double sgstTaxRate;
	Date expiryDate;
	
	
	

	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public double getCess() {
		return cess;
	}
	public void setCess(double cess) {
		this.cess = cess;
	}
 
 
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	
	public double getItemDiscount() {
		return itemDiscount;
	}

	public void setItemDiscount(double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}


	public double getMrp() {
		return mrp;
	}
	public void setMrp(double mrp) {
		this.mrp = mrp;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}


	
	public double getAddCessRate() {
		return addCessRate;
	}
	public void setAddCessRate(double addCessRate) {
		this.addCessRate = addCessRate;
	}
	public double getCgstTaxRate() {
		return cgstTaxRate;
	}
	public void setCgstTaxRate(double cgstTaxRate) {
		this.cgstTaxRate = cgstTaxRate;
	}
	public String getItemTaxaxId() {
		return itemTaxaxId;
	}
	public void setItemTaxaxId(String itemTaxaxId) {
		this.itemTaxaxId = itemTaxaxId;
	}
	public double getSgstTaxRate() {
		return sgstTaxRate;
	}
	public void setSgstTaxRate(double sgstTaxRate) {
		this.sgstTaxRate = sgstTaxRate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	@Override
	public String toString() {
		return "ItemPopupEvent [itemName=" + itemName + ", itemCode=" + itemCode + ", barCode=" + barCode + ", taxRate="
				+ taxRate + ", itemDiscount=" + itemDiscount + ", itemId=" + itemId + ", unitId=" + unitId + ", mrp="
				+ mrp + ", qty=" + qty + ", cess=" + cess + ", unitName=" + unitName + ", batch=" + batch
				+ ", addCessRate=" + addCessRate + ", cgstTaxRate=" + cgstTaxRate + ", itemTaxaxId=" + itemTaxaxId
				+ ", sgstTaxRate=" + sgstTaxRate + ", expiryDate=" + expiryDate + "]";
	}
	
}
