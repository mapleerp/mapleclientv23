package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class WatchObservationEvent {
	String observationId;
	String observation;
	public String getObservationId() {
		return observationId;
	}
	public void setObservationId(String observationId) {
		this.observationId = observationId;
	}
	public String getObservation() {
		return observation;
	}
	public void setObservation(String observation) {
		this.observation = observation;
	}
	

}
