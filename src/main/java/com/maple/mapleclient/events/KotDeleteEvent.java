package com.maple.mapleclient.events;

import java.util.ArrayList;

import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;

public class KotDeleteEvent {

	String tables;
	String waiter;
	ArrayList<SalesDtl> arrayofsalesdtl;
	SalesTransHdr salesTransHdr;
	
	String actionKey;
	public String getTables() {
		return tables;
	}
	public void setTables(String tables) {
		this.tables = tables;
	}
	public String getWaiter() {
		return waiter;
	}
	public void setWaiter(String waiter) {
		this.waiter = waiter;
	}
	public ArrayList<SalesDtl> getArrayofsalesdtl() {
		return arrayofsalesdtl;
	}
	public void setArrayofsalesdtl(ArrayList<SalesDtl> arrayofsalesdtl) {
		this.arrayofsalesdtl = arrayofsalesdtl;
	}
	public SalesTransHdr getSalesTransHdr() {
		return salesTransHdr;
	}
	public void setSalesTransHdr(SalesTransHdr salesTransHdr) {
		this.salesTransHdr = salesTransHdr;
	}
	
	public String getActionKey() {
		return actionKey;
	}
	public void setActionKey(String actionKey) {
		this.actionKey = actionKey;
	}
	@Override
	public String toString() {
		return "KotScreenRefresh [tables=" + tables + ", waiter=" + waiter + ", arrayofsalesdtl=" + arrayofsalesdtl
				+ ", salesTransHdr=" + salesTransHdr + ", actionKey=" + actionKey + "]";
	}
	
	
}
