package com.maple.mapleclient.events;

import java.util.Date;

import org.springframework.stereotype.Component;


public class ReceiptInvoicePopupEvent {

	String id;
	String invoiceNumber;
	Double dueAmount;
	Double paidAmount;
	Double balanceAmount;
	Date invoiceDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Double getDueAmount() {
		return dueAmount;
	}
	public void setDueAmount(Double dueAmount) {
		this.dueAmount = dueAmount;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	@Override
	public String toString() {
		return "ReceiptInvoicePopupEvent [invoiceNumber=" + invoiceNumber + ", dueAmount=" + dueAmount + ", paidAmount="
				+ paidAmount + ", balanceAmount=" + balanceAmount + "]";
	}
	
}
