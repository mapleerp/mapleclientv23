package com.maple.mapleclient.events;

import java.time.LocalDate;
import java.util.Date;

public class TableOccupiedEvent {

	String tableId;
	Date occupiedTime;
	Date orderedTime;
	Date billingTime;
	Date deliveryTime;
	Date kitchenReadyTime;
	Double invoiceAmount;
	String waiterId;
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public Date getOccupiedTime() {
		return occupiedTime;
	}
	

	public Date getOrderedTime() {
		return orderedTime;
	}
	public void setOrderedTime(Date orderedTime) {
		this.orderedTime = orderedTime;
	}
	public void setOccupiedTime(Date occupiedTime) {
		this.occupiedTime = occupiedTime;
	}
	public Date getBillingTime() {
		return billingTime;
	}
	public void setBillingTime(Date billingTime) {
		this.billingTime = billingTime;
	}
	public Date getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Date deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public Date getKitchenReadyTime() {
		return kitchenReadyTime;
	}
	public void setKitchenReadyTime(Date kitchenReadyTime) {
		this.kitchenReadyTime = kitchenReadyTime;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getWaiterId() {
		return waiterId;
	}
	public void setWaiterId(String waiterId) {
		this.waiterId = waiterId;
	}
	@Override
	public String toString() {
		return "TableOccupiedEvent [tableId=" + tableId + ", occupiedTime=" + occupiedTime  + ", billingTime=" + billingTime + ", deliveryTime=" + deliveryTime
				+ ", kitchenReadyTime=" + kitchenReadyTime + ", invoiceAmount=" + invoiceAmount + ", waiterId="
				+ waiterId + "]";
	}
	
}
