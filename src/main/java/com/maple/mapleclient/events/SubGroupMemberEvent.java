package com.maple.mapleclient.events;

public class SubGroupMemberEvent {
	
	
	String subGroupId;
	String SubGroupName;
	public String getSubGroupId() {
		return subGroupId;
	}
	public void setSubGroupId(String subGroupId) {
		this.subGroupId = subGroupId;
	}
	public String getSubGroupName() {
		return SubGroupName;
	}
	public void setSubGroupName(String subGroupName) {
		SubGroupName = subGroupName;
	}
	

}
