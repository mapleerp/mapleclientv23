package com.maple.mapleclient.events;

import java.util.Date;

public class PatientEvent {

	String id;
	String patientName;
	String nationalId;
	String address1;
	String address2;
	String hospitalId;
	String contactPerson;
	String insuranceCompanyId;
	String insuranceCard;
	String policyType;
	String gender;
	String percentageCoverage;
	String phoneNumber;
	Date dateOfBirth;
	String nationality;
	Date insuranceCardExpiry;
	
	
	String insuranceCompanyName;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getPatientName() {
		return patientName;
	}


	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}


	public String getNationalId() {
		return nationalId;
	}


	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getHospitalId() {
		return hospitalId;
	}


	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}


	public String getContactPerson() {
		return contactPerson;
	}


	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}


	public String getInsuranceCompanyId() {
		return insuranceCompanyId;
	}


	public void setInsuranceCompanyId(String insuranceCompanyId) {
		this.insuranceCompanyId = insuranceCompanyId;
	}


	public String getInsuranceCard() {
		return insuranceCard;
	}


	public void setInsuranceCard(String insuranceCard) {
		this.insuranceCard = insuranceCard;
	}


	public String getPolicyType() {
		return policyType;
	}


	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getPercentageCoverage() {
		return percentageCoverage;
	}


	public void setPercentageCoverage(String percentageCoverage) {
		this.percentageCoverage = percentageCoverage;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public Date getDateOfBirth() {
		return dateOfBirth;
	}


	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public Date getInsuranceCardExpiry() {
		return insuranceCardExpiry;
	}


	public void setInsuranceCardExpiry(Date insuranceCardExpiry) {
		this.insuranceCardExpiry = insuranceCardExpiry;
	}


	public String getInsuranceCompanyName() {
		return insuranceCompanyName;
	}


	public void setInsuranceCompanyName(String insuranceCompanyName) {
		this.insuranceCompanyName = insuranceCompanyName;
	}
}
