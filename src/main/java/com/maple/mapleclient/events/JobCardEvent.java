package com.maple.mapleclient.events;

public class JobCardEvent {
	
	String CustomerName;
	String CustomerId;
	String itemName;
	String ServiceNo;
	String jobCardId;
	
	
	public String getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(String customerId) {
		CustomerId = customerId;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getServiceNo() {
		return ServiceNo;
	}
	public void setServiceNo(String serviceNo) {
		ServiceNo = serviceNo;
	}
	public String getJobCardId() {
		return jobCardId;
	}
	public void setJobCardId(String jobCardId) {
		this.jobCardId = jobCardId;
	}
	
	
	
	

}
