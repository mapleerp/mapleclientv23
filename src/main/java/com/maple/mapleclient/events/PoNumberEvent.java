package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class PoNumberEvent {

	String poNumber;
	String supplierName;
	String supGst;
	String supId;
	
	
	public String getSupId() {
		return supId;
	}
	public void setSupId(String supId) {
		this.supId = supId;
	}
	public String getSupGst() {
		return supGst;
	}
	public void setSupGst(String supGst) {
		this.supGst = supGst;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	@Override
	public String toString() {
		return "PoNumberEvent [poNumber=" + poNumber + ", supplierName=" + supplierName + "]";
	}
	
	
}
