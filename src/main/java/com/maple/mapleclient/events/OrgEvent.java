package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class OrgEvent {
	String id;
	String orgId;
	String orgName;
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "OrgEvent [id=" + id + ", orgId=" + orgId + ", orgName=" + orgName + "]";
	}
	
	
	

}
