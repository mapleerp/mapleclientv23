package com.maple.mapleclient.events;

public class EnquiryPopUpEvent {
	
	String itemName;
	String unitName;
	String voucherType;
	Double qyt;
	Double mrp;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public Double getQyt() {
		return qyt;
	}
	public void setQyt(Double qyt) {
		this.qyt = qyt;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	@Override
	public String toString() {
		return "EnquiryPopUpEvent [itemName=" + itemName + ", unitName=" + unitName + ", voucherType=" + voucherType
				+ ", qyt=" + qyt + ", mrp=" + mrp + "]";
	}
	

}
