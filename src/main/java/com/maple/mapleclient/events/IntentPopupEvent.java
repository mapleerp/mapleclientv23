package com.maple.mapleclient.events;

import java.util.Date;

public class IntentPopupEvent {

	
	String intentNumber;
	String fromBranch;
	String voucherNumber;
	Date voucherDate;

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getIntentNumber() {
		return intentNumber;
	}

	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}

	public String getFromBranch() {
		return fromBranch;
	}

	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}

	@Override
	public String toString() {
		return "IntentPopupEvent [intentNumber=" + intentNumber + ", fromBranch=" + fromBranch + ", voucherNumber="
				+ voucherNumber + "]";
	}

	
	
}
