package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

public class PaymentPopUpEvent {
			
			Integer id;
			
			String account;

			String modeOfPayment;
	
			 String remark;
	
			 Double amount;
	
			 String instrumentNumber;
	
			 String voucherNumber;
			
			 String bankAccountNumber;
			
			 Object instrumentDate;
			
			 Object transactionDate;
			
			public Integer getId() {
				return id;
			}

			public String getAccount() {
				return account;
			}

			public void setAccount(String account) {
				this.account = account;
			}

			public String getModeOfPayment() {
				return modeOfPayment;
			}

			public void setModeOfPayment(String modeOfPayment) {
				this.modeOfPayment = modeOfPayment;
			}

			public String getRemark() {
				return remark;
			}

			public void setRemark(String remark) {
				this.remark = remark;
			}

			public Double getAmount() {
				return amount;
			}

			public void setAmount(Double amount) {
				this.amount = amount;
			}

			public String getInstrumentNumber() {
				return instrumentNumber;
			}

			public void setInstrumentNumber(String instrumentNumber) {
				this.instrumentNumber = instrumentNumber;
			}

			public String getVoucherNumber() {
				return voucherNumber;
			}

			public void setVoucherNumber(String voucherNumber) {
				this.voucherNumber = voucherNumber;
			}

			public String getBankAccountNumber() {
				return bankAccountNumber;
			}

			public void setBankAccountNumber(String bankAccountNumber) {
				this.bankAccountNumber = bankAccountNumber;
			}

			public Object getInstrumentDate() {
				return instrumentDate;
			}

			public void setInstrumentDate(Object instrumentDate) {
				this.instrumentDate = instrumentDate;
			}

			public Object getTransactionDate() {
				return transactionDate;
			}

			public void setTransactionDate(Object transactionDate) {
				this.transactionDate = transactionDate;
			}

			public PaymentPopUpEvent(){
				
			}

		

}
