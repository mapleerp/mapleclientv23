package com.maple.mapleclient.events;

import java.util.Date;

public class OtherBranchSalesEvent {
	
	String voucherNumber;
	Date voucherDate;
	String otherBranchSalesHdrId;
	String fromBranchCode;
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getOtherBranchSalesHdrId() {
		return otherBranchSalesHdrId;
	}
	public void setOtherBranchSalesHdrId(String otherBranchSalesHdrId) {
		this.otherBranchSalesHdrId = otherBranchSalesHdrId;
	}
	public String getFromBranchCode() {
		return fromBranchCode;
	}
	public void setFromBranchCode(String fromBranchCode) {
		this.fromBranchCode = fromBranchCode;
	}
	
}
