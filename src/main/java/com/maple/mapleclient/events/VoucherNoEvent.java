package com.maple.mapleclient.events;

import java.util.Date;

public class VoucherNoEvent {

	String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	String intentNumber;
	String voucherNumber;
	Date voucherDate;
	String toBranch;
	String fromBranchCode;
	
	public String getIntentNumber() {
		return intentNumber;
	}
	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getToBranch() {
		return toBranch;
	}
	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}
	
	public String getFromBranchCode() {
		return fromBranchCode;
	}
	public void setFromBranchCode(String fromBranchCode) {
		this.fromBranchCode = fromBranchCode;
	}
	@Override
	public String toString() {
		return "VoucherNoEvent [intentNumber=" + intentNumber + ", voucherNumber=" + voucherNumber + ", voucherDate="
				+ voucherDate + ", toBranch=" + toBranch + "]";
	}
	
}
