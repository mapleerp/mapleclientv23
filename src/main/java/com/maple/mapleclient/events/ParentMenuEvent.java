package com.maple.mapleclient.events;

public class ParentMenuEvent {

	

	private String id;

	private String menuName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	@Override
	public String toString() {
		return "ParentMenuEvent [id=" + id + ", menuName=" + menuName + "]";
	}
	
	
}
