package com.maple.mapleclient.events;

public class MenuEvent {
	

	private String id;

	private String menuName;
//	private String menuFxml;
//	private String menuDescription;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	@Override
	public String toString() {
		return "MenuEvent [id=" + id + ", menuName=" + menuName + "]";
	}
	
	
}
