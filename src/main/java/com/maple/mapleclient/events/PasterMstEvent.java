package com.maple.mapleclient.events;

public class PasterMstEvent {

	String pasterId;
	String pasterName;
	public String getPasterId() {
		return pasterId;
	}
	public void setPasterId(String pasterId) {
		this.pasterId = pasterId;
	}
	public String getPasterName() {
		return pasterName;
	}
	public void setPasterName(String pasterName) {
		this.pasterName = pasterName;
	}
	@Override
	public String toString() {
		return "PasterMstEvent [pasterId=" + pasterId + ", pasterName=" + pasterName + "]";
	}
	
	
}
