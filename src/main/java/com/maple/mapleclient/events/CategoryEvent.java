package com.maple.mapleclient.events;

public class CategoryEvent {
	
	String categoryId;
	String categoryName;
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	@Override
	public String toString() {
		return "CategoryEvent [categoryId=" + categoryId + ", categoryName=" + categoryName + "]";
	}
	
	

}
