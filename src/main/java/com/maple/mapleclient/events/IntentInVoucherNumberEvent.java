package com.maple.mapleclient.events;

import java.util.Date;

public class IntentInVoucherNumberEvent {

	String intentInVoucherNumber;
	String fromBranch;
	Date inVoucherDate;
	String id;
	public String getIntentInVoucherNumber() {
		return intentInVoucherNumber;
	}
	public void setIntentInVoucherNumber(String intentInVoucherNumber) {
		this.intentInVoucherNumber = intentInVoucherNumber;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	

	public Date getInVoucherDate() {
		return inVoucherDate;
	}
	public void setInVoucherDate(Date inVoucherDate) {
		this.inVoucherDate = inVoucherDate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "IntentInVoucherNumberEvent [intentInVoucherNumber=" + intentInVoucherNumber + ", fromBranch="
				+ fromBranch + ", inVoucherDate=" + inVoucherDate + ", id=" + id + "]";
	}
	
}
