package com.maple.mapleclient.events;

import org.springframework.stereotype.Component;

public class SupplierPaymentEvent {

	String supplierId;
	String purchaseHdrId;
	String takId;
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getPurchaseHdrId() {
		return purchaseHdrId;
	}
	public void setPurchaseHdrId(String purchaseHdrId) {
		this.purchaseHdrId = purchaseHdrId;
	}
	public String getTakId() {
		return takId;
	}
	public void setTakId(String takId) {
		this.takId = takId;
	}
	@Override
	public String toString() {
		return "SupplierPaymentEvent [supplierId=" + supplierId + ", purchaseHdrId=" + purchaseHdrId + ", takId="
				+ takId + "]";
	}
	
}
