package com.maple.mapleclient.events;

public class AccountEventDr {
	
	String accountId;
	String accountName;
	String parentId;
	String groupOnly;
	String machineId;
	String taxId;
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getGroupOnly() {
		return groupOnly;
	}
	public void setGroupOnly(String groupOnly) {
		this.groupOnly = groupOnly;
	}
	public String getMachineId() {
		return machineId;
	}
	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	@Override
	public String toString() {
		return "AccountEvent [accountId=" + accountId + ", accountName=" + accountName + ", parentId=" + parentId
				+ ", groupOnly=" + groupOnly + ", machineId=" + machineId + ", taxId=" + taxId + "]";
	}
	
	

}
