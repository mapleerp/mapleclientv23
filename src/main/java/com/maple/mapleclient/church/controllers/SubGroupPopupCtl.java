package com.maple.mapleclient.church.controllers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.SubGroupMember;

import com.maple.mapleclient.events.SubGroupMemberEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class SubGroupPopupCtl {
		
	    SubGroupMemberEvent subGroupMemberEvent;
	
		private EventBus eventBus = EventBusFactory.getEventBus();

		private ObservableList<SubGroupMember> subGroupList = FXCollections.observableArrayList();

		StringProperty SearchString = new SimpleStringProperty();


	    @FXML
	    private TextField txtname;

	    
	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TableView<SubGroupMember> tablePopupView;

	    @FXML
	    private TableColumn<SubGroupMember,String> custAddress;

	    @FXML
	    private TableColumn<SubGroupMember,String> custName;

	    @FXML
	    private Button btnCancel;
	    
	    
	    
	    @FXML
	  		private void initialize() {


	  			txtname.textProperty().bindBidirectional(SearchString);
	  			
	  			SubGroupMemberBySearch("");
	  			
	  			subGroupMemberEvent = new SubGroupMemberEvent();
	  			eventBus.register(this);
	  			btnSubmit.setDefaultButton(true);
	  			tablePopupView.setItems(subGroupList);

	  			btnCancel.setCache(true);

	  			custAddress.setCellValueFactory(cellData -> cellData.getValue().getSubGroupIdProperty());
	  			//custName.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());

	  			tablePopupView.setItems(subGroupList);
	  			for (SubGroupMember s : subGroupList) {
	  				
	  			}

	  			tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	  				if (newSelection != null) {
	  					if (null != newSelection.getSubGroupId() && newSelection.getSubGroupId().length() > 0) {
	  						subGroupMemberEvent.setSubGroupId(newSelection.getSubGroupId());
	  				
	  						eventBus.post(subGroupMemberEvent);
	  					}
	  				}
	  			});

	  		
	  			SearchString.addListener(new ChangeListener() {
	  				@Override
	  				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

	  					//SubGroupMemberBySearch((String) newValue);
	  				}
	  			});

	  		}

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB) {

			} else {
				txtname.requestFocus();
			}


	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tablePopupView.requestFocus();
				tablePopupView.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			}
	    	

	    }

	    @FXML
	    void onCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();


	    }

	    @FXML
	    void submit(ActionEvent event) {
	     	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();


	    }
	    
  private void SubGroupMemberBySearch(String searchData) {

			
			ArrayList subGroupMember = new ArrayList();
			SubGroupMember cust = new SubGroupMember();
			RestTemplate restTemplate = new RestTemplate();

			subGroupMember = RestCaller.SearchSubGroupByNames( searchData);

			Iterator itr = subGroupMember.iterator();
			while (itr.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				
				Object subGroupId = lm.get("subGroupId");
				System.out.println("================="+lm.get("subGroupId"));
			
				Object id = lm.get("id");
				if (null != id) {
					cust = new SubGroupMember();
					cust.setSubGroupId((String) subGroupId);
				
				
					cust.setId((String) id );
					
					
				
					

					System.out.println(lm);

					subGroupList.add(cust);
				}

			}
			return;
	    }

	}


