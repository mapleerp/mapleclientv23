package com.maple.mapleclient.church.controllers;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.FamilyMst;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.PasterDtl;
import com.maple.mapleclient.entity.PasterMst;
import com.maple.mapleclient.events.MemberMstEvent;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
public class PasterMstCtl {

	private EventBus eventBus = EventBusFactory.getEventBus();
	 private ObservableList<PasterMst> pasterMstList = FXCollections.observableArrayList();
	 


	    @FXML
	    private TextField txtPasterName;

	    @FXML
	    private DatePicker dpDoj;

	    @FXML
	    private DatePicker dpdol;

	    @FXML
	    private TextField txtOrgId;

	    @FXML
	    private TextField txtMemberId;

	    @FXML
	    private Button btnSave1;

	    @FXML
	    private Button btnClear;
	    @FXML
	    private Button btnShowAll;

	    @FXML
	    private Button btnDelete1;

	    @FXML
	    private TableView<PasterMst> tblPasterMst;
	
	    @FXML
	    private TableColumn<PasterMst, String> clPasterMstId;
	
	    @FXML
	    private TableColumn<PasterMst, String> clPasterName;
	
	    @FXML
	    private TableColumn<PasterMst, LocalDate> clDOJ;
	
	    @FXML
	    private TableColumn<PasterMst, LocalDate> clDOL;
	
	    @FXML
	    private TableColumn<PasterMst, String> clOrgId;
	
	    @FXML
	    private TableColumn<PasterMst, String> clMemberId;
	   PasterMst pasterMst =new PasterMst();

	    @FXML
	    void ActionDate1(KeyEvent event) {
	      	if (event.getCode() == KeyCode.ENTER) {
	    		dpdol.requestFocus();
	
	    	}
	    }

	    @FXML
	    void ActionDate2(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		btnSave1.requestFocus();
	
	    	}
	    }

	    @FXML
	    void ActionDelete(ActionEvent event) {
	    	if(null != pasterMst) {
				  System.out.println("deleteeeeeeeeeee");
			  if(null != pasterMst.getId()) {
				  System.out.println("deleteeeeeeeeeee");
			  RestCaller.deletePasterMst(pasterMst.getId());
			  notifyMessage(5,"Deleted!!!");
			  tblPasterMst.getItems().clear(); 
			 showAll();
			 pasterMst=new PasterMst();
			
			  txtMemberId.setText("");
				txtOrgId.setText("");
				txtPasterName.setText("");
				dpDoj.setValue(null);
				dpdol.setValue(null);	
			 
	    }}}
			  private void showAll() {
			    	
			    	ResponseEntity<List<PasterMst>> pasterMstResp = RestCaller.getAllPasterMst();
			    	pasterMstList = FXCollections.observableArrayList(pasterMstResp.getBody());
			    	tblPasterMst.getItems().clear();
			    	FillTable();
			  }
	  

	    @FXML
	    void ActionPasterName(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		txtOrgId.requestFocus();
	
	        
	    	}
	    }

	    @FXML
	    void ActionSave(ActionEvent event) {
	    	if(txtPasterName.getText().isEmpty()) {
	    		
	    		notifyMessage(5, "Enter paster name");
	    		return;
	    		
	    	}if(txtOrgId.getText().isEmpty()) {
	    		notifyMessage(5, "Enter organisation id");
	    		return;	
	    	}
	    
	    	if(txtMemberId.getText().isEmpty()) {
	    		notifyMessage(5, "Enter member id");
	    		return;	
	    	}
	    	if(null==dpdol.getValue()) {
	    		notifyMessage(5, "Enter date of leaving ");
	    		return;	
	    	}
	    	if(null==dpDoj.getValue()) {
	    		notifyMessage(5, "Enter date of joining ");
	    		return;	
	    	}
	    
	    	if(null!=pasterMst.getId()) {
	    		pasterMst.setPasterName(txtPasterName.getText());
	    		pasterMst.setOrgId(txtOrgId.getText());
		    	pasterMst.setMemberId(txtMemberId.getText());
		    	if(null!=dpdol.getValue()) {
		     	pasterMst.setdol(Date.valueOf(dpdol.getValue()));
		    	}
		    	if(null!=dpDoj.getValue()){
		     	pasterMst.setdoj(Date.valueOf(dpDoj.getValue()));
		    	}
		     	RestCaller.updatePasterMst(pasterMst);
		     	notifyMessage(5, "Edited Successfully Completed");
		//    	PasterMst pasterMst=null;
			//	FillTable();
		    	showAll();
				PasterMst pasterMst=null;
				txtMemberId.setText("");
				txtOrgId.setText("");
				
				txtPasterName.setText("");
				dpDoj.setValue(null);
				dpdol.setValue(null);	
				
			
	    	}else {
	    		
	    
	    	String PastId = RestCaller.getVoucherNumber("PASTR");
	    	pasterMst.setPasterMstId(PastId);
	    	pasterMst.setPasterName(txtPasterName.getText());
	    	if(null!=dpDoj.getValue()){
	    	pasterMst.setdoj(Date.valueOf(dpDoj.getValue()));
	    	}
	    	if(null!=dpdol.getValue()) {
	    	pasterMst.setdol(Date.valueOf(dpdol.getValue()));
	    	}
	    	pasterMst.setOrgId(txtOrgId.getText());
	    	pasterMst.setMemberId(txtMemberId.getText());
	    	ResponseEntity<PasterMst> respentity = RestCaller.savePasterMst(pasterMst);
	      	PasterMst pasterMst=respentity.getBody();
	    	
	       
	    //	pasterMst = respentity.getBody();
	    //	pasterMstList.add(pasterMst);
	     	showAll();
			System.out.println("added paster mst" + pasterMst);
			notifyMessage(5, "Save Successfully Completed");
		
			FillTable();
			 pasterMst=null;
			txtMemberId.setText("");
			txtOrgId.setText("");
			
			txtPasterName.setText("");
			dpDoj.setValue(null);
			dpdol.setValue(null);	
			
	    	}
			}
	    @FXML
	    void ActionShowAll(ActionEvent event) {
	    	showAll();
	    }

	    @FXML
	    void OnKeyOrg(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		txtMemberId.requestFocus();
	
	        
	    	}
	    	loadOrgPopup(); 
	    }

	    @FXML
	    void OnKeyPressMem(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		dpDoj.requestFocus();
	     
	    	}
	    	loadMemberPopup();
	    }

	    public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");
			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
	}
	    private void FillTable() {
	    			tblPasterMst.setItems(pasterMstList);
	    			System.out.println("table fillingggggggggggg2222222");
	    			clMemberId.setCellValueFactory(cellData -> cellData.getValue().getMemberIdProperty());
	    			clOrgId.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());
	    			clPasterMstId.setCellValueFactory(cellData -> cellData.getValue().getPasterMstIdProperty());
	    			clPasterName.setCellValueFactory(cellData -> cellData.getValue().getPasterNameProperty());
	    			clDOJ.setCellValueFactory(cellData -> cellData.getValue().getDojProperty());
	    			clDOL.setCellValueFactory(cellData -> cellData.getValue().getDolProperty());
	    			System.out.println("table fillingggggggggggg");
	    			
	    		}
	    @FXML
   	private void initialize() {
   		System.out.println("ssssssss=====initialize=====sssssssssss");
   		eventBus.register(this);
   	  tblPasterMst.getSelectionModel().selectedItemProperty().addListener((obs,
			  oldSelection, newSelection) -> { if (newSelection != null) {
			  System.out.println("getSelectionModel"); 
			  if (null != newSelection.getId())
			  {
			  
				  pasterMst = new PasterMst(); 
				  pasterMst.setId(newSelection.getId());
				  pasterMst.setPasterMstId(newSelection.getPasterMstId());
			  txtPasterName.setText(newSelection.getPasterName());
			  txtOrgId.setText(newSelection.getOrgId());
			  txtMemberId.setText(newSelection.getMemberId());
			  dpDoj.setValue(SystemSetting.utilToLocaDate(newSelection.getdoj()));
			  }} });
   	  
   	txtOrgId.setEditable(false);
   	txtMemberId.setEditable(false);
   	
   	}	private void loadMemberPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MemberMstPopUp.fxml"));
			Parent root1;
	
			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();
	
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			dpDoj.requestFocus();
		
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	@Subscribe
		public void popupMemberlistner(MemberMstEvent memberMstEvent) {
	
			Stage stage = (Stage) btnSave1.getScene().getWindow();
			if (stage.isShowing()) {
	
				txtMemberId.setText(memberMstEvent.getMemberId());
	
			}
	
		}
	    private void loadOrgPopup() {
			
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/OrganisationIdpopup.fxml"));
				Parent root1;
	
				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();
	
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				txtMemberId.requestFocus();
			
	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		}

	    @FXML
	    void clearFields(ActionEvent event) {
	    	PasterMst pasterMst=new PasterMst();
	    	txtMemberId.setText("");
			txtOrgId.setText("");
			txtPasterName.setText("");
			dpDoj.setValue(null);
			dpdol.setValue(null);	
	    }

	@Subscribe
	public void popupOrglistner(OrgEvent orgEvent) {
	
		Stage stage = (Stage) btnShowAll.getScene().getWindow();
		if (stage.isShowing()) {
			txtOrgId.setText(orgEvent.getOrgName());
		
		}
	}
	

	    }   

