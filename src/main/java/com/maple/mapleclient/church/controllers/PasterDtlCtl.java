package com.maple.mapleclient.church.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MeetingMst;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.PasterDtl;
import com.maple.mapleclient.entity.PasterMst;
import com.maple.mapleclient.events.MemberMstEvent;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.events.PasterMstEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
public class PasterDtlCtl {
	PasterDtl pasterDtl=null;
	private EventBus eventBus = EventBusFactory.getEventBus();
	 private ObservableList<PasterDtl> pasterDtlList = FXCollections.observableArrayList();
    @FXML
    private TextField txtMemberName;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnShowAll;


    @FXML
    private Button btnDelete;

    @FXML
    private TextField txtPasterId;

    @FXML
    private TextField txtMemberId;

    @FXML
    private DatePicker dpPasterMstDoj;

    @FXML
    private TableView<PasterDtl> tblPasterDetails;

    @FXML
    private TableColumn<PasterDtl, String> clMemberName;

    @FXML
    private TableColumn<PasterDtl, String> clMemberId;

    @FXML
    private TableColumn<PasterDtl, String> clPasterId;

    @FXML
    private TableColumn<PasterDtl, LocalDate> clPasterMstDoj;
    @FXML
    void ActionDelete(ActionEvent event) {
    	if(null != pasterDtl) {
    		if(null != pasterDtl.getId()) {
    		  RestCaller.deletePasterDtl(pasterDtl.getId());
    		  notifyMessage(5,"Deleted!!!");
    		  
    		  tblPasterDetails.getItems().clear();
    		  showAll();
    		  
    		  } }
    }

    @FXML
    void ActionShowAll(ActionEvent event) {
    	showAll();
    }

    @FXML
    void ActionSave(ActionEvent event) {
    	pasterDtl = new PasterDtl();
    	pasterDtl.setMemberName(txtMemberName.getText());
    	pasterDtl.setPasterId(txtPasterId.getText());
    	pasterDtl.setMemberId(txtMemberId.getText());

  	pasterDtl.setpasterMstDoj(Date.valueOf(dpPasterMstDoj.getValue()));



    	ResponseEntity<PasterDtl> respentity = RestCaller.savePasterDtl(pasterDtl);
    	pasterDtl = respentity.getBody();
    	pasterDtlList.add(pasterDtl);
		System.out.println("saved paster dtlssss" + pasterDtl);
    	
		notifyMessage(5, "Save Successfully Completed");
	 FillTable();
       txtMemberId.setText("");
   	txtMemberName.setText("");
   	txtPasterId.setText("");
   	dpPasterMstDoj.setValue(null);
//    	
    	
    }
    private void FillTable() {

		tblPasterDetails.setItems(pasterDtlList);
		System.out.println("table fillingggggggggggg2222222");
		
		clMemberId.setCellValueFactory(cellData -> cellData.getValue().getMemberIdProperty());
		clMemberName.setCellValueFactory(cellData -> cellData.getValue().getMemberNameProperty());
		clPasterId.setCellValueFactory(cellData -> cellData.getValue().getPasterIdProperty());
		clPasterMstDoj.setCellValueFactory(cellData -> cellData.getValue().getPasterMstDojProperty());
	
		System.out.println("table fillingggggggggggg");
		
	}
    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    @FXML
   	private void initialize() {
   		System.out.println("ssssssss=====initialize=====sssssssssss");
   		eventBus.register(this);
   		tblPasterDetails.getSelectionModel().selectedItemProperty().addListener((obs,
  			  oldSelection, newSelection) -> { if (newSelection != null) {
  			  System.out.println("getSelectionModel"); if (null != newSelection.getId())
  			  {
  			  
  				  pasterDtl = new PasterDtl(); 
  				pasterDtl.setId(newSelection.getId()); } }
  			  });
  		
   	}
    private void loadMemberPopup() {
    	
    	try {
    		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MemberMstPopUp.fxml"));
    		Parent root1;

    		root1 = (Parent) fxmlLoader.load();
    		Stage stage = new Stage();

    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.initStyle(StageStyle.UNDECORATED);
    		stage.setTitle("ABC");
    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.setScene(new Scene(root1));
    		stage.show();
    		txtPasterId.requestFocus();
    	

    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}

    }
    private void showAll() {
    	
    	ResponseEntity<List<PasterDtl>> pasterDtlResp = RestCaller.getAllPasterDtls();
    	pasterDtlList = FXCollections.observableArrayList(pasterDtlResp.getBody());
    	tblPasterDetails.getItems().clear();
    	FillTable();

    	
    }  
    @Subscribe
    	public void popupMemberlistner(MemberMstEvent memberMstEvent) {

    		Stage stage = (Stage) btnSave.getScene().getWindow();
    		if (stage.isShowing()) {

    			txtMemberId.setText(memberMstEvent.getMemberId());
    			txtMemberName.setText(memberMstEvent.getMemberName());
    			
    		}

    	}   
        
        

    @FXML
    void ObKeyPressMem(KeyEvent event) {
    	loadMemberPopup();
    }

    @FXML
    void OnKeyPressPastr(KeyEvent event) {
    	 loadPastPopup();
    }
    private void loadPastPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/PasterDtlPopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			dpPasterMstDoj.requestFocus();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
@Subscribe
public void popupPastlistner(PasterMstEvent pastEvent) {
	Stage stage = (Stage) btnShowAll.getScene().getWindow();
	if (stage.isShowing()) {
		txtPasterId.setText(pastEvent.getPasterId());
	}
}  
       
}
