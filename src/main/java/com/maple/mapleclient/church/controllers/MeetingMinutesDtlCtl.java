package com.maple.mapleclient.church.controllers;



import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.FamilyMst;
import com.maple.mapleclient.entity.MeetingMinutesDtl;
import com.maple.mapleclient.entity.MeetingMst;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.events.ItemPopupEvent;
import com.maple.mapleclient.events.MeetingEvent;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
public class MeetingMinutesDtlCtl {
	private EventBus eventBus = EventBusFactory.getEventBus();
	MeetingMinutesDtl meetingMinutesDtl=null;
	 private ObservableList<MeetingMinutesDtl> meetingMinutesDtlList = FXCollections.observableArrayList();
	@FXML
    private TextField txtMeetingId;

  

    @FXML
    private TextField txtMinutesText;

    @FXML
    private DatePicker dpdateOfAction;

    @FXML
    private TextField txtOrgId;

    @FXML
    private TextField txtMinutePointByMember;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnShowAll;


    @FXML
    private Button btnDelete;

    @FXML
    private TableView<MeetingMinutesDtl> tblMeetingMinutes;

    @FXML
    private TableColumn<MeetingMinutesDtl, String> clMeetingId;

    @FXML
    private TableColumn<MeetingMinutesDtl, String> clMinutesId;

    @FXML
    private TableColumn<MeetingMinutesDtl, String> clMinutesText;

    @FXML
    private TableColumn<MeetingMinutesDtl, String> clOrgId;

    @FXML
    private TableColumn<MeetingMinutesDtl, String> clMinutespointByMember;

    @FXML
    private TableColumn<MeetingMinutesDtl, LocalDate> clDateOfAction;
    @FXML
    void ActionDelete(ActionEvent event) {
    	if(null != meetingMinutesDtl) {
    		if(null != meetingMinutesDtl.getId()) {
    		  RestCaller.deleteMeetingMinutes(meetingMinutesDtl.getId());
    		  notifyMessage(5,"Deleted!!!");
    		  
    		  tblMeetingMinutes.getItems().clear();
    		  showAll();
    		  
    		  } }
        }
    

    @FXML
    void ActionShowAll(ActionEvent event) {
    	showAll();
    }
//    @FXML
//    void OnMouseClickedOrg(MouseEvent event) {
//
//    }
    @FXML
    void OnKeyPressOrg(KeyEvent event) {
    	loadOrgPopup();
    }
   
    @FXML
    void onKeyMeetingId(KeyEvent event) {
    	 loadMeetingPopup();
    }
    @FXML
    void onKeyMinutText(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    	txtOrgId.requestFocus();
    	}
    }
    @FXML
    void onKeyMinutePoint(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
        	dpdateOfAction.requestFocus();
        	}
    }
	@FXML
	private void initialize() {
		System.out.println("ssssssss=====initialize=====sssssssssss");
		eventBus.register(this);
		tblMeetingMinutes.getSelectionModel().selectedItemProperty().addListener((obs,
  			  oldSelection, newSelection) -> { if (newSelection != null) {
  			  System.out.println("getSelectionModel"); if (null != newSelection.getId())
  			  {
  			  
  				  meetingMinutesDtl = new MeetingMinutesDtl(); 
  				meetingMinutesDtl.setId(newSelection.getId()); } }
  			  });
  		
	}
	
	    @FXML
	    void ActionSave(ActionEvent event) {
	    	if (txtMeetingId.getText().trim().isEmpty()) {
				notifyMessage(5, " Please enter minutes!!!");
				return;
			} else if (txtMinutesText.getText().trim().isEmpty())  {
				notifyMessage(5, " Please enter minutes text...!!!");
				return;
			}else if (txtOrgId.getText().trim().isEmpty())  {
					notifyMessage(5, " Please enter org id ...!!!");
					return;
			}else if (txtMinutePointByMember.getText().trim().isEmpty())  {
				notifyMessage(5, " Please enter minute point by member ...!!!");
				return;
			}else if (null==dpdateOfAction.getValue())  {
					notifyMessage(5, " Please enter date of action...!!!");
					return;
			} else {
				if (null == meetingMinutesDtl) {
			
	    	meetingMinutesDtl=new MeetingMinutesDtl();
	    	meetingMinutesDtl.setMeetingId(txtMeetingId.getText());
	    	String minutesId = RestCaller.getVoucherNumber("MINT");
	    	meetingMinutesDtl.setMinutesId(minutesId);
	    	meetingMinutesDtl.setMinutesText(txtMinutesText.getText());
	    	meetingMinutesDtl.setOrgId(txtOrgId.getText());
	    	meetingMinutesDtl.setMinutePointbyMember(txtMinutePointByMember.getText());
   	meetingMinutesDtl.setDateOfAction(Date.valueOf(dpdateOfAction.getValue()));
	    	ResponseEntity<MeetingMinutesDtl> respentity = RestCaller.saveMeetingMinutsDtl(meetingMinutesDtl);
	    	meetingMinutesDtl = respentity.getBody();
			System.out.println("saved meeting minutes" + meetingMinutesDtl);
			meetingMinutesDtlList.add(meetingMinutesDtl);
	          
	    	notifyMessage(5, "Save Successfully Completed");
	    	 FillTable();
	        txtMeetingId.setText("");
	    	txtMinutePointByMember.setText("");
	  
	    	txtMinutesText.setText("");
	    	txtOrgId.setText("");
	
	    	dpdateOfAction.setValue(null);
	  
	    	
				}}
	    }
	    private void FillTable() {

			tblMeetingMinutes.setItems(meetingMinutesDtlList);
			System.out.println("table fillingggggggggggg2222222");
			clDateOfAction.setCellValueFactory(cellData -> cellData.getValue().getDateOfActionProperty());
			clMeetingId.setCellValueFactory(cellData -> cellData.getValue().getMeetingIdProperty());
			clMinutesId.setCellValueFactory(cellData -> cellData.getValue().getMinutesIdProperty());
			
			clMinutespointByMember.setCellValueFactory(cellData -> cellData.getValue().getMinutePointbyMemberProperty());
			clMinutesText.setCellValueFactory(cellData -> cellData.getValue().getMinutesTextProperty());
			clOrgId.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());
			System.out.println("table fillingggggggggggg");
}
	    
	    public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");
			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}
 private void loadOrgPopup() {
			
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/OrganisationIdpopup.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				txtMinutePointByMember.requestFocus();
			

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
 private void showAll() {
 	
 	ResponseEntity<List<MeetingMinutesDtl>> pasterDtlResp = RestCaller.getAllMeetingMinutes();
 	meetingMinutesDtlList = FXCollections.observableArrayList(pasterDtlResp.getBody());
 	tblMeetingMinutes.getItems().clear();
 	FillTable();

 	
 } 
 
 @Subscribe
	public void popupOrglistner(OrgEvent orgEvent) {

		Stage stage = (Stage) btnDelete.getScene().getWindow();
		if (stage.isShowing()) {

			
			txtOrgId.setText(orgEvent.getOrgId());
			

		}

	}
 @FXML
 void loadPopup2(KeyEvent event) {
 	if (event.getCode() == KeyCode.ENTER) {
 		loadMeetingPopup();
 	}
 }
private void loadMeetingPopup() {
 	
 	try {
 		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MeetingPopup.fxml"));
 		Parent root1;

 		root1 = (Parent) fxmlLoader.load();
 		Stage stage = new Stage();

 		stage.initModality(Modality.APPLICATION_MODAL);
 		stage.initStyle(StageStyle.UNDECORATED);
 		stage.setTitle("ABC");
 		stage.initModality(Modality.APPLICATION_MODAL);
 		stage.setScene(new Scene(root1));
 		stage.show();
 		txtMinutesText.requestFocus();
 	

 	} catch (IOException e) {
 		// TODO Auto-generated catch block
 		e.printStackTrace();
 	}
 	 
 }
//@FXML
// void OnKeyPressMeet(KeyEvent event) {
//	
// }

 @Subscribe
 	public void popupMeetinglistner(MeetingEvent meetingEvent) {

 		Stage stage = (Stage) btnDelete.getScene().getWindow();
 		if (stage.isShowing()) {
 			txtMeetingId.setText(meetingEvent.getMeetingId());
 		}

 	}   
     
		}

