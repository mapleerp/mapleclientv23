package com.maple.mapleclient.church.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

import java.io.IOException;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptHdr;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.events.AccountPopupEvent;
import com.maple.mapleclient.events.MemberMstEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
public class ReceiptsWindowCtl {
	EventBus eventBus = EventBusFactory.getEventBus();
	ReceiptDtl receipt;
	Double totalamount;
	ReceiptHdr receipthdr = null;
	ReceiptDtl recieptDelete;
	private ObservableList<ReceiptDtl> receiptList1 = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
    @FXML
    private TextField txtAccount;

    @FXML
    private TextField txtRemark;

    @FXML
    private TextField txtMemberName;
    @FXML
    private Button btnClear;

   
    
    
    @FXML
    private TextField txtAmount;

    @FXML
    private TextField txtInstrumentNumber;

    @FXML
    private TextField txtVoucherNumber;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnDelete;

    @FXML
    private DatePicker dpTransDate;

    @FXML
    private DatePicker dpInstrumentDate;

    @FXML
    private ComboBox<String> cmbDepositBank;

    @FXML
    private ComboBox<String> cmbModeofReceipt;

    @FXML
    private TextField txtMemberId;

    @FXML
    private TableView<ReceiptDtl> tblReceiptWindow;

    @FXML
    private TableColumn<ReceiptDtl, String> RWC1;

    @FXML
    private TableColumn<ReceiptDtl, LocalDate> RWC2;

    @FXML
    private TableColumn<ReceiptDtl, String> RWC4;

    @FXML
    private TableColumn<ReceiptDtl, Number> RWC5;

    @FXML
    private TableColumn<ReceiptDtl, String> RWC6;

    @FXML
    private TableColumn<ReceiptDtl, String> RWC7;

    @FXML
    private TableColumn<ReceiptDtl, LocalDate> RWC8;

    @FXML
    private TableColumn<ReceiptDtl, String> RWC9;

    @FXML
    private TableColumn<ReceiptDtl, String> RWC11;
    @FXML
    private TableColumn<ReceiptDtl, String> RWC111;
    
    @FXML
    private TextField txtTotal;
    
    private void loadMemberPopup() {
    	
    	try {
    		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MemberMstPopUp.fxml"));
    		Parent root1;

    		root1 = (Parent) fxmlLoader.load();
    		Stage stage = new Stage();

    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.initStyle(StageStyle.UNDECORATED);
    		stage.setTitle("ABC");
    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.setScene(new Scene(root1));
    		stage.show();
    		txtAccount.requestFocus();
    	

    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}

    }
    @Subscribe
	public void popupMemberlistner(MemberMstEvent memberMstEvent) {

		Stage stage = (Stage) btnClear.getScene().getWindow();
		if (stage.isShowing()) {
			txtMemberId.setText(memberMstEvent.getMemberId());
			txtMemberName.setText(memberMstEvent.getMemberName());
		}	
	}  
  
    @FXML
    void loadPopup(MouseEvent event) {
    	loadMemberPopup();
    }
   
    @FXML
    private Button btnFinalSubmit;

    @FXML
    void OnEnterClick(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			if(!txtAccount.getText().trim().isEmpty())
				cmbModeofReceipt.requestFocus();
			else
				showPopup();

		}
    }

    @FXML
    void RemarkKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			dpTransDate.requestFocus();

		}
    }

    @FXML
    void SaveOnKeyPrss(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			addItem();
		}
    }

    @FXML
    void actionMemberId(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		loadMemberPopup();

		}
    }
    @FXML
    void amountKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			if (txtInstrumentNumber.isDisable()) {
				btnSave.requestFocus();
			} else
				txtInstrumentNumber.requestFocus();
		}
    }

    @FXML
    void bankOnEnter(KeyEvent event) {
    	 if (event.getCode() == KeyCode.ENTER) {
				dpInstrumentDate.requestFocus();

			}
    }

    @FXML
    void clear(ActionEvent event) {
    	txtTotal.clear();
    }

    @FXML
    void delete(ActionEvent event) {
    	RestCaller.deleteReceiptDtl(recieptDelete.getId());
		notifyMessage(5, "Deleted");
		
		tblReceiptWindow.getItems().clear();
		ResponseEntity<List<ReceiptDtl>> receiptdtlSaved = RestCaller.getReceiptDtl(receipthdr);
		receiptList1 = FXCollections.observableArrayList(receiptdtlSaved.getBody());
		for(ReceiptDtl recptdtl : receiptList1)
		{
			ResponseEntity<AccountHeads> accSaved = RestCaller.getAccountById(recptdtl.getAccount());
			recptdtl.setAccountName(accSaved.getBody().getAccountName());
		}
		tblReceiptWindow.setItems(receiptList1);
		txtTotal.clear();
		setTotal();
		
    }

    @FXML
    void finalSubmit(ActionEvent event) {
    	ResponseEntity<List<ReceiptDtl>> receiptdtlSaved = RestCaller.getReceiptDtl(receipthdr);
		if (receiptdtlSaved.getBody().size() == 0) {
			return;
		}
		String financialYear = SystemSetting.getFinancialYear();
		String vNo = RestCaller.getVoucherNumber(financialYear + "RECEIPT");
		receipthdr.setVoucherNumber(vNo);
		  Date date = Date.valueOf(dpTransDate.getValue());
			
			Format formatter;
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			
			
			receipthdr.setVoucherDate(date);
			
			RestCaller.updateReceipthdr(receipthdr);
		

			String strDate = formatter.format(receipthdr.getVoucherDate());
			
			try {
			
				JasperPdfReportService.OrgReceiptInvoice(receipthdr.getVoucherNumber(),strDate,receipt.getMemberId());
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		 
		txtTotal.clear();
		notifyMessage(5, "Saved Successfully!!!!");
		tblReceiptWindow.getItems().clear();
		receipthdr = null;
		
    }

    @FXML
    void instrumentDate(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			btnSave.requestFocus();
		}
    }

    @FXML
    void instrumnetOnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		cmbDepositBank.requestFocus();
		}
    }
    
    @FXML
    void loadpopup(MouseEvent event) {
    	showPopup();
    }

    @FXML
    void modeOnEnter(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtRemark.requestFocus();

		}
    }

    @FXML
    void save(ActionEvent event) {
    	addItem();
    }

    @FXML
    void voucherDateKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			txtAmount.requestFocus();

		}
    }
	@FXML
	private void initialize() {
		eventBus.register(this);
		ResponseEntity<AccountHeads>accountHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
				
				cmbModeofReceipt.getItems().add("TRASNFER");
				cmbModeofReceipt.getItems().add("CHEQUE");
				cmbModeofReceipt.getItems().add("DD");
				cmbModeofReceipt.getItems().add("MOBILE PAY");
				cmbModeofReceipt.getItems().add(accountHead.getBody().getAccountName());
				cmbModeofReceipt.valueProperty().addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						if (newValue.matches(SystemSetting.getSystemBranch()+"-CASH")) {
							cmbDepositBank.setDisable(true);
							txtInstrumentNumber.setDisable(true);
							dpInstrumentDate.setDisable(true);
						} else {
							cmbDepositBank.setDisable(false);
							txtInstrumentNumber.setDisable(false);
							dpInstrumentDate.setDisable(false);
						}

					}

				});
				txtAmount.textProperty().addListener(new ChangeListener<String>() {
					@Override
					public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
						if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
							txtAmount.setText(oldValue);
						}
					}
				});
				ArrayList bankList = new ArrayList();
				bankList = RestCaller.getAllBankAccounts();
				Iterator itr1 = bankList.iterator();
				while (itr1.hasNext()) {
					LinkedHashMap lm = (LinkedHashMap) itr1.next();
					Object bankName = lm.get("accountName");
					Object bankid= lm.get("id");
					String bankId = (String) bankid;
					if (bankName != null) {
						cmbDepositBank.getItems().add((String) bankName);
						
								    //accountHeads.getAccountName);
					}
				}
				tblReceiptWindow.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
					if (newSelection != null) {
						System.out.println("getSelectionModel");
						if (null != newSelection.getId()) {

							recieptDelete = new ReceiptDtl();
							recieptDelete.setId(newSelection.getId());
						}
					}
				});
			
	}
	
	
	private void clearfields()
	{
		http://localhost:8185/MMC/menuconfigmstresource/menuconfigmstbymenuname/TASK LIST.clear();
		txtAmount.clear();
		cmbDepositBank.getSelectionModel().clearSelection();
		cmbModeofReceipt.getSelectionModel().clearSelection();
		txtInstrumentNumber.clear();
		txtRemark.clear();
		txtMemberId.clear();
		txtMemberName.clear();
	}
	private void filltable() {
		tblReceiptWindow.setItems(receiptList1);
		RWC1.setCellValueFactory(cellData -> cellData.getValue().getaccountProperty());
		RWC11.setCellValueFactory(cellData -> cellData.getValue().getremarkProperty());
		RWC5.setCellValueFactory(cellData -> cellData.getValue().getamountPropery());
		RWC6.setCellValueFactory(cellData -> cellData.getValue().getdepositBankProperty());
		RWC2.setCellValueFactory(cellData -> cellData.getValue().gettransDateProperty());
		RWC8.setCellValueFactory(cellData -> cellData.getValue().getInstrumentDateProperty());
		RWC4.setCellValueFactory(cellData -> cellData.getValue().getmodeOfPaymentProperty());
		RWC7.setCellValueFactory(cellData -> cellData.getValue().getinstnumberProperty());
	//RWC9.setCellValueFactory(cellData -> cellData.getValue().getinvoiceNumberProperty());
		RWC111.setCellValueFactory(cellData-> cellData.getValue().getmemberIdProperty());
	}
	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountNewPopup.fxml"));
			Parent root1;
			root1 = (Parent) loader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("Accounts");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			cmbModeofReceipt.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(AccountPopupEvent accountPopupEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnClear.getScene().getWindow();
		if (stage.isShowing()) {

					txtAccount.setText(accountPopupEvent.getAccountName());
	
		}

	}
	
	private void addItem() {
		if(txtMemberId.getText().isEmpty()){
			notifyMessage(5, "Select Member!!!!");
			return;
		}
		if (txtAccount.getText().trim().isEmpty()) {
			notifyMessage(5, "Select Account Number!!!!");
			txtAccount.requestFocus();
		} else if (cmbModeofReceipt.getSelectionModel().isEmpty()) {
			notifyMessage(5, "Select Mode Of Receipt!!!!");
			cmbModeofReceipt.requestFocus();
		
		} else if (txtAmount.getText().trim().isEmpty()) {
			notifyMessage(5, "Type Amount!!!!");
			txtAmount.requestFocus();
		} else if (null == dpTransDate.getValue()) {
			notifyMessage(5, "Please Select Date!!!!");
			dpTransDate.requestFocus();
		} else {
			if (null == receipthdr) {
				receipthdr = new ReceiptHdr();

			
					receipthdr.setMemberId(txtMemberId.getText());
				
				receipthdr.setTransdate(Date.valueOf(dpTransDate.getValue()));
				java.util.Date uDate = Date.valueOf(dpTransDate.getValue());
				receipthdr.setVoucherDate(uDate);
				receipthdr.setBranchCode(SystemSetting.systemBranch);
			
				ResponseEntity<ReceiptHdr> respentity = RestCaller.saveReceipthdr(receipthdr);
				receipthdr = respentity.getBody();
			}
			receipt = new ReceiptDtl();
			ResponseEntity<AccountHeads > accSaved = RestCaller.getAccountHeadByName(txtAccount.getText());
			receipt.setMemberId(txtMemberId.getText());
			 System.out.println("receipt dtlssssssssss..........."+txtMemberId.getText());
			receipt.setAccount(accSaved.getBody().getId());
			receipt.setRemark(receipt.getRemark());
			receipt.setAmount(Double.parseDouble(txtAmount.getText()));
			if (cmbModeofReceipt.getSelectionModel().getSelectedItem().toString().equalsIgnoreCase(SystemSetting.getSystemBranch()+"-CASH"))
			{
				receipt.setInstrumentDate(null);
				receipt.setRemark(txtRemark.getText());
				
				receipt.setTransDate(Date.valueOf(dpTransDate.getValue()));
				
				receipt.setModeOfpayment(cmbModeofReceipt.getSelectionModel().getSelectedItem());
				receipt.setReceipthdr(receipthdr);
 
				ResponseEntity<AccountHeads > accountHeadsResp = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch()+"-CASH");
				if(null==accountHeadsResp.getBody()) {
					notifyMessage(5, "Branch Cash account not set");
					return;
				}
				AccountHeads accountHeads = accountHeadsResp.getBody();
				receipt.setDebitAccountId(accountHeads.getId());
				
				
				ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);
				
				
				receipt = respentity.getBody();

				setTotal();
				receipt.setAccountName(txtAccount.getText());
				receiptList1.add(receipt);
				filltable();
			}
			else {
				if (txtInstrumentNumber.getText().trim().isEmpty()) {
					notifyMessage(5, "Type Instrument Number!!!!");
					txtInstrumentNumber.setDisable(false);
					txtInstrumentNumber.requestFocus();
					return;
				} else if (cmbDepositBank.getSelectionModel().getSelectedItem().trim().isEmpty()) {
					notifyMessage(5, "Type Deposit Bank!!!!");
					cmbDepositBank.setDisable(false);
					cmbDepositBank.requestFocus();
					return;
				} else if (null == dpInstrumentDate.getValue()) {
					notifyMessage(5, "Select Instrument Date!!!!");
					dpInstrumentDate.setDisable(false);
					dpInstrumentDate.requestFocus();
					return;
				} else {
					receipt.setInstrumentDate(Date.valueOf(dpInstrumentDate.getValue()));
					receipt.setInstnumber(txtInstrumentNumber.getText());
					receipt.setDepositBank(cmbDepositBank.getSelectionModel().getSelectedItem());
					receipt.setRemark(txtRemark.getText());
					receipt.setTransDate(Date.valueOf(dpTransDate.getValue()));				
					receipt.setModeOfpayment(cmbModeofReceipt.getSelectionModel().getSelectedItem());
					receipt.setReceipthdr(receipthdr);
					ResponseEntity<AccountHeads > accountHeadsResp = RestCaller.getAccountHeadByName(receipt.getDepositBank());	
					if(null==accountHeadsResp.getBody()) {
						
						notifyMessage(5, " Bank account not set");
						return;
					}					
					receipt.setDebitAccountId(accountHeadsResp.getBody().getId());	
					ResponseEntity<ReceiptDtl> respentity = RestCaller.saveReceiptDtl(receipt);
					receipt = respentity.getBody();
					 
					setTotal();
					receipt.setAccountName(txtAccount.getText());
					receiptList1.add(receipt);
					filltable();
				}

			}
		}
		txtAccount.requestFocus();
		clearfields();
	}
	private void setTotal() {

		totalamount = RestCaller.getSummaryReceiptTotal(receipthdr.getId());
		txtTotal.setText(Double.toString(totalamount));
	}
	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();

		notificationBuilder.show();
	}
}
