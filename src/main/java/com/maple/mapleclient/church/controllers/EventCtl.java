
package com.maple.mapleclient.church.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.EventMst;
import com.maple.mapleclient.entity.OrgMst;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class EventCtl {

	 EventBus eventBus = EventBusFactory.getEventBus();
	 EventMst eventMst=null;
	   private ObservableList<EventMst> EventList = FXCollections.observableArrayList();
	    @FXML
	    private Button btnGenerateReport;

	    @FXML
	    private TextField txteventid;

	    @FXML
	    private TextField txtincome;

	    @FXML
	    private TextField txtorgid;

	    @FXML
	    private TextField txteventname;

	    @FXML
	    private TextField txtvenue;

	    @FXML
	    private TextField txtexpense;

	    @FXML
	    private DatePicker txtstartdate;

	    @FXML
	    private DatePicker txtenddate;
	    @FXML
	    private Button btndelete;
	    @FXML
	    private Button btnshowall;
	    @FXML
	    private Button btneventmsdid;

	    @FXML
	    private TextField txteventmsdid;

	    @FXML
	    private TextField txtorgname;

	    @FXML
	    private TableView<EventMst> table3;

	    @FXML
	    private TableColumn<EventMst,String> eventname;

	    @FXML
	    private TableColumn<EventMst, String> eventvenue;

	    @FXML
	    private TableColumn<EventMst, String> evstartdate;

	    @FXML
	    private TableColumn<EventMst, String> evenddate;

	    @FXML
	    private TableColumn<EventMst, Number> evincome;

	    @FXML
	    private TableColumn<EventMst, Number> evexpense;
	    
	    
	    @FXML
		private void initialize() 
	    {
	    	
		    	eventBus.register(this);
		  	  table3.getSelectionModel().selectedItemProperty().addListener((obs,
					  oldSelection, newSelection) -> { if (newSelection != null) {
					  System.out.println("getSelectionModel"); if (null != newSelection.getId())
					  {
					  
						  eventMst = new EventMst(); 
						  eventMst.setId(newSelection.getId());}}
					  
					  });
		  	  
		  	  
		  	txtexpense.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtexpense.setText(oldValue);
					}
				}
			});
		  	
			txtincome.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					if (!newValue.matches("\\d{0,10}([\\.]\\d{0,3})?")) {
						txtincome.setText(oldValue);
					}
				}
			});
					 
	    }
	    
	    @FXML
	    void actionOrgpop(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    		
	    		 loadOrgPopup();
	    		
	    		
	    	}
	    }
	    @FXML
	    void ActionMsdid(ActionEvent event) {
	    	
	    	String orgId = RestCaller.getVoucherNumber("EVN");
	    	txteventmsdid.setText(orgId);


	    }
	    @FXML
	    void actionorg(KeyEvent event) {
	    	
	     if (event.getCode() == KeyCode.ENTER) {
				
	    	 txtorgname.requestFocus();
		    	}
	    }

	  
	    @FXML
	    void Showall(ActionEvent event) {
	    	showAll();

	    }


	    @FXML
	    void Save(ActionEvent event) {
	    	
	    	if(txteventname.getText().trim().isEmpty())
	    	{
		    	notifyMessage(5, "Enter event name");
		    	txteventname.requestFocus();
		    	return;
	    	}
	    	if(txtorgname.getText().trim().isEmpty())
	    	{
		    	notifyMessage(5, "Select organization name");
		    	txtorgname.requestFocus();
		    	return;
	    	}
	    	
	    	
	    	if(txtvenue.getText().trim().isEmpty())
	    	{
		    	notifyMessage(5, "Please enter venue");
		    	txtvenue.requestFocus();
		    	return;
	    	}
	    	
	    	if(null == txtstartdate.getValue())
	    	{
		    	notifyMessage(5, "Please select event start date");
		    	txtstartdate.requestFocus();
		    	return;
	    	}
	    	if(null == txtenddate.getValue())
	    	{
		    	notifyMessage(5, "Please select event end date");
		    	txtenddate.requestFocus();
		    	return;
	    	}
	    	if(txtincome.getText().trim().isEmpty())
	    	{
		    	notifyMessage(5, "Please enter income");
		    	txtincome.requestFocus();
		    	return;
	    	}
	    	if(txtexpense.getText().trim().isEmpty())
	    	{
		    	notifyMessage(5, "Please enter expense");
		    	txtexpense.requestFocus();
		    	return;
	    	}
	    	EventMst eventMst=new EventMst();
	    	
	    	String evnId = RestCaller.getVoucherNumber("EVN");
	    	eventMst.setEventId(evnId);
	    	eventMst.setOrgId(txtorgname.getText());
	   
	    	eventMst.setEventName(txteventname.getText());
	    	eventMst.setEventVenue(txtvenue.getText());
	    	eventMst.setEventStartDate(Date.valueOf(txtstartdate.getValue()));
	    	eventMst.setEventEndDate(Date.valueOf(txtenddate.getValue()));
	    	eventMst.setTotalIncome(Double.parseDouble(txtincome.getText()));
	    	eventMst.setTotalExpense(Double.parseDouble(txtexpense.getText()));
	    	ResponseEntity<EventMst> respentity = RestCaller.EventCreation(eventMst);
	    	eventMst = respentity.getBody();
	    	EventList.add(eventMst);
	    	notifyMessage(5, "Save Successfully Completed");
	    	FillTable();
	    	txteventname.setText("");
	    	txtvenue.setText("");
	    	txtstartdate.setValue(null);
	    	txtenddate.setValue(null);
	    	txtincome.setText("");
	    	txtexpense.setText("");
	    	txtorgname.setText("");

	    }
	    
	    @FXML
	    void Delete(ActionEvent event) {
	    	  if(null != eventMst) {
	  			if(null != eventMst.getId()) {
	  			  RestCaller.deleteEventMst(eventMst.getId());
	  			
	  			  notifyMessage(5,"Deleted!!!");
	  			  
	  			  table3.getItems().clear(); 
	  			  showAll();
	  			  
	  			  } }


	    }

	    @FXML
	    void actionLocation(KeyEvent event) {

	    }

	    @FXML
	    void actionOrgID(KeyEvent event) {

	    }

	    @FXML
	    void actionsave(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				
	    		btnGenerateReport.requestFocus();
		    	}

	    }

	    @FXML
	    void actionIncome(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtstartdate.requestFocus();
		    	}


	    }

	    @FXML
	    void actionOneventName(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtenddate.requestFocus();
		    	}


	    }

	  

	    @FXML
	    void actionenddate(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtvenue.requestFocus();
		    	}

	    }

	    @FXML
	    void actioneventvenue(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtincome.requestFocus();
		    	}
	    	

	    }

	    @FXML
	    void actionorgid(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtexpense.requestFocus();
		    	}
	    	

	    }
      private void showAll() {
	    	
	    	ResponseEntity<List<EventMst>> EventMstResp = RestCaller.getAllEvent();
	    	EventList = FXCollections.observableArrayList(EventMstResp.getBody());
	    	table3.getItems().clear();
			FillTable();

			
		}

		private void FillTable() {

			table3.setItems(EventList);
			eventname.setCellValueFactory(cellData -> cellData.getValue().getEventNameProperty());
			eventvenue.setCellValueFactory(cellData -> cellData.getValue().getEventVenueProperty());
			evstartdate.setCellValueFactory(cellData -> cellData.getValue().getEventIdProperty());
			evenddate.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());
			
			evincome.setCellValueFactory(cellData -> cellData.getValue().getTotalIncomeProperty());
			evexpense.setCellValueFactory(cellData -> cellData.getValue().getTotalExpenseProperty());
			
			
		}
		  public void notifyMessage(int duration, String msg) {
				System.out.println("OK Event Receid");

				Image img = new Image("done.png");
				Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
						.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
						.onAction(new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								System.out.println("clicked on notification");
							}
						});
				notificationBuilder.darkStyle();
				notificationBuilder.show();
			}


		    @FXML
		    void actioneventEndDate(KeyEvent event) {
   if (event.getCode() == KeyCode.ENTER) {
	
	 txtexpense.requestFocus();
	}

		    }
		  
		    private void loadOrgPopup() {
				
				try {
					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/OrganisationIdpopup.fxml"));
					Parent root1;

					root1 = (Parent) fxmlLoader.load();
					Stage stage = new Stage();

					stage.initModality(Modality.APPLICATION_MODAL);
					stage.initStyle(StageStyle.UNDECORATED);
					stage.setTitle("ABC");
					stage.initModality(Modality.APPLICATION_MODAL);
					stage.setScene(new Scene(root1));
					stage.show();
					txtstartdate.requestFocus();
				

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		    
		    @Subscribe
			public void popupOrglistner(OrgEvent orgEvent) {

				Stage stage = (Stage) btnGenerateReport.getScene().getWindow();
				if (stage.isShowing()) {

					
					txtorgname.setText(orgEvent.getOrgId());
			
					

				}

			}

	    }

	



