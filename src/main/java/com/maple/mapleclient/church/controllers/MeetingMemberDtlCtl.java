package com.maple.mapleclient.church.controllers;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MeetingMemberDtl;
import com.maple.mapleclient.entity.PasterDtl;
import com.maple.mapleclient.events.MeetingEvent;
import com.maple.mapleclient.events.MemberMstEvent;
import com.maple.mapleclient.restService.RestCaller;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
public class MeetingMemberDtlCtl {
	MeetingMemberDtl meetingMemberDtl;
	private EventBus eventBus = EventBusFactory.getEventBus();
	 private ObservableList<MeetingMemberDtl> meetingMemberDtlList = FXCollections.observableArrayList();
    @FXML
    private Button btnSave;
 
    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnDelete;



    @FXML
    private TextField txtMeetingId;

    @FXML
    private TextField txtMemberId;

    @FXML
    private TableView<MeetingMemberDtl> tblMeetingMember;

    @FXML
    private TableColumn<MeetingMemberDtl, String> clMMid;

    @FXML
    private TableColumn<MeetingMemberDtl, String> clMeetingId;

    @FXML
    private TableColumn<MeetingMemberDtl, String> clMemberId;

    @FXML
    void ActionShowAll(ActionEvent event) {
  	showAll();
    }
 private void showAll() {
    	
    	ResponseEntity<List<MeetingMemberDtl>> meetingMemDtlResp = RestCaller.getAllMeetingMemberDtl();
    	meetingMemberDtlList = FXCollections.observableArrayList(meetingMemDtlResp.getBody());
    	tblMeetingMember.getItems().clear();
    	FillTable();

 
    }  
    @FXML
    void actionDelete(ActionEvent event) {
    	if(null != meetingMemberDtl) {
    		if(null != meetingMemberDtl.getId()) {
    		  RestCaller.deletePasterDtl(meetingMemberDtl.getId());
    		  notifyMessage(5,"Deleted!!!");
    		  
    		  tblMeetingMember.getItems().clear();
    		  showAll();
    		  
    		  } }

    }
    private void loadMemberPopup() {
    	
    	try {
    		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MemberMstPopUp.fxml"));
    		Parent root1;

    		root1 = (Parent) fxmlLoader.load();
    		Stage stage = new Stage();

    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.initStyle(StageStyle.UNDECORATED);
    		stage.setTitle("ABC");
    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.setScene(new Scene(root1));
    		stage.show();
    		btnSave.requestFocus();
    	

    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}

    }

    @Subscribe
    	public void popupMemberlistner(MemberMstEvent memberMstEvent) {

    		Stage stage = (Stage) btnDelete.getScene().getWindow();
    		if (stage.isShowing()) {
    			txtMemberId.setText(memberMstEvent.getMemberId());	
    		}
    	}   
        
    @FXML
    void loadPopup(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    	loadMemberPopup();
    	}
    }
    @FXML
    void loadPopup2(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		loadMeetingPopup();
    	}
    }
private void loadMeetingPopup() {
    	
    	try {
    		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MeetingPopup.fxml"));
    		Parent root1;

    		root1 = (Parent) fxmlLoader.load();
    		Stage stage = new Stage();

    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.initStyle(StageStyle.UNDECORATED);
    		stage.setTitle("ABC");
    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.setScene(new Scene(root1));
    		stage.show();
    		txtMemberId.requestFocus();
    	

    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}

    }

    @Subscribe
    	public void popupMeetinglistner(MeetingEvent meetingEvent) {

    		Stage stage = (Stage) btnDelete.getScene().getWindow();
    		if (stage.isShowing()) {
    			txtMeetingId.setText(meetingEvent.getMeetingId());
    		}

    	}   
        
        
  
    @FXML
    void actionSave(ActionEvent event) {

    	if(txtMemberId.getText().isEmpty()) {
    		
    		
    		notifyMessage(5, "Enter Member Id");
    		return;
    	}if(txtMeetingId.getText().isEmpty()) {
    		
    		notifyMessage(5, "Enter Meeting Id");
    		return;
    	}
    	meetingMemberDtl = new MeetingMemberDtl();
    	String PastId = RestCaller.getVoucherNumber("MM");
    	meetingMemberDtl.setMmid(PastId);
    	meetingMemberDtl.setMeetingId(txtMeetingId.getText());
    	meetingMemberDtl.setMemberId(txtMemberId.getText());

    	ResponseEntity<MeetingMemberDtl> respentity = RestCaller.saveMeetingMemberDtl(meetingMemberDtl);
    	System.out.println("meeting mst111111111 ......" );
    	meetingMemberDtl = respentity.getBody();
    	System.out.println("meeting mst1111111112222222222 ......" );
    	meetingMemberDtlList.add(meetingMemberDtl);
		System.out.println("saved paster dtlssss" + meetingMemberDtl);
    	
		notifyMessage(5, "Save Successfully Completed");
	 FillTable();
       txtMemberId.setText("");
   	txtMeetingId.setText("");

//   
   	
    }
    @FXML
   	private void initialize() {
   		System.out.println("ssssssss=====initialize=====sssssssssss");
   		eventBus.register(this);
//   		tblMeetingMember.getSelectionModel().selectedItemProperty().addListener((obs,
//  			  oldSelection, newSelection) -> { if (newSelection != null) {
//  			  System.out.println("getSelectionModel"); if (null != newSelection.getId())
//  			  {
//  			  
//  				meetingMemberDtl = new MeetingMemberDtl(); 
//  				meetingMemberDtl.setId(newSelection.getId()); } }
//  			  });
  		
   	}
    private void FillTable() {

		tblMeetingMember.setItems(meetingMemberDtlList);
		System.out.println("table fillingggggggggggg2222222");
	
		clMemberId.setCellValueFactory(cellData -> cellData.getValue().getMemberIdProperty());
		clMeetingId.setCellValueFactory(cellData -> cellData.getValue().getMeetingIdProperty());
		clMMid.setCellValueFactory(cellData -> cellData.getValue().getMmidProperty());
		
		System.out.println("table fillingggggggggggg");
		
	}
    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
}
