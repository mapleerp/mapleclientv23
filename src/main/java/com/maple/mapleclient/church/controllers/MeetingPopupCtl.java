


package com.maple.mapleclient.church.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MeetingMst;
import com.maple.mapleclient.entity.OrgMst;
import com.maple.mapleclient.events.MeetingEvent;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class MeetingPopupCtl {
	MeetingEvent meetingEvent;
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<MeetingMst> meetingList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();


	    @FXML
	    private TextField txtname;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TableView<MeetingMst> tablePopupView;

	    @FXML
	    private TableColumn<MeetingMst,String> tblmeetingid;

	    @FXML
	    private TableColumn<MeetingMst,String> custName;

	    @FXML
	    private Button btnCancel;
	    
	    
	    @FXML
		private void initialize() {


			txtname.textProperty().bindBidirectional(SearchString);
			
			MeetingBySearch("");
			
			meetingEvent = new MeetingEvent();
			eventBus.register(this);
			btnSubmit.setDefaultButton(true);
			tablePopupView.setItems(meetingList);

			btnCancel.setCache(true);

			
			tblmeetingid.setCellValueFactory(cellData -> cellData.getValue().getMeetingIdProperty());

			tablePopupView.setItems(meetingList);
			

			tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getMeetingId() && newSelection.getMeetingId().length() > 0) {
						meetingEvent.setMeetingId(newSelection.getMeetingId());
						
						eventBus.post(meetingEvent);
					}
				}
			});

		
			SearchString.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

					//OrgBySearch((String) newValue);
				}
			});

		}

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB) {

			} else {
				txtname.requestFocus();
			}


	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tablePopupView.requestFocus();
				tablePopupView.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			}

	    }

	    @FXML
	    void onCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();


	    }

	    @FXML
	    void submit(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();

	    }
	    
	    
	    private void MeetingBySearch(String searchData) {

			
			ArrayList meetingMst = new ArrayList();
			MeetingMst cust = new MeetingMst();
			RestTemplate restTemplate = new RestTemplate();

			meetingMst = RestCaller.SearchMeetingByName(searchData);

			Iterator itr = meetingMst.iterator();
			while (itr.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				

				Object meetid = lm.get("meetingId");
				Object orgId = lm.get("orgId");
			
				Object id = lm.get("id");
				if (null != id) {
					cust = new MeetingMst();
					cust.setOrgId((String) orgId);
					cust.setMeetingId((String) meetid);
					cust.setId((String) id);
					
					
				
					

					System.out.println(lm);

					meetingList.add(cust);
				}

			}
			
			

			return;

		}

	}

	
	

