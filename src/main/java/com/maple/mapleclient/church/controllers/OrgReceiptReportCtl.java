package com.maple.mapleclient.church.controllers;

import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.OrgReceiptExport;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.OrgReceiptReport;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;

public class OrgReceiptReportCtl {
	 
	OrgReceiptExport orgReceiptExport = new OrgReceiptExport();
	EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<OrgReceiptReport> receiptList = FXCollections.observableArrayList();

	@FXML
	private DatePicker dpFromDate;
	String voucherNumber = "";
	java.util.Date voucherDate;
	String memberName = "";
	String strDate = "";
	@FXML
	private TableView<OrgReceiptReport> tbReport;
	
List<OrgReceiptReport> orgReceiptList =new ArrayList<OrgReceiptReport>();
	String accountId;
	@FXML
	private TableColumn<OrgReceiptReport, String> clVoucherDate;

	@FXML
	private TableColumn<OrgReceiptReport, String> clVoucherNumber;

	@FXML
	private TableColumn<OrgReceiptReport, String> clumnVoucherType;

	@FXML
	private TableColumn<OrgReceiptReport, String> clmnMember;

	@FXML
	private TableColumn<OrgReceiptReport, Number> clmnAmount;

	@FXML
	private Button btnShow;

	@FXML
	private ComboBox<String> branchselect;

	@FXML
	private Button ok;
	


    @FXML
    private Button btnExcelExport;
	@FXML
	private Button btnPrint;

	@FXML
	private DatePicker dpToDate;
	@FXML
	private TextField txtTotalAmount;



    @FXML
    private TextField txtAccount;

    @FXML
    private Button btnDelete;

    

    @FXML
    void exportToExcel(ActionEvent event) {
    	
    	orgReceiptList.clear();
    	tbReport.getItems().clear();
		// tbreports.getItems().clear();
		// totalamount = 0.0;
		// grandTotal = 0.0;

		if (null == branchselect.getValue()) {
			notifyMessage(5, "Please select branch", false);
			return;
		}

		
		
		
		
		
		
		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "Please select from date", false);
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(5, "Please select to date", false);
			return;
		}
		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		
		if(txtAccount.getText().isEmpty()) {
		ResponseEntity<List<OrgReceiptReport>> receiptHdrRep = RestCaller.getOrgReceiptReport(startDate, endDate,
				branchselect.getValue());
		receiptList = FXCollections.observableArrayList(receiptHdrRep.getBody());
	//	orgReceiptList=receiptHdrRep.getBody();
		FillTables();
		for(int i=0;i<receiptList.size();i++) {
			OrgReceiptReport orgReceiptReport=new OrgReceiptReport();
			orgReceiptReport.setVoucherNumber(receiptList.get(i).getVoucherNumber());
			orgReceiptReport.setAmount(receiptList.get(i).getAmount());
			orgReceiptReport.setMemberNmae(receiptList.get(i).getMemberNmae());
			orgReceiptReport.setVoucherDate(receiptList.get(i).getVoucherDate());
			orgReceiptReport.setVoucherType(receiptList.get(i).getVoucherType());
			orgReceiptList.add(orgReceiptReport);
		}
		
		Double totalReceiptAmount=0.0;
		
		for(int i=0;i<receiptList.size();i++) {
			
			Double receiptAmount=receiptList.get(i).getAmount();
			
	
			totalReceiptAmount=receiptAmount+totalReceiptAmount;
			
			txtTotalAmount.setText(String.valueOf(totalReceiptAmount));
		}
		}else {
			
			
			
			ResponseEntity<List<OrgReceiptReport>> receiptHdrRep = RestCaller.getOrgReceiptReportAccountWise(startDate, endDate,
					branchselect.getValue(),accountId);
			receiptList = FXCollections.observableArrayList(receiptHdrRep.getBody());
			//orgReceiptList=receiptHdrRep.getBody();
			FillTables();
			
			for(int i=0;i<receiptList.size();i++) {
				OrgReceiptReport orgReceiptReport=new OrgReceiptReport();
				orgReceiptReport.setVoucherNumber(receiptList.get(i).getVoucherNumber());
				orgReceiptReport.setAmount(receiptList.get(i).getAmount());
				orgReceiptReport.setMemberNmae(receiptList.get(i).getMemberNmae());
				orgReceiptReport.setVoucherDate(receiptList.get(i).getVoucherDate());
				orgReceiptReport.setVoucherType(receiptList.get(i).getVoucherType());
				orgReceiptList.add(orgReceiptReport);
			}
			Double totalReceiptAmount=0.0;
			
			for(int i=0;i<receiptList.size();i++) {
				
				Double receiptAmount=receiptList.get(i).getAmount();
				
		
				totalReceiptAmount=receiptAmount+totalReceiptAmount;
				
				txtTotalAmount.setText(String.valueOf(totalReceiptAmount));
				
			
		}}
    	System.out.print(orgReceiptList.size()+" inside export  org receipt list sze isssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		orgReceiptExport.exportToExcel("ReceiptExport" + branchselect.getSelectionModel().getSelectedItem() + strDate  + ".xls", orgReceiptList);
    }


	@FXML
	void onClickOk(ActionEvent event) {

	}

	@FXML
	void reportPrint(ActionEvent event) {

		if (voucherNumber.isEmpty()) {
			notifyMessage(5, "Please select a row ", false);
			return;
		} else {

			ResponseEntity<List<MemberMst>> resp = RestCaller.findByMemberName(memberName);

			String memberId = resp.getBody().get(0).getMemberId();

			try {

				JasperPdfReportService.OrgReceiptInvoiceReprint(voucherNumber,strDate , memberId);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@FXML
	private void initialize() {

		setBranches();
		eventBus.register(this);
		tbReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				System.out.println("getSelectionModel");
				if (null != newSelection.getVoucherNumber()) {
					System.out.println("getSelectionModel--getVoucherNumber");
					String voucher = newSelection.getVoucherNumber();
					System.out.println(voucher);
					memberName = newSelection.getMemberNmae();

					System.out.print(memberName
							+ "member name issssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");

					Format formatter;
					formatter = new SimpleDateFormat("yyyy-MM-dd");

					voucherNumber = newSelection.getVoucherNumber();
					voucherDate = newSelection.getVoucherDate();
					strDate = SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd");

					FillTables();
				}

			}
		});

	}

	private void setBranches() {

		ResponseEntity<List<BranchMst>> branchMstRep = RestCaller.getBranchMst();
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();
		branchMstList = branchMstRep.getBody();

		for (BranchMst branchMst : branchMstList) {
			branchselect.getItems().add(branchMst.getBranchCode());

		}

	}

	private void FillTables() {
		tbReport.setItems(receiptList);

		clVoucherDate.setCellValueFactory(cellData -> cellData.getValue().getVoucherDateProperty());
		clVoucherNumber.setCellValueFactory(cellData -> cellData.getValue().getVoucherNumberProperty());
		clmnMember.setCellValueFactory(cellData -> cellData.getValue().getMemberNmaeProperty());
		clumnVoucherType.setCellValueFactory(cellData -> cellData.getValue().getVoucherTypeProperty());
		clmnAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}
	}

	@FXML
	void show(ActionEvent event) {

		tbReport.getItems().clear();
		// tbreports.getItems().clear();
		// totalamount = 0.0;
		// grandTotal = 0.0;

		if (null == branchselect.getValue()) {
			notifyMessage(5, "Please select branch", false);
			return;
		}

		
		
		
		
		
		
		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "Please select from date", false);
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(5, "Please select to date", false);
			return;
		}
		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		
		if(txtAccount.getText().isEmpty()) {
		ResponseEntity<List<OrgReceiptReport>> receiptHdrRep = RestCaller.getOrgReceiptReport(startDate, endDate,
				branchselect.getValue());
		receiptList = FXCollections.observableArrayList(receiptHdrRep.getBody());
		orgReceiptList=receiptHdrRep.getBody();
		
		
		FillTables();
		Double totalReceiptAmount=0.0;
		
		for(int i=0;i<receiptList.size();i++) {
			
			Double receiptAmount=receiptList.get(i).getAmount();
			
	
			totalReceiptAmount=receiptAmount+totalReceiptAmount;
			
			txtTotalAmount.setText(String.valueOf(totalReceiptAmount));
		}
		}else {
			
			
			
			ResponseEntity<List<OrgReceiptReport>> receiptHdrRep = RestCaller.getOrgReceiptReportAccountWise(startDate, endDate,
					branchselect.getValue(),accountId);
			receiptList = FXCollections.observableArrayList(receiptHdrRep.getBody());
			orgReceiptList=receiptHdrRep.getBody();
			
			
			FillTables();
			Double totalReceiptAmount=0.0;
			
			for(int i=0;i<receiptList.size();i++) {
				
				Double receiptAmount=receiptList.get(i).getAmount();
				
		
				totalReceiptAmount=receiptAmount+totalReceiptAmount;
				
				txtTotalAmount.setText(String.valueOf(totalReceiptAmount));
			
		}}
	}
	
	
	private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		//	btnGenerateReport.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) txtAccount.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountEvent.getAccountName());
			accountId = accountEvent.getAccountId();

		}

	}

    @FXML
    void onClickAccount(ActionEvent event) {

    	showPopup() ;
    	
    }

    @FXML
    void deleteAction(ActionEvent event) {


		if (null != voucherNumber) {
			if (null != voucherNumber) {
				
				RestCaller.orgReceiptDelete(voucherNumber);
				notifyMessage(3, "receipt deleted", false);
				show();
			}
		}else {
			notifyMessage(3, "plz select recept entry", false);
		}
		
		MapleclientApplication.mainFrameController.initialize(null, null);
    }
    
    
    
    @FXML
    void onMouseClickedOnAccount(MouseEvent event) {
    	showPopup() ;
    }
    
    
    void show() {
    	tbReport.getItems().clear();
		// tbreports.getItems().clear();
		// totalamount = 0.0;
		// grandTotal = 0.0;

		if (null == branchselect.getValue()) {
			notifyMessage(5, "Please select branch", false);
			return;
		}

		
		
		
		
		
		
		if (null == dpFromDate.getValue()) {
			notifyMessage(5, "Please select from date", false);
			return;
		}

		if (null == dpToDate.getValue()) {
			notifyMessage(5, "Please select to date", false);
			return;
		}
		java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
		String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date toDate = Date.valueOf(dpToDate.getValue());
		String endDate = SystemSetting.UtilDateToString(toDate, "yyy-MM-dd");
		
		if(txtAccount.getText().isEmpty()) {
		ResponseEntity<List<OrgReceiptReport>> receiptHdrRep = RestCaller.getOrgReceiptReport(startDate, endDate,
				branchselect.getValue());
		receiptList = FXCollections.observableArrayList(receiptHdrRep.getBody());

		FillTables();
		Double totalReceiptAmount=0.0;
		
		for(int i=0;i<receiptList.size();i++) {
			
			Double receiptAmount=receiptList.get(i).getAmount();
			
	
			totalReceiptAmount=receiptAmount+totalReceiptAmount;
			
			txtTotalAmount.setText(String.valueOf(totalReceiptAmount));
		}
		}else {
			
			
			
			ResponseEntity<List<OrgReceiptReport>> receiptHdrRep = RestCaller.getOrgReceiptReportAccountWise(startDate, endDate,
					branchselect.getValue(),accountId);
			receiptList = FXCollections.observableArrayList(receiptHdrRep.getBody());

			FillTables();
			Double totalReceiptAmount=0.0;
			
			for(int i=0;i<receiptList.size();i++) {
				
				Double receiptAmount=receiptList.get(i).getAmount();
				
		
				totalReceiptAmount=receiptAmount+totalReceiptAmount;
				
				txtTotalAmount.setText(String.valueOf(totalReceiptAmount));
			
		}}
    }
}
