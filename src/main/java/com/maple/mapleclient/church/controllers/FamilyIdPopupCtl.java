package com.maple.mapleclient.church.controllers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.FamilyMst;

import com.maple.mapleclient.events.FamilyEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
public class FamilyIdPopupCtl {


	FamilyEvent familyEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<FamilyMst> familyList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();


	    @FXML
	    private TextField txtname;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TableView<FamilyMst> tablePopupView;

	    @FXML
	    private TableColumn<FamilyMst,String> custAddress;

	    @FXML
	    private TableColumn<FamilyMst,String> custName;
	    @FXML
	    private TableColumn<FamilyMst, String> custAddress1;

	    @FXML
	    private Button btnCancel;

	    
	    
	    @FXML
	  		private void initialize() {


	  			txtname.textProperty().bindBidirectional(SearchString);
	  			
	  			FamilyBySearch("");
	  			
	  			familyEvent = new FamilyEvent();
	  			eventBus.register(this);
	  			btnSubmit.setDefaultButton(true);
	  			tablePopupView.setItems(familyList);

	  			btnCancel.setCache(true);

	  			custName.setCellValueFactory(cellData -> cellData.getValue().getFamilyMstIdProperty());
	  			custAddress.setCellValueFactory(cellData -> cellData.getValue().getFamilyNameProperty());
	  			custAddress1.setCellValueFactory(cellData -> cellData.getValue().getHeadOfFamilyProperty());
	  			tablePopupView.setItems(familyList);
	  			for (FamilyMst s : familyList) {
	  				
	  			}

	  			tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	  				if (newSelection != null) {
	  					if (null != newSelection.getFamilyName() && newSelection.getFamilyName().length() > 0) {
	  						familyEvent.setFamilyName(newSelection.getFamilyName());
	  						familyEvent.setFamilyMstId(newSelection.getFamilyMstId());
	  						familyEvent.setHeadOfFamily(newSelection.getHeadOfFamily());
	  						familyEvent.setOrgId(newSelection.getOrgId());
	  						
	  						eventBus.post(familyEvent);
	  					}
	  				}
	  			});

	  		
	  			SearchString.addListener(new ChangeListener() {
	  				@Override
	  				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

	  					FamilyBySearch((String) newValue);
	  				}
	  			});

	  		}

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB) {

			} else {
				txtname.requestFocus();
			}


	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tablePopupView.requestFocus();
				tablePopupView.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			}

	    }

	    @FXML
	    void onCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();


	    }

	    @FXML
	    void submit(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();


	    }
	    private void FamilyBySearch(String searchData) {

	    	familyList.clear();
			ArrayList familyMst = new ArrayList();
			FamilyMst cust = new FamilyMst();
			RestTemplate restTemplate = new RestTemplate();

			familyMst = RestCaller.SearchFamilyByNames( searchData);

			Iterator itr = familyMst.iterator();
			while (itr.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				
				Object famMstId = lm.get("familyMstId");
			
				Object fm=lm.get("familyName");
				Object headofFamily=lm.get("headOfFamily");
				Object orgId=lm.get("orgId");
				Object id = lm.get("id");
				if (null != id) {
					cust = new FamilyMst();
					cust.setFamilyMstId((String) famMstId);
					cust.setFamilyName((String) fm);
					cust.setHeadOfFamily((String) headofFamily);
					cust.setOrgId((String) orgId );
					cust.setId((String) id );
					
					
				
					

					System.out.println(lm);

					familyList.add(cust);
				}

			}
			return;
	    }

}


	
	

