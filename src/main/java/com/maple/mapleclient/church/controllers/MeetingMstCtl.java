
package com.maple.mapleclient.church.controllers;


import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.DamageDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MeetingMst;
import com.maple.mapleclient.entity.PasterDtl;
import com.maple.mapleclient.entity.ReceiptInvoiceDtl;
import com.maple.mapleclient.entity.SubGroupMember;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.events.SubGroupMemberEvent;
import com.maple.mapleclient.restService.RestCaller;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.scene.input.KeyCode;


import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.collections.FXCollections;
public class MeetingMstCtl {
	
	
	private EventBus eventBus = EventBusFactory.getEventBus();
	 private ObservableList<MeetingMst> meetingMstList = FXCollections.observableArrayList();
	MeetingMst meetingMst=null;
    @FXML
    private TextField txtOrgId;

    @FXML
    private DatePicker dpMeetingDate;

    @FXML
    private TextField txtChairPerson;

    @FXML
    private TextField txtSubject;

    @FXML
    private TextField txtMinutesBy;


    @FXML
    private TextField txtsubGroup;

    @FXML
    private TableView<MeetingMst> tblMeetingMst;

    @FXML
    private TableColumn<MeetingMst, String> clOrgId;

    @FXML
    private TableColumn<MeetingMst, LocalDate> clMeetingDate;

    @FXML
    private TableColumn<MeetingMst, String> clChairPerson;

    @FXML
    private TableColumn<MeetingMst, String> clSubject;

    @FXML
    private TableColumn<MeetingMst, String> clMinutesBy;

    @FXML
    private TableColumn<MeetingMst, String> clMeetingId;

    @FXML
    private TableColumn<MeetingMst, String> clSubgroup;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnShowAll;

    @FXML
    private Button btnDelete;

    @FXML
    void ActionDelete(ActionEvent event) {

		  if(null != meetingMst) {
    	if (null != meetingMst.getId()) {
			RestCaller.deleteMeetingMst(meetingMst.getId());
			notifyMessage(5, " Deleted Successfully");
			tblMeetingMst.getItems().clear();

			
			 showAll();
	}}}

  
    @FXML
    void actionSubGroupId(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    	loadSubGroupPopup();
    	}
    }
    @FXML
    void ActionShowAll(ActionEvent event) {
           showAll();
    }

    @FXML
    void ActionSave(ActionEvent event) {
    	
    	if(txtOrgId.getText().isEmpty()) {
    		   notifyMessage(5, "Enter org id");
    		   
    		   return;
    	}
    	if(txtChairPerson.getText().isEmpty()) {
         notifyMessage(5, "Enter ChairPerson");
    		   return;
    	}
    	if(txtSubject.getText().isEmpty()) {
    		
    		 notifyMessage(5, "Enter Subject");
  		   return;
    	}
    	if(txtMinutesBy.getText().isEmpty()) {
    		
    		 notifyMessage(5, "Enter Mnutes By");
    		   return;
    	}
    	if(txtsubGroup.getText().isEmpty()) {
    		
    		 notifyMessage(5, "Enter SubGroup");
  		   return;
    		
    	}
    	
    	
    	meetingMst= new MeetingMst();
    	meetingMst.setOrgId(txtOrgId.getText());
   	    meetingMst.setmeetingDate(Date.valueOf(dpMeetingDate.getValue()));
    	meetingMst.setChairPerson(txtChairPerson.getText());
    	meetingMst.setSubject(txtSubject.getText());
    	meetingMst.setMinutesBy(txtMinutesBy.getText());
    	String MeetId = RestCaller.getVoucherNumber("MEET");
    	meetingMst.setMeetingId(MeetId);
    	meetingMst.setSubGroup(txtsubGroup.getText());
    	ResponseEntity<MeetingMst> respentity = RestCaller.saveMeetingMst(meetingMst);
    	meetingMst = respentity.getBody();
		System.out.println("saved meeting mst" + meetingMst);
		meetingMstList.add(meetingMst);
   	    notifyMessage(5, "Save Successfully Completed");
    	 FillTable();
  
    	txtChairPerson.setText("");
    	txtMinutesBy.setText("");
    	txtOrgId.setText("");
    	txtSubject.setText("");
    	txtsubGroup.setText("");
    	dpMeetingDate.setValue(null);

    }
  
    private void FillTable() {

		tblMeetingMst.setItems(meetingMstList);
		System.out.println("table fil");
	    clMeetingDate.setCellValueFactory(cellData -> cellData.getValue().getmeetingDateProperty());
		clMeetingId.setCellValueFactory(cellData -> cellData.getValue().getMeetingIdProperty());
		clChairPerson.setCellValueFactory(cellData -> cellData.getValue().getChairPersonProperty());
		clSubgroup.setCellValueFactory(cellData -> cellData.getValue().getSubGroupProperty());
		clSubject.setCellValueFactory(cellData -> cellData.getValue().getSubjectProperty());
		clMinutesBy.setCellValueFactory(cellData -> cellData.getValue().getMinutesByProperty());
		clOrgId.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());
		System.out.println("table fillingggggggggggg");
		
	}
 private void showAll() {
    	
    	ResponseEntity<List<MeetingMst>> pasterDtlResp = RestCaller.getAllMeetingMst();
    	meetingMstList = FXCollections.observableArrayList(pasterDtlResp.getBody());
    	tblMeetingMst.getItems().clear();
    	FillTable();

    	
    } 
    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");
		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    private void loadOrgPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/OrganisationIdpopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			dpMeetingDate.requestFocus();
		

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
@Subscribe
public void popupOrglistner(OrgEvent orgEvent) {

	Stage stage = (Stage) btnDelete.getScene().getWindow();
	if (stage.isShowing()) {
		txtOrgId.setText(orgEvent.getOrgId());
	}
}

private void loadSubGroupPopup() {
	
	try {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SubGroupPopup.fxml"));
		Parent root1;

		root1 = (Parent) fxmlLoader.load();
		Stage stage = new Stage();

		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setTitle("ABC");
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setScene(new Scene(root1));
		stage.show();
		txtChairPerson.requestFocus();
	

	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
@Subscribe
public void popupSubGrouplistner(SubGroupMemberEvent subGroupMemberEvent) {

	Stage stage = (Stage) btnDelete.getScene().getWindow();
	if (stage.isShowing()) {
		txtsubGroup.setText(subGroupMemberEvent.getSubGroupId());
		System.out.println("=============+++++++++"+subGroupMemberEvent.getSubGroupId());

	}
}


@FXML
void OnKeyPressOrg(KeyEvent event) {
	if (event.getCode() == KeyCode.ENTER) {
	loadOrgPopup();
}}
	

    @FXML
    void OnKeyPressMeetDate(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
    		txtsubGroup.requestFocus();
    }
}
@FXML
void OnKeyPressMeetChairPerson(KeyEvent event) {
	if (event.getCode() == KeyCode.ENTER) {
		txtSubject.requestFocus();
	}	
}

@FXML
void OnKeyPressSubj(KeyEvent event) {
	if (event.getCode() == KeyCode.ENTER) {
		txtMinutesBy.requestFocus();
	}	
}

    @FXML
	private void initialize() {
		System.out.println("ssssssss=====initialize=====sssssssssss");
		eventBus.register(this);
		
	
    
		tblMeetingMst.getSelectionModel().selectedItemProperty().addListener((obs,
	  			  oldSelection, newSelection) -> { if (newSelection != null) {
	  			  System.out.println("getSelectionModel"); if (null != newSelection.getId())
	  			  {
	  			  
	  				meetingMst = new MeetingMst(); 
	  				meetingMst.setId(newSelection.getId()); } }
	  			  });
	  		
    
    
    
    }
    
    
    
    
    
    
}

