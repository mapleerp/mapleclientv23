
package com.maple.mapleclient.church.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.OrgMst;
import com.maple.mapleclient.events.MemberMstEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class MemberMstPopupCtl {
		
  
	
	MemberMstEvent memberMstEvent;
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<MemberMst> memberMstList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();

	    @FXML
	    private TextField txtname;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TableView<MemberMst> tablePopupView;

	    @FXML
	    private TableColumn<MemberMst,String> memberId;

	    @FXML
	    private TableColumn<MemberMst, String> memberName;

	    @FXML
	    private Button btnCancel;
	    
	    @FXML
	 	private void initialize() {


	 			txtname.textProperty().bindBidirectional(SearchString);
	 			
	 			MemberMstBySearch("");
	 			
	 			memberMstEvent = new MemberMstEvent();
	 			eventBus.register(this);
	 			btnSubmit.setDefaultButton(true);
	 			tablePopupView.setItems(memberMstList);

	 			btnCancel.setCache(true);

	 			memberId.setCellValueFactory(cellData -> cellData.getValue().getMemberIdProperty());
	 			memberName.setCellValueFactory(cellData -> cellData.getValue().getMemberNameProperty());

	 			tablePopupView.setItems(memberMstList);
	 			for (MemberMst s : memberMstList) {
	 				
	 			}

	 			tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
	 				if (newSelection != null) {
	 					if (null != newSelection.getMemberName() && newSelection.getMemberName().length() > 0) {
	 						memberMstEvent.setMemberId(newSelection.getMemberId());
	 						memberMstEvent.setMemberName(newSelection.getMemberName());
	 						eventBus.post(memberMstEvent);
	 					}
	 				}
	 			});

				SearchString.addListener(new ChangeListener() {
					@Override
					public void changed(ObservableValue observable, Object oldValue, Object newValue) {

						MemberMstBySearch((String) newValue);
					}
				});
	    }


	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB) {

			} else {
				txtname.requestFocus();
			}


	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tablePopupView.requestFocus();
				tablePopupView.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			}

	    }

	    @FXML
	    void onCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();

	    }

	    @FXML
	    void submit(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();


	    }

		private void MemberMstBySearch(String searchData) {

			memberMstList.clear();
			ArrayList memberMst = new ArrayList();
			MemberMst cust = new MemberMst();
			RestTemplate restTemplate = new RestTemplate();

			memberMst = RestCaller.SearchMemberMstByName(searchData);

			Iterator itr = memberMst.iterator();
			while (itr.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				

				Object memName = lm.get("memberName");
				Object memId = lm.get("memberId");
			
				Object id = lm.get("id");
				if (null != id) {
					cust = new MemberMst();
					cust.setMemberId((String) memId);
					cust.setMemberName((String) memName);
				
					
					
				
					

					System.out.println(lm);

					memberMstList.add(cust);
				}

			}
			
			

			return;

		}

	}



