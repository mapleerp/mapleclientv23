package com.maple.mapleclient.church.controllers;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.entity.FinancialYearMst;
import com.maple.mapleclient.entity.OrgMst;
import com.maple.mapleclient.entity.ProcessMst;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;


public class OrganisationMstCtl {
	
	private ObservableList<OrgMst> OrgMstList = FXCollections.observableArrayList();

	OrgMst orgMst =null;

	@FXML
	 private DatePicker dpDate;

	 @FXML
	  private Button btnGenerateReport;

	    @FXML
	    private TextField txtorgname;

	    @FXML
	    private TextField txtorglocation;

	    @FXML
	    private TextField txtlatitude;

	    @FXML
	    private TextField txtlongitude;

	    @FXML
	    private TextField txtorgid;
	    
	    
	    @FXML
	    private TableView<OrgMst> table1;

	    @FXML
	    private TableColumn<OrgMst,String> orgid;

	    @FXML
	    private TableColumn<OrgMst,String> orgname;

	    @FXML
	    private TableColumn<OrgMst,String> orgloc;

	    @FXML
	    private TableColumn<OrgMst,String> orglat;

	    @FXML
	    private TableColumn<OrgMst,String> orglong;
	    @FXML
	    private Button btndelete;
	    @FXML
	    private Button btnshowall;

	    @FXML
	    private TextField txtorgidd;

	    @FXML
	    private ComboBox<String> cmbCheck;
        @FXML
		private void initialize() {
	    	
        	
        	cmbCheck.getItems().add("Yes");
        	cmbCheck.getItems().add("No");
		
		  table1.getSelectionModel().selectedItemProperty().addListener((obs,
		  oldSelection, newSelection) -> { if (newSelection != null) {
		  System.out.println("getSelectionModel"); 
		  if (null != newSelection.getId())
		  {
		  
			  orgMst = new OrgMst(); 
			  orgMst.setId(newSelection.getId()); } }
		  });
		 
	    	
	    	
	    }

	    @FXML
	    void Delete(ActionEvent event) {
		
		  if(null != orgMst) {
		if(null != orgMst.getId()) {
		  RestCaller.deleteOrgMst(orgMst.getId());
		  notifyMessage(5,"Deleted!!!");
		  
		  table1.getItems().clear();
		  showAll();
		  
		  } }
		 
	    	

	    }
	 
	    @FXML
	    void ShowAll(ActionEvent event) {
	    	 showAll();

	    }
	    @FXML
	    void Save(ActionEvent event) {
	    

			ResponseEntity<List<FinancialYearMst>> getFinancialYear = RestCaller.getAllFinancialYear();
			if (getFinancialYear.getBody().size() == 0) {
				notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
				return;
			}
			int count = RestCaller.getFinancialYearCount();
			if (count == 0) {
				notifyMessage(3, "Please Add Financial Year In the Configuration Menu");
				return;
			}
	    	
			if(txtorgname.getText().isEmpty()) {
				notifyMessage(5, "enter organisation name");
				
				return;
			}
			if(txtorglocation.getText().isEmpty()) {
				
				notifyMessage(5, "enter organisation location");
				return;
			}
			
			if(txtlatitude.getText().isEmpty()) {
				notifyMessage(5, "enter lattitude location");
				return;
			}
			
			if(txtlongitude.getText().isEmpty()) {
				notifyMessage(5, "enter longittude location");
				return;
			}
			if(null==dpDate.getValue()) {
				
				notifyMessage(5, "enter organisation date");
				return;
				
			}
			if(null==cmbCheck.getValue()) {
				notifyMessage(5, "enter Is church ");
				
				return;
			}
	    	OrgMst orgMst = new OrgMst();
	        orgMst.setOrgName(txtorgname.getText());
	    	orgMst.setOrgLocation(txtorglocation.getText());
	    	orgMst.setOrgLatitude(txtlatitude.getText());
	    	orgMst.setOrgLongtitude(txtlongitude.getText());
	    	orgMst.setOrgCheck(cmbCheck.getValue());
	    	if(null!=dpDate.getValue()) {
	        orgMst.setOrgCreationDate(Date.valueOf(dpDate.getValue()));
	    	}
	        String orgId = RestCaller.getVoucherNumber("ORG");
	        orgMst.setOrgId(orgId);
	    	ResponseEntity<OrgMst> respentity = RestCaller.OrganisationCreation(orgMst);
	    	orgMst = respentity.getBody();
	    	OrgMstList.add(orgMst);
	    	notifyMessage(5, "Save Successfully Completed");
	    	
	    	txtorgname.setText("");
	    	txtorglocation.setText("");
	    	txtlatitude.setText("");
	    	dpDate.setValue(null);
	    	cmbCheck.setValue(null);
	    	//txtorgid.setText("");
	    	txtlongitude.setText("");
	    //	txtorgidd.setText("");
	    
	    	
		    FillTable();
	    	
	    	

	    }
	  
	    @FXML
	    void actionCreatedDate(KeyEvent event) {
	    	
	    	if (event.getCode() == KeyCode.ENTER) {
				
	    		txtlongitude.requestFocus();
	    	}
	    }

	    @FXML
	    void actionLocation(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				
	    		txtorglocation.requestFocus();
	    	}
	    }

	    @FXML
	    void actionOrgID(KeyEvent event) {
              if (event.getCode() == KeyCode.ENTER) {
				
            	  txtlatitude.requestFocus();
	    	}

	    }

	    @FXML
	    void actionOrgId(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtorgidd.requestFocus();
	    	}

	    }

	    @FXML
	    void actionsave(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				
	    		dpDate.requestFocus();
	    	}

	    }

	    @FXML
	    void focusEndDate(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 cmbCheck.requestFocus();
	    	}

	    }
	    
	    private void showAll() {
	    	
	    	ResponseEntity<List<OrgMst>> orgMstResp = RestCaller.getAllOrganisation();
	    	OrgMstList = FXCollections.observableArrayList(orgMstResp.getBody());
	    	table1.getItems().clear();
			FillTable();

			
		}

	    @FXML
	    void actionCheck(KeyEvent event) {

	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 btnGenerateReport.requestFocus();
	    	}
	    }


		private void FillTable() {

			table1.setItems(OrgMstList);
			orgid.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());
			orgname.setCellValueFactory(cellData -> cellData.getValue().getOrgNameProperty());
			orgloc.setCellValueFactory(cellData -> cellData.getValue().getOrgLocationProperty());
			orglat.setCellValueFactory(cellData -> cellData.getValue().getOrgLatitudeProperty());
			orglong.setCellValueFactory(cellData -> cellData.getValue().getOrgLongtitudeProperty());
			
		}
	    public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}

	}


