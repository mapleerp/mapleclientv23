package com.maple.mapleclient.church.controllers;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MemberMst;

import com.maple.mapleclient.entity.PasterMst;
import com.maple.mapleclient.events.MemberMstEvent;
import com.maple.mapleclient.events.PasterMstEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
public class PasterMstPopupCtl {
PasterMstEvent pasterMstEvent;
	
	private EventBus eventBus = EventBusFactory.getEventBus();

	private ObservableList<	PasterMst> pasterMstList = FXCollections.observableArrayList();

	StringProperty SearchString = new SimpleStringProperty();

	@FXML
    private TextField txtname;

    @FXML
    private Button btnSubmit;

    @FXML
    private TableView<PasterMst> tablePopupView;

    @FXML
    private TableColumn<PasterMst, String> clpasterId;

    @FXML
    private TableColumn<PasterMst, String> clpasterName;

    @FXML
    private Button btnCancel;
//    @FXML
//    void OnKeyPress(KeyEvent event) {
//
//    }
//    @FXML
//    void OnKeyPressTxt(KeyEvent event) {
//
//    }
    @FXML
    void OnKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB) {

		} else {
			txtname.requestFocus();
		}
    }
    @FXML
    void OnKeyPressTxt(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
			tablePopupView.requestFocus();
			tablePopupView.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();
		}
    }
    @FXML
    void onCancel(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    @FXML
    void submit(ActionEvent event) {
    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
		stage.close();
    }
    @FXML
 	private void initialize() {
 			txtname.textProperty().bindBidirectional(SearchString);
 			PasterMstBySearch("");
 			pasterMstEvent = new PasterMstEvent();
 			eventBus.register(this);
 			btnSubmit.setDefaultButton(true);
 			tablePopupView.setItems(pasterMstList);
 			btnCancel.setCache(true);
 			clpasterId.setCellValueFactory(cellData -> cellData.getValue().getPasterMstIdProperty());
 			clpasterName.setCellValueFactory(cellData -> cellData.getValue().getPasterNameProperty());
 			tablePopupView.setItems(pasterMstList);
 			for (PasterMst s : pasterMstList) {	
 			}
 			tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
 				if (newSelection != null) {
 					if (null != newSelection.getPasterName() && newSelection.getPasterName().length() > 0) {
 						pasterMstEvent.setPasterId(newSelection.getPasterMstId());
 						pasterMstEvent.setPasterName(newSelection.getPasterName());
 						eventBus.post(pasterMstEvent);
 					}
 				}
 			});
			SearchString.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {
					//OrgBySearch((String) newValue);
					
				}
			});
    }
    private void PasterMstBySearch(String searchData) {
		ArrayList pasterMst = new ArrayList();
		PasterMst cust = new PasterMst();
		RestTemplate restTemplate = new RestTemplate();
		pasterMst = RestCaller.SearchPasterMstByName(searchData);
		Iterator itr = pasterMst.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object pasName = lm.get("pasterName");
			Object pasId = lm.get("pasterMstId");
			Object id = lm.get("id");
			if (null != id) {
				cust = new PasterMst();
				cust.setPasterMstId((String) pasId);
				cust.setPasterName((String) pasName);
				System.out.println(lm);
				pasterMstList.add(cust);
			}
		}
		return;
	}
}
