package com.maple.mapleclient.church.controllers;

import java.util.Date;

import org.controlsfx.control.Notifications;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.events.AccountEvent;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
public class OrgMemberWiseReceiptCtl {
	
EventBus eventBus = EventBusFactory.getEventBus();
	
	String accountId = "";
    @FXML
    private TextField txtAccount;

    @FXML
    private DatePicker dpStartDate;

    @FXML
    private DatePicker dpEndDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    void FocusOnBotton(KeyEvent event) {
    	showPopup();	
    }
    @FXML
   	private void initialize() {
       	
       	eventBus.register(this);
       	
      }
    @FXML
    void FocusOnEndDate(KeyEvent event) {
    	txtAccount.requestFocus();	
    }

    @FXML
    void FocusOnTxtAccount(KeyEvent event) {

    }

    @FXML
    void GenerateReport(ActionEvent event) {
    	generateReport();
    }

    @FXML
    void KeyPressGenerateReport(KeyEvent event) {

    }
private void generateReport() {
    	
    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(5, "Please select From date", false);
    		
    	} else if (null == dpEndDate.getValue()) {
    		
    		notifyMessage(5, "Please select End date", false);
			
		} else if (txtAccount.getText().trim().isEmpty()) {
			
			notifyMessage(5, "Please select Account", false);
			
		} else {
			
			Date sDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
			String startDate = SystemSetting.UtilDateToString(sDate, "yyyy-MM-dd");
			
			Date eDate = SystemSetting.localToUtilDate(dpEndDate.getValue());
			String endDate = SystemSetting.UtilDateToString(eDate, "yyyy-MM-dd");
			
			if(!accountId.equals("") && !accountId.equals(null))
			{
				//call jasperPdf
				try {
					JasperPdfReportService.OrgMemberwiseReport(startDate,endDate,accountId);
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
 }
    
    private void showPopup() {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			btnGenerateReport.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Subscribe
	public void popuplistner(AccountEvent accountEvent) {

		System.out.println("------AccountEvent-------popuplistner-------------");
		Stage stage = (Stage) btnGenerateReport.getScene().getWindow();
		if (stage.isShowing()) {

			txtAccount.setText(accountEvent.getAccountName());
			accountId = accountEvent.getAccountId();

		}

	}
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

}


