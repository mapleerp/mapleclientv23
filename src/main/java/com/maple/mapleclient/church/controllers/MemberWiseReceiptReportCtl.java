package com.maple.mapleclient.church.controllers;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MemberWiseReceiptReport;
import com.maple.mapleclient.events.MemberMstEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.PendingAmountReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class MemberWiseReceiptReportCtl {
	EventBus eventBus = EventBusFactory.getEventBus();
	 private ObservableList<MemberWiseReceiptReport> receiptReportList = FXCollections.observableArrayList();
    @FXML
    private TextField txtMember;

    @FXML
    private DatePicker dpStartDate;

    @FXML
    private TableView<MemberWiseReceiptReport> tblReceiptReport;

    @FXML
    private TableColumn<MemberWiseReceiptReport, String> membername;

    @FXML
    private TableColumn<MemberWiseReceiptReport, String> familname;

    @FXML
    private TableColumn<MemberWiseReceiptReport, String> tblAccount;

    @FXML
    private TableColumn<MemberWiseReceiptReport, String> tblTransDate;

    @FXML
    private TableColumn<MemberWiseReceiptReport, Number> tblAmount;

    @FXML
    private DatePicker dpStartDate1;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private Button btnshow;
    
    @FXML
    private TextField txtGrandTotal;

    String memberId;
  private void loadMemberPopup() {
    	
    	try {
    		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MemberMstPopUp.fxml"));
    		Parent root1;

    		root1 = (Parent) fxmlLoader.load();
    		Stage stage = new Stage();

    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.initStyle(StageStyle.UNDECORATED);
    		stage.setTitle("ABC");
    		stage.initModality(Modality.APPLICATION_MODAL);
    		stage.setScene(new Scene(root1));
    		stage.show();
    		txtMember.requestFocus();
    	

    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}

    }
  
  @Subscribe
	public void popupMemberlistner(MemberMstEvent memberMstEvent) {

	  System.out.print("inside sub scribe issssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		Stage stage = (Stage) btnshow.getScene().getWindow();
		if (stage.isShowing()) {
			txtMember.setText(memberMstEvent.getMemberName());
			memberId=memberMstEvent.getMemberId();
		}	
	}  
  
  
  @FXML
  void loadPopup(MouseEvent event) {
  	loadMemberPopup();
  }
 
    @FXML
    void FocusOnBotton(KeyEvent event) {

    }

    @FXML
    void FocusOnEndDate(KeyEvent event) {

    }

    @FXML
    void GenerateReport(ActionEvent event) {

    }

    @FXML
    void GenerateShow(ActionEvent event) {

    	

    	if(null == dpStartDate.getValue())
    	{
    		notifyMessage(5, "Please select From date", false);
    		
    		
		}else if (null == dpStartDate1.getValue()) {
    		
    		notifyMessage(5, "Please select End date", false);
		} 
    	else if (txtMember.getText().trim().isEmpty()) {
			
			notifyMessage(5, "Please select member", false);
			
		} else {
		Date sDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
		String startDate = SystemSetting.UtilDateToString(sDate, "yyyy-MM-dd");

		Date eDate = SystemSetting.localToUtilDate(dpStartDate1.getValue());
		String endDate = SystemSetting.UtilDateToString(eDate, "yyyy-MM-dd");
		

		if(!memberId.equals("") && !memberId.equals(null))
		{
		ResponseEntity<List<MemberWiseReceiptReport>>pendingSave=RestCaller.getMemberWiseReceiptReport(startDate,endDate,memberId);
		receiptReportList = FXCollections.observableArrayList(pendingSave.getBody());
		Double grandTotal=0.0;
		
		for(int i=0;i<receiptReportList.size();i++) {
		Double	amount=receiptReportList.get(i).getAmount();
		grandTotal=amount+grandTotal;
		}
		txtGrandTotal.setText(grandTotal.toString());
		
		System.out.print(receiptReportList.size()+"receipt list size isssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		}
		fillTable();
		
		}
    	
    	
    }
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    @FXML
    void KeyPressGenerateReport(KeyEvent event) {

    }


    @FXML
    void loadpopup(MouseEvent event) {
    	loadMemberPopup();
    }

    @FXML
	private void initialize() {
		eventBus.register(this);
    }
    
	private void fillTable()
    {
		tblReceiptReport.setItems(receiptReportList);
		membername.setCellValueFactory(cellData -> cellData.getValue().getMemberNameProperty());
		familname.setCellValueFactory(cellData -> cellData.getValue().getFamilyNameProperty());
		tblAccount.setCellValueFactory(cellData -> cellData.getValue().getAccountNameProperty());
		tblTransDate.setCellValueFactory(cellData -> cellData.getValue().getTransDateProperty());
		tblAmount.setCellValueFactory(cellData -> cellData.getValue().getAmountProperty());
    }
}
