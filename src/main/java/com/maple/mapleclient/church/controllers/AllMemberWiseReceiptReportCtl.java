package com.maple.mapleclient.church.controllers;


import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.ExportAllMemberWiseReceiptReport;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.MemberWiseReceipt;

import com.maple.mapleclient.restService.RestCaller;

import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import javafx.geometry.Pos;



public class AllMemberWiseReceiptReportCtl {

	ExportAllMemberWiseReceiptReport exportAllMemberWiseReceiptReport = new ExportAllMemberWiseReceiptReport();
	@FXML
    private DatePicker dpFromDate;

    @FXML
    private Button btnGenerateReport;

    @FXML
    private DatePicker dpToDate;

    @FXML
    void GenerateReport(ActionEvent event) {

    	if(null==dpFromDate.getValue()||null==dpToDate.getValue()) {
    		
    		notifyMessage(5, "select proper date",false);
    		return;
    	}else {
    		

       	 java.util.Date uDate = Date.valueOf(dpFromDate.getValue());
   			String startDate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
   			java.util.Date uDate1 = Date.valueOf(dpToDate.getValue());
   			String endDate = SystemSetting.UtilDateToString(uDate1, "yyy-MM-dd");
   			
   		 ArrayList<MemberWiseReceipt> memberWiseReceiptList=RestCaller.getAllMemberWiseReceiptReport(startDate,endDate);
   	
   		 
   		exportAllMemberWiseReceiptReport.exportToExcel("AllMemberWiseReceiptReport"+startDate+endDate+".xls", memberWiseReceiptList);
   		 
    	}
    	
    	
    	
    }
    
    public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}
    
}
