package com.maple.mapleclient.church.controllers;



import java.util.Date;

import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.FamilyMst;
import com.maple.mapleclient.events.AccountEvent;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.DayBook;
import com.maple.report.entity.PendingAmountReport;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class DueAmountReportCtl {
	

 @FXML
 private TextField txtAccount;

	EventBus eventBus = EventBusFactory.getEventBus();
	 private ObservableList<PendingAmountReport> PendingAmountList = FXCollections.observableArrayList();
	
	 String accountId = null;

	    @FXML
	    private DatePicker dpStartDate;

	    @FXML
	    private Button btnGenerateReport;
	    @FXML
	    private DatePicker dpStartDate1;


	    @FXML
	    private TableView<PendingAmountReport> TablePending;


	    @FXML
	    private TableColumn<PendingAmountReport, String> membername;

	    @FXML
	    private TableColumn<PendingAmountReport, String> familname;

	    @FXML
	    private TableColumn<PendingAmountReport, String> headoffamily;

	    @FXML
	    private TableColumn<PendingAmountReport, String> orgids;

	    @FXML
	    private TableColumn<PendingAmountReport, String> email;


	    @FXML
	    private Button btnshow;
	    @FXML
	    void GenerateShow(ActionEvent event) {

	    	if(null == dpStartDate.getValue())
	    	{
	    		notifyMessage(5, "Please select From date", false);
	    		
	    		
			}else if (null == dpStartDate1.getValue()) {
	    		
	    		notifyMessage(5, "Please select End date", false);
			} 
	    	else if (txtAccount.getText().trim().isEmpty()) {
				
				notifyMessage(5, "Please select Account", false);
				
			} else {
			Date sDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
			String startDate = SystemSetting.UtilDateToString(sDate, "yyyy-MM-dd");

			Date eDate = SystemSetting.localToUtilDate(dpStartDate1.getValue());
			String endDate = SystemSetting.UtilDateToString(eDate, "yyyy-MM-dd");
			

			if(!accountId.equals("") && !accountId.equals(null))
			{
			ResponseEntity<List<PendingAmountReport>>pendingSave=RestCaller.getPendingAmountReport(startDate,endDate,accountId);
			PendingAmountList = FXCollections.observableArrayList(pendingSave.getBody());
			}
			fillTable();
			
			}
	    }

	    @FXML
		private void initialize() {
	    	
	    	eventBus.register(this);
	    	
	    }
	    @FXML
	    void FocusOnBotton(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    	showPopup();
	    	}
	    }

	    @FXML
	    void FocusOnEndDate(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
	    	dpStartDate1.requestFocus();
	    	}
	    }

	    @FXML
	    void GenerateReport(ActionEvent event) {
	    	generateReport();
	    	
	    }

	    @FXML
	    void KeyPressGenerateReport(KeyEvent event) {

	    }

	    private void generateReport() {
	    	
	    	if(null == dpStartDate.getValue())
	    	{
	    		notifyMessage(5, "Please select From date", false);
	    		
	    		
			} else if (txtAccount.getText().trim().isEmpty()) {
				
				notifyMessage(5, "Please select Account", false);
				
			} else {
				
				Date sDate = SystemSetting.localToUtilDate(dpStartDate.getValue());
				String startDate = SystemSetting.UtilDateToString(sDate, "yyyy-MM-dd");

				Date eDate = SystemSetting.localToUtilDate(dpStartDate1.getValue());
				String endDate = SystemSetting.UtilDateToString(eDate, "yyyy-MM-dd");
				
		
				if(!accountId.equals("") && !accountId.equals(null))
				{
					//call jasperPdf
					try {
						JasperPdfReportService.PendingCashReport(startDate,endDate,accountId);
						
						accountId = null;
						
					} catch (JRException e) {
						//TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
			
	    }
	    
		private void showPopup() {

			try {
				System.out.println("inside the popup");
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AccountPopup.fxml"));
				Parent root1;
				root1 = (Parent) loader.load();
				Stage stage = new Stage();
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("Accounts");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				
//					dpSupplierInvDate.requestFocus();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@Subscribe
		public void popuplistner(AccountEvent accountEvent) {

			System.out.println("------AccountEvent-------popuplistner-------------");
			Stage stage = (Stage) btnGenerateReport.getScene().getWindow();
			if (stage.isShowing()) {

				txtAccount.setText(accountEvent.getAccountName());
				accountId = accountEvent.getAccountId();

			}

		}
		public void notifyMessage(int duration, String msg, boolean success) {

			Image img;
			if (success) {
				img = new Image("done.png");

			} else {
				img = new Image("failed.png");
			}

			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();

		}
		private void fillTable()
	    {
			TablePending.setItems(PendingAmountList);
			membername.setCellValueFactory(cellData -> cellData.getValue().getMemberNameProperty());
			familname.setCellValueFactory(cellData -> cellData.getValue().getFamilyNameProperty());
			headoffamily.setCellValueFactory(cellData -> cellData.getValue().getHeadOfFamilyProperty());
			orgids.setCellValueFactory(cellData -> cellData.getValue().getMobileNoProperty());
			email.setCellValueFactory(cellData -> cellData.getValue().getEmailProperty());
	    }

	}


