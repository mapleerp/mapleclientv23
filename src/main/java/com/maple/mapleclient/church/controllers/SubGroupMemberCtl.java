package com.maple.mapleclient.church.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.FamilyMst;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.OrgMst;
import com.maple.mapleclient.entity.SubGroupMember;
import com.maple.mapleclient.events.MemberMstEvent;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
public class SubGroupMemberCtl 
{
	
	 EventBus eventBus = EventBusFactory.getEventBus();
	 private ObservableList<SubGroupMember> subGroupMemberList = FXCollections.observableArrayList();

	SubGroupMember subGroupMember = null;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnshowall;
    @FXML
    private Button btnEdit;

    @FXML
    private Button btnDelete;

    @FXML
    private TextField txtOrgId;

    @FXML
    private TextField txtSubGrpId;

    @FXML
    private TextField txtMemberId;

    
    @FXML
    private TextField txtSubGroupName;
    
    @FXML
    private DatePicker dpMemberSince;

    @FXML
    private TextField txtRecordId;
    
    @FXML
    private TableView<SubGroupMember> tblSubGroupMember;

    @FXML
    private TableColumn<SubGroupMember,String> clOrgId;

    @FXML
    private TableColumn<SubGroupMember, String> clSubGroupId;

    @FXML
    private TableColumn<SubGroupMember, String> clMemberId;

    @FXML
    private TableColumn<SubGroupMember,Date> clMemberSince;

    @FXML
    private TableColumn<SubGroupMember, String> clRecordId;
    
    @FXML
    private TableColumn<SubGroupMember, String>clumnSubGpName;
    @FXML
     private void initialize() 
 	 {
    	
 		 eventBus.register(this);	
 		tblSubGroupMember.getSelectionModel().selectedItemProperty().addListener((obs,
  			  oldSelection, newSelection) -> { if (newSelection != null) {
  			  System.out.println("getSelectionModel"); if (null != newSelection.getId())
  			  {
  			  
  				subGroupMember = new SubGroupMember(); 
  				subGroupMember.setId(newSelection.getId()); } }
  			  });
  			 
 	  }

    @FXML
    void ActionDelete(ActionEvent event) {
    	delete();

    }


    @FXML
    void ActionShowall(ActionEvent event) {
    	showAll();

    }
    @FXML
    void ActionEdit(ActionEvent event) {

    }
    @FXML
    void ActionMemberId(KeyEvent event) {

    	 if (event.getCode() == KeyCode.ENTER) {
 	    	if (txtSubGrpId.getText().trim().isEmpty()) {
 				String SubId = RestCaller.getVoucherNumber("SUB");
 				txtSubGrpId.setText(SubId);
 			}
 	    	 }
    	 txtMemberId.requestFocus();
    }
    @FXML
    void ActionMemberSince(KeyEvent event) {
    	  if (event.getCode() == KeyCode.ENTER) {
				
    		  loadMemberPopup();
    		  dpMemberSince.requestFocus();
    	}

    }

    @FXML
    void ActionRecordId(KeyEvent event) {
    	 if (event.getCode() == KeyCode.ENTER) {
				
    		 txtRecordId.requestFocus();
   	}

    }
    @FXML
    void ActionSubGroupId(KeyEvent event) {
    	loadOrgPopup();
    	

    }

    @FXML
    void ActionToSave(KeyEvent event) {

   	 if (event.getCode() == KeyCode.ENTER) {
	    	if (txtRecordId.getText().trim().isEmpty()) {
				String RecId = RestCaller.getVoucherNumber("REC");
				txtRecordId.setText(RecId);
			}
	    	 }
   	btnSave.requestFocus();
    }

    @FXML
    void ActionSave(ActionEvent event) 
    {
    	if(txtOrgId.getText().isEmpty()) {
    		notifyMessage(5, "Enter org id");
    		return;
    	}if(txtMemberId.getText().isEmpty()) {
    		notifyMessage(5, "Enter member id");
    		return;
    	}
    	if(txtSubGroupName.getText().isEmpty()) {
    		notifyMessage(5, "Enter subgroup name");
    		return;
    	}
    	if(null==dpMemberSince.getValue()){
    		notifyMessage(5, "Enter member since");
    		return;
    	}
    	subGroupMember=new SubGroupMember();
    	subGroupMember.setOrgId(txtOrgId.getText());
    	String SubId = RestCaller.getVoucherNumber("SUB");
    	subGroupMember.setSubGroupId(SubId);
    	subGroupMember.setMemberId(txtMemberId.getText());
    	subGroupMember.setSubGroupName(txtSubGroupName.getText());
    	subGroupMember.setMembersince(Date.valueOf(dpMemberSince.getValue()));
    	
    	String RecId = RestCaller.getVoucherNumber("REC");
    	subGroupMember.setRecordId(RecId);
    	ResponseEntity<SubGroupMember> respentity = RestCaller.saveSubGroupMember(subGroupMember);
     	subGroupMember = respentity.getBody();
		System.out.println("saved sub group member" + subGroupMember);
		subGroupMemberList.add(subGroupMember);
    	notifyMessage(5, "Save Successfully Completed");
    	
	    FillTable();
		clear();
    }

    void clear()
    {
    	txtOrgId.setText("");
    	txtMemberId.setText("");
    	dpMemberSince.setValue(null);
    	txtSubGroupName.setText("");
    	
    }
    private void loadOrgPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/OrganisationIdpopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtSubGrpId.requestFocus();
		

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
     private void loadMemberPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/MemberMstPopUp.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
			txtSubGrpId.requestFocus();
		

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
 
    @Subscribe
   	public void popupMemberlistner(MemberMstEvent memberMstEvent) {

   		Stage stage = (Stage) btnSave.getScene().getWindow();
   		if (stage.isShowing()) {

   			txtMemberId.setText(memberMstEvent.getMemberId());

   			
   		}

   	}
    @Subscribe
 	public void popupOrglistner(OrgEvent orgEvent) {

 		Stage stage = (Stage) btnSave.getScene().getWindow();
 		if (stage.isShowing()) {

 			txtOrgId.setText(orgEvent.getOrgId());

 			
 		}

 	}
	private void FillTable() {

		tblSubGroupMember.setItems(subGroupMemberList);
		clOrgId.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());
		clSubGroupId.setCellValueFactory(cellData -> cellData.getValue().getSubGroupIdProperty());
		clMemberId.setCellValueFactory(cellData -> cellData.getValue().getMemberIdProperty());
		clRecordId.setCellValueFactory(cellData -> cellData.getValue().getRecordIdProperty());
		clumnSubGpName.setCellValueFactory(cellData -> cellData.getValue().getSubGroupNameProperty());
		
	}
    public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}
    private void showAll() {
    	
    	ResponseEntity<List<SubGroupMember>> SubGroupMemberResp = RestCaller.getAllSubGroupMember();
    	subGroupMemberList = FXCollections.observableArrayList(SubGroupMemberResp.getBody());
    	tblSubGroupMember.getItems().clear();
		FillTable();

		
	}

  void delete()
  {
	  if(null != subGroupMember) {
	if(null != subGroupMember.getId()) {
	  RestCaller.deleteSubGroupMember(subGroupMember.getId());
	  notifyMessage(5,"Deleted!!!");
	  
	  tblSubGroupMember.getItems().clear();
	  showAll();
	  
	  } }
}
}
