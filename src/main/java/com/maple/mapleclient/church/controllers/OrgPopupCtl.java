package com.maple.mapleclient.church.controllers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.maple.mapleclient.EventBusFactory;

import com.maple.mapleclient.entity.OrgMst;

import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class OrgPopupCtl {

		OrgEvent orgEvent;
		
		private EventBus eventBus = EventBusFactory.getEventBus();

		private ObservableList<OrgMst> orgList = FXCollections.observableArrayList();

		StringProperty SearchString = new SimpleStringProperty();
	

	    @FXML
	    private TextField txtname;

	    @FXML
	    private Button btnSubmit;

	    @FXML
	    private TableView<OrgMst> tablePopupView;

	    @FXML
	    private TableColumn<OrgMst,String> custName;

	    @FXML
	    private TableColumn<OrgMst, String> custAddress;

	    @FXML
	    private Button btnCancel;
	    
	    @FXML
		private void initialize() {


			txtname.textProperty().bindBidirectional(SearchString);
			
			OrgBySearch("");
			
			orgEvent = new OrgEvent();
			eventBus.register(this);
			btnSubmit.setDefaultButton(true);
			tablePopupView.setItems(orgList);

			btnCancel.setCache(true);

			custName.setCellValueFactory(cellData -> cellData.getValue().getOrgNameProperty());
			custAddress.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());

			tablePopupView.setItems(orgList);
			for (OrgMst s : orgList) {
				 System.out.println("@@ SupplierName @@"+s.getId());
			}

			tablePopupView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
				if (newSelection != null) {
					if (null != newSelection.getOrgName() && newSelection.getOrgName().length() > 0) {
						orgEvent.setOrgName(newSelection.getOrgName());
						orgEvent.setOrgId(newSelection.getOrgId());
						eventBus.post(orgEvent);
					}
				}
			});

		
			SearchString.addListener(new ChangeListener() {
				@Override
				public void changed(ObservableValue observable, Object oldValue, Object newValue) {

					OrgBySearch((String) newValue);
				}
			});

		}

	    @FXML
	    void OnKeyPress(KeyEvent event) {
	    	if (event.getCode() == KeyCode.ENTER) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
					|| event.getCode() == KeyCode.TAB) {

			} else {
				txtname.requestFocus();
			}

	    }

	    @FXML
	    void OnKeyPressTxt(KeyEvent event) {
	    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
				tablePopupView.requestFocus();
				tablePopupView.getSelectionModel().selectFirst();
			}
			if (event.getCode() == KeyCode.ESCAPE) {
				Stage stage = (Stage) btnSubmit.getScene().getWindow();
				stage.close();
			}

	    }

	    @FXML
	    void onCancel(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();

	    }

	    @FXML
	    void submit(ActionEvent event) {
	    	Stage stage = (Stage) btnSubmit.getScene().getWindow();
			stage.close();

	    }

		private void OrgBySearch(String searchData) {

		
			ArrayList orgMst = new ArrayList();
			OrgMst cust = new OrgMst();
			RestTemplate restTemplate = new RestTemplate();

			orgMst = RestCaller.SearchOrgByName(searchData);

			Iterator itr = orgMst.iterator();
			while (itr.hasNext()) {
				LinkedHashMap lm = (LinkedHashMap) itr.next();
				

				Object orgName = lm.get("orgName");
				Object orgId = lm.get("orgId");
			
				Object id = lm.get("id");
				if (null != id) {
					cust = new OrgMst();
					cust.setOrgId((String) orgId);
					cust.setOrgName((String) orgName);
					cust.setId((String) id);
					
					
				
					

					System.out.println(lm);

					orgList.add(cust);
				}

			}
			
			

			return;

		}
		
	}
	



