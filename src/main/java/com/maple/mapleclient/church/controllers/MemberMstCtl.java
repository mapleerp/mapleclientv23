package com.maple.mapleclient.church.controllers;


import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.MembersMst;
import com.maple.mapleclient.events.FamilyEvent;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class MemberMstCtl {
	

	 EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<MembersMst> MembersMstList = FXCollections.observableArrayList();
	private ObservableList<MemberMst> MemberMstList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();
	MemberMst memberMst=new MemberMst();
   @FXML
   private Button btnSave;


   @FXML
   private Button btnClear;
   @FXML
   private Button btnEdit;

   @FXML
   private Button btnDelete;

   String familyIds;
   
   @FXML
   private TextField txtMemberId;
   
   @FXML
   private TextField famId;

   @FXML
   private TextField txtfamilyId;


   @FXML
   private TextField txtMemberName;

   @FXML
   private ComboBox<String> cmbSex;

   @FXML
   private DatePicker dpDob;

   @FXML
   private DatePicker dpDos;

   @FXML
   private DatePicker dpDowb;

   @FXML
   private DatePicker dpDohsb;

   @FXML
   private DatePicker dpDom;

   @FXML
   private TextField txtReasonLeav;

   @FXML
   private TextField txtMemberIdActive;

   @FXML
   private TextField txtProfession;

   @FXML
   private TextField txtPreviouscommunity;

   @FXML
   private DatePicker dpDateOfJoining;

   @FXML
   private DatePicker dpdod;

   @FXML
   private DatePicker dpDol;

   @FXML
   private ComboBox<String> cmbHeadOfFamily;
   @FXML
   private TextField txtReasonOfDeath;

   @FXML
   private TextField txtMobileNo;

   @FXML
   private DatePicker dpDateAddedDate;




   @FXML
   private TextField txtEmail;

   @FXML
   private TextField txttotalOfferings;


   @FXML
   private Button btnPrint;

@FXML
private ComboBox<String>cmbIsApaster;
	/*
	 * @FXML private TextField txtHeadofFam;
	 */
   @FXML
   private TextField orgid;
   @FXML
   private Button tdelete;

   @FXML
   private Button tshowall;
   
   
   @FXML
   private TableView<MembersMst> tablemember;

   @FXML
   private TableColumn<MembersMst,String> memberid;

   @FXML
   private TableColumn<MembersMst,String> familyid;

   @FXML
   private TableColumn<MembersMst,String> tblmembername;

   @FXML
   private TableColumn<MembersMst,String> tblsex;

   @FXML
   private TableColumn<MembersMst,String> tblemailid;

   @FXML
   private TableColumn<MembersMst,String> tblmobno;

   @FXML
   private TableColumn<MembersMst,String> tblorgid;

   
 
	    

  
   @FXML
   void ActionFamilyID(KeyEvent event) {
   	if (event.getCode() == KeyCode.ENTER) {
   		
   		loadFamilyPopup();
   		
  	    	}
 
    txtMemberName.requestFocus();
   	
  	   
   	
   

   }

   @FXML
   void ActionMemberId(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
	    	if (txtMemberId.getText().trim().isEmpty()) {
				String MemId = RestCaller.getVoucherNumber("MEM");
				txtMemberId.setText(MemId);
			}
	    	 }
   	 txtfamilyId.requestFocus();

   }

   @FXML
   void ActionOrgID(KeyEvent event) {
   	if (event.getCode() == KeyCode.ENTER) {
  		 //loadOrgPopup();
  		
	    	}
   	cmbIsApaster.requestFocus();
   }

  
   
   @FXML
   void ActionSave(ActionEvent event) {
   	
	   
   	if(null==txtfamilyId.getText()) {
       	notifyMessage(5, "please enter familty ID");
       	txtfamilyId.requestFocus();
       	return;
   	}
   	if(txtfamilyId.getText().trim().isEmpty())
   	{
   		notifyMessage(5, "please enter familty ");
       	txtfamilyId.requestFocus();
       	return;
   	}
   	if(null==txtMemberName.getText()) {
   		notifyMessage(5, "please enter membername");
   		txtMemberName.requestFocus();
       	return;
   	}
   	
   	if(txtMemberName.getText().trim().isEmpty())
   	{
   		notifyMessage(5, "please enter membername");
   		txtMemberName.requestFocus();
       	return;
   	}
   	if(null == cmbSex.getSelectionModel().getSelectedItem())
   	{
   		notifyMessage(5, "please select sex");
   		cmbSex.requestFocus();
       	return;
   	}
	
if(null!=memberMst.getId()) {
	System.out.print("inside update isssssssssssssssssssssssssss");
	if(null!=txtMemberName.getText()) {
	memberMst.setMemberName(txtMemberName.getText());
	}
  if(null!=txtfamilyId.getText()) {
	 	memberMst.setFamilyId(familyIds);
	 	
	// 	System.out.print(famId+"family id issssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
  }
  
   	if(null!=cmbSex.getValue()) {
   	memberMst.setSex(cmbSex.getSelectionModel().getSelectedItem().toString());
   	}
   	if(null!=dpDob.getValue()) {
   	memberMst.setDob(Date.valueOf(dpDob.getValue()));
   	}
   	if(null!=dpDos.getValue()) {
   	memberMst.setDos(Date.valueOf(dpDos.getValue()));
   	}
   	if(null!=dpDowb.getValue()) {
		memberMst.setDowb(Date.valueOf(dpDowb.getValue()));
   	}
   	if(null!=dpDohsb.getValue()) {
		memberMst.setDohsb(Date.valueOf(dpDohsb.getValue()));
   	}
   	if(null!=dpDom.getValue()) {
		memberMst.setDom(Date.valueOf(dpDom.getValue()));
   	}
   	if(null!=txtReasonOfDeath.getText()) {
		memberMst.setReasonOfLeaving(txtReasonOfDeath.getText());
   	}
   	if(null!=txtMemberIdActive.getText()) {
		memberMst.setMemberIdActive(txtMemberIdActive.getText());
   	}
   	if(null!=txtProfession.getText()) {
		memberMst.setProffession(txtProfession.getText());
   	}
   	if(null!=txtPreviouscommunity.getText()) {
		memberMst.setPreviousCommunity(txtPreviouscommunity.getText());
   	}
   	if(null!=dpDateOfJoining.getValue()) {
		memberMst.setDateOfJoining(Date.valueOf(dpDateOfJoining.getValue()));
   	}
   	if(null!=dpdod.getValue()) {
		memberMst.setDod(Date.valueOf(dpdod.getValue()));
   	}
   	if(null!=txtReasonOfDeath.getText()) {
		memberMst.setReasonOfDeath(txtReasonOfDeath.getText());
   	}
   	if(null!=dpDol.getValue()) {
		memberMst.setDol(Date.valueOf(dpDol.getValue()));
		if(null!=dpDom.getValue()) {
			
			memberMst.setDom(Date.valueOf(dpDol.getValue()));
		}
   	}
   	if(null!=orgid.getText()) {
		memberMst.setOrgId(orgid.getText());
   	}
	
	if(null!=txtMobileNo.getText()) {
		memberMst.setMobileNo(txtMobileNo.getText());
	}
	if(null!=dpDateAddedDate.getValue()) {
		memberMst.setDataAddedDate(Date.valueOf(dpDateAddedDate.getValue()));
	}
	

	if(null!=txtEmail.getText()) {
		memberMst.setEmail(txtEmail.getText());
	}
	if(!txttotalOfferings.getText().isEmpty()) {
		memberMst.setTotalOfferings(Double.parseDouble((txttotalOfferings.getText())));
	}
	if(null!=cmbIsApaster.getValue()) {
		memberMst.setIsAPaster(cmbIsApaster.getValue());
	}
	memberMst.setDataAddedBy(SystemSetting.getUserId());
	if(null!=cmbHeadOfFamily.getValue()) {
		memberMst.setHeadOfFamily(cmbHeadOfFamily.getValue());
	}
	if(null!=txtPreviouscommunity.getText()) {
		memberMst.setPreviousCommunity(txtPreviouscommunity.getText());
   	}
	if(null!=txtReasonOfDeath.getText()) {
		memberMst.setReasonOfLeaving(txtReasonOfDeath.getText());
   	}
	System.out.print(memberMst.getMemberId()+"member id izzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
	RestCaller.updateMemberMst(memberMst);

	FillTable();
   	notifyMessage(5, "Updated Successfully Completed");
   	
	
	clear();
	memberMst=new MemberMst();
	showAll();
	
}else {
   	memberMst.setMemberName(txtMemberName.getText());
   	String MemId = RestCaller.getVoucherNumber("MEM");
       memberMst.setMemberId(MemId);
       if(null!=txtfamilyId.getText()) {
   	memberMst.setFamilyId(familyIds);
       }
   	if(null!=cmbSex.getValue()) {
   	memberMst.setSex(cmbSex.getSelectionModel().getSelectedItem().toString());
   	}
   	if(null!=dpDob.getValue()) {
   	memberMst.setDob(Date.valueOf(dpDob.getValue()));
   	}
   	if(null!=dpDos.getValue()) {
   	memberMst.setDos(Date.valueOf(dpDos.getValue()));
   	}
   	if(null!=dpDowb.getValue()) {
		memberMst.setDowb(Date.valueOf(dpDowb.getValue()));
   	}
   	if(null!=dpDohsb.getValue()) {
		memberMst.setDohsb(Date.valueOf(dpDohsb.getValue()));
   	}
   	if(null!=dpDom.getValue()) {
		memberMst.setDom(Date.valueOf(dpDom.getValue()));
   	}
   	if(null!=txtReasonOfDeath.getText()) {
		memberMst.setReasonOfLeaving(txtReasonOfDeath.getText());
   	}
   	if(null!=txtMemberIdActive.getText()) {
		memberMst.setMemberIdActive(txtMemberIdActive.getText());
   	}
   	if(null!=txtProfession.getText()) {
		memberMst.setProffession(txtProfession.getText());
   	}
   	if(null!=txtPreviouscommunity.getText()) {
		memberMst.setPreviousCommunity(txtPreviouscommunity.getText());
   	}
   	if(null!=dpDateOfJoining.getValue()) {
		memberMst.setDateOfJoining(Date.valueOf(dpDateOfJoining.getValue()));
   	}
   	if(null!=dpdod.getValue()) {
		memberMst.setDod(Date.valueOf(dpdod.getValue()));
   	}
   	if(null!=txtReasonOfDeath.getText()) {
		memberMst.setReasonOfDeath(txtReasonOfDeath.getText());
   	}
   	if(null!=dpDol.getValue()) {
		memberMst.setDol(Date.valueOf(dpDol.getValue()));
   	}
   	if(null!=orgid.getText()) {
		memberMst.setOrgId(orgid.getText());
   	}
	
	if(null!=txtMobileNo.getText()) {
		memberMst.setMobileNo(txtMobileNo.getText());
	}
	if(null!=dpDateAddedDate.getValue()) {
		memberMst.setDataAddedDate(Date.valueOf(dpDateAddedDate.getValue()));
	}
	

	if(null!=txtEmail.getText()) {
		memberMst.setEmail(txtEmail.getText());
	}
	if(!txttotalOfferings.getText().isEmpty()) {
		memberMst.setTotalOfferings(Double.parseDouble((txttotalOfferings.getText())));
	}
	if(null!=cmbIsApaster.getValue()) {
		memberMst.setIsAPaster(cmbIsApaster.getValue());
	}
	memberMst.setDataAddedBy(SystemSetting.getUserId());
	if(null!=cmbHeadOfFamily.getValue()) {
		memberMst.setHeadOfFamily(cmbHeadOfFamily.getValue());
	}
		ResponseEntity<MemberMst> respentity = RestCaller.saveMemberMst(memberMst);
		memberMst = respentity.getBody();
//		MemberMstList.add(memberMst);
		FillTable();
   	notifyMessage(5, "Save Successfully Completed");
   	
	showAll();
	clear();

}
   }
   @FXML
  	private void initialize() {
	
   	txtMemberId.setVisible(false);
   	eventBus.register(this);
   	cmbHeadOfFamily.getItems().add("YES");
   	cmbHeadOfFamily.getItems().add("NO");
   	cmbIsApaster.getItems().add("YES");
   	cmbIsApaster.getItems().add("NO");
   	
    //famId.setVisible(false);
//   	dpDob.setValue(LocalDate.now());
//   	dpDos.setValue(LocalDate.now());
//   	dpDowb.setValue(LocalDate.now());
//   	dpDohsb.setValue(LocalDate.now());
//   	dpDom.setValue(LocalDate.now());
//   	dpdod.setValue(LocalDate.now());
//   	dpDohsb.setValue(LocalDate.now());
//   	dpDol.setValue(LocalDate.now());
//   	dpDateOfJoining.setValue(LocalDate.now());
//   	dpDateAddedDate.setValue(LocalDate.now());
   	
   	tablemember.getSelectionModel().selectedItemProperty().addListener((obs,
			  oldSelection, newSelection) -> { if (newSelection != null) {
			  System.out.println("getSelectionModel"); 
			  if (null != newSelection.getMemberId())
			  {
				  MemberMst memberMst = new MemberMst(); 
				  memberMst.setId(newSelection.getId());}}
			  if(null!=newSelection.getMemberName()) {
			  txtMemberName.setText(newSelection.getMemberName());
			  }if(null!=newSelection.getSex()) {
			  cmbSex.setValue(newSelection.getSex());
			  }
			  txtMemberId.setText(newSelection.getMemberId());
			  if(null!=newSelection.getPreviousCommunity()) {
			  txtPreviouscommunity.setText(newSelection.getPreviousCommunity());
			  }
			  if(null!=newSelection.getFamilyId()){
			
				  familyIds=newSelection.getFamilyId();
				  txtfamilyId.setText(newSelection.getFamilyName());
			  }
			  
			  txtProfession.setText(newSelection.getProffession());
			  txtMobileNo.setText(newSelection.getMobileNo());
			  txtMemberIdActive.setText(newSelection.getMemberIdActive());
			  cmbHeadOfFamily.setValue(newSelection.getHeadOfFamily());
			  txtEmail.setText(newSelection.getEmail());
			  cmbIsApaster.setValue(newSelection.getIsAPaster());
			  txtReasonOfDeath.setText(newSelection.getReasonOfDeath());
			  orgid.setText(newSelection.getOrgId());
			  txttotalOfferings.setText(String.valueOf(newSelection.getTotalOfferings()));
			  if(null!=newSelection.getReasonOfLeaving()) {
			  txtReasonLeav.setText(newSelection.getReasonOfLeaving());
			  }
			  if(null!=newSelection.getDateOfJoining()) {
			  dpDateOfJoining.setValue(SystemSetting.utilToLocaDate(newSelection.getDateOfJoining()));
			  }if(null!=newSelection.getDataAddedDate()) {
			  dpDateAddedDate.setValue(SystemSetting.utilToLocaDate(newSelection.getDataAddedDate()));
			  }if(null!=newSelection.getDob()) {
			  dpDob.setValue(SystemSetting.utilToLocaDate(newSelection.getDob()));
			  }if(null!=newSelection.getDol()){
           	  dpDol.setValue(SystemSetting.utilToLocaDate(newSelection.getDol()));
			  }if(null!=newSelection.getDos()) {
			  dpDos.setValue(SystemSetting.utilToLocaDate(newSelection.getDos()));
			  }
			  if(null!=newSelection.getDom()){
			  dpDom.setValue(SystemSetting.utilToLocaDate(newSelection.getDom()));
			  }
			  if(null!=newSelection.getDohsb()) {
			  dpDohsb.setValue(SystemSetting.utilToLocaDate(newSelection.getDohsb()));
			  }if(null!=newSelection.getDowb()) {
			  dpDowb.setValue(SystemSetting.utilToLocaDate(newSelection.getDowb()));
			  }if(null!=newSelection.getDowb()) {
			  dpdod.setValue(SystemSetting.utilToLocaDate(newSelection.getDowb()));
			  }if(null!=newSelection.getDod()) {
			  dpdod.setValue(SystemSetting.utilToLocaDate(newSelection.getDod()));
			  }
			  });
   	txtMemberName.textProperty().bindBidirectional(SearchString);
   	SearchString.addListener(new ChangeListener() {
		@Override
		public void changed(ObservableValue observable, Object oldValue, Object newValue) {
			System.out.println("item Search changed");
			LoadMemberBySearch((String) newValue);
		}
	});
   	
   	
   	
   	memberMst=new MemberMst();
   	cmbSex.getItems().add("Male");
   	cmbSex.getItems().add("Female");
   	tablemember.getSelectionModel().selectedItemProperty().addListener((obs,
   			  oldSelection, newSelection) -> { if (newSelection != null) {
   			  System.out.println("getSelectionModel"); if (null != newSelection.getId())
   			  {
   			  
   				  memberMst = new MemberMst(); 
   				  memberMst.setId(newSelection.getId()); } }
   			  });
   			 
   	
   }


	private void LoadMemberBySearch(String searchData) {

		tablemember.getItems().clear();
		ArrayList cat = new ArrayList();
		RestTemplate restTemplate = new RestTemplate();
//		searchData = "cust";
		cat = RestCaller.SearchMemberByName(searchData);
		Iterator itr = cat.iterator();
		while (itr.hasNext()) {
			LinkedHashMap lm = (LinkedHashMap) itr.next();
			Object memberName = lm.get("memberName");
			Object sex = lm.get("sex");
			Object memberId = lm.get("memberId");
			Object dob = lm.get("dob");
			Object dos = lm.get("dos");
			Object id = lm.get("id");
			Object proffession = lm.get("proffession");
			Object mobileNo =  lm.get("mobileNo");
			Object headOfFamily = lm.get("headOfFamily");
			Object familyId = lm.get("familyId");
			Object email = lm.get("email");
			Object familyName=lm.get("familyName");
			Object totalOfferings = lm.get("totalOfferings");
			Object reasonOfDeath = lm.get("reasonOfDeath");
			Object previousCommunity = lm.get("previousCommunity");
			Object reasonOfLeaving = lm.get("reasonOfLeaving");
			
			String stringTotalOfferings=String.valueOf(totalOfferings);

			  
			  Date dowb; Date dohsb; Date dom;  Date
			  dateOfJoining; Date dod; 
			  Date dol; 
			   String memberIdActive;
			  
			  Date dataAddedDate;
			  
			  String dataAddedBy;  String orgId; String
			  isAPaster;
			 
	
			
			
			if (memberName != null) {
				MemberMst member = new MemberMst();
			String StringDob=	String.valueOf(dob);
			String StringDos=	String.valueOf(dos);
			java.sql.Date sqlDob=	SystemSetting.StringToSqlDateSlash(StringDob, "yyyy-MM-dd");
			java.sql.Date sqlDos=	SystemSetting.StringToSqlDateSlash(StringDos, "yyyy-MM-dd");
			
			try {
				member.setId(String.valueOf(id));
				member.setMemberName((String) memberName);
				member.setReasonOfDeath(String.valueOf(previousCommunity));
				member.setPreviousCommunity(String.valueOf(previousCommunity));
				member.setEmail(String.valueOf(email));
				member.setTotalOfferings(Double.valueOf(stringTotalOfferings));
				member.setReasonOfLeaving(String.valueOf(reasonOfLeaving));
				member.setSex((String)sex);
				member.setMemberId((String)memberId);
				member.setFamilyId(String.valueOf(familyId));
				member.setDob((sqlDob));
				member.setDos(sqlDos);
				member.setProffession((String)proffession);
				member.setMobileNo((String)mobileNo);
				member.setHeadOfFamily((String)headOfFamily);
				MemberMstList.add(member);
				tablemember.setItems(MembersMstList);
				}catch(Exception e) {
					System.out.println(e.toString());
				}
				
				FillTable();
			}
		}
	}
   @FXML
   void ActionAddedBy(KeyEvent event) {

   }
   @FXML
   void txtOrgId(ActionEvent event) {

   }
   void clear()
   {
   	txtMemberId.setText("");
   	txtfamilyId.setText("");
   	txtMemberName.setText("");
   	txtReasonLeav.setText("");
   	txtMemberIdActive.setText("");
   	txtProfession.setText("");
   	txtPreviouscommunity.setText("");
   	txtReasonOfDeath.setText("");
   	cmbIsApaster.setValue(null);
   	txtEmail.setText("");
   	txttotalOfferings.setText("");
   	txtMobileNo.setText("");
   	orgid.setText("");
   	cmbSex.setValue("");
   	dpDob.setValue(null);
   	dpdod.setValue(null);
   	dpDos.setValue(null);
   	dpDowb.setValue(null);
   	dpDohsb.setValue(null);
   	dpDom.setValue(null);
   	dpDateOfJoining.setValue(null);
   	dpDol.setValue(null);
   	dpDateAddedDate.setValue(null);
    cmbHeadOfFamily.setValue(null);
   }
   @FXML
   void actionShowAlls(ActionEvent event) {
   	showAll();

   }
   @FXML
   void actionDeletes(ActionEvent event) {


		  if(null != memberMst) {
		if(null != memberMst.getId()) {
		  RestCaller.deleteMemberMst(memberMst.getId());
		  notifyMessage(5,"Deleted!!!");
		  
		  tablemember.getItems().clear();
		  showAll();
		  
		  } }
   }

   private void loadOrgPopup() {
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/OrganisationIdpopup.fxml"));
			Parent root1;

			root1 = (Parent) fxmlLoader.load();
			Stage stage = new Stage();

			stage.initModality(Modality.APPLICATION_MODAL);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.setTitle("ABC");
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(root1));
			stage.show();
		
			
		

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
   private void showAll() {
   	
   	ResponseEntity<List<MembersMst>> orgMstResp = RestCaller.getAllMembersMst();
   	MembersMstList = FXCollections.observableArrayList(orgMstResp.getBody());
   	tablemember.getItems().clear();
		FillTable();

		
	}
   @Subscribe
	public void popupOrglistner(OrgEvent orgEvent) {

		Stage stage = (Stage) btnSave.getScene().getWindow();
		if (stage.isShowing()) {

			
			orgid.setText(orgEvent.getOrgId());
	
		

		}
		
   }
   @Subscribe
  	public void popupFamilylistner(FamilyEvent familyEvent) {

  		Stage stage = (Stage) btnSave.getScene().getWindow();
  		if (stage.isShowing()) {

  			familyIds=familyEvent.getFamilyMstId();
  		//	famId.setText(familyEvent.getFamilyMstId());
  			txtfamilyId.setText(familyEvent.getFamilyName());
  			cmbHeadOfFamily.setValue(familyEvent.getHeadOfFamily());
  			orgid.setText(familyEvent.getOrgId());
  		
  		}
  		
      }
   private void loadFamilyPopup() {
		
 		try {
 			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/familyidpopup1.fxml"));
 			Parent root1;

 			root1 = (Parent) fxmlLoader.load();
 			Stage stage = new Stage();

 			stage.initModality(Modality.APPLICATION_MODAL);
 			stage.initStyle(StageStyle.UNDECORATED);
 			stage.setTitle("ABC");
 			stage.initModality(Modality.APPLICATION_MODAL);
 			stage.setScene(new Scene(root1));
 			stage.show();
 	

 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
}
   
   
  

   @FXML
   void ActionDeath(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 txtReasonOfDeath.requestFocus();
   			    	}


   }

   @FXML
   void ActionDj(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 dpDateOfJoining.requestFocus();
   			    	}



   }

   @FXML
   void ActionDod(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 dpdod.requestFocus();
   			    	}



   }

   @FXML
   void ActionDohsb(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 dpDohsb.requestFocus();
   			    	}



   }

   @FXML
   void ActionDom(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 dpDom.requestFocus();
   			    	}



   }

   @FXML
   void ActionDos(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 dpDos.requestFocus();
   			    	}


   }

   @FXML
   void ActionDowb(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 dpDowb.requestFocus();
   			    	}



   }

   @FXML
   void ActionEmail(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 txtEmail.requestFocus();
   			    	}


   }




   @FXML
   void ActionMemActive(KeyEvent event) { 
   	if (event.getCode() == KeyCode.ENTER) {
		
   		txtMemberIdActive.requestFocus();
			    	}



   }



   @FXML
   void ActionOnDob(KeyEvent event) {

 	  if (event.getCode() == KeyCode.ENTER) {
				
 		dpDob.requestFocus();
		    	}

   }

   @FXML
   void ActionOrd(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 cmbIsApaster.requestFocus();
   			    	}



   }


   @FXML
   void ActionPrevCommunity(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 txtPreviouscommunity.requestFocus();
   			    	}



   }

   @FXML
   void ActionReason(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 txtReasonLeav.requestFocus();
   			    	}



   }


   @FXML
   void ActionSex(KeyEvent event) {
   	
   	  if (event.getCode() == KeyCode.ENTER) {
				
   		  cmbSex.requestFocus();
		    	}

   }

   @FXML
   void actionDataAded(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 dpDateAddedDate.requestFocus();
   			    	}


   }

	 @FXML
void isApasterComboAction(KeyEvent event) {
		 if (event.getCode() == KeyCode.ENTER) {
			 cmbHeadOfFamily.requestFocus();
		 }
	 }
   @FXML
   void ActionDol(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 dpDol.requestFocus();
   			    	}


   }
   @FXML
   void  headOfFamilyCmbAction (KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
   	btnSave.requestFocus();
   	 }
   }
   @FXML
   void actionHeadFamily(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 btnSave.requestFocus();
   			    	}



   }

   @FXML
   void actionMobile(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 txtMobileNo.requestFocus();
   			    	}



   }

   @FXML
   void actionOffering(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 txttotalOfferings.requestFocus();
   			    	}



   }

   @FXML
   void actionPrefession(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 txtProfession.requestFocus();
   			    	}



   }

   @FXML
   void actionSave(KeyEvent event) {
   	 if (event.getCode() == KeyCode.ENTER) {
				
   		 btnSave.requestFocus();
   			    	}


   }

	private void FillTable() {

		tablemember.setItems(MembersMstList);
		memberid.setCellValueFactory(cellData -> cellData.getValue().getMemberIdProperty());
		familyid.setCellValueFactory(cellData -> cellData.getValue().getFamilyIdProperty());
		tblmembername.setCellValueFactory(cellData -> cellData.getValue().getMemberNameProperty());
		tblsex.setCellValueFactory(cellData -> cellData.getValue().getSexProperty());
		tblemailid.setCellValueFactory(cellData -> cellData.getValue().getEmailProperty());
		tblmobno.setCellValueFactory(cellData -> cellData.getValue().getMobileNoProperty());
		tblorgid.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());
		
	}
   public void notifyMessage(int duration, String msg) {
		System.out.println("OK Event Receid");

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

 
   @FXML
   void clearFields(ActionEvent event) {
	   memberMst=new MemberMst();
	   cmbHeadOfFamily.setValue(null);
    	clear();
	   
   }
   @FXML
   void actionPrint(ActionEvent event) {

	   
	String branchCode=  SystemSetting.systemBranch;
	   try {

		   JasperPdfReportService.printMemberDtl( branchCode);
	   }catch (Exception e) {
		// TODO: handle exception
	}
	
	   
   }
}
