package com.maple.mapleclient.church.controllers;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.EventBusFactory;
import com.maple.mapleclient.entity.FamilyMst;
import com.maple.mapleclient.entity.OrgMst;
import com.maple.mapleclient.events.OrgEvent;
import com.maple.mapleclient.restService.RestCaller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
	
public class FamilyCtl {
	
	 EventBus eventBus = EventBusFactory.getEventBus();
	 FamilyMst familyMst=new FamilyMst();
	 private ObservableList<FamilyMst> familyList = FXCollections.observableArrayList();
	  String orgId = "";
	    @FXML
	    private Button btnGenerateReport;

	
	    @FXML
	    private TextField txtfamilyid;

	    @FXML
	    private TextField txtorglocation;

	    @FXML
	    private TextField txtlatitude;

	    @FXML
	    private TextField txtlongitude;

	    @FXML
	    private TextField txtorgid;
	    
	    @FXML
	    private Button btnClear;

	    @FXML
	    private TextField txtfamilyname;

	    @FXML
	    private TextField txtheadoffamily;

	    @FXML
	    private TextField txtlandline;
	    
	    @FXML
	    private TextField txtCreated;

	    @FXML
	    private DatePicker txtcreateddate;
	    @FXML
	    private TableView<FamilyMst> table2;

	    @FXML
	    private TableColumn<FamilyMst, String> familyname;

	    @FXML
	    private TableColumn<FamilyMst, String> hdFamily;

	    @FXML
	    private TableColumn<FamilyMst,String> orgname;

	    @FXML
	    private TableColumn<FamilyMst,String> familyLoc;

	    @FXML
	    private TableColumn<FamilyMst,String> loclat;

	    @FXML
	    private TableColumn<FamilyMst,String> loclong;

	    @FXML
	    private TableColumn<FamilyMst,String> familyland;

	    @FXML
	    private Button btndelete;
	    @FXML
	    private Button btnfamilymsd;

	    @FXML
	    private Button btnShowAll;
	    @FXML
	    private TextField txtfamilyMsdid;

	    
	    @FXML
		private void initialize() 
	    {
	    	
		    	eventBus.register(this);

				  table2.getSelectionModel().selectedItemProperty().addListener((obs,
				  oldSelection, newSelection) -> { if (newSelection != null) {
				  System.out.println("getSelectionModel");
				  if (null != newSelection.getOrgId())
				  {
					  familyMst = new FamilyMst(); 
					  familyMst.setId(newSelection.getId());
					  txtfamilyname.setText(newSelection.getFamilyName());
					  txtheadoffamily.setText(newSelection.getHeadOfFamily());
					  txtCreated.setText(newSelection.getOrgId());
					  txtorglocation.setText(newSelection.getFamilyLocationPlace());
					  txtlatitude.setText(newSelection.getFamilyLocationLatitude());
					  txtlongitude.setText(newSelection.getFamilyLocationLongitude());
					  txtlandline.setText(newSelection.getFamilyLandLine());
					  txtcreateddate.setValue(SystemSetting.utilToLocaDate(newSelection.getCreationDate()));
				  }}
			
				  });
				 

		 }

	    @FXML
	    void Save(ActionEvent event) 
	    {

	    	if(txtfamilyname.getText().isEmpty()) {
	    		
	    		notifyMessage(5, "Enter family name");
	    		return;
	    	}
	    	
	    	if(txtorglocation.getText().isEmpty()) {
	    		
	    		notifyMessage(5, "Enter location");
	    		return;
	    	}
	    	if(txtlatitude.getText().isEmpty()) {
	    		txtlongitude.getText().isEmpty();

	    		notifyMessage(5, "Enter longittude");
	    		return;
	    	}
	    	
	    	if(txtheadoffamily.getText().isEmpty()) {
	    		

	    		notifyMessage(5, "Enter head of family");
	    		return;
	    	}
	    	if(txtlandline.getText().isEmpty()) {
	    		

	    		notifyMessage(5, "Enter land line");
	    		return;
	    	}
	    	
	    	if(txtCreated.getText().isEmpty()) {
	    		
	    		notifyMessage(5, "Enter created by");
	    		return;
	    	}
	    	
	    	if(null!=familyMst.getId()) {
	    	familyMst.setFamilyName(txtfamilyname.getText());
	    	familyMst.setHeadOfFamily(txtheadoffamily.getText());
	    	familyMst.setOrgId(txtCreated.getText());
	    
	    	//ResponseEntity<OrgMst> orgMst = RestCaller.getOrgById(orgId);
	    	//familyMst.setOrgMst(orgMst.getBody());
	    	//System.out.println("=================="+orgMst);
	    	familyMst.setFamilyLocationPlace(txtorglocation.getText());
	    	familyMst.setFamilyLocationLatitude(txtlatitude.getText());
	    	familyMst.setFamilyLocationLongitude(txtlongitude.getText());
	    	familyMst.setFamilyLandLine(txtlandline.getText());
	    	familyMst.setCreationDate(Date.valueOf(txtcreateddate.getValue()));
	    	
	    	showAll();
	    	RestCaller.updateFamilyMst(familyMst);
	    	
	    	notifyMessage(5, "Edited Successfully Completed");
	    	  txtfamilyname.setText("");
		      txtorglocation.setText("");
		      txtlatitude.setText("");
		      txtlongitude.setText("");
		      txtheadoffamily.setText("");
		      txtorglocation.setText("");
		      txtlandline.setText("");
		      txtcreateddate.setValue(null);
		      txtCreated.setText("");
		   
	    	}else {
	    	String familyMstId = RestCaller.getVoucherNumber("FAM");
	    	familyMst.setFamilyMstId(familyMstId);
	    	familyMst.setFamilyName(txtfamilyname.getText());
	    	familyMst.setHeadOfFamily(txtheadoffamily.getText());
	    	familyMst.setOrgId(txtCreated.getText());
	    
	    	//ResponseEntity<OrgMst> orgMst = RestCaller.getOrgById(orgId);
	    	//familyMst.setOrgMst(orgMst.getBody());
	    	//System.out.println("=================="+orgMst);
	    	familyMst.setFamilyLocationPlace(txtorglocation.getText());
	    	familyMst.setFamilyLocationLatitude(txtlatitude.getText());
	    	familyMst.setFamilyLocationLongitude(txtlongitude.getText());
	    	familyMst.setFamilyLandLine(txtlandline.getText());
	    	familyMst.setCreationDate(Date.valueOf(txtcreateddate.getValue()));
	    	ResponseEntity<FamilyMst> respentity = RestCaller.FamilyCreation(familyMst);
	    	familyMst = respentity.getBody();
	    	System.out.println("++++++++++"+familyMst);
	    	
	    	familyList.add(familyMst);
	    	familyMst=new FamilyMst();
	    	notifyMessage(5, "Save Successfully Completed");
	    	 FillTable();
	        txtfamilyname.setText("");
	    	txtorglocation.setText("");
	    	txtlatitude.setText("");
	    	txtlongitude.setText("");
	    	txtheadoffamily.setText("");
	    	txtorglocation.setText("");
	    	txtlandline.setText("");
	    	txtcreateddate.setValue(null);
	    	txtCreated.setText("");
	    	txtfamilyMsdid.setText("");
	    
	    	}
	    	
	    }
	    @FXML
	    void FamilyMsdId(ActionEvent event) {
	    	String familyMstId = RestCaller.getVoucherNumber("FAM");
	    	txtfamilyMsdid.setText(familyMstId);
	    	txtfamilyname.requestFocus();

	    }
	    
	    @FXML
	    void Delete(ActionEvent event) {
	    	

			  if(null != familyMst) {
				 
			  if(null != familyMst.getId()) {
			
			  RestCaller.deleteFamilyMst(familyMst.getId());
			  notifyMessage(5,"Deleted!!!");
			  table2.getItems().clear(); 
			 showAll();
			 familyMst=  new FamilyMst();
			 
			    txtfamilyname.setText("");
		    	txtorglocation.setText("");
		    	txtlatitude.setText("");
		    	txtlongitude.setText("");
		    	txtheadoffamily.setText("");
		    	txtorglocation.setText("");
		    	txtlandline.setText("");
		    	txtcreateddate.setValue(null);
		    	txtCreated.setText("");
		    	txtfamilyMsdid.setText("");
			  } }
	    }
	    
	    
	    @FXML
	    void showAll(ActionEvent event) {
	    	showAll();

	    }

	    @FXML
	    void actionCreatedDate(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 btnGenerateReport.requestFocus();
		    	}

	    }

	    @FXML
	    void actionLocation(KeyEvent event) {

	    	if (event.getCode() == KeyCode.ENTER) {
				
	    		txtorglocation.requestFocus();
	    	}

	    }

	    @FXML
	    void actionOrgID(KeyEvent event) {

	    }

	   

	    @FXML
	    void actionsave(KeyEvent event) {

	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 loadOrgPopup();
	    		 
		    	}


	    }

	   

	    @FXML
	    void ActionorgidOn(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtlandline.requestFocus();
		    	}

	    }

	    @FXML
	    void actionCreatedBy(KeyEvent event) {
	    	
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtorglocation.requestFocus();
		    	}

	    }


	    @FXML
	    void actionLatitude(KeyEvent event) {
          if (event.getCode() == KeyCode.ENTER) {
				
        	  txtheadoffamily.requestFocus();
	    	}

	    }

	   

	    @FXML
	    void actionLongitude(KeyEvent event) {
	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtCreated.requestFocus();
		    	}
	    }

	    
	    @FXML
	    void actionfamilyname(KeyEvent event) {
	    	
	    	if (event.getCode() == KeyCode.ENTER) {
					
	    		txtlatitude.requestFocus();
		    	}


	    }

	    @FXML
	    void actionheadOfFamily(KeyEvent event) {

	    	 if (event.getCode() == KeyCode.ENTER) {
					
	    		 txtlongitude.requestFocus();
		    	}

	    }
          private void showAll() {
	    	
	    	ResponseEntity<List<FamilyMst>> FamilyMstResp = RestCaller.getAllFamily();
	    	familyList = FXCollections.observableArrayList(FamilyMstResp.getBody());
	    	table2.getItems().clear();
			FillTable();

			
		}
	    

		private void FillTable() {

			table2.setItems(familyList);
			familyname.setCellValueFactory(cellData -> cellData.getValue().getFamilyNameProperty());
			hdFamily.setCellValueFactory(cellData -> cellData.getValue().getHeadOfFamilyProperty());
			familyLoc.setCellValueFactory(cellData -> cellData.getValue().getFamilyLocationPlaceProperty());
			orgname.setCellValueFactory(cellData -> cellData.getValue().getOrgIdProperty());
			loclat.setCellValueFactory(cellData -> cellData.getValue().getFamilyMstIdProperty());
			loclong.setCellValueFactory(cellData -> cellData.getValue().getFamilyLocationLongtitudeProperty());
			familyland.setCellValueFactory(cellData -> cellData.getValue().getFamilyLandLineProperty());
			
			
		}
	    
	    private void loadOrgPopup() {
			
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/OrganisationIdpopup.fxml"));
				Parent root1;

				root1 = (Parent) fxmlLoader.load();
				Stage stage = new Stage();

				stage.initModality(Modality.APPLICATION_MODAL);
				stage.initStyle(StageStyle.UNDECORATED);
				stage.setTitle("ABC");
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setScene(new Scene(root1));
				stage.show();
				txtcreateddate.requestFocus();
			

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	    
	    @Subscribe
		public void popupOrglistner(OrgEvent orgEvent) {

			Stage stage = (Stage) btnGenerateReport.getScene().getWindow();
			if (stage.isShowing()) {

				txtCreated.setText(orgEvent.getOrgId());

				orgId=orgEvent.getId();
				System.out.println("====="+orgEvent.getId());

			}

		}
	    public void notifyMessage(int duration, String msg) {
			System.out.println("OK Event Receid");

			Image img = new Image("done.png");
			Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
					.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
					.onAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							System.out.println("clicked on notification");
						}
					});
			notificationBuilder.darkStyle();
			notificationBuilder.show();
		}

	    @FXML
	    void clearFields(ActionEvent event) {
	    	
	    	
	    	FamilyMst familyMst =new FamilyMst();
	    	 txtfamilyname.setText("");
		    	txtorglocation.setText("");
		    	txtlatitude.setText("");
		    	txtlongitude.setText("");
		    	txtheadoffamily.setText("");
		    	txtorglocation.setText("");
		    	txtlandline.setText("");
		    	txtcreateddate.setValue(null);
		    	txtCreated.setText("");
		    	txtfamilyMsdid.setText("");
	    }

	}

	
	

