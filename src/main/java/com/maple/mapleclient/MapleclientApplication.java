package com.maple.mapleclient;

 
 

import org.springframework.stereotype.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.*;

import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.eventbus.EventBus;
import com.maple.maple.util.SystemSetting;
import com.maple.maple.util.TSCBarcode;
import com.maple.mapleclient.controllers.MainFrameController;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.restService.RestCaller;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;


@Configuration
@PropertySource("classpath:application.properties")
@SpringBootApplication
@EnableScheduling
public class MapleclientApplication extends Application implements EnvironmentAware{
	
	private  Environment environment;
		 
	private static final Logger logger = LoggerFactory.getLogger(TSCBarcode.class);

	
	 
	

	public static Connection conn;
	public static MainFrameController mainFrameController;
	public  ConfigurableApplicationContext context;
    public static Parent rootNode;
   public static FXMLLoader loader;
 
    public static AnchorPane mainWorkArea;
    public static  Node payments;
 
    public static MapleclientApplication mapleclientApplication;
    public static Stage primarySate;
    
  
    
 
    @Bean
    public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

		PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
		properties.setLocation(new FileSystemResource("application.properties"));
		properties.setIgnoreResourceNotFound(false);

		return properties;
	}
    
    
    @Override
    public void init() throws Exception {
    	
    	mapleclientApplication = this;
    	//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	
    	/*
    	 * Set Current System Date
    	 */
    	SystemSetting.systemDate  = new java.util.Date();
    	
    
    	
    	 
        SpringApplicationBuilder builder = new SpringApplicationBuilder(MapleclientApplication.class);
        context = builder.run(getParameters().getRaw().toArray(new String[0]));
 
         loader = new FXMLLoader(getClass().getResource("/fxml/login.fxml"));
       
        loader.setControllerFactory(context::getBean);
        rootNode = loader.load();
//        insertItemmst(getDbConn("memdb"));
        
        logger.info("LOGGING DONE");
       
    }
 
    
   
    
  /* 	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
   	    PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
   	    properties.setLocation(new FileSystemResource("application.properties"));
   	    properties.setIgnoreResourceNotFound(false);
   	    return properties;
   	}
      
*/
    
   	
	public static void main(String[] args) {
		//SpringApplication.run(DTclientApplication.class, args);
		
		 
	
		
		launch(args);
		// Create an In Memmory DB
		
		
		
				
				
	}
	
	
	
	 @Override
	    public void start(Stage primaryStage) throws Exception {
	
		 
		 primarySate = primaryStage;
		 
				
		 logger.info("LOGGING DONE");
	        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
	        double width = visualBounds.getWidth()/2;
	        double height = visualBounds.getHeight()/1.5;
	 
	        primaryStage.setScene(new Scene(rootNode, width, height));
	        primaryStage.centerOnScreen();
	        primaryStage.show();
	    }
	  @Override
	    public void stop() throws Exception {
	        context.close();
	    }
	  public HostServices getHostServerFromMain() {
		  return getHostServices();
		  
	  }


	@Override
	public void setEnvironment(Environment environment) {
		 
		this.environment = environment;
		 final String  posformat = environment.getProperty("print.posformat");
		 final String  hascashdrawer = environment.getProperty("print.hascashdrawer");
		 final String  logo_name = environment.getProperty("print.logo_name");
		 final String  reportpath = environment.getProperty("reportpath");
		 final String  mycompany = environment.getProperty("mycompany");
		 final String  myhost = environment.getProperty("myhost");
		 final String  myhost2 = environment.getProperty("myhost2");
		 final String  logox = environment.getProperty("logox");
		 final String  logoy = environment.getProperty("logoy");
		 final String  logow = environment.getProperty("logow");
		 final String  logoh = environment.getProperty("logoh");
		 
		 final String posinvoicetitle1=environment.getProperty("posinvoicetitle1");
		 final String posinvoicetitle2=environment.getProperty("posinvoicetitle2");
		 final String posinvoicetitle3=environment.getProperty("posinvoicetitle3");
		 final String posinvoicetitle4=environment.getProperty("posinvoicetitle4");
		 final String posinvoicetitle5=environment.getProperty("posinvoicetitle5");
		  
		 final String  title1x = environment.getProperty("title1x");
		 final String  title1y = environment.getProperty("title1y");
		 final String  title2x = environment.getProperty("title2x");
		 final String  title2y = environment.getProperty("title2y");
		 final String  title3x = environment.getProperty("title3x");
		 final String  title3y = environment.getProperty("title3y");
		 final String  title4x = environment.getProperty("title4x");
		 final String  title4y = environment.getProperty("title4y");
		 final String  title5x = environment.getProperty("title5x");
		 final String  title5y = environment.getProperty("title5y");
		 final String  barcodex = environment.getProperty("barcodex");
		 final String  barcodey = environment.getProperty("barcodey");
		 final String  barcodeManufactureDatex = environment.getProperty("barcodeManufactureDatex");
		 final String  barcodeManufactureDatey = environment.getProperty("barcodeManufactureDatey");
		 
		 final String  barcodeExpiryDatex = environment.getProperty("barcodeExpiryDatex");
		 final String  barcodeExpiryDatey = environment.getProperty("barcodeExpiryDatey");
		 final String  barcodeItemNamex = environment.getProperty("barcodeItemNamex");
		 final String  barcodeItemNamey = environment.getProperty("barcodeItemNamey");
		 final String  barcodeIngLine1x = environment.getProperty("barcodeIngLine1x");
		 final String  barcodeIngLine1y = environment.getProperty("barcodeIngLine1y");
		 final String  barcodeIngLine2x = environment.getProperty("barcodeIngLine2x");
		 final String  barcodeIngLine2y = environment.getProperty("barcodeIngLine2y");
		 final String  barcodeMrpx = environment.getProperty("barcodeMrpx");
		 final String  barcodeMrpy = environment.getProperty("barcodeMrpy");
		 
	
		 final String  BARCODE_SIZE = environment.getProperty("BARCODE_SIZE");
		 final String  BARCODE_FORMAT = environment.getProperty("BARCODE_FORMAT");
		 
		
		 final String  NUTRITION_SIZE = environment.getProperty("NUTRITION_SIZE");
		 final String  NUTRITION_FORMAT = environment.getProperty("NUTRITION_FORMAT");
		 
		 
		 
		 final String  BARCODETITLE = environment.getProperty("BARCODETITLE");
		 final String  DLLPATH = environment.getProperty("DLLPATH");
		 final String PRINTER_NAME=environment.getProperty("PRINTER_NAME");
		 final String NUTRITION_FACTS_PRINTER=environment.getProperty("NUTRITION_FACTS_PRINTER");
		 
		 final String posInvoiceBottomLine = environment.getProperty("POS_INVOICE_BOTTOM_LINE");
		 final String posInvoiceBottomLine2 = environment.getProperty("POS_INVOICE_BOTTOM_LINE2");
		 final String posInvoiceBottomLine3 = environment.getProperty("POS_INVOICE_BOTTOM_LINE3");
		 
		 SystemSetting.BARCODE_SIZE =BARCODE_SIZE;
		 SystemSetting.BARCODE_FORMAT =BARCODE_FORMAT;
		 SystemSetting.BARCODETITLE=BARCODETITLE;
		 SystemSetting.DLLPATH=DLLPATH;
		 
		 
		
		 final String  ONLINE_SALES_PREFIX = environment.getProperty("ONLINE_SALES_PREFIX");
		 final String  POS_SALES_PREFIX = environment.getProperty("POS_SALES_PREFIX");
		 final String  WHOLE_SALES_PREFIX = environment.getProperty("WHOLE_SALES_PREFIX");
		 final String  KOT_SALES_PREFIX = environment.getProperty("KOT_SALES_PREFIX");
		 
		 if(ONLINE_SALES_PREFIX.length()>0) {
			 SystemSetting.ONLINE_SALES_PREFIX = ONLINE_SALES_PREFIX;
		 }
		 if(POS_SALES_PREFIX.length()>0) {
			 SystemSetting.POS_SALES_PREFIX = POS_SALES_PREFIX;
		 }
		 if(WHOLE_SALES_PREFIX.length()>0) {
			 SystemSetting.WHOLE_SALES_PREFIX = WHOLE_SALES_PREFIX;
		 }
		 if(KOT_SALES_PREFIX.length()>0) {
			 SystemSetting.KOT_SALES_PREFIX = KOT_SALES_PREFIX;
		 }
		 SystemSetting.setNUTRITION_FORMAT(NUTRITION_FORMAT);
		 SystemSetting.setNUTRITION_SIZE(NUTRITION_SIZE);
		 
		 SystemSetting.setPosinvoicetitle1(posinvoicetitle1);
		 SystemSetting.setPosinvoicetitle2(posinvoicetitle2);
		 SystemSetting.setPosinvoicetitle3(posinvoicetitle3);
		 SystemSetting.setPosinvoicetitle4(posinvoicetitle4);
		 SystemSetting.setPosinvoicetitle5(posinvoicetitle5);
				 
		 RestCaller.HOST = myhost;
		 RestCaller.HOST2 = myhost2;
		 
				 
		 SystemSetting.setPosFormat(posformat);
		 SystemSetting.setCashDrawerPresent(hascashdrawer);
		 SystemSetting.setLogo_name(logo_name);
		 SystemSetting.setReportpath(reportpath);	
		 SystemSetting.setReportpath(reportpath);	
		 
		 SystemSetting.setPosInvoiceBottomLine(posInvoiceBottomLine);
		 SystemSetting.setPosInvoiceBottomLine2(posInvoiceBottomLine2);
		 SystemSetting.setPosInvoiceBottomLine3(posInvoiceBottomLine3);
		 
		 
		 
		 SystemSetting.setPrinter_name(PRINTER_NAME);
		 if(logox.length()>0) {
			 SystemSetting.setLogox(Integer.parseInt(logox)); 
		 }
		 
		 SystemSetting.setNutrition_facts_printer(NUTRITION_FACTS_PRINTER);
		 if(logox.length()>0) {
			 SystemSetting.setLogox(Integer.parseInt(logox)); 
		 }
		 if(logoy.length()>0) {
			 SystemSetting.setLogoy(Integer.parseInt(logoy)); 
		 }
		 if(logow.length()>0) {
			 SystemSetting.setLogow(Integer.parseInt(logow)); 
		 }
		 
		
		 
		 if(logoh.length()>0) {
			 SystemSetting.setLogoh(Integer.parseInt(logoh)); 
		 }
		 
		 if(title1x.length()>0) {
			 SystemSetting.setTitle1x(Integer.parseInt(title1x)); 
		 }
		 
		 if(title2x.length()>0) {
			 SystemSetting.setTitle2x(Integer.parseInt(title2x)); 
		 }
		 
		 if(title3x.length()>0) {
			 SystemSetting.setTitle3x(Integer.parseInt(title3x)); 
		 }
		 
		 if(title4x.length()>0) {
			 SystemSetting.setTitle4x(Integer.parseInt(title4x)); 
		 }
		 
		 
		 if(title5x.length()>0) {
			 SystemSetting.setTitle5x(Integer.parseInt(title5x)); 
		 }
		 
		 if(title1y.length()>0) {
			 SystemSetting.setTitle1y(Integer.parseInt(title1y)); 
		 }
		 
		 if(title2y.length()>0) {
			 SystemSetting.setTitle2y(Integer.parseInt(title2y)); 
		 }
		 if(title3y.length()>0) {
			 SystemSetting.setTitle3y(Integer.parseInt(title3y)); 
		 }
		 
		 
		 if(title4y.length()>0) {
			 SystemSetting.setTitle4y(Integer.parseInt(title4y)); 
		 }
		  
 
		 if(title5y.length()>0) {
			 SystemSetting.setTitle5y(Integer.parseInt(title5y)); 
		 }
		  
		/*
		 * if(barcodex.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodex)); }
		 * 
		 * if(barcodey.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodey)); }
		 * 
		 * if(barcodeManufactureDatex.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeManufactureDatex)); }
		 * 
		 * if(barcodeManufactureDatey.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeManufactureDatey)); }
		 * if(barcodeExpiryDatex.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeExpiryDatex)); }
		 * 
		 * if(barcodeExpiryDatey.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeExpiryDatey)); }
		 * 
		 * if(barcodeItemNamex.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeItemNamex)); }
		 * 
		 * if(barcodeItemNamey.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeItemNamey)); }
		 * 
		 * 
		 * if(barcodeIngLine1x.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeIngLine1x)); }
		 * if(barcodeIngLine1y.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeIngLine1y)); }
		 * 
		 * if(barcodeIngLine2x.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeIngLine2x)); }
		 * 
		 * if(barcodeIngLine2y.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeIngLine2y)); }
		 * 
		 * if(barcodeMrpx.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeMrpx)); }
		 * 
		 * if(barcodeMrpy.length()>0) {
		 * SystemSetting.setTitle5y(Integer.parseInt(barcodeMrpy)); }
		 */
		 
		 
		 
		 
	}
	
	public static Connection getDbConn(String dbName) throws UnknownHostException, Exception {
		// String driver = "org.apache.derby.jdbc.EmbeddedDriver";


		String password = "thisisthepassword913781";

		// Class.forName(driver).newInstance();
		try {

			/*
			 * NetworkServerControl server = new
			 * NetworkServerControl(InetAddress.getByName("localhost"), 1527);
			 * server.start(null); while (true) { Thread.sleep(500); try { //server.ping();
			 * break; } catch (Exception e) { } }
			 */

			Properties props = new Properties(); // connection properties

			props.put("user", "xposdemo");
			props.put("password", password);
			// String dbName = dbNamewithoutDB + "DB"; // the name of the database

			String url = "";

			url = "jdbc:derby:memory:memdb;create=true";

			conn = DriverManager.getConnection(url);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			System.out.println("Connected to local db and created database " + dbName);

			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			conn.setAutoCommit(true);

		} catch (SQLException sqle) {
			System.out.println("Error Connecting Client DB");
			return null;
		}
		return conn;
	}
	public void insertItemmst(Connection Conn) throws SQLException {
		String itemName = "";
		String barCode = "";
		Double mrp = 0.0;
		String unitname = "";
		Integer qty = 0;
		Double cess = 0.0;
		Double tax = 0.0;
		String itemId = "";
		String catid = "";
		String hsnCode = "";
		String itemCode = "";
		String unitId = "";
		String sqlstr1 = "CREATE TABLE ITEM_MST (item_name VARCHAR(100),barcode VARCHAR(50)"
				+ ",unit_name VARCHAR(50) ,standard_price DOUBLE,cess DOUBLE,"
				+ "tax_rate DOUBLE,item_id VARCHAR(100),unit_id VARCHAR(50),category_id VARCHAR(50),"
				+ "hsn_code VARCHAR(50),item_code VARCHAR(50))";
		PreparedStatement pst1 = Conn.prepareStatement(sqlstr1);
		 pst1.execute();
		ArrayList itemList = RestCaller.getAllItemsForInMemDb();
		Iterator itr = itemList.iterator();
		while (itr.hasNext()) 
		{
			List element = (List) itr.next();
			itemName = (String) element.get(0);
			barCode = (String) element.get(1);
			unitname = (String) element.get(2);
			mrp = (Double) element.get(3);
			cess = (Double) element.get(4);
			tax = (Double) element.get(5);
			itemId = (String) element.get(6);
			unitId = (String) element.get(7);
			catid = (String) element.get(8);
			hsnCode = (String) element.get(9);
			itemCode = (String) element.get(14);
			
			try
			{
			String sqlstr2 = "insert into item_mst (item_name,barcode,unit_name,standard_price,"
					+ "cess,tax_rate,item_id,unit_id,category_id,hsn_code,"
					+ "item_code) values ('"+
					itemName+"','"+barCode+"','"+unitname+"',"+mrp+","+cess+","+tax+","+"'"+itemId
					+"','"+unitId+"','"+catid+"','"+hsnCode+"','"+itemCode+"')";
			System.out.println(sqlstr2);
			PreparedStatement pst2 = Conn.prepareStatement(sqlstr2);
			pst2.execute();
			
			pst2.close();
			}
			
			catch (Exception e) {
				// TODO: handle exception
			}
		
		}
		pst1.close();
//
		String sql3 = "select * from item_mst";
		PreparedStatement pst3 = Conn.prepareStatement(sql3);

		ResultSet rs3 = pst3.executeQuery();

		while (rs3.next()) {
			itemName = rs3.getString("item_name");
			System.out.println(itemName);
		}
		pst3.close();
		rs3.close();
//		return itemName;

	}
}
