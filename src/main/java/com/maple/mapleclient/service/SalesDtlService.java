package com.maple.mapleclient.service;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public interface SalesDtlService {

	String AddSalesDtl(String hdrId, String itemId, String unitId, String batch, double qty, double rate);
	
	
	String AddSalesDtl(String hdrId, String itemId,  double qty, double rate);
	
	String AddSalesDtl(String hdrId, String itemId,  double qty, double rate, String unitId);



}
