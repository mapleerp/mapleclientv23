package com.maple.mapleclient.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.ItemBatchExpiryDtl;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.MultiUnitMst;
import com.maple.mapleclient.entity.PriceDefenitionMst;
import com.maple.mapleclient.entity.PriceDefinition;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.TaxMst;
import com.maple.mapleclient.entity.UnitMst;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.mapleclient.service.SalesDtlService;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
@Component
@Configuration
@Service
public class SalesDtlServiceImpl implements SalesDtlService {


	@Override
	public String AddSalesDtl(String hdrId, String itemId, String unitId, String batch, double qty, double rate) {
		
		ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();


		SalesDtl salesDtl = new SalesDtl();

		SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(hdrId);

		ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(itemId);
		ItemMst itemMst = itemMstResp.getBody();

		ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(unitId);
		UnitMst unitMst = unitMstResp.getBody();

		ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(itemMst.getId(), unitId);
		if (!unitId.equalsIgnoreCase(itemMst.getUnitId())) {
			if (null == getmulti.getBody()) {
				return "Please Add the item in Multi Unit";
			}
		}
		ArrayList items = new ArrayList();

		items = RestCaller.getSingleStockItemByName(itemMst.getItemName(), batch);

		Double chkQty = 0.0;
		itemId = null;
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			List element = (List) itr.next();
			chkQty = (Double) element.get(4);
			itemId = (String) element.get(7);
		}

		if (!unitId.equalsIgnoreCase(itemMst.getUnitId())) {

			Double conversionQty = RestCaller.getConversionQty(itemMst.getId(), unitId, itemMst.getUnitId(), qty);
			System.out.println(conversionQty);
			if (chkQty < conversionQty) {

				return "Not in Stock!!!";
			}
		} else if (chkQty < qty) {

			return "Not in Stock!!!";
		}
		
		
		//==================================

		Double itemsqty = 0.0;
		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),itemMst.getId(), batch);
		
		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
		if (saleListItemTable.size() > 1) {
			Double PrevQty = 0.0;
			for (SalesDtl saleDtl : saleListItemTable) {
				if (!saleDtl.getUnitId().equalsIgnoreCase(itemMst.getUnitId())) {
					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
							itemMst.getUnitId(), saleDtl.getQty());
				} else {
					PrevQty = saleDtl.getQty();
				}
				itemsqty = itemsqty + PrevQty;
			}
		} else {
			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId,batch);
		}
		if (!unitMst.getId().equalsIgnoreCase(itemMst.getUnitId())) {
			Double conversionQty = RestCaller.getConversionQty(itemMst.getId(),
					unitMst.getId(), itemMst.getUnitId(),
					qty);
			System.out.println(conversionQty);
			if (chkQty < itemsqty + conversionQty) {
				return "Not in Stock!!!";
			}
		}

		else if (chkQty < itemsqty + qty) {
		
			return "No Stock!!";
		}
		if (null == salesDtl) {
			salesDtl = new SalesDtl();
		}
		if (null != salesDtl.getId()) {

			RestCaller.deleteSalesDtl(salesDtl.getId());


			if (null != salesDtl.getSchemeId()) {
				return "";
			}

		}

		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setBatchCode(batch);
		
			
		salesDtl.setQty(qty);

		salesDtl.setItemCode(itemMst.getItemCode());
		Double mrpRateIncludingTax = 00.0;
		mrpRateIncludingTax = rate;
		salesDtl.setMrp(mrpRateIncludingTax);

		Double taxRate = 00.0;

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
		salesDtl.setBarcode(item.getBarCode());
//		salesDtl.setUnitName(unitMst.getUnitName());
		salesDtl.setStandardPrice(item.getStandardPrice());

//		if(null != item.getTaxRate())
//		{
//			salesDtl.setTaxRate(item.getTaxRate());
//			taxRate = item.getTaxRate();
//
//		} else {
//			salesDtl.setTaxRate(0.0);
//		}
		salesDtl.setItemId(item.getId());
		

		if(batch != null)
		{
			ResponseEntity<List<ItemBatchExpiryDtl>> batchExpiryDtlResp = RestCaller.
					getItemBatchExpByItemAndBatch(salesDtl.getItemId(), salesDtl.getBatch());
			
			List<ItemBatchExpiryDtl> itemBatchExpiryDtlList= batchExpiryDtlResp.getBody();
			if(itemBatchExpiryDtlList.size()>0 && null != itemBatchExpiryDtlList)
			{
			ItemBatchExpiryDtl itemBatchExpiryDtl = itemBatchExpiryDtlList.get(0);
			
			if(null != itemBatchExpiryDtl)
			{
				String expirydate = SystemSetting.UtilDateToString(itemBatchExpiryDtl.getExpiryDate(), "yyyy-MM-dd");
				salesDtl.setExpiryDate(java.sql.Date.valueOf(expirydate));
			}
		}
		}

		salesDtl.setUnitId(unitMst.getId());
		salesDtl.setUnitName(unitMst.getUnitName());

		// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());

		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.getBody().size() > 0) {
			for (TaxMst taxMst : getTaxMst.getBody()) {

				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getAccountName();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//						BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//						BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
					ResponseEntity<TaxMst> taxMst1 = RestCaller.getTaxMstByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
					if (null != taxMst1.getBody()) {
						
						salesDtl.setTaxRate(taxMst1.getBody().getTaxRate());
					}
				} 
				
				
				else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesDtl.setCgstTaxRate(0.0);
						salesDtl.setCgstAmount(0.0);
						salesDtl.setSgstTaxRate(0.0);
						salesDtl.setSgstAmount(0.0);

						salesDtl.setTaxRate(taxMst.getTaxRate());
						salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//							BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setIgstAmount(igstAmount.doubleValue());
					}
				}
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesDtl.setCessRate(taxMst.getTaxRate());
//						BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCessAmount(cessAmount.doubleValue());
					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtl.setAddCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//							Double.valueOf(txtRate.getText()));
//					salesDtl.setAddCessAmount(cessAmount.doubleValue());
				}

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax)
					/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate()
							+ salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
			BigDecimal addcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
		}

		else {
			
// verificed
			
			if (unitMst.getId().equalsIgnoreCase(item.getUnitId())) {
				salesDtl.setStandardPrice(item.getStandardPrice());
			} else {
	
				ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),unitMst.getId());
				MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
				salesDtl.setStandardPrice(multiUnitMst.getPrice());
	
			}
			if(null != item.getTaxRate())
				{
					salesDtl.setTaxRate(item.getTaxRate());
					taxRate = item.getTaxRate();
		
				} else {
					salesDtl.setTaxRate(0.0);
				}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
			
			// if Discount
			
			
			// Calculate discount on base price
			if(null != salesTransHdr.getAccountHeads().getDiscountProperty())
			{
				
			
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE"))
			{
				calcDiscountOnBasePrice(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl);
				
			}
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP"))
			{
//				salesDtl.setTaxRate(0.0);
				calcDiscountOnMRP(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl,rate);
			}
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX"))
			{
			ambrossiaDiscount(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl,rate);
			}
			}
			else
			{
				
				double cessAmount = 0.0;
				double cessRate = 0.0;
				salesDtl.setRate(rateBeforeTax);
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C"))
				{
					if (item.getCess() > 0) {
						cessRate = item.getCess();
						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
					
					salesDtl.setRate(rateBeforeTax);
					cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;
					}
					else {
						cessAmount = 0.0;
						cessRate = 0.0;
					}
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setCessRate(cessRate);
					salesDtl.setCessAmount(cessAmount);
				}
				
				salesDtl.setStandardPrice(rate);
				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesDtl.setCgstTaxRate(cgstTaxRate);

				salesDtl.setSgstTaxRate(sgstTaxRate);
				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					salesDtl.setSgstTaxRate(taxRate / 2);

					salesDtl.setCgstTaxRate(taxRate / 2);

					Double cgstAmt = 0.0,sgstAmt = 0.0;
					cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
					bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
							
					salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
					sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
					bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
				

				} else {
					salesDtl.setSgstTaxRate(0.0);

					salesDtl.setCgstTaxRate(0.0);

					salesDtl.setCgstAmount(0.0);

					salesDtl.setSgstAmount(0.0);

					salesDtl.setIgstTaxRate(taxRate);
					salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				}
			}
			}
//			

			String companyState = SystemSetting.getUser().getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				salesDtl.setSgstTaxRate(taxRate / 2);

				salesDtl.setCgstTaxRate(taxRate / 2);

				Double cgstAmt =0.0,sgstAmt =0.0;
				cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
				bdcgstAmt = bdcgstAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setCgstAmount(bdcgstAmt.doubleValue());
				sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
				bdsgstAmt = bdsgstAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setIgstAmount(0.0);
			

			} else {
				salesDtl.setSgstTaxRate(0.0);

				salesDtl.setCgstTaxRate(0.0);

				salesDtl.setCgstAmount(0.0);

				salesDtl.setSgstAmount(0.0);

				salesDtl.setIgstTaxRate(taxRate);
				salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

			}
		

		
		BigDecimal settoamount = new BigDecimal(
				(rate *salesDtl.getRate() ));
		
		double includingTax= (settoamount.doubleValue()*salesDtl.getTaxRate())/100;
		double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(setamount.doubleValue());

		salesDtl.setAddCessRate(0.0);
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp1 = RestCaller
				.getPriceDefenitionMstByName("MRP");
		PriceDefenitionMst priceDefenitionMst1 = priceDefenitionMstResp1.getBody();
		if(null != priceDefenitionMst1)
		{
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(),sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setMrp(priceDefinition.getAmount());
			}
		}
		
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if(null != priceDefenitionMst)
		{
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(),sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setCostPrice(priceDefinition.getAmount());
			}
		}
		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		salesDtl = respentity.getBody();

//		//---------------offer---------------

		ResponseEntity<List<SalesDtl>> respentityList = RestCaller.getSalesDtl(salesDtl.getSalesTransHdr());

		List<SalesDtl> salesDtlList = respentityList.getBody();



		return "Item Successfully added";
	
	}

	@Override
	public String AddSalesDtl(String hdrId, String itemId, double qty, double rate, String unitId) {

		
		ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
		Boolean notinstock = false;

		SalesDtl salesDtl = new SalesDtl();

		SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(hdrId);

		ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(itemId);
		ItemMst itemMst = itemMstResp.getBody();

		ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(unitId);
		UnitMst unitMst = unitMstResp.getBody();

		ResponseEntity<MultiUnitMst> getmulti = RestCaller.getMultiUnitbyprimaryunit(itemMst.getId(), unitId);
		if (!unitId.equalsIgnoreCase(itemMst.getUnitId())) {
			if (null == getmulti.getBody()) {
				return "Please Add the item in Multi Unit";
			}
		}
		
//		ArrayList items = new ArrayList();
//		items = RestCaller.getSingleStockItemByName(itemMst.getItemName(), batch);
//		Double chkQty = 0.0;
//		itemId = null;
//		Iterator itr = items.iterator();
//		while (itr.hasNext()) {
//			List element = (List) itr.next();
//			chkQty = (Double) element.get(4);
//			itemId = (String) element.get(7);
//		}
//
//		if (!unitId.equalsIgnoreCase(itemMst.getUnitId())) {
//
//			Double conversionQty = RestCaller.getConversionQty(itemMst.getId(), unitId, itemMst.getUnitId(), qty);
//			System.out.println(conversionQty);
//			if (chkQty < conversionQty) {
//
//				return "Not in Stock!!!";
//			}
//		} else if (chkQty < qty) {
//
//			return "Not in Stock!!!";
//		}
		
		
		//==================================
		
		Map<String, Double> map = new HashMap<String, Double>();
		map = RestCaller.getRequiredMaterial(itemMst.getId(),qty);
		
		if(map.size()==0)
		{
			notinstock = true;
			
			return "Not in Stock";

		}
		
		for(Map.Entry<String, Double> batchAndQty : map.entrySet())
		{
			
			String batch = batchAndQty.getKey();
			Double quantity = batchAndQty.getValue();

//		Double itemsqty = 0.0;
//		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),itemMst.getId(), batch);
//		
//		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
//		if (saleListItemTable.size() > 1) {
//			Double PrevQty = 0.0;
//			for (SalesDtl saleDtl : saleListItemTable) {
//				if (!saleDtl.getUnitId().equalsIgnoreCase(itemMst.getUnitId())) {
//					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
//							itemMst.getUnitId(), saleDtl.getQty());
//				} else {
//					PrevQty = saleDtl.getQty();
//				}
//				itemsqty = itemsqty + PrevQty;
//			}
//		} else {
//			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId,batch);
//		}
//		if (!unitMst.getId().equalsIgnoreCase(itemMst.getUnitId())) {
//			Double conversionQty = RestCaller.getConversionQty(itemMst.getId(),
//					unitMst.getId(), itemMst.getUnitId(),
//					qty);
//			System.out.println(conversionQty);
//			if (chkQty < itemsqty + conversionQty) {
//				return "Not in Stock!!!";
//			}
//		}
//
//		else if (chkQty < itemsqty + qty) {
//		
//			return "No Stock!!";
//		}
//		if (null == salesDtl) {
			salesDtl = new SalesDtl();
//		}
//		if (null != salesDtl.getId()) {
//
//			RestCaller.deleteSalesDtl(salesDtl.getId());
//
//
//			if (null != salesDtl.getSchemeId()) {
//				return "";
//			}
//
//		}

		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setBatchCode(batch);
		
			
		salesDtl.setQty(quantity);

		salesDtl.setItemCode(itemMst.getItemCode());
		Double mrpRateIncludingTax = 00.0;
		mrpRateIncludingTax = rate;
		salesDtl.setMrp(mrpRateIncludingTax);

		Double taxRate = 00.0;

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
		salesDtl.setBarcode(item.getBarCode());
//		salesDtl.setUnitName(unitMst.getUnitName());
		salesDtl.setStandardPrice(item.getStandardPrice());

//		if(null != item.getTaxRate())
//		{
//			salesDtl.setTaxRate(item.getTaxRate());
//			taxRate = item.getTaxRate();
//
//		} else {
//			salesDtl.setTaxRate(0.0);
//		}
		salesDtl.setItemId(item.getId());
		

		if(batch != null)
		{
			ResponseEntity<List<ItemBatchExpiryDtl>> batchExpiryDtlResp = RestCaller.
					getItemBatchExpByItemAndBatch(salesDtl.getItemId(), salesDtl.getBatch());
			
			List<ItemBatchExpiryDtl> itemBatchExpiryDtlList= batchExpiryDtlResp.getBody();
			if(itemBatchExpiryDtlList.size()>0 && null != itemBatchExpiryDtlList)
			{
			ItemBatchExpiryDtl itemBatchExpiryDtl = itemBatchExpiryDtlList.get(0);
			
			if(null != itemBatchExpiryDtl)
			{
				String expirydate = SystemSetting.UtilDateToString(itemBatchExpiryDtl.getExpiryDate(), "yyyy-MM-dd");
				salesDtl.setExpiryDate(java.sql.Date.valueOf(expirydate));
			}
		}
		}

		salesDtl.setUnitId(unitMst.getId());
		salesDtl.setUnitName(unitMst.getUnitName());

		// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());

		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.getBody().size() > 0) {
			for (TaxMst taxMst : getTaxMst.getBody()) {

				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//						BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//						BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
					ResponseEntity<TaxMst> taxMst1 = RestCaller.getTaxMstByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
					if (null != taxMst1.getBody()) {
						
						salesDtl.setTaxRate(taxMst1.getBody().getTaxRate());
					}
				} 
				
				
				else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesDtl.setCgstTaxRate(0.0);
						salesDtl.setCgstAmount(0.0);
						salesDtl.setSgstTaxRate(0.0);
						salesDtl.setSgstAmount(0.0);

						salesDtl.setTaxRate(taxMst.getTaxRate());
						salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//							BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setIgstAmount(igstAmount.doubleValue());
					}
				}
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesDtl.setCessRate(taxMst.getTaxRate());
//						BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCessAmount(cessAmount.doubleValue());
					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtl.setAddCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//							Double.valueOf(txtRate.getText()));
//					salesDtl.setAddCessAmount(cessAmount.doubleValue());
				}

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax)
					/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate()
							+ salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
			BigDecimal addcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
		}

		else {
			
// verificed
			
			if (unitMst.getId().equalsIgnoreCase(item.getUnitId())) {
				salesDtl.setStandardPrice(item.getStandardPrice());
			} else {
	
				ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),unitMst.getId());
				MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
				salesDtl.setStandardPrice(multiUnitMst.getPrice());
	
			}
			if(null != item.getTaxRate())
				{
					salesDtl.setTaxRate(item.getTaxRate());
					taxRate = item.getTaxRate();
		
				} else {
					salesDtl.setTaxRate(0.0);
				}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
			
			// if Discount
			
			
			// Calculate discount on base price
			if(null != salesTransHdr.getAccountHeads().getDiscountProperty())
			{
				
			
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE"))
			{
				calcDiscountOnBasePrice(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl);
				
			}
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP"))
			{
//				salesDtl.setTaxRate(0.0);
				calcDiscountOnMRP(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl,rate);
			}
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX"))
			{
			ambrossiaDiscount(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl,rate);
			}
			}
			else
			{
				
				double cessAmount = 0.0;
				double cessRate = 0.0;
				salesDtl.setRate(rateBeforeTax);
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C"))
				{
					if (item.getCess() > 0) {
						cessRate = item.getCess();
						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
					
					salesDtl.setRate(rateBeforeTax);
					cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;
					}
					else {
						cessAmount = 0.0;
						cessRate = 0.0;
					}
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setCessRate(cessRate);
					salesDtl.setCessAmount(cessAmount);
				}
				
				salesDtl.setStandardPrice(rate);
				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesDtl.setCgstTaxRate(cgstTaxRate);

				salesDtl.setSgstTaxRate(sgstTaxRate);
				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					salesDtl.setSgstTaxRate(taxRate / 2);

					salesDtl.setCgstTaxRate(taxRate / 2);

					Double cgstAmt = 0.0,sgstAmt = 0.0;
					cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
					bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
							
					salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
					sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
					bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
				

				} else {
					salesDtl.setSgstTaxRate(0.0);

					salesDtl.setCgstTaxRate(0.0);

					salesDtl.setCgstAmount(0.0);

					salesDtl.setSgstAmount(0.0);

					salesDtl.setIgstTaxRate(taxRate);
					salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				}
			}
			}
//			

			String companyState = SystemSetting.getUser().getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				salesDtl.setSgstTaxRate(taxRate / 2);

				salesDtl.setCgstTaxRate(taxRate / 2);

				Double cgstAmt =0.0,sgstAmt =0.0;
				cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
				bdcgstAmt = bdcgstAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setCgstAmount(bdcgstAmt.doubleValue());
				sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
				bdsgstAmt = bdsgstAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setIgstAmount(0.0);
			

			} else {
				salesDtl.setSgstTaxRate(0.0);

				salesDtl.setCgstTaxRate(0.0);

				salesDtl.setCgstAmount(0.0);

				salesDtl.setSgstAmount(0.0);

				salesDtl.setIgstTaxRate(taxRate);
				salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

			}
		

		
		BigDecimal settoamount = new BigDecimal(
				(rate *salesDtl.getRate() ));
		
		double includingTax= (settoamount.doubleValue()*salesDtl.getTaxRate())/100;
		double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(setamount.doubleValue());

		salesDtl.setAddCessRate(0.0);
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp1 = RestCaller
				.getPriceDefenitionMstByName("MRP");
		PriceDefenitionMst priceDefenitionMst1 = priceDefenitionMstResp1.getBody();
		if(null != priceDefenitionMst1)
		{
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(),sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setMrp(priceDefinition.getAmount());
			}
		}
		
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if(null != priceDefenitionMst)
		{
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(),sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setCostPrice(priceDefinition.getAmount());
			}
		}
		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		salesDtl = respentity.getBody();
		
		}

//		//---------------offer---------------



		return "Item Successfully added";
	
	
	}
	
	@Override
	public String AddSalesDtl(String hdrId, String itemId, double qty, double rate) {


		
		ObservableList<SalesDtl> saleListItemTable = FXCollections.observableArrayList();
		Boolean notinstock = false;

		SalesDtl salesDtl = new SalesDtl();

		SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(hdrId);

		ResponseEntity<ItemMst> itemMstResp = RestCaller.getitemMst(itemId);
		ItemMst itemMst = itemMstResp.getBody();

		ResponseEntity<UnitMst> unitMstResp = RestCaller.getunitMst(itemMst.getUnitId());
		UnitMst unitMst = unitMstResp.getBody();

	
		
//		ArrayList items = new ArrayList();
//		items = RestCaller.getSingleStockItemByName(itemMst.getItemName(), batch);
//		Double chkQty = 0.0;
//		itemId = null;
//		Iterator itr = items.iterator();
//		while (itr.hasNext()) {
//			List element = (List) itr.next();
//			chkQty = (Double) element.get(4);
//			itemId = (String) element.get(7);
//		}
//
//		if (!unitId.equalsIgnoreCase(itemMst.getUnitId())) {
//
//			Double conversionQty = RestCaller.getConversionQty(itemMst.getId(), unitId, itemMst.getUnitId(), qty);
//			System.out.println(conversionQty);
//			if (chkQty < conversionQty) {
//
//				return "Not in Stock!!!";
//			}
//		} else if (chkQty < qty) {
//
//			return "Not in Stock!!!";
//		}
		
		
		//==================================
		
		Map<String, Double> map = new HashMap<String, Double>();
		map = RestCaller.getRequiredMaterial(itemMst.getId(),qty);
		
		if(map.size()==0)
		{
			notinstock = true;
			
			return "Not in Stock";

		}
		
		for(Map.Entry<String, Double> batchAndQty : map.entrySet())
		{
			
			String batch = batchAndQty.getKey();
			Double quantity = batchAndQty.getValue();

//		Double itemsqty = 0.0;
//		ResponseEntity<List<SalesDtl>> getSalesDtl = RestCaller.getSalesDtlByItemAndBatch(salesTransHdr.getId(),itemMst.getId(), batch);
//		
//		saleListItemTable = FXCollections.observableArrayList(getSalesDtl.getBody());
//		if (saleListItemTable.size() > 1) {
//			Double PrevQty = 0.0;
//			for (SalesDtl saleDtl : saleListItemTable) {
//				if (!saleDtl.getUnitId().equalsIgnoreCase(itemMst.getUnitId())) {
//					PrevQty = RestCaller.getConversionQty(saleDtl.getItemId(), saleDtl.getUnitId(),
//							itemMst.getUnitId(), saleDtl.getQty());
//				} else {
//					PrevQty = saleDtl.getQty();
//				}
//				itemsqty = itemsqty + PrevQty;
//			}
//		} else {
//			itemsqty = RestCaller.SalesDtlItemQty(salesTransHdr.getId(), itemId,batch);
//		}
//		if (!unitMst.getId().equalsIgnoreCase(itemMst.getUnitId())) {
//			Double conversionQty = RestCaller.getConversionQty(itemMst.getId(),
//					unitMst.getId(), itemMst.getUnitId(),
//					qty);
//			System.out.println(conversionQty);
//			if (chkQty < itemsqty + conversionQty) {
//				return "Not in Stock!!!";
//			}
//		}
//
//		else if (chkQty < itemsqty + qty) {
//		
//			return "No Stock!!";
//		}
//		if (null == salesDtl) {
			salesDtl = new SalesDtl();
//		}
//		if (null != salesDtl.getId()) {
//
//			RestCaller.deleteSalesDtl(salesDtl.getId());
//
//
//			if (null != salesDtl.getSchemeId()) {
//				return "";
//			}
//
//		}

		salesDtl.setSalesTransHdr(salesTransHdr);
		salesDtl.setItemName(itemMst.getItemName());
		salesDtl.setBarcode(itemMst.getBarCode());
		salesDtl.setBatchCode(batch);
		
			
		salesDtl.setQty(quantity);

		salesDtl.setItemCode(itemMst.getItemCode());
		Double mrpRateIncludingTax = 00.0;
		mrpRateIncludingTax = rate;
		salesDtl.setMrp(mrpRateIncludingTax);

		Double taxRate = 00.0;

		ResponseEntity<ItemMst> respsentity = RestCaller.getItemByNameRequestParam(salesDtl.getItemName()); // itemmst =
		ItemMst item = respsentity.getBody();
		salesDtl.setBarcode(item.getBarCode());
//		salesDtl.setUnitName(unitMst.getUnitName());
		salesDtl.setStandardPrice(item.getStandardPrice());

//		if(null != item.getTaxRate())
//		{
//			salesDtl.setTaxRate(item.getTaxRate());
//			taxRate = item.getTaxRate();
//
//		} else {
//			salesDtl.setTaxRate(0.0);
//		}
		salesDtl.setItemId(item.getId());
		

		if(batch != null)
		{
			ResponseEntity<List<ItemBatchExpiryDtl>> batchExpiryDtlResp = RestCaller.
					getItemBatchExpByItemAndBatch(salesDtl.getItemId(), salesDtl.getBatch());
			
			List<ItemBatchExpiryDtl> itemBatchExpiryDtlList= batchExpiryDtlResp.getBody();
			if(itemBatchExpiryDtlList.size()>0 && null != itemBatchExpiryDtlList)
			{
			ItemBatchExpiryDtl itemBatchExpiryDtl = itemBatchExpiryDtlList.get(0);
			
			if(null != itemBatchExpiryDtl)
			{
				String expirydate = SystemSetting.UtilDateToString(itemBatchExpiryDtl.getExpiryDate(), "yyyy-MM-dd");
				salesDtl.setExpiryDate(java.sql.Date.valueOf(expirydate));
			}
		}
		}

		salesDtl.setUnitId(unitMst.getId());
		salesDtl.setUnitName(unitMst.getUnitName());

		// ResponseEntity<UnitMst> unitMst = RestCaller.getunitMst(item.getUnitId());

		ResponseEntity<List<TaxMst>> getTaxMst = RestCaller.getTaxByItemId(salesDtl.getItemId());
		if (getTaxMst.getBody().size() > 0) {
			for (TaxMst taxMst : getTaxMst.getBody()) {

				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					if (taxMst.getTaxId().equalsIgnoreCase("CGST")) {
						salesDtl.setCgstTaxRate(taxMst.getTaxRate());
//						BigDecimal CgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCgstAmount(CgstAmount.doubleValue());
					}
					if (taxMst.getTaxId().equalsIgnoreCase("SGST")) {
						salesDtl.setSgstTaxRate(taxMst.getTaxRate());
//						BigDecimal SgstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setSgstAmount(SgstAmount.doubleValue());
					}
					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
					ResponseEntity<TaxMst> taxMst1 = RestCaller.getTaxMstByItemIdAndTaxId(salesDtl.getItemId(), "IGST");
					if (null != taxMst1.getBody()) {
						
						salesDtl.setTaxRate(taxMst1.getBody().getTaxRate());
					}
				} 
				
				
				else {
					if (taxMst.getTaxId().equalsIgnoreCase("IGST")) {
						salesDtl.setCgstTaxRate(0.0);
						salesDtl.setCgstAmount(0.0);
						salesDtl.setSgstTaxRate(0.0);
						salesDtl.setSgstAmount(0.0);

						salesDtl.setTaxRate(taxMst.getTaxRate());
						salesDtl.setIgstTaxRate(taxMst.getTaxRate());
//							BigDecimal igstAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//							salesDtl.setIgstAmount(igstAmount.doubleValue());
					}
				}
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
					if (taxMst.getTaxId().equalsIgnoreCase("KFC")) {
						salesDtl.setCessRate(taxMst.getTaxRate());
//						BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(), Double.valueOf(txtRate.getText()));
//						salesDtl.setCessAmount(cessAmount.doubleValue());
					}
				}
				if (taxMst.getTaxId().equalsIgnoreCase("AC")) {
					salesDtl.setAddCessRate(taxMst.getTaxRate());
//					BigDecimal cessAmount = RestCaller.TaxCalculator(taxMst.getTaxRate(),
//							Double.valueOf(txtRate.getText()));
//					salesDtl.setAddCessAmount(cessAmount.doubleValue());
				}

			}
			Double rateBeforeTax = (100 * mrpRateIncludingTax)
					/ (100 + salesDtl.getIgstTaxRate() + salesDtl.getCessRate() + salesDtl.getAddCessRate()
							+ salesDtl.getSgstTaxRate() + salesDtl.getCgstTaxRate());
			salesDtl.setRate(rateBeforeTax);
			BigDecimal igstAmount = RestCaller.TaxCalculator(salesDtl.getIgstTaxRate(), rateBeforeTax);
			salesDtl.setIgstAmount(igstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal SgstAmount = RestCaller.TaxCalculator(salesDtl.getSgstTaxRate(), rateBeforeTax);
			salesDtl.setSgstAmount(SgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal CgstAmount = RestCaller.TaxCalculator(salesDtl.getCgstTaxRate(), rateBeforeTax);
			salesDtl.setCgstAmount(CgstAmount.doubleValue() * salesDtl.getQty());
			BigDecimal cessAmount = RestCaller.TaxCalculator(salesDtl.getCessRate(), rateBeforeTax);
			salesDtl.setCessAmount(cessAmount.doubleValue() * salesDtl.getQty());
			BigDecimal addcessAmount = RestCaller.TaxCalculator(salesDtl.getAddCessRate(), rateBeforeTax);
			salesDtl.setAddCessAmount(addcessAmount.doubleValue() * salesDtl.getQty());
		}

		else {
			
// verificed
			
			if (unitMst.getId().equalsIgnoreCase(item.getUnitId())) {
				salesDtl.setStandardPrice(item.getStandardPrice());
			} else {
	
				ResponseEntity<MultiUnitMst> multiUnitMstResp = RestCaller.getMultiUnitbyprimaryunit(item.getId(),unitMst.getId());
				MultiUnitMst multiUnitMst = multiUnitMstResp.getBody();
				salesDtl.setStandardPrice(multiUnitMst.getPrice());
	
			}
			if(null != item.getTaxRate())
				{
					salesDtl.setTaxRate(item.getTaxRate());
					taxRate = item.getTaxRate();
		
				} else {
					salesDtl.setTaxRate(0.0);
				}
			Double rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate);
			
			// if Discount
			
			
			// Calculate discount on base price
			if(null != salesTransHdr.getAccountHeads().getDiscountProperty())
			{
				
			
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF BASE PRICE"))
			{
				calcDiscountOnBasePrice(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl);
				
			}
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF MRP"))
			{
//				salesDtl.setTaxRate(0.0);
				calcDiscountOnMRP(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl,rate);
			}
			if(salesTransHdr.getAccountHeads().getDiscountProperty().equalsIgnoreCase("ON BASIS OF DISCOUNT INCLUDING TAX"))
			{
			ambrossiaDiscount(salesTransHdr,rateBeforeTax,item,mrpRateIncludingTax,taxRate,salesDtl,rate);
			}
			}
			else
			{
				
				double cessAmount = 0.0;
				double cessRate = 0.0;
				salesDtl.setRate(rateBeforeTax);
				if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C"))
				{
					if (item.getCess() > 0) {
						cessRate = item.getCess();
						rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());
					
					salesDtl.setRate(rateBeforeTax);
					cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;
					}
					else {
						cessAmount = 0.0;
						cessRate = 0.0;
					}
					salesDtl.setRate(rateBeforeTax);
					salesDtl.setCessRate(cessRate);
					salesDtl.setCessAmount(cessAmount);
				}
				
				salesDtl.setStandardPrice(rate);
				double sgstTaxRate = taxRate / 2;
				double cgstTaxRate = taxRate / 2;
				salesDtl.setCgstTaxRate(cgstTaxRate);

				salesDtl.setSgstTaxRate(sgstTaxRate);
				String companyState = SystemSetting.getUser().getCompanyMst().getState();
				String customerState = "KERALA";
				try {
					customerState = salesTransHdr.getAccountHeads().getCustomerState();
				} catch (Exception e) {

				}

				if (null == customerState) {
					customerState = "KERALA";
				}

				if (null == companyState) {
					companyState = "KERALA";
				}

				if (customerState.equalsIgnoreCase(companyState)) {
					salesDtl.setSgstTaxRate(taxRate / 2);

					salesDtl.setCgstTaxRate(taxRate / 2);

					Double cgstAmt = 0.0,sgstAmt = 0.0;
					cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdCgstAmt = new BigDecimal(cgstAmt);
					bdCgstAmt = bdCgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
							
					salesDtl.setCgstAmount(bdCgstAmt.doubleValue());
					sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
					BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
					bdsgstAmt = bdsgstAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					
					salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

					salesDtl.setIgstTaxRate(0.0);
					salesDtl.setIgstAmount(0.0);
				

				} else {
					salesDtl.setSgstTaxRate(0.0);

					salesDtl.setCgstTaxRate(0.0);

					salesDtl.setCgstAmount(0.0);

					salesDtl.setSgstAmount(0.0);

					salesDtl.setIgstTaxRate(taxRate);
					salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

				}
			}
			}
//			

			String companyState = SystemSetting.getUser().getCompanyMst().getState();
			String customerState = "KERALA";
			try {
				customerState = salesTransHdr.getAccountHeads().getCustomerState();
			} catch (Exception e) {

			}

			if (null == customerState) {
				customerState = "KERALA";
			}

			if (null == companyState) {
				companyState = "KERALA";
			}

			if (customerState.equalsIgnoreCase(companyState)) {
				salesDtl.setSgstTaxRate(taxRate / 2);

				salesDtl.setCgstTaxRate(taxRate / 2);

				Double cgstAmt =0.0,sgstAmt =0.0;
				cgstAmt = salesDtl.getCgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdcgstAmt = new BigDecimal(cgstAmt);
				bdcgstAmt = bdcgstAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setCgstAmount(bdcgstAmt.doubleValue());
				sgstAmt = salesDtl.getSgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100;
				BigDecimal bdsgstAmt = new BigDecimal(sgstAmt);
				bdsgstAmt = bdsgstAmt.setScale(2,BigDecimal.ROUND_HALF_EVEN);
				salesDtl.setSgstAmount(bdsgstAmt.doubleValue());

				salesDtl.setIgstTaxRate(0.0);
				salesDtl.setIgstAmount(0.0);
			

			} else {
				salesDtl.setSgstTaxRate(0.0);

				salesDtl.setCgstTaxRate(0.0);

				salesDtl.setCgstAmount(0.0);

				salesDtl.setSgstAmount(0.0);

				salesDtl.setIgstTaxRate(taxRate);
				salesDtl.setIgstAmount(salesDtl.getIgstTaxRate() * salesDtl.getQty() * salesDtl.getRate() / 100);

			}
		

		
		BigDecimal settoamount = new BigDecimal(
				(rate *salesDtl.getRate() ));
		
		double includingTax= (settoamount.doubleValue()*salesDtl.getTaxRate())/100;
		double amount = settoamount.doubleValue() + includingTax + salesDtl.getCessAmount();
		BigDecimal setamount = new BigDecimal(amount);
		setamount = setamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setAmount(setamount.doubleValue());

		salesDtl.setAddCessRate(0.0);
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp1 = RestCaller
				.getPriceDefenitionMstByName("MRP");
		PriceDefenitionMst priceDefenitionMst1 = priceDefenitionMstResp1.getBody();
		if(null != priceDefenitionMst1)
		{
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst1.getId(), salesDtl.getUnitId(),sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setMrp(priceDefinition.getAmount());
			}
		}
		
		ResponseEntity<PriceDefenitionMst> priceDefenitionMstResp = RestCaller
				.getPriceDefenitionMstByName("COST PRICE");
		PriceDefenitionMst priceDefenitionMst = priceDefenitionMstResp.getBody();
		if(null != priceDefenitionMst)
		{
			String sdate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
			ResponseEntity<PriceDefinition> priceDefenitionResp = RestCaller
					.getPriceDefenitionByCostPrice(salesDtl.getItemId(), priceDefenitionMst.getId(), salesDtl.getUnitId(),sdate);

			PriceDefinition priceDefinition = priceDefenitionResp.getBody();
			if(null != priceDefinition)
			{
			salesDtl.setCostPrice(priceDefinition.getAmount());
			}
		}
		ResponseEntity<SalesDtl> respentity = RestCaller.saveSalesDtl(salesDtl);
		salesDtl = respentity.getBody();
		
		}



		return "Item Successfully added";
	
	
	
	}
	
	
	
	private void ambrossiaDiscount(SalesTransHdr salesTransHdr,Double rateBeforeTax,
			ItemMst item,Double mrpRateIncludingTax,double taxRate,SalesDtl salesDtl,Double rate)
	{
		
		if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
		{
		double discoutAmount = (rateBeforeTax * salesTransHdr.getAccountHeads().getCustomerDiscount())/100;
		BigDecimal BrateAfterDiscount = new BigDecimal(discoutAmount);
		
		BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		double newRate = rateBeforeTax - discoutAmount;
		salesDtl.setDiscount(discoutAmount);
		BigDecimal BnewRate = new BigDecimal(newRate);
		BnewRate = BnewRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		salesDtl.setRate(BnewRate.doubleValue());
		BigDecimal BrateBeforeTax = new BigDecimal(rateBeforeTax);
		BrateBeforeTax = BrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		//salesDtl.setMrp(Double.parseDouble(txtRate.getText()));
		salesDtl.setStandardPrice(BrateBeforeTax.doubleValue());
		}
		else
		{
		salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;
		
		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);
				
				if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
				{
					Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					//salesDtl.setMrp(rateBefrTax.doubleValue());
					salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				}
				else
				{
				salesDtl.setRate(rateBeforeTax);
				}
				//salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);

	}
	private void calcDiscountOnBasePrice(SalesTransHdr salesTransHdr,Double rateBeforeTax,
			ItemMst item,Double mrpRateIncludingTax,double taxRate,SalesDtl salesDtl)
	{
		if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
		{
			Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BrateAfterDiscount.doubleValue());
			BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
			rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			//salesDtl.setMrp(rateBefrTax.doubleValue());
			//salesDtl.setStandardPrice(rateBefrTax.doubleValue());
		}
		else
		{
		salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);
				
				if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
				{
					Double rateAfterDiscount = (100 * rateBeforeTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					//salesDtl.setMrp(rateBefrTax.doubleValue());
					//salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				}
				else
				{
				salesDtl.setRate(rateBeforeTax);
				}
				//salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);

		
	}

	
	
	private void calcDiscountOnMRP(SalesTransHdr salesTransHdr,Double rateBeforeTax,
			ItemMst item,Double mrpRateIncludingTax,double taxRate,SalesDtl salesDtl,Double rate)
	{
		if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
		{
			Double rateAfterDiscount = (100 * mrpRateIncludingTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
			Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

			BigDecimal BrateAfterDiscount = new BigDecimal(rateAfterDiscount);
			BigDecimal BnewrateBeforeTax = new BigDecimal(newrateBeforeTax);
			
			BnewrateBeforeTax = BnewrateBeforeTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			salesDtl.setRate(BnewrateBeforeTax.doubleValue());
			//BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
		//	rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_CEILING);
			//salesDtl.setMrp(BrateAfterDiscount.doubleValue());
			//salesDtl.setStandardPrice(rate);
		}
		else
		{
		salesDtl.setRate(rateBeforeTax);
		}
		double cessAmount = 0.0;
		double cessRate = 0.0;

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("B2C")) {
			if (item.getCess() > 0) {
				cessRate = item.getCess();

				rateBeforeTax = (100 * mrpRateIncludingTax) / (100 + taxRate + item.getCess());

				System.out.println("rateBeforeTax---------" + rateBeforeTax);
				
				if(salesTransHdr.getAccountHeads().getCustomerDiscount() > 0)
				{
					Double rateAfterDiscount = (100 * mrpRateIncludingTax) / (100 + salesTransHdr.getAccountHeads().getCustomerDiscount());
					Double newrateBeforeTax = (100 * rateAfterDiscount) / (100 + taxRate);

					BigDecimal BrateAfterDiscount = new BigDecimal(newrateBeforeTax);
					BrateAfterDiscount = BrateAfterDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					salesDtl.setRate(BrateAfterDiscount.doubleValue());
					BigDecimal rateBefrTax = new BigDecimal(rateBeforeTax);
					rateBefrTax = rateBefrTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
					//salesDtl.setMrp(mrpRateIncludingTax);
				//	salesDtl.setStandardPrice(rateBefrTax.doubleValue());
				}
				else
				{
				salesDtl.setRate(rateBeforeTax);
				}
				//salesDtl.setRate(rateBeforeTax);

				cessAmount = salesDtl.getQty() * salesDtl.getRate() * item.getCess() / 100;

				/*
				 * Recalculate RateBefore Tax if Cess is applied
				 */

			}
		} else {
			cessAmount = 0.0;
			cessRate = 0.0;
		}

		salesDtl.setCessRate(cessRate);
		salesDtl.setCessAmount(cessAmount);
			
		
	}



}
