package com.maple.mapleclient.restService;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;

/*
 * Production Code
 * Usage:
 * AutoWire RestCaller  and CallRest , passing the Entity Type and Entity.
 * If the the URI is passed as third parameter, then it is used
 * for building the Rest API otherwise  URI will be deducted from the class name.
 * 
 * eg:-  rest.CallRest(User.class, user,"user");
 * 
 */

import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.controllers.BatchWiseStockEvaluationCtl;
//import com.maple.mapleclient.controllers.BalanceSheetConfigDtl;
import com.maple.mapleclient.controllers.ConsumptionHdrReport;
import com.maple.mapleclient.controllers.ConsumptionReportDtl;
import com.maple.mapleclient.controllers.ReceiptModeWiseReport;
import com.maple.mapleclient.controllers.SalesProfitReportHdr;

//import com.maple.mapleclient.controllers.WeighBridgeReport;

//import com.maple.mapleclient.entity.DayEndClosureHdr;
//import com.maple.mapleclient.entity.DayEndClosureDtl;
//import com.maple.mapleclient.entity.SessionEndMst;
//import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.entity.*;
import com.maple.mapleclient.events.ItemPropertyInstance;
//import com.maple.report.entity.AMDCPurchaseReturnSummaryReport;
import com.maple.report.entity.*;

import javafx.scene.control.DatePicker;

@Component

public class RestCaller {

	public static String HOST;
	// = "http://localhost:8080/";
	public static String HOST2;

//	private static String HOST = "http://192.168.1.101:8080/";
	// private static String HOST = "http://localhost:8080/";
//	private static String HOST2 = "http://localhost:8080/";

//	private static String HOST = "http://192.168.1.106:8080/";

//private  static String HOST = "http://192.168.1.113:8080/";

//private static String HOST = "http://192.168.1.101:8080/";
	// private static String HOST = "http://192.168.1.106:8080/";

	// private static String HOST = "http://192.168.1.101:8080/";

	// private static String HOST = "http://192.168.1.101:8080/";

	// private static String HOST = "http://192.168.1.106:8080/";

	// private static String HOST = "http://localhost:8080/";

	// private static String HOST = "http://192.168.1.113:8080/";

	// private static String HOST = "http://192.168.1.101:8080/";

	// private final static String HOST = "http://139.59.81.214:8080/";
//private   static String HOST = "http://localhost:8080/";

	static boolean hostUpdated = false;

	static RestTemplate restTemplate = new RestTemplate();
	static RestTemplate restTemplatePdf = new RestTemplate();
	private static final Logger logger = LoggerFactory.getLogger(RestCaller.class);


	// static RestTemplate restTemplate =
	// RestTemplateBuilder.securityRestTemplateBuilder("hawtio", "hawtio");

	// static RestTemplate restTemplatePdf =
	// RestTemplateBuilder.securityRestTemplateBuilder("hawtio", "hawtio");

	/*
	 * public static void updateHostWithComapnyId() { if(!hostUpdated) {
	 * 
	 * HOST = HOST+SystemSetting.getUser().getCompanyMst().getId()+"/" ; hostUpdated
	 * = true; } System.out.println(HOST); }
	 */
	static public byte[] getpdfReportOfAccountHeads() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "accountheadspdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfStatementOfAccounts() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "statementsofaccountspdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfCustomerRegistration() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "customerregistrationpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfDailySalesSummary() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "dailysalessummarypdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfSalesOrderbyDate() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "salesorderbydatepdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfIntentdtl() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "intentdtlpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfKitDefinitionDtl() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "kitdefinitiondtlpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfPaymentDtl() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "paymentdtlpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfProductionDtl() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "productiondtlspdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfProductionMst() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "productionmstpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfPurchaseHdr() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "purchasehdrpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	// ------------------------------BarcodeConfigurationMst------------------------------

	static public ResponseEntity<BarcodeConfigurationMst> getBarcodeConfig() {
		String path = HOST + "barcodeconfigurationmstresource/showallbarcodeconfigurationmst/"
				+ SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<BarcodeConfigurationMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BarcodeConfigurationMst>() {
				});
		return response;
	}

	static public ResponseEntity<BarcodeConfigurationMst> updateBarcodeConfigurationMst(
			BarcodeConfigurationMst barcodeConfigurationMst) {

		restTemplate.put(
				HOST + "barcodeconfigurationmstresource/" + barcodeConfigurationMst.getId() + "/updatebranchmst",
				barcodeConfigurationMst);
		return null;

	}

	static public ResponseEntity<BarcodeConfigurationMst> saveBarcodeConfigurationMst(
			BarcodeConfigurationMst barcodeConfigurationMst) {
		System.out.println(HOST + "barcodeconfigurationmstresource/addbarcodeconfiguration");
		return restTemplate.postForEntity(HOST + "barcodeconfigurationmstresource/addbarcodeconfiguration",
				barcodeConfigurationMst, BarcodeConfigurationMst.class);

	}

	static public ResponseEntity<List<BarcodeConfigurationMst>> returnValueOfBarcodeConfigmst() {

		String path = HOST + "barcodeconfigurationmstresource/showallbarcodeconfigurationmst";

		ResponseEntity<List<BarcodeConfigurationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BarcodeConfigurationMst>>() {
				});

		return response;

	}

	// ---------------------------------------------BarcodeConfigurationMst
	// End--------------------

	static public byte[] getpdfReportOfReorderMst() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "reordermstpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfStockReport() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "stockpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfStockTransferOutDtl() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "stocktransferoutdtlpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfStockTransferOutHdr() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "stocktransferouthdrpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfSuppliers() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "supplierpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfUnitMst() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "unitmstpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfPaymentHdr() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "paymenthdrpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfKitDefinitionMst() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "kitdefinitionmstpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfIntenthdr() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "intenthdrpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfSalesTranshdr() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "salestranshdrpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfOnlineSales() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "onlinesalespdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public byte[] getpdfReportOfItemMst() {
		ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();

		List<MediaType> supportedApplicationTypes = new ArrayList<MediaType>();
		MediaType pdfApplication = new MediaType("application", "pdf");
		supportedApplicationTypes.add(pdfApplication);

		byteArrayHttpMessageConverter.setSupportedMediaTypes(supportedApplicationTypes);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(byteArrayHttpMessageConverter);

		RestTemplate restTemplatePdf = new RestTemplate();

		restTemplatePdf.setMessageConverters(messageConverters);

		Object result = restTemplatePdf.getForObject(HOST + "itemmstpdf", byte[].class);
		byte[] resultByteArr = (byte[]) result;
		return resultByteArr;
	}

	static public ResponseEntity CallRest(Class<?> cls, Object obj) {

		// No URI passed as parameter. So build the uri from class name
		String ClassName = cls.getSimpleName();
		String uriName = ClassName.substring(0, 1).toLowerCase() + ClassName.substring(1);

		return restTemplate.postForEntity(HOST + uriName, obj, String.class);

	}

	//================old url for unit mst==============
	static public ResponseEntity CallRest(Class<?> cls, Object obj, String uriName) {

		// URI is passed as third parameter, so use it

		return restTemplate.postForEntity(HOST + uriName, obj, String.class);

	}
	
	// new save url of itemmst with online or offline process========anandu=====01-07-2021
	static public ResponseEntity CallRest(Class<?> cls, Object obj, String uriName,String status) {

		// URI is passed as third parameter, so use it

		return restTemplate.postForEntity(HOST + "unitmst/" + status, obj, UnitMst.class);

	}
	//=============end new url=================01-07-2021===================

	static public ResponseEntity<ArrayList> getTaskByGroup(String groupName) {

		String host =  SystemSetting.CAMUNDAHOST ;
		if(!host.endsWith("/"))
		{
			host = host + "/";
		}		String path = host + "rest/task?assignee=" + groupName;
		System.out.println(path);

		return restTemplate.getForEntity(path, ArrayList.class);

	}

	static public Object getTaskVariables(String taskId) {
		
		String host =  SystemSetting.CAMUNDAHOST ;
		if(!host.endsWith("/"))
		{
			host = host + "/";
		}	
		String path = host+ "rest/task/" + taskId + "/variables";

		System.out.println(path);
		return restTemplate.getForObject(path, Object.class);

	}

	static public String completeTask(String taskId) {

		String host =  SystemSetting.CAMUNDAHOST ;
		if(!host.endsWith("/"))
		{
			host = host + "/";
		}		
		String path = host+ "rest/task/" + taskId + "/complete";
		HashMap variables = new HashMap();
		variables.put("voucherNumber", "123");
		restTemplate.postForEntity(path, variables, String.class);
		return "ok";
	}

	static public Object getProcessVariables(String taskId) {

		// GET /process-instance/{id}/variables
		String host =  SystemSetting.CAMUNDAHOST ;
		if(!host.endsWith("/"))
		{
			host = host + "/";
		}		
		String path = host+SystemSetting.myCompany+ "/rest/process-instance/" + taskId + "/variables";

		return restTemplate.getForObject(path, Object.class);

	}

	static public Object getFormVariables(String taskId) {

		// GET /process-instance/{id}/variables
		// GET /task/{id}/form-variables
		String host =  SystemSetting.CAMUNDAHOST ;
		if(!host.endsWith("/"))
		{
			host = host + "/";
		}		
		String path = host+ "rest/task/" + taskId + "/form-variables";

		return restTemplate.getForObject(path, Object.class);

	}

	static public ResponseEntity<AccountHeads> saveAccountHeads(AccountHeads accountHeads) {

		return restTemplate.postForEntity(HOST + "accountheads", accountHeads, AccountHeads.class);

	}

	static public ResponseEntity<PurchaseSchemeMst> savePurchaseSchemeMst(PurchaseSchemeMst purchaseSchemeMst) {
		System.out.println(HOST + "purchaseschememstresource/purchaseschememstsave");
		return restTemplate.postForEntity(HOST + "purchaseschememstresource/purchaseschememstsave", purchaseSchemeMst,
				PurchaseSchemeMst.class);

	}

	// for tab category
	static public ResponseEntity<KitchenCategoryDtl> saveKitchenCategoryDtl(KitchenCategoryDtl kitchenCategoryDtl) {

		return restTemplate.postForEntity(HOST + "kitchencategorydtlresource/savekitchencategory", kitchenCategoryDtl,
				KitchenCategoryDtl.class);

	}

//	static public ResponseEntity<AccountHeads> saveAccountHeadsnew() {
//
//		return restTemplate.postForEntity(HOST + "/accountheads/addheadbankaccount",  AccountHeads.class);
//
//	}
	
	//===========old url for save supplier=====================
//	static public ResponseEntity<Supplier> saveSupplier(Supplier supplier) {
//		System.out.println(HOST + "supplier");
//		return restTemplate.postForEntity(HOST + "supplier", supplier, Supplier.class);
//
//	}
	
	// new save url of itemmst with online or offline process========anandu=====01-07-2021
//	static public ResponseEntity<Supplier> saveSupplier(Supplier supplier,String status) {
//		System.out.println(HOST + "supplier/" + status);
//		return restTemplate.postForEntity(HOST + "supplier/" + status, supplier, Supplier.class);
//
//	}

	//=====================end new url=========================01-07-2021======
	
	static public ResponseEntity<SalesManMst> saveSalesMan(SalesManMst salesManMst) {
		System.out.println(HOST + "salesmanmstresource/createsalesman");
		return restTemplate.postForEntity(HOST + "salesmanmstresource/createsalesman", salesManMst, SalesManMst.class);

	}

	static public ResponseEntity<PurchaseReturnHdr> savePurchaseReturnHdr(PurchaseReturnHdr purchaseReturnHdr) {
		System.out.println(HOST + "purchasereturnhdrresource/savepurchasereturnhdr");
		return restTemplate.postForEntity(HOST + "purchasereturnhdrresource/savepurchasereturnhdr", purchaseReturnHdr,
				PurchaseReturnHdr.class);

	}

	static public ResponseEntity<PurchaseReturnDtl> savePurchaseReturnDtl(PurchaseReturnDtl purchaseReturnDtl,
			String purchasereturnhdrid) {
		System.out.println(HOST + "purchasereturndtlresource/savepurchasereturndtl");
		return restTemplate.postForEntity(
				HOST + "purchasereturndtlresource/" + purchasereturnhdrid + "/savepurchasereturndtl", purchaseReturnDtl,
				PurchaseReturnDtl.class);

	}
	// --------------------------------MACHINE RESOURCE--------------------------

	static public ResponseEntity<MixCatItemLinkMst> saveResourceCatItemLinkMst(
			MixCatItemLinkMst resourceCatItemLinkMst) {

		System.out.println(HOST + "resourcecatitemmstresource/createresourcecatitemmst");
		return restTemplate.postForEntity(HOST + "resourcecatitemmstresource/createresourcecatitemmst",
				resourceCatItemLinkMst, MixCatItemLinkMst.class);

		// --------------------------------MACHINE RESOURCE--------------------------

	}

	static public ResponseEntity<MixCatItemLinkMst> updateResourceCatItemLinkMst(
			MixCatItemLinkMst resourceCatItemLinkMst) {

		System.out.println(HOST + "resourcecatitemmstresource/updateresourcecatitemmst");
		return restTemplate.postForEntity(HOST + "resourcecatitemmstresource/updateresourcecatitemmst",
				resourceCatItemLinkMst, MixCatItemLinkMst.class);

		// --------------------------------MACHINE RESOURCE--------------------------

	}

//---------------------------------------------------start--------------
	static public ResponseEntity<MachineResourceMst> saveMachineResource(MachineResourceMst machineResource) {

		System.out.println(HOST + "machineresourcemstresource/createmachineresourcemst");
		return restTemplate.postForEntity(HOST + "machineresourcemstresource/createmachineresourcemst", machineResource,
				MachineResourceMst.class);

	}
//-----------------------------------------	end-----------------------------------------

//------------------------------------MACHINE RESOURCE--------------------------------

//	---------------------------ResourceCategory------------------------------------

	static public ResponseEntity<MixCategoryMst> saveResourceCategory(MixCategoryMst machineResource) {

		System.out.println(HOST + "subcategorymstresource/createsubcategory");
		return restTemplate.postForEntity(HOST + "subcategorymstresource/createsubcategory", machineResource,
				MixCategoryMst.class);

	}
//----end

//	static public ResponseEntity<Supplier> getSupplier(String id) {
//		String path = HOST + "supplier/" + id + "/supplier";
//		System.out.println("=============path=========" + path);
//
//		ResponseEntity<Supplier> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<Supplier>() {
//				});
//		return response;
//
//	}

	static public ResponseEntity<AddKotTable> saveAddKotTable(AddKotTable addKottable) {

		return restTemplate.postForEntity(HOST + "addkottable", addKottable, AddKotTable.class);

	}

	// get kot table by name=================
	static public ResponseEntity<AddKotTable> getTablebyName(String tablename) {

		String path = HOST + "getkottablebyname/" + tablename + "/" + SystemSetting.systemBranch;

		ResponseEntity<AddKotTable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AddKotTable>() {
				});

		return response;

	}

	static public ResponseEntity<AddKotTable> getTablebyId(String id) {

		String path = HOST + "getkottablebyid/" + id;

		ResponseEntity<AddKotTable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AddKotTable>() {
				});

		return response;

	}

	// get kot waiter by name===============
	static public ResponseEntity<AddKotWaiter> getWaiterbyName(String waiter) {

		String path = HOST + "getkotwaiterbyname/" + waiter + "/" + SystemSetting.systemBranch;

		ResponseEntity<AddKotWaiter> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AddKotWaiter>() {
				});

		return response;

	}

	// ============================ get table================
	static public ResponseEntity<List<AddKotTable>> getTable() {

		String path = HOST + "addkottables";

		ResponseEntity<List<AddKotTable>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AddKotTable>>() {
				});

		return response;

	}

	static public ResponseEntity<TableOccupiedMst> getTableOccupiedMstBySalesHdrId(String hdrid) {

		String path = HOST + "tableoccupiedmst/gettableoccupiedmstbysaleshdrid/" + hdrid;

		ResponseEntity<TableOccupiedMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<TableOccupiedMst>() {
				});

		return response;

	}

	// ================= get active tables================
	static public ResponseEntity<List<AddKotTable>> getTablebyStatus() {

		String path = HOST + "getkottablesbystatus";

		ResponseEntity<List<AddKotTable>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AddKotTable>>() {
				});

		return response;

	}

	// ======================= Account heads Detail==================
	static public ResponseEntity<List<AccountHeads>> getSAccountDtl() {

		String path = HOST + "accountheads";

		ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountHeads>>() {
				});

		return response;

	}

	static public ResponseEntity<SalesTypeMst> getSaleTypeByname(String salesType) {

		String path = HOST + "salestypemstrepository/salestypemstsbysaletype/" + salesType + "/"
				+ SystemSetting.systemBranch;

		System.out.println(path);
		ResponseEntity<SalesTypeMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesTypeMst>() {
				});

		return response;

	}

	static public ResponseEntity<NutritionMst> getNutritionByname(String nutritionFacts) {

		String path = HOST + "nutritionmsts/findbyname/" + nutritionFacts;

		ResponseEntity<NutritionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<NutritionMst>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesReturnDtl>> getSalesReturnDtl(String id) {

		String path = HOST + "salesreturndtl/" + id + "/salesreturndtls";
		ResponseEntity<List<SalesReturnDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesReturnDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<BranchMst>> getBranchDtl() {

		String path = HOST + "allbranches";

		ResponseEntity<List<BranchMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchMst>>() {
				});

		return response;

	}

	// ============================Account Heads Save=============
	static public ResponseEntity<AddKotWaiter> saveAddKotWaiter(AddKotWaiter addKotwaiter) {

		return restTemplate.postForEntity(HOST + "addkotwaiter", addKotwaiter, AddKotWaiter.class);

	}

	// ======================= get waiters======================
	static public ResponseEntity<List<AddKotWaiter>> getWaiter() {

		String path = HOST + "addkotwaiters";

		ResponseEntity<List<AddKotWaiter>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AddKotWaiter>>() {
				});

		return response;

	}

	// ============================== get waiter by status=============
	static public ResponseEntity<List<AddKotWaiter>> getWaiterbyStatus() {

		String path = HOST + "getkotwaiterbystatus/" + SystemSetting.systemBranch;

		ResponseEntity<List<AddKotWaiter>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AddKotWaiter>>() {
				});

		return response;

	}

	static public ArrayList SearchSupplierByName(String searchData) {

		return restTemplate.getForObject(HOST + "supplierssearch?data=" + searchData, ArrayList.class);
	}

//	static public Supplier getSupplierByName(String suppliername) {
//
//		return restTemplate.getForObject(
//				HOST + "suplierresource/supplierbynamerequestparam?supplierName=" + suppliername, Supplier.class);
//	}

	static public ArrayList SearchSupplierByName() {

		System.out.println(HOST + "suppliers");

		return restTemplate.getForObject(HOST + "suppliers", ArrayList.class);

	}

	public static ResponseEntity<List<MachineResourceMst>> machineDetails() {
		String path = HOST + "machineresourcemstresource/findallmachineresourcemst";

		System.out.println(path);
		ResponseEntity<List<MachineResourceMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MachineResourceMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<MixCategoryMst>> resourceCategory() {
		String path = HOST + "subcategorymstresource/retriveresourcecategory";

		System.out.println(path);
		ResponseEntity<List<MixCategoryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MixCategoryMst>>() {
				});

		return response;
	}

//----------------------------------------------resourcecategoryitemfetch------------------

	public static ResponseEntity<List<MixCatItemLinkMst>> resourceCategoryItemLinkFetch() {
		String path = HOST + "resourcecatitemmstresource/findallresourcecatitemmst";

		System.out.println(path);
		ResponseEntity<List<MixCatItemLinkMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MixCatItemLinkMst>>() {
				});

		return response;
	}

	// ------------------------------------------end-------------------------
	static public ArrayList SearchItemByName(String searchData) {
		System.out.println("SearchItemByNameSearchItemByName" + searchData);

		return restTemplate.getForObject(HOST + "itempopupsearch?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchItemByNameWithComp(String searchData) {
		System.out.println(HOST + "itempopupsearchwithcomp?data=" + searchData);

		return restTemplate.getForObject(HOST + "itempopupsearchwithcomp?data=" + searchData, ArrayList.class);

	}

	static public ArrayList getAllItemsForInMemDb() {
		System.out.println(HOST + "popupresource/itemforinmemdb");

		return restTemplate.getForObject(HOST + "popupresource/itemforinmemdb", ArrayList.class);

	}

  

	static public ArrayList SearchItemByNameWithCategory(String searchData, String categoryName) {
		System.out.println(
				HOST + "itempopupsearchwithcatregoryname?data=" + searchData + "&&categoryname=" + categoryName);

		return restTemplate.getForObject(
				HOST + "itempopupsearchwithcatregoryname?data=" + searchData + "&&categoryname=" + categoryName,
				ArrayList.class);

	}

	static public ArrayList getPendingIntent(String fdate, String tdate) {

		return restTemplate.getForObject(HOST + "intentindtl/getpendingintent?rdate=" + fdate + "&&tdate=" + tdate,
				ArrayList.class);

	}

	static public ArrayList getPendingIntentByBranch(String branch) {

		return restTemplate.getForObject(HOST + "intentindtl/getpendingintentbybranch/" + branch, ArrayList.class);

	}

	static public ArrayList SearchItemByNameLimited(String searchData) {
		System.out.println("SearchItemByNameSearchItemByName" + searchData);

		return restTemplate.getForObject(HOST + "popup/itempopupsearchlimit?data=" + searchData, ArrayList.class);

	}

	// --------------------
	static public ArrayList SearchAccountByName(String searchData) {
		System.out.println(HOST + "accountheadsresource/accountheads?data=" + searchData);

		return restTemplate.getForObject(HOST + "accountheadsresource/accountheads?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchProductByName(String searchData) {
		System.out.println(HOST + "productmst?data" + searchData);

		return restTemplate.getForObject(HOST + "productmst?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchPoNumber(String searchData) {
		System.out.println(HOST + "purchaseorder/getponumbers?data=" + searchData);

		return restTemplate.getForObject(HOST + "purchaseorder/getponumbers?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchBrandByName(String searchData) {
		System.out.println(HOST + "brandmst/brandbyname?data" + searchData);

		return restTemplate.getForObject(HOST + "brandmst/brandbyname?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchLastFiveTransactions(String searchData, String number) {

		return restTemplate.getForObject(
				HOST + "stockitemsearch/" + searchData + "/" + number + "/lastfivetransactions", ArrayList.class);

	}

	static public ArrayList SearchLastFiveTransactionsWithCustomer(String searchData, String custId, String number) {

		return restTemplate.getForObject(HOST + "stockitemsearch/" + searchData + "/" + custId + "/" + number
				+ "/lastfivetransactionswithcustomer", ArrayList.class);

	}

	static public ResponseEntity<String> adddefaultvalueinorgansationdb() {

		String path = HOST + "adddefaultvalueinorgansationdb";
		System.out.println("===path ==" + path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	static public ResponseEntity<AccountHeads> getAccountHeadByName(String accname) {

		String path = HOST + "accountheadsbyname/" + accname;
		System.out.println("===path ==" + path);
		ResponseEntity<AccountHeads> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountHeads>() {
				});
		return response;
	}

	static public ResponseEntity<String> accountMerge(String fromAccountId, String toAccountId) {

		String path = HOST + "accountheadresource/mergeaccounthead/" + fromAccountId + "/" + toAccountId;
		System.out.println("===path ==" + path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	static public ArrayList getTallyImport(String branchCode, String fdate, String tdate) {

		String path = HOST + "tallyimportreportresource/tallyimportreport/" + branchCode + "?fdate=" + fdate
				+ "&&tdate=" + tdate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);
	}

	static public ArrayList salesProfitExportToExcel(String branchCode, String fdate, String tdate) {

		String path = HOST + "salesprofitreportresource/salesprofitreport/" + branchCode + "?rdate=" + fdate
				+ "&&tdate=" + tdate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);
	}

	static public ArrayList receiptSummaryExportToExcel(String branchCode, String fdate, String tdate) {

		String path = HOST + "salesprofitreportresource/salesprofitreport/" + branchCode + "?rdate=" + fdate
				+ "&&tdate=" + tdate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);
	}

	static public ArrayList exportReceiptReport(String branchCode, String fdate) {

		String path = HOST + "dailysalesreportresourec/receiptmodewisesummaryreport/" + branchCode + "?rdate=" + fdate;

		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);
	}

	static public ArrayList exportReceiptReportDtl(String branchCode, String fdate) {

		String path = HOST + "dailysalesreportresourec/receiptmodewisereport/" + branchCode + "?rdate=" + fdate;

		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);
	}

	static public ArrayList exportPaymentReport(String branchCode, String fdate, String tdate) {

		String path = HOST + "paymentreport/exportpaymentreports/" + branchCode + "?fromdate=" + fdate + "&&todate="
				+ tdate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);
	}

	static public ArrayList getPurchasReportImport(String branchCode, String fdate, String tdate) {

		String path = HOST + "/purchasereport/exportpurchasereporttoexcel/" + branchCode + "?fromdate=" + fdate
				+ "&&todate=" + tdate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);
	}

	static public ResponseEntity<ProductConversionConfigMst> getProductConversionConfigMstWithItemIds(String fromId,
			String ToId) {

		String path = HOST + "productconversionconfigurationmstresource/makeproduction/" + fromId + "/" + ToId + "/"
				+ SystemSetting.systemBranch;
		System.out.println("===path ==" + path);
		ResponseEntity<ProductConversionConfigMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProductConversionConfigMst>() {
				});
		return response;
	}

	static public List<Map<String, Object>> executeSql(ExecuteSql sqlToExecute) {
		System.out.println("execute sql " + sqlToExecute);

		// sqlToExecute = sqlToExecute.replaceAll(" ", "%20");

//restTemplate.getForObject(HOST + "executesql/" + sqlToExecute, List.class);
		ResponseEntity resp = restTemplate.postForEntity(HOST + "executesqlpost", sqlToExecute, List.class);
		return (List<Map<String, Object>>) resp.getBody();

	}

	// ----------------------SearchCustomerByName---
	static public ArrayList SearchCustomerByName(String searchData) {
		if(null==searchData) {
			searchData="";
		}
		System.out.println("accountheads" + searchData);
//accountheads
		return restTemplate.getForObject(HOST + "accountheadssearchbyname?data=" + searchData, ArrayList.class);
	}

	static public ArrayList SearchMemberByName(String searchData) {
		System.out.println("membermstsearch" + searchData);

		return restTemplate.getForObject(HOST + "membermstsearch?data=" + searchData, ArrayList.class);
	}

	static public ArrayList SearchMenuByName(String searchData) {
		System.out.println(HOST + "menuconfigsearch?data=" + searchData);

		return restTemplate.getForObject(HOST + "menuconfigsearch?data=" + searchData, ArrayList.class);
	}

//=====================orgname========================
	static public ArrayList SearchOrgByName(String searchData) {
		System.out.println("organisationsearch" + searchData);

		return restTemplate.getForObject(HOST + "organizationsearch?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchMeetingByName(String searchData) {
		System.out.println("meetingmstsearch" + searchData);

		return restTemplate.getForObject(HOST + "meetingmstsearch?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchFamilyByNames(String searchData) {
		System.out.println("familysearch" + searchData);

		return restTemplate.getForObject(HOST + "familysearch?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchSubGroupByNames(String searchData) {

		System.out.println("subgroupmembersearch" + searchData);

		return restTemplate.getForObject(HOST + "subgroupmembersearch?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchFamilyByNames() {
		System.out.println("familysearch");

		return restTemplate.getForObject(HOST + "findallfamily", ArrayList.class);

	}

	static public ArrayList SearchCustomerByName() {
		System.out.println("customersearchcustomersearchcustomersearch");

		return restTemplate.getForObject(HOST + "customerregistrations", ArrayList.class);

	}

	static public ArrayList SearchMemberMstByName(String searchData) {
		System.out.println("membermstsearch" + searchData);

		return restTemplate.getForObject(HOST + "membermstsearch?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchPasterMstByName(String searchData) {
		System.out.println("pastermstsearch" + searchData);

		return restTemplate.getForObject(HOST + "pastermstsearch?data=" + searchData, ArrayList.class);

	}

	// ------------------
	static public ArrayList SearchStockItemByName(String searchData) {
		System.out.println("itemstocksearch" + HOST + "stockitempopupsearch?data=" + searchData);

		return restTemplate.getForObject(HOST + "stockitempopupsearch?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchStockItemByNameWithComp(String searchData) {

		System.out.println(HOST + "stockitempopupsearchwithcom/" + SystemSetting.systemBranch + "?data=" + searchData);
		return restTemplate.getForObject(
				HOST + "stockitempopupsearchwithcom/" + SystemSetting.systemBranch + "?data=" + searchData,
				ArrayList.class);

	}

	static public ArrayList searchStockItemByCategory(String searchData) {
		System.out.println("itemstocksearch" + HOST + "categorymst/stockitemsearchbycatname?data=" + searchData);

		return restTemplate.getForObject(HOST + "categorymst/stockitemsearchbycatname?data=" + searchData,
				ArrayList.class);

	}

	static public ArrayList searchStockByItemAndCategory(String catName, String itemName) {
		System.out.println("itemstocksearch" + HOST + "itemmst/getitembycategory/" + catName + "?itemname=" + itemName);

		return restTemplate.getForObject(HOST + "itemmst/getitembycategory/" + catName + "?itemname=" + itemName,
				ArrayList.class);

	}

	static public ArrayList SearchStockItemByNameWithCompWithoutZero(String searchData) {

		System.out.println(
				HOST + "popupresource/stokcpopupwithouzerostock/" + SystemSetting.systemBranch + "?data=" + searchData);

		return restTemplate.getForObject(
				HOST + "popupresource/stokcpopupwithouzerostock/" + SystemSetting.systemBranch + "?data=" + searchData,
				ArrayList.class);

	}

	static public ArrayList getSingleStockItemByName(String searchData, String batch) {
		System.out.println(HOST + "stockitemsearch/" + searchData + "/" + batch);

		String path = HOST + "stockitemsearch/" + searchData + "/" + batch + "/" + SystemSetting.systemBranch;

		return restTemplate.getForObject(
				HOST + "stockitemsearch/" + searchData + "/" + batch + "/" + SystemSetting.systemBranch,
				ArrayList.class);

	}
	
	//================ stock checking by using store name ========== anandu===============
	static public ArrayList getSingleStockItemByName(String searchData, String batch, String storeName) {

		String path = HOST + "popupresource/stockitemsearch/" + searchData + "/" + batch + "/" + SystemSetting.systemBranch + "/" + storeName;
		System.out.println(path);

		return restTemplate.getForObject(
				 path,ArrayList.class);

	}
	
	// ====================== stock checking end =======================

	static public Double getQtyFromItemBatchDtlDailyByItemIdAndQty(String itemId, String batch) {
		System.out.println("Items" + HOST + "itembatchdtldaily/itembatchdtlqty/" + batch + "/" + itemId);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "itembatchdtldaily/itembatchdtlqty/" + batch + "/" + itemId,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double SalesDtlItemQty(String hdrId, String itemId, String batch) {
		System.out.println("Items" + HOST + "salesdetail/" + hdrId + "/" + itemId + "/getsalesdetailqty");
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "salesdetail/bybatch/" + hdrId + "/" + itemId + "/getsalesdetailqty?batch=" + batch,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public BigDecimal TaxCalculator(Double taxpercent, Double mrp) {
		System.out.println("Tax" + HOST + "taxmst/taxcalculator/" + taxpercent + "/" + mrp);
		BigDecimal rtnValue = null;
		BigDecimal ownAccAmt = restTemplate.getForObject(HOST + "taxmst/taxcalculator/" + taxpercent + "/" + mrp,
				BigDecimal.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double DamageDtlItemQty(String hdrId, String itemId, String batch) {
		System.out.println("Items" + HOST + "damage/" + hdrId + "/" + itemId + "/" + batch + "/getdamagedetailqty");
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "damage/" + hdrId + "/" + itemId + "/" + batch + "/getdamagedetailqty", Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double PaymentInvoiceSettlemnt(String accid, Double paidamount, String paymenthdr, String rdate) {
		System.out.println("accountpayable/automaticbilladjustment/" + accid + "/" + paymenthdr + "/" + paidamount
				+ "?rdate=" + rdate);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "accountpayable/automaticbilladjustment/" + accid + "/"
				+ paymenthdr + "/" + paidamount + "?rdate=" + rdate, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double ReceiptInvoiceSettlement(String accid, Double receivedamt, String receipthdrid, String rdate) {
		System.out.println("accountreceivable/automaticadjustment/" + accid + "/" + receipthdrid + "/" + receivedamt
				+ "?rdate=" + rdate);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "accountreceivable/automaticadjustment/" + accid + "/"
				+ receipthdrid + "/" + receivedamt + "?rdate=" + rdate, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double getCustomerBalance(String custid, String date) {
		System.out.println("Items" + HOST + "accountingresource/getcustomerbalance/" + custid + "?startdate=" + date);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "accountingresource/getcustomerbalance/" + custid + "?startdate=" + date, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double getSumOfSaleReceiptCard(String sdate) {
		Double rtnValue = 0.0;
		System.out.println(HOST + "dailysalessummary/gettotalcardpayment/" + SystemSetting.getSystemBranch()
				+ "?reportdate=" + sdate);

		Double ownAccAmt = restTemplate.getForObject(HOST + "dailysalessummary/gettotalcardpayment/"
				+ SystemSetting.getSystemBranch() + "?reportdate=" + sdate, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double getSumOfSaleOrderReceiptCard(String sdate) {
		double rtnValue = 0;

		System.out.println(HOST + "dailysalessummary/gettotalcardpaymentso/" + SystemSetting.getSystemBranch()
				+ "?reportdate=" + sdate);
		Double ownAccAmt = restTemplate.getForObject(HOST + "dailysalessummary/gettotalcardpaymentso/"
				+ SystemSetting.getSystemBranch() + "?reportdate=" + sdate, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;
	}

	static public Double getBatchProdTotalAllocatedQty(String itemId, String actId) {

		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "productionbatchstockdtlresource/productionrowmaterialcount/" + itemId + "/" + actId,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double ProdcutConversionItemQty(String hdrId, String itemId, String batch) {
		System.out.println("Items" + HOST + "productconversiondtl/" + hdrId + "/" + itemId + "/" + batch
				+ "/getproductconversiondetailqty");
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "productconversiondtl/" + hdrId + "/" + itemId + "/" + batch + "/getproductconversiondetailqty",
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double getConversionQty(String itemid, String sourceunit, String targetunit, Double sourceQty) {
		double rtnValue = 0;
		Double stock = restTemplate.getForObject(
				HOST + "purchasehdr/getconversion/" + itemid + "/" + sourceunit + "/" + targetunit + "/" + sourceQty,
				Double.class);
		if (null != stock) {
			rtnValue = stock;
		}

		return rtnValue;

	}

	static public ArrayList SearchStockItemByBarcode(String barcode) {

		System.out.println(HOST + "stockitempopupsearchbybarcode/" + SystemSetting.systemBranch + "?data=" + barcode);

		return restTemplate.getForObject(
				HOST + "stockitempopupsearchbybarcode/" + SystemSetting.systemBranch + "?data=" + barcode,
				ArrayList.class);

	}

	static public ArrayList SearchStockItemByBarcodeAndBatch(String barcode, String batch) {

		System.out.println(HOST + "popup/stockitempopupsearchbybarcode/" + SystemSetting.systemBranch + "?data="
				+ barcode + "&batch=" + batch);
		return restTemplate.getForObject(HOST + "popup/stockitempopupsearchbybarcode/" + SystemSetting.systemBranch
				+ "?data=" + barcode + "&batch=" + batch, ArrayList.class);

	}

	static public ArrayList ItemSearchByBarcode(String barcode) {
		System.out.println(HOST + "popupresource/itempopupbybarcode?data=" + barcode);
		return restTemplate.getForObject(HOST + "popupresource/itempopupbybarcode?data=" + barcode, ArrayList.class);

	}

	static public ArrayList SearchItemByName() {

		return restTemplate.getForObject(HOST + "itemmsts", ArrayList.class);

	}

	static public ArrayList getDenominationAndCount(String rdate) {

		return restTemplate.getForObject(
				HOST + "dayendclosuremst/countanddenomination/" + SystemSetting.systemBranch + "?rdate=" + rdate,
				ArrayList.class);

	}

	static public String getVoucherNumber(String uID) {

		System.out.println("==path==" + HOST + "vouchernumber?id=" + uID);

		return restTemplate.getForObject(HOST + "vouchernumber?id=" + uID, String.class);

	}

	static public String updateitemBatchMstWithDtl() {

		return restTemplate.getForObject(HOST + SystemSetting.systemBranch + "/updatestockfromdtl", String.class);

	}

	static public String getRandomItems(int stk_count, String fdate) {

		System.out.println("==path==" + HOST + "/stcokverificationmst/getitems");

		return restTemplate.getForObject(HOST + "stcokverificationmst/getitems/" + stk_count + "?fdate=" + fdate,
				String.class);

	}

	static public String getVoucherNumberBranch(String companyId, String uID) {

		System.out.println("Path ======" + HOST + "/vouchernumber?id=" + uID);
		return restTemplate.getForObject(HOST + "/vouchernumber?id=" + uID, String.class);
	}

	static public ResponseEntity<PaymentHdr> savePaymenthdr(PaymentHdr paymentHdr) {

		return restTemplate.postForEntity(HOST + "paymenthdr", paymentHdr, PaymentHdr.class);

	}

	static public ResponseEntity<PaymentDtl> savePaymentdtl(PaymentDtl paymentDtl) {

		return restTemplate.postForEntity(HOST + "paymenthdrresource/paymentdtl", paymentDtl, PaymentDtl.class);

	}

	static public ResponseEntity<PaymentDtl> savePaymentdtls(PaymentDtl paymentDtl) {

		return restTemplate.postForEntity(
				HOST + "paymenthdrresource/" + paymentDtl.getPaymenthdr().getId() + "/paymentdtl", paymentDtl,
				PaymentDtl.class);

	}

//===================== payment delete==================
	static public void deletePaymentDtl(String paymentId) {
		restTemplate.delete(HOST + "paymentdtl/" + paymentId);

	}

	static public void deleteActualProdutcionDtlById(String actid) {
		restTemplate.delete(HOST + "actualproductiondtl/deleteactualproduction/" + actid);

	}

	static public void deleteCategoryManagementById(String actid) {
		restTemplate.delete(HOST + "categorymanagementmst/deletecategorymanagementmstbyid/" + actid);

	}

	// ================== payment invoice delete==========
	static public void deletePaymentInvoiceDtl(String payInvDtlid) {

		restTemplate.delete(HOST + "paymentInvoiceDtl/" + payInvDtlid + "/deletepaymentInvoiceDtl");

	}

	static public void updatePaymenthdr(PaymentHdr paymentHdr) {

		restTemplate.put(HOST + "paymenthdr/" + paymentHdr.getId(), paymentHdr);
		return;

	}

	static public void updateBranchMst(BranchMst branchMst) {

		restTemplate.put(HOST + "branchmst/" + branchMst.getId() + "/updatebranchmst", branchMst);
		return;

	}

	static public ResponseEntity<List<PaymentDtl>> getPaymentDtl(PaymentHdr paymentHdr) {

		String path = HOST + "paymenthdr/" + paymentHdr.getId() + "/paymentdtl";

		ResponseEntity<List<PaymentDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesTransHdr>> getHoldedSales() {
		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/getholdedsales";

		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesTransHdr>> getHoldedTakerOrderSales() {
		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/getholdedtakeordersales";

		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesTransHdr>> getHoldedPOSSales() {
		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/getholdedpossales";

		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesTransHdr>> getHoldedPOSSalesByUser(String userid) {
		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/getholdedpossalesbyuser/" + userid;

		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;

	}
	// ----------------------------------------------ReorderReturnValue-------

	static public ResponseEntity<List<ReorderMst>> returnValueOfReoderItemWise(String itemId, String minQty) {

		String path = HOST + "reordermsts/" + itemId + "/" + minQty;

		ResponseEntity<List<ReorderMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReorderMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReorderMst>> returnValueOfReoder() {

		String path = HOST + "reordermsts";

		ResponseEntity<List<ReorderMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReorderMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<JobCardInHdr>> getPendingVoucher() {

		String path = HOST + "jobcardinhdr/getjobcardinbystatus";

		ResponseEntity<List<JobCardInHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JobCardInHdr>>() {
				});

		return response;

	}

	static public void deleteGroupMst1(String groupId) {

		restTemplate.delete(HOST + "groupmst/" + groupId);

	}

	static public ResponseEntity<List<GroupMst>> getGroupMst() {

		String path = HOST + "allgroup";

		ResponseEntity<List<GroupMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GroupMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<BranchMst>> getBranchMst() {

		String path = HOST + "allbranches";

		ResponseEntity<List<BranchMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<CategoryMst>> getCategoryMst() {

		String path = HOST + "categorymsts";

		ResponseEntity<List<CategoryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ConsumptionReasonMst>> getConsumptionReasonMst() {

		String path = HOST + "consumptionreasonmstresource/findallconsumptionreasonmst";

		ResponseEntity<List<ConsumptionReasonMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReasonMst>>() {
				});

		return response;

	}

	static public ResponseEntity<ConsumptionReasonMst> getConsumptionReasonMstId(String reason) {

		String path = HOST + "consumptionreasonmstresource/findallconsumptionreasonmstbyreason/" + reason;

		ResponseEntity<ConsumptionReasonMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ConsumptionReasonMst>() {
				});

		return response;

	}

	static public ResponseEntity<List<BranchMst>> returnValueOfBranchmst() {

		String path = HOST + "allbranches";

		ResponseEntity<List<BranchMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<GroupMst>> returnValueOfGroupmst() {

		String path = HOST + "allgroup";

		ResponseEntity<List<GroupMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GroupMst>>() {
				});

		return response;

	}

	// =======================search reoder by item name===========

	static public ResponseEntity<List<ReorderMst>> SearchReoderByItemName(String searchData) {
		System.out.println("customersearchcustomersearchcustomersearch" + searchData);

		String path = HOST + "reordersearch?data=" + searchData;

		ResponseEntity<List<ReorderMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReorderMst>>() {
				});

		return response;
	}

	// ==========================Delete Reorder===============
	static public void deleteReOrderMst(String reorderid) {

		restTemplate.delete(HOST + "reordermst/" + reorderid);

	}

	static public void deleteGroupMst(String groupid) {
		System.out.println("=====id==id=====" + groupid);

		restTemplate.delete(HOST + "groupmst/" + groupid);

	}

	static public void deleteSalesType(String id) {

		restTemplate.delete(HOST + "salestypemstrepository/salestypemstdelete/" + id);

	}

	static public void deleteNutritionFacts(String id) {

		restTemplate.delete(HOST + "nutritionfacts/" + id);

	}

	static public void deleteBranchMst(String branchid) {
		System.out.println("=====id==id=====" + branchid);

		restTemplate.delete(HOST + "branchmst/" + branchid);

	}

	static public void deleteBatchPriceDefinition(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "batchpricdefinition/" + id);

	}

	static public void deleteActualProductionHdr(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "actualproductiondtl/deleteactualproductiondtl/" + id);

	}

	static public void deleteItemLocationMst(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "itemlocationmstresource/deleteitemlocationmst/" + id);

	}

	static public void deleteUrsMst(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "ursmstresource/ursmstdelete/" + id);

	}

	public static void deletepurchaseSchemeMst(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "purchaseschememstresource/purchaseschememstdeletebyid/" + id);

	}

	static public void deleteKitchenCategoryDtl(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "kitchencategorydtlresource/deletekitchencategory/" + id);

	}

	static public void deleteSalesTransHdrVoucherNull() {
		restTemplate.delete(HOST + "salestranshdr/deletenullvouchernumber");

	}

	static public void deleteStockTransferInHdr(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "stocktransfeinhdrresource/" + id + "/stocktransferinhdrdelete");

	}

	static public void deleteConsumptionReason(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "consumptionreasonmstresource/deleteconsumptionreasonmst/" + id);

	}

	//
	static public void deleteConsumptiondelete(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "consumptiondtl/deletebydtlid/" + id);

	}

	static public void CategoryDeleteById(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "categorymst/deletecategorybyid/" + id);

	}

	static public void deleteKOtCatPrint(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "deletekotcategorymst/" + id);

	}

	static public void deletePrinterMst(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "deleteprintermstmst/" + id);

	}

	static public void deleteProductConversionConfig(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(
				HOST + "productconversionconfigurationmstresource/productconversionconfigurationmsdelete/" + id);

	}

	static public void deleteProductionBatchStockDtl(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "productionbatchstockdtldelete/" + id);

	}

	static public void deleteServiceDtl(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "servicedtl/serviceindtldelete/" + id);

	}

	static public void deleteReceiptInvoiceByReceiptHdrId(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "receiptinvoicedtl/" + id + "/deletereceiptinvoicedtlbyhdr");

	}

	static public void deleteOwnAccByReceiptHdr(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "ownaccdelete/" + id + "/deleteownaccbyreceipthdr");

	}

	static public void deletePurchaseHdr(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "purchasehdr/" + id + "/purchasedelete");

	}

	static public void deleteNutritionValueDtl(String id) {
		System.out.println("=====id==id=====" + id);

		restTemplate.delete(HOST + "nutritionvaluedtl/" + id + "/deletenutritionvaluedtl");

	}

	static public void deleteRawmaterialIssueDtl(String rawmaterialhdrId) {
		restTemplate.delete(HOST + "rawmaterialissuedtl/" + rawmaterialhdrId + "/deleterawmaterialissuedtl");

	}

	static public void deleteRawmaterialReturnDtl(String rawmaterialhdrId) {
		restTemplate.delete(HOST + "rawmaterialreturn/" + rawmaterialhdrId + "/deleterawmaterialretunrdtl");

	}

	// ------------------------------saveReoder----------------
	static public ResponseEntity<ReorderMst> saveReoder(ReorderMst reorderMst) {

		return restTemplate.postForEntity(HOST + "reordermst", reorderMst, ReorderMst.class);

	}
	// ----------------get Unit Details for Item----------------------

	static public ArrayList SearchUnit() {

		System.out.println("---path---" + HOST + "unitmst");

		return restTemplate.getForObject(HOST + "unitmst", ArrayList.class);

	}
//----------------------------RESOURCE CATEGORY-----------------

	static public ArrayList SearcResourceCat() {

		System.out.println("---path---" + HOST + "subcategorymstresource/fetchresourcecategory");

		return restTemplate.getForObject(HOST + "subcategorymstresource/fetchresourcecategory", ArrayList.class);

	}

	// --------------------------------------------------END---------

	static public ArrayList SearchMachine() {

		System.out.println("---path---" + HOST + "machineresourcemstresource/findallmachineresourcemsts");

		return restTemplate.getForObject(HOST + "machineresourcemstresource/findallmachineresourcemsts",
				ArrayList.class);

	}

	// ===========================================Voucher
	// reprint=====================
	static public ArrayList ShowVoucherNoSales(String voucherType, String date) {
		System.out.println(HOST + "getvouchernumber/" + voucherType + "/salestype?voucherdate=" + date);

		return restTemplate.getForObject(HOST + "getvouchernumber/" + voucherType + "/salestype?voucherdate=" + date,
				ArrayList.class);

	}

//	static public ResponseEntity <List<SalesTransHdr>> getSalesdtlPrint(String voucherno,String date ) {
//		System.out.println(HOST + "saleshdrByvouchernumber/" + voucherno + "/saleshdr?date="+date);
//		String path = HOST + "saleshdrByvouchernumber/" + voucherno + "/saleshdr?date="+date;
//		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<SalesTransHdr>>() {
//				});
//		System.out.println("dtlzzzzzzzzz11222");
//		return response;
//	}
//	
//	static public ResponseEntity <List<StockTransferOutHdr>> getStockdtlPrint(String voucherno,String date ) {
//		System.out.println(HOST + "stocktransferouthdrbyvouchernumber/" + voucherno + "/saleshdr?date="+date +"/stocktransferouthdr");
//		String path = HOST + "stocktransferouthdrbyvouchernumber/" + voucherno + "/stocktransferouthdr?date="+date;
//		ResponseEntity<List<StockTransferOutHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<StockTransferOutHdr>>() {
//				});
//		System.out.println("dtlzzzzzzzzz11222");
//		return response;
//	}
//	

//	
	// ===========

	// ======================================//
	static public ArrayList SearchBranch() {

		System.out.println("----- SearchUnit--rest caller---");

		return restTemplate.getForObject(HOST + "otherbranches", ArrayList.class);

	}

	// -----------------get category--------------------------getDailySalesReport
	static public ArrayList SearchCategory() {

		System.out.println(HOST + "categorymsts");

		return restTemplate.getForObject(HOST + "categorymsts", ArrayList.class);

	}

	// ------------- Get All tax Types---------------------

	static public ArrayList SearchTaxTypes() {

		System.out.println(HOST + "getalltax");

		return restTemplate.getForObject(HOST + "getalltax", ArrayList.class);

	}

	// -----------------SearchPurchaseDtls----
	static public ArrayList SearchPurchaseDtls(String purHdrId) {

		System.out.println("----- SearchPurchaseDtls--rest caller---");

		return restTemplate.getForObject(HOST + "purchasehdr/" + purHdrId + "/purchasedtl", ArrayList.class);

	}
	// ----------------- get customer----------------

	static public ArrayList SearchCustomer() {

		System.out.println("----- SearchCategory--rest caller---");

		return restTemplate.getForObject(HOST + "customerregistrations", ArrayList.class);

	}

	// -----------------get category--------------------------
	static public ArrayList SearchBranchs() {

		System.out.println("----- branch--rest caller---" + HOST + "allbranches");

		return restTemplate.getForObject(HOST + "allbranches", ArrayList.class);

	}

	// ===============get branch from rest
	static public ArrayList SearchBranchLocalhost() {

		System.out.println("----- branch--rest caller SearchBranchLocalhost---");

		return restTemplate.getForObject(HOST + "rest/group", ArrayList.class);

	}

//---------------------------------------branch
	static public ResponseEntity<BranchMst> SearchMyBranch() {

		String path = HOST + "mybranch";
		System.out.println(path);
		ResponseEntity<BranchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BranchMst>() {
				});

		return response;

	}

	// ====================== get Accounts================
	static public ArrayList SearchAccounts() {

		System.out.println("----- SearchAccountHeads--rest caller---");

		return restTemplate.getForObject(HOST + "accountheads", ArrayList.class);

	}

	static public ArrayList SearchAccountsByGroup() {

		System.out.println("----- SearchAccountHeads--rest caller---");

		return restTemplate.getForObject(HOST + "accountheadsbystatus", ArrayList.class);

	}

	// -----old url----------add category-------------

	static public ResponseEntity<CategoryMst> saveCategory(CategoryMst categoryMst) {

		return restTemplate.postForEntity(HOST + "categorymst", categoryMst, CategoryMst.class);

	}
	
	
	// new save url of category_mst with online or offline process========anandu=====01-07-2021
	static public ResponseEntity<CategoryMst> saveCategory(CategoryMst categoryMst,String status) {

		return restTemplate.postForEntity(HOST + "categorymst/" + status, categoryMst, CategoryMst.class);

	}
	//=====================end new url=========================01-07-2021======
	
	// -----------item By item Id-------------

	static public ResponseEntity<CategoryManagementMst> saveCategoryManagementMst(
			CategoryManagementMst categoryManagementMst) {

		return restTemplate.postForEntity(HOST + "categorymanagementmst/savecategorymanagementmst",
				categoryManagementMst, CategoryManagementMst.class);

	}

	static public ResponseEntity<List<ItemMst>> getItemMstById(ItemMst itemMst) {

		String path = HOST + "itemmsts/" + itemMst.getId() + "/purchasedtl";

		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;

	}

	// -----------Item deleted fiel updation---------------
	static public void updateItemMst(ItemMst itemMst) {

		System.out.println(HOST + "itemmst/" + itemMst.getId());
		restTemplate.put(HOST + "itemmst/" + itemMst.getId() + "/itemmst", itemMst);
		return;

	}

	static public String deleteItem(String itemId) {

		return restTemplate.getForObject(HOST + "itemmst/deleteitem/" + itemId, String.class);

	}

	static public void updateRankItemMst(ItemMst itemMst) {

		System.out.println(HOST + "itemmst/" + itemMst.getId() + "/itemmstrankupdate");
		restTemplate.put(HOST + "itemmst/" + itemMst.getId() + "/itemmstrankupdate", itemMst);
		return;

	}

	// --------------item msts---------------

	static public ArrayList SearchItemMsts() {

		return restTemplate.getForObject(HOST + "itemmsts", ArrayList.class);

	}

	static public ArrayList exportItemMsts() {
		System.out.print(HOST + "itemmstresource/exportitem");

		return restTemplate.getForObject(HOST + "itemmstresource/exportitem", ArrayList.class);

	}

	// -------------------For Purchase ------------------

	static public ResponseEntity<PurchaseHdr> savePurchaseHdr(PurchaseHdr purchaseHdr) {

		return restTemplate.postForEntity(HOST + "purchasehdr", purchaseHdr, PurchaseHdr.class);

	}

	static public ResponseEntity<PurchaseOrderHdr> savePurchaseHdrOrderHdr(PurchaseOrderHdr purchaseHdr) {

		return restTemplate.postForEntity(HOST + "purchaseorderhdr", purchaseHdr, PurchaseOrderHdr.class);

	}

//	static public ResponseEntity<PurchaseOrderHdr> savePurchaseOrderHdr(PurchaseOrderHdr purchaseOrderHdr) {
//
//		System.out.println(HOST + "purchaseorderhdr");
//		
//		System.out.println(purchaseOrderHdr);
//
//		return restTemplate.postForEntity(HOST + "purchaseorderhdr", purchaseOrderHdr, PurchaseOrderHdr.class);
//
//	}
	// -------------old url for-----customer registration------------

//	static public ResponseEntity<CustomerMst> customerRegistration(CustomerMst customerRegistration) {
//
//		System.out.println(HOST + "customerregistration");
//		return restTemplate.postForEntity(HOST + "customerregistration", customerRegistration, CustomerMst.class);
//
//	}
	
	// new save url of itemmst with online or offline process========anandu=====01-07-2021
//	static public ResponseEntity<CustomerMst> customerRegistration(CustomerMst customerRegistration, String status) {
//
//		System.out.println(HOST + "customerregistration/" + status);
//		return restTemplate.postForEntity(HOST + "customerregistration/" + status, customerRegistration, CustomerMst.class);
//
//	}
	//=====================end new url=========================01-07-2021======
	
	// -------------------sitecreation------------

	static public ResponseEntity<SiteMst> SiteCreation(SiteMst siteMst) {
		System.out.println("Path =" + HOST + "sitemst");
		return restTemplate.postForEntity(HOST + "sitemst", siteMst, SiteMst.class);

	}
//=======================================org creation===========================

	static public ResponseEntity<OrgMst> OrganisationCreation(OrgMst orgMst) {
		System.out.println("Path =" + HOST + "organisation");
		return restTemplate.postForEntity(HOST + "organisation", orgMst, OrgMst.class);

	}
//==================================================family creation=========================

	static public ResponseEntity<FamilyMst> FamilyCreation(FamilyMst familyMst) {
		System.out.println("Path =" + HOST + "familymst");
		return restTemplate.postForEntity(HOST + "familymst", familyMst, FamilyMst.class);

	}

	static public ResponseEntity<EventMst> EventCreation(EventMst eventMst) {
		System.out.println("Path =" + HOST + "eventmst");
		return restTemplate.postForEntity(HOST + "eventmst", eventMst, EventMst.class);

	}

	static public ResponseEntity<MemberMst> saveMemberMst(MemberMst memberMst) {
		System.out.println("Path =" + HOST + "member");
		return restTemplate.postForEntity(HOST + "member", memberMst, MemberMst.class);

	}

	public static ResponseEntity<List<MembersMst>> getAllMembersMst() {
		String path = HOST + "findallmembers";
		System.out.println("path---------" + path);

		ResponseEntity<List<MembersMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MembersMst>>() {
				});
		return response;
	}

	static public ResponseEntity<SubGroupMember> saveSubGroupMember(SubGroupMember subGroupMember) {
		System.out.println("Path =" + HOST + "subgroupmember");
		return restTemplate.postForEntity(HOST + "subgroupmember", subGroupMember, SubGroupMember.class);

	}

	// =================================userregistration==========
	static public ResponseEntity<UserMst> userRegistration(UserMst userRegistration, String branch) {

		System.out.println("Path =" + HOST + "user");

		return restTemplate.postForEntity(HOST + branch + "/user", userRegistration, UserMst.class);

	}

	// ========================================groupcreation=======
	static public ResponseEntity<GroupMst> groupCreation(GroupMst groupcreation) {

		return restTemplate.postForEntity(HOST + "group", groupcreation, GroupMst.class);

	}

	// ================================branchcreation============
	static public ResponseEntity<BranchMst> SavebranchCreation(BranchMst branchcreation) {

		if (null == branchcreation) {
			System.out.println("Branch creation is null");
		}

		ResponseEntity<BranchMst> response = null;

		String path = HOST + "branch?companymstid=";
		System.out.println(" Caling " + path);

		response = restTemplate.postForEntity(HOST + "branch", branchcreation, BranchMst.class);
		System.out.println(path);

		return response;

	}

	// ----------------------

	static public ResponseEntity<PurchaseHdr> getPurchaseHdr(String id) {

		String path = HOST + "purchasehdr/" + id + "/purchasehdr";
		System.out.println(path);
		return restTemplate.getForEntity(path, PurchaseHdr.class);

	}

	static public ResponseEntity<PurchaseHdr> getPurchaseHdrByVoucher(String voucher) {

		String path = HOST + "purchasehdr/" + voucher + "/purchasehdrbyvoucher/" + SystemSetting.systemBranch;
		System.out.println(path);
		return restTemplate.getForEntity(path, PurchaseHdr.class);

	}

	// ==================== get kit mst by kit id=================
	static public ResponseEntity<KitDefinitionMst> getKitDefinitionMst(String id) {

		String path = HOST + "kitdefinitionmst/" + id + "/kitdefinitionmst/" + SystemSetting.systemBranch;
		System.out.println(path);
		return restTemplate.getForEntity(path, KitDefinitionMst.class);

	}

	static public ResponseEntity<PurchaseDtl> savePurchaseDtl(PurchaseDtl purchaseDtl) {

		return restTemplate.postForEntity(HOST + "purchasedtlresource/purchasehdr/" + purchaseDtl.getPurchaseHdr().getId() + "/purchasedtl",
				purchaseDtl, PurchaseDtl.class);

	}

	static public ResponseEntity<PurchaseOrderDtl> savePurchaseOrderDtl(PurchaseOrderDtl purchaseOrderDtl) {

		return restTemplate.postForEntity(HOST + "purchaseorderdtls/" + purchaseOrderDtl.getPurchaseOrderHdr().getId(),
				purchaseOrderDtl, PurchaseOrderDtl.class);

	}
	// -----get summary----------

	static public Summary getSummary(String purchasehdrId) {

		System.out.println(HOST + "purchasehdr/" + purchasehdrId + "/purchasedtlsummary");
		return restTemplate.getForObject(HOST + "purchasehdr/" + purchasehdrId + "/purchasedtlsummary", Summary.class);

	}
	// {purchaseorderhdrId}/

	static public Summary getSummaryOrderHDr(String purchasehdrId) {

		System.out.println(HOST + "purchaseorderhdr/" + purchasehdrId + "/purchaseorderdtlsummary");
		return restTemplate.getForObject(HOST + "purchaseorderhdr/" + purchasehdrId + "/purchaseorderdtlsummary",
				Summary.class);

	}
	/*
	 * Rest call to return Summary of amount , qty amd tax during data entry.
	 */

	// {companymstid}/salestranshdrbyvoucheranddate/{vouchernumber}

	static public SalesTransHdr getSalesTransHdrByVoucherAndDate(String voucherNumber, String voucherDate) {

		String path = HOST + "salestranshdrbyvoucheranddate/" + voucherNumber + "/" + SystemSetting.systemBranch
				+ "?rdate=" + voucherDate;
		System.out.println(path);
		return restTemplate.getForObject(path, SalesTransHdr.class);

	}

	static public Summary getSalesWindowSummary(String salesTransHdrId) {

		String path = HOST + "salestranshdr/" + salesTransHdrId + "/saleswindowsummary";
		System.out.println(path);
		return restTemplate.getForObject(path, Summary.class);

	}

	static public FCSummarySalesDtl getFCSalesWindowSummary(String salesTransHdrId) {

		String path = HOST + "salestranshdr/" + salesTransHdrId + "/fcsaleswindowsummary";
		System.out.println(path);
		return restTemplate.getForObject(path, FCSummarySalesDtl.class);

	}

	static public Summary getSalesWindowSummaryDiscount(String salesTransHdrId) {

		String path = HOST + "salestranshdr/" + salesTransHdrId + "/saleswindowsummaryfordiscount";
		System.out.println(path);
		return restTemplate.getForObject(path, Summary.class);

	}

	static public FCSummarySalesDtl getFCSalesWindowSummaryDiscount(String salesTransHdrId) {

		String path = HOST + "salestranshdr/" + salesTransHdrId + "/fcsaleswindowsummaryfordiscount";
		System.out.println(path);
		return restTemplate.getForObject(path, FCSummarySalesDtl.class);

	}

	static public Summary getSalesReturnWindowSummary(String salesTransHdrId) {

		String path = HOST + "salesreturnhdr/" + salesTransHdrId + "/saleswindowsummary";
		System.out.println(path);
		return restTemplate.getForObject(path, Summary.class);

	}

	static public Summary getSalesWindowSummaryReport() {

		String path = HOST + "salestranshdr/";
		System.out.println(path);
		return restTemplate.getForObject(path, Summary.class);

	}

	static public SummarySalesOderDtl getSalesOrderWindowSummary(String salesOrderTransHdrId) {

		String path = HOST + "salesordertranshdr/" + salesOrderTransHdrId + "/saleorderwindowsummary";
		System.out.println(path);
		return restTemplate.getForObject(path, SummarySalesOderDtl.class);

	}

	static public double getSummaryStocktotal(String stockouthdrid) {

		return restTemplate.getForObject(HOST + "stocktransferoutdtl/" + stockouthdrid + "/sumTotalStockTranOut",
				Double.class);

	}

	static public List<ItemBatchMst> getBatchExpDate(String batchName) {
		return restTemplate.getForObject(HOST + "itembatchmst/" + batchName + "/itembatchmstdtls", List.class);
	}

	static public void updatePurchasehdr(PurchaseHdr purchaseHdr) {
		System.out.println("--purchaseHdr===" + purchaseHdr.getFinalSavedStatus());

		restTemplate.put(HOST + "purchasehdr/" + purchaseHdr.getId(), purchaseHdr);
		return;

	}

	static public void updatePurchaseOrderHdr(PurchaseOrderHdr purchaseOrderHdr) {
		System.out.println("--purchaseHdr===" + purchaseOrderHdr.getFinalSavedStatus());

		restTemplate.put(HOST + "purchaseorderhdr/" + purchaseOrderHdr.getId(), purchaseOrderHdr);
		return;

	}

	static public void updatePurchaseReturnHdr(PurchaseReturnHdr purchaseReturnHdr) {
		System.out.println("--purchaseHdr===" + purchaseReturnHdr.getFinalSavedStatus());

		restTemplate.put(HOST + "purchasereturnhdrresource/" + purchaseReturnHdr.getId() + "/purchasehdrupdate",
				purchaseReturnHdr);
	

	}

	static public void updateStockTransferReturnHdr(StockTransferInReturnHdr stockTransferIReturnHdr) {

		restTemplate.put(HOST + "stocktransferinreturnhdrresource/stocktransferinreturnhdrupdate/"
				+ stockTransferIReturnHdr.getId(), stockTransferIReturnHdr);
		return;

	}

	static public void updatePurchasehdrToPArtialSave(PurchaseHdr purchaseHdr) {
		System.out.println("--purchaseHdr===" + purchaseHdr.getFinalSavedStatus());

		restTemplate.put(HOST + "purchasehdr/changetopartialsave/" + purchaseHdr.getId(), purchaseHdr);
		return;

	}

	// ------------------delete purchase dtl delete
	static public void purchaseDtlDelete(String purchasedtlId) {

		restTemplate.delete(HOST + "purchasedtl/" + purchasedtlId);
		return;

	}

	static public void purchaseOrderDtlDelete(String purchasedtlId) {

		restTemplate.delete(HOST + "purchaseorderdtl/" + purchasedtlId);
		return;

	}
	// --------------

	// version1.7
	static public String CloseAllPurchaseOrder() {

		restTemplate.getForObject(HOST + "closeallpurchaseorderhdr", String.class);
		return "Done";
	}

	static public String CloseAllPurchaseOrderBySupId(String supid) {

		restTemplate.getForObject(HOST + "closeallsupplierpurchaseorderhdr", String.class);
		return "Done";
	}

	static public ResponseEntity<List<PurchaseOrderHdr>> getPurchaseOrderHdrByStatsAndSupId(String supid,
			String status) {

		String path = HOST + "purchaseorderhdr/purchaseorderbyvoucherdatesupid/" + supid + "/" + status;

		ResponseEntity<List<PurchaseOrderHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseOrderHdr>>() {
				});

		return response;
	}

	static public ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtlByVoucherNoAndDate(String voucherno,
			String vdate) {
		String path = HOST + "purchaseorderhdr/" + voucherno + "/purchasedtlbyvouchernoanddateandstatus?voucherDate="
				+ vdate;

		System.out.println(path);
		ResponseEntity<List<PurchaseOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseOrderDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<PurchaseDtl> getPurchaseDtlById(String id) {
		String path = HOST + "purchasedtl/" + id + "/purchasedtlbyid";

		System.out.println(path);
		ResponseEntity<PurchaseDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PurchaseDtl>() {
				});

		return response;

	}

	static public ResponseEntity<PurchaseOrderHdr> getPurchaseOrderByDtlId(String id) {
		String path = HOST + "purchaseorderhdr/purchaseorderhdrbydtlid/" + id;

		System.out.println(path);
		ResponseEntity<PurchaseOrderHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PurchaseOrderHdr>() {
				});

		return response;

	}

	// version1.7ends

	// version 1.4----------------------------------------
	static public ResponseEntity<List<PurchaseOrderHdr>> getPurchaseOrderHdrByDate(String vDate) {

		String path = HOST + "purchaseorderhdr/purchaseorderhdrbyvoucherdate?voucherdate=" + vDate;

		ResponseEntity<List<PurchaseOrderHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseOrderHdr>>() {
				});

		return response;
	}

	static public ResponseEntity<PurchaseOrderDtl> getPurchaseOrderDtlById(String id) {

		String path = HOST + "purchaseorderhdr/" + id + "/purchaseorderdtlbyid";

		System.out.println(path);
		ResponseEntity<PurchaseOrderDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PurchaseOrderDtl>() {
				});

		return response;
	}

	static public void deletePurchaseOrderDtl(String dtlid) {
		restTemplate.delete(HOST + "purchaseorderdtl/deletepurchaseorder/" + dtlid);
	}

	/// --------version 1.4 end--------------------------------

	static public ResponseEntity<List<PurchaseDtl>> getPurchaseDtl(PurchaseHdr purchaseHdr) {

		String path = HOST + "purchasehdr/" + purchaseHdr.getId() + "/purchasedtl";
		System.out.println(path);

		ResponseEntity<List<PurchaseDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PurchaseReturnDtl>> getPurchaseReturnDtlByHdrId(String purchasereturnhdrid) {

		String path = HOST + "purchasereturndtlresource/" + purchasereturnhdrid + "/getpurchasereturndtl";
		System.out.print(path);
		ResponseEntity<List<PurchaseReturnDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReturnDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PurchaseSummaryReport>> getPurchaseDtlWithBranch(String fdate, String tDate) {

		String path = HOST + "purchasesummary/purchasesummarymonthlyreport?fdate=" + fdate + "&&tdate=" + tDate;

		ResponseEntity<List<PurchaseSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseSummaryReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtl(PurchaseOrderHdr purchaseOrderHdr) {

		String path = HOST + "purchaseorderhdr/" + purchaseOrderHdr.getId() + "/purchaseorderdtl";
		System.out.println(path);

		ResponseEntity<List<PurchaseOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseOrderDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PurchaseHdrReport>> getPurchaseHdrReportByDate(String rdate) {

		String path = HOST + "purchasereturndtlresource/getpurchasereturndtl?voucherdate=" + rdate;

		System.out.print(path);
		ResponseEntity<List<PurchaseHdrReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseHdrReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PurchaseOrderDtl>> getPurchaseOrderDtlByVoucherNo(String voucher) {
		String path = HOST + "purchaseorderhdr/" + voucher + "/purchaseorderdtlbyvoucher";

		ResponseEntity<List<PurchaseOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseOrderDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<PurchaseOrderHdr> getPurchaseOrderHdrByVoucherNo(String voucher) {
		String path = HOST + "purchaseorderhdr/" + voucher + "/purchaseorderhdrbyvoucher";
		System.out.println("oooooooooooooooooooooooooooooooooooooooo");

		System.out.println(path);
		ResponseEntity<PurchaseOrderHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PurchaseOrderHdr>() {
				});

		return response;

	}

	///////// --------------------for intent--------------------/////////////

	static public ResponseEntity<IntentHdr> saveIntentHdr(IntentHdr intentHdr) {
		return restTemplate.postForEntity(HOST + "/intenthdr/", intentHdr, IntentHdr.class);
	}

	static public ResponseEntity<IntentDtl> saveIntentDtl(IntentDtl intentDtl) {
		String path = HOST + "intenthdr/" + intentDtl.getIntentHdr().getId() + "/intentdtl";
		System.out.println(path);
		return restTemplate.postForEntity(path, intentDtl, IntentDtl.class);
	}

	static public void updateIntenthdr(IntentHdr intentHdr) {
		restTemplate.put(HOST + "intenthdr/" + intentHdr.getId(), intentHdr);
		return;
	}

//	===========================delete==========================//
	static public void deleteIntentDtl(String intid) {
		restTemplate.delete(HOST + "intenthdrdelete/" + intid);
	}

	// ============================================================//
	static public ResponseEntity<List<IntentDtl>> getIntentDtl(IntentHdr intentHdr) {

		String path = HOST + "intenthdr/" + intentHdr.getId() + "/intentdtl";

		System.out.println("Path = " + path);
		ResponseEntity<List<IntentDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<IntentDtl>>() {
				});
		return response;
	}

	static public ResponseEntity<List<IntentInHdr>> getIntentInHdr(String rdate, String branchCode) {

		String path = HOST + "intentinhdr/" + branchCode + "/intenthdrs?rdate=" + rdate;

		System.out.println("Path = " + path);
		ResponseEntity<List<IntentInHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<IntentInHdr>>() {
				});
		return response;
	}

	static public ResponseEntity<IntentHdr> getIntentHdrById(String id) {

		String path = HOST + "intenthdrs/intenthdrbyid/" + id;

		System.out.println("Path = " + path);
		ResponseEntity<IntentHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<IntentHdr>() {
				});
		return response;
	}

	static public void deleteDamageDtl(String dmgid) {
		restTemplate.delete(HOST + "damagedtl/" + dmgid);
	}

	static public void deleteDayBookBySourceVoucher(String sourceVoucher) {
		restTemplate.delete(HOST + "daybook/daybookdelete/" + sourceVoucher);

	}

	static public void deleteProductionDtls(String dmgid) {
		restTemplate.delete(HOST + "/productiondtl/deleteproductiondtl/" + dmgid);
	}

	static public void deleteProductionPrePlanning(String prodFromDate, String prodToDate) {
		String path = HOST + "machineresourcemstresource/deleteproductionpreplanning?fromdate=" + prodFromDate
				+ "&&tdate=" + prodToDate;
		System.out.println(path);
		restTemplate.delete(HOST + "machineresourcemstresource/deleteproductionpreplanning?fromdate=" + prodFromDate
				+ "&&tdate=" + prodToDate);
	}

	// ====================================save==============================================//
	static public ResponseEntity<PasterDtl> savePasterDtl(PasterDtl pasterDtl) {

		String path = HOST + "pasterdtl";
		System.out.println("paster detail" + path);
		return restTemplate.postForEntity(path, pasterDtl, PasterDtl.class);

	}

	static public ResponseEntity<PasterMst> savePasterMst(PasterMst pasterMst) {

		String path = HOST + "pastermst";
		System.out.println("paster mst" + path);
		return restTemplate.postForEntity(path, pasterMst, PasterMst.class);

	}

	static public ResponseEntity<MeetingMinutesDtl> saveMeetingMinutsDtl(MeetingMinutesDtl meetingMinutesDtl) {

		String path = HOST + "meetingminutes";
		System.out.println("meeting minutes dtls......" + path);

		return restTemplate.postForEntity(HOST + "meetingminutes", meetingMinutesDtl, MeetingMinutesDtl.class);

	}

	static public ResponseEntity<MeetingMst> saveMeetingMst(MeetingMst meetingMst) {

		String path = HOST + "meetingmst";
		System.out.println("meeting mst ......" + path);

		return restTemplate.postForEntity(HOST + "meetingmst", meetingMst, MeetingMst.class);

	}

	static public ResponseEntity<MeetingMemberDtl> saveMeetingMemberDtl(MeetingMemberDtl meetingMemberDtl) {

		String path = HOST + "meetingmemberdtl";
		System.out.println("meeting mst ......" + path);
		return restTemplate.postForEntity(HOST + "meetingmemberdtl", meetingMemberDtl, MeetingMemberDtl.class);

	}

//==================================show all=====================================//
	public static ResponseEntity<List<PasterMst>> getAllPasterMst() {
		String path = HOST + "finalpastermst";
		System.out.println("path show all---------" + path);

		ResponseEntity<List<PasterMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PasterMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<MeetingMemberDtl>> getAllMeetingMemberDtl() {
		String path = HOST + "finalmeetingmember";
		System.out.println("path show all---------" + path);

		ResponseEntity<List<MeetingMemberDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MeetingMemberDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<PasterDtl>> getAllPasterDtls() {
		String path = HOST + "finalpasterdtls";
		System.out.println("path show all---------" + path);

		ResponseEntity<List<PasterDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PasterDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<MeetingMinutesDtl>> getAllMeetingMinutes() {
		String path = HOST + "finalmeetingminutes";
		System.out.println("path show all---------" + path);

		ResponseEntity<List<MeetingMinutesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MeetingMinutesDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<MeetingMst>> getAllMeetingMst() {
		String path = HOST + "finalmeetingmst";
		System.out.println("path show all---------" + path);

		ResponseEntity<List<MeetingMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MeetingMst>>() {
				});
		return response;
	}

//==================================delete=====================//
	public static void deletePasterDtl(String id) {
		// TODO Auto-generated method stub

		restTemplate.delete(HOST + "deletepasterdtl/" + id);
	}

	static public void deleteMeetingMemberDtl(String id) {
		restTemplate.delete(HOST + "meetingmemberdtldelete/" + id);
	}

	static public void deleteMeetingMst(String id) {
		restTemplate.delete(HOST + "meetingmstdelete/" + id);
	}

	static public void deletePasterMst(String id) {
		restTemplate.delete(HOST + "pastermstdelete/" + id);
	}

	static public void deleteMeetingMinutes(String id) {
		restTemplate.delete(HOST + "meetingminutesdelete/" + id);
	}

//	============================DAMAGE ENTRY================================//
	static public ResponseEntity<DamageHdr> saveDamageHdr(DamageHdr damageHdr) {

		String path = HOST + "/damagehdr/";
		System.out.println("damage entry" + path);

		return restTemplate.postForEntity(path, damageHdr, DamageHdr.class);

	}

	static public ResponseEntity<DamageDtl> savedamageDtl(DamageDtl damageDtl) {

		String path = HOST + "damagehdr/" + damageDtl.getDamageHdr().getId() + "/damagedtl";
		System.out.println("Path = " + path);
		return restTemplate.postForEntity(path, damageDtl, DamageDtl.class);
	}

	static public ResponseEntity<DayBook> savedayBook(DayBook dayBook) {

		String path = HOST + "daybook";
		System.out.println("Path = " + path);
		return restTemplate.postForEntity(path, dayBook, DayBook.class);
	}

	static public ResponseEntity<BatchPriceDefinition> saveBatchPriceDefinition(
			BatchPriceDefinition batchPriceDefinition) {

		String path = HOST + "batchpricedefinition";
		System.out.println("Path = " + path);
		return restTemplate.postForEntity(path, batchPriceDefinition, BatchPriceDefinition.class);
	}

	static public ResponseEntity<List<DamageDtl>> getDamageDtl(DamageHdr damageHdr) {

		String path = HOST + damageHdr.getId() + "/damagedtls";
		System.out.println("Path = " + path);
		ResponseEntity<List<DamageDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DamageDtl>>() {
				});

		return response;
	}

	static public ResponseEntity<List<ItemMergeMst>> getItemMergeMst(ItemMergeMst itemMergeMst) {

		String path = HOST + itemMergeMst.getId() + "/itemmergemsts";
		System.out.println("Path = " + path);
		ResponseEntity<List<ItemMergeMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMergeMst>>() {
				});

		return response;
	}

	static public void updateDamageHdr(DamageHdr damageHdr) {
		restTemplate.put(HOST + "damagehdr/" + damageHdr.getId(), damageHdr);
		return;
	}

	static public void updateDamageHdrBatch(DamageHdr damageHdr) {
		restTemplate.put(HOST + "damagehdr/batchwisefinasave/" + damageHdr.getId(), damageHdr);
		return;
	}

	static public ResponseEntity<ItemMergeMst> getItemMergeSave(ItemMergeMst itemMergeMst) {
		System.out.println("Path =" + HOST + "itemmergeresource/itemmergemst");
		return restTemplate.postForEntity(HOST + "itemmergeresource/itemmergemst", itemMergeMst, ItemMergeMst.class);

	}

	static public void updateItemMerge(String fromItemid, String toItemid, String fromItemBarCode) {

		restTemplate.put(
				HOST + "itemmstmergeresource/" + fromItemid + "/" + toItemid + "/" + fromItemBarCode + "/updateitems",
				ItemMergeMst.class);
		return;

	}

	//////////////////// ------------for customer
	//////////////////// Registration-----------////////////////////////////

//	static public ResponseEntity<CustomerMst> saveCustomerRegistration(CustomerMst customerRegistration) {
//
//		return restTemplate.postForEntity(HOST + "customerregistration", customerRegistration, CustomerMst.class);
//
//	}

//	static public void updateCustomerRegistration(CustomerMst customerRegistration) {
//
//		restTemplate.put(HOST + "customerregistration/" + customerRegistration.getId(), customerRegistration);
//		return;
//
//	}

//	static public ResponseEntity<List<CustomerMst>> getCustomerRegistration(CustomerMst customerRegistration) {
//
//		String path = HOST + "customerregistration/" + customerRegistration.getId() + "/customerregistration";
//
//		ResponseEntity<List<CustomerMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<CustomerMst>>() {
//				});
//
//		return response;
//
//	}

	///////////////////////// ------------------end customer
	///////////////////////// Registration--------------/////////////////////////////////

// ------------------------ for POS Sale -------------------------------

	static public ResponseEntity<SalesTransHdr> saveSalesHdr(SalesTransHdr salestransHdr) {

		System.out.println("==path==" + HOST + "salestranshdr" + salestransHdr);
		return restTemplate.postForEntity(HOST + "salestranshdr", salestransHdr, SalesTransHdr.class);

	}

	static public ResponseEntity<SalesOrderTransHdr> saveSalesOrderHdr(SalesOrderTransHdr salesordertransHdr) {
		System.out.println(HOST + "salesordertranshdr");

		return restTemplate.postForEntity(HOST + "salesordertranshdr", salesordertransHdr, SalesOrderTransHdr.class);

	}

	static public ResponseEntity<SalesDtl> saveSalesDtl(SalesDtl salesDtl) {

		String date = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-yyyy");

		// System.out.print("qqqqqqqqqqqqqqqqqq"+salesDtl.getSalesTransHdr().getId());

		String path = HOST + "salestranshdr/" + salesDtl.getSalesTransHdr().getId() + "/salesdtlandoffer?logdate="
				+ date;
		System.out.println(path);
		return restTemplate.postForEntity(path, salesDtl, SalesDtl.class);

	}

//	static public ResponseEntity<SalesOrderDtl> saveSalesOrderDtl(SalesOrderDtl salesOrderDtl) {
//
//		String salesordertranshdrId = salesOrderDtl.getSalesOrderTransHdr().getId();
//		String path = HOST + "salesordertranshdr/" + salesordertranshdrId + "/salesorderdtl";
//		System.out.println(path);
//
//		return restTemplate.postForEntity(path, salesOrderDtl, SalesOrderDtl.class);
//
//	}
//	static public ResponseEntity<SalesOrderDtl> saveSalesOrderDtl(SalesOrderDtl salesOrderDtl) {
//
//		String date = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-yyyy");
//		String path = HOST + "salesordertranshdr/" + salesOrderDtl.getSalesOrderTransHdr().getId() + "/salesorderdtlandoffer?logdate="
//				+ date;
//		System.out.println(path);
//		return restTemplate.postForEntity(path, salesOrderDtl, SalesOrderDtl.class);
//
//	}

	static public void updateSalesTranshdrPerformaInvoicePrint(SalesTransHdr salestransHdr) {

		restTemplate.put(HOST + "salestranshdrresource/" + salestransHdr.getId() + "/updateperformainvoice",
				salestransHdr);
		return;
	}

	
	static public void updateSalesTranshdr(SalesTransHdr salestransHdr) {

		String date = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-yyyy");

		System.out.println(HOST + "salestranshdrfinalsaveoffer/" + salestransHdr.getId() + "/" + SystemSetting.STORE
				+ "?logdate=" + date);

		restTemplate.put(HOST + "salestranshdrfinalsaveoffer/" + salestransHdr.getId() + "/" + SystemSetting.STORE
				+ "?logdate=" + date, salestransHdr);
		return;
	}
	
	
	//created by anandu  for passing store parameter from ctrl.....28-06-2021.....
//	static public void updateSalesTranshdr(SalesTransHdr salestransHdr, String storeNamefrompopUp) {
//
//		String date = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-yyyy");
//
//		System.out.println(HOST + "salestranshdrfinalsaveoffer/" + salestransHdr.getId() + "/" + storeNamefrompopUp
//				+ "?logdate=" + date);
//
//		restTemplate.put(HOST + "salestranshdrfinalsaveoffer/" + salestransHdr.getId() + "/" + storeNamefrompopUp
//				+ "?logdate=" + date, salestransHdr);
//		return;
//	}

	static public void updateSalesTranshdrWithStore(SalesTransHdr salestransHdr, String store) {

		String date = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-yyyy");

		System.out.println(
				HOST + "salestranshdrfinalsaveoffer/" + salestransHdr.getId() + "/" + store + "?logdate=" + date);

		restTemplate.put(
				HOST + "salestranshdrfinalsaveoffer/" + salestransHdr.getId() + "/" + store + "?logdate=" + date,
				salestransHdr);
		return;
	}

	static public ResponseEntity<SalesReceipts> saveSalesReceipts(SalesReceipts salesReceipts) {
		return restTemplate.postForEntity(HOST + "salesreceipts", salesReceipts, SalesReceipts.class);

	}

	static public ResponseEntity<CardSalesDtl> saveCardSaleDtl(CardSalesDtl cardSalesDtl) {
		return restTemplate.postForEntity(HOST + "cardsaledtlresource/createcardsaledtl", cardSalesDtl,
				CardSalesDtl.class);

	}

	static public void updateSalesOrderTranshdr(SalesOrderTransHdr salesordertransHdr) {

		restTemplate.put(HOST + "salesordertranshdr/" + salesordertransHdr.getId(), salesordertransHdr);
		return;

	}

	static public ResponseEntity<List<SalesDtl>> getSalesDtl(SalesTransHdr salesTransHdr) {

		String path = HOST + "salestranshdr/" + salesTransHdr.getId() + "/salesdtl";
		System.out.println(path);
		ResponseEntity<List<SalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesDtl>> getOtherBranchSalesDtl(SalesTransHdr salesTransHdr) {

		String path = HOST + "salestranshdrotherbanch/" + salesTransHdr.getId() + "/salesdtl";
		System.out.println(path);
		ResponseEntity<List<SalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesDltdDtl>> getSalesDltdDtl(String rdate) {

		String path = HOST + "salesdeleteddetailsresource/getsalesdeletebydate/" + SystemSetting.systemBranch
				+ "?rdate=" + rdate;
		System.out.println(path);
		ResponseEntity<List<SalesDltdDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDltdDtl>>() {
				});

		return response;

	}

	// =========================== Sales Dtl Delete================
	static public void deleteSalesDtl(String dtlid) {

		restTemplate.delete(HOST + "salesdtlsandoffer/" + dtlid);

	}

	static public void deleteSalesDtlEditInvoice(String dtlid) {

		restTemplate.delete(HOST + "salesdtl/salesdtleditdelete/" + dtlid);

	}

	// ===========================Sale Order DTL delete====================
	static public void deleteSalesOrderDtl(String dtlid) {

		restTemplate.delete(HOST + "salesdtlsandoffer/" + dtlid);

	}

	static public ResponseEntity<List<SalesOrderDtl>> getSalesOrderDtl(SalesOrderTransHdr salesOrderTransHdr) {

		String path = HOST + "salesordertranshdr/" + salesOrderTransHdr.getId() + "/salesorderdtl";

		ResponseEntity<List<SalesOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesTransHdr>> getInProgressKOT(String salesMan, String tableName,
			String customSalesMde) {

		String path = HOST + "salestranshdr/" + salesMan + "/" + tableName + "/" + customSalesMde
				+ "/kotinprogressinvoice";
		System.out.println(path);
		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<StockTransferOutHdr> saveStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr) {

		return restTemplate.postForEntity(HOST + "stocktransferouthdr", stockTransferOutHdr, StockTransferOutHdr.class);

	}

	// ---------------- Stock Transfer Detail Save --------------------

	static public ResponseEntity<StockTransferOutDtl> saveStockTransferOutDtl(StockTransferOutDtl stockTransferOutDtl) {

		String path = HOST + "stocktransferouthdr/" + stockTransferOutDtl.getStockTransferOutHdr().getId()
				+ "/stocktransferoutdtl";
		System.out.println(path);
		return restTemplate.postForEntity(path, stockTransferOutDtl, StockTransferOutDtl.class);

	}

	static public void updateStockTransferOutHdr(StockTransferOutHdr stockTransferOutHdr) {

		System.out.println(HOST + "stocktransferouthdr/" + stockTransferOutHdr.getId());
		restTemplate.put(HOST + "stocktransferouthdr/" + stockTransferOutHdr.getId(), stockTransferOutHdr);
		return;

	}

	static public void deleteStockTransferOutDtl(String stkdtlid) {

		restTemplate.delete(HOST + "stocktransferoutdtl/" + stkdtlid);

	}

	static public ResponseEntity<List<StockTransferOutDtl>> getStockTransferOutDtl(
			StockTransferOutHdr stockTransferOutHdr) {

		String path = HOST + "stocktransferouthdr/" + stockTransferOutHdr.getId() + "/stocktransferoutdtl";
		System.out.println(path);

		ResponseEntity<List<StockTransferOutDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferOutDtl>> getStockTransferOutDtlByItemIdAndBatchAndHdrId(String hdrid,
			String ItemId, String batch) {

		String path = HOST + "stocktransferoutdtl/totalqtybyitemidandbatch/" + ItemId + "/" + batch + "/" + hdrid;

		ResponseEntity<List<StockTransferOutDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferOutDtl>> getStockTransferOutDtlByVoucherNo(String voucherNo,
			String branchCode) {

		String path = HOST + "stocktransferoutdtl/stocktransferoutdtlbyvoucher/" + voucherNo + "/" + branchCode;

		System.out.println(path);
		ResponseEntity<List<StockTransferOutDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferInDtl>> getStockTransferInDtlByVoucherNo(String voucherNo,
			String branchCode) {

		String path = HOST + "stocktransfeindtlresource/getstocktransferindtlreport/" + voucherNo + "/" + branchCode;

		System.out.println(path);
		ResponseEntity<List<StockTransferInDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInDtl>>() {
				});

		return response;

	}
	// -----------------------For Receipts Window -----------------

	static public ResponseEntity<ReceiptHdr> saveReceiptsHdr(ReceiptHdr receiptsHdr) {

		return restTemplate.postForEntity(HOST + "receiptshdr", receiptsHdr, ReceiptHdr.class);

	}

	static public ResponseEntity<ReceiptDtl> receiptsDtl(ReceiptDtl receiptsDtl) {

		return restTemplate.postForEntity(
				HOST + "salestranshdr/" + receiptsDtl.getReceipthdr().getId() + "/receiptsdtl", receiptsDtl,
				ReceiptDtl.class);

	}

	static public void updateReceiptsHdr(ReceiptHdr receiptsHdr) {

		restTemplate.put(HOST + "salestranshdr/" + receiptsHdr.getId(), receiptsHdr);
		return;

	}

	static public ResponseEntity<List<ReceiptDtl>> getReceiptDtl(ReceiptHdr receiptsHdr) {

		String path = HOST + "receipthdr/" + receiptsHdr.getId() + "/receiptdtl";

		ResponseEntity<List<ReceiptDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptDtl>>() {
				});
		return response;

	}

	static public ResponseEntity<List<PaymentDtl>> getPaymentDtls(PaymentHdr paymentsHdr) {

		String path = HOST + "paymenthdr/" + paymentsHdr.getId() + "/paymentdtl";

		ResponseEntity<List<PaymentDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentDtl>>() {
				});
		return response;

	}

	static public ResponseEntity<ReceiptHdr> getReceiptHdrById(String receiptsHdr) {

		String path = HOST + "receipthdrs/receipthdrbyid/" + receiptsHdr;

		System.out.println(path);
		ResponseEntity<ReceiptHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ReceiptHdr>() {
				});
		return response;

	}
	//

	static public ResponseEntity<PaymentHdr> getPaymentHdrById(String paymentHdr) {

		String path = HOST + "paymenthdrresouce/paymenthdr/" + paymentHdr;

		ResponseEntity<PaymentHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PaymentHdr>() {
				});

		return response;

	}

	static public ResponseEntity<PaymentHdr> getPaymentHdrByVoucherNumber(String voucherNumber) {

		String path = HOST + "paymenthdrresouce/findbyvouchernumber/" + voucherNumber;

		ResponseEntity<PaymentHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PaymentHdr>() {
				});

		return response;

	}

	static public ResponseEntity<ReceiptHdr> getReceiptHdrByVDateAndVno(String date, String vno) {

		String path = HOST + "receipthdr/getreceiptbydateandvoucher/" + vno + "/?voucherDate=" + date;

		System.out.println(path);
		ResponseEntity<ReceiptHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ReceiptHdr>() {

				});
		return response;
	}

	//////////////////////////////////////////// sale codes
	//////////////////////////////////////////// fetching/////////////////

//		static public ResponseEntity<ItemMst> saleBarcode(ItemMst barcodes) {
//			
//			 
//			return restTemplate.postForEntity(HOST+"itemmsts",barcodes,ItemMst.class);
// 
	static public ResponseEntity<List<ItemMst>> getItemByBarcode(String barcode) {

		String path = HOST + "itemmst/" + barcode + "/barcode";

		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;

	}

	//================old url for itemmst==============
	static public ResponseEntity<ItemMst> saveItemmst(ItemMst temMst) {

		return restTemplate.postForEntity(HOST + "itemmst", temMst, ItemMst.class);

	}
	
	// new save url of itemmst with online or offline process========anandu=====01-07-2021
	static public ResponseEntity<ItemMst> saveItemmst(ItemMst temMst,String status) {

		return restTemplate.postForEntity(HOST + "itemmst/" + status, temMst, ItemMst.class);

	}

	//=====================end new url=========================01-07-2021======
	
	
	static public ResponseEntity<ItemMst> getItemByItemBarcode(ItemMst itemmst) {

		String path = HOST + "itemmst/" + itemmst.getId() + "/barcode";

		ResponseEntity<ItemMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ItemMst>() {
				});

		return response;

	}

	/*
	 * static public ResponseEntity<ItemMst> getItemByName(String itemname) {
	 * 
	 * String path = HOST + "itemmst/" + itemname + "/itemname";
	 * 
	 * System.out.println(path); ResponseEntity<ItemMst> response =
	 * restTemplate.exchange(path, HttpMethod.GET, null, new
	 * ParameterizedTypeReference<ItemMst>() { });
	 * 
	 * return response;
	 * 
	 * }
	 */

	/*
	 * String path = HOST + "productiondtlsbydate?rdate="+date;
	 * 
	 * ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path,
	 * HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductionDtl>>() {
	 * });
	 */

	static public ResponseEntity<ItemMst> getItemByNameRequestParam(String itemname) {

		String path = HOST + "itemmst/itemname?itemdata=" + itemname;

		System.out.println(path);
		ResponseEntity<ItemMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ItemMst>() {
				});

		return response;

	}
	
	static public ResponseEntity<ItemMst> getItemByNameRequestParamForCloud(String itemname) {

		String path = HOST + "itemmst/itemname?item=" + itemname;

		System.out.println(path);
		ResponseEntity<ItemMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ItemMst>() {
				});

		return response;

	}

	static public ResponseEntity<KitValidationReport> vailDateKit(String itemname) {

		String path = HOST + "machineresourcemstresource/validatekit/" + itemname;

		System.out.println(path);
		ResponseEntity<KitValidationReport> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<KitValidationReport>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockReport>> ItemStockReport(String vFDate, String itemId, String branchCode) {

		String path = HOST + "stockreportresurce/itemwisestock?fromdate=" + vFDate + "&&itemid=" + itemId
				+ "&&branchcode=" + branchCode;

		System.out.println(path);
		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<TaxReport>> taxReport(String vFDate, String toDate, String branchCode) {

		String path = HOST + "/taxreportresource/taxreport?fdate=" + vFDate + "&&tdate=" + toDate + "&&branchCode="
				+ branchCode;

		System.out.println(path);
		ResponseEntity<List<TaxReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<TaxReport>> purchaseTaxReport(String vFDate, String toDate, String branchCode) {

		String path = HOST + "/taxreportresource/purchasetaxreport?fdate=" + vFDate + "&&tdate=" + toDate
				+ "&&branchCode=" + branchCode;

		System.out.println(path);
		ResponseEntity<List<TaxReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemBatchDtl>> getitemBatchDtlByItemId(String itemId, String batch) {

		String path = HOST + "itembatchmst/itembatchdtlbyitemid/" + itemId + "/" + batch;

		System.out.println(path);
		ResponseEntity<List<ItemBatchDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemBatchDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<ItemMst> getitemMst(String itemId) {

		String path = HOST + "itemmst/" + itemId + "/itemmst";

		System.out.println(path);
		ResponseEntity<ItemMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ItemMst>() {
				});

		return response;

	}

	// =========== Get UnitMSt======================
	static public ResponseEntity<UnitMst> getunitMst(String unitId) {

		String path = HOST + "unitmst/" + unitId + "/unitmst";

		ResponseEntity<UnitMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<UnitMst>() {
				});

		return response;

	}

	// ==================== Save KitMaster============================
	static public void UpdateKitDefenitionMst(KitDefinitionMst kitDefinitionMst) {

		restTemplate.put(HOST + "kitdefinitionmst/" + kitDefinitionMst.getId(), kitDefinitionMst,
				KitDefinitionMst.class);
		return;

	}
	
	// ==================== new url for online publish -  Save KitMaster============================
		static public void UpdateKitDefenitionMst(KitDefinitionMst kitDefinitionMst, String status) {

			restTemplate.put(HOST + "kitdefinitionmst/" + kitDefinitionMst.getId() + "/" + status, kitDefinitionMst,
					KitDefinitionMst.class);
			return;

		}
	

	//==========old url for kit_mst=================
	static public ResponseEntity<KitDefinitionMst> saveKitDefenitionMst(KitDefinitionMst kitDefinitionMst) {

		return restTemplate.postForEntity(HOST + "kitdefinitionmst", kitDefinitionMst, KitDefinitionMst.class);

	}
	
	// new save url of kit_mst with online or offline process========anandu=====01-07-2021
	static public ResponseEntity<KitDefinitionMst> saveKitDefenitionMst(KitDefinitionMst kitDefinitionMst, String status) {

		return restTemplate.postForEntity(HOST + "kitdefinitionmst/" + status, kitDefinitionMst, KitDefinitionMst.class);

	}
	//=====================end new url=========================01-07-2021======
	
	
	
	// ==========old url for======= save kit detail ================================
	static public ResponseEntity<KitDefinitionDtl> saveKitDefenitionDtl(KitDefinitionDtl kitDefinitionDtl) {

		String path = HOST + "kitdefinitionmsts/" + kitDefinitionDtl.getKitDefenitionmst().getId() + "/kitdefinitondtl";
		return restTemplate.postForEntity(path, kitDefinitionDtl, KitDefinitionDtl.class);

	}
	
	
	// new save url of kit_mst with online or offline process========anandu=====01-07-2021
	static public ResponseEntity<KitDefinitionDtl> saveKitDefenitionDtl(KitDefinitionDtl kitDefinitionDtl,String status) {
		String path = HOST + "kitdefinitionmsts/" + kitDefinitionDtl.getKitDefenitionmst().getId() + "/kitdefinitondtl/" + status;
		return restTemplate.postForEntity(path, kitDefinitionDtl, KitDefinitionDtl.class);

	}
	//=====================end new url=========================01-07-2021======
	
	
	

//======================================accept stock=================================//

	static public ResponseEntity<List<StockTransferInHdr>> getStockTransfer(StockTransferInHdr stockTransfer) {

		String path = HOST + "stocktransferinhdr";

		ResponseEntity<List<StockTransferInHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferOutHdr>> getStockTransferBetweenDate(String fdate, String tdate) {

		String path = HOST + "stocktransferouthdr/stocktransferouthdrbetweendate/" + SystemSetting.systemBranch
				+ "?fdate=" + fdate + "&&tdate=" + tdate;

		System.out.println(path);
		ResponseEntity<List<StockTransferOutHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferInHdr>> getStockTransferInHdrBetweenDate(String fdate,
			String tdate) {

		String path = HOST + "stocktransfeinhdrresource/getstocktransferinhdrreport/" + SystemSetting.systemBranch
				+ "?fromdate=" + fdate + "&&todate=" + tdate;

		System.out.println(path);
		ResponseEntity<List<StockTransferInHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInHdr>>() {
				});

		return response;

	}

	/////////////////////////////////// daily sales reposrt

	static public ResponseEntity<List<Object>> getDailySalesReport(String branchCode, String date) {

		String path = HOST + "dailybussinessummary/" + branchCode + "/dailybussinessummary?reportdate=" + date;

		System.out.println("======path=====" + path);
		ResponseEntity<List<Object>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Object>>() {
				});
		return response;
	}

	// ====================== Get kitDtls =============================
	static public ResponseEntity<List<KitDefinitionDtl>> getKidefinitionDtl(KitDefinitionMst kitDefinitionMst) {

		String path = HOST + "kitdefinitionmst/" + kitDefinitionMst.getId() + "/kitdefinitiondtl";

		ResponseEntity<List<KitDefinitionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KitDefinitionDtl>>() {
				});
		return response;

	}

	static public ArrayList getRawMaterialQty(String hdrid) {

		System.out.println(HOST + "actualproductionhdr/getrawmaterialqty/" + hdrid);
		return restTemplate.getForObject(HOST + "actualproductionhdr/getrawmaterialqty/" + hdrid, ArrayList.class);

	}

	static public ArrayList kitDefenitionDtlArray(KitDefinitionMst kitDefenitionmst) {

		return restTemplate.getForObject(HOST + "/kitdefinitiondtl/" + kitDefenitionmst.getId() + "/kitdefinitiondtls",
				ArrayList.class);

	}

	// ========================= get production DtlDtl========================
	static public ResponseEntity<List<ProductionDtlDtl>> getProductionDtlDtl(String id) {

		String path = HOST + "productiondtl/" + id + "/productiondtldtl";

		ResponseEntity<List<ProductionDtlDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtlDtl>>() {
				});
		return response;

	}

	static public ResponseEntity<ActualProductionDtl> getActualProductionById(String id) {

		String path = HOST + "actualproductiondtl/" + id + "/actualproductiondtlbyid";

		ResponseEntity<ActualProductionDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ActualProductionDtl>() {
				});
		return response;

	}

	static public ResponseEntity<ActualProductionHdr> getActualProductionByDate(String voucherDate) {

		String path = HOST + "actualproductionhdr/getactualproductionbydate/" + SystemSetting.systemBranch + "?rdate="
				+ voucherDate;

		System.out.println(path);
		ResponseEntity<ActualProductionHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ActualProductionHdr>() {
				});
		return response;

	}

	// ====================== Get productionDtls by Date=================
	static public ResponseEntity<List<ProductionDtl>> getProductionDtlsbyDate(String startDate) {

		String path = HOST + "productiondtl/" + startDate + "/productiondtsummary";

		ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtl>>() {
				});
		return response;

	}

	// ========================= Get productionDtls==========================
	static public ResponseEntity<List<ProductionDtl>> getProductionDtls() {

		String path = HOST + "productiondtls/" + SystemSetting.systemBranch;

		ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionDtl>> getProductionDtlsbyMstId(ProductionMst productionMst) {

		String path = HOST + "productiondtlbyproductionmst/" + productionMst.getId();

		ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ActualProductionReport>> getActualProductioReportId(String fromDate,
			String toDate, String branchCode) {

		String path = HOST + "actualproductionreportwithid/" + branchCode + "?fdate=" + fromDate + "&&tdate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<ActualProductionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ActualProductionReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ActualProductionReport>> getActualProductioReport(String fromDate, String toDate,
			String branchCode) {

		String path = HOST + "actualproductionreport/" + branchCode + "?fdate=" + fromDate + "&&tdate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<ActualProductionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ActualProductionReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionDtl>> getKItDtlsbyDate(String date) {

		String path = HOST + "productiondtlsbydate/" + SystemSetting.systemBranch + "?rdate=" + date;
		System.out.println(path);

		ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtl>>() {
				});

		return response;

	}
	// -----------------------saleOrderToProduction---start------------

	static public ResponseEntity<ProductionMst> saleOrderToProduction(String branchCode, String prodDate) {

		String path = HOST + "machineresourcemstresource/" + branchCode + "/findproductmachineratio?fromdate="
				+ prodDate;
		System.out.println(path);

		// restTemplate.delete(HOST + "saleordertoproductionresource/+branchCode+/"
		// +"convertsaleordertoproduction");

		ResponseEntity<ProductionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProductionMst>() {
				});

		return response;

	}

	static public ResponseEntity<ProductionPreplanningMst> fectchProductionPreplanningMst(String branchCode,
			String prodDate) {

		String path = HOST + "producctionpreplaningmstresource/" + branchCode + "/getproductionpreplaningmst?fdate="
				+ prodDate;
		System.out.println(path);

		// restTemplate.delete(HOST + "saleordertoproductionresource/+branchCode+/"
		// +"convertsaleordertoproduction");

		ResponseEntity<ProductionPreplanningMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProductionPreplanningMst>() {
				});

		return response;

	}

	static public ResponseEntity<String> prePlaningToProduction() {

		String path = HOST + "machineresourcemstresource/productionpreplaningtoproduction";
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionDtl>> planedProductionDtl() {

		String path = HOST + "productionmstresource/getplanedproduction";
		System.out.println(path);

		ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionPreplanningDtlReport>> fetchProductionPreplanningDtl(String branchCode,
			String fDate, String toDate) {

		String path = HOST + "producctionpreplaningmstresource/" + branchCode + "/getproductionpreplaningdtl?fdate="
				+ fDate + "&&tdate=" + toDate;
		System.out.println(path);

		ResponseEntity<List<ProductionPreplanningDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<ProductionPreplanningDtlReport>>() {
				});

		return response;

	}

	static public ResponseEntity<String> fetchSaleOrderProductionPrePlanning(String branchCode, String fDate,
			String toDate) {

		String path = HOST + "machineresourcemstresource/" + branchCode + "/fetchsaleorderproductqty?fromdate=" + fDate
				+ "&&tdate=" + toDate;
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionDtl>> prePlaningToProductionPlaning() {

		String path = HOST + "machineresourcemstresource/fetchproductionpreplaningtoproduction";
		System.out.println(path);

		ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtl>>() {
				});

		return response;

	}

	// -----------------------saleOrderToProduction-----end------------

	// ====================== Delete kitdtl=================
	static public void deleteKitDefintionDtl(String kitdtlid) {

		restTemplate.delete(HOST + "kitdefinitiondtl/" + kitdtlid);

	}

	// ================================ Get kit mst by kitname-===================
	static public ResponseEntity<KitDefinitionMst> getKitId(String kitname) {

		String path = HOST + "kitdefinitionmst/" + kitname + "/" + SystemSetting.systemBranch;

		ResponseEntity<KitDefinitionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<KitDefinitionMst>() {
				});

		return response;

	}

	static public ResponseEntity<KitDefinitionMst> getKitByItemId(String itemId) {

		String path = HOST + "kitdefinitionmst/" + itemId + "/kitdefinitionmst/" + SystemSetting.systemBranch;

		ResponseEntity<KitDefinitionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<KitDefinitionMst>() {
				});

		return response;

	}

	// ========================== update kit mst====================
//	static public void updatekitDefinitionMst(KitDefinitionMst kitDefinitionMst) {
//
//		restTemplate.put(HOST + "kitDefinitionmst/" + kitDefinitionMst.getId(), kitDefinitionMst);
//		return;
//
//	}

	// ========================== Save Production Mst======================

	static public ResponseEntity<ProductionMst> saveProductionMst(ProductionMst productionMst) {

		return restTemplate.postForEntity(HOST + "productionmst", productionMst, ProductionMst.class);

	}

	static public ResponseEntity<String> saveProductionDtlList(
			List<ProductionPreplanningDtlReport> productionPreplanningDtlList, String productionmstid) {

		return restTemplate.postForEntity(
				HOST + "producctionpreplaningmstresource/" + productionmstid + "/salevproductiondtl",
				productionPreplanningDtlList, String.class);

	}
	// ========================== make production======================

	static public ResponseEntity<Double> makeProductionByDtlId(String dtlId) {

		String path = HOST + "makeproduction/" + dtlId;
		return restTemplate.postForEntity(path, "", Double.class);

	}

	// ======================== Save Production Dtl========================
	static public ResponseEntity<ProductionDtl> saveProductionDtl(ProductionDtl productionDtl) {

		String path = HOST + "productionmst/" + productionDtl.getProductionMst().getId() + "/productiondtl";

		return restTemplate.postForEntity(path, productionDtl, ProductionDtl.class);

	}

	// ======================== Save production DtlDtl===============

	static public ResponseEntity<ProductionDtlDtl> saveProductionDtlDtl(ProductionDtlDtl productionDtlDtl) {

		return restTemplate.postForEntity(
				HOST + "productiondtl/" + productionDtlDtl.getProductionDtl().getId() + "/productiondtldtl",
				productionDtlDtl, ProductionDtlDtl.class);

	}

	// =================================orderwindow=============================================

	static public ResponseEntity<SalesOrderTransHdr> SaveSalesOrder(SalesOrderTransHdr salesOrderTransHdr) {

		String path = HOST + "salesordertranshdr";
		System.out.println(path);

		return restTemplate.postForEntity(path, salesOrderTransHdr, SalesOrderTransHdr.class);

	}

	// ------------------------price Defenition--------------------------

	static public ResponseEntity<PriceDefinition> savePriceDefenition(PriceDefinition priceDefenition) {

		return restTemplate.postForEntity(HOST + "pricedefinition", priceDefenition, PriceDefinition.class);

	}

	static public ResponseEntity<List<PriceDefinition>> SearchPriceDefinition() {
		String path = HOST + "getpricdefiniton";
		ResponseEntity<List<PriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefinition>>() {
				});

		return response;
	}

	static public ResponseEntity<List<BatchPriceDefinition>> SearchBatchPriceDefinition() {
		String path = HOST + "getbatchpricdefiniton";
		ResponseEntity<List<BatchPriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BatchPriceDefinition>>() {
				});

		return response;
	}

//....................show all price definition.//
	static public ResponseEntity<List<PriceDefenitionMst>> ShowAllPriceDefinition() {
		String path = HOST + "findallpricedefinitionmst";
		System.out.println(path);
		ResponseEntity<List<PriceDefenitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefenitionMst>>() {
				});

		return response;
	}

//---------------------get partialy saved purchase
	static public ResponseEntity<List<PurchaseHdr>> getPartialyPurchaseHdr() {

		String path = HOST + "purchasehdrbystatus/" + SystemSetting.systemBranch;
		System.out.println(path);

		ResponseEntity<List<PurchaseHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<ParamValueConfig> getParamValueConfig(String param) {

		String path = HOST + "paramvalueconfig/" + param + "/" + SystemSetting.systemBranch;
		System.out.println(path);

		ResponseEntity<ParamValueConfig> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ParamValueConfig>() {
				});

		return response;

	}

	// ===========================kotposlist========================///
	static public ResponseEntity<List<AddKotTable>> getTableDtls() {

		String path = HOST + "addkottables";

		ResponseEntity<List<AddKotTable>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AddKotTable>>() {
				});
		return response;
	}

	// ==================== Sales Order Report==================

	// ==================== Sales Order Report==================

	static public ResponseEntity<List<SalesOrderTransHdr>> getSalesOrderByDate(Date orderDate) {

		String path = HOST + "dailyordersummary/" + orderDate + "/dailyordersummary";

		ResponseEntity<List<SalesOrderTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderTransHdr>>() {
				});

		return response;

	}

	// ==========================receipthdr==============================//

	static public ResponseEntity<ReceiptHdr> saveReceipthdr(ReceiptHdr receiptHdr) {

		return restTemplate.postForEntity(HOST + "receipthdr", receiptHdr, ReceiptHdr.class);

	}

	static public ResponseEntity<ReceiptDtl> saveReceiptDtl(ReceiptDtl receiptDtl) {

		return restTemplate.postForEntity(HOST + "receipthdr/" + receiptDtl.getReceipthdr().getId() + "/receiptdtl",
				receiptDtl, ReceiptDtl.class);

	}

	static public ResponseEntity<PaymentDtl> savePaymentDtl(PaymentDtl paymentDtl) {

		return restTemplate.postForEntity(HOST + "paymenthdr/" + paymentDtl.getPaymenthdr().getId() + "/paymentdtl",
				paymentDtl, PaymentDtl.class);

	}

	// Save reciept Invoice Detail ================================
	static public ResponseEntity<ReceiptInvoiceDtl> saveReceiptInvoiceDtl(ReceiptInvoiceDtl receiptInvoiceDtl) {

		return restTemplate.postForEntity(HOST + "receiptinvoicedtl", receiptInvoiceDtl, ReceiptInvoiceDtl.class);

	}

	static public void updateReceipthdr(ReceiptHdr receiptHdr) {

		restTemplate.put(HOST + "receipthdr/" + receiptHdr.getId(), receiptHdr);
		return;
	}

	static public void updatePaymentHdr(PaymentHdr paymentHdr) {

		restTemplate.put(HOST + "paymenthdr/" + paymentHdr.getId(), paymentHdr);
		return;
	}

	static public void updateProductionMst(ProductionMst productionMst) {

		restTemplate.put(HOST + "productionmst/updateproductionmst", productionMst);
		return;
	}

	static public void deleteReceiptDtl(String receiptid) {

		restTemplate.delete(HOST + "receiptdtl/" + receiptid);

	}

	static public double getSummaryReceiptTotal(String receipthdrid) {

		return restTemplate.getForObject(HOST + "receiptdtl/" + receipthdrid + "/receipttotalamount", Double.class);
	}

	static public double getSummaryReceiptInvTotal(String receipthdrid) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "receiptinvoicedtl/getreceivedamount/" + receipthdrid,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public double getSummaryOwnAccByReceiptHdr(String receipthdrid) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "ownaccount/" + receipthdrid + "/sumofpaidbyreceipthdr",
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;
	}

	static public double getSummaryOwnAccByPaymentHdr(String paymenthdrId) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "ownaccount/" + paymenthdrId + "/sumofpaidbypaymenthdr",
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;
	}

	static public double getSummaryPaymentInvTotal(String receipthdrid) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "paymentInvoiceDtl/" + receipthdrid + "/sumofpaidamount",
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;
	}

	static public double getOwnAccountBySupId(String supid) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "ownaccount/" + supid + "/sumofpaidamountbysupplier",
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;
	}

	static public double getOwnAccountByCustId(String custid) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "ownaccount/" + custid + "/sumofpaidamountbycustomer",
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;
	}

	// ===================== Additional Expense Report=================
	static public ResponseEntity<List<PaymentDtl>> getPettyExpensesByDate(Date orderDate) {

		String path = HOST + "paymentsummary/" + orderDate + "/paymentsummary";

		ResponseEntity<List<PaymentDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentDtl>>() {
				});
		return response;

	}

	/////////////////////////////// bill summary
	static public ResponseEntity<List<BillSummaryReport>> getDailyBillSummury() {

		String path = HOST + "dailybussinessummary/dailybussinessummary";

		ResponseEntity<List<BillSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BillSummaryReport>>() {
				});
		return response;

	}

	// ==============physical stock
	static public ResponseEntity<PhysicalStockHdr> savePhysicalStockHdr(PhysicalStockHdr physicalStockHdr) {

		return restTemplate.postForEntity(HOST + "physicalstockhdr", physicalStockHdr, PhysicalStockHdr.class);

	}

	static public ResponseEntity<PhysicalStockDtl> savePhysicalStockDtl(PhysicalStockDtl physicalStockDtl) {

		return restTemplate.postForEntity(
				HOST + "physicalstockhdr/" + physicalStockDtl.getPhysicalStockHdrId().getId() + "/physicalstockdtl",
				physicalStockDtl, PhysicalStockDtl.class);

	}

	static public ResponseEntity<List<PhysicalStockDtl>> getPhysicalStockDtl(PhysicalStockHdr physicalStockHdr) {

		String path = HOST + "physicalstockdtltest/" + physicalStockHdr.getId() + "/physicalstockdtl";

		ResponseEntity<List<PhysicalStockDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PhysicalStockDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<PhysicalStockHdr> getPhysicalHdr(PhysicalStockHdr physicalStockHdr) {

		String path = HOST + "physicalstockhdrs/" + physicalStockHdr.getId();

		ResponseEntity<PhysicalStockHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PhysicalStockHdr>() {
				});
		return response;
	}

	static public Map<String, Object> generateIntentHdr() {
		System.out.println(HOST + "generateintenthdr");
		return restTemplate.getForObject(HOST + SystemSetting.systemBranch + "/generateintenthdr", Map.class);

	}

	static public Map<String, Object> generateGraph(String brancheMstList, String type, String fdate, String tdate) {
		System.out.println(HOST + "analyticsreportconsolidatedresource/analyticsreportconsolidated/" + type + "?fdate="
				+ fdate + "&&tdate=" + tdate + "&&branchlist=" + brancheMstList);
		return restTemplate.getForObject(HOST + "analyticsreportconsolidatedresource/analyticsreportconsolidated/"
				+ type + "?fdate=" + fdate + "&&tdate=" + tdate + "&&branchlist=" + brancheMstList, Map.class);

	}

	static public List<HashMap<String, Double>> getProductionValue(String branch, String fdate, String tdate) {
		System.out.println(HOST + "productionsummarydailyresource/productionvalueagainstrawmaterial/" + branch
				+ "?fdate=" + fdate + "&&tdate=" + tdate);
		return restTemplate.getForObject(HOST + "productionsummarydailyresource/productionvalueagainstrawmaterial/"
				+ branch + "?fdate=" + fdate + "&&tdate=" + tdate, List.class);

	}

	static public List<HashMap<String, Double>> getProductionPlanningQty(String itemid, String branch, String fdate,
			String tdate) {
		System.out.println(HOST + "productionsummarydaily/sumofaplaningqty/" + branch + "/" + itemid + "?fdate=" + fdate
				+ "&&tdate=" + tdate);
		return restTemplate.getForObject(HOST + "productionsummarydaily/sumofaplaningqty/" + branch + "/" + itemid
				+ "?fdate=" + fdate + "&&tdate=" + tdate, List.class);

	}

	static public List<HashMap<String, Double>> getActulaProductionQty(String itemid, String branch, String fdate,
			String tdate) {
		System.out.println(HOST + "productionsummarydaily/sumofactualqty/" + branch + "/" + itemid + "?fdate=" + fdate
				+ "&&tdate=" + tdate);
		return restTemplate.getForObject(HOST + "productionsummarydaily/sumofactualqty/" + branch + "/" + itemid
				+ "?fdate=" + fdate + "&&tdate=" + tdate, List.class);

	}

	static public List<HashMap<String, Double>> getRawMaterialValue(String branch, String fdate, String tdate) {
		System.out.println(HOST + "productionsummarydailyresource/productionrawmaterialvalue/" + branch + "?fdate="
				+ fdate + "&&tdate=" + tdate);
		return restTemplate.getForObject(HOST + "productionsummarydailyresource/productionrawmaterialvalue/" + branch
				+ "?fdate=" + fdate + "&&tdate=" + tdate, List.class);

	}

	public static ResponseEntity<List<TableWaiterReport>> getAllocatedTable(String branchCode,
			String customiseSalesMode) {
		String path = HOST + branchCode + "/getplacedtable/" + customiseSalesMode;
		System.out.println(path);
		ResponseEntity<List<TableWaiterReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TableWaiterReport>>() {
				});
		return response;
	}

	static public void updatePhysicalStockHdr(PhysicalStockHdr physicalStockHdr) {

		restTemplate.put(HOST + "physicalstockhdr/" + physicalStockHdr.getId(), physicalStockHdr);
		return;

	}

	static public void deletePhysicalStockDtl(String id) {

		restTemplate.delete(HOST + "physicalstockdtl/" + id);
		return;

	}

	static public ArrayList SearchPhysicalStockHeader() {

		return restTemplate.getForObject(HOST + "physicalstockhdrnovoucher", ArrayList.class);

	}

	/*
	 * 
	 * Stock Management of Physical stock is done while hdr is put . Please dont
	 * call below function. regy
	 * 
	 * static public ResponseEntity<ArrayList> saveStockFromPhysicalStock(String
	 * itemName, String barcode, String batchcode, String qty, String mrp, String
	 * expirydate) {
	 * 
	 * 
	 * ArrayList<ItemMst> arrayItemMst = new ArrayList<ItemMst>();
	 * 
	 * return restTemplate .postForEntity( HOST + "physicalstockfinalsave?itemname="
	 * + itemName + "&barcode=" + barcode + "&batchcode=" + batchcode + "&qty=" +
	 * qty + "&mrp=" + mrp + "&expirydate=" + expirydate, arrayItemMst,
	 * ArrayList.class);
	 * 
	 * }
	 */

	// =================== Save Journal Hdr=========================
	static public ResponseEntity<JournalHdr> saveJournalHdr(JournalHdr journalHdr) {

		return restTemplate.postForEntity(HOST + "journalhdr", journalHdr, JournalHdr.class);

	}

	static public void updateJournalFinalSave(JournalHdr journalHdr) {

		restTemplate.put(HOST + "journalhdr/" + journalHdr.getId(), journalHdr);
		return;

	}

	// ------------------user verification

	static public ResponseEntity<UserMst> verifyUser(String username, String password, String mybranch) {

		String path = HOST + "usermst/" + username + "/" + password + "/" + mybranch + "/login";

		System.out.println(path);

		ResponseEntity<UserMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<UserMst>() {
				});
		return response;
	}

	static public ResponseEntity<UserMst> verifyUserNoPassword(String username, String mybranch) {

		String path = HOST + "usermst/" + username + "/" + mybranch + "/login";

		System.out.println(path);

		ResponseEntity<UserMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<UserMst>() {
				});
		return response;
	}

	// =============== journal hdr update==========================
	static public ResponseEntity<JournalHdr> updateJournalHdr(JournalHdr journalHdr) {

		return restTemplate.postForEntity(HOST + "journalhdr/" + journalHdr.getId(), journalHdr, JournalHdr.class);

	}

	// ==================== Save Journal Detail=======================
	static public ResponseEntity<JournalDtl> saveJournalDtl(JournalDtl journalDtl) {

		return restTemplate.postForEntity(HOST + "journalhdr/" + journalDtl.getJournalHdr().getId() + "/journaldtl",
				journalDtl, JournalDtl.class);

	}

	// ============= journal detail delete===============================
	static public void deleteJournalDtl(String journaldtlId) {

		restTemplate.delete(HOST + "journaldtl/" + journaldtlId);

	}

	// ============= get journal Details============
	static public ResponseEntity<List<JournalDtl>> getJournalDtl(JournalHdr journalHdr) {

		String path = HOST + "journalhdr/" + journalHdr.getId() + "/journaldtls";

		ResponseEntity<List<JournalDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JournalDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<CategoryMst>> getChildCategory(String catName) {

		String path = HOST + "categorymst/getchildcategoryusingparentid/" + catName;

		ResponseEntity<List<CategoryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<CategoryManagementMst>> getChildCategoryManagementMst(String catName) {

		String path = HOST + "categorymanagementmst/getchildCategory/" + catName;

		ResponseEntity<List<CategoryManagementMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryManagementMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<CategoryMst>> getParentCategory() {

		String path = HOST + "categorymst/getparentcategory";

		ResponseEntity<List<CategoryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryMst>>() {
				});

		return response;

	}

	// =================== get debit total====================
	static public double getJournalDebitTotal(String JournalHdrid) {

		return restTemplate.getForObject(HOST + "journaldtl/" + JournalHdrid + "/sumdebitamount", Double.class);

	}

	static public double getJournalCreditTotal(String JournalHdrid) {

		return restTemplate.getForObject(HOST + "journaldtl/" + JournalHdrid + "/sumcreditamount", Double.class);

	}

	// ======================= process mst======================
	static public ResponseEntity<ProcessMst> saveProcessMst(ProcessMst processMst) {

		return restTemplate.postForEntity(HOST + "processcreation", processMst, ProcessMst.class);

	}

	static public void deleteProcessMst(String processId) {

		restTemplate.delete(HOST + "processmst/" + processId);

	}

	static public ResponseEntity<List<ProcessMst>> getprocessMst() {

		String path = HOST + "processmsts";

		ResponseEntity<List<ProcessMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProcessMst>>() {
				});

		return response;

	}

	static public ArrayList getAllUsers() {

		System.out.println("PATH =" + HOST + "allusers");
		return restTemplate.getForObject(HOST + "allusers", ArrayList.class);

	}

	static public ArrayList getAllProcess() {

		System.out.println("PATH =" + HOST + "processmsts");

		return restTemplate.getForObject(HOST + "processmsts", ArrayList.class);

	}
//------------------get processdtl by process name

	static public ResponseEntity<ProcessMst> getProcessDtls(String processName) {

		String path = HOST + "processmstbyname/" + processName;

		ResponseEntity<ProcessMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProcessMst>() {
				});
		return response;
	}
//---------------save process premission

	static public ResponseEntity<ProcessPermissionMst> saveProcessPermissionMst(
			ProcessPermissionMst processPermissionMst) {

		return restTemplate.postForEntity(HOST + "processpermission", processPermissionMst, ProcessPermissionMst.class);

	}

	static public ResponseEntity<UserMst> getUserNameById(String id) {

		String path = HOST + "userdetailbyid/" + id;

		ResponseEntity<UserMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<UserMst>() {
				});
		return response;
	}

	static public ResponseEntity<List<ProcessPermissionMst>> getProcessPermissionbyUserId(String id) {

		String path = HOST + "processpermissionbyuserid/" + id;

		ResponseEntity<List<ProcessPermissionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProcessPermissionMst>>() {
				});
		return response;
	}

	static public ResponseEntity<List<ProductConversionConfigMst>> getAllProductConversionConfigMst() {

		String path = HOST + "productconversionconfigurationmstresource/productconversionconfigurationmsshowall/"
				+ SystemSetting.systemBranch;

		ResponseEntity<List<ProductConversionConfigMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductConversionConfigMst>>() {
				});
		return response;
	}

	static public ResponseEntity<ProcessMst> getProcessById(String id) {

		String path = HOST + "processmstbyid/" + id;

		ResponseEntity<ProcessMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProcessMst>() {
				});
		return response;
	}

	static public ResponseEntity<UserMst> getUserByName(String name) {

		String path = HOST + "userdetailbyname/" + name;

		ResponseEntity<UserMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<UserMst>() {
				});
		return response;
	}

	static public void processPermissionMstDelete(String id) {

		restTemplate.delete(HOST + "processpermissiondelete/" + id);
		return;

	}

//============get permission
	static public ArrayList getPermissionDtl() {

		return restTemplate.getForObject(HOST + "findallprocesspermission", ArrayList.class);

	}

	static public ArrayList getPermissionDtlByUserId(String id) {

		return restTemplate.getForObject(HOST + "processpermissionbyuserid/" + id, ArrayList.class);

	}

	static public ArrayList getPermissionDtlByProcessId(String id) {

		return restTemplate.getForObject(HOST + "processpermissionbyprocessid/" + id, ArrayList.class);

	}

	static public ArrayList getUserRoleByUserId(String id) {

		System.out.println("----- getPermissionDtl--rest caller---" + HOST + "userrole/" + id);

		return restTemplate.getForObject(HOST + "userrole/" + id, ArrayList.class);

	}

//==============get all group
	static public ArrayList getAllGroup() {

		return restTemplate.getForObject(HOST + "allgroup", ArrayList.class);

	}

	static public ResponseEntity<GroupMst> getGroupDtls(String name) {

		String path = HOST + "groupmstbyname/" + name;

		ResponseEntity<GroupMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<GroupMst>() {
				});
		return response;
	}

	static public ResponseEntity<GroupPermissionMst> saveGroupPermissionMst(GroupPermissionMst groupPermissionMst) {

		return restTemplate.postForEntity(HOST + "grouppermission", groupPermissionMst, GroupPermissionMst.class);

	}

	static public ResponseEntity<GroupMst> getGroupNameById(String id) {

		String path = HOST + "groupmstbyid/" + id;

		ResponseEntity<GroupMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<GroupMst>() {
				});
		return response;
	}

	static public ArrayList getGroupPermissionDtl() {

		return restTemplate.getForObject(HOST + "allgrouppermission", ArrayList.class);
	}

	static public void groupPermissionDelete(String id) {

		restTemplate.delete(HOST + "grouppermissionmst/" + id);
		return;

	}

	static public ArrayList getGroupPermissionDtlByProcessId(String id) {

		return restTemplate.getForObject(HOST + "grouppermissionbyprocessid/" + id, ArrayList.class);

	}

	static public ArrayList getGroupPermissionDtlByGroupId(String id) {

		System.out.println("=======Suuuuuuuuuu======grouppermissionbygroupid=======" + id);

		return restTemplate.getForObject(HOST + "grouppermissionbygroupid/" + id, ArrayList.class);

	}

	static public ResponseEntity<UserGroupMst> saveUserGroupPermissionMst(UserGroupMst userGroupMst) {

		return restTemplate.postForEntity(HOST + "usergroup", userGroupMst, UserGroupMst.class);

	}

	static public ArrayList getAllUserGroupPermissionDtl() {

		return restTemplate.getForObject(HOST + "allusergroup", ArrayList.class);

	}

	static public ResponseEntity<List<UserGroupMst>> getUserGroupMst() {

		String path = HOST + "allusergroup";

		System.out.println("==================path=================" + path);

		ResponseEntity<List<UserGroupMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UserGroupMst>>() {
				});

		return response;

	}

	static public void userGroupPermissionDelete(String id) {

		restTemplate.delete(HOST + "usergroupmst/" + id);
		return;

	}

	static public ArrayList getUserGroupPermissionDtlByGroupId(String id) {

		System.out.println("=======Suuuuuuuuuu======getUserGroupPermissionDtlByGroupId=======" + id);

		return restTemplate.getForObject(HOST + "findusergroup/" + id, ArrayList.class);

	}

	static public ArrayList getUserGroupPermissionDtlByUserId(String id) {

		System.out.println("=======Suuuuuuuuuu======getUserGroupPermissionDtlByUserId=======" + id);

		return restTemplate.getForObject(HOST + "usergroupbyuserid/" + id, ArrayList.class);

	}

	// ===============================================
	static public List<Object> getAllTaxRate(String salestranshdrid) {

		String path = HOST + "taxrate/" + salestranshdrid;

		ResponseEntity<List<Object>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Object>>() {
				});

		return response.getBody();

	}

	static public ResponseEntity<Double> getTaxAmount(Double taxrate, String hdrid) {

		String path = HOST + "taxamount/" + hdrid + "/" + taxrate;

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public ResponseEntity<Double> getAllTaxAmount(String hdrid) {

		String path = HOST + "gettotaltaxsummary/" + hdrid;

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public ResponseEntity<Double> getAllCessAmount(String hdrid) {

		String path = HOST + "salesdetails/gettotalcesssummary/" + hdrid;

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public ResponseEntity<Double> getGrandTotalIncludingTax(String hdrid) {

		String path = HOST + "getgrandtotalincludingtax/" + hdrid;

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public SalesTransHdr getSalesTransHdr(String salestranshdrid) {

		String path = HOST + "salestranshdr/" + salestranshdrid;
		ResponseEntity<SalesTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesTransHdr>() {
				});

		return response.getBody();

	}

	static public SalesTransHdr getSalesTransHdrByVoucherNo(String voucher) {

		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/getsalestranshdrbyvoucher/" + voucher;
		ResponseEntity<SalesTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesTransHdr>() {
				});
		return response.getBody();

	}

	static public List<Object> getSalesTransHdrandItem(String salestranshdrid) {

		String path = HOST + "getitemdetails/" + salestranshdrid;

		ResponseEntity<List<Object>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Object>>() {
				});

		return response.getBody();

	}

	static public BranchMst getBranchDtls(String branchCode) {

		String path = HOST + "branchmstbybranchcode/" + branchCode;

		System.out.println(path);
		ResponseEntity<BranchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BranchMst>() {
				});

		return response.getBody();

	}

	static public ResponseEntity<DayEndClosureHdr> saveDayEndClosureHdr(DayEndClosureHdr dayEndClosure) {

		return restTemplate.postForEntity(HOST + "dayendclosure", dayEndClosure, DayEndClosureHdr.class);

	}

	static public ResponseEntity<DayEndClosureDtl> saveDayEndClosureDtl(DayEndClosureDtl dayEndClosureDtl) {

		System.out.println("=======saveDayEndClosureDtl======" + dayEndClosureDtl.getDayEndClosure().getId());
		return restTemplate.postForEntity(
				HOST + "dayendclosurehdr/" + dayEndClosureDtl.getDayEndClosure().getId() + "/dayendclosuredtl",
				dayEndClosureDtl, DayEndClosureDtl.class);

	}

	static public ResponseEntity<List<DayEndClosureDtl>> getDayEndClosureDtl(DayEndClosureHdr dayEndClosure) {

		String path = HOST + "dayendclosurehdr/" + dayEndClosure.getId() + "/dayendclosuredtl";

		System.out.println("==================path=================" + path);

		ResponseEntity<List<DayEndClosureDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DayEndClosureDtl>>() {
				});

		return response;

	}

	// ================= Save Session End
	static public ResponseEntity<SessionEndClosureMst> saveSessionEndClosureHdr(
			SessionEndClosureMst sessionEndClosure) {

		return restTemplate.postForEntity(HOST + "sessionendclosuremst", sessionEndClosure, SessionEndClosureMst.class);

	}

	static public ResponseEntity<SessionEndDtl> saveSessionEndDtl(SessionEndDtl sessionEndDtl) {

		return restTemplate.postForEntity(
				HOST + "sessionendclosurehdr/" + sessionEndDtl.getSessionEndClosure().getId() + "/sessionendclosuredtl",
				sessionEndDtl, SessionEndDtl.class);

	}

	static public void deleteSessionDtl(String id) {

		restTemplate.delete(HOST + "sessionenddtldelete/" + id);
		return;

	}

	static public ResponseEntity<List<SessionEndDtl>> getSessionEndDtl(SessionEndClosureMst sessionEndClosure) {

		String path = HOST + "sessiondtl/" + sessionEndClosure.getId();
		System.out.println("====path======" + path);

		ResponseEntity<List<SessionEndDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SessionEndDtl>>() {
				});

		return response;

	}

	static public ArrayList getDayEndDtlByHdrId(String id) {

		System.out.println("----- getDayEndDtlByHdrId--rest caller---");

		return restTemplate.getForObject(HOST + "dayendclosurehdr/" + id + "/dayendclosuredtl", ArrayList.class);

	}

	static public void updateDayEndClosure(DayEndClosureHdr dayEndClosure) {

		restTemplate.put(HOST + "dayendclosurehdr/" + dayEndClosure.getId(), dayEndClosure);
		return;

	}

	static public void dayEndClosureDtlDelete(String id) {

		restTemplate.delete(HOST + "dayendclosuredtl/" + id);
		return;

	}

	static public void deleteOwnAccSettlement(String id) {

		restTemplate.delete(HOST + "/deleteownaccountsettlementdtl/" + id);
		return;

	}

	static public ResponseEntity<DayEndClosureHdr> getDayEndClosureByDate(String date) {

		String path = HOST + "getdayendclosuremstbydate?voucherdate=" + date;

		System.out.println("========path==========" + path);

		return restTemplate.getForEntity(path, DayEndClosureHdr.class);

	}

	static public ResponseEntity<DayEndClosureHdr> getMaxDayEndClosure() {

		String path = HOST + "dayendclosuremst/maxofdayend/" + SystemSetting.systemBranch;

		System.out.println("========path==========" + path);

		return restTemplate.getForEntity(path, DayEndClosureHdr.class);

	}

	static public double getSummaryDayEndClosure(String id) {

		System.out.println(HOST + "findtotaldayendclosurehdr/" + id);
		return restTemplate.getForObject(HOST + "findtotaldayendclosurehdr/" + id, Double.class);

	}

	// ============sales order

	static public ArrayList getAllSalesOrder() {

		System.out.println(HOST + "getsaleorder");
		return restTemplate.getForObject(HOST + "getsaleorder", ArrayList.class);

	}

	static public ResponseEntity<SalesOrderTransHdr> getSalesOrderTransHdrByVoucherNo(String voucherNumber) {

		String path = HOST + "getsaleorderbyvoucherno/" + voucherNumber + "/" + SystemSetting.systemBranch;

		ResponseEntity<SalesOrderTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesOrderTransHdr>() {
				});
		return response;
	}

//	static public ResponseEntity<CustomerMst> getCustomerById(String custId) {
//
//		String path = HOST + "customermst/" + custId;
//		System.out.println("=============path=========" + path);
//
//		ResponseEntity<CustomerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<CustomerMst>() {
//				});
//		return response;
//	}

	static public ResponseEntity<OrgMst> getOrgById(String id) {

		String path = HOST + "orgssmst/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<OrgMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OrgMst>() {
				});
		return response;
	}

	static public ResponseEntity<OrgMst> getOrgById() {

		String path = HOST + "findallorgmst";
		System.out.println("=============path=========" + path);

		ResponseEntity<OrgMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OrgMst>() {
				});
		return response;
	}

	// =============== Get local customer by id ==============
	static public ResponseEntity<LocalCustomerMst> getLocalCustomerById(String id) {

		String path = HOST + "getlocalcustomerbyid/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<LocalCustomerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<LocalCustomerMst>() {
				});
		return response;
	}

	static public ResponseEntity<List<SalesOrderDtl>> getSalesOrderTransDtlById(String id) {

		String path = HOST + "salesordertranshdr/" + id + "/salesorderdtl";
		System.out.println(path);

		ResponseEntity<List<SalesOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderDtl>>() {
				});
		return response;
	}

	// ======================== Save Production Conversion================
	static public ResponseEntity<ProductConversionMst> saveProductConversionMst(
			ProductConversionMst productConversionMst) {

		return restTemplate.postForEntity(HOST + "productconversionmst", productConversionMst,
				ProductConversionMst.class);

	}

	static public void saveProductConversionMstWithBatch(ProductConversionMst productConversionMst) {
		restTemplate.put(
				HOST + "productconversionhdr/" + productConversionMst.getId() + "/batchwiseporoductconversiondtl",
				productConversionMst);

		return;

	}

	static public ResponseEntity<ProductConversionConfigMst> saveProductConversionConfig(
			ProductConversionConfigMst productConversionConfigMst) {
		return restTemplate.postForEntity(
				HOST + "productconversionconfigurationmstresource/productconversionconfiguration",
				productConversionConfigMst, ProductConversionConfigMst.class);

	}

	static public ResponseEntity<ProductConversionDtl> saveProductConversionDtl(
			ProductConversionDtl productConversionDtl) {

		return restTemplate.postForEntity(HOST + "productconversionhdr/"
				+ productConversionDtl.getProductConversionMst().getId() + "/poroductconversiondtl",
				productConversionDtl, ProductConversionDtl.class);
	}

	static public void deleteProductConversionDtl(String prodcutconversiondtlid) {

		restTemplate.delete(HOST + "productconversiondtl/" + prodcutconversiondtlid);

	}

	static public ResponseEntity<List<ProductConversionDtl>> getProductConversionDtl(
			ProductConversionMst productConversionMst) {

		String path = HOST + "productconversionbyid/" + productConversionMst.getId();

		ResponseEntity<List<ProductConversionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductConversionDtl>>() {
				});

		return response;

	}

	// =======get sales order hrd
	static public ResponseEntity<SalesDtl> getSalesDtlBydtlId(String id) {

		String path = HOST + "salesdtl/" + id + "/getsalesdtlbyid";
		System.out.println("=============path=========" + path);

		ResponseEntity<SalesDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesDtl>() {
				});
		return response;
	}

	static public ResponseEntity<List<SalesDtl>> getSalesDtlByItemAndBatch(String Hdrid, String itemID, String batch) {

		String path = HOST + "salesdetail/" + Hdrid + "/" + itemID + "/" + batch + "/getsalesdetailsbyitem";
		System.out.println("=============path=========" + path);

		ResponseEntity<List<SalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDtl>>() {
				});
		return response;
	}

	static public ResponseEntity<List<SalesDtl>> getSalesDtlById(String id) {

		String path = HOST + "salesordertranshdr/" + id + "/salesorderdtl";
		System.out.println("=============path=========" + path);

		ResponseEntity<List<SalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDtl>>() {
				});
		return response;
	}

	static public ResponseEntity<List<BranchMst>> getOtherBranchesDtl() {

		String path = HOST + "otherbranches";

		ResponseEntity<List<BranchMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchMst>>() {
				});

		return response;

	}

	static public ArrayList SearchVoucherNo(String searchData) {
		System.out.println(HOST + "getvouchernosearch?data=" + searchData);
		return restTemplate.getForObject(HOST + "getvouchernosearch?data=" + searchData, ArrayList.class);

	}

	static public ArrayList SearchPenidngIntentInVoucherNo(String searchData) {
		System.out.println(HOST + "intentinhdr/searchpendingvoucher?data=" + searchData);
		return restTemplate.getForObject(HOST + "intentinhdr/searchpendingvoucher?data=" + searchData, ArrayList.class);

	}

	static public ResponseEntity<BranchMst> getBranchMstById(String id) {
		String path = HOST + "branchmstbyid/" + id;
		System.out.println(path);
		ResponseEntity<BranchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BranchMst>() {
				});
		return response;
	}

//====================== Stock in detail get======================
	static public ResponseEntity<List<StockTransferInDtl>> getStockTransferInDtl(String id) {

		// stocktransferinhdr/{stocktransferinhdrId}/stocktransferindtl
		String path = HOST + "stocktransferinhdr/" + id + "/stocktransferindtl";
		System.out.println("=============path=========" + path);

		ResponseEntity<List<StockTransferInDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInDtl>>() {
				});
		return response;
	}

	static public ResponseEntity<List<StockTransferInReturnDtl>> getStockTransferInRetyurnDtl(String id) {

		// stocktransferinhdr/{stocktransferinhdrId}/stocktransferindtl
		String path = HOST + "stocktransferinhdr/" + id + "/stocktransferindtl";
		System.out.println("=============path=========" + path);

		ResponseEntity<List<StockTransferInReturnDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInReturnDtl>>() {
				});
		return response;
	}

	// ====================== other branch acknowledgement in detail
	// get======================
	static public ResponseEntity<List<OtherBranchSalesDtl>> getOtherBranchSalesDtl(String id) {

		String path = HOST + "otherbranchsalestranshdr/" + id + "/otherbranchsalesdtl";
		System.out.println("=============path=========" + path);

		ResponseEntity<List<OtherBranchSalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OtherBranchSalesDtl>>() {
				});
		return response;
	}

	static public ResponseEntity<SessionEndClosureMst> getSessionEndClosureByDate(String date, String userid) {

		String path = HOST + "getsessionendclosuremstbyuserid/" + userid + "/date?processdate=" + date;

		System.out.println("========path==========" + path);

		return restTemplate.getForEntity(path, SessionEndClosureMst.class);

	}

	static public void updateSessionEndClosure(SessionEndClosureMst sessionEndClosureMst) {

		restTemplate.put(HOST + "sessionendclosurehdr/" + sessionEndClosureMst.getId(), sessionEndClosureMst);
		return;

	}

	static public double getSummarySessionEndClosure(String id) {

		return restTemplate.getForObject(HOST + "totalsessionendphysicalcash/" + id, Double.class);

	}

	static public double getStockOfItem(String itemid) {

		return restTemplate.getForObject(HOST + "stockresource/stockifitem/" + itemid, Double.class);

	}

//=============== get voucher no======================
	static public ArrayList SearchIntentNo(String searchData) {
		return restTemplate.getForObject(HOST + "intenthdrinsearch?data=" + searchData, ArrayList.class);

	}

	// ======================= get intent in dtls =======================
	static public ResponseEntity<List<IntentInDtl>> getintentdtlsbyId(String intentid) {

		String path = HOST + "intentindtl/" + intentid + "/intentindtls";

		System.out.println(path);
		ResponseEntity<List<IntentInDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<IntentInDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PurchaseSchemeMst>> getPurchaseSchemeMst() {

		String path = HOST + "purchaseschememstresource/purchaseschememstfindall";

		ResponseEntity<List<PurchaseSchemeMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseSchemeMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<AccountHeads>> getAccountHeads() {

		String path = HOST + "accountheads";

		ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountHeads>>() {
				});

		return response;

	}

//	/{vouchernumber}/{vouchertype}
	static public ResponseEntity<LmsQueueMst> getlmsQueueByDateAndVoucherNo(String vNo, String vType, String vDate) {

		String path = HOST + "lmsqueuemstbydateandvouchernumber/" + vNo + "/" + vType + "?fdate=" + vDate;

		System.out.println(path);
		ResponseEntity<LmsQueueMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<LmsQueueMst>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductConversionReport>> getProductConversionReport(String fDate,
			String toDate) {

		String path = HOST + "productconversionreportresource/productconversionreport/" + SystemSetting.systemBranch
				+ "?fdate=" + fDate + "&&tdate=" + toDate;

		System.out.println(path);
		ResponseEntity<List<ProductConversionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductConversionReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockValueReports>> getStockValueReport(String fDate) {

		String path = HOST + "stocksummaryallitem/" + SystemSetting.systemBranch + "?fdate=" + fDate;

		System.out.println(path);
		ResponseEntity<List<StockValueReports>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockValueReports>>() {
				});

		return response;

	}

	static public ResponseEntity<AccountHeads> getAccountById(String accountId) {

		String path = HOST + "accountheadsbyid/" + accountId;
		ResponseEntity<AccountHeads> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountHeads>() {
				});

		return response;

	}

	static public ResponseEntity<ItemLocationMst> getItemLocationMst(String itemId, String floor, String shelf,
			String rack) {

		String path = HOST + "itemlocationmstresource/getitembylocation/" + itemId + "/" + floor + "/" + shelf + "/"
				+ rack;
		ResponseEntity<ItemLocationMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ItemLocationMst>() {
				});

		return response;

	}

	static public ResponseEntity<UrsMst> getUrsMstByDateAndItemId(String itemId, String date) {

		String path = HOST + "ursmstresource/ursshowbydateanditemid/" + itemId + "/" + SystemSetting.systemBranch
				+ "?rdate=" + date;
		ResponseEntity<UrsMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<UrsMst>() {
				});

		return response;

	}

	static public ResponseEntity<List<ConsumptionReasonMst>> getAllConsumptionReason() {

		String path = HOST + "consumptionreasonmstresource/findallconsumptionreasonmst";
		ResponseEntity<List<ConsumptionReasonMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReasonMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationMst>> getAllItemLocationMst() {

		String path = HOST + "itemlocationmstresource/getallitemlocation/" + SystemSetting.systemBranch;
		ResponseEntity<List<ItemLocationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationMst>> getAllItemLocationMstByItemId(String itemid) {

		String path = HOST + "itemlocationmstresource/getlocationbyitemid/" + itemid + "/" + SystemSetting.systemBranch;
		ResponseEntity<List<ItemLocationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationMst>> getAllItemLocationMstByFloor(String floor) {

		String path = HOST + "itemlocationmstresource/getlocationbyfloor/" + floor + "/" + SystemSetting.systemBranch;
		ResponseEntity<List<ItemLocationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<UrsMst>> getUrsMstDate(String sdate) {

		String path = HOST + "ursmstresource/ursshowbydate?rdate=" + sdate;
		ResponseEntity<List<UrsMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UrsMst>>() {
				});

		return response;

	}

	static public ResponseEntity<UrsMst> getUrsMstById(String id) {

		String path = HOST + "ursmstresource/ursshowbyid/" + id;
		ResponseEntity<UrsMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<UrsMst>() {
				});

		return response;

	}

	static public ResponseEntity<List<CategoryWiseStockMovement>> getCategoryWiseStockMovement(String fdate,
			String tDate) {

		String path = HOST + "categorywisestockmovement1/" + SystemSetting.systemBranch + "?fromdate="

				+ fdate + "&todate=" + tDate;

		ResponseEntity<List<CategoryWiseStockMovement>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryWiseStockMovement>>() {
				});
		return response;

	}

	static public ResponseEntity<List<CategoryWiseStockMovement>> getItemWiseWiseStockMovement(String catId,
			String fdate, String tDate) {

		String path = HOST + "categorywisestockmovementreportresource/categorywisestockmovementreport/"
				+ SystemSetting.systemBranch + "/" + catId + "?fromdate=" + fdate + "&&todate=" + tDate;
		ResponseEntity<List<CategoryWiseStockMovement>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryWiseStockMovement>>() {
				});
		return response;

	}

	static public ResponseEntity<TaxMst> getTaxMstByItemIdAndTaxId(String itemId, String taxId) {

		String path = HOST + "taxmst/taxbyitemidtaxid/" + itemId + "/" + taxId;
		ResponseEntity<TaxMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<TaxMst>() {
				});

		return response;

	}

	static public ResponseEntity<List<TaxMst>> getAllTaxs() {

		String path = HOST + "getalltaxmsts/";
		ResponseEntity<List<TaxMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<TaxMst>> getAllTaxsByItemIdAndTaxId(String itemId, String taxId) {

		String path = HOST + "getalltaxmstbyitemidandtaxid/" + itemId + "/" + taxId;
		ResponseEntity<List<TaxMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<TaxMst>> getAllTaxsByItemId(String itemId) {

		String path = HOST + "getalltaxmstbyitemid/" + itemId;
		ResponseEntity<List<TaxMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxMst>>() {
				});

		return response;

	}

//	static public ResponseEntity<Supplier> getSupplierByAccountById(String accountId) {
//
//		String path = HOST + "suppliersbyaccountid/" + accountId;
//		ResponseEntity<Supplier> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<Supplier>() {
//				});
//
//		return response;
//
//	}

	static public ResponseEntity<List<SummaryStatementOfAccountReport>> getStatementOfAccounts() {

		String path = HOST + "statementofaccountreport";

		ResponseEntity<List<SummaryStatementOfAccountReport>> response = restTemplate.exchange(path, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<SummaryStatementOfAccountReport>>() {
				});

		return response;

	}

//	static public ResponseEntity<List<CustomerMst>> getCustomerDtls() {
//
//		String path = HOST + "customerregistrations";
//
//		ResponseEntity<List<CustomerMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<CustomerMst>>() {
//				});
//
//		return response;
//
//	}

	static public ResponseEntity<List<StockReport>> getDailystockReport(String branchcode, String date,
			String categoryId) {

		String path = HOST + "stockreport/" + branchcode + "/" + categoryId + "?fromdate=" + date;

		System.out.println(path); 

		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});
		

		return response;
	}

	@SuppressWarnings("unchecked")
	static public ArrayList getItemWiseStock(String branchcode, String date) {
		// String path = HOST + "stockreport/" + branchcode + "/" + categoryId +
		// "?fromdate=" + date;
		String path = HOST + "stockreport/dailystockreport" + "/" + branchcode + "?fromdate=" + date;

		System.out.println(path);

		return restTemplate.getForObject(path, ArrayList.class);

	}

	static public ResponseEntity<List<PaymentVoucher>> getPaymentVoucherReport(String branchcode, String voucherNo,
			String date) {
		String path = HOST + "paymentreport/" + voucherNo + "/" + branchcode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<PaymentVoucher>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentVoucher>>() {
				});

		return response;
	}

	static public ResponseEntity<List<OrgPaymentVoucher>> getOrgPaymentVoucherReport(String branchcode,
			String voucherNo, String date, String memid) {
		String path = HOST + "paymentreport/" + voucherNo + "/" + branchcode + "/" + memid + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<OrgPaymentVoucher>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgPaymentVoucher>>() {
				});

		return response;
	}

//	static public ResponseEntity<List<Supplier>> getSupplierDtls() {
//
//		String path = HOST + "suppliers";
//
//		ResponseEntity<List<Supplier>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<Supplier>>() {
//				});
//
//		return response;
//
//	}

	static public ResponseEntity<List<SalesTransHdr>> getCustomerWiseReport(String date) {

		String branchCode = SystemSetting.systemBranch;

		String path = HOST + "salestranshdrreport/dailycustomerwisereport/" + branchCode + "/?rdate=" + date;
		System.out.println(path);
		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesOrderDtl>> getSaleOrderStatusReport(String date) {

		String path = HOST + "saleorderstatusreport?rdate=" + date;
		System.out.println(path);
		ResponseEntity<List<SalesOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesTransHdr>> getSalesDtls() {

		String path = HOST + "saless";

		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesInvoiceReport>> getInvoice(String voucherNumber, String date) {

		String path = HOST + "salesinvoice/" + voucherNumber + "/" + SystemSetting.systemBranch + "?rdate=" + date;
		System.out.println(path);

		ResponseEntity<List<SalesInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesInvoiceReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesReturnInvoiceReport>> getSalesReturnInvoice(String voucherNumber,
			String date) {

		String path = HOST + "salesreturninvoiceresource/salesreturninvoice/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);

		ResponseEntity<List<SalesReturnInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesReturnInvoiceReport>>() {
				});

		return response;

	}
//	static public ResponseEntity<List<SalesReturnInvoiceReport>> getReturnInvoice(String voucherNumber, String date) {
//
//		String path = HOST + "salesinvoice/" + voucherNumber + "?rdate=" + date;
//		System.out.println(path);
//
//		ResponseEntity<List<SalesReturnInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<SalesReturnInvoiceReport>>() {
//				});
//
//		return response;
//
//	}

	static public ResponseEntity<List<StockTransferOutReport>> getOtherBranchStockTransfer(String voucherNumber,
			String date) {

		String path = HOST + "otherbranchsalestranshdr/getstocktransfervoucher/" + voucherNumber + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<StockTransferOutReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferOutReport>> getStockTransferInvoice(String voucherNumber,
			String date) {

		String path = HOST + "stocktransferoutreport/" + voucherNumber + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<StockTransferOutReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutReport>>() {
				});

		return response;

	}

	// @GetMapping("{companymstid}/stocktransferoutreport/getreportbyhdrid/{hdrid}")
	static public ResponseEntity<List<StockTransferOutReport>> getStockTransferInvoiceByHdrid(String hdrid) {

		String path = HOST + "stocktransferoutreport/getreportbyhdrid/" + hdrid;

		System.out.println(path);

		ResponseEntity<List<StockTransferOutReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferOutReport>> stockOutSummaryBetweenDate(String fdate, String tdate) {

		String path = HOST + "stocktransferoutreport/stocktransferoutsummaryreportbetweendate?fromdate=" + fdate
				+ "&todate=" + tdate;

		System.out.println(path);

		ResponseEntity<List<StockTransferOutReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionDtlsReport>> getProductionDtlReport(String date, String banchCode) {

		String path = HOST + "productiondtlreports/" + banchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<ProductionDtlsReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtlsReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ServiceInvoice>> getServiceInvoice(String voucherNumber, String voucherDate) {

		String path = HOST + "serviceinvoice/" + SystemSetting.systemBranch + "/" + voucherNumber + "?fdate="
				+ voucherDate;
		System.out.println(path);
		ResponseEntity<List<ServiceInvoice>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ServiceInvoice>>() {
				});

		return response;

	}

	static public ResponseEntity<List<DailySalesReport>> getDailySalesReport1(String branchCode, String date) {

		String path = HOST + "stocktransferoutreport/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<DailySalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockReport>> getDailyStockReport(String branchCode, String date) {

		String path = HOST + "stockreport/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PurchaseReport>> getPurchaseInvoice(String voucherNumber, String date) {

		String path = HOST + "purchasereport/" + voucherNumber + "/" + SystemSetting.systemBranch + "?rdate=" + date;

		System.out.println(path);
		System.out.println("purchaseeee 1111333333333333322222222222222255555566666777777111111");
		ResponseEntity<List<PurchaseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PurchaseReport>> getPurchaseOrderInvoice(String voucherNumber, String date) {

		String path = HOST + "purchaseorderreport/" + voucherNumber + "/" + SystemSetting.systemBranch + "?rdate="
				+ date;

		System.out.println(path);
		System.out.println("purchase");
		ResponseEntity<List<PurchaseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReceiptInvoice>> getreceipts(String voucherNumber, String date) {

		String path = HOST + "receiptinvoice/" + voucherNumber + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<ReceiptInvoice>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptInvoice>>() {
				});

		return response;
	}

	static public ResponseEntity<List<ReceiptHdr>> getAllreceipts() {

		String path = HOST + "receipthdrs";

		System.out.println(path);

		ResponseEntity<List<ReceiptHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptHdr>>() {
				});

		return response;
	}

	/// {voucherDate}
	public static ArrayList getAllreceiptsByVoucherDate(String voucherDate) {

		String path = HOST + "invoiceeditenable/getreceiptbydate/" + SystemSetting.systemBranch + "/"
				+ SystemSetting.getUser().getId() + "?voucherDate=" + voucherDate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);

	}

	public static ArrayList getAllpaymentsByVoucherDate(String voucherDate) {

		String path = HOST + "invoiceeditenable/getpaymentbydate/" + SystemSetting.systemBranch + "/"
				+ SystemSetting.getUser().getId() + "?voucherDate=" + voucherDate;

		System.out.println(path);

		return restTemplate.getForObject(path, ArrayList.class);

	}

	static public ResponseEntity<List<RawMaterialIssueReport>> getRawMaterial(String voucherNumber, String date) {

		String path = HOST + "rawmaterialissuedtl/" + voucherNumber + "/rawmaterialissuereport?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<RawMaterialIssueReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RawMaterialIssueReport>>() {
				});

		return response;
	}

	static public ResponseEntity<List<CustomerBalanceReport>> getCustomerBillBalance(String fdate, String tdate,
			String cusId) {

		String path = HOST + "accountreceivable/customerbalance/" + cusId + "?fromdate=" + fdate + "&&todate=" + tdate;

		System.out.println(path);

		ResponseEntity<List<CustomerBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CustomerBalanceReport>>() {
				});

		return response;
	}

	static public ResponseEntity<List<CustomerBalanceReport>> getAllCustomerBillBalance(String fdate, String tdate) {

		String path = HOST + "accountreceivable/allcustomerbalance" + "?fromdate=" + fdate + "&&todate=" + tdate;

		System.out.println(path);

		ResponseEntity<List<CustomerBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CustomerBalanceReport>>() {
				});

		return response;
	}

	static public ResponseEntity<List<AccountBalanceReport>> getAllCustomerBillBalanceBetweenDate(String fdate,
			String tdate) {

		String path = HOST + "accountingresource/accountstatement/" + SystemSetting.getSystemBranch() + "?startdate="
				+ fdate + "&&enddate=" + tdate;

		System.out.println(path);

		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});

		return response;
	}

	static public ResponseEntity<List<RawMaterialIssueReport>> getRawMaterialBetweenDate(String fdate, String tdate) {

		String path = HOST + "rawmaterialissuereport/" + SystemSetting.systemBranch + "?fdate=" + fdate + "&&tdate="
				+ tdate;

		System.out.println(path);

		ResponseEntity<List<RawMaterialIssueReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RawMaterialIssueReport>>() {
				});

		return response;
	}

	static public ResponseEntity<List<RawMaterialReturnReport>> getRawMaterialReturn(String voucherNumber,
			String date) {

		String path = HOST + "rawmaterialreturndtl/" + voucherNumber + "/rawmaterialreturnreport?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<RawMaterialReturnReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RawMaterialReturnReport>>() {
				});

		return response;
	}

	static public ResponseEntity<List<OrgReceiptInvoice>> getOrgreceipts(String voucherNumber, String date,
			String memberId) {

		String path = HOST + "orgreceiptinvoice/" + voucherNumber + "/" + memberId + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<OrgReceiptInvoice>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgReceiptInvoice>>() {
				});

		return response;
	}

	static public ResponseEntity<List<OrgReceiptInvoice>> getOrgreceiptsReprint(String voucherNumber, String date,
			String memberId) {

		String path = HOST + "orgreceiptinvoice/" + voucherNumber + "/" + memberId + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<OrgReceiptInvoice>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgReceiptInvoice>>() {
				});

		return response;
	}

	static public ResponseEntity<List<PurchaseReport>> getPurchaseTax(String voucherNumber, String date) {

		String path = HOST + "retrivetaxrate/" + voucherNumber + "/" + SystemSetting.systemBranch + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<PurchaseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReport>>() {
				});

		return response;

	}

	static public ResponseEntity<DailySaleSummaryReport> getDailySalesSummaryReport(String branchcode, String date) {

		String path = HOST + "dailysalesummary/" + branchcode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<DailySaleSummaryReport> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DailySaleSummaryReport>() {
				});

		return response;

	}

	static public ResponseEntity<DailySalesReport> getSalesR(String branchCode, String date) {

		String path = HOST + "daily/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<DailySalesReport> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DailySalesReport>() {
				});

		return response;

	}

	static public ResponseEntity<List<DailySalesReportDtl>> getDailySaleReportAll(String branchCode, String date,
			String strDate1) {

		String path = HOST + "dailysalesreportdtl/" + branchCode + "?rdate=" + date + "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<DailySalesReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReportDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferInExceptionHdr>> getStockTransferInExceptionReportHdr(
			String branchCode, String date, String strDate1) {

		String path = HOST + "stocktransferinexceptionhdrresource/stocktransferinhdrexceptionreport" + branchCode
				+ "?fromdate=" + date + "&&todate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<StockTransferInExceptionHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInExceptionHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReceiptReports>> getReceiptReportPrinting(String branchCode, String date,
			String strDate1) {

		String path = HOST + "/receipthdr/receiptreportprinting/" + branchCode + "?fromdate=" + date + "&&todate="
				+ strDate1;

		System.out.println(path);

		ResponseEntity<List<ReceiptReports>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptReports>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PaymentReports>> paymentReportPrintinge(String branchCode, String date,
			String strDate1) {

		String path = HOST + "/receipthdr/receiptreportprinting/" + branchCode + "?fromdate=" + date + "&&todate="
				+ strDate1;

		System.out.println(path);

		ResponseEntity<List<PaymentReports>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentReports>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SalesProfitReportHdr>> getSalesProfit(String branchCode, String date,
			String strDate1) {

		String path = HOST + "salesprofitreportresource/salesprofitreport/" + branchCode + "?rdate=" + date + "&&tdate="
				+ strDate1;

		System.out.println(path);

		ResponseEntity<List<SalesProfitReportHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesProfitReportHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<List<DailySalesReportDtl>> getSalesDtlDaily(String branchCode, String date,
			String strDate1) {

		String path = HOST + "dailysalesreport/salesdtldaily/" + branchCode + "?rdate=" + date + "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<DailySalesReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReportDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<MonthlySalesTransHdr>> getMonthlySalesReport(String branchCode, String date,
			String strDate1) {

		String path = HOST + "monthlysalesreportresource/" + branchCode + "/monthlysalesreport?rdate=" + date
				+ "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<MonthlySalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MonthlySalesTransHdr>>() {
				});

		return response;

	}

	static public ResponseEntity<String> salesToMonthlySales(String branchCode, String date, String strDate1) {

		String path = HOST + "copytotables/" + branchCode + "?rdate=" + date + "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;

	}

	static public ResponseEntity<List<WeighBridgeWeights>> getWeighBridgeReport(String startDate, String strDate1) {

		String path = HOST + "weighbridgereportresurce/weighbridgereport?fromdate=" + startDate + "&&todate="
				+ strDate1;

		System.out.println(path);

		ResponseEntity<List<WeighBridgeWeights>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeWeights>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ConsumptionHdrReport>> getConsumptionHdrReport(String branchCode, String date,
			String strDate1) {

		String path = HOST + "consumptionreportresourec/distinctconsumptionreason/" + branchCode + "?rdate=" + date
				+ "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<ConsumptionHdrReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionHdrReport>>() {
				});

		return response;

	}

	static public ResponseEntity<Double> getConsumptionAmount(String branchCode, String date, String strDate1) {

		String path = HOST + "consumptionreportresourec/consumptionamount/" + branchCode + "?rdate=" + date + "&&tdate="
				+ strDate1;

		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReceiptModeReport>> getReceiptModeReportBetweenDate(String branchCode,
			String date, String strDate1) {

		String path = HOST + "/salesreceipts/getreceiptmodebetweendate/" + branchCode + "?fdate=" + date + "&&tdate="
				+ strDate1;

		System.out.println(path);

		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PharmacySalesReturnReport>> getPharmacySalesReturn(String fdate, String tDate,
			String branchCode, String category) {

		String path = HOST + "pharmacysalesreturnreportresource/" + category + "/getpharmacysalesreturn/" + branchCode
				+ "?fdate=" + fdate + "&&tdate=" + tDate;

		System.out.println(path);

		ResponseEntity<List<PharmacySalesReturnReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacySalesReturnReport>>() {
				});

		return response;
	}

	static public ResponseEntity<List<PharmacyItemMovementAnalisysReport>> getPharmacyItemMovement(String fdate,
			String tDate, String branchCode, String category, String item, String batch) {

		String path = HOST + "pharmacyinventorymovementreportresource/" + batch
				+ "/getitemmovementreportbycatanditem/" + branchCode + "?fdate=" + fdate + "&&tdate=" + tDate 
				+ "&&category=" + category + "&&item=" + item;
		

		System.out.println("PharmacyItemMovementAnalisysReport");
		System.out.println(path);

		ResponseEntity<List<PharmacyItemMovementAnalisysReport>> response = restTemplate.exchange(path, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<PharmacyItemMovementAnalisysReport>>() {
				});

		return response;
	}

	static public ArrayList getAllMemberWiseReceiptReport(String fdate, String tDate) {

		String path = HOST + "memberwisereportresource/allmemberwisereceiptreport?startDate=" + fdate + "&&endDate="
				+ tDate;

		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);
	}

	static public ResponseEntity<List<PharmacyPurchaseReport>> getPharmacyPurchaseReport(String fdate, String tDate,
			String branchCode, String category) {

		String path = HOST + "purchasereport/" + category + "/getpharmacypurchasereport/" + branchCode + "?fdate="
				+ fdate + "&&tdate=" + tDate;

		System.out.println(path);

		ResponseEntity<List<PharmacyPurchaseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyPurchaseReport>>() {
				});

		return response;
	}

//	static public ResponseEntity<List<PharmacyGroupWiseSalesReport>> getPharmacyGroupWiseSales(String fdate,
//			String tDate, String branchCode, String category) {
//
//		String path = HOST + "salestranshdrreport/" + category + "/getpharmacygroupwisesalesreport/" + branchCode
//				+ "?fdate=" + fdate + "&&tdate=" + tDate;
//
//		System.out.println(path);
//
//		ResponseEntity<List<PharmacyGroupWiseSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<PharmacyGroupWiseSalesReport>>() {
//				});
//
//		return response;
//	}
	
	
	/////........................Sibi....................// 8-07/2021///
	static public ResponseEntity<List<PharmacyGroupWiseSalesReport>> getPharmacyGroupWiseSales(String fdate,
			String tDate, String branchCode, String category) {

		String path = HOST + "salestranshdrreport/getpharmacygroupwisesalesreport/" + branchCode
				+ "?fdate=" + fdate + "&&tdate=" + tDate+"&&category="+category;

		System.out.println(path);

		ResponseEntity<List<PharmacyGroupWiseSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyGroupWiseSalesReport>>() {
				});

		return response;
	}

//	static public ResponseEntity<List<PharmacySalesDetailReport>> getPharmacSalesDtlReport(String fdate, String tDate,
//			String branchCode, String category) {
//
//		String path = HOST + "salestranshdrreport/" + category + "/getpharmacygroupwisesalesdtlreport/" + branchCode
//				+ "?fdate=" + fdate + "&&tdate=" + tDate;
//
//		
//		
//		System.out.println(path);
//		
//
//		ResponseEntity<List<PharmacySalesDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<PharmacySalesDetailReport>>() {
//				});
//
//		return response;
//	}
	static public ResponseEntity<List<PharmacySalesDetailReport>> getPharmacSalesDtlReport(String fdate, String tDate,
			String branchCode, String category) {

		String path = HOST + "salestranshdrreport/getpharmacygroupwisesalesdtlreport/" + branchCode
         + "?fdate=" + fdate + "&&tdate=" + tDate+"&&category="+category;

      System.out.println(path);

		ResponseEntity<List<PharmacySalesDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacySalesDetailReport>>() {
				});

		return response;
	}

	static public ResponseEntity<List<DailySalesReportDtl>> getDailySaleReport(String branchCode, String date,
			String strDate1) {

		String path = HOST + "dailysalesreportdtl/" + branchCode + "?rdate=" + date + "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<DailySalesReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReportDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<DailySalesReportDtl>> getSalesReport(String branchCode, String date) {

		String path = HOST + "dailysalerreportresource/dailysalesreport/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<DailySalesReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReportDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<String> getVoucherDateUpdate(String startingVoucherNumber, String endingVoucherNumber,
			String convertedToDate, String voucherSuffix, String branchCode, String odlDate) {

		// http://localhost:8082/AMB/updatevoucherdate/AMB?rdate=2020-06-03&&startvoucher=B000252&&endvoucher=B000252&&olddate=2020-05-29&&vouchersuffix=A
		String path = HOST + "updatevoucherdate/" + branchCode + "?newdate=" + convertedToDate + "&&startvoucher="
				+ startingVoucherNumber + "&&endvoucher=" + endingVoucherNumber + "&&olddate=" + odlDate
				+ "&&vouchersuffix=" + voucherSuffix;

		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;

	}

	static public ResponseEntity<List<ConsumptionReport>> getConsumptionReport(String branchCode, String date,
			String strDate1) {

		String path = HOST + "consumptionreportresourec/dailyconsumption/" + branchCode + "?rdate=" + date + "&&tdate="
				+ strDate1;

		System.out.println(path);

		ResponseEntity<List<ConsumptionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReceiptModeWiseReport>> getReceiptModeWiseReport(String branchCode, String date) {

		String path = HOST + "dailysalesreportresourec/receiptmodewisereport/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<ReceiptModeWiseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeWiseReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReceiptModeWiseReport>> getReceiptModeWiseSummaryReport(String branchCode,
			String date) {

		String path = HOST + "dailysalesreportresourec/receiptmodewisesummaryreport/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<ReceiptModeWiseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeWiseReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReceiptModeWiseReport>> getReceiptModeCashWiseReport(String branchCode,
			String date) {

		String path = HOST + "dailysalesreportresourec/receiptmodecashwisereport/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<ReceiptModeWiseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeWiseReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SettledAmountDtlReport>> getSettledAmt(String branchCode, String date) {

		String path = HOST + "dailysalesreportresourec/settleamountdtlreport/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<SettledAmountDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SettledAmountDtlReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<SettledAmountDtlReport>> getSettledAmtCash(String branchCode, String date) {

		String path = HOST + "dailysalesreportresourec/settleamountdtlcashreport/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<SettledAmountDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SettledAmountDtlReport>>() {
				});

		return response;

	}

	static public ResponseEntity<Double> getSettlementAmount(String branchCode, String date) {

		String path = HOST + "dailysalesreportresourec/saleordersettledamount/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public ResponseEntity<Double> getSettlementCashAmount(String branchCode, String date) {

		String path = HOST + "dailysalesreportresourec/saleordersettledcashamount/" + branchCode + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public ResponseEntity<List<DailySalesReturnReportDtl>> getDailySaleReturnReportAll(String branchCode,
			String date, String strDate1) {

		String path = HOST + "dailysalesreportdtl/salesreturn/" + branchCode + "?rdate=" + date + "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<DailySalesReturnReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReturnReportDtl>>() {
				});

		return response;

	}

//=====================================reprint voucher==========================//
	static public ResponseEntity<List<VoucherReprintDtl>> getVoucherReprintDtl(String voucherNumber,
			String branchName) {

		String path = HOST + "voucherreprint/" + voucherNumber + "/" + branchName;

		System.out.println(path);

		ResponseEntity<List<VoucherReprintDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<VoucherReprintDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockTransferInExceptionDtl>> getExceptionReportDtl(String voucherNumber,
			String vDate) {

		String path = HOST + "stocktransferinexceptionhdrresource/stocktransferindtlexceptionreport/" + voucherNumber
				+ "?fromdate=" + vDate;

		System.out.println(path);

		ResponseEntity<List<StockTransferInExceptionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInExceptionDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<MonthlySalesDtl>> getMonthlySalesReportDtl(String voucherNumber,
			String branchName) {

		String path = HOST + "/monthlysalesreportresource/monthlysalesreportdtl/" + voucherNumber;

		System.out.println(path);

		ResponseEntity<List<MonthlySalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MonthlySalesDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ConsumptionReportDtl>> getConsumerReprintDtl(String reasonId, String fromDate,
			String toDate) {

		String path = HOST + "/consumptionreportresourec/consumptiondtlreport/" + reasonId + "/"
				+ SystemSetting.systemBranch + "?rdate=" + fromDate + "&&tdate=" + toDate;

		System.out.println(path);

		ResponseEntity<List<ConsumptionReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReportDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<VoucherReprintDtl>> getSalesReturnVoucherReprintDtl(String voucherNumber,
			String branchName) {

		String path = HOST + "voucherreprint/salesreturn/" + voucherNumber + "/" + branchName;

		System.out.println(path);

		ResponseEntity<List<VoucherReprintDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<VoucherReprintDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<DailySaleSummaryReport>> getOpeningPettyCashReport(String branchCode,
			String date) {

		String path = HOST + "openingpettycash/" + branchCode + "?rdate=" + date;

		ResponseEntity<List<DailySaleSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySaleSummaryReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<DailySaleSummaryReport>> getAdvanceReceivedReport(String branchCode,
			String date) {

		String path = HOST + "advancerecivedfromorder/" + branchCode + "?rdate=" + date;

		ResponseEntity<List<DailySaleSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySaleSummaryReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<DailySaleSummaryReport>> getDailySalesDetails(String branchCode, String date) {

		String path = HOST + "dailysalesdtl/" + branchCode + "?rdate=" + date;
		ResponseEntity<List<DailySaleSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySaleSummaryReport>>() {
				});
		return response;
	}

	static public ResponseEntity<List<DailySaleSummaryReport>> getCreditSalesDetails(String branchCode, String date) {
		String path = HOST + "dailycreditsalesdtl/" + branchCode + "?rdate=" + date;
		ResponseEntity<List<DailySaleSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySaleSummaryReport>>() {
				});
		return response;
	}

	static public ResponseEntity<List<TaxSummaryMst>> getInvoiceTax(String voucherNumber, String date) {

		String path = HOST + "salesinvoicetax/" + voucherNumber + "/" + SystemSetting.systemBranch + "?rdate=" + date;
		System.out.println(path);
		ResponseEntity<List<TaxSummaryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxSummaryMst>>() {
				});
		return response;
	}

	static public ResponseEntity<List<TaxSummaryMst>> getReturnInvoiceTax(String voucherNumber, String date) {

		String path = HOST + "salesreturninvoiceresource/salesreturninvoicetax/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);
		ResponseEntity<List<TaxSummaryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxSummaryMst>>() {
				});
		return response;
	}

	static public ResponseEntity<List<ItemMst>> getItemNames() {
		String path = HOST + "itemmsts";
		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});
		return response;
	}

	static public ResponseEntity<PriceDefenitionMst> savePriceDefenitionMst(PriceDefenitionMst priceDefenitionMst) {
		return restTemplate.postForEntity(HOST + "pricedefinitionmst", priceDefenitionMst, PriceDefenitionMst.class);
	}

	static public void deletePriceDefenitionMst(String id) {
		restTemplate.delete(HOST + "pricedefinitionmstdelete/" + id);
	}

	static public ResponseEntity<List<PriceDefenitionMst>> getPriceDefenitionByName(String name) {

		String path = HOST + "pricedefinitionmstbyname/" + name;
		System.out.println("====path====" + path);

		ResponseEntity<List<PriceDefenitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefenitionMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PriceDefenitionMst>> getPriceDefenitionByPricecalculator(String pricecalculator) {

		String path = HOST + "pricedefinitionmstbypricecalculator/" + pricecalculator;
		System.out.println("====path====" + path);

		ResponseEntity<List<PriceDefenitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefenitionMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PriceDefenitionMst>> getAllPriceDefenition() {

		String path = HOST + "findallpricedefinitionmst";
		System.out.println("====path====" + path);

		ResponseEntity<List<PriceDefenitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefenitionMst>>() {
				});

		return response;

	}

	static public ArrayList getPriceDefinition() {

		System.out.println("----- SearchUnit--rest caller---");

		return restTemplate.getForObject(HOST + "findallpricedefinitionmst", ArrayList.class);

	}

	static public ResponseEntity<PriceDefenitionMst> getPriceNameById(String id) {

		String path = HOST + "pricedefinitionmstbyid/" + id;
		ResponseEntity<PriceDefenitionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PriceDefenitionMst>() {
				});

		return response;

	}

	static public ResponseEntity<List<PriceDefinition>> getPriceByItemId(String itemId, String pricetype) {
		String path = HOST + "pricedefinition/" + itemId + "/" + pricetype + "/" + "pricedefinitionbyitem";
		System.out.println("==path====" + path);
		ResponseEntity<List<PriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefinition>>() {
				});

		return response;

	}

	static public ResponseEntity<List<BatchPriceDefinition>> getBatchPriceDefinitionByItemId(String itemId,
			String pricetype, String batch, String sdate) {
		String path = HOST + "batchpricedefinition/" + itemId + "/" + pricetype + "/" + batch
				+ "/batchpricedefinitionbyitem?tdate=" + sdate;
		System.out.println("==path====" + path);
		ResponseEntity<List<BatchPriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BatchPriceDefinition>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PriceDefinition>> SearchPriceDefinitionByitemid(String itemid) {
		String path = HOST + "pricedefinitionbyitemid/" + itemid;
		System.out.println("==path====" + path);
		ResponseEntity<List<PriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefinition>>() {
				});

		return response;
	}

	static public ResponseEntity<List<PriceDefinition>> SearchPriceDefinitionByPriceType(String pricetype) {
		String path = HOST + "pricedefinitionbypricetype/" + pricetype;
		System.out.println("==path====" + path);
		ResponseEntity<List<PriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefinition>>() {
				});

		return response;
	}

	static public void deletePriceDefenition(String id) {

		restTemplate.delete(HOST + "pricedefinition/" + id);

	}

	static public ResponseEntity<List<PriceDefinition>> getPriceDefenitionByDate(String startdate) {
		System.out.println("===LoadPriceDefenitionByDate====");
		String path = HOST + "pricedefinitionbystartdate/" + startdate;
		System.out.println("==path====" + path);
		ResponseEntity<List<PriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefinition>>() {
				});

		return response;
	}

	static public ResponseEntity<List<PriceDefinition>> getPriceDefenitionByItemId(String startdate) {
		System.out.println("===LoadPriceDefenitionByDate====");
		String path = HOST + "pricedefinitionbystartdate/" + startdate;
		System.out.println("==path====" + path);
		ResponseEntity<List<PriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefinition>>() {
				});

		return response;
	}

	static public ResponseEntity<PriceDefinition> getPriceDefenitionByItemIdAndUnit(String itemId, String priceId,
			String unitId, String sdate) {
		String path = HOST + "pricedefinition/" + itemId + "/" + priceId + "/" + unitId
				+ "/pricedefinitionbyitemandunit?tdate=" + sdate;
		System.out.println(path);
		ResponseEntity<PriceDefinition> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PriceDefinition>() {
				});

		return response;
	}

	static public ResponseEntity<PriceDefinition> getPriceDefenitionByCostPrice(String itemId, String priceId,
			String unitId, String sdate) {
		String path = HOST + "pricedefinition/" + itemId + "/" + priceId + "/" + unitId
				+ "/pricedefinitionbyitemcostprice" + "?sdate=" + sdate;
		ResponseEntity<PriceDefinition> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PriceDefinition>() {
				});

		return response;
	}

	static public ResponseEntity<PriceDefinition> getPriceDefenitionByItemIDate(String startdate) {
		System.out.println("===LoadPriceDefenitionByDate====");
		String path = HOST + "pricedefinition/" + startdate;
		System.out.println("==path====" + path);
		ResponseEntity<PriceDefinition> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PriceDefinition>() {
				});

		return response;
	}
//====================== Get branch code by branch name=====================================

	static public String getBranchCode(String branchName) {

		return restTemplate.getForObject(HOST + "branchcodebyname/" + branchName, String.class);

	}

//=========================== Save Multi unit=====================
	static public ResponseEntity<MultiUnitMst> saveMultiUnit(MultiUnitMst multiUnit) {

		return restTemplate.postForEntity(HOST + "multiunitmst", multiUnit, MultiUnitMst.class);

	}

// multi unit delete ==================================================
	static public void multiUnitDelete(String multiunitmstId) {

		restTemplate.delete(HOST + "multyunitmst/" + multiunitmstId);
		return;

	}

	static public ResponseEntity<List<MultiUnitMst>> getAllMultiUnitMst() {

		String path = HOST + "allmultyunitmst";

		ResponseEntity<List<MultiUnitMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MultiUnitMst>>() {
				});

		return response;

	}

// ==================== get unit mst==============================
	static public ResponseEntity<UnitMst> getUnitByName(Object object) {

		String path = HOST + "unitmst/" + object;
		System.out.println("==path====" + path);
		ResponseEntity<UnitMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<UnitMst>() {
				});

		return response;

	}

//===================== save unit mst========================
	static public ResponseEntity<UnitMst> saveUnitMst(UnitMst unitMst) {

		return restTemplate.postForEntity(HOST + "unitmst", unitMst, UnitMst.class);

	}

// ========================= Save company mst===========================
	static public ResponseEntity<CompanyMst> saveCompanyMst(CompanyMst companyMst) {

		return restTemplate.postForEntity(HOST2 + "companymst", companyMst, CompanyMst.class);

	}

//===========================
	static public ResponseEntity<CompanyMst> getCompanyMst(String mycompany) {
		String path = HOST2 + "companymst/" + mycompany + "/companymst";
		System.out.println(path);
		ResponseEntity<CompanyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CompanyMst>() {
				});

		return response;
	}

//================================
	static public ResponseEntity<List<CompanyMst>> returnValueOfCompanymst() {

		String path = HOST2 + "companymst";
		System.out.println(path);
		ResponseEntity<List<CompanyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CompanyMst>>() {
				});

		return response;

	}

//=========================== get account receivable voucher number==========
	static public ArrayList getAccountreceivable(String custid) {
		System.out.println(HOST + "accountreceivable/fetchaccountreceivable/" + custid);
		return restTemplate.getForObject(HOST + "accountreceivable/fetchaccountreceivable/" + custid, ArrayList.class);

	}

	static public ResponseEntity<AccountReceivable> getAccountRecByVoucherId(String voucherNo) {
		String path = HOST + "accountreceivable/fetchaccountreceivablebyvouchernumber/" + voucherNo;
		System.out.println("=============path=========" + path);

		ResponseEntity<AccountReceivable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountReceivable>() {
				});
		return response;
	}

	static public ResponseEntity<AccountReceivable> getAccountRecById(String id) {
		String path = HOST + "accountreceivable/fetchaccreceivablebyid/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<AccountReceivable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountReceivable>() {
				});
		return response;
	}

	static public ResponseEntity<AccountPayable> getAccountPayableById(String id) {
		String path = HOST + "accountreceivable/fetchaccountpayableablebyid/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<AccountPayable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountPayable>() {
				});
		return response;
	}

	static public ResponseEntity<AccountPayable> getAccountPayableByVoucherId(String voucherNo) {
		String path = HOST + "accountpayable/fetchaccountpayablebyvouchernumber/" + voucherNo;
		System.out.println("=============path=========" + path);

		ResponseEntity<AccountPayable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountPayable>() {
				});
		return response;
	}

//	static public ResponseEntity<CustomerMst> getCustomerByName(String custName) {
//		String path = HOST + "customermstbyname/" + custName;
//		System.out.println("=============path=========" + path);
//
//		ResponseEntity<CustomerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<CustomerMst>() {
//				});
//		return response;
//	}

	static public ResponseEntity<CategoryMst> getCategoryByName(String name) {

		String path = HOST + name + "/categorymstbyname";

		System.out.println("========path==========" + path);

		// return restTemplate.getForEntity(path, List<CategoryMst>.class);

		ResponseEntity<CategoryMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CategoryMst>() {
				});

		return response;

	}

	static public ResponseEntity<List<CategoryWiseSalesReport>> getCategoryOfItemDtl(String startDate, String endDate,
			String branchCode) {

		String path = HOST + startDate + "/categorymstbyname";

		System.out.println("========path==========" + path);

		// return restTemplate.getForEntity(path, List<CategoryMst>.class);

		ResponseEntity<List<CategoryWiseSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryWiseSalesReport>>() {
				});

		return response;

	}

	public static ResponseEntity<List<ServiceInDtl>> getServiceByHdrId(String hdrId) {
		String path = HOST + "serviceindtl/" + hdrId + "/serviceindtls";

		ResponseEntity<List<ServiceInDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ServiceInDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemMst>> getItemsByCategory(CategoryMst category) {
		String path = HOST + "itemmst/" + category.getId() + "/categorymst";

		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemMst>> getItemsByCategoryAndTax(CategoryMst category, String tax) {
		String path = HOST + "itemmsts/" + category + "/tax/" + tax;

		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<TaxMst>> getItemsByCategoryAndTaxAndTaxId(String categoryid, String taxId,
			Double taxRate, String strDate) {
		String path = HOST + "taxmst/" + taxRate + "/" + categoryid + "/" + taxId + "?rdate=" + strDate;

		ResponseEntity<List<TaxMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<TaxMst>> getTaxByRateTaxId(String categoryid, String taxId, Double taxRate) {
		String path = HOST + "getalltaxmstbyitemidandtaxidandtaxrate/" + categoryid + "/" + taxId + "/" + taxRate;

		ResponseEntity<List<TaxMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<TaxMst>> getTaxByRate(String itemid, Double taxRate) {
		String path = HOST + "getalltaxmstbyitemidandtaxrate/" + itemid + "/" + taxRate;

		ResponseEntity<List<TaxMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<TaxMst>> getTaxByItemId(String itemid) {
		String path = HOST + "taxmst/taxmstbyitemidandenddate/" + itemid;

		ResponseEntity<List<TaxMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxMst>>() {
				});

		return response;
	}

	static public ResponseEntity<TaxMst> saveTaxMst(TaxMst taxMst) {

		return restTemplate.postForEntity(HOST + "taxmst", taxMst, TaxMst.class);

	}

	static public ArrayList getAccountPayable(String supid) {

		String path = HOST + "accountpayable/" + supid;
		System.out.println(path);
		return restTemplate.getForObject(HOST + "accountpayable/" + supid, ArrayList.class);

	}

	static public ResponseEntity<List<AccountPayable>> getAccountPayableEntity(String supid) {

		String path = HOST + "accountpayable/" + supid;

		ResponseEntity<List<AccountPayable>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountPayable>>() {
				});

		return response;

	}

	///

	static public ResponseEntity<List<StockVerificationDtl>> getStockVerificationDtlByHdrId(String hdrId) {

		String path = HOST + "stockverificationdtl/stockverificationdtlbyhdrid/" + hdrId;

		ResponseEntity<List<StockVerificationDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockVerificationDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<AccountPayable> saveAccountPayable(AccountPayable accountPayable) {

		return restTemplate.postForEntity(HOST + "accountpayables", accountPayable, AccountPayable.class);

	}

	static public ArrayList getSchemeName() {

		System.out.println("----- SearchCategory--rest caller---");

		return restTemplate.getForObject(HOST + "categorymsts", ArrayList.class);

	}

	static public String initailizeData(String branchCode) {

		return restTemplate.getForObject(HOST + branchCode + "/init", String.class);

	}

//==========================  get scheme def=======================
	static public ResponseEntity<SchOfferDef> getSchemeOfferDef(String offerName) {

		String path = HOST + "schofferdefbyname/" + offerName;
		System.out.println(path);
		ResponseEntity<SchOfferDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchOfferDef>() {
				});

		return response;
	}

	static public ResponseEntity<SchemeInstance> getSchemeInstance(String shemename) {

		String path = HOST + "schemebyname/" + shemename;
		System.out.println("=============path=========" + path);

		ResponseEntity<SchemeInstance> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchemeInstance>() {
				});
		return response;
	}

	static public ResponseEntity<List<SchEligiAttrListDef>> getEligibilityAttributeNameByEligibilityId(
			String eligibilityId) {

		String path = HOST + "scheligiattrlistdefbyeligibilityid/" + eligibilityId;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<SchEligiAttrListDef>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchEligiAttrListDef>>() {
				});
		return response;
	}

	static public ResponseEntity<List<SchSelectAttrListDef>> getSelectionAttributeName(String selectionId) {

		String path = HOST + "stocktransferdtls/" + selectionId;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<SchSelectAttrListDef>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchSelectAttrListDef>>() {
				});
		return response;
	}

	static public ResponseEntity<List<SchOfferAttrListDef>> getOfferAttributeNameByOfferId(String offerId) {

		String path = HOST + "schofferattrlistdefbyofferid/" + offerId;
		System.out.println(path);

		ResponseEntity<List<SchOfferAttrListDef>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchOfferAttrListDef>>() {
				});
		return response;
	}

	static public ResponseEntity<List<SchSelectAttrListDef>> getSelectionAttributeBySelectionId(String selectionid) {

		String path = HOST + "schselectsttrlistdefbyselectionid/" + selectionid;
		System.out.println(path);

		ResponseEntity<List<SchSelectAttrListDef>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchSelectAttrListDef>>() {
				});
		return response;
	}

	static public ResponseEntity<SchEligiAttrListDef> getEligibilityAttributeNameByName(String name) {

		String path = HOST + "stocktransferdtls/" + name;
		System.out.println("=============path=========" + path);

		ResponseEntity<SchEligiAttrListDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchEligiAttrListDef>() {
				});
		return response;
	}

	static public ResponseEntity<SchOfferAttrListDef> getOfferAttributeNameByName(String name) {

		String path = HOST + "stocktransferdtls/" + name;
		System.out.println("=============path=========" + path);

		ResponseEntity<SchOfferAttrListDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchOfferAttrListDef>() {
				});
		return response;
	}

	static public ResponseEntity<SchEligibilityAttribInst> saveSchEligibilityAttribInst(
			SchEligibilityAttribInst schEligibilityAttribInst) {

		return restTemplate.postForEntity(HOST + "accountheads", schEligibilityAttribInst,
				SchEligibilityAttribInst.class);

	}

	static public ResponseEntity<SchEligiAttrListDef> getEligibilityAttributeById(String id) {

		String path = HOST + "stocktransferdtls/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<SchEligiAttrListDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchEligiAttrListDef>() {
				});
		return response;
	}

	static public ResponseEntity<SchSelectAttrListDef> getSelectionAttributeNameById(String id) {

		String path = HOST + "stocktransferdtls/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<SchSelectAttrListDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchSelectAttrListDef>() {
				});
		return response;
	}

	static public ResponseEntity<SchOfferAttrListDef> getOfferAttributeNameById(String id) {

		String path = HOST + "stocktransferdtls/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<SchOfferAttrListDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchOfferAttrListDef>() {
				});
		return response;
	}

	static public void deleteSchemeAttributes(String id) {

		restTemplate.delete(HOST + "purchasedtl/" + id);
		return;

	}

	static public ResponseEntity<List<SchEligibilityAttribInst>> getAllSchemeDtls(String id) {

		String path = HOST + "stocktransferdtls/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<SchEligibilityAttribInst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchEligibilityAttribInst>>() {
				});

		return response;

	}

	static public ResponseEntity<SchOfferDef> getSchemeOfferDefbyId(String id) {

		String path = HOST + "schofferdefbyid/" + id;
		System.out.println("==path====" + path);
		ResponseEntity<SchOfferDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchOfferDef>() {
				});

		return response;

	}

	static public ResponseEntity<SchSelectDef> getSchSelectDefbyName(String selection) {

		String path = HOST + "schselectdef/" + selection;
		System.out.println("==path====" + path);
		ResponseEntity<SchSelectDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchSelectDef>() {
				});

		return response;

	}

	static public ResponseEntity<SchEligibilityDef> getSchemeEligibilityDef(String eligibility) {

		String path = HOST + "scheligibilitydefbyname/" + eligibility;
		System.out.println("==path====" + path);
		ResponseEntity<SchEligibilityDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchEligibilityDef>() {
				});

		return response;

	}

	static public ResponseEntity<SchEligibilityDef> getSchemeEligibilityId(String id) {

		String path = HOST + "scheligibilitydefbyid/" + id;
		System.out.println("==path====" + path);
		ResponseEntity<SchEligibilityDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchEligibilityDef>() {
				});

		return response;

	}

	static public ResponseEntity<SchSelectDef> getSchSelectDefbyId(String id) {

		String path = HOST + "schselectdefbyid/" + id;
		System.out.println("==path====" + path);
		ResponseEntity<SchSelectDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchSelectDef>() {
				});

		return response;

	}

//========================== Save scheme instance=========================
	static public ResponseEntity<SchemeInstance> saveSchemeInstance(SchemeInstance schemeInstance) {

		return restTemplate.postForEntity(HOST + "schemeinstance", schemeInstance, SchemeInstance.class);

	}

	static public ResponseEntity<AccountReceivable> saveAccountReceivable(AccountReceivable accountReceivable) {

		return restTemplate.postForEntity(HOST + "accountreceivables", accountReceivable, AccountReceivable.class);

	}

	static public void deleteSchemeInstance(String id) {

		restTemplate.delete(HOST + "deleteschemeinstance/" + id);

	}

	public static ResponseEntity<List<SchemeInstance>> getAllSchemeInstance() {
		String path = HOST + "schemeinstances";

		ResponseEntity<List<SchemeInstance>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchemeInstance>>() {
				});

		return response;
	}

//================== save offer=====================
	static public ResponseEntity<SchOfferDef> saveSchOfferDef(SchOfferDef schOfferDef) {

		String path = HOST + "schofferdef";
		System.out.println(path);
		return restTemplate.postForEntity(path, schOfferDef, SchOfferDef.class);

	}

	static public ResponseEntity<SchEligibilityDef> saveSchEligibilityDef(SchEligibilityDef schEligibilityDef) {

		return restTemplate.postForEntity(HOST + "scheligibilitydef", schEligibilityDef, SchEligibilityDef.class);

	}

	static public ResponseEntity<SchSelectDef> saveSchSelectDef(SchSelectDef schSelectDef) {

		return restTemplate.postForEntity(HOST + "schselectdef", schSelectDef, SchSelectDef.class);

	}

	static public ArrayList getAllSelectDef() {

		return restTemplate.getForObject(HOST + "schselectdefall", ArrayList.class);

	}

	static public ArrayList getAllOffers() {

		return restTemplate.getForObject(HOST + "schofferdefdefbyall", ArrayList.class);

	}

	static public ArrayList getAllEligibility() {

		return restTemplate.getForObject(HOST + "scheligibilitydefbyall", ArrayList.class);
	}

	static public void updateBatchWiseActualProductionHdr(ActualProductionHdr actualProductionHdr) {

		System.out.println(HOST + "actualproductionbatchwise/" + actualProductionHdr.getId());
		restTemplate.put(HOST + "actualproductionbatchwise/" + actualProductionHdr.getId(), actualProductionHdr,
				ActualProductionHdr.class);

		return;
	}
//========================Update Production Dtl===============================

	public static ResponseEntity<List<SaleOrderInvoiceReport>> getSaleOrder(String voucherNumber, String date,
			String branch) {
		String path = HOST + "saleorderreport/" + voucherNumber + "/" + branch + "?rdate=" + date;
		System.out.println("getSaleOrder==" + path);

		ResponseEntity<List<SaleOrderInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderInvoiceReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SaleOrderTax>> getSaleOrderTax(String voucherNumber, String date, String branch) {
		String path = HOST + "saleordertaxreport/" + voucherNumber + "/" + branch + "?rdate=" + date;
		System.out.println("getSaleOrderTax==" + path);

		ResponseEntity<List<SaleOrderTax>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderTax>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderDueReport>> getSaleOrderDue(String voucherNumber, String date,
			String branch) {
		String path = HOST + "saleorderduereport/" + voucherNumber + "/" + branch + "?rdate=" + date;

		System.out.println("getSaleOrderDue==" + path);
		ResponseEntity<List<SaleOrderDueReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderDueReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderDetailsReport>> getSaleOrderPendingDtls(String date) {
		String path = HOST + "saleorderpendingreport/" + SystemSetting.systemBranch + "?rdate=" + date;
		System.out.println("getSaleOrderPendingDtls==" + path);

		ResponseEntity<List<SaleOrderDetailsReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderDetailsReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderDetailsReport>> getSaleOrderRealizedDtls(String date) {
		String path = HOST + "saleorderrealizedreport/" + SystemSetting.systemBranch + "?rdate=" + date;

		System.out.println("getSaleOrderRealizedDtls==" + path);
		ResponseEntity<List<SaleOrderDetailsReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderDetailsReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemSaleReport>> getItemSaleReport(String itemId, String date) {
		// TODO Auto-generated method stub
		return null;
	}

	public static ResponseEntity<List<ItemSaleReport>> getCategorySaleReport(String category, String date) {
		return null;
	}
//static public ResponseEntity<List<IntentInReport>> getIntentIn(String voucherNumber, String date) {

	static public ResponseEntity<List<IntentVoucherReport>> getIntentVoucher(String voucherNumber, String date) {

		String path = HOST + "intentvoucherreport/" + voucherNumber + "?rdate=" + date;

		ResponseEntity<List<IntentVoucherReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<IntentVoucherReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionDtlsReport>> getProductionDtlsDtl(String voucherNumber, String date,
			String branch) {

		String path = HOST + "productiondetailsreport/" + voucherNumber + "?rdate=" + date;

		ResponseEntity<List<ProductionDtlsReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtlsReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<IntentInReport>> getIntentInpdfPrint(String voucherNumber, String date) {
		String path = HOST + "intentinvoucherreport/" + voucherNumber + "?rdate=" + date;
		ResponseEntity<List<IntentInReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<IntentInReport>>() {
				});
		return response;
	}

	static public ResponseEntity<List<ProductionDtl>> getActualProductionDtls(String date) {

		String path = HOST + "productionplanningkitbydate/" + SystemSetting.systemBranch + "?rdate=" + date;

		ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtl>>() {
				});

		return response;

	}

//======================= get production raw material summary===================
	static public ResponseEntity<List<ProductionDtlDtl>> getProductionSummary(String date) {

		String path = HOST + "productionplanningrmbydate/" + SystemSetting.systemBranch + "?rdate=" + date;

		ResponseEntity<List<ProductionDtlDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtlDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionDtlsReport>> getProductionSummaryReport(String date) {

		String path = HOST + "productionplanningsumrmbydate/" + SystemSetting.systemBranch + "?rdate=" + date;

		ResponseEntity<List<ProductionDtlsReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtlsReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ProductionDtlsReport>> getProductionSummaryReportBetweenDate(String date,
			String tdate) {

		String path = HOST + "productionplanningsumrmbybetweendate?fdate=" + date + "&&tdate=" + tdate;

		ResponseEntity<List<ProductionDtlsReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtlsReport>>() {
				});

		return response;

	}

//  =============================== Actual production save====================
	static public ResponseEntity<ActualProductionHdr> saveActualProductionHdr(ActualProductionHdr actualProductionHdr) {

		return restTemplate.postForEntity(HOST + "productionhdr", actualProductionHdr, ActualProductionHdr.class);

	}

// ================= save kit detail ================================
	static public ResponseEntity<ActualProductionDtl> saveActualProductionDtl(ActualProductionDtl actualProductionDtl) {

		String path = HOST + "actualproductionhdr/" + actualProductionDtl.getActualProductionHdr().getId()
				+ "/actualproductiondtl";
		return restTemplate.postForEntity(path, actualProductionDtl, ActualProductionDtl.class);

	}

	static public ResponseEntity<List<ActualProductionDtl>> getActualProductionDtl(
			ActualProductionHdr actualProductionHdr) {

		String path = HOST + "actualproductionhdr/" + actualProductionHdr.getId() + "/actualproductiondtl";

		ResponseEntity<List<ActualProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ActualProductionDtl>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReqRawMaterial>> getReqRowMaterial(String actualProductionHdr) {

		String path = HOST + "actualproductionhdrresource/getrawmaterialstock/" + actualProductionHdr;
		System.out.println(path);
		ResponseEntity<List<ReqRawMaterial>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReqRawMaterial>>() {
				});

		return response;

	}

	// {dtlid}
	static public ResponseEntity<ActualProductionDtl> getActualProductionDtlById(String dtlId) {

		String path = HOST + "actualproductiondtlbyDtlId/" + dtlId;

		ResponseEntity<ActualProductionDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ActualProductionDtl>() {
				});

		return response;

	}

	static public void updateActualProductionHdr(ActualProductionHdr actualProductionHdr) {

		restTemplate.put(HOST + "actualproductionhdr/" + actualProductionHdr.getId(), actualProductionHdr,
				ActualProductionHdr.class);

		return;
	}
//========================Update Production Dtl===============================

	static public void updateProductionDtl(ProductionDtl productionDtl) {

		restTemplate.put(HOST + "productiondtlupdatebyid/" + productionDtl.getId(), productionDtl);
		return;

	}

	static public void updateAccountReceivable(AccountReceivable accountReceivable) {

		restTemplate.put(HOST + "updateaccountreceivablebyvouchernumber/" + accountReceivable.getVoucherNumber(),
				accountReceivable, AccountReceivable.class);

		return;
	}

	static public void updateAccountReceivablebyId(AccountReceivable accountReceivable) {

		restTemplate.put(HOST + "updateaccountreceivable/" + accountReceivable.getId(), accountReceivable,
				AccountReceivable.class);

		return;
	}

	static public void updateRawMaterialIssueHdr(RawMaterialIssueHdr rawMaterialIssueHdr) {

		restTemplate.put(HOST + "rawmaterialissuemst/" + rawMaterialIssueHdr.getId() + "/updaterawmaterialissuemst",
				rawMaterialIssueHdr, RawMaterialIssueHdr.class);

		return;
	}

	static public void updateRawMaterialReturnHdr(RawMaterialReturnHdr rawMaterialReturnHdr) {
		restTemplate.put(HOST + "rawmaterialreturnhdr/" + rawMaterialReturnHdr.getId() + "/updaterawmaterialreturnhdr",
				rawMaterialReturnHdr, RawMaterialReturnHdr.class);

		return;
	}

	static public void updateAccountPayablebyVoucher(AccountPayable accountPayable) {

		restTemplate.put(HOST + "updateaccountpayable/" + accountPayable.getVoucherNumber(), accountPayable,
				AccountPayable.class);

		return;
	}

	static public void updateSalesTransHdrWithLocalCustMst(SalesTransHdr salestransHdr) {

		restTemplate.put(HOST + "salestranshdrresource/updatelocalcustomer/" + salestransHdr.getId(), salestransHdr,
				SalesTransHdr.class);

		return;
	}

	public static ResponseEntity<List<ItemStockReport>> getItemStockReport(String itemId) {
		return null;
	}

	public static ResponseEntity<List<ItemStockReport>> getCategoryStockReport(String categoryId) {
		return null;
	}

//================================= Get bank Mst====================

	static public ArrayList getAllBankmsts() {

		System.out.println(HOST + "bankmst");

		return restTemplate.getForObject(HOST + "bankmst", ArrayList.class);

	}

	static public ArrayList getAllBankAccounts() {

		System.out.println("----- SearchAccountHeads--rest caller---");

		return restTemplate.getForObject(HOST + "accountheads/fetchaccountheadsbybankaccount", ArrayList.class);

	}

	public static ResponseEntity<LocalCustomerMst> saveLocalCustomer(LocalCustomerMst localCustomerMst) {
		return restTemplate.postForEntity(HOST + "localcustomer", localCustomerMst, LocalCustomerMst.class);
	}

	public static ArrayList SearchLocalCustomer(String searchData) {
		// TODO Auto-generated method stub
		return restTemplate.getForObject(HOST + "localcustomersearch?data=" + searchData, ArrayList.class);
	}

//=================== save order taker ==========================
	static public ResponseEntity<OrderTakerMst> saveOrderTakerMst(OrderTakerMst orderTakerMst) {

		return restTemplate.postForEntity(HOST + "ordertakermst", orderTakerMst, OrderTakerMst.class);

	}

	static public ResponseEntity<DeliveryBoyMst> saveDeliveryBoyMst(DeliveryBoyMst deliveryBoyMst) {

		return restTemplate.postForEntity(HOST + "delivaryboymst", deliveryBoyMst, DeliveryBoyMst.class);

	}

	static public void updateDelivery(DeliveryBoyMst deliveryBoyMst) {

		restTemplate.put(HOST + "updatedelivaryboy/" + deliveryBoyMst.getId() + "/deliveryboy", deliveryBoyMst);
		return;

	}

	static public void updatePurchaseOrderDtl(PurchaseOrderDtl purchaseOrderDtl) {

		restTemplate.put(HOST + "purchaseorderdtlupdate/" + purchaseOrderDtl.getId(), purchaseOrderDtl);
		return;

	}

	static public void updateOrdertaker(OrderTakerMst orderTakerMst) {

		restTemplate.put(HOST + "updateordertaker/" + orderTakerMst.getId() + "/ordertaker", orderTakerMst);
		return;

	}

	static public void updateOwnAccount(OwnAccount ownAccount) {

		restTemplate.put(HOST + "updateownaccountbyid/" + ownAccount.getId(), ownAccount);
		return;

	}

	static public ResponseEntity<List<NutritionMst>> getAllNutritions() {

		String path = HOST + "allnutritionmsts";

		ResponseEntity<List<NutritionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<NutritionMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<DeliveryBoyMst>> getDeliveryBoyMst() {

		String path = HOST + "getdeliveryboy/" + SystemSetting.systemBranch;

		ResponseEntity<List<DeliveryBoyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DeliveryBoyMst>>() {
				});

		return response;

	}

//
	static public ResponseEntity<List<SalesTypeMst>> getSalesTypeMst() {

		String path = HOST + "salestypemstrepository/salestypemsts/" + SystemSetting.systemBranch;

		ResponseEntity<List<SalesTypeMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTypeMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<NutritionMst>> getNutritionMst() {

		String path = HOST + "allnutritionmsts";

		ResponseEntity<List<NutritionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<NutritionMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<DeliveryBoyMst>> getDeliveryBoyMstByStatus() {

		String path = HOST + "getdeliveryboybystatus/" + SystemSetting.systemBranch;

		ResponseEntity<List<DeliveryBoyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DeliveryBoyMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<OrderTakerMst>> getOrderTakerMst() {

		String path = HOST + "getordertaker/" + SystemSetting.systemBranch;

		ResponseEntity<List<OrderTakerMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrderTakerMst>>() {
				});

		return response;

	}

// ======================== get order taker by status==============
	static public ResponseEntity<List<OrderTakerMst>> getOrderTakerMstByStatus() {

		String path = HOST + "getordertakerbystatus/" + SystemSetting.systemBranch;

		ResponseEntity<List<OrderTakerMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrderTakerMst>>() {
				});

		return response;

	}

// ======================= customer update============================
	static public void updateLocalCustomer(LocalCustomerMst localCustomerMst) {

		System.out.println(HOST + "updatelocalcustomer/" + localCustomerMst.getId() + "/localcustomer");
		restTemplate.put(HOST + "updatelocalcustomer/" + localCustomerMst.getId() + "/localcustomer", localCustomerMst);
		return;

	}
//=============================== get delivery boy id====================

	static public ResponseEntity<DeliveryBoyMst> getdeliveryBoyId(String id) {

		String path = HOST + "deliveryboymst/deliveryboymstbyid/" + id;
		System.out.println(path);
		ResponseEntity<DeliveryBoyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DeliveryBoyMst>() {
				});

		return response;

	}

	static public ResponseEntity<OrderTakerMst> getOrderTakerId(String ordername) {

		String path = HOST + "getordertakerbyname/" + ordername + "/" + SystemSetting.systemBranch;
		System.out.println(path);

		ResponseEntity<OrderTakerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OrderTakerMst>() {
				});

		return response;

	}

	static public ResponseEntity<LocalCustomerMst> getLocalCustomerbyName(String name) {

		String path = HOST + "getlocalcustomerbyname/" + name;
		System.out.println("======path" + path);
		ResponseEntity<LocalCustomerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<LocalCustomerMst>() {
				});
		return response;

	}

//	public static ResponseEntity<CustomerMst> getGstCustomerByName(String customername) {
//		String path = HOST + "customermstbyname/" + customername;
//		System.out.println("======path" + path);
//		ResponseEntity<CustomerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<CustomerMst>() {
//				});
//
//		return response;
//	}

//=============== Get payment sum============
	static public double getPaymentTotal(String receipthdrid) {

		return restTemplate.getForObject(HOST + "paymenttotal/" + receipthdrid + "/paymenttotalamount", Double.class);

	}

	static public String UpdateAllIntent() {

		return restTemplate.getForObject(HOST + "intentindtl/updateallintent", String.class);

	}

	static public Integer getCountOfSalesDtl(String hdrid) {

		return restTemplate.getForObject(HOST + "salesdtl/" + hdrid + "/countofsalesdtl/", Integer.class);

	}

	static public Integer getBatchItemStock(String itemId) {

		return restTemplate.getForObject(HOST + "productionbatchstockdtlresource/productionrowmaterialcount/" + itemId,
				Integer.class);

	}

	static public String getallMastrs(String msg) {
		
		System.out.println(HOST + "getallmasters/" + msg);
		return restTemplate.getForObject(HOST + "getallmasters/" + msg, String.class);

	}

	static public String resendStockTransfer(String id) {
		System.out.println(HOST + "stocktransferouthdr/resendfailedstocktransfer/" + id);
		return restTemplate.getForObject(HOST + "stocktransferouthdr/resendfailedstocktransfer/" + id, String.class);

	}

	static public String getServerVersion() {
		return restTemplate.getForObject(HOST + "companymst/getversion/", String.class);

	}

	static public String itemBatchMstUpdateByDate(String date) {
		return restTemplate.getForObject(HOST + "/itembatchmst/updatestockfromdtl/?fdate=" + date, String.class);

	}

	static public Integer getSavedBatchItemStock(String actProdId) {

		return restTemplate.getForObject(
				HOST + "productionbatchstockdtlresource/productionsavedrowmaterialcount/" + actProdId, Integer.class);

	}

//======================== get sales trans hdr sum===============
	static public double getSalesTransHdrInvoiceTotal(String date) {

		return restTemplate.getForObject(HOST + "paymenttotal/" + date + "/paymenttotalamount", Double.class);

	}

	public static ResponseEntity<LocalCustomerMst> getLocalCustomerbyId(String id) {
		String path = HOST + "getlocalcustomerbyid/" + id;
		System.out.println("=============path=========" + path);

		ResponseEntity<LocalCustomerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<LocalCustomerMst>() {
				});
		return response;
	}

	// ======== get Local Customer ByPhone
	public static ResponseEntity<LocalCustomerMst> getLocalCustomerbyPhone(String phno) {
		String path = HOST + "getlocalcustomerbyphoneno/" + phno;
		System.out.println("=============path=========" + path);

		ResponseEntity<LocalCustomerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<LocalCustomerMst>() {
				});
		return response;
	}

// ================================ Save Item Branch Hdr=========================
	static public ResponseEntity<ItemBranchHdr> saveItemBranchHdr(ItemBranchHdr itemBranchHdr) {

		return restTemplate.postForEntity(HOST + "itembranchmst/itembranchmst", itemBranchHdr, ItemBranchHdr.class);

	}

	static public ResponseEntity<ItemBranchDtl> saveItemBranchDtl(ItemBranchDtl itemBranchDtl) {

		String path = HOST + "itembranchdtl/itembranchdtl/" + itemBranchDtl.getItemBranchHdr().getId();
		return restTemplate.postForEntity(path, itemBranchDtl, ItemBranchDtl.class);

	}

	static public void deleteItemBranchDeletebyId(String id) {

		restTemplate.delete(HOST + "itembranchdtl/" + id);
		return;

	}

	static public void deleteItemBatchDtlByParentId(String id) {

		System.out.println(HOST + "itembatchmst/deleteitembatchbyhdrid/" + id);
		restTemplate.delete(HOST + "itembatchmst/deleteitembatchbyhdrid/" + id);
		return;

	}

//====================== Get item Branch Dtls============================
	public static ResponseEntity<List<ItemBranchDtl>> getItemBranchDtlByHdrId(String id) {
		String path = HOST + "itembranchhdr/" + id + "/itembranchdtls";
		System.out.println("=============path=========" + path);

		ResponseEntity<List<ItemBranchDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemBranchDtl>>() {
				});
		return response;
	}

	static public void updateItemBranchHdr(ItemBranchHdr itemBranchHdr) {

		System.out.println(HOST + "itembranchmst/itembranchmstupdate/" + itemBranchHdr.getId());
		restTemplate.put(HOST + "itembranchmst/itembranchmstupdate/" + itemBranchHdr.getId(), itemBranchHdr,
				ItemBranchHdr.class);

		return;
	}

	public static ResponseEntity<List<PaymentDtl>> getPaymentPettyCash(String accountId, String strDate) {
		String path = HOST + "paymentdtlreport/" + accountId + "?rdate=" + strDate;

		System.out.println("=============path=========" + path);

		ResponseEntity<List<PaymentDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentDtl>>() {
				});
		return response;
	}

////{branchcode}
	public static Double getPaymentSumByDate(String strDate, String acId) {
		return restTemplate.getForObject(HOST + "paymentreport/paymentfordayend/" + SystemSetting.systemBranch + "/"
				+ acId + "?rdate=" + strDate, Double.class);

	}

	public static Double getReceiptSumByDate(String strDate, String acId) {
		return restTemplate.getForObject(
				HOST + "receipthdr/receiptdtlsum/" + SystemSetting.systemBranch + "/" + acId + "?rdate=" + strDate,
				Double.class);

	}

	public static ResponseEntity<List<ReceiptDtl>> getReceiptPettyCash(String accountId, String strDate) {
		String path = HOST + "receiptdtl/receiptdtlreport/" + accountId + "?rdate=" + strDate;

		System.out.println("=============path=========" + path);

		ResponseEntity<List<ReceiptDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptDtl>>() {
				});
		return response;
	}

//static public ResponseEntity<List<ItemWiseDtlReport>> getDateWiseSales(String branch, String fromDate, String toDate) {
//
//	String path = HOST + "dailyisalereportresource/itemwise/"+branch+"?rdate=" + fromDate ;
//	ResponseEntity<List<ItemWiseDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//			new ParameterizedTypeReference<List<ItemWiseDtlReport>>() {
//			});
//	return response;
//
//}
	static public ResponseEntity<List<ItemWiseDtlReport>> getMonthSales(String branch, String fromDate, String toDate) {

		String path = HOST + "dailyisalereportresourcebetweendate/itemwise/" + branch + "?rdate=" + fromDate
				+ "&&tdate=" + toDate;
		System.out.println(path);

		ResponseEntity<List<ItemWiseDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemWiseDtlReport>>() {
				});
		return response;

	}

	static public ResponseEntity<List<ItemWiseDtlReport>> getCategorybytwoDates(String branch, String fromDate,
			String toDate, String category) {

		String path = HOST + "categoryWiseSalesReportbetweendate/itemwise/" + branch + "/" + category + "?rdate="
				+ fromDate + "&&tdate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<ItemWiseDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemWiseDtlReport>>() {
				});
		return response;

	}

	static public ResponseEntity<List<CategoryWiseSalesReport>> getCategorySalesDtl(String branch, String fromDate,
			String toDate) {

		String path = HOST + "categorywisesalesreportresource/" + branch + "/getcategorywisesalesreport" + "?fromdate="
				+ fromDate + "&&todate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<CategoryWiseSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryWiseSalesReport>>() {
				});
		return response;

	}

	static public ResponseEntity<List<ItemWiseDtlReport>> getItembytwoDates(String branch, String fromDate,
			String toDate, String itemid) {

		String path = HOST + "itemwisesalesreportbetweendate/itemwise/" + branch + "/" + itemid + "?rdate=" + fromDate
				+ "&&tdate=" + toDate;
		System.out.println(path);

		ResponseEntity<List<ItemWiseDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemWiseDtlReport>>() {
				});
		return response;

	}

	public static ResponseEntity<List<SupplierMonthlySummary>> getSupplierMonthlySummary(String startDate,
			String endDate, String supplierId) {
		String path = HOST + "getmonthlysupplierledgerrepot/" + supplierId + "/" + SystemSetting.systemBranch
				+ "?fromdate=" + startDate + "&&todate=" + endDate;
		System.out.println("path---------" + path);

		ResponseEntity<List<SupplierMonthlySummary>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SupplierMonthlySummary>>() {
				});
		return response;
	}

//	public static ResponseEntity<Supplier> getsupplierByName(String supplierName) {
//
//		String path = HOST + "supplierbyname/" + supplierName;
//		System.out.println("path---------" + path);
//
//		ResponseEntity<Supplier> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<Supplier>() {
//				});
//		return response;
//	}

//	public static ResponseEntity<Supplier> getsupplierByAccountHeadId(String ahid) {
//
//		String path = HOST + "supplierbyacountid/" + ahid;
//		System.out.println("path---------" + path);
//
//		ResponseEntity<Supplier> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<Supplier>() {
//				});
//		return response;
//	}

	public static ResponseEntity<List<SaleOrderReport>> getSaleOrderReport(String startDate) {
		String path = HOST + "saleorderreportbybranchcodeanddate/" + SystemSetting.systemBranch + "?rdate=" + startDate;
		System.out.println("path---------" + path);

		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SalesOrderAdvanceBalanceReport>> getSaleOrderAdvanceBalance(String startDate) {
		String path = HOST + "saleorderbalanceadvancereport/" + SystemSetting.systemBranch + "?rdate=" + startDate;
		System.out.println("path---------" + path);

		ResponseEntity<List<SalesOrderAdvanceBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<SalesOrderAdvanceBalanceReport>>() {
				});
		return response;

	}

	public static ResponseEntity<List<SalesModeWiseSummaryreport>> getSaleModeWise(String date) {
		String path = HOST + "dailysalessummary/" + SystemSetting.systemBranch + "/dailypaymentmodewisereport"
				+ "?rdate=" + date;
		System.out.println("path---------" + path);

		ResponseEntity<List<SalesModeWiseSummaryreport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesModeWiseSummaryreport>>() {
				});
		return response;

	}

	public static ResponseEntity<List<B2bSalesReport>> getB2bSaleSummary(String startDate, String stodate) {
		String path = HOST + "salestranshdrreport/b2bsalesreport/" + SystemSetting.systemBranch + "?rdate=" + startDate
				+ "&&tdate=" + stodate;
		System.out.println("path---------" + path);

		ResponseEntity<List<B2bSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<B2bSalesReport>>() {
				});
		return response;

	}

	public static ResponseEntity<List<B2bSalesReport>> getB2bSaleDtlSummary(String startDate, String stodate) {
		String path = HOST + "salestranshdrreport/b2bsalesreportdtl/" + SystemSetting.systemBranch + "?rdate="
				+ startDate + "&&tdate=" + stodate;
		System.out.println("path---------" + path);

		ResponseEntity<List<B2bSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<B2bSalesReport>>() {
				});
		return response;

	}

	public static ResponseEntity<List<SaleOrderReport>> getHomedeliveryReport(String startDate) {

		String path = HOST + "homedelivaryreport/" + SystemSetting.systemBranch + "?rdate=" + startDate;
		System.out.println("path---------" + path);

		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SaleOrderReport>> getDeliveryBoyReport(String startDate, String deliveryboyid) {
		String path = HOST + "delivaryreportbydelivaryboyid/" + deliveryboyid + "/" + SystemSetting.systemBranch
				+ "?rdate=" + startDate;
		System.out.println("path---------" + path);

		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<RawMaterialIssueDtl>> getRawMaterialIssueDtl(String hdrid) {
		String path = HOST + "rawmaterialissuedtl/" + hdrid + "/rawmaterialdtls";
		ResponseEntity<List<RawMaterialIssueDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RawMaterialIssueDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<RawMaterialReturnDtl>> getRawMaterialReturnDtl(String hdrid) {
		String path = HOST + "rawmaterialretunrdtl/" + hdrid + "/rawmaterialreturndtls";
		ResponseEntity<List<RawMaterialReturnDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RawMaterialReturnDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<AccountBalanceReport>> getAccountBalanceReport(String startDate, String endDate,
			String accountid) {
		String path = HOST + "accountingresource/accountstatement/" + accountid + "/" + SystemSetting.systemBranch
				+ "?startdate=" + startDate + "&&enddate=" + endDate;

		System.out.println("accountstatement" + path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<AccountBalanceReport>> getAccountBalanceReportMSingleMode(String startDate,
			String endDate, String accountid) {
		String path = HOST + "accountingresource/accountstatementsingle/" + accountid + "/" + SystemSetting.systemBranch
				+ "?startdate=" + startDate + "&&enddate=" + endDate;

		System.out.println("accountstatement" + path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});
		return response;
	}

	public static ResponseEntity<ConsumptionReasonMst> getConsumptionReasonMstById(String id) {
		String path = HOST + "consumptionreasonmstresource/findconsumptionreasonmst/" + id;

		System.out.println("accountstatement" + path);
		ResponseEntity<ConsumptionReasonMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ConsumptionReasonMst>() {
				});
		return response;
	}

	public static ResponseEntity<ConsumptionReasonMst> getConsumptionReasonMstByReason(String reason) {
		String path = HOST + "consumptionreasonmstresource/findallconsumptionreasonmstbyreason/" + reason;

		System.out.println("accountstatement" + path);
		ResponseEntity<ConsumptionReasonMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ConsumptionReasonMst>() {
				});
		return response;
	}

	public static ResponseEntity<List<ConsumptionDtl>> getConsumptionDtl(String hdrid) {
		String path = HOST + "consumptiondtl/getconsumptiondtlbyhdr/" + hdrid;

		System.out.println("accountstatement" + path);
		ResponseEntity<List<ConsumptionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ShortExpiryReport>> getCategoryWiseExpiry(String catList) {
		String path = HOST + "shortexpiryreportresource/catogarywiseshortexpiryreport/" + SystemSetting.systemBranch
				+ "/?categoryname=" + catList;

		System.out.println("getCategoryWiseExpiry" + path);
		ResponseEntity<List<ShortExpiryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ShortExpiryReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ShortExpiryReport>> getItemWiseExpiry(String itemList) {
		String path = HOST + "shortexpiryreportresource/itemwiseshortexpiryreport/" + SystemSetting.systemBranch
				+ "?itemlist=" + itemList;

		System.out.println("getCategoryWiseExpiry" + path);
		ResponseEntity<List<ShortExpiryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ShortExpiryReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ShortExpiryReport>> getShortExpiryReport(String currentDate) {
		String path = HOST + "shortexpiryreportresource/shortexpiryreport/" + SystemSetting.systemBranch + "/" + currentDate;

		System.out.println("shortexpiryreportresource" + path);
		ResponseEntity<List<ShortExpiryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ShortExpiryReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ProductionBatchStockDtl>> getProductionBatchStockDtlByActualProduction(
			String id) {
		String path = HOST + "productionbatchstockdtlresource/productionbatchstockdtlbyactualproduction/" + id;

		System.out.println("productionbatchstockdtlresource" + path);
		ResponseEntity<List<ProductionBatchStockDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionBatchStockDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ProductionBatchStockDtl>> getProductionBatchStockDtl(String itemId) {
		String path = HOST + "productionbatchstockdtlresource/productionbatchstockdtl/" + itemId;

		System.out.println("productionbatchstockdtlresource" + path);
		ResponseEntity<List<ProductionBatchStockDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionBatchStockDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<BatchPriceDefinition> getBatchPriceDefinition(String itemid, String priceId,
			String unitId, String batch, String sdate) {
		String path = HOST + "batchpricedefinition/" + itemid + "/" + priceId + "/" + unitId + "/" + batch
				+ "/batchpricedefinitionbyitemandunitandbatch?tdate=" + sdate;

		System.out.println("accountstatement" + path);
		ResponseEntity<BatchPriceDefinition> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BatchPriceDefinition>() {
				});
		return response;
	}

//{barcode}/
	public static ResponseEntity<WeighingItemMst> getWeighingItemMstByBarcode(String barcode) {
		String path = HOST + "weighingitemmst/" + barcode + "/weighingitembybarcode";

		System.out.println("accountstatement" + path);
		ResponseEntity<WeighingItemMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighingItemMst>() {
				});
		return response;
	}

	public static ResponseEntity<WeighingItemMst> getWeighingItemMstByItemId(String itemId) {
		String path = HOST + "weighingitemmst/" + itemId + "/weighingitembyitemid";

		System.out.println("weighingitemmst" + path);
		ResponseEntity<WeighingItemMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighingItemMst>() {
				});
		return response;
	}

	public static ResponseEntity<List<NutritionValueDtl>> getNutritionValueDtlByMstId(
			NutritionValueMst nutritionValueMst) {
		String path = HOST + "nutrinvaluedtl/" + nutritionValueMst.getId() + "/getnutritionvaluedtl";

		System.out.println("NutritionValue" + path);
		ResponseEntity<List<NutritionValueDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<NutritionValueDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<NutritionValueDtl> getNutritionValueMstByItemMstAndNutrition(String HdrId,
			String nutrition) {
		String path = HOST + "nutrinvaluedtl/" + HdrId + "/" + nutrition + "/getnutritionvaluedtl";

		System.out.println("NutritionValue" + path);
		ResponseEntity<NutritionValueDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<NutritionValueDtl>() {
				});
		return response;
	}

	public static ResponseEntity<NutritionValueMst> getNutritionValueMstByItemMst(ItemMst itemmst) {
		String path = HOST + "nutritionnvaluemst/" + itemmst.getId() + "/getnutritionvaluemst";

		System.out.println("NutritionValue" + path);
		ResponseEntity<NutritionValueMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<NutritionValueMst>() {
				});
		return response;
	}

	public static ResponseEntity<List<OrgMonthlyAccountReport>> getOrgMonthlyAccountBalanceReport(String startDate,
			String endDate, String accountid) {
		String path = HOST + "orgmonthlyaccountreport/" + accountid + "?startDate=" + startDate + "&&endDate="
				+ endDate;

		System.out.println("accountstatement" + path);
		ResponseEntity<List<OrgMonthlyAccountReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgMonthlyAccountReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<AccountBalanceReport>> getSupplierLedgerReport(String startDate, String endDate,
			String accountid) {
		String path = HOST + "accountingresource/supplieraccountstatement/" + accountid + "/"
				+ SystemSetting.systemBranch + "?startdate=" + startDate + "&&enddate=" + endDate;

		System.out.println("accountstatement" + path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<MultiUnitMst>> getMultiUnitByItemId(String itemid) {
		String path = HOST + "multiunitmst/" + itemid + "/multiunititemdtls";
		ResponseEntity<List<MultiUnitMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MultiUnitMst>>() {
				});
		return response;
	}

	public static ResponseEntity<MultiUnitMst> getMultiUnitByItemIdUnitId(String itemid, String unitId) {
		String path = HOST + "multiunitmst/" + itemid + "/" + unitId + "/multiunititemdtls";
		ResponseEntity<MultiUnitMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MultiUnitMst>() {
				});
		return response;
	}

	public static ResponseEntity<MultiUnitMst> getMultiUnitbyprimaryunit(String itemid, String unitId) {
		String path = HOST + "multiunitmst/" + itemid + "/" + unitId + "/getbyprimaryunit";
		System.out.println(path);
		ResponseEntity<MultiUnitMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MultiUnitMst>() {
				});
		return response;
	}

	public static ResponseEntity<List<OwnAccount>> getOwnAccountBySupAccId(String accountid) {
		String path = HOST + "ownaccount/" + accountid;

		ResponseEntity<List<OwnAccount>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OwnAccount>>() {
				});
		return response;
	}

	public static ResponseEntity<List<OwnAccount>> getOwnAccountDtlsByReceiptHdrId(String id) {
		String path = HOST + "ownaccount/" + id + "/ownaccounthdrid";

		System.out.println(path);

		ResponseEntity<List<OwnAccount>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OwnAccount>>() {
				});
		return response;
	}

	public static ResponseEntity<List<OwnAccount>> getOwnAccountDtlsByPaymentHdrId(String id) {
		String path = HOST + "ownaccount/" + id + "/ownaccountpaymenthdrid";

		System.out.println(path);

		ResponseEntity<List<OwnAccount>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OwnAccount>>() {
				});
		return response;
	}

	public static ResponseEntity<List<OwnAccount>> getOwnAccountDtlsByCustId(String accountid) {
		String path = HOST + "ownaccount/" + accountid + "/customer";

		ResponseEntity<List<OwnAccount>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OwnAccount>>() {
				});
		return response;
	}

	public static ResponseEntity<OwnAccountSettlementDtl> getOwnAccountSettlementDtlbyInvoiceNo(String invNo) {
		String path = HOST + "getownaccountsettlementdtls/" + invNo;

		ResponseEntity<OwnAccountSettlementDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OwnAccountSettlementDtl>() {
				});
		return response;
	}

	public static ResponseEntity<OwnAccount> getOwnAccountById(String id) {
		String path = HOST + "ownaccount/fetchownaccountbyid/" + id;

		ResponseEntity<OwnAccount> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OwnAccount>() {
				});
		return response;
	}

	public static ResponseEntity<List<supplierLedger>> getSupplierLedger(String startDate, String supplierId) {
		String path = HOST + "getsupplierledgerrepot/" + supplierId + "/" + SystemSetting.systemBranch + "?rdate="
				+ startDate;

		System.out.println(path);
		ResponseEntity<List<supplierLedger>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<supplierLedger>>() {
				});
		return response;
	}

	public static ResponseEntity<List<AccountBalanceReport>> getCustomerLedger(String startDate, String endDate,
			String customerId) {
		String path = HOST + "accountingresource/accountstatement/" + customerId + "/" + SystemSetting.systemBranch
				+ "?startdate=" + startDate + "&&enddate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SupplierMonthlySummary>> getSupplierBillWise(String startDate, String endDate,
			String supplierId) {
		String path = HOST + "accountingresource/accountstatement/" + supplierId + "/" + SystemSetting.systemBranch
				+ "?startdate=" + startDate + "&&enddate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<SupplierMonthlySummary>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SupplierMonthlySummary>>() {
				});
		return response;
	}

	public static ResponseEntity<DeliveryBoyMst> getDelivaryBoyByName(String delivaryboyname) {
		String path = HOST + "getdeliveryboybyname/" + delivaryboyname + "/" + SystemSetting.systemBranch;
		System.out.println("path---------" + path);

		ResponseEntity<DeliveryBoyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DeliveryBoyMst>() {
				});
		return response;
	}

	public static ResponseEntity<ReceiptModeMst> saveReceiptMode(ReceiptModeMst receiptMode) {

		String path = HOST + "receiptmode/" + SystemSetting.getSystemBranch();

		return restTemplate.postForEntity(path, receiptMode, ReceiptModeMst.class);
	}

//Following to be removed and use getAllReceiptModes
	public static ResponseEntity<List<ReceiptModeMst>> getAllReceiptMode() {

		String path = HOST + "findallreceipt";
		System.out.println("path---------" + path);

		ResponseEntity<List<ReceiptModeMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ReceiptModeMst>> getAllReceiptModes() {
		String path = HOST + "findallreceiptmodes";
		System.out.println("path---------" + path);

		ResponseEntity<List<ReceiptModeMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeMst>>() {
				});
		return response;
	}

/////organisation show alllll
	public static ResponseEntity<List<OrgMst>> getAllOrganisation() {
		String path = HOST + "finalorganisation";
		System.out.println("path---------" + path);

		ResponseEntity<List<OrgMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SiteMst>> getAllSites() {
		String path = HOST + "showsites";
		System.out.println("path---------" + path);

		ResponseEntity<List<SiteMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SiteMst>>() {
				});
		return response;
	}

//=================================================
	public static ResponseEntity<List<MemberMst>> getAllMemberMst() {
		String path = HOST + "finalmembermst";
		System.out.println("path---------" + path);

		ResponseEntity<List<MemberMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MemberMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<MemberMst>> getMemberMst() {
		String path = HOST + "getmembermst";
		System.out.println("path---------" + path);

		ResponseEntity<List<MemberMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MemberMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<FamilyMst>> getAllFamily() {
		String path = HOST + "finalfamily";
		System.out.println("path---------" + path);

		ResponseEntity<List<FamilyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FamilyMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<EventMst>> getAllEvent() {
		String path = HOST + "finalevent";
		System.out.println("path---------" + path);

		ResponseEntity<List<EventMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<EventMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SubGroupMember>> getAllSubGroupMember() {
		String path = HOST + "finalsubgroupmember";
		System.out.println("path---------" + path);

		ResponseEntity<List<SubGroupMember>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SubGroupMember>>() {
				});
		return response;
	}

	public static void deleteReceiptMode(String id) {
		// TODO Auto-generated method stub

		restTemplate.delete(HOST + "receiptmodedelete/" + id);
	}

	public static void deleteOrgMst(String id) {
		// TODO Auto-generated method stub

		restTemplate.delete(HOST + "orgmstdelete/" + id);
	}

	public static void deleteSiteMst(String id) {
		// TODO Auto-generated method stub

		restTemplate.delete(HOST + "sitemstdelete/" + id);
	}

	public static void deleteSubGroupMember(String id) {
		// TODO Auto-generated method stub

		restTemplate.delete(HOST + "subgroupmemberdelete/" + id);
	}

	public static void deleteMemberMst(String id) {
		// TODO Auto-generated method stub

		restTemplate.delete(HOST + "membermstdelete/" + id);
	}

	public static void deleteFamilyMst(String id) {
		// TODO Auto-generated method stub

		restTemplate.delete(HOST + "familydelete/" + id);
	}

	public static void deleteEventMst(String id) {
		// TODO Auto-generated method stub

		restTemplate.delete(HOST + "eventdelete/" + id);
	}

	public static ResponseEntity<ReceiptModeMst> getReceiptModeByName(String receiptMode) {

		String path = HOST + "findreceiptmode/" + receiptMode;
		System.out.println(path);

		ResponseEntity<ReceiptModeMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ReceiptModeMst>() {
				});
		return response;
	}

	public static void updateReceiptmode(ReceiptModeMst receiptModeMst) {
		String path = HOST + "updatereceiptmode/" + receiptModeMst.getId();

		System.out.print(path);
		restTemplate.put(HOST + "updatereceiptmode/" + receiptModeMst.getId(), receiptModeMst);
		return;
	}

	public static ResponseEntity<ReceiptModeMst> getReceiptModeById(String receiptMode) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void deleteSalesReceipts(String id) {

		restTemplate.delete(HOST + "salesreceiptdelete/" + id);

	}

	public static void deleteCardSaleDtl(String id) {

		String path = HOST + "cardsaledtlresource/deletecardsale/" + id;

		System.out.print(path);
		restTemplate.delete(HOST + "cardsaledtlresource/deletecardsale/" + id);

	}

	public static ResponseEntity<List<SalesReceipts>> getAllSalesReceiptsBySalesTransHdr(String salestrandhdrid) {

		String path = HOST + "retrivesalesreceipt/" + salestrandhdrid;
		System.out.println("path---------" + path);

		ResponseEntity<List<SalesReceipts>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesReceipts>>() {
				});
		return response;
	}

	public static double getSumofSalesReceiptbySalestransHdrId(String id) {

		return restTemplate.getForObject(HOST + "salesreceiptamount/" + id, Double.class);
	}

	public static ResponseEntity<List<StockSummaryReport>> getStockSummary(String branchCode, String sDate,
			String tDate) {
		String path = HOST + "allcategorystocksummaryreport/" + branchCode + "?fromdate=" + sDate + "&&todate=" + tDate;
		System.out.println(path);
		ResponseEntity<List<StockSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockSummaryReport>>() {
				});
		return response;
	}

	static public ResponseEntity<List<StockSummaryDtlReport>> getStockSummaryDtlReport(String fromDate, String toDate,
			String itemid, String branchCode) {

		String path = HOST + "stocksummarydtls/" + itemid + "/" + branchCode + "?fromdate=" + fromDate + "&&todate="
				+ toDate;
		System.out.println(path);
		ResponseEntity<List<StockSummaryDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockSummaryDtlReport>>() {
				});
		return response;

	}

	static public ResponseEntity<List<InventoryReorderStatusReport>> getInventoryReorderStatusReport(String fromDate,
			String toDate, String branchCode) {

		String path = HOST + "inventoryreorderstatus/" + branchCode + "?rdate=" + fromDate + "&&tdate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<InventoryReorderStatusReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InventoryReorderStatusReport>>() {
				});
		return response;

	}

	public static ResponseEntity<List<AccountBalanceReport>> getCustomerBillWiseReport(String startDate, String endDate,
			String customerId) {
		String path = HOST + "accountingresource/accountstatement/" + customerId + "/" + SystemSetting.systemBranch
				+ "?startdate=" + startDate + "&&enddate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});
		return response;
	}

	static public ResponseEntity<StockTransferInHdr> saveAcceptStock(StockTransferInHdr stockTransferInHdr) {

		return restTemplate.postForEntity(HOST + "stocktransferinhdr/acceptstock", stockTransferInHdr,
				StockTransferInHdr.class);
	}

	static public ResponseEntity<StockTransferInReturnHdr> saveStockTransferInReturnHdr(
			StockTransferInReturnHdr StockTransferInReturnHdr) {

		return restTemplate.postForEntity(HOST + "stocktransferinreturnhdrresource/savestocktransferinreturnhdr",
				StockTransferInReturnHdr, StockTransferInReturnHdr.class);
	}

	static public ResponseEntity<StockTransferInReturnDtl> saveStockTransferInReturnDtl(
			StockTransferInReturnDtl stockTransferInReturnDtl) {

		return restTemplate.postForEntity(HOST + "stocktransferinreturndtlresource/savestocktransferinreturndtl",
				stockTransferInReturnDtl, StockTransferInReturnDtl.class);
	}

	static public ResponseEntity<ItemLocationMst> saveItemLocationMst(ItemLocationMst itemLocationMst) {

		return restTemplate.postForEntity(HOST + "itemlocationmstresource/saveitemlocationmst", itemLocationMst,
				ItemLocationMst.class);
	}

	static public ResponseEntity<UrsMst> saveUrsMst(UrsMst ursMst) {

		return restTemplate.postForEntity(HOST + "ursmstresource/ursmstresourcesave", ursMst, UrsMst.class);
	}

	static public ResponseEntity<ConsumptionReasonMst> saveConsumptionReasonMst(
			ConsumptionReasonMst consumptionReasonMst) {

		return restTemplate.postForEntity(HOST + "consumptionreasonmstresource/addconsumptionreasonmst",
				consumptionReasonMst, ConsumptionReasonMst.class);
	}

	static public ResponseEntity<ConsumptionHdr> saveConsumptionHdr(ConsumptionHdr consumptionHdr) {

		return restTemplate.postForEntity(HOST + "consumptionhdr/saveconsumptionhdr", consumptionHdr,
				ConsumptionHdr.class);
	}

	static public ResponseEntity<ConsumptionDtl> saveConsumptionDtl(ConsumptionDtl consumptionDtl) {
		return restTemplate.postForEntity(
				HOST + "consumptiondtl/" + consumptionDtl.getConsumptionHdr().getId() + "/consumptiondtlsave",
				consumptionDtl, ConsumptionDtl.class);
	}

	static public ResponseEntity<SalesDltdDtl> saveDeletedSalesDtl(SalesDltdDtl salesDeleteDtl) {

		return restTemplate.postForEntity(HOST + "salesdeleteddetailsresource/salesdeleteddtl", salesDeleteDtl,
				SalesDltdDtl.class);
	}

	static public String saveAnalyticsTest() {

		return restTemplate.getForObject(HOST + "purchasesummary/analaticstest/", String.class);

	}

	static public ResponseEntity<ServiceInHdr> saveServiceinhdr(ServiceInHdr serviceInHdr) {

		return restTemplate.postForEntity(HOST + "serviceinhdr", serviceInHdr, ServiceInHdr.class);
	}

	// {servicehdrid}
	static public ResponseEntity<ServiceInDtl> saveServiceinDtl(ServiceInDtl serviceInDtl) {

		return restTemplate.postForEntity(HOST + "serviceindtl/" + serviceInDtl.getServiceInHdr().getId(), serviceInDtl,
				ServiceInDtl.class);
	}

	static public ResponseEntity<ProductionBatchStockDtl> saveProductionBatchStockDtl(
			ProductionBatchStockDtl productionBatchStockDtl) {

		return restTemplate.postForEntity(HOST + "productionbatchstockdtl", productionBatchStockDtl,
				ProductionBatchStockDtl.class);
	}

	static public ResponseEntity<WeighingItemMst> saveWeighingItemMst(WeighingItemMst weighingItemMst) {

		return restTemplate.postForEntity(HOST + "weighingitemmst", weighingItemMst, WeighingItemMst.class);

	}

	static public ResponseEntity<NutritionValueDtl> saveNutritionValueDtl(NutritionValueDtl nutritionValueDtl) {

		return restTemplate.postForEntity(HOST + "nutritionvaluedtl", nutritionValueDtl, NutritionValueDtl.class);

	}

	static public ResponseEntity<NutritionValueMst> saveNutritionValueMst(NutritionValueMst nutritionValueMst) {

		return restTemplate.postForEntity(HOST + "nutritionvaluemst", nutritionValueMst, NutritionValueMst.class);

	}

	static public ResponseEntity<SysDateMst> saveSysDateMst(SysDateMst sysDateMst) {

		return restTemplate.postForEntity(HOST + "systemdate", sysDateMst, SysDateMst.class);

	}

	static public ResponseEntity<SalesTypeMst> saveSalesType(SalesTypeMst salesTypeMst) {

		return restTemplate.postForEntity(HOST + "salestypemst", salesTypeMst, SalesTypeMst.class);

	}

	static public ResponseEntity<NutritionMst> saveNutritionMst(NutritionMst nutritionMst) {

		return restTemplate.postForEntity(HOST + "nutritionmst", nutritionMst, NutritionMst.class);

	}

	static public ResponseEntity<RawMaterialIssueHdr> saveRawMaterialIssueMst(RawMaterialIssueHdr rawMaterialIssueMst) {

		return restTemplate.postForEntity(HOST + "rawmaterialissuemst/", rawMaterialIssueMst,
				RawMaterialIssueHdr.class);

	}

	static public ResponseEntity<RawMaterialIssueDtl> saveRawMaterialIssueDtl(RawMaterialIssueDtl rawMaterialIssueDtl) {
		return restTemplate.postForEntity(HOST + "rawmaterialissuemst/"
				+ rawMaterialIssueDtl.getRawMaterialIssueHdr().getId() + "/rawmaterialissuedtl", rawMaterialIssueDtl,
				RawMaterialIssueDtl.class);

	}

	static public ResponseEntity<RawMaterialReturnHdr> saveRawMaterialReturnHdr(
			RawMaterialReturnHdr rawMaterialReturnHdr) {

		return restTemplate.postForEntity(HOST + "rawmaterialireturnhdr/", rawMaterialReturnHdr,
				RawMaterialReturnHdr.class);

	}

	static public ResponseEntity<RawMaterialReturnDtl> saveRawMaterialReturnDtl(
			RawMaterialReturnDtl rawMaterialReturnDtl) {
		return restTemplate.postForEntity(
				HOST + "rawmaterialreturndtl/" + rawMaterialReturnDtl.getRawMaterialReturnHdr().getId(),
				rawMaterialReturnDtl, RawMaterialReturnDtl.class);

	}

	static public ResponseEntity<OwnAccountSettlementDtl> saveOwnAccountSettlementDtl(
			OwnAccountSettlementDtl ownAccountSettlementDtl) {

		return restTemplate.postForEntity(HOST + "ownaccountsettlementdtl", ownAccountSettlementDtl,
				OwnAccountSettlementDtl.class);

	}

	static public ResponseEntity<OwnAccountSettlementMst> saveOwnAccountSettlementMst(
			OwnAccountSettlementMst ownAccountSettlementMst) {

		return restTemplate.postForEntity(HOST + "ownaccountsettlementmst", ownAccountSettlementMst,
				OwnAccountSettlementMst.class);

	}

	static public ResponseEntity<OwnAccount> saveOwnAccount(OwnAccount ownAccount) {

		return restTemplate.postForEntity(HOST + "ownaccount", ownAccount, OwnAccount.class);

	}

	public static ResponseEntity<SaleOrderReceipt> saveSaleOrderReceipts(SaleOrderReceipt saleOrderReceipt) {
		return restTemplate.postForEntity(HOST + "saleorderreceipts", saleOrderReceipt, SaleOrderReceipt.class);

	}

	public static ResponseEntity<List<SaleOrderReceipt>> getAllSaleOrderReceiptsBySalesTranOrdersHdr(
			String saleordertranshdrid) {
		String path = HOST + "retrivesalesorderreceipt/" + saleordertranshdrid;
		System.out.println(path);

		ResponseEntity<List<SaleOrderReceipt>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceipt>>() {
				});
		return response;
	}

	public static void deleteSalesOrderReceipts(String id) {
		restTemplate.delete(HOST + "deletesaleorderreceipt/" + id);
	}

	public static void deleteSalesOrderDtls(String id) {
		restTemplate.delete(HOST + "deletesalesorderdtls/" + id);
	}

	public static Double getTotalSaleOrederReceiptAmountBySaleOrderTransHdrId(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public static double getSumofSalesOrderReceiptbySalesOrdertransHdrId(String saleordertranshdrid) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "salesorderreceiptamount/" + saleordertranshdrid,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	public static ResponseEntity<List<SalesOrderDtl>> getsaleOrderDtlByHdrId(String saleordertranshdrid) {
		String path = HOST + "salesordertranshdr/" + saleordertranshdrid + "/salesorderdtl";
		System.out.println(path);
		ResponseEntity<List<SalesOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<SaleOrderEdit> saveSaleOrderEdit(SaleOrderEdit saleOrderEdit) {
		return restTemplate.postForEntity(HOST + "saleorderedit", saleOrderEdit, SaleOrderEdit.class);

	}

	public static Double getSaleOrderEditInvoiceAmount(String id) {
		return restTemplate.getForObject(HOST + "salesordereditamount/" + id, Double.class);

	}

	public static void DeleteSaleOrderEdit(String id) {
		restTemplate.delete(HOST + "deletesaleorderedit/" + id);
	}

	public static void DeleteSalesReturnDtl(String id) {
		restTemplate.delete(HOST + "salesreturndtl/salesreturndtlsdelete/" + id);
	}

	public static ResponseEntity<List<SaleOrderEdit>> getSaleOrderEditDtlsByHdrId(String id) {
		String path = HOST + "retrivesalesorderedit/" + id;
		System.out.println(path);
		ResponseEntity<List<SaleOrderEdit>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderEdit>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SalesInvoiceReport>> getInvoiceCustomerDtl(String voucherNumber, String date) {
		String path = HOST + "salesinvoicereport/gettaxinvoicecustomer/" + voucherNumber + "/"
				+ SystemSetting.systemBranch + "?rdate=" + date;
		System.out.println(path);

		ResponseEntity<List<SalesInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesInvoiceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesReturnInvoiceReport>> getReturnInvoiceCustomerDtl(String voucherNumber,
			String date) {
		String path = HOST + "salesreturninvoiceresource/gettaxinvoicecustomer/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);

		ResponseEntity<List<SalesReturnInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesReturnInvoiceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockSummaryCategoryWiseReport>> getItemwiseStockSummary(String id,
			String branchCode, String sDate, String tDate) {
		String path = HOST + "categorywisestocksummaryreport/" + id + "/" + branchCode + "?fromdate=" + sDate
				+ "&&todate=" + tDate;
		System.out.println(path);
		ResponseEntity<List<StockSummaryCategoryWiseReport>> response = restTemplate.exchange(path, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<StockSummaryCategoryWiseReport>>() {
				});
		return response;
	}

	// ============================= get all users=================
	public static ResponseEntity<List<UserMst>> getAllusers() {
		String path = HOST + "getallusers/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<UserMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<UserMst>>() {
				});
		return response;
	}

	static public ResponseEntity<List<DamageEntryReport>> getDamageEntryReport(String sDate, String tDate,
			String branchCode) {
		String path = HOST + "damageentryreport/" + branchCode + "?fromdate=" + sDate + "&&todate=" + tDate;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<DamageEntryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DamageEntryReport>>() {
				});
		return response;
	}

//========================== get receipt invoice dtls==============
	public static ResponseEntity<List<ReceiptInvoiceDtl>> getReceiptInvDtlById(ReceiptHdr receiptHdr) {
		String path = HOST + "receiptinvoicedtl/" + receiptHdr.getId() + "/getreceiptinvdtlbyid";
		System.out.println(path);
		ResponseEntity<List<ReceiptInvoiceDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptInvoiceDtl>>() {
				});
		return response;
	}

	static public void deleteReceiptInvDtl(String id) {

		restTemplate.delete(HOST + "receiptinvoicedtl/" + id + "/deletereceiptinvoicedtl");
		return;

	}

	static public void deleteOwnAcc(String id) {

		restTemplate.delete(HOST + "ownaccdelete/" + id);
		return;

	}

	public static ResponseEntity<List<PaymentInvoiceDtl>> getPaymentInvDtlById(PaymentHdr paymentHdr) {

		String path = HOST + "paymentInvoiceDtl/" + paymentHdr.getId() + "/getreceiptinvoicedtlbyid";
		System.out.println(path);
		ResponseEntity<List<PaymentInvoiceDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentInvoiceDtl>>() {
				});
		return response;
	}

	static public ResponseEntity<List<ReceiptInvoice>> getReceiptHdrByCustomerBetweenDate(String custId, String rdate,
			String tdate) {

		String path = HOST + "paymentreport/paymentbetweendatebyaccount/" + custId + "/" + SystemSetting.systemBranch
				+ "?rdate=" + rdate + "&&tdate=" + tdate;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<ReceiptInvoice>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptInvoice>>() {
				});
		return response;
	}

	static public ResponseEntity<List<PaymentVoucher>> getPaymentByCustomerBetweenDate(String custId, String rdate,
			String tdate) {
		String path = HOST + "paymentreport/paymentbetweendatebyaccount/" + custId + "/" + SystemSetting.systemBranch
				+ "?rdate=" + rdate + "&&tdate=" + tdate;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<PaymentVoucher>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentVoucher>>() {
				});
		return response;
	}

///{accountid}/{branchcode}""

	static public ResponseEntity<List<ReceiptInvoice>> getReceiptHdrBetweenDate(String rdate, String tdate) {
		String path = HOST + "receipthdr/receiptdtlreport?rdate=" + rdate + "&&tdate=" + tdate;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<ReceiptInvoice>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptInvoice>>() {
				});
		return response;
	}

	static public ResponseEntity<List<PaymentVoucher>> getpaymentHdrBetweenDate(String rdate, String tdate) {
		String path = HOST + "paymentreport/paymentbetweendate/" + SystemSetting.systemBranch + "?rdate=" + rdate
				+ "&&tdate=" + tdate;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<PaymentVoucher>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentVoucher>>() {
				});
		return response;
	}

	static public ResponseEntity<List<PaymentVoucher>> getPaymentHdrBetweenDate(String rdate, String tdate) {
		String path = HOST + "paymentreport/paymentbetweendate/" + SystemSetting.systemBranch + "?rdate=" + rdate
				+ "&&tdate=" + tdate;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<PaymentVoucher>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentVoucher>>() {
				});
		return response;
	}

	static public ResponseEntity<List<PaymentVoucher>> getPaymentHdrBetweenDateByAccount(String accId, String rdate,
			String tdate) {
		String path = HOST + "paymentreport/paymentbetweendatebyaccount/" + accId + "/" + SystemSetting.systemBranch
				+ "?rdate=" + rdate + "&&tdate=" + tdate;
		System.out.println("=============path=========" + path);

		ResponseEntity<List<PaymentVoucher>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentVoucher>>() {
				});
		return response;
	}

	static public ResponseEntity<List<ReceiptInvoiceDtl>> getReceiptInvDtlByCustomer(String custId) {
		String path = HOST + "receiptinvoicedtl/" + custId + "/retrivereceiptinvoicedtlbycustomer";
		System.out.println("=============path=========" + path);

		ResponseEntity<List<ReceiptInvoiceDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptInvoiceDtl>>() {
				});
		return response;
	}
	/*
	 * static public ResponseEntity<DamageEntryReport> getDamageEntries(String
	 * branchCode,String sDate, String tDate) { String path = HOST +
	 * "damageentries/" + "/"+branchCode +"?fromdate="+sDate+"&&todate="+tDate;
	 * System.out.println("=============path========="+path);
	 * 
	 * ResponseEntity<DamageEntryReport> response = restTemplate.exchange(path,
	 * HttpMethod.GET, null, new ParameterizedTypeReference<DamageEntryReport>() {
	 * }); return response; }
	 */

	public static ResponseEntity<SysDateMst> getSysDate(String branchCode) {

		String path = HOST + "getsysdate/" + branchCode;
		System.out.println(path);
		ResponseEntity<SysDateMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SysDateMst>() {
				});
		return response;
	}

	static public Double getCessAmountInTaxInvoiceReport(String voucherNo, String date) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "salesinvoice/cessamount/" + voucherNo + "/" + SystemSetting.systemBranch + "?rdate=" + date,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double getCessAmountInTaxReturnInvoiceReport(String voucherNo, String date) {

		System.out.println(HOST + "salesreturninvoiceresource/cessamount/" + voucherNo + "?rdate=" + date);
		return restTemplate.getForObject(HOST + "salesreturninvoiceresource/cessamount/" + voucherNo + "?rdate=" + date,
				Double.class);

	}

	public static ResponseEntity<List<ItemWiseDtlReport>> getDailySalesReport(String systemDate) {
		String path = HOST + "dailysalereportresource/itemwise/" + SystemSetting.systemBranch + "?rdate=" + systemDate;
		System.out.println(path);
		ResponseEntity<List<ItemWiseDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemWiseDtlReport>>() {
				});
		return response;
	}

//================ save payment inv dtl=======
	static public ResponseEntity<PaymentInvoiceDtl> savePaymentInvDtl(PaymentInvoiceDtl paymentInvDtl) {

		return restTemplate.postForEntity(HOST + "paymentInvoiceDtl", paymentInvDtl, PaymentInvoiceDtl.class);

	}

////////===================== Day book report=======================
	public static ResponseEntity<List<DayBook>> getDayBookReport(String branchCode, String date) {

		String path = HOST + "daybookreport/" + branchCode + "?rdate=" + date;
		System.out.println("path---------" + path);

		ResponseEntity<List<DayBook>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DayBook>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SaleOrderDetailsReport>> getSaleOrderPenidngReport(String branchCode, String date,
			String tdate) {

		String path = HOST + "saleorderreport/saleorderpendingreport/" + branchCode + "?fdate=" + date + "&&tdate="
				+ tdate;
		System.out.println("path---------" + path);

		ResponseEntity<List<SaleOrderDetailsReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderDetailsReport>>() {
				});
		return response;
	}

	public static ArrayList getWholeSaleInvoiceBill(String sdate) {
		System.out.println(
				HOST + SystemSetting.systemBranch + "/salestranshdr/getcustomerandvouchernumber?vDate=" + sdate);

		return restTemplate.getForObject(
				HOST + SystemSetting.systemBranch + "/salestranshdr/getcustomerandvouchernumber?vDate=" + sdate,
				ArrayList.class);

	}

	public static ArrayList getWholeSaleInvoiceBillByUser(String sdate, String userid, String branchcode) {

		String path = HOST + "invoiceeditenablemstbyuser/" + userid + "/" + branchcode + "?rdate=" + sdate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);

	}

	public static ArrayList getStockTransferVoucher(String sdate, String userid) {

		String path = HOST + "web/invoiceeditenablemst/getstocktransfervoucher/" + userid + "?vdate=" + sdate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);

	}

	public static ArrayList getPurchaseBillsByDate(String sdate) {
		System.out.println(
				HOST + SystemSetting.systemBranch + "/invoiceeditenable/getsupplierandvouchernumber?vDate=" + sdate);

		return restTemplate.getForObject(
				HOST + SystemSetting.systemBranch + "/invoiceeditenable/getsupplierandvouchernumber?vDate=" + sdate,
				ArrayList.class);

	}

	public static ResponseEntity<List<PurchaseHdr>> getPurchaseHdrByVoucherNumber(String voucherNo, String sdate) {
		String path = HOST + "purchasehdrbyvouchernumber/" + voucherNo + "/purchasehdr?date=" + sdate;
		System.out.println(path);
		ResponseEntity<List<PurchaseHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseHdr>>() {
				});
		return response;

	}

	public static ResponseEntity<AccountPayable> getAccountPayableByPrchaseHdrId(String purchaseHdrId) {
		String path = HOST + "retriveaccountpayablebypurchasehdrid/" + purchaseHdrId;
		System.out.println(path);
		ResponseEntity<AccountPayable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountPayable>() {
				});
		return response;

	}

	// {
	public static ResponseEntity<StockVerificationMst> getStockVerificationMst(String strDate) {
		String path = HOST + "stcokverificationmst/getpendingitems/" + SystemSetting.systemBranch + "?rdate=" + strDate;
		System.out.println(path);
		ResponseEntity<StockVerificationMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<StockVerificationMst>() {
				});
		return response;

	}

	public static ResponseEntity<StockVerificationMst> getStockVerificationMstByVoucherDate(String fdate) {
		String path = HOST + "stcokverificationmst/getstockverificationbydate/?rdate=" + fdate;
		System.out.println(path);
		ResponseEntity<StockVerificationMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<StockVerificationMst>() {
				});
		return response;

	}

	public static void updateWeighingItemMst(WeighingItemMst weighingItemMst) {
		restTemplate.put(HOST + "weighingitemmst/" + weighingItemMst.getItemId() + "/updateitem", weighingItemMst);
		return;
	}

	public static void updateAccountPayable(AccountPayable accountPayable) {
		restTemplate.put(HOST + "updateaccountpayable/" + accountPayable.getId(), accountPayable);
		return;
	}
	//

	public static void updateConsumptionHdr(ConsumptionHdr consumptionHdr) {
		restTemplate.put(HOST + "consumptionhdr/updateconsumptionhdr", consumptionHdr);
		return;
	}

	public static void updateIntentItemBranch(IntentItemBranchMst intentItemBranchMst) {
		restTemplate.put(HOST + "intentitembranch/updateitembranch/" + intentItemBranchMst.getItemId(),
				intentItemBranchMst);
		return;
	}

	// {intentid}/
	public static void updateIntentDtl(IntentInDtl intentInDtl) {
		restTemplate.put(HOST + "intentindtl/" + intentInDtl.getId() + "/updateintent", intentInDtl);
		return;
	}

	public static void updateServiceInHdr(ServiceInHdr serviceInHdr) {
		restTemplate.put(HOST + "serviceinhdr/" + serviceInHdr.getId() + "/updateserviceinhdr", serviceInHdr);
		return;
	}

	public static void updateReceiptDtl(ReceiptDtl receiptDtl) {
		restTemplate.put(HOST + "receipthdr/" + receiptDtl.getId() + "/updatereceiptdtl", receiptDtl);
		return;
	}

	public static void updatePaymentDtl(PaymentDtl paymentDtl) {
		restTemplate.put(HOST + "paymenthdr/" + paymentDtl.getId() + "/updatepaymentdtl", paymentDtl);
		return;
	}

	// {hdrId}/
	public static void updateStockVerificationMst(StockVerificationMst stockVerificationMst) {
		restTemplate.put(HOST + "stcokverificationmst/" + stockVerificationMst.getId() + "/finalsave",
				stockVerificationMst);
		return;
	}

	public static void updateStockVerification(StockVerificationDtl stockVerificationDtl) {
		restTemplate.put(HOST + "stockverificationdtl/" + stockVerificationDtl.getId() + "/updateqty",
				stockVerificationDtl);
		return;
	}

	public static void updateNUtritionValueDtl(NutritionValueDtl nutritionValueDtl) {
		restTemplate.put(HOST + "nutritionvaluedtl/" + nutritionValueDtl.getId() + "/updatenutritionvaluedtl",
				nutritionValueDtl);
		return;
	}

	public static void updateNutritionValueMst(NutritionValueMst nutritionValueMst) {
		restTemplate.put(HOST + "nutritionvaluemst/" + nutritionValueMst.getId() + "/updatenutritionvaluemst",
				nutritionValueMst);
		return;
	}

	public static void updateSysDate(SysDateMst sysDateMst) {
		restTemplate.put(HOST + "updatesysdate/" + sysDateMst.getId(), sysDateMst);
		return;
	}

	public static void updateSalesDtlByReturn(SalesDtl salesDtl) {
		restTemplate.put(HOST + "salesdetail/" + salesDtl.getId() + "/updatesalesdeatilreturn", salesDtl);
		return;
	}

	public static void updateSalesDtlByReturn(SalesReturnHdr salesReturnHdr) {
		restTemplate.put(HOST + "salesreturnhdr/" + salesReturnHdr.getId() + "/updatesalesreturn", salesReturnHdr);
		return;
	}

//	public static void updateCustomer(CustomerMst customerMst) {
//		restTemplate.put(HOST + "customerregistration/" + customerMst.getId() + "/updatecustomer", customerMst);
//		return;
//	}

	public static void updatePasterMst(PasterMst pasterMst) {
		restTemplate.put(HOST + "pastermstresource/" + pasterMst.getPasterMstId() + "/updatepastermst", pasterMst);
		return;
	}

	public static void updateMemberMst(MemberMst memberMst) {
		restTemplate.put(HOST + "membermstresource/" + memberMst.getId() + "/updatemembermst", memberMst);
		return;
	}

	public static void updateFamilyMst(FamilyMst familyMst) {
		restTemplate.put(HOST + "familymstresource/" + familyMst.getId() + "/updatefamilymst", familyMst);
		return;
	}

//	public static void updateSupplier(Supplier supplier) {
//		restTemplate.put(HOST + "supplierresource/" + supplier.getId() + "/updatesupplier", supplier);
//		return;
//	}

	public static void updateCategory(CategoryMst categoryMst) {
		restTemplate.put(HOST + "categorymst/" + categoryMst.getId() + "/updatecategory", categoryMst);
		return;
	}

	public static void updatePassword(UserMst userMst) {
		restTemplate.put(HOST + "usermst/" + userMst.getId() + "/changepassword", userMst);
		return;
	}

//	public static void updateCustomerRank(CustomerMst customerMst) {
//		restTemplate.put(HOST + "customerregistration/" + customerMst.getId() + "/updatecustomerrank", customerMst);
//		return;
//	}

	public static void updateOwnAccountSettlementbyInvNo(OwnAccountSettlementDtl ownAccountSettlementDtl) {
		System.out.println(HOST + "ownaccountsettlementdtl/" + ownAccountSettlementDtl.getInvoiceNumber());
		restTemplate.put(HOST + "ownaccountsettlementdtl/" + ownAccountSettlementDtl.getInvoiceNumber(),
				ownAccountSettlementDtl);
		return;
	}

	public static ResponseEntity<List<OwnAccountSettlementDtl>> getOwnAccountSettlebyMstid(String id) {
		String path = HOST + "ownaccountsettlementdtls/" + id;
		System.out.println(path);
		ResponseEntity<List<OwnAccountSettlementDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OwnAccountSettlementDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<TaxSummaryMst>> getInvoiceTotal(String voucherNumber, String date) {
		String path = HOST + "sumoftaxes/" + voucherNumber + "/" + SystemSetting.systemBranch + "?rdate=" + date;
		System.out.println(path);
		ResponseEntity<List<TaxSummaryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxSummaryMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<TaxSummaryMst>> getReturnInvoiceTotal(String voucherNumber, String date) {
		String path = HOST + "salesreturninvoiceresource/sumoftaxes/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);
		ResponseEntity<List<TaxSummaryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxSummaryMst>>() {
				});
		return response;
	}

	public static ResponseEntity<AccountReceivable> getAccountReceivableBySalesTransHdrId(String salestranshdrid) {
		String path = HOST + "accountreceivable/fetchaccreceivablebysalestranshdr/" + salestranshdrid;
		System.out.println(path);
		ResponseEntity<AccountReceivable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountReceivable>() {
				});
		return response;
	}

	public static ResponseEntity<List<SalesReceipts>> getSalesReceiptsByTransHdrId(String salestranshdrid) {
		String path = HOST + "salestranshdr/sales_receipts/" + salestranshdrid;
		System.out.println(path);

		ResponseEntity<List<SalesReceipts>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesReceipts>>() {
				});
		return response;
	}

	public static ResponseEntity<SalesTransHdr> deleteSalesReceiptsByTransHdr(SalesTransHdr salesTransHdr) {
		return restTemplate.postForEntity(HOST + "/salestranshdr/sales_receipts/delete", salesTransHdr,
				SalesTransHdr.class);
	}

	public static ResponseEntity<SalesReturnHdr> saveSalesReturnHdr(SalesReturnHdr salesReturnHdr) {
		return restTemplate.postForEntity(HOST + "salesreturnhdr", salesReturnHdr, SalesReturnHdr.class);
	}

	public static ResponseEntity<SalesReturnDtl> saveSalesReturnDtl(SalesReturnDtl salesReturnDtl) {
		return restTemplate.postForEntity(HOST + "salesreturndtl", salesReturnDtl, SalesReturnDtl.class);
	}

	public static ResponseEntity<List<SupplierBillwiseDueDayReport>> getSupplierBillwiseDueDayReport(String startDate,
			String endDate, String supplierId) {
		System.out.println("---------restcaller-----------");
		String path = HOST + "supplierledgerrepot/supplierduedayreport/" + supplierId + "?fromdate=" + startDate
				+ "&&todate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<SupplierBillwiseDueDayReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SupplierBillwiseDueDayReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<PurchaseHdr>> getAllPurchaseHdrByDate(String startDate, String endDate,
			String branchCode) {
		String path = HOST + "purchasereport1/" + branchCode + "/purchasereport?fromdate=" + startDate + "&&todate="
				+ endDate;

		System.out.println("" + path);
		ResponseEntity<List<PurchaseHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseHdr>>() {
				});
		return response;

	}

	public static ResponseEntity<List<ReceiptHdr>> getAllReceiptHdrByDate(String startDate, String endDate,
			String branchCode) {
		String path = HOST + "receiptreport/receiptreporthdr/" + branchCode + "?fromdate=" + startDate + "&&todate="
				+ endDate;

		System.out.println("" + path);
		ResponseEntity<List<ReceiptHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptHdr>>() {
				});
		return response;

	}

	public static ResponseEntity<List<OrgReceiptReport>> getOrgReceiptReport(String startDate, String endDate,
			String branchCode) {
		String path = HOST + "receiptreport/orgreceiptreport/" + branchCode + "?fromdate=" + startDate + "&&todate="
				+ endDate;

		System.out.println("" + path);
		ResponseEntity<List<OrgReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgReceiptReport>>() {
				});
		return response;

	}

	public static ResponseEntity<List<OrgReceiptReport>> getOrgReceiptReportAccountWise(String startDate,
			String endDate, String branchCode, String accountId) {
		String path = HOST + "receiptreport/orgreceiptreportaccountwise/" + branchCode + "/" + accountId + "?fromdate="
				+ startDate + "&&todate=" + endDate;

		System.out.println("" + path);
		ResponseEntity<List<OrgReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgReceiptReport>>() {
				});
		return response;

	}

	public static ResponseEntity<List<JournalHdr>> getJournalHdrReport(String startDate, String endDate,
			String branchCode) {
		String path = HOST + "journalreportresource/journalhdrreport/" + branchCode + "?fromdate=" + startDate
				+ "&&todate=" + endDate;

		System.out.println("" + path);
		ResponseEntity<List<JournalHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JournalHdr>>() {
				});
		return response;

	}

	public static ResponseEntity<List<PaymentHdr>> getAllPaymentHdrByDate(String startDate, String endDate,
			String branchCode) {
		String path = HOST + "paymentreport/paymentreports/" + branchCode + "?fromdate=" + startDate + "&&todate="
				+ endDate;

		System.out.println("" + path);
		ResponseEntity<List<PaymentHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentHdr>>() {
				});
		return response;

	}

	public static ResponseEntity<List<PurchaseDtl>> getPurchaseReportDtl(String purid) {
		String path = HOST + "purchasehdr/" + purid + "/purchasedtl";

		System.out.println("accountstatement" + path);
		ResponseEntity<List<PurchaseDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseDtl>>() {
				});
		return response;

	}

	public static ResponseEntity<List<ReceiptDtl>> getReceiptReportDtl(String receiptid) {
		String path = HOST + "receipthdr/" + receiptid + "/receiptdtlreports";

		System.out.println("accountstatement" + path);
		ResponseEntity<List<ReceiptDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptDtl>>() {
				});
		return response;

	}

	public static ResponseEntity<List<JournalDtl>> getJurnalDtl(String journalhdrid) {
		String path = HOST + "journalreportresource/journaldtlreport/" + journalhdrid;

		System.out.println("accountstatement " + path);
		ResponseEntity<List<JournalDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JournalDtl>>() {
				});
		return response;

	}

	public static ResponseEntity<List<PaymentDtl>> getPaymentDtlReportDtl(String paymentHdrId) {
		String path = HOST + "paymentreport/" + paymentHdrId + "/paymentdtl";

		System.out.println("accountstatement" + path);
		ResponseEntity<List<PaymentDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentDtl>>() {
				});
		return response;

	}

	public static ResponseEntity<List<OrgMonthlyAccPayReport>> getOrgMonthlyAccPayReport(String startDate,
			String endDate, String accountid) {
		String path = HOST + "orgaccpaymentreports/" + accountid + "?startDate=" + startDate + "&&endDate=" + endDate;

		System.out.println("orgmonthlyaccountpayreport" + path);
		ResponseEntity<List<OrgMonthlyAccPayReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgMonthlyAccPayReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<OrgMemberWiseReceiptReport>> getOrgMemberwiseReport(String startDate,
			String endDate, String accountid) {
		String path = HOST + "memberwisereport/" + accountid + "?startDate=" + startDate + "&&endDate=" + endDate;

		System.out.println("memberwisereport" + path);
		ResponseEntity<List<OrgMemberWiseReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OrgMemberWiseReceiptReport>>() {
				});
		return response;
	}

	static public void updateBarcode(ItemMst itemMst) {

		restTemplate.put(HOST + "itemmstresource/" + SystemSetting.systemBranch + "/barcodeprintingupdate", itemMst);
		return;
	}

	static public void updateIngredients(IngredientsMst ingredientsMst) {

		restTemplate.put(HOST + "itemmstresource/" + SystemSetting.systemBranch + "/ingredientsupdate", ingredientsMst);
		return;
	}

	public static ResponseEntity<IngredientsMst> getIngredientByItemId(String id) {

		String path = HOST + "IngredientsMstResource/" + id + "/findbyitemmst";

		System.out.println("accountstatement" + path);
		ResponseEntity<IngredientsMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<IngredientsMst>() {
				});
		return response;

	}

	public static ResponseEntity<List<ItemBatchExpiryDtl>> getItemBatchExpByItemAndBatch(String itemId, String bacth) {

		String path = HOST + "itembatchexpirydtl/getitemexpiry/" + itemId + "/" + bacth;

		System.out.println("itembatchexpirydtl" + path);
		ResponseEntity<List<ItemBatchExpiryDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemBatchExpiryDtl>>() {
				});
		return response;

	}

	public static ResponseEntity<IngredientsMst> saveIngredientsMst(IngredientsMst ingredientsMst) {

		return restTemplate.postForEntity(HOST + "itemmstresource/ingredients", ingredientsMst, IngredientsMst.class);

	}

	public static Double getTaxableInvoiceAmount(String voucherNumber, String date) {
		System.out.println(HOST + "salesinvoice/nontaxableamount/" + voucherNumber + "/" + SystemSetting.systemBranch
				+ "?rdate=" + date);
		return restTemplate.getForObject(HOST + "salesinvoice/nontaxableamount/" + voucherNumber + "/"
				+ SystemSetting.systemBranch + "?rdate=" + date, Double.class);

	}

	public static Double getReturnTaxableInvoiceAmount(String voucherNumber, String date) {
		return restTemplate.getForObject(
				HOST + "salesreturninvoiceresource/nontaxableamount/" + voucherNumber + "?rdate=" + date, Double.class);

	}

	public static ResponseEntity<SchOfferAttrListDef> getSchemeOfferAttributeListDefByName(String offerAttribute) {
		String path = HOST + "schofferattributelistdefbyname/" + offerAttribute;
		System.out.println(path);
		ResponseEntity<SchOfferAttrListDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchOfferAttrListDef>() {
				});

		return response;
	}

	public static ResponseEntity<SchOfferAttrListDef> saveSchOfferAttrListDef(SchOfferAttrListDef schOfferAttrListDef) {
		String path = HOST + "schofferattrlistdef";
		System.out.println(path);
		return restTemplate.postForEntity(path, schOfferAttrListDef, SchOfferAttrListDef.class);
	}

	public static ResponseEntity<SchSelectAttrListDef> saveSchSelectionAtrribute(
			SchSelectAttrListDef schSelectAttrListDef) {
		return restTemplate.postForEntity(HOST + "schselectattrlistdef", schSelectAttrListDef,
				SchSelectAttrListDef.class);
	}

	public static ResponseEntity<SchSelectAttrListDef> getSchSelectionAttributeNameByName(String selectionAttribute) {
		String path = HOST + "schselectsttrlistdefbyname/" + selectionAttribute;
		System.out.println(path);
		ResponseEntity<SchSelectAttrListDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchSelectAttrListDef>() {
				});

		return response;
	}

	public static ResponseEntity<SchEligiAttrListDef> getSchEligibilityAttrListDefByName(String eligibilityAttrName,
			String eligibilityId) {
		String path = HOST + "scheligiattrlistdefbyname/" + eligibilityAttrName + "/" + eligibilityId;
		System.out.println(path);
		ResponseEntity<SchEligiAttrListDef> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchEligiAttrListDef>() {
				});

		return response;
	}

	public static ResponseEntity<SchEligiAttrListDef> saveSchEligibilityAttrDef(
			SchEligiAttrListDef schEligibilityAttrDef) {
		System.out.println(HOST + "scheligiattrlistdef");
		return restTemplate.postForEntity(HOST + "scheligiattrlistdef", schEligibilityAttrDef,
				SchEligiAttrListDef.class);
	}

	public static ResponseEntity<SchSelectionAttribInst> saveSchSelectionAttribInst(
			SchSelectionAttribInst schSelectionAttribInst) {
		System.out.println(HOST + "schselectionattributeinst");
		return restTemplate.postForEntity(HOST + "schselectionattributeinst", schSelectionAttribInst,
				SchSelectionAttribInst.class);
	}

	public static ResponseEntity<SchOfferAttrInst> saveSchOfferAttrInst(SchOfferAttrInst schOfferAttrInst) {
		System.out.println(HOST + "schofferattrinst");
		return restTemplate.postForEntity(HOST + "schofferattrinst", schOfferAttrInst, SchOfferAttrInst.class);
	}

	public static ResponseEntity<List<SiteMst>> getSiteMstByCustomerId(String custId) {
		String path = HOST + "sitemstbycustomerid/" + custId;
		System.out.println(path);
		ResponseEntity<List<SiteMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SiteMst>>() {
				});

		return response;

	}

	public static ResponseEntity<BranchMst> getBranchMstByName(String customerName) {
		String path = HOST + "branchmstbybranchname/" + customerName;
		System.out.println(path);
		ResponseEntity<BranchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BranchMst>() {
				});

		return response;
	}

	public static void deleteLocalCustomerMst(String id) {
		// TODO Auto-generated method stub

	}

	public static ResponseEntity<List<LocalCustomerMst>> getAllLocalCustomer() {
		String path = HOST + "getalllocalcustomer";
		System.out.println(path);
		ResponseEntity<List<LocalCustomerMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LocalCustomerMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<LocalCustomerMst>> getLocalCustomerByCustomerId(String customerId) {
		String path = HOST + "localcustomerbycustomerid/" + customerId;
		System.out.println(path);
		ResponseEntity<List<LocalCustomerMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LocalCustomerMst>>() {
				});

		return response;
	}

	public static ArrayList searchCategoryByName(String searchData) {
		System.out.println("categorysearch?data" + searchData);

		return restTemplate.getForObject(HOST + "categorysearch?data=" + searchData, ArrayList.class);
	}

	public static ArrayList searchMenuByName(String searchData) {

		String path = HOST + "menumstresource/menusearch?searchdata=" + searchData;

		System.out.println(path);

		return restTemplate.getForObject(HOST + "menumstresource/menusearch?searchdata=" + searchData, ArrayList.class);
	}

	public static ArrayList searchProcessPermissionByName(String searchData) {

		String path = HOST + "processmstresource/processsearch?data=" + searchData;

		System.out.println(path);

		return restTemplate.getForObject(HOST + "processmstresource/processsearch?data=" + searchData, ArrayList.class);
	}

	public static ArrayList searchBatch(String searchData) {

		String path = HOST + "batchsearch?data" + searchData;
		System.out.println(path);
		return restTemplate.getForObject(HOST + "batchsearch?data=" + searchData, ArrayList.class);

	}

	public static ResponseEntity<SchEligibilityAttribInst> saveSchEligibilityAttrInst(
			SchEligibilityAttribInst schEligibilityAttribInst) {
		logger.info(HOST + "schofferattrinst");
		
		ResponseEntity schEligibilityAttribInstResp = restTemplate.postForEntity(HOST + "scheligibilityattribinst", schEligibilityAttribInst,
				SchEligibilityAttribInst.class);
		
		return schEligibilityAttribInstResp;
	}

	public static ResponseEntity<SchEligibilityAttribInst> SchEligibilityAttribInstByAttribName(String attribName,
			String schemeId) {
		String path = HOST + "eligibilityattribinstbyattributename/" + attribName + "/" + schemeId;
		System.out.println(path);
		ResponseEntity<SchEligibilityAttribInst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchEligibilityAttribInst>() {
				});

		return response;
	}

	public static ResponseEntity<SchSelectionAttribInst> SchSelectionAttribInstByAttrNameAndSchemeId(String attribName,
			String id) {
		String path = HOST + "schselectionattributeinstbyattribname/" + attribName + "/" + id;
		System.out.println(path);
		ResponseEntity<SchSelectionAttribInst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchSelectionAttribInst>() {
				});

		return response;
	}

	public static ResponseEntity<SchOfferAttrInst> SchOfferAttrInstByAttrNameAndSchemeId(String attribName, String id) {
		String path = HOST + "schofferattrinstbyattrname/" + attribName + "/" + id;
		System.out.println(path);
		ResponseEntity<SchOfferAttrInst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchOfferAttrInst>() {
				});

		return response;
	}

	public static ResponseEntity<List<SchEligibilityAttribInst>> getSchEligibilityAttrInstBySchemeId(String id) {
		String path = HOST + "allscheligibilityattribinstbyschemeid/" + id;
		System.out.println(path);
		ResponseEntity<List<SchEligibilityAttribInst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchEligibilityAttribInst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SchSelectionAttribInst>> getSchSelectionAttribInstBySchemeId(String id) {
		String path = HOST + "allschselectionattributeinstbyschemeid/" + id;
		ResponseEntity<List<SchSelectionAttribInst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchSelectionAttribInst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SchOfferAttrInst>> getSchOfferAttrInstBySchemeId(String id) {
		String path = HOST + "allschofferattrinstbyschemeid/" + id;
		ResponseEntity<List<SchOfferAttrInst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchOfferAttrInst>>() {
				});

		return response;
	}

	public static void deleteSchSelectionAttribInst(String id) {
		restTemplate.delete(HOST + "deleteschselectionattribinst/" + id);
		return;
	}

	public static void deleteSchEligibilityAttribInst(String id) {
		restTemplate.delete(HOST + "deletescheligibilityattribinst/" + id);
		return;
	}

	public static void deleteSchOfferAttrInst(String id) {
		restTemplate.delete(HOST + "deleteschofferattrinst/" + id);
		return;
	}

	public static ResponseEntity<List<ItemBatchMst>> getItemBatchMstByItemId(String itemId) {
		String path = HOST + "itembatchmstbyitemid/" + itemId;
		ResponseEntity<List<ItemBatchMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemBatchMst>>() {
				});

		return response;
	}

	public static ArrayList getItemBatchMstByBatch(String searchData) {
		System.out.println("categorysearch?data" + searchData);

		return restTemplate.getForObject(HOST + "itembatchmstbybatch?data=" + searchData, ArrayList.class);
	}

	public static ResponseEntity<LanguageMst> saveLanguageMst(LanguageMst lang) {

		System.out.println(HOST + "languagemst");
		return restTemplate.postForEntity(HOST + "languagemst", lang, LanguageMst.class);
	}

	public static ResponseEntity<List<LanguageMst>> getAllLangues() {
		String path = HOST + "languagemsts";
		ResponseEntity<List<LanguageMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LanguageMst>>() {
				});

		return response;
	}

	public static void deleteLanguageById(String id) {
		restTemplate.delete(HOST + "deletelanguagemst/" + id);

	}

	public static ResponseEntity<ItemNameInLanguage> saveItemNameInLanguage(ItemNameInLanguage itemInLang) {

		return restTemplate.postForEntity(HOST + "itemnameinlanguage", itemInLang, ItemNameInLanguage.class);
	}

	public static ResponseEntity<LanguageMst> getLanguageMstByLangName(String langName) {
		String path = HOST + "languagemstsbylangname/" + langName;
		ResponseEntity<LanguageMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<LanguageMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemNameInLanguage>> getAllItemNameInLanguage() {
		String path = HOST + "itemnameinlanguages";
		ResponseEntity<List<ItemNameInLanguage>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemNameInLanguage>>() {
				});

		return response;
	}

	public static void deleteItemNameInLanguageById(String id) {
		restTemplate.delete(HOST + "deleteitemnameinlanguages/" + id);

	}

	static public ResponseEntity<List<ItemBatchMst>> returnStatus() {

		String path = HOST + "itembatchmst";

		ResponseEntity<List<ItemBatchMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemBatchMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReorderMst>> returnValueOfReoderItemWise(String itemId) {

		String path = HOST + "reordermstseraches/" + itemId;
		ResponseEntity<List<ReorderMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReorderMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReorderMst>> returnReoderItemWise(String itemname, String suppliername) {

		String path = HOST + "reordermst/" + itemname + "/" + suppliername + "/reoders";
		ResponseEntity<List<ReorderMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReorderMst>>() {
				});

		return response;

	}

	public static ResponseEntity<ItemBatchMst> getItemBatchMstByItemIdAndBatch(String attributeValue, String id) {
		String path = HOST + "itembatchmstbyitemidandbatch/" + id + "/" + attributeValue;
		ResponseEntity<ItemBatchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ItemBatchMst>() {
				});

		return response;
	}

	public static void updateSchemeMst(SchemeInstance schemeInstance) {
		restTemplate.put(HOST + "updateschemeinstance/" + schemeInstance.getId(), schemeInstance);
		return;

	}

	public static ResponseEntity<List<SchemeInstance>> getActiveSchemeDeteils(String wholesaleorretail) {
		String path = HOST + "activeschemeinstances/" + wholesaleorretail;
		ResponseEntity<List<SchemeInstance>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchemeInstance>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesDtl>> getSalesDtlsByItemId(String salesTransHdrId, String itemId) {
		String path = HOST + "salesdtlsbyitemid/" + salesTransHdrId + "/" + itemId;
		ResponseEntity<List<SalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesDtl>> getSalesDtlsByCategoryIdAndSalesTransHdr(String categoryid,
			String salesTransHdrId) {
		String path = HOST + "salesdtlbycategoryid/" + salesTransHdrId + "/" + categoryid;
		ResponseEntity<List<SalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SchEligibilityAttribInst>> getSchEligibAttrInstByEligibIdAndSchemeId(
			String eligibilityId, String schemeId) {
		String path = HOST + "allscheligibilityattribinstbyschemeidandeligibilityid/" + eligibilityId + "/" + schemeId;
		ResponseEntity<List<SchEligibilityAttribInst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchEligibilityAttribInst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesDtl>> getSalesDtlItemAndBatch(String itemid, String batch,
			String salesTransHdrId) {

		String path = HOST + "salesdtlbyitemidandbatchandsalestranshdrid/" + itemid + "/" + batch + "/"
				+ salesTransHdrId;
		ResponseEntity<List<SalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<CategoryMst> getCategoryById(String categoryId) {
		String path = HOST + "categorymstbyid/" + categoryId;
		ResponseEntity<CategoryMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CategoryMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesDtl>> getSalesDtlByOfferReferenceId(String referenceid,
			String salestranshdrid) {
		String path = HOST + "salesdtlbyofferreferenceid/" + referenceid + "/" + salestranshdrid;
		ResponseEntity<List<SalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesDtl>>() {
				});

		return response;
	}

	public static void updateSalesDlt(SalesDtl salesDtlref) {
		restTemplate.put(HOST + "updatesalesdtl/" + salesDtlref.getId(), salesDtlref);
		return;

	}

	public static ResponseEntity<SchemeInstance> getSchemeInstanceById(String schemeId) {

		String path = HOST + "schemeinstbyid/" + schemeId;
		ResponseEntity<SchemeInstance> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchemeInstance>() {
				});

		return response;
	}

	public static void updateSchemeInstance(String id) {
		restTemplate.put(HOST + "finalupdateschemeinstance/" + id, null);
		return;

	}

	public static ResponseEntity<DayEndClosureHdr> getTotalCashAndCardSale(String strDate) {
		String path = HOST + "gettotalcardandcashsale?voucherdate=" + strDate;

		System.out.println(path);

		return restTemplate.getForEntity(path, DayEndClosureHdr.class);
	}

	public static ResponseEntity<List<DailySalesReportDtl>> getCustomerDailySaleReport(String branchCode,
			String strDate, String endDate, String customerId) {
		String path = HOST + "customerdailysalesreportdtl/" + branchCode + "/" + customerId + "?fdate=" + strDate
				+ "&&tdate=" + endDate;
		ResponseEntity<List<DailySalesReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReportDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<PDCReceipts> SavePDCReceipts(PDCReceipts pdcReceipts) {

		return restTemplate.postForEntity(HOST + "pdcreceiptresource/pdcreceipts", pdcReceipts, PDCReceipts.class);
	}

	public static void deletePDCReceipts(String id) {
		restTemplate.delete(HOST + "pdcreceipts/" + id);
	}

	public static ResponseEntity<List<PDCReceipts>> getPDCReceiptsByDate(String date) {
		String path = HOST + "pdcreceiptsbydate/" + SystemSetting.systemBranch + "?rdate=" + date;
		ResponseEntity<List<PDCReceipts>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PDCReceipts>>() {
				});

		return response;
	}

	public static ResponseEntity<PDCPayment> SavePDCPaymen(PDCPayment pdcPayment) {
		return restTemplate.postForEntity(HOST + "pdcpaymentresource/pdcpayment", pdcPayment, PDCPayment.class);

	}

	public static ResponseEntity<List<PDCPayment>> getPDCPaymentsByDate(String date) {

		String path = HOST + "pdcpaymentbydate/" + SystemSetting.systemBranch + "?rdate=" + date;
		ResponseEntity<List<PDCPayment>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PDCPayment>>() {
				});

		return response;

	}

	public static void deletePDCPayment(String id) {

		System.out.println(HOST + "pdcpayment/" + id);
		restTemplate.delete(HOST + "pdcpayment/" + id);

	}

	public static ResponseEntity<List<PDCPayment>> getPDCPaymentByBankAndStatus(String bank) {
		String path = HOST + "pdcpaymentbystatus/" + bank;
		ResponseEntity<List<PDCPayment>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PDCPayment>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PDCReceipts>> getPDCReceiptsByBankAndStatus(String bank) {
		String path = HOST + "pdcreceiptbystatus/" + bank;
		ResponseEntity<List<PDCReceipts>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PDCReceipts>>() {
				});

		return response;
	}

	public static ResponseEntity<PDCPayment> getPDCPaymentById(String id) {
		String path = HOST + "pdcpaymentbyid/" + id;
		ResponseEntity<PDCPayment> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PDCPayment>() {
				});

		return response;
	}

	public static ResponseEntity<PDCReceipts> getPDCReceiptsById(String id) {
		String path = HOST + "pdcreceiptsbyid/" + id;
		ResponseEntity<PDCReceipts> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PDCReceipts>() {
				});

		return response;
	}

	public static void updatePDCPayment(PDCPayment pdcPaymentObj) {
		restTemplate.put(HOST + "pdcpaymentresource/updatepdcpayment/" + pdcPaymentObj.getId(), pdcPaymentObj);
		return;
	}

	public static void updatePDCReceipts(PDCReceipts pdcReceipts) {
		restTemplate.put(HOST + "pdcreceiptresource/updatepdcreceipt/" + pdcReceipts.getId(), pdcReceipts);
		return;
	}

	public static ResponseEntity<PDCReconcileHdr> savePDCReconcileHdr(PDCReconcileHdr pdcReconcileHdr) {
		return restTemplate.postForEntity(HOST + "pdcreconcilehdrresource/pdcreconcilehdr", pdcReconcileHdr,
				PDCReconcileHdr.class);

	}

	public static ResponseEntity<PDCReconcileDtl> savePDCReconcile(PDCReconcileDtl pdcReconcile) {
		System.out
				.println(HOST + "pdcreconciledtlresource/pdcreconciledtl/" + pdcReconcile.getPdcReconcileHdr().getId());
		ResponseEntity<PDCReconcileDtl> resp = restTemplate.postForEntity(
				HOST + "pdcreconciledtlresource/pdcreconciledtl/" + pdcReconcile.getPdcReconcileHdr().getId(),
				pdcReconcile, PDCReconcileDtl.class);

		return resp;
	}

	public static ResponseEntity<List<PDCReconcileDtl>> getAllPDCReconcileDtl(String id) {
		String path = HOST + "pdcreconciledtlsbyhdr/" + id;
		ResponseEntity<List<PDCReconcileDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PDCReconcileDtl>>() {
				});

		return response;
	}

	public static void updatePDCReconcileHdr(PDCReconcileHdr pdcReconcileHdr) {

		restTemplate.put(HOST + "pdcreconcilehdrresource/updatepdcreconcile/" + pdcReconcileHdr.getId(),
				pdcReconcileHdr);
		return;

	}

	public static void deletePDCReconcileDtl(String id) {
		restTemplate.delete(HOST + "deletepdcreconcile/" + id);
	}

	public static ResponseEntity<AccountHeads> getAccountHeadsById(String account) {
		String path = HOST + "accountheadsbyid/" + account;
		System.out.println(path);
		ResponseEntity<AccountHeads> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountHeads>() {
				});

		return response;
	}

	public static ResponseEntity<List<SchSelectionAttribInst>> getSchSelectionAttribInstSelectionIDAndBySchemeId(
			String id, String selectionid) {
		String path = HOST + "allschselectionattributeinstbyschemeidandselectionid/" + id + "/" + selectionid;
		ResponseEntity<List<SchSelectionAttribInst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchSelectionAttribInst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SchOfferAttrInst>> getSchOfferAttrInstBySchemeIdAndOfferId(String id,
			String offerId) {
		String path = HOST + "allschofferattrinstbyschemeidandofferid/" + id + "/" + offerId;
		ResponseEntity<List<SchOfferAttrInst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SchOfferAttrInst>>() {
				});

		return response;
	}

	public static ResponseEntity<SchEligibilityAttribInst> SchEligibilityAttribInstByAttribNameAndEligibilityId(
			String attribName, String schemeId, String eligibilityId) {
		String path = HOST + "eligibilityattribinstbyattributenameandeligibilityid/" + attribName + "/" + schemeId + "/"
				+ eligibilityId;
		
		logger.info("ResponseEntity<SchEligibilityAttribInst> SchEligibilityAttribInstByAttribNameAndEligibilityId");
		logger.info("Path = {} ", path);
		
		ResponseEntity<SchEligibilityAttribInst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchEligibilityAttribInst>() {
				});

		return response;
	}

	public static ResponseEntity<SchSelectionAttribInst> SchSelectionAttribInstByAttrNameAndSchemeIdAndSelectionId(
			String attribName, String id, String selectionId) {
		String path = HOST + "schselectionattributeinstbyattribnameandselectionid/" + attribName + "/" + id + "/"
				+ selectionId;
		System.out.println(path);
		ResponseEntity<SchSelectionAttribInst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchSelectionAttribInst>() {
				});
		return response;
	}

	public static ResponseEntity<SchOfferAttrInst> SchOfferAttrInstByAttrNameAndSchemeIdAndOfferId(String attribName,
			String id, String offerid) {
		String path = HOST + "schofferattrinstbyattrnameandofferid/" + attribName + "/" + id + "/" + offerid;
		ResponseEntity<SchOfferAttrInst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SchOfferAttrInst>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesTransHdr>> getAllSalesTransHdrByDate(String date) {
		String path = HOST + "salestranshdrbyvoucherdate?date=" + date;
		System.out.println(path);
		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesTransHdr>> getAllSalesTransHdrByDateAndMode(String date) {
		String path = HOST + "salestranshdr/salestranshdrbyvoucherdateandmode?date=" + date;
		System.out.println(path);
		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<SalesReturnDtl> findBySalesDtl(String hdrId, SalesDtl salesDtl) {
		return restTemplate.postForEntity(HOST + "salesreturndtlbyitemidandhdr/" + hdrId, salesDtl,
				SalesReturnDtl.class);

	}

	public static void deleteSalesTransHdr(String id) {
		restTemplate.delete(HOST + "deletesalestranshdr/" + id);

	}

	public static ResponseEntity<ProductMst> findProductMstByName(String productName) {
		String path = HOST + "productmstresource/productmstbyname?productname=" + productName;
		ResponseEntity<ProductMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProductMst>() {
				});

		return response;
	}

	public static ResponseEntity<ProductMst> saveServiceProductMst(ProductMst productMst) {
		return restTemplate.postForEntity(HOST + "productmstresource/saveproductmst", productMst, ProductMst.class);

	}

	public static ResponseEntity<BrandMst> findBrandMstByName(String brand) {
		String path = HOST + "brandmstrepository/brandmstbyname?brandname=" + brand;
		ResponseEntity<BrandMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BrandMst>() {
				});

		return response;
	}

	public static ResponseEntity<BrandMst> saveServiceBrandMst(BrandMst brandMst) {
		return restTemplate.postForEntity(HOST + "brandmstrepository/savebrandmst", brandMst, BrandMst.class);

	}

	public static ResponseEntity<List<ItemMst>> getAllItemMst() {

		String path = HOST + "allitemmsts";
		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<ProductMst> productById(String id) {
		String path = HOST + "productmstresource/productmstbyid/" + id;
		ResponseEntity<ProductMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProductMst>() {
				});

		return response;
	}

	public static ResponseEntity<BrandMst> brandById(String brandId) {
		String path = HOST + "brandmstrepository/brandmstbyid/" + brandId;
		ResponseEntity<BrandMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BrandMst>() {
				});

		return response;
	}

	public static ResponseEntity<LocationMst> saveLocationMst(LocationMst locationMst) {
		return restTemplate.postForEntity(HOST + "locationmstresource/savelocationmst", locationMst, LocationMst.class);

	}

	public static ResponseEntity<List<LocationMst>> findAllLocationMst() {
		String path = HOST + "locationmstresource/productmsts";
		ResponseEntity<List<LocationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LocationMst>>() {
				});

		return response;
	}

	public static ResponseEntity<JobCardHdr> saveJobCard(JobCardHdr jobCardHdr) {
		return restTemplate.postForEntity(HOST + "jobcardhdr", jobCardHdr, JobCardHdr.class);

	}

	public static ResponseEntity<ServiceItemMst> saveServiceItem(ServiceItemMst serviceItemMst) {
		return restTemplate.postForEntity(HOST + "serviceitemmst", serviceItemMst, ServiceItemMst.class);

	}

	public static ArrayList getTrialBalance(String sdate) {
		System.out.println(HOST + "trialbalance?startdate=" + sdate);
		return restTemplate.getForObject(HOST + "trialbalance?startdate=" + sdate, ArrayList.class);

	}
	
	public static ArrayList getTrialBalance(String sdate,String edate,String reportdate) {
		System.out.println(HOST + "trialbalance/" + SystemSetting.getUserId() + "?startdate=" + sdate);
		return restTemplate.getForObject(HOST + "trialbalance/gettrialbalancedetail/" + SystemSetting.getUserId() + "?startdate=" + sdate 
				+ "&enddate=" + edate + "&reportdate=" + reportdate, ArrayList.class);

	}

	public static ResponseEntity<List<BranchWiseProfitReport>> branchWiseProfit(String startDate, String endDate) {
		String path = HOST + "salestranshdrreport/brnchwiseprofit?fdate=" + startDate + "&&tdate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<BranchWiseProfitReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchWiseProfitReport>>() {
				});

		return response;
	}

	public static ResponseEntity<PriceDefenitionMst> getPriceDefenitionMstByName(String name) {
		String path = HOST + "pricedefinitionmstbyname?pricename=" + name;
		ResponseEntity<PriceDefenitionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PriceDefenitionMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<BranchWiseProfitReport>> branchProfit(String startDate, String endDate,
			String branchCode) {
		String path = HOST + "salestranshdrreport/profitbybranch/" + branchCode + "?fdate=" + startDate + "&&tdate="
				+ endDate;
		System.out.println(path);
		ResponseEntity<List<BranchWiseProfitReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchWiseProfitReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemMst>> getAllSeriviceItems() {
		String path = HOST + "serviceitemmsts";
		System.out.println(path);
		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<ServiceItemMst> getServiceItemById(String id) {
		String path = HOST + "serviceitemmst/getserviceitemmstbyid/" + id;
		System.out.println(path);
		ResponseEntity<ServiceItemMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ServiceItemMst>() {
				});

		return response;
	}

	public static void updateServiceItemMst(ServiceItemMst serviceItemMst) {
		restTemplate.put(HOST + "serviceitemmst/" + serviceItemMst.getId() + "/updateserviceitemmst", serviceItemMst);
		return;
	}

	public static ResponseEntity<List<JobCardHdr>> JobCardHdrByComanyAndStatus() {
		String path = HOST + "activejobcardhdrs";
		System.out.println(path);
		ResponseEntity<List<JobCardHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JobCardHdr>>() {
				});

		return response;
	}

	public static ArrayList SearchLocation(String searchData) {
		System.out.println(HOST + "serchforlocation?data=" + searchData);
		return restTemplate.getForObject(HOST + "serchforlocation?data=" + searchData, ArrayList.class);

	}

	public static ArrayList SearchServiceItem(String searchData) {
		System.out.println(HOST + "serchforserviceitem?data=" + searchData);
		return restTemplate.getForObject(HOST + "serchforserviceitem?data=" + searchData, ArrayList.class);

	}

	public static ResponseEntity<ServiceItemMst> getServiceItemByNameRequestParam(String itemname) {
		itemname = itemname.replaceAll(" ", "%20");
		String path = HOST + "serviceitemmstbyname?data=" + itemname;

		System.out.println(path);
		ResponseEntity<ServiceItemMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ServiceItemMst>() {
				});

		return response;
	}

	public static ResponseEntity<JobCardDtl> saveJobCardDtl(JobCardDtl jobCardDtl) {
		return restTemplate.postForEntity(HOST + "jobcarddtl/" + jobCardDtl.getJobCardHdr().getId(), jobCardDtl,
				JobCardDtl.class);

	}

	public static ResponseEntity<JobCardHdr> JobCardHdrById(String jobCardId) {
		String path = HOST + "jobcardhdr/" + jobCardId;

		System.out.println(path);
		ResponseEntity<JobCardHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<JobCardHdr>() {
				});

		return response;
	}

	public static ResponseEntity<List<JobCardDtl>> getJobCardDtlByHdrId(String jobCardHdrId) {
		String path = HOST + "jobcarddtlbyhdrid/" + jobCardHdrId;

		System.out.println(path);
		ResponseEntity<List<JobCardDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JobCardDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<JobCardInHdr> getJobCardByVoucherNo(String voucherNo) {
		String path = HOST + "jobcardinhdr/getjobcardinbyvoucherno/" + voucherNo;

		System.out.println(path);
		ResponseEntity<JobCardInHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<JobCardInHdr>() {
				});

		return response;
	}

	public static ResponseEntity<List<JobCardInDtl>> getJobCardInDtlByHdrId(String id) {
		String path = HOST + "jobcardindtl/getjobcardindtlbyhdrid/" + id;

		System.out.println(path);
		ResponseEntity<List<JobCardInDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JobCardInDtl>>() {
				});

		return response;
	}

	public static void deleteJobCardDtlById(String id) {
		restTemplate.delete(HOST + "deletejobcarddtl/" + id);

	}

	public static void updateJobCardHdr(JobCardHdr jobCardHdr) {

		restTemplate.put(HOST + "jobcardhdr/" + jobCardHdr.getId() + "/updatejobcardhdr", jobCardHdr);
		return;
	}

	public static ResponseEntity<List<ServiceInReport>> getServiceInReportBetweenDate(String startDate,
			String endDate) {
		String path = HOST + "serviceindtl/serviceindtlbydate?fdate=" + startDate + "&&tdate=" + endDate;

		System.out.println(path);
		ResponseEntity<List<ServiceInReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ServiceInReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<JobCardHdr>> getJobCardHdrBetweenDate(String startDate, String endDate) {
		String path = HOST + "jobcardhdrresource/jobcardhdrbetweendate?fdate=" + startDate + "&&tdate=" + endDate;

		System.out.println(path);
		ResponseEntity<List<JobCardHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JobCardHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<PriceDefinition> getPriceDefinitionByItemIdAndPriceId(String itemId, String priceId,
			String unitId) {
		String path = HOST + "getpricedefrnition//" + itemId + "/" + priceId + "/" + unitId;

		System.out.println(path);
		ResponseEntity<PriceDefinition> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PriceDefinition>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockValueReports>> getStockValueByCostPrice(String categoryId, String date) {
		String path = HOST + "stocksummarybycostprice/" + categoryId + "/" + SystemSetting.systemBranch + "?fdate="
				+ date;

		System.out.println(path);
		ResponseEntity<List<StockValueReports>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockValueReports>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockValueReports>> getStockValueByStandardPrice(String categoryId, String date) {
		String path = HOST + "stocksummaryallitem/" + categoryId + "/" + SystemSetting.systemBranch + "?fdate=" + date;

		System.out.println(path);
		ResponseEntity<List<StockValueReports>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockValueReports>>() {
				});

		return response;
	}

//	public static ResponseEntity<List<SalesTransHdr>> PrintKot(String salesMan, String tableName) {
//		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/" + salesMan + "/" + tableName
//				+ "/printkotinvoice";
//
//		System.out.println(path);
//		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<SalesTransHdr>>() {
//				});
//
//		return response;
//	}
	public static ResponseEntity<List<SalesTransHdr>> PrintKot(String salesMan, String tableName) {
		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/" + salesMan + "/" + tableName + "/printkot";

		System.out.println(path);
		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesTransHdr>> PrintKotTakeOrder(String hdrid) {
		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/takeorder/printkotinvoice/" + hdrid;

		System.out.println(path);
		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesTransHdr>> PrintKotBySalesTransID(String salesTrqnsId) {
		String path = HOST + SystemSetting.systemBranch + "/salestranshdr/" + salesTrqnsId + "/printkot";

		System.out.println(path);
		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<LocationMst> findLocationMstById(String locationId) {
		String path = HOST + "locationmstbyid/" + locationId;

		System.out.println(path);
		ResponseEntity<LocationMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<LocationMst>() {
				});

		return response;
	}

	public static ResponseEntity<JobCardOutHdr> saveJobCardOutHdr(JobCardOutHdr jobCardOutHdr) {
		return restTemplate.postForEntity(HOST + "jobcardouthdr", jobCardOutHdr, JobCardOutHdr.class);

	}

	public static ResponseEntity<JobCardOutDtl> saveJobCardOutDtl(JobCardOutDtl jobCardOutDtl) {
		return restTemplate.postForEntity(HOST + "jobcardoutdtl/" + jobCardOutDtl.getJobCardOutHdr().getId(),
				jobCardOutDtl, JobCardOutDtl.class);

	}

	public static ResponseEntity<List<JobCardOutDtl>> getJobCardOutDtlByHdrId(String id) {
		String path = HOST + "jobcardoutdtls/" + id;

		System.out.println(path);
		ResponseEntity<List<JobCardOutDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JobCardOutDtl>>() {
				});

		return response;
	}

	public static void deleteJobCardOutDtlById(String id) {
		restTemplate.delete(HOST + "deletejobcardoutdtlbyid/" + id);

	}

	public static void deleteJobCardOutHdrById(String id) {
		restTemplate.delete(HOST + "deletejobcardouthdrbyid/" + id);

	}

	public static void updateJobCardOutHdr(JobCardOutHdr jobCardOutHdr) {
		restTemplate.put(HOST + "jobcardouthdr/" + jobCardOutHdr.getId() + "/updatejobcardouthdr", jobCardOutHdr);
		return;
	}

	public static ResponseEntity<List<DayEndClosureHdr>> DayEndReportByDate(String sdate) {
		String path = HOST + "dayendclosuremst/dayendhdrbydate?voucherdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<DayEndClosureHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DayEndClosureHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesReport>> getSalesSummaryReportDayEnd(String sdate) {
		String path = HOST + "dayendclosurehdr/getdayendreport?voucherdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<SalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<DailyPaymentSummaryReport>> PaymentSummaryDayEnd(String sdate) {
		String path = HOST + "paymentreport/dailypaymentsummary?rdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<DailyPaymentSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailyPaymentSummaryReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<DailyReceiptsSummaryReport>> ReceiptSummaryDayEnd(String sdate) {
		String path = HOST + "receiptdtl/receiptsummaryreport?rdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<DailyReceiptsSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailyReceiptsSummaryReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesModeWiseBillReport>> BillSeriesDayEnd(String sdate) {
		String path = HOST + "allsalesnumericbyvoucherdate?date=" + sdate;

		System.out.println(path);
		ResponseEntity<List<SalesModeWiseBillReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesModeWiseBillReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<KitchenCategoryDtl>> getAllKitchenCategoryDtl() {
		String path = HOST + "kitchencategorydtlresource/findallkitchencategorydtl";

		System.out.println(path);
		ResponseEntity<List<KitchenCategoryDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KitchenCategoryDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ActiveJobCardReport>> getActiveJobCardResport(String fDate, String toDate) {
		String path = HOST + "jobcardhdrresource/activejobcardreport?fdate=" + fDate + "&&tdate=" + toDate;

		System.out.println(path);
		ResponseEntity<List<ActiveJobCardReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ActiveJobCardReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockValueReports>> getItemOrServiceProfit(String fromDate, String toDate,
			String type) {
		String path = HOST + "serviceitemmst/serviceoritemprofit/" + type + "?fdate=" + fromDate + "&&tdate=" + toDate;

		System.out.println(path);
		ResponseEntity<List<StockValueReports>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockValueReports>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SupplierBillwiseDueDayReport>> SupplierDueReport(String startDate,
			String endDate) {
		// TODO Auto-generated method stub
		return null;
	}

	public static ResponseEntity<FinanceMst> saveFinanceComapny(FinanceMst financeCompanyMst) {
		return restTemplate.postForEntity(HOST + "savefinancemst", financeCompanyMst, FinanceMst.class);
	}

	public static ResponseEntity<List<FinanceMst>> findAllFinanceMst() {
		String path = HOST + "findallfinancemst";

		System.out.println(path);
		ResponseEntity<List<FinanceMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FinanceMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesOrderTransHdr>> getPendingSaleOrderTransHdr(String date, String tdate,
			String branchCode) {

		String path = HOST + "saleordertranshdr/getsaleorderbyduedate/" + branchCode + "?fdate=" + date + "&&tdate="
				+ tdate;

		System.out.println(path);
		ResponseEntity<List<SalesOrderTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderTransHdr>>() {
				});
		return response;
	}

	public static ResponseEntity<SalesOrderTransHdr> getSaleOrderTransHdrById(String id) {

		String path = HOST + "getsaleorderbyid/" + id;

		System.out.println(path);
		ResponseEntity<SalesOrderTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesOrderTransHdr>() {
				});
		return response;
	}

	public static ResponseEntity<FinanceMst> findAllFinanceMstByName(String name) {
		String path = HOST + "findallfinancemstbyname?name=" + name;

		System.out.println(path);
		ResponseEntity<FinanceMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<FinanceMst>() {
				});
		return response;
	}

	public static ResponseEntity<List<CardReconcileReport>> getCardReconcileReport(String fromDate, String toDate,
			String type) {
		String path = HOST + "salesreceipts/cardreconcilereport/" + type + "/" + SystemSetting.systemBranch + "?fdate="
				+ fromDate + "&&tdate=" + toDate;

		System.out.println(path);
		ResponseEntity<List<CardReconcileReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CardReconcileReport>>() {
				});

		return response;
	}

	public static ResponseEntity<StoreMst> saveStoreMst(StoreMst storeMst) {
		return restTemplate.postForEntity(HOST + "storemstresource/storemst", storeMst, StoreMst.class);

	}

	public static ResponseEntity<List<StoreMst>> getAllStoreMst() {
		String path = HOST + "storemstresource/storemstbycompanymst";

		System.out.println(path);
		ResponseEntity<List<StoreMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StoreMst>>() {

				});

		return response;
	}

	public static ResponseEntity<List<HsnCodeSaleReport>> getHsnCodeSaleReport(String fromDate, String toDate) {
		String path = HOST + "hsnwisereportresource/hsnwisereport/" + SystemSetting.systemBranch + "?fdate=" + fromDate
				+ "&&tdate=" + toDate;

		System.out.println(path);
		ResponseEntity<List<HsnCodeSaleReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<HsnCodeSaleReport>>() {
				});

		return response;
	}

	public static void deleteStoreMst(String id) {
		restTemplate.delete(HOST + "storemstresource/storemstdelete/" + id);

	}

	public static ResponseEntity<StoreChangeMst> SaveStoreChange(StoreChangeMst storeChangeMst) {

		return restTemplate.postForEntity(HOST + "storechangemstresource/storechangemst", storeChangeMst,
				StoreChangeMst.class);

	}

	public static ResponseEntity<StoreChangeDtl> SaveStoreChangeDtl(StoreChangeDtl storeChangeDtl) {
		return restTemplate.postForEntity(
				HOST + "storechangedtlresource/storechangedtl/" + storeChangeDtl.getStoreChangeMst().getId(),
				storeChangeDtl, StoreChangeDtl.class);

	}

	public static void deleteStoreChangeDtl(String id) {
		restTemplate.delete(HOST + "storechangedtlresource/storechangedtldelete/" + id);

	}

	public static ResponseEntity<List<StoreChangeDtl>> findStoreChangeByHdrId(String id) {
		String path = HOST + "storechangedtlresource/storechangedtlbyhdrid/" + id;

		System.out.println(path);
		ResponseEntity<List<StoreChangeDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StoreChangeDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<HsnCodeSaleReport>> getHsnCodePuchaseReport(String fromDate, String toDate) {
		String path = HOST + "purchasedtl/hsncodepuchasereport/" + SystemSetting.systemBranch + "?fdate=" + fromDate
				+ "&&tdate=" + toDate;

		System.out.println(path);
		ResponseEntity<List<HsnCodeSaleReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<HsnCodeSaleReport>>() {
				});

		return response;
	}

	public static void updateStoreChangeMst(StoreChangeMst storeChangeMst) {
		restTemplate.put(HOST + "storechangemstresource/storechangemstupdate/" + storeChangeMst.getId(),
				storeChangeMst);
		return;
	}

	static public ResponseEntity<IntentItemBranchMst> getIntentItemBranchMstByItemIdByBranch(String itemId,
			String branch) {

		String path = HOST + "intentitembranch/getintentitembranchbyitemid/" + itemId + "/" + branch;

		System.out.println(path);
		ResponseEntity<IntentItemBranchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<IntentItemBranchMst>() {
				});

		return response;

	}

	static public ResponseEntity<IntentItemBranchMst> getIntentItemBranchMstByItemId(String itemId) {

		String path = HOST + "intentitembranch/getintentitembranchbyitemid/" + itemId;

		System.out.println(path);
		ResponseEntity<IntentItemBranchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<IntentItemBranchMst>() {
				});

		return response;

	}

	static public ResponseEntity<IntentItemBranchMst> saveIntentItemBranchMst(IntentItemBranchMst intentItemBranchMst) {

		return restTemplate.postForEntity(HOST + "intentitembranch", intentItemBranchMst, IntentItemBranchMst.class);

	}

	public static ArrayList SearchWatchStrapByName(String searchData) {

		return restTemplate.getForObject(HOST + "watchstrapmstresource/watchstrapmstbycompanymst?data=" + searchData,
				ArrayList.class);

	}

	public static ResponseEntity<WatchStrapMst> findWatchStrapByName(String strap) {
		String path = HOST + "watchstrapmstresource/watchmstbyname?strapname=" + strap;
		ResponseEntity<WatchStrapMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WatchStrapMst>() {
				});

		return response;
	}

	public static ResponseEntity<WatchStrapMst> SaveWatchStrapMst(WatchStrapMst watchStrapMst) {
		return restTemplate.postForEntity(HOST + "watchstrapmstresource/savewatchsatrapmst", watchStrapMst,
				WatchStrapMst.class);

	}

	static public ResponseEntity<PrinterMst> savePrinterMst(PrinterMst printerMst) {

		return restTemplate.postForEntity(HOST + "printermstresource/printermst", printerMst, PrinterMst.class);

	}

	static public ResponseEntity<KotCategoryMst> saveKotCategoryMst(KotCategoryMst kotCategoryMst) {

		return restTemplate.postForEntity(HOST + "kotcategorymstresource/kotcategorymst", kotCategoryMst,
				KotCategoryMst.class);

	}

	static public ResponseEntity<List<KotCategoryMst>> getAllKotCategoryMst() {

		String path = HOST + "kotcategorymstresource/findallkotcategorymst";

		ResponseEntity<List<KotCategoryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KotCategoryMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<PrinterMst>> getAllPrinterMst() {

		String path = HOST + "printermstresource/findallprintermst";

		ResponseEntity<List<PrinterMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PrinterMst>>() {
				});

		return response;

	}

	static public ResponseEntity<PrinterMst> getPrinterByName(String name) {

		String path = HOST + "printermstresource/fetchbyprintername/" + name;

		System.out.println("========path==========" + path);

		// return restTemplate.getForEntity(path, List<CategoryMst>.class);

		ResponseEntity<PrinterMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PrinterMst>() {
				});

		return response;

	}

	static public ResponseEntity<PrinterMst> getPrinterById(String id) {

		String path = HOST + "printermstresource/fetchbyprinterid/" + id;

		System.out.println("========path==========" + path);

		// return restTemplate.getForEntity(path, List<CategoryMst>.class);

		ResponseEntity<PrinterMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PrinterMst>() {
				});

		return response;

	}

	static public ResponseEntity<KotCategoryMst> getKotCategoryMstByCatId(String id) {

		String path = HOST + "kotcategorymstresource/fetchbycategoryid/" + id;

		System.out.println("========path==========" + path);

		// return restTemplate.getForEntity(path, List<CategoryMst>.class);

		ResponseEntity<KotCategoryMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<KotCategoryMst>() {
				});

		return response;

	}

	static public ResponseEntity<KotCategoryMst> getKotCategoryMstByCatIdAndPrinterId(String id, String printId) {

		String path = HOST + "kotcategorymstresource/fetchbycategoryidandprinterid/" + id + "/" + printId;

		System.out.println("========path==========" + path);

		// return restTemplate.getForEntity(path, List<CategoryMst>.class);

		ResponseEntity<KotCategoryMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<KotCategoryMst>() {
				});

		return response;

	}

	public static ArrayList SearchSaleOrederByVoucherNo(String searchData) {
		System.out.println(HOST + "searchsaleorderbyvoucherno?data=" + searchData);
		return restTemplate.getForObject(HOST + "searchsaleorderbyvoucherno?data=" + searchData, ArrayList.class);
	}

	/*
	 * Below url is same as SearchSaleOrederByVoucherNo but reurn local customer
	 * name instead of ID
	 */
	public static ArrayList SearchSaleOrederByVoucherNoCustomerName(String searchData) {
		System.out.println(HOST + "searchsaleorderbyvouchernocname?data=" + searchData);
		return restTemplate.getForObject(HOST + "searchsaleorderbyvouchernocname?data=" + searchData, ArrayList.class);
	}

	static public Double getConsumptionTotalAmount(String hdrid) {

		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "consumptionhdr/getsumofamount/" + hdrid, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double getInvoiceTotalSales(String date) {

		System.out.println(
				HOST + "dailysalessummary/sumofsales/" + SystemSetting.getSystemBranch() + "?reportdate=" + date);

		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "dailysalessummary/sumofsales/" + SystemSetting.getSystemBranch() + "?reportdate=" + date,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double getCreditSaleAdjstmnt(String branchCode, String date) {

		System.out.println(HOST + "dayendreportresource/creditsaleadjustment/" + branchCode + "?reportdate=" + date);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "dayendreportresource/creditsaleadjustment/" + branchCode + "?reportdate=" + date, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	//
	static public Double getConsumptionTotalQtyByItem(String hdrid, String itemId, String batch) {
		System.out.println(HOST + "consumptionhdr/getsumofqty/" + hdrid + "/" + itemId + "/" + batch);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate
				.getForObject(HOST + "consumptionhdr/getsumofqty/" + hdrid + "/" + itemId + "/" + batch, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	static public Double getSaleAmountByAccount(String accountId, String fdate) {
		System.out.println("Items" + HOST + "dailybussinessummary/" + SystemSetting.systemBranch + "/" + accountId
				+ "/sumofonlinesales?reportdate=" + fdate);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "dailybussinessummary/" + SystemSetting.systemBranch + "/"
				+ accountId + "/sumofonlinesales?reportdate=" + fdate, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	public static ResponseEntity<SessionEndClosureMst> findLastUpdatedSessionByDate(String sdate) {
		// TODO Auto-generated method stub
		return null;
	}

	public static ResponseEntity<SessionEndClosureMst> saveSessionEndMst(SessionEndClosureMst sessionEndClosureMst) {
		return restTemplate.postForEntity(HOST + "sessionendclosuremst", sessionEndClosureMst,
				SessionEndClosureMst.class);

	}

	public static ResponseEntity<SessionEndDtl> saveSessionDtl(SessionEndDtl sessionEndDtl) {
		return restTemplate.postForEntity(
				HOST + "sessionendclosurehdr/" + sessionEndDtl.getSessionEndClosure().getId() + "/sessionendclosuredtl",
				sessionEndDtl, SessionEndDtl.class);

	}

	public static ResponseEntity<List<SessionEndDtl>> findSessionEndDtlByHdrId(String id) {
		String path = HOST + "sessiondtl/" + id;
		ResponseEntity<List<SessionEndDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SessionEndDtl>>() {
				});

		return response;
	}

	public static void updateSessinEndMst(SessionEndClosureMst sessionEndClosureMst) {

		restTemplate.put(HOST + "sessionendclosurehdr/finalsave/" + sessionEndClosureMst.getId(), sessionEndClosureMst);
		return;

	}

	public static String updateInvoiceEditEnableMst(String voucherDate, String voucherNumber) {

		restTemplate.put(HOST + "invoiceeditenablemst/" + voucherNumber + "/" + voucherDate + "/updatestatus",
				String.class);
		return "Success";

	}

	public static Integer findLastUpdatedSessionSlNoByDate(String sdate) {

		System.out.println(HOST + "sessionendclosuremstresource/maxofsessionslno?processdate=" + sdate);
		return restTemplate.getForObject(HOST + "sessionendclosuremstresource/maxofsessionslno?processdate=" + sdate,
				Integer.class);
	}

	public static Double findTotalAmountOfSessionEndByHdrId(String id) {
		return restTemplate.getForObject(HOST + "totalsessionendphysicalcash/" + id, Double.class);
	}

	public static ResponseEntity<SessionEndClosureMst> findSessionEndMstById(String id) {
		String path = HOST + "sessionendclosuremstbyid/" + id;
		ResponseEntity<SessionEndClosureMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SessionEndClosureMst>() {
				});

		return response;
	}

	public static ResponseEntity<SessionEndClosureMst> findSessionEndClosureMstBySlNoAndDate(String slNo, String date) {
		String path = HOST + "sessionendclosuremstbyslnoanddate/" + slNo + "?date=" + date;
		ResponseEntity<SessionEndClosureMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SessionEndClosureMst>() {
				});

		return response;
	}

	static public ResponseEntity<List<ReceiptModeReport>> getReceiptModeReport(String fdate) {

		String path = HOST + "dailysalessummary/" + SystemSetting.systemBranch + "/sumofreceiptmode?reportdate="
				+ fdate;

		System.out.println(path);
		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReceiptModeReport>> getReceiptModeReportByMode(String fdate, String mode) {

		String path = HOST + "dailysalessummary/" + SystemSetting.systemBranch + "/sumofreceiptmodebymode/" + mode
				+ "?reportdate=" + fdate;

		System.out.println(path);
		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ReceiptModeReport>> getReceiptModeReportByAccID(String fdate, String accid) {

		String path = HOST + "dailysalessummary/" + SystemSetting.systemBranch + "/sumofreceiptmodebyaccountid/" + accid
				+ "?reportdate=" + fdate;

		System.out.println(path);
		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});

		return response;

	}

	// {companymstid}/dailysalesreportresourec/previousadvanceamount/{branchcode}
	static public ResponseEntity<Double> getPreviousAdvanceAmount(String strDate, String receiptMode) {

		String path = HOST + "dailysalesreportresourec/previousadvanceamount/" + SystemSetting.systemBranch + "?rdate="
				+ strDate + "&receiptmode=" + receiptMode;

		System.out.println(path);
		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public ResponseEntity<List<SaleOrderReceiptReport>> getPreviousAdvanceAmountDtl(String strDate,
			String receiptMode) {

		String path = HOST + "dailysalesreportresourec/previousadvanceamountpopup/" + SystemSetting.systemBranch
				+ "?rdate=" + strDate + "&receiptmode=" + receiptMode;

		System.out.println(path);
		ResponseEntity<List<SaleOrderReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceiptReport>>() {
				});

		return response;

	}

	static public ResponseEntity<Double> getPreviousAdvanceAmountCard(String strDate) {

		String path = HOST + "dailysalesreportresourec/previousadvanceamountcard/" + SystemSetting.systemBranch
				+ "?rdate=" + strDate;

		System.out.println(path);
		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	static public ResponseEntity<Double> getPreviousAdvanceAmountCardRelized(String strDate, String cardMode) {

		String path = HOST + "dailysalesreportresourec/previousadvanceamountcardrealized/" + SystemSetting.systemBranch
				+ "?rdate=" + strDate + "&cardMode=" + cardMode;

		System.out.println(path);
		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});

		return response;

	}

	public static ArrayList SearchWatchCompliantByName(String searchData) {

		System.out.println(HOST + "watchcomplaintmst?data=" + searchData);
		return restTemplate.getForObject(HOST + "watchcomplaintmst?data=" + searchData, ArrayList.class);

	}

	public static ResponseEntity<WatchComplaintMst> SearchWatchComplaintByName(String name) {
		String path = HOST + "watchcomplaintmst/complaintbyname?name=" + name;
		ResponseEntity<WatchComplaintMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WatchComplaintMst>() {
				});

		return response;
	}

	public static ResponseEntity<WatchComplaintMst> saveWatchCompliant(WatchComplaintMst watchComplaintMst) {
		return restTemplate.postForEntity(HOST + "watchcomplaintmst/savewatchcomplaintmst", watchComplaintMst,
				WatchComplaintMst.class);

	}

	public static ResponseEntity<WatchObservationMst> SearchWatchObseravtionByName(String name) {
		String path = HOST + "watchobservationmst/observationbyname?name=" + name;
		ResponseEntity<WatchObservationMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WatchObservationMst>() {
				});

		return response;
	}

	public static ResponseEntity<WatchObservationMst> saveWatchObservation(WatchObservationMst watchObservationMst) {
		return restTemplate.postForEntity(HOST + "watchobservationmst/savewatchobservationmst", watchObservationMst,
				WatchObservationMst.class);

	}

	public static ArrayList SearchWatchObservationByName(String searchData) {
		System.out.println(HOST + "watchobservationmst?data=" + searchData);
		return restTemplate.getForObject(HOST + "watchobservationmst?data=" + searchData, ArrayList.class);
	}

	public static ResponseEntity<ServiceInReceipt> saveServiceInReceipt(ServiceInReceipt serviceInReceipt) {
		return restTemplate.postForEntity(
				HOST + "serviceinreceiptresource/saveserviceinreceipt/" + serviceInReceipt.getServiceInHdr().getId(),
				serviceInReceipt, ServiceInReceipt.class);

	}

	public static Double findTotalAmountOfServiceInReceipts(String id) {
		return restTemplate.getForObject(HOST + "serviceinreceiptresource/saveserviceinreceipt/" + id, Double.class);

	}

	public static void deleteServiceInReceipt(String id) {
		restTemplate.delete(HOST + "serviceinreceiptresource/deleteserviceinreceipt/" + id);

	}

	public static ResponseEntity<List<ServiceInReceipt>> getAllServiceInReceiptByHdrId(String id) {
		String path = HOST + "serviceinreceiptresource/serviceinreceiptbyserviceinhdr/" + id;
		System.out.println(path);
		ResponseEntity<List<ServiceInReceipt>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ServiceInReceipt>>() {
				});

		return response;
	}

	public static ResponseEntity<ServiceInHdr> getServiceInHdrById(String id) {
		String path = HOST + "getserviceinhdrbyid/" + id;
		System.out.println(path);
		ResponseEntity<ServiceInHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ServiceInHdr>() {
				});

		return response;
	}

	public static Double getInvoiceAmountByJobCardHdrId(String id) {

		System.out.println(HOST + "jobcardhdrresource/invoiceamount/" + id);
		return restTemplate.getForObject(HOST + "jobcardhdrresource/invoiceamount/" + id, Double.class);
	}

	public static Double JobCardDtlItemQty(String jobCardHdrId, String itemId) {
		System.out.println(HOST + "jobcarddtlresource/itemqty/" + jobCardHdrId + "/" + itemId);
		return restTemplate.getForObject(HOST + "jobcarddtlresource/itemqty/" + jobCardHdrId + "/" + itemId,
				Double.class);
	}

	public static ResponseEntity<List<SalesInvoiceReport>> getSalesAnalysisReport(String startDate, String endDate) {
		String path = HOST + "salestranshdrreport/salesanalysisreport?fdate=" + startDate + "&&tdate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<SalesInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesInvoiceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<SalesAnalysis> saveSalesAnalysis(SalesAnalysis salesAnalysis) {
		return restTemplate.postForEntity(HOST + "salesanalysis", salesAnalysis, SalesAnalysis.class);

	}

	static public ResponseEntity<ItemImageMst> saveItemIamgeMst(ItemImageMst itemImageMst) {

		return restTemplate.postForEntity(HOST + "itemimagemst/", itemImageMst, ItemImageMst.class);

	}

	static public ResponseEntity<List<ItemImageMst>> getimagebyitem(String itemId) {

		String path = HOST + "getimagebyitem/" + itemId + "/";

		ResponseEntity<List<ItemImageMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemImageMst>>() {
				});

		return response;

	}

//	public static ResponseEntity<CustomerMst> getCustomerByNameAndBarchCode(String custName, String branchCode) {
//		String path = HOST + "customermstbynameandbrachcode/" + custName + "/" + branchCode;
//		System.out.println(path);
//
//		ResponseEntity<CustomerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<CustomerMst>() {
//				});
//		return response;
//	}

	public static ResponseEntity<List<ReceiptModeReport>> getSaleOrderReceiptModeReport(String strDate) {
		String path = HOST + "dailysalessummary/sumofsaleorderreceiptmodebydate?reportdate=" + strDate;
		System.out.println(path);
		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ReceiptModeReport>> getSaleOrderReceiptModeReportByMode(String strDate,
			String mode) {
		String path = HOST + "dailysalessummary/sumofsaleorderreceiptmodebydatebymode/" + mode + "?reportdate="
				+ strDate;
		System.out.println(path);
		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<FinishedProduct>> findPlaningAndActualProductionReport(String startDate,
			String endDate) {
		String path = HOST + "actualproductionreportresource/actualproductionbetweendate/" + SystemSetting.systemBranch
				+ "?fdate=" + startDate + "&&tdate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<FinishedProduct>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FinishedProduct>>() {
				});

		return response;
	}

	public static ResponseEntity<String> uploadImage(File file, ItemMst item) throws IOException {

		MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
		bodyMap.add("file", new FileSystemResource(file));
		bodyMap.add("itemid", item);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(HOST + "uploadimage", HttpMethod.POST, requestEntity,
				String.class);

		System.out.println("response status: " + response.getStatusCode());
		System.out.println("response body: " + response.getBody());

		return response;
	}

	public static Map<String, Double> getRequiredMaterial(String rawMaterialItemId, Double qty) {
		System.out.println(HOST + "itembatchmst/" + rawMaterialItemId + "/" + qty);
		return restTemplate.getForObject(HOST + "itembatchmst/" + rawMaterialItemId + "/" + qty, Map.class);

	}

	public static ResponseEntity<List<LmsQueueTallyMst>> TallyByVoucherTypeAndDate(String sdate, String selectedItem) {
		String path = HOST + "tallyrsource/tallymstbydateandvouchertype/" + selectedItem + "?date=" + sdate;
		System.out.println(path);
		ResponseEntity<List<LmsQueueTallyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LmsQueueTallyMst>>() {
				});

		return response;
	}

	public static String SalesToTallyIntegration(String str, String sdate) {
		return restTemplate.getForObject(HOST + "salestotallybyvouchernoanddate/" + str + "?date=" + sdate,
				String.class);
	}

	public static String PurchaseToTallyIntegration(String str, String sdate) {
		return restTemplate.getForObject(HOST + "purchasetotallybyvouchernoanddate/" + str + "?date=" + sdate,
				String.class);
	}

	public static ResponseEntity<List<SaleOrderReport>> getSaleOrderTakeAwayReport(String startDate) {
		String path = HOST + "saleorderreportresource/saleordertakeawayreport?date=" + startDate;
		System.out.println(path);
		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<LmsQueueMst>> findAllLmsQueueMstByVoucherTypeBetweenDate(String sdate,
			String enddate, String string) {
		String path = HOST + "lmsqueuemstbydateandtype/" + string + "?fdate=" + sdate + "&&tdate=" + enddate;
		System.out.println(path);
		ResponseEntity<List<LmsQueueMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LmsQueueMst>>() {
				});

		return response;
	}

	public static ResponseEntity<LmsQueueMst> findLmsQueueMstById(String str) {
		String path = HOST + "lmsqueuemstbyid/" + str;
		System.out.println(path);
		ResponseEntity<LmsQueueMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<LmsQueueMst>() {
				});

		return response;
	}

	public static void updateLmsQueueMst(LmsQueueMst lmsQueueMst) {
		restTemplate.put(HOST + "lmsqueue/" + lmsQueueMst.getId() + "/updatelmsqueue", lmsQueueMst);
		return;
	}

	// ----------------------version 1.5

	public static ResponseEntity<List<SalesModeWiseBillReport>> BillSeriesDayEndByInvoiceSeries(String sdate) {
		String path = HOST + "allbillseriesbyvoucherdate?date=" + sdate;

		System.out.println(path);
		ResponseEntity<List<SalesModeWiseBillReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesModeWiseBillReport>>() {
				});

		return response;
	}

	// ------------------version 1.5 end

	public static ResponseEntity<VoucherNumberDtlReport> getInvoiceNumberStatistics(String branchcode, String sdate) {
		String path = HOST + "dayendreportresource/" + branchcode + "/vouchernuberdtlreport?reportdate=" + sdate;

		System.out.println(path);
		ResponseEntity<VoucherNumberDtlReport> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<VoucherNumberDtlReport>() {
				});

		return response;
	}

	// version1.10

	static public ResponseEntity<FinancialYearMst> getFinancialYear(String tdate) {

		String path = HOST + "financialyearmst/getfinanicialyear?date=" + tdate;

		System.out.println(path);

		ResponseEntity<FinancialYearMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<FinancialYearMst>() {
				});

		return response;
	}
	// version1.10ends

	public static ResponseEntity<List<FinancialYearMst>> cheackFinancialYearDuplication(String sdate, String edate) {
		String path = HOST + "financialyearmst/cheackfinancialyearduplication?startdate=" + sdate + "&&enddate="
				+ edate;
		System.out.println(path);
		ResponseEntity<List<FinancialYearMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FinancialYearMst>>() {
				});

		return response;
	}

	// version2.0

	public static ResponseEntity<FinancialYearMst> saveFinancialYearMst(FinancialYearMst financialYearMst) {
		return restTemplate.postForEntity(HOST + "financialyearmst", financialYearMst, FinancialYearMst.class);
	}

	static public ResponseEntity<List<FinancialYearMst>> getAllFinancialYear() {

		String path = HOST + "financialyearmst";

		System.out.println(path);

		ResponseEntity<List<FinancialYearMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<FinancialYearMst>>() {
				});

		return response;
	}

	static public Integer getFinancialYearCount() {

		Integer rtnValue = 0;
		Integer rtnCount = restTemplate.getForObject(HOST + "financialyearmst/getfinanicialyearcount", Integer.class);

		if (null != rtnCount) {
			rtnValue = rtnCount;
		}
		return rtnValue;
	}

	// version2.0ends

	// version3.5sari
	static public String updateCustDiscount() {
		return restTemplate.getForObject(HOST + "accountheadsresource/setdiscount", String.class);

	}
	// version3.5end

	// version3.6sari

	public static ResponseEntity<List<KitDefinitionMst>> getAllKit() {
		String path = HOST + "kitdefinitionmst/findallkitname/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<KitDefinitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KitDefinitionMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<KitDefinitionMst>> getAllKitByName(String data) {
		String path = HOST + "kitdefinitionmst/findallkitnamebyname/" + SystemSetting.systemBranch + "?data=" + data;
		System.out.println(path);
		ResponseEntity<List<KitDefinitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KitDefinitionMst>>() {
				});

		return response;
	}

	// version3.6nds
	static public ResponseEntity<TableOccupiedMst> saveTableOccupiedMst(TableOccupiedMst tableOccupiedMst) {

		return restTemplate.postForEntity(HOST + "tableoccuiedmst", tableOccupiedMst, TableOccupiedMst.class);

	}

	static public ResponseEntity<TableOccupiedMst> getTableOccupiedMstbyTable(String tableid) {

		String path = HOST + "tableoccupiedmst/gettableoccupiedmstbytable/" + tableid;
		ResponseEntity<TableOccupiedMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<TableOccupiedMst>() {
				});

		return response;

	}

	public static void updateTableOccupiedMst(TableOccupiedMst tableOccupiedMst) {
		restTemplate.put(HOST + "tableoccuiedmst/updatetableoccupiedmst", tableOccupiedMst);
		return;
	}

	public static ArrayList getReorderExportToExcel(String strDate) {
		String path = HOST + "intenthdrresource/intentdtlbydate/" + SystemSetting.systemBranch + "?date=" + strDate;
		System.out.println(path);
		return restTemplate.getForObject(path, ArrayList.class);

	}

	public static ResponseEntity<List<SaleOrderReceiptReport>> getSaleOrderReceiptByModeAndDate(String sdate,
			String mode) {
		String path = HOST + "saleorderreceipt/getsaleorderreceiptbymodeanddate/" + mode + "?rdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<SaleOrderReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceiptReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderReceiptReport>> getSaleOrderReceiptByCard(String sdate) {
		String path = HOST + "saleorderreceipt/getsaleorderreceiptcard?rdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<SaleOrderReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceiptReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderReceiptReport>> getSaleOrderReceiptPreviousCard(String sdate) {
		String path = HOST + "saleorderreceipt/getsaleorderreceiptpreviouscard?rdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<SaleOrderReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceiptReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderReceiptReport>> getSaleReceiptByModeAndDate(String sdate, String mode) {
		String path = HOST + "salesreceipts/getsalesreceiptbymodeanddate/" + mode + "/"
				+ SystemSetting.getSystemBranch() + "?fdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<SaleOrderReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceiptReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderReceiptReport>> getSaleReceiptByCard(String sdate) {
		String path = HOST + "salesreceipts/getsalesreceiptbycard/" + SystemSetting.getSystemBranch() + "?fdate="
				+ sdate;

		System.out.println(path);
		ResponseEntity<List<SaleOrderReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceiptReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderReceiptReport>> getSaleReceiptByAccidAndDate(String sdate, String mode) {
		String path = HOST + "salesreceipts/getsalesreceiptbyaccidanddate/" + mode + "/"
				+ SystemSetting.getSystemBranch() + "?fdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<SaleOrderReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceiptReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderReport>> DeliveryBoyPendingReport(String sdate, String id) {
		String path = HOST + "saleorderreportresource/saleordertakeawayreport/" + id + "/" + SystemSetting.systemBranch
				+ "?date=" + sdate;

		System.out.println(path);

		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});

		return response;

	}

	public static ResponseEntity<List<ItemMergeMst>> getAllItemMergeMst() {
		String path = HOST + "itemmstresource/getallitemmergemst";

		System.out.println(path);

		ResponseEntity<List<ItemMergeMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMergeMst>>() {
				});

		return response;
	}

	public static ArrayList getSundryDebitorBalance(String sdate) {

		System.out.println(HOST + "sundrydebitorbalance?startdate=" + sdate);
		return restTemplate.getForObject(HOST + "sundrydebitorbalance?startdate=" + sdate, ArrayList.class);

	}

	public static void updateAccountHeads(AccountHeads accountHeads) {
		restTemplate.put(HOST + "updateaccountheads/" + accountHeads.getId(), accountHeads);
		return;
	}

	public static void updateStockTransfer(StockTransferOutHdr stockTransferOutHdr) {
		restTemplate.put(HOST + "stocktransferouthdr/" + stockTransferOutHdr.getId() + "/updatestocktransfer",
				stockTransferOutHdr);
		return;
	}

	public static void updateStockTransferSlNo(String hdrid) {
		restTemplate.getForObject(HOST + "stocktransferoutdtl/updatestocktransfer/" + hdrid, String.class);
		return;
	}

	public static void updateUrsMst(UrsMst ursMst) {
		restTemplate.put(HOST + "ursmstresource/ursmstupdate/" + ursMst.getId(), ursMst);
		return;
	}

	public static ResponseEntity<DayEndReportStore> saveDayEndReportStore(DayEndReportStore dayEndReportStore) {
		System.out.println("-----------------savedayendreportstore----------------");

		return restTemplate.postForEntity(HOST + "savedayendreportstore", dayEndReportStore, DayEndReportStore.class);

	}

	public static ResponseEntity<DayEndReportStore> getDayEndReportFromStore(String sdate) {
		String path = HOST + "dayendreportstorebydate/" + SystemSetting.systemBranch + "?reportdate=" + sdate;

		System.out.println(path);

		ResponseEntity<DayEndReportStore> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DayEndReportStore>() {
				});

		return response;
	}
	// ------------ version 4.8

	public static ResponseEntity<List<SaleOrderReport>> getDailyCakeSetting(String categoryid, String sdate) {

		String path = HOST + "saleorderreportresource/dailycakesetting/" + categoryid + "/" + SystemSetting.systemBranch
				+ "?rdate=" + sdate;

		System.out.println(path);

		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});

		return response;
	}

	// ------------ version 4.8 end

	// -------------version 4.9

	public static ResponseEntity<List<SaleOrderReport>> SaleOrderHomeDeliveryReport(String sdate) {
		String path = HOST + "saleorderreportresource/homedeliveryreport/" + SystemSetting.systemBranch + "?rdate="
				+ sdate;

		System.out.println(path);

		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});

		return response;
	}

	// -----------------version 4.9 end
	// -----------------version 4.10
	public static ResponseEntity<List<SaleOrderReport>> getOrderSummary(String sdate) {
		String path = HOST + "saleorderreportresource/dailyordersummury/" + SystemSetting.systemBranch + "?rdate="
				+ sdate;

		System.out.println(path);

		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});

		return response;
	}

	// -----------------version 4.10 end
	public static ResponseEntity<List<SalesTaxSplitReport>> getSalesSplit(String sdate) {
		String path = HOST + "salestranshdrreport/taxsplit/" + SystemSetting.systemBranch + "?tdate=" + sdate;

		System.out.println(path);

		ResponseEntity<List<SalesTaxSplitReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTaxSplitReport>>() {
				});

		return response;
	}
//				public static ResponseEntity<SubBranchMst> SaveSubbranchCreation(SubBranchMst branchcreation) {
//					System.out.println(HOST + "subbranchmst");
//					
//					return restTemplate.postForEntity(HOST + "subbranchmst", branchcreation, SubBranchMst.class);
//
//				}
//
//				public static ResponseEntity<List<SubBranchMst>> getAllSubbranches() {
//					String path = HOST + "subbranches";
//
//					System.out.println(path);
//
//					ResponseEntity<List<SubBranchMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//							new ParameterizedTypeReference<List<SubBranchMst>>() {
//							});
//
//					return response;
//				}

	public static ResponseEntity<List<SubBranchSalesDtl>> findSpneserDtlBySpenserIdAndDate(String branchCode,
			String sdate, String edate) {

		String path = HOST + "spencersalesdtl/spensersalesdtlbybranchcode/" + branchCode + "?fdate=" + sdate
				+ "&&tdate=" + edate;

		System.out.println(path);

		ResponseEntity<List<SubBranchSalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SubBranchSalesDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<SubBranchSalesTransHdr> saveSpencerSalesTransHdr(
			SubBranchSalesTransHdr spencerSalesTransHdr) {
		System.out.println(HOST + "savespencersalestranshdr");
		return restTemplate.postForEntity(HOST + "savespencersalestranshdr", spencerSalesTransHdr,
				SubBranchSalesTransHdr.class);

	}

	public static ResponseEntity<SubBranchSalesDtl> saveSpencerSalesDtl(SubBranchSalesDtl spencerSalesDtl) {
		System.out.println(HOST + "spencersalesdtl/" + spencerSalesDtl.getSubBranchSalesTransHdr().getId());

		return restTemplate.postForEntity(
				HOST + "spencersalesdtl/" + spencerSalesDtl.getSubBranchSalesTransHdr().getId(), spencerSalesDtl,
				SubBranchSalesDtl.class);

	}

	public static void updateSpencerSalesTranshdr(SalesTransHdr salesTransHdr) {

		String date = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-yyyy");

		restTemplate.put(HOST + "spencersalestranshdrfinalsave/" + salesTransHdr.getId() + "?logdate=" + date,
				salesTransHdr);
		return;

	}

	public static ResponseEntity<DailyReceiptsSummaryReport> TotalReceiptSummaryByReceiptMode(String sdate,
			String receiptMode) {
		String path = HOST + "receiptdtl/receiptsummaryreportbyreceiptmode/" + receiptMode + "?rdate=" + sdate;
		System.out.println(path);
		ResponseEntity<DailyReceiptsSummaryReport> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DailyReceiptsSummaryReport>() {
				});

		return response;
	}

	public static ResponseEntity<DailyPaymentSummaryReport> TotalPaymentSummaryByPaymentMode(String sdate,
			String paymentMode) {
		String path = HOST + "paymentreport/dailypaymentsummarybyreceiptmode/" + paymentMode + "?rdate=" + sdate;
		System.out.println(path);
		ResponseEntity<DailyPaymentSummaryReport> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DailyPaymentSummaryReport>() {
				});

		return response;
	}

	public static ResponseEntity<List<JournalDtl>> getJournalReportPrint(String sdate, String eDate,
			String branchCode) {
		String path = HOST + "journalreportresource/journaljasperreport/" + branchCode + "?fromdate=" + sdate
				+ "&&todate=" + sdate;
		System.out.println(path);
		ResponseEntity<List<JournalDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JournalDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<AccountBalanceReport>> getBankAccountStatement(String sdate, String branchCode) {
		String path = HOST + "accountingresource/bankaccountstatement/" + branchCode + "?startdate=" + sdate
				+ "&&enddate=" + sdate;
		System.out.println(path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<MixCategoryMst> findByResoureCatName(String searchData) {
		System.out.println("subcategorymstresource/findallsubcategorymsts/" + searchData);

		String path = HOST + "subcategorymstresource/findallsubcategorymsts/" + searchData;

		System.out.println(path);
		ResponseEntity<MixCategoryMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MixCategoryMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<SubBranchSalesDtl>> findSpneserDtlAndDate(String sdate, String edate) {

		String path = HOST + "spencersalesdtl/spensersalesdtlbydate" + "?fdate=" + sdate + "&&tdate=" + edate;

		System.out.println(path);

		ResponseEntity<List<SubBranchSalesDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SubBranchSalesDtl>>() {
				});

		return response;

	}
	// ------------version 4.18 end

	public static ResponseEntity<List<DailyReceiptsSummaryReport>> getreceiptSummary(String sdate) {
		String path = HOST + "receiptdtl/accountwisereceiptsummaryreport/" + SystemSetting.systemBranch + "?rdate="
				+ sdate;

		System.out.println(path);

		ResponseEntity<List<DailyReceiptsSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailyReceiptsSummaryReport>>() {
				});

		return response;

	}
	// ------------version 5.0 surya

	public static String StockVerificationBySalesTransHdr(String id) {

		String path = HOST + "stockverification/" + id;
		System.out.println(path);
		return restTemplate.getForObject(HOST + "stockverification/" + id, String.class);

	}

//	public static String StockVerificationBySalesOrderTransHdr(String id) {
//
//		String path = HOST + "stockverification/" + id;
//		System.out.println(path);
//		return restTemplate.getForObject(HOST + "stockverification/" + id, String.class);
//
//	}
	public static String StockVerificationByProductConversion(String id) {

		String path = HOST + "itembatchdtldaily/stockverificationforproductconversion/" + id + "/"
				+ SystemSetting.systemBranch;
		System.out.println(path);
		return restTemplate.getForObject(HOST + "itembatchdtldaily/stockverificationforproductconversion/" + id + "/"
				+ SystemSetting.systemBranch, String.class);

	}

	public static String StockVerificationByStockTransfer(String id) {

		String path = HOST + "stockverification/" + id;
		System.out.println(path);
		return restTemplate.getForObject(HOST + "itembatchdtldaily/stockverificationforstocktransfer/" + id,
				String.class);

	}

	// ================stock verification from itembatchdtl========anandu===========
	public static String StockVerificationByStockTransferFromItmBatchDtl(String id) {

		String path = HOST + "itembatchdtlresource/stockverificationforstocktransfer/" + id;
		System.out.println(path);
		return restTemplate.getForObject(HOST + "itembatchdtlresource/stockverificationforstocktransfer/" + id,
				String.class);

	}
	// ====================end stock verification ======================
	
	public static String StockVerificationByDamageEntry(String id) {

		String path = HOST + "stockverification/" + id;
		System.out.println(path);
		return restTemplate.getForObject(HOST + "itembatchdtldaily/stockverificationfordamagentry/" + id, String.class);

	}
	// ------------version 5.0 surya end
	// --------------------version 5.9 surya

	// Code corrected by Regy on Dec 7, 2020.
	// Other Branch Sales was calling other branch purchase !!.

	public static ResponseEntity<OtherBranchSalesTransHdr> saveOtherBranchSalesTransHdr(
			OtherBranchSalesTransHdr otherBranchSalesTransHdr) {
		return restTemplate.postForEntity(HOST + "saveotherbranchsalestranshdr", otherBranchSalesTransHdr,
				OtherBranchSalesTransHdr.class);

	}

	/*
	 * Following code to be corrected. Othjer Branch Sales is calling Otherbranch
	 * Purchase !!
	 */
	/*
	 * public static ResponseEntity<OtherBranchSalesTransHdr>
	 * saveOtherBranchSalesTransHdr( OtherBranchSalesTransHdr
	 * otherBranchSalesTransHdr) { return restTemplate.postForEntity(HOST +
	 * "otherbranchpurchasehdr/saveotherbranchpurchasehdr",
	 * otherBranchSalesTransHdr, OtherBranchSalesTransHdr.class);
	 * 
	 * }
	 */

	public static ResponseEntity<OtherBranchSalesDtl> saveOtherBranchSalesDtl(OtherBranchSalesDtl otherBranchSalesDtl) {

		System.out.println("==path==" + HOST + "saveotherbranchsalesdtl/"
				+ otherBranchSalesDtl.getOtherBranchSalesTransHdr().getId());
		return restTemplate.postForEntity(
				HOST + "saveotherbranchsalesdtl/" + otherBranchSalesDtl.getOtherBranchSalesTransHdr().getId(),
				otherBranchSalesDtl, OtherBranchSalesDtl.class);

	}

	public static void updateOtherBranchSalesTransHdr(OtherBranchSalesTransHdr otherBranchSalesTransHdr) {
		restTemplate.put(HOST + "otherbranchsalestranshdrfinalsave/" + otherBranchSalesTransHdr.getId(),
				otherBranchSalesTransHdr);
		return;
	}

	public static Summary getOtherBranchSalesWindowSummaryDiscount(String id) {
		String path = HOST + "otherbranchsalesdtl/" + id + "/saleswindowsummaryfordiscount";
		System.out.println(path);
		return restTemplate.getForObject(path, Summary.class);
	}

	public static Summary getOtherBranchSalesWindowSummary(String id) {
		String path = HOST + "otherbranchsalesdtl/" + id + "/saleswindowsummary";
		System.out.println(path);
		return restTemplate.getForObject(path, Summary.class);
	}

	// --------------------version 5.9 surya end
	public static ResponseEntity<List<SalesInvoiceReport>> getOtherBranchInvoice(String voucherNumber, String date) {
		String path = HOST + "otherbranchsalesinvoice/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);

		ResponseEntity<List<SalesInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesInvoiceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<TaxSummaryMst>> getOtherBranchInvoiceTax(String voucherNumber, String date) {
		String path = HOST + "otherbranchsalesinvoicetax/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);
		ResponseEntity<List<TaxSummaryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxSummaryMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<TaxSummaryMst>> getOtherBranchInvoiceTotal(String voucherNumber, String date) {
		String path = HOST + "otherbranchsumoftaxes/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);
		ResponseEntity<List<TaxSummaryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxSummaryMst>>() {
				});
		return response;
	}

	public static ResponseEntity<List<SalesInvoiceReport>> getOtherBranchInvoiceCustomerDtl(String voucherNumber,
			String date) {
		String path = HOST + "otherbranchsalesinvoicereport/gettaxinvoicecustomer/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);

		ResponseEntity<List<SalesInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesInvoiceReport>>() {
				});

		return response;
	}

	public static Double getCessAmountInOtherBranchTaxInvoiceReport(String voucherNumber, String date) {
		System.out.println(HOST + "otherbranchsalesinvoice/cessamount/" + voucherNumber + "?rdate=" + date);
		return restTemplate.getForObject(
				HOST + "otherbranchsalesinvoice/cessamount/" + voucherNumber + "?rdate=" + date, Double.class);

	}

	public static Double getOtherTaxableInvoiceAmount(String voucherNumber, String date) {
		System.out.println(HOST + "otherbranchsalesinvoice/nontaxableamount/" + voucherNumber + "?rdate=" + date);
		return restTemplate.getForObject(
				HOST + "otherbranchsalesinvoice/nontaxableamount/" + voucherNumber + "?rdate=" + date, Double.class);
	}

	public static OtherBranchSalesTransHdr getOtherBranchSalesTransHdrByVoucherAndDate(String voucherNumber,
			String date) {
		String path = HOST + "otherbranchsalestranshdrbyvoucheranddate/" + voucherNumber + "?rdate=" + date;
		System.out.println(path);
		return restTemplate.getForObject(path, OtherBranchSalesTransHdr.class);
	}

	public static String sendSMS(String mobileNumber, String message) {
		String path = "http://alerts.smsclogin.com/api/web2sms.php?workingkey=A4be256ac86f90f8809d00f77a6059e26&sender=STMARY&to="
				+ mobileNumber + "&message=" + message;
		System.out.println(path);
		return restTemplate.getForObject(path, String.class);

	}

	public static ResponseEntity<List<String>> getAllFloor() {
		String path = HOST + "itemlocationmstresource/getfloordistinct/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<String>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<String>>() {
				});

		return response;

	}

	public static ResponseEntity<List<String>> getAllShelf() {
		String path = HOST + "itemlocationmstresource/getshelfdistinct/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<String>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<String>>() {
				});

		return response;

	}

	public static ResponseEntity<List<String>> getAllRack() {
		String path = HOST + "itemlocationmstresource/getrackdistinct/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<String>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<String>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationReport>> getItemLocationReportItemId(String id) {

		String path = HOST + "itemlocationmstresource/getlocationreportbyitemid/" + id + "/"
				+ SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<ItemLocationReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationReport>> getItemLocationReportFloor(String id) {

		String path = HOST + "itemlocationmstresource/getlocationreportbyfloor/" + id + "/"
				+ SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<ItemLocationReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationReport>> getItemLocationReportFloorAndShelf(String floor,
			String shelf) {

		String path = HOST + "itemlocationmstresource/getlocationreportbyfloorandshelf/" + floor + "/" + shelf;
		System.out.println(path);
		ResponseEntity<List<ItemLocationReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationMst>> getItemLocationFloorAndShelf(String floor, String shelf) {

		String path = HOST + "itemlocationmstresource/getlocationbyfloorandshelf/" + floor + "/" + shelf;
		System.out.println(path);
		ResponseEntity<List<ItemLocationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationMst>> getItemLocationFloorAndShelfAndRack(String floor, String shelf,
			String rack) {

		String path = HOST + "itemlocationmstresource/getlocationbyfloorandshelfandrack/" + floor + "/" + shelf + "/"
				+ rack;
		System.out.println(path);
		ResponseEntity<List<ItemLocationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationMst>>() {
				});

		return response;

	}

	static public ResponseEntity<List<ItemLocationReport>> getItemLocationReportFloorAndShelfAndRack(String floor,
			String shelf, String rack) {

		String path = HOST + "itemlocationmstresource/getlocationreportbyfloorandshelfandrack/" + floor + "/" + shelf
				+ "/" + rack;
		System.out.println(path);
		ResponseEntity<List<ItemLocationReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemLocationReport>>() {
				});

		return response;

	}

	// ------------------version 6.1 surya

	public static ResponseEntity<List<PaymentReports>> getDayEndPaymentSummary(String sdate) {

		String path = HOST + "paymentreport/accountwisepaymentreport/" + SystemSetting.systemBranch + "?fromdate="
				+ sdate;
		System.out.println(path);
		ResponseEntity<List<PaymentReports>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentReports>>() {
				});

		return response;

	}

	// ------------------version 6.1 surya end
	// ------------------version 6.4 surya

	public static ResponseEntity<List<InvoiceEditEnableMst>> FindSaleOrderInvoiceEditByUser(String voucherNumber,
			String voucherDate, String userid) {
		String path = HOST + "saleorderinvoiceeditbyuser/" + userid + "/" + voucherNumber + "?date=" + voucherDate;
		System.out.println(path);
		ResponseEntity<List<InvoiceEditEnableMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InvoiceEditEnableMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SaleOrderReceipt>> SaleOrderReceiptByRecieptMode(String hdrid,
			String receiptMode) {
		String path = HOST + "salesorderreceiptbymode/" + hdrid + "/" + receiptMode;
		System.out.println(path);
		ResponseEntity<List<SaleOrderReceipt>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReceipt>>() {
				});

		return response;
	}
	// ------------------version 6.4 surya end
	// ------------------version 6.9 surya end

	public static ResponseEntity<List<StockTransferOutDltdDtl>> getStockTransferDltdDtlSummary(String strtDate) {
		String path = HOST + "stocktransferoutdltddtlbydate?date=" + strtDate;
		System.out.println(path);
		ResponseEntity<List<StockTransferOutDltdDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutDltdDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockTransferOutDltdDtl>> getStockTransferDltdDtlList(String id) {
		String path = HOST + "stocktransferoutdltddtlbyhdr/" + id;
		System.out.println(path);
		ResponseEntity<List<StockTransferOutDltdDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutDltdDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<WeighBridgeVehicleMst>> getAllWeighBridgeVehicleMst() {
		String path = HOST + "findallweighbridgevehiclemst";
		System.out.println(path);
		ResponseEntity<List<WeighBridgeVehicleMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeVehicleMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<WeighBridgeMaterialMst>> getAllWeighBridgeMaterialMst() {
		String path = HOST + "findallweighbridgematerialmst";
		System.out.println(path);
		ResponseEntity<List<WeighBridgeMaterialMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeMaterialMst>>() {
				});

		return response;
	}

	public static ResponseEntity<WeighBridgeVehicleMst> getWeighBridgeVehicleMstByVehicleType(String vehicleType) {
		String path = HOST + "findweighbridgevehiclemstbyvehicletype/" + vehicleType;
		System.out.println(path);
		ResponseEntity<WeighBridgeVehicleMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeVehicleMst>() {
				});

		return response;
	}

	public static ResponseEntity<WeighBridgeVehicleMst> SaveWeighBridgeVehicleMst(
			WeighBridgeVehicleMst weighBridgeVehicleMst) {
		return restTemplate.postForEntity(HOST + "saveweighbridgevehiclemst", weighBridgeVehicleMst,
				WeighBridgeVehicleMst.class);

	}

	public static ResponseEntity<WeighBridgeMaterialMst> getWeighBridgeMaterialMstByMaterial(String material) {
		String path = HOST + "findweighbridgematerialmstbymaterial/" + material;
		System.out.println(path);
		ResponseEntity<WeighBridgeMaterialMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeMaterialMst>() {
				});

		return response;
	}

	public static ResponseEntity<WeighBridgeMaterialMst> getWeighBridgeMaterialMstById(String materialId) {
		String path = HOST + "findweighbridgematerialmstbyid/" + materialId;
		System.out.println(path);
		ResponseEntity<WeighBridgeMaterialMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeMaterialMst>() {
				});

		return response;
	}

	public static ResponseEntity<WeighBridgeMaterialMst> SaveWeighBridgeMaterialMst(
			WeighBridgeMaterialMst weighBridgeMaterialMstSaved) {
		return restTemplate.postForEntity(HOST + "weighbridgematerialmst", weighBridgeMaterialMstSaved,
				WeighBridgeMaterialMst.class);

	}

	public static ResponseEntity<List<ItemMst>> getAllExceptionItemReport() {
		String path = HOST + "exceptionitems";
		System.out.println(path);
		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});
		return response;
	}

	public static ResponseEntity<ProductionMst> getPartiallySavedProduction(String vdate) {
		String path = HOST + "productionmst/getpartiallysavedproductionbydate/" + SystemSetting.systemBranch + "?rdate="
				+ vdate;
		System.out.println(path);
		ResponseEntity<ProductionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProductionMst>() {
				});

		return response;
	}

	public static ResponseEntity<PriceDefinition> saveExceptionPriceDefenition(PriceDefinition priceDefenition) {

		return restTemplate.postForEntity(HOST + "saveexceptionpricedefinition", priceDefenition,
				PriceDefinition.class);

	}

	public static ResponseEntity<WeighBridgeTareWeight> getWeighBridgeTareWeightByVehicleNo(String vehicleNo) {
		String path = HOST + "weighbridgetareweightbyvehicleno/" + vehicleNo;
		System.out.println(path);
		ResponseEntity<WeighBridgeTareWeight> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeTareWeight>() {
				});

		return response;
	}

	public static ResponseEntity<WeighBridgeWeights> getFirstWeightByVehicleNo(String vehicleNo) {
		String path = HOST + "getfistweight/" + vehicleNo;
		System.out.println(path);
		ResponseEntity<WeighBridgeWeights> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeWeights>() {
				});

		return response;
	}

	// @PostMapping("{companymstid}/saveweighbridgetareweight")

	public static ResponseEntity<WeighBridgeTareWeight> SaveWeighBridgeTareWeight(
			WeighBridgeTareWeight weighBridgeTareWeightSaved) {
		return restTemplate.postForEntity(HOST + "saveweighbridgetareweight", weighBridgeTareWeightSaved,
				WeighBridgeTareWeight.class);

	}

	public static ResponseEntity<WeighBridgeWeights> SaveWeighBridgeWeights(
			WeighBridgeWeights weighBridgeWeightsSaved) {
		return restTemplate.postForEntity(HOST + "saveweighbridgeweights", weighBridgeWeightsSaved,
				WeighBridgeWeights.class);

	}

	public static ResponseEntity<List<WeighBridgeWeights>> searchWeightByVehicleNo(String vehicleno) {

		String path = HOST + "getallweight?data=" + vehicleno;
		System.out.println(path);
		ResponseEntity<List<WeighBridgeWeights>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeWeights>>() {
				});

		return response;

	}

	public static ResponseEntity<List<WeighBridgeWeights>> searchWeightByVehicleNoWithSecondWtNull(String vehicleno) {

		String path = HOST + "weighingbridgeweight/getweighbridgewithsecndwtnull?data=" + vehicleno;
		System.out.println(path);
		ResponseEntity<List<WeighBridgeWeights>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeWeights>>() {
				});

		return response;

	}

	public static ResponseEntity<List<WeighBridgeTareWeight>> searchTareWeightByVehicleNo(String vehicleno) {

		String path = HOST + "getalltareweight?data=" + vehicleno;
		System.out.println(path);
		ResponseEntity<List<WeighBridgeTareWeight>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeTareWeight>>() {
				});

		return response;

	}

	public static ResponseEntity<WeighBridgeWeights> getWeighBridgeWeightById(String id) {
		String path = HOST + "weighbridgeweights/" + id;
		System.out.println(path);
		ResponseEntity<WeighBridgeWeights> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeWeights>() {
				});

		return response;
	}

	public static ResponseEntity<List<JournalDtl>> getJournalVoucherNumberByDate(String date) {
		// TODO Auto-generated method stub
		return null;
	}

	public static ResponseEntity<List<JournalHdr>> getJournalVoucherNumbers() {
		String path = HOST + "retrievealljournalhdr";
		System.out.println(path);
		ResponseEntity<List<JournalHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<JournalHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<JournalHdr> JournalHdrById(String journalHdrId) {
		String path = HOST + "retrievejournalhdrbyid/" + journalHdrId;
		System.out.println(path);
		ResponseEntity<JournalHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<JournalHdr>() {
				});

		return response;
	}

	public static ResponseEntity<List<DayBook>> getAllDayBookBySourceVoucherNumber(String voucherNumber) {
		String path = HOST + "retrievejournalhdrbyid/" + voucherNumber;
		System.out.println(path);
		ResponseEntity<List<DayBook>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DayBook>>() {
				});

		return response;
	}

	// ------------------version 6.9 surya end

	public static ResponseEntity<MachineResourceMst> findByMachineResourecName(String searchData) {
		System.out.println("findByMachineResourecName" + searchData);

		String path = HOST + "machineresourcemstresource/findallmachineresourcebyname/" + searchData;

		System.out.println(path);
		ResponseEntity<MachineResourceMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MachineResourceMst>() {
				});

		return response;

	}

	// --------------------------RESOURCECATEGORYMST---------------------

	public static ResponseEntity<List<MixCategoryMst>> findByResoureCatNameAndItem(String itemId,
			String resourceCatId) {
		System.out.println("subcategorymstresource/findallsubcategorymsts/" + itemId);

		String path = HOST + "resourcecatitemmstresource/findallresourcecatitemmst/" + itemId + "?resourcecatid="
				+ resourceCatId;

		System.out.println(path);
		ResponseEntity<List<MixCategoryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MixCategoryMst>>() {
				});

		return response;

	}

	public static ResponseEntity<MixCatItemLinkMst> findByItemId(String itemId) {
		System.out.println("resourcecatitemmstresource/fetchresourcecatitemmstbyitemid/" + itemId);

		String path = HOST + "resourcecatitemmstresource/fetchresourcecatitemmstbyitemid/" + itemId;

		System.out.println(path);
		ResponseEntity<MixCatItemLinkMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MixCatItemLinkMst>() {
				});

		return response;

	}

	public static ResponseEntity<List<MemberMst>> findByMemberName(String member) {
		// System.out.println("resourcecatitemmstresource/fetchresourcecatitemmstbyitemid/"
		// + itemId);

		String path = HOST + "membermstresource/findbymembermstbymame/" + member;

		System.out.println(path);
		ResponseEntity<List<MemberMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MemberMst>>() {
				});

		return response;

	}

	public static void updateMixCategory(String itemId, String mixCatId) {
		System.out.println("resourcecatitemmstresource/fetchresourcecatitemmstbyitemid/" + itemId);

		String path = HOST + "resourcecatitemmstresource/updateresourcecatitemmst/" + itemId + "/" + mixCatId;

	}

	public static ResponseEntity<List<StockReport>> getFastMovingItems(String sfDate, String stDate,
			String windowName) {

		String path = HOST + "itembatchmstresource/fastmovingitems/" + windowName + "/"
				+ SystemSetting.getSystemBranch() + "?fdate=" + sfDate + "&tdate=" + stDate;

		System.out.println(path);
		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<LmsQueueMst>> getpendindLmsQueueMst() {
		System.out.println("lmsqueuemst/selectpendingitems");

		String path = HOST + "lmsqueuemst/selectpendingitems";

		System.out.println(path);
		ResponseEntity<List<LmsQueueMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LmsQueueMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<String>> findLmsQueueMstDistinctVoucherTypes() {

		String path = HOST + "lmsqueuemst/vouchertypes";

		System.out.println(path);
		ResponseEntity<List<String>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<String>>() {
				});

		return response;
	}

	public static ResponseEntity<List<LmsQueueMst>> getLmsQueueMstByVoucherTypeAndStatus(String voucherType,
			String status, String startDate, String endtDate) {
		String path = HOST + "lmsqueuemst/lmsqueuemstbyvouchertypeandstatus/" + voucherType + "/" + status + "?fdate="
				+ startDate + "&&tdate=" + endtDate;

		System.out.println(path);
		ResponseEntity<List<LmsQueueMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LmsQueueMst>>() {
				});

		return response;
	}

	public static ResponseEntity<String> getLmsQueueMstRetry(String voucherType, String status, String startDate,
			String endtDate) {
		String path = HOST + "lmsqueuemst/forwardlmsqueuemstitems/" + voucherType + "/" + status + "?fdate=" + startDate
				+ "&&tdate=" + endtDate;
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;
	}

	public static ResponseEntity<List<PaymentReports>> getPettyCashPayment(String sdate) {

		String path = HOST + "paymentreport/pettycashpayment/" + SystemSetting.systemBranch + "?fromdate=" + sdate;
		System.out.println(path);
		ResponseEntity<List<PaymentReports>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PaymentReports>>() {
				});

		return response;

	}

	public static ResponseEntity<List<ItemStockEnquiryReport>> getItemStockEnquiryReport(String itemId,
			String voucherType) {
		String path = HOST + "stockreportresurce/itemstockenquiryreport/" + SystemSetting.systemBranch + "/" + itemId
				+ "/" + voucherType;
		System.out.println(path);
		ResponseEntity<List<ItemStockEnquiryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemStockEnquiryReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<CategoryManagementMst>> getAlcategoryManagement() {
		String path = HOST + "categorymanagementmst/findallcategorymanagement";
		System.out.println(path);
		ResponseEntity<List<CategoryManagementMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryManagementMst>>() {
				});

		return response;
	}

	public static ResponseEntity<CategoryManagementMst> getcategoryManagementByCategoryIdAndSubCategoryId(String id,
			String id2) {
		String path = HOST + "categorymanagementmst/getbycatidandsubcatid/" + id + "/" + id2;
		System.out.println(path);
		ResponseEntity<CategoryManagementMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CategoryManagementMst>() {
				});

		return response;
	}

	public static ResponseEntity<SalesTransHdr> getNullSalesTransHdrByCustomer(String customerId, String sdate,
			String customSalesMode) {
		String path = HOST + "salestranshdr/getnullvoucherbbycustomerid/" + customerId + "/" + customSalesMode
				+ "?sdate=" + sdate;
		System.out.println(path);
		ResponseEntity<SalesTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesTransHdr>() {
				});

		return response;
	}

	static public ArrayList SearchItemLikeName(String searchData) {
		System.out.println(HOST + "itemandcategorysearchwithname?data=" + searchData);

		return restTemplate.getForObject(HOST + "itemandcategorysearchwithname?data=" + searchData, ArrayList.class);

	}

	public static ResponseEntity<StockTransferOutHdr> getstockTransferOutHdrByVNoAndDate(String sdate, String vno) {
		String path = HOST + "stocktransferouthdrbyvouchernumber/" + vno + "/stocktransferouthdr?date=" + sdate;
		System.out.println(path);
		ResponseEntity<StockTransferOutHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<StockTransferOutHdr>() {
				});

		return response;
	}

	public static ResponseEntity<List<BranchAssetReport>> getAccountBalanceByDate(String strtDate) {
		String path = HOST + "accountingresource/customerclosingbalance?date=" + strtDate;
		System.out.println(path);
		ResponseEntity<List<BranchAssetReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchAssetReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<BranchAssetReport>> getSupplierAccountBalanceByDate(String strtDate) {

		String path = HOST + "accountingresource/supplierclosingbalance?date=" + strtDate;
		System.out.println(path);
		ResponseEntity<List<BranchAssetReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchAssetReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<BranchAssetReport>> getBankAccountBalanceByDate(String strtDate) {
		String path = HOST + "accountingresource/bankaccountclosingbalance?date=" + strtDate;
		System.out.println(path);
		ResponseEntity<List<BranchAssetReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchAssetReport>>() {
				});

		return response;
	}

	public static ResponseEntity<String> MergeAccount(String fromAccountId, String toAccountId) {
		String path = HOST + "mergeaccount/" + fromAccountId + "/" + toAccountId;
		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;
	}

	public static ResponseEntity<List<MemberWiseReceiptReport>> getMemberWiseReceiptReport(String startDate,
			String endDate, String memberid) {
		String path = HOST + "memberwisereportresource/memberwisereceiptreport/" + memberid + "?startDate=" + startDate
				+ "&&endDate=" + endDate;

		System.out.println("" + path);
		ResponseEntity<List<MemberWiseReceiptReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MemberWiseReceiptReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<PendingAmountReport>> getPendingAmountReport(String startDate, String endDate,
			String accountId) {
		String path = HOST + "pendingamountreport/" + accountId + "?startDate=" + startDate + "&&endDate=" + endDate;

		System.out.println("" + path);
		ResponseEntity<List<PendingAmountReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PendingAmountReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<PhysicalStockReport>> getPhysicalStockReport(String strDate, String toDate) {
		String path = HOST + "physicalstockreport" + "?fromdate=" + strDate + "&&todate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<PhysicalStockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PhysicalStockReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<BranchSaleReport>> getBranchSaleReport(String strDate, String toDate) {
		String path = HOST + "getbranchsalehdrreport/" + SystemSetting.systemBranch + "?fromdate=" + strDate
				+ "&&todate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<BranchSaleReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchSaleReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<CategoryMst>> getCategoryMsts() {
		String path = HOST + "categorymsts";
		System.out.println(path);
		ResponseEntity<List<CategoryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ProcessMst>> getprocessMstByProcessType(String processType) {
		String path = HOST + "processmstbyprocesstype/" + processType;
		System.out.println(path);
		ResponseEntity<List<ProcessMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProcessMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<DailySalesReportDtl>> SalesReportByPriceType(String strDate, String toDate,
			String priceTypeId) {
		String path = HOST + "salesinvoicereportresource/salesreportbypricetype/" + SystemSetting.systemBranch + "/"
				+ priceTypeId + "?fromdate=" + strDate + "&&todate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<DailySalesReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReportDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<CategoryExceptionList> saveCategoryExceptionList(
			CategoryExceptionList categoryExceptionList) {
		return restTemplate.postForEntity(HOST + "savecategoryexceptionlist/" + SystemSetting.systemBranch,
				categoryExceptionList, CategoryExceptionList.class);

	}

	public static ResponseEntity<List<CategoryExceptionList>> getAllCategoryExceptionList() {
		String path = HOST + "categoryexceptionlist";
		System.out.println(path);
		ResponseEntity<List<CategoryExceptionList>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryExceptionList>>() {
				});

		return response;
	}

	public static ResponseEntity<InvoiceFormatMst> saveJasperByPriceType(InvoiceFormatMst jasperByPriceType) {
		return restTemplate.postForEntity(HOST + "savejasperbypricetype", jasperByPriceType, InvoiceFormatMst.class);

	}

	public static ResponseEntity<List<InvoiceFormatMst>> getAllJasperByPriceType() {
		String path = HOST + "getalljasperbypricetype";
		System.out.println(path);
		ResponseEntity<List<InvoiceFormatMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InvoiceFormatMst>>() {
				});

		return response;
	}

	public static void deleteCategoryExceptionList(String id) {
		restTemplate.delete(HOST + "categoryexceptionlistbyid/" + id);

	}

	public static ResponseEntity<List<InsuranceCompanyMst>> getInsuranceCompanyByName(String insurancecompname) {
		String path = HOST + "insurancecompanymst/getinsurancecompbyname?insurancecompname=" + insurancecompname;
		ResponseEntity<List<InsuranceCompanyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceCompanyMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<InvoiceFormatMst>> getJasperByPriceTypeParamPriceTypeId(String priceTypeId) {
		String path = HOST + "getjasperbypricetypewithpricetypeid/" + priceTypeId;
		System.out.println(path);
		ResponseEntity<List<InvoiceFormatMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InvoiceFormatMst>>() {
				});

		return response;
	}

	public static ResponseEntity<InsuranceCompanyMst> saveInsuranceCompanyMst(InsuranceCompanyMst insuranceCompanyMst) {
		return restTemplate.postForEntity(HOST + "insurancecompanymst/saveinsurancecompany", insuranceCompanyMst,
				InsuranceCompanyMst.class);

	}

	public static ResponseEntity<List<InsuranceCompanyMst>> getAllInsuranceCompanyMst() {
		String path = HOST + "insurancecompanymst/findall";
		System.out.println(path);
		ResponseEntity<List<InsuranceCompanyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceCompanyMst>>() {
				});

		return response;
	}

	public static void DeleteJasperByPriceType(String id) {
		restTemplate.delete(HOST + "deletejasperbypricetype/" + id);

	}

	public static void deleteInsuranceCompanyMstById(String id) {
		restTemplate.delete(HOST + "insurancecompanymst/deleteinsurancecompbyid/" + id);

	}

	public static void updateInsuranceCompanyMst(InsuranceCompanyMst insuranceCompanyMst) {
		restTemplate.put(HOST + "insurancecompanymst/updateinsurancecompbyid/" + insuranceCompanyMst.getId(),
				insuranceCompanyMst);
		return;

	}

	public static ResponseEntity<PatientMst> getPatientMstByName(String patientName) {
		String path = HOST + "patientmst/getpatientmstbyname?patientname=" + patientName;
		System.out.println(path);
		ResponseEntity<PatientMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PatientMst>() {
				});

		return response;
	}

	public static ResponseEntity<PatientMst> savePatientMst(PatientMst patientMst) {
		return restTemplate.postForEntity(HOST + "patientmst/savepatientmst", patientMst, PatientMst.class);

	}

	public static ResponseEntity<InsuranceCompanyMst> getinsuraCompanyMstById(String insuranceCompanyId) {
		String path = HOST + "insurancecompanymst/getinsurancecompbyid/" + insuranceCompanyId;
		System.out.println(path);
		ResponseEntity<InsuranceCompanyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<InsuranceCompanyMst>() {
				});

		return response;
	}

	public static void deletePatientMst(String id) {
		restTemplate.delete(HOST + "patientmst/deletepatientmst/" + id);

	}

	public static ResponseEntity<List<PatientMst>> getAllPatientMsts() {
		String path = HOST + "patientmst";
		System.out.println(path);
		ResponseEntity<List<PatientMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PatientMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PatientMst>> searchPatientByName(String searchData) {
		String path = HOST + "patientmst/searchpatient?data=" + searchData;
		System.out.println(path);
		ResponseEntity<List<PatientMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PatientMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<InvoiceFormatMst>> getJasperByReportName(String priceTypeId, String gst,
			String customerDiscount, String batch, String custId) {
		String path = HOST + "invoiceformatmst/getjasperbyreportname?pricetypeid=" + priceTypeId + "&&gst=" + gst
				+ "&&discount=" + customerDiscount + "&&batch=" + batch + "&&custid=" + custId;

		System.out.println(path);
		ResponseEntity<List<InvoiceFormatMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InvoiceFormatMst>>() {
				});

		return response;
	}

	public static ResponseEntity<CurrencyMst> getCurrencyMstByName(String currencyName) {
		String path = HOST + "currencymst/getcurrencybyname/" + currencyName;
		System.out.println(path);
		ResponseEntity<CurrencyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CurrencyMst>() {
				});

		return response;
	}

	public static ResponseEntity<CurrencyMst> saveCurrencyMst(CurrencyMst currencymst) {
		return restTemplate.postForEntity(HOST + "currencymst/savecurrencymst", currencymst, CurrencyMst.class);

	}

	public static ResponseEntity<MenuWindowMst> getMenuWindowMstByMenuName(String menu) {

		String path = HOST + "menuwindowmst/getmenuwindowbymenu/" + menu;
		System.out.println(path);
		ResponseEntity<MenuWindowMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MenuWindowMst>() {
				});

		return response;

	}

	public static ResponseEntity<List<CurrencyMst>> getallCurrencyMst() {
		String path = HOST + "currencymst/getallcurrency";
		System.out.println(path);
		ResponseEntity<List<CurrencyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CurrencyMst>>() {
				});

		return response;
	}

	public static ResponseEntity<CurrencyMst> getcurrencyMsyById(String currencyId) {
		String path = HOST + "currencymst/getcurrencybyid/" + currencyId;
		System.out.println(path);
		ResponseEntity<CurrencyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CurrencyMst>() {
				});

		return response;
	}

	public static ResponseEntity<CurrencyConversionMst> getCurrencyConversionMstByCurrencyId(String currencyId) {

		String path = HOST + "currencyconversionmst/currencyconversionmstbycurrencyid/" + currencyId;
		System.out.println(path);
		ResponseEntity<CurrencyConversionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CurrencyConversionMst>() {
				});

		return response;

	}

	public static void updatePatientMstById(PatientMst patientMst) {
		restTemplate.put(HOST + "patientmst/updatepatientmstbyid/" + patientMst.getId(), patientMst);
		return;

	}

	public static ResponseEntity<PatientMst> getPatientMstById(String patientID) {
		String path = HOST + "patientmst/getpatientmstbyid/" + patientID;
		System.out.println(path);
		ResponseEntity<PatientMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PatientMst>() {
				});

		return response;
	}

	public static Double getFCRate(double rate, String selectedItem) {
		double rtnValue = 0;
		Double ownAccAmt = restTemplate
				.getForObject(HOST + "currencyconversionmst/findfcrate/" + rate + "/" + selectedItem, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;
	}

	public static Double getCompanyCurrencyAmountOfFC(double rate, String selectedItem) {

		String path = "currencyconversionmst/findcompanyCurrency/" + rate + "/" + selectedItem;

		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "currencyconversionmst/findcompanyCurrency/" + rate + "/" + selectedItem, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	public static void updateFCFieldsInSalesDtl(SalesDtl salesDtl) {
		restTemplate.put(HOST + "salesdetail/" + salesDtl.getId() + "/updatefcfields", salesDtl);
		return;

	}

	public static ResponseEntity<List<String>> getSalesModes() {
		String path = HOST + "salestranshdr/getsalesmode";
		System.out.println(path);
		ResponseEntity<List<String>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<String>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockReport>> getFastMovingItemsBySalesMode(String sfDate, String stDate,
			String salesmode, String windowname) {
		String path = HOST + "itembatchmstresource/fastmovingitemsbysalesmode/" + salesmode + "/" + windowname + "/"
				+ SystemSetting.getSystemBranch() + "?fdate=" + sfDate + "&tdate=" + stDate;

		System.out.println(path);
		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockReport>> getFastMovingItemsBySalesModeAndCategory(String sfDate,
			String stDate, String salesmode, String categoryId, String windowname) {
		String path = HOST + "itembatchmstresource/fastmovingitemsbysalesmodeandcategory/" + salesmode + "/"
				+ categoryId + "/" + windowname + "/" + SystemSetting.getSystemBranch() + "?fdate=" + sfDate + "&tdate="
				+ stDate;

		System.out.println(path);
		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});

		return response;
	}

	public static ResponseEntity<PurchaseAdditionalExpenseHdr> getPurchaseAdditionalExpenseHdrByPurchaseDtl(String id) {

		String path = HOST + "purchaseadditionalexpensehdr/getbypurchasedtlid/" + id;
		System.out.println(path);
		ResponseEntity<PurchaseAdditionalExpenseHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PurchaseAdditionalExpenseHdr>() {
				});

		return response;

	}

	public static ResponseEntity<PurchaseAdditionalExpenseHdr> savePurchaseAdditionalExpenseHdr(
			PurchaseAdditionalExpenseHdr purchaseAdditionalExpenseHdr) {
		return restTemplate.postForEntity(HOST + "purchaseadditionalexpensehdr/savepurchaseadditionalexpense",
				purchaseAdditionalExpenseHdr, PurchaseAdditionalExpenseHdr.class);

	}

	public static ResponseEntity<PurchaseAdditionalExpenseDtl> savePurchaseAdditionalExpenseDtl(
			PurchaseAdditionalExpenseDtl purchaseAdditionalExpenseDtl) {
		System.out.println(HOST + "purchaseadditionalexpensedtl/savepurchasedtladditionalexpense/"
				+ purchaseAdditionalExpenseDtl.getPurchaseAdditionalExpenseHdr().getId());
		return restTemplate.postForEntity(
				HOST + "purchaseadditionalexpensedtl/savepurchasedtladditionalexpense/"
						+ purchaseAdditionalExpenseDtl.getPurchaseAdditionalExpenseHdr().getId(),
				purchaseAdditionalExpenseDtl, PurchaseAdditionalExpenseDtl.class);

	}

	public static ResponseEntity<List<PurchaseAdditionalExpenseDtl>> getpurchaseAdditionalExpenseDtlByHdrId(String id) {
		String path = HOST + "purchaseaddtionalexpensedtl/getpurchaseaddtionaldtlbyhdrid/" + id;
		System.out.println(path);
		ResponseEntity<List<PurchaseAdditionalExpenseDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseAdditionalExpenseDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PurchaseAdditionalExpenseDtl>> getpurchaseAdditionalExpenseDtlByHdrIdByCalculated(
			String id) {
		String path = HOST + "purchaseaddtionalexpensedtl/getpurchaseaddtionaldtlbyhdridnotcalculated/" + id;
		System.out.println(path);
		ResponseEntity<List<PurchaseAdditionalExpenseDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseAdditionalExpenseDtl>>() {
				});

		return response;
	}

	public static void updatepurchaseNetCost(PurchaseDtl purchase) {

		restTemplate.put(HOST + "purchasedtl/updatenetcost", purchase);
		return;

	}

	public static ResponseEntity<List<AccountHeads>> getAccountHeadsByExpense() {
		String path = HOST + "accountheads/getaccountheadsunderexpense";
		System.out.println(path);
		ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountHeads>>() {
				});

		return response;
	}

	public static ResponseEntity<AdditionalExpense> savePurchaseAdditionalExpenseDtl(
			AdditionalExpense additionalExpense) {
		return restTemplate.postForEntity(HOST + "additionalexpense", additionalExpense, AdditionalExpense.class);

	}

	public static Double getSumOfAdditionalExpenseByPurchaseHdr(String id) {
		System.out.println("Items" + HOST + "additionalexpense/sumofamount/" + id);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "additionalexpense/sumofamount/" + id, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	public static Double getSumOfFCAdditionalExpenseByPurchaseHdr(String id) {
		System.out.println("Items" + HOST + "additionalexpense/sumoffcamount/" + id);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "additionalexpense/sumoffcamount/" + id, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	public static void deleteAdditionalExpenseById(String id) {
		restTemplate.delete(HOST + "additionalexpense/deleteadditionalexpense/" + id);

	}

	public static ResponseEntity<List<AdditionalExpense>> getallAdditionalExpenseByHdrId(String id) {
		String path = HOST + "additionalexpense/getalladditonalexpensebypurchasehdr/" + id;
		System.out.println(path);
		ResponseEntity<List<AdditionalExpense>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AdditionalExpense>>() {
				});
		return response;
	}

	public static ResponseEntity<List<IntentDtl>> ItemWiseIntentSummaryReport(String date) {

		String path = HOST + "itembatchmstresource/fastmovingitemsbysalesmodeandcategory/" + SystemSetting.systemBranch
				+ "?date=" + date;

		System.out.println(path);
		ResponseEntity<List<IntentDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<IntentDtl>>() {
				});

		return response;
	}

	public static Double getSumOfAdditionalExpenseByExpenseTye(String id, String expensetype) {
		System.out.println(HOST + "additionalexpense/sumoffcamountinlcudedinbill/" + id + "/" + expensetype);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "additionalexpense/sumoffcamountinlcudedinbill/" + id + "/" + expensetype, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	public static void updatePurchaseDtlFcFields(PurchaseDtl purchaseDtl2) {
		restTemplate.put(HOST + "purchasedtl/updatefcfields", purchaseDtl2);
		return;

	}

	public static void updatePurchaseAdditionalExpenseDtl(PurchaseAdditionalExpenseDtl purchaseAddExpDtl1) {
		restTemplate.put(HOST + "purchaseaddtionalexpensedtl/updatepurchasedtlstatus", purchaseAddExpDtl1);
		return;

	}

	public static Double getSumOfAdditionalExpenseByStatusPurchaseHdr(String id) {
		System.out.println("Items" + HOST + "additionalexpense/sumoffcamountwithstatus/" + id);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "additionalexpense/sumoffcamountwithstatus/" + id,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;
	}

	public static ResponseEntity<List<PDCReceipts>> getPDCReceiptByStatusBankAndAccountAndDate(String bank,
			String accountId, String sdate) {
		String path = HOST + "pdcreceipt/getbystatusandaccountidanddate/" + bank + "/" + accountId + "?rdate=" + sdate;

		System.out.println(path);
		ResponseEntity<List<PDCReceipts>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PDCReceipts>>() {
				});

		return response;
	}

//	public static String updateAdditionalExpenseStatusWithPurchaseHdrId(String id) {
//		return restTemplate.getForObject(HOST + "additionalexpense/updateadditionalexpensestatus/" + id, String.class);
//		}

	public static ResponseEntity<List<SalesManMst>> findAllSalesMan() {

		String path = HOST + "salesmanmstresource/findallsalesman";
		System.out.println(path);
		ResponseEntity<List<SalesManMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesManMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PurchaseReport>> getSupplierWisePurchaseReport(String cat, String startDate,
			String endDate, String selectedItem) {

		String path = HOST + "purchasereport/supplierwisepurchasereportwithcategory/" + selectedItem + "/"
				+ SystemSetting.systemBranch + "?fromdate=" + startDate + "&todate=" + endDate + "&categoryName=" + cat;
		System.out.println(path);
		ResponseEntity<List<PurchaseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PurchaseReport>> getSupplierWisePurchaseReportWithDate(String startDate,
			String endDate, String selectedItem) {

		String path = HOST + "purchasereport/supplierwisepurchasereport/" + selectedItem + "/"
				+ SystemSetting.systemBranch + "?fromdate=" + startDate + "&todate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<PurchaseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ConsumptionReport>> getdepartmentWiseConsumption(String startDate, String endDate,
			String cat, String selectedItem) {

		String path = HOST + "consumptionreportresource/getdepatmentwiseconsumption/" + SystemSetting.systemBranch + "/"
				+ selectedItem + "?rdate=" + startDate + "&tdate=" + endDate + "&category=" + cat;
		System.out.println(path);
		ResponseEntity<List<ConsumptionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesInvoiceReport>> getcustomerWiseSalesbyCategoryList(String startDate,
			String endDate, Object selectedItem, String cat) {

		String path = HOST + "salestranshdr/getcustomerwisesalesreportbycategory/" + selectedItem + "/"
				+ SystemSetting.systemBranch + "?fdate=" + startDate + "&tdate=" + endDate + "&categoryname=" + cat;
		System.out.println(path);
		ResponseEntity<List<SalesInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesInvoiceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesInvoiceReport>> getcustomerWiseSalesByDate(String startDate, String endDate,
			Object selectedItem) {

		String path = HOST + "salestranshdr/getcustomerwisesalesreport/" + selectedItem + "/"
				+ SystemSetting.systemBranch + "?fdate=" + startDate + "&tdate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<SalesInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesInvoiceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<InsuranceCompanySalesSummaryReport>> getInsuranceSalesSummary(String startDate,
			String endDate) {
		String path = HOST + "salestranshdr/getsaleswithinsurance/" + SystemSetting.systemBranch + "?fdate=" + startDate
				+ "&tdate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<InsuranceCompanySalesSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<InsuranceCompanySalesSummaryReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<InsuranceCompanySalesSummaryReport>> getInsuranceSalesSummaryWithInsuranceComp(
			String startDate, String endDate, String inscomp) {

		String path = HOST + "salestranshdr/getsaleswithinsuranceid/" + SystemSetting.systemBranch + "/?fdate="
				+ startDate + "&tdate=" + endDate + "&insurancelist=" + inscomp;
		System.out.println(path);
		ResponseEntity<List<InsuranceCompanySalesSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<InsuranceCompanySalesSummaryReport>>() {
				});

		return response;
	}

	public static AccountClass getaccountClassById(String accountClassId) {

		String path = HOST + "accountclass/accountclassbyid/" + accountClassId;
		System.out.println(path);
		return restTemplate.getForObject(path, AccountClass.class);

	}

	public static ResponseEntity<SalesPropertiesConfigMst> saveSalePropertiesConfigMst(
			SalesPropertiesConfigMst salesPropertiesConfigMst) {
		return restTemplate.postForEntity(HOST + "salespropertiesconfigmst/savesalesproperties",
				salesPropertiesConfigMst, SalesPropertiesConfigMst.class);

	}

	public static void deleteSalePropertyConfiguration(String id) {
		restTemplate.delete(HOST + "salespropertiesconfigmst/deletebyid/" + id);

	}

	public static ResponseEntity<List<SalesPropertiesConfigMst>> getAllSalesPropertyConfigMst() {

		String path = HOST + "salespropertiesconfigmst/getallsalesproperties/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<SalesPropertiesConfigMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesPropertiesConfigMst>>() {
				});

		return response;

	}

	public static ResponseEntity<List<SalesPropertiesConfigMst>> getsalesPropertyConfigMstByPropertyName(
			String selectedItem) {

		String path = HOST + "salespropertiesconfigmst/findsalespropertiesbyname/" + selectedItem;
		System.out.println(path);
		ResponseEntity<List<SalesPropertiesConfigMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesPropertiesConfigMst>>() {
				});

		return response;

	}

	public static ResponseEntity<SalesPropertyTransHdr> saveSalesPropertyTransHdr(
			SalesPropertyTransHdr salesPropertyTransHdr) {
		return restTemplate.postForEntity(HOST + "salespropertytranshdr/savesalespropertytranshdr",
				salesPropertyTransHdr, SalesPropertyTransHdr.class);
	}

	public static void deleteSalesPropertyTransHdr(String id) {
		restTemplate.delete(HOST + "salespropertytranshdr/deletebyid//" + id);

	}

	public static ResponseEntity<SalesPropertiesConfigMst> getsalespropertConfigMstById(String id) {
		String path = HOST + "salespropertiesconfigmst/findsalespropertiesbyid/" + id;
		System.out.println(path);
		ResponseEntity<SalesPropertiesConfigMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesPropertiesConfigMst>() {
				});

		return response;

	}

	public static ResponseEntity<List<SalesPropertyTransHdr>> getSalesPropertyTransHdrBySalesHdrId(
			String salesTransHdrId) {
		String path = HOST + "salespropertytranshdr/getallsalespropertytranshdrbyhdrid/" + salesTransHdrId;
		System.out.println(path);
		ResponseEntity<List<SalesPropertyTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesPropertyTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockTransferInReport>> StockTransferInDetailReport(String branchCode,
			String strDate, String strDate1) {
		String path = HOST + "stocktransfeindtlresource/stocktransferindetailreport/" + branchCode + "?fromdate="
				+ strDate + "&&todate=" + strDate1;
		System.out.println(path);
		ResponseEntity<List<StockTransferInReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockTransferInReport>> StockTransferInDetailReportByFromBranch(String branchCode,
			String fromBranchCode, String strDate, String strDate1) {
		String path = HOST + "stocktransfeindtlresource/stocktransferindetailreportbyfrombranch/" + branchCode + "/"
				+ fromBranchCode + "?fromdate=" + strDate + "&&todate=" + strDate1;
		System.out.println(path);
		ResponseEntity<List<StockTransferInReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockTransferInReport>> StockTransferInDetailReportByFromBranch(String branchCode,
			String fromBranchCode, String categoryIds, String sdate, String edate) {
		String path = HOST + "stocktransfeindtlresource/stocktransferindetailreportbyfrombranchandcategory/"
				+ branchCode + "/" + fromBranchCode + "?categoryids=" + categoryIds + "&&fromdate=" + sdate
				+ "&&todate=" + edate;
		System.out.println(path);
		ResponseEntity<List<StockTransferInReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockTransferInReport>> StockTransferInDetailReportByCategory(String branchCode,
			String categoryIds, String sdate, String edate) {
		String path = HOST + "stocktransfeindtlresource/stocktransferindetailreportbycategory/" + branchCode
				+ "?categoryids=" + categoryIds + "&&fromdate=" + sdate + "&&todate=" + edate;
		System.out.println(path);
		ResponseEntity<List<StockTransferInReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferInReport>>() {
				});

		return response;

	}

	public static ResponseEntity<List<StockTransferOutReport>> getStockTransferOutReportBetweenDate(String fdate,
			String tdate, String tobranch, String cat) {

		String path = HOST + "stocktransferoutreport/stockreportbetweendate/" + SystemSetting.systemBranch
				+ "?tobranch=" + tobranch + "&categorylist=" + cat + "&fromdate=" + fdate + "&todate=" + tdate;
		System.out.println(path);
		ResponseEntity<List<StockTransferOutReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutReport>>() {
				});

		return response;
	}

	public static ResponseEntity<OpeningBalanceMst> saveOpeningBalanceMst(OpeningBalanceMst openingBalanceMst) {
		return restTemplate.postForEntity(HOST + "openingbalancemst/saveopeningbalancemst", openingBalanceMst,
				OpeningBalanceMst.class);
	}

	public static void deleteOpeningBalanceById(String id) {
		restTemplate.delete(HOST + "openingbalancemst/deleteopeningbalancebyid/" + id);

	}

	public static ResponseEntity<List<OpeningBalanceMst>> getAllOpeningBalance() {

		String path = HOST + "openingbalancemst/getallopeningbalance";
		System.out.println(path);
		ResponseEntity<List<OpeningBalanceMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OpeningBalanceMst>>() {
				});

		return response;
	}

	public static ResponseEntity<OpeningBalanceMst> getOpeningBalanceByAccId(String accountId) {

		String path = HOST + "openingbalancemst/getallopeningbalancebyaccid/" + accountId;
		System.out.println(path);
		ResponseEntity<OpeningBalanceMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OpeningBalanceMst>() {
				});

		return response;

	}

	public static void updateOpeningBalance(OpeningBalanceMst openingBalanceMst) {
		restTemplate.put(HOST + "openingbalancemst/updateopeningbalance", openingBalanceMst);
		return;

	}

	public static ResponseEntity<OpeningBalanceConfigMst> saveOpeningBalanceConfigMst(
			OpeningBalanceConfigMst openingBalanceConfigMst) {
		return restTemplate.postForEntity(HOST + "openingbalanceconfigmst/saveopeningbalanceconfigmst",
				openingBalanceConfigMst, OpeningBalanceConfigMst.class);
	}

	public static void deleteOpeningBalanceConfigMst(String id) {
		restTemplate.delete(HOST + "openingbalanceconfigmst/deleteopeningbalanceconfigmstbyid/" + id);

	}

	public static ResponseEntity<List<OpeningBalanceConfigMst>> getAllOpeningBalanceConfigMst() {

		String path = HOST + "openingbalanceconfigmst/getallopeningbalanceconfigmst";
		System.out.println(path);
		ResponseEntity<List<OpeningBalanceConfigMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OpeningBalanceConfigMst>>() {
				});

		return response;

	}

	public static String changeFinancialYear(String status) {
		// TODO Auto-generated method stub
		return null;
	}

	public static String changeFinancialYear(String enddate, Integer id) {
		return restTemplate.getForObject(HOST + "accountingresource/changefinancilayr/" + id + "?enddate=" + enddate,
				String.class);
	}

	public static void updateFinancialYrOpeninClosingStatus(FinancialYearMst financialYearMst) {
		restTemplate.put(HOST + "financialyearmst/updateopenigclosingstatus", financialYearMst);
		return;

	}

	public static ResponseEntity<List<AccountHeads>> getAssetAccounts() {

		String path = HOST + "accountheads/fetchaccountheadsbyassetaccount";
		System.out.println(path);
		ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountHeads>>() {
				});

		return response;

	}

	public static ResponseEntity<List<AccountHeads>> SearchAccountByNameByParentId(String parentId, String searchData) {
//		return restTemplate.getForObject(HOST + "accountheadsresource/accountheadsbyparentid/"  + parentId+"/"+searchData, ArrayList.class);

		String path = HOST + "accountheadsresource/accountheadsbyparentid/" + parentId + "?accountname=" + searchData;
		System.out.println(path);
		ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountHeads>>() {
				});

		return response;
	}

	public static ResponseEntity<List<AccountHeads>> getAccountHeadsSerialNumber() {

		String path = HOST + "accountheads/getaccountheadbyserialnumbers";
		System.out.println(path);
		ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountHeads>>() {
				});

		return response;

	}

	public static ResponseEntity<List<AccountBalanceReport>> getAccountBalanceReportByParentId(String parentid,
			String date) {
		String path = HOST + "accountingresource/accountbalancebyparentaccount/" + parentid + "?date=" + date;
		System.out.println(path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<InvoiceFormatMst> saveinvoiceFormatMst(InvoiceFormatMst invoiceFormatMst) {
		return restTemplate.postForEntity(HOST + "savejasperbypricetype", invoiceFormatMst, InvoiceFormatMst.class);
	}

	public static ResponseEntity<OpeningBalanceMst> getBankaccountOppeningBalance(String id) {
		String path = HOST + "openingbalancemst/getopeningbalancebybankaccount";
		System.out.println(path);
		ResponseEntity<OpeningBalanceMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OpeningBalanceMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<AccountBalanceReport>> getTrialBalanceById(String id, String sdate) {
		String path = HOST + "accountingresource/accountbalancebyid/" + id + "?date=" + sdate;
		System.out.println(path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<StockReport>> getDailyMobilestockReport(String branchcode, String date) {

		String path = HOST + "stockreport/dailymobilestockreport/" + branchcode + "?fromdate=" + date;

		System.out.println(path);

		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});

		return response;

	}

	public static ResponseEntity<List<MenuConfigMst>> MenuConfigMstByMenuName(String string) {

		String path = HOST + "menuconfigmstresource/menuconfigmstbymenuname/" + string;
		System.out.println(path);
		ResponseEntity<List<MenuConfigMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MenuConfigMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<MenuConfigMst>> MenuConfigMstByParentId(String parentid) {

		String path = HOST + "menumstresource/menuconfigmstbyparentid?parentid=" + parentid;
		System.out.println(path);
		ResponseEntity<List<MenuConfigMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MenuConfigMst>>() {
				});

		return response;
	}

	public static ResponseEntity<MenuConfigMst> saveMenuConfigMst(MenuConfigMst menuConfigMst) {
		return restTemplate.postForEntity(HOST + "menuconfigmstresource/savemenuconfigmst", menuConfigMst,
				MenuConfigMst.class);
	}

	public static ResponseEntity<List<MenuConfigMst>> MenuConfigMstByMenuDescription(String string) {
		String path = HOST + "menuconfigmstresource/menuconfigmstbydescription/" + string;
		System.out.println(path);
		ResponseEntity<List<MenuConfigMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MenuConfigMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<MenuConfigMst>> getAllMenuConfigMst() {
		String path = HOST + "menuconfigmstresource/menuconfigmsts";
		System.out.println(path);
		ResponseEntity<List<MenuConfigMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MenuConfigMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<BarcodeFormatMst>> getAllBarCodeFormatMst() {
		String path = HOST + "barcodeformatmst/showallbarcodeformatmst";
		System.out.println(path);
		ResponseEntity<List<BarcodeFormatMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BarcodeFormatMst>>() {
				});

		return response;
	}

	static public ResponseEntity<List<StockReport>> getDailyMobilestockReportByCategoryOrItemName(String branchcode,
			String date, String itemName, String categoryName) {

		String path = HOST + "stockreport/dailymobilestockreportwithitemorcategory/" + branchcode + "?fromdate=" + date
				+ "&itemname=" + itemName + "&categoryname=" + categoryName;

		System.out.println(path);

		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {

				});
		return response;
	}

	public static ResponseEntity<InvoiceTermsAndConditionsMst> saveInvoiceTermsAndConditionsMst(
			InvoiceTermsAndConditionsMst invoiceTermsAndConditionsMst) {
		return restTemplate.postForEntity(HOST + "invoicetermsandconditions/saveinvoicetermsandconditions",
				invoiceTermsAndConditionsMst, InvoiceTermsAndConditionsMst.class);
	}

	public static ResponseEntity<MenuMst> saveMenuMst(MenuMst menuMst) {
		return restTemplate.postForEntity(HOST + "menumstresource/savemenumst", menuMst, MenuMst.class);
	}

	public static ResponseEntity<List<InvoiceTermsAndConditionsMst>> getAllInvoiceTermsAndConditions() {
		String path = HOST + "invoicetermsandconditions/showallinvoicetermsandconditions";

		System.out.println(path);

		ResponseEntity<List<InvoiceTermsAndConditionsMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InvoiceTermsAndConditionsMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<MenuMst>> getAllMenuMst() {
		String path = HOST + "menumstresource/menumsts/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<MenuMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<MenuMst>>() {
				});

		return response;
	}

	public static ResponseEntity<MenuConfigMst> MenuConfigMstById(String menuId) {
		String path = HOST + "menuconfigmstresource/menuconfigmstbyid/" + menuId;
		System.out.println(path);
		ResponseEntity<MenuConfigMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MenuConfigMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<TallyIntegrationMst>> getTallyIntegratonMsts() {
		String path = HOST + "tallyintegrationmst/gettallyintegrationmst";
		System.out.println(path);
		ResponseEntity<List<TallyIntegrationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TallyIntegrationMst>>() {
				});

		return response;
	}

	public static void deleteInvoiceTermsAndConditions(String id) {
		restTemplate.delete(HOST + "invoicetermsandcondiotns/deleteinvoicetermsandconditionsbyid/" + id);

	}

	public static void deleteAllTallyIntegrationMst() {

		restTemplate.delete(HOST + "tallyintegrationmst/deletetallyintegrationmst");

	}

	public static ResponseEntity<TallyIntegrationMst> saveTallyIntegrationMst(TallyIntegrationMst tallyIntegrationMst) {

		return restTemplate.postForEntity(HOST + "tallyintegrationmst/savetallyintegrationmst", tallyIntegrationMst,
				TallyIntegrationMst.class);
	}

	public static void DeleteMenuConfigMst(String id) {
		restTemplate.delete(HOST + "menuconfigmstresource/deletemenuconfigmstbyid/" + id);

	}

	public static void DeleteMenuMstById(String id) {

		restTemplate.delete(HOST + "menumstresource/deletemenumsts/" + id);

	}

	public static ResponseEntity<String> DefaultMenuCreation() {
		String path = HOST + "menumstresource/initializemenumst/" + SystemSetting.systemBranch;
		System.out.println("===path ==" + path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	public static ResponseEntity<MenuMst> getMenuMstById(String id) {

		String path = HOST + "menumstresource/menumstbyid/" + id;
		System.out.println(path);
		ResponseEntity<MenuMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MenuMst>() {
				});

		return response;

	}

	static public void orgReceiptDelete(String vouchernumber) {

		restTemplate.delete(HOST + "paymentreport/deletepaymentreport/" + vouchernumber);
		return;
	}

	static public void orgPaymentDelete(String vouchernumber) {

		restTemplate.delete(HOST + "paymentreport/deletepaymentreport/" + vouchernumber);
		return;
	}

	public static ResponseEntity<MenuMst> getMenuMstByMenuId(String menuid) {

		String path = HOST + "menumstresource/menumstbymenuname/" + menuid;
		System.out.println(path);
		ResponseEntity<MenuMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<MenuMst>() {
				});

		return response;

	}

	public static String updateCustomerMstByPriceTypeAndReportName(String reportname, String priceid) {
		return restTemplate.getForObject(
				HOST + "customerregistration/updateallcustoerbyreportnameandpriceid/" + priceid + "/" + reportname,
				String.class);

	}

	public static String updateCustomerMstByReportName(String reportname) {
		return restTemplate.getForObject(HOST + "customerregistration/updateallcustoerbyreportname/" + reportname,
				String.class);

	}

	public static ResponseEntity<List<TallyIntegrationMst>> getlAllTallyIntegrationMst() {
		String path = HOST + "tallyintegrationmst/gettallyintegrationmst";
		System.out.println(path);
		ResponseEntity<List<TallyIntegrationMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TallyIntegrationMst>>() {
				});

		return response;
	}

	public static ResponseEntity<OtherBranchPurchaseDtl> getOtherBranchPurchaseDtlById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void OtherBranchPurchaseDtlDelete(String id) {

		restTemplate.delete(HOST + "otherbranchpurchasedtl/" + id);
	}

	public static ResponseEntity<OtherBranchPurchaseDtl> saveOtherBranchPurchaseDtl(
			OtherBranchPurchaseDtl otherBranchPurchaseDtl) {

		return restTemplate.postForEntity(HOST + "otherbranchpurchasedtl/"
				+ otherBranchPurchaseDtl.getOtherBranchPurchaseHdr().getId() + "/otherbranchpurchasedtl",
				otherBranchPurchaseDtl, OtherBranchPurchaseDtl.class);

	}

	public static void updateOtherBranchPurchaseHdr(OtherBranchPurchaseHdr otherBranchPurchaseHdr) {

		restTemplate.put(HOST + "otherbranchpurchasehdr/finalsave/" + otherBranchPurchaseHdr.getId(),
				otherBranchPurchaseHdr);
		return;
	}

	public static ResponseEntity<OtherBranchPurchaseHdr> saveOtherBranchPurchaseHdr(
			OtherBranchPurchaseHdr otherBranchPurchaseHdr) {
		return restTemplate.postForEntity(HOST + "otherbranchpurchasehdr/saveotherbranchpurchasehdr",
				otherBranchPurchaseHdr, OtherBranchPurchaseHdr.class);

	}

	public static SummarySalesDtl getOtherBranchPurchaseHdrSummary(String id) {
		return restTemplate.getForObject(HOST + "otherbranchpurchasedtl/" + id + "/otherbranchpurchasedtlsummary",
				SummarySalesDtl.class);

	}

	public static ResponseEntity<List<OtherBranchPurchaseDtl>> getOtherBranchPurchaseDtlByHdr(
			OtherBranchPurchaseHdr otherBranchPurchaseHdr) {
		String path = HOST + "otherbranchpurchasedtl/" + otherBranchPurchaseHdr.getId() + "/getotherbranchpurchasedtl";
		System.out.println(path);
		ResponseEntity<List<OtherBranchPurchaseDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OtherBranchPurchaseDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<ParamValueConfig> saveParamValueConfig(ParamValueConfig paramValueConfig) {
		return restTemplate.postForEntity(HOST + "paramvalueconfig", paramValueConfig, ParamValueConfig.class);

	}

	public static ResponseEntity<List<ParamValueConfig>> getAllParamValueConfig() {
		String path = HOST + "paramvalueconfig/getallparamvalueconfig";
		System.out.println(path);
		ResponseEntity<List<ParamValueConfig>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ParamValueConfig>>() {
				});

		return response;
	}

	public static void deleteParamValueConfigById(String id) {

		restTemplate.delete(HOST + "paramvalueconfig/deletebyid/" + id);
	}

	public static ResponseEntity<List<InvoiceEditEnableMst>> getInvoiceEditEnableMstByCustomer(String custname,
			String userid) {

		String path = HOST + "invoiceditenablemst/" + custname + "/" + userid + "/getcustomerfrominvoiceeditenablemst";
		System.out.println(path);
		ResponseEntity<List<InvoiceEditEnableMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InvoiceEditEnableMst>>() {
				});

		return response;

	}

	public static String resendOtherBranchSales(String id) {
		System.out.println(HOST + "otherbranchsalestranshdr/resendotherbranchsales/" + id);
		return restTemplate.getForObject(HOST + "otherbranchsalestranshdr/resendotherbranchsales/" + id, String.class);

	}

	public static ResponseEntity<List<PriceDefinition>> SearchPriceDefinitionByCategoryId(String id) {
		String path = HOST + "pricedefinitionbycategoryid/" + id;
		System.out.println(path);
		ResponseEntity<List<PriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PriceDefinition>>() {
				});

		return response;
	}

	public static ResponseEntity<List<OtherBranchSalesTransHdr>> OtherBranchSalesTransHdrByDate(String voucherDate) {
		String path = HOST + "otherbranchsalestranshdrbydate?date=" + voucherDate;
		System.out.println(path);
		ResponseEntity<List<OtherBranchSalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OtherBranchSalesTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<OtherBranchSalesTransHdr> getOtherBranchSalesTransHdrBySalesId(String id) {
		String path = HOST + "otherbranchsalestranshdr/otherbranchsalestranshdrbysalesid/" + id;
		System.out.println(path);
		ResponseEntity<OtherBranchSalesTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OtherBranchSalesTransHdr>() {
				});

		return response;
	}

	public static ResponseEntity<OtherBranchSalesTransHdr> OtherBranchSalesTransHdrById(String id) {
		String path = HOST + "otherbranchsalestranshdr/otherbranchsalestranshdrbyid/" + id;
		System.out.println(path);
		ResponseEntity<OtherBranchSalesTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OtherBranchSalesTransHdr>() {
				});

		return response;
	}

	public static void deletekiTDefinitionMstById(String id) {
		restTemplate.delete(HOST + "kitdefinitionmst/deletekitbyid/" + id);

	}

	public static ResponseEntity<ChequePrintMst> saveChequePrintMst(ChequePrintMst chequePrintMst) {
		return restTemplate.postForEntity(HOST + "chequeprintmst/savechequeprintmst", chequePrintMst,
				ChequePrintMst.class);
	}

	public static void deleteAllOtherBranchSalesDtlByHdrId(String id) {

		restTemplate.delete(HOST + "deleteallotherbranchsalesdtlbyhdrid/" + id);

	}

	public static void deleteChequePrintMstById(String id) {
		restTemplate.delete(HOST + "chequeprintmst/deletechequeprintmstbyid/" + id);

	}

	public static ResponseEntity<List<ChequePrintMst>> getallChequePrintByAccountId(String id) {
		String path = HOST + "chequeprintmst/showallchequeprintbybankname/" + id;
		System.out.println(path);
		ResponseEntity<List<ChequePrintMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ChequePrintMst>>() {
				});

		return response;
	}

	public static ResponseEntity<CustomInvoicePropertyFormat> saveCustomInvoicePropertyFormat(
			CustomInvoicePropertyFormat customInvoicePropertyFormat) {
		return restTemplate.postForEntity(HOST + "custominvoicepropertyformat/savecustominvoicepropertyformat",
				customInvoicePropertyFormat, CustomInvoicePropertyFormat.class);
	}

	public static ResponseEntity<List<BatchPriceDefinition>> SearchBatchPriceDefinitionByItemId(String id) {
		String path = HOST + "batchpricedefinition/batchpricedefinitionbyitemid/" + id;
		System.out.println(path);
		ResponseEntity<List<BatchPriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BatchPriceDefinition>>() {
				});

		return response;
	}

	public static ResponseEntity<List<BatchPriceDefinition>> SearchBatchPriceDefinitionByPriceTypeId(String id) {
		String path = HOST + "batchpricedefinition/batchpricedefinitionbypricetypeid/" + id;
		System.out.println(path);
		ResponseEntity<List<BatchPriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BatchPriceDefinition>>() {
				});

		return response;
	}

	public static ResponseEntity<List<BatchPriceDefinition>> getBatchPriceDefenitionByCostPrice(String itemid,
			String pricetypeid, String unitId, String date, String batch) {
		String path = HOST + "batchpricedefinition/batchpricedefinitionbycostprice/" + itemid + "/" + pricetypeid + "/"
				+ batch + "/" + unitId + "?date=" + date;
		System.out.println(path);
		ResponseEntity<List<BatchPriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BatchPriceDefinition>>() {
				});

		return response;
	}

	public static void delteCustomInvoiceFormat(String id) {
		restTemplate.delete(HOST + "custominvoicepropertyformat/deletbyid/" + id);

	}

	public static ResponseEntity<List<CustomInvoicePropertyFormat>> getCustomInvoiceFormatByCustomSalesMode(
			String selectedItem) {
		String path = HOST + "custominvoicepropertyformat/findbycustomsalesmode/" + selectedItem;
		System.out.println(path);
		ResponseEntity<List<CustomInvoicePropertyFormat>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CustomInvoicePropertyFormat>>() {
				});

		return response;
	}

	public static void updateIntentInhdr(IntentInHdr intentInHdr) {
		restTemplate.put(HOST + "intentinhdr/" + intentInHdr.getId() + "/updateintenthdr", intentInHdr);
		return ;

	}

	public static ResponseEntity<List<SalesOrderTransHdr>> getHoldedSalesOrderTransHdr() {
		String path = HOST + "salesordertranshdr/holdedsalesordertranshdr";
		System.out.println(path);
		ResponseEntity<List<SalesOrderTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<OrderTakerMst> getOrderTakerMstById(String salesManId) {
		String path = HOST + "ordertakermst/ordertakerbyid/" + salesManId;
		System.out.println(path);
		ResponseEntity<OrderTakerMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OrderTakerMst>() {
				});

		return response;
	}

	public static ResponseEntity<DeliveryBoyMst> getDeliveryBoyMstById(String deliveryBoyId) {
		String path = HOST + "ordertakermst/ordertakerbyid/" + deliveryBoyId;
		System.out.println(path);
		ResponseEntity<DeliveryBoyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DeliveryBoyMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<StockTransferOutHdr>> getHoldedStockTransfer() {
		String path = HOST + "stocktransferouthdr/holdedstocktransferouthdrs";
		System.out.println(path);
		ResponseEntity<List<StockTransferOutHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockTransferOutHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<StockTransferOutHdr> getStockTransferOutHdrById(String id) {
		String path = HOST + "stocktransferouthdr/stocktransferouthdrbyid/" + id;
		System.out.println(path);
		ResponseEntity<StockTransferOutHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<StockTransferOutHdr>() {
				});

		return response;
	}

	public static ResponseEntity<IntentInDtl> getIntentInDtlById(String id) {

		String path = HOST + "intentdtl/intentdtlbyid/" + id;
		System.out.println(path);
		ResponseEntity<IntentInDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<IntentInDtl>() {
				});

		return response;
	}

	public static ResponseEntity<BarcodeFormatMst> saveBarcodeFormatMst(BarcodeFormatMst barcodeFormatMst) {
		return restTemplate.postForEntity(HOST + "barcodeformatmst/savebarcodeformatmst", barcodeFormatMst,
				BarcodeFormatMst.class);

	}

	public static void deleteBarcodeFormatMstById(String id) {
		restTemplate.delete(HOST + "barcodeformatmst/deletebarcodeformatmstbyid/" + id);

	}

	public static ResponseEntity<List<BarcodeFormatMst>> showAllBarcodeFormat() {

		String path = HOST + "barcodeformatmst/showallbarcodeformatmst";
		System.out.println(path);
		ResponseEntity<List<BarcodeFormatMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BarcodeFormatMst>>() {
				});

		return response;

	}

	public static ResponseEntity<List<BarcodeFormatMst>> getBarcodeConfigMstByFormatName(String formatname) {

		String path = HOST + "barcodeformatmst/showallbarcodeformatmstbyname/" + formatname;
		System.out.println(path);
		ResponseEntity<List<BarcodeFormatMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BarcodeFormatMst>>() {
				});

		return response;

	}

	public static ResponseEntity<List<StockReport>> getStoreWiseStockReport(String store) {
		String path = HOST + "stockreport/storewisestockreport/" + store + "/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StoreMst>> StoreMstByStoreName(String name) {
		String path = HOST + "storemstresource/storemstbycompanymstandname/" + name;
		System.out.println(path);
		ResponseEntity<List<StoreMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StoreMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<DailySalesReportDtl>> getUserWiseSaleReportSummary(String branchCode,
			String userid, String strDate, String endDate) {

		String path = HOST + "dailysalesreport/userwisesalesreportsummary/" + userid + "/" + branchCode + "?fdate="
				+ strDate + "&&tdate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<DailySalesReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReportDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<DailySalesReportDtl>> getUserWiseSalesReport(String id, String branchCode,
			String strDate, String endDate) {
		String path = HOST + "dailysalesreport/userwisesalesreport/" + id + "/" + branchCode + "?fdate=" + strDate
				+ "&&tdate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<DailySalesReportDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReportDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<StoreChangeMst>> StoreChangeMstBtwnDate(String fsdate, String tsdate) {

		String path = HOST + "storechangemstresource/storechangemstbetweendate?fromdate=" + fsdate + "&todate="
				+ tsdate;
		System.out.println(path);
		ResponseEntity<List<StoreChangeMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StoreChangeMst>>() {
				});

		return response;

	}

	public static ResponseEntity<List<ReceiptModeReport>> getUserWiseSalesReceiptReport(String id, String branchCode,
			String strDate, String endDate) {
		String path = HOST + "salesreceipts/userwisesalesreceiptsummaryreport/" + id + "/" + branchCode + "?fdate="
				+ strDate + "&&tdate=" + endDate;

		System.out.println(path);

		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<BranchWiseProfitReport>> CustomerWiseProfitReport(String sdate, String edate) {

		String path = HOST + "salestranshdrreport/customerwiseprofit/" + SystemSetting.systemBranch + "?fdate=" + sdate
				+ "&&tdate=" + edate;

		System.out.println(path);

		ResponseEntity<List<BranchWiseProfitReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchWiseProfitReport>>() {
				});

		return response;
	}

	public static String getRootAccount(String account) {
		System.out.println(HOST + "accountheads/getroot?account=" + account);
		return restTemplate.getForObject(HOST + "accountheads/getroot?account=" + account, String.class);

	}

	public static ResponseEntity<BalanceSheetHorizontal> saveBalanceSheetHorizontal(
			BalanceSheetHorizontal balanceSheetHorizontal) {
		return restTemplate.postForEntity(HOST + "balancesheethorizontal/savebalancesheethorizontal",
				balanceSheetHorizontal, BalanceSheetHorizontal.class);
	}

	public static ResponseEntity<List<AccountHeads>> getAccountHeadsiWithParentNull() {
		String path = HOST + "accountheads/getaccountheadswithnullparent";

		System.out.println(path);

		ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountHeads>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ProcessPermissionMst>> getprocesspermissionMstByProcessName(String menuName) {

		String path = HOST + "processpermission/processpermissionbyname/" + menuName;

		System.out.println(path);

		ResponseEntity<List<ProcessPermissionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProcessPermissionMst>>() {
				});

		return response;

	}

	public static ArrayList SearchStockItemByBatch(String searchData, String date) {
		System.out.println(HOST + "stockitempopupsearchwithbatch?date=" + date + "&&data=" + searchData);
		return restTemplate.getForObject(HOST + "stockitempopupsearchwithbatch?date=" + date + "&&data=" + searchData,
				ArrayList.class);

	}

	public static ResponseEntity<KotItemMst> saveKotItemmst(KotItemMst kotItemMst) {
		return restTemplate.postForEntity(HOST + "kotitemmst/savekotitemmst", kotItemMst, KotItemMst.class);

	}

	public static void deleteKotItemMstById(String id) {
		restTemplate.delete(HOST + "kotitemmst/deletekotitemmst/" + id);

	}

	public static ResponseEntity<List<KotItemMst>> getAllKotItemMst() {
		String path = HOST + "kotitemmst/findallkotitemmst";

		System.out.println(path);

		ResponseEntity<List<KotItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KotItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<KotItemMst>> getAllKotItemMstByItemName(String itemName) {
		String path = HOST + "kotitemmst/kotitemmstbyitemname?itemname=" + itemName;

		System.out.println(path);

		ResponseEntity<List<KotItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KotItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemMst>> getItemWithNullCategory() {
		String path = HOST + "itemmst/getitemwithnullcategory";

		System.out.println(path);

		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemMst>> getItemNotInKot() {
		String path = HOST + "itemmst/finditemnotinkotitem";

		System.out.println(path);

		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemMst>> searchItemNotInKot(String itemname) {
		String path = HOST + "itemmst/searchitemnotinkotitem?data=" + itemname;

		System.out.println(path);

		ResponseEntity<List<ItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<String>> getCategoryNotInKotCategory() {
		String path = HOST + "kotcategorymstresource/getcategorynotinkot";

		System.out.println(path);

		ResponseEntity<List<String>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<String>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemNotInBatchPriceReport>> getItemsNotInBatchPrice() {
		String path = HOST + "batchpricedefinition/getitemsnotinbatchpricedefintion";

		System.out.println(path);

		ResponseEntity<List<ItemNotInBatchPriceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemNotInBatchPriceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<BatchPriceDefinition>> getBatchPriceDefinitionByItemIdAndPriceId(String itemid,
			String priceid) {

		String path = HOST + "batchpricedefinition/getbatchpricebyitemidandpriceid/" + itemid + "/" + priceid;

		System.out.println(path);

		ResponseEntity<List<BatchPriceDefinition>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BatchPriceDefinition>>() {
				});

		return response;

	}

	public static ResponseEntity<LocalCustomerDtl> saveLocalCustomerDtl(LocalCustomerDtl localCustomerDtl) {
		return restTemplate.postForEntity(
				HOST + "localcustomerdtl/savelocalcustomerdtl/" + localCustomerDtl.getLocalCustomerMst().getId(),
				localCustomerDtl, LocalCustomerDtl.class);

	}

	public static ResponseEntity<List<LocalCustomerDtl>> getLocalCustomerDtlByHdrId(String id) {
		String path = HOST + "localcustomerdtl/getlocalcustomerdtl/" + id;

		System.out.println(path);

		ResponseEntity<List<LocalCustomerDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LocalCustomerDtl>>() {
				});

		return response;
	}

	public static void deleteLocalCustomerDtl(String id) {

		restTemplate.delete(HOST + "localcustomerdtl/deletelocalcustomerdtl/" + id);

	}

	public static ResponseEntity<List<String>> getCountryList() {

		String path = HOST + "companymst/getcountry";
		System.out.println(path);
		ResponseEntity<List<String>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<String>>() {
				});

		return response;

	}

	public static ResponseEntity<List<String>> getStateListByCountry(String country) {

		String path = HOST + "companymst/getstatebycountry?country=" + country;
		System.out.println(path);
		ResponseEntity<List<String>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<String>>() {
				});

		return response;

	}

	public static String getGSTTypeByCustomerState(String customerState) {
		return restTemplate.getForObject(HOST + "customerregistration/getgsttype/" + customerState, String.class);

	}

	public static ResponseEntity<TermsAndConditionsMst> saveTermsAndConditionMst(
			TermsAndConditionsMst termsAndConditionMst) {
		return restTemplate.postForEntity(HOST + "termsandconditionmst/savetermsandconditionmst", termsAndConditionMst,
				TermsAndConditionsMst.class);
	}

	public static ResponseEntity<List<TermsAndConditionsMst>> getAllTermsAndConditionMst() {
		String path = HOST + "termsandconditionmst/gettermsandconditionmst";
		System.out.println(path);
		ResponseEntity<List<TermsAndConditionsMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TermsAndConditionsMst>>() {
				});

		return response;
	}

	public static void deleteTermsAndConditionMst(String id) {

		restTemplate.delete(HOST + "termsandconditionmst/deletetermsandconditionmst/" + id);

	}

	public static ResponseEntity<List<TermsAndConditionsMst>> getTermsAndConditionMstByInvoiceFormat(
			String jasperName) {
		String path = HOST + "/termsandconditionmst/gettermsandconditionmstbyinvoiceformat/" + jasperName;
		System.out.println(path);
		ResponseEntity<List<TermsAndConditionsMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TermsAndConditionsMst>>() {
				});

		return response;
	}

	public static ResponseEntity<SaleOrderEdit> SaleOrderEditById(String id) {

		String path = HOST + "saleorderedit/saleordereditbyid/" + id;
		System.out.println(path);
		ResponseEntity<SaleOrderEdit> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SaleOrderEdit>() {
				});

		return response;
	}

	public static ResponseEntity<SaleOrderEdit> saveSaleOrderEditWithLoginDate(SaleOrderEdit saleOrderEdit,
			String logdate) {
		return restTemplate.postForEntity(HOST + "saleorderedit/" + saleOrderEdit.getSalesOrderTransHdr().getId()
				+ "/savesaleorderedit?logdate=" + logdate, saleOrderEdit, SaleOrderEdit.class);
	}

	public static ResponseEntity<SalesOrderDtl> saveSalesOrderDtlWithLoginDate(SalesOrderDtl salesOrderDtl,
			String logdate) {
		System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
		System.out.println(HOST + "saleordedtl/" + salesOrderDtl.getSalesOrderTransHdr().getId()
				+ "/savesaleordedtlwithlogdate?logdate=" + logdate);
		return restTemplate.postForEntity(HOST + "saleordedtl/" + salesOrderDtl.getSalesOrderTransHdr().getId()
				+ "/savesaleordedtlwithlogdate?logdate=" + logdate, salesOrderDtl, SalesOrderDtl.class);
	}

	public static ResponseEntity<List<AccountBalanceReport>> getAccountBalanceReportByIncomeParentId(String id,
			String sdate) {
		String path = HOST + "accountingresource/accountbalancebyincomeparentaccount/" + id + "?date=" + sdate;
		System.out.println(path);
		ResponseEntity<List<AccountBalanceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountBalanceReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<HsnWisePurchaseDtlReport>> getHsnPurchaseDtl(String startDate, String endDate,
			String cat) {
		String path = HOST + "purchasereport/hsnpurchasedtlreport/" + SystemSetting.systemBranch + "?fromdate="
				+ startDate + "&todate=" + endDate + "&categoryName=" + cat;
		System.out.println(path);
		ResponseEntity<List<HsnWisePurchaseDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<HsnWisePurchaseDtlReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<OtherBranchPurchaseHdr>> getAllOtherBranchPurchaseHdrByDate(String startDate,
			String endDate, String branchcode) {
		String path = HOST + "otherbranchpurchasehdr/" + branchcode + "/otherbranchpurchasesummary?fromdate="
				+ startDate + "&&todate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<OtherBranchPurchaseHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OtherBranchPurchaseHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<OtherBranchPurchaseHdr> OtherBranchPurchaseHdrById(String id) {

		String path = HOST + "otherbranchpurchasehdr/" + id + "/otherbranchpurchasehdrbyid";
		System.out.println(path);
		ResponseEntity<OtherBranchPurchaseHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<OtherBranchPurchaseHdr>() {
				});

		return response;
	}

	public static ResponseEntity<List<HsnWiseSalesDtlReport>> getHsnSalesDtl(String startDate, String endDate,
			String cat) {
		String path = HOST + "salestranshdrreport/hsnsalesdtlreport/" + SystemSetting.systemBranch + "?fromdate="
				+ startDate + "&todate=" + endDate + "&categoryName=" + cat;
		System.out.println(path);
		ResponseEntity<List<HsnWiseSalesDtlReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<HsnWiseSalesDtlReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<B2bSalesReport>> getgstSalesReport(String date, String stodate) {

		String path = HOST + "salestranshdrreport/gstsalesreport/" + SystemSetting.systemBranch + "?rdate=" + date
				+ "&tdate=" + stodate;
		System.out.println(path);
		ResponseEntity<List<B2bSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<B2bSalesReport>>() {
				});

		return response;

	}

	public static ResponseEntity<ItemDeviceMst> saveItemDeviceMst(ItemDeviceMst itemDeviceMst) {
		return restTemplate.postForEntity(HOST + "itemdevicemst/saveitemdevicemst", itemDeviceMst, ItemDeviceMst.class);

	}

	public static ResponseEntity<List<ItemDeviceMst>> getAllItemDeviceMst() {

		String path = HOST + "itemdevicemst/getallitemdevicemst/" + SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<List<ItemDeviceMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemDeviceMst>>() {
				});

		return response;
	}

	public static void deleteItemDeviceMstById(String id) {
		restTemplate.delete(HOST + "itemdevicemst/deleteitemdevicemst/" + id);

	}

	public static ResponseEntity<List<ReceiptModeReport>> getTotalCardAmountByDate(String date) {
		String path = HOST + "receiptmodemst/findtotalcardamount/" + SystemSetting.systemBranch + "?date=" + date;
		System.out.println(path);
		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ReceiptModeMst>> getReceiptModeMstByName(String receiptMode) {
		String path = HOST + "findreceiptmode/getrecieptmodebyname/" + receiptMode;
		System.out.println(path);
		ResponseEntity<List<ReceiptModeMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesOrderTransHdr>> getCanceledSaleOrder(String strDate, String endDate) {
		String path = HOST + "salesordertranshdr/canceledsalesordertranshdr/" + SystemSetting.systemBranch + "?fdate="
				+ strDate + "&&tdate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<SalesOrderTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderTransHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<List<WeighBridgeWeights>> getWeighBridgeWtsWithNullSecondWt() {
		String path = HOST + "weighbridgereportresurce/getweighbridgewtswithsecondwtnull";
		System.out.println(path);
		ResponseEntity<List<WeighBridgeWeights>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeWeights>>() {
				});

		return response;
	}

	public static ResponseEntity<WeighBridgeVehicleMst> getWeighBridgeVehicleById(String id) {
		String path = HOST + "findweighbridgevehiclemstbyid/" + id;
		System.out.println(path);
		ResponseEntity<WeighBridgeVehicleMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeVehicleMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<CategoryWiseStockMovement>> getItemWiseWiseStockMovement(String strtDate,
			String endDate) {
		String path = HOST + "categorywisestockmovementreportresource/itemwisestockmovementreport/"
				+ SystemSetting.systemBranch + "?fromdate=" + strtDate + "&&todate=" + endDate;
		System.out.println(path);
		ResponseEntity<List<CategoryWiseStockMovement>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryWiseStockMovement>>() {
				});

		return response;
	}

	public static void deleteWeighBridgeWeights(String id) {
		restTemplate.delete(HOST + "weighbridgeweights/deletebyid/" + id);
	}

	public static void updateWeighBridgeWeights(WeighBridgeWeights weighBridgeWeights) {
		restTemplate.put(HOST + "weighbridgereportresurce/upateweighbridgeweightbyid/", weighBridgeWeights);
		return;

	}

	public static ResponseEntity<List<SalesOrderDtl>> SalesOrderDtlByItemAndBatch(String Hdrid, String itemID) {
		String path = HOST + "saleorderdetail/" + Hdrid + "/" + itemID + "/getsaleorderdetailsbyitem";
		System.out.println(path);

		ResponseEntity<List<SalesOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderDtl>>() {
				});
		return response;
	}

	public static Summary getSalesWindowOrderSummaryDiscount(String id) {
		String path = HOST + "saleorderdtl/" + id + "/saleorderwindowsummaryfordiscount";
		System.out.println(path);
		return restTemplate.getForObject(path, Summary.class);
	}

	public static Summary getSalesOrderSummary(String id) {
		String path = HOST + "saleorderdtl/" + id + "/saleorderwindowsummary";
		System.out.println(path);
		return restTemplate.getForObject(path, Summary.class);
	}

	public static String callLoyaltyApi(String text) {
		return restTemplate.getForObject(HOST + "loyaltyvoucher/callloyalty?phno=" + text, String.class);
	}

	public static void deleteSalesOrderTransHdr(String id) {

		restTemplate.delete(HOST + "salesorderhdr/deletesalesorderhdr/" + id);
	}

//	public static ResponseEntity<List<CustomerMst>> CustomerMstByGst(String gst) {
//		String path = HOST + "customerregistration/customerbygst/" + gst;
//		System.out.println(path);
//
//		ResponseEntity<List<CustomerMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<CustomerMst>>() {
//				});
//		return response;
//	}

	public static ResponseEntity<SalesOrderDtl> getSalesOrderDtlById(String id) {
		String path = HOST + "saleorderdetail/getsaleorderdetailbyid/" + id;
		System.out.println(path);

		ResponseEntity<SalesOrderDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesOrderDtl>() {
				});
		return response;
	}

	public static ResponseEntity<ItemPropertyConfig> saveItemPropertyConfig(ItemPropertyConfig itemPropertyConfig) {
		return restTemplate.postForEntity(HOST + "itemconfiginstanceresource/createitemconfiginstance",
				itemPropertyConfig, ItemPropertyConfig.class);

	}

	public static void deleteItemPropertyConfig(String id) {
		restTemplate.delete(HOST + "itempropertyconfigresource/itempropertyconfigdelete" + id);

	}

	public static ResponseEntity<List<SaleOrderReport>> saleOrderConvertedReport(String date) {
		String path = HOST + "saleorderreportresource/soconvertedreport/" + SystemSetting.getSystemBranch() + "?rdate="
				+ date;
		System.out.println(path);

		ResponseEntity<List<SaleOrderReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderReport>>() {
				});
		return response;
	}

	public static ResponseEntity<ItemPropertyConfig> getItemPropertyConfigByItemNameAndProperty(String id,
			String propertyName) {
		String path = HOST + "itempropertyconfigresource/getitempropertyconfigbyitemidandpropertyname/" + id
				+ "?propertyname=" + propertyName;
		System.out.println(path);

		ResponseEntity<ItemPropertyConfig> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ItemPropertyConfig>() {
				});
		return response;
	}

	public static ResponseEntity<List<ItemPropertyConfig>> getAllItemPropertyConfig() {
		String path = HOST + "itempropertyconfigresource/findallitempropertyconfig";
		System.out.println(path);

		ResponseEntity<List<ItemPropertyConfig>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemPropertyConfig>>() {
				});
		return response;
	}

	public static String generateBatch(String itemId) {
		return restTemplate.getForObject(HOST + "purchasedtl/autogeneratebatch/" + itemId, String.class);
	}

	public static ResponseEntity<ItemPropertyInstance> saveItemPropertyInstance(
			ItemPropertyInstance itemPropertyInstance) {
		return restTemplate.postForEntity(
				HOST + "itempropertyinstanceresource/createitempropertyinstance/"
						+ itemPropertyInstance.getPurhcaseDtl().getId(),
				itemPropertyInstance, ItemPropertyInstance.class);

	}

	public static void delteItemPropertyInstance(String id) {
		restTemplate.delete(HOST + "itempropertyinstanceresource/itempropertyinstanceresourcedelete/" + id);

	}

	public static ResponseEntity<List<ItemPropertyConfig>> getItemPropertyConfigByItemId(String itemId) {

		String path = HOST + "itempropertyconfig/itempropertyconfigbyitemid/" + itemId;
		System.out.println(path);

		ResponseEntity<List<ItemPropertyConfig>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemPropertyConfig>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ItemPropertyInstance>> getItemPropertyInstanceByPurchaseDtl(String id) {
		String path = HOST + "itempropertyinstanceresource/getitempropertyinstancebypurchasedtl/"
				+ SystemSetting.systemBranch + "/" + id;
		System.out.println(path);

		ResponseEntity<List<ItemPropertyInstance>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemPropertyInstance>>() {
				});
		return response;
	}

	public static Boolean checkForAllPropertyInItemInstance(String itemid, String purchasedtlid) {
		return restTemplate.getForObject(HOST + "itempropertyinstanceresource/checkforallitempropertyinstance/"
				+ SystemSetting.systemBranch + "/" + itemid + "/" + purchasedtlid, Boolean.class);
	}

	public static ResponseEntity<ItemPropertyInstance> getitemPropertyInstanceByPurchaseDtlAndPropertyName(String id,
			String text) {
		String path = HOST + "itempropertyinstanceresource/getitempropertyinstancebypropertyandpurchasedtlid/"
				+ SystemSetting.systemBranch + "/" + id + "/" + text;
		System.out.println(path);

		ResponseEntity<ItemPropertyInstance> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ItemPropertyInstance>() {
				});
		return response;
	}

	public static ResponseEntity<List<SalesInvoiceReport>> getWholeSaleReportBetweenDate(String sdate, String edate) {
		String path = HOST + "salestranshdrreport/getwholesalesreport/" + SystemSetting.systemBranch + "?rdate=" + sdate
				+ "&&tdate=" + edate;
		System.out.println(path);

		ResponseEntity<List<SalesInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesInvoiceReport>>() {
				});
		return response;
	}

	public static ArrayList searchItemBatchExpDtlPopup(String data) {
		System.out.println(HOST + "itembatchexpirydtl/searchbyitemnameandbatch?data=" + data);
		return restTemplate.getForObject(HOST + "itembatchexpirydtl/searchbyitemnameandbatch?data=" + data,
				ArrayList.class);

	}

	public static void deleteSalesTransHdrBySalesOrderTransHdr(String orderId) {

		restTemplate.delete(HOST + "salestranshdr/salestranshdrdeletebyorderid/" + orderId);
	}

	public static ResponseEntity<DynamicProductionHdr> saveDynamicProductionHdr(
			DynamicProductionHdr dynamicproductionHdr) {
		return restTemplate.postForEntity(HOST + "dynamicproductionhdr/savedynamicproduction", dynamicproductionHdr,
				DynamicProductionHdr.class);

	}

	public static ResponseEntity<DynamicProductionDtl> saveDynamicProductionDtl(
			DynamicProductionDtl dynamicProductionDtl) {
		return restTemplate.postForEntity(
				HOST + "dynamicproductiondtl/savedynamicproductiondtl/"
						+ dynamicProductionDtl.getDynamicProductionHdr().getId(),
				dynamicProductionDtl, DynamicProductionDtl.class);

	}

	public static ResponseEntity<List<DynamicProductionDtl>> getDynamicProductionHdr(
			DynamicProductionHdr dynamicproductionHdr) {
		String path = HOST + "dynamicproductiondtl/getdynamicproductiondtlbyhdrid/" + dynamicproductionHdr.getId();
		System.out.println(path);

		ResponseEntity<List<DynamicProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DynamicProductionDtl>>() {
				});
		return response;
	}

	public static void updateDynamicProductionHdr(DynamicProductionHdr dynamicproductionHdr) {
		restTemplate.put(HOST + "dynamicproductionhdr/updatedynamicproduction/", dynamicproductionHdr);

	}

	public static void deleteDynamicProductionDtl(String id) {
		System.out.println(HOST + "dynamicproductiondtl/deletedynamicproduction/" + id);
		restTemplate.delete(HOST + "dynamicproductiondtl/deletedynamicproduction/" + id);

	}

	public static ResponseEntity<List<DayEndProcessing>> getDayEndReceiptMode(String strDate) {

		String path = HOST + "dayendreportresource/dayendreceiptmode/" + SystemSetting.systemBranch + "?reportdate="
				+ strDate;
		System.out.println(path);

		ResponseEntity<List<DayEndProcessing>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DayEndProcessing>>() {
				});
		return response;

	}

	public static ResponseEntity<List<KotItemMst>> getAllKotItemMstByItemId(String itemid) {
		String path = HOST + "kotitemmst/kotitemmstbyitemid/" + itemid;
		System.out.println(path);

		ResponseEntity<List<KotItemMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KotItemMst>>() {
				});
		return response;
	}

	public static ResponseEntity<SaleOrderRealizedMst> saveSaleOrderRealizedMst(
			SaleOrderRealizedMst saleOrderRealizedMst) {
		return restTemplate.postForEntity(HOST + "saleorderrealizedmst/savesaleorderrealizedmst", saleOrderRealizedMst,
				SaleOrderRealizedMst.class);

	}

	public static ResponseEntity<CurrencyConversionMst> saveCurrencyConversionMst(
			CurrencyConversionMst currencyConversionMst) {
		return restTemplate.postForEntity(HOST + "currencyconversionmst/savecompanycurrency", currencyConversionMst,
				CurrencyConversionMst.class);
	}

	public static ResponseEntity<List<CurrencyConversionMst>> getAllCurrencyConversionMst() {
		String path = HOST + "currencyconversionmst/getallcurrencyconversionmst";
		System.out.println(path);

		ResponseEntity<List<CurrencyConversionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CurrencyConversionMst>>() {
				});
		return response;
	}

	public static void deleteCurrencyConversionMst(String id) {
		restTemplate.delete(HOST + "currencyconversionmstresource/deletecurrencyconversionmst/" + id);

	}

	public static ArrayList SearchStockItemByBatchAndItemId(String searchData, String logDate) {
		System.out.println(HOST + "stockitempopupsearchwithbatchanditemid?date=" + logDate + "&&data=" + searchData);
		return restTemplate.getForObject(
				HOST + "stockitempopupsearchwithbatchanditemid?date=" + logDate + "&&data=" + searchData,
				ArrayList.class);
	}

	public static ResponseEntity<SalesDtl> saveSalesDtlWithCalculation(SalesDtl salesDtl) {
		String date = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-yyyy");
		String path = HOST + "salestranshdr/" + salesDtl.getSalesTransHdr().getId()
				+ "/savesalesdtlwithcalculation?logdate=" + date;
		System.out.println(path);

		return restTemplate.postForEntity(path, salesDtl, SalesDtl.class);
	}

	public static ResponseEntity<BranchCategoryMst> saveBranchCategory(BranchCategoryMst branchCategoryMst) {
		return restTemplate.postForEntity(HOST + "branchcategorymstresource/createbranchcategorymst", branchCategoryMst,
				BranchCategoryMst.class);
	}

	public static ResponseEntity<List<BranchCategoryMst>> getBranchCategory() {
		String path = HOST + "branchcategorymstresource/showallbranchcategorymst";
		System.out.println(path);

		ResponseEntity<List<BranchCategoryMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchCategoryMst>>() {
				});
		return response;
	}

	public static void deleteBranchCategoryMst(String id) {
		restTemplate.delete(HOST + "branchcategorymstresource/deletebranchcategorymst/" + id);

	}

	// -----------------------------SupplierPriceMst---------------------------------------------------

	public static ResponseEntity<SupplierPriceMst> createSupplierPriceMst(SupplierPriceMst supplierPriceMst) {
		return restTemplate.postForEntity(HOST + "supplierpricemstresource/savesupplierpricemst", supplierPriceMst,
				SupplierPriceMst.class);
	}

	public static ResponseEntity<List<SupplierPriceMst>> getSupplierPrice() {
		String path = HOST + "supplierpricemstresource/showallsupplierpricemst";
		System.out.println(path);

		ResponseEntity<List<SupplierPriceMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SupplierPriceMst>>() {
				});
		return response;
	}

	public static void deleteSupplierPriceMst(String id) {
		restTemplate.delete(HOST + "supplierpricemstresource/deletesupplierpricemst/" + id);

	}

	// --------------------branch price defenition --------------------

	public static ResponseEntity<BranchPriceDefenitionMst> createBranchPriceDefenitionMst(
			BranchPriceDefenitionMst branchPriceDefenitionMst) {
		return restTemplate.postForEntity(HOST + "branchpricedefinition/savebranchpricedefinition",
				branchPriceDefenitionMst, BranchPriceDefenitionMst.class);
	}

	public static ResponseEntity<List<BranchPriceDefenitionMst>> getBranchPriceDefenition() {
		String path = HOST + "branchpricedefinitionresource/showallbranchpricedefinition";
		System.out.println(path);

		ResponseEntity<List<BranchPriceDefenitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchPriceDefenitionMst>>() {
				});
		return response;
	}

	public static void deleteBranchPriceDefenitionMst(String id) {
		System.out.print(HOST + "branchpricedefinition/deletebranchpricedefinition/" + id);
		restTemplate.delete(HOST + "branchpricedefinition/deletebranchpricedefinition/" + id);

	}

	// --------------------Task Approve --------------------

	public static ResponseEntity<TaskApproveMst> createTaskApproveMst(TaskApproveMst taskApproveMst) {
		return restTemplate.postForEntity(HOST + "taskapprovemst/savetaskapprovemst", taskApproveMst,
				TaskApproveMst.class);
	}

	public static ResponseEntity<List<TaskApproveMst>> getTaskApproveMst() {
		String path = HOST + "taskapprovemstresource/showalltaskapprovemst";
		System.out.println(path);

		ResponseEntity<List<TaskApproveMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaskApproveMst>>() {
				});
		return response;
	}

	public static void deleteTaskApproveMst(String id) {
		System.out.print(HOST + "taskapprovemst/deletetaskapprovemst/" + id);
		restTemplate.delete(HOST + "taskapprovemst/deletetaskapprovemst/" + id);

	}

	public static ResponseEntity<List<PDCPayment>> getPDCPaymentReportByAccountIdAndStatus(String accountId,
			String status) {
		String path = HOST + "pdcpaymentbyaccountid/" + accountId + "/" + status;
		System.out.println(path);

		ResponseEntity<List<PDCPayment>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PDCPayment>>() {
				});
		return response;
	}

	public static ResponseEntity<StockTransferInHdr> getStockTransferInHdrByHdrId(String hdrId) {

		String path = HOST + "stocktransfeinhdrresource/getstocktransferinhdrbyid/" + hdrId;
		System.out.println(path);

		ResponseEntity<StockTransferInHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<StockTransferInHdr>() {
				});
		return response;
	}

	public static ResponseEntity<StockTransferInHdr> getStockTransferInHdrByVoucherNumberIn(String voucherNumberIn) {
		String path = HOST + "stocktransfeinhdrresource/getstocktransferinhdrbyvouchernumberin/" + voucherNumberIn;
		System.out.println(path);

		ResponseEntity<StockTransferInHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<StockTransferInHdr>() {
				});
		return response;
	}

	public static ResponseEntity<List<PDCReceipts>> getPDCReceiptsReportRestReportByAccountIdAndStatus(String accountId,
			String string) {
		String path = HOST + "pdcreceipts/pdcreceiptsbyaccountidandstatus/" + accountId + "/" + string;
		System.out.println(path);

		ResponseEntity<List<PDCReceipts>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PDCReceipts>>() {
				});
		return response;
	}

	public static ResponseEntity<ProductionMst> getProductionMstbyHdrId(String hdrId) {

		String path = HOST + "productionmstbyproductionHdr/" + hdrId;

		ResponseEntity<ProductionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ProductionMst>() {
				});

		return response;

	}

	public static ResponseEntity<UserCreatedTask> createUserCreatedTask(UserCreatedTask userCreatedTask) {
		return restTemplate.postForEntity(HOST + "usercreatetaskresource/saveusertaskbyusername", userCreatedTask,
				UserCreatedTask.class);
	}

	public static ArrayList SearchUserByName(String searchData) {

		return restTemplate.getForObject(HOST + "usermstresource/usersearch?data=" + searchData, ArrayList.class);

	}

	// --------------------Debit note --------------------

	public static ResponseEntity<DebitNote> createDebitNote(DebitNote debitNote) {
		return restTemplate.postForEntity(HOST + "debitnoteresource/savedebitnotedetails", debitNote, DebitNote.class);
	}

	public static ResponseEntity<List<DebitNote>> getDebitNote() {
		String path = HOST + "debitnoteresource/getdebitnotedetails";
		System.out.println(path);

		ResponseEntity<List<DebitNote>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DebitNote>>() {
				});
		return response;
	}

	public static void deleteDebitNote(String id) {
		System.out.print(HOST + "debitnoteresource/deletedebitnotedetails/" + id);
		restTemplate.delete(HOST + "debitnoteresource/deletedebitnotedetails/" + id);

	}
	// --------------------------------------Debit note
	// End---------------------------------------//

	// --------------------Credit note --------------------

	public static ResponseEntity<CreditNote> createCreditNote(CreditNote creditNote) {
		return restTemplate.postForEntity(HOST + "creditnoteresource/savecreditnotedetails", creditNote,
				CreditNote.class);
	}

	public static ResponseEntity<List<CreditNote>> getCreditNote() {
		String path = HOST + "creditnoteresource/getcreditnotedetails";
		System.out.println(path);

		ResponseEntity<List<CreditNote>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CreditNote>>() {
				});
		return response;
	}

	public static void deleteCreditNote(String id) {
		System.out.print(HOST + "creditnoteresource/deletecreditnotedetails/" + id);
		restTemplate.delete(HOST + "creditnoteresource/deletecreditnotedetails/" + id);

	}

	// --------------------------------------Credit note
	// End---------------------------------------//

	static public ArrayList SearchOtherBranchSaleVoucherNo(String searchData) {
		System.out.println(HOST + "otherbranchssalestranshdrresource/getotherbranchpopup?data=" + searchData);
		return restTemplate.getForObject(
				HOST + "otherbranchssalestranshdrresource/getotherbranchpopup?data=" + searchData, ArrayList.class);

	}

	public static ResponseEntity<String> FetchOtherBranchSalesTransHdrById(String id) {
		String path = HOST + "otherbranchsalestranshdr/" + id + "/getotherbranchsalesbyid";
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

//---------------------------GoodReceiveNoteHdr----------------------------------------//

	public static ResponseEntity<GoodReceiveNoteHdr> saveGoodReceiveNoteHdr(GoodReceiveNoteHdr goodReceiveNoteHdr) {
		return restTemplate.postForEntity(HOST + "goodreceivenotehdrresource/savesgoodreceivenotehdr",
				goodReceiveNoteHdr, GoodReceiveNoteHdr.class);
	}

	public static ResponseEntity<List<GoodReceiveNoteHdr>> getGoodReceiveNoteHdr() {
		String path = HOST + "goodreceivenotehdrresource/getgoodreceivenotehdr";
		System.out.println(path);

		ResponseEntity<List<GoodReceiveNoteHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GoodReceiveNoteHdr>>() {
				});
		return response;
	}

	// ---------------------------GoodReceiveNoteHdr End
	// ----------------------------------------//

	// ---------------------------GoodReceiveNoteDtl----------------------------------------//
	public static ResponseEntity<GoodReceiveNoteDtl> createGoodReceiveNoteDtl(GoodReceiveNoteDtl goodReceiveNoteDtl) {
		return restTemplate.postForEntity(HOST + "goodreceivenotedtlresource/savegoodreceivenotedtl",
				goodReceiveNoteDtl, GoodReceiveNoteDtl.class);
	}

	

	public static void deleteGoodReceiveNoteDtl(String id) {
		System.out.print(HOST + "goodreceivenotedtlresource/deletegoodreceivenotedtl/" + id);
		restTemplate.delete(HOST + "goodreceivenotedtlresource/deletegoodreceivenotedtl/" + id);

	}

	public static ResponseEntity<List<BranchSaleReport>> getBranchSaleReportSummary(String strDate, String toDate) {
		String path = HOST + "salesinvoicereport/getbranchsalehdrreport/" + SystemSetting.systemBranch + "?fromdate="
				+ strDate + "&&todate=" + toDate;
		System.out.println(path);
		ResponseEntity<List<BranchSaleReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchSaleReport>>() {
				});

		return response;
	}

	public static void deletePurchaseReturnDtlById(String id) {
		restTemplate.delete(HOST + "purchasereturndtlresource/deletepurchasereturndtl/" + id);

	}

	// -------------------------------------------End
	// ---------------------------------------//

	static public ResponseEntity<List<VoucherReprintDtl>> getVoucherReprintDtlWithDate(String voucherNumber,
			String branchName, String date) {

		String path = HOST + "voucherreprintbydate/" + voucherNumber + "/" + branchName + "?date=" + date;
		System.out.println(path);

		ResponseEntity<List<VoucherReprintDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<VoucherReprintDtl>>() {
				});

		return response;

	}

	public static ResponseEntity<PurchaseHdr> purchaseToTallyDirect(PurchaseHdr purchaseHdr) {
		return null;

	}

	public static ResponseEntity<String> ReceiptToTally(String startDate) {
		String path = HOST + "receiptstally/" + startDate;
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;
	}

	public static ResponseEntity<String> PaymentToTally(String startDate) {
		String path = HOST + "paymentstally/" + startDate;
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	public static ResponseEntity<List<SalesManMst>> getSalesManMstByName(String name) {
		String path = HOST + "salesmanmstresource/findsalesmanbyname/" + name;
		System.out.println(path);

		ResponseEntity<List<SalesManMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesManMst>>() {
				});

		return response;
	}

	static public void updateAccountReceivableWithVoucherDate(AccountReceivable accountReceivable) {

		restTemplate.put(
				HOST + "accountreceivable/updateaccountreceivablebyvouchernumberanddate/"
						+ accountReceivable.getVoucherNumber() + "?date="
						+ SystemSetting.UtilDateToString(accountReceivable.getVoucherDate(), "yyyy-MM-dd"),
				accountReceivable, AccountReceivable.class);

		return;
	}

	public static ResponseEntity<AccountReceivable> getAccountRecByVoucherIdAndDate(String invoiceNumber, String date) {
		String path = HOST + "accountreceivable/fetchaccountreceivablebyvouchernumberanddate/" + invoiceNumber
				+ "?rdate=" + date;
		System.out.println(path);

		ResponseEntity<AccountReceivable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountReceivable>() {
				});
		return response;
	}

	public static ResponseEntity<List<DailySalesReport>> TotalDailySalesSummary(String date) {
		String path = HOST + "dailysalesreport?rdate=" + date;
		System.out.println(path);

		ResponseEntity<List<DailySalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DailySalesReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ReceiptModeReport>> ReceiptModeWiseDailySale(String branchCode, String sdate) {
		String path = HOST + "dailysalesreportresource/receiptmodewisesalesandsoconverted/" + branchCode + "?rdate="
				+ sdate;
		System.out.println(path);

		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ReceiptModeReport>> TodaysSOAdvanceReport(String branchCode, String sdate) {
		String path = HOST + "dailysalesreportresource/soadvancereceivedtoday/" + branchCode + "?rdate=" + sdate;
		System.out.println(path);

		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ReceiptModeReport>> TodaysReceiptByReceiptMode(String branchCode, String sdate) {
		String path = HOST + "dailysalesreportresource/receiptbyreceiptmodewisereport/" + branchCode + "?rdate="
				+ sdate;
		System.out.println(path);

		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});
		return response;
	}

	public static ResponseEntity<Double> TodaysTotalCard(String branchCode, String sdate) {
		String path = HOST + branchCode + "/cardreceiptsalesandso/" + sdate;
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<DayEndPettyCashPaymentAndReceipt> DayEndPettyCashPaymentAndReceipt(String accountid,
			String sdate, String branchCode) {
		String path = HOST + "dailysalesreportresource/dayendpettycashreceiptandpayment/" + branchCode + "/" + accountid
				+ "?rdate=" + sdate;
		System.out.println(path);

		ResponseEntity<DayEndPettyCashPaymentAndReceipt> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<DayEndPettyCashPaymentAndReceipt>() {
				});
		return response;
	}

	public static ResponseEntity<Double> TodaysAdvanceByReceiptMode(String sdate, String mode) {
		String path = HOST + "saleorderreceipt/getsoreceiptbymode/" + mode + "?rdate=" + sdate;
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<Double> getTodaysTotalReceiptByAccountId(String branchCode, String accountid,
			String sdate) {
		String path = HOST + "dailysalesreportresource/dayendreceiptcash/" + branchCode + "/" + accountid + "?rdate="
				+ sdate;
		System.out.println("SSSSSSSSSSSSSSSSSSSSSSSSSS");

		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<Double> TodaysCashSaleByReceiptMode(String sdate, String receiptmode) {
		String path = HOST + "salesreceiptsbymodeanddate/" + receiptmode + "?fdate=" + sdate;
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<Double> TodaysTotalAdvanceByDate(String sdate) {
		String path = HOST + "saleorderreceipt/getalladvancefordate?rdate=" + sdate;
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<Double> DayEndPettyCashPaymentAndReceiptByAccontId(String accountid, String sdate,
			String branchCode) {
		String path = HOST + "dayendreportresource/paymentbyaccountid/" + branchCode + "/" + accountid + "?reportdate="
				+ sdate;
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

//---------Sibi------09/04/2021--------------//
	public static ResponseEntity<String> purchaseToTally(String startDate) {
		String path = HOST + "purchasetotallydirectdate/" + startDate;
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;

	}

	public static ResponseEntity<String> journalToTally(String startDate) {
		String path = HOST + "journaltotallydirectdate/" + startDate;
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;

	}

	public static ResponseEntity<String> salesToTally(String startDate) {
		// TODO Auto-generated method stub
		return null;
	}

	public static ResponseEntity<Double> TodaysTotalSales(String strDate, String dpToDate) {

		String path = HOST + "dailysalesreport/totalsales?sdate=" + strDate + "&&edate=" + dpToDate;
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<Double> TodaysPreviousAdvance(String sdate) {

		String path = HOST + "dailysalesreport/previousadvance/" + SystemSetting.systemBranch + "?date=" + sdate;
		System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPP");
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<Double> getTodaysTotalReceiptByReceiptMode(String branchCode, String receiptmode,
			String sdate) {

		String path = HOST + "dailysalesreportresource/dayendreceiptbyreceiptmode/" + branchCode + "/" + receiptmode
				+ "?rdate=" + sdate;
		System.out.println("@@@@@@@@@@@@@@@RRRRRRRRRR");
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<VoucherNumberDtlReport> PosVoucherNumberDtlReport(String posPrefix, String date) {

		String path = HOST + "dayendreportresource/" + SystemSetting.systemBranch + "/posvouchernumbersummary/"
				+ posPrefix + "?reportdate=" + date;

		System.out.println("pathpathpathpathpathpathpathpathpathpathpathpathpathpathpathpathpath");

		System.out.println(path);

		ResponseEntity<VoucherNumberDtlReport> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<VoucherNumberDtlReport>() {
				});
		return response;
	}
	// -----------------------------ItemNutritions---------------------------------------------------

	public static ResponseEntity<ItemNutritionMst> createItemNutritions(ItemNutritionMst itemNutritionMsts) {
		return restTemplate.postForEntity(HOST + "itemnutritionresource/saveitemnutrition", itemNutritionMsts,
				ItemNutritionMst.class);
	}

	public static ResponseEntity<List<ItemNutritionMst>> getItemNutritions() {
		String path = HOST + "itemnutritionresource/showallitemnutrition";
		System.out.println(path);

		ResponseEntity<List<ItemNutritionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemNutritionMst>>() {
				});
		return response;
	}

	public static void deleteItemNutritions(String id) {
		restTemplate.delete(HOST + "itemnutritionresource/deleteitemnutrition/" + id);

	}
	// -----------------------------ItemNutritions
	// End---------------------------------------------------
	// -----------------------------DoctorMst---------------------------------------------------

	public static ResponseEntity<NewDoctorMst> createNewDoctorMst(NewDoctorMst newDoctorMst) {
		return restTemplate.postForEntity(HOST + "newdoctormstresource/savenewdoctormst", newDoctorMst,
				NewDoctorMst.class);
	}

	public static ResponseEntity<List<NewDoctorMst>> getNewDoctorMst() {
		String path = HOST + "newdoctormstresource/showallnewdoctormst";
		System.out.println(path);

		ResponseEntity<List<NewDoctorMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<NewDoctorMst>>() {
				});
		return response;
	}

	public static void deleteNewDoctorMst(String id) {
		restTemplate.delete(HOST + "newdoctormstresource/deletenewdoctormst/" + id);

	}
	// -----------------------------DoctorMst
	// End---------------------------------------------------

	public static ResponseEntity<BarcodeBatchMst> saveBarcodeBatchMst(BarcodeBatchMst barcodeBatchMst) {

		System.out.println(HOST + "barcodebatchmstresource/barcodebatchmst");
		return restTemplate.postForEntity(HOST + "barcodebatchmstresource/barcodebatchmst", barcodeBatchMst,
				BarcodeBatchMst.class);
	}

	public static ResponseEntity<BarcodeBatchMst> getBarcodeBatchMstById(String barcodestring) {

		String path = HOST + "barcodebatchmstresource/getbarcodebatchmstbyid/" + barcodestring;

		System.out.println(path);

		ResponseEntity<BarcodeBatchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BarcodeBatchMst>() {
				});
		return response;
	}

	public static ResponseEntity<List<ReceiptModeReport>> ReceiptModeWisePreviousSales(String branchCode,
			String sdate) {
		String path = HOST + "dailysalesreportresource/receiptmodewiseprevioussalesreport/" + branchCode + "?rdate="
				+ sdate;
		System.out.println("@@@@@@@@@@@@@@@RRRRRRRRRR");

		System.out.println(path);

		ResponseEntity<List<ReceiptModeReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReceiptModeReport>>() {
				});
		return response;
	}

	public static ResponseEntity<Double> TodaysPreviousCashAdvance(String sdate) {
		String path = HOST + "dailysalesreportresource/receiptmodewisepreviouscashsalesreport/"
				+ SystemSetting.systemBranch + "?rdate=" + sdate;
		System.out.println("@@@@@@@@@@@@@@@RRRRRRRRRR");
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	// -----------------Amdc report 30-04-2021-----------------------

//	static public ResponseEntity<AMDCPurchaseReturnSummaryReport> saveAMDCPurchaseReturnDtl(AMDCPurchaseReturnSummaryReport amdcPurchaseReturnDtl,
//			String amdcPurchasereturnhdrid) {
//		System.out.println(HOST + "amdcpurchasereturnsummaryreportresource/saveamdcpurchasereturndtl");
//		return restTemplate.postForEntity(
//				HOST + "amdcpurchasereturnsummaryreportresource/" + amdcPurchasereturnhdrid + "/saveamdcpurchasereturndtl", amdcPurchaseReturnDtl,
//				AMDCPurchaseReturnSummaryReport.class);
//
//	}

	// -----------------------------ParamMst--------(anandu)-------------------------------------------

	public static ResponseEntity<ParamMst> createParamMst(ParamMst paramMst) {
		return restTemplate.postForEntity(HOST + "parammstresource/saveparammst", paramMst, ParamMst.class);
	}

	public static ResponseEntity<ParamMst> ParamMstByName(String string) {
		String path = HOST + "parammstresource/findbyname/" + string;

		ResponseEntity<ParamMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ParamMst>() {
				});

		return response;
	}

	// -------------------2021-05-1 Anandhu end

//---------------------------local purchase details report--------------------
	static public ResponseEntity<List<LocalPurchaseDetailReport>> getLocalPurchaseDtlInvoice(String voucherNumber,
			String date) {

		String path = HOST + "localpurchasedetailreport/" + voucherNumber + "/" + SystemSetting.systemBranch + "?rdate="
				+ date;

		System.out.println(path);
		System.out.println("purchaseeee 1111333333333333322222222222222255555566666777777111111");
		ResponseEntity<List<LocalPurchaseDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LocalPurchaseDetailReport>>() {
				});

		return response;

	}

	static public ResponseEntity<List<LocalPurchaseDetailReport>> getLocalPurchaseTax(String voucherNumber,
			String date) {

		String path = HOST + "retrivetaxrate/" + voucherNumber + "/" + SystemSetting.systemBranch + "?rdate=" + date;

		System.out.println(path);

		ResponseEntity<List<LocalPurchaseDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LocalPurchaseDetailReport>>() {
				});

		return response;

	}

	// ---------------------------local purchase details report End
	// (30/04/2021)--------

	// -----------------------------TaxLedger---------------------------------------------------

	public static ResponseEntity<TaxLedger> createTaxLedger(TaxLedger taxLedger) {
		return restTemplate.postForEntity(HOST + "taxledgerresource/savetaxledger", taxLedger, TaxLedger.class);
	}

	public static ResponseEntity<List<TaxLedger>> getTaxLedger() {
		String path = HOST + "taxledgerresource/showalltaxledger";
		System.out.println(path);

		ResponseEntity<List<TaxLedger>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TaxLedger>>() {
				});
		return response;

	}

	public static void deleteTaxLedger(String id) {
		restTemplate.delete(HOST + "taxledgerresource/deletetaxledger/" + id);

	}

	// --------------------------------------------TaxLedger
	// End---------------------------------------
	public static ResponseEntity<Double> TodaysTotalCardSales(String branchCode, String sdate) {

		String path = HOST + branchCode + "/salesreceipts/totalcardreceiptsalesandso/" + "?date=" + sdate;
		System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPP");
		System.out.println(path);

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {
				});
		return response;
	}

	public static ResponseEntity<BarcodeBatchMst> barcodeBatchMstByBarcodeAndBatch(String barcode, String batch) {
		String path = HOST + "barcodebatchmstresource/getbarcodebatchmstbybarcodeandbatch/" + barcode + "/" + batch
				+ "/" + SystemSetting.getUser().getBranchCode();
		System.out.println(path);

		ResponseEntity<BarcodeBatchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BarcodeBatchMst>() {

				});
		return response;
	}

	public static ResponseEntity<String> saveKotOrder(List<SalesDtl> salesDtlList) {
		return restTemplate.postForEntity(
				HOST + "salesdetails/savekotordersalesdtl/" + SystemSetting.getUser().getBranchCode(), salesDtlList,
				String.class);
	}

//	// -------------ProfitAndLossConfig------anandu------------
//
//	public static ResponseEntity<ProfitAndLossConfigHdr> createProfitAndLossConfigHdr(
//			ProfitAndLossConfigHdr profitAndLossConfigHdr) {
//		return restTemplate.postForEntity(HOST + "profitandlossconfighdrresource/saveprofitandlossconfighdr",
//				profitAndLossConfigHdr, ProfitAndLossConfigHdr.class);
//	}
//
//	public static ResponseEntity<ProfitAndLossConfigDtl> createProfitAndLossConfigDtl(
//			ProfitAndLossConfigDtl profitAndLossConfigDtl) {
//		return restTemplate.postForEntity(HOST + "profitandlossconfigdtlresource/saveprofitandlossconfigdtl",
//				profitAndLossConfigDtl, ProfitAndLossConfigDtl.class);
//	}
//
//	public static ResponseEntity<List<ProfitAndLossConfigHdr>> getProfitAndLossConfigHdrList() {
//		String path = HOST + "profitandlossconfigdtlresource/getallpandlconfighdr";
//		System.out.println(path);
//
//		ResponseEntity<List<ProfitAndLossConfigHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<ProfitAndLossConfigHdr>>() {
//
//				});
//		return response;
//	}
//
//	public static ResponseEntity<ProfitAndLossConfigHdr> ProfitAndLossConfigHdrByName(String accountName) {
//
//		String path = HOST + "profitandlossconfigdtlresource/getpandlconfighdrbyname/" + accountName + "/"
//				+ SystemSetting.getUser().getBranchCode();
//		System.out.println(path);
//
//		ResponseEntity<ProfitAndLossConfigHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<ProfitAndLossConfigHdr>() {
//
//				});
//		return response;
//
//	}
//
//	public static ResponseEntity<ProfitAndLossConfigDtl> saveProfitAndLossConfigDtl(
//			ProfitAndLossConfigDtl profitAndLossConfigDtl) {
//		return restTemplate.postForEntity(HOST + "profitandlossconfigdtlresource/saveprofitandlossconfigdtl",
//				profitAndLossConfigDtl, ProfitAndLossConfigDtl.class);
//	}
//
	public static ResponseEntity<String> InitializeProfitAndLossConfigHdr() {
		String path = HOST + "profitandlossconfigdtlresource/initializepandlhdr/"
				+ SystemSetting.getUser().getBranchCode();
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {

				});
		return response;
	}
//
//	public static ResponseEntity<List<ProfitAndLossConfigDtl>> getAllProfitAndLossConfigDtl() {
//		String path = HOST + "profitandlossconfigdtlresource/getallpandlconfigdtl";
//		System.out.println(path);
//
//		ResponseEntity<List<ProfitAndLossConfigDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<ProfitAndLossConfigDtl>>() {
//
//				});
//		return response;
//	}
//
//	public static ResponseEntity<ProfitAndLossConfigHdr> getProfitAndLossConfigHdrById(String hdrId) {
//		String path = HOST + "profitandlossconfighdrresource/getpandlconfighdrbyid/" + hdrId;
//		System.out.println(path);
//
//		ResponseEntity<ProfitAndLossConfigHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<ProfitAndLossConfigHdr>() {
//
//				});
//		return response;
//	}
//
//	public static void deleteProfitAndLossConfigDtl(String id) {
//
//		restTemplate.delete(HOST + "balancesheetconfigdtlresource/" + id + "/deletebalancesheetconfigdtl");
//	}

	public static ArrayList SearchParamByName(String searchData) {

		System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
		System.out.println(HOST + "paramsearch?data=" + searchData);
		return restTemplate.getForObject(HOST + "paramsearch?data=" + searchData, ArrayList.class);

	}

	public static void deleteBalanceSheetConfigHdr(String id) {

		restTemplate.delete(HOST + "balancesheetconfigresource/deletebalancesheetconfig/" + id);

	}

	public static ResponseEntity<String> InitializeBalanceSheetConfigHdr() {
		String path = HOST + "balancesheetconfighdrresource/initializebalancesheethdr/"
				+ SystemSetting.getUser().getBranchCode();
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {

				});
		return response;
	}

	// -------------ProfitAndLossConfig---end-----------------

//	public static ResponseEntity<PurchaseHdr> savePurchaseHdrFromPurchaseOrderHdr(
//			ResponseEntity<PurchaseOrderHdr> purchaseHdrSaved) {
//		return restTemplate.postForEntity(HOST + "purchasehdrresource", purchaseHdrSaved, PurchaseHdr.class);
//	}

//	public static ResponseEntity<PurchaseOrderDtl> getPurchaseOrderDtl(String id) {
//		//purchaseorderhdr/{purchaseorderHdrId}/purchaseorderdtl
//		String path = HOST + "purchaseorderhdr/"+id+"/purchaseorderdtl";
//		System.out.println(path);
//
//		ResponseEntity<PurchaseOrderDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
//				new ParameterizedTypeReference<PurchaseOrderDtl>() {
//
//				});
//		return response;
//	}

//	static public ResponseEntity<PurchaseHdr> savePurchaseHdrFromPurchaseOrderHdr(PurchaseHdr purchaseHdr) {
//
//		return restTemplate.postForEntity(HOST + "purchasehdr", purchaseHdr, PurchaseHdr.class);
//
//	}

	// ------------------------Sibi-------30/04/2021.............//

	public static ResponseEntity<List<PharmacyBranchStockReport>> getPharmacyBranchStockReport(String fdate,
			String tDate, String branchCode, String category, String supplierName) {
		String path = HOST + "pharmacybranchstockreportresource/" + category + "/getpharmacybranchstockreport/"
				+ branchCode + "?fdate=" + fdate + "&&tdate=" + tDate + supplierName;

		System.out.println(path);

		ResponseEntity<List<PharmacyBranchStockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyBranchStockReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<WriteOffDetailAndSummaryReport>> getPharmacyWriteOffSummaryReport(String fdate,
			String tDate, String branchCode, String itemgroup) {

		String path = HOST + "writeoffsummaryreportresource/" + itemgroup + "/getpharmacywriteoffsummaryreport/"
				+ branchCode + "?fdate=" + fdate + "&&tdate=" + tDate;

		System.out.println(path);

		ResponseEntity<List<WriteOffDetailAndSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WriteOffDetailAndSummaryReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<LocalPurchaseSummaryReport>> getLocalPurchaseSummaryReport(String fdate,
			String tDate, String branchCode, String itemgroup, String supplierName) {

		String path = HOST + "localpurchasesummayresource/" + itemgroup + "/getlocalpurchasesummayreport/" + branchCode
				+ "?fdate=" + fdate + "&&tdate=" + tDate + supplierName;

		System.out.println(path);

		ResponseEntity<List<LocalPurchaseSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LocalPurchaseSummaryReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PharmacyPatientType>> getallpatienttype() {
		String path = HOST + "pharmacypatienttyperresource/getallpatienttype/";
		System.out.println(path);
		ResponseEntity<List<PharmacyPatientType>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyPatientType>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PharmacyPatientType>> getPharmacyPatientTypeidByPatientType(String patienttype) {
		String path = HOST + "pharmacypatienttyperresource/getpharmacypatienttypeidbypatienttype/" + patienttype;
		System.out.println(path);
		ResponseEntity<List<PharmacyPatientType>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyPatientType>>() {
				});

		return response;
	}

	public static ResponseEntity<PharmacyPatientType> getPatientTypeById(String patienttypeid) {

		String path = HOST + "pharmacypatienttyperresource/getpatienttypebyid/" + patienttypeid;
		System.out.println(path);
		ResponseEntity<PharmacyPatientType> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<PharmacyPatientType>() {
				});

		return response;

	}

	static public ResponseEntity<String> converSalesOrderToSales(String salesorderhdr, String userid) {
//{companymstid}/salesordertranshdr/salesordertosales/{salesordertanshdrid}/{userid}
		String path = HOST + "salesordertranshdr/salesordertosales/" + salesorderhdr + "/"
				+ SystemSetting.getUser().getId();

		System.out.println("========path==========" + path);

		return restTemplate.postForEntity(path, "", String.class);

	}

	// ====================new receiptmode ======anandu=======

	public static ResponseEntity<String> getInitializeReceiptModeMstByName() {
		String path = HOST + "receiptmodemstresource/findnewreceiptmode/getnewrecieptmodebyname/"
				+ SystemSetting.systemBranch;
		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;
	}

	// =================end new receiptmode============

	// ============================================Pharmacy Sales
	// Order========================
	public static ResponseEntity<SalesOrderTransHdr> deleteSalesReceiptsByTransHdr(
			SalesOrderTransHdr salesOrderTransHdr) {
		return restTemplate.postForEntity(HOST + "/salesordertranshdr/sales_receipts/delete", salesOrderTransHdr,
				SalesOrderTransHdr.class);
	}

	public static ResponseEntity<AccountReceivable> getAccountReceivableBySalesOrderTransHdrId(
			String salesordertranshdrid) {
		String path = HOST + "accountreceivable/fetchaccreceivablebysalesordertranshdr/" + salesordertranshdrid;
		System.out.println(path);
		ResponseEntity<AccountReceivable> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<AccountReceivable>() {
				});
		return response;
	}

	static public SalesOrderTransHdr getSalesOrderTransHdr(String salesordertranshdrid) {

		String path = HOST + "salesordertranshdr/" + salesordertranshdrid;
		ResponseEntity<SalesOrderTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesOrderTransHdr>() {
				});

		return response.getBody();

	}

	static public ResponseEntity<List<SalesOrderDtl>> getSalesOrderDtlByItemAndBatch(String Hdrid, String itemID,
			String batch) {
		// @GetMapping("{companymstid}/salesorderdetail/{salesordertranshdr}/{itemid}/{batch}/getsalesorderdetailsbyitem")

		String path = HOST + "salesorderdetail/" + Hdrid + "/" + itemID + "/" + batch + "/getsalesorderdetailsbyitem";
		System.out.println("=============path=========" + path);

		ResponseEntity<List<SalesOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderDtl>>() {
				});

		return response;
	}

	static public Double SalesOrderDtlItemQty(String hdrId, String itemId, String batch) {
		System.out.println("Items" + HOST + "salesorderdetail/" + hdrId + "/" + itemId + "/getsalesorderdetailqty");
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(
				HOST + "salesorderdetail/bybatch/" + hdrId + "/" + itemId + "/getsalesorderdetailqty?batch=" + batch,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}

	public static ResponseEntity<SalesOrderTransHdr> getNullSalesOrderTransHdrByCustomer(String customerId,
			String sdate, String customSalesMode) {
		String path = HOST + "salesorderhdr/getnullvoucherbbycustomerid/" + customerId + "/" + customSalesMode
				+ "?sdate=" + sdate;
		System.out.println(path);
		ResponseEntity<SalesOrderTransHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<SalesOrderTransHdr>() {
				});

		return response;
	}

	// -------------------------ReceiptModepopup---------------------------------
	public static ArrayList SearchReceiptModeByName(String searchData) {

		System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
		System.out.println(HOST + "receiptmodesearch?data=" + searchData);

		return restTemplate.getForObject(HOST + "receiptmodemst/receiptmodesearch?data=" + searchData, ArrayList.class);

	}

	// ========================================Initialize===Local Customer
	// Mst====================
	public static ResponseEntity<String> InitializeLocalCustomer() {
		String path = HOST + "localcustomerresource/initializelocalcustomer";

		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {

				});
		return response;
	}

	public static ResponseEntity<String> getInitializeInsuranceCompanyMst() {
		String path = HOST + "insurancetypeinitializeresource/getinitializeinsurancecompanymst/"
				+ SystemSetting.systemBranch;
		;
		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	public static ResponseEntity<List<SalesTransHdr>> getSalesTranHdrByorderId(String id) {
		String path = HOST + "salestranshdr/getsalestranshdrbyorderid/" + id;
		System.out.println(path);
		ResponseEntity<List<SalesTransHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesTransHdr>>() {
				});
		return response;
	}

	// =================Gst output detail report========anandu======

	public static ResponseEntity<List<GSTOutputDetailReport>> getLedgerClassDetails(String fdate, String tdate) {
		String path = HOST + "gstoutputdetailreportresource/getledgerclassdetails" + "?fromdate=" + fdate + "&&todate="
				+ tdate;
		System.out.println(path);
		ResponseEntity<List<GSTOutputDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GSTOutputDetailReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<GSTOutputDetailReport>> getGstOutputDetailReport(String fdate, String tDate) {
		String path = HOST + "gstoutputdetailreportresource/getledgerclassdetails" + "?fromdate=" + fdate + "&&todate="
				+ tDate;
		System.out.println(path);
		ResponseEntity<List<GSTOutputDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GSTOutputDetailReport>>() {
				});
		return response;
	}

	// ==========================end=============================

	// =================Gst input detail report========anandu======

	public static ResponseEntity<List<GSTInputDtlAndSmryReport>> getGstInputDetails(String sfdate, String stdate) {
		String path = HOST + "gstinputdtlandsmryreportresource/getgstinputdtlandsmry" + "?fromdate=" + sfdate
				+ "&&todate=" + stdate;
		System.out.println(path);
		ResponseEntity<List<GSTInputDtlAndSmryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GSTInputDtlAndSmryReport>>() {
				});
		return response;
	}

//
//		public static ResponseEntity<List<ImportPurchaseSummary>> getImportPurchaseSummary(LocalDate fdate, LocalDate tdate) {
//			
//			String path = HOST + "/purchaserhdrresource/getimportpurchasesummary/"  + "?fromdate=" + fdate
//					+ "&&todate=" + tdate;
//			ResponseEntity<List<ImportPurchaseSummary>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//					new ParameterizedTypeReference<List<ImportPurchaseSummary>>() {
//					});
//			return response;
//			
//			
//		}

	public static ResponseEntity<List<NewDoctorMst>> getNewDoctorMstByName(String name) {
		String path = HOST + "newdoctormstresource/getdoctormstbyname/" + name;
		System.out.println(path);
		ResponseEntity<List<NewDoctorMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<NewDoctorMst>>() {
				});
		return response;
	}

//===================================================InsurenceCompanyDtl================
	public static ResponseEntity<List<InsuranceCompanyDtl>> getAllInsuranceCompanyDtl() {
		String path = HOST + "insurancecompanydtl/findall";
		System.out.println(path);
		ResponseEntity<List<InsuranceCompanyDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceCompanyDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<InsuranceCompanyDtl> createInsuranceCompanyDtl(
			InsuranceCompanyDtl insuranceCompanydtl) {
		return restTemplate.postForEntity(HOST + "insurancecompanydtlresource/saveinsurancecompanydtl",
				insuranceCompanydtl, InsuranceCompanyDtl.class);
	}

	public static ResponseEntity<InsuranceCompanyDtl> getInsuranceCompDtlByPolicyType(String policytype) {
		String path = HOST + "insurancecompanydtl/getinsurancecompanydtlbypolicytype?policytype=" + policytype;
		System.out.println(path);
		ResponseEntity<InsuranceCompanyDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<InsuranceCompanyDtl>() {
				});

		return response;
	}

	public static ResponseEntity<InsuranceCompanyDtl> getinsuraCompanyDtlById(String insurancecompanydtlid) {
		String path = HOST + "insurancecompanydtl/getinsurancecompdtlbyid/" + insurancecompanydtlid;
		System.out.println(path);
		ResponseEntity<InsuranceCompanyDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<InsuranceCompanyDtl>() {
				});

		return response;
	}

	public static ArrayList getReceiptModeWithOutInsurance() {

		System.out.println(HOST + "findallreceiptexceptinsurance");
		return restTemplate.getForObject(HOST + "findallreceiptexceptinsurance", ArrayList.class);

	}

	public static void deleteInsuranceCompanyDtlById(String id) {
		restTemplate.delete(HOST + "insurancecompanydtl/deleteinsurancecompanydtl/" + id);

	}

	public static void deleteInsuranceCompanyMstByInsuranceMstId(String insurancemstid) {
		restTemplate.delete(HOST + "insurancecompanymst/deleteinsurancecompbyid/" + insurancemstid);

	}

	public static ResponseEntity<List<InsuranceCompanyDtl>> getInsuranceCompanyDtl() {
		String path = HOST + "insurancecompanydtlresource/showallinsurancecompanydtl";
		System.out.println(path);

		ResponseEntity<List<InsuranceCompanyDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceCompanyDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<String> saveCustomerByPatient(PatientMst patientMst) {
		String path = HOST + "patientmstresource/cusandaccountfrompatient";
		return restTemplate.postForEntity(path, patientMst, String.class);

	}

	public static void updateInsuranceCompanyDtl(InsuranceCompanyDtl insurancedtlid) {
		restTemplate.put(HOST + "insurancecompanydtl/updateinsurancecompdtlbyid/" + insurancedtlid.getId(),
				insurancedtlid);
		return;

	}

	public static ResponseEntity<InsuranceCompanyDtl> saveInsuranceCompanyDtl(InsuranceCompanyDtl insuranceCompanyDtl) {
		return restTemplate.postForEntity(HOST + "insurancecompanydtl/saveinsurancecompanydtl", insuranceCompanyDtl,
				InsuranceCompanyDtl.class);

	}

	public static ResponseEntity<InsuranceCompanyDtl> InsuranceCompanyDtlById(String insurencemstId) {
		String path = HOST + "insurancecompanydtlresource/insurancecompanydtlbyid/" + insurencemstId;

		System.out.println(path);
		ResponseEntity<InsuranceCompanyDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<InsuranceCompanyDtl>() {
				});

		return response;

	}

	public static ResponseEntity<InsuranceCompanyMst> createInsuranceCompanyMst(
			InsuranceCompanyMst insuranceCompanyMst) {
		return restTemplate.postForEntity(HOST + "insurancecompanymst/saveinsurancecompanymst", insuranceCompanyMst,
				InsuranceCompanyMst.class);
	}

	public static ResponseEntity<List<InsuranceCompanyDtl>> getInsuranceCompanyDtlByname(String policytype) {
		String path = HOST + "insurancecompanydtlresource/getinsurancecompdtlbyname?insurancecompname=" + policytype;
		ResponseEntity<List<InsuranceCompanyDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceCompanyDtl>>() {
				});

		return response;
	}

	public static ResponseEntity<List<InsuranceCompanyMst>> getInsuranceCompanyMst() {
		String path = HOST + "insurancecompanymst/showallinsurancecompanymst";
		System.out.println(path);

		ResponseEntity<List<InsuranceCompanyMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceCompanyMst>>() {
				});
		return response;
	}

	public static void deleteInsuranceCompanyMst(String id) {
		restTemplate.delete(HOST + "insurancecompanymst/deleteinsurancecompanymst/" + id);

	}

	public static ResponseEntity<InsuranceCompanyDtl> getInsuranceCompanyDtlByInsuranceCompanyMst(
			InsuranceCompanyMst insuranceCompanyMst) {
		String path = HOST + "insurancecompanydtlresource/" + insuranceCompanyMst.getId() + "/getinsurancecompanydtl";

		ResponseEntity<InsuranceCompanyDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<InsuranceCompanyDtl>() {
				});
		return response;
	}

	// **************************************BranchWiseStockEvaluationReport***************************************//

	static public ResponseEntity<List<BatchWiseStockEvaluationReport>> getDailyBatchWiseStockEvaluationReport(
			String itemid, String batch, String startdate, String enddate) {
		String path = HOST + "batchwisestockevaluationreportresource" + "/" + itemid + "/" + batch + "?fromdate="
				+ startdate + "&&todate=" + enddate;

		System.out.println(path);

		ResponseEntity<List<BatchWiseStockEvaluationReport>> response = restTemplate.exchange(path, HttpMethod.GET,
				null, new ParameterizedTypeReference<List<BatchWiseStockEvaluationReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<InsuranceCompanyDtl>> getInsuranceCompanyDtlByMstId(
			InsuranceCompanyMst insuranceCompanyMst) {
		String path = HOST + "insurancecompanydtlresource/" + insuranceCompanyMst.getId() + "/getinsurancecompanydtl";

		ResponseEntity<List<InsuranceCompanyDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceCompanyDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<InsuranceCompanyMst> InsuranceCompanyMstById(String insurencemstId) {
		String path = HOST + "insurancecompanymstresource/insurancecompanymstbyid/" + insurencemstId;
		System.out.println(path);
		ResponseEntity<InsuranceCompanyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<InsuranceCompanyMst>() {
				});

		return response;

	}

	public static ResponseEntity<List<ImportPurchaseSummary>> getImportPurchaseSummary(String fdate, String tdate) {

		String path = HOST + "purchasehdrresource/getimportpurchasesummary" + "?fromdate=" + fdate + "&&todate="
				+ tdate;
		System.out.println(path);
		ResponseEntity<List<ImportPurchaseSummary>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ImportPurchaseSummary>>() {
				});
		return response;

	}

	public static ResponseEntity<List<PurchasePriceDefinitionMst>> getPurchasePriceDefinitionByHdrId(String hdrId) {
		String path = HOST + "purchasepricedefinitionmst/purchasepricedefinitionmstbyid/" + hdrId;
		System.out.println(path);
		ResponseEntity<List<PurchasePriceDefinitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchasePriceDefinitionMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ImportPurchaseSummary>> getImportPurchaseSummaryReport(String fdate,
			String tdate) {
		String path = HOST + "purchasehdrresource/getimportpurchasesummary" + "?fromdate=" + fdate + "&&todate="
				+ tdate;
		System.out.println(path);
		ResponseEntity<List<ImportPurchaseSummary>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ImportPurchaseSummary>>() {
				});

		return response;
	}

	public static ResponseEntity<List<GSTOutputDetailReport>> getGstOutputDetailReport(String fdate, String tDate,
			String branchCode) {
		// TODO Auto-generated method stub
		return null;
	}

	public static ResponseEntity<PurchasePriceDefinitionDtl> SavePurchasePriceDefinitionDtl(
			PurchasePriceDefinitionDtl purchasePriceDefinitionDtl) {
		return restTemplate.postForEntity(HOST + "purchasepricedefinitiondtl/savepurchasepricedefinitiondtl",
				purchasePriceDefinitionDtl, PurchasePriceDefinitionDtl.class);
	}

	public static ResponseEntity<List<PurchasePriceDefinitionDtl>> getPurchasePriceDefinitionDtl(String hdrId) {

		String path = HOST + "purchasepricedefinitiondtl/purchasepricedefinitionmstbyid/" + hdrId;
		System.out.println(path);
		ResponseEntity<List<PurchasePriceDefinitionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchasePriceDefinitionDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<String> updatePriceDefinitionFromPurchaseBatch(String hdrId) {
		String path = HOST + "purchasepricedefinitiondtl/pricedefinitionupdationfrompurchasebatch/" + hdrId;
		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	public static void deletePurchasePriceDefinitionDtlById(String id) {

		restTemplate.delete(HOST + "purchasepricedefinitiondtl/deletebyid/" + id);

	}

	public static ResponseEntity<List<InsuranceWiseSalesReport>> getInsuranceWiseSalesReportReport(String sdate,
			String edate) {
		String path = HOST + "insurancewisesalesreportresource/getinsurancewisesalesreportresource" + "?fromdate="
				+ sdate + "&&todate=" + edate;
		System.out.println(path);
		ResponseEntity<List<InsuranceWiseSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceWiseSalesReport>>() {
				});
		return response;
	}

	public static ResponseEntity<List<InsuranceCompanyDtl>> getInsuranceCompanyDtlByInsuranceCompanyMstId(
			String insuranceCompanyMst) {
		String path = HOST + "insurancecompanydtlresource/insurancedtlbycompanymst/" + insuranceCompanyMst;

		ResponseEntity<List<InsuranceCompanyDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<InsuranceCompanyDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<String> UpdateSubscription(String text, String text2) {

		return null;
	}

	public static ResponseEntity<String> initializeAccountHeadsMain() {
		String path = HOST +SystemSetting.getSystemBranch() + "/accountheads/initilize";
		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	public static ResponseEntity<List<ConsumptionReport>> getConsumptionReportSummary(String branch, String strDate,
			String strDate1) {

		String path = HOST + "consumptionreportresourec/consumptionreportsummary/" + branch + "?rdate=" + strDate
				+ "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<ConsumptionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReport>>() {
				});

		return response;

	}

	// =================AMDC Daily Sales Summary Report======anandu=========

	public static ResponseEntity<List<AMDCDailySalesSummaryReport>> getAMDCDailySalesSummaryReport(String currentDate) {
		String path = HOST + "amdcdailysalessummaryreportresource/getamdcdailysalessummaryreport" + "?currentDate="
				+ currentDate;

		ResponseEntity<List<AMDCDailySalesSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AMDCDailySalesSummaryReport>>() {
				});
		return response;
	}

	// =============end AMDC Daily Sales Summary Report============

	// =================WholeSale Detail Report======anandu=========

	public static ResponseEntity<List<WholeSaleDetailReport>> getWholeSaleDetailReport(String fdate, String tdate) {
		String path = HOST + "wholesaledetailreportresource/getwholesaledetailreport" + "?fromdate=" + fdate
				+ "&&todate=" + tdate;

		System.out.println(path);
		ResponseEntity<List<WholeSaleDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WholeSaleDetailReport>>() {
				});
		return response;
	}

	// =============end WholeSale Detail Report============

	// =================BranchStockReport======sharon=========

	public static ResponseEntity<List<BranchStockReport>> getBranchStockReport(String date) {
		String path = HOST + "branchstockreportresource/getbranchstockreport" + "?date=" + date;

		ResponseEntity<List<BranchStockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchStockReport>>() {
				});
		return response;
	}

	// =============end BranchStockReport============

	// =================stock report price type wise=============
	public static ResponseEntity<List<StockReport>> getStockSummaryDetails(String sfdate, String stdate,
			String selectedItem, String selectedItem2) {

		String path = HOST + "stockreportresource/getstocksummarydetails/" + selectedItem + "/" + selectedItem2
				+ "?fromdate=" + sfdate + "&&todate=" + stdate;
		System.out.println(path);
		ResponseEntity<List<StockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockReport>>() {
				});
		return response;
	}

	// =============end WholeSale Detail Report============

	// ====import puchase invoice=============anandu========
	public static ResponseEntity<List<PurchaseReport>> getImportPurchaseInvoice(String phdrid) {
		String path = HOST + "purchasereportresource/getimportpurchaseinvoice/" + phdrid;

		System.out.println(path);
		ResponseEntity<List<PurchaseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReport>>() {
				});
		return response;
	}

	///
	/*
	 * 
	 * //===================end============================
	 * 
	 * // .................Sibi..............//d public static
	 * ResponseEntity<String> getaccountheadsforcreation() { String path = HOST +
	 * "/" + SystemSetting.getSystemBranch() +
	 * "/accountheadsresource/getaccountheadsforcreation"; System.out.println(path);
	 * ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET,
	 * null, new ParameterizedTypeReference<String>() { }); return response; }
	 */

//=================AMDCShortExpiryReport======sharon=========

	public static ResponseEntity<List<AMDCShortExpiryReport>> getAMDCShortExpiryReport(String date) {
		String path = HOST + "amdcshortexpiryreportresource/getamdcshortexpiryreport" + "?date=" + date;

		ResponseEntity<List<AMDCShortExpiryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AMDCShortExpiryReport>>() {

				});
		return response;
	}

	// =============end AMDCShortExpiryReport============

	// .................Sibi..............//d
	public static ResponseEntity<String> getaccountheadsforcreation() {
		String path = HOST + "/" + SystemSetting.getSystemBranch() + "/accountheadsresource/getaccountheadsforcreation";
		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	//            [PoliceReport]   (0_0)       SharonN 

	public static ResponseEntity<List<PoliceReport>> getPoliceReport(String fromdate, String todate,String cust) {
		String path = HOST + "policereportresource/getpolicereport" + "?fromdate=" + fromdate + "&&todate=" + todate + "&&customer=" + cust;
		
		System.out.println(path);
		ResponseEntity<List<PoliceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PoliceReport>>() {
				});
		return response;
	}

	public static String getIgstType(String companyState, String customerState, String companyCountry) {
		return restTemplate.getForObject(
				HOST + "taxmstresource/gettaxtype/" + customerState + "/" + companyState + "/" + companyCountry,
				String.class);
	}

	public static ResponseEntity<Double> getTotalAdditionalExpenseByHdrId(String id) {
		String path = HOST + "purchasereportresource/gettotaladditionalexpensebyhdrid/" + id;

		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {

				});
		return response;
	}

	public static ResponseEntity<Double> getTotalImportExpense(String id) {
		String path = HOST + "purchasereportresource/gettotalimportexpense/" + id;

		System.out.println(path);
		ResponseEntity<Double> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<Double>() {

				});
		return response;
	}

	// =============end PoliceReport============

	// --------------------patientpopupWithcustomer--------corporate
	// pharmacy------------------------------

	public static ArrayList getPatientWithCustomer(String customerid) {
		System.out.println("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
		System.out.println(HOST + "patientmstresource/findallpatientwithcustomer?data=" + customerid);
		return restTemplate.getForObject(HOST + "patientmstresource/findallpatientwithcustomer?data=" + customerid,
				ArrayList.class);

	}

	// ========================================Initialize===CORPORATE
	// sale====================
	public static ResponseEntity<String> InitializeCorporateSale(String branchcode) {
		String path = HOST + "salestypemstresource/initializecorporatesale/" + branchcode;

		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {

				});
		return response;

	}
	// ======================Stock Migration===========//

	public static ResponseEntity<List<StockMigrationReport>> getItemDetailsforMigration() {

		String path = HOST + "itembatchmstresource/showallitembatchdtl";

		System.out.println(path);

		ResponseEntity<List<StockMigrationReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<StockMigrationReport>>() {

				});

		return response;
	}

	public static ResponseEntity<String> stockMigration() {
		String path = HOST + "datatransferresource/stockmigration/" + SystemSetting.systemBranch;
		System.out.println(path);

		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {

				});

		return response;
	}

	public static void deleteItemBatchDtl(String id) {
		restTemplate.delete(HOST + "itembatchmstresource/deleteitembatchdtl/" + id);

	}

	// ==================end===============================================================

	public static ResponseEntity<List<PurchaseReport>> getImportPurchaseInvoiceByHdrId(String phdrid) {
		String path = HOST + "purchasereportresource/getimportpurchaseinvoice/" + phdrid;

		System.out.println(path);
		ResponseEntity<List<PurchaseReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReport>>() {
				});
		return response;
	}

	public static Double getCurrencyConvertedAmount(String fromCurrency, String toCurrency, Double fcDiscountAmount) {
		System.out.println(HOST + "currencyconversionmst/getconvertedcurrencyamount/" + fromCurrency + "/" + toCurrency
				+ "/" + fcDiscountAmount);
		return restTemplate.getForObject(HOST + "currencyconversionmst/getconvertedcurrencyamount/" + fromCurrency + "/"
				+ toCurrency + "/" + fcDiscountAmount, Double.class);
	}

	// ====================================ProfitAndLossExpense===================================//
	public static ResponseEntity<ProfitAndLossExpense> createProfitAndLossExpense(
			ProfitAndLossExpense profitAndLossExpense) {
		return restTemplate.postForEntity(HOST + "profitandlossexpenseresource/saveprofitandlossexpense",
				profitAndLossExpense, ProfitAndLossExpense.class);
	}

	public static ResponseEntity<List<ProfitAndLossExpense>> getProfitAndLossExpense() {
		String path = HOST + "profitandlossexpenseresource/getallprofitandlossexpense";
		System.out.println(path);

		ResponseEntity<List<ProfitAndLossExpense>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProfitAndLossExpense>>() {
				});
		return response;
	}

	public static void deleteProfitAndLossExpense(String id) {
		System.out.print(HOST + "profitandlossexpenseresource/deleteprofitandlossexpense/" + id);
		restTemplate.delete(HOST + "profitandlossexpenseresource/deleteprofitandlossexpense/" + id);

	}

	// ===============================End=====ProfitAndLossExpense===================================//

	// ====================================ProfitAndLossIncome===================================//
	public static ResponseEntity<ProfitAndLossIncome> createProfitAndLossIncome(
			ProfitAndLossIncome profitAndLossIncome) {
		return restTemplate.postForEntity(HOST + "profitandlossincomeresource/saveprofitandlossincome",
				profitAndLossIncome, ProfitAndLossIncome.class);
	}

	public static ResponseEntity<ItemBatchDtl> getSaveToItembatchDtl() {
		// TODO Auto-generated method stub
		return null;
	}

	// ======================BalanceSheetConfigAsset&Liability
	// ============anandu=====
	public static ResponseEntity<BalanceSheetConfigAsset> saveBalanceSheetConfigAsset(
			BalanceSheetConfigAsset balanceSheetConfigAssetDtl) {

		return restTemplate.postForEntity(HOST + "balancesheetconfigassetresource/savebalancesheetconfigasset",
				balanceSheetConfigAssetDtl, BalanceSheetConfigAsset.class);
	}

	public static void deleteBalanceSheetConfigAsset(String id) {
		restTemplate.delete(HOST + "balancesheetconfigassetresource/deletebalancesheetconfigasset/" + id);

	}

	public static ResponseEntity<List<BalanceSheetConfigAsset>> getAllBalanceSheetConfigAsset() {
		String path = HOST + "balancesheetconfigassetresource/getallbalancesheetconfigasset";

		System.out.println(path);
		ResponseEntity<List<BalanceSheetConfigAsset>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BalanceSheetConfigAsset>>() {
				});
		return response;
	}

	public static ResponseEntity<BalanceSheetConfigLiability> saveBalanceSheetConfigLiability(
			BalanceSheetConfigLiability balanceSheetConfigLiabilityDtl) {

		return restTemplate.postForEntity(HOST + "balancesheetconfigliabilityresource/savebalancesheetconfigliability",
				balanceSheetConfigLiabilityDtl, BalanceSheetConfigLiability.class);
	}

	public static ResponseEntity<List<BalanceSheetConfigLiability>> getAllBalanceSheetConfigLiability() {
		String path = HOST + "balancesheetconfigliabilityresource/getallbalancesheetconfigliability";

		System.out.println(path);
		ResponseEntity<List<BalanceSheetConfigLiability>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BalanceSheetConfigLiability>>() {
				});
		return response;
	}

	public static void deleteBalanceSheetConfigLiability(String id) {
		restTemplate.delete(HOST + "balancesheetconfigassetresource/deletebalancesheetconfigliability/" + id);

	}

	public static ResponseEntity<List<ProfitAndLossIncome>> getProfitAndLossIncome() {
		String path = HOST + "profitandlossincomeresource/getallprofitandlossincome";
		System.out.println(path);

		ResponseEntity<List<ProfitAndLossIncome>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProfitAndLossIncome>>() {
				});
		return response;
	}

	public static void deleteProfitAndLossIncome(String id) {
		System.out.print(HOST + "profitandlossincomeresource/deleteprofitandlossincome/" + id);
		restTemplate.delete(HOST + "profitandlossincomeresource/deleteprofitandlossincome/" + id);

	}
	// ===============================End=====ProfitAndLossExpense========================

	public static ResponseEntity<OpeningStockDtl> getSaveToOpeningStockDtll(OpeningStockDtl openingStockDtl) {
		return restTemplate.postForEntity(HOST + "openingstockdtlresource/saveopeningstockdtl", openingStockDtl,
				OpeningStockDtl.class);

	}

	public static void updateOpeningStock(OpeningStockDtl open) {
		restTemplate.put(HOST + "openingstockdtlresource/finalsave", open);
		return;
	}

	public static ResponseEntity<List<OpeningStockDtl>> getOpeningStockDtl() {
		String path = HOST + "openingstockdtlresource/getallopeningstockdtl";
		System.out.println(path);

		ResponseEntity<List<OpeningStockDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OpeningStockDtl>>() {
				});
		return response;
	}

	public static Double getSumOfAdditionalExpenseByExpenseTyeInFc(String id, String expenceType) {

		return restTemplate.getForObject(
				HOST + "additionalexpense/sumofamountinfcbyexpencetype/" + id + "/" + expenceType, Double.class);
	}

	// =================Pharmacy CustomerWiseSaleReport========sharon=====

	public static ResponseEntity<List<PharmacyNewCustomerWiseSaleReport>> getAMDCCustomerWiseSaleReport(String sfdate,
			String stdate, String cust) {
		String path = HOST + "pharmacynewcustomerwisesalesreportresource/getpharmacynewcustomerwisesalesreport" + "?fromdate=" + sfdate
				+ "&&todate=" + stdate + "&&customer=" + cust;
		System.out.println(path);
		ResponseEntity<List<PharmacyNewCustomerWiseSaleReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyNewCustomerWiseSaleReport>>() {
				});
		return response;
	}

	// --------------------------------------Master ReportSql------sharon-------------
	public static ResponseEntity<ReportSqlDtl> saveReportSqlDtl(ReportSqlDtl reportSqlDtl) {
		return restTemplate.postForEntity(HOST + "reportsqldtlresource/savereportsqldtl", reportSqlDtl,
				ReportSqlDtl.class);
	}

	public static void deletereportSqlDtlById(String id) {
		restTemplate.delete(HOST + "reportsqldtlresource/deletereportsqldtl/" + id);

	}

	public static ResponseEntity<ReportSqlHdr> saveReportSqlHdr(ReportSqlHdr reportSqlHdr) {
		return restTemplate.postForEntity(HOST + "reportsqlhdrresource/savereportsqlhdr", reportSqlHdr,
				ReportSqlHdr.class);
	}

	public static void deleteOpeningStockDtl(String id) {
		restTemplate.delete(HOST + "openingstockdtlresource/deleteopeningstockdtl/" + id);
	}

	public static ResponseEntity<List<OpeningStockDtl>> getOpeningStockDtlnull() {
		String path = HOST + "openingstockdtlresource/getallopeningstockdtlnull";
		System.out.println(path);

		ResponseEntity<List<OpeningStockDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<OpeningStockDtl>>() {
			
		});
		return response;

		}

	public static ResponseEntity<List<ReportSqlDtl>> getReportSqlDtl() {

		String path = HOST + "reportsqldtlresource/showallreportsqldtl";
		System.out.println(path);

		ResponseEntity<List<ReportSqlDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReportSqlDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<AccountHeads>> getAccountHeadsByParentId(String assetId) {
		String path = HOST + "accountheadsresource/getallaccountheadsbyparentid/" + assetId;

		System.out.println(path);
		ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountHeads>>() {

				});
		return response;
	}

	public static ResponseEntity<List<BalanceSheetAssetReport>> getBalanceSheetConfigAsset(String currentDate) {
		return null;

	}

	public static ResponseEntity<List<BalanceSheetLiabilityReport>> getBalanceSheetConfigLiability(
			String currentDate) {
		return null;

	}

	// --------------------------------------ReportSqlHdr-----------------------

	static public ResponseEntity<List<ReportSqlHdr>> getAllReportSqlHdr() {

		String path = HOST + "reportsqlhdrresource/allreportsqlhdr";

		ResponseEntity<List<ReportSqlHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReportSqlHdr>>() {
				});

		return response;

	}

	public static ResponseEntity<List<ExpenseReport>> getExpenseReport(String date) {
		// TODO Auto-generated method stub
		return null;
	}

	public static ResponseEntity<List<IncomeReport>> getIncomeReport(String date) {
		// TODO Auto-generated method stub
		return null;
	}

	public static ResponseEntity<List<SaleOrderInvoiceReport>> getSaleOrderInvoice(String vNo, String vdate) {
		String path = HOST + "salesorderinvoicereportresource/" + vNo + "/" + SystemSetting.systemBranch + "?rdate="
				+ vdate;
		System.out.println(path);

		ResponseEntity<List<SaleOrderInvoiceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SaleOrderInvoiceReport>>() {
				});

		return response;
	}

	public static SalesOrderTransHdr getSalesOrderTransHdrByVoucherAndDate(String vNo, String vdate) {
		String path = HOST + "salesorderteanshdrresource/salesordertranshdrbyvoucheranddate/" + vNo + "/"
				+ SystemSetting.systemBranch + "?rdate=" + vdate;
		System.out.println(path);
		return restTemplate.getForObject(path, SalesOrderTransHdr.class);
	}

	public static ResponseEntity<String> getSalesTransHdrIdFromSaleOrder(String orderid) {

		String path = HOST + "salestranshdr/savesalestranshdridfromsalesorderid/" + orderid;
		System.out.println("===path ==" + path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});

		return response;

	}

	public static Double getSumOfAdditionalExpenseByExpenseTyeAndCurrency(String id, String expensetype,
			String currencyid) {
		System.out.println(HOST + "additionalexpense/sumoffcamountinlcudedinbillbycurrencyid/" + id + "/" + expensetype
				+ "/" + currencyid);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "additionalexpense/sumoffcamountinlcudedinbillbycurrencyid/"
				+ id + "/" + expensetype + "/" + currencyid, Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}
		return rtnValue;

	}

	public static ResponseEntity<ReportSqlHdr> getReportSqlHdrById(String reportsqlhdrid) {
		String path = HOST + "reportsqlhdrresource/getreportsqlhdrbyid/" + reportsqlhdrid;
		System.out.println(path);
		ResponseEntity<ReportSqlHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<ReportSqlHdr>() {
				});

		return response;
	}

		public static ResponseEntity<List<WriteOffDetailAndSummaryReport>> getWriteOffSummary(String fDate,
				String tDate, String systemBranch, String reason) {
			String path = HOST + "damageentryreportresource/writeoffdetailandsummary/" + systemBranch + "/" + reason + "?fromdate=" + fDate + "&&todate=" + tDate;
			System.out.println("=============path=========" + path);

			ResponseEntity<List<WriteOffDetailAndSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<WriteOffDetailAndSummaryReport>>() {
					});
			return response;
		}
		
	public static ResponseEntity<List<ReportSqlHdr>> getReportSqlHdrByReportName(String reportName) {
		String path = HOST + "reportsqlhdrresource/reportsqlhdrbyreportname/" + reportName;
		System.out.println(path);
		ResponseEntity<List<ReportSqlHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReportSqlHdr>>() {
				});

		return response;
	}

	public static ResponseEntity<List<SalesOrderDtl>> getSalesOrderDtlByOrderTransHdrId(String id) {
		String path = HOST + "salesordertranshdrresource/sales_receipts/" + id;
		System.out.println(path);

		ResponseEntity<List<SalesOrderDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<SalesOrderDtl>>() {
				});
		return response;
	}

	public static ResponseEntity<List<ReportSqlDtl>> getReportSqlDtlByHdrId(String hdr) {
		String path = HOST + "reportsqldtlresource/reportsqldtlbyhdrid/" + hdr;
		System.out.println(path);
		ResponseEntity<List<ReportSqlDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReportSqlDtl>>() {
				});

		return response;
	}


	public static ResponseEntity<List<LocalPurchaseSummaryReport>> getlocalPurchaseSummaryDtlReport(String fromdate,
			String todate) {
		String path = HOST + "purchasehdrresource/getlocalPurchaseSummaryDtlReport" + "?fromdate=" + fromdate + "&&todate="
				+ todate;
		System.out.println(path);
		ResponseEntity<List<LocalPurchaseSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LocalPurchaseSummaryReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PharmacyLocalPurchaseDetailsReport>> getPharmacyLocalPurchaseDetailsReport(
			String sdate, String edate) {
		
		String path = HOST + "purchasehdrresource/getPharmacyLocalPurchaseDetailsReport" + "?fromdate=" + sdate + "&&todate="
				+ edate;
		System.out.println(path);
		ResponseEntity<List<PharmacyLocalPurchaseDetailsReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyLocalPurchaseDetailsReport>>() {
				});

		return response;
		
		
	}

	// =================PHARMACY DAY CLOSING  Report======sharon=========

		public static ResponseEntity<List<PharmacyDayClosingReport>> getPharmacyDayClosingReport(String fdate) {
			String path = HOST + "dayendreportresource/getpharmacydayclosingreport" + "?fdate="  + fdate;
			
			ResponseEntity<List<PharmacyDayClosingReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<PharmacyDayClosingReport>>() {
					});
			return response;
		}


	
	static public void updateReportSqlHdr(ReportSqlHdr reportSqlHdr) {

		restTemplate.put(HOST + "reportsqlhdrresource/reportsqlhdrupdate/" + reportSqlHdr.getId(), reportSqlHdr);
		return;

	}
	// =================KitDefinitionPublishWindow====14 /06/2021==sharon=========
	
	
	
	public static ResponseEntity<KitDefinitionMst> publishAllKitDefinition(String branchcode,String itemid) {
		String path = HOST + "kitdefinitionmstresource/itemmst/allitem/"+itemid+"?branchcode=" + branchcode ;
		System.out.println(path);
		ResponseEntity<KitDefinitionMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<KitDefinitionMst>() {
				});
		return response;
	}
	// =================PharmacyDailySalesTransactionReport====18 /06/2021==sharon=========
	public static ResponseEntity<List<PharmacyDailySalesTransactionReport>> getPharmacyDailySalesTransactionReport(
			String fromdate, String todate) {
            String path = HOST + "amdcdailysalessummaryreportresource/getpharmacydailysalestransactionreport" + "?fromdate=" + fromdate + "&&todate=" + todate;
		
		System.out.println(path);
		ResponseEntity<List<PharmacyDailySalesTransactionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyDailySalesTransactionReport>>() {
				});
		return response;
	}
	// =================PharmacyItemWiseSalesReport====18 /06/2021==sharon=========
	public static ResponseEntity<List<PharmacyItemWiseSalesReport>> getPharmacyItemWiseSalesReport(String fromdate,
			String todate) {
		 String path = HOST + "itembatchmstresource/getitemwisesalesreport" + "?fromdate=" + fromdate + "&&todate=" + todate;
			
			System.out.println(path);
			ResponseEntity<List<PharmacyItemWiseSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<PharmacyItemWiseSalesReport>>() {
					});
			return response;
	}
	public static ResponseEntity<List<PharmacyItemWiseSalesReport>> getPharmacyItemWiseSalesReportByItemName(String fromdate,
			String todate, String itename) {
		 String path = HOST + "itembatchmstresource/getitemwisesalesreportbyitemname" + "?fromdate=" + fromdate + "&&todate=" + todate+"&&itename="+itename;
			
			System.out.println(path);
			ResponseEntity<List<PharmacyItemWiseSalesReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<PharmacyItemWiseSalesReport>>() {
					});
			return response;
	}
	// =================ItemBatchDtlDaily====19 /06/2021==sharon=========
	static public Double getQtyFromItemBatchMstByItemIdAndQty(String itemId, String batch) {
		System.out.println("Items" + HOST + "itembatchmstresource/itembatchdtlqty/" + batch + "/" + itemId+"/"+SystemSetting.STORE);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "itembatchmstresource/itembatchdtlqty/" + batch + "/" + itemId+"/"+SystemSetting.STORE,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}
	
	
	// by anandu================for stock checking with store==============29-06-2021==========
	static public Double getQtyFromItemBatchMstByItemIdAndQty(String itemId, String batch, String storeNameFromPopUp) {
		System.out.println("Items" + HOST + "itembatchmstresource/itembatchdtlqty/" + batch + "/" + itemId+"/"+storeNameFromPopUp);
		double rtnValue = 0;
		Double ownAccAmt = restTemplate.getForObject(HOST + "itembatchmstresource/itembatchdtlqty/" + batch + "/" + itemId+"/"+storeNameFromPopUp,
				Double.class);
		if (null != ownAccAmt) {
			rtnValue = ownAccAmt;
		}

		return rtnValue;

	}
	

	public static ResponseEntity<List<PharmacyBrachStockMarginReport>> getPharmacyBranchStockMarginReport(String sdate,
			String edate, String branchcode) {
		
		String path = HOST + "itembatchdtlresource/getpharmacybranchstockmarginreport/" + branchcode +"?fromdate=" + sdate + "&&todate="
				+ edate;
		System.out.println(path);
		ResponseEntity<List<PharmacyBrachStockMarginReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyBrachStockMarginReport>>() {
		});
		return response;
		}

	public static ResponseEntity<TrialBalanceHdr> getTrialBalanceHdrByReportId(String reportId) {
		String path = HOST + "trialbalancehdrresource/gettrialbalancehdrbyreportid/" + reportId;
		System.out.print(path);
		ResponseEntity<TrialBalanceHdr> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<TrialBalanceHdr>() {

				});

		return response;
	}


	public static ArrayList getTrialBalanceDtlByHdrId(String trialBalanceHdrId) {
		System.out.print(HOST + "trialbalancedtlresource/" + trialBalanceHdrId);
		return restTemplate.getForObject(HOST + "trialbalancedtlresource/" + trialBalanceHdrId, ArrayList.class);
	}

	public static ArrayList getReportId(String sdate, String userId) {
		System.out.print(HOST + "trialbalancehdrresource/getreportid/" + userId + "?sdate=" + sdate);
		return restTemplate.getForObject(HOST + "trialbalancehdrresource/getreportid/" + userId + "?sdate=" + sdate, ArrayList.class);
	}
	
	
	

	public static ResponseEntity<List<RetailSalesDetailReport>> getRetailSalesDetailReport(String fromDate, String toDate) {
		String path = HOST + "salesdetailresource/getretailsalesdetailreport" + "?fromdate=" + fromDate + "&&todate=" + toDate;
		System.out.print(path);
		ResponseEntity<List<RetailSalesDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RetailSalesDetailReport>>() {

				});
		return response;
	}

	public static ResponseEntity<List<RetailSalesSummaryReport>> getRetailSalesSummaryReport(String fromDate,
			String toDate) {
		String path = HOST + "salesdetailresource/getretailsalessummaryreport" + "?fromdate=" + fromDate + "&&todate=" + toDate;
		System.out.print(path);
		ResponseEntity<List<RetailSalesSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<RetailSalesSummaryReport>>() {

				});
		return response;
	}
	//------------------------------------------------Kit Publish Window--------------------
	public static ResponseEntity<List<KitDefinitionMst>> getkitNamePopupByItemId() {
		String path = HOST + "kitdefinitionmstresource/kitnamebykitid";
		System.out.println(path);
		ResponseEntity<List<KitDefinitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KitDefinitionMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<KitDefinitionMst>> getKitDefinitionByItemId(String id) {
		String path = HOST + "kitdefinitionmstresource/getkitdefinitionbyitemid/"+id ;
		System.out.println(path);
		ResponseEntity<List<KitDefinitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KitDefinitionMst>>() {
				});
		return response;
	}
	
	public static ResponseEntity<List<KitDefinitionMst>> getKitDefinition() {
		String path = HOST + "kitdefinitionmstresource/getkitdefinition" ;
		System.out.println(path);
		ResponseEntity<List<KitDefinitionMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<KitDefinitionMst>>() {
				});
		return response;
	}

	public static ArrayList SearchKitByNameForPopup(String searchData) {
		System.out.println(HOST + "popupresource/kititempopupsearch?data=" + searchData);

		return restTemplate.getForObject(HOST + "popupresource/kititempopupsearch?data=" + searchData, ArrayList.class);

	}

//	public static ResponseEntity<List<PharmacyPurchaseReturnDetailReport>> getPurchaseReturnDetailReport(
//			String fromdate, String todate) {
//		String path = HOST + "purchasereturndetailresource/getpurchasereturndetailandsummary" + "?fromdate=" + fromdate + "&&todate=" + todate;
//			
//			System.out.println(path);
//			ResponseEntity<List<PharmacyPurchaseReturnDetailReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
//					new ParameterizedTypeReference<List<PharmacyPurchaseReturnDetailReport>>() {
//					});
//			return response;
//	}
	public static ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> getPurchaseReturnDetailReport(
			String fromdate, String todate) {
		String path = HOST + "purchasereturndetailresource/getpurchasereturndetailandsummary" + "?fromdate=" + fromdate + "&&todate=" + todate;
			
			System.out.println(path);
			ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<PurchaseReturnDetailAndSummaryReport>>() {
					});
			return response;
	}

	public static ResponseEntity<List<PharmacyPhysicalStockVarianceReport>> getPharmacyPhysicalStockVarianceReport(String fromdate, String todate) {
		String path = HOST + "physicalstockdtlresource/getpharmacyphysicalstockvariancereport" + "?fromdate=" + fromdate + "&&todate=" + todate;
		
		System.out.println(path);
		ResponseEntity<List<PharmacyPhysicalStockVarianceReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyPhysicalStockVarianceReport>>() {
				});
		return response;
	}
//----------------------------------sharon-----------------------jun25 --------------

        static public ArrayList SearchStoreItemByBarcode(String barcode) {

		System.out.println(HOST + "popupresource/storeitempopupsearchbybarcode/" + SystemSetting.systemBranch + "?data=" + barcode);

		return restTemplate.getForObject(
				HOST + "popupresource/storeitempopupsearchbybarcode/" + SystemSetting.systemBranch + "?data=" + barcode,
				ArrayList.class);

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static ResponseEntity<List<PharmacyDayEndReport>> getPharmacyDayEndReport(String fromdate,String branchCash) {
		String path = HOST + "salestranshdr/getpharmacydayendreport/"+branchCash+"?fromdate=" + fromdate;

		ResponseEntity<List<PharmacyDayEndReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacyDayEndReport>>() {

				});
		return response;
		
	}


	public static Double getPharmacyCashSales(String fromdate, String branchCash) {
		
		System.out.println(HOST + "salestranshdr/getpharmacycashsales/" + branchCash + "?fromdate=" + fromdate);
		return restTemplate.getForObject(HOST + "salestranshdr/getpharmacycashsales/" + branchCash + "?fromdate=" + fromdate, Double.class);

	}

	public static Double getPharmacyCashRevceived(String fromdate, String branchCash) {
		System.out.println(HOST + "salestranshdr/getpharmacycashrevceived/" + branchCash + "?fromdate=" + fromdate);
		return restTemplate.getForObject(HOST + "salestranshdr/getpharmacycashrevceived/" + branchCash + "?fromdate=" + fromdate, Double.class);

	}

	public static Double getPharmacyCashPaid(String fromdate, String branchCash) {
		System.out.println(HOST + "salestranshdr/getpharmacycashpaid/" + branchCash + "?fromdate=" + fromdate);
		return restTemplate.getForObject(HOST + "salestranshdr/getpharmacycashpaid/" + branchCash + "?fromdate=" + fromdate, Double.class);

	}

	public static Double getPharmacyOpeningCash(String fromdate, String branchCash) {
		System.out.println(HOST + "salestranshdr/getpharmacyopeningcash/" + branchCash + "?fromdate=" + fromdate);
		return restTemplate.getForObject(HOST + "salestranshdr/getpharmacyopeningcash/" + branchCash + "?fromdate=" + fromdate, Double.class);

	}

	public static Double getPharmacyClosingCash(String fromdate, String branchCash) {
		System.out.println(HOST + "salestranshdr/getpharmacyclosingcash/" + branchCash + "?fromdate=" + fromdate);
		return restTemplate.getForObject(HOST + "salestranshdr/getpharmacyclosingcash/" + branchCash + "?fromdate=" + fromdate, Double.class);

	}

	public static String getAccountIdByAccountName(String branchCash) {
		System.out.println(HOST + "salestranshdr/getaccountidbyaccountname/" + branchCash);
		return restTemplate.getForObject(HOST + "salestranshdr/getaccountidbyaccountname/" + branchCash , String.class);

	}

	
	

	public static ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> getPurchaseReturnDetailAndSummary(
			String sfdate, String stdate) {
		
		String path = HOST + "purchasereturndetailresource/getpurchasereturndetailandsummary" + "?fromdate=" + sfdate + "&&todate=" + stdate;
		System.out.print(path);
		ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReturnDetailAndSummaryReport>>() {

				});
		return response;
	}
	public static ResponseEntity<String> initializeUnitMst() {
		String path = HOST +"unitmst/initializeunitmst/"+SystemSetting.getSystemBranch();
		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	public static ResponseEntity<String> initializeStoreMst() {
		String path = HOST +"storemstresource/initializestoremst/"+SystemSetting.getSystemBranch();
		System.out.println(path);
		ResponseEntity<String> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<String>() {
				});
		return response;
	}

	//----------------------------------GOOD Receive Note---------------------SHARON[9/7/2021]-------
	// ------------------delete goodReceiveNote dtl delete
	/*
	 * static public void goodReceiveNoteDtlDelete(String goodReceiveNoteDtlId) {
	 * 
	 * restTemplate.delete(HOST + "goodreceivenotedtlresource/goodreceivenotedtl/" +
	 * goodReceiveNoteDtlId); return;
	 * 
	 * }
	 */
	
	
		static public ResponseEntity<GoodReceiveNoteDtl> getGoodReceiveNoteDtlById(String id) {
			String path = HOST + "goodreceivenotedtlresource/goodreceivenote/" + id + "/goodreceivenotedtlbyid";

			System.out.println(path);
			ResponseEntity<GoodReceiveNoteDtl> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<GoodReceiveNoteDtl>() {
					});

			return response;

		}
	
		
	
		static public ResponseEntity<GoodReceiveNoteDtl> saveGoodReceiveNoteDtl(GoodReceiveNoteDtl goodReceiveNoteDtl) {
			
			return restTemplate.postForEntity(HOST + "goodreceivenotedtlresource/goodreceivenotedtls/" + goodReceiveNoteDtl.getGoodReceiveNoteHdr().getId(),
					goodReceiveNoteDtl, GoodReceiveNoteDtl.class);

		}
	
		static public ResponseEntity<List<GoodReceiveNoteDtl>> getGoodReceiveNoteDtl(GoodReceiveNoteHdr goodReceiveNoteHdr) {
			String path = HOST + "goodreceivenotedtlresource/goodreceivenote/" + goodReceiveNoteHdr.getId() + "/goodreceivenotedtlbyid";
			System.out.println(path);

			ResponseEntity<List<GoodReceiveNoteDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<GoodReceiveNoteDtl>>() {
					});

			return response;

		}
		
		static public void updateGoodReceiveNoteHdr(GoodReceiveNoteHdr goodReceiveNoteHdr) {
			System.out.println("--goodReceiveNoteHdr===" + goodReceiveNoteHdr.getFinalSavedStatus());

			restTemplate.put(HOST + "goodreceivenotehdrresource/finalsavegoodreceivenotehdr/" + goodReceiveNoteHdr.getId(), goodReceiveNoteHdr);
			return;

		}
		
		
		
		static public ArrayList SearchGoodReceiveNoteDtls(String goodreceivenoteHdrId) {

			System.out.println("----- SearchGoodReceiveNoteDtls--rest caller---");

			return restTemplate.getForObject(HOST + "goodreceivenotedtlresource/goodreceivenotehdr/" + goodreceivenoteHdrId + "/goodreceivenotedtl", ArrayList.class);

		}
		
		static public Summary getSummaryGoodReceiveNoteHDr(String goodreceivenoteHdrId) {

			System.out.println(HOST + "goodreceivenotedtlresource/" + goodreceivenoteHdrId + "/goodreceivenotedtlsummary");
			return restTemplate.getForObject(HOST + "goodreceivenotedtlresource/" + goodreceivenoteHdrId + "/goodreceivenotedtlsummary",
					Summary.class);

		}
		
		
		static public void goodReceiveNoteDtlDelete(String goodReceiveNoteDtlId) {
			
			restTemplate.delete(HOST + "goodreceivenotedtlresource/goodreceivenotedelete/" + goodReceiveNoteDtlId);
			return;

		}
		
		static public ResponseEntity<List<GoodReceiveNoteDtl>> getGoodReceiveNoteDtld(GoodReceiveNoteHdr goodReceiveNoteHdr) {
		
			String path = HOST + "goodreceivenotedtlresource/goodreceivenotehdr/" + goodReceiveNoteHdr.getId() + "/goodreceivenotedtl";
			System.out.println(path);

			ResponseEntity<List<GoodReceiveNoteDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<GoodReceiveNoteDtl>>() {
					});

			return response;

		}
		
		//----------------------------------GOOD Receive Note---------------------END--------------------------------------------------------------------------------------
	
	
	public static void deleteActualProductionDtlsByHdrId(String id) {

		System.out.println(HOST + "actualproductiondtl/deleteactualproductiondtlsbyhdrid/" + id);
		restTemplate.delete(HOST + "actualproductiondtl/deleteactualproductiondtlsbyhdrid/" + id);

	}
	
	
	
	
	static public ResponseEntity<List<ProductionDtl>> getActualProductionDtlsByDateAndStore(String date,String store) {

		String path = HOST + "productiondtlresource/productionplanningkitbydateandstore/" + SystemSetting.systemBranch 
				+"/"+store+ "?rdate=" + date;
		
		System.out.println(path);

		ResponseEntity<List<ProductionDtl>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ProductionDtl>>() {
				});

		return response;

	}


	public static ResponseEntity<CloudUserMst> CloudLogin(String checkuser, String checkpassword) {
		String path = HOST + "cloudusermst/" + checkuser +"/"+checkpassword+ "/login";
		System.out.println(path);
		ResponseEntity<CloudUserMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CloudUserMst>() {
				});

		return response;
	}

	public static ResponseEntity<List<ReportSqlHdr>> getAllReportSqlHdrByUserId(String id) {
		String path = HOST + "reportsqlhdrresource/allreportsqlhdrbyuser/" + id;
		System.out.println(path);
		ResponseEntity<List<ReportSqlHdr>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ReportSqlHdr>>() {
				});

		return response;
	}


	public static ResponseEntity<List<CategoryWiseStockMovement>> getPharmacyStockMovementByCategorywise(
			String strfDate, String strtDate, String cat) {

		String path = HOST + "categorywisestockmovementreportresource/getpharmacystockmovementreportbycategorywise/" +
		
                         SystemSetting.systemBranch +"?strfDate=" + strfDate + "&&strtDate=" + strtDate+"&&cat="+cat;

		System.out.println(path);

		ResponseEntity<List<CategoryWiseStockMovement>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<CategoryWiseStockMovement>>() {
		});

		return response;
	}
		

	//==================consumptiom summary report with category===================
	public static ResponseEntity<List<ConsumptionReport>> getConsumptionSummary(String strDate, String strDate1,
			String cat,String username) {
	
		String path = HOST + "consumptionreportresource/consumptionsummary/" + username +"?rdate=" + strDate
				+ "&&tdate=" + strDate1 + "&&category=" + cat;

		System.out.println(path);

		ResponseEntity<List<ConsumptionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReport>>() {

				});

		return response;
	}


	public static ResponseEntity<List<BranchStockReport>> getPharmacyStockReportBranchWise(String startDate,
			String branchCode) {
		String path = HOST + "branchstockreportresource/getpharmacystockreportbranchwise/" + branchCode
				+ "?startDate=" + startDate ;

		System.out.println(path);

		ResponseEntity<List<BranchStockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchStockReport>>() {
		});

		return response;
	}
		

	
	//==================consumptiom summary report with out category===================
	public static ResponseEntity<List<ConsumptionReport>> getConsumptionSummaryWithoutCategory(String strDate, String strDate1,String username) {
	
		String path = HOST + "consumptionreportresource/consumptionsummarywithoutcategory/" + username + "?rdate=" + strDate
				+ "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<ConsumptionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReport>>() {

				});

		return response;
	}


	public static ResponseEntity<List<BranchStockReport>> getPharmacyStockReportBranchWiseAndItems(String startDate,
			String branchCode, String category) {

		String path = HOST + "branchstockreportresource/getpharmacystockreportbranchwiseanditem/" + branchCode
				+ "?startDate=" + startDate +"&&category="+category;

		System.out.println(path);

		ResponseEntity<List<BranchStockReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<BranchStockReport>>() {
		});

		return response;
	}
		

	
	//==================consumptiom detail report with category===================
	public static ResponseEntity<List<ConsumptionReport>> getConsumptionDetail(String strDate, String strDate1,
			String cat, String username) {
		
		String path = HOST + "consumptionreportresource/consumptiondetail/" + username + "?rdate=" + strDate
				+ "&&tdate=" + strDate1 + "&&category=" + cat;

		System.out.println(path);

		ResponseEntity<List<ConsumptionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReport>>() {

				});

		return response;
	}

	public static ResponseEntity<List<PurchaseReturnInvoice>> getPurchaseReturnInvoice(String purchaseHdrId) {
		
		String path = HOST + "purchasereturndtlresource/getpurchasereturninvoicebyhdrid/" + purchaseHdrId;

		System.out.println(path);
		ResponseEntity<List<PurchaseReturnInvoice>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PurchaseReturnInvoice>>() {
				});
		return response;
	}

	public static Double getPurchaseReturnDtl(String purchasedtlId) {
		String path = HOST + "purchasereturndtlresource/getpurchasereturnqtydetails/" + purchasedtlId;

		System.out.println(path);
		Double qtyValue = restTemplate.getForObject(path, Double.class);
		if(null==qtyValue)
		{
			qtyValue=0.0;
		}
		
		return qtyValue;
	}

	
	public static Double getItemStockByStore(String itemId, String batch, String storeFrom) {
		
		String path = HOST + "itembatchdtlresource/getitemstockdetailsbystore/" +itemId+ "/" +batch+ "/" +storeFrom;

		System.out.println(path);
		Double qtyValue = restTemplate.getForObject(path, Double.class);
		if(null==qtyValue)
		{
			qtyValue=0.0;
		}
		
		return qtyValue;
	}

	//==================consumptiom detail report with out category===================
	public static ResponseEntity<List<ConsumptionReport>> getConsumptionDetailWithoutCategory(String strDate, String strDate1,String username) {
		
		String path = HOST + "consumptionreportresource/consumptiondetailwithoutcategory/" + username + "?rdate=" + strDate
				+ "&&tdate=" + strDate1;

		System.out.println(path);

		ResponseEntity<List<ConsumptionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ConsumptionReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<DepartmentMst>> getAlldepartment() {
		String path = HOST + "departmentmstresource/getalldepartment";
		System.out.println(path);

		ResponseEntity<List<DepartmentMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DepartmentMst>>() {
				});

		return response;
		
	}

	public static ResponseEntity<List<DepartmentMst>> getDepartmentByName(String selectedItem) {
		String path = HOST + "departmentmstresource/getdepartmentbyname/" + selectedItem;
		System.out.println(path);

		ResponseEntity<List<DepartmentMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<DepartmentMst>>() {
				});

		return response;
	}

	public static ResponseEntity<List<PharmacySalesReturnReport>> getPharmacySalesReturnWithoutCategory(
			String startDate, String endDate, String branchcode) {
		String path = HOST + "pharmacysalesreturnreportresource/getpharmacysalesreturn/" + branchcode
				+ "?fdate=" + startDate + "&&tdate=" + endDate;

		System.out.println(path);

		ResponseEntity<List<PharmacySalesReturnReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<PharmacySalesReturnReport>>() {
				});

		return response;
	}

	public static ResponseEntity<List<ItemCorrectionReport>> getStockCorrectionReport() {
		 
			String path = HOST + "itemmst/getitemlistwithcountofsalesstockandpurchase";

			System.out.println(path);

			ResponseEntity<List<ItemCorrectionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<ItemCorrectionReport>>() {
					});

			return response;
	}
	
	
	//====MAP-122-api-calling-for-show-all-items-from-cloud-after-item-correction=========anandu===========
	public static ResponseEntity<List<ItemCorrectionReport>> getAllItemsFromCloud() {
		 
		String host =  SystemSetting.CAMUNDAHOST ;
		if(!host.endsWith("/"))
		{
			host = host + "/";
		}		
		String path = host+SystemSetting.myCompany+ "/itemmstresource/getitemlistwithcountofsalesstockandpurchasefromcloud";

		System.out.println(path);

		ResponseEntity<List<ItemCorrectionReport>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<ItemCorrectionReport>>() {
				});

		return response;
}
	
	
	

	public static void deleteItemForCorrectionbyId(String itemId) {
		
		
		String path = HOST + "itemmstresource/deleteitemnewurl/"+itemId;

		System.out.println(path);
		
		restTemplate.delete( HOST + "itemmstresource/deleteitemnewurl/"+itemId);

	}

	
	//feature/MAP-117-api-calling-in-client-for-the-it//
	
public static void deleteItemForCorrectionbyIdFromCloud(String itemId) {
		
		
	String host =  SystemSetting.CAMUNDAHOST ;
	if(!host.endsWith("/"))
	{
		host = host + "/";
	}		
	String path = host+SystemSetting.myCompany+ "/itemmstresource/deleteitemnewurl/"+itemId;

		System.out.println(path);
		
		restTemplate.delete(path);

	}
		

		


//---MAP-118-----------------call the api in client  for item merge on cloud----------

	static public ResponseEntity<ItemMergeMst> ItemMergeInCloud(ItemMergeMst itemMergeMst) {

		String host =  SystemSetting.CAMUNDAHOST ;
		if(!host.endsWith("/"))
		{
			host = host + "/";
		}		
		String path = host+SystemSetting.myCompany+ "/itemmergeresource/itemmergemst";
		System.out.println(path);
		return restTemplate.postForEntity( path, itemMergeMst,
				ItemMergeMst.class);
		

	}
//-----------------------KOTRefresh screen--------------------------------
	
	
	  public static ResponseEntity<List<KOTItems>> getkotdtls() {
	  
	  String path = HOST + "salestranshdrresource/getkotdetail";
	  
	  System.out.println(path);
	  
	  ResponseEntity<List<KOTItems>> response = restTemplate.exchange(path,
	  HttpMethod.GET, null, new ParameterizedTypeReference<List<KOTItems>>() { });
	  
	 
	 return response;
	 }
	  
	 //----------------------item Merge in server ----

	  static public ResponseEntity<ItemMergeMst> ItemMergeInRestServer(ItemMergeMst itemMergeMst) {

			return restTemplate.postForEntity(HOST + "itemmergeresource/itemmergemst", itemMergeMst, ItemMergeMst.class);

		}
	  
	  //----------------------Account Merge dtl get ----
	  
		public static ResponseEntity<List<AccountMergeMst>> getAccountMergeDtls()
		{
			
			  
			  String path = HOST + "accountheads/getaccountmergedtl";
			  
			  System.out.println(path);
			  
			  ResponseEntity<List<AccountMergeMst>> response = restTemplate.exchange(path,
			  HttpMethod.GET, null, new ParameterizedTypeReference<List<AccountMergeMst>>() { });
			  
			 
			 return response;
		}
	  
	  
	  
	
	//---------------------MAP-278================================anandu========================
	  public static ResponseEntity<LinkedAccounts> saveLinkedAccounts(LinkedAccounts linkedAccount) {
			String path = HOST + "linkedaccountsresource/savelinkedaccounts";
			System.out.println(path);
			return restTemplate.postForEntity( path, linkedAccount,
					LinkedAccounts.class);
		}

		public static ResponseEntity<List<LinkedAccounts>> finAllLinkedAccounts() {
			String path = HOST + "linkedaccountsresource/getalllinkedaccounts";
			System.out.println(path);
			ResponseEntity<List<LinkedAccounts>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<LinkedAccounts>>() {
					});

			return response;
		}

		public static ArrayList searchByParentAccount(String newValue) {
			System.out.println("searchparentaccount" + newValue);

			return restTemplate.getForObject(HOST + "searchparentaccount?data=" + newValue, ArrayList.class);
		}

		public static ResponseEntity<LinkedAccounts> getLinkedByParentAndChild(String parentAccountId, String childAccountId) {
			String path = HOST + "linkedaccountsresource/getlinkedaccountsbyparentandchild/"+parentAccountId+"/"+childAccountId;
			System.out.println(path);
			ResponseEntity<LinkedAccounts> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<LinkedAccounts>() {
					});

			return response;
		}

		public static ResponseEntity<AccountMergeMst> getAccountMergeMstById(String fromAccountId, String toAccountId) {
               String path = HOST + "accountheads/getaccountmergemstbyid/"+fromAccountId+"/"+toAccountId;
			  
			  System.out.println(path);
			  
			  ResponseEntity<AccountMergeMst> response = restTemplate.exchange(path,
			  HttpMethod.GET, null, new ParameterizedTypeReference<AccountMergeMst>() { });
			  
			 
			 return response;
		}

		public static ArrayList SearchAccountByNameForNewPopUp(String searchData) {
			
			 String path = HOST + "accountheads/searchaccountbysupplierandboth?data="+searchData;
			  
			  System.out.println(path);
			  
			  return restTemplate.getForObject(path, ArrayList.class);
			  
			 
			  
		}

		
		/*
		 * new url for getting customer from account heads by Account name====05-01-2022
		 */
		static public ResponseEntity<AccountHeads> getAccountHeadsByName(String custName) {
			String path = HOST + "accountheadsresource/accountheadsbyname/" + custName;
			System.out.println("=============path=========" + path);

			ResponseEntity<AccountHeads> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<AccountHeads>() {
					});
			return response;
		}
		public static ArrayList SearchAccountByTypeAndBoth(String searchData, String primaryType,String commonType) {
  String path = HOST + "accountheads/searchaccountbysupplierandboth/"+primaryType+"/"+commonType+"?data=" +searchData;
  
 
		       System.out.println(path);
			 
				return restTemplate.getForObject(path, ArrayList.class);
		}

		public static ResponseEntity<AccountHeads> getAccountHeadsByNameAndBranchCode(String accountName,
				String systemBranch) {
			String path = HOST + "accountheadsresource/accountheadsbynameandbrachcode/" + accountName + "/" + systemBranch;
			System.out.println(path);

			ResponseEntity<AccountHeads> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<AccountHeads>() {
					});
			return response;
		}

	
	
	//-----------------------Party registration new url---------------
		public static void updatePartyRegistration(AccountHeads accountHeads) {
			restTemplate.put(HOST + "accountheadsresource/" + accountHeads.getId() + "/updatepartycreation", accountHeads);
			return;
		}
	
		static public ResponseEntity<AccountHeads> getNameById(String partycreatinId) {

			String path = HOST + "accountheadsbyid/" + partycreatinId;
			System.out.println("=============path=========" + path);

			ResponseEntity<AccountHeads> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<AccountHeads>() {
					});
			return response;
		}
	
	
	
		public static ResponseEntity<List<AccountHeads>> AccountHeadsByGst(String gst) {
			String path = HOST + "accountheadsresource/partycreationbygst/" + gst;
			System.out.println(path);

			ResponseEntity<List<AccountHeads>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<AccountHeads>>() {
					});
			return response;
		}
	
	
		static public ResponseEntity<AccountHeads> partyRegistration(AccountHeads partyRegistration) {

			System.out.println(HOST + "accountheadsresource/partyregistration");
			return restTemplate.postForEntity(HOST + "accountheadsresource/partyregistration", partyRegistration, AccountHeads.class);

		}
		
		// new save url of itemmst with online or offline process========anandu=====01-07-2021
		static public ResponseEntity<AccountHeads> partyRegistration(AccountHeads partyRegistration, String status) {

			System.out.println(HOST + "accountheadsresource/" + status);
			return restTemplate.postForEntity(HOST + "customerregistration/" + status, partyRegistration, AccountHeads.class);

		}
		//=====================end new url=========================01-07-2021======
	
	
		// ----------------------SearchPartyByName---
		static public ArrayList SearchPartyByName(String searchData) {
			System.out.println("partysearchpartysearchpartysearch" + searchData);

			return restTemplate.getForObject(HOST + "partysearch?data=" + searchData, ArrayList.class);
		}
		
		
		/*
		 * new single url for common final save of sales including 
		 * sales_trans_hdr, sales_receipts, account_receivable
		 * jan 14 2022
		 */
		public static void FinalSaveForSales(SalesFinalSave salesFinalSave) {
			String date = SystemSetting.UtilDateToString(SystemSetting.systemDate, "dd-MM-yyyy");
			System.out.println(HOST + "salestranshdrresource/salesfinalsave");
			restTemplate.put(HOST + "salestranshdrresource/salesfinalsave/"+ SystemSetting.STORE
					+ "?logdate=" + date, salesFinalSave,SalesFinalSave.class);

			
		}
	
	
		
		
		

		public static ResponseEntity<AccountHeads> getAccountHeadByNameAndStatus(String selectedItem,
				Boolean accountCreateStatus) {
			String path = HOST + "accountheadsresource/accountheadsbynamewithstatus/" + accountCreateStatus+"?selectedItem="+selectedItem;
			System.out.println("===path ==" + path);
			ResponseEntity<AccountHeads> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<AccountHeads>() {
					});
			return response;
		}

//		public static ResponseEntity<InstallationModelClass> installationModelClass() {
//			String path = HOST + "installationmodelclassresource/installationfile"  ;
//
//			System.out.println(path);
//			ResponseEntity<InstallationModelClass> response = restTemplate.exchange(path, HttpMethod.GET, null,
//					new ParameterizedTypeReference<InstallationModelClass>() {
//					});
//
//			return response;
//		}
		
		
		
		public static  String installationModelClass() {

			String path = HOST + "/installationmodelclassresource/fileinstallation";
			System.out.println(path);
				return restTemplate.getForObject(path, String.class);

		}
		
		public static  String installationFilecreation() {

			String path = HOST + "/installationmodelclassresource/installationmodel";
			System.out.println(path);
			return restTemplate.getForObject(path,String.class);

	}

		public static ResponseEntity<List<AccountHeadsProperties>> getAllAccountProperty() {
			String path = HOST + "accountheadspropertiesresource/findallaccountheadsproperties";
			System.out.println(path);

			ResponseEntity<List<AccountHeadsProperties>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<AccountHeadsProperties>>() {
					});
			return response;
		}

		public static ResponseEntity<AccountHeads> getAccountHeadsByNameRequestParam(String text) {
			String path = HOST + "accountheadsresource/retrieveaccountheadsbynamerequestparam?accountdata=" + text;

			System.out.println(path);
			ResponseEntity<AccountHeads> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<AccountHeads>() {
					});

			return response;
		}

		public static ResponseEntity<AccountHeadsProperties> getAccountHeadsPropertiesByAccountNameAndProperty(
				String id, String propertyName) {
			String path = HOST + "accountheadspropertiesresource/getaccountheadspropertiesbyaccountidandpropertyname/" + id
					+ "?propertyname=" + propertyName;
			System.out.println(path);

			ResponseEntity<AccountHeadsProperties> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<AccountHeadsProperties>() {
					});
			return response;
		}

		public static ResponseEntity<AccountHeadsProperties> saveAccountHeadsProperties(
				AccountHeadsProperties accountHeadsProperties) {
			return restTemplate.postForEntity(HOST + "accountheadspropertiesresource/createaccountheadsproperty",
					accountHeadsProperties, AccountHeadsProperties.class);
		}

		public static void deleteAccountHeadsProperties(String id) {
			System.out.println("accountheadspropertiesresource/deleteaccountheadsproperties/"+id);
		restTemplate.delete(HOST + "accountheadspropertiesresource/deleteaccountheadsproperties/" + id);
	
			
			
		}
		
		
		
		
		public static ResponseEntity<BatchPriceDefinition> getBatchPriceDefinitionByItemIdAndPriceId(String itemId, String priceId,
				String unitId) {
			String path = HOST + "batchpricedefinition/getbatchpricedefinition/" + itemId + "/" + priceId + "/" + unitId;
			System.out.println(path);
			ResponseEntity<BatchPriceDefinition> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<BatchPriceDefinition>() {
					});

			return response;
		}

		public static String publishScheme(String schemeName) {
			String path = HOST + "schemeresource/publishschemetoallbranches/" + schemeName;
			System.out.println("===path ==" + path);
			  return restTemplate.getForObject(path,  String.class);
		}

		
		
		
		
	
	}


















	
		


		

	



