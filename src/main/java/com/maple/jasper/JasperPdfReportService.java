package com.maple.jasper;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.Get;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.maple.javapos.print.DayEndThermalPrintingFunction;
import com.maple.javapos.print.HotCakesStockTransferPrint;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.controllers.ReceiptModeWiseReport;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.AdditionalExpense;
import com.maple.mapleclient.entity.BalanceSheetConfigAsset;
import com.maple.mapleclient.entity.BalanceSheetConfigLiability;
import com.maple.mapleclient.entity.BalanceSheetHorizontal;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryWiseSalesReport;
import com.maple.mapleclient.entity.CurrencyMst;

import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.DayEndClosureDtl;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.DayEndReportStore;
import com.maple.mapleclient.entity.GSTOutputDetailReport;
import com.maple.mapleclient.entity.InsuranceCompanyMst;
import com.maple.mapleclient.entity.InvoiceFormatMst;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemWiseDtlReport;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.entity.JournalDtl;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.OpeningBalanceMst;
import com.maple.mapleclient.entity.OtherBranchSalesTransHdr;
import com.maple.mapleclient.entity.PDCPayment;
import com.maple.mapleclient.entity.PDCReceipts;
import com.maple.mapleclient.entity.PatientMst;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentReports;

import com.maple.report.entity.AMDCDailySalesSummaryReport;

import com.maple.mapleclient.entity.PharmacyItemMovementAnalisysReport;
import com.maple.mapleclient.entity.PharmacySalesDetailReport;
import com.maple.mapleclient.entity.PharmacySalesReturnReport;

import com.maple.mapleclient.entity.PharmacyItemMovementAnalisysReport;

import com.maple.mapleclient.entity.PharmacySalesDetailReport;
import com.maple.mapleclient.entity.PharmacySalesReturnReport;

import com.maple.mapleclient.entity.PharmacyItemMovementAnalisysReport;

import com.maple.mapleclient.entity.PharmacySalesDetailReport;
import com.maple.mapleclient.entity.PharmacySalesReturnReport;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.entity.PurchaseAdditionalExpenseHdr;
import com.maple.mapleclient.entity.PurchaseDtl;
import com.maple.mapleclient.entity.PurchaseHdr;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.ReceiptReports;
import com.maple.mapleclient.entity.SaleOrderReceipt;
import com.maple.mapleclient.entity.SalesOrderDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesReport;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.ServiceInHdr;
import com.maple.mapleclient.entity.SessionEndClosureMst;
import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.entity.StoreChangeDtl;
import com.maple.mapleclient.entity.StoreChangeMst;
import com.maple.mapleclient.entity.StoreMst;
import com.maple.mapleclient.entity.TermsAndConditionsMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.entity.VoucherNumberDtlReport;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;
import com.maple.report.entity.ActualProductionReport;
import com.maple.report.entity.B2bSalesReport;
import com.maple.report.entity.PharmacyDayClosingReport;
import com.maple.report.entity.BalanceSheetAssetReport;
import com.maple.report.entity.BalanceSheetLiabilityReport;
import com.maple.report.entity.BatchWiseStockEvaluationReport;
import com.maple.report.entity.CardReconcileReport;
import com.maple.report.entity.CategoryWiseStockMovement;
import com.maple.report.entity.ConsumptionReport;
import com.maple.report.entity.CustomerBalanceReport;
import com.maple.report.entity.DailyPaymentSummaryReport;
import com.maple.report.entity.DailyReceiptsSummaryReport;
import com.maple.report.entity.DailySaleSummaryReport;
import com.maple.report.entity.DailySalesReport;
import com.maple.report.entity.DamageEntryReport;
import com.maple.report.entity.DayBook;
import com.maple.mapleclient.entity.PharmacyBranchStockReport;
import com.maple.report.entity.DayEndProcessing;
import com.maple.report.entity.ExpenseReport;
import com.maple.report.entity.FinishedProduct;
import com.maple.report.entity.GSTInputDtlAndSmryReport;
import com.maple.report.entity.HsnCodeSaleReport;
import com.maple.report.entity.HsnWisePurchaseDtlReport;
import com.maple.report.entity.HsnWiseSalesDtlReport;
import com.maple.report.entity.ImportPurchaseSummary;
import com.maple.report.entity.IncomeReport;
import com.maple.report.entity.InsuranceCompanySalesSummaryReport;
import com.maple.report.entity.InsuranceWiseSalesReport;
import com.maple.report.entity.IntentInReport;
import com.maple.report.entity.IntentVoucherReport;
import com.maple.report.entity.InventoryReorderStatusReport;
import com.maple.report.entity.ItemLocationReport;
import com.maple.report.entity.ItemNotInBatchPriceReport;
import com.maple.report.entity.ItemSaleReport;
import com.maple.report.entity.ItemStockReport;
import com.maple.report.entity.OrgMemberWiseReceiptReport;
import com.maple.report.entity.OrgMonthlyAccPayReport;
import com.maple.report.entity.OrgMonthlyAccountReport;
import com.maple.report.entity.OrgPaymentVoucher;
import com.maple.report.entity.OrgReceiptInvoice;
import com.maple.report.entity.PaymentVoucher;
import com.maple.report.entity.PendingAmountReport;
import com.maple.report.entity.PharmacyBrachStockMarginReport;
import com.maple.report.entity.PharmacyLocalPurchaseDetailsReport;
import com.maple.report.entity.ProductConversionReport;
import com.maple.report.entity.ProductionDtlsReport;
import com.maple.report.entity.PurchaseReport;
import com.maple.report.entity.PurchaseReturnDetailAndSummaryReport;
import com.maple.report.entity.RawMaterialIssueReport;
import com.maple.report.entity.RawMaterialReturnReport;
import com.maple.report.entity.ReceiptInvoice;
import com.maple.report.entity.ReceiptModeReport;
import com.maple.report.entity.RetailSalesDetailReport;
import com.maple.report.entity.RetailSalesSummaryReport;
import com.maple.report.entity.SaleOrderDetailsReport;
import com.maple.report.entity.SaleOrderDueReport;
import com.maple.report.entity.SaleOrderInvoiceReport;
import com.maple.report.entity.SaleOrderReport;
import com.maple.report.entity.SaleOrderTax;
import com.maple.report.entity.SalesInvoiceReport;
import com.maple.report.entity.SalesModeWiseBillReport;
import com.maple.report.entity.SalesModeWiseSummaryreport;
import com.maple.report.entity.SalesOrderAdvanceBalanceReport;
import com.maple.report.entity.SalesReturnInvoiceReport;
import com.maple.report.entity.SalesTaxSplitReport;
import com.maple.report.entity.ServiceInReport;
import com.maple.report.entity.ServiceInvoice;
import com.maple.report.entity.ShortExpiryReport;
import com.maple.report.entity.StockReport;
import com.maple.report.entity.PharmacyNewCustomerWiseSaleReport;
import com.maple.report.entity.PharmacyPurchaseReturnDetailReport;
import com.maple.report.entity.StockSummaryDtlReport;
import com.maple.report.entity.StockTransferInReport;
import com.maple.report.entity.StockTransferOutReport;
import com.maple.report.entity.SummaryStatementOfAccountReport;
import com.maple.report.entity.SupplierBillwiseDueDayReport;
import com.maple.report.entity.SupplierMonthlySummary;
import com.maple.report.entity.TaxSummaryMst;
import com.maple.report.entity.BranchStockReport;
import com.maple.report.entity.PharmacyPhysicalStockVarianceReport;
//
//import com.maple.report.entity.WriteOffSummaryReport;
//
//import com.maple.report.entity.LocalPurchaseSummayReport;
import com.maple.report.entity.WriteOffDetailAndSummaryReport;

import com.maple.report.entity.AMDCShortExpiryReport;
import com.maple.report.entity.WholeSaleDetailReport;
import com.maple.report.entity.PoliceReport;
import com.maple.report.entity.PharmacyDailySalesTransactionReport;
import com.maple.report.entity.PharmacyItemWiseSalesReport;
import com.maple.mapleclient.entity.PharmacyGroupWiseSalesReport;
import com.maple.mapleclient.entity.PharmacyPurchaseReport;
import javafx.application.HostServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import com.maple.report.entity.LocalPurchaseDetailReport;
import com.maple.report.entity.PharmacyDayEndReport;
import com.maple.report.entity.LocalPurchaseSummaryReport; 

import com.maple.report.entity.PurchaseReturnInvoice;
@Service
public class JasperPdfReportService {

	public static void AccountHeadsReport() throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/accountheads.pdf";

		// Call url
		ResponseEntity<List<AccountHeads>> accountHeadsResponse = RestCaller.getAccountHeads();

		List<AccountHeads> accountHeadsList = accountHeadsResponse.getBody();
		for (AccountHeads accountHeads : accountHeadsList) {
			accountHeads.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
			accountHeads.setBranchName(SystemSetting.systemBranch);
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(accountHeadsList);

		String jrxmlPath = "jrxml/accountheads.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void OrgPaymentReport(String voucherNo, String date, String memid) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/orgpayment" + voucherNo + date + ".pdf";

		// Call url

		ResponseEntity<List<OrgPaymentVoucher>> PaymentVoucherReportResponse = RestCaller
				.getOrgPaymentVoucherReport(SystemSetting.getSystemBranch(), voucherNo, date, memid);

		List<OrgPaymentVoucher> PaymentVoucherReport = PaymentVoucherReportResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(PaymentVoucherReport);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBankName());
		parameters.put("BranchAddress1", branchMst.getBranchAddress1());
		parameters.put("BranchAddress2", branchMst.getBranchAddress2());
		parameters.put("BranchEmail", branchMst.getBranchEmail());
		parameters.put("BranchGst", branchMst.getBranchGst());
		parameters.put("BranchPhn", branchMst.getBranchTelNo());
		parameters.put("BranchState", branchMst.getBranchState());

		String jrxmlPath = "jrxml/OrgPaymentInvoice.jrxml";
		if (PaymentVoucherReport.size() > 0) {
			String amount = PaymentVoucherReport.get(0).getAmount() + "";

			String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");

			parameters.put("AmountInWords", amountinwords);
			System.out.println("" + amountinwords);
		}
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PendingCashReport(String startDate, String endDate, String accountId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/Dueamount" + accountId + startDate + endDate + ".pdf";

		// Call url

		ResponseEntity<List<PendingAmountReport>> PendingBalanceReport = RestCaller.getPendingAmountReport(startDate,
				endDate, accountId);

		List<PendingAmountReport> PendingBalanceReportList = PendingBalanceReport.getBody();
		for (PendingAmountReport dueamount : PendingBalanceReportList) {
			Date date = SystemSetting.StringToUtilDate(startDate, "yyyy-MM-dd");
			dueamount.setCurrentDate(date);

			Date edate = SystemSetting.StringToUtilDate(endDate, "yyyy-MM-dd");
			dueamount.setEndDate(edate);
			dueamount.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
			dueamount.setBranchName(SystemSetting.getUser().getBranchCode());
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				PendingBalanceReportList);

		String jrxmlPath = "jrxml/PendingAmountReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void StatementOfAccountReport() throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/summarystatementofaccounts.pdf";

		// Call url

		ResponseEntity<List<SummaryStatementOfAccountReport>> statementOfAccountsResponse = RestCaller
				.getStatementOfAccounts();

		List<SummaryStatementOfAccountReport> summaryStatementOfAccountReportList = statementOfAccountsResponse
				.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				summaryStatementOfAccountReportList);

		String jrxmlPath = "jrxml/summarystatementofaccounts.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SupplierReport() throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/supplier.pdf";

		// Call url

		ResponseEntity<List<AccountHeads>> accountHeadsResponse = RestCaller.getAccountHeads();

		List<AccountHeads> accountHeadsList = accountHeadsResponse.getBody();
		for (AccountHeads accountHeads : accountHeadsList) {
			accountHeads.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
			accountHeads.setBranchName(SystemSetting.systemBranch);
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(accountHeadsList);

		String jrxmlPath = "jrxml/suppliers.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DailyCustomerWiseReport(String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/customerwisereport" + date + ".pdf";

		// Call url

		ResponseEntity<List<SalesTransHdr>> CustomerWiseResponse = RestCaller.getCustomerWiseReport(date);

		List<SalesTransHdr> CustomerWise = CustomerWiseResponse.getBody();
		for (SalesTransHdr salesTransHdr : CustomerWise) {
			
			salesTransHdr.setCustomerId(salesTransHdr.getAccountHeads().getAccountName());
			salesTransHdr.setBranchCode(SystemSetting.systemBranch);
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(CustomerWise);

		String jrxmlPath = "jrxml/customerWiseReports.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SaleOrderStatusReport(String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/saleorderstatus" + date + ".pdf";

		// Call url

		ResponseEntity<List<SalesOrderDtl>> SaleOrderStatusResponse = RestCaller.getSaleOrderStatusReport(date);

		List<SalesOrderDtl> SaleOrderStatus = SaleOrderStatusResponse.getBody();

		for (SalesOrderDtl salesOrderDtl : SaleOrderStatus) {
			salesOrderDtl.setBranchCode(SystemSetting.systemBranch);
			salesOrderDtl.setrDate(date);
			salesOrderDtl.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
		}

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(SaleOrderStatus);

		String jrxmlPath = "jrxml/saleorderstatusreport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void CustomerRegistrationReport() throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/customerregistration.pdf";

		// Call url

		ResponseEntity<List<AccountHeads>> customerResponse = RestCaller.getAccountHeads();

		List<AccountHeads> customerRegistration = customerResponse.getBody();
		for (AccountHeads customerMst : customerRegistration) {
			customerMst.setBranchName(SystemSetting.systemBranch);
			customerMst.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
			System.out.println(SystemSetting.getUser().getCompanyMst().getCompanyName());
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(customerRegistration);

		String jrxmlPath = "jrxml/customerregistration.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemReport() throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/itemmsts.pdf";

		// Call url

		ResponseEntity<List<ItemMst>> itemMstResponse = RestCaller.getItemNames();

		List<ItemMst> itemMsts = itemMstResponse.getBody();
		for (ItemMst itemMst : itemMsts) {
			itemMst.setBranchName(SystemSetting.systemBranch);
			itemMst.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());

		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemMsts);

		String jrxmlPath = "jrxml/itemmst.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

//public static void IntentReport() throws JRException {
//		String pdfToGenerate = SystemSetting.reportPath + "/intent.pdf";
//		ResponseEntity<List<IntentDtl>> intentDtlResponse = RestCaller.getIntentreports();
//		List<IntentDtl> intentdtls = intentDtlResponse.getBody();
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(intentdtls);
//		String jrxmlPath = "jrxml/intentdtl.jrxml";
//		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
//		HashMap parameters = null;
//		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
//		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
//		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
//		hs.showDocument(pdfToGenerate);
//	}

	public static void TaxInvoiceReport(String voucherNumber, String date) throws JRException {

		// voucherNumber = "000173";
		// date = "2019-07-21";
		System.out.println("======TaxInvoiceReport===========");

		String pdfToGenerate = SystemSetting.reportPath + "/invoice" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<SalesInvoiceReport>> invoiceResponse = RestCaller.getInvoice(voucherNumber, date);
		List<SalesInvoiceReport> invoice = invoiceResponse.getBody();

		ResponseEntity<List<TaxSummaryMst>> invoiceTaxResponse = RestCaller.getInvoiceTax(voucherNumber, date);
		List<TaxSummaryMst> invoiceTax = invoiceTaxResponse.getBody();

		ResponseEntity<List<TaxSummaryMst>> taxResponse = RestCaller.getInvoiceTotal(voucherNumber, date);
		List<TaxSummaryMst> invoiceTaxSum = taxResponse.getBody();

		ResponseEntity<List<SalesInvoiceReport>> customerResponse = RestCaller.getInvoiceCustomerDtl(voucherNumber,
				date);
		List<SalesInvoiceReport> custDetails = customerResponse.getBody();

		Double cessAmount = RestCaller.getCessAmountInTaxInvoiceReport(voucherNumber, date);

		Double taxableAmount = RestCaller.getTaxableInvoiceAmount(voucherNumber, date);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(invoiceTax);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(invoiceTax);
//		JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(custDetails);

		HashMap<String, Object> parameters = new HashMap();

		String jrxmlPath = "";
		if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("HARDWARE")) {
			parameters.put("logoname", "stmarys.png");
			if (null != invoice) {
				if (null != invoice.get(0)) {
					SalesInvoiceReport salesInvoice = invoice.get(0);
					if (salesInvoice.getDiscount().equalsIgnoreCase(null)) {
						parameters.put("DiscountAmount", salesInvoice.getInvoiceDiscount());
						parameters.put("DiscountPercentage", salesInvoice.getDiscountPercent());

						parameters.put("DiscountAmountLabel", "Discount Amount:");
						parameters.put("DiscountPercentageLabel", "Discount Percentange:");
					}
				}
			}

			for (SalesInvoiceReport salesInvoiceReport : custDetails) {
				SalesInvoiceReport salesInvoice = new SalesInvoiceReport();
				if (null != invoice && invoice.size() > 0) {
					salesInvoice = invoice.get(0);
				}
				if (null != salesInvoice) {

					// ---------------------version 2.0.0 surya

					ResponseEntity<AccountHeads> customerMstResp = RestCaller
							.getAccountHeadByName(custDetails.get(0).getCustomerName());
					AccountHeads customerMst = customerMstResp.getBody();

					ResponseEntity<List<InvoiceFormatMst>> jasperByPriceTypeResp = RestCaller
							.getJasperByPriceTypeParamPriceTypeId(customerMst.getPriceTypeId());

					List<InvoiceFormatMst> jasperList = jasperByPriceTypeResp.getBody();

					// ---------------------version 2.0.0 surya end
					if (!salesInvoice.getSalesMode().equalsIgnoreCase("B2B")) {
						// ---------------------version 2.0.0 surya

						if (jasperList.size() > 0) {
							jrxmlPath = "jrxml/" + jasperList.get(0).getJasperName() + ".jrxml";

						} else {
							// ---------------------version 2.0.0 surya end

							jrxmlPath = "jrxml/HWTaxInvoice.jrxml";
							// ---------------------version 2.0.0 surya

						}
						// ---------------------version 2.0.0 surya end

						if (null != salesInvoiceReport.getLocalCustomerName()) {
							salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
							salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
							salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
							salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
							salesInvoiceReport.setCustomerGst(null);
							salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
						}
					} else {
//						jrxmlPath = "jrxml/HWTaxInvoiceWithGst.jrxml";
						jrxmlPath = "jrxml/HWTaxInvoiceWithOutMrp.jrxml";

					}
				}
			}
		} else {
			SalesTransHdr getSalesHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNumber, date);

			// jrxmlPath = "jrxml/taxinvoicee.jrxml";
			if (null == getSalesHdr.getAccountHeads().getCustomerDiscount()) {
				getSalesHdr.getAccountHeads().setCustomerDiscount(0.0);
			}
			if (getSalesHdr.getAccountHeads().getCustomerDiscount() > 0) {
				jrxmlPath = "jrxml/discountTaxInvoice.jrxml";
			} else {
				jrxmlPath = "jrxml/taxinvoicee.jrxml";
			}

			if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("BAKERY_WITH_BATCH")) {
				jrxmlPath = "jrxml/TaxinvoiceeWithbatch.jrxml";

			}
			for (SalesInvoiceReport salesInvoiceReport : custDetails) {
				if (null != salesInvoiceReport.getLocalCustomerName()) {
					salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
					salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
					salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
					salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
					salesInvoiceReport.setCustomerGst(null);
					salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
				}
			}
		}

		// String jrxmlPath2 = "jrxml/taxsumary.jrxml";

		// String jrxmlPath3 = "jrxml/taxsumarytable.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		// JasperReport jasperReport3 = JasperCompileManager.compileReport(jrxmlPath3);

		// JasperReport jasperReport2 = JasperCompileManager.compileReport(jrxmlPath2);

//		parameters.put("subreportdata", jrBeanCollectionDataSource2);
//		parameters.put("stax_invoice_tax_sub", "jrxml/tax_invoice_tax_sub.jasper");

		String LocalCustomerName = null;
		String LocalCustomerAddress = null;
		String LocalCustomerPoneNo = null;
		String LocalCustomerPlace = null;
		String LocalCustomerSate = null;
		if (null != invoice) {
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			if (invoice.size() > 0) {
				salesInvoiceReport = invoice.get(0);
				if (null != salesInvoiceReport.getSalesMode()) {
					if (salesInvoiceReport.getSalesMode().equalsIgnoreCase("B2B")) {
						SalesInvoiceReport salesCust = new SalesInvoiceReport();
						if (null != custDetails) {
							salesCust = custDetails.get(0);
						}
						LocalCustomerName = salesCust.getLocalCustomerName();
						LocalCustomerAddress = salesCust.getLocalCustomerAddres();
						LocalCustomerPoneNo = salesCust.getLocalCustomerPhone();
						LocalCustomerPlace = salesCust.getLocalCustomerPlace();
						LocalCustomerSate = salesCust.getLocalCustomerState();

					}
				}
			}
		}

		parameters.put("LocalCustomerName", LocalCustomerName);
		parameters.put("LocalCustomerAddress", LocalCustomerAddress);
		parameters.put("LocalCustomerPoneNo", LocalCustomerPoneNo);
		parameters.put("LocalCustomerPlace", LocalCustomerPlace);
		parameters.put("LocalCustomerSate", LocalCustomerSate);

		parameters.put("subreportdata2", jrBeanCollectionDataSource3);
		parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");

		parameters.put("CustomerAddress", custDetails.get(0).getCustomerAddress());
		parameters.put("CustomerName", custDetails.get(0).getCustomerName());
		parameters.put("PhoneNumber", custDetails.get(0).getCustomerPhone());
		parameters.put("CustomerGst", custDetails.get(0).getCustomerGst());
		parameters.put("CustomerPlace", custDetails.get(0).getCustomerPlace());

		String taxableAmountS = "0.0";
		if (invoice.size() > 0) {
			Double amountWithDiscount = invoice.get(0).getAmount() + invoice.get(0).getInvoiceDiscount();
			parameters.put("InvoiceAmountWithDiscount", amountWithDiscount);
			BigDecimal amountBig = new BigDecimal(amountWithDiscount);
			amountBig = amountBig.setScale(0, BigDecimal.ROUND_CEILING);

//			BigDecimal taxableAmountInBig = new BigDecimal(taxableAmount);
//			taxableAmountInBig = taxableAmountInBig.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			String amount = amountBig + "";

			if (null != taxableAmount) {
				taxableAmountS = taxableAmount + "";

			}
			String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");

			String taxamount = "0.0";
			Double taxDouble = 0.0;

			if (null != invoiceTax) {
				if (invoiceTax.size() > 0) {

					for (TaxSummaryMst tax : invoiceTax) {
						taxDouble = taxDouble + tax.getTaxAmount();
					}

				}
			}

			taxamount = taxDouble + "";

			String taxableAmountInWords = SystemSetting.AmountInWords(taxableAmountS, "Rs", "Ps");
			String taxamountinwords = SystemSetting.AmountInWords(taxamount, "Rs", "Ps");
			parameters.put("AmountInWords", amountinwords);
			parameters.put("taxableAmountInWords", taxableAmountInWords);
			parameters.put("TaxAmountInWords", taxamountinwords);
			parameters.put("taxamount", taxamount);

			parameters.put("taxableAmount", taxableAmountS);
			if (null == cessAmount) {
				cessAmount = 0.0;
			}

			parameters.put("CessAmount", cessAmount);

			String gst = "";
		}
//		for(TaxSummaryMst taxSummaryMst : invoiceTax)
//		{
//			gst = gst + taxSummaryMst.getTaxPercentage()+"\t"+taxSummaryMst.getTaxAmount()+"\n";
////		}
//		Double AmtAftrDiscount = 0.0;
//		if(invoice.size() >= 0)
//		{
//		if(null != invoice.get(0).getInvoiceDiscount())
//		{
//			 AmtAftrDiscount = invoice.get(0).getAmount()+invoice.get(0).getInvoiceDiscount();
//			
//		}else {
//			AmtAftrDiscount = invoice.get(0).getAmount();
//		}
//		} 
//		parameters.put("AmtAftrDiscount",AmtAftrDiscount);

		double cgstPercent = 0.0;
		double sgstPercent = 0.0;
		double igstPercent = 0.0;

		if (invoiceTaxSum.size() > 0) {

			cgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfcgstAmount(), 2);
			sgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfsgstAmount(), 2);
			igstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfigstAmount(), 2);
		}
		parameters.put("cgst%", cgstPercent);
		parameters.put("sgst%", sgstPercent);
		parameters.put("igst%", igstPercent);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SalesReturnTaxInvoiceReport(String voucherNumber, String date) throws JRException {

		// voucherNumber = "000173";
		// date = "2019-07-21";
		System.out.println("======TaxInvoiceReport===========");

		String pdfToGenerate = SystemSetting.reportPath + "/salesReturn" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<SalesReturnInvoiceReport>> invoiceResponse = RestCaller.getSalesReturnInvoice(voucherNumber,
				date);
		List<SalesReturnInvoiceReport> invoice = invoiceResponse.getBody();

		ResponseEntity<List<TaxSummaryMst>> invoiceTaxResponse = RestCaller.getReturnInvoiceTax(voucherNumber, date);
		List<TaxSummaryMst> invoiceTax = invoiceTaxResponse.getBody();

		ResponseEntity<List<TaxSummaryMst>> taxResponse = RestCaller.getReturnInvoiceTotal(voucherNumber, date);
		List<TaxSummaryMst> invoiceTaxSum = taxResponse.getBody();

		ResponseEntity<List<SalesReturnInvoiceReport>> customerResponse = RestCaller
				.getReturnInvoiceCustomerDtl(voucherNumber, date);
		List<SalesReturnInvoiceReport> custDetails = customerResponse.getBody();

		Double cessAmount = RestCaller.getCessAmountInTaxReturnInvoiceReport(voucherNumber, date);

		Double taxableAmount = RestCaller.getReturnTaxableInvoiceAmount(voucherNumber, date);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(invoiceTax);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(invoiceTax);
//		JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(custDetails);

		HashMap<String, Object> parameters = new HashMap();

		String jrxmlPath = "";
		if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("HARDWARE")) {
			parameters.put("logoname", "stmarys.png");
			if (null != invoice) {
				if (null != invoice.get(0)) {
					SalesReturnInvoiceReport salesInvoice = invoice.get(0);

				}
			}

			for (SalesReturnInvoiceReport salesInvoiceReport : custDetails) {
				SalesReturnInvoiceReport salesInvoice = new SalesReturnInvoiceReport();
				if (null != invoice && invoice.size() > 0) {
					salesInvoice = invoice.get(0);
				}
				if (null != salesInvoice) {
					if (!salesInvoice.getSalesMode().equalsIgnoreCase("B2B")) {
						jrxmlPath = "jrxml/HWTaxInvoice.jrxml";

						if (null != salesInvoiceReport.getLocalCustomerName()) {
							salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
							salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
							salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
							salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
							salesInvoiceReport.setCustomerGst(null);
							salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
						}
					} else {
						jrxmlPath = "jrxml/HWTaxInvoiceWithGst.jrxml";
					}
				}
			}
		} else {
			jrxmlPath = "jrxml/SalesReturnInvoice.jrxml";

			for (SalesReturnInvoiceReport salesInvoiceReport : custDetails) {
				if (null != salesInvoiceReport.getLocalCustomerName()) {
					salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
					salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
					salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
					salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
					salesInvoiceReport.setCustomerGst(null);
					salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
				}
			}
		}

		// String jrxmlPath2 = "jrxml/taxsumary.jrxml";

		// String jrxmlPath3 = "jrxml/taxsumarytable.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		// JasperReport jasperReport3 = JasperCompileManager.compileReport(jrxmlPath3);

		// JasperReport jasperReport2 = JasperCompileManager.compileReport(jrxmlPath2);

//		parameters.put("subreportdata", jrBeanCollectionDataSource2);
//		parameters.put("stax_invoice_tax_sub", "jrxml/tax_invoice_tax_sub.jasper");

		String LocalCustomerName = null;
		String LocalCustomerAddress = null;
		String LocalCustomerPoneNo = null;
		String LocalCustomerPlace = null;
		String LocalCustomerSate = null;
		if (null != invoice) {
			SalesReturnInvoiceReport salesInvoiceReport = new SalesReturnInvoiceReport();
			if (invoice.size() > 0) {
				salesInvoiceReport = invoice.get(0);
				if (null != salesInvoiceReport.getSalesMode()) {
					if (salesInvoiceReport.getSalesMode().equalsIgnoreCase("B2B")) {
						SalesReturnInvoiceReport salesCust = new SalesReturnInvoiceReport();
						if (null != custDetails) {
							salesCust = custDetails.get(0);
						}
						LocalCustomerName = salesCust.getLocalCustomerName();
						LocalCustomerAddress = salesCust.getLocalCustomerAddres();
						LocalCustomerPoneNo = salesCust.getLocalCustomerPhone();
						LocalCustomerPlace = salesCust.getLocalCustomerPlace();
						LocalCustomerSate = salesCust.getLocalCustomerState();

					}
				}
			}
		}

		parameters.put("LocalCustomerName", LocalCustomerName);
		parameters.put("LocalCustomerAddress", LocalCustomerAddress);
		parameters.put("LocalCustomerPoneNo", LocalCustomerPoneNo);
		parameters.put("LocalCustomerPlace", LocalCustomerPlace);
		parameters.put("LocalCustomerSate", LocalCustomerSate);

		parameters.put("subreportdata2", jrBeanCollectionDataSource3);
		parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");

		parameters.put("CustomerAddress", custDetails.get(0).getCustomerAddress());
		parameters.put("CustomerName", custDetails.get(0).getCustomerName());
		parameters.put("PhoneNumber", custDetails.get(0).getCustomerPhone());
		parameters.put("CustomerGst", custDetails.get(0).getCustomerGst());
		parameters.put("CustomerPlace", custDetails.get(0).getCustomerPlace());

		String taxableAmountS = "0.0";
		if (invoice.size() > 0) {
			Double amountWithDiscount = invoice.get(0).getAmount();
//			parameters.put("InvoiceAmountWithDiscount", amountWithDiscount);
			BigDecimal amountBig = new BigDecimal(amountWithDiscount);
			amountBig = amountBig.setScale(0, BigDecimal.ROUND_CEILING);

//			BigDecimal taxableAmountInBig = new BigDecimal(taxableAmount);
//			taxableAmountInBig = taxableAmountInBig.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			String amount = amountBig + "";

			if (null != taxableAmount) {
				taxableAmountS = taxableAmount + "";

			}
			String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");

			String taxamount = "0.0";
			Double taxDouble = 0.0;

			if (null != invoiceTax) {
				if (invoiceTax.size() > 0) {

					for (TaxSummaryMst tax : invoiceTax) {
						taxDouble = taxDouble + tax.getTaxAmount();
					}

				}
			}

			taxamount = taxDouble + "";

			String taxableAmountInWords = SystemSetting.AmountInWords(taxableAmountS, "Rs", "Ps");
			String taxamountinwords = SystemSetting.AmountInWords(taxamount, "Rs", "Ps");
			parameters.put("AmountInWords", amountinwords);
			parameters.put("taxableAmountInWords", taxableAmountInWords);
			parameters.put("TaxAmountInWords", taxamountinwords);
			parameters.put("taxamount", taxamount);

			parameters.put("taxableAmount", taxableAmountS);
			if (null == cessAmount) {
				cessAmount = 0.0;
			}

			parameters.put("CessAmount", cessAmount);

			String gst = "";
		}
//		for(TaxSummaryMst taxSummaryMst : invoiceTax)
//		{
//			gst = gst + taxSummaryMst.getTaxPercentage()+"\t"+taxSummaryMst.getTaxAmount()+"\n";
////		}
//		Double AmtAftrDiscount = 0.0;
//		if(invoice.size() >= 0)
//		{
//		if(null != invoice.get(0).getInvoiceDiscount())
//		{
//			 AmtAftrDiscount = invoice.get(0).getAmount()+invoice.get(0).getInvoiceDiscount();
//			
//		}else {
//			AmtAftrDiscount = invoice.get(0).getAmount();
//		}
//		} 
//		parameters.put("AmtAftrDiscount",AmtAftrDiscount);

		double cgstPercent = 0.0;
		double sgstPercent = 0.0;
		double igstPercent = 0.0;

		if (invoiceTaxSum.size() > 0) {

			cgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfcgstAmount(), 2);
			sgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfsgstAmount(), 2);
			igstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfigstAmount(), 2);
		}
		parameters.put("cgst%", cgstPercent);
		parameters.put("sgst%", sgstPercent);
		parameters.put("igst%", igstPercent);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DayEndReport(String strDate) throws JRException {

		String dataPAram = strDate;

		strDate.replaceAll("/", "");

		strDate.replaceAll("-", "");

		String pdfToGenerate = SystemSetting.reportPath + strDate + ".pdf";

		// Call url

		ResponseEntity<DailySaleSummaryReport> invoiceResponse = RestCaller
				.getDailySalesSummaryReport(SystemSetting.systemBranch, dataPAram);
		DailySaleSummaryReport billNoReport = invoiceResponse.getBody();

		ArrayList<DailySaleSummaryReport> billNoReportList = new ArrayList();

		billNoReportList.add(billNoReport);
		// =======get payment accountId
		String account = SystemSetting.systemBranch;
		System.out.println("branch----" + account);
		account = account.concat("-");
		account = account.concat("PETTY CASH");
		ResponseEntity<AccountHeads> parentAccountHead = RestCaller.getAccountHeadByName(account);
		AccountHeads accountHeads = parentAccountHead.getBody();

		System.out.println("------accountHeads--------" + accountHeads);
		ResponseEntity<List<PaymentDtl>> paymentResponse = RestCaller.getPaymentPettyCash(accountHeads.getId(),
				strDate);
//		PaymentDtl paymentDtl = paymentResponse.getBody();

		List<PaymentDtl> paymentPettyCashList = paymentResponse.getBody();

		ResponseEntity<List<ReceiptDtl>> receiptResponse = RestCaller.getReceiptPettyCash(accountHeads.getId(),
				strDate);

		List<ReceiptDtl> receiptPettyCashList = receiptResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(billNoReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(paymentPettyCashList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(receiptPettyCashList);

		String jrxmlPath = "jrxml/dailysalessummary.jrxml";

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		parameters.put("subreportdata", jrBeanCollectionDataSource2);
		parameters.put("paymentpettycash", "jrxml/PaymentPettyCash.jasper");

		parameters.put("subreportdata2", jrBeanCollectionDataSource3);
		parameters.put("receiptpettycash", "jrxml/ReceiptPettyCash.jasper");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DayEndReportNew(String strDate, String changeInCash) throws JRException {
		ObservableList<DayEndClosureDtl> dayEndList = FXCollections.observableArrayList();

		String dataPAram = strDate;

		strDate.replaceAll("/", "");

		strDate.replaceAll("-", "");

		String pdfToGenerate = SystemSetting.reportPath + strDate + ".pdf";
		// Call url

		ResponseEntity<DailySaleSummaryReport> invoiceResponse = RestCaller
				.getDailySalesSummaryReport(SystemSetting.systemBranch, dataPAram);
		DailySaleSummaryReport billNoReport = invoiceResponse.getBody();

		ResponseEntity<List<ReceiptModeReport>> getReceiptMode = RestCaller.getReceiptModeReport(strDate);
		List<ReceiptModeReport> receiptModeResponseList = getReceiptMode.getBody();

		ResponseEntity<DayEndClosureHdr> dayEndClosuredtlSaved = RestCaller.getDayEndClosureByDate(strDate);
		DayEndClosureHdr dayEndClosureHdr = dayEndClosuredtlSaved.getBody();

		ArrayList day_end_dtl = new ArrayList();
		day_end_dtl = RestCaller.getDenominationAndCount(strDate);
		Double denomination = 0.0;
		Double totalAmt = 0.0;
		Double count = 0.0;
		Iterator itr = day_end_dtl.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			denomination = (Double) element.get(2);
			count = (Double) element.get(1);

			DayEndClosureDtl dayEndClosureDtl = new DayEndClosureDtl();
			dayEndClosureDtl.setCount(count);
			dayEndClosureDtl.setDenomination(denomination);
			totalAmt = totalAmt + (count * denomination);
			dayEndList.add(dayEndClosureDtl);

		}
		if (null != dayEndClosureHdr) {
			totalAmt = totalAmt - dayEndClosureHdr.getChangeInCash();
		}

		ArrayList<DailySaleSummaryReport> billNoReportList = new ArrayList();

		billNoReportList.add(billNoReport);
		// =======get payment accountId
		String account = SystemSetting.systemBranch;
		System.out.println("branch----" + account);
		account = account.concat("-");
		account = account.concat("CASH");
//		ResponseEntity<AccountHeads> parentAccountHead = RestCaller.getAccountHeadByName(account);
//		AccountHeads accountHeads = parentAccountHead.getBody();
//
//		System.out.println("------accountHeads--------"+accountHeads);
		Double paymentResponse = RestCaller.getPaymentSumByDate(strDate, account);
//		PaymentDtl paymentDtl = paymentResponse.getBody();

//		List<PaymentDtl> paymentPettyCashList = paymentResponse.getBody();

		Double receiptResponse = RestCaller.getReceiptSumByDate(strDate, account);

		// List<ReceiptDtl> receiptPettyCashList= receiptResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(billNoReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(dayEndList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(
				receiptModeResponseList);

		// JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new
		// JRBeanCollectionDataSource(receiptPettyCashList);
		String jrxmlPath = "";

		if (SystemSetting.ONLINEDAYENDREPORT.equalsIgnoreCase("NO")) {
			jrxmlPath = "jrxml/dayendreport.jrxml";

		} else if (SystemSetting.ONLINEDAYENDREPORT.equalsIgnoreCase("YES")) {
			jrxmlPath = "jrxml/dailysalessummary.jrxml";

		}
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("subreportdata", paymentResponse);

		// parameters.put("paymentpettycash", "jrxml/PaymentPettyCash.jasper");

		parameters.put("subreportdata2", receiptResponse);

		parameters.put("subreportdata1", jrBeanCollectionDataSource2);
		parameters.put("totalAmt", totalAmt);
		parameters.put("countdata", "jrxml/CountAndDenomination.jasper");

		parameters.put("subreport3data", jrBeanCollectionDataSource3);
		parameters.put("subreport3jrxml", "jrxml/dayEndReceiptMode.jasper");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void IntentVoucherReport(String voucherNumber, String date) throws JRException {

		System.out.println("======Intent Voucher Report===========");

		String pdfToGenerate = SystemSetting.reportPath + "/intent" + voucherNumber + date + ".pdf";

		ResponseEntity<List<IntentVoucherReport>> intentResponse = RestCaller.getIntentVoucher(voucherNumber, date);
		List<IntentVoucherReport> voucher = intentResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(voucher);
		String jrxmlPath = "jrxml/IntentVoucher.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);

	}

	public static void ProductionDetailsDtlReport(String voucherNumber, String date) throws JRException {

		System.out.println("======Production Details Dtl Report===========");

		String pdfToGenerate = SystemSetting.reportPath + "/production" + voucherNumber + date + ".pdf";

		ResponseEntity<List<ProductionDtlsReport>> prodtndtls = RestCaller.getProductionDtlsDtl(voucherNumber, date,
				SystemSetting.getSystemBranch());
		List<ProductionDtlsReport> voucher = prodtndtls.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(voucher);
		String jrxmlPath = "jrxml/ProductionDetails.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);

	}

	/*
	 * public static void SalesReport(String voucherNumber, String date) throws
	 * JRException {
	 * 
	 * System.out.println("======sales reprint Report===========");
	 * 
	 * String pdfToGenerate = SystemSetting.reportPath + "/salesreprint.pdf";
	 * 
	 * ResponseEntity<List<SalesTransHdr>> salesResponse =
	 * RestCaller.getSalesdtlpdfPrint(voucherNumber,date); List<SalesTransHdr>
	 * voucher = salesResponse.getBody();
	 * 
	 * JRBeanCollectionDataSource jrBeanCollectionDataSource = new
	 * JRBeanCollectionDataSource(voucher); String jrxmlPath =
	 * "jrxml/salestranshdr.jrxml";
	 * 
	 * // Compile the Jasper report from .jrxml to .japser JasperReport jasperReport
	 * = JasperCompileManager.compileReport(jrxmlPath); HashMap parameters = new
	 * HashMap(); JasperPrint jasperPrint =
	 * JasperFillManager.fillReport(jasperReport, parameters,
	 * jrBeanCollectionDataSource);
	 * JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
	 * 
	 * HostServices hs =
	 * MapleclientApplication.mapleclientApplication.getHostServerFromMain();
	 * hs.showDocument(pdfToGenerate);
	 * 
	 * }
	 */

	public static void IntentInReport(String voucherNumber, String date) throws JRException {

		System.out.println("======intent reprint Report===========");

		String pdfToGenerate = SystemSetting.reportPath + "/intentinreprint.pdf";

		ResponseEntity<List<IntentInReport>> stockResponse = RestCaller.getIntentInpdfPrint(voucherNumber, date);
		List<IntentInReport> voucher = stockResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(voucher);
		String jrxmlPath = "jrxml/intentInDtl.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);

	}

	public static void DailySalesSummaryReport(String branchCode, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/dailysalesinv" + branchCode + ".pdf";

		ResponseEntity<DailySaleSummaryReport> DailyOpeningbillResponse = RestCaller
				.getDailySalesSummaryReport(branchCode, date);
		DailySaleSummaryReport DailyOpeningbill = DailyOpeningbillResponse.getBody();

		ResponseEntity<List<DailySaleSummaryReport>> OpeningPettyCashResponse = RestCaller
				.getOpeningPettyCashReport(branchCode, date);
		List<DailySaleSummaryReport> OpeningPettyCash = OpeningPettyCashResponse.getBody();

		ResponseEntity<List<DailySaleSummaryReport>> AdvanceReceivedOrdersResponse = RestCaller
				.getAdvanceReceivedReport(branchCode, date);
		List<DailySaleSummaryReport> AdvanceReceivedOrders = AdvanceReceivedOrdersResponse.getBody();

		ResponseEntity<List<DailySaleSummaryReport>> DailySalesDetailsResponse = RestCaller
				.getDailySalesDetails(branchCode, date);
		List<DailySaleSummaryReport> DailySalesDetails = DailySalesDetailsResponse.getBody();

		ResponseEntity<List<DailySaleSummaryReport>> CreditSalesDetailsResponse = RestCaller
				.getCreditSalesDetails(branchCode, date);
		List<DailySaleSummaryReport> CreditSalesDetails = CreditSalesDetailsResponse.getBody();

		// JRBeanCollectionDataSource jrBeanCollectionDataSource = new
		// JRBeanCollectionDataSource( DailyOpeningbill);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(OpeningPettyCash);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(AdvanceReceivedOrders);

		JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(DailySalesDetails);
		JRBeanCollectionDataSource jrBeanCollectionDataSource5 = new JRBeanCollectionDataSource(CreditSalesDetails);

		String jrxmlPath = "jrxml/dailysalessummary.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		// JasperReport jasperReport3 = JasperCompileManager.compileReport(jrxmlPath3);

		// JasperReport jasperReport2 = JasperCompileManager.compileReport(jrxmlPath2);

		HashMap parameters = new HashMap();

		parameters.put("subreportdata1", jrBeanCollectionDataSource2);
		parameters.put("opening_petty_cash_sub", "jrxml/OpeningPettyCashNew.jasper");

		parameters.put("subreportdata", jrBeanCollectionDataSource3);
		parameters.put("advance_received_order_sub", "jrxml/Advancereceived.jasper");

		parameters.put("subreportdata2", jrBeanCollectionDataSource4);
		parameters.put("cash_received_order_sub", "jrxml/cashreceivedfromorders.jasper");

		parameters.put("subreportdata3", jrBeanCollectionDataSource5);
		parameters.put("credit_sales_details_sub", "jrxml/creditsaledetails.jasper");

		/*
		 * parameters.put("subreportdata4", jrBeanCollectionDataSource6);
		 * parameters.put("sales_details_sub", "jrxml/salesdetails.jasper");
		 */

		// Fill the report

		// JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
		// parameters, jrBeanCollectionDataSource);
		// JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		// HostServices hs =
		// MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		// hs.showDocument(pdfToGenerate);

	}

	public static void ProductionDtlReport(String date, String branchCode) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/productionDtls" + branchCode + date + ".pdf";

		ResponseEntity<List<ProductionDtlsReport>> productionDtlList = RestCaller.getProductionDtlReport(date,
				branchCode);

		List<ProductionDtlsReport> productionDtlListResponse = productionDtlList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("rdate", date);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				productionDtlListResponse);

		String jrxmlPath = "jrxml/ProductionPlanning.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void serviceInvoice(ServiceInHdr serviceInHdr, String voucherNumber, String voucherDate)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/serviceVoucher" + voucherNumber + voucherDate + ".pdf";

		ResponseEntity<List<ServiceInvoice>> ServiceInvoiceList = RestCaller.getServiceInvoice(voucherNumber,
				voucherDate);

		List<ServiceInvoice> ServiceInvoiceListResponse = ServiceInvoiceList.getBody();

		ResponseEntity<ServiceInHdr> serviceInHdrResp = RestCaller.getServiceInHdrById(serviceInHdr.getId());
		ServiceInHdr serviceInHdrSaved = serviceInHdrResp.getBody();
		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("branchAddress1", branchMst.getBranchAddress1());
		parameters.put("branchAddress2", branchMst.getBranchAddress2());
		parameters.put("branchTel", branchMst.getBranchTelNo());
		parameters.put("branchState", branchMst.getBranchState());
		parameters.put("branchEmail", branchMst.getBranchEmail());
		parameters.put("GST", branchMst.getBranchGst());

		parameters.put("CustomerPhn", serviceInHdrSaved.getAccountHeads().getCustomerContact());
		parameters.put("AdvanceAmont", serviceInHdrSaved.getAdvanceAmount());
		parameters.put("EstimaedDate",
				SystemSetting.UtilDateToString(serviceInHdrSaved.getEstimatedDeliveryDate(), "yyyy-MM-dd"));
		parameters.put("EstimatedAmount", serviceInHdrSaved.getEstimatedCharge());

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				ServiceInvoiceListResponse);

		String jrxmlPath = "jrxml/serviceInvoice.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SalesReportBetweenDate(String branch, String fdate, String tdate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/SalesReport" + fdate + tdate + ".pdf";

		ResponseEntity<List<DailySalesReportDtl>> saleslList = RestCaller.getDailySaleReportAll(branch, fdate, tdate);

		List<DailySalesReportDtl> saleslListResponse = saleslList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", fdate);
		parameters.put("Todate", tdate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleslListResponse);

		String jrxmlPath = "jrxml/SalesReportBetweenDate.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ReceiptReportPrintinge(String branch, String fdate, String tdate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/ReeciptReport" + fdate + tdate + ".pdf";

		ResponseEntity<List<ReceiptReports>> receiptList = RestCaller.getReceiptReportPrinting(branch, fdate, tdate);

		List<ReceiptReports> receiptListResponse = receiptList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", fdate);
		parameters.put("Todate", tdate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(receiptListResponse);

		String jrxmlPath = "jrxml/ReceiptReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void paymentReportPrintinge(String branch, String fdate, String tdate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/PamentReport" + fdate + tdate + ".pdf";

		ResponseEntity<List<PaymentReports>> paymentList = RestCaller.paymentReportPrintinge(branch, fdate, tdate);

		List<PaymentReports> paymentListResponse = paymentList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", fdate);
		parameters.put("Todate", tdate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(paymentListResponse);

		String jrxmlPath = "jrxml/PamentReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void CustomerSalesReportBetweenDate(String branch, String fdate, String tdate, String customerid)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/SalesReport" + fdate + tdate + ".pdf";

		ResponseEntity<List<DailySalesReportDtl>> dailysales = RestCaller
				.getCustomerDailySaleReport(SystemSetting.systemBranch, fdate, tdate, customerid);
		List<DailySalesReportDtl> saleslListResponse = dailysales.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", fdate);
		parameters.put("Todate", tdate);

		ResponseEntity<AccountHeads> customerResp = RestCaller.getAccountHeadsById(customerid);
		AccountHeads customerMst = customerResp.getBody();
		parameters.put("CustomerName", customerMst.getAccountName());
		parameters.put("CustomerAddress", customerMst.getPartyAddress());
		parameters.put("CustomerPlace", customerMst.getCustomerState());
		parameters.put("CustomerPhone", customerMst.getCustomerContact());
		parameters.put("CustomerGst", customerMst.getPartyGst());

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleslListResponse);

		String jrxmlPath = "jrxml/CustomerSalesReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ActualProductionDtlReport(String fromDate, String toDate, String branchCode) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/ActualProduction" + fromDate + toDate + branchCode + ".pdf";

		ResponseEntity<List<ActualProductionReport>> ActualProductionList = RestCaller
				.getActualProductioReport(fromDate, toDate, branchCode);

		List<ActualProductionReport> ActualProductionReportResponse = ActualProductionList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("rdate", fromDate);
		parameters.put("todate", toDate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				ActualProductionReportResponse);

		String jrxmlPath = "jrxml/ActualProductionReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ProductionRawMaterialReport(String date, String branchCode) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/productionRawMaterial" + branchCode + date + ".pdf";

		ResponseEntity<List<ProductionDtlsReport>> productionDtlList = RestCaller.getProductionSummaryReport(date);

		List<ProductionDtlsReport> productionDtlListResponse = productionDtlList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("rdate", date);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				productionDtlListResponse);

		String jrxmlPath = "jrxml/ProductionPlannigRawMaterial.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ProductionRawMaterialReportBetweenDate(String fdate, String tdate, String branchCode)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/productionRawMaterialBetweenDate" + branchCode + fdate
				+ tdate + ".pdf";

		ResponseEntity<List<ProductionDtlsReport>> productionDtlList = RestCaller
				.getProductionSummaryReportBetweenDate(fdate, tdate);

		List<ProductionDtlsReport> productionDtlListResponse = productionDtlList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fdate", fdate);
		parameters.put("tdate", tdate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				productionDtlListResponse);

		String jrxmlPath = "jrxml/RawMaterialBetweenDate.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ProductionRawMaterialReportItemWise(ObservableList<ProductionDtlDtl> productionDtlDtlList,
			String itemName) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/productionRawMaterialItemwise.pdf";

		// List<ProductionDtlsReport> productionDtlListResponse =
		// productionDtlDtlList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("itemName", itemName);
//		parameters.put("fdate", fdate);
//		parameters.put("tdate", tdate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(productionDtlDtlList);

		String jrxmlPath = "jrxml/rawMaterialItemWise.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void stockTransferOutPrintPOSformat(String stockTransferHdrId) {
		HotCakesStockTransferPrint posFormatStockTransfer = new HotCakesStockTransferPrint();

		try {

			posFormatStockTransfer.setupBottomline("This is a computer generated document");
			posFormatStockTransfer.setupLogo();

			posFormatStockTransfer.PrintInvoiceThermalPrinter(stockTransferHdrId);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void StockTransferInvoiceReport(String voucherNumber, String date) throws JRException {

		// -------Surya StockTransfer

		String pdfToGenerate = SystemSetting.reportPath + "/stkinv" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<StockTransferOutReport>> stocktransferResponse = RestCaller
				.getStockTransferInvoice(voucherNumber, date);

		List<StockTransferOutReport> stockTransferInvoiceResponse = stocktransferResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		if (stockTransferInvoiceResponse.size() > 0) {
			ResponseEntity<BranchMst> transferToBranchMstResp = RestCaller
					.getBranchMstByName(stockTransferInvoiceResponse.get(0).getToBranch());

			BranchMst transferToBranchMst = transferToBranchMstResp.getBody();

			parameters.put("toBranchName", transferToBranchMst.getBranchName());
			parameters.put("toBranchAddress1", transferToBranchMst.getBranchAddress1());
			parameters.put("toBranchAddress2", transferToBranchMst.getBranchAddress2());
			parameters.put("toBranchTelNo", transferToBranchMst.getBranchTelNo());
			parameters.put("toBranchState", transferToBranchMst.getBranchState());
			parameters.put("toBranchGst", transferToBranchMst.getBranchGst());
		}

		Double totalAmount = 0.0;

		for (StockTransferOutReport stcktrnsfr : stockTransferInvoiceResponse) {
			totalAmount = totalAmount + stcktrnsfr.getAmount();
		}

		String totalAmountInWords = SystemSetting.AmountInWords(totalAmount.toString(), "Rs", "Ps");
		parameters.put("totalAmountInWords", totalAmountInWords);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				stockTransferInvoiceResponse);

		String jrxmlPath = "jrxml/stockinhc.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PurchaseInvoiceReport(String voucherNumber, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/purinv" + voucherNumber + ".pdf";
		ResponseEntity<List<PurchaseReport>> purchaseResponse = RestCaller.getPurchaseInvoice(voucherNumber, date);
		List<PurchaseReport> purchaseInvoiceResponse = purchaseResponse.getBody();
		ResponseEntity<List<PurchaseReport>> purTaxResponse = RestCaller.getPurchaseTax(voucherNumber, date);
		List<PurchaseReport> purchaseTax = purTaxResponse.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(purchaseInvoiceResponse);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(purchaseTax);
		String jrxmlPath = "jrxml/purchasedtls.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();

		parameters.put("subreportdata", jrBeanCollectionDataSource2);
		parameters.put("taxsliptup", "jrxml/taxsplits.jasper");
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);

	}

	public static void PurchaseOrderInvoiceReport(String voucherNumber, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/purOrder" + voucherNumber + ".pdf";
		ResponseEntity<List<PurchaseReport>> purchaseResponse = RestCaller.getPurchaseOrderInvoice(voucherNumber, date);
		List<PurchaseReport> purchaseInvoiceResponse = purchaseResponse.getBody();
//		ResponseEntity<List<PurchaseReport>> purTaxResponse = RestCaller.getPurchaseTax(voucherNumber, date);
//		List<PurchaseReport> purchaseTax = purTaxResponse.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(purchaseInvoiceResponse);
//		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(purchaseTax);
		String jrxmlPath = "jrxml/PurchaseOrderInvoice.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();

//		parameters.put("subreportdata", jrBeanCollectionDataSource2);
//		parameters.put("taxsliptup", "jrxml/taxsplits.jasper");
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);

	}

	/*
	 * public static void StatementofAccountReport() throws JRException {
	 * 
	 * 
	 * 
	 * String pdfToGenerate = SystemSetting.reportPath +
	 * "/statementofaccountreport.pdf";
	 * 
	 * // Call url
	 * 
	 * ResponseEntity<List<StatementOfAccountReport>> stockTransferInvoiceResponse =
	 * RestCaller.getStockTransferInvoice(); List<StatementOfAccountReport>
	 * stockTransferinvoice = stockTransferInvoiceResponse.getBody();
	 * 
	 * 
	 * JRBeanCollectionDataSource jrBeanCollectionDataSource = new
	 * JRBeanCollectionDataSource(stockTransferinvoice);
	 * 
	 * String jrxmlPath = "jrxml/summarystatementofaccounts.jrxml";
	 * 
	 * // Compile the Jasper report from .jrxml to .japser JasperReport jasperReport
	 * = JasperCompileManager.compileReport(jrxmlPath);
	 * 
	 * HashMap parameters = null;
	 * 
	 * // Fill the report JasperPrint jasperPrint =
	 * JasperFillManager.fillReport(jasperReport, parameters,
	 * jrBeanCollectionDataSource);
	 * JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
	 * 
	 * HostServices hs =
	 * MapleclientApplication.mapleclientApplication.getHostServerFromMain();
	 * 
	 * hs.showDocument(pdfToGenerate);
	 * 
	 * }
	 * 
	 */

	// ----------------sale order

	/*
	 * //voucherNumber = "000173"; //date = "2019-07-21";
	 * 
	 * System.out.println("======TaxInvoiceReport===========");
	 * 
	 * String pdfToGenerate = SystemSetting.reportPath + "/saleorderinvoice.pdf";
	 * 
	 * // Call url
	 * 
	 * ResponseEntity<List<SaleOrderInvoiceReport>> saleResponse =
	 * RestCaller.getSaleOrder(voucherNumber,date); List<SaleOrderInvoiceReport>
	 * saleOrder = saleResponse.getBody();
	 * 
	 * 
	 * 
	 * 
	 * ResponseEntity<List<SaleOrderTax>> saleTaxResponse =
	 * RestCaller.getSaleOrderTax(voucherNumber,date); List<SaleOrderTax>
	 * saleOrderTax = saleTaxResponse.getBody();
	 * 
	 * 
	 * ResponseEntity<List<SaleOrderDueReport>> saleDueResponse =
	 * RestCaller.getSaleOrderDue(voucherNumber,date); List<SaleOrderDueReport>
	 * saleOrderDue = saleDueResponse.getBody();
	 * 
	 * 
	 * JRBeanCollectionDataSource jrBeanCollectionDataSource = new
	 * JRBeanCollectionDataSource(saleOrder); JRBeanCollectionDataSource
	 * jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(saleOrderTax);
	 * JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new
	 * JRBeanCollectionDataSource(saleOrderDue);
	 * 
	 * String jrxmlPath = "jrxml/SaleOrderNew.jrxml"; //String jrxmlPath2 =
	 * "jrxml/taxsumary.jrxml";
	 * 
	 * //String jrxmlPath3 = "jrxml/taxsumarytable.jrxml";
	 * 
	 * 
	 * // Compile the Jasper report from .jrxml to .japser JasperReport jasperReport
	 * = JasperCompileManager.compileReport(jrxmlPath); //JasperReport jasperReport3
	 * = JasperCompileManager.compileReport(jrxmlPath3);
	 * 
	 * //JasperReport jasperReport2 =
	 * JasperCompileManager.compileReport(jrxmlPath2);
	 * 
	 * HashMap parameters = new HashMap();
	 * 
	 * parameters.put("subreportdatatax", jrBeanCollectionDataSource2);
	 * parameters.put("sale-order_tax_sub", "jrxml/GstSaleOrder.jasper");
	 * 
	 * parameters.put("subreportdatadue", jrBeanCollectionDataSource3);
	 * parameters.put("sale-order_due_sub", "jrxml/SaleOrderDueReport.jasper"
	 */

	/*
	 * // Fill the report JasperPrint jasperPrint =
	 * JasperFillManager.fillReport(jasperReport, parameters,
	 * jrBeanCollectionDataSource);
	 * JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
	 * 
	 * HostServices hs =
	 * MapleclientApplication.mapleclientApplication.getHostServerFromMain();
	 * 
	 * 
	 * 
	 * hs.showDocument(pdfToGenerate);
	 */

	public static void SaleOrderReport(String voucherNumber, String date) throws JRException {
		System.out.println("======SaleOrderReport===========");

		String pdfToGenerate = SystemSetting.reportPath + "/soinv" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<SaleOrderInvoiceReport>> saleResponse = RestCaller.getSaleOrder(voucherNumber, date,
				SystemSetting.getSystemBranch());
		List<SaleOrderInvoiceReport> saleOrder = saleResponse.getBody();
		ResponseEntity<List<SaleOrderTax>> saleTaxResponse = RestCaller.getSaleOrderTax(voucherNumber, date,
				SystemSetting.getSystemBranch());
		List<SaleOrderTax> saleOrderTax = saleTaxResponse.getBody();
		ResponseEntity<List<SaleOrderDueReport>> saleDueResponse = RestCaller.getSaleOrderDue(voucherNumber, date,
				SystemSetting.getSystemBranch());
		List<SaleOrderDueReport> saleOrderDue = saleDueResponse.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleOrder);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(saleOrderTax);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(saleOrderDue);
		String jrxmlPath = "jrxml/SaleOrderNew.jrxml";
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();
		parameters.put("subreportdatadue", jrBeanCollectionDataSource3);
		parameters.put("saleorderduesub", "jrxml/SaleOrderDueReport.jasper");
		parameters.put("subreportdatatax", jrBeanCollectionDataSource2);
		parameters.put("saleordertaxsub", "jrxml/GstSaleOrder.jasper");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);

	}

	public static void SaleOrderPendingDetails(String date) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/saleorderpendingdtl" + date + ".pdf";

		// Call url

		ResponseEntity<List<SaleOrderDetailsReport>> saleResponse = RestCaller.getSaleOrderPendingDtls(date);

		List<SaleOrderDetailsReport> saleList = saleResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleList);

		String jrxmlPath = "jrxml/SaleOrderDetailPendingReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SaleOrderRealizedDetails(String date) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/saleorderpendingdtl" + date + ".pdf";

		// Call url

		ResponseEntity<List<SaleOrderDetailsReport>> saleResponse = RestCaller.getSaleOrderRealizedDtls(date);

		List<SaleOrderDetailsReport> saleList = saleResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleList);

		String jrxmlPath = "jrxml/SaleOrderRealizedDetailsReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemSalesReport(String itemId, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/itemsalereport" + itemId + date + ".pdf";

		// Call url

		ResponseEntity<List<ItemSaleReport>> itemsaleResponse = RestCaller.getItemSaleReport(itemId, date);

		List<ItemSaleReport> itemsalests = itemsaleResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemsalests);

		String jrxmlPath = "jrxml/ItemviseSaleReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void CategorySaleReport(String category, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/categorysalereport" + category + date + ".pdf";

		// Call url

		ResponseEntity<List<ItemSaleReport>> itemsaleResponse = RestCaller.getCategorySaleReport(category, date);

		List<ItemSaleReport> itemsalests = itemsaleResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemsalests);

		String jrxmlPath = "jrxml/ItemviseSaleReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemStockReport(String itemId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/itemstockreport" + itemId + ".pdf";

		// Call url

		ResponseEntity<List<ItemStockReport>> itemstockResponse = RestCaller.getItemStockReport(itemId);

		List<ItemStockReport> itemstock = itemstockResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemstock);

		String jrxmlPath = "jrxml/ItemStockReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void CategoryStockReport(String categoryId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/categorystockreport" + categoryId + ".pdf";

		// Call url

		ResponseEntity<List<ItemStockReport>> catstockResponse = RestCaller.getCategoryStockReport(categoryId);

		List<ItemStockReport> catStock = catstockResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(catStock);

		String jrxmlPath = "jrxml/CategoryStockReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// ============================================dailysales===========================//

	public static void DailySalesReportSummary(String branchCode, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/dailysalesreport" + branchCode + date + ".pdf";

		// Call url

		ResponseEntity<List<DailySalesReport>> DailySalesResponse = RestCaller.getDailySalesReport1(branchCode, date);
		List<DailySalesReport> DailySales = DailySalesResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(DailySales);

		String jrxmlPath = "jrxml/Dailysales.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DailyStockReport(String date, String categoryId) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/stockreport.pdf";

		// Call urld

		ResponseEntity<List<StockReport>> DailyStockReportResponse = RestCaller
				.getDailystockReport(SystemSetting.getSystemBranch(), date, categoryId);

		List<StockReport> DailyStockReport = DailyStockReportResponse.getBody();
		for (StockReport stockReport : DailyStockReport) {
			stockReport.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());

		}

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("rdate", date);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(DailyStockReport);

		String jrxmlPath = "jrxml/stockreport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PaymentReport(String voucherNo, String date) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/payment" + voucherNo + date + ".pdf";

		// Call url

		ResponseEntity<List<PaymentVoucher>> PaymentVoucherReportResponse = RestCaller
				.getPaymentVoucherReport(SystemSetting.getSystemBranch(), voucherNo, date);

		List<PaymentVoucher> PaymentVoucherReport = PaymentVoucherReportResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(PaymentVoucherReport);

		String jrxmlPath = "jrxml/newPaymentInvoice.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemWiseSalesReport(String startdate, String enddate, String branchcode, String itemnameid)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/itemwisesales" + itemnameid + branchcode + startdate
				+ ".pdf";

		// Call url

		ResponseEntity<List<ItemWiseDtlReport>> itemWiseSalesResponse = RestCaller.getItembytwoDates(branchcode,
				startdate, enddate, itemnameid);
		List<ItemWiseDtlReport> itemWiseDtlReport = itemWiseSalesResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemWiseDtlReport);

		String jrxmlPath = "jrxml/ItemWiseOrCategoryWiseReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startdate);
		parameters.put("End Date", enddate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void CategoryWiseSalesReport(String startdate, String enddate, String branchcode, String catid)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/categorysales" + catid + startdate + ".pdf";

		// Call url

		ResponseEntity<List<ItemWiseDtlReport>> itemWiseSalesResponse = RestCaller.getCategorybytwoDates(branchcode,
				startdate, enddate, catid);
		List<ItemWiseDtlReport> itemWiseDtlReport = itemWiseSalesResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemWiseDtlReport);

		String jrxmlPath = "jrxml/ItemWiseOrCategoryWiseReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startdate);
		parameters.put("End Date", enddate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", "Ph." + branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void CategoryWiseSalesDtlReport(String startdate, String enddate, String branchcode)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/categorysalesdtls" + startdate + ".pdf";

		// Call url

		ResponseEntity<List<CategoryWiseSalesReport>> categoryWiseSalesResponse = RestCaller
				.getCategorySalesDtl(branchcode, startdate, enddate);
		List<CategoryWiseSalesReport> categoryWiseDtlReport = categoryWiseSalesResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(categoryWiseDtlReport);

		String jrxmlPath = "jrxml/CategoryWiseSalesReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("StartDate", startdate);
		parameters.put("EndDate", enddate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DateBetweenSalesReport(String startdate, String enddate, String branchcode) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/categorysales" + branchcode + startdate + ".pdf";

		// Call url

		ResponseEntity<List<ItemWiseDtlReport>> itemWiseSalesResponse = RestCaller.getMonthSales(branchcode, startdate,
				enddate);
		List<ItemWiseDtlReport> itemWiseDtlReport = itemWiseSalesResponse.getBody();
		for (ItemWiseDtlReport itemWiseDtl : itemWiseDtlReport) {
			itemWiseDtl.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
			itemWiseDtl.setBranchName(SystemSetting.getUser().getBranchName());
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemWiseDtlReport);

		String jrxmlPath = "jrxml/ItemWiseOrCategoryWiseReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startdate);
		parameters.put("End Date", enddate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	/*
	 * public static void ReceiptInvoice(String voucherNumber, String date) throws
	 * JRException {
	 * 
	 * String pdfToGenerate = SystemSetting.reportPath + "/recvoucher" +
	 * voucherNumber + ".pdf";
	 * 
	 * // Call url
	 * 
	 * ResponseEntity<List<ReceiptInvoice>> receiptInvoiceResponse =
	 * RestCaller.getreceipts(voucherNumber, date);
	 * 
	 * List<ReceiptInvoice> receiptInvoice = receiptInvoiceResponse.getBody();
	 * 
	 * for (ReceiptInvoice Receiptinvoice : receiptInvoice) {
	 * Receiptinvoice.setCompanyName(SystemSetting.getUser().getCompanyMst().
	 * getCompanyName()); }
	 * 
	 * JRBeanCollectionDataSource jrBeanCollectionDataSource = new
	 * JRBeanCollectionDataSource(receiptInvoice);
	 * 
	 * String jrxmlPath = "jrxml/ReceiptVoucher.jrxml";
	 * 
	 * // Compile the Jasper report from .jrxml to .japser JasperReport jasperReport
	 * = JasperCompileManager.compileReport(jrxmlPath);
	 * 
	 * HashMap parameters = null;
	 * 
	 * // Fill the report JasperPrint jasperPrint =
	 * JasperFillManager.fillReport(jasperReport, parameters,
	 * jrBeanCollectionDataSource);
	 * JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
	 * 
	 * HostServices hs =
	 * MapleclientApplication.mapleclientApplication.getHostServerFromMain();
	 * 
	 * hs.showDocument(pdfToGenerate);
	 * 
	 * }
	 */

	public static void PDCReceiptInvoice(ObservableList<PDCReceipts> receiptList) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/recvoucher" + receiptList.get(0).getAccountName() + ".pdf";

		// Call url

//		ResponseEntity<List<ReceiptInvoice>> receiptInvoiceResponse = RestCaller.getreceipts(voucherNumber, date);

//		List<ReceiptInvoice> receiptInvoice = receiptInvoiceResponse.getBody();
//
//		for (ReceiptInvoice Receiptinvoice : receiptInvoice) {
//			Receiptinvoice.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
//		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(receiptList);

		String jrxmlPath = "jrxml/PDCReceiptInvoice.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PDCPaymentInvoice(ObservableList<PDCPayment> paymentList) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/recvoucher" + paymentList.get(0).getAccountName() + ".pdf";

		// Call url

//		ResponseEntity<List<ReceiptInvoice>> receiptInvoiceResponse = RestCaller.getreceipts(voucherNumber, date);

//		List<ReceiptInvoice> receiptInvoice = receiptInvoiceResponse.getBody();
//
//		for (ReceiptInvoice Receiptinvoice : receiptInvoice) {
//			Receiptinvoice.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
//		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(paymentList);

		String jrxmlPath = "jrxml/PDCPayment.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void RawMaterialReturn(String voucherNumber, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/rawMaterialReturn" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<RawMaterialReturnReport>> rawMaterialRetunrResponse = RestCaller
				.getRawMaterialReturn(voucherNumber, date);

		List<RawMaterialReturnReport> rawMatList = rawMaterialRetunrResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(rawMatList);

		String jrxmlPath = "jrxml/RawMaterialIssue.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("heading", "Raw Material Return");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void RawMaterialIssue(String voucherNumber, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/rawMaterialIssue" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<RawMaterialIssueReport>> rawMaterialIssueResponse = RestCaller.getRawMaterial(voucherNumber,
				date);

		List<RawMaterialIssueReport> rawMatList = rawMaterialIssueResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(rawMatList);

		String jrxmlPath = "jrxml/RawMaterialIssue.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("heading", "Raw Material Issue");
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void CustomerBillWiseBalanceReport(String fdate, String tdate, String custId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/custBalance" + fdate + ".pdf";

		// Call url

		ResponseEntity<List<CustomerBalanceReport>> CustomerBalanceReportResponse = RestCaller
				.getCustomerBillBalance(fdate, tdate, custId);

		List<CustomerBalanceReport> CustomerBalanceReportList = CustomerBalanceReportResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				CustomerBalanceReportList);

		String jrxmlPath = "jrxml/CustomerBalanceReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phone", branchMst.getBranchTelNo());
		parameters.put("fdate", fdate);
		parameters.put("tdate", tdate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void AllCustomerBillWiseBalanceReport(String fdate, String tdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/custBalance" + fdate + ".pdf";

		// Call url

		ResponseEntity<List<AccountBalanceReport>> CustomerBalanceReportResponse = RestCaller
				.getAllCustomerBillBalanceBetweenDate(fdate, tdate);

		List<AccountBalanceReport> CustomerBalanceReportList = CustomerBalanceReportResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				CustomerBalanceReportList);

		String jrxmlPath = "jrxml/AllCustomerBalanceAsofnow.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phone", branchMst.getBranchTelNo());
		parameters.put("fdate", fdate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void RawMaterialIssueBetweenDate(String fdate, String tdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/rawMaterialIssue" + fdate + ".pdf";

		// Call url

		ResponseEntity<List<RawMaterialIssueReport>> rawMaterialIssueResponse = RestCaller
				.getRawMaterialBetweenDate(fdate, tdate);

		List<RawMaterialIssueReport> rawMatList = rawMaterialIssueResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(rawMatList);

		String jrxmlPath = "jrxml/RawMaterialReportBetweenDate.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("heading", "Raw Material Issue");
		parameters.put("fdate", fdate);
		parameters.put("tdate", tdate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ReceiptReportByCustomerBetweenDate(String custId, String fdate, String tdate)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/ReceiptReport" + custId + fdate + ".pdf";

		// Call url

		ResponseEntity<List<ReceiptInvoice>> ReceiptInvoiceResponse = RestCaller
				.getReceiptHdrByCustomerBetweenDate(custId, fdate, tdate);

		List<ReceiptInvoice> ReceiptInvoiceList = ReceiptInvoiceResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ReceiptInvoiceList);

		String jrxmlPath = "jrxml/receiptBetweenDateReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phone", branchMst.getBranchTelNo());
		parameters.put("heading", "Raw Material Issue");
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fDate", fdate);
		parameters.put("tDate", tdate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ReceiptReportBetweenDate(String fdate, String tdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/ReceiptReport" + fdate + ".pdf";

		// Call url

		ResponseEntity<List<ReceiptInvoice>> ReceiptInvoiceResponse = RestCaller.getReceiptHdrBetweenDate(fdate, tdate);

		List<ReceiptInvoice> ReceiptInvoiceList = ReceiptInvoiceResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ReceiptInvoiceList);

		String jrxmlPath = "jrxml/receiptBetweenDateReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("heading", "Raw Material Issue");
		parameters.put("fDate", fdate);
		parameters.put("tDate", tdate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PaymentReportByAccountBetweenDate(String accountId, String strtDate, String endDate)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/PaymentReport" + accountId + strtDate + ".pdf";

		// Call url

		ResponseEntity<List<PaymentVoucher>> PaymentInvoiceResponse = RestCaller
				.getPaymentHdrBetweenDateByAccount(accountId, strtDate, endDate);

		List<PaymentVoucher> PaymentInvoiceList = PaymentInvoiceResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(PaymentInvoiceList);

		String jrxmlPath = "jrxml/paymentInvoiceReportBetweenDate.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchGst());
		parameters.put("fDate", strtDate);
		parameters.put("tDate", endDate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PaymentReportByBetweenDate(String strtDate, String endDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/PaymentReport" + strtDate + ".pdf";

		// Call url

		ResponseEntity<List<PaymentVoucher>> PaymentInvoiceResponse = RestCaller.getPaymentHdrBetweenDate(strtDate,
				endDate);

		List<PaymentVoucher> PaymentInvoiceList = PaymentInvoiceResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(PaymentInvoiceList);

		String jrxmlPath = "jrxml/paymentInvoiceReportBetweenDate.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchGst());
		parameters.put("fDate", strtDate);
		parameters.put("tDate", endDate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void OrgReceiptInvoice(String voucherNumber, String date, String memberId) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/recvoucher" + voucherNumber + ".pdf";
		ResponseEntity<List<OrgReceiptInvoice>> receiptInvoiceResponse = RestCaller.getOrgreceipts(voucherNumber, date,
				memberId);
		List<OrgReceiptInvoice> receiptInvoice = receiptInvoiceResponse.getBody();
		for (OrgReceiptInvoice Receiptinvoice : receiptInvoice) {
			Receiptinvoice.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
		}
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(receiptInvoice);
		String jrxmlPath = "jrxml/OrgReceipt.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBankName());
		parameters.put("BranchAddress1", branchMst.getBranchAddress1());
		parameters.put("BranchAddress2", branchMst.getBranchAddress2());
		parameters.put("BranchEmail", branchMst.getBranchEmail());
		parameters.put("BranchGst", branchMst.getBranchGst());
		parameters.put("BranchPhn", branchMst.getBranchTelNo());
		parameters.put("BranchState", branchMst.getBranchState());

		// OrgReceiptInvoice
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);
	}

	public static void OrgReceiptInvoiceReprint(String voucherNumber, String date, String memberId) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/recvoucher" + voucherNumber + ".pdf";
		ResponseEntity<List<OrgReceiptInvoice>> receiptInvoiceResponse = RestCaller.getOrgreceiptsReprint(voucherNumber,
				date, memberId);
		List<OrgReceiptInvoice> receiptInvoice = receiptInvoiceResponse.getBody();
		for (OrgReceiptInvoice Receiptinvoice : receiptInvoice) {
			Receiptinvoice.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());
		}
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(receiptInvoice);
		String jrxmlPath = "jrxml/OrgReceipt.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBankName());
		parameters.put("BranchAddress1", branchMst.getBranchAddress1());
		parameters.put("BranchAddress2", branchMst.getBranchAddress2());
		parameters.put("BranchEmail", branchMst.getBranchEmail());
		parameters.put("BranchGst", branchMst.getBranchGst());
		parameters.put("BranchPhn", branchMst.getBranchTelNo());
		parameters.put("BranchState", branchMst.getBranchState());

		// OrgReceiptInvoice
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);
	}

	public static void MonthlySummaryReport(String startDate, String endDate, String supplierId) throws JRException {

		// URL NOT RECEIVED
		String pdfToGenerate = SystemSetting.reportPath + "/suppliermonthlysummary" + supplierId + startDate + ".pdf";

		// Call url

		ResponseEntity<List<SupplierMonthlySummary>> supplierMonthlySummaryResponse = RestCaller
				.getSupplierMonthlySummary(startDate, endDate, supplierId);

		List<SupplierMonthlySummary> monthlySummaryInvoice = supplierMonthlySummaryResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(monthlySummaryInvoice);

		String jrxmlPath = "jrxml/supplierMonthlySummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBankName());
		parameters.put("BranchAddress1", branchMst.getBranchAddress1());
		parameters.put("BranchAddress2", branchMst.getBranchAddress2());
		parameters.put("BranchEmail", branchMst.getBranchEmail());
		parameters.put("BranchGst", branchMst.getBranchGst());
		parameters.put("BranchPhn", branchMst.getBranchTelNo());
		parameters.put("BranchState", branchMst.getBranchState());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SaleOrederReport(String startDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/saleorderreport" + startDate + ".pdf";

		// Call url

		ResponseEntity<List<SaleOrderReport>> saleOrderReport = RestCaller.getSaleOrderReport(startDate);

		List<SaleOrderReport> saleOrderReportList = saleOrderReport.getBody();
		for (SaleOrderReport saleoreder : saleOrderReportList) {
			String receiptMode = "";
			ResponseEntity<SalesOrderTransHdr> getSalesOrder = RestCaller
					.getSalesOrderTransHdrByVoucherNo(saleoreder.getVoucherNo());

			ResponseEntity<List<SaleOrderReceipt>> getSalereceiptList = RestCaller
					.getAllSaleOrderReceiptsBySalesTranOrdersHdr(getSalesOrder.getBody().getId());

			for (SaleOrderReceipt saleOrderRec : getSalereceiptList.getBody()) {
				receiptMode = receiptMode.concat(saleOrderRec.getReceiptMode());
				receiptMode = receiptMode.concat(",");
			}

			saleoreder.setPaymentMode(receiptMode);
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleOrderReportList);

		String jrxmlPath = "jrxml/CategoryWiseSaleOrderReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("rdate", startDate);
//		parameters.put("receiptMode", receiptMode);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SaleOrderAdvanceAndBalance(String startDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/saleorderadvanceandbalance" + startDate + ".pdf";
		// Call url
		ResponseEntity<List<SalesOrderAdvanceBalanceReport>> saleOrderReport = RestCaller
				.getSaleOrderAdvanceBalance(startDate);

		List<SalesOrderAdvanceBalanceReport> saleOrderReportList = saleOrderReport.getBody();

		for (SalesOrderAdvanceBalanceReport salesOrderAdvanceBalanceReport : saleOrderReportList) {
			String receiptMode = "";
			salesOrderAdvanceBalanceReport.setRdate(startDate);
			ResponseEntity<SalesOrderTransHdr> getSalesOrder = RestCaller
					.getSalesOrderTransHdrByVoucherNo(salesOrderAdvanceBalanceReport.getVoucherNumber());
			ResponseEntity<List<SaleOrderReceipt>> getSalereceiptList = RestCaller
					.getAllSaleOrderReceiptsBySalesTranOrdersHdr(getSalesOrder.getBody().getId());
			for (SaleOrderReceipt saleOrderRec : getSalereceiptList.getBody()) {
				receiptMode = receiptMode.concat(saleOrderRec.getReceiptMode());
				receiptMode = receiptMode.concat(",");
			}
			salesOrderAdvanceBalanceReport.setReceiptMode(receiptMode);
		}
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleOrderReportList);

		String jrxmlPath = "jrxml/saleadvancebal.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
//		parameters.put("receiptMode",receiptMode);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SalesModeWiseSummary(String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/salemodewisesummary" + date + ".pdf";
		ResponseEntity<List<SalesModeWiseSummaryreport>> SaleWiseResponse = RestCaller.getSaleModeWise(date);
		List<SalesModeWiseSummaryreport> SalesWise = SaleWiseResponse.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(SalesWise);
		String jrxmlPath = "jrxml/SaleModeWiseSummaryy.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		System.out.println("company name......." + SystemSetting.getUser().getCompanyMst().getCompanyName());
//	parameters.put("Address1", branchMst.getBranchAddress1());
//	parameters.put("Address2", branchMst.getBranchAddress2());
//	parameters.put("Phone", branchMst.getBranchTelNo());
//	parameters.put("State", branchMst.getBranchState());
//	parameters.put("GST", branchMst.getBranchGst());
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void B2BSalesSummary(String date, String stodate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/b2bsalessummary" + date + ".pdf";
		ResponseEntity<List<B2bSalesReport>> b2bSalesReport = RestCaller.getB2bSaleSummary(date, stodate);

		List<B2bSalesReport> saleb2bReportList = b2bSalesReport.getBody();
		for (B2bSalesReport bb2bSalesReport : saleb2bReportList) {

			bb2bSalesReport.setDate(date);
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleb2bReportList);

		String jrxmlPath = "jrxml/B2Bsales.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	private static Connection jrBeanCollectionDataSource(List<SalesModeWiseSummaryreport> saleModeWise) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void SaleOrderHomedeliveryReport(String startDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/homedelivery" + startDate + ".pdf";

		// Call url

		ResponseEntity<List<SaleOrderReport>> homedeliveryReport = RestCaller.getHomedeliveryReport(startDate);

		List<SaleOrderReport> homedeliveryReportList = homedeliveryReport.getBody();

		for (SaleOrderReport saleOrderReport : homedeliveryReportList) {
			saleOrderReport.setStartDate(SystemSetting.StringToUtilDate(startDate, "yyyy-MM-dd"));
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(homedeliveryReportList);

		String jrxmlPath = "jrxml/homeDelivery.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DeliveryBoyReport(String startDate, String deliveryBoyId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/deliveryboyreport" + deliveryBoyId + startDate + ".pdf";

		// Call url

		ResponseEntity<List<SaleOrderReport>> deliveryBoyReport = RestCaller.getDeliveryBoyReport(startDate,
				deliveryBoyId);

		List<SaleOrderReport> deliveryBoyReportList = deliveryBoyReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(deliveryBoyReportList);

		String jrxmlPath = "jrxml/DeliveryMan.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SaleOrderTakeAwayReport(String startDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/saleordertakeaway" + startDate + ".pdf";
		// Call url

		ResponseEntity<List<SaleOrderReport>> deliveryBoyReport = RestCaller.getSaleOrderTakeAwayReport(startDate);

		List<SaleOrderReport> deliveryBoyReportList = deliveryBoyReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(deliveryBoyReportList);

		String jrxmlPath = "jrxml/SaleOrderTakeAwayReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", "Ph." + branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST: " + branchMst.getBranchGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void AccountBalanceReport(String startDate, String endDate, String accountId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/accountbalance" + accountId + startDate + ".pdf";

		// Call url

		ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller.getAccountBalanceReport(startDate,
				endDate, accountId);

		List<AccountBalanceReport> accountBalanceReportList = accountBalanceReport.getBody();

		ResponseEntity<List<PDCPayment>> pdcPaymentReportRest = RestCaller
				.getPDCPaymentReportByAccountIdAndStatus(accountId, "PENDING");

		List<PDCPayment> pdcPaymentReport = pdcPaymentReportRest.getBody();

		ResponseEntity<List<PDCReceipts>> pdcReceiptsReportRest = RestCaller
				.getPDCReceiptsReportRestReportByAccountIdAndStatus(accountId, "PENDING");

		List<PDCReceipts> pdcReceiptsReport = pdcReceiptsReportRest.getBody();

		for (AccountBalanceReport accountBalance : accountBalanceReportList) {
			Date date = SystemSetting.StringToUtilDate(startDate, "yyyy-MM-dd");
			accountBalance.setStartDate(date);

			Date edate = SystemSetting.StringToUtilDate(endDate, "yyyy-MM-dd");
			accountBalance.setEndDate(edate);
		}
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				accountBalanceReportList);

		JRBeanCollectionDataSource jrBeanCollectionDataSource1 = new JRBeanCollectionDataSource(pdcPaymentReport);

		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(pdcReceiptsReport);

		ResponseEntity<AccountHeads> getAccountHeadsById = RestCaller.getAccountHeadsById(accountId);
//		ResponseEntity<Supplier> supplierById = RestCaller.getSupplier(accountId);
		String jrxmlPath = null;
		if (null != getAccountHeadsById.getBody()) {
			jrxmlPath = "jrxml/AccountBalanceCustomer.jrxml";
		} else {
			jrxmlPath = "jrxml/AccountBalance.jrxml";
		}
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", "Ph." + branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST: " + branchMst.getBranchGst());

		parameters.put("pdcpaymentData", jrBeanCollectionDataSource1);
		parameters.put("pdcpayment", "jrxml/PDCPaymentReport.jasper");

		parameters.put("pdcreceiptData", jrBeanCollectionDataSource2);
		parameters.put("pdcreceipt", "jrxml/PDCRecieptReport.jasper");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SupplierLedgerReport(String startDate, String endDate, String accountId, String supplierName)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "supplierledger" + accountId + ".pdf";

		System.out.println(pdfToGenerate);
		// Call url

		ResponseEntity<List<AccountBalanceReport>> supplierLedgerReport = RestCaller.getSupplierLedgerReport(startDate,
				endDate, accountId);

		List<AccountBalanceReport> supplierLedgerReportList = supplierLedgerReport.getBody();
		for (AccountBalanceReport accountBalanceReport : supplierLedgerReportList) {
			accountBalanceReport.setCustomerAccount(accountId);
		}

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);
		parameters.put("supplierName", supplierName);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		if (null != branchMst) {
			parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("BranchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", branchMst.getBranchGst());

		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				supplierLedgerReportList);

		String jrxmlPath = "jrxml/SupplierLedgerReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void SupplierBillwiseDueDayReport(String startDate, String endDate, String accountId)
			throws JRException {

		System.out.println("--------------SupplierBillwiseDueDayReport----------------------");

		String pdfToGenerate = SystemSetting.reportPath + "supplierbillwiseduedayreport" + accountId + ".pdf";

		System.out.println(pdfToGenerate);
		// Call url

		ResponseEntity<List<SupplierBillwiseDueDayReport>> supplierLedgerReport = RestCaller
				.getSupplierBillwiseDueDayReport(startDate, endDate, accountId);

		List<SupplierBillwiseDueDayReport> supplierLedgerReportList = supplierLedgerReport.getBody();

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		if (null != branchMst) {
			parameters.put("CompanyNameParam", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("BranchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", branchMst.getBranchGst());

		}
		parameters.put("today", SystemSetting.systemDate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				supplierLedgerReportList);

		String jrxmlPath = "jrxml/supplierbillwisebyduedays.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void CustomerLedgerReport(String startDate, String endDate, String customerId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/customerledger" + customerId + startDate + ".pdf";

		// Call url

		ResponseEntity<List<AccountBalanceReport>> customerLedgerReport = RestCaller.getCustomerLedger(startDate,
				endDate, customerId);

		List<AccountBalanceReport> customerLedgerReportList = customerLedgerReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				customerLedgerReportList);

		String jrxmlPath = "jrxml/CustLedger.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void SupplierBillWiseReport(String startDate, String endDate, String supplierId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/supplierbillwaysreport" + supplierId + startDate + ".pdf";

		// Call url

		ResponseEntity<List<SupplierMonthlySummary>> supplierBillWiseReport = RestCaller.getSupplierBillWise(startDate,
				endDate, supplierId);

		List<SupplierMonthlySummary> supplierBillWiseReportList = supplierBillWiseReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				supplierBillWiseReportList);

		String jrxmlPath = "jrxml/supplierbillwisebyduedays.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);

		// Fill the report

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void CustomerBillWiseReport(String startDate, String endDate, String customerId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/customerbillwaysreport" + customerId + startDate + ".pdf";

		// Call url

		ResponseEntity<List<AccountBalanceReport>> supplierBillWiseReport = RestCaller
				.getCustomerBillWiseReport(startDate, endDate, customerId);

		List<AccountBalanceReport> supplierBillWiseReportList = supplierBillWiseReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				supplierBillWiseReportList);

		String jrxmlPath = "jrxml/CustommerBillWays.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void DamageEntriesReport(String startDate, String endDate, String branchCode) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/damageentryreport" + startDate + ".pdf";
// Call url
		ResponseEntity<List<DamageEntryReport>> damageEntryReport = RestCaller.getDamageEntryReport(startDate, endDate,
				branchCode);

		List<DamageEntryReport> damageEntryReportReportList = damageEntryReport.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				damageEntryReportReportList);

		String jrxmlPath = "jrxml/DamageEntry.jrxml";
		// String jrxmlPath ="jrxml/WriteOffSummaryReport.jrxml";

// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		// parameters.put("branch Code", branchCode);
		// Fill the report

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void StockSummaryDtslReport(String fromDate, String toDate, String itemId, String branchCode)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stockDtls" + itemId + toDate + ".pdf";

		// Call url

		ResponseEntity<List<StockSummaryDtlReport>> stockSummaryDtlReport = RestCaller
				.getStockSummaryDtlReport(fromDate, toDate, itemId, branchCode);

		List<StockSummaryDtlReport> stockSummaryDtlReportList = stockSummaryDtlReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				stockSummaryDtlReportList);

		String jrxmlPath = "jrxml/StockSummaryDtlsReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();
		parameters.put("Start Date", fromDate);
		parameters.put("End Date", toDate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void InventoryReorderstatusReport(String startDate, String endDate, String branchCode)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/inventoryreorderstatus" + branchCode + startDate + ".pdf";

		ResponseEntity<List<InventoryReorderStatusReport>> inventoryReorderReport = RestCaller
				.getInventoryReorderStatusReport(startDate, endDate, branchCode);

		List<InventoryReorderStatusReport> inventoryReorderReportList = inventoryReorderReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				inventoryReorderReportList);

		String jrxmlPath = "jrxml/InventoryReorder.jrxml";
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DailySalesReport() throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/dailysalesreport.pdf";

		// Call url
		String sysDate = SystemSetting.UtilDateToString(SystemSetting.systemDate, "yyyy-MM-dd");
		ResponseEntity<List<ItemWiseDtlReport>> itemWiseDtlReport = RestCaller.getDailySalesReport(sysDate);

		List<ItemWiseDtlReport> itemWiseDtlReportList = itemWiseDtlReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(itemWiseDtlReportList);

		String jrxmlPath = "jrxml/DailySalesReport.jrxml";
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = null;
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DayBookReport(String branchCode, String rdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/DayBookReport" + branchCode + rdate + ".pdf";

		// Call url

		ResponseEntity<List<DayBook>> dayBookReport = RestCaller.getDayBookReport(branchCode, rdate);

		List<DayBook> dayBookReportList = dayBookReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(dayBookReportList);

		String jrxmlPath = "jrxml/DayBookReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());
		parameters.put("RDate", sdate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void BarcodePrinting(String barcode) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/barcode" + barcode + ".pdf";

		// Call url

		// JRBeanCollectionDataSource jrBeanCollectionDataSource = new
		// JRBeanCollectionDataSource(dayBookReportList);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = null;

		String jrxmlPath = "jrxml/barcode.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());
		parameters.put("RDate", sdate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SaleOrderPendingReport(String branchCode, String rdate, String tdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/SaleOrderPending" + branchCode + rdate + ".pdf";

		// Call url

		ResponseEntity<List<SaleOrderDetailsReport>> saleOrderReport = RestCaller.getSaleOrderPenidngReport(branchCode,
				rdate, tdate);

		List<SaleOrderDetailsReport> SaleOrderDetailsReportList = saleOrderReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				SaleOrderDetailsReportList);

		String jrxmlPath = "jrxml/SaleOrderPendingReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("rdate", rdate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void CardReconcileReport(String fdate, String tdate, String mode) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/CardReconcileReport" + fdate + mode + ".pdf";

		// Call url

		ResponseEntity<List<CardReconcileReport>> CardReconcile = RestCaller.getCardReconcileReport(fdate, tdate, mode);

		List<CardReconcileReport> CardReconcileList = CardReconcile.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(CardReconcileList);

		String jrxmlPath = "jrxml/CardReconcileReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());
		parameters.put("RDate", sdate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("State", branchMst.getBranchState());
		parameters.put("fdate", fdate);
		parameters.put("tdate", tdate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void HsnCodeSalesReport(String fdate, String tdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/HsnCodeSaleReport" + fdate + ".pdf";

		// Call url

		ResponseEntity<List<HsnCodeSaleReport>> gstHsnCodeSaleReport = RestCaller.getHsnCodeSaleReport(fdate, tdate);

		List<HsnCodeSaleReport> HsnCodeSaleReportList = gstHsnCodeSaleReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(HsnCodeSaleReportList);

		String jrxmlPath = "jrxml/HSNCodeSaleReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("StartDate", fdate);
		parameters.put("EndDate", tdate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void HsnCodePurchaseReport(String fdate, String tdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/HsnCodeSaleReport" + fdate + ".pdf";

		// Call url

		ResponseEntity<List<HsnCodeSaleReport>> gstHsnCodeSaleReport = RestCaller.getHsnCodePuchaseReport(fdate, tdate);

		List<HsnCodeSaleReport> HsnCodeSaleReportList = gstHsnCodeSaleReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(HsnCodeSaleReportList);

		String jrxmlPath = "jrxml/HsnCodePurchaseReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("StartDate", fdate);
		parameters.put("EndDate", tdate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ProductConversionReport(String branchCode, String rdate, String tdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/DayBookReport" + branchCode + rdate + ".pdf";

		// Call url

		ResponseEntity<List<DayBook>> dayBookReport = RestCaller.getDayBookReport(branchCode, rdate);

		List<DayBook> dayBookReportList = dayBookReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(dayBookReportList);

		String jrxmlPath = "jrxml/DayBookReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());
		parameters.put("RDate", sdate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ShortExpiryReport(String branchCode, String rdate, String currentDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/ShortExpiry" + branchCode + ".pdf";

		// Call url
		ResponseEntity<List<ShortExpiryReport>> ShortExpiryReport = RestCaller.getShortExpiryReport(currentDate);

		List<ShortExpiryReport> ShortExpiryReportList = ShortExpiryReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ShortExpiryReportList);

		String jrxmlPath = "jrxml/ShortExpiryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());
		parameters.put("rDate", rdate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		parameters.put("GST", branchMst.getBranchGst());
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// version1.10
	public static void getDayend(String branchCode, String rdate) throws JRException {

		// -----------version 4.6

		// DayEndReportStore dayEndReportStore = new DayEndReportStore();

		// -----------version 4.6 end

		Double dtodayscashSale = 0.0, dPreviousCashSaleOrder = 0.0, dTodaysCashSaleOrder = 0.0;

		Double yesBank = 0.0;

		Double sbiBank = 0.0;

		Double ebs = 0.0;
		Double cashr = 0.0;
		Double cash = 0.0;
		Double creditr = 0.0;
		Double gpayr = 0.0;
		Double syggyr = 0.0;
		Double zomator = 0.0;
		Double paytmr = 0.0;

		String pdfToGenerate = SystemSetting.reportPath + "/dayend" + branchCode + ".pdf";

		// Call url
		ResponseEntity<List<SalesModeWiseBillReport>> billSeries = RestCaller.BillSeriesDayEndByInvoiceSeries(rdate);
		List<SalesModeWiseBillReport> salesMOdeList = billSeries.getBody();

		ResponseEntity<VoucherNumberDtlReport> voucherNumberDtlReport = RestCaller
				.getInvoiceNumberStatistics(branchCode, rdate);

		ResponseEntity<List<ReceiptModeReport>> salesReceiptsRespList = RestCaller
				.getReceiptModeReportBetweenDate(SystemSetting.getUser().getBranchCode(), rdate, rdate);

		List<ReceiptModeReport> salesReceiptsList = salesReceiptsRespList.getBody();

		// ---------------new version 1.1 surya
		ResponseEntity<List<ReceiptModeReport>> totalCardAmount = RestCaller.getTotalCardAmountByDate(rdate);
		List<ReceiptModeReport> totalCardAmountList = totalCardAmount.getBody();

		// ---------------new version 1.1 surya end

		// ---------------new version 1.0 -22-2-21 surya

		ResponseEntity<List<DayEndProcessing>> dayendReceiptMode = RestCaller.getDayEndReceiptMode(rdate);
		List<DayEndProcessing> dayendReceiptList = dayendReceiptMode.getBody();
		// ---------------new version 1.0 -22-2-21 surya end

		// rest doubl
		Double creditConvertTOCashforToday = RestCaller.getCreditSaleAdjstmnt(branchCode, rdate);

		BranchMst branchMst = RestCaller.getBranchDtls(branchCode);

		List<BranchMst> branchList = new ArrayList<BranchMst>();

		if (null != branchMst) {
			branchList.add(branchMst);
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(salesReceiptsList);
		// ---------------new version 1.1 surya end

		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(totalCardAmountList);
		// ---------------new version 1.1 surya

		// ---------------new version 1.0 -22-2-21 surya
		JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(dayendReceiptList);

		// ---------------new version 1.0 -22-2-21 surya end

//		String jrxmlPath = "jrxml/dayendreport_amb.jrxml";// add new filename

		String jrxmlPath = "jrxml/DayEndReportWithReceiptMode.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("branchName", branchMst.getBranchName());

		// -----------------version 1.11
		long BSPOSMAXNO = 0L;
		long BSPOSMINNO = 0L;
		long BSPOSCOUNT = 0L;

		long BSWSMAXNO = 0L;
		long BSWSMINNO = 0L;
		long BSWSCOUNT = 0L;
		parameters.put("swiggyr", 0.0 + "");
		parameters.put("zomator", 0.0 + "");
		parameters.put("CashVariance", 0.0 + "");
		parameters.put("GPAYr", 0.0 + "");
		parameters.put("paytmr", 0.0 + "");
		parameters.put("ebsr", 0.0 + "");
		parameters.put("cardsales", 0.0 + "");
		parameters.put("creditr", 0.0 + "");
		parameters.put("neftsale", 0.0 + "");

		parameters.put("BSPOSMAXNO", voucherNumberDtlReport.getBody().getPosEndingBillNumber() + "");
		parameters.put("BSPOSMINNO", voucherNumberDtlReport.getBody().getPosStartingBillNumber() + "");
		parameters.put("BSPOSCOUNT", voucherNumberDtlReport.getBody().getPosBillCount() + "");

		parameters.put("BSWSMAXNO", voucherNumberDtlReport.getBody().getWholeSaleEndingBillNumber() + "");
		parameters.put("BSWSMINNO", voucherNumberDtlReport.getBody().getWholeSaleStartingBillNumber() + "");
		parameters.put("BSWSCOUNT", voucherNumberDtlReport.getBody().getWholeSaleBillCount() + "");
		parameters.put("previouscashSo", "0");

		parameters.put("subreportdata2", jrBeanCollectionDataSource2);
		parameters.put("receiptmodewisereport", "jrxml/ReceiptModeWiseReport.jasper");

		// -----------------new version 1.8 surya

		parameters.put("subreportdata3", jrBeanCollectionDataSource3);
		parameters.put("totalCardReport", "jrxml/TotalReceiptModeWiseReport.jasper");

		parameters.put("subreportdata4", jrBeanCollectionDataSource4);
		parameters.put("receiptModeReport", "jrxml/DayendReceiptModeNew.jasper");

		// -----------------new version 1.8 surya end

//		dayEndReportStore.setPreviousCashSaleOrder("0");
//		dayEndReportStore.setPosMaxBillNo(voucherNumberDtlReport.getBody().getPosEndingBillNumber() + "");
//		dayEndReportStore.setPosMinBillNo(voucherNumberDtlReport.getBody().getPosStartingBillNumber() + "");
//		dayEndReportStore.setPosBillCount( voucherNumberDtlReport.getBody().getPosBillCount() + "");
//		
//		dayEndReportStore.setWholeSaleMaxBillNo(voucherNumberDtlReport.getBody().getWholeSaleEndingBillNumber() + "");
//		dayEndReportStore.setWholeSaleMinBillNo(voucherNumberDtlReport.getBody().getWholeSaleStartingBillNumber() + "");
//		dayEndReportStore.setWholeSaleBillCount(voucherNumberDtlReport.getBody().getWholeSaleBillCount() + "");

		ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-" + "PETTY CASH");
		AccountHeads accountHeads = accountHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> pettyCashAccountBalance = RestCaller.getAccountBalanceReport(rdate,
				rdate, accountHeads.getId());

		List<AccountBalanceReport> pettyCash = pettyCashAccountBalance.getBody();
		if (pettyCash.size() > 0) {
			if (pettyCash.get(0).getClosingBalanceType().equalsIgnoreCase("Cr")) {

				double pcash = pettyCash.get(0).getClosingBalance();
				parameters.put("PettyCash", "-" + pcash + "");
				// dayEndReportStore.setPettyCash("-"+pcash + "");
			} else {
				double pcash = pettyCash.get(0).getClosingBalance();
				// -------------version 4.6
				// dayEndReportStore.setPettyCash(pcash + "");
				// ------------version 4.6 end
				parameters.put("PettyCash", pcash + "");
			}
		} else {
			// ---------version 4.6
			// dayEndReportStore.setPettyCash("0");
			// ------------version 4.6 end
			parameters.put("PettyCash", "0");
		}

		ResponseEntity<AccountHeads> cashHeadsResp = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-CASH");
		AccountHeads cashAccountHeads = cashHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> cashAccountBalance = RestCaller.getAccountBalanceReport(rdate, rdate,
				cashAccountHeads.getId());

		List<AccountBalanceReport> casbalance = cashAccountBalance.getBody();

		try {
			ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(rdate);
			List<DailyReceiptsSummaryReport> receipts = receiptReport.getBody();

			Double receiptCash = 0.0;
			for (DailyReceiptsSummaryReport rec : receipts) {
				if (rec.getReceiptMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
					receiptCash = rec.getTotalCash();
				}
			}

			double dClosingCashBal = 0.0;
			if (null != casbalance && casbalance.size() > 0) {

				dClosingCashBal = casbalance.get(0).getClosingBalance();
			} else {
				dClosingCashBal = 0;
			}

//			BigDecimal bdClosingBal = new BigDecimal(dClosingCashBal);
//
//			bdClosingBal = bdClosingBal.setScale(0, bdClosingBal.ROUND_HALF_EVEN);
//			parameters.put("ClosingCash",0);

			double doublePettyCash = 0.0;

			if (null == pettyCash || pettyCash.size() == 0) {
				doublePettyCash = 0.0;
			} else {
				if (pettyCash.get(0).getClosingBalanceType().equalsIgnoreCase("Cr")) {
					doublePettyCash = 0 - pettyCash.get(0).getClosingBalance();
				} else {
					doublePettyCash = pettyCash.get(0).getClosingBalance();
				}
			}

			double doubleCashBalance = 0.0;
			if (null == casbalance || casbalance.size() == 0) {
				doubleCashBalance = 0.0;
			} else {
				doubleCashBalance = casbalance.get(0).getClosingBalance();
			}

			Double totalCash = doubleCashBalance + doublePettyCash;

			BigDecimal bdTotalCash = new BigDecimal(totalCash);

			bdTotalCash = bdTotalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);

//			parameters.put("totalCash", bdTotalCash.toPlainString());

			ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(rdate);
			List<DayEndClosureHdr> dayEndClosureHdr = dayEndReport.getBody();

			double dPhysicalCashP = 0.0;

			if (null != dayEndClosureHdr && dayEndClosureHdr.size() > 0) {
				dPhysicalCashP = dayEndClosureHdr.get(0).getPhysicalCash();
			} else {
				dPhysicalCashP = 0;
			}
			BigDecimal bdPhysicalCash = new BigDecimal(dPhysicalCashP);

			bdPhysicalCash = bdPhysicalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			parameters.put("PhysicalCash", bdPhysicalCash.toPlainString());
			// -----------version 4.6
			// dayEndReportStore.setPhysicalCash(bdPhysicalCash.toPlainString());
//		parameters.put("CashVariance", (casbalance.get(0).getClosingBalance()+receiptCash)-dayEndClosureHdr.get(0).getPhysicalCash());

//		parameters.put("CashVariance", (casbalance.get(0).getClosingBalance()+ pettyCash.get(0).getClosingBalance())-dayEndClosureHdr.get(0).getPhysicalCash()+"");

			// version 1.12 end

//-----------------version 1.11 end

			java.sql.Date utilDate = SystemSetting.StringToSqlDateSlash(rdate, "yyyy-MM-dd");

			parameters.put("rDate", utilDate);
			// dayEndReportStore.setDate(utilDate);

//----------version 1.11

			ResponseEntity<List<ReceiptModeReport>> getReceiptMod = RestCaller.getReceiptModeReport(rdate);
			Double totalSales = 0.0;

//		ResponseEntity<List<ReceiptModeReport>> getReceiptMode = RestCaller.getSaleOrderReceiptModeReport(rdate);

			ResponseEntity<List<ReceiptModeMst>> respReceiptMode = RestCaller.getAllReceiptMode();
			// ArrayList receiptModeList = (ArrayList) respReceiptMode.getBody();
			ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeCash = RestCaller
					.getReceiptModeReportByMode(rdate, SystemSetting.getSystemBranch() + "-CASH");
			if (getReceiptAmtByModeCash.getBody().size() > 0) {
				dtodayscashSale = dtodayscashSale + getReceiptAmtByModeCash.getBody().get(0).getAmount();
			}
			for (ReceiptModeMst receiptModeMst : respReceiptMode.getBody()) {
				ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByMode = RestCaller
						.getReceiptModeReportByMode(rdate, receiptModeMst.getReceiptMode());

				if (getReceiptAmtByMode.getBody().size() > 0) {

					if (receiptModeMst.getReceiptMode().equalsIgnoreCase("YES BANK"))

					{
						BigDecimal bYesBankr = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
						bYesBankr = bYesBankr.setScale(0, BigDecimal.ROUND_HALF_EVEN);

						parameters.put("yesBankr", bYesBankr.toPlainString());
						// dayEndReportStore.setYesBankReceipt(bYesBankr.toPlainString());
					}

					if (receiptModeMst.getReceiptMode().equalsIgnoreCase("PAYTM")) {
						BigDecimal bpaytm = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
						bpaytm = bpaytm.setScale(0, BigDecimal.ROUND_HALF_EVEN);

						parameters.put("paytmr", bpaytm.toPlainString());
						// -----------version 4.6
						// dayEndReportStore.setPaytmReceipt(bpaytm.toPlainString());
						// -----------version 4.6 end
					}

					if (receiptModeMst.getReceiptMode().equalsIgnoreCase("EBS")) {
						BigDecimal bEbsr = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
						bEbsr = bEbsr.setScale(0, BigDecimal.ROUND_HALF_EVEN);
						parameters.put("ebsr", bEbsr.toPlainString());
						// -----------version 4.6
						// dayEndReportStore.setEbsReceipt(bEbsr.toPlainString());
						// -----------version 4.6 end
					}
					if (receiptModeMst.getReceiptMode().equalsIgnoreCase("GPAY")) {
						BigDecimal bGpay = new BigDecimal(getReceiptAmtByMode.getBody().get(0).getAmount());
						bGpay = bGpay.setScale(0, BigDecimal.ROUND_HALF_EVEN);
						parameters.put("GPAYr", bGpay.toPlainString());
						// -----------version 4.6
						// dayEndReportStore.setGpayReceipt(bGpay.toPlainString());
						// -----------version 4.6 end
					}

				}
			}
			ResponseEntity<Double> getCashPA = RestCaller.getPreviousAdvanceAmount(rdate, "CASH");

			try {
				dPreviousCashSaleOrder = getCashPA.getBody().doubleValue();
				BigDecimal bdPreviousCashSaleOrder = new BigDecimal(dPreviousCashSaleOrder);
				bdPreviousCashSaleOrder = bdPreviousCashSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				parameters.put("previouscashSo", bdPreviousCashSaleOrder.toPlainString());
				// -----------version 4.6
				// dayEndReportStore.setPreviousCashSaleOrder(bdPreviousCashSaleOrder.toPlainString());
				// -----------version 4.6 end
			} catch (Exception e) {
				System.out.println("Error converting to  double value - Provius advance YES BANK");
			}

//		ResponseEntity<List<ReceiptModeReport>> getReceiptMod = RestCaller.getReceiptModeReport(rdate);
			Double yesBankso = 0.0;
			Double sbiBankso = 0.0;
			Double ebsso = 0.0;
			Double saleOrderPlusTodaysCash = 0.0;
			Double cashso = 0.0;
			parameters.put("CASHso", 0.0 + "");
			// -----------version 4.6
			// dayEndReportStore.setCashSaleOrder("0");
			// -----------version 4.6 end
			parameters.put("cardso", 0.0 + "");
			// -----------version 4.6
			// dayEndReportStore.setCardSaleOrder("0");
			// -----------version 4.6 end
			dtodayscashSale = dtodayscashSale + creditConvertTOCashforToday - dPreviousCashSaleOrder;
			System.out.println(" credit" + creditConvertTOCashforToday);
			System.out.println("Todays Cash including credit" + dtodayscashSale);
			BigDecimal bdtodayscashSale = new BigDecimal(dtodayscashSale);
			bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			parameters.put("CASH", bdtodayscashSale.toPlainString());
			// -----------version 4.6
			// dayEndReportStore.setCash(bdtodayscashSale.toPlainString());
			// -----------version 4.6 end
			ResponseEntity<List<ReceiptModeReport>> getReceiptMode = RestCaller
					.getSaleOrderReceiptModeReportByMode(rdate, "CASH");
			if (getReceiptMode.getBody().size() > 0)

			{
				saleOrderPlusTodaysCash = dtodayscashSale + getReceiptMode.getBody().get(0).getAmount();
				bdtodayscashSale = new BigDecimal(dtodayscashSale);
				bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//			parameters.put("CASH",bdtodayscashSale.toPlainString());

				BigDecimal bdtodayscashSaleOrder = new BigDecimal(getReceiptMode.getBody().get(0).getAmount());
				bdtodayscashSaleOrder = bdtodayscashSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				parameters.put("CASHso", bdtodayscashSaleOrder.toPlainString());
				// -----------version 4.6
				// dayEndReportStore.setCashSaleOrder(bdtodayscashSaleOrder.toPlainString());
				// -----------version 4.6 end
			} else {
				saleOrderPlusTodaysCash = dtodayscashSale;
			}
//			ResponseEntity<List<ReceiptModeReport>> getReceiptModeYEs = RestCaller.getSaleOrderReceiptModeReportByMode(rdate,"YES BANK");
//			ResponseEntity<List<ReceiptModeReport>> getReceiptModeSbi = RestCaller.getSaleOrderReceiptModeReportByMode(rdate,"SBI");
//			ResponseEntity<List<ReceiptModeReport>> getReceiptModeEbs = RestCaller.getSaleOrderReceiptModeReportByMode(rdate,"EBS");
//			ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeYesBank = RestCaller.getReceiptModeReportByMode
//					(rdate,"YES BANK");
//			Double dTodyasCard = 0.0;
//			if(getReceiptAmtByModeYesBank.getBody().size()>0)
//			{
//				dTodyasCard =dTodyasCard + getReceiptAmtByModeYesBank.getBody().get(0).getAmount();
//			}
//			
//			ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeSbi = RestCaller.getReceiptModeReportByMode
//					(rdate,"SBI");
//			if(getReceiptAmtByModeSbi.getBody().size()>0)
//			{
//				dTodyasCard =dTodyasCard + getReceiptAmtByModeSbi.getBody().get(0).getAmount();
//			}
//			ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeEbs = RestCaller.getReceiptModeReportByMode
//					(rdate,"EBS");
//			if(getReceiptAmtByModeEbs.getBody().size()>0)
//			{
//				dTodyasCard =dTodyasCard + getReceiptAmtByModeEbs.getBody().get(0).getAmount();
//			}
			// Get previous card from sale order

			Double dTodyasCard = 0.0;
			try {
				dTodyasCard = RestCaller.getSumOfSaleReceiptCard(rdate);
			} catch (Exception e) {
				System.out.println(e);
			}
			Double previousCardAmount = 0.0;
			ResponseEntity<Double> previousCardAmountResp = RestCaller.getPreviousAdvanceAmountCard(rdate);
			if (null != previousCardAmountResp.getBody()) {
				previousCardAmount = previousCardAmountResp.getBody().doubleValue();
			}
			dTodyasCard = dTodyasCard - previousCardAmount;
			BigDecimal bdtodayscardSale = new BigDecimal(dTodyasCard);
			bdtodayscardSale = bdtodayscardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//			ResponseEntity<Double> getSBIPA = RestCaller.getPreviousAdvanceAmount(   rdate, "SBI");
//			ResponseEntity<Double> getEBSPA = RestCaller.getPreviousAdvanceAmount(   rdate, "EBS");
//			ResponseEntity<Double> getYESPA = RestCaller.getPreviousAdvanceAmount(   rdate, "YES BANK");
//
//			Double previousCardAmount =0.0;
//			if(null != getSBIPA.getBody())
//			{
//				previousCardAmount = previousCardAmount + getSBIPA.getBody().doubleValue();
//			}
//			if(null != getEBSPA.getBody())
//			{
//				previousCardAmount = previousCardAmount + getEBSPA.getBody().doubleValue();
//			}
//			if(null != getYESPA.getBody())
//			{
//				previousCardAmount = previousCardAmount + getYESPA.getBody().doubleValue();
//			}
//			BigDecimal bdtodayscardSale = new BigDecimal(dTodyasCard);
//			bdtodayscardSale = bdtodayscardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			parameters.put("cardsales", bdtodayscardSale.toPlainString());
			// -----------version 4.6
			// dayEndReportStore.setCardSale(bdtodayscardSale.toPlainString());
			// -----------version 4.6 end
			BigDecimal bdpreviousCardAmount = new BigDecimal(previousCardAmount);
			bdpreviousCardAmount = bdpreviousCardAmount.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			Double cardSaleOrder = 0.0;
//			ResponseEntity<List<ReceiptModeReport>> getReceiptModeYEs = RestCaller.getSaleOrderReceiptModeReportByMode(rdate,"YES BANK");
//			ResponseEntity<List<ReceiptModeReport>> getReceiptModeSbi = RestCaller.getSaleOrderReceiptModeReportByMode(rdate,"SBI");
//			ResponseEntity<List<ReceiptModeReport>> getReceiptModeEbs = RestCaller.getSaleOrderReceiptModeReportByMode(rdate,"EBS");
//			if(getReceiptModeYEs.getBody().size()>0)
//			{
//				cardSaleOrder =cardSaleOrder + getReceiptModeYEs.getBody().get(0).getAmount();
//			}
//			if(getReceiptModeSbi.getBody().size()>0)
//			{
//				cardSaleOrder =cardSaleOrder + getReceiptModeSbi.getBody().get(0).getAmount();
//			}
//			if(getReceiptModeEbs.getBody().size()>0)
//			{
//				cardSaleOrder =cardSaleOrder + getReceiptModeEbs.getBody().get(0).getAmount();
//			}

			// Previous Card SO

			// Loop through each card.

			ResponseEntity<List<ReceiptModeMst>> receiptModeResp = RestCaller.getAllReceiptModes();

			List<ReceiptModeMst> receiptModeList = receiptModeResp.getBody();
			Double previousCardAmountRealized = 0.0;
			for (int i = 0; i < receiptModeList.size(); i++) {
				ReceiptModeMst aReceiptModeMst = receiptModeList.get(i);
				if (null != aReceiptModeMst.getCreditCardStatus()
						&& aReceiptModeMst.getCreditCardStatus().equalsIgnoreCase("YES")) {

					ResponseEntity<Double> previousCardAmountRealizedResp = RestCaller
							.getPreviousAdvanceAmountCardRelized(rdate, aReceiptModeMst.getReceiptMode());
					if (null != previousCardAmountRealizedResp.getBody()) {
						previousCardAmountRealized = previousCardAmountRealized
								+ previousCardAmountRealizedResp.getBody().doubleValue();
					}
				}
			}

			System.out.println("PREVIOUS CARD SALE " + previousCardAmountRealized);
			BigDecimal bdpreviousCardAmountRealized = new BigDecimal(previousCardAmountRealized);
			bdpreviousCardAmountRealized = bdpreviousCardAmountRealized.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			parameters.put("prevcardso", bdpreviousCardAmountRealized.toPlainString());

			cardSaleOrder = RestCaller.getSumOfSaleOrderReceiptCard(rdate);
			BigDecimal bdtodayscardSaleOrder = new BigDecimal(cardSaleOrder);
			bdtodayscardSaleOrder = bdtodayscardSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			parameters.put("cardso", bdtodayscardSaleOrder.toPlainString());

			// -----------version 4.6
			// dayEndReportStore.setCardSaleOrder(bdtodayscardSaleOrder.toPlainString());
			// //-----------version 4.6 end

			ResponseEntity<AccountHeads> getAcc = RestCaller.getAccountHeadByName("SWIGGY");
			ResponseEntity<List<ReceiptModeReport>> getReceiptModeSWIGGY = RestCaller.getReceiptModeReportByAccID(rdate,
					getAcc.getBody().getId());

			if (getReceiptModeSWIGGY.getBody().size() > 0)

			{
				BigDecimal bswiggy = new BigDecimal(getReceiptModeSWIGGY.getBody().get(0).getAmount());
				bswiggy = bswiggy.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				parameters.put("swiggyr", bswiggy.toPlainString());
				// -----------version 4.6
				// dayEndReportStore.setSwiggyReceipt(bswiggy.toPlainString());
				// -----------version 4.6 end
			}
			ResponseEntity<AccountHeads> getAcczomato = RestCaller.getAccountHeadByName("ZOMATO");
			ResponseEntity<List<ReceiptModeReport>> getReceiptModeZOMATO = RestCaller.getReceiptModeReportByAccID(rdate,
					getAcczomato.getBody().getId());

			if (getReceiptModeZOMATO.getBody().size() > 0)

			{
				BigDecimal bzomato = new BigDecimal(getReceiptModeZOMATO.getBody().get(0).getAmount());
				bzomato = bzomato.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				parameters.put("zomator", bzomato.toPlainString());
				// -----------version 4.6
				// dayEndReportStore.setZomatoReceipt(bzomato.toPlainString());
				// -----------version 4.6 end
			}

			ResponseEntity<List<ReceiptModeReport>> getReceiptModeCREDIT = RestCaller.getReceiptModeReportByMode(rdate,
					"CREDIT");

			if (getReceiptModeCREDIT.getBody().size() > 0)

			{
				BigDecimal bcredit = new BigDecimal(
						getReceiptModeCREDIT.getBody().get(0).getAmount() - creditConvertTOCashforToday);
				bcredit = bcredit.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				parameters.put("creditr", bcredit.toPlainString());
				// -----------version 4.6
				// dayEndReportStore.setCreditReceipt(bcredit.toPlainString());
				// -----------version 4.6 end
			}

			ResponseEntity<List<ReceiptModeReport>> getReceiptModeNEFT = RestCaller.getReceiptModeReportByMode(rdate,
					"NEFT");

			if (getReceiptModeNEFT.getBody().size() > 0)

			{
				BigDecimal bneftt = new BigDecimal(getReceiptModeNEFT.getBody().get(0).getAmount());
				bneftt = bneftt.setScale(0, BigDecimal.ROUND_HALF_EVEN);

				parameters.put("neftsale", bneftt.toPlainString());
				// -----------version 4.6
				// dayEndReportStore.setNeftSales(bneftt.toPlainString());
				// -----------version 4.6 end

			}

			// totalCash = saleOrderPlusTodaysCash - dPreviousCashSaleOrder;
			dClosingCashBal = totalCash - doublePettyCash;
			BigDecimal bdClosingCashBal = new BigDecimal(dClosingCashBal);
			bdClosingCashBal = bdClosingCashBal.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			BigDecimal bdtotalCash = new BigDecimal(saleOrderPlusTodaysCash.doubleValue());
			bdtotalCash = bdtotalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			// parameters.put("ClosingCash",bdClosingCashBal.toPlainString());
			parameters.put("totalCash", bdtotalCash.toPlainString());
			/// parameters.put("CASH",bdtodayscashSale.toPlainString());

			// -----------version 4.6
			// dayEndReportStore.setTotalCash(bdtotalCash.toPlainString());
			// -----------version 4.6 end
			Double cashVariance = saleOrderPlusTodaysCash - dPhysicalCashP;
			BigDecimal bdCashVariance = new BigDecimal(cashVariance);
			bdCashVariance = bdCashVariance.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			parameters.put("CashVariance", bdCashVariance.toPlainString());

			// -----------version 4.6
			// dayEndReportStore.setCashVarience(bdCashVariance.toPlainString());
			// -----------version 4.6 end
			// Find total Sale
			double totalSale = RestCaller.getInvoiceTotalSales(rdate);
			BigDecimal btotalSale = new BigDecimal(totalSale);
			btotalSale = btotalSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			parameters.put("totalsales", btotalSale.toPlainString());
			// -----------version 4.6
			// dayEndReportStore.setTotalSales(btotalSale.toPlainString());
			// ResponseEntity<DayEndReportStore> dayEndReportStoreSaved =
			// RestCaller.saveDayEndReportStore(dayEndReportStore);
			// -----------version 4.6 end

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}

	}

//version1.10ends
	public static void CategoryWiseShortExpiryReport(String selectedItems, String branchCode, String rdate)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/ShortExpiry" + branchCode + ".pdf";

		// Call url
		ResponseEntity<List<ShortExpiryReport>> getSavedItems = RestCaller.getCategoryWiseExpiry(selectedItems);

		List<ShortExpiryReport> ShortExpiryReportList = getSavedItems.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ShortExpiryReportList);

		String jrxmlPath = "jrxml/CategoryWiseShortExpiryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());
		parameters.put("rDate", rdate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		parameters.put("GST", branchMst.getBranchGst());
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemWiseShortExpiryReport(String selectedItems, String branchCode, String rdate)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/ShortExpiry" + branchCode + ".pdf";

		// Call url
		ResponseEntity<List<ShortExpiryReport>> getSavedItems = RestCaller.getItemWiseExpiry(selectedItems);

		List<ShortExpiryReport> ShortExpiryReportList = getSavedItems.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ShortExpiryReportList);

		String jrxmlPath = "jrxml/ItemWiseShortExpiry.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		java.util.Date sdate = SystemSetting.localToUtilDate(LocalDate.now());
		parameters.put("rDate", rdate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		parameters.put("GST", branchMst.getBranchGst());
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void OrgMonthlyAccountBalanceReport(String startDate, String endDate, String accountId)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/orgAccountBalance" + accountId + startDate + ".pdf";
		ResponseEntity<List<OrgMonthlyAccountReport>> accountBalanceReport = RestCaller
				.getOrgMonthlyAccountBalanceReport(startDate, endDate, accountId);
		List<OrgMonthlyAccountReport> accountBalanceReportList = accountBalanceReport.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				accountBalanceReportList);

		String jrxmlPath = "jrxml/OrgMonthlyAccountReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		// java.util.Date sDate =SystemSetting.localToUtilDate(LocalDate.now());
		parameters.put("sDate", SystemSetting.getSystemDate());
		// parameters.put("End Date", endDate);

		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void OrgMonthlyAccountPayReport(String startDate, String endDate, String accountId)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/orgAccountPay" + accountId + startDate + ".pdf";
		ResponseEntity<List<OrgMonthlyAccPayReport>> accountBalanceReport = RestCaller
				.getOrgMonthlyAccPayReport(startDate, endDate, accountId);
		List<OrgMonthlyAccPayReport> accountBalanceReportList = accountBalanceReport.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				accountBalanceReportList);

		String jrxmlPath = "jrxml/OrgMonthlyAccPayReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("sDate", SystemSetting.getSystemDate());
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void OrgMemberwiseReport(String startDate, String endDate, String accountId) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/memberwisereport" + accountId + startDate + ".pdf";
		ResponseEntity<List<OrgMemberWiseReceiptReport>> memberwiseReport = RestCaller.getOrgMemberwiseReport(startDate,
				endDate, accountId);
		List<OrgMemberWiseReceiptReport> memberwiseReportList = memberwiseReport.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(memberwiseReportList);

		String jrxmlPath = "jrxml/MemberWise.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("sDate", SystemSetting.getSystemDate());
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ProductConvertion(String fromdate, String todate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/produvtconvertion.pdf";
		ResponseEntity<List<ProductConversionReport>> respentity = RestCaller.getProductConversionReport(fromdate,
				todate);

		List<ProductConversionReport> memberwiseReportList = respentity.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(memberwiseReportList);

		String jrxmlPath = "jrxml/MemberWise.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

		if (null != branchMst) {
			parameters.put("branchName", branchMst.getBranchName());
			parameters.put("branchAddress", branchMst.getBranchAddress1());
			parameters.put("branchAddress2", branchMst.getBranchAddress2());
			parameters.put("branchSatate", branchMst.getBranchState());
			parameters.put("branchPhone", branchMst.getBranchTelNo());
			parameters.put("branchGst", branchMst.getBranchGst());
		}

		parameters.put("StartDate", fromdate);
		parameters.put("EndDate", todate);

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void JobCardInvoice(String voucheNo, String date, JobCardHdr jobCardHdr) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/customerwisereport" + date + ".pdf";

		// Call url

		ResponseEntity<List<SalesTransHdr>> CustomerWiseResponse = RestCaller.getCustomerWiseReport(date);

		List<SalesTransHdr> CustomerWise = CustomerWiseResponse.getBody();
		for (SalesTransHdr salesTransHdr : CustomerWise) {
			salesTransHdr.setCustomerId(salesTransHdr.getAccountHeads().getAccountName());
			salesTransHdr.setBranchCode(SystemSetting.systemBranch);
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(CustomerWise);

		String jrxmlPath = "jrxml/customerWiseReports.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());
		parameters.put("Email", branchMst.getBranchEmail());
		parameters.put("WebSite", branchMst.getBranchWebsite());

		ResponseEntity<AccountHeads> customerResp = RestCaller.getAccountHeadsById(jobCardHdr.getCustomerId());
		AccountHeads customerMst = customerResp.getBody();

		if (null != customerMst) {
			parameters.put("CustomerName", customerMst.getAccountName());
			parameters.put("CustometAddress", customerMst.getPartyAddress());
			parameters.put("CustomerState", customerMst.getCustomerState());
			parameters.put("CustomerPhone", customerMst.getCustomerContact());

			parameters.put("CustomerEmail", customerMst.getPartyMail());
			parameters.put("CustomerGst", "GST :" + customerMst.getPartyGst());

		}

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ServiceInReport(String stratDate, String endDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/servicein.pdf";

		// Call url
		ResponseEntity<List<ServiceInReport>> serviceInList = RestCaller.getServiceInReportBetweenDate(stratDate,
				endDate);
		List<ServiceInReport> serviceListTable = serviceInList.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(serviceListTable);

		String jrxmlPath = "jrxml/ServiceInReport.jrxml";

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());
		parameters.put("Email", branchMst.getBranchEmail());
		parameters.put("WebSite", branchMst.getBranchWebsite());
		parameters.put("StartDate", stratDate);

		parameters.put("EndDate", endDate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void JobCardReport(String stratDate, String endDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/jobcard.pdf";

		// Call url
		ResponseEntity<List<JobCardHdr>> jobCardListResp = RestCaller.getJobCardHdrBetweenDate(stratDate, endDate);
		List<JobCardHdr> jobCardHdrList = jobCardListResp.getBody();
		jobCardHdrList = FXCollections.observableArrayList(jobCardListResp.getBody());

		for (JobCardHdr job : jobCardHdrList) {
			if (null != job.getCustomerId()) {
				ResponseEntity<AccountHeads> customerResp = RestCaller.getAccountHeadsById(job.getCustomerId());
				AccountHeads customerMst = customerResp.getBody();
				if (null != customerMst) {
					job.setCustomerName(customerMst.getAccountName());
					job.setItemName(job.getServiceInDtl().getItemName());
				}

			}
		}

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(jobCardHdrList);

		String jrxmlPath = "jrxml/JobCardReport.jrxml";

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());
		parameters.put("Email", branchMst.getBranchEmail());
		parameters.put("WebSite", branchMst.getBranchWebsite());
		parameters.put("StartDate", stratDate);

		parameters.put("EndDate", endDate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SupplierDueReport(String startDate, String endDate) throws JRException {

		System.out.println("--------------SupplierBillwiseDueDayReport----------------------");

		String pdfToGenerate = SystemSetting.reportPath + "supplierbillwiseduedayreport" + ".pdf";

		System.out.println(pdfToGenerate);
		// Call url

		ResponseEntity<List<SupplierBillwiseDueDayReport>> supplierLedgerReport = RestCaller
				.SupplierDueReport(startDate, endDate);

		List<SupplierBillwiseDueDayReport> supplierLedgerReportList = supplierLedgerReport.getBody();

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		if (null != branchMst) {
			parameters.put("CompanyNameParam", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("BranchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", branchMst.getBranchGst());

		}
		parameters.put("today", SystemSetting.systemDate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				supplierLedgerReportList);

		String jrxmlPath = "jrxml/supplierbillwisebyduedays.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void dayEndReportByDate(String strDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "dayendreport" + strDate + ".pdf";

		// Call url

		ResponseEntity<List<SalesReport>> salesReport = RestCaller.getSalesSummaryReportDayEnd(strDate);

		List<SalesReport> salesReportList = salesReport.getBody();

		Double totalAmount = 0.0;
		Double invoiceAmount = 0.0;
		Double creditAmount = 0.0;

		for (SalesReport sales : salesReportList) {
			totalAmount = totalAmount + sales.getCard() + sales.getCash();
			invoiceAmount = invoiceAmount + sales.getInvovicetotal();
			creditAmount = invoiceAmount - totalAmount;

			sales.setCard2(creditAmount);
		}

		ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(strDate);
		List<DayEndClosureHdr> dayEndClosureHdrList = dayEndReport.getBody();
		for (DayEndClosureHdr hdr : dayEndClosureHdrList) {
			ResponseEntity<List<SalesReport>> salesReportResp = RestCaller.getSalesSummaryReportDayEnd(strDate);
			List<SalesReport> salesReportDayend = salesReportResp.getBody();

			for (SalesReport sales : salesReportDayend) {
				hdr.setCardSale(sales.getCard());
//					dayEndClosure.setCardSale2(sales.getCardSale2());
				hdr.setCashSale(sales.getCash());
			}

			ResponseEntity<List<DailyPaymentSummaryReport>> paymentReport = RestCaller.PaymentSummaryDayEnd(strDate);
			List<DailyPaymentSummaryReport> paymentSummaryReport = paymentReport.getBody();

			for (DailyPaymentSummaryReport payment : paymentSummaryReport) {
				if (payment.getPaymentMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
					Double cash = 0.0;
					cash = payment.getTotalCash();
					if (null != hdr.getCashSale()) {
						cash = cash + hdr.getCashSale();
					}
					hdr.setCashSale(cash);
				}

			}

			ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(strDate);

			List<DailyReceiptsSummaryReport> receiptSummary = receiptReport.getBody();

			for (DailyReceiptsSummaryReport receipts : receiptSummary) {
				if (receipts.getReceiptMode().equalsIgnoreCase(SystemSetting.systemBranch + "-CASH")) {
					Double cash = 0.0;
					cash = receipts.getTotalCash();
					if (null != hdr.getCashSale()) {
						cash = cash + hdr.getCashSale();
					}
					hdr.setCashSale(cash);
				}

				if (receipts.getReceiptMode().equalsIgnoreCase("CARD")) {
					Double card = 0.0;
					card = receipts.getTotalCash();
					if (null != hdr.getCardSale()) {
						card = card + hdr.getCardSale();
					}
					hdr.setCardSale(card);
				}

			}

			if (null != hdr.getUserId()) {
				ResponseEntity<UserMst> userResp = RestCaller.getUserNameById(hdr.getUserId());
				UserMst userMst = userResp.getBody();
				if (null != userMst) {
					hdr.setUserName(userMst.getUserName());
				}
			}
		}

		ResponseEntity<List<DailyPaymentSummaryReport>> paymentReport = RestCaller.PaymentSummaryDayEnd(strDate);
		List<DailyPaymentSummaryReport> paymentReportList = paymentReport.getBody();

		ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(strDate);
		List<DailyReceiptsSummaryReport> receiptReportList = FXCollections.observableArrayList(receiptReport.getBody());

		ResponseEntity<List<SalesModeWiseBillReport>> billSeries = RestCaller.BillSeriesDayEnd(strDate);
		List<SalesModeWiseBillReport> billSeriesList = billSeries.getBody();

		ResponseEntity<List<ReceiptModeReport>> resceiptModeReport = RestCaller.getReceiptModeReport(strDate);
		List<ReceiptModeReport> receiptModeReport = resceiptModeReport.getBody();

		// ----------------------------------------------------

		ObservableList<DayEndClosureDtl> dayEndList = FXCollections.observableArrayList();

		ArrayList day_end_dtl = new ArrayList();
		day_end_dtl = RestCaller.getDenominationAndCount(strDate);
		Double denomination = 0.0;
		Double totalAmt = 0.0;
		Double count = 0.0;
		Iterator itr = day_end_dtl.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			denomination = (Double) element.get(2);
			count = (Double) element.get(1);

			DayEndClosureDtl dayEndClosureDtl = new DayEndClosureDtl();
			dayEndClosureDtl.setCount(count);
			dayEndClosureDtl.setDenomination(denomination);
			dayEndClosureDtl.setTotalAmount(count * denomination);

			dayEndList.add(dayEndClosureDtl);

		}

		// --------------------------------------------------------------
		ResponseEntity<List<ReceiptModeReport>> saleOrderResceiptModeReport = RestCaller
				.getSaleOrderReceiptModeReport(strDate);
		List<ReceiptModeReport> saleOrderReceiptModeReport = saleOrderResceiptModeReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(salesReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(dayEndClosureHdrList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(paymentReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(receiptReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource5 = new JRBeanCollectionDataSource(billSeriesList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource6 = new JRBeanCollectionDataSource(receiptModeReport);
		JRBeanCollectionDataSource jrBeanCollectionDataSource7 = new JRBeanCollectionDataSource(dayEndList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource8 = new JRBeanCollectionDataSource(
				saleOrderReceiptModeReport);

		String jrxmlPath1 = "jrxml/DayEndReportByDate.jrxml";

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath1);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		if (null != branchMst) {
			parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("BranchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", "Ph." + branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", "GST:" + branchMst.getBranchGst());
			parameters.put("Date", strDate);

		}

		parameters.put("subreportdata", jrBeanCollectionDataSource2);
		parameters.put("subreportphysicalcash", "jrxml/PhysicalCash.jasper");

		parameters.put("subreportdatapayment", jrBeanCollectionDataSource3);
		parameters.put("subreportpayment", "jrxml/DayEndPaymentSummary.jasper");

		parameters.put("subreportreceiptdata", jrBeanCollectionDataSource4);
		parameters.put("subreportreceipt", "jrxml/DayEndReceiptSummary.jasper");

		parameters.put("subreportbillseriesdata", jrBeanCollectionDataSource5);
		parameters.put("subreportbillseries", "jrxml/DayEndBillSeries.jasper");

		parameters.put("subreport3data", jrBeanCollectionDataSource6);
		parameters.put("subreport3jrxml", "jrxml/dayEndReceiptMode.jasper");

		parameters.put("subreport4data", jrBeanCollectionDataSource7);
		parameters.put("subreport4jrxml", "jrxml/CountAndDenomination.jasper");

		parameters.put("subreport5data", jrBeanCollectionDataSource8);
		parameters.put("subreport5jrxml", "jrxml/SaleOrderAdvanceReport.jasper");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PrintSessionEndReport(SessionEndClosureMst sessionEndClosureMst, String strDate)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "dayendreport" + strDate + ".pdf";

		// Call url

		ResponseEntity<List<SalesReport>> salesReport = RestCaller.getSalesSummaryReportDayEnd(strDate);

		List<SalesReport> salesReportList = salesReport.getBody();

		Double totalAmount = 0.0;
		Double invoiceAmount = 0.0;
		Double creditAmount = 0.0;

		for (SalesReport sales : salesReportList) {
			totalAmount = totalAmount + sales.getCard() + sales.getCash();
			invoiceAmount = invoiceAmount + sales.getInvovicetotal();
			creditAmount = invoiceAmount - totalAmount;

			sales.setCard2(creditAmount);
		}

		ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(strDate);
		List<DayEndClosureHdr> dayEndClosureHdrList = dayEndReport.getBody();
		for (DayEndClosureHdr hdr : dayEndClosureHdrList) {
			if (null != hdr.getUserId()) {
				ResponseEntity<UserMst> userResp = RestCaller.getUserNameById(hdr.getUserId());
				UserMst userMst = userResp.getBody();
				if (null != userMst) {
					hdr.setUserName(userMst.getUserName());
				}
			}
		}

		ResponseEntity<List<DailyPaymentSummaryReport>> paymentReport = RestCaller.PaymentSummaryDayEnd(strDate);
		List<DailyPaymentSummaryReport> paymentReportList = paymentReport.getBody();

		ResponseEntity<List<DailyReceiptsSummaryReport>> receiptReport = RestCaller.ReceiptSummaryDayEnd(strDate);
		List<DailyReceiptsSummaryReport> receiptReportList = FXCollections.observableArrayList(receiptReport.getBody());

		ResponseEntity<List<SalesModeWiseBillReport>> billSeries = RestCaller.BillSeriesDayEnd(strDate);
		List<SalesModeWiseBillReport> billSeriesList = billSeries.getBody();

		ResponseEntity<List<ReceiptModeReport>> resceiptModeReport = RestCaller.getReceiptModeReport(strDate);
		List<ReceiptModeReport> receiptModeReport = resceiptModeReport.getBody();

//----------------------------------------------------

		ObservableList<DayEndClosureDtl> dayEndList = FXCollections.observableArrayList();

		ArrayList day_end_dtl = new ArrayList();
		day_end_dtl = RestCaller.getDenominationAndCount(strDate);
		Double denomination = 0.0;
		Double totalAmt = 0.0;
		Double count = 0.0;
		Iterator itr = day_end_dtl.iterator();
		while (itr.hasNext()) {

			List element = (List) itr.next();
			denomination = (Double) element.get(2);
			count = (Double) element.get(1);

			DayEndClosureDtl dayEndClosureDtl = new DayEndClosureDtl();
			dayEndClosureDtl.setCount(count);
			dayEndClosureDtl.setDenomination(denomination);
			dayEndClosureDtl.setTotalAmount(count * denomination);

			dayEndList.add(dayEndClosureDtl);

		}

		ResponseEntity<SessionEndClosureMst> sessionEndMstResp = RestCaller
				.findSessionEndMstById(sessionEndClosureMst.getId());
		SessionEndClosureMst sessionEndMst = sessionEndMstResp.getBody();

		ResponseEntity<List<SessionEndDtl>> sessionEndDtlResp = RestCaller
				.findSessionEndDtlByHdrId(sessionEndClosureMst.getId());

		List<SessionEndDtl> sessionEndDtlList = sessionEndDtlResp.getBody();

		ResponseEntity<UserMst> userResp = RestCaller.getUserNameById(sessionEndMst.getUserId());
		UserMst userMst = userResp.getBody();
//--------------------------------------------------------------
		ResponseEntity<List<ReceiptModeReport>> saleOrderResceiptModeReport = RestCaller
				.getSaleOrderReceiptModeReport(strDate);
		List<ReceiptModeReport> saleOrderReceiptModeReport = saleOrderResceiptModeReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(salesReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(dayEndClosureHdrList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(paymentReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(receiptReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource5 = new JRBeanCollectionDataSource(billSeriesList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource6 = new JRBeanCollectionDataSource(receiptModeReport);
		JRBeanCollectionDataSource jrBeanCollectionDataSource7 = new JRBeanCollectionDataSource(dayEndList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource8 = new JRBeanCollectionDataSource(
				saleOrderReceiptModeReport);

		JRBeanCollectionDataSource jrBeanCollectionDataSource9 = new JRBeanCollectionDataSource(sessionEndDtlList);

		String jrxmlPath1 = "jrxml/SessionEndTransactionReport.jrxml";

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath1);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		if (null != branchMst) {
			parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("BranchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", "Ph." + branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", "GST:" + branchMst.getBranchGst());
			parameters.put("Date", strDate);

		}

		parameters.put("subreportdata", jrBeanCollectionDataSource2);
		parameters.put("subreportphysicalcash", "jrxml/PhysicalCash.jasper");

		parameters.put("subreportdatapayment", jrBeanCollectionDataSource3);
		parameters.put("subreportpayment", "jrxml/DayEndPaymentSummary.jasper");

		parameters.put("subreportreceiptdata", jrBeanCollectionDataSource4);
		parameters.put("subreportreceipt", "jrxml/DayEndReceiptSummary.jasper");

		parameters.put("subreportbillseriesdata", jrBeanCollectionDataSource5);
		parameters.put("subreportbillseries", "jrxml/DayEndBillSeries.jasper");

		parameters.put("subreport3data", jrBeanCollectionDataSource6);
		parameters.put("subreport3jrxml", "jrxml/dayEndReceiptMode.jasper");

//		parameters.put("subreport4data", jrBeanCollectionDataSource7);
//        parameters.put("subreport4jrxml", "jrxml/CountAndDenomination.jasper");

		parameters.put("subreport5data", jrBeanCollectionDataSource8);
		parameters.put("subreport5jrxml", "jrxml/SaleOrderAdvanceReport.jasper");

		parameters.put("subreport6data", jrBeanCollectionDataSource9);
		parameters.put("subreport6jrxml", "jrxml/SessionEndReport.jasper");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PrintSessionEndReportOld(SessionEndClosureMst sessionEndClosureMst) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "sessionend"
				+ SystemSetting.UtilDateToString(sessionEndClosureMst.getSessionDate(), "yyyy-MM-dd") + "SlNo"
				+ sessionEndClosureMst.getSlNo() + ".pdf";

		ResponseEntity<SessionEndClosureMst> sessionEndMstResp = RestCaller
				.findSessionEndMstById(sessionEndClosureMst.getId());
		SessionEndClosureMst sessionEndMst = sessionEndMstResp.getBody();

		ResponseEntity<List<SessionEndDtl>> sessionEndDtlResp = RestCaller
				.findSessionEndDtlByHdrId(sessionEndClosureMst.getId());

		List<SessionEndDtl> sessionEndDtlList = sessionEndDtlResp.getBody();

		ResponseEntity<UserMst> userResp = RestCaller.getUserNameById(sessionEndMst.getUserId());
		UserMst userMst = userResp.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(sessionEndDtlList);
		String jrxmlPath = "jrxml/SessionEndReport.jrxml";

		JasperReport jasperReport;

		jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("SessionDate", SystemSetting.UtilDateToString(sessionEndMst.getSessionDate(), "yyyy-MM-dd"));
		parameters.put("Session", sessionEndMst.getSlNo() + "");
		parameters.put("UserName", userMst.getUserName());
		parameters.put("Cash", sessionEndMst.getCashSale());
		parameters.put("Card", sessionEndMst.getCardSale());
		parameters.put("PhysicalCash", sessionEndMst.getPhysicalCash());

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		if (null != branchMst) {
			parameters.put("CompanyNameParam", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("BranchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", "Ph." + branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", "GST:" + branchMst.getBranchGst());

		}

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PlaningAndActualProductionReport(String startDate, String endDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "actualandplaningproductionreport" + startDate + "to"
				+ endDate + ".pdf";

		ResponseEntity<List<FinishedProduct>> actualProductionResp = RestCaller
				.findPlaningAndActualProductionReport(startDate, endDate);
		List<FinishedProduct> planingReport = actualProductionResp.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(planingReport);
		String jrxmlPath = "jrxml/SessionEndReport.jrxml";

		JasperReport jasperReport;

		jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("startDate", startDate);
		parameters.put("endDate", endDate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		if (null != branchMst) {
			parameters.put("CompanyNameParam", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("BranchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", "Ph." + branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", "GST:" + branchMst.getBranchGst());

		}

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void getDayendFromReportStore(String sdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/dayend" + SystemSetting.systemBranch + ".pdf";

		ResponseEntity<List<SalesModeWiseBillReport>> billSeries = RestCaller.BillSeriesDayEndByInvoiceSeries(sdate);
		List<SalesModeWiseBillReport> salesMOdeList = billSeries.getBody();

		ResponseEntity<DayEndReportStore> dayEndReportStoreSaved = RestCaller.getDayEndReportFromStore(sdate);
		DayEndReportStore dayEndReportStore = dayEndReportStoreSaved.getBody();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(salesMOdeList);

		String jrxmlPath = "jrxml/dayendreport_amb.jrxml";

		JasperReport jasperReport;

		jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();
		parameters.put("branchName", branchMst.getBranchName());
		// ------------version 4.7
		dayEndReportStore.setBranchName(branchMst.getBranchName());

		// ------------version 4.7 end

		parameters.put("BSPOSMAXNO", dayEndReportStore.getPosMaxBillNo());
		parameters.put("BSPOSMINNO", dayEndReportStore.getPosMinBillNo());
		parameters.put("BSPOSCOUNT", dayEndReportStore.getPosBillCount());

		parameters.put("BSWSMAXNO", dayEndReportStore.getWholeSaleMaxBillNo());
		parameters.put("BSWSMINNO", dayEndReportStore.getWholeSaleMinBillNo());
		parameters.put("BSWSCOUNT", dayEndReportStore.getWholeSaleBillCount());

		parameters.put("previouscashSo", "0");
		parameters.put("PettyCash", "0");

		parameters.put("PettyCash", dayEndReportStore.getPettyCash());

		parameters.put("PhysicalCash", dayEndReportStore.getPhysicalCash());

		java.sql.Date utilDate = SystemSetting.StringToSqlDateSlash(sdate, "yyyy-MM-dd");

		parameters.put("rDate", utilDate);

		parameters.put("yesBankr", dayEndReportStore.getYesBankReceipt());

		parameters.put("paytmr", dayEndReportStore.getPaytmReceipt());
		parameters.put("ebsr", dayEndReportStore.getEbsReceipt());
		parameters.put("GPAYr", dayEndReportStore.getGpayReceipt());
		parameters.put("previouscashSo", dayEndReportStore.getPreviousCashSaleOrder());

		parameters.put("CASHso", dayEndReportStore.getCashSaleOrder());

		parameters.put("cardso", dayEndReportStore.getCardSaleOrder());

		parameters.put("CASH", dayEndReportStore.getCash());

		parameters.put("CASHso", dayEndReportStore.getCashSaleOrder());

		parameters.put("cardsales", dayEndReportStore.getCardSale());

		parameters.put("cardso", dayEndReportStore.getCardSaleOrder());

		parameters.put("swiggyr", dayEndReportStore.getSwiggyReceipt());

		parameters.put("zomator", dayEndReportStore.getZomatoReceipt());

		parameters.put("creditr", dayEndReportStore.getCreditReceipt());

		parameters.put("neftsale", dayEndReportStore.getNeftSales());

		parameters.put("totalCash", dayEndReportStore.getTotalCash());

		parameters.put("totalsales", dayEndReportStore.getTotalSales());
		parameters.put("CashVariance", dayEndReportStore.getCashVarience());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DayEndThermalReport(String sdate) throws JRException {

//		String pdfToGenerate = SystemSetting.reportPath + "/dayend" + SystemSetting.systemBranch + ".pdf";
//
//		Double dtodayscashSale = 0.0, dPreviousCashSaleOrder = 0.0, dTodaysCashSaleOrder = 0.0;
//
//		ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeCash = RestCaller.getReceiptModeReportByMode(sdate,
//				SystemSetting.getSystemBranch() + "-CASH");
//		if (getReceiptAmtByModeCash.getBody().size() > 0) {
//			dtodayscashSale = dtodayscashSale + getReceiptAmtByModeCash.getBody().get(0).getAmount();
//		}
//
//		Double creditConvertTOCashforToday = RestCaller.getCreditSaleAdjstmnt(SystemSetting.getUser().getBranchCode(),
//				sdate);
//
//		ResponseEntity<List<SalesTaxSplitReport>> salesTaxSplitReport = RestCaller.getSalesSplit(sdate);
//		List<SalesTaxSplitReport> salesTaxSplit = salesTaxSplitReport.getBody();
//
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(salesTaxSplit);
//		String jrxmlPath = "jrxml/dayendreport_amb.jrxml";// add new filename
//		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
//
//		HashMap parameters = new HashMap();
//
//		dtodayscashSale = dtodayscashSale + creditConvertTOCashforToday - dPreviousCashSaleOrder;
//		System.out.println(" credit" + creditConvertTOCashforToday);
//		System.out.println("Todays Cash including credit" + dtodayscashSale);
//		BigDecimal bdtodayscashSale = new BigDecimal(dtodayscashSale);
//		bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//		parameters.put("CASH", bdtodayscashSale.toPlainString());
//
//		Double dTodyasCard = 0.0;
//		dTodyasCard = RestCaller.getSumOfSaleReceiptCard(sdate);
//
//		Double previousCardAmount = 0.0;
//		ResponseEntity<Double> previousCardAmountResp = RestCaller.getPreviousAdvanceAmountCard(sdate);
//		if (null != previousCardAmountResp.getBody()) {
//			previousCardAmount = previousCardAmountResp.getBody().doubleValue();
//		}
//
//		dTodyasCard = dTodyasCard - previousCardAmount;
//		BigDecimal bdtodayscardSale = new BigDecimal(dTodyasCard);
//		bdtodayscardSale = bdtodayscardSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//
//		parameters.put("cardsales", bdtodayscardSale.toPlainString());
//
//		// Fill the report
//		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
//		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
//
//		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
//
//		hs.showDocument(pdfToGenerate);
		
		DayEndThermalPrintingFunction posFormatDayEnd = new DayEndThermalPrintingFunction();

		try {

			posFormatDayEnd.setupBottomline("This is a computer generated document");
			posFormatDayEnd.setupLogo();

			posFormatDayEnd.PrintInvoiceThermalPrinter(sdate);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	public static void HardWareDayEndReport(String sdate) throws JRException {

		System.out.println("day---------HardWareDayEndReport--------report");

		String pdfToGenerate = SystemSetting.reportPath + "/hardwaredayend" + SystemSetting.systemBranch + ".pdf";

		ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller.getBankAccountStatement(sdate,
				SystemSetting.getUser().getBranchCode());
		List<AccountBalanceReport> accountBalanceReportList = accountBalanceReport.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		List<BranchMst> branchList = new ArrayList<BranchMst>();
		branchList.add(branchMst);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchList);

		String jrxmlPath = "jrxml/dayendreport.jrxml";// add new filename

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		// ----------------bill series
		ResponseEntity<VoucherNumberDtlReport> voucherNumberDtlReport = RestCaller
				.getInvoiceNumberStatistics(SystemSetting.getUser().getBranchCode(), sdate);

		parameters.put("rdate", sdate);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("BSWSMAXNO", voucherNumberDtlReport.getBody().getWholeSaleEndingBillNumber() + "");
		parameters.put("BSWSMINNO", voucherNumberDtlReport.getBody().getWholeSaleStartingBillNumber() + "");
		parameters.put("BSWSCOUNT", voucherNumberDtlReport.getBody().getWholeSaleBillCount() + "");
		parameters.put("POSMAX", voucherNumberDtlReport.getBody().getPosEndingBillNumber() + "");
		parameters.put("POSMIN", voucherNumberDtlReport.getBody().getPosStartingBillNumber() + "");

		parameters.put("POSCOUNT", voucherNumberDtlReport.getBody().getPosBillCount() + "");
		// ------------------version 6.0 surya

		ResponseEntity<List<DailyReceiptsSummaryReport>> getReceipt = RestCaller.getreceiptSummary(sdate);
		List<DailyReceiptsSummaryReport> receiptReportList = getReceipt.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource01 = new JRBeanCollectionDataSource(receiptReportList);

		// ------------------version 6.1 surya

		ResponseEntity<List<PaymentReports>> paymentSummaryResp = RestCaller.getDayEndPaymentSummary(sdate);
		List<PaymentReports> paymentSummaryReport = paymentSummaryResp.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource02 = new JRBeanCollectionDataSource(paymentSummaryReport);

		ResponseEntity<List<PaymentReports>> pettycashPayment = RestCaller.getPettyCashPayment(sdate);
		List<PaymentReports> pettycashPaymentReport = pettycashPayment.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource03 = new JRBeanCollectionDataSource(
				pettycashPaymentReport);

		// ------------------version 6.1 surya end

		parameters.put("subreportdata2", jrBeanCollectionDataSource01);
		parameters.put("sub_report_data", "jrxml/DayEndReceiptSummary.jasper");
		// ------------------version 6.0 surya end

		// ------------------version 6.1 surya

		parameters.put("subreportdata3", jrBeanCollectionDataSource02);
		parameters.put("sub_report_jasper", "jrxml/PaymentSummaryReport.jasper");

		// sari
		parameters.put("sub_report3", jrBeanCollectionDataSource03);
		parameters.put("petty-cash-payment-sub-report", "jrxml/pettycashPaymentSubreport.jasper");
		// ------------------version 6.1 surya end

		// Petty Cash
		ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
				.getAccountHeadByName(SystemSetting.systemBranch + "-" + "PETTY CASH");
		AccountHeads accountHeads = accountHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> pettyCashAccountBalance = RestCaller.getAccountBalanceReport(sdate,
				sdate, accountHeads.getId());

		List<AccountBalanceReport> pettyCash = pettyCashAccountBalance.getBody();
		if (pettyCash.size() > 0) {
			if (pettyCash.get(0).getClosingBalanceType().equalsIgnoreCase("Cr")) {

				double pcash = pettyCash.get(0).getClosingBalance();
				parameters.put("PettyCash", "-" + pcash + "");
				// dayEndReportStore.setPettyCash("-"+pcash + "");
			} else {
				double pcash = pettyCash.get(0).getClosingBalance();
				// -------------version 4.6
				// dayEndReportStore.setPettyCash(pcash + "");
				// ------------version 4.6 end
				parameters.put("PettyCash", pcash + "");
			}
		} else {
			// ---------version 4.6
			// dayEndReportStore.setPettyCash("0");
			// ------------version 4.6 end
			parameters.put("PettyCash", "0");
		}

		// System Cash------------
		ResponseEntity<AccountHeads> getAccHead = RestCaller
				.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
		ResponseEntity<List<AccountBalanceReport>> accountBalanceCash = RestCaller.getAccountBalanceReport(sdate, sdate,
				getAccHead.getBody().getId());
		Double dclosingBalance = accountBalanceCash.getBody().get(0).getClosingBalance();
		BigDecimal bddclosingBalance = new BigDecimal(dclosingBalance);
		bddclosingBalance = bddclosingBalance.setScale(0, BigDecimal.ROUND_HALF_EVEN);

		parameters.put("ClosinBalance", bddclosingBalance.toPlainString());
		// ---------------cash sale
		Double dtodayscashSale = 0.0, dPreviousCashSaleOrder = 0.0;
		ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeCash = RestCaller.getReceiptModeReportByMode(sdate,
				SystemSetting.getSystemBranch() + "-CASH");
		if (getReceiptAmtByModeCash.getBody().size() > 0) {
			dtodayscashSale = dtodayscashSale + getReceiptAmtByModeCash.getBody().get(0).getAmount();
		}

		ResponseEntity<Double> getCashPA = RestCaller.getPreviousAdvanceAmount(sdate, "CASH");

		if (null != getCashPA.getBody()) {
			try {
				dPreviousCashSaleOrder = getCashPA.getBody().doubleValue();
				BigDecimal bdPreviousCashSaleOrder = new BigDecimal(dPreviousCashSaleOrder);
				bdPreviousCashSaleOrder = bdPreviousCashSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			} catch (Exception e) {
				System.out.println("Error converting to  double value - Provius advance YES BANK");
			}
		}
		dtodayscashSale = dtodayscashSale + dPreviousCashSaleOrder;
		BigDecimal bdtodayscashSale = new BigDecimal(dtodayscashSale);
		bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		parameters.put("CASH", bdtodayscashSale.toPlainString());

		// -----------------credit sale

		Double creditConvertTOCashforToday = RestCaller.getCreditSaleAdjstmnt(SystemSetting.getUser().getBranchCode(),
				sdate);

		ResponseEntity<List<ReceiptModeReport>> getReceiptModeCREDIT = RestCaller.getReceiptModeReportByMode(sdate,
				"CREDIT");
		if (getReceiptModeCREDIT.getBody().size() > 0)

		{
			BigDecimal bcredit = new BigDecimal(getReceiptModeCREDIT.getBody().get(0).getAmount());
			bcredit = bcredit.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			parameters.put("CREDIT", bcredit.toPlainString());
		}

		// -----------------total Sale

		double totalSale = RestCaller.getInvoiceTotalSales(sdate);
		BigDecimal btotalSale = new BigDecimal(totalSale);
		btotalSale = btotalSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		parameters.put("TOTAL SALE", btotalSale.toPlainString());

		// -----------------physical cash

		ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(sdate);
		List<DayEndClosureHdr> dayEndClosureHdr = dayEndReport.getBody();
		double dPhysicalCashP = 0.0;

		if (null != dayEndClosureHdr && dayEndClosureHdr.size() > 0) {
			dPhysicalCashP = dayEndClosureHdr.get(0).getPhysicalCash();
		} else {
			dPhysicalCashP = 0;
		}

		BigDecimal bdPhysicalCash = new BigDecimal(dPhysicalCashP);
		bdPhysicalCash = bdPhysicalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		parameters.put("PHYSICAL CASH", bdPhysicalCash.toPlainString());

		// ---------------receipt summary

		ResponseEntity<DailyReceiptsSummaryReport> receiptSummary = RestCaller.TotalReceiptSummaryByReceiptMode(sdate,
				SystemSetting.getUser().getBranchCode() + "-CASH");

		DailyReceiptsSummaryReport dailyReceiptsSummaryReport = receiptSummary.getBody();

		if (null != dailyReceiptsSummaryReport) {
			if (null != dailyReceiptsSummaryReport.getTotalCash()) {
				parameters.put("RECEIPT SUMMARY", dailyReceiptsSummaryReport.getTotalCash() + "");
			} else {
				parameters.put("RECEIPT SUMMARY", "0");
			}
		} else {
			parameters.put("RECEIPT SUMMARY", "0");

		}

		// ---------------payment summury

		ResponseEntity<DailyPaymentSummaryReport> paymentSummary = RestCaller.TotalPaymentSummaryByPaymentMode(sdate,
				SystemSetting.getUser().getBranchCode() + "-CASH");
		DailyPaymentSummaryReport dailyPaymentSummaryReport = paymentSummary.getBody();

		if (null != dailyPaymentSummaryReport) {
			if (null != dailyPaymentSummaryReport.getTotalCash()) {
				parameters.put("PAYMENT SUMMARY", dailyPaymentSummaryReport.getTotalCash() + "");
			} else {
				parameters.put("PAYMENT SUMMARY", "0");
			}
		} else {
			parameters.put("PAYMENT SUMMARY", "0");

		}

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}
// -------------------version 4.13 end

////-------------------version 4.20 surya
//public static void OtherBranchMstTaxInvoiceReport(String voucherNumber, String date) throws JRException {
//	System.out.println("======OtherBranchMstTaxInvoiceReport===========");
//
//	String pdfToGenerate = SystemSetting.reportPath + "/invoice" + voucherNumber + ".pdf";
//	// Call url
//	ResponseEntity<List<SalesInvoiceReport>> invoiceResponse = RestCaller.getInvoice(voucherNumber, date);
//	List<SalesInvoiceReport> invoice = invoiceResponse.getBody();
//
//	ResponseEntity<List<TaxSummaryMst>> invoiceTaxResponse = RestCaller.getInvoiceTax(voucherNumber, date);
//	List<TaxSummaryMst> invoiceTax = invoiceTaxResponse.getBody();
//
//	ResponseEntity<List<TaxSummaryMst>> taxResponse = RestCaller.getInvoiceTotal(voucherNumber, date);
//	List<TaxSummaryMst> invoiceTaxSum = taxResponse.getBody();
//
//	ResponseEntity<List<SalesInvoiceReport>> customerResponse = RestCaller.getInvoiceCustomerDtl(voucherNumber,
//			date);
//	List<SalesInvoiceReport> custDetails = customerResponse.getBody();
//
//	Double cessAmount = RestCaller.getCessAmountInTaxInvoiceReport(voucherNumber, date);
//
//	Double taxableAmount = RestCaller.getTaxableInvoiceAmount(voucherNumber, date);
//	
//	//----------------version 5.7 surya
//	
//	SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNumber, date);
//	if(null != salesTransHdr)
//	{
//		if(null != salesTransHdr.getCustomerMst())
//		{
//			ResponseEntity<BranchMst> barnchMstResp = RestCaller.getBranchMstById(salesTransHdr.getCustomerMst().getId());
//			BranchMst branchMst = barnchMstResp.getBody();
//			
//			if(invoice.size() > 0)
//			{
//				for(SalesInvoiceReport salesInvoice : invoice)
//				{
//					salesInvoice.setBranchName(branchMst.getBranchName());
//					salesInvoice.setBranchAddress1(branchMst.getBranchAddress1());
//					salesInvoice.setBranchAddress2(branchMst.getBranchAddress2());
//					salesInvoice.setBranchGst(branchMst.getBranchGst());
//					salesInvoice.setBranchTelNo(branchMst.getBranchTelNo());
//					salesInvoice.setBranchWebsite(branchMst.getBranchWebsite());
//					salesInvoice.setBranchEmail(branchMst.getBranchEmail());
//				}
//			}
//		}
//	}
//	
//	//----------------version 5.7 surya end
//
//
//	JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);
//	JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(invoiceTax);
//	JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(invoiceTax);
////	JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(custDetails);
//
//	HashMap<String, Object> parameters = new HashMap();
//
//	String jrxmlPath = "";
//	if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("HARDWARE")) {
//		parameters.put("logoname", "stmarys.png");
//		if (null != invoice) {
//			if (null != invoice.get(0)) {
//				SalesInvoiceReport salesInvoice = invoice.get(0);
//				if (salesInvoice.getDiscount().equalsIgnoreCase(null)) {
//					parameters.put("DiscountAmount", salesInvoice.getInvoiceDiscount());
//					parameters.put("DiscountPercentage", salesInvoice.getDiscountPercent());
//
//					parameters.put("DiscountAmountLabel", "Discount Amount:");
//					parameters.put("DiscountPercentageLabel", "Discount Percentange:");
//				}
//			}
//		}
//
//		for (SalesInvoiceReport salesInvoiceReport : custDetails) {
//			SalesInvoiceReport salesInvoice = new SalesInvoiceReport();
//			if (null != invoice && invoice.size() > 0) {
//				salesInvoice = invoice.get(0);
//			}
//			if (null != salesInvoice) {
//				if (!salesInvoice.getSalesMode().equalsIgnoreCase("B2B")) {
//					jrxmlPath = "jrxml/HWTaxInvoice.jrxml";
//
//					if (null != salesInvoiceReport.getLocalCustomerName()) {
//						salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
//						salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
//						salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
//						salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
//						salesInvoiceReport.setCustomerGst(null);
//						salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
//					}
//				} else {
//					jrxmlPath = "jrxml/HWTaxInvoiceWithGst.jrxml";
//				}
//			}
//		}
//	} else {
//		SalesTransHdr getSalesHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNumber, date);
//
//		if (null == getSalesHdr.getCustomerMst().getCustomerDiscount()) {
//			getSalesHdr.getCustomerMst().setCustomerDiscount(0.0);
//		}
//		if (getSalesHdr.getCustomerMst().getCustomerDiscount() > 0) {
//			jrxmlPath = "jrxml/discountTaxInvoice.jrxml";
//		} else {
//			jrxmlPath = "jrxml/taxinvoicee.jrxml";
//		}
//
//		if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("BAKERY_WITH_BATCH")) {
//			jrxmlPath = "jrxml/TaxinvoiceeWithbatch.jrxml";
//
//		}
//	}
//
//	JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
//
//	String LocalCustomerName = null;
//	String LocalCustomerAddress = null;
//	String LocalCustomerPoneNo = null;
//	String LocalCustomerPlace = null;
//	String LocalCustomerSate = null;
//
////	parameters.put("LocalCustomerName", LocalCustomerName);
////	parameters.put("LocalCustomerAddress", LocalCustomerAddress);
////	parameters.put("LocalCustomerPoneNo", LocalCustomerPoneNo);
////	parameters.put("LocalCustomerPlace", LocalCustomerPlace);
////	parameters.put("LocalCustomerSate", LocalCustomerSate);
//
//	parameters.put("subreportdata2", jrBeanCollectionDataSource3);
//	parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");
//	//------------version 5.8 surya 
//	
//	ResponseEntity<CustomerMst> customerMstResp = RestCaller.getCustomerById(salesTransHdr.getBranchSaleCustomer());
//	CustomerMst customerMst = customerMstResp.getBody();
//
////
//	parameters.put("CustomerAddress", customerMst.getCustomerAddress());
//	parameters.put("CustomerName", customerMst.getCustomerName());
//	parameters.put("PhoneNumber", customerMst.getCustomerContact());
//	parameters.put("CustomerGst", customerMst.getCustomerGst());
//	parameters.put("CustomerPlace", customerMst.getCustomerState());
//	
//	//------------version 5.8 surya end
//
////	System.out.println(custDetails.get(0).getCustomerAddress());
////	System.out.println(custDetails.get(0).getCustomerName());
////	System.out.println(custDetails.get(0).getCustomerPhone());
////	System.out.println(custDetails.get(0).getCustomerGst());
////	System.out.println(custDetails.get(0).getCustomerPlace());
//
//
//	String taxableAmountS = "0.0";
//	if (invoice.size() > 0) {
//		Double amountWithDiscount = invoice.get(0).getAmount() + invoice.get(0).getInvoiceDiscount();
//		parameters.put("InvoiceAmountWithDiscount", amountWithDiscount);
//		BigDecimal amountBig = new BigDecimal(amountWithDiscount);
//		amountBig = amountBig.setScale(0, BigDecimal.ROUND_CEILING);
//
////		BigDecimal taxableAmountInBig = new BigDecimal(taxableAmount);
////		taxableAmountInBig = taxableAmountInBig.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//		String amount = amountBig + "";
//
//		if (null != taxableAmount) {
//			taxableAmountS = taxableAmount + "";
//
//		}
//		String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");
//
//		String taxamount = "0.0";
//		Double taxDouble = 0.0;
//
//		if (null != invoiceTax) {
//			if (invoiceTax.size() > 0) {
//
//				for (TaxSummaryMst tax : invoiceTax) {
//					taxDouble = taxDouble + tax.getTaxAmount();
//				}
//
//			}
//		}
//
//		taxamount = taxDouble + "";
//
//		String taxableAmountInWords = SystemSetting.AmountInWords(taxableAmountS, "Rs", "Ps");
//		String taxamountinwords = SystemSetting.AmountInWords(taxamount, "Rs", "Ps");
//		parameters.put("AmountInWords", amountinwords);
//		parameters.put("taxableAmountInWords", taxableAmountInWords);
//		parameters.put("TaxAmountInWords", taxamountinwords);
//		parameters.put("taxamount", taxamount);
//
//		parameters.put("taxableAmount", taxableAmountS);
//		if (null == cessAmount) {
//			cessAmount = 0.0;
//		}
//
//		parameters.put("CessAmount", cessAmount);
//
//		String gst = "";
//	}
//
//	double cgstPercent = 0.0;
//	double sgstPercent = 0.0;
//	double igstPercent = 0.0;
//
//	if (invoiceTaxSum.size() > 0) {
//
//		cgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfcgstAmount(), 2);
//		sgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfsgstAmount(), 2);
//		igstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfigstAmount(), 2);
//	}
//	parameters.put("cgst%", cgstPercent);
//	parameters.put("sgst%", sgstPercent);
//	parameters.put("igst%", igstPercent);
//	// Fill the report
//	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
//	JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
//
//	HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
//
//	hs.showDocument(pdfToGenerate);
//
//} 

	public static void OtherBranchMstTaxInvoiceReport(String voucherNumber, String date) throws JRException {

		// voucherNumber = "000173";
		// date = "2019-07-21";
		System.out.println("======TaxInvoiceReport===========");

		String pdfToGenerate = SystemSetting.reportPath + "/invoice002" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<SalesInvoiceReport>> invoiceResponse = RestCaller.getOtherBranchInvoice(voucherNumber,
				date);
		List<SalesInvoiceReport> invoice = invoiceResponse.getBody();

		ResponseEntity<List<TaxSummaryMst>> invoiceTaxResponse = RestCaller.getOtherBranchInvoiceTax(voucherNumber,
				date);
		List<TaxSummaryMst> invoiceTax = invoiceTaxResponse.getBody();

		ResponseEntity<List<TaxSummaryMst>> taxResponse = RestCaller.getOtherBranchInvoiceTotal(voucherNumber, date);
		List<TaxSummaryMst> invoiceTaxSum = taxResponse.getBody();

		ResponseEntity<List<SalesInvoiceReport>> customerResponse = RestCaller
				.getOtherBranchInvoiceCustomerDtl(voucherNumber, date);
		List<SalesInvoiceReport> custDetails = customerResponse.getBody();

		Double cessAmount = RestCaller.getCessAmountInOtherBranchTaxInvoiceReport(voucherNumber, date);

		Double taxableAmount = RestCaller.getOtherTaxableInvoiceAmount(voucherNumber, date);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(invoiceTax);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(invoiceTax);
//	JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(custDetails);

		HashMap<String, Object> parameters = new HashMap();

		String jrxmlPath = "";
		if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("HARDWARE")) {
			parameters.put("logoname", "stmarys.png");
			if (null != invoice) {
				if (null != invoice.get(0)) {
					SalesInvoiceReport salesInvoice = invoice.get(0);
					if (salesInvoice.getDiscount().equalsIgnoreCase(null)) {
						parameters.put("DiscountAmount", salesInvoice.getInvoiceDiscount());
						parameters.put("DiscountPercentage", salesInvoice.getDiscountPercent());

						parameters.put("DiscountAmountLabel", "Discount Amount:");
						parameters.put("DiscountPercentageLabel", "Discount Percentange:");
					}
				}
			}

			for (SalesInvoiceReport salesInvoiceReport : custDetails) {
				SalesInvoiceReport salesInvoice = new SalesInvoiceReport();
				if (null != invoice && invoice.size() > 0) {
					salesInvoice = invoice.get(0);
				}
				if (null != salesInvoice) {
					if (!salesInvoice.getSalesMode().equalsIgnoreCase("B2B")) {
						jrxmlPath = "jrxml/HWTaxInvoice.jrxml";

						if (null != salesInvoiceReport.getLocalCustomerName()) {
							salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
							salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
							salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
							salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
							salesInvoiceReport.setCustomerGst(null);
							salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
						}
					} else {
						jrxmlPath = "jrxml/HWTaxInvoiceWithGst.jrxml";
					}
				}
			}
		} else {
			OtherBranchSalesTransHdr getSalesHdr = RestCaller.getOtherBranchSalesTransHdrByVoucherAndDate(voucherNumber,
					date);

			// jrxmlPath = "jrxml/taxinvoicee.jrxml";
			if (null == getSalesHdr.getAccountHeads().getCustomerDiscount()) {
				getSalesHdr.getAccountHeads().setCustomerDiscount(0.0);
			}
			if (getSalesHdr.getAccountHeads().getCustomerDiscount() > 0) {
				jrxmlPath = "jrxml/discountTaxInvoice.jrxml";
			} else {
				jrxmlPath = "jrxml/taxinvoicee.jrxml";
			}

			if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("BAKERY_WITH_BATCH")) {
				jrxmlPath = "jrxml/TaxinvoiceeWithbatch.jrxml";

			}
			for (SalesInvoiceReport salesInvoiceReport : custDetails) {
				if (null != salesInvoiceReport.getLocalCustomerName()) {
					salesInvoiceReport.setCustomerName(salesInvoiceReport.getLocalCustomerName());
					salesInvoiceReport.setCustomerPhone(salesInvoiceReport.getLocalCustomerPhone());
					salesInvoiceReport.setCustomerPlace(salesInvoiceReport.getLocalCustomerPlace());
					salesInvoiceReport.setCustomerState(salesInvoiceReport.getLocalCustomerState());
					salesInvoiceReport.setCustomerGst(null);
					salesInvoiceReport.setCustomerAddress(salesInvoiceReport.getLocalCustomerAddres());
				}
			}
		}

		// String jrxmlPath2 = "jrxml/taxsumary.jrxml";

		// String jrxmlPath3 = "jrxml/taxsumarytable.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		// JasperReport jasperReport3 = JasperCompileManager.compileReport(jrxmlPath3);

		// JasperReport jasperReport2 = JasperCompileManager.compileReport(jrxmlPath2);

//	parameters.put("subreportdata", jrBeanCollectionDataSource2);
//	parameters.put("stax_invoice_tax_sub", "jrxml/tax_invoice_tax_sub.jasper");

		String LocalCustomerName = null;
		String LocalCustomerAddress = null;
		String LocalCustomerPoneNo = null;
		String LocalCustomerPlace = null;
		String LocalCustomerSate = null;
		if (null != invoice) {
			SalesInvoiceReport salesInvoiceReport = new SalesInvoiceReport();
			if (invoice.size() > 0) {
				salesInvoiceReport = invoice.get(0);
				if (null != salesInvoiceReport.getSalesMode()) {
					if (salesInvoiceReport.getSalesMode().equalsIgnoreCase("B2B")) {
						SalesInvoiceReport salesCust = new SalesInvoiceReport();
						if (null != custDetails) {
							salesCust = custDetails.get(0);
						}
						LocalCustomerName = salesCust.getLocalCustomerName();
						LocalCustomerAddress = salesCust.getLocalCustomerAddres();
						LocalCustomerPoneNo = salesCust.getLocalCustomerPhone();
						LocalCustomerPlace = salesCust.getLocalCustomerPlace();
						LocalCustomerSate = salesCust.getLocalCustomerState();

					}
				}
			}
		}

		parameters.put("LocalCustomerName", LocalCustomerName);
		parameters.put("LocalCustomerAddress", LocalCustomerAddress);
		parameters.put("LocalCustomerPoneNo", LocalCustomerPoneNo);
		parameters.put("LocalCustomerPlace", LocalCustomerPlace);
		parameters.put("LocalCustomerSate", LocalCustomerSate);

		parameters.put("subreportdata2", jrBeanCollectionDataSource3);
		parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");

		parameters.put("CustomerAddress", custDetails.get(0).getCustomerAddress());
		parameters.put("CustomerName", custDetails.get(0).getCustomerName());
		parameters.put("PhoneNumber", custDetails.get(0).getCustomerPhone());
		parameters.put("CustomerGst", custDetails.get(0).getCustomerGst());
		parameters.put("CustomerPlace", custDetails.get(0).getCustomerPlace());

		String taxableAmountS = "0.0";
		if (invoice.size() > 0) {
			Double amountWithDiscount = invoice.get(0).getAmount() + invoice.get(0).getInvoiceDiscount();
			parameters.put("InvoiceAmountWithDiscount", amountWithDiscount);
			BigDecimal amountBig = new BigDecimal(amountWithDiscount);
			amountBig = amountBig.setScale(0, BigDecimal.ROUND_CEILING);

//		BigDecimal taxableAmountInBig = new BigDecimal(taxableAmount);
//		taxableAmountInBig = taxableAmountInBig.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			String amount = amountBig + "";

			if (null != taxableAmount) {
				taxableAmountS = taxableAmount + "";

			}
			String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");

			String taxamount = "0.0";
			Double taxDouble = 0.0;

			if (null != invoiceTax) {
				if (invoiceTax.size() > 0) {

					for (TaxSummaryMst tax : invoiceTax) {
						taxDouble = taxDouble + tax.getTaxAmount();
					}

				}
			}

			taxamount = taxDouble + "";

			String taxableAmountInWords = SystemSetting.AmountInWords(taxableAmountS, "Rs", "Ps");
			String taxamountinwords = SystemSetting.AmountInWords(taxamount, "Rs", "Ps");
			parameters.put("AmountInWords", amountinwords);
			parameters.put("taxableAmountInWords", taxableAmountInWords);
			parameters.put("TaxAmountInWords", taxamountinwords);
			parameters.put("taxamount", taxamount);

			parameters.put("taxableAmount", taxableAmountS);
			if (null == cessAmount) {
				cessAmount = 0.0;
			}

			parameters.put("CessAmount", cessAmount);

			String gst = "";
		}
//	for(TaxSummaryMst taxSummaryMst : invoiceTax)
//	{
//		gst = gst + taxSummaryMst.getTaxPercentage()+"\t"+taxSummaryMst.getTaxAmount()+"\n";
////	}
//	Double AmtAftrDiscount = 0.0;
//	if(invoice.size() >= 0)
//	{
//	if(null != invoice.get(0).getInvoiceDiscount())
//	{
//		 AmtAftrDiscount = invoice.get(0).getAmount()+invoice.get(0).getInvoiceDiscount();
//		
//	}else {
//		AmtAftrDiscount = invoice.get(0).getAmount();
//	}
//	} 
//	parameters.put("AmtAftrDiscount",AmtAftrDiscount);

		double cgstPercent = 0.0;
		double sgstPercent = 0.0;
		double igstPercent = 0.0;

		if (invoiceTaxSum.size() > 0) {

			cgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfcgstAmount(), 2);
			sgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfsgstAmount(), 2);
			igstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfigstAmount(), 2);
		}
		parameters.put("cgst%", cgstPercent);
		parameters.put("sgst%", sgstPercent);
		parameters.put("igst%", igstPercent);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemLocationReportbyItemId(String itemId) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/ItemLocationReport" + itemId + ".pdf";

		ResponseEntity<List<ItemLocationReport>> itemLocationReportList = RestCaller
				.getItemLocationReportItemId(itemId);

		List<ItemLocationReport> ItemLocationReport = itemLocationReportList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ItemLocationReport);

		String jrxmlPath = "jrxml/ItemLocationReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemLocationReportbyFloor(String floor) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/ItemLocationReportByFloor" + floor + ".pdf";

		ResponseEntity<List<ItemLocationReport>> itemLocationReportList = RestCaller.getItemLocationReportFloor(floor);

		List<ItemLocationReport> ItemLocationReport = itemLocationReportList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ItemLocationReport);

		String jrxmlPath = "jrxml/ItemLocationReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemLocationReportbyFloorAndShelf(String floor, String shelf) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/ItemLocationReportByFloor" + floor + shelf + ".pdf";

		ResponseEntity<List<ItemLocationReport>> itemLocationReportList = RestCaller
				.getItemLocationReportFloorAndShelf(floor, shelf);

		List<ItemLocationReport> ItemLocationReport = itemLocationReportList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ItemLocationReport);

		String jrxmlPath = "jrxml/ItemLocationReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ItemLocationReportbyFloorAndShelfAndRack(String floor, String shelf, String rack)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/ItemLocationReportByFloorAndRack" + floor + shelf + rack
				+ ".pdf";

		ResponseEntity<List<ItemLocationReport>> itemLocationReportList = RestCaller
				.getItemLocationReportFloorAndShelfAndRack(floor, shelf, rack);

		List<ItemLocationReport> ItemLocationReport = itemLocationReportList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ItemLocationReport);

		String jrxmlPath = "jrxml/ItemLocationReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void fastMovingItems(String fdate, String tdate, String windowName) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/fastMoving" + fdate + ".pdf";

		ResponseEntity<List<StockReport>> fastMoving = RestCaller.getFastMovingItems(fdate, tdate, windowName);

		List<StockReport> fastMovingList = fastMoving.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("fdate", fdate);
		parameters.put("tdate", tdate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(fastMovingList);

		String jrxmlPath = "jrxml/FastMovingItems.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void OtherBranchMstStockTrasferReport(OtherBranchSalesTransHdr otherbranchSalesTransHdr,
			String voucherNumber, String strDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stkinv" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<StockTransferOutReport>> stocktransferResponse = RestCaller
				.getOtherBranchStockTransfer(voucherNumber, strDate);

		List<StockTransferOutReport> stockTransferInvoiceResponse = stocktransferResponse.getBody();

		HashMap parameters = new HashMap();

		ResponseEntity<BranchMst> branchMst = RestCaller
				.getBranchMstById(otherbranchSalesTransHdr.getBranchSaleCustomer());
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBody().getBranchName());
		parameters.put("Address1", branchMst.getBody().getBranchAddress1());
		parameters.put("Address2", branchMst.getBody().getBranchAddress2());
		parameters.put("Phone", branchMst.getBody().getBranchTelNo());
		parameters.put("State", branchMst.getBody().getBranchState());
		parameters.put("GST", branchMst.getBody().getBranchGst());

		ResponseEntity<BranchMst> transferToBranchMstResp = RestCaller
				.getBranchMstById(otherbranchSalesTransHdr.getAccountHeads().getId());
		BranchMst transferToBranchMst = transferToBranchMstResp.getBody();

		parameters.put("toBranchName", transferToBranchMst.getBranchName());
		parameters.put("toBranchAddress1", transferToBranchMst.getBranchAddress1());
		parameters.put("toBranchAddress2", transferToBranchMst.getBranchAddress2());
		parameters.put("toBranchTelNo", transferToBranchMst.getBranchTelNo());
		parameters.put("toBranchState", transferToBranchMst.getBranchState());
		parameters.put("toBranchGst", transferToBranchMst.getBranchGst());

		Double totalAmount = 0.0;

		for (StockTransferOutReport stcktrnsfr : stockTransferInvoiceResponse) {
			totalAmount = totalAmount + stcktrnsfr.getAmount();
		}

		String totalAmountInWords = SystemSetting.AmountInWords(totalAmount.toString(), "Rs", "Ps");
		parameters.put("totalAmountInWords", totalAmountInWords);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				stockTransferInvoiceResponse);

		String jrxmlPath = "jrxml/stockinhc.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DayEndReportWithoutCashBalance(String sdate) throws JRException {

		System.out.println("day---------HardWareDayEndReport--------report");

		String pdfToGenerate = SystemSetting.reportPath + "/dayendwithoutclosingbalance" + SystemSetting.systemBranch
				+ ".pdf";

		ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller.getBankAccountStatement(sdate,
				SystemSetting.getUser().getBranchCode());
		List<AccountBalanceReport> accountBalanceReportList = accountBalanceReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				accountBalanceReportList);

		String jrxmlPath = "jrxml/DayEndWithoutClosingBalance.jrxml";// add new filename

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		// ----------------bill series
		ResponseEntity<VoucherNumberDtlReport> voucherNumberDtlReport = RestCaller
				.getInvoiceNumberStatistics(SystemSetting.getUser().getBranchCode(), sdate);

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("rdate", sdate);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("BSWSMAXNO", voucherNumberDtlReport.getBody().getWholeSaleEndingBillNumber() + "");
		parameters.put("BSWSMINNO", voucherNumberDtlReport.getBody().getWholeSaleStartingBillNumber() + "");
		parameters.put("BSWSCOUNT", voucherNumberDtlReport.getBody().getWholeSaleBillCount() + "");
		parameters.put("POSMAX", voucherNumberDtlReport.getBody().getPosEndingBillNumber() + "");
		parameters.put("POSMIN", voucherNumberDtlReport.getBody().getPosStartingBillNumber() + "");

		parameters.put("POSCOUNT", voucherNumberDtlReport.getBody().getPosBillCount() + "");
		// ------------------version 6.0 surya

		ResponseEntity<List<DailyReceiptsSummaryReport>> getReceipt = RestCaller.getreceiptSummary(sdate);
		List<DailyReceiptsSummaryReport> receiptReportList = getReceipt.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource01 = new JRBeanCollectionDataSource(receiptReportList);

		// ------------------version 6.1 surya

		ResponseEntity<List<PaymentReports>> paymentSummaryResp = RestCaller.getDayEndPaymentSummary(sdate);
		List<PaymentReports> paymentSummaryReport = paymentSummaryResp.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource02 = new JRBeanCollectionDataSource(paymentSummaryReport);

		// ------------------version 6.1 surya end

//	parameters.put("subreportdata2", jrBeanCollectionDataSource01);
//	parameters.put("sub_report_data", "jrxml/DayEndReceiptSummary.jasper");
//	//------------------version 6.0 surya end
//
//	//------------------version 6.1 surya 
//
//	parameters.put("subreportdata3", jrBeanCollectionDataSource02);
//	parameters.put("sub_report_jasper", "jrxml/PaymentSummaryReport.jasper");
//	//------------------version 6.1 surya end

//
//	//Petty Cash
//	ResponseEntity<AccountHeads> accountHeadsResp = RestCaller
//			.getAccountHeadByName(SystemSetting.systemBranch + "-" + "PETTY CASH");
//	AccountHeads accountHeads = accountHeadsResp.getBody();
//
//	ResponseEntity<List<AccountBalanceReport>> pettyCashAccountBalance = RestCaller.getAccountBalanceReport(sdate,
//			sdate, accountHeads.getId());
//
//	List<AccountBalanceReport> pettyCash = pettyCashAccountBalance.getBody();
//	if (pettyCash.size() > 0) {
//		if(pettyCash.get(0).getClosingBalanceType().equalsIgnoreCase("Cr"))
//		{
//		
//		double pcash = pettyCash.get(0).getClosingBalance();
//		parameters.put("PettyCash", "-"+pcash + "");
//		//dayEndReportStore.setPettyCash("-"+pcash + "");
//		}
//		else
//		{
//			double pcash = pettyCash.get(0).getClosingBalance();
//			//-------------version 4.6
//			//dayEndReportStore.setPettyCash(pcash + "");
//			//------------version 4.6 end
//			parameters.put("PettyCash", pcash + "");
//		}
//	} else {
//		//---------version 4.6 
//		//dayEndReportStore.setPettyCash("0");
//		//------------version 4.6 end
//		parameters.put("PettyCash", "0");
//	}
//	

//	//System Cash------------
//	ResponseEntity<AccountHeads> getAccHead = RestCaller.getAccountHeadByName(SystemSetting.getSystemBranch() + "-CASH");
//	ResponseEntity<List<AccountBalanceReport>> accountBalanceCash = RestCaller.getAccountBalanceReport(sdate,
//			sdate, getAccHead.getBody().getId());
//	Double dclosingBalance =accountBalanceCash.getBody().get(0).getClosingBalance();
//	BigDecimal bddclosingBalance = new BigDecimal(dclosingBalance);
//	bddclosingBalance = bddclosingBalance.setScale(0, BigDecimal.ROUND_HALF_EVEN);
//
//	parameters.put("ClosinBalance", bddclosingBalance.toPlainString());
		// ---------------cash sale
		Double dtodayscashSale = 0.0, dPreviousCashSaleOrder = 0.0;
		ResponseEntity<List<ReceiptModeReport>> getReceiptAmtByModeCash = RestCaller.getReceiptModeReportByMode(sdate,
				SystemSetting.getSystemBranch() + "-CASH");
		if (getReceiptAmtByModeCash.getBody().size() > 0) {
			dtodayscashSale = dtodayscashSale + getReceiptAmtByModeCash.getBody().get(0).getAmount();
		}

		ResponseEntity<Double> getCashPA = RestCaller.getPreviousAdvanceAmount(sdate, "CASH");

		if (null != getCashPA.getBody()) {
			try {
				dPreviousCashSaleOrder = getCashPA.getBody().doubleValue();
				BigDecimal bdPreviousCashSaleOrder = new BigDecimal(dPreviousCashSaleOrder);
				bdPreviousCashSaleOrder = bdPreviousCashSaleOrder.setScale(0, BigDecimal.ROUND_HALF_EVEN);

			} catch (Exception e) {
				System.out.println("Error converting to  double value - Provius advance YES BANK");
			}
		}
		dtodayscashSale = dtodayscashSale + dPreviousCashSaleOrder;
		BigDecimal bdtodayscashSale = new BigDecimal(dtodayscashSale);
		bdtodayscashSale = bdtodayscashSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		parameters.put("CASH", bdtodayscashSale.toPlainString());

		// -----------------credit sale

		Double creditConvertTOCashforToday = RestCaller.getCreditSaleAdjstmnt(SystemSetting.getUser().getBranchCode(),
				sdate);

		ResponseEntity<List<ReceiptModeReport>> getReceiptModeCREDIT = RestCaller.getReceiptModeReportByMode(sdate,
				"CREDIT");
		if (getReceiptModeCREDIT.getBody().size() > 0)

		{
			BigDecimal bcredit = new BigDecimal(getReceiptModeCREDIT.getBody().get(0).getAmount());
			bcredit = bcredit.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			parameters.put("CREDIT", bcredit.toPlainString());
		}

		// -----------------total Sale

		double totalSale = RestCaller.getInvoiceTotalSales(sdate);
		BigDecimal btotalSale = new BigDecimal(totalSale);
		btotalSale = btotalSale.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		parameters.put("TOTAL SALE", btotalSale.toPlainString());

		// -----------------physical cash

		ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(sdate);
		List<DayEndClosureHdr> dayEndClosureHdr = dayEndReport.getBody();
		double dPhysicalCashP = 0.0;

		if (null != dayEndClosureHdr && dayEndClosureHdr.size() > 0) {
			dPhysicalCashP = dayEndClosureHdr.get(0).getPhysicalCash();
		} else {
			dPhysicalCashP = 0;
		}

		BigDecimal bdPhysicalCash = new BigDecimal(dPhysicalCashP);
		bdPhysicalCash = bdPhysicalCash.setScale(0, BigDecimal.ROUND_HALF_EVEN);
		parameters.put("PHYSICAL CASH", bdPhysicalCash.toPlainString());

		// ---------------receipt summary

		ResponseEntity<DailyReceiptsSummaryReport> receiptSummary = RestCaller.TotalReceiptSummaryByReceiptMode(sdate,
				SystemSetting.getUser().getBranchCode() + "-CASH");

		DailyReceiptsSummaryReport dailyReceiptsSummaryReport = receiptSummary.getBody();

		if (null != dailyReceiptsSummaryReport) {
			if (null != dailyReceiptsSummaryReport.getTotalCash()) {
				parameters.put("RECEIPT SUMMARY", dailyReceiptsSummaryReport.getTotalCash() + "");
			} else {
				parameters.put("RECEIPT SUMMARY", "0");
			}
		} else {
			parameters.put("RECEIPT SUMMARY", "0");

		}

		// ---------------payment summury

		ResponseEntity<DailyPaymentSummaryReport> paymentSummary = RestCaller.TotalPaymentSummaryByPaymentMode(sdate,
				SystemSetting.getUser().getBranchCode() + "-CASH");
		DailyPaymentSummaryReport dailyPaymentSummaryReport = paymentSummary.getBody();

		if (null != dailyPaymentSummaryReport) {
			if (null != dailyPaymentSummaryReport.getTotalCash()) {
				parameters.put("PAYMENT SUMMARY", dailyPaymentSummaryReport.getTotalCash() + "");
			} else {
				parameters.put("PAYMENT SUMMARY", "0");
			}
		} else {
			parameters.put("PAYMENT SUMMARY", "0");

		}

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void sample(String stratDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/servicein.pdf";

		// Call url
		ResponseEntity<List<PaymentReports>> pettycashPayment = RestCaller.getPettyCashPayment(stratDate);
		List<PaymentReports> pettycashPaymentReport = pettycashPayment.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource03 = new JRBeanCollectionDataSource(
				pettycashPaymentReport);

		String jrxmlPath = "jrxml/pettycashPaymentSubreport.jrxml";

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource03);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void printMemberDtl(String branchcode) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/MemberMstLIst" + ".pdf";

		// Call url

		ResponseEntity<List<MemberMst>> memberMstResponse = RestCaller.getMemberMst();
		List<MemberMst> categoryWiseDtlReport = memberMstResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(categoryWiseDtlReport);

		String jrxmlPath = "jrxml/MemberMstList.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void printJournalReportDtl(String startDate, String endDate, String branchCode) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/journalreport" + startDate + ".pdf";

		// Call url

		ResponseEntity<List<JournalDtl>> journalResponse = RestCaller.getJournalReportPrint(startDate, endDate,
				branchCode);
		List<JournalDtl> journalDtlReport = journalResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(journalDtlReport);

		String jrxmlPath = "jrxml/journalReports.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(branchCode);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("FromDate", startDate);
		parameters.put("ToDate", endDate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void SupplierwisePurchaseReport(String startDate, String endDate, String cat, String supplier)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/supplierwisepurchase" + supplier + startDate + ".pdf";

		// Call url
		ResponseEntity<List<PurchaseReport>> purchasereportList;
		if (cat.equalsIgnoreCase(null) || cat.equalsIgnoreCase("")) {
			purchasereportList = RestCaller.getSupplierWisePurchaseReportWithDate(startDate, endDate, supplier);

		} else {
			purchasereportList = RestCaller.getSupplierWisePurchaseReport(cat, startDate, endDate, supplier);
		}
		List<PurchaseReport> purchasereport = purchasereportList.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(purchasereport);

		String jrxmlPath = "jrxml/SupplierwisePurchaseReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("FromDate", startDate);
		parameters.put("ToDate", endDate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void InhouseConsumptionReport(String startDate, String endDate, String cat, String department)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/inhouseConsumption" + department + startDate + ".pdf";

		// Call url

		ResponseEntity<List<ConsumptionReport>> departmentReport = RestCaller.getdepartmentWiseConsumption(startDate,
				endDate, cat, department);

		List<ConsumptionReport> departmentList = departmentReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/InhouseConsumptionReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("FromDate", startDate);
		parameters.put("ToDate", endDate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}
	

	public static void PharmacyCustomerwiseSales(String startDate, String endDate, String cat, String customer)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/custwisesales" + customer + startDate + ".pdf";

		// Call url
		ResponseEntity<List<SalesInvoiceReport>> departmentReport;

		if (cat.equalsIgnoreCase(null) || cat.equalsIgnoreCase("")) {
			departmentReport = RestCaller.getcustomerWiseSalesByDate(startDate, endDate, customer);

		} else {
			departmentReport = RestCaller.getcustomerWiseSalesbyCategoryList(startDate, endDate, customer, cat);
		}
		List<SalesInvoiceReport> departmentList = departmentReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/PharmacyCustomerwiseSalesReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("FromDate", startDate);
		parameters.put("ToDate", endDate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// -----------------anandu 29-04-2021----------------------

	public static void Pharmacyitemmovement(String fDate, String tDate, String branchCode, String category, String item,
			String batch) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/PharmacyItemTransfer" + item + fDate + ".pdf";

		// Call url
		ResponseEntity<List<PharmacyItemMovementAnalisysReport>> departmentReport = RestCaller
				.getPharmacyItemMovement(fDate, tDate, branchCode, category, item, batch);

//		if (cat.equalsIgnoreCase(null) || cat.equalsIgnoreCase("")) {
//			departmentReport = RestCaller.getPharmacyItemMovement(startDate, endDate, customer);
//
//		} else {
//			departmentReport = RestCaller.getcustomerWiseSalesbyCategoryList(startDate, endDate, customer, cat);
//		}
		List<PharmacyItemMovementAnalisysReport> departmentList = departmentReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/PharmacyItemTransfer.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());
		parameters.put("BranchName", branchMst.getBranchName());
		
		parameters.put("fromDate", fDate);
		parameters.put("toDate", tDate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// ----------------AMDC reports 30-04-2021------------------------

	public static void PurchaseReturnSummary(String fDate, String tDate, String branchCode, String voucher,
			String itemgroup) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/PharmacyItemTransfer" + voucher + fDate + ".pdf";

		// Call url
		// ResponseEntity<List<PharmacyItemMovementAnalisysReport>>
		// departmentReport=RestCaller.getPharmacyItemMovement(fDate, tDate, branchCode,
		// voucher, itemgroup);

//		if (cat.equalsIgnoreCase(null) || cat.equalsIgnoreCase("")) {
//			departmentReport = RestCaller.getPharmacyItemMovement(startDate, endDate, customer);
//
//		} else {
//			departmentReport = RestCaller.getcustomerWiseSalesbyCategoryList(startDate, endDate, customer, cat);
//		}
		// List<PharmacyItemMovementAnalisysReport> departmentList =
		// departmentReport.getBody();

		// JRBeanCollectionDataSource jrBeanCollectionDataSource = new
		// JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/PharmacyItemTransfer.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());
		parameters.put("BranchName", branchMst.getBranchName());

		// Fill the report
//		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
//		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DailySalesDtlBetweenDate(String branch, String fdate, String tdate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/SalesReport" + fdate + tdate + ".pdf";

		ResponseEntity<List<DailySalesReportDtl>> saleslList = RestCaller.getSalesDtlDaily(branch, fdate, tdate);

		List<DailySalesReportDtl> saleslListResponse = saleslList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", fdate);
		parameters.put("Todate", tdate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleslListResponse);

		String jrxmlPath = "jrxml/SalesDtlBetweenDate.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void receiptModeWiseDtlReport(String branch, String fdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/ReceiptModeDetailsReport" + fdate + fdate + ".pdf";

		System.out.println(pdfToGenerate);
		ResponseEntity<List<ReceiptModeWiseReport>> receiptModeWiseReportList = RestCaller
				.getReceiptModeWiseReport(branch, fdate);

		List<ReceiptModeWiseReport> receiptModeWiseReportResponse = receiptModeWiseReportList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("voucherDate", fdate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				receiptModeWiseReportResponse);

		String jrxmlPath = "jrxml/ReceiptModeDetailsReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void receiptModeWiseSummaryPrint(String branch, String fdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/ReceiptModeSummary" + fdate + fdate + ".pdf";
		System.out.println(pdfToGenerate);
		ResponseEntity<List<ReceiptModeWiseReport>> receiptModeWiseReportList = RestCaller
				.getReceiptModeWiseSummaryReport(branch, fdate);

		List<ReceiptModeWiseReport> receiptModeWiseReportResponse = receiptModeWiseReportList.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("voucherDate", fdate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				receiptModeWiseReportResponse);

		String jrxmlPath = "jrxml/ReceiptModeSummary.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void InsuranceWiseSalesSummary(String fdate, String tdate, String insurance) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/insuranceSales" + fdate + tdate + ".pdf";
		ResponseEntity<List<InsuranceCompanySalesSummaryReport>> insurancelist;
		if (insurance.equalsIgnoreCase(null) || insurance.equalsIgnoreCase("")) {
			insurancelist = RestCaller.getInsuranceSalesSummary(fdate, tdate);
		} else {
			insurancelist = RestCaller.getInsuranceSalesSummaryWithInsuranceComp(fdate, tdate, insurance);

		}
		List<InsuranceCompanySalesSummaryReport> insurancelistResponse = insurancelist.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());
		parameters.put("fromDate", fdate);
		parameters.put("Todate", tdate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(insurancelistResponse);

		String jrxmlPath = "jrxml/InsuranceCompSalesSummary.jrxml";
	

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void StockTransferOutDetailReport(String fdate, String tdate, String tobranch, String cat)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/stockTransOutDtl" + cat + fdate + ".pdf";

		// Call url

		ResponseEntity<List<StockTransferOutReport>> stkreport = RestCaller.getStockTransferOutReportBetweenDate(fdate,
				tdate, tobranch, cat);

		List<StockTransferOutReport> reportlist = stkreport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(reportlist);

		String jrxmlPath = "jrxml/StockTransferOutDtleport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("FromDate", fdate);
		parameters.put("ToDate", tdate);
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void StockTransferInDetailReport(String strDate, String strDate1) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stocktransferindetailreport" + strDate + "to" + strDate1
				+ ".pdf";

		ResponseEntity<List<StockTransferInReport>> stockTransferIndetailReportResp = RestCaller
				.StockTransferInDetailReport(SystemSetting.getUser().getBranchCode(), strDate, strDate1);

		List<StockTransferInReport> saleslListResponse = stockTransferIndetailReportResp.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", strDate);
		parameters.put("Todate", strDate1);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleslListResponse);

		String jrxmlPath = "jrxml/StockTransferInDetailReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void StockTransferInDetailReport(String branchCode, String fromBranchCode, String categoryIds,
			String sdate, String edate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stocktransferindetailreport" + sdate + "to" + edate
				+ ".pdf";

		ResponseEntity<List<StockTransferInReport>> stockTransferInDetailReportResp = RestCaller
				.StockTransferInDetailReportByFromBranch(SystemSetting.getUser().getBranchCode(), fromBranchCode,
						categoryIds, sdate, edate);

		List<StockTransferInReport> saleslListResponse = stockTransferInDetailReportResp.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", sdate);
		parameters.put("Todate", edate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleslListResponse);

		String jrxmlPath = "jrxml/StockTransferInDetailReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void StockTransferInDetailReportByFromBranch(String branchCode, String fromBranchCode, String sdate,
			String edate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stocktransferindetailreport" + sdate + "to" + edate
				+ ".pdf";

		ResponseEntity<List<StockTransferInReport>> stockTransferInDetailReportResp = RestCaller
				.StockTransferInDetailReportByFromBranch(SystemSetting.getUser().getBranchCode(), fromBranchCode, sdate,
						edate);

		List<StockTransferInReport> saleslListResponse = stockTransferInDetailReportResp.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", sdate);
		parameters.put("Todate", edate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleslListResponse);

		String jrxmlPath = "jrxml/StockTransferInDetailReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void StockTransferInDetailReportByCategory(String branchCode, String categoryIds, String sdate,
			String edate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stocktransferindetailreport" + sdate + "to" + edate
				+ ".pdf";

		ResponseEntity<List<StockTransferInReport>> stockTransferInDetailReportResp = RestCaller
				.StockTransferInDetailReportByCategory(SystemSetting.getUser().getBranchCode(), categoryIds, sdate,
						edate);

		List<StockTransferInReport> saleslListResponse = stockTransferInDetailReportResp.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromDate", sdate);
		parameters.put("Todate", edate);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleslListResponse);

		String jrxmlPath = "jrxml/StockTransferInDetailReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void IncomeAndExpenceReport(String sdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stocktransferindetailreport" + sdate + ".pdf";

//	BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());
//	parameters.put("BranchName", branchMst.getBranchName());
//	parameters.put("Address1", branchMst.getBranchAddress1());
//	parameters.put("Phone", branchMst.getBranchTelNo());
//	parameters.put("fromDate", fdate);
//	parameters.put("Todate", tdate);
//	// Fill the report
//	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
//	JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		ResponseEntity<AccountHeads> incomeAccountHeadsResp = RestCaller.getAccountHeadByName("INCOME");
		AccountHeads incomeAccountHeads = incomeAccountHeadsResp.getBody();

		ResponseEntity<AccountHeads> expenceAccountHeadsResp = RestCaller.getAccountHeadByName("EXPENSE");
		AccountHeads expenceAccountHeads = expenceAccountHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> incomeAccountBalanceReportResp = RestCaller
				.getAccountBalanceReportByIncomeParentId(incomeAccountHeads.getId(), sdate);
		List<AccountBalanceReport> incomeAccountBalanceReport = incomeAccountBalanceReportResp.getBody();

		Double totalIncome = 0.0;
		for (AccountBalanceReport balance : incomeAccountBalanceReport) {
			totalIncome = totalIncome + balance.getClosingBalance();
		}

		ResponseEntity<List<AccountBalanceReport>> expenceAccountBalanceReportResp = RestCaller
				.getAccountBalanceReportByParentId(expenceAccountHeads.getId(), sdate);
		List<AccountBalanceReport> expenceAccountBalanceReport = expenceAccountBalanceReportResp.getBody();

		Double totalExpence = 0.0;
		for (AccountBalanceReport balance : expenceAccountBalanceReport) {
			totalExpence = totalExpence + balance.getClosingBalance();
		}

		ResponseEntity<AccountHeads> cashInHandAccountResp = RestCaller
				.getAccountHeadByName(SystemSetting.getUser().getBranchCode() + "-CASH");
		AccountHeads cashInHandAccount = cashInHandAccountResp.getBody();

		ResponseEntity<OpeningBalanceMst> openinBalanceBYAccId = RestCaller
				.getOpeningBalanceByAccId(cashInHandAccount.getId());
		OpeningBalanceMst openinBalanceOfCashInHand = openinBalanceBYAccId.getBody();

		Double cashInHandOpeningBalance = 0.0;
		if (null != openinBalanceOfCashInHand) {
			cashInHandOpeningBalance = openinBalanceOfCashInHand.getCreditAmount()
					- openinBalanceOfCashInHand.getDebitAmount();
		}
		ResponseEntity<OpeningBalanceMst> bankAccountOpeningBalanceResp = RestCaller
				.getBankaccountOppeningBalance(cashInHandAccount.getId());
		OpeningBalanceMst bankAccountOpeningBalance = bankAccountOpeningBalanceResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> closingCashInHandAccountResp = RestCaller
				.getTrialBalanceById(cashInHandAccount.getId(), sdate);
		List<AccountBalanceReport> closingCashInHandAccount = closingCashInHandAccountResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> closingBankAccountResp = RestCaller
				.getTrialBalanceById(expenceAccountHeads.getId(), sdate);
		List<AccountBalanceReport> closingBankAccount = closingBankAccountResp.getBody();

		HashMap parameters = new HashMap();
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		List<BranchMst> branchList = new ArrayList<BranchMst>();
		branchList.add(branchMst);

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("phno", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("Date", "DATE :" + sdate);

		parameters.put("CashInHandOpeningBalance", cashInHandOpeningBalance);
		Double bankOpeningBalance = 0.0;
		if (null != bankAccountOpeningBalance) {
			bankOpeningBalance = bankAccountOpeningBalance.getCreditAmount();
		}
		parameters.put("BankAccountOpeningBalance", bankOpeningBalance);

		Double cashClosingBalance = 0.0;
		if (closingCashInHandAccount.size() > 0) {
			cashClosingBalance = closingCashInHandAccount.get(0).getClosingBalance();
		}

		parameters.put("CashInHandClosingBalance", cashClosingBalance);
		Double bankClosingBalance = 0.0;
		if (closingBankAccount.size() != 0) {
			if (null != closingBankAccount.get(0)) {
				if (null != closingBankAccount.get(0).getClosingBalance()) {
					bankClosingBalance = closingBankAccount.get(0).getClosingBalance();

				}

			}
		}
		parameters.put("BankAccountClosingBalance", bankClosingBalance);

		totalIncome = totalIncome + cashInHandOpeningBalance + bankOpeningBalance;
		parameters.put("totalIncome", totalIncome);

		totalExpence = totalExpence + cashClosingBalance + bankClosingBalance;
		parameters.put("totalExpence", totalExpence);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource1 = new JRBeanCollectionDataSource(
				incomeAccountBalanceReport);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(
				expenceAccountBalanceReport);

		String jrxmlPath = "jrxml/IncomeAndExpenceReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		parameters.put("incomesubreportdata", jrBeanCollectionDataSource1);
		parameters.put("incomereport", "jrxml/IncomeAndExpenceSubReport.jasper");

		parameters.put("expencesubreportdata", jrBeanCollectionDataSource2);
		parameters.put("expencereport", "jrxml/IncomeAndExpenceSubReport.jasper");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void AccountBalanceReportSingleMode(String startDate, String endDate, String accountId)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/accountbalance" + accountId + startDate + ".pdf";

		// Call url

		ResponseEntity<List<AccountBalanceReport>> accountBalanceReport = RestCaller
				.getAccountBalanceReportMSingleMode(startDate, endDate, accountId);

		List<AccountBalanceReport> accountBalanceReportList = accountBalanceReport.getBody();

		for (AccountBalanceReport accountBalance : accountBalanceReportList) {
			Date date = SystemSetting.StringToUtilDate(startDate, "yyyy-MM-dd");
			accountBalance.setStartDate(date);

			Date edate = SystemSetting.StringToUtilDate(endDate, "yyyy-MM-dd");
			accountBalance.setEndDate(edate);
		}
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				accountBalanceReportList);
		ResponseEntity<AccountHeads> getAccountHeadsById = RestCaller.getAccountHeadsById(accountId);
//		ResponseEntity<Supplier> supplierById = RestCaller.getSupplier(accountId);
		String jrxmlPath = null;
		if (null != getAccountHeadsById.getBody() ) {
			jrxmlPath = "jrxml/AccountBalanceCustomer.jrxml";
		} else {
			jrxmlPath = "jrxml/AccountBalance.jrxml";
		}
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();
		parameters.put("Start Date", startDate);
		parameters.put("End Date", endDate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", "Ph." + branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST: " + branchMst.getBranchGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void StockTransferOutSummaryReport(String strfDate, String strtDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/StockOutSummary" + strfDate + ".pdf";

		// Call url

		ResponseEntity<List<StockTransferOutReport>> stocktransferResponse = RestCaller
				.stockOutSummaryBetweenDate(strfDate, strtDate);

		List<StockTransferOutReport> stockTransferInvoiceResponse = stocktransferResponse.getBody();

		HashMap parameters = new HashMap();
		parameters.put("FromDate", strfDate);
		parameters.put("ToDate", strtDate);
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				stockTransferInvoiceResponse);

		String jrxmlPath = "jrxml/StockTransferOutSummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void DailyMobileStockReport(String date) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/stockreport.pdf";

		// Call urld

		ResponseEntity<List<StockReport>> MobileStockReport = RestCaller
				.getDailyMobilestockReport(SystemSetting.getSystemBranch(), date);

		List<StockReport> DailyStockReport = MobileStockReport.getBody();
		for (StockReport stockReport : DailyStockReport) {
			stockReport.setCompanyName(SystemSetting.getUser().getCompanyMst().getCompanyName());

		}

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("rdate", date);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(DailyStockReport);

		String jrxmlPath = "jrxml/stockreport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void UserWiseSalesReport(String strDate, String endDate, UserMst userMst, String branchCode)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stockreport.pdf";

		// Call urld

		ResponseEntity<List<DailySalesReportDtl>> userWiseSalesSummaryResp = RestCaller
				.getUserWiseSalesReport(userMst.getId(), branchCode, strDate, endDate);

		List<DailySalesReportDtl> userWiseSalesSummary = userWiseSalesSummaryResp.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());

		parameters.put("strDate", strDate);
		parameters.put("endDate", endDate);
		parameters.put("userName", userMst.getUserName());

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(userWiseSalesSummary);

		String jrxmlPath = "jrxml/UserWiseSalesReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void storeChangeReport(StoreChangeMst storeChangeMst) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + storeChangeMst.getToStore() + "-storechangereport.pdf";

		// Call urld
		ResponseEntity<List<StoreChangeDtl>> storeChangeresp = RestCaller
				.findStoreChangeByHdrId(storeChangeMst.getId());

		List<StoreChangeDtl> StoreChangeDtlReport = storeChangeresp.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("todate", SystemSetting.UtilDateToString(storeChangeMst.getVoucherDate(), "yyyy-MM-dd"));
		parameters.put("fstore", storeChangeMst.getStore());
		parameters.put("toStore", storeChangeMst.getToStore());

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(StoreChangeDtlReport);

		String jrxmlPath = "jrxml/StoreChangeReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void storeWiseStockReport(String store) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + store + "-storeWiseStockreport.pdf";

		// Call urld
		ResponseEntity<List<StockReport>> stockReportResp = RestCaller.getStoreWiseStockReport(store);
		List<StockReport> StoreChangeDtlReport = stockReportResp.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("rdate", SystemSetting.UtilDateToString(SystemSetting.applicationDate, "yyyy-MM-dd"));

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(StoreChangeDtlReport);

		String jrxmlPath = "jrxml/store_stockreport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void UserWiseSalesReceiptSummaryReport(String strDate, String endDate, UserMst userMst,
			String branchCode) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/stockreport.pdf";

		// Call urld

		ResponseEntity<List<ReceiptModeReport>> userWiseSalesSummaryResp = RestCaller
				.getUserWiseSalesReceiptReport(userMst.getId(), branchCode, strDate, endDate);

		List<ReceiptModeReport> userWiseSalesSummary = userWiseSalesSummaryResp.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

		List<BranchMst> branchList = new ArrayList<BranchMst>();
		branchList.add(branchMst);
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchList);

		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(userWiseSalesSummary);

		String jrxmlPath = "jrxml/UserWiseSalesReceiptSummary.jrxml";

		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());

		parameters.put("strDate", strDate);
		parameters.put("endDate", endDate);
		parameters.put("userName", userMst.getUserName());

		parameters.put("subreportdata2", jrBeanCollectionDataSource2);
		parameters.put("receiptmodewisereport", "jrxml/ReceiptModeWiseReport.jasper");

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void BalanceSheetHorizontal(ObservableList<BalanceSheetHorizontal> balancesheetlist)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/" + balancesheetlist.get(0).getFinancialYear()
				+ "balanceSheet.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

		List<BranchMst> branchList = new ArrayList<BranchMst>();
		branchList.add(branchMst);

		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(balancesheetlist);

		String jrxmlPath = "jrxml/BalanceSheetHorizontal.jrxml";

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource2);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void ExceptionReport(List getAcc) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/ExceptionReport.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

		List<BranchMst> branchList = new ArrayList<BranchMst>();
		branchList.add(branchMst);

		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(getAcc);

		String jrxmlPath = "jrxml/ExceptionReport.jrxml";

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource2);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void itemNotInBatchPriceDefinition(
			ObservableList<ItemNotInBatchPriceReport> itemNotInBatchPriceReportList) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/BatchPriceItemException.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

		List<BranchMst> branchList = new ArrayList<BranchMst>();
		branchList.add(branchMst);

		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(
				itemNotInBatchPriceReportList);

		String jrxmlPath = "jrxml/ItemNotInBatchPriceDefReport.jrxml";

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource2);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void hsnCodeWisePurchaseDtlReport(String startDate, String endDate, String cat) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/" + startDate + "hsnpurchaseDtl.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		ResponseEntity<List<HsnWisePurchaseDtlReport>> hsn = RestCaller.getHsnPurchaseDtl(startDate, endDate, cat);
		List<HsnWisePurchaseDtlReport> hsnWisePurchaseDtlReportList = hsn.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				hsnWisePurchaseDtlReportList);
		String jrxmlPath = "jrxml/HsnPurchaseDetailReport.jrxml";
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());
		parameters.put("fromdate", startDate);
		parameters.put("todate", endDate);
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void hsnCodeWiseSalesDtlReport(String startDate, String endDate, String cat) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/" + startDate + "hsnsalesDtl.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		ResponseEntity<List<HsnWiseSalesDtlReport>> hsn = RestCaller.getHsnSalesDtl(startDate, endDate, cat);
		List<HsnWiseSalesDtlReport> hsnWisePurchaseDtlReportList = hsn.getBody();
		for (HsnWiseSalesDtlReport hsnsales : hsnWisePurchaseDtlReportList) {
			BigDecimal bdRate = new BigDecimal(hsnsales.getRate());
			bdRate = bdRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			hsnsales.setRate(bdRate.doubleValue());

			BigDecimal bdsgst = new BigDecimal(hsnsales.getSgstAmount());
			bdsgst = bdsgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			hsnsales.setSgstAmount(bdsgst.doubleValue());

			BigDecimal bdcgst = new BigDecimal(hsnsales.getCgstAmount());
			bdcgst = bdcgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			hsnsales.setCgstAmount(bdcgst.doubleValue());

			BigDecimal bdvalue = new BigDecimal(hsnsales.getValue());
			bdvalue = bdvalue.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			hsnsales.setValue(bdvalue.doubleValue());

			BigDecimal bdcess = new BigDecimal(hsnsales.getCessRate());
			bdcess = bdcess.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			hsnsales.setCessRate(bdcess.doubleValue());

			BigDecimal bdqty = new BigDecimal(hsnsales.getQty());
			bdqty = bdqty.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			hsnsales.setQty(bdqty.doubleValue());
		}
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				hsnWisePurchaseDtlReportList);
		String jrxmlPath = "jrxml/HsnSalesDtlReport.jrxml";
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());
		parameters.put("fromdate", startDate);
		parameters.put("todate", endDate);
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void gstSalesReport(String date, String stodate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/" + date + "gstSalesReport.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		ResponseEntity<List<B2bSalesReport>> gstSales = RestCaller.getgstSalesReport(date, stodate);
		List<B2bSalesReport> hsnWisePurchaseDtlReportList = gstSales.getBody();
		for (B2bSalesReport b2b : hsnWisePurchaseDtlReportList) {

			BigDecimal bdcess = new BigDecimal(b2b.getCessAmount());
			bdcess = bdcess.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			b2b.setCessAmount(bdcess.doubleValue());

			BigDecimal bdcgst = new BigDecimal(b2b.getCgstAmount());
			bdcgst = bdcgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			b2b.setCgstAmount(bdcgst.doubleValue());

			BigDecimal bdsgst = new BigDecimal(b2b.getSgstAmount());
			bdsgst = bdsgst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			b2b.setSgstAmount(bdsgst.doubleValue());

			BigDecimal bdigst = new BigDecimal(b2b.getIgstAmount());
			bdigst = bdigst.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			b2b.setIgstAmount(bdigst.doubleValue());

			BigDecimal bdtax = new BigDecimal(b2b.getTaxableAmount());
			bdtax = bdtax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			b2b.setTaxableAmount(bdtax.doubleValue());

			BigDecimal bdtaxAmt = new BigDecimal(b2b.getTaxAmount());
			bdtaxAmt = bdtaxAmt.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			b2b.setTaxAmount(bdtaxAmt.doubleValue());

			BigDecimal bdamount = new BigDecimal(b2b.getAmount());
			bdamount = bdamount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			b2b.setAmount(bdamount.doubleValue());

		}
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				hsnWisePurchaseDtlReportList);
		String jrxmlPath = "jrxml/gstSalesReport.jrxml";
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());
		parameters.put("fromdate", date);
		parameters.put("todate", stodate);
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// -------------------new version 2.1 surya

	public static void PrintItemWiseStockMovementReport(String strtDate, String endDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/" + strtDate + "itemwisestockmovementreport.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

		ResponseEntity<List<CategoryWiseStockMovement>> itemWiseStockMovementResp = RestCaller
				.getItemWiseWiseStockMovement(strtDate, endDate);
		List<CategoryWiseStockMovement> itemWiseStockMovementList = itemWiseStockMovementResp.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				itemWiseStockMovementList);
		String jrxmlPath = "jrxml/ItemWiseStockMovementReport.jrxml";

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST :" + branchMst.getBranchGst());
		parameters.put("fromdate", strtDate);
		parameters.put("todate", endDate);

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);
	}

	// -------------------new version 2.1 surya end

	public static void soconvertedReport(List<SaleOrderReport> saleorderlist, String strtDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/" + strtDate + "soconverted.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(saleorderlist);
		String jrxmlPath = "jrxml/soconvertedReport.jrxml";

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);
	}

	public static void DayEndReceiptMode(ResponseEntity<List<DayEndProcessing>> dayendReceiptMode, String vdate)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/" + vdate + "DayEndeceipt.pdf";

		// Call urld

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		for (DayEndProcessing day : dayendReceiptMode.getBody()) {
			if (null == day.getRealizedAmount()) {
				day.setRealizedAmount(0.0);
			} else if (null != day.getRealizedAmount()) {
				BigDecimal bd = new BigDecimal(day.getRealizedAmount());
				bd = bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				day.setRealizedAmount(bd.doubleValue());
			}
			if (null == day.getSalesAmount()) {
				day.setSalesAmount(0.0);

			} else if (null != day.getSalesAmount()) {
				BigDecimal bd = new BigDecimal(day.getSalesAmount());
				bd = bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				day.setSalesAmount(bd.doubleValue());
			}
			if (null == day.getSoAmount()) {
				day.setSoAmount(0.0);
			} else if (null != day.getSoAmount()) {
				BigDecimal bd = new BigDecimal(day.getSoAmount());
				bd = bd.setScale(2, BigDecimal.ROUND_HALF_EVEN);
				day.setSoAmount(bd.doubleValue());
			}
		}
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				dayendReceiptMode.getBody());
		String jrxmlPath = "jrxml/DayendReceiptModeNew.jrxml";

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("FromDate", vdate);
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);
	}

	public static void BarcodePrintingByJasper(String batchCode, String numberOfCopies, String sProdDate,
			String manufactureDate, String expiryDate, String itemName, String barcode, String mrp, String ingLine1,
			String ingLine2, String printerNAme, String netWt, String barcodeBatchId, String itemId)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/" + "Barcode.pdf";

		// urld
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getUser().getBranchCode());
		List<BranchMst> branchMstList = new ArrayList<BranchMst>();

		branchMstList.add(branchMst);
		HashMap parameters = new HashMap();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchMstList);
		String jrxmlPath = "jrxml/Barcode4JLekshmi.jrxml";

//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchMstList);
//		String jrxmlPath = "jrxml/TEST1.jrxml";

		parameters.put("batchCode", batchCode);
		parameters.put("numberOfCopies", numberOfCopies);
		parameters.put("sProdDate", sProdDate);
		parameters.put("manufactureDate", manufactureDate);
		parameters.put("expiryDate", expiryDate);
		parameters.put("itemName", itemName);
		parameters.put("barcode", barcode);
		parameters.put("mrp", mrp);
		parameters.put("ingLine1", ingLine1);
		parameters.put("ingLine2", ingLine2);
		parameters.put("printerNAme", printerNAme);
		parameters.put("netWt", netWt);
		parameters.put("barcodeBatchId", barcodeBatchId);
		parameters.put("BARCODETITLE", SystemSetting.getBARCODETITLE());

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
		hs.showDocument(pdfToGenerate);

	}

	// ----------------Sibi.....27-04-2021..............//

	public static void PharmacyPurchaseReport(String fdate, String tDate, String branchCode, String category)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/pharmacypurchasereport" + fdate + tDate + ".pdf";

		// Call url
		ResponseEntity<List<PharmacyPurchaseReport>> departmentReport = RestCaller.getPharmacyPurchaseReport(fdate,
				tDate, branchCode, category);

		List<PharmacyPurchaseReport> departmentList = departmentReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/PharmacyPurchaseReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("EntryDate", departmentList.get(0).getTrEntryDate());
		parameters.put("SupplierName", departmentList.get(0).getSupplierName());
		parameters.put("VoucherDate", departmentList.get(0).getOurVoucherDate());
		parameters.put("SupplierVoucherDate", departmentList.get(0).getSupplierVoucherDate());
		parameters.put("NetInvoiceTotal", departmentList.get(0).getNetInvoiceTotal());
		parameters.put("TinNo", departmentList.get(0).getTinNo());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// -----------sibi-------29/04/2021............//

	public static void PharmacyGroupWiseSalesReport(String fdate, String tDate, String branchCode, String category)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/pharmacygroupwisesalesreport" + fdate + tDate + ".pdf";

		// Call url
		ResponseEntity<List<PharmacyGroupWiseSalesReport>> departmentReport = RestCaller
				.getPharmacyGroupWiseSales(fdate, tDate, branchCode, category);

		List<PharmacyGroupWiseSalesReport> departmentList = departmentReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/PharmacyGroupwiseSalesReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
	//	JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		
		parameters.put("fromDate",fdate);
		parameters.put("toDate",tDate);

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("itemName", departmentList.get(0).getItemName());
		parameters.put("amount", departmentList.get(0).getAmount());
		parameters.put("tax", departmentList.get(0).getTax());
		parameters.put("batchCode", departmentList.get(0).getBatchCode());
		parameters.put("qty", departmentList.get(0).getQty());
		
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// ---------------------------------sharon---------------28/04/21
	// WED-----------------------
	public static void printPharmacySalesReturnReportDtl(String fdate, String tDate, String branchCode, String category)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/Pharmacy_Salesreturn_Report" + fdate + ".pdf";

		// Call url

		ResponseEntity<List<PharmacySalesReturnReport>> pharmacySalesReturnReportResponse = RestCaller
				.getPharmacySalesReturn(fdate, tDate, branchCode, category);
		List<PharmacySalesReturnReport> pharmacySalesReturnReportReport = pharmacySalesReturnReportResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				pharmacySalesReturnReportReport);

		String jrxmlPath = "jrxml/Pharmacy_Salesreturn_Report.jrxml";

		// Compile the Jasper report from .jrxml to .japser

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(branchCode);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("FromDate", fdate);
		parameters.put("ToDate", tDate);

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// ----------Sibi................30/04/2021...............//

	public static void PharmacyBranchStockReport(String fdate, String tDate, String branchCode, String category,
			String supplierName) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/PharmacyBranchStockReport" + fdate + tDate + ".pdf";

		// Call url
		ResponseEntity<List<PharmacyBranchStockReport>> departmentReport = RestCaller
				.getPharmacyBranchStockReport(fdate, tDate, branchCode, category, supplierName);

		List<PharmacyBranchStockReport> departmentList = departmentReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/BranchStockReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("EmailId", branchMst.getBranchEmail());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("itemName", departmentList.get(0).getItemName());
		parameters.put("amount", departmentList.get(0).getAmount());
		parameters.put("GroupName", departmentList.get(0).getGroupName());
		parameters.put("batchCode", departmentList.get(0).getBatchCode());
		parameters.put("qty", departmentList.get(0).getQty());
		parameters.put("itemCode", departmentList.get(0).getItemCode());
		parameters.put("expiryDate", departmentList.get(0).getExpiryDate());
		parameters.put("rate", departmentList.get(0).getRate());

	}

	public static void printPharmacyGroupWiseSalesDtlReport(String fdate, String tDate, String branchCode,
			String category) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/Groupwise_Salesdetails_Pharmacyreport" + fdate + ".pdf";

		// Call url

		ResponseEntity<List<PharmacySalesDetailReport>> pharmacySalesDetailReportResponse = RestCaller
				.getPharmacSalesDtlReport(fdate, tDate, branchCode, category);
		List<PharmacySalesDetailReport> pharmacySalesDetailReportReport = pharmacySalesDetailReportResponse.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				pharmacySalesDetailReportReport);

		//String jrxmlPath = "jrxml/Groupwise_Salesdetails_Pharmacyreport.jrxml";
		
		String jrxmlPath = "jrxml/PharmacyGroupwiseSalesdetails.jrxml";

		// Compile the Jasper report from .jrxml to .japser

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(branchCode);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("FromDate", fdate);
		parameters.put("ToDate", tDate);

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PharmacyWriteOffSummaryReport(String fdate, String tDate, String branchCode, String itemgroup)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/PharmacyWriteOffSummaryReport" + fdate + tDate + ".pdf";

		// Call url
		ResponseEntity<List<WriteOffDetailAndSummaryReport>> departmentReport = RestCaller
				.getPharmacyWriteOffSummaryReport(fdate, tDate, branchCode, itemgroup);

		List<WriteOffDetailAndSummaryReport> departmentList = departmentReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/WriteOffSummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("EmailId", branchMst.getBranchEmail());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		parameters.put("voucherNumber", departmentList.get(0).getVoucherNumber());
		parameters.put("date", departmentList.get(0).getDate());
		parameters.put("department", departmentList.get(0).getDepartment());
		parameters.put("user", departmentList.get(0).getUser());
		parameters.put("remarks", departmentList.get(0).getRemarks());
		parameters.put("amount", departmentList.get(0).getAmount());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

	}

	// --------------------------------sharon---------ShortExpiryReport(30/04/2021)---------------------
//	public static void printShortExpiryReportReport(String fdate, String tDate, String branchCode,String priceType,String itemName,String itemGroup,String batchCode,String itemCode) throws JRException {
//		String pdfToGenerate = SystemSetting.reportPath + "/Groupwise_Salesdetails_Pharmacyreport" + fdate + ".pdf";
//
//		// Call url
//
//		ResponseEntity<List<ShortExpiryReport>> shortExpiryReportResponse = RestCaller.getShortExpiryReport(fdate, tDate,
//				branchCode,priceType,itemName,itemGroup,batchCode,itemCode);
//		List<ShortExpiryReport> shortExpiryReport = shortExpiryReportResponse.getBody();
//
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(shortExpiryReport);
//
//		String jrxmlPath = "jrxml/Groupwise_Salesdetails_Pharmacyreport.jrxml";
//
//		// Compile the Jasper report from .jrxml to .japser
//
//		HashMap parameters = new HashMap();
//
//		BranchMst branchMst = RestCaller.getBranchDtls(branchCode);
//		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//		parameters.put("BranchName", branchMst.getBranchName());
//		parameters.put("Address1", branchMst.getBranchAddress1());
//		parameters.put("Phone", branchMst.getBranchTelNo());
//		parameters.put("State", branchMst.getBranchState());
//		parameters.put("FromDate", fdate);
//		parameters.put("ToDate", tDate);
//		
//		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
//
//		// Fill the report
//		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
//		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
//
//		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
//
//		hs.showDocument(pdfToGenerate);
//
//	}
	public static void LocalPurchaseDetailReport(String voucherNumber, String date) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/localPurchaseDetailReport" + voucherNumber + ".pdf";
		ResponseEntity<List<LocalPurchaseDetailReport>> localpurchaseResponse = RestCaller
				.getLocalPurchaseDtlInvoice(voucherNumber, date);
		List<LocalPurchaseDetailReport> localpurchaseInvoiceResponse = localpurchaseResponse.getBody();
		ResponseEntity<List<LocalPurchaseDetailReport>> localpurchaseTaxResponse = RestCaller
				.getLocalPurchaseTax(voucherNumber, date);
		List<LocalPurchaseDetailReport> localpurchaseTax = localpurchaseTaxResponse.getBody();
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				localpurchaseInvoiceResponse);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(localpurchaseTax);
		String jrxmlPath = "jrxml/LocalPurchaseDetailReport.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
		HashMap parameters = new HashMap();

		parameters.put("subreportdata", jrBeanCollectionDataSource2);
		parameters.put("taxsliptup", "jrxml/taxsplits.jasper");
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

//..............Sibi...............2/05/2021............//

	public static void LocalPurchaseSummaryReport(String fdate, String tDate, String branchCode, String itemgroup,
			String supplierName) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/LocalPurchaseSummaryReport" + fdate + tDate + ".pdf";

		// Call url
		ResponseEntity<List<LocalPurchaseSummaryReport>> departmentReport = RestCaller
				.getLocalPurchaseSummaryReport(fdate, tDate, branchCode, itemgroup, supplierName);

		List<LocalPurchaseSummaryReport> departmentList = departmentReport.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(departmentList);

		String jrxmlPath = "jrxml/LocalPurchaseSummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("EmailId", branchMst.getBranchEmail());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		parameters.put("invoiceNumber", departmentList.get(0).getInvoiceNumber());
		parameters.put("supplier", departmentList.get(0).getSupplier());
		parameters.put("date", departmentList.get(0).getDate());
		parameters.put("user", departmentList.get(0).getUser());
		parameters.put("tinNo", departmentList.get(0).getTinNo());

		parameters.put("supplierInvoiceNumber", departmentList.get(0).getSupplierInvoiceNumber());
		parameters.put("invoiceDate", departmentList.get(0).getInvoiceDate());
		parameters.put("pONumber", departmentList.get(0).getpONumber());
		parameters.put("netInvoice", departmentList.get(0).getNetInvoice());
		parameters.put("beforeGst", departmentList.get(0).getBeforeGst());
		parameters.put("GST", departmentList.get(0).getGst());

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void PharmacyTaxInvoiceReport(String voucherNumber, String date) throws JRException {

		// voucherNumber = "000173";
		// date = "2019-07-21";
		System.out.println("======PharmacyTaxInvoiceReport===========");

		String pdfToGenerate = SystemSetting.reportPath + "/invoice" + voucherNumber + ".pdf";
		// Call url
		ResponseEntity<List<SalesInvoiceReport>> invoiceResponse = RestCaller.getInvoice(voucherNumber, date);
		List<SalesInvoiceReport> invoice = invoiceResponse.getBody();
		SalesTransHdr salesTransHdrResp = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNumber, date);

		ResponseEntity<List<SalesReceipts>> SalesReceiptsResp = RestCaller
				.getSalesReceiptsByTransHdrId(salesTransHdrResp.getId());

		List<SalesReceipts> salesReceipts = SalesReceiptsResp.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);

		JRBeanCollectionDataSource jrBeanCollectionDataSource1 = new JRBeanCollectionDataSource(salesReceipts);

		String jrxmlPath = "jrxml/PharmacyTaxInvoiceReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		if (null != salesTransHdrResp) {
			if (null != salesTransHdrResp.getPatientMst()) {
				parameters.put("PatientName", salesTransHdrResp.getPatientMst().getPatientName());
			}
		}

		String accountName = null;
		Double discount = null;

		if (null != salesTransHdrResp) {
			if (null != salesTransHdrResp.getPatientMst()) {
				if (null != salesTransHdrResp.getPatientMst().getInsuranceCompanyId()) {
					accountName = salesTransHdrResp.getPatientMst().getInsuranceCompanyId();

					ResponseEntity<InsuranceCompanyMst> insuranceCompanynamebyId = RestCaller
							.getinsuraCompanyMstById(accountName);

					InsuranceCompanyMst insuranceName = insuranceCompanynamebyId.getBody();

					if (!insuranceName.getInsuranceCompanyName().equalsIgnoreCase("NO INSURANCE")) {
						parameters.put("AccountName", insuranceName.getInsuranceCompanyName());

					}
				} else {
					if (null != salesTransHdrResp.getPatientMst().getAccountHeads()) {
						if (null != salesTransHdrResp.getPatientMst().getAccountHeads().getAccountName()) {
							parameters.put("AccountName",
									salesTransHdrResp.getPatientMst().getAccountHeads().getAccountName());
						}
					}
				}
			}

			if (null != salesTransHdrResp) {

				String currencyType = "FC";
				if (null != salesTransHdrResp.getCurrencyId()) {
					ResponseEntity<CurrencyMst> currencyMstResp = RestCaller
							.getcurrencyMsyById(salesTransHdrResp.getCurrencyId());
					CurrencyMst currencyMst = currencyMstResp.getBody();

					currencyType = currencyMst.getCurrencyName();

				}

				if (null != salesTransHdrResp.getFcInvoiceDiscount()) {

					parameters.put("DiscountInFcType", "Discount in " + currencyType);

					parameters.put("FcInvoiceDiscount", salesTransHdrResp.getFcInvoiceDiscount());

				}

				if (null != salesTransHdrResp.getFcInvoiceAmount()) {
					parameters.put("AmountInFCType", "Amount in " + currencyType);

					parameters.put("FcInvoiceAmount", salesTransHdrResp.getFcInvoiceAmount());

				}
			}

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("branchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("EmailId", branchMst.getBranchEmail());
			parameters.put("GST", branchMst.getBranchGst());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());

			String amount = "0.0";
			if (invoice.size() > 0) {
				if (null != invoice.get(0)) {
					amount = invoice.get(0).getAmount() + "";
				}
			}
			String amountinwords = SystemSetting.AmountInWords(amount, "Rufiyaa", "Larri");
			parameters.put("invoiceAmount", amountinwords);

			parameters.put("subreportdata2", jrBeanCollectionDataSource1);
			parameters.put("stax_invoice_tax_sub2", "jrxml/SalesReceiptsReport.jasper");

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);

		}

	}

	public static void importPurchaseSummaryReport(String fdate, String tdate) throws JRException {

		// voucherNumber = "000173";
		// date = "2019-07-21";
		System.out.println("======importPurchaseSummaryReport===========");

		String pdfToGenerate = SystemSetting.reportPath + "/importPurchaseSummaryReport" + fdate + tdate + ".pdf";

		System.out.println(pdfToGenerate);

		ResponseEntity<List<ImportPurchaseSummary>> importPurchaseSummary = RestCaller
				.getImportPurchaseSummaryReport(fdate, tdate);
		List<ImportPurchaseSummary> importPurchaseSummaryReport = importPurchaseSummary.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				importPurchaseSummaryReport);

		String jrxmlPath = "jrxml/ImportPurchaseSummarycopy.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
 
		HashMap parameters = new HashMap(); 

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("EmailId", branchMst.getBranchEmail());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());

		parameters.put("fromDate", fdate);
		parameters.put("Todate", tdate);

		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// ======================GST report ========anandu=======11-05-2021============

	public static void GstOutputDetailReport(String fdate, String tDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/gstoutputdetailreport" + fdate + tDate + ".pdf";

		// Call url
		ResponseEntity<List<GSTOutputDetailReport>> gSTOutputDetailReport = RestCaller.getGstOutputDetailReport(fdate,
				tDate);

		List<GSTOutputDetailReport> gSTOutputDetailReportList = gSTOutputDetailReport.getBody();

		GSTOutputDetailReport gstOutputDetailReport = new GSTOutputDetailReport();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				gSTOutputDetailReportList);

		String jrxmlPath = "jrxml/GSTOutputDetailReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("fromdate", fdate);
		parameters.put("todate", tDate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void GstOutputSummaryReport(String fdate, String tDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/gstoutputsummaryreport" + fdate + tDate + ".pdf";

		// Call url
		ResponseEntity<List<GSTOutputDetailReport>> gSTOutputDetailReport = RestCaller.getGstOutputDetailReport(fdate,
				tDate);

		List<GSTOutputDetailReport> gSTOutputDetailReportList = gSTOutputDetailReport.getBody();

		GSTOutputDetailReport gstOutputDetailReport = new GSTOutputDetailReport();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				gSTOutputDetailReportList);

		String jrxmlPath = "jrxml/GSTOutputSummaryeport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("fromdate", fdate);
		parameters.put("todate", tDate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void GstInputDetailReport(String sfdate, String stdate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/gstinputdetailreport" + sfdate + stdate + ".pdf";

		// Call url
		ResponseEntity<List<GSTInputDtlAndSmryReport>> gSTInputDtlAndSmryReport = RestCaller.getGstInputDetails(sfdate,
				stdate);

		List<GSTInputDtlAndSmryReport> gSTInputDtlAndSmryReportList = gSTInputDtlAndSmryReport.getBody();

		GSTInputDtlAndSmryReport gstInputDtlAndSmryReport = new GSTInputDtlAndSmryReport();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				gSTInputDtlAndSmryReportList);

		String jrxmlPath = "jrxml/GSTInputDetailReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

		parameters.put("fromdate", sfdate);
		parameters.put("todate", stdate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void GstInputSummaryReport(String sfdate, String stdate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/gstinputsummaryreport" + sfdate + stdate + ".pdf";

		// Call url
		ResponseEntity<List<GSTInputDtlAndSmryReport>> gSTInputDtlAndSmryReport = RestCaller.getGstInputDetails(sfdate,
				stdate);

		List<GSTInputDtlAndSmryReport> gSTInputDtlAndSmryReportList = gSTInputDtlAndSmryReport.getBody();

		GSTInputDtlAndSmryReport gstInputDtlAndSmryReport = new GSTInputDtlAndSmryReport();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				gSTInputDtlAndSmryReportList);

		String jrxmlPath = "jrxml/GSTInputSummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("fromdate", sfdate);
		parameters.put("todate", stdate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// =========================end=================================

	// ******************branch wise stock evaluation report**********************
	public static void BatchWiseStockEvaluationReport(String itemid, String batch, String startdate, String enddate)
			throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/BatchWiseStockEvaluationReport.pdf";

		// Call urld

		ResponseEntity<List<BatchWiseStockEvaluationReport>> BranchWiseStockEvaluationResponse = RestCaller
				.getDailyBatchWiseStockEvaluationReport(itemid, batch, startdate, enddate);

		List<BatchWiseStockEvaluationReport> DailyBranchWiseStockEvaluationReport = BranchWiseStockEvaluationResponse
				.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("CompanyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fdate", startdate);
		parameters.put("rdate", enddate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				DailyBranchWiseStockEvaluationReport);

		String jrxmlPath = "jrxml/BatchWiseStockEvaluationReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

//=============================================WholeSaleDetailReport===============================
	public static void WholeSaleDetailReport(String fromDate, String toDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/wholesaledetailreport.pdf";

		// Call urld

		ResponseEntity<List<WholeSaleDetailReport>> wholeSaleDetailReportResponse = RestCaller
				.getWholeSaleDetailReport(fromDate, toDate);

		List<WholeSaleDetailReport> wholeSaleDetailReport = wholeSaleDetailReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromdate", fromDate);
		parameters.put("todate", toDate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(wholeSaleDetailReport);

		String jrxmlPath = "jrxml/WholeSaleDetailReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void WholeSaleSummaryReport(String fromDate, String toDate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/wholesalesummaryreport.pdf";

		// Call urld

		ResponseEntity<List<WholeSaleDetailReport>> wholeSaleSummaryReportResponse = RestCaller
				.getWholeSaleDetailReport(fromDate, toDate);

		List<WholeSaleDetailReport> wholeSaleSummaryReport = wholeSaleSummaryReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromdate", fromDate);
		parameters.put("todate", toDate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(wholeSaleSummaryReport);

		String jrxmlPath = "jrxml/WholeSalesSummary.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void insurancewisesalesreport(String sdate, String edate) {

		System.out.println("======insurancewisesalesreport===========");
		String pdfToGenerate = SystemSetting.reportPath + "/insurancewisesalesreport" + sdate + edate + ".pdf";
		// Call url
		ResponseEntity<List<InsuranceWiseSalesReport>> insuranceWiseSalesRepo = RestCaller
				.getInsuranceWiseSalesReportReport(sdate, edate);

		List<InsuranceWiseSalesReport> insuranceWiseSalesReportList = new ArrayList<InsuranceWiseSalesReport>();
		insuranceWiseSalesReportList = insuranceWiseSalesRepo.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				insuranceWiseSalesReportList);

		String jrxmlPath = "jrxml/InsuranceWiseSalesReport.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport;

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("EmailId", branchMst.getBranchEmail());

		parameters.put("fromdate", sdate);
		parameters.put("todate", edate);

		JasperPrint jasperPrint;
		try {
			jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
			hs.showDocument(pdfToGenerate);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void consumptionreportsummary(String branch, String sdate, String edate) {

		System.out.println("======insurancewisesalesreport===========");
		String pdfToGenerate = SystemSetting.reportPath + "/consumptionreport" + sdate + edate + ".pdf";
		// Call url
		ResponseEntity<List<ConsumptionReport>> consumptionreportsummaryRepo = RestCaller
				.getConsumptionReportSummary(branch, sdate, edate);

		List<ConsumptionReport> consumptionreportsummaryList = new ArrayList<ConsumptionReport>();
		consumptionreportsummaryList = consumptionreportsummaryRepo.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				consumptionreportsummaryList);

		String jrxmlPath = "jrxml/ConsumptionReport.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport;

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("EmailId", branchMst.getBranchEmail());

		parameters.put("fromdate", sdate);
		parameters.put("todate", edate);

		JasperPrint jasperPrint;
		try {
			jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
			hs.showDocument(pdfToGenerate);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// =============================================BranchStockReport===============================
	public static void BranchStockReport(String date) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/BranchStockReport.pdf";

		// Call urld

		ResponseEntity<List<BranchStockReport>> branchStockReportResponse = RestCaller.getBranchStockReport(date);

		List<BranchStockReport> branchStockReport = branchStockReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("date", date);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchStockReport);

		String jrxmlPath = "jrxml/BranchStockReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// ....................................sibi.....20/05/2021.....////
	public static void PharmacyTaxInvoiceTotal(String voucherNumber, String date) throws JRException {

		// voucherNumber = "000173";
		// date = "2019-07-21";

		System.out.println("======PharmacyTaxInvoiceTotal===========");

		String pdfToGenerate = SystemSetting.reportPath + "/TaxInvoiceTotal" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<SalesInvoiceReport>> invoiceResponse = RestCaller.getInvoice(voucherNumber, date);
		List<SalesInvoiceReport> invoice = invoiceResponse.getBody();

		SalesTransHdr salesTransHdrResp = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNumber, date);

		ResponseEntity<List<TermsAndConditionsMst>> termsAndConditionMstResp = RestCaller
				.getTermsAndConditionMstByInvoiceFormat("PharmacyWholeSaleInvoice");
		List<TermsAndConditionsMst> termsAndConditionMstList = termsAndConditionMstResp.getBody();

		ResponseEntity<List<SalesReceipts>> SalesReceiptsResp = RestCaller
				.getSalesReceiptsByTransHdrId(salesTransHdrResp.getId());

		List<SalesReceipts> salesReceipts = SalesReceiptsResp.getBody();

		// Compile the Jasper report from .jrxml to .japser

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);

		JRBeanCollectionDataSource jrBeanCollectionDataSource1 = new JRBeanCollectionDataSource(salesReceipts);

		String jrxmlPath = "jrxml/PharmacyWholesaleInvoiceNew.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		if (null != salesTransHdrResp) {
			if (null != salesTransHdrResp.getPatientMst()) {
				parameters.put("PatientName", salesTransHdrResp.getPatientMst().getPatientName());
			}
		}

		String accountName = null;

		if (null != salesTransHdrResp) {
			if (null != salesTransHdrResp.getPatientMst()) {
				if (null != salesTransHdrResp.getPatientMst().getInsuranceCompanyId()) {
					accountName = salesTransHdrResp.getPatientMst().getInsuranceCompanyId();

					ResponseEntity<InsuranceCompanyMst> insuranceCompanynamebyId = RestCaller
							.getinsuraCompanyMstById(accountName);

					InsuranceCompanyMst insuranceName = insuranceCompanynamebyId.getBody();

					if (!insuranceName.getInsuranceCompanyName().equalsIgnoreCase("NO INSURANCE")) {
						parameters.put("AccountName", insuranceName.getInsuranceCompanyName());

					}
				} else {
					parameters.put("AccountName", salesTransHdrResp.getPatientMst().getAccountHeads().getAccountName());
				}

			}

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("branchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("EmailId", branchMst.getBranchEmail());
			parameters.put("GST", branchMst.getBranchGst());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("CustomerPo", salesTransHdrResp.getPoNumber());
			parameters.put("CustomerTIN", salesTransHdrResp.getAccountHeads().getPartyGst());

			parameters.put("subreportdata2", jrBeanCollectionDataSource1);
			parameters.put("stax_invoice_tax_sub2", "jrxml/SalesReceiptsReport.jasper");

			String amount = "0.0";
			if (invoice.size() > 0) {
				if (null != invoice.get(0)) {
					amount = invoice.get(0).getAmount() + "";
				}
			}
			String amountinwords = SystemSetting.AmountInWords(amount, "Rufiyaa", "Larri");
			parameters.put("invoiceAmount", amountinwords);

			if (termsAndConditionMstList.size() > 0) {
				int length = termsAndConditionMstList.size();
				if (length >= 1 && null != termsAndConditionMstList.get(0)) {
					parameters.put("termsAndCondition1", "1. " + termsAndConditionMstList.get(0).getConditions());
				}
				if (length >= 2 && null != termsAndConditionMstList.get(1)) {
					parameters.put("termsAndCondition2", "2. " + termsAndConditionMstList.get(1).getConditions());
				}
				if (length >= 3 && null != termsAndConditionMstList.get(2)) {
					parameters.put("termsAndCondition3", "3. " + termsAndConditionMstList.get(2).getConditions());
				}
				if (length >= 4 && null != termsAndConditionMstList.get(3)) {
					parameters.put("termsAndCondition4", "4. " + termsAndConditionMstList.get(3).getConditions());
				}
				if (length >= 5 && null != termsAndConditionMstList.get(4)) {
					parameters.put("termsAndCondition5", "5. " + termsAndConditionMstList.get(4).getConditions());
				}
				if (length >= 6 && null != termsAndConditionMstList.get(5)) {
					parameters.put("termsAndCondition6", "6. " + termsAndConditionMstList.get(5).getConditions());
				}
				if (length >= 6 && null != termsAndConditionMstList.get(6)) {
					parameters.put("termsAndCondition7", "7. " + termsAndConditionMstList.get(6).getConditions());
				}
			}

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);

		}

	}

	public static void temsandconditions(String selectedItem) {

		System.out.println("======temsandconditions===========");
		String pdfToGenerate = SystemSetting.reportPath + "/temsandconditions .pdf";
		// Call url
		ResponseEntity<List<TermsAndConditionsMst>> termsAndConditionMstResp = RestCaller
				.getTermsAndConditionMstByInvoiceFormat(selectedItem);
		List<TermsAndConditionsMst> termsAndConditionMstList = termsAndConditionMstResp.getBody();

//		List<ConsumptionReport> consumptionreportsummaryList = new ArrayList<ConsumptionReport>();
//		consumptionreportsummaryList = consumptionreportsummaryRepo.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				termsAndConditionMstList);

		String jrxmlPath = "jrxml/Terms&Condition.jrxml";
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport;

		HashMap parameters = new HashMap();

//		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
//		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//		parameters.put("branchName", branchMst.getBranchName());
//		parameters.put("Address1", branchMst.getBranchAddress1());
//		parameters.put("Address2", branchMst.getBranchAddress2());
//		parameters.put("Phone", branchMst.getBranchTelNo());
//		parameters.put("State", branchMst.getBranchState());
//		parameters.put("GST", branchMst.getBranchGst());
//		parameters.put("EmailId", branchMst.getBranchEmail());
//		

		JasperPrint jasperPrint;
		try {
			jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
			hs.showDocument(pdfToGenerate);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// ===================priceTypeWiseStockSummaryReport=========anandu========
	public static void priceTypeWiseStockSummaryReport(String sfdate, String stdate, String pricetype, String branch)
			throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/pricetypewisestocksummaryreport" + sfdate + stdate + ".pdf";

		// Call url
		ResponseEntity<List<StockReport>> priceTypeWiseStockSummaryReporttt = RestCaller.getStockSummaryDetails(sfdate,
				stdate, pricetype, branch);

		List<StockReport> priceTypeWiseStockSummaryReportttList = priceTypeWiseStockSummaryReporttt.getBody();

		StockReport stockReport = new StockReport();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				priceTypeWiseStockSummaryReportttList);

		String jrxmlPath = "jrxml/PriceTypeWiseStockSummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());

		parameters.put("fromdate", sfdate);
		parameters.put("todate", stdate);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void PharmmacyPurchaseInvoiceReport(String purchasId) {
		try {
			
			ResponseEntity<PurchaseHdr> purchaseResp = RestCaller.getPurchaseHdr(purchasId);
			PurchaseHdr purchaseHdr = purchaseResp.getBody();
			
			System.out.println("PurchaseHdr="+purchaseHdr);
			
			System.out.println("INSIDE PharmmacyPurchaseInvoiceReport");
			String pdfToGenerate = SystemSetting.reportPath + "/PurchaseInvoiceReport" + purchaseHdr.getVoucherNumber() + ".pdf";
			System.out.println("pdfToGenerate"+pdfToGenerate);


			ResponseEntity<List<PurchaseReport>> pPurchaseInvoiceReportResponse = RestCaller
					.getImportPurchaseInvoiceByHdrId(purchasId);
			

			List<PurchaseReport> PurchaseInvoiceReport = pPurchaseInvoiceReportResponse.getBody();
			
			System.out.println("PurchaseInvoiceReport="+PurchaseInvoiceReport);


			


			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					PurchaseInvoiceReport);

			String jrxmlPath = "jrxml/PharmacyLocalPurchaseInvoice.jrxml";
			
			System.out.println("jrxmlPath="+jrxmlPath);

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport;

			jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			HashMap parameters = new HashMap();

			System.out.println("parameters");

			// parameters.put("subreportdata", jrBeanCollectionDataSource2);
			parameters.put("taxsliptup", "jrxml/taxsplits.jasper");
			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("branchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("EmailId", branchMst.getBranchEmail());
			parameters.put("GST", branchMst.getBranchGst());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());

			parameters.put("Currency", SystemSetting.getUser().getCompanyMst().getCurrencyName());
			String amount = "0.0";
			if (PurchaseInvoiceReport.size() > 0) {
				if (null != PurchaseInvoiceReport.get(0)) {
					amount = PurchaseInvoiceReport.get(0).getAmountTotal() + "";
				}
			}

			if (purchaseHdr.getCompanyMst().getCurrencyName().equalsIgnoreCase("MVR")) {
				String amountinwords = SystemSetting.AmountInWords(amount, "Rufiyaa", "Larri");
				parameters.put("invoiceAmount", amountinwords);
				System.out.println("amountinwords="+amountinwords);

			} else {
				String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");
				parameters.put("invoiceAmount", amountinwords);
				System.out.println("amountinwords="+amountinwords);

			}


			parameters.put("discount", purchaseHdr.getDiscount());

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
			hs.showDocument(pdfToGenerate);
			
			System.out.println("pdfToGenerate="+pdfToGenerate);

		} catch (JRException e) {
			e.printStackTrace();
		}

	}

	public static void importPurchaseInvoice(String id) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/importpurchaseinvoice" + ".pdf";

		// Call url
		ResponseEntity<List<PurchaseReport>> importPurchaseInvoiceDetails = RestCaller.getImportPurchaseInvoice(id);
		List<PurchaseReport> importPurchaseInvoiceDetailsList = importPurchaseInvoiceDetails.getBody();

		ResponseEntity<PurchaseHdr> purchaseHdrResp = RestCaller.getPurchaseHdr(id);
		PurchaseHdr purchaseHdr = purchaseHdrResp.getBody();

		PurchaseReport purchaseReport = new PurchaseReport();

		ResponseEntity<Double> totalImportExpense = RestCaller.getTotalImportExpense(id);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				importPurchaseInvoiceDetailsList);

		String jrxmlPath = "jrxml/ImportPurchaseInvoice.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("GST", branchMst.getBranchGst());

		parameters.put("itemTotalInFC", 0.0);
		parameters.put("itemTotalInLC", 0.0);
		parameters.put("expenseInBillLC", 0.0);
		parameters.put("expenseInBillFC", 0.0);
		parameters.put("expenseNotInBill", 0.0);
		parameters.put("totalImportDuty", 0.0);

		BigDecimal itemTotalInLC = new BigDecimal(0.0);
		BigDecimal expenseInBillLC = new BigDecimal(0.0);
		BigDecimal totalImportDutyLc = new BigDecimal(0.0);
		BigDecimal totalExpenseNotInBill = new BigDecimal(0.0);

		if (null != purchaseHdr) {
			if (null != purchaseHdr.getFcInvoiceTotal()) {
				parameters.put("itemTotalInFC", importPurchaseInvoiceDetailsList.get(0).getAmount());
			}
			if (null != purchaseHdr.getInvoiceTotal()) {
				itemTotalInLC = new BigDecimal(purchaseHdr.getInvoiceTotal());
				parameters.put("itemTotalInLC", purchaseHdr.getInvoiceTotal());
			}

		}

		ResponseEntity<CurrencyMst> currencyMstResp = RestCaller
				.getCurrencyMstByName(SystemSetting.getUser().getCompanyMst().getCurrencyName());

		CurrencyMst currencyMst = currencyMstResp.getBody();

		Double additionalExpenseTotalInFc = RestCaller.getSumOfAdditionalExpenseByExpenseTyeInFc(purchaseHdr.getId(),
				"INCLUDED IN BILL");

		if (null != additionalExpenseTotalInFc) {
			parameters.put("expenseInBillFC", additionalExpenseTotalInFc);

		} else {
			additionalExpenseTotalInFc = 0.0;
		}

		if(!purchaseHdr.getCurrency().equalsIgnoreCase("")) {

		String fromCurrency = purchaseHdr.getCurrency();
		String toCurrency = SystemSetting.getUser().getCompanyMst().getCurrencyName();

		if (null != purchaseHdr.getCurrency() && !purchaseHdr.getCurrency().trim().isEmpty()) {

			Double additionalExpenseTotal = RestCaller.getCurrencyConvertedAmount(fromCurrency, toCurrency,
					additionalExpenseTotalInFc);

			if (null != additionalExpenseTotal) {
				expenseInBillLC = new BigDecimal(additionalExpenseTotal);
				parameters.put("expenseInBillLC", additionalExpenseTotal);

			}
		}
		}

		Double additionalExpenseTotalNotInBill = RestCaller.getSumOfAdditionalExpenseByExpenseTye(purchaseHdr.getId(),
				"NOT IN BILL");
		if (null != additionalExpenseTotalNotInBill) {
			totalExpenseNotInBill = new BigDecimal(additionalExpenseTotalNotInBill);
			parameters.put("expenseNotInBill", additionalExpenseTotalNotInBill);

		}

		ResponseEntity<Double> totalImportDutyResp = RestCaller.getTotalImportExpense(purchaseHdr.getId());
		Double totalImportDuty = totalImportDutyResp.getBody();

		if (null != totalImportDuty) {
			totalImportDutyLc = new BigDecimal(totalImportDuty);
			parameters.put("totalImportDuty", totalImportDuty);

		}

		BigDecimal totalBillAmount = totalImportDutyLc
				.add(itemTotalInLC.add(expenseInBillLC.add(totalExpenseNotInBill)));

		totalBillAmount = totalBillAmount.setScale(0, BigDecimal.ROUND_CEILING);

		parameters.put("totalBillAmount", totalBillAmount);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

//	public static void importPurchaseInvoiceSubreport(Double totalAdditionalExpense, Double totalAmountLocalCurrency,
//			Double totalImportExpense,String id)throws JRException {
//		String pdfToGenerate = SystemSetting.reportPath + "/importpurchaseinvoice" + ".pdf";
//
//		// Call url
//	    ResponseEntity<List<PurchaseReport>> importPurchaseInvoiceDetails = RestCaller.
//	    		getImportPurchaseInvoice(id);
//
//
//
//		List<PurchaseReport> importPurchaseInvoiceDetailsList = importPurchaseInvoiceDetails.getBody();
//	
//		
//		PurchaseReport purchaseReport=new PurchaseReport();
//
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(importPurchaseInvoiceDetailsList);
//
//		String jrxmlPath = "jrxml/importPurcaseInvoiceSubreport.jrxml";
//
//		// Compile the Jasper report from .jrxml to .japser
//		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
//
//		HashMap parameters = new HashMap();
//
//		
////		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
////		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
////		parameters.put("branchName", branchMst.getBranchName());
////		parameters.put("Address1", branchMst.getBranchAddress1());
////		parameters.put("Address2", branchMst.getBranchAddress2());
////		parameters.put("GST", branchMst.getBranchGst());
////		
//		
//		parameters.put("totalFCAmount", 0.0);
//		parameters.put("localcurrency", 0.0);
//		if(null!=totalAdditionalExpense) {
//		parameters.put("totalAdditionalExpense", totalAdditionalExpense);
//		}
//		if(null!=totalAmountLocalCurrency) {
//		parameters.put("totalAmountLocalCurrency", totalAmountLocalCurrency);
//		}
//		if(null!=totalImportExpense) {
//		parameters.put("totalImportExpense", totalImportExpense);
//		}
//		
//		// Fill the report
//		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
//		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
//
//		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
//
//		hs.showDocument(pdfToGenerate);
//		
//	}

	// =============================================AMDCShortExpiryReport===============================
	public static void AMDCShortExpiryReport(String date) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/AMDC_Short_Expiry_Report.pdf";

		// Call urld

		ResponseEntity<List<AMDCShortExpiryReport>> AMDCShortExpiryReportResponse = RestCaller
				.getAMDCShortExpiryReport(date);

		List<AMDCShortExpiryReport> AMDCShortExpiryReport = AMDCShortExpiryReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("date", date);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(AMDCShortExpiryReport);

		String jrxmlPath = "jrxml/AMDC_Short_Expiry_Report.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	// =======================SHARON======================PoliceReport===============================
	public static void PoliceReport(String fromdate, String todate, String cust) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/Police_Report.pdf";

		// Call urld

		ResponseEntity<List<PoliceReport>> policeReportResponse = RestCaller.getPoliceReport(fromdate, todate,cust);

		List<PoliceReport> policeReport = policeReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromdate", fromdate);
		parameters.put("todate", todate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(policeReport);

		String jrxmlPath = "jrxml/Police_Report.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void BalanceSheetConfigReport(String currentDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/balancesheetconfigreport.pdf";

		// Call urld

		ResponseEntity<List<BalanceSheetAssetReport>> balanceSheetConfigAssetResponse = RestCaller
				.getBalanceSheetConfigAsset(currentDate);

		List<BalanceSheetAssetReport> balanceSheetAssetReport = balanceSheetConfigAssetResponse.getBody();

		ResponseEntity<List<BalanceSheetLiabilityReport>> balanceSheetConfigLiabilityResponse = RestCaller
				.getBalanceSheetConfigLiability(currentDate);

		List<BalanceSheetLiabilityReport> balanceSheetLiabilityReport = balanceSheetConfigLiabilityResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Address2", branchMst.getBranchAddress2());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromdate", currentDate);
		// parameters.put("todate", todate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(balanceSheetAssetReport);
		JRBeanCollectionDataSource jrBeanCollectionDataSource1 = new JRBeanCollectionDataSource(
				balanceSheetLiabilityReport);

		String jrxmlPath = "jrxml/BalanceSheetConfigReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		parameters.put("subreport", jrBeanCollectionDataSource1);
		parameters.put("BalanceSheetLiabilitySubReport", "jrxml/BalanceSheetLiabilitySubReport.jasper");

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void ProfitAndLossConfigReport(String date) throws JRException {

		// voucherNumber = "000173";
		// date = "2019-07-21";
		System.out.println("======ProfitAndLossReport===========");

		String pdfToGenerate = SystemSetting.reportPath + "/ProfitAndLossReport" + ".pdf";
		// Call url
		ResponseEntity<List<IncomeReport>> incomeResponse = RestCaller.getIncomeReport(date);
		List<IncomeReport> incomeReport = incomeResponse.getBody();

		ResponseEntity<List<ExpenseReport>> expenseReportResp = RestCaller.getExpenseReport(date);

		List<ExpenseReport> expenseReport = expenseReportResp.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(incomeReport);

		JRBeanCollectionDataSource jrBeanCollectionDataSource1 = new JRBeanCollectionDataSource(expenseReport);

		String jrxmlPath = "jrxml/Profit_And_Loss_Report.jrxml";
	}

	public static void AMDCDailySalesSummaryReport(String currentDate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/PharmacyDailySalesSummaryReport.pdf";

		// Call urld

		ResponseEntity<List<AMDCDailySalesSummaryReport>> aMDCDailySalesSummaryReportResponse = RestCaller
				.getAMDCDailySalesSummaryReport(currentDate);

		List<AMDCDailySalesSummaryReport> aMDCDailySalesSummaryReport = aMDCDailySalesSummaryReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
	
		parameters.put("fromdate", currentDate);
		// parameters.put("todate", todate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				aMDCDailySalesSummaryReport);

		String jrxmlPath = "jrxml/PharmacyDailySalesSummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void PharmacySaleOrderInvoice(String vNo, String vdate) throws JRException {
		System.out.println("======PharmacySaleOrderInvoice===========");

		String pdfToGenerate = SystemSetting.reportPath + "/SaleOrderInvoice" + vNo + ".pdf";

		// Call url

		ResponseEntity<List<SaleOrderInvoiceReport>> invoiceResponse = RestCaller.getSaleOrderInvoice(vNo, vdate);
		List<SaleOrderInvoiceReport> invoice = invoiceResponse.getBody();

		SalesOrderTransHdr salesOrderTransHdrResp = RestCaller.getSalesOrderTransHdrByVoucherAndDate(vNo, vdate);

		ResponseEntity<List<TermsAndConditionsMst>> termsAndConditionMstResp = RestCaller
				.getTermsAndConditionMstByInvoiceFormat("PharmacySaleOrderInvoice");
		List<TermsAndConditionsMst> termsAndConditionMstList = termsAndConditionMstResp.getBody();

		ResponseEntity<List<SalesOrderDtl>> SalesReceiptsResp = RestCaller
				.getSalesOrderDtlByOrderTransHdrId(salesOrderTransHdrResp.getId());

		List<SalesOrderDtl> salesReceipts = SalesReceiptsResp.getBody();

		// Compile the Jasper report from .jrxml to .japser

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);

		JRBeanCollectionDataSource jrBeanCollectionDataSource1 = new JRBeanCollectionDataSource(salesReceipts);

		String jrxmlPath = "jrxml/PharmacySaleOrderInvoice.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		if (null != salesReceipts) {
			parameters.put("igstTaxRate", salesReceipts.get(0).getIgstTaxRate());
			parameters.put("igstAmount", salesReceipts.get(0).getIgstAmount());

		}

		if (null != salesOrderTransHdrResp) {

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("branchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("EmailId", branchMst.getBranchEmail());
			parameters.put("GST", branchMst.getBranchGst());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());

			parameters.put("subreport", jrBeanCollectionDataSource1);
			parameters.put("SubReportExpense", "jrxml/SubReportExpense.jasper");
//			parameters.put("CustomerPo", salesOrderTransHdrResp.getPoNumber());
//			parameters.put("CustomerTIN", salesOrderTransHdrResp.getCustomerMst().getCustomerGst());

			parameters.put("subreportdata", jrBeanCollectionDataSource1);
			parameters.put("subsalesorderinvoice", "jrxml/PharmacySalesOrderInvoiceSubReport.jasper");

			String amount = "0.0";
			if (invoice.size() > 0) {
				if (null != invoice.get(0)) {
					amount = invoice.get(0).getAmount() + "";
				}
			}
			String amountinwords = SystemSetting.AmountInWords(amount, "Rufiyaa", "Larri");
			parameters.put("invoiceAmount", amountinwords);

			if (termsAndConditionMstList.size() > 0) {
				int length = termsAndConditionMstList.size();
				if (length >= 1 && null != termsAndConditionMstList.get(0)) {
					parameters.put("termsAndCondition1", "1. " + termsAndConditionMstList.get(0).getConditions());
				}
				if (length >= 2 && null != termsAndConditionMstList.get(1)) {
					parameters.put("termsAndCondition2", "2. " + termsAndConditionMstList.get(1).getConditions());
				}
				if (length >= 3 && null != termsAndConditionMstList.get(2)) {
					parameters.put("termsAndCondition3", "3. " + termsAndConditionMstList.get(2).getConditions());
				}
				if (length >= 4 && null != termsAndConditionMstList.get(3)) {
					parameters.put("termsAndCondition4", "4. " + termsAndConditionMstList.get(3).getConditions());
				}
				if (length >= 5 && null != termsAndConditionMstList.get(4)) {
					parameters.put("termsAndCondition5", "5. " + termsAndConditionMstList.get(4).getConditions());
				}
				if (length >= 6 && null != termsAndConditionMstList.get(5)) {
					parameters.put("termsAndCondition6", "6. " + termsAndConditionMstList.get(5).getConditions());
				}
				if (length >= 6 && null != termsAndConditionMstList.get(6)) {
					parameters.put("termsAndCondition7", "7. " + termsAndConditionMstList.get(6).getConditions());
				}
			}

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
					jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
		}
	}


	public static void PharmacyNewCustomerWiseSaleReport(String sfdate, String stdate, String cust) throws JRException {
		


		String pdfToGenerate = SystemSetting.reportPath + "/AMDCCustomerWiseSalesReport.pdf";

		// Call urld

		ResponseEntity<List<PharmacyNewCustomerWiseSaleReport>> amdccustomerWiseSaleReportResponse = RestCaller
				.getAMDCCustomerWiseSaleReport(sfdate,stdate,cust);


		List<PharmacyNewCustomerWiseSaleReport> amdccustomerWiseSaleReport = amdccustomerWiseSaleReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("Fromdate", sfdate);
		parameters.put("todate", stdate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				amdccustomerWiseSaleReport);

		String jrxmlPath = "jrxml/AMDCCustomerWiseSalesReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}
//  	public static void IncomeAndExpenseReport( String date) throws JRException {
//
//		// voucherNumber = "000173";
//		// date = "2019-07-21";
//		System.out.println("======ProfitAndLossReport===========");
//
//		String pdfToGenerate = SystemSetting.reportPath + "/ProfitAndLossReport"  + ".pdf";
//		// Call url
//		ResponseEntity<List<IncomeReport>> incomeResponse = RestCaller.getIncomeReport( date);
//		List<IncomeReport> incomeReport = incomeResponse.getBody();
//		
//		
//		ResponseEntity<List<ExpenseReport>> expenseReportResp = RestCaller
//				.getExpenseReport(date);
//
//		List<ExpenseReport> expenseReport = expenseReportResp.getBody();
//
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(incomeReport);
//
//		JRBeanCollectionDataSource jrBeanCollectionDataSource1 = new JRBeanCollectionDataSource(expenseReport);
//
//		String jrxmlPath = "jrxml/Profit_And_Loss_Report.jrxml";
//
//		// Compile the Jasper report from .jrxml to .japser
//		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
//
//		HashMap parameters = new HashMap();
//
//			
//			
//			
//
//			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
//			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//			parameters.put("branchName", branchMst.getBranchName());
//			parameters.put("Address1", branchMst.getBranchAddress1());
//			parameters.put("Address2", branchMst.getBranchAddress2());
//			parameters.put("EmailId", branchMst.getBranchEmail());
//			parameters.put("GST", branchMst.getBranchGst());
//			parameters.put("Phone", branchMst.getBranchTelNo());
//			parameters.put("State", branchMst.getBranchState());
//
//			
//			
//			parameters.put("subreport", jrBeanCollectionDataSource1);
//			parameters.put("SubReportExpense", "jrxml/SubReportExpense.jasper");
//
//			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
//					jrBeanCollectionDataSource);
//			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
//
//			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
//
//			hs.showDocument(pdfToGenerate);
//
//		}
	
	
	//=====================BIN CARD REPORT=============////
	
	
//	public static void BinCardReport(String fromdate, String todate) throws JRException {
//		String pdfToGenerate = SystemSetting.reportPath + "/BinCardReport.pdf";
//
//		// Call urld
//
//		ResponseEntity<List<BinCardReport>> binCardReportResponse = RestCaller.getBinCardReport(fromdate, todate);
//
//		List<BinCardReport> binCardReport = binCardReportResponse.getBody();
//
//		HashMap parameters = new HashMap();
//
//		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
//		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//		parameters.put("branchName", branchMst.getBranchName());
//		parameters.put("Address1", branchMst.getBranchAddress1());
//		parameters.put("Address2", branchMst.getBranchAddress2());
//		parameters.put("State", branchMst.getBranchState());
//		parameters.put("GST", branchMst.getBranchGst());
//		parameters.put("fromdate", fromdate);
//		parameters.put("todate", todate);
//
//		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(binCardReport);
//
//		String jrxmlPath = "jrxml/BinCardReport.jrxml";
//
//		// Compile the Jasper report from .jrxml to .japser
//		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
//
//		/// HashMap parameters = null;
//		// Fill the report
//		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
//		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);
//
//		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();
//
//		hs.showDocument(pdfToGenerate);
//
//	}
	
	public static void localPurchaseSummaryDtlReport(String fromdate, String todate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/localPurchaseSummaryDtlReport.pdf";

		// Call urld

		ResponseEntity<List<LocalPurchaseSummaryReport>> localPurchaseSummaryDtlResponse = RestCaller.getlocalPurchaseSummaryDtlReport(fromdate, todate);

		List<LocalPurchaseSummaryReport> localPurchaseSummaryDtl = localPurchaseSummaryDtlResponse.getBody();

		HashMap parameters = new HashMap();

//		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
//		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//		parameters.put("branchName", branchMst.getBranchName());
//		parameters.put("Address1", branchMst.getBranchAddress1());
//		parameters.put("Address2", branchMst.getBranchAddress2());
//		parameters.put("State", branchMst.getBranchState());
//		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromdate", fromdate);
		parameters.put("todate", todate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(localPurchaseSummaryDtl);

		String jrxmlPath = "jrxml/LocalPurchaseSummaryReportDtl.jrxml";
	
	
		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}
	
	

	public static void PharmacyLocalPurchaseDetailsReports(String sdate, String edate) throws JRException {
		
		String pdfToGenerate = SystemSetting.reportPath + "/PharmacyLocalPurchaseDetailReport"+sdate+".pdf";

		// Call urld

		ResponseEntity<List<PharmacyLocalPurchaseDetailsReport>> PharmacyLocalPurchaseDetailsResponse = RestCaller.getPharmacyLocalPurchaseDetailsReport(sdate, edate);

		List<PharmacyLocalPurchaseDetailsReport> PharmacyLocalPurchaseDetails = PharmacyLocalPurchaseDetailsResponse.getBody();

		HashMap parameters = new HashMap();

		
		
//		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
//		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//		parameters.put("branchName", branchMst.getBranchName());
//		parameters.put("Address1", branchMst.getBranchAddress1());
//		parameters.put("Address2", branchMst.getBranchAddress2());
//		parameters.put("State", branchMst.getBranchState());
//		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromdate", sdate);
		parameters.put("todate", edate);
		
		
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(PharmacyLocalPurchaseDetails);

		String jrxmlPath = "jrxml/PharmacyLocalPurchaseDetailsReport.jrxml";
	
		

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
		
		
	}

	public static void PharmacyBranchStockMarginReports(String sdate, String edate, String branchCode) throws JRException  {
	
		String pdfToGenerate = SystemSetting.reportPath + "/PharmacyBranchStockMarginReport"+sdate+".pdf";

		// Call urld

		ResponseEntity<List<PharmacyBrachStockMarginReport>> pharmacyBrachStockMarginReportResponse = RestCaller.getPharmacyBranchStockMarginReport(sdate, edate,branchCode);

		List<PharmacyBrachStockMarginReport> pharmacyBrachStockMarginDetails = pharmacyBrachStockMarginReportResponse.getBody();

		HashMap parameters = new HashMap();
		
		
		
//		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
//		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//		parameters.put("branchName", branchMst.getBranchName());
//		parameters.put("Address1", branchMst.getBranchAddress1());
//		parameters.put("Address2", branchMst.getBranchAddress2());
//		parameters.put("State", branchMst.getBranchState());
//		parameters.put("GST", branchMst.getBranchGst());
		parameters.put("fromdate", sdate);
		parameters.put("todate", edate);
		
		
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(pharmacyBrachStockMarginDetails);

		String jrxmlPath = "jrxml/PharmacyBrachStockMarginReport.jrxml";
	
		

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
		
		
		
	}

	public static void writeOffSummaryReport(String fDate, String tDate, String systemBranch, String reason) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/WriteOffSummaryReport.pdf";

		// Call urld

		ResponseEntity<List<WriteOffDetailAndSummaryReport>> writeOffDetailAndSummaryReportResponse = RestCaller.getWriteOffSummary(fDate, tDate,systemBranch,reason);

		List<WriteOffDetailAndSummaryReport> writeOffDetailAndSummaryReport = writeOffDetailAndSummaryReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromDate", fDate);
		parameters.put("toDate", tDate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(writeOffDetailAndSummaryReport);

		String jrxmlPath = "jrxml/WriteOffSummaryReportNew.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void writeOffDetailReport(String fDate, String tDate, String systemBranch, String reason) throws JRException {
		
		String pdfToGenerate = SystemSetting.reportPath + "/WriteOffDetailReport.pdf";

		// Call urld

		ResponseEntity<List<WriteOffDetailAndSummaryReport>> writeOffDetailAndSummaryReportResponse = RestCaller.getWriteOffSummary(fDate, tDate,systemBranch,reason);

		List<WriteOffDetailAndSummaryReport> writeOffDetailAndSummaryReport = writeOffDetailAndSummaryReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromDate", fDate);
		parameters.put("toDate", tDate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(writeOffDetailAndSummaryReport);

		String jrxmlPath = "jrxml/WriteOffDetailReportNew.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
		
		
	}
	//------------------------------------pharmacy day closing -----------------sharon --------------
	public static void PharmacyDayClosingReport(String fdate) throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/Pharmacy_Day_Closing_Report.pdf";

		// Call urld

		ResponseEntity<List<PharmacyDayClosingReport>> pharmacyDayClosingReportResponse = RestCaller
				.getPharmacyDayClosingReport(fdate);

		List<PharmacyDayClosingReport> pharmacyDayClosingReport = pharmacyDayClosingReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
		parameters.put("branchName", branchMst.getBranchName());
		
		parameters.put("date", fdate);
		

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(pharmacyDayClosingReport);

		String jrxmlPath = "jrxml/Pharmacy_Day_Closing_Report.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}
	// =======================SHARON======================PharmacyDailySalesTransactionReport===============================
	public static void PharmacyDailySalesTransactionReport(String fromdate, String todate) throws JRException{
		String pdfToGenerate = SystemSetting.reportPath + "/Pharmacy_Daily_Sale_Transaction.pdf";

		// Call urld

		ResponseEntity<List<PharmacyDailySalesTransactionReport>> PharmacyDailySalesTransactionReportResponse = RestCaller.getPharmacyDailySalesTransactionReport(fromdate, todate);

		List<PharmacyDailySalesTransactionReport> DailySalesTransactionReport = PharmacyDailySalesTransactionReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromdate", fromdate);
		parameters.put("ToDate", todate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(DailySalesTransactionReport);

		String jrxmlPath = "jrxml/Pharmacy_Daily_Sale_Transaction.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
		
	}
	// =======================SHARON======================PharmacyItemWiseSalesReport===============================
	public static void PharmacyItemWiseSalesReport(String fromdate, String todate, String itename)throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/Pharmacy_ItemWise_SalesReport.pdf";

		// Call urld

		//ResponseEntity<List<PharmacyItemWiseSalesReport>> PharmacyItemWiseSalesReportResponse = RestCaller.getPharmacyItemWiseSalesReport(fromdate, todate);
		ResponseEntity<List<PharmacyItemWiseSalesReport>> PharmacyItemWiseSalesReportResponse = RestCaller.getPharmacyItemWiseSalesReportByItemName(fromdate, todate,itename);
		List<PharmacyItemWiseSalesReport> ItemWiseSalesReport = PharmacyItemWiseSalesReportResponse.getBody();

	
		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromDate", fromdate);
		parameters.put("toDate", todate);

		
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(ItemWiseSalesReport);

		String jrxmlPath = "jrxml/Pharmacy_ItemWise_SalesReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void retailSalesDetailREport(String sfdate, String stdate)throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/Retail_Sales_Detail_Report.pdf";

		// Call urld

		ResponseEntity<List<RetailSalesDetailReport>> RetailSalesDetailReportResponse = RestCaller.getRetailSalesDetailReport(sfdate, stdate);

		List<RetailSalesDetailReport> retailSalesDetailReport = RetailSalesDetailReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromDate", sfdate);
		parameters.put("toDate", stdate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(retailSalesDetailReport);

		String jrxmlPath = "jrxml/RetailSalesDetailReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
		
	}

	public static void retailSalesSummaryREport(String sfdate, String stdate)throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/Retail_Sales_Summary_Report.pdf";

		// Call urld

		ResponseEntity<List<RetailSalesSummaryReport>> RetailSalesSummaryReportResponse = RestCaller.getRetailSalesSummaryReport(sfdate, stdate);

		List<RetailSalesSummaryReport> retailSalesSummaryReport = RetailSalesSummaryReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromDate", sfdate);
		parameters.put("toDate", stdate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(retailSalesSummaryReport);

		String jrxmlPath = "jrxml/RetailSalesSummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}
	

	public static void purchaseReturnDetailAndSummary(String sfdate, String stdate) throws JRException{
		String pdfToGenerate = SystemSetting.reportPath + "/PurchaseReturnSummaryReport.pdf";

		// Call urld

		ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> PurchaseReturnDetailAndSummaryReportResponse = RestCaller.getPurchaseReturnDetailAndSummary(sfdate, stdate);

		List<PurchaseReturnDetailAndSummaryReport> purchaseReturnDetailAndSummaryReport = PurchaseReturnDetailAndSummaryReportResponse.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromDate", sfdate);
		parameters.put("toDate", stdate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(purchaseReturnDetailAndSummaryReport);

		String jrxmlPath = "jrxml/PurchaseReturnSummaryReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}
//-------------------------------------PurchaseReturnDetailReport for Pharmacy----------------------------------------------------------------
	public static void PurchaseReturnDetailReport(String fromdate, String todate)throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/Pharmacy_PurchaseReturnDetailReport.pdf";

		// Call urld

		ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> purchaseReturndtlReportResp = RestCaller.getPurchaseReturnDetailAndSummary(fromdate, todate);
//		ResponseEntity<List<PurchaseReturnDetailAndSummaryReport>> purchaseReturndtlReportResp = RestCaller.getPurchaseReturnDetailReport(fromdate, todate);


		List<PurchaseReturnDetailAndSummaryReport> purchaseReturnDetailReport = purchaseReturndtlReportResp.getBody();

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("fromdate", fromdate);
		parameters.put("todate", todate);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(purchaseReturnDetailReport);

		String jrxmlPath = "jrxml/Pharmacy_PurchaseReturnDetailReport.jrxml";
		
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
		
	
	}
	
	public static void PharmacyDayEndReport(String fromdate)throws JRException {
		String pdfToGenerate = SystemSetting.reportPath + "/PharmacyDayEndReport.pdf";

		// Call urld
		String branchCash=SystemSetting.getUser().getBranchCode()+"-CASH";

		ResponseEntity<List<PharmacyDayEndReport>> pharmacyDayEndReportResponse = RestCaller.getPharmacyDayEndReport(fromdate, branchCash);

		List<PharmacyDayEndReport> SalesTransHdrReport = pharmacyDayEndReportResponse.getBody();

		
		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		String accountName =RestCaller.getAccountIdByAccountName(branchCash);
		
		Double cashSales =RestCaller.getPharmacyCashSales(fromdate, branchCash);
		Double cashRevceived=RestCaller.getPharmacyCashRevceived(fromdate, branchCash);
		Double cashPaid=RestCaller.getPharmacyCashPaid(fromdate, branchCash);
		Double openingCash=RestCaller.getPharmacyOpeningCash(fromdate, accountName);
		Double closingCash=RestCaller.getPharmacyClosingCash(fromdate, accountName);
		parameters.put("fromDate", fromdate);
		parameters.put("cashSales", cashSales);
		parameters.put("cashRevceived", cashRevceived);
		parameters.put("cashPaid", cashPaid);
		parameters.put("openingCash", openingCash);
		parameters.put("closingCash", closingCash);
		
		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(SalesTransHdrReport);

		String jrxmlPath = "jrxml/PharmacyDayEnd.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		/// HashMap parameters = null;
		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}
	//-------------END-----------------------PurchaseReturnDetailReport for Pharmacy----------------------------------------------------------------

	// =======================SHARON======================PhysicalStockVarianceReport==  for Pharmacy=============================
		public static void PhysicalStockVarianceReport(String fromdate, String todate) throws JRException {
			String pdfToGenerate = SystemSetting.reportPath + "/Pharmacy_PhysicalStockVariance_Report.pdf";

			// Call urld

			ResponseEntity<List<PharmacyPhysicalStockVarianceReport>> physicalStockVarianceReportResponse = RestCaller.getPharmacyPhysicalStockVarianceReport(fromdate, todate);

			List<PharmacyPhysicalStockVarianceReport> physicalStockVarianceReport = physicalStockVarianceReportResponse.getBody();

			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("fromdate", fromdate);
			parameters.put("todate", todate);

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(physicalStockVarianceReport);

			String jrxmlPath = "jrxml/Pharmacy_PhysicalStockVariance_Report.jrxml";

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			/// HashMap parameters = null;
			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);

		}






		//------------------------------------pharmacy import purchase report-----------------sharon ---july 7 testing-----------
		public static void PharmacyImportPurchaseSummaryReport(String fdate, String tdate) throws JRException {
			String pdfToGenerate = SystemSetting.reportPath + "/ImportPurchaseSummarycopy.pdf";

			// Call urld

			ResponseEntity<List<ImportPurchaseSummary>> importPurchaseSummary = RestCaller
					.getImportPurchaseSummaryReport(fdate, tdate);
			List<ImportPurchaseSummary> importPurchaseSummaryReport = importPurchaseSummary.getBody();



			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//			parameters.put("branchName", branchMst.getBranchName());
			
			parameters.put("fromDate", fdate);
			parameters.put("Todate", tdate);
			

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(importPurchaseSummaryReport);

			String jrxmlPath = "jrxml/ImportPurchaseSummarycopy.jrxml";

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			/// HashMap parameters = null;

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);

		}

		
		//===========================categorywise stock report===========anandu=================09-07-2021===============
		public static void PrintCategoryWiseStockMovementReport(String strfDate, String strtDate) throws JRException {
			String pdfToGenerate = SystemSetting.reportPath + "/CategoryWiseStockMovementReport.pdf";

			// Call urld

			ResponseEntity<List<CategoryWiseStockMovement>> categoryWiseStockMovement = RestCaller
					.getCategoryWiseStockMovement(strfDate, strtDate);
			List<CategoryWiseStockMovement> categoryWiseStockMovementReport = categoryWiseStockMovement.getBody();



			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//			parameters.put("branchName", branchMst.getBranchName());
			
			parameters.put("fromDate", strfDate);
			parameters.put("Todate", strtDate);
			

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(categoryWiseStockMovementReport);

			String jrxmlPath = "jrxml/CategoryWiseStockReportNew.jrxml";

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			/// HashMap parameters = null;

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
		}


		public static void PrintStockMovementbyCategoryWiseSReport(String strfDate, String strtDate, String cat)  throws JRException {
		
			String pdfToGenerate = SystemSetting.reportPath + "/CategoryWiseStockMovementReport.pdf";

			// Call urld

			ResponseEntity<List<CategoryWiseStockMovement>> categoryWiseStockMovement = RestCaller
					.getPharmacyStockMovementByCategorywise(strfDate, strtDate,cat);
			List<CategoryWiseStockMovement> categoryWiseStockMovementReport = categoryWiseStockMovement.getBody();



			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
//			parameters.put("branchName", branchMst.getBranchName());
			
			parameters.put("fromDate", strfDate);
			parameters.put("Todate", strtDate);
			

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(categoryWiseStockMovementReport);

			String jrxmlPath = "jrxml/CategoryWiseStockReportNew.jrxml";

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
		}

			/// HashMap parameters = null;

		public static void pharmacySalesReturnDetailReport(String fdate, String tDate, String branchcode, String cat)throws JRException {
			String pdfToGenerate = SystemSetting.reportPath + "/PharmacySalesReturnDetailReport" + fdate + ".pdf";

			// Call url

			ResponseEntity<List<PharmacySalesReturnReport>> pharmacySalesReturnDetailReportResponse = RestCaller
					.getPharmacySalesReturn(fdate, tDate, branchcode, cat);
			List<PharmacySalesReturnReport> pharmacySalesReturnDetailReportList = pharmacySalesReturnDetailReportResponse.getBody();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					pharmacySalesReturnDetailReportList);

			String jrxmlPath = "jrxml/PharmacySalesReturnDetailReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser

			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(branchcode);
			parameters.put("FromDate", fdate);
			parameters.put("ToDate", tDate);

			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
		}

		public static void pharmacySalesReturnSummaryReport(String fdate, String tDate, String branchcode, String cat)throws JRException {
			String pdfToGenerate = SystemSetting.reportPath + "/PharmacySalesReturnSummaryReport" + fdate + ".pdf";

			// Call url

			ResponseEntity<List<PharmacySalesReturnReport>> pharmacySalesReturnSummaryReportResponse = RestCaller
					.getPharmacySalesReturn(fdate, tDate, branchcode, cat);
			List<PharmacySalesReturnReport> pharmacySalesReturnSummaryReportList = pharmacySalesReturnSummaryReportResponse.getBody();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					pharmacySalesReturnSummaryReportList);

			String jrxmlPath = "jrxml/PharmacySalesReturnSummaryReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser

			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(branchcode);
			
			parameters.put("FromDate", fdate);
			parameters.put("ToDate", tDate);

			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
		}

		public static void consumptionSummaryReport(String strDate, String strDate1, String cat,String username) throws JRException{
			String pdfToGenerate = SystemSetting.reportPath + "/PharmacyConsumptionSummaryReport" + strDate + ".pdf";

			// Call url

			ResponseEntity<List<ConsumptionReport>> pharmacyConsumptionSummaryReportResponse = RestCaller
					.getConsumptionSummary(strDate, strDate1, cat,username);
			List<ConsumptionReport> pharmacyConsumptionSummaryReportList = pharmacyConsumptionSummaryReportResponse.getBody();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					pharmacyConsumptionSummaryReportList);

			String jrxmlPath = "jrxml/ConsumptionSummaryReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser

			HashMap parameters = new HashMap();

//			BranchMst branchMst = RestCaller.getBranchDtls(branchcode);
			
			parameters.put("FromDate", strDate);
			parameters.put("ToDate", strDate1);

			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
		}
		
		public static void consumptionSummaryReportWithoutCategory(String strDate, String strDate1, String username) throws JRException{
			String pdfToGenerate = SystemSetting.reportPath + "/PharmacyConsumptionSummaryReport" + strDate + ".pdf";

			// Call url

			ResponseEntity<List<ConsumptionReport>> pharmacyConsumptionSummaryReportResponse = RestCaller
					.getConsumptionSummaryWithoutCategory(strDate, strDate1,username);
			List<ConsumptionReport> pharmacyConsumptionSummaryReportList = pharmacyConsumptionSummaryReportResponse.getBody();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					pharmacyConsumptionSummaryReportList);

			String jrxmlPath = "jrxml/ConsumptionSummaryReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser

			HashMap parameters = new HashMap();

//			BranchMst branchMst = RestCaller.getBranchDtls(branchcode);
			
			parameters.put("FromDate", strDate);
			parameters.put("ToDate", strDate1);

			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
		}

		public static void consumptionDetailReport(String strDate, String strDate1, String cat, String username) throws JRException{
			String pdfToGenerate = SystemSetting.reportPath + "/PharmacyConsumptionDetailReport" + strDate + ".pdf";

			// Call url

			ResponseEntity<List<ConsumptionReport>> getConsumptionDetailReportResponse = RestCaller
					.getConsumptionDetail( strDate,strDate1,cat,username);
			List<ConsumptionReport> pharmacyConsumptionDetailReportList = getConsumptionDetailReportResponse.getBody();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					pharmacyConsumptionDetailReportList);

			String jrxmlPath = "jrxml/ConsumptionDetailReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser

			HashMap parameters = new HashMap();

//			BranchMst branchMst = RestCaller.getBranchDtls(branchcode);
			
			parameters.put("FromDate", strDate);
			parameters.put("ToDate", strDate1);

			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
		}

		public static void consumptionDetailReportWithoutCategory(String strDate, String strDate1,String username) throws JRException{
			String pdfToGenerate = SystemSetting.reportPath + "/PharmacyConsumptionDetailReport" + strDate + ".pdf";

			// Call url

			ResponseEntity<List<ConsumptionReport>> getConsumptionDetailReportResponse = RestCaller
					.getConsumptionDetailWithoutCategory( strDate,strDate1,username);
			List<ConsumptionReport> pharmacyConsumptionDetailReportList = getConsumptionDetailReportResponse.getBody();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					pharmacyConsumptionDetailReportList);

			String jrxmlPath = "jrxml/ConsumptionDetailReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser

			HashMap parameters = new HashMap();

//			BranchMst branchMst = RestCaller.getBranchDtls(branchcode);
			
			parameters.put("FromDate", strDate);
			parameters.put("ToDate", strDate1);

			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
		}

		public static void pharmacySalesReturnSummaryReportWithoutCategory(String fdate, String tDate, String value) throws JRException{
			String pdfToGenerate = SystemSetting.reportPath + "/PharmacySalesReturnSummaryReport" + fdate + ".pdf";

			// Call url

			ResponseEntity<List<PharmacySalesReturnReport>> pharmacySalesReturnSummaryReportResponse = RestCaller
					.getPharmacySalesReturnWithoutCategory(fdate, tDate, value);
			List<PharmacySalesReturnReport> pharmacySalesReturnSummaryReportList = pharmacySalesReturnSummaryReportResponse.getBody();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					pharmacySalesReturnSummaryReportList);

			String jrxmlPath = "jrxml/PharmacySalesReturnSummaryReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser

			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(value);
			
			parameters.put("FromDate", fdate);
			parameters.put("ToDate", tDate);

			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);


			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
			
		}


		public static void PharmacyStockReportBranchWise(String startDate, String selectedItem)  throws JRException {
			
			
			String pdfToGenerate = SystemSetting.reportPath + "/BranchWiseStockReport.pdf";

			// Call urld

			ResponseEntity<List<BranchStockReport>> branchStockReportResponse = RestCaller.getPharmacyStockReportBranchWise(startDate,selectedItem);

			List<BranchStockReport> branchStockReport = branchStockReportResponse.getBody();

			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("branchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", branchMst.getBranchGst());
			parameters.put("date", startDate);

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchStockReport);

			String jrxmlPath = "jrxml/BranchStockReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			/// HashMap parameters = null;

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);

			
			
		}
		public static void pharmacySalesReturnDetailReportWithoutCategory(String fdate, String tDate, String value) throws JRException{
			String pdfToGenerate = SystemSetting.reportPath + "/PharmacySalesReturnDetailReport" + fdate + ".pdf";

			// Call url

			ResponseEntity<List<PharmacySalesReturnReport>> pharmacySalesReturnDetailReportResponse = RestCaller
					.getPharmacySalesReturnWithoutCategory(fdate, tDate, value);
			List<PharmacySalesReturnReport> pharmacySalesReturnDetailReportList = pharmacySalesReturnDetailReportResponse.getBody();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					pharmacySalesReturnDetailReportList);

			String jrxmlPath = "jrxml/PharmacySalesReturnDetailReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser

			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(value);
			parameters.put("FromDate", fdate);
			parameters.put("ToDate", tDate);

			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);
			
			
		}

		



		public static void PharmacyStockReportBranchWiseAndItems(String startDate, String selectedItem, String cat) throws JRException {
			
			String pdfToGenerate = SystemSetting.reportPath + "/Branch&CategoryWiseStockReport.pdf";

			// Call urld

			ResponseEntity<List<BranchStockReport>> branchStockReportResponse =RestCaller.getPharmacyStockReportBranchWiseAndItems(startDate,selectedItem,cat);

			List<BranchStockReport> branchStockReport = branchStockReportResponse.getBody();

			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("branchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());
			parameters.put("Address2", branchMst.getBranchAddress2());
			parameters.put("Phone", branchMst.getBranchTelNo());
			parameters.put("State", branchMst.getBranchState());
			parameters.put("GST", branchMst.getBranchGst());
			parameters.put("date", startDate);

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(branchStockReport);

			String jrxmlPath = "jrxml/BranchStockReport.jrxml";

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			/// HashMap parameters = null;

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);

			
			
		}

		public static void purchaseReturnInvoice(String purchaseReturnHdrId) throws JRException {
			
			

			String pdfToGenerate = SystemSetting.reportPath + "/PurchaseReturnInvoiceReport.pdf";

			// Call urld

			ResponseEntity<List<PurchaseReturnInvoice>> PurchaseReportResponse = RestCaller.getPurchaseReturnInvoice(purchaseReturnHdrId);

			List<PurchaseReturnInvoice> PurchaseReturnReport = PurchaseReportResponse.getBody();

			HashMap parameters = new HashMap();

		   BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		   Double totalAmount=0.0;
//		
			for (PurchaseReturnInvoice purchaseReturnInvoice:PurchaseReturnReport)
			{
				
				totalAmount=totalAmount+(purchaseReturnInvoice.getQty()*purchaseReturnInvoice.getUnitPrice()+purchaseReturnInvoice.getGst());
		
			}
      		parameters.put("totalAmount",totalAmount);
      		totalAmount=0.0;
//			parameters.put("Address1", branchMst.getBranchAddress1());
//			parameters.put("Address2", branchMst.getBranchAddress2());
//			parameters.put("Phone", branchMst.getBranchTelNo());
//			parameters.put("State", branchMst.getBranchState());
//			parameters.put("GST", branchMst.getBranchGst());
//			parameters.put("date", startDate);

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(PurchaseReturnReport);

			String jrxmlPath = "jrxml/PurchaseReturnInvoice.jrxml";

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			/// HashMap parameters = null;

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);

			
			
		}

	
		
		public static void ReceiptInvoice(String voucherNumber, String date) throws JRException {
			String pdfToGenerate = SystemSetting.reportPath + "/ReceiptVoucher"+ voucherNumber + ".pdf";

			// Call url
			

			ResponseEntity<List<ReceiptInvoice>> receiptInvoiceResponse = RestCaller.getreceipts(voucherNumber, date);

			List<ReceiptInvoice> receiptInvoiceList = receiptInvoiceResponse.getBody();

			ReceiptInvoice receiptInvoice = new ReceiptInvoice();

			JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
					receiptInvoiceList);

			String jrxmlPath = "jrxml/ReceiptVoucher.jrxml";

			// Compile the Jasper report from .jrxml to .japser
			JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

			HashMap parameters = new HashMap();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
			parameters.put("companyName", SystemSetting.getUser().getCompanyMst().getCompanyName());
			parameters.put("branchName", branchMst.getBranchName());
			parameters.put("Address1", branchMst.getBranchAddress1());

			

			// Fill the report
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
			JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

			HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

			hs.showDocument(pdfToGenerate);

		}
		
	
		
		
		
		
		
		
		
		




	}
	




