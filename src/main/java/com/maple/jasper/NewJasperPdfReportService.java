package com.maple.jasper;

import java.math.BigDecimal;
import java.sql.Connection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.MapleclientApplication;
import com.maple.mapleclient.controllers.ReceiptModeWiseReport;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CategoryWiseSalesReport;

import com.maple.mapleclient.entity.DailySalesReportDtl;
import com.maple.mapleclient.entity.DayEndClosureDtl;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.DayEndReportStore;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.ItemWiseDtlReport;
import com.maple.mapleclient.entity.InvoiceFormatMst;
import com.maple.mapleclient.entity.JobCardHdr;
import com.maple.mapleclient.entity.LocalCustomerMst;
import com.maple.mapleclient.entity.MemberMst;
import com.maple.mapleclient.entity.OtherBranchSalesTransHdr;
import com.maple.mapleclient.entity.PDCPayment;
import com.maple.mapleclient.entity.PDCReceipts;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.PaymentDtl;
import com.maple.mapleclient.entity.PaymentReports;
import com.maple.mapleclient.entity.ProductionDtlDtl;
import com.maple.mapleclient.entity.ReceiptDtl;
import com.maple.mapleclient.entity.ReceiptModeMst;
import com.maple.mapleclient.entity.SaleOrderReceipt;
import com.maple.mapleclient.entity.SalesOrderDtl;
import com.maple.mapleclient.entity.SalesOrderTransHdr;
import com.maple.mapleclient.entity.SalesPropertyTransHdr;
import com.maple.mapleclient.entity.SalesReport;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.entity.ServiceInHdr;
import com.maple.mapleclient.entity.SessionEndClosureMst;
import com.maple.mapleclient.entity.SessionEndDtl;
import com.maple.mapleclient.entity.TermsAndConditionsMst;
import com.maple.mapleclient.entity.UserMst;
import com.maple.mapleclient.entity.VoucherNumberDtlReport;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.AccountBalanceReport;
import com.maple.report.entity.ActualProductionReport;
import com.maple.report.entity.B2bSalesReport;
import com.maple.report.entity.CardReconcileReport;
import com.maple.report.entity.CustomerBalanceReport;
import com.maple.report.entity.DailyPaymentSummaryReport;
import com.maple.report.entity.DailyReceiptsSummaryReport;
import com.maple.report.entity.DailySaleSummaryReport;
import com.maple.report.entity.DailySalesReport;
import com.maple.report.entity.DamageEntryReport;
import com.maple.report.entity.DayBook;
import com.maple.report.entity.DayEndPettyCashPaymentAndReceipt;
import com.maple.report.entity.FinishedProduct;
import com.maple.report.entity.HsnCodeSaleReport;
import com.maple.report.entity.IntentInReport;
import com.maple.report.entity.IntentVoucherReport;
import com.maple.report.entity.InventoryReorderStatusReport;
import com.maple.report.entity.ItemLocationReport;
import com.maple.report.entity.ItemSaleReport;
import com.maple.report.entity.ItemStockReport;
import com.maple.report.entity.OrgMemberWiseReceiptReport;
import com.maple.report.entity.OrgMonthlyAccPayReport;
import com.maple.report.entity.OrgMonthlyAccountReport;
import com.maple.report.entity.OrgReceiptInvoice;
import com.maple.report.entity.PaymentVoucher;
import com.maple.report.entity.PendingAmountReport;
import com.maple.report.entity.ProductConversionReport;
import com.maple.report.entity.ProductionDtlsReport;
import com.maple.report.entity.PurchaseReport;
import com.maple.report.entity.RawMaterialIssueReport;
import com.maple.report.entity.RawMaterialReturnReport;
import com.maple.report.entity.ReceiptInvoice;
import com.maple.report.entity.ReceiptModeReport;
import com.maple.report.entity.SaleOrderDetailsReport;
import com.maple.report.entity.SaleOrderDueReport;
import com.maple.report.entity.SaleOrderInvoiceReport;
import com.maple.report.entity.SaleOrderReport;
import com.maple.report.entity.SaleOrderTax;
import com.maple.report.entity.SalesInvoiceReport;
import com.maple.report.entity.SalesModeWiseBillReport;
import com.maple.report.entity.SalesModeWiseSummaryreport;
import com.maple.report.entity.SalesOrderAdvanceBalanceReport;
import com.maple.report.entity.SalesReturnInvoiceReport;
import com.maple.report.entity.SalesTaxSplitReport;
import com.maple.report.entity.ServiceInReport;
import com.maple.report.entity.ServiceInvoice;
import com.maple.report.entity.ShortExpiryReport;
import com.maple.report.entity.StockReport;
import com.maple.report.entity.StockSummaryDtlReport;
import com.maple.report.entity.StockTransferOutReport;
import com.maple.report.entity.SummaryStatementOfAccountReport;
import com.maple.report.entity.SupplierBillwiseDueDayReport;
import com.maple.report.entity.SupplierMonthlySummary;
import com.maple.report.entity.TaxSummaryMst;

import javafx.application.HostServices;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class NewJasperPdfReportService {

	public static void TaxInvoiceReport(String voucherNumber, String date) throws JRException {

		System.out.println("======TaxInvoiceReport===========");

		String pdfToGenerate = SystemSetting.reportPath + "/invoice" + voucherNumber + ".pdf";

		// Call url

		ResponseEntity<List<SalesInvoiceReport>> invoiceResponse = RestCaller.getInvoice(voucherNumber, date);
		List<SalesInvoiceReport> invoice = invoiceResponse.getBody();

		ResponseEntity<List<TaxSummaryMst>> invoiceTaxResponse = RestCaller.getInvoiceTax(voucherNumber, date);
		List<TaxSummaryMst> invoiceTax = invoiceTaxResponse.getBody();

		ResponseEntity<List<TaxSummaryMst>> taxResponse = RestCaller.getInvoiceTotal(voucherNumber, date);
		List<TaxSummaryMst> invoiceTaxSum = taxResponse.getBody();

		ResponseEntity<List<SalesInvoiceReport>> customerResponse = RestCaller.getInvoiceCustomerDtl(voucherNumber,
				date);
		List<SalesInvoiceReport> custDetails = customerResponse.getBody();

		Double cessAmount = RestCaller.getCessAmountInTaxInvoiceReport(voucherNumber, date);

		Double taxableAmount = RestCaller.getTaxableInvoiceAmount(voucherNumber, date);

		SalesTransHdr getSalesHdr = RestCaller.getSalesTransHdrByVoucherAndDate(voucherNumber, date);

		ResponseEntity<List<SalesPropertyTransHdr>> salesPropertiesResp = RestCaller
				.getSalesPropertyTransHdrBySalesHdrId(getSalesHdr.getId());
		List<SalesPropertyTransHdr> salesProperties = salesPropertiesResp.getBody();

		// jrxmlPath = "jrxml/taxinvoicee.jrxml";

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(invoice);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(invoiceTax);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(invoiceTax);
		JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(salesProperties);

		HashMap<String, Object> parameters = new HashMap();

		String jrxmlPath = "";

		String gstType = "NO";
		String discount = "NO";
		String batch = "NO";

		if (invoice.get(0).getSalesMode().equalsIgnoreCase("B2B")) {

			gstType = "YES";
		}

		if (null != getSalesHdr.getAccountHeads().getCustomerDiscount()) {

			if (getSalesHdr.getAccountHeads().getCustomerDiscount() > 0) {
				discount = "YES";

			}

		}

		if (SystemSetting.wholesale_invoice_format.equalsIgnoreCase("BAKERY_WITH_BATCH")) {

			batch = "YES";
		}
		ResponseEntity<AccountHeads> customerMstResp = RestCaller.getAccountHeadsByNameAndBranchCode(
				custDetails.get(0).getCustomerName(),SystemSetting.systemBranch);
		AccountHeads customerMst = customerMstResp.getBody();

		if (null == customerMst) {
			ResponseEntity<AccountHeads> customerMstResp1 = RestCaller
					.getAccountHeadsByName(custDetails.get(0).getCustomerName());
			customerMst = customerMstResp1.getBody();
		}
		
		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("bankName", branchMst.getBankName());	
		parameters.put("accountNumber", branchMst.getAccountNumber());
		parameters.put("bankBranch", branchMst.getBankBranch());
		parameters.put("ifsc", branchMst.getIfsc());

		String priceTypeId = null;
		if (null != customerMst.getPriceTypeId()) {
			priceTypeId = customerMst.getPriceTypeId();
		}
		ResponseEntity<List<InvoiceFormatMst>> jasperByPriceTypeResp = RestCaller
				.getJasperByReportName(customerMst.getPriceTypeId(), gstType, discount, batch, customerMst.getId());

		List<InvoiceFormatMst> jasperList = jasperByPriceTypeResp.getBody();
		if(jasperList.size() > 0) {
		jrxmlPath = "jrxml/" + jasperList.get(0).getJasperName() + ".jrxml";
		}

//		jrxmlPath = "jrxml/GstInvoiceHw.jrxml";
		System.out.println(jrxmlPath);

		parameters.put("logoname", "stmarys.png");
		if (null != invoice) {
			if (null != invoice.get(0)) {
				SalesInvoiceReport salesInvoice = invoice.get(0);
				if (salesInvoice.getDiscount().equalsIgnoreCase(null)) {
					parameters.put("DiscountAmount", salesInvoice.getInvoiceDiscount());
					parameters.put("DiscountPercentage", salesInvoice.getDiscountPercent());

					parameters.put("DiscountAmountLabel", "Discount Amount:");
					parameters.put("DiscountPercentageLabel", "Discount Percentange:");
				}
			}
		}

		for (SalesPropertyTransHdr salesPropertyTransHdr : salesProperties) {
			if (salesPropertyTransHdr.getPropertyName().equalsIgnoreCase("VEHICLE_DETAILS")) {
				parameters.put("vehicleDetails", salesPropertyTransHdr.getValue());

			}
		}
		String LocalCustomerName = null;
		String LocalCustomerAddress = null;
		String LocalCustomerPoneNo = null;
		String LocalCustomerPlace = null;
		String LocalCustomerSate = null;
		String LocalCustomerGst = null;

		for (SalesInvoiceReport salesInvoiceReport : custDetails) {
			if (null != salesInvoiceReport.getLocalCustomerName()) {
				LocalCustomerName = salesInvoiceReport.getLocalCustomerName();
				LocalCustomerPoneNo = salesInvoiceReport.getLocalCustomerPhone();
				LocalCustomerPlace = salesInvoiceReport.getLocalCustomerPlace();
				LocalCustomerSate = salesInvoiceReport.getLocalCustomerState();
				LocalCustomerGst = null;
				LocalCustomerAddress = salesInvoiceReport.getLocalCustomerAddres();
			} else {
				LocalCustomerName = custDetails.get(0).getCustomerName();
//				LocalCustomerName =custDetails.get(0).getCustomerName().replaceAll(custDetails.get(0).getCustomerPhone(),"");
				LocalCustomerPoneNo = custDetails.get(0).getCustomerPhone();
				LocalCustomerPlace = custDetails.get(0).getCustomerPlace();
				LocalCustomerSate = salesInvoiceReport.getLocalCustomerState();
				LocalCustomerGst = custDetails.get(0).getCustomerGst();
				LocalCustomerAddress = custDetails.get(0).getCustomerAddress();
			}
		}

		parameters.put("LocalCustomerName", LocalCustomerName);
		parameters.put("LocalCustomerAddress", LocalCustomerAddress);
		parameters.put("LocalCustomerPoneNo", LocalCustomerPoneNo);
		parameters.put("LocalCustomerPlace", LocalCustomerPlace);
		parameters.put("LocalCustomerSate", LocalCustomerSate);

		parameters.put("subreportdata2", jrBeanCollectionDataSource3);
		parameters.put("stax_invoice_tax_sub2", "jrxml/taxsumarytable.jasper");

		String customerName = custDetails.get(0).getCustomerName();
		if (null != custDetails.get(0).getCustomerPhone() && null != custDetails.get(0).getCustomerName()) {
			System.out.println(custDetails.get(0).getCustomerPhone());
			System.out.println(custDetails.get(0).getCustomerName());

			if (custDetails.get(0).getCustomerName().contains(custDetails.get(0).getCustomerPhone())) {
				customerName = custDetails.get(0).getCustomerName().replaceAll(custDetails.get(0).getCustomerPhone(),
						"");
			}

		}
		
		parameters.put("CustomerAddress", LocalCustomerAddress);
		parameters.put("CustomerName", LocalCustomerName);
		parameters.put("PhoneNumber", LocalCustomerPoneNo);
		parameters.put("CustomerGst", LocalCustomerGst);
		parameters.put("CustomerPlace", LocalCustomerPlace);
		
		
		

		if (null != getSalesHdr.getSaleOrderHrdId()) {

			if (null != getSalesHdr.getInvoicePrintType()) {
				if (getSalesHdr.getInvoicePrintType().equalsIgnoreCase("LOCAl CUSTOMER")) {
					ResponseEntity<SalesOrderTransHdr> saleOrderTransHdrResp = RestCaller
							.getSaleOrderTransHdrById(getSalesHdr.getSaleOrderHrdId());
					SalesOrderTransHdr salesOrderTransHdr = saleOrderTransHdrResp.getBody();

					ResponseEntity<LocalCustomerMst> localCustomerMstResp = RestCaller
							.getLocalCustomerById(salesOrderTransHdr.getLocalCustomerId().getId());
					LocalCustomerMst localCustomer = localCustomerMstResp.getBody();

					if (localCustomer.getLocalcustomerName().contains(localCustomer.getPhoneNo())) {
						customerName = localCustomer.getLocalcustomerName().replaceAll(localCustomer.getPhoneNo(), "");
					}
					parameters.put("CustomerAddress", localCustomer.getAddress());
					parameters.put("CustomerName", customerName);
					parameters.put("PhoneNumber", localCustomer.getPhoneNo());
//					parameters.put("CustomerGst", localCustomer.getLandMark());
					parameters.put("CustomerPlace", localCustomer.getLandMark());
				}

			} else {

				ResponseEntity<SalesOrderTransHdr> saleOrderTransHdrResp = RestCaller
						.getSaleOrderTransHdrById(getSalesHdr.getSaleOrderHrdId());
				SalesOrderTransHdr salesOrderTransHdr = saleOrderTransHdrResp.getBody();

				ResponseEntity<LocalCustomerMst> localCustomerMstResp = RestCaller
						.getLocalCustomerById(salesOrderTransHdr.getLocalCustomerId().getId());
				LocalCustomerMst localCustomer = localCustomerMstResp.getBody();

				if (localCustomer.getLocalcustomerName().contains(localCustomer.getPhoneNo())) {
					customerName = localCustomer.getLocalcustomerName().replaceAll(localCustomer.getPhoneNo(), "");
				}
				parameters.put("CustomerAddress", localCustomer.getAddress());
				parameters.put("CustomerName", customerName);
				parameters.put("PhoneNumber", localCustomer.getPhoneNo());
//				parameters.put("CustomerGst", localCustomer.getLandMark());
				parameters.put("CustomerPlace", localCustomer.getLandMark());

			}

		}

		parameters.put("subreportdata3", jrBeanCollectionDataSource4);
		parameters.put("salesproperties", "jrxml/SalesPropertiesReport.jasper");

		ResponseEntity<List<TermsAndConditionsMst>> termsAndConditionMstResp = RestCaller
				.getTermsAndConditionMstByInvoiceFormat(jasperList.get(0).getJasperName());
		List<TermsAndConditionsMst> termsAndConditionMstList = termsAndConditionMstResp.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource5 = new JRBeanCollectionDataSource(
				termsAndConditionMstList);
		parameters.put("subreportdata4", jrBeanCollectionDataSource5);
		parameters.put("termsandcondition", "jrxml/TermsAndConditionReport.jasper");

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		String taxableAmountS = "0.0";
		if (invoice.size() > 0) {
			Double amountWithDiscount = invoice.get(0).getAmount() + invoice.get(0).getInvoiceDiscount();
			parameters.put("InvoiceAmountWithDiscount", amountWithDiscount);
			BigDecimal amountBig = new BigDecimal(amountWithDiscount);
			amountBig = amountBig.setScale(0, BigDecimal.ROUND_CEILING);

//			BigDecimal taxableAmountInBig = new BigDecimal(taxableAmount);
//			taxableAmountInBig = taxableAmountInBig.setScale(0, BigDecimal.ROUND_HALF_EVEN);
			String amount = amountBig + "";

			if (null != taxableAmount) {
				taxableAmountS = taxableAmount + "";

			}
			String amountinwords = SystemSetting.AmountInWords(amount, "Rs", "Ps");

			String taxamount = "0.0";
			Double taxDouble = 0.0;

			if (null != invoiceTax) {
				if (invoiceTax.size() > 0) {

					for (TaxSummaryMst tax : invoiceTax) {
						taxDouble = taxDouble + tax.getTaxAmount();
					}

				}
			}

			taxamount = taxDouble + "";

			String taxableAmountInWords = SystemSetting.AmountInWords(taxableAmountS, "Rs", "Ps");
			String taxamountinwords = SystemSetting.AmountInWords(taxamount, "Rs", "Ps");
			parameters.put("AmountInWords", amountinwords);
			parameters.put("taxableAmountInWords", taxableAmountInWords);
			parameters.put("TaxAmountInWords", taxamountinwords);
			parameters.put("taxamount", taxamount);

			parameters.put("taxableAmount", taxableAmountS);
			if (null == cessAmount) {
				cessAmount = 0.0;
			}

			parameters.put("CessAmount", cessAmount);

			String gst = "";
		}
//		for(TaxSummaryMst taxSummaryMst : invoiceTax)
//		{
//			gst = gst + taxSummaryMst.getTaxPercentage()+"\t"+taxSummaryMst.getTaxAmount()+"\n";
////		}
//		Double AmtAftrDiscount = 0.0;
//		if(invoice.size() >= 0)
//		{
//		if(null != invoice.get(0).getInvoiceDiscount())
//		{
//			 AmtAftrDiscount = invoice.get(0).getAmount()+invoice.get(0).getInvoiceDiscount();
//			
//		}else {
//			AmtAftrDiscount = invoice.get(0).getAmount();
//		}
//		} 
//		parameters.put("AmtAftrDiscount",AmtAftrDiscount);

		double cgstPercent = 0.0;
		double sgstPercent = 0.0;
		double igstPercent = 0.0;

		if (invoiceTaxSum.size() > 0) {

			cgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfcgstAmount(), 2);
			sgstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfsgstAmount(), 2);
			igstPercent = SystemSetting.round(invoiceTaxSum.get(0).getSumOfigstAmount(), 2);
		}
		parameters.put("cgst%", cgstPercent);
		parameters.put("sgst%", sgstPercent);
		parameters.put("igst%", igstPercent);
		// Fill the report
	
		
		
		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

	public static void getDayend(String branchCode, String sdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportPath + "/" + sdate + "dayend.pdf";

		Double totalSalesAmoount = 0.0;
		Double cardSettlementAmount = 0.0;
		Double branchCashOpeningBalance = 0.0;
		Double branchCashClosingBalance = 0.0;
		Double pettyCashOpeningBalance = 0.0;
		Double pettyCashClosingBalance = 0.0;
		Double CASHATDRAWYER = 0.0;
		Double pettyCashReceipt = 0.0;
		Double pettyCashPayment = 0.0;

		Double hodPayment = 0.0;

		ResponseEntity<VoucherNumberDtlReport> voucherNumberDtlReport = RestCaller
				.getInvoiceNumberStatistics(branchCode, sdate);

		List<VoucherNumberDtlReport> voucherNumberDtlReportList = new ArrayList<VoucherNumberDtlReport>();
		voucherNumberDtlReportList.add(voucherNumberDtlReport.getBody());

//		ResponseEntity<List<DailySalesReport>> totalSalesResp = RestCaller.TotalDailySalesSummary(sdate);
//		List<DailySalesReport> totalSales = totalSalesResp.getBody();
//
//		if (totalSales.size() > 0) {
//			totalSalesAmoount = totalSales.get(0).getTotalSales();
//		}

		ResponseEntity<Double> totalSales = RestCaller.TodaysTotalSales(sdate, sdate);

		if (null != totalSales.getBody()) {
			totalSalesAmoount = totalSales.getBody();
		}

		ResponseEntity<List<ReceiptModeReport>> receiptModeWiseDailySaleResp = RestCaller
				.ReceiptModeWiseDailySale(SystemSetting.getUser().getBranchCode(), sdate);

		List<ReceiptModeReport> receiptModeWiseDailySale = receiptModeWiseDailySaleResp.getBody();

		ResponseEntity<List<ReceiptModeReport>> todaysSOAdvanceReportResp = RestCaller
				.TodaysSOAdvanceReport(SystemSetting.getUser().getBranchCode(), sdate);

		List<ReceiptModeReport> todaysSOAdvanceReportList = todaysSOAdvanceReportResp.getBody();

		ResponseEntity<List<ReceiptModeReport>> todaysReceiptByReceiptMode = RestCaller
				.TodaysReceiptByReceiptMode(SystemSetting.getUser().getBranchCode(), sdate);

		List<ReceiptModeReport> todaysReceiptByReceiptModeList = todaysReceiptByReceiptMode.getBody();

		ResponseEntity<List<ReceiptModeReport>> receiptModeWisePrevAdvanceResp = RestCaller
				.ReceiptModeWisePreviousSales(SystemSetting.getUser().getBranchCode(), sdate);

		List<ReceiptModeReport> receiptModeWisePreviousSale = receiptModeWisePrevAdvanceResp.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				voucherNumberDtlReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource2 = new JRBeanCollectionDataSource(
				receiptModeWiseDailySale);
		JRBeanCollectionDataSource jrBeanCollectionDataSource3 = new JRBeanCollectionDataSource(
				todaysSOAdvanceReportList);
		JRBeanCollectionDataSource jrBeanCollectionDataSource4 = new JRBeanCollectionDataSource(
				todaysReceiptByReceiptModeList);

		JRBeanCollectionDataSource jrBeanCollectionDataSource5 = new JRBeanCollectionDataSource(
				receiptModeWisePreviousSale);

		String jrxmlPath = "jrxml/DayEndBakery.jrxml";

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap<String, Object> parameters = new HashMap();

		parameters.put("date", sdate);

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("BranchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("Phone", branchMst.getBranchTelNo());
		parameters.put("State", branchMst.getBranchState());
		parameters.put("GST", "GST" + branchMst.getBranchGst());

		parameters.put("TOTALSALES", totalSalesAmoount);

		// -----------------------------------------------------

		String posPrefix = SystemSetting.POSPREFIX;
		String kotPrefix = SystemSetting.KOTPREFIX;

		ResponseEntity<VoucherNumberDtlReport> posVoucherNumberResp = RestCaller.PosVoucherNumberDtlReport(posPrefix,
				sdate);
		VoucherNumberDtlReport posVoucherNumber = posVoucherNumberResp.getBody();

		parameters.put("POSPREFIX", posPrefix);
		parameters.put("POSMINVOUCHERNO", posVoucherNumber.getPosStartingBillNumber());
		parameters.put("POSMAXVOUCHERNO", posVoucherNumber.getPosEndingBillNumber());
		parameters.put("POSCOUNT", posVoucherNumber.getPosBillCount());

		ResponseEntity<VoucherNumberDtlReport> kotVoucherNumberResp = RestCaller.PosVoucherNumberDtlReport(kotPrefix,
				sdate);
		VoucherNumberDtlReport kotVoucherNumber = kotVoucherNumberResp.getBody();

		parameters.put("KOTPREFIX", kotPrefix);
		parameters.put("KOTMINVOUCHERNO", kotVoucherNumber.getPosStartingBillNumber());
		parameters.put("KOTMAXVOUCHERNO", kotVoucherNumber.getPosEndingBillNumber());
		parameters.put("KOTCOUNT", kotVoucherNumber.getPosBillCount());

		// -----------------------------------------------------

		ResponseEntity<List<DayEndClosureHdr>> dayEndReport = RestCaller.DayEndReportByDate(sdate);
		List<DayEndClosureHdr> dayEndClosureHdr = dayEndReport.getBody();

		if (dayEndClosureHdr.size() > 0) {
			if (null != dayEndClosureHdr.get(0).getCardSettlementAmount()) {
				cardSettlementAmount = dayEndClosureHdr.get(0).getCardSettlementAmount();
			}

			if (null != dayEndClosureHdr.get(0).getPhysicalCash()) {
				CASHATDRAWYER = dayEndClosureHdr.get(0).getPhysicalCash();
			}

		}

		ResponseEntity<Double> totalCardResp = RestCaller.TodaysTotalCardSales(branchCode, sdate);
		Double totalCard = totalCardResp.getBody();

		if (null == totalCard) {
			totalCard = 0.0;
		}
		System.out.println("@@@@@@@@@@@@@@@@@@@@TOTAL  CARD = " + totalCard);

		parameters.put("totalCard", totalCard);
		parameters.put("cardSettlementAmount", cardSettlementAmount);
		parameters.put("cashAtDrawyer", CASHATDRAWYER);

		parameters.put("subreportdata", jrBeanCollectionDataSource2);
		parameters.put("receiptmodewisereport", "jrxml/ReceiptModeWiseReport.jasper");

		ResponseEntity<Double> TodaysTotalAdvanceResp = RestCaller.TodaysTotalAdvanceByDate(sdate);
		Double totalAdvance = TodaysTotalAdvanceResp.getBody();

		if (null == totalAdvance) {
			totalAdvance = 0.0;
		}

		parameters.put("totalAdvance", totalAdvance);

		ResponseEntity<Double> previousAdvanceResp = RestCaller.TodaysPreviousAdvance(sdate);
		Double previousAdvance = previousAdvanceResp.getBody();

		if (null == previousAdvance) {
			previousAdvance = 0.0;
		}

		parameters.put("previousAdvance", previousAdvance);

		parameters.put("subreportdata2", jrBeanCollectionDataSource3);
		parameters.put("todaysadvance", "jrxml/ReceiptModeWiseReport.jasper");

		parameters.put("subreportdata3", jrBeanCollectionDataSource4);
		parameters.put("todaysreceipts", "jrxml/ReceiptModeWiseReport.jasper");

		parameters.put("subreportdata4", jrBeanCollectionDataSource5);
		parameters.put("previousreceipts", "jrxml/ReceiptModeWiseReport.jasper");

		// --------------------------barnch cahs

		ResponseEntity<AccountHeads> cashHeadsResp = RestCaller
				.getAccountHeadByName(SystemSetting.getUser().getBranchCode() + "-CASH");
		AccountHeads cashAccountHeads = cashHeadsResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> cashAccountBalance = RestCaller.getAccountBalanceReport(sdate, sdate,
				cashAccountHeads.getId());
		List<AccountBalanceReport> cashOpeningAndClosingCashBalance = cashAccountBalance.getBody();

		if (cashOpeningAndClosingCashBalance.size() > 0) {

			branchCashOpeningBalance = cashOpeningAndClosingCashBalance.get(0).getOpeningBalance();
			branchCashClosingBalance = cashOpeningAndClosingCashBalance.get(0).getClosingBalance();

		}

		parameters.put("branchCashOpeningBalance", branchCashOpeningBalance);
		parameters.put("branchCashClosingBalance", branchCashClosingBalance);

		ResponseEntity<Double> todaysCashSaleResp = RestCaller.TodaysCashSaleByReceiptMode(sdate,
				SystemSetting.getUser().getBranchCode() + "-CASH");

		Double todaysCashSale = todaysCashSaleResp.getBody();

		if (null == todaysCashSale) {
			todaysCashSale = 0.0;
		}

		ResponseEntity<Double> previousCashAdvanceResp = RestCaller.TodaysPreviousCashAdvance(sdate);
		Double previousCashAdvance = previousCashAdvanceResp.getBody();

		if (null == previousCashAdvance) {
			previousCashAdvance = 0.0;
		}

		todaysCashSale = todaysCashSale - previousCashAdvance;

		parameters.put("todaysCashSale", todaysCashSale);

		ResponseEntity<Double> todaysAdvanceCashResp = RestCaller.TodaysAdvanceByReceiptMode(sdate, "CASH");

		Double todaysAdvanceCash = todaysAdvanceCashResp.getBody();
		if (null == todaysAdvanceCash) {
			todaysAdvanceCash = 0.0;
		}
		parameters.put("todaysAdvanceCash", todaysAdvanceCash);

		ResponseEntity<Double> todaysCashReceiptResp = RestCaller.getTodaysTotalReceiptByReceiptMode(branchCode,
				SystemSetting.getUser().getBranchCode() + "-CASH", sdate);
		Double todaysCashReceipt = todaysCashReceiptResp.getBody();
		if (null == todaysCashReceipt) {
			todaysCashReceipt = 0.0;
		}
		parameters.put("todaysCashReceipt", todaysCashReceipt);

		Double totalCash = branchCashOpeningBalance + todaysCashSale + todaysAdvanceCash + todaysCashReceipt;

		if (null == totalCash)

		{
			totalCash = 0.0;
		}

		parameters.put("totalCash", totalCash);

		Double cashShort = totalCash - CASHATDRAWYER;

		if (cashShort < 0) {

			cashShort = 0.0;
		}
		parameters.put("cashShort", cashShort);

		Double cashEccess = totalCash - CASHATDRAWYER;

		if (cashEccess > 0) {
			cashEccess = 0.0;
		}

		parameters.put("cashEccess", cashEccess);

		// --------------------------petty cahs

		ResponseEntity<AccountHeads> cashHeadsPettyCashResp = RestCaller
				.getAccountHeadByName(SystemSetting.getUser().getBranchCode() + "-PETTY CASH");

		AccountHeads cashAccountHeadsPettyCash = cashHeadsPettyCashResp.getBody();

		ResponseEntity<List<AccountBalanceReport>> pettyCashAccountBalance = RestCaller.getAccountBalanceReport(sdate,
				sdate, cashAccountHeadsPettyCash.getId());
		List<AccountBalanceReport> pettyCashOpeningAndClosingCashBalance = pettyCashAccountBalance.getBody();

		if (pettyCashOpeningAndClosingCashBalance.size() > 0) {

			pettyCashOpeningBalance = pettyCashOpeningAndClosingCashBalance.get(0).getOpeningBalance();
			pettyCashClosingBalance = pettyCashOpeningAndClosingCashBalance.get(0).getClosingBalance();

		}

		ResponseEntity<DayEndPettyCashPaymentAndReceipt> dayEndPettyCashPaymentAndReceiptResp = RestCaller
				.DayEndPettyCashPaymentAndReceipt(SystemSetting.getUser().getBranchCode() + "-PETTY CASH", sdate,
						branchCode);

		DayEndPettyCashPaymentAndReceipt dayEndPettyCashPaymentAndReceipt = dayEndPettyCashPaymentAndReceiptResp
				.getBody();

		if (null != dayEndPettyCashPaymentAndReceipt) {

			if (null != dayEndPettyCashPaymentAndReceipt.getPettyCasReceipt()) {
				pettyCashReceipt = dayEndPettyCashPaymentAndReceipt.getPettyCasReceipt();
			}

			if (null != dayEndPettyCashPaymentAndReceipt.getPettyCashPayment()) {
				pettyCashPayment = dayEndPettyCashPaymentAndReceipt.getPettyCashPayment();
			}
		}

		parameters.put("pettyCashOpeningBalance", pettyCashOpeningBalance);
		parameters.put("pettyCashClosingBalance", pettyCashClosingBalance);
		parameters.put("pettyCashPayment", pettyCashPayment);
		parameters.put("pettyCashReceipt", pettyCashReceipt);

		// -------------hod payment

		ResponseEntity<ParamValueConfig> getParamValue = RestCaller.getParamValueConfig("HODACCOUNT");
		if (null != getParamValue.getBody()) {

			ResponseEntity<AccountHeads> hodAccountResp = RestCaller
					.getAccountHeadByName(getParamValue.getBody().getValue());

			AccountHeads hodAccount = hodAccountResp.getBody();

			if (null != hodAccount) {
				ResponseEntity<Double> hodPaymentResp = RestCaller
						.DayEndPettyCashPaymentAndReceiptByAccontId(hodAccount.getId(), sdate, branchCode);

				hodPayment = hodPaymentResp.getBody();

				if (null == hodPayment) {
					hodPayment = 0.0;
				}
			}

		}

		parameters.put("hodPayment", hodPayment);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = MapleclientApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);

	}

}
