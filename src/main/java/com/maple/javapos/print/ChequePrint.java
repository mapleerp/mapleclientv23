package com.maple.javapos.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.maple.javapos.print.PosTaxLayoutPrint.MyPrintable;
import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.ChequePrintMst;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;

/*
 * This model is deployed in HotCakes
 * Tax is printed 
 * Logo is placed at top
 * Tabular layout for item details
 */

public class ChequePrint {
	private static final Logger log = LoggerFactory.getLogger(ChequePrint.class);
	static JTable itemsTable;
	public static int total_item_count = 0;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss a";
	public static String title[] = new String[] { "Srl", "Item Name", "Price", "Qty", "Amount" };
	// static String parentID;
	public static String branchCode = "";
	public static int INVOICELINE_Y = SystemSetting.INVOICELINE_Y;
	public static String invoiceBottomLine = "";

	static BufferedImage read;

	public void setupLogo() throws IOException {

		if(null==read) {
			
		//Path path = FileSystems.getDefault().getPath(SystemSetting.getLogo_name());
			FileSystem fs =  FileSystems.getDefault();
		Path path = fs.getPath(SystemSetting.getLogo_name());

		InputStream iStream = Files.newInputStream(path);

		read = ImageIO.read(iStream);
		
		iStream.close();
		fs.close();
 
		}
	}

	public static PageFormat getPageFormat(PrinterJob pj) {
		PageFormat pf = pj.defaultPage();
		Paper paper = pf.getPaper();

		double middleHeight = total_item_count * 1.0; // dynamic----->change with the row count of jtable
		double headerHeight = 5.0; // fixed----->but can be mod
		double footerHeight = 200.0; // fixed----->but can be mod

		double width = convert_CM_To_PPI(SystemSetting.WEIGHBRIDGEPAPERSIZE); // printer know only point per inch.default value is 72ppi
		double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
		paper.setSize(width, height);
		paper.setImageableArea(convert_CM_To_PPI(0.25), convert_CM_To_PPI(0.5), width - convert_CM_To_PPI(0.35),
				height - convert_CM_To_PPI(1)); // define boarder size after that print area width is about 180 points

		pf.setOrientation(PageFormat.PORTRAIT); // select orientation portrait or landscape but for this time portrait
		pf.setPaper(paper);

		return pf;
	}

	protected static double convert_CM_To_PPI(double cm) {
		return toPPI(cm * 0.393600787);
	}

	protected static double toPPI(double inch) {
		return inch * 72d;
	}

	public static String now() {
		// get current date and time as a String output
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());

	}

	public static class MyPrintable implements Printable {

		String bankName;
		String accountName;
		String amount;
		String amountInWords;

		String date;

		MyPrintable(String bankName,String accountName,String amount,String amountInWords,String date) {
			this.accountName = accountName;

			this.bankName = bankName;
			this.amount = amount;

			this.amountInWords = amountInWords;

			this.date = date;

			


		}

		@Override
		public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {

			int result = NO_SUCH_PAGE;
			if (pageIndex == 0) {
				Graphics2D g2d = (Graphics2D) graphics;

				pageFormat.getImageableWidth();
				pageFormat.getImageableHeight();
				g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
				Font font = new Font("Monospaced", Font.PLAIN, 7);
				g2d.setFont(font);

				int x = 1; // print start at 100 on x axies
				int y = 1; // print start at 10 on y axies
				// imperial
				int imagewidth = 150;
				int imageheight = 50;

				x = SystemSetting.getLogox();
				y = SystemSetting.getLogoy();
				imagewidth = SystemSetting.getLogow();
				imageheight = SystemSetting.getLogoh();
				if (null != read) {
					g2d.drawImage(read, x, y, imagewidth, imageheight, null); // draw image
				}
				//g2d.drawLine(10, y + INVOICELINE_Y, 180, y + INVOICELINE_Y); // draw line

				try {

//					font = new Font("Arial", Font.BOLD, SystemSetting.WEIGHCOMPANYTITLEFONTSIZE);
//					g2d.setFont(font);
//
//					g2d.drawString(SystemSetting.WEIGHCOMPANYTITLE, SystemSetting.WEIGHCOMPANYTITLEX,
//							SystemSetting.WEIGHCOMPANYTITLEY);
//
//					font = new Font("Arial", Font.BOLD, SystemSetting.WEIGHCOMPANYADDRESS1FONTSIZE);
//					g2d.setFont(font);
//
//					g2d.drawString(SystemSetting.WEIGHCOMPANYADDRESS1, SystemSetting.WEIGHCOMPANYADDRESS1X,
//							SystemSetting.WEIGHCOMPANYADDRESS1Y);
//
//					font = new Font("Arial", Font.BOLD, SystemSetting.WEIGHCOMPANYADDRESS2FONTSIZE);
//					g2d.setFont(font);
//
//					g2d.drawString(SystemSetting.WEIGHCOMPANYADDRESS2, SystemSetting.WEIGHCOMPANYADDRESS2X,
//							SystemSetting.WEIGHCOMPANYADDRESS2Y);
//
//					font = new Font("Arial", Font.BOLD, SystemSetting.WEIGHCOMPANYADDRESS3FONTSIZE);
//					g2d.setFont(font);
//
//					g2d.drawString(SystemSetting.WEIGHCOMPANYADDRESS3, SystemSetting.WEIGHCOMPANYADDRESS3X,
//							SystemSetting.WEIGHCOMPANYADDRESS3Y);
//					g2d.drawLine(10,SystemSetting.WEIGHCOMPANYADDRESS3Y + 80, 2500,SystemSetting.WEIGHCOMPANYADDRESS3Y + 80);
//
//					font = new Font("Arial", Font.BOLD, SystemSetting.WEIGHCOMPANYVEHICKENOFONTSIZE);
//					g2d.setFont(font);
					configureChequeProperty(bankName);
					
					
					font = new Font("Arial", Font.PLAIN, SystemSetting.CHEQUEPRINTFONTSIZE);
					g2d.setFont(font);
					
					g2d.drawString(date, SystemSetting.CHEQUEDATEX,
							SystemSetting.CHEQUEDATEY);
					
					g2d.drawString(accountName, SystemSetting.CHEQUEACCOUNTNAMEX, SystemSetting.CHEQUEACCOUNTNAMEY);

					
					g2d.drawString(amount, SystemSetting.CHEQUEAMOUNTX, SystemSetting.CHEQUEAMOUNTY);

					g2d.drawString(amountInWords, SystemSetting.CHEQUEAMOUNTINWORDSX,
							SystemSetting.CHEQUEAMOUNTINWORDSY);

					

					/* Draw Colums */
					g2d.drawLine(SystemSetting.CHEQUECROSSLINE1X1,SystemSetting.CHEQUECROSSLINE1Y1,SystemSetting.CHEQUECROSSLINE1X2,SystemSetting.CHEQUECROSSLINE1Y2);
					g2d.drawLine(SystemSetting.CHEQUECROSSLINE2X1,SystemSetting.CHEQUECROSSLINE2Y1,SystemSetting.CHEQUECROSSLINE2X2,SystemSetting.CHEQUECROSSLINE2Y2);
					// end of the reciept
				} catch (Exception r) {
					r.printStackTrace();
				}

				result = PAGE_EXISTS;
			}
			return result;
		}

		private void configureChequeProperty(String bankName2) {
			ResponseEntity<AccountHeads> getAccByName = RestCaller.getAccountHeadByName(bankName);
	    	ResponseEntity<List<ChequePrintMst>> chequePrintMst = RestCaller.getallChequePrintByAccountId(getAccByName.getBody().getId());
	    	for(int i=0;i<chequePrintMst.getBody().size();i++)
	    	{
	    		if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUECROSSLINE1X1"))
	    		{
	    			SystemSetting.CHEQUECROSSLINE1X1=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUECROSSLINE1Y1"))
	    		{
	    			SystemSetting.CHEQUECROSSLINE1Y1=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUECROSSLINE1X2"))
	    		{
	    			SystemSetting.CHEQUECROSSLINE1X2=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUECROSSLINE1Y2"))
	    		{
	    			SystemSetting.CHEQUECROSSLINE1Y2=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUECROSSLINE2X1"))
	    		{
	    			SystemSetting.CHEQUECROSSLINE2X1=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUECROSSLINE2Y1"))
	    		{
	    			SystemSetting.CHEQUECROSSLINE2Y1=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUECROSSLINE2X2"))
	    		{
	    			SystemSetting.CHEQUECROSSLINE2X2=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUECROSSLINE2Y2"))
	    		{
	    			SystemSetting.CHEQUECROSSLINE2Y2=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEDATEX"))
	    		{
	    			SystemSetting.CHEQUEDATEX=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEDATEY"))
	    		{
	    			SystemSetting.CHEQUEDATEY=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEACCOUNTNAMEX"))
	    		{
	    			SystemSetting.CHEQUEACCOUNTNAMEX=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEACCOUNTNAMEY"))
	    		{
	    			SystemSetting.CHEQUEACCOUNTNAMEY=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEAMOUNTINWORDSX"))
	    		{
	    			SystemSetting.CHEQUEAMOUNTINWORDSX=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEAMOUNTINWORDSY"))
	    		{
	    			SystemSetting.CHEQUEAMOUNTINWORDSY=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEAMOUNTX"))
	    		{
	    			SystemSetting.CHEQUEAMOUNTX=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEAMOUNTY"))
	    		{
	    			SystemSetting.CHEQUEAMOUNTY=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		else if(chequePrintMst.getBody().get(i).getChequeProperty().equalsIgnoreCase("CHEQUEPRINTFONTSIZE"))
	    		{
	    			SystemSetting.CHEQUEPRINTFONTSIZE=Integer.parseInt(chequePrintMst.getBody().get(i).getChequeValue());
	    		}	
	    		
	    	}
			
		}
	}

	public static String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s);
	}

	public static String formatDecimal(float number) {
		float epsilon = 0.004f; // 4 tenths of a cent
		if (Math.abs(Math.round(number) - number) < epsilon) {
			return String.format("%10.0f", number); // sdb
		} else {
			return String.format("%10.2f", number); // dj_segfault
		}
	}

	public void PrintInvoiceThermalPrinter(String bankName,String accountName, String amount, String amountInWords, String date) throws SQLException {

		new BigDecimal("0");
		ChequePrint ps = new ChequePrint();

		new BigDecimal("0");

		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setPrintable(new MyPrintable(bankName,accountName, amount, amountInWords, date),
				ps.getPageFormat(pj));
		try {
			pj.print();

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}

	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public void setupBottomline(String bottomLine) throws IOException {
		invoiceBottomLine = bottomLine;

	}

}
/*
 * ################# THIS IS HOW TO USE THIS CLASS #######################
 * 
 * Printsupport ps=new Printsupport(); Object printitem
 * [][]=ps.getTableData(jTable); ps.setItems(printitem);
 * 
 * PrinterJob pj = PrinterJob.getPrinterJob(); pj.setPrintable(new
 * MyPrintable(),ps.getPageFormat(pj)); try { pj.print();
 * 
 * } catch (PrinterException ex) { ex.printStackTrace(); } ##################
 * JOIN TO SHARE KNOWLADGE ###########################
 * 
 */