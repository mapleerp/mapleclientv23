package com.maple.javapos.print;

import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.nio.charset.StandardCharsets;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 
 

public class BarcodePrinterZeebra {
	private static final Logger log = LoggerFactory.getLogger(BarcodePrinterZeebra.class);
	
	public static void ZeebraPrint(String printName, String barcodeLabel, String barcodeQty) {
		
		log.info("Printer name " + printName);

		PrintService pservice = PrintServiceLookup.lookupDefaultPrintService();

		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
		System.out.println("Number of print services: " + printServices.length);

		for (PrintService printer : printServices) {
			System.out.println("Printer: " + printer.getName());
			if (printName.equalsIgnoreCase(printer.getName())) {
				printLabel(printer, barcodeLabel, barcodeQty);
			}
		}

	}

	private static boolean printLabel(PrintService printService, String label, String barcodeQty) {
		if (printService == null || label == null) {
			System.err.println("[Print Label] print service or label is invalid.");
			return false;
		}

		log.info("Starting zeebra Printing");

		String command = "N\n" +

				"B10,10,0,1,2,2,120,B,\"" + label + "\"\n" +

				"P" + barcodeQty + "\n";
		// "A50,50,0,3,1,1,N,\""+label+"\"\n"+
//"A50,310,0,3,1,1,N,\""+czas+"\"\n"+
		// String aaa = "^XA\r\n" +
		// "^FO50,5^B3N,N,100,Y,N^FD123456^FS\r\n" +
		// "^XZ";
		
		/*
		 * PrintService pservice = ... // acquire print service of your printer
DocPrintJob job = pservice.createPrintJob();  
String commands = "^XA\n\r^MNM\n\r^FO050,50\n\r^B8N,100,Y,N\n\r^FD1234567\n\r^FS\n\r^PQ3\n\r^XZ";
DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
Doc doc = new SimpleDoc(commands.getBytes(), flavor, null);
job.print(doc, null);
		 */

		byte[] data;
		data = command.getBytes(StandardCharsets.US_ASCII);
		Doc doc = new SimpleDoc(data, DocFlavor.BYTE_ARRAY.AUTOSENSE, null);

		boolean result = false;
		try {
			printService.createPrintJob().print(doc, null);
			result = true;
		} catch (PrintException e) {
			e.printStackTrace();
		}
		return result;
	}

}
