package com.maple.javapos.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.DayEndClosureDtl;
import com.maple.mapleclient.entity.DayEndClosureHdr;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.report.entity.ReceiptModeReport;
import com.maple.report.entity.SalesModeWiseSummaryreport;
import com.maple.report.entity.StockTransferOutReport;

//HotCakesPOSPrint
/*
 * This model is deployed in HotCakes
 * Tax is printed 
 * Logo is placed at top
 * Tabular layout for item details
 */
public class DayEndThermalPrintingFunction extends POSThermalPrintABS {
	private static final Logger log = LoggerFactory.getLogger(DayEndThermalPrintingFunction.class);
	static JTable itemsTable;
	public static int total_item_count = 0;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss a";
	public static String title[] = new String[] { "Srl", "Receipt MOde ", "Amount" };
	static String date;
	public static String branchCode = "";

	public static int INVOICELINE_Y = SystemSetting.INVOICELINE_Y;
	public static String invoiceBottomLine = "";
	public static String invoiceBottomLine2 = "";
	public static String invoiceBottomLine3 = "";

	static Double GrandTotalIncludingTax = 0.0;

	static String dayEndDate = "";
	String voucher_type = "";
	static String InvoiceTimeToPrint = "";

	static BufferedImage read;

	public void setupLogo() throws IOException {

		if (null == read) {
			FileSystem fs = FileSystems.getDefault();
			Path path = fs.getPath(SystemSetting.getLogo_name());

			InputStream iStream = Files.newInputStream(path);

			read = ImageIO.read(iStream);
//		iStream.close();
//		fs.close();

		}
	}

	/*
	 * public static void main(String[] args) { // TODO Auto-generated method stub
	 * 
	 * HotCakesStockTransferPrint ps = new HotCakesStockTransferPrint();
	 * 
	 * DefaultTableModel model = new DefaultTableModel( new Object[][] { { 1,
	 * "text", 1, 1 }, { 2, "text", 3, 3 }, { 3, "m4ore", 5, 5 }, { 4, "strings", 5,
	 * 5 }, { 5, "other", 7, 7 }, { 6, "values", 6, 6 } }, new Object[] { "itemid",
	 * "desc ", "rate", "Qty", "Amt" });
	 * 
	 * JTable table = new JTable(model);
	 * 
	 * Object printitem[][] = ps.getTableData(table); ps.setItems(printitem);
	 * 
	 * PrinterJob pj = PrinterJob.getPrinterJob(); pj.setPrintable(new
	 * MyPrintable(), ps.getPageFormat(pj)); try { pj.print();
	 * 
	 * } catch (PrinterException ex) { ex.printStackTrace(); }
	 * 
	 * 
	 * }
	 */

	public static void setItems(Object[][] printitem) {
		Object data[][] = printitem;
		DefaultTableModel model = new DefaultTableModel();

		model.addColumn(title[0]);
		model.addColumn(title[1]);
		model.addColumn(title[2]);

		int rowcount = printitem.length;

		addtomodel(model, data, rowcount);

		itemsTable = new JTable(model);
		itemsTable.setRowSorter(null);
	}

	public static void addtomodel(DefaultTableModel model, Object[][] data, int rowcount) {
		int count = 0;
		while (count < rowcount) {
			model.addRow(data[count]);
			count++;
		}
		if (model.getRowCount() != rowcount)
			addtomodel(model, data, rowcount);

		System.out.println("Check Passed.");
	}

	public Object[][] getTableData(JTable table) {
		int itemcount = table.getRowCount();
		System.out.println("Item Count:" + itemcount);

		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
		Object[][] tableData = new Object[nRow][nCol];
		if (itemcount == nRow) // check is there any data loss.
		{
			for (int i = 0; i < nRow; i++) {
				for (int j = 0; j < nCol; j++) {
					tableData[i][j] = dtm.getValueAt(i, j); // pass data into object array.
				}
			}
			if (tableData.length != itemcount) { // check for data losses in object array
				getTableData(table); // recursively call method back to collect data
			}
			System.out.println("Data check passed");
		} else {
			// collecting data again because of data loss.
			getTableData(table);
		}
		return tableData; // return object array with data.
	}

	public static PageFormat getPageFormat(PrinterJob pj) {
		PageFormat pf = pj.defaultPage();
		Paper paper = pf.getPaper();

		double middleHeight = total_item_count * 1.0; // dynamic----->change with the row count of jtable
		double headerHeight = 5.0; // fixed----->but can be mod
		double footerHeight = 200.0; // fixed----->but can be mod

		double width = convert_CM_To_PPI(7); // printer know only point per inch.default value is 72ppi
		double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
		paper.setSize(width, height);
		paper.setImageableArea(convert_CM_To_PPI(0.25), convert_CM_To_PPI(0.5), width - convert_CM_To_PPI(0.35),
				height - convert_CM_To_PPI(1)); // define boarder size after that print area width is about 180 points

		pf.setOrientation(PageFormat.PORTRAIT); // select orientation portrait or landscape but for this time portrait
		pf.setPaper(paper);

		return pf;
	}

	protected static double convert_CM_To_PPI(double cm) {
		return toPPI(cm * 0.393600787);
	}

	protected static double toPPI(double inch) {
		return inch * 72d;
	}

	public static String now() {
		// get current date and time as a String output
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());

	}

	public static class MyPrintable implements Printable {
		@Override
		public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {

			int result = NO_SUCH_PAGE;
			if (pageIndex == 0) {
				Graphics2D g2d = (Graphics2D) graphics;

				double width = pageFormat.getImageableWidth();
				double height = pageFormat.getImageableHeight();
				g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
				Font font = new Font("Monospaced", Font.PLAIN, 7);
				g2d.setFont(font);

				/*
				 * Draw Image* assume that printing reciept has logo on top that logo image is
				 * in .gif format .png also support image resolution is width 100px and height
				 * 50px image located in root--->image folder
				 */

				int x = 1; // print start at 100 on x axies
				int y = 1; // print start at 10 on y axies
				// imperial
				int imagewidth = 150;
				int imageheight = 50;

				x = SystemSetting.getLogox();
				y = SystemSetting.getLogoy();
				imagewidth = SystemSetting.getLogow();
				imageheight = SystemSetting.getLogoh();
				if (null != read) {
					g2d.drawImage(read, x, y, imagewidth, imageheight, null); // draw image
				}
				// g2d.drawLine(10, y + 100, 180, y + 100); // draw line
				g2d.drawLine(10, y + INVOICELINE_Y, 180, y + INVOICELINE_Y); // draw line

				try {
					/* Draw Header */

					String SalesManId = "";

					double invoice_amount = 0.0;
					double invoice_discount = 0.0;

					String strInvoiceDate = "";
					String voucher_type = "";
					String customerName = "";
					String customerPhno = "";

					LocalDateTime Voucher_time = null;
					Date invoice_date = null;

					// SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(parentID);

					voucher_type = "DAY END REPORT";

					// Timestamp Voucher_time = null;
					// Date invoice_date = null;
					/*
					 * Pass ID and get Sales Trans HDR
					 * 
					 */

					/*
					 * Pass ID and get BranchMst Details
					 */

					// BranchMst branchMst =
					// RestCaller.getBranchDtls(SystemSetting.getSystemBranch());

					font = new Font("Arial", Font.BOLD, 8);
					g2d.setFont(font);

					g2d.drawString(SystemSetting.getPosinvoicetitle1(), SystemSetting.getTitle1x(),
							SystemSetting.getTitle1y());

					// if (null != branchMst.getBranchState()) {

					// g2d.drawString(branchMst.getBranchState(), 50, y + 10); // shift a line by
					// adding 10 to y value
					g2d.drawString(SystemSetting.getPosinvoicetitle2(), SystemSetting.getTitle2x(),
							SystemSetting.getTitle2y());

					// }

					// if (null != branchMst.getBranchName()) {

					// g2d.drawString(branchMst.getBranchName(), 50, y + 10); // shift a line by
					// adding 10 to y value
					g2d.drawString(SystemSetting.getPosinvoicetitle3(), SystemSetting.getTitle3x(),
							SystemSetting.getTitle3y());
					// }

					// if (null != branchMst.getBranchName()) {

					// g2d.drawString(branchMst.getBranchName(), 50, y + 10); // shift a line by
					// adding 10 to y value
					g2d.drawString(SystemSetting.getPosinvoicetitle4(), SystemSetting.getTitle4x(),
							SystemSetting.getTitle4y());

					// }

					// if (null != branchMst.getBranchGst()) {

					// g2d.drawString(branchMst.getBranchGst(), 50, y + 10); // shift a line
					// by adding
					// 10 to y
					// value
					g2d.drawString(SystemSetting.getPosinvoicetitle5(), SystemSetting.getTitle5x(),
							SystemSetting.getTitle5y());
					// }

					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

					// InvoiceTimeToPrint = Voucher_time.format(dtf);

					// This has to be cuncommendted for time print

					/*
					 * if (!s1.equalsIgnoreCase(s2)) { InvoiceTimeToPrint = "Inv.Date" +
					 * SystemSetting.SqlDateTostring(InvoiceDate) + "(" +
					 * Voucher_time.toLocaleString() + ")";
					 * 
					 * } else { InvoiceTimeToPrint = Voucher_time.toLocaleString(); }
					 */
					y = SystemSetting.getTitle5y();
					// g2d.drawString(voucher_type, 80, y + 10); // print voucher type

//					if(null !=salesTransHdr.getTakeOrderNumber() &&  !salesTransHdr.getTakeOrderNumber().equalsIgnoreCase(null))
//					{
//						if(!customerName.equalsIgnoreCase(null ) && !customerName.equalsIgnoreCase(""))
//							g2d.drawString("Customer Name:"+customerName, 70, y ); 
//					}
					g2d.drawString("DATE   : " + date, 10, y + 20);
					// g2d.drawString("Voucher Date : " + InvoiceDate, 10, y + 30);

//					if(null !=salesTransHdr.getTakeOrderNumber() &&  !salesTransHdr.getTakeOrderNumber().equalsIgnoreCase(null))
//					{
//						if(!customerPhno.equalsIgnoreCase(null ) && !customerPhno.equalsIgnoreCase(""))
//							g2d.drawString("Customer Phno : " + customerPhno, 70, y);
//					}

					y = y + 30;
					g2d.drawString(voucher_type, 30, y);
					y = y + 20;

					/* Draw Colums */
					g2d.drawLine(10, y, 180, y);
					y = y + 10;
					g2d.drawString(title[0], 10, y);
					// y = y+10;
					g2d.drawString(title[1], 50, y);
					// y = y+10;
					g2d.drawString(title[2], 140, y);
					// y = y+10;
					y = y + 10;
					g2d.drawLine(10, y, 180, y);
					y = y + 10;
					int cH = 0;
					TableModel mod = itemsTable.getModel();
					int lastLine = 0;

					for (int i = 0; i < mod.getRowCount(); i++) {
						/*
						 * Assume that all parameters are in string data type for this situation All
						 * other premetive data types are accepted.
						 */
						String itemid = mod.getValueAt(i, 0).toString();
						String itemname = mod.getValueAt(i, 1).toString();
						if (itemname.length() > 15) {
							itemname = itemname.substring(0, 15);
						}
						String amount = mod.getValueAt(i, 2).toString();

						cH = (y + 10) + (10 * i); // shifting drawing line

						font = new Font("Arial", Font.BOLD, 8); // changed font size
						g2d.setFont(font);

						g2d.drawString(itemid, 0, cH);
						g2d.drawString(itemname, 10, cH);

						// String amountFmt = formatDecimal(Float.parseFloat(amount));
						g2d.drawString(padLeft(amount, 10), 150, cH);
						lastLine = i;

					}
					cH = (y + 40) + (10 * (lastLine + 2));
					g2d.drawString("", 150, cH);

					font = new Font("Arial", Font.PLAIN, 8);
					g2d.setFont(font);

					String strGrandTotalIncludingTax = String.format("%9.00f", GrandTotalIncludingTax);

					font = new Font("Arial", Font.BOLD, 12); // changed font size
					g2d.setFont(font);
					String stg = "Total Sales:  ";
					g2d.drawString(stg + "          " + strGrandTotalIncludingTax + " /-", 10, cH);
					cH = cH + 10;

					g2d.drawLine(10, cH, 180, cH);
					cH = cH + 20;

					// User

					if (!SystemSetting.getUser().getUserName().equalsIgnoreCase("TEST")) {
						font = new Font("Arial", Font.PLAIN, 8); // changed font size
						g2d.setFont(font);
						g2d.drawString("Bill Generated By:", 10, cH);
						g2d.drawString(SystemSetting.getUser().getUserName(), 80, cH);
						cH = cH + 20;
					}
					font = new Font("Arial", Font.PLAIN, 8); // changed font size
					g2d.setFont(font);

					if (invoiceBottomLine.trim().length() > 1) {
						g2d.drawString(invoiceBottomLine, 1, cH);

						cH = cH + 10;
					}

					if (invoiceBottomLine2.trim().length() > 1) {
						g2d.drawString(invoiceBottomLine2, 1, cH);
						cH = cH + 10;
					}

					if (invoiceBottomLine3.trim().length() > 1) {
						g2d.drawString(invoiceBottomLine3, 1, cH);
					}

					// end of the reciept
				} catch (Exception r) {
					r.printStackTrace();
				}

				result = PAGE_EXISTS;
			}
			return result;
		}
	}

	public static String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s);
	}

	public static String formatDecimal(float number) {
		float epsilon = 0.004f; // 4 tenths of a cent
		if (Math.abs(Math.round(number) - number) < epsilon) {
			return String.format("%10.0f", number); // sdb
		} else {
			return String.format("%10.2f", number); // dj_segfault
		}
	}

//================================================================= Get all tax======================//
	public static double getItemTotalTax(double vatrate, String ParentId) {

		double InvTotal = 0.0;

		try {

			ResponseEntity<Double> ojjList = RestCaller.getTaxAmount(vatrate, ParentId);

//			String sqlstr = " select sum( rate * qty * tax_rate/100)  "
//					+ "  from xpos_sales_dtl where parent_id  = ?  and tax_rate = ?";
			// PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);

			// pst.setString(1, ParentId);
			// pst.setDouble(2, vatrate);
			// ResultSet rs = pst.executeQuery();

			// while (rs.next()) {
			InvTotal = ojjList.getBody();
			// }
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

//=============================================================================================================//
	public static double getItemTotalTaxSummary(String ParentId) {

		double InvTotal = 0.0;

		try {

			/*
			 * String sqlstr = "select sum( rate * qty * tax_rate/100)  " +
			 * "  from xpos_sales_dtl where parent_id  = ?  "; PreparedStatement pst =
			 * APlicationWindow.LocalConn.prepareStatement(sqlstr);
			 * 
			 * pst.setString(1, ParentId); // pst.setDouble(2, vatrate); ResultSet rs =
			 * pst.executeQuery();
			 * 
			 * while (rs.next()) {
			 */
			ResponseEntity<Double> ojjList = RestCaller.getAllTaxAmount(ParentId);

			InvTotal = ojjList.getBody();
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public static double getItemTotalCessSummary(String ParentId) {

		double InvTotal = 0.0;

		try {

			/*
			 * String sqlstr = "select sum( rate * qty * tax_rate/100)  " +
			 * "  from xpos_sales_dtl where parent_id  = ?  "; PreparedStatement pst =
			 * APlicationWindow.LocalConn.prepareStatement(sqlstr);
			 * 
			 * pst.setString(1, ParentId); // pst.setDouble(2, vatrate); ResultSet rs =
			 * pst.executeQuery();
			 * 
			 * while (rs.next()) {
			 */
			ResponseEntity<Double> ojjList = RestCaller.getAllCessAmount(ParentId);

			InvTotal = ojjList.getBody();
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public void PrintInvoiceThermalPrinter(String sdate) throws SQLException {

		DefaultTableModel model = null;

		// stocktransferoutreport/getreportbyhdrid/{hdrid}

		this.date = sdate;

		double invoice_amount = 0.0;
		BigDecimal TotalTax = new BigDecimal("0");

		double invoice_discount = 0.0;

		double OtherChartges = 0.0;
		String remark1 = "";

		String remark2 = "";

		String TelNo = "";

		String deleivery_address = "";
		String delivery_vehicle = "";

		String strInvoiceDate = "";
		String deliveryAddrerss = "";
		String logoPath = "";
		int X1 = 0;
		int Y1 = 0;
		float x2 = 0f;
		float y2 = 0f;

		// HotCakesStockTransferPrint ps = new HotCakesStockTransferPrint();

		// SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(parentID);

		ResponseEntity<List<ReceiptModeReport>> receiptModeWiseDailySaleResp = RestCaller
				.ReceiptModeWiseDailySale(SystemSetting.getUser().getBranchCode(), sdate);

		List<ReceiptModeReport> receiptModeWiseDailySale = receiptModeWiseDailySaleResp.getBody();

		// ===========================================MAP-163-denomination-in-day-end-print=====================================
		ResponseEntity<DayEndClosureHdr> dayEndClosureHdrResp = RestCaller.getDayEndClosureByDate(sdate);
		DayEndClosureHdr dayEndClosureHdr = dayEndClosureHdrResp.getBody();

		if (null == dayEndClosureHdr) {
			return;
		}

		ResponseEntity<List<DayEndClosureDtl>> dayEndClosureDtlResp = RestCaller.getDayEndClosureDtl(dayEndClosureHdr);
		List<DayEndClosureDtl> dayEndClosureDtlList = dayEndClosureDtlResp.getBody();

		// ===========================MAP-163-denomination-in-day-end-print===
		// end=====================

		dayEndDate = sdate;

		// invoice_amount =stockTransferOutReport.getAmount();

		voucher_type = "DAY END";

		invoice_discount = 0.0;// salesTransHdr.getInvoiceDiscount();

		int i = 0;
		BigDecimal TotalAmount = new BigDecimal("0");

		int jj = 1;

		GrandTotalIncludingTax = 0.0;
		for (ReceiptModeReport receiptModeReport : receiptModeWiseDailySale) {

			GrandTotalIncludingTax = GrandTotalIncludingTax + receiptModeReport.getAmount();

			String itemName = (String) receiptModeReport.getReceiptMode();
			BigDecimal Amount = new BigDecimal((Double) receiptModeReport.getAmount());

			Amount = Amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			String hexMOP = "";

			// int j = itemName.length();
			String itemNameLeft = "";
			int lineLength = 20;
			int sublineCount = 0;
			int ii = 0;
			String strToPrint = "";

			// JTable exampleJTable = new JTable(data, columnNames);

			if (i == 0) {
				Object[][] insertRowData = { { jj, itemName, Amount } };
				model = new DefaultTableModel(insertRowData, title);
				i = i + 1;

				// Object[][] rowData2=new Object[][] { { "", hsn_code, "@"+TaxRate+"%", "",""
				// },new Object[] { "itemid", "desc ", "rate", "Amt" }};
				// model.addRow(rowData2);

			} else {

				Object[] insertRowData2 = { jj, itemName, Amount };
				model.insertRow(i, insertRowData2);
				i = i + 1;

			}

			jj = jj + 1;
		}

		// ===============================MAP-163-denomination-in-day-end-print========================

		if (null == model) {

			Object[][] insertRowData = { { "", "", "" } };
			model = new DefaultTableModel(insertRowData, title);

			i = i + 1;

			Object[] insertRowData2 = { "", "DENOMINATION", "COUNT" };
			model.insertRow(i, insertRowData2);
			i = i + 1;
		} else {

			Object[] insertRowData = { "", "", "" };
			model.insertRow(i, insertRowData);
			i = i + 1;

			Object[] insertRowData2 = { "", "DENOMINATION", "COUNT" };
			model.insertRow(i, insertRowData2);
			i = i + 1;
		}

		Double totalPhysicalCash = 0.0;

		jj = 1;
		for (DayEndClosureDtl dayENd : dayEndClosureDtlList) {

			Object[] insertRowData2 = { jj, dayENd.getDenomination(), dayENd.getCount()+"" };
			model.insertRow(i, insertRowData2);

			totalPhysicalCash = totalPhysicalCash + (dayENd.getDenomination() * dayENd.getCount());

			i = i + 1;

			jj = jj + 1;

		}

		Object[] insertRowData = { "", "", "" };
		model.insertRow(i, insertRowData);
		i = i + 1;

		Object[] insertRowData2 = { "", "Physical Cash =", totalPhysicalCash };
		model.insertRow(i, insertRowData2);

		// ===========================MAP-163-denomination-in-day-end-print===
		// end=====================

		// model.removeRow(1);

		JTable table = new JTable(model);

		Object printitem[][] = this.getTableData(table);
		this.setItems(printitem);

		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setPrintable(new MyPrintable(), this.getPageFormat(pj));
		try {
			pj.print();

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}

	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	@Override
	public void setupBottomline(String bottomLine) throws IOException {
		invoiceBottomLine = bottomLine;

	}

	@Override
	public void setupBottomline2(String bottomLine) throws IOException {
		invoiceBottomLine2 = bottomLine;

	}

	@Override
	public void setupBottomline3(String bottomLine) throws IOException {
		invoiceBottomLine3 = bottomLine;

	}

}
/*
 * ################# THIS IS HOW TO USE THIS CLASS #######################
 * 
 * Printsupport ps=new Printsupport(); Object printitem
 * [][]=ps.getTableData(jTable); ps.setItems(printitem);
 * 
 * PrinterJob pj = PrinterJob.getPrinterJob(); pj.setPrintable(new
 * MyPrintable(),ps.getPageFormat(pj)); try { pj.print();
 * 
 * } catch (PrinterException ex) { ex.printStackTrace(); } ##################
 * JOIN TO SHARE KNOWLADGE ###########################
 * 
 */