package com.maple.javapos.print;

import java.io.IOException;
import java.sql.SQLException;


public abstract class POSThermalPrintABS {

	public abstract void PrintInvoiceThermalPrinter(String ParentID) throws SQLException ;
	public abstract void setupLogo() throws IOException;
	public abstract void setupBottomline(String bottomLine) throws IOException;
	public abstract void setupBottomline2(String bottomLine) throws IOException;
	public abstract void setupBottomline3(String bottomLine) throws IOException;
	
}
