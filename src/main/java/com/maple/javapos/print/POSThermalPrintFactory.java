package com.maple.javapos.print;

import com.maple.maple.util.SystemSetting;

public class POSThermalPrintFactory {

	public static POSThermalPrintABS getPOSThermalPrintABS(){
		
		System.out.println("FORMAT  " + SystemSetting.getPosFormat());
		if(SystemSetting.getPosFormat().equalsIgnoreCase("IMPERIAL")){
			return new ImperialPOSPrint();
		}else if(SystemSetting.getPosFormat().equalsIgnoreCase("LEKSHMI")){
			return new LekshmiPOSPrint();
			
		}else if(SystemSetting.getPosFormat().equalsIgnoreCase("TAX")){
			return new PosTaxLayoutPrint();
			
		}else if(SystemSetting.getPosFormat().equalsIgnoreCase("GSTSUMMARY")){
			return new PosTaxLayoutGSTSummaryPrint();
			
		}else if(SystemSetting.getPosFormat().equalsIgnoreCase("NORMAL")){
			//NOTAX Format
			System.out.println("FORMAT IS NO TAX");
			return new PosNoTaxLayoutPrint();
			
		}else if(SystemSetting.getPosFormat().equalsIgnoreCase("KOT")){
			return new ImperialKitchenPrint2();
			
		}
		else if(SystemSetting.getPosFormat().equalsIgnoreCase("TAX2")){
			return new PosTaxLayoutPrint2();
			
		}
		else if(SystemSetting.getPosFormat().equalsIgnoreCase("HOTCAKES")){
			return new HotCakesPOSPrint();
			
		}
		else {
			return new PosTaxLayoutPrint();
		}
		
	}
}
