 

package com.maple.javapos.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.restService.RestCaller;
 

 


/**
 *
 * @author All Open source developers
 * @version 1.0.0.0
 * @since 2014/12/22
 */
/*
 * This Printsupport java class was implemented to get printout. This class was
 * specially designed to print a Jtable content to a paper. Specially this class
 * formated to print 7cm width paper. Generally for pos thermel printer. Free to
 * customize this source code as you want. Illustration of basic invoice is in
 * this code. demo by gayan liyanaarachchi
 * 
 */

public class ImperialKitchenPrint2 extends POSThermalPrintABS{
	 
	
	private static final Logger log = LoggerFactory.getLogger(BarcodePrinterZeebra.class);

	static JTable itemsTable;
	public static int total_item_count = 0;
	
	public static final String DATE_FORMAT_NOW = "dd/MM/yyyy hh:mm  a";
	public static String title[] = new String[] { "Srl", "Item Name",  "Qty", "Price"};
	  static String parentID;
 
 public static String invoiceBottomLine ="";
 public static String invoiceBottomLine2 ="";
 public static String invoiceBottomLine3 ="";
		 
		static BufferedImage read ;
	  public void setupLogo() throws IOException {
			
		  if(null==read) {
			  
			  FileSystem fs =  FileSystems.getDefault();
		  Path path = fs.getPath(SystemSetting.getLogo_name());
			 
			 
			// List<String> lines = Files.readAllLines(path, charset);

			InputStream iStream =  Files.newInputStream(path); 
			
			read = ImageIO.read(iStream);
			
			iStream.close();
			fs.close();
		  }
		}
			
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PosTaxLayoutPrint ps = new PosTaxLayoutPrint();

		DefaultTableModel model = new DefaultTableModel(
				new Object[][] { { 1, "text", 1, 1 }, { 2, "text", 3, 3 }, { 3, "m4ore", 5, 5 }, { 4, "strings", 5, 5 },
						{ 5, "other", 7, 7 }, { 6, "values", 6, 6 } },
				new Object[] { "itemid", "desc ", "rate", "Qty", "Amt" });

		JTable table = new JTable(model);

		Object printitem[][] = ps.getTableData(table);
		ps.setItems(printitem);

		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setPrintable(new MyPrintable(), ps.getPageFormat(pj));
		try {
			pj.print();

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}

	}

	public static void setItems(Object[][] printitem) {
		Object data[][] = printitem;
		DefaultTableModel model = new DefaultTableModel();
		// assume jtable has 4 columns.
		model.addColumn(title[0]);
		model.addColumn(title[1]);
		model.addColumn(title[2]);
		model.addColumn(title[3]);
		//model.addColumn(title[4]);
 
		int rowcount = printitem.length;

		addtomodel(model, data, rowcount);

		itemsTable = new JTable(model);
		itemsTable.setRowSorter(null);
	}

	public static void addtomodel(DefaultTableModel model, Object[][] data, int rowcount) {
		int count = 0;
		while (count < rowcount) {
			model.addRow(data[count]);
			count++;
		}
		if (model.getRowCount() != rowcount)
			addtomodel(model, data, rowcount);

		System.out.println("Check Passed.");
	}

	public Object[][] getTableData(JTable table) {
		int itemcount = table.getRowCount();
		System.out.println("Item Count:" + itemcount);

		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
		Object[][] tableData = new Object[nRow][nCol];
		if (itemcount == nRow) // check is there any data loss.
		{
			for (int i = 0; i < nRow; i++) {
				for (int j = 0; j < nCol; j++) {
					tableData[i][j] = dtm.getValueAt(i, j); // pass data into object array.
				}
			}
			if (tableData.length != itemcount) { // check for data losses in object array
				getTableData(table); // recursively call method back to collect data
			}
			System.out.println("Data check passed");
		} else {
			// collecting data again because of data loss.
			getTableData(table);
		}
		return tableData; // return object array with data.
	}

	public static PageFormat getPageFormat(PrinterJob pj) {
		PageFormat pf = pj.defaultPage();
		Paper paper = pf.getPaper();

		double middleHeight = total_item_count * 1.0; // dynamic----->change with the row count of jtable
		double headerHeight = 10; // fixed----->but can be mod
		double footerHeight = 500.0; // fixed----->but can be mod

		double width = convert_CM_To_PPI(7); // printer know only point per inch.default value is 72ppi
		double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
		paper.setSize(width, height);
		paper.setImageableArea(convert_CM_To_PPI(0.001), convert_CM_To_PPI(0.05), width - convert_CM_To_PPI(0.35),
				height - convert_CM_To_PPI(1)); // define boarder size after that print area width is about 180 points

		pf.setOrientation(PageFormat.PORTRAIT); // select orientation portrait or landscape but for this time portrait
		pf.setPaper(paper);

		return pf;
	}

	protected static double convert_CM_To_PPI(double cm) {
		return toPPI(cm * 0.393600787);
	}

	protected static double toPPI(double inch) {
		return inch * 72d;
	}

	public static String now() {
		// get current date and time as a String output
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());

	}

	public static class MyPrintable implements Printable {
		public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
			int result = NO_SUCH_PAGE;
			if (pageIndex == 0) {
				
				
				
				
				
				Graphics2D g2d = (Graphics2D) graphics;

				double width = pageFormat.getImageableWidth();
				double height = pageFormat.getImageableHeight();
				g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
				Font font = new Font("MONOSPACED", Font.BOLD, 12);
				Font font2 = new Font("Monospaced", Font.PLAIN, 7);
				g2d.setFont(font);

				//try {
					/*
					 * Draw Image* assume that printing reciept has logo on top that logo image is
					 * in .gif format .png also support image resolution is width 100px and height
					 * 50px image located in root--->image folder
					 */
					int x = 1; // print start at 100 on x axies
					int y = 1; // print start at 10 on y axies
					int imagewidth =150;
					int imageheight = 50;
					
					
					//String logoName = App.cSqlFunctions11.GetParamValue("LOGO_NAME_HEADER");
					//if(logoName.length()>0){
					//BufferedImage read = ImageIO.read(getClass().getResource(logoName));
					if(null!=read) {
						g2d.drawImage(read, x, y, imagewidth, imageheight, null); // draw image
					}
			
					//}
					
					
					g2d.setFont(font2);
					
					
					//g2d.drawLine(10, y  , 180, y ); // draw line
				//} catch (IOException e) {
				//	e.printStackTrace();
				//}
				try {
					/* Draw Header */
					 y = 30;
					
					
					 
					
					String SalesManId = "";
					//String Voucher_number = "";
					//Timestamp Voucher_time  = null;
					Date invoice_date = null;

					/*
					 * String sqlstr1 = "select   t.invoice_date  from   sales_trans_hdr t" +
					 * " where     t.record_id   =  '" + parentID + "'";
					 */
 

				 
			 
					font = new Font(Font.SERIF, Font.BOLD, 12); 	
					g2d.setFont(font);
					//g2d.drawString(CompanyNameTitle, 30, y);
				
					
					
					 
 
					 
					 String InvoiceTimeToPrint = "";
					 
					// Date date = new Date(Voucher_time.getTime());
					 String s1 = "";
					 if(null!=invoice_date) {
					   s1 =  SystemSetting.SqlDateTostring(invoice_date);
					 }else {
						 invoice_date = (Date) SystemSetting.getSystemDate();
					 }
					 //String s2 =  CJavaFunctions.SqlDateTostring(Voucher_time);
							 
				   // InvoiceTimeToPrint = " TABLE :" +TableName;
					//g2d.drawString(InvoiceTimeToPrint  , 10, y + 20);
					//y +=10;
					 
					 
					 
					 
					 InvoiceTimeToPrint = "Date  "+  now()   ;
						g2d.drawString(InvoiceTimeToPrint  , 10, y + 20);
						 
					
					font = new Font("Arial", Font.BOLD, 8); 	
					g2d.setFont(font);
					
					/* Draw Colums */
					g2d.drawLine(1, y + 30, 180, y + 30);
					g2d.drawString(title[0], 1, y + 40);
					g2d.drawString(title[1], 30, y + 40);
					g2d.drawString(title[2], 130, y + 40);
					g2d.drawString(title[3], 160, y + 40);
					//g2d.drawString(title[4], 150, y + 40);
					g2d.drawLine(1, y + 50, 180, y + 50);

					int cH = 0;
					TableModel mod = itemsTable.getModel();
					int lastLine = 0;
					for (int i = 0; i < mod.getRowCount(); i++) {
						/*
						 * Assume that all parameters are in string data type for this situation All
						 * other premetive data types are accepted.
						 */
						String itemid = mod.getValueAt(i, 0).toString();
						String itemname = mod.getValueAt(i, 1).toString();
						if(itemname.length()>15){
							itemname = itemname.substring(0, 15);
						}
						String price = mod.getValueAt(i, 2).toString();
						try {
						int intP = Integer.parseInt(price);
						price=intP+"";
						}catch(Exception e) {
							
						}
						
						String quantity = mod.getValueAt(i, 3).toString();
						//String amount = mod.getValueAt(i, 4).toString();

						cH = (y + 60) + (10 * i); // shifting drawing line

						font = new Font("Arial", Font.BOLD, 10); // changed font size
						g2d.setFont(font);
					
						g2d.drawString(itemid, 0, cH);
						g2d.drawString(itemname, 10, cH);
						
						g2d.drawString(quantity, 130, cH);
						g2d.drawString(price, 160, cH);
						//String amountFmt = formatDecimal(Float.parseFloat(amount));
						//g2d.drawString(padLeft(amount,10), 150, cH);
						lastLine = i;

					}
					cH = (y + 60) + (10 * (lastLine + 2));
					g2d.drawString("", 150, cH);

					/* Footer */
					
					
					 
					/*
					 * String sqlstr = "select distinct tax_rate  " +
					 * "  from xpos_sales_dtl where parent_id  =  '" + parentID + "'";
					 * 
					 * PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);
					 * 
					 * ResultSet rs = pst.executeQuery();
					 */
						/*while (rs.next()) {
							double vatrate = rs.getDouble(1);
							if (vatrate > 0) {
								
								cH = cH + 10 ; 
								double ItemTotalTax = getItemTotalTax(vatrate, parentID);
								
								
						 
								
								double sgst = ItemTotalTax/2;
								String Taxstr = String.format("%4.2f", sgst);
								
								String vatTitle = vatrate/2+"% SGST Tax" ;
								Taxstr = String.format("%4.2f", sgst); 
								Taxstr = padLeft(Taxstr, 30-vatTitle.length());
								
								 
								g2d.drawString(vatrate/2+"% SGST Tax"  + Taxstr, 10, cH );
								 
								cH = cH + 10 ; 

								 vatTitle = vatrate/2+"% CGST Tax";
								   Taxstr = String.format("%4.2f", sgst); 
									Taxstr = padLeft(Taxstr, 30-vatTitle.length());
								   g2d.drawString(vatrate/2+"% CGST Tax"  + Taxstr, 10, cH );
								  
										 
										  
							}
						}*/
					List<Object> ojjList1 = RestCaller.getAllTaxRate(parentID);
					//ResultSet rs = pst.executeQuery();
				 for(int i=0;i<ojjList1.size();i++)
				 {
					 Object[] objAray = (Object[]) ojjList1.get(i);
				
						double vatrate = ((double)objAray[0]);
						if (vatrate > 0) {
							
							cH = cH + 10 ; 
							double ItemTotalTax = getItemTotalTax(vatrate, parentID);
							
							
					 
							
							double sgst = ItemTotalTax/2;
							String Taxstr = String.format("%4.2f", sgst);
							
							String vatTitle = vatrate/2+"% SGST Tax" ;
							Taxstr = String.format("%4.2f", sgst); 
							Taxstr = padLeft(Taxstr, 30-vatTitle.length());
							
							 
							g2d.drawString(vatrate/2+"% SGST Tax"  + Taxstr, 10, cH );
							 
							cH = cH + 10 ; 

							 vatTitle = vatrate/2+"% CGST Tax";
							   Taxstr = String.format("%4.2f", sgst); 
								Taxstr = padLeft(Taxstr, 30-vatTitle.length());
							   g2d.drawString(vatrate/2+"% CGST Tax"  + Taxstr, 10, cH );
							  
									 
									  
						}
					}
						cH = cH + 10 ; 
						double ItemTotalTax = getItemTotalTaxSummary( parentID);

						double sgst = ItemTotalTax/2;
						double cgst = ItemTotalTax/2;
						String Taxstr = String.format("%4.2f", sgst);
						
						
						String tVatTitle = "Total  SGST Tax" ; 
						
						Taxstr = padLeft(Taxstr, 30-tVatTitle.length());
						
						//g2d.drawString(tVatTitle  + Taxstr, 10, cH );
						//cH = cH + 10 ; 
						
						
						  tVatTitle = "Total  CGST Tax" ;

						  Taxstr = padLeft(Taxstr, 30-tVatTitle.length());
						//g2d.drawString(tVatTitle  + Taxstr, 10, cH );
						//cH = cH + 20 ; 
						
						double GrandTotalIncludingTax = getGrandTotalIncTax(parentID);
						
						String strGrandTotalIncludingTax = String.format("%9.00f", GrandTotalIncludingTax);
						
						font = new Font("Arial", Font.BOLD, 12); // changed font size
						g2d.setFont(font);
						String stg = "Grand Total:  ";
						//g2d.drawString(stg +  padLeft( strGrandTotalIncludingTax,25)+" /-", 10, cH );
						//cH = cH + 10 ; 
						
						g2d.drawLine(1,cH, 180, cH);
						cH = cH + 20 ;
					
					//font = new Font("Arial", Font.BOLD, 12); // changed font size
					//g2d.setFont(font);
					//g2d.drawString("Thank you.  Have a nice day ", 10, cH );
					// end of the reciept
				} catch (Exception r) {
					r.printStackTrace();
				}

				result = PAGE_EXISTS;
			}
			return result;
		}
	}

	public static String padLeft(String s, int n) {
	    return String.format("%1$" + n + "s", s);  
	}
	public static String formatDecimal(float number) {
		  float epsilon = 0.004f; // 4 tenths of a cent
		  if (Math.abs(Math.round(number) - number) < epsilon) {
		     return String.format("%10.0f", number); // sdb
		  } else {
		     return String.format("%10.2f", number); // dj_segfault
		  }
		}
	
	public static double getItemTotalTax(double vatrate, String ParentId) {

		double InvTotal = 0.0;

		try {

			/*String sqlstr = "select sum( rate * qty * tax_rate/100)  " + "  from xpos_sales_dtl where parent_id  = ?  ";
			PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);

			pst.setString(1, ParentId);
			// pst.setDouble(2, vatrate);
			ResultSet rs = pst.executeQuery();

			while (rs.next()) {*/
			ResponseEntity<Double> ojjList =  RestCaller.getAllTaxAmount(ParentId);

				InvTotal = ojjList.getBody() ;
			}
		 catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public static double getItemTotalTaxSummary( String ParentId ) {

		double InvTotal = 0.0;

		try {

			/*String sqlstr = "select sum( rate * qty * tax_rate/100)  " + "  from xpos_sales_dtl where parent_id  = ?  ";
			PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);

			pst.setString(1, ParentId);
			// pst.setDouble(2, vatrate);
			ResultSet rs = pst.executeQuery();

			while (rs.next()) {*/
			ResponseEntity<Double> ojjList =  RestCaller.getAllTaxAmount(ParentId);

				InvTotal = ojjList.getBody();
			}
		 catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}
	
	public static double getGrandTotalIncTax( String ParentId ) {

		double InvTotal = 0.0;

		try {
//
//			String sqlstr = "select sum(( rate * qty * tax_rate/100) + (rate * qty))   "
//					+ "  from xpos_sales_dtl where parent_id  = ?  ";
			 
			ResponseEntity<Double> ojjList =  RestCaller.getAllTaxAmount(ParentId);
		//	pst.setString(1, ParentId);
			// pst.setDouble(2, vatrate);
			//ResultSet rs = pst.executeQuery();

			//while (rs.next()) {
				InvTotal =  ojjList.getBody() ;
			}
		 catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}
	
 
	public static synchronized  void  PrintInvoiceThermalPrinter(String ParentID, String tableName ) throws SQLException {
		
	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	@Override
	public void PrintInvoiceThermalPrinter(String ParentID) throws SQLException {


		DefaultTableModel model = null;
			
	  	
		BigDecimal TotalTax = new BigDecimal("0");
		
 
		//String logoPath = "";
		int X1 = 0;
		int Y1 = 0;
		float x2 = 0f;
		float y2 = 0f;
		

		//ImperialKitchenPrint2 ps = new ImperialKitchenPrint2();
 
				
		
/*
		try {

			PreparedStatement preparedStatement2 = App.LocalConn
					.prepareStatement("select * from pre_print_logo where bill_name  = 'POS' ");

			preparedStatement2.execute();
			ResultSet rs2 = preparedStatement2.getResultSet();

			if (rs2.next()) {

				logoPath = rs2.getString("logo_path");
				X1 = rs2.getInt("X1");
				Y1 = rs2.getInt("Y1");

				x2 = rs2.getFloat("X2");
				y2 = rs2.getFloat("Y2");
			}
		} catch (Exception e) {
			log.info("Error" + e.toString());
		}

		*/
List<Object> ojjList = RestCaller.getSalesTransHdrandItem(parentID);
		
		
		int i = 0;
		BigDecimal TotalAmount = new BigDecimal("0");
		
		  int jj=1;
		//model.removeRow(0);
		  for(int k=0;k<ojjList.size();k++)
			 {
				 Object[] objAray = (Object[]) ojjList.get(k);
		//while (rs.next()) {
			

				 String itemCode = ((String)objAray[1]);
					String hsn_code = ((String)objAray[0]);


			if (null == hsn_code) {
				hsn_code = "";
			}
			
			BigDecimal TaxRate = ((BigDecimal)objAray[2]);
			TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal Qty =((BigDecimal)objAray[3]);
			Qty =Qty.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal Rate = ((BigDecimal)objAray[4]);
			Rate = Rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			String itemName = ((String)objAray[5]);
			BigDecimal Amount =((BigDecimal)objAray[6]);
 
			
			
			//String item_code = rs.getString("item_code");

			BigDecimal AmountIncludingTax = (Qty.multiply(Rate)).add(Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100"))))) ;
			AmountIncludingTax = AmountIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN) ;
			
			
			
			
			BigDecimal rateIncludingTax = Rate.add(Rate.multiply(TaxRate.divide(new BigDecimal("100")))) ;
			rateIncludingTax = rateIncludingTax.setScale(2,  BigDecimal.ROUND_HALF_EVEN);

			BigDecimal TaxAmount = (Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100"))))) ;
			TaxAmount =TaxAmount.setScale(2,  BigDecimal.ROUND_HALF_EVEN);

			TotalTax = TotalTax.add(TaxAmount) ;
			
			
			Amount = Amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			TotalAmount = TotalAmount.add(Amount);;

			int Mop = 0;
			String hexMOP = "";

			int j = itemName.length();
			String itemNameLeft = "";
			int lineLength = 20;
			int sublineCount = 0;
			int ii = 0;
			String strToPrint = "";

			BigDecimal sgst_rate = TaxRate.divide(new BigDecimal("2"));

			BigDecimal cgst_rate = TaxRate.divide(new BigDecimal("2"));

			BigDecimal bdSgst_rate =  sgst_rate;
			BigDecimal bdCgst_rate =  cgst_rate;

			BigDecimal bdIgst_rate =  cgst_rate.add(sgst_rate);

			bdSgst_rate = bdSgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			bdCgst_rate = bdCgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			bdIgst_rate = bdIgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			
			

			
			  if(i==0) {
			Object[][] insertRowData = {{ jj,itemName,rateIncludingTax ,Qty,AmountIncludingTax}};
			 model = new DefaultTableModel(insertRowData,title);
			i=i+1;
			 
			 
		
			  
			  }else {
				  
				  Object[] insertRowData2 = {jj,itemName,rateIncludingTax ,Qty,AmountIncludingTax};
				  model.insertRow(i,insertRowData2); 
				  i=i+1;


			  }
			 
			  jj = jj+1;
		}
		//model.removeRow(1);
		BigDecimal dbsgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);
		BigDecimal dbcgstAmount =  TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);

		BigDecimal bdSgstAmount =  dbsgstAmount;
		BigDecimal bdCgstAmount = dbcgstAmount;
		bdSgstAmount = bdSgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		bdCgstAmount = bdCgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		
		
		JTable table = new JTable(model);

		Object printitem[][] = this.getTableData(table);
		ImperialKitchenPrint2.setItems(printitem);

		PrinterJob pj = PrinterJob.getPrinterJob();
		
		  PageFormat pf = pj.defaultPage();
          Paper paper = new Paper();
          double margin = 0.1; 
          paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight()
              - margin * 2);
          pf.setPaper(paper);
          
  
		
		pj.setPrintable(new MyPrintable(), ImperialKitchenPrint2.getPageFormat(pj));
		
		
		
		try {
			pj.print();
			
			
			
			if(SystemSetting.hasCashDrawer()) {
				//--------- OPEN CASH DRAWER -------
					 byte[] open = {27, 112, 48, 55, 121};
			        DocPrintJob job = pj.getPrintService().createPrintJob();
			        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
			        Doc doc = new SimpleDoc(open,flavor,null);
			        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
			        try {
			            job.print(doc, aset);
			        } catch (PrintException ex) {
			            System.out.println(ex.getMessage());
			        }
				}

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}
		
		
		

	
		
	}

	@Override
	public void setupBottomline(String bottomLine) throws IOException {
		invoiceBottomLine =bottomLine;
		
	}

	
	@Override
	public void setupBottomline2(String bottomLine) throws IOException {
		invoiceBottomLine2 =bottomLine;
		
	}
	
	@Override
	public void setupBottomline3(String bottomLine) throws IOException {
		invoiceBottomLine3 =bottomLine;
		
	}

}
 