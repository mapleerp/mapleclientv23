package com.maple.javapos.print;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.BranchMst;
import com.maple.mapleclient.entity.CustomInvoicePropertyFormat;
import com.maple.mapleclient.entity.ParamValueConfig;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;

/*
 * This model is deployed in HotCakes
 * Tax is printed 
 * Logo is placed at top
 * Tabular layout for item details
 */
public class PosTaxLayoutPrint extends POSThermalPrintABS {
	private static final Logger log = LoggerFactory.getLogger(PosTaxLayoutPrint.class);
	static JTable itemsTable;
	public static int total_item_count = 0;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss a";
	public static String title[] = new String[] { "Srl", "Item Name", "Price", "Qty", "Amount" };
	static String parentID;
	public static String branchCode = "";
	public static String invoiceBottomLine = "";

	public static String invoiceBottomLine2 ="";
	 public static String invoiceBottomLine3 ="";
	 
	
	static BufferedImage read;

	public void setupLogo() throws IOException {

		
		if(null!=read) {
			return;
		}
		FileSystem fs =  FileSystems.getDefault();
		Path path = fs.getPath(SystemSetting.getLogo_name());

		InputStream iStream = Files.newInputStream(path);

		read = ImageIO.read(iStream);
		
		iStream.close();
		fs.close();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		PosTaxLayoutPrint ps = new PosTaxLayoutPrint();

		DefaultTableModel model = new DefaultTableModel(
				new Object[][] { { 1, "text", 1, 1 }, { 2, "text", 3, 3 }, { 3, "m4ore", 5, 5 }, { 4, "strings", 5, 5 },
						{ 5, "other", 7, 7 }, { 6, "values", 6, 6 } },
				new Object[] { "itemid", "desc ", "rate", "Qty", "Amt" });

		JTable table = new JTable(model);

		Object printitem[][] = ps.getTableData(table);
		ps.setItems(printitem);

		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setPrintable(new MyPrintable(), ps.getPageFormat(pj));
		try {
			pj.print();

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}

	}

	public static void setItems(Object[][] printitem) {
		Object data[][] = printitem;
		DefaultTableModel model = new DefaultTableModel();

		model.addColumn(title[0]);
		model.addColumn(title[1]);
		model.addColumn(title[2]);
		model.addColumn(title[3]);
		model.addColumn(title[4]);

		int rowcount = printitem.length;

		addtomodel(model, data, rowcount);

		itemsTable = new JTable(model);
		itemsTable.setRowSorter(null);
	}

	public static void addtomodel(DefaultTableModel model, Object[][] data, int rowcount) {
		int count = 0;
		while (count < rowcount) {
			model.addRow(data[count]);
			count++;
		}
		if (model.getRowCount() != rowcount)
			addtomodel(model, data, rowcount);

		System.out.println("Check Passed.");
	}

	public Object[][] getTableData(JTable table) {
		int itemcount = table.getRowCount();
		System.out.println("Item Count:" + itemcount);

		DefaultTableModel dtm = (DefaultTableModel) table.getModel();
		int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
		Object[][] tableData = new Object[nRow][nCol];
		if (itemcount == nRow) // check is there any data loss.
		{
			for (int i = 0; i < nRow; i++) {
				for (int j = 0; j < nCol; j++) {
					tableData[i][j] = dtm.getValueAt(i, j); // pass data into object array.
				}
			}
			if (tableData.length != itemcount) { // check for data losses in object array
				getTableData(table); // recursively call method back to collect data
			}
			System.out.println("Data check passed");
		} else {
			// collecting data again because of data loss.
			getTableData(table);
		}
		return tableData; // return object array with data.
	}

	public static PageFormat getPageFormat(PrinterJob pj) {
		PageFormat pf = pj.defaultPage();
		Paper paper = pf.getPaper();

		double middleHeight = total_item_count * 1.0; // dynamic----->change with the row count of jtable
		double headerHeight = 5.0; // fixed----->but can be mod
		double footerHeight = 200.0; // fixed----->but can be mod

		double width = convert_CM_To_PPI(7); // printer know only point per inch.default value is 72ppi
		double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
		paper.setSize(width, height);
		paper.setImageableArea(convert_CM_To_PPI(0.25), convert_CM_To_PPI(0.5), width - convert_CM_To_PPI(0.35),
				height - convert_CM_To_PPI(1)); // define boarder size after that print area width is about 180 points

		pf.setOrientation(PageFormat.PORTRAIT); // select orientation portrait or landscape but for this time portrait
		pf.setPaper(paper);

		return pf;
	}

	protected static double convert_CM_To_PPI(double cm) {
		return toPPI(cm * 0.393600787);
	}

	protected static double toPPI(double inch) {
		return inch * 72d;
	}

	public static String now() {
		// get current date and time as a String output
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());

	}

	public static class MyPrintable implements Printable {
		@Override
		public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {

			int result = NO_SUCH_PAGE;
			if (pageIndex == 0) {
				Graphics2D g2d = (Graphics2D) graphics;

				double width = pageFormat.getImageableWidth();
				double height = pageFormat.getImageableHeight();
				g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());
				Font font = new Font("Monospaced", Font.PLAIN, 7);
				g2d.setFont(font);

				/*
				 * Draw Image* assume that printing reciept has logo on top that logo image is
				 * in .gif format .png also support image resolution is width 100px and height
				 * 50px image located in root--->image folder
				 */

				// hotcake
				// int x = 100; // print start at 100 on x axies
				// int y = 10; // print start at 10 on y axies

				// hotcake
				// int imagewidth = 70;
				// int imageheight = 50;

				// imperial
				int x = 1; // print start at 100 on x axies
				int y = 1; // print start at 10 on y axies
				// imperial
				int imagewidth = 150;
				int imageheight = 50;

				SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(parentID);

				String customerSalesMode = salesTransHdr.getCustomiseSalesMode();
				if (null != customerSalesMode) {

					ResponseEntity<List<CustomInvoicePropertyFormat>> custproperty = RestCaller
							.getCustomInvoiceFormatByCustomSalesMode(customerSalesMode);

					if (null != custproperty.getBody()) {
						setCustomInvoiceProperty(custproperty.getBody());
					}
				}
				x = SystemSetting.getLogox();
				y = SystemSetting.getLogoy();
				imagewidth = SystemSetting.getLogow();
				imageheight = SystemSetting.getLogoh();

				if (null != read) {
					g2d.drawImage(read, x, y, imagewidth, imageheight, null); // draw image
				}
				g2d.drawLine(10, y + 100, 180, y + 100); // draw line

				try {
					/* Draw Header */
					y = 120;

					String SalesManId = "";
					String Voucher_number = "";

					double invoice_amount = 0.0;
					double invoice_discount = 0.0;

					String strInvoiceDate = "";
					String voucher_type = "";
					String customerName = "";
					String customerPhno = "";

					LocalDateTime Voucher_time = null;
					Date invoice_date = null;

//					SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(parentID);

					invoice_date = salesTransHdr.getVoucherDate();

					Voucher_number = salesTransHdr.getVoucherNumber();

					invoice_amount = salesTransHdr.getInvoiceAmount();

					voucher_type = salesTransHdr.getSalesMode() + " SALE";

					/*
					 * Check if the sale is TAKE ORDER if the sale is take order then print customer
					 * name and phone number in the invoice
					 */
					if (null != salesTransHdr.getTakeOrderNumber()
							&& !salesTransHdr.getTakeOrderNumber().equalsIgnoreCase(null)) {
						customerName = salesTransHdr.getLocalCustomerMst().getLocalcustomerName();
						customerPhno = salesTransHdr.getLocalCustomerMst().getPhoneNo();
					}
					if (salesTransHdr.getSalesMode().equalsIgnoreCase("ONLINE")) {

						voucher_type = salesTransHdr.getAccountHeads().getAccountName();
						System.out.println("ONINE CUST" + voucher_type);
					}
					Voucher_time = salesTransHdr.getUpdatedTime();
					// invoice_discount = salesTransHdr.getInvoiceDiscount();

					// Timestamp Voucher_time = null;
					// Date invoice_date = null;
					/*
					 * Pass ID and get Sales Trans HDR
					 * 
					 */

					/*
					 * Pass ID and get BranchMst Details
					 */

					// BranchMst branchMst =
					// RestCaller.getBranchDtls(SystemSetting.getSystemBranch());

					font = new Font("Arial", Font.BOLD, 8);
					g2d.setFont(font);

					g2d.drawString(SystemSetting.getPosinvoicetitle1(), SystemSetting.getTitle1x(),
							SystemSetting.getTitle1y());

					// if (null != branchMst.getBranchState()) {

					// g2d.drawString(branchMst.getBranchState(), 50, y + 10); // shift a line by
					// adding 10 to y value
					g2d.drawString(SystemSetting.getPosinvoicetitle2(), SystemSetting.getTitle2x(),
							SystemSetting.getTitle2y());

					// }

					// if (null != branchMst.getBranchName()) {

					y += 10;
					// g2d.drawString(branchMst.getBranchName(), 50, y + 10); // shift a line by
					// adding 10 to y value
					g2d.drawString(SystemSetting.getPosinvoicetitle3(), SystemSetting.getTitle3x(),
							SystemSetting.getTitle3y());
					// }

					// if (null != branchMst.getBranchName()) {
					y += 10;

					// g2d.drawString(branchMst.getBranchName(), 50, y + 10); // shift a line by
					// adding 10 to y value
					g2d.drawString(SystemSetting.getPosinvoicetitle4(), SystemSetting.getTitle4x(),
							SystemSetting.getTitle4y());

					// }

					// if (null != branchMst.getBranchGst()) {
					y += 10;
					// g2d.drawString(branchMst.getBranchGst(), 50, y + 10); // shift a line
					// by adding
					// 10 to y
					// value
					g2d.drawString(SystemSetting.getPosinvoicetitle5(), SystemSetting.getTitle5x(),
							SystemSetting.getTitle5y());
					// }

					y += 10;

					String InvoiceTimeToPrint = "";

					Double amountInPerforma = 0.0;

					Date InvoiceDate = salesTransHdr.getVoucherDate();

					if (null == InvoiceDate) {
						Voucher_number = "Performa Invoice";
						InvoiceDate = SystemSetting.systemDate;
					}

					if (null == Voucher_number) {
						Voucher_number = "Performa Invoice";
					}
					String s1 = SystemSetting.SqlDateTostring(InvoiceDate);
					String s2 = "TIME ";// SystemSetting.SqlDateTostring(Voucher_time);

					DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

					InvoiceTimeToPrint = Voucher_time.format(dtf);

					// This has to be cuncommendted for time print

					/*
					 * if (!s1.equalsIgnoreCase(s2)) { InvoiceTimeToPrint = "Inv.Date" +
					 * SystemSetting.SqlDateTostring(InvoiceDate) + "(" +
					 * Voucher_time.toLocaleString() + ")";
					 * 
					 * } else { InvoiceTimeToPrint = Voucher_time.toLocaleString(); }
					 */

					g2d.drawString(voucher_type, 80, y + 20); // print voucher type

					g2d.drawString(InvoiceTimeToPrint, 10, y + 20); // print date
//					if(null !=salesTransHdr.getTakeOrderNumber() &&  !salesTransHdr.getTakeOrderNumber().equalsIgnoreCase(null))
//					{
//						if(!customerName.equalsIgnoreCase(null ) && !customerName.equalsIgnoreCase(""))
//							g2d.drawString("Customer Name:"+customerName, 70, y ); 
//					}
					g2d.drawString("Invoice No : " + Voucher_number, 10, y + 30);
//					if(null !=salesTransHdr.getTakeOrderNumber() &&  !salesTransHdr.getTakeOrderNumber().equalsIgnoreCase(null))
//					{
//						if(!customerPhno.equalsIgnoreCase(null ) && !customerPhno.equalsIgnoreCase(""))
//							g2d.drawString("Customer Phno : " + customerPhno, 70, y);
//					}
					if(null != salesTransHdr.getServingTableName())
					{
						
						g2d.drawString("Table : " + salesTransHdr.getServingTableName(), 10, y + 30);
					}
					/* Draw Colums */
					g2d.drawLine(10, y + 40, 180, y + 40);
					g2d.drawString(title[0], 10, y + 50);
					g2d.drawString(title[1], 50, y + 50);
					g2d.drawString(title[2], 100, y + 50);
					g2d.drawString(title[3], 130, y + 50);
					g2d.drawString(title[4], 150, y + 50);
					g2d.drawLine(10, y + 60, 180, y + 60);

					int cH = 0;
					TableModel mod = itemsTable.getModel();
					int lastLine = 0;
					for (int i = 0; i < mod.getRowCount(); i++) {
						/*
						 * Assume that all parameters are in string data type for this situation All
						 * other premetive data types are accepted.
						 */
						String itemid = mod.getValueAt(i, 0).toString();
						String itemname = mod.getValueAt(i, 1).toString();
						if (itemname.length() > 15) {
							itemname = itemname.substring(0, 15);
						}
						String price = mod.getValueAt(i, 2).toString();
						String quantity = mod.getValueAt(i, 3).toString();
						String amount = mod.getValueAt(i, 4).toString();
						if(amount.trim().length()>0) {

							amountInPerforma = amountInPerforma + Double.parseDouble(amount);
						}
						cH = (y + 70) + (10 * i); // shifting drawing line

						font = new Font("Arial", Font.BOLD, 8); // changed font size
						g2d.setFont(font);

						g2d.drawString(itemid, 0, cH);
						g2d.drawString(itemname, 10, cH);
						g2d.drawString(price, 100, cH);
						g2d.drawString(quantity, 130, cH);

						// String amountFmt = formatDecimal(Float.parseFloat(amount));
						g2d.drawString(padLeft(amount, 10), 150, cH);
						lastLine = i;

					}
					cH = (y + 70) + (10 * (lastLine + 2));
					g2d.drawString("", 150, cH);

					/* Footer */
//
//					String sqlstr = "select distinct tax_rate  " + "  from xpos_sales_dtl where parent_id  =  '"
//							+ parentID + "'";
//
//					

					List<Object> ojjList = RestCaller.getAllTaxRate(parentID);

					// PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);

					for (int i = 0; i < ojjList.size(); i++) {
						Double vatrate = (Double) ojjList.get(i);

//					ResultSet rs = pst.executeQuery();
//					while (rs.next()) {

						if (vatrate > 0) {

							cH = cH + 10;
							double ItemTotalTax = getItemTotalTax(vatrate, parentID);

							double sgst = ItemTotalTax / 2;
							String Taxstr = String.format("%4.2f", sgst);
// Not in use
							// g2d.drawString(vatrate + "% SGST Tax" + " " + Taxstr, 150, cH);

							cH = cH + 10;

							Taxstr = String.format("%4.2f", sgst);
							// g2d.drawString(vatrate + "% CGST Tax" + " " + Taxstr, 150, cH);

						}
					}

					cH = cH + 10;
					double ItemTotalTax = getItemTotalTaxSummary(parentID);
					double ItemTotalCess = getItemTotalCessSummary(parentID);
					String cessString = String.format("%4.2f", ItemTotalCess);

					double sgst = ItemTotalTax / 2;
					double cgst = ItemTotalTax / 2;
					String Taxstr = String.format("%4.2f", sgst);

					g2d.drawString("Total  SGST Tax" + "     " + Taxstr, 87, cH);
					cH = cH + 10;

					g2d.drawString("Total  CGST Tax" + "     " + Taxstr, 87, cH);
					cH = cH + 20;

					if (ItemTotalCess > 0) {
						g2d.drawString("Kerala Flood Cess " + "   " + cessString, 80, cH);
						cH = cH + 20;
					}

					double GrandTotalIncludingTax = getGrandTotalIncTax(parentID);
					ResponseEntity<ParamValueConfig> getParam = RestCaller.getParamValueConfig("PosDiscount");
					if (null != getParam.getBody()) {
						if (getParam.getBody().getValue().equalsIgnoreCase("YES")) {

							BigDecimal bdInvAmount = new BigDecimal(invoice_amount);
							bdInvAmount = bdInvAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
							GrandTotalIncludingTax = bdInvAmount.doubleValue();
						}
					}
					System.out.println("GrandTotalIncludingTax" + GrandTotalIncludingTax);
					/*
					 * for performa invoice print
					 */
					if (GrandTotalIncludingTax == 0) {
						if(!(sgst > 0))
						{
							sgst =0;
						}
						if(!(ItemTotalCess>0))
						{
							ItemTotalCess = 0;
						}
							GrandTotalIncludingTax = amountInPerforma +sgst +sgst+ItemTotalCess ;
						
					}
					String strGrandTotalIncludingTax = String.format("%9.00f", GrandTotalIncludingTax);

					font = new Font("Arial", Font.BOLD, 10); // changed font size
					g2d.setFont(font);
					String stg = "Grand Total:  ";
					g2d.drawString(stg + "                    " + strGrandTotalIncludingTax + " /-", 10, cH);
					cH = cH + 10;

					g2d.drawLine(10, cH, 180, cH);
					cH = cH + 20;

					// User

					font = new Font("Arial", Font.PLAIN, 8); // changed font size
					g2d.setFont(font);
					g2d.drawString("Bill Generated By:", 10, cH);
					g2d.drawString(SystemSetting.getUser().getUserName(), 80, cH);

					// Bottom Libne

					cH = cH + 20;
					font = new Font("Arial", Font.BOLD, 8); // changed font size
					g2d.setFont(font);
					g2d.drawString(invoiceBottomLine, 10, cH);
					// end of the reciept
				} catch (Exception r) {
					r.printStackTrace();
				}

				result = PAGE_EXISTS;
			}
			return result;
		}
	}

	public static String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s);
	}

	public static void setCustomInvoiceProperty(List<CustomInvoicePropertyFormat> customPropertyList) {

		for (int i = 0; i < customPropertyList.size(); i++) {
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("print.logo_name")) {
				SystemSetting.setLogo_name(customPropertyList.get(i).getPropertyValue());
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("print.posformat")) {
				SystemSetting.setPosFormat(customPropertyList.get(i).getPropertyValue());
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("print.hascashdrawer")) {
				SystemSetting.setCashDrawerPresent(customPropertyList.get(i).getPropertyValue());
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("logox")) {
				SystemSetting.setLogox(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("logoy")) {
				SystemSetting.setLogoy(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("logow")) {
				SystemSetting.setLogow(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("logoh")) {
				SystemSetting.setLogoh(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("posinvoicetitle1")) {
				SystemSetting.setPosinvoicetitle1(customPropertyList.get(i).getPropertyValue());
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("posinvoicetitle2")) {
				SystemSetting.setPosinvoicetitle2(customPropertyList.get(i).getPropertyValue());
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("posinvoicetitle3")) {
				SystemSetting.setPosinvoicetitle3(customPropertyList.get(i).getPropertyValue());
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("posinvoicetitle4")) {
				SystemSetting.setPosinvoicetitle4(customPropertyList.get(i).getPropertyValue());
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("posinvoicetitle5")) {
				SystemSetting.setPosinvoicetitle5(customPropertyList.get(i).getPropertyValue());
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title1x")) {
				SystemSetting.setTitle1x(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title1y")) {
				SystemSetting.setTitle1y(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title2x")) {
				SystemSetting.setTitle2x(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title2y")) {
				SystemSetting.setTitle2y(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title3x")) {
				SystemSetting.setTitle3x(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title3y")) {
				SystemSetting.setTitle3y(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title4x")) {
				SystemSetting.setTitle4x(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title4y")) {
				SystemSetting.setTitle4y(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title5x")) {
				SystemSetting.setTitle5x(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
			if (customPropertyList.get(i).getPropertyName().equalsIgnoreCase("title5y")) {
				SystemSetting.setTitle5y(Integer.parseInt(customPropertyList.get(i).getPropertyValue()));
			}
		}

	}

	public static String formatDecimal(float number) {
		float epsilon = 0.004f; // 4 tenths of a cent
		if (Math.abs(Math.round(number) - number) < epsilon) {
			return String.format("%10.0f", number); // sdb
		} else {
			return String.format("%10.2f", number); // dj_segfault
		}
	}

//================================================================= Get all tax======================//
	public static double getItemTotalTax(double vatrate, String ParentId) {

		double InvTotal = 0.0;

		try {

			ResponseEntity<Double> ojjList = RestCaller.getTaxAmount(vatrate, ParentId);

//			String sqlstr = " select sum( rate * qty * tax_rate/100)  "
//					+ "  from xpos_sales_dtl where parent_id  = ?  and tax_rate = ?";
			// PreparedStatement pst = APlicationWindow.LocalConn.prepareStatement(sqlstr);

			// pst.setString(1, ParentId);
			// pst.setDouble(2, vatrate);
			// ResultSet rs = pst.executeQuery();

			// while (rs.next()) {
			InvTotal = ojjList.getBody();
			// }
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

//=============================================================================================================//
	public static double getItemTotalTaxSummary(String ParentId) {

		double InvTotal = 0.0;

		try {

			/*
			 * String sqlstr = "select sum( rate * qty * tax_rate/100)  " +
			 * "  from xpos_sales_dtl where parent_id  = ?  "; PreparedStatement pst =
			 * APlicationWindow.LocalConn.prepareStatement(sqlstr);
			 * 
			 * pst.setString(1, ParentId); // pst.setDouble(2, vatrate); ResultSet rs =
			 * pst.executeQuery();
			 * 
			 * while (rs.next()) {
			 */
			ResponseEntity<Double> ojjList = RestCaller.getAllTaxAmount(ParentId);

			InvTotal = ojjList.getBody();
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public static double getItemTotalCessSummary(String ParentId) {

		double InvTotal = 0.0;

		try {

			/*
			 * String sqlstr = "select sum( rate * qty * tax_rate/100)  " +
			 * "  from xpos_sales_dtl where parent_id  = ?  "; PreparedStatement pst =
			 * APlicationWindow.LocalConn.prepareStatement(sqlstr);
			 * 
			 * pst.setString(1, ParentId); // pst.setDouble(2, vatrate); ResultSet rs =
			 * pst.executeQuery();
			 * 
			 * while (rs.next()) {
			 */
			ResponseEntity<Double> ojjList = RestCaller.getAllCessAmount(ParentId);

			InvTotal = ojjList.getBody();
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public static double getGrandTotalIncTax(String ParentId) {

		double InvTotal = 0.0;

		try {
//
//			String sqlstr = "select sum(( rate * qty * tax_rate/100) + (rate * qty))   "
//					+ "  from xpos_sales_dtl where parent_id  = ?  ";

			ResponseEntity<Double> ojjList = RestCaller.getGrandTotalIncludingTax(ParentId);
			// pst.setString(1, ParentId);
			// pst.setDouble(2, vatrate);
			// ResultSet rs = pst.executeQuery();

			// while (rs.next()) {
			InvTotal = ojjList.getBody();
		} catch (Exception e) {

			log.debug(e.toString());
		}
		return InvTotal;
	}

	public void PrintInvoiceThermalPrinter(String ParentID) throws SQLException {

		DefaultTableModel model = null;

		boolean isIGST = false;
		String CustStateCode = " 32 (KL) ";
		this.parentID = ParentID;

		double insurance_comapny = 0.0;
		double cash_paid_amt = 0.0;
		double invoice_amount = 0.0;
		BigDecimal TotalTax = new BigDecimal("0");
		String voucher_type = "";
		double CreditAmount = 0.0;
		double card_paid_amt = 0.0;
		double invoice_discount = 0.0;
		double bank_paid_amt = 0.0;
		double OtherChartges = 0.0;
		String remark1 = "";
		String insurance_cardno = "";
		String remark2 = "";

		String TelNo = "";

		String deleivery_address = "";
		String delivery_vehicle = "";

		Date InvoiceDate = null;
		String InvoiceNo = "";
		String strInvoiceDate = "";
		String deliveryAddrerss = "";
		String logoPath = "";
		int X1 = 0;
		int Y1 = 0;
		float x2 = 0f;
		float y2 = 0f;

		PosTaxLayoutPrint ps = new PosTaxLayoutPrint();

		SalesTransHdr salesTransHdr = RestCaller.getSalesTransHdr(parentID);
		InvoiceDate = salesTransHdr.getVoucherDate();

		InvoiceNo = salesTransHdr.getVoucherNumber();

		invoice_amount = salesTransHdr.getInvoiceAmount();

		if (salesTransHdr.getSalesMode().equalsIgnoreCase("ONLINE")) {

			voucher_type = salesTransHdr.getAccountHeads().getAccountName();
			System.out.println("ONINE CUST " + voucher_type);
		} else {
			voucher_type = salesTransHdr.getSalesMode();
		}

		invoice_discount = 0.0;// salesTransHdr.getInvoiceDiscount();

		int i = 0;
		BigDecimal TotalAmount = new BigDecimal("0");

		int jj = 1;
		List<Object> ojjList = RestCaller.getSalesTransHdrandItem(parentID);

		for (int k = 0; k < ojjList.size(); k++) {
			ArrayList objAray = (ArrayList) ojjList.get(k);

			String itemCode = (String) objAray.get(0);
			String hsn_code = (String) objAray.get(1);

			if (null == hsn_code) {
				hsn_code = "";
			}
			BigDecimal TaxRate = new BigDecimal((Double) objAray.get(2));

			TaxRate = TaxRate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal Qty = new BigDecimal((Double) objAray.get(3));
			Qty = Qty.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal Rate = new BigDecimal((Double) objAray.get(4));
			Rate = Rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			String itemName = (String) objAray.get(5);
			BigDecimal Amount = new BigDecimal((Double) objAray.get(6));
			// String batch_code = rs.getString("batch_code");

			// String expiry_date = rs.getString("expiry_date");
			// String item_code = rs.getString("item_code");

			BigDecimal AmountIncludingTax = (Qty.multiply(Rate))
					.add(Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
			AmountIncludingTax = AmountIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal rateIncludingTax = Rate.add(Rate.multiply(TaxRate.divide(new BigDecimal("100"))));
			rateIncludingTax = rateIncludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			BigDecimal TaxAmount = (Qty.multiply(Rate.multiply(TaxRate.divide(new BigDecimal("100")))));
			TaxAmount = TaxAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			TotalTax = TotalTax.add(TaxAmount);

			Amount = Amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			TotalAmount = TotalAmount.add(Amount);
			;

			BigDecimal AmountExcludingTax = Qty.multiply(Rate);
			AmountExcludingTax = AmountExcludingTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			int Mop = 0;
			String hexMOP = "";

			int j = itemName.length();
			String itemNameLeft = "";
			int lineLength = 20;
			int sublineCount = 0;
			int ii = 0;
			String strToPrint = "";

			BigDecimal sgst_rate = TaxRate.divide(new BigDecimal("2"));

			BigDecimal cgst_rate = TaxRate.divide(new BigDecimal("2"));

			BigDecimal bdSgst_rate = sgst_rate;
			BigDecimal bdCgst_rate = cgst_rate;

			BigDecimal bdIgst_rate = cgst_rate.add(sgst_rate);

			bdSgst_rate = bdSgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			bdCgst_rate = bdCgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			bdIgst_rate = bdIgst_rate.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			// JTable exampleJTable = new JTable(data, columnNames);

			if (i == 0) {
				Object[][] insertRowData = { { jj, itemName, Rate, Qty, AmountExcludingTax } };
				model = new DefaultTableModel(insertRowData, title);
				i = i + 1;

				// Object[][] rowData2=new Object[][] { { "", hsn_code, "@"+TaxRate+"%", "",""
				// },new Object[] { "itemid", "desc ", "rate", "Amt" }};
				// model.addRow(rowData2);

				Object[] insertRowDataHsn = { "", "HSN: " + hsn_code, "@" + TaxRate + "%", "", "" };
				model.insertRow(i, insertRowDataHsn);
				i = i + 1;

			} else {

				Object[] insertRowData2 = { jj, itemName, Rate, Qty, AmountExcludingTax };
				model.insertRow(i, insertRowData2);
				i = i + 1;
				Object[] insertRowDataHsn = { "", "HSN: " + hsn_code, "@" + TaxRate + "%", "", "" };
				model.insertRow(i, insertRowDataHsn);
				i = i + 1;
			}
			// model = new DefaultTableModel(insertRowData,title);

			// model.addRow(new Object[]{ i, itemName, AmountIncludingTax, Qty,Amount});

			// Object[][] rowData=new Object[][] { { i, itemName, AmountIncludingTax,
			// Qty,Amount },new Object[] { "itemid", "desc ", "rate", "Amt" }};
			// model.addRow(rowData);
			// Object[][] rowData2=new Object[][] { { "", hsn_code, "@"+TaxRate+"%", "",""
			// },new Object[] { "itemid", "desc ", "rate", "Amt" }};
			// model.addRow(rowData2);
			jj = jj + 1;
		}
		// model.removeRow(1);
		BigDecimal dbsgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);
		BigDecimal dbcgstAmount = TotalTax.divide(new BigDecimal("2"), BigDecimal.ROUND_HALF_EVEN);

		BigDecimal bdSgstAmount = dbsgstAmount;
		BigDecimal bdCgstAmount = dbcgstAmount;
		bdSgstAmount = bdSgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		bdCgstAmount = bdCgstAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);

		JTable table = new JTable(model);

		Object printitem[][] = ps.getTableData(table);
		ps.setItems(printitem);

		PrinterJob pj = PrinterJob.getPrinterJob();
		pj.setPrintable(new MyPrintable(), ps.getPageFormat(pj));
		try {
			pj.print();
			SalesTransHdr salesTransHdrresp = RestCaller.getSalesTransHdr(parentID);
			salesTransHdrresp.setPerformaInvoicePrinted("Y");
			RestCaller.updateSalesTranshdrPerformaInvoicePrint(salesTransHdrresp);

		} catch (PrinterException ex) {
			ex.printStackTrace();
		}

	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	@Override
	public void setupBottomline(String bottomLine) throws IOException {
		invoiceBottomLine = bottomLine;

	}

	
	@Override
	public void setupBottomline2(String bottomLine) throws IOException {
		invoiceBottomLine2 =bottomLine;
		
	}
	
	@Override
	public void setupBottomline3(String bottomLine) throws IOException {
		invoiceBottomLine3 =bottomLine;
		
	}
}
/*
 * ################# THIS IS HOW TO USE THIS CLASS #######################
 * 
 * Printsupport ps=new Printsupport(); Object printitem
 * [][]=ps.getTableData(jTable); ps.setItems(printitem);
 * 
 * PrinterJob pj = PrinterJob.getPrinterJob(); pj.setPrintable(new
 * MyPrintable(),ps.getPageFormat(pj)); try { pj.print();
 * 
 * } catch (PrinterException ex) { ex.printStackTrace(); } ##################
 * JOIN TO SHARE KNOWLADGE ###########################
 * 
 */