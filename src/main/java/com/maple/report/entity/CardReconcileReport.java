package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CardReconcileReport {
	
	Date voucherDate;
	String voucherNumber;
	String receiptMode;
	Double receiptAmount;
	
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty receiptModeProperty;
	
	@JsonIgnore
	private DoubleProperty receiptAmountProperty;
	
	
	
	public CardReconcileReport() {
		
		this.voucherDateProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.receiptModeProperty =new SimpleStringProperty();
		this.receiptAmountProperty = new SimpleDoubleProperty();
	}
	
	
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate));
		return voucherDateProperty;
	}
	

	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	

	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public StringProperty getreceiptModeProperty() {
		receiptModeProperty.set(receiptMode);
		return receiptModeProperty;
	}
	

	public void setreceiptModeProperty(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	
	
	public DoubleProperty getreceiptAmountProperty() {
		receiptAmountProperty.set(receiptAmount);
		return receiptAmountProperty;
	}
	

	public void setreceiptAmountProperty(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public Double getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	@Override
	public String toString() {
		return "CardReconcileReport [voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber + ", receiptMode="
				+ receiptMode + ", receiptAmount=" + receiptAmount + "]";
	}
	
	
	

}
