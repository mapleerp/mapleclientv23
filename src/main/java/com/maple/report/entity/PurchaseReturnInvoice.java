package com.maple.report.entity;

public class PurchaseReturnInvoice {
	
	private String supplierName;
	private String supplierAdd;
	private String supplierState;
	private String supplierEmail;
	private String supplierTin;
	private String returnVoucherNo;
	private String voucherDate;
	private String supplierInv;
	private String invoiceDate;
	private String itemcode;
	private String description;
	private String batch;
	private String expDate;
	private Double qty;
	private String unit;
	private Double gst;
	
	
	private Double unitPrice;
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierAdd() {
		return supplierAdd;
	}
	public void setSupplierAdd(String supplierAdd) {
		this.supplierAdd = supplierAdd;
	}
	public String getSupplierState() {
		return supplierState;
	}
	public void setSupplierState(String supplierState) {
		this.supplierState = supplierState;
	}
	public String getSupplierEmail() {
		return supplierEmail;
	}
	public void setSupplierEmail(String supplierEmail) {
		this.supplierEmail = supplierEmail;
	}
	public String getSupplierTin() {
		return supplierTin;
	}
	public void setSupplierTin(String supplierTin) {
		this.supplierTin = supplierTin;
	}
	public String getReturnVoucherNo() {
		return returnVoucherNo;
	}
	public void setReturnVoucherNo(String returnVoucherNo) {
		this.returnVoucherNo = returnVoucherNo;
	}
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getSupplierInv() {
		return supplierInv;
	}
	public void setSupplierInv(String supplierInv) {
		this.supplierInv = supplierInv;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getItemcode() {
		return itemcode;
	}
	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	
	
	public Double getGst() {
		return gst;
	}
	public void setGst(Double gst) {
		this.gst = gst;
	}
	
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	@Override
	public String toString() {
		return "PurchaseReturnInvoice [supplierName=" + supplierName + ", supplierAdd=" + supplierAdd
				+ ", supplierState=" + supplierState + ", supplierEmail=" + supplierEmail + ", supplierTin="
				+ supplierTin + ", returnVoucherNo=" + returnVoucherNo + ", voucherDate=" + voucherDate
				+ ", supplierInv=" + supplierInv + ", invoiceDate=" + invoiceDate + ", itemcode=" + itemcode
				+ ", description=" + description + ", batch=" + batch + ", expDate=" + expDate + ", qty=" + qty
				+ ", unit=" + unit + ", gst=" + gst + ", unitPrice=" + unitPrice + "]";
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	

}
