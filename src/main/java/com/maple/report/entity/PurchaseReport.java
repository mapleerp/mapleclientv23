package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class PurchaseReport {

	String supplierName;
	String supplierInvNo;
	String voucherNumber;
	Date voucherDate;
	String itemName;
	Double qty;
	Double purchseRate;
	Double taxRate;
	String unit;
	Double amount;
	Double amountTotal;
	String supplierGst;
	Double taxAmount;
	Double additionalExpense;
	Double importDuty;
	
	String itemCode;
	String batch;
	Date expiry;
	Double unitPrice;
	
	String supplierEmail;
	String incoiceDate;
	Double netCost;
	String supplierAddress;
	String supplierState;
	
	Double discount;

	
	
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getAdditionalExpense() {
		return additionalExpense;
	}
	public void setAdditionalExpense(Double additionalExpense) {
		this.additionalExpense = additionalExpense;
	}
	public Double getImportDuty() {
		return importDuty;
	}
	public void setImportDuty(Double importDuty) {
		this.importDuty = importDuty;
	}
	public String getSupplierAddress() {
		return supplierAddress;
	}
	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}
	public String getSupplierEmail() {
		return supplierEmail;
	}
	public void setSupplierEmail(String supplierEmail) {
		this.supplierEmail = supplierEmail;
	}
	public String getIncoiceDate() {
		return incoiceDate;
	}
	public void setIncoiceDate(String incoiceDate) {
		this.incoiceDate = incoiceDate;
	}
	public Double getNetCost() {
		return netCost;
	}
	public void setNetCost(Double netCost) {
		this.netCost = netCost;
	}
	
	
	
	StringProperty supNameProperty;
	StringProperty voucherDateProperty;
	StringProperty itemNameProperty;
	StringProperty supInvProperty;
	DoubleProperty qtyProperty;
	DoubleProperty amountProperty;
	DoubleProperty purchaseRateProperty;
	StringProperty voucherNumberProperty;
	public PurchaseReport() {
		

		this.supNameProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();
		this.itemNameProperty =new SimpleStringProperty();
		this.supInvProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.purchaseRateProperty = new SimpleDoubleProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
	}
	public void setsupNameProperty(String supplierName) {
		this.supplierName = supplierName;
	}
	
	public StringProperty getsupInvProperty() {
		supInvProperty.set(supplierInvNo);
		return supInvProperty;
	}
	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.SqlDateTostring(voucherDate));
		return voucherDateProperty;
	}
	
	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	
	
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	
	public DoubleProperty getpurchaseRateProperty() {
		purchaseRateProperty.set(purchseRate);
		return purchaseRateProperty;
	}
	public void setpurchaseRateProperty(Double purchseRate) {
		this.purchseRate = purchseRate;
	}
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	
	public DoubleProperty getamountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setamountProperty(Double amount) {
		this.amount = amount;
	}
	
	public void setsupInvProperty(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}
	
	public StringProperty getsupNameProperty() {
		supNameProperty.set(supplierName);
		return supNameProperty;
	}
	
	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getSupplierGst() {
		return supplierGst;
	}
	public void setSupplierGst(String supplierGst) {
		this.supplierGst = supplierGst;
	}
	Double taxSplit;
	public Double getTaxSplit() {
		return taxSplit;
	}
	public void setTaxSplit(Double taxSplit) {
		this.taxSplit = taxSplit;
	}
	
	public String getSupplierInvNo() {
		return supplierInvNo;
	}
	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getPurchseRate() {
		return purchseRate;
	}
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate = purchseRate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getAmountTotal() {
		return amountTotal;
	}
	public void setAmountTotal(Double amountTotal) {
		this.amountTotal = amountTotal;
	}
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiry() {
		return expiry;
	}
	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	

	public String getSupplierState() {
		return supplierState;
	}
	public void setSupplierState(String supplierState) {
		this.supplierState = supplierState;
	}
	@Override
	public String toString() {
		return "PurchaseReport [supplierName=" + supplierName + ", supplierInvNo=" + supplierInvNo + ", voucherNumber="
				+ voucherNumber + ", voucherDate=" + voucherDate + ", itemName=" + itemName + ", qty=" + qty
				+ ", purchseRate=" + purchseRate + ", taxRate=" + taxRate + ", unit=" + unit + ", amount=" + amount
				+ ", amountTotal=" + amountTotal + ", supplierGst=" + supplierGst + ", taxAmount=" + taxAmount
				+ ", additionalExpense=" + additionalExpense + ", importDuty=" + importDuty + ", itemCode=" + itemCode
				+ ", batch=" + batch + ", expiry=" + expiry + ", unitPrice=" + unitPrice + ", supplierEmail="
				+ supplierEmail + ", incoiceDate=" + incoiceDate + ", netCost=" + netCost + ", supplierAddress="
				+ supplierAddress + ", supplierState=" + supplierState + ", discount=" + discount + ", supNameProperty="
				+ supNameProperty + ", voucherDateProperty=" + voucherDateProperty + ", itemNameProperty="
				+ itemNameProperty + ", supInvProperty=" + supInvProperty + ", qtyProperty=" + qtyProperty
				+ ", amountProperty=" + amountProperty + ", purchaseRateProperty=" + purchaseRateProperty
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", taxSplit=" + taxSplit + "]";
	}
	
	
	
	
	
}
