package com.maple.report.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

 

public class StockTransferOutReport implements Serializable{

	
	 
    private String id;
    String intentNumber;
    Integer slNo;
	Date voucherDate;

	String companyName;
	String hsnCode;
	String gstRate;
	String bankName;
	String accountNumber;
	String bankBranch;
	String itemName;
	String disc;
	String voucherNumber;
	String voucherType;
	String toBranch;
	String fromBranch;
	 String toBranchPlace;

	
 String batch;
 Double qty;
	 Double rate;
	 String itemCode;
	Double taxRate;
	 Date expiryDate;
	 Double amount;
	 Double unit;
	 String unitId;
	 String itemId;
	Double mrp;
	 String barcode;

	    String branchName;
		String branchAddress1;
		String branchAddress2;
		String branchPlace;
		String branchTelNo;
		String branchGst;
		String branchState;
		String branchEmail;
		String companyAddress;
		String companyplace;
		String companyPhoneNumber;
		String unitName;
		String categoryName;
		
		@JsonIgnore
		private StringProperty voucherDateProperty;
		
		@JsonIgnore
		StringProperty voucherNumberProperty;
		
		@JsonIgnore
		StringProperty toBranchProperty;
		
		@JsonIgnore
		StringProperty itemNameproperty;
		
		@JsonIgnore
		StringProperty categoryNameProperty;
		
		@JsonIgnore
		StringProperty batchProperty;
		
		@JsonIgnore
		StringProperty itemCodeProperty;
		
		@JsonIgnore
		StringProperty expiryDateProperty;
		
		@JsonIgnore
		DoubleProperty qtyProperty;
		
		@JsonIgnore
		StringProperty unitNameProperty;
		
		@JsonIgnore
		DoubleProperty rateProperty;
		
		@JsonIgnore
		DoubleProperty amountProperty;
		
	public StockTransferOutReport() {
			this.voucherDateProperty = new SimpleStringProperty();
			this.voucherNumberProperty =  new SimpleStringProperty();
			this.toBranchProperty =  new SimpleStringProperty();
			this.itemNameproperty =  new SimpleStringProperty();
			this.categoryNameProperty =  new SimpleStringProperty();
			this.batchProperty = new SimpleStringProperty();
			this.itemCodeProperty =  new SimpleStringProperty();
			this.expiryDateProperty =  new SimpleStringProperty();
			this.qtyProperty = new SimpleDoubleProperty();
			this.unitNameProperty = new SimpleStringProperty();
			this.rateProperty =  new SimpleDoubleProperty();
			this.amountProperty =  new SimpleDoubleProperty();
		}
	
	@JsonIgnore
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		return voucherDateProperty;
	}
	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	

	@JsonIgnore
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	@JsonIgnore
	public StringProperty gettoBranchProperty() {
		toBranchProperty.set(toBranch);
		return toBranchProperty;
	}
	public void settoBranchProperty(String toBranch) {
		this.toBranch = toBranch;
	}
	
	@JsonIgnore
	public StringProperty getitemNameproperty() {
		itemNameproperty.set(itemName);
		return itemNameproperty;
	}
	public void setitemNameproperty(String itemName) {
		this.itemName = itemName;
	}
	
	@JsonIgnore
	public StringProperty getcategoryNameProperty() {
		categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}
	public void setcategoryNameProperty(String categoryName) {
		this.categoryName = categoryName;
	}
	
	@JsonIgnore
	public StringProperty getexpiryDateProperty() {
		if(null != expiryDate)
		{
		expiryDateProperty.set(SystemSetting.UtilDateToString(expiryDate, "yyyy-MM-dd"));
		}
		return expiryDateProperty;
	}
	public void setexpiryDateProperty(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	@JsonIgnore
	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}
	public void setbatchProperty(String batch) {
		this.batch = batch;
	}
	@JsonIgnore
	public StringProperty getitemCodeProperty() {
		itemCodeProperty.set(itemCode);
		return itemCodeProperty;
	}
	public void setitemCodeProperty(String itemCode) {
		this.itemCode = itemCode;
	}
	
	@JsonIgnore
	public StringProperty getunitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setunitNameProperty(String unitName) {
		this.unitName = unitName;
	}
	
	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	
	@JsonIgnore
	public DoubleProperty getrateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}
	public void setrateProperty(Double rate) {
		this.rate = rate;
	}
	
	@JsonIgnore
	public DoubleProperty getamountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setamountProperty(Double amount) {
		this.amount = amount;
	}
	public String getUnitName() {
			return unitName;
		}
		public void setUnitName(String unitName) {
			this.unitName = unitName;
		}
		public String getCategoryName() {
			return categoryName;
		}
		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	
	public String getIntentNumber() {
		return intentNumber;
	}
	public void setIntentNumber(String intentNumber) {
		this.intentNumber = intentNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getToBranch() {
		return toBranch;
	}
	public void setToBranch(String toBranch) {
		this.toBranch = toBranch;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	

	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public String getGstRate() {
		return gstRate;
	}
	public void setGstRate(String gstRate) {
		this.gstRate = gstRate;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBankBranch() {
		return bankBranch;
	}
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getDisc() {
		return disc;
	}
	public void setDisc(String disc) {
		this.disc = disc;
	}
	public Double getUnit() {
		return unit;
	}
	public void setUnit(Double unit) {
		this.unit = unit;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress1() {
		return branchAddress1;
	}
	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}
	public String getBranchAddress2() {
		return branchAddress2;
	}
	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}
	public String getBranchPlace() {
		return branchPlace;
	}
	public void setBranchPlace(String branchPlace) {
		this.branchPlace = branchPlace;
	}
	public String getBranchTelNo() {
		return branchTelNo;
	}
	public void setBranchTelNo(String branchTelNo) {
		this.branchTelNo = branchTelNo;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	public String getBranchState() {
		return branchState;
	}
	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getCompanyplace() {
		return companyplace;
	}
	public void setCompanyplace(String companyplace) {
		this.companyplace = companyplace;
	}
	public String getCompanyPhoneNumber() {
		return companyPhoneNumber;
	}
	public void setCompanyPhoneNumber(String companyPhoneNumber) {
		this.companyPhoneNumber = companyPhoneNumber;
	}
	

	
	public Integer getSlNo() {
		return slNo;
	}
	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}
	@Override
	public String toString() {
		return "StockTransferOutReport [id=" + id + ", intentNumber=" + intentNumber + ", slNo=" + slNo
				+ ", voucherDate=" + voucherDate + ", companyName=" + companyName + ", hsnCode=" + hsnCode
				+ ", gstRate=" + gstRate + ", bankName=" + bankName + ", accountNumber=" + accountNumber
				+ ", bankBranch=" + bankBranch + ", itemName=" + itemName + ", disc=" + disc + ", voucherNumber="
				+ voucherNumber + ", voucherType=" + voucherType + ", toBranch=" + toBranch + ", fromBranch="
				+ fromBranch + ", batch=" + batch + ", qty=" + qty + ", rate=" + rate + ", itemCode=" + itemCode
				+ ", taxRate=" + taxRate + ", expiryDate=" + expiryDate + ", amount=" + amount + ", unit=" + unit
				+ ", unitId=" + unitId + ", itemId=" + itemId + ", mrp=" + mrp + ", barcode=" + barcode
				+ ", branchName=" + branchName + ", branchAddress1=" + branchAddress1 + ", branchAddress2="
				+ branchAddress2 + ", branchPlace=" + branchPlace + ", branchTelNo=" + branchTelNo + ", branchGst="
				+ branchGst + ", branchState=" + branchState + ", branchEmail=" + branchEmail + ", companyAddress="
				+ companyAddress + ", companyplace=" + companyplace + ", companyPhoneNumber=" + companyPhoneNumber
				+ "]";
	}
	public String getToBranchPlace() {
		return toBranchPlace;
	}
	public void setToBranchPlace(String toBranchPlace) {
		this.toBranchPlace = toBranchPlace;
	}
	
	
}
