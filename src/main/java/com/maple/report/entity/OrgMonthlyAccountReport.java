package com.maple.report.entity;

import java.util.Date;

public class OrgMonthlyAccountReport {
	String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	String companyName;
	String orgName;
	String memberId;
	String familyName;
	String memberName;
	String account;
	Double amount;
	Date startDate;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "OrgMonthlyAccountReport [id=" + id + ", companyName=" + companyName + ", orgName=" + orgName
				+ ", memberId=" + memberId + ", familyName=" + familyName + ", memberName=" + memberName + ", account="
				+ account + ", amount=" + amount + ", startDate=" + startDate + "]";
	}
	
	
	
	
}
