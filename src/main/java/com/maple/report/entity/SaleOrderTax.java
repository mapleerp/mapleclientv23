package com.maple.report.entity;

import org.springframework.stereotype.Component;

public class SaleOrderTax {
	
	Double sgst;
	Double cgst;
	Double total;
	
	public Double getSgst() {
		return sgst;
	}
	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}
	public Double getCgst() {
		return cgst;
	}
	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}

	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "SaleOrderTax [sgst=" + sgst + ", cgst=" + cgst + ", total=" + total + "]";
	}

}
