package com.maple.report.entity;

 

import com.fasterxml.jackson.annotation.JsonProperty;

public class UnitMstReport {
	 private String id;
	 
	    String unitName;
		
	    String unitDescription;

		double unitPrecision;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getUnitName() {
			return unitName;
		}

		public void setUnitName(String unitName) {
			this.unitName = unitName;
		}

		public String getUnitDescription() {
			return unitDescription;
		}

		public void setUnitDescription(String unitDescription) {
			this.unitDescription = unitDescription;
		}

		public double getUnitPrecision() {
			return unitPrecision;
		}

		public void setUnitPrecision(double unitPrecision) {
			this.unitPrecision = unitPrecision;
		}

		@Override
		public String toString() {
			return "UnitMstReport [id=" + id + ", unitName=" + unitName + ", unitDescription=" + unitDescription
					+ ", unitPrecision=" + unitPrecision + "]";
		}

		
		 
	 
		
		 
}
