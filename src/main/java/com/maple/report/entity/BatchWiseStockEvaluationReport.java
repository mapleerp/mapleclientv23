package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BatchWiseStockEvaluationReport {
	String companyName;
	String date;
	Integer slNo;
	String itemName;
	Double qty;
	String branchName;
	String branchAddress;
	String branchPlace;
	String branchPhone;
	String branchGst;
	String branchState;
	Double Price;
	String batch;
	String unitName;
	
	

	public BatchWiseStockEvaluationReport() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.batchProperty =new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty(0.0);
		this.unitProperty = new SimpleStringProperty("");
		this.priceProperty = new SimpleDoubleProperty(0.0);
	}

	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;

	@JsonIgnore
	private StringProperty unitProperty;

	@JsonIgnore
	private StringProperty batchProperty;

	@JsonIgnore
	private DoubleProperty priceProperty;



	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public String getBranchPlace() {
		return branchPlace;
	}

	public void setBranchPlace(String branchPlace) {
		this.branchPlace = branchPlace;
	}

	public String getBranchPhone() {
		return branchPhone;
	}

	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}

	public String getBranchGst() {
		return branchGst;
	}

	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}

	public String getBranchState() {
		return branchState;
	}

	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}

	public Double getPrice() {
		return Price;
	}

	public void setPrice(Double price) {
		Price = price;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public StringProperty getUnitProperty() {
		unitProperty.set(unitName);
		return unitProperty;
	}

	public void setUnitProperty(StringProperty unitProperty) {
		this.unitProperty = unitProperty;
	}

	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public DoubleProperty getPriceProperty() {
		priceProperty.set(Price);
		return priceProperty;
	}

	public void setPriceProperty(DoubleProperty priceProperty) {
		this.priceProperty = priceProperty;
	}

	@Override
	public String toString() {
		return "BranchWiseStockEvaluationReport [companyName=" + companyName + ", date=" + date + ", slNo=" + slNo
				+ ", itemName=" + itemName + ", qty=" + qty + ", branchName=" + branchName + ", branchAddress="
				+ branchAddress + ", branchPlace=" + branchPlace + ", branchPhone=" + branchPhone + ", branchGst="
				+ branchGst + ", branchState=" + branchState + ", Price=" + Price + ", batch=" + batch + ", unitName="
				+ unitName + ", itemNameProperty=" + itemNameProperty + ", qtyProperty=" + qtyProperty
				+ ", unitProperty=" + unitProperty + ", batchProperty=" + batchProperty + ", priceProperty="
				+ priceProperty + "]";
	}
	
	




}
