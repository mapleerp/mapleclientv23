package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JacksonAnnotation;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SaleOrderReceiptReport {

	String voucherNumber;
	Double receiptAmount;
	String customerName;
	
	@JsonIgnore
	StringProperty customerNameProperty;
	@JsonIgnore
	StringProperty voucherNumberProperty;
	
	@JsonIgnore
	DoubleProperty receiptAmountProperty;

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Double getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}


	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public SaleOrderReceiptReport() {
		super();
		this.customerNameProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.receiptAmountProperty = new SimpleDoubleProperty();
	}
	
	public StringProperty getsourceVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	

	public void setcustomerNameProperty(String customerName) {
		this.customerName = customerName;
	}
	
	public StringProperty getcustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}
	

	public void setsourceVoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public DoubleProperty getreceiptAmountProperty() {
		receiptAmountProperty.set(receiptAmount);
		return receiptAmountProperty;
	}
	

	public void setreceiptAmountProperty(Double receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	@Override
	public String toString() {
		return "SaleOrderReceiptReport [voucherNumber=" + voucherNumber + ", receiptAmount=" + receiptAmount
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", receiptAmountProperty="
				+ receiptAmountProperty + "]";
	}
	
}
