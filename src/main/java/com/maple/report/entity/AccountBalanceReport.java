package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AccountBalanceReport {
	
	String branchCode;
	String branchName;
	String branchAddress;
	String branchPhone;
    String voucherNumber;

    String accountClassId;
    String customerAccount;
	String vDate;


	int slNo;
	public int getSlNo() {
		return slNo;
	}
	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}
	String companyName;
	String branchEmail;
	String branchWebsite;
	
	String accountHeads;
	Date startDate;
	Date endDate;
//	Date date;
	String description;
	String remark;
	Double debit;
	Double openingBalance;
	Double closingBalance;
	String openingBalanceType;
	String closingBalanceType;
	
	

	public AccountBalanceReport() {
		this.accountHeadsProperty = new SimpleStringProperty("");
		this.creditProperty = new SimpleDoubleProperty(0.0);
		this.debitProperty = new SimpleDoubleProperty(0.0);
		this.remarkProperty = new SimpleStringProperty("");
		this.date = new SimpleObjectProperty<LocalDate>();
		
		this.closingBalanceProperty = new SimpleDoubleProperty(0.0);
		this.voucherNumberProperty = new SimpleStringProperty();
		this.slnoProperty = new SimpleStringProperty();
	}
	@JsonIgnore
	StringProperty slnoProperty;
	@JsonIgnore
	StringProperty voucherNumberProperty;
	@JsonIgnore
	StringProperty accountHeadsProperty;
	
	@JsonIgnore
	StringProperty remarkProperty;
	
	@JsonIgnore
	DoubleProperty creditProperty;
	
	@JsonIgnore
	DoubleProperty debitProperty;
	
	@JsonIgnore
	DoubleProperty closingBalanceProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> date;
	
	
	@JsonProperty("date")
	public java.sql.Date getDate( ) {
		if(null==this.date.get() ) {
			return null;
		}else {
			return java.sql.Date.valueOf(this.date.get() );
		}
	 
		
	}

	public void setDate (java.sql.Date dateProperty) {
						if(null!=dateProperty)
		this.date.set(dateProperty.toLocalDate());
	}
	public ObjectProperty<LocalDate> getdateProperty( ) {
		return date;
	}

	public void setdateProperty(ObjectProperty<LocalDate> date) {
		this.date = (SimpleObjectProperty<LocalDate>) date;
	}
	
	
	
	
	
	public String getAccountClassId() {
		return accountClassId;
	}
	public void setAccountClassId(String accountClassId) {
		this.accountClassId = accountClassId;
	}
	public String getOpeningBalanceType() {
		return openingBalanceType;
	}
	public void setOpeningBalanceType(String openingBalanceType) {
		this.openingBalanceType = openingBalanceType;
	}
	public String getClosingBalanceType() {
		return closingBalanceType;
	}
	public void setClosingBalanceType(String closingBalanceType) {
		this.closingBalanceType = closingBalanceType;
	}
	public Double getDebit() {
		return debit;
	}
	public void setDebit(Double debit) {
		this.debit = debit;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	Double credit;
	Double balance;
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	
	public String getAccountHeads() {
		return accountHeads;
	}
	public void setAccountHeads(String accountHeads) {
		this.accountHeads = accountHeads;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
//	public Date getDate() {
//		return date;
//	}
//	public void setDate(Date date) {
//		this.date = date;
//	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	
	public Double getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(Double closingBalance) {
		this.closingBalance = closingBalance;
	}
	public Double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}
	
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	
	
	public String getCustomerAccount() {
		return customerAccount;
	}
	public void setCustomerAccount(String customerAccount) {
		this.customerAccount = customerAccount;
	}
	
	
	public StringProperty getAccountHeadsProperty() {
		accountHeadsProperty.set(accountHeads);
		return accountHeadsProperty;
	}
	public void setAccountHeadsProperty(StringProperty accountHeadsProperty) {
		this.accountHeadsProperty = accountHeadsProperty;
	}
	public DoubleProperty getCreditProperty() {
		if(null != credit)
		{
		creditProperty.set(credit);
		}
		return creditProperty;
	}
	public void setCreditProperty(DoubleProperty creditProperty) {
		this.creditProperty = creditProperty;
	}
	public DoubleProperty getDebitProperty() {
		if(null != debit)
		{
		debitProperty.set(debit);
		}
		return debitProperty;
	}
	public void setDebitProperty(DoubleProperty debitProperty) {
		this.debitProperty = debitProperty;
	}
	
	
	public StringProperty getRemarkProperty() {
		remarkProperty.set(remark);
		return remarkProperty;
	}
	public void setRemarkProperty(StringProperty remarkProperty) {
		this.remarkProperty = remarkProperty;
	}
	
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	
	public StringProperty getslnoProperty() {
		slnoProperty.set(slNo+"");
		return slnoProperty;
	}
	public void setslnoProperty(int slNo) {
		this.slNo = slNo;
	}
	
	public DoubleProperty getClosingBalanceProperty() {
		
		closingBalanceProperty.set(closingBalance);
		return closingBalanceProperty;
	}
	public void setClosingBalanceProperty(DoubleProperty closingBalanceProperty) {
		this.closingBalanceProperty = closingBalanceProperty;
	}
	@Override
	public String toString() {
		return "AccountBalanceReport [branchCode=" + branchCode + ", branchName=" + branchName + ", branchAddress="
				+ branchAddress + ", branchPhone=" + branchPhone + ", voucherNumber=" + voucherNumber
				+ ", customerAccount=" + customerAccount + ", slNo=" + slNo + ", companyName=" + companyName
				+ ", branchEmail=" + branchEmail + ", branchWebsite=" + branchWebsite + ", accountHeads=" + accountHeads
				+ ", startDate=" + startDate + ", endDate=" + endDate + ", date=" + date + ", description="
				+ description + ", remark=" + remark + ", debit=" + debit + ", openingBalance=" + openingBalance
				+ ", closingBalance=" + closingBalance + ", openingBalanceType=" + openingBalanceType
				+ ", closingBalanceType=" + closingBalanceType + ", accountHeadsProperty=" + accountHeadsProperty
				+ ", creditProperty=" + creditProperty + ", debitProperty=" + debitProperty + ", credit=" + credit
				+ ", balance=" + balance + "]";
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	
	

}
