package com.maple.report.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ShortExpiryReport {
	String itemName;
	String batch;
	Double qty;
	String categoryName;
	
	String itemCode;
	Double rate;
	Double amount;
	String daysToExpiry;
	
	@JsonIgnore
	private StringProperty itemCodeProperty;
	
	@JsonIgnore
	private StringProperty daysToExpiryProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private StringProperty categoryNameProperty;
	
	@JsonIgnore
	private SimpleObjectProperty<LocalDate> expiryDate;
	
	@JsonIgnore
	private SimpleObjectProperty<LocalDate> updatedDate;
	
	
	public ShortExpiryReport() {
		
		this.itemNameProperty = new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
		this.categoryNameProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.expiryDate = new SimpleObjectProperty<LocalDate>(null);
		this.updatedDate = new SimpleObjectProperty<LocalDate>(null);
		
		this.itemCodeProperty = new SimpleStringProperty();
		this.daysToExpiryProperty = new SimpleStringProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.rateProperty = new SimpleDoubleProperty();
	}
	
	
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	

	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}

	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	

	public String getDaysToExpiry() {
		return daysToExpiry;
	}
	public void setDaysToExpiry(String daysToExpiry) {
		this.daysToExpiry = daysToExpiry;
	}

	
	public StringProperty getItemCodeProperty() {
		if(null != itemCode) {
			itemCodeProperty.set(itemCode);
		}
		return itemCodeProperty;
	}
	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	
	public StringProperty getDaysToExpiryProperty() {
		if(null != daysToExpiry) {
			daysToExpiryProperty.set(daysToExpiry);
		}
		return daysToExpiryProperty;
	}
	public void setDaysToExpiryProperty(StringProperty daysToExpiryProperty) {
		this.daysToExpiryProperty = daysToExpiryProperty;
	}

	
	public DoubleProperty getAmountProperty() {
		if(null != amount) {
			amountProperty.set(amount);
		}
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	
	public DoubleProperty getRateProperty() {
		if(null != rate) {
			rateProperty.set(rate);
		}
		return rateProperty;
	}
	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}



	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	

	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	
	
	public StringProperty getcategoryNameProperty() {
		categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}
	public void setcategoryNameProperty(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}
	public void setbatchProperty(String batch) {
		this.batch = batch;
	}
	
	
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	
	
	@JsonProperty("expiryDate")
	public java.sql.Date getexpiryDate( ) {
		if(null==this.expiryDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.expiryDate.get() );
		}
	 
		
	}
	
   public void setexpiryDate (java.sql.Date expiryDateProperty) {
						if(null!=expiryDateProperty)
		this.expiryDate.set(expiryDateProperty.toLocalDate());
	}
   public ObjectProperty<LocalDate> getexpiryDateProperty( ) {
		return expiryDate;
	}

	public void setexpiryDateProperty(ObjectProperty<LocalDate> expiryDate) {
		this.expiryDate = (SimpleObjectProperty<LocalDate>) expiryDate;
	}
	
	
	
	@JsonProperty("updatedDate")
	public java.sql.Date getupdatedDate( ) {
		if(null==this.updatedDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.updatedDate.get() );
		}
	 
		
	}
	
   public void setupdatedDate(java.sql.Date updatedDateProperty) {
						if(null!=updatedDateProperty)
		this.updatedDate.set(updatedDateProperty.toLocalDate());
	}
   public ObjectProperty<LocalDate> getupdatedDateProperty( ) {
		return updatedDate;
	}

	public void setupdatedDateProperty(ObjectProperty<LocalDate> updatedDate) {
		this.updatedDate = (SimpleObjectProperty<LocalDate>) updatedDate;
	}


	@Override
	public String toString() {
		return "ShortExpiryReport [itemName=" + itemName + ", batch=" + batch + ", qty=" + qty + ", categoryName="
				+ categoryName + ", itemCode=" + itemCode + ", rate=" + rate + ", amount=" + amount + ", daysToExpiry="
				+ daysToExpiry + ", itemCodeProperty=" + itemCodeProperty + ", daysToExpiryProperty="
				+ daysToExpiryProperty + ", amountProperty=" + amountProperty + ", rateProperty=" + rateProperty
				+ ", itemNameProperty=" + itemNameProperty + ", batchProperty=" + batchProperty + ", qtyProperty="
				+ qtyProperty + ", categoryNameProperty=" + categoryNameProperty + ", expiryDate=" + expiryDate
				+ ", updatedDate=" + updatedDate + "]";
	}

}
