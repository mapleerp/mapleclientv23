package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InsuranceCompanySalesSummaryReport {

	Date voucherDate;
	String voucherNumber;
	String customerName;
	String isuranceCompanyName;
	String insuranceCard;
	String policyType;
	Double creditAmount;
	Double cashPaid;
	Double cardPaid;
	Double insuranceAmount;
	Double invoiceAmount;
	String userName;
	String patientName;
	
	
	@JsonIgnore
	private StringProperty patientNameProperty;
	@JsonIgnore
	private StringProperty policyTypeProperty;
	@JsonIgnore
	private DoubleProperty creditAmountProperty;
	@JsonIgnore
	private DoubleProperty cashPaidProperty;
	@JsonIgnore
	private DoubleProperty cardPaidProperty;
	@JsonIgnore
	private DoubleProperty insuranceAmountProperty;
	@JsonIgnore
	private DoubleProperty invoiceAmountProperty;
	@JsonIgnore
	private StringProperty userNameProperty;
	@JsonIgnore
	private StringProperty voucherDateProperty;
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	@JsonIgnore
	private StringProperty customerNameProperty;
	@JsonIgnore
	private StringProperty insuranceCompanyNameProperty;
	@JsonIgnore
	private StringProperty insuranceCardProperty;
	
	
	public InsuranceCompanySalesSummaryReport() {
		this.policyTypeProperty = new SimpleStringProperty();
		this.creditAmountProperty = new SimpleDoubleProperty();
		this.cashPaidProperty =new SimpleDoubleProperty();
		this.cardPaidProperty = new SimpleDoubleProperty();
		this.insuranceAmountProperty = new SimpleDoubleProperty();
		this.invoiceAmountProperty =new SimpleDoubleProperty();
		this.userNameProperty = new SimpleStringProperty();
		this.voucherDateProperty =  new SimpleStringProperty();
		this.voucherNumberProperty =  new SimpleStringProperty();
		this.customerNameProperty =  new SimpleStringProperty();
		this.insuranceCompanyNameProperty =  new SimpleStringProperty();
		this.insuranceCardProperty =  new SimpleStringProperty();
		this.patientNameProperty = new SimpleStringProperty();
	}
	
	
	
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	@JsonIgnore
	public StringProperty getPatientNameProperty() {
		if(null != patientName) {
			patientNameProperty.set(patientName);
		}
		return patientNameProperty;
	}
	public void setPatientNameProperty(StringProperty patientNameProperty) {
		this.patientNameProperty = patientNameProperty;
	}



	public DoubleProperty getcreditAmountProperty() {
		if(null != creditAmount) {
		creditAmountProperty.set(creditAmount);
		}
		return creditAmountProperty;
	}
	public void setcreditAmountProperty(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	
	public DoubleProperty getcashPaidProperty() {
		if(null != cashPaid) {
		cashPaidProperty.set(cashPaid);
		}
		return cashPaidProperty;
	}
	public void setcashPaidProperty(Double cashPaid) {
		this.cashPaid = cashPaid;
	}
	
	public DoubleProperty getinsuranceAmountProperty() {
		if(null != insuranceAmount) {
		insuranceAmountProperty.set(insuranceAmount);
		}
		return insuranceAmountProperty;
	}
	public void setinsuranceAmountProperty(Double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	
	public DoubleProperty getinvoiceAmountProperty() {
		if(null != invoiceAmount) {
		invoiceAmountProperty.set(invoiceAmount);
		}
		return invoiceAmountProperty;
	}
	public void setinvoiceAmountProperty(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	
	public DoubleProperty getcardPaidProperty() {
		if(null != cardPaid) {
		cardPaidProperty.set(cardPaid);
		}
		return cardPaidProperty;
	}
	public void setcardPaidProperty(Double cardPaid) {
		this.cardPaid = cardPaid;
	}
	
	public StringProperty getpolicyTypeProperty() {
		if(null != policyType) {
		policyTypeProperty.set(policyType);
		}
		return policyTypeProperty;
	}
	public void setpolicyTypeProperty(String policyType) {
		this.policyType = policyType;
	}
	
	public StringProperty getuserNameProperty() {
		if(null != userName) {
		userNameProperty.set(userName);
		}
		return userNameProperty;
	}
	public void setuserNameProperty(String userName) {
		this.userName = userName;
	}
	
	public StringProperty getvoucherDateProperty() {
		if(null != voucherDate) {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "dd-MM-yyyy"));
		}
		return voucherDateProperty;
	}
	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	public StringProperty getvoucherNumberProperty() {
		if(null != voucherNumber) {
		voucherNumberProperty.set(voucherNumber);
		}
		return voucherNumberProperty;
	}
	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public StringProperty getcustomerNameProperty() {
		if(null != customerName) {
		customerNameProperty.set(customerName);
		}
		return customerNameProperty;
	}
	public void setcustomerNameProperty(String customerName) {
		this.customerName = customerName;
	}
	
	public StringProperty getinsuranceCompanyNameProperty() {
		if(null != isuranceCompanyName) {
		insuranceCompanyNameProperty.set(isuranceCompanyName);
		}
		return insuranceCompanyNameProperty;
	}
	public void setinsuranceCompanyNameProperty(String isuranceCompanyName) {
		this.isuranceCompanyName = isuranceCompanyName;
	}
	
	public StringProperty getinsuranceCardProperty() {
		if(null != insuranceCard) {
		insuranceCardProperty.set(insuranceCard);
		}
		return insuranceCardProperty;
	}
	public void setinsuranceCardProperty(String insuranceCard) {
		this.insuranceCard = insuranceCard;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getIsuranceCompanyName() {
		return isuranceCompanyName;
	}
	public void setIsuranceCompanyName(String isuranceCompanyName) {
		this.isuranceCompanyName = isuranceCompanyName;
	}
	public String getInsuranceCard() {
		return insuranceCard;
	}
	public void setInsuranceCard(String insuranceCard) {
		this.insuranceCard = insuranceCard;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public Double getCashPaid() {
		return cashPaid;
	}
	public void setCashPaid(Double cashPaid) {
		this.cashPaid = cashPaid;
	}
	public Double getCardPaid() {
		return cardPaid;
	}
	public void setCardPaid(Double cardPaid) {
		this.cardPaid = cardPaid;
	}
	public Double getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(Double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
}
