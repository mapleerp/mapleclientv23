package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyBrachStockMarginReport {
	
	String groupName;
	String itemName;
	String batchCode;
	String itemCode;
	Date expiryDate;
	Double quantity;
	Double rate;
	Double amount;
	Double purchaseRate;
	Double purchaseValue;
    Double marginRate;
    Double marginValue;
    
    
	@JsonIgnore
	private DoubleProperty quantityProperty;
	@JsonIgnore
	private DoubleProperty rateProperty;
	@JsonIgnore
	private DoubleProperty amountProperty;
	@JsonIgnore
	private DoubleProperty purchaseRateProperty;
	@JsonIgnore
	private DoubleProperty purchaseValueProperty;
	@JsonIgnore
	private DoubleProperty marginRateProperty;
	@JsonIgnore
	private DoubleProperty marginValueProperty;
	@JsonIgnore
	private StringProperty groupNameProperty;
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty itemCodeProperty;
	@JsonIgnore
	private StringProperty expiryDateProperty;
	@JsonIgnore
	private StringProperty batchCodeProperty;
	
	
	public PharmacyBrachStockMarginReport()
	{
		this.quantityProperty = new SimpleDoubleProperty();
		this.rateProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.purchaseRateProperty = new SimpleDoubleProperty();
		this.purchaseValueProperty = new SimpleDoubleProperty();
		this.marginRateProperty = new SimpleDoubleProperty();
		this.marginValueProperty = new SimpleDoubleProperty();
		this.groupNameProperty = new SimpleStringProperty();
		this.itemNameProperty = new SimpleStringProperty();
		this.itemCodeProperty = new SimpleStringProperty();
		this.expiryDateProperty = new SimpleStringProperty();
		this.batchCodeProperty = new SimpleStringProperty();
		
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getBatchCode() {
		return batchCode;
	}


	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public Date getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public Double getQuantity() {
		return quantity;
	}


	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public Double getPurchaseRate() {
		return purchaseRate;
	}


	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}


	public Double getPurchaseValue() {
		return purchaseValue;
	}


	public void setPurchaseValue(Double purchaseValue) {
		this.purchaseValue = purchaseValue;
	}


	public Double getMarginRate() {
		return marginRate;
	}


	public void setMarginRate(Double marginRate) {
		this.marginRate = marginRate;
	}


	public Double getMarginValue() {
		return marginValue;
	}


	public void setMarginValue(Double marginValue) {
		this.marginValue = marginValue;
	}


	public DoubleProperty getQuantityProperty() {
		if(null!=quantity) {
			quantityProperty.set(quantity);
		}
		return quantityProperty;
	}


	public void setQuantityProperty(DoubleProperty quantityProperty) {
		this.quantityProperty = quantityProperty;
	}


	public DoubleProperty getRateProperty() {
		if(null!=rate) {
			rateProperty.set(rate);
		}
		return rateProperty;
	}


	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}


	public DoubleProperty getAmountProperty() {
		if(null!=amount) {
			amountProperty.set(amount);
		}
		return amountProperty;
	}


	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}


	public DoubleProperty getPurchaseRateProperty() {
		if(null!=purchaseRate) {
			purchaseRateProperty.set(purchaseRate);
		}
		return purchaseRateProperty;
	}


	public void setPurchaseRateProperty(DoubleProperty purchaseRateProperty) {
		this.purchaseRateProperty = purchaseRateProperty;
	}


	public DoubleProperty getPurchaseValueProperty() {
		if(null!=purchaseValue) {
			purchaseValueProperty.set(purchaseValue);
		}
		return purchaseValueProperty;
	}


	public void setPurchaseValueProperty(DoubleProperty purchaseValueProperty) {
		this.purchaseValueProperty = purchaseValueProperty;
	}


	public DoubleProperty getMarginRateProperty() {
		if(null!=marginRate) {
			marginRateProperty.set(marginRate);
		}
		return marginRateProperty;
	}


	public void setMarginRateProperty(DoubleProperty marginRateProperty) {
		this.marginRateProperty = marginRateProperty;
	}


	public DoubleProperty getMarginValueProperty() {
		if(null!=marginValue) {
			marginValueProperty.set(marginValue);
		}
		return marginValueProperty;
	}


	public void setMarginValueProperty(DoubleProperty marginValueProperty) {
		this.marginValueProperty = marginValueProperty;
	}


	public StringProperty getGroupNameProperty() {
		if(null!=groupName) {
			groupNameProperty.set(groupName);
		}
		return groupNameProperty;
	}


	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}


	public StringProperty getItemNameProperty() {
		if(null!=itemName) {
			itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}


	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}


	public StringProperty getItemCodeProperty() {
		if(null!=itemCode) {
			itemCodeProperty.set(itemCode);
		}
		return itemCodeProperty;
	}


	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}


	public StringProperty getExpiryDateProperty() {
		if(null!=expiryDate) {
			expiryDateProperty.set(SystemSetting.UtilDateToString(expiryDate, "yyyy-MM-dd"));
		}
		return expiryDateProperty;
	}


	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}


	public StringProperty getBatchCodeProperty() {
		if(null!=batchCode) {
			batchCodeProperty.set(batchCode);
		}
		return batchCodeProperty;
	}


	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}


	@Override
	public String toString() {
		return "PharmacyBrachStockMarginReport [groupName=" + groupName + ", itemName=" + itemName + ", batchCode="
				+ batchCode + ", itemCode=" + itemCode + ", expiryDate=" + expiryDate + ", quantity=" + quantity
				+ ", rate=" + rate + ", amount=" + amount + ", purchaseRate=" + purchaseRate + ", purchaseValue="
				+ purchaseValue + ", marginRate=" + marginRate + ", marginValue=" + marginValue + ", quantityProperty="
				+ quantityProperty + ", rateProperty=" + rateProperty + ", amountProperty=" + amountProperty
				+ ", purchaseRateProperty=" + purchaseRateProperty + ", purchaseValueProperty=" + purchaseValueProperty
				+ ", marginRateProperty=" + marginRateProperty + ", marginValueProperty=" + marginValueProperty
				+ ", groupNameProperty=" + groupNameProperty + ", itemNameProperty=" + itemNameProperty
				+ ", itemCodeProperty=" + itemCodeProperty + ", expiryDateProperty=" + expiryDateProperty
				+ ", batchCodeProperty=" + batchCodeProperty + "]";
	}


	
}
