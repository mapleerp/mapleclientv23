package com.maple.report.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ActiveJobCardReport {
	String customer;
	String itemName;
	String serviceInNo;
	String complaints;
	String observation;
	String jobCardHdrId;
	
	
	public ActiveJobCardReport() {
		this.customerNameProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.serviceInNoProperty = new SimpleStringProperty("");
		this.complaintsProperty = new SimpleStringProperty("");
		this.observationProperty = new SimpleStringProperty("");
		this.voucherDate = new SimpleObjectProperty<LocalDate>(null);

	}

	@JsonIgnore
	private StringProperty customerNameProperty;

	@JsonIgnore
	private StringProperty itemNameProperty;

	@JsonIgnore
	private StringProperty serviceInNoProperty;

	@JsonIgnore
	private StringProperty complaintsProperty;

	@JsonIgnore
	private StringProperty observationProperty;

	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;

	@JsonProperty
	public ObjectProperty<LocalDate> getVoucherDateProperty() {
		return voucherDate;
	}

	public void setVoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
		this.voucherDate = voucherDate;
	}

	@JsonProperty("voucherDate")
	public java.sql.Date getVoucherDate() {
		if (null == this.voucherDate.get()) {
			return null;
		} else {
			return Date.valueOf(this.voucherDate.get());
		}
	}

	public void setVoucherDate(java.sql.Date voucherDateProperty) {
		if (null != voucherDateProperty)
			this.voucherDate.set(voucherDateProperty.toLocalDate());
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getServiceInNo() {
		return serviceInNo;
	}

	public void setServiceInNo(String serviceInNo) {
		this.serviceInNo = serviceInNo;
	}

	public String getComplaints() {
		return complaints;
	}

	public void setComplaints(String complaints) {
		this.complaints = complaints;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customer);
		return customerNameProperty;
	}

	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getServiceInNoProperty() {
		serviceInNoProperty.set(serviceInNo);
		return serviceInNoProperty;
	}

	public void setServiceInNoProperty(StringProperty serviceInNoProperty) {
		this.serviceInNoProperty = serviceInNoProperty;
	}

	public StringProperty getComplaintsProperty() {
		complaintsProperty.set(complaints);
		return complaintsProperty;
	}

	public void setComplaintsProperty(StringProperty complaintsProperty) {
		this.complaintsProperty = complaintsProperty;
	}

	public StringProperty getObservationProperty() {
		observationProperty.set(observation);
		return observationProperty;
	}

	public void setObservationProperty(StringProperty observationProperty) {
		this.observationProperty = observationProperty;
	}

	public String getJobCardHdrId() {
		return jobCardHdrId;
	}

	public void setJobCardHdrId(String jobCardHdrId) {
		this.jobCardHdrId = jobCardHdrId;
	}

	@Override
	public String toString() {
		return "ActiveJobCardReport [customer=" + customer + ", itemName=" + itemName + ", serviceInNo=" + serviceInNo
				+ ", complaints=" + complaints + ", observation=" + observation + ", jobCardHdrId=" + jobCardHdrId
				+ ", customerNameProperty=" + customerNameProperty + ", itemNameProperty=" + itemNameProperty
				+ ", serviceInNoProperty=" + serviceInNoProperty + ", complaintsProperty=" + complaintsProperty
				+ ", observationProperty=" + observationProperty + ", voucherDate=" + voucherDate + "]";
	}
	

}
