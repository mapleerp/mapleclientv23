package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyNewCustomerWiseSaleReport {

	
	private Date invoiceDate;
	private String voucherNumber;
	private String itemName;
	private String batchcode;
	private Double quantity;
	private Double amount;
	private Double tax;
	
	
public PharmacyNewCustomerWiseSaleReport() {
		
		this.voucherNumberProperty= new SimpleStringProperty("");
		this.itemNameProperty= new SimpleStringProperty("");
		this.amountProperty=new SimpleDoubleProperty();
		this.taxProperty=new SimpleDoubleProperty();
		this.batchCodeProperty= new SimpleStringProperty("");
		this.qtyProperty= new SimpleDoubleProperty();
	}
	
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
    private	DoubleProperty  amountProperty;
	
	@JsonIgnore
	 private DoubleProperty  taxProperty;
	
	@JsonIgnore
	 private StringProperty batchCodeProperty;

	@JsonIgnore
	 private DoubleProperty qtyProperty;
	
	
	
	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		if(null != voucherNumber) {
			voucherNumberProperty.set(voucherNumber);
		}
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	
	@JsonIgnore
	public StringProperty getItemNameProperty() {
		if(null != itemName) {
			itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		if(null != amount) {
			amountProperty.set(amount);
		}
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getTaxProperty() {
		if(null != tax) {
			taxProperty.set(tax);
		}
		return taxProperty;
	}
	public void setTaxProperty(DoubleProperty taxProperty) {
		this.taxProperty = taxProperty;
	}
	
	@JsonIgnore
	public StringProperty getBatchCodeProperty() {
		if(null != batchcode) {
			batchCodeProperty.set(batchcode);
		}
		return batchCodeProperty;
	}
	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		if(null != quantity) {
			qtyProperty.set(quantity);
		}
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	
	
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBatchcode() {
		return batchcode;
	}
	public void setBatchcode(String batchcode) {
		this.batchcode = batchcode;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTax() {
		return tax;
	}
	public void setTax(Double tax) {
		this.tax = tax;
	}
	@Override
	public String toString() {
		return "AMDCCustomerWiseSaleReport [invoiceDate=" + invoiceDate + ", voucherNumber=" + voucherNumber
				+ ", itemName=" + itemName + ", batchcode=" + batchcode + ", quantity=" + quantity + ", amount="
				+ amount + ", tax=" + tax + "]";
	}
	
	
	
	
}
