package com.maple.report.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyPhysicalStockVarianceReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	String id;
	String stockcorrectionDate;
	String voucherNumber;
	String itemGroup;
	String itemName;
	
	String itemCode;
	String batchCode;
	String userName;
	
	Double systemQty;
	Double physicalQty;
	Double varianceQty;
	Double price;
	Double cost;

	@JsonIgnore
	private StringProperty stockcorrectionDateProperty;
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	@JsonIgnore
	private StringProperty itemGroupProperty;
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty userNameProperty;
	@JsonIgnore
	private StringProperty batchCodeProperty;
	@JsonIgnore
	private DoubleProperty systemQtyProperty;
	@JsonIgnore
	private DoubleProperty physicalQtyProperty;
	@JsonIgnore
	private DoubleProperty varianceQtyProperty;
	@JsonIgnore
	private DoubleProperty priceProperty;
	@JsonIgnore
	private DoubleProperty costProperty;
	
	
public PharmacyPhysicalStockVarianceReport() {
		
		this.stockcorrectionDateProperty = new SimpleStringProperty("");
		this.voucherNumberProperty = new SimpleStringProperty("");
		this.itemGroupProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.userNameProperty = new SimpleStringProperty("");
		this.batchCodeProperty = new SimpleStringProperty("");
		
		this.systemQtyProperty = new SimpleDoubleProperty(0.0);
		this.physicalQtyProperty = new SimpleDoubleProperty(0.0);
		this.varianceQtyProperty = new SimpleDoubleProperty(0.0);
		this.priceProperty = new SimpleDoubleProperty(0.0);
		this.costProperty = new SimpleDoubleProperty(0.0);

	}


public String getId() {
	return id;
}


public void setId(String id) {
	this.id = id;
}


public String getStockcorrectionDate() {
	return stockcorrectionDate;
}


public void setStockcorrectionDate(String stockcorrectionDate) {
	this.stockcorrectionDate = stockcorrectionDate;
}


public String getVoucherNumber() {
	return voucherNumber;
}


public void setVoucherNumber(String voucherNumber) {
	this.voucherNumber = voucherNumber;
}


public String getItemGroup() {
	return itemGroup;
}


public void setItemGroup(String itemGroup) {
	this.itemGroup = itemGroup;
}


public String getItemName() {
	return itemName;
}


public void setItemName(String itemName) {
	this.itemName = itemName;
}


public String getItemCode() {
	return itemCode;
}


public void setItemCode(String itemCode) {
	this.itemCode = itemCode;
}


public String getBatchCode() {
	return batchCode;
}


public void setBatchCode(String batchCode) {
	this.batchCode = batchCode;
}


public String getUserName() {
	return userName;
}


public void setUserName(String userName) {
	this.userName = userName;
}


public Double getSystemQty() {
	return systemQty;
}


public void setSystemQty(Double systemQty) {
	this.systemQty = systemQty;
}


public Double getPhysicalQty() {
	return physicalQty;
}


public void setPhysicalQty(Double physicalQty) {
	this.physicalQty = physicalQty;
}


public Double getVarianceQty() {
	return varianceQty;
}


public void setVarianceQty(Double varianceQty) {
	this.varianceQty = varianceQty;
}


public Double getPrice() {
	return price;
}


public void setPrice(Double price) {
	this.price = price;
}


public Double getCost() {
	return cost;
}


public void setCost(Double cost) {
	this.cost = cost;
}

@JsonIgnore
public StringProperty getStockcorrectionDateProperty() {
	if(null!=stockcorrectionDate) {
		stockcorrectionDateProperty.set(stockcorrectionDate);
	}
	return stockcorrectionDateProperty;
}


public void setStockcorrectionDateProperty(StringProperty stockcorrectionDateProperty) {
	this.stockcorrectionDateProperty = stockcorrectionDateProperty;
}

@JsonIgnore
public StringProperty getVoucherNumberProperty() {
	if(null!=voucherNumber) {
		voucherNumberProperty.set(voucherNumber);
	}
	return voucherNumberProperty;
}


public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
	this.voucherNumberProperty = voucherNumberProperty;
}

@JsonIgnore
public StringProperty getItemGroupProperty() {
	if(null!=itemGroup) {
		itemGroupProperty.set(itemGroup);
	}
	return itemGroupProperty;
}


public void setItemGroupProperty(StringProperty itemGroupProperty) {
	this.itemGroupProperty = itemGroupProperty;
}
@JsonIgnore

public StringProperty getItemNameProperty() {
	if(null!=itemName) {
		itemNameProperty.set(itemName);
	}
	return itemNameProperty;
}


public void setItemNameProperty(StringProperty itemNameProperty) {
	this.itemNameProperty = itemNameProperty;
}

@JsonIgnore
public StringProperty getUserNameProperty() {
	if(null!=userName) {
		userNameProperty.set(userName);
	}
	return userNameProperty;
}


public void setUserNameProperty(StringProperty userNameProperty) {
	this.userNameProperty = userNameProperty;
}

@JsonIgnore
public StringProperty getBatchCodeProperty() {
	if(null!=batchCode) {
		batchCodeProperty.set(batchCode);
	}
	return batchCodeProperty;
}


public void setBatchCodeProperty(StringProperty batchCodeProperty) {
	this.batchCodeProperty = batchCodeProperty;
}

@JsonIgnore
public DoubleProperty getSystemQtyProperty() {
	if(null!=systemQty) {
		systemQtyProperty.set(systemQty);
	}
	return systemQtyProperty;
}


public void setSystemQtyProperty(DoubleProperty systemQtyProperty) {
	this.systemQtyProperty = systemQtyProperty;
}

@JsonIgnore
public DoubleProperty getPhysicalQtyProperty() {
	if(null!=physicalQty) {
		physicalQtyProperty.set(physicalQty);
	}
	return physicalQtyProperty;
}


public void setPhysicalQtyProperty(DoubleProperty physicalQtyProperty) {
	this.physicalQtyProperty = physicalQtyProperty;
}

@JsonIgnore
public DoubleProperty getVarianceQtyProperty() {
	if(null!=varianceQty) {
		varianceQtyProperty.set(varianceQty);
	}
	return varianceQtyProperty;
}


public void setVarianceQtyProperty(DoubleProperty varianceQtyProperty) {
	this.varianceQtyProperty = varianceQtyProperty;
}

@JsonIgnore
public DoubleProperty getPriceProperty() {
	if(null!=price) {
		priceProperty.set(price);
	}
	return priceProperty;
}


public void setPriceProperty(DoubleProperty priceProperty) {
	this.priceProperty = priceProperty;
}

@JsonIgnore
public DoubleProperty getCostProperty() {
	if(null!=cost) {
		costProperty.set(cost);
	}
	return costProperty;
}


public void setCostProperty(DoubleProperty costProperty) {
	this.costProperty = costProperty;
}


@Override
public String toString() {
	return "PharmacyPhysicalStockVarianceReport [id=" + id + ", stockcorrectionDate=" + stockcorrectionDate
			+ ", voucherNumber=" + voucherNumber + ", itemGroup=" + itemGroup + ", itemName=" + itemName + ", itemCode="
			+ itemCode + ", batchCode=" + batchCode + ", userName=" + userName + ", systemQty=" + systemQty
			+ ", physicalQty=" + physicalQty + ", varianceQty=" + varianceQty + ", price=" + price + ", cost=" + cost
			+ ", stockcorrectionDateProperty=" + stockcorrectionDateProperty + ", voucherNumberProperty="
			+ voucherNumberProperty + ", itemGroupProperty=" + itemGroupProperty + ", itemNameProperty="
			+ itemNameProperty + ", userNameProperty=" + userNameProperty + ", batchCodeProperty=" + batchCodeProperty
			+ ", systemQtyProperty=" + systemQtyProperty + ", physicalQtyProperty=" + physicalQtyProperty
			+ ", varianceQtyProperty=" + varianceQtyProperty + ", priceProperty=" + priceProperty + ", costProperty="
			+ costProperty + "]";
}



}
