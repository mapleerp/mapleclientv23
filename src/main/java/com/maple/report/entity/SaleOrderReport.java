package com.maple.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SaleOrderReport {
	
	Date startDate;
	String supplierName;
	String branchCode;
	String branchName;
	String branchAddress;
	String branchPhone;
	String voucherNumber;
	String companyName;
	String branchEmail;
	String branchWebsite;
	String orderMsgToPrint;
	String orderAdvice;
	String state;
    String companyGst;
	int slNo;
	String orderNo;
	String customerName;
	String customerPhone;
	String customerAddress;
	String deliveryType;
	String dueTime;
	String itemName;
	Date dueDate;
	String description;
	String ingrediant;
	Double qty;
	String unit;
	String deliveryBoy;
	String orderBy;
	String voucherNo;
	Double invoiceAmount;
	Double balance;
	Double invoiceToatal;
	Double advanceAmount;
	Double paidAmount;
	
	String paymentMode;
	
	
	//-----------------version 4.1
	
	Date voucherDate;
	String account;
	
	
	
	
	

	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	
	
	public SaleOrderReport() {
		this.amountProperty = new SimpleDoubleProperty(0.0);
		this.voucherNoProperty = new SimpleStringProperty("");
		this.voucherDateProperty = new SimpleStringProperty("");
		this.dueDateProperty = new SimpleStringProperty("");
		this.dueTimeProperty = new SimpleStringProperty("");
		this.custNameProperty = new SimpleStringProperty("");
		this.custAddProperty = new SimpleStringProperty("");
		this.custPhnProperty = new SimpleStringProperty("");
		this.accountProperty = new SimpleStringProperty("");

		//------------version 4.8
				this.unitProperty = new SimpleStringProperty("");
				this.ingerdientProperty = new SimpleStringProperty("");
				this.descriptionProperty = new SimpleStringProperty("");
				this.itemProperty = new SimpleStringProperty("");
				this.qtyProperty = new SimpleDoubleProperty(0.0);

				//------------version 4.8 end
		//--------------version 4.9
				this.typeProperty = new SimpleStringProperty("");
				this.invoiceProperty = new SimpleDoubleProperty(0.0);
				this.advanceProperty = new SimpleDoubleProperty(0.0);
				this.balanceProperty = new SimpleDoubleProperty(0.0);

				//-----------version 4.9 end

		}


	@JsonIgnore
	StringProperty voucherNoProperty;
	
	@JsonIgnore
	StringProperty voucherDateProperty;
	
	@JsonIgnore
	StringProperty dueDateProperty;
	
	@JsonIgnore
	StringProperty dueTimeProperty;
	
	@JsonIgnore
	StringProperty custNameProperty;
	
	@JsonIgnore
	StringProperty custAddProperty;
	
	@JsonIgnore
	StringProperty custPhnProperty;
	
	@JsonIgnore
	StringProperty accountProperty;
	
	@JsonIgnore
	DoubleProperty amountProperty;
	

	//----------------version 4.8
	
	@JsonIgnore
	DoubleProperty qtyProperty;
	
	@JsonIgnore
	StringProperty unitProperty;
	
	@JsonIgnore
	StringProperty ingerdientProperty;
	
	@JsonIgnore
	StringProperty descriptionProperty;
	
	@JsonIgnore
	StringProperty itemProperty;
//---------------version 4.9
	
	@JsonIgnore
	StringProperty typeProperty;
	
	@JsonIgnore
	DoubleProperty invoiceProperty;
	
	@JsonIgnore
	DoubleProperty advanceProperty;
	
	@JsonIgnore
	DoubleProperty balanceProperty;
	
	
	public StringProperty getTypeProperty() {
		typeProperty.set(deliveryType);
		return typeProperty;
	}
	public void setTypeProperty(StringProperty typeProperty) {
		this.typeProperty = typeProperty;
	}
	public DoubleProperty getInvoiceProperty() {
		invoiceProperty.set(invoiceAmount);
		return invoiceProperty;
	}
	public void setInvoiceProperty(DoubleProperty invoiceProperty) {
		this.invoiceProperty = invoiceProperty;
	}
	public DoubleProperty getAdvanceProperty() {
		advanceProperty.set(advanceAmount);
		return advanceProperty;
	}
	public void setAdvanceProperty(DoubleProperty advanceProperty) {
		this.advanceProperty = advanceProperty;
	}
	public DoubleProperty getBalanceProperty() {
		balanceProperty.set(balance);
		return balanceProperty;
	}
	public void setBalanceProperty(DoubleProperty balanceProperty) {
		this.balanceProperty = balanceProperty;
	}
	
	//-----------------version 4.9 end
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	public StringProperty getUnitProperty() {
		unitProperty.set(unit);
		return unitProperty;
	}
	public void setUnitProperty(StringProperty unitProperty) {
		this.unitProperty = unitProperty;
	}
	public StringProperty getIngerdientProperty() {
		ingerdientProperty.set(ingrediant);
		return ingerdientProperty;
	}
	public void setIngerdientProperty(StringProperty ingerdientProperty) {
		this.ingerdientProperty = ingerdientProperty;
	}
	public StringProperty getDescriptionProperty() {
		descriptionProperty.set(description);
		return descriptionProperty;
	}
	public void setDescriptionProperty(StringProperty descriptionProperty) {
		this.descriptionProperty = descriptionProperty;
	}
	public StringProperty getItemProperty() {
		itemProperty.set(itemName);
		return itemProperty;
	}
	public void setItemProperty(StringProperty itemProperty) {
		this.itemProperty = itemProperty;
	}
	
	
	
	
	
	
	
	
	
	
	
		

	
	public StringProperty getAccountProperty() {
		accountProperty.set(account);
		return accountProperty;
	}
	public void setAccountProperty(StringProperty accountProperty) {
		this.accountProperty = accountProperty;
	}
	public StringProperty getVoucherNoProperty() {
		voucherNoProperty.set(voucherNo);

		return voucherNoProperty;
	}
	public void setVoucherNoProperty(StringProperty voucherNoProperty) {
		this.voucherNoProperty = voucherNoProperty;
	}
	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));

		return voucherDateProperty;
	}
	public void setVoucherDateProperty(StringProperty voucherDateProperty) {

		this.voucherDateProperty = voucherDateProperty;
	}
	public StringProperty getDueDateProperty() {
		dueDateProperty.set(SystemSetting.UtilDateToString(dueDate, "yyyy-MM-dd"));

		return dueDateProperty;
	}
	public void setDueDateProperty(StringProperty dueDateProperty) {
		this.dueDateProperty = dueDateProperty;
	}
	public StringProperty getDueTimeProperty() {
		dueTimeProperty.set(dueTime);

		return dueTimeProperty;
	}
	public void setDueTimeProperty(StringProperty dueTimeProperty) {
		this.dueTimeProperty = dueTimeProperty;
	}
	public StringProperty getCustNameProperty() {
		custNameProperty.set(customerName);

		return custNameProperty;
	}
	public void setCustNameProperty(StringProperty custNameProperty) {
		custAddProperty.set(customerAddress);

		this.custNameProperty = custNameProperty;
	}
	public StringProperty getCustAddProperty() {
		custAddProperty.set(customerAddress);
		
		return custAddProperty;
	}
	public void setCustAddProperty(StringProperty custAddProperty) {
		this.custAddProperty = custAddProperty;
	}
	public StringProperty getCustPhnProperty() {
		custPhnProperty.set(customerPhone);

		return custPhnProperty;
	}
	public void setCustPhnProperty(StringProperty custPhnProperty) {
		this.custPhnProperty = custPhnProperty;
	}
	public DoubleProperty getAmountProperty() {
		amountProperty.set(invoiceAmount);

		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	
	
	//-----------------version 4.1 end

	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public int getSlNo() {
		return slNo;
	}
	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getDueTime() {
		return dueTime;
	}
	public void setDueTime(String dueTime) {
		this.dueTime = dueTime;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getIngrediant() {
		return ingrediant;
	}
	public void setIngrediant(String ingrediant) {
		this.ingrediant = ingrediant;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getDeliveryBoy() {
		return deliveryBoy;
	}
	public void setDeliveryBoy(String deliveryBoy) {
		this.deliveryBoy = deliveryBoy;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getInvoiceToatal() {
		return invoiceToatal;
	}
	public void setInvoiceToatal(Double invoiceToatal) {
		this.invoiceToatal = invoiceToatal;
	}
	
	
	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	
	
	
	
	public String getOrderMsgToPrint() {
		return orderMsgToPrint;
	}
	public void setOrderMsgToPrint(String orderMsgToPrint) {
		this.orderMsgToPrint = orderMsgToPrint;
	}
	public String getOrderAdvice() {
		return orderAdvice;
	}
	public void setOrderAdvice(String orderAdvice) {
		this.orderAdvice = orderAdvice;
	}
	
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	
	
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	//--------------version 4.1
	@Override
	public String toString() {
		return "SaleOrderReport [startDate=" + startDate + ", supplierName=" + supplierName + ", branchCode="
				+ branchCode + ", branchName=" + branchName + ", branchAddress=" + branchAddress + ", branchPhone="
				+ branchPhone + ", voucherNumber=" + voucherNumber + ", companyName=" + companyName + ", branchEmail="
				+ branchEmail + ", branchWebsite=" + branchWebsite + ", orderMsgToPrint=" + orderMsgToPrint
				+ ", orderAdvice=" + orderAdvice + ", state=" + state + ", companyGst=" + companyGst + ", slNo=" + slNo
				+ ", orderNo=" + orderNo + ", customerName=" + customerName + ", customerPhone=" + customerPhone
				+ ", customerAddress=" + customerAddress + ", deliveryType=" + deliveryType + ", dueTime=" + dueTime
				+ ", itemName=" + itemName + ", dueDate=" + dueDate + ", description=" + description + ", ingrediant="
				+ ingrediant + ", qty=" + qty + ", unit=" + unit + ", deliveryBoy=" + deliveryBoy + ", orderBy="
				+ orderBy + ", voucherNo=" + voucherNo + ", invoiceAmount=" + invoiceAmount + ", balance=" + balance
				+ ", invoiceToatal=" + invoiceToatal + ", advanceAmount=" + advanceAmount + ", paidAmount=" + paidAmount
				+ ", paymentMode=" + paymentMode + ", voucherDate=" + voucherDate + ", account=" + account
				+ ", voucherNoProperty=" + voucherNoProperty + ", voucherDateProperty=" + voucherDateProperty
				+ ", dueDateProperty=" + dueDateProperty + ", dueTimeProperty=" + dueTimeProperty
				+ ", custNameProperty=" + custNameProperty + ", custAddProperty=" + custAddProperty
				+ ", custPhnProperty=" + custPhnProperty + ", accountProperty=" + accountProperty + ", amountProperty="
				+ amountProperty + "]";
	}
	
	//-----------------version 4.1
	

}
