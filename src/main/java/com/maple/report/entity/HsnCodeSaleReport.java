package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class HsnCodeSaleReport {

	
	Date voucherDate;
	String hsnCode;
	Double qty;
	Double sgst;
	Double cgst;
	Double value;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty hsnCodeProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty sgstProperty;
	
	@JsonIgnore
	private DoubleProperty cgstProperty;
	
	@JsonIgnore
	private DoubleProperty valueProperty;
	
	
	public HsnCodeSaleReport() {
		
		this.voucherDateProperty = new SimpleStringProperty();
		this.hsnCodeProperty =new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.sgstProperty = new SimpleDoubleProperty();
		this.cgstProperty = new SimpleDoubleProperty();
		this.valueProperty =new SimpleDoubleProperty();
	}
	
	
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate));
		return voucherDateProperty;
	}
	

	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public StringProperty gethsnCodeProperty() {
		hsnCodeProperty.set(hsnCode);
		return hsnCodeProperty;
	}
	

	public void sethsnCodeProperty(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	
	public DoubleProperty getsgstProperty() {
		sgstProperty.set(sgst);
		return sgstProperty;
	}
	

	public void setsgstProperty(Double sgst) {
		this.sgst = sgst;
	}
	
	public DoubleProperty getcgstProperty() {
		cgstProperty.set(cgst);
		return cgstProperty;
	}
	

	public void setcgstProperty(Double cgst) {
		this.cgst = cgst;
	}
	
	public DoubleProperty getvalueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}
	

	public void setvalueProperty(Double value) {
		this.value = value;
	}
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getSgst() {
		return sgst;
	}
	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}
	public Double getCgst() {
		return cgst;
	}
	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "HsnCodeSaleReport [voucherDate=" + voucherDate + ", hsnCode=" + hsnCode + ", qty=" + qty + ", sgst="
				+ sgst + ", cgst=" + cgst + ", value=" + value + "]";
	}
	
	
}
