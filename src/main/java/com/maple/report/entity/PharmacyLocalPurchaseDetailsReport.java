package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyLocalPurchaseDetailsReport {
	
	private Date transEntryDate;
	private Date invoiceDate;
	private String voucherNumber;
	private String supplierInvoice;
	private String supplier;
	private String description;
	private String itemCode;
	private String group;
	private String batch;
	private Date expiryDate;
	private String poNumber;
	private Double qty;
	private Double purchaseRate;
	private Double gst;
	private Double amount;
	private Double netCost;
	private String user;
	
	
	@JsonIgnore
	private DoubleProperty netCostProperty;
	@JsonIgnore
	private DoubleProperty amountProperty;
	@JsonIgnore
	private DoubleProperty gstProperty;
	@JsonIgnore
	private DoubleProperty purchaseRateProperty;
	@JsonIgnore
	private DoubleProperty qtyProperty;
	@JsonIgnore
	private StringProperty expiryDateProperty;
	@JsonIgnore
	private StringProperty invoiceDateProperty;
	@JsonIgnore
	private StringProperty transEntryDateProperty;
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	@JsonIgnore
	private StringProperty supplierInvoiceProperty;
	@JsonIgnore
	private StringProperty supplierProperty;
	@JsonIgnore
	private StringProperty descriptionProperty;
	@JsonIgnore
	private StringProperty itemCodeProperty;
	@JsonIgnore
	private StringProperty groupProperty;
	@JsonIgnore
	private StringProperty batchProperty;
	@JsonIgnore
	private StringProperty poNumberProperty;
	@JsonIgnore
	private StringProperty userProperty;
	
	public PharmacyLocalPurchaseDetailsReport()
	{
		this.userProperty = new SimpleStringProperty();
		this.poNumberProperty = new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
		this.groupProperty = new SimpleStringProperty();
		this.itemCodeProperty = new SimpleStringProperty();
		this.descriptionProperty = new SimpleStringProperty();
		this.supplierProperty = new SimpleStringProperty();
		this.supplierInvoiceProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.transEntryDateProperty = new SimpleStringProperty();
		this.invoiceDateProperty = new SimpleStringProperty();
		this.expiryDateProperty = new SimpleStringProperty();
		this.qtyProperty= new SimpleDoubleProperty();
		this.gstProperty= new SimpleDoubleProperty();
		this.amountProperty= new SimpleDoubleProperty();
		this.netCostProperty= new SimpleDoubleProperty();
		this.purchaseRateProperty=new SimpleDoubleProperty();
		
	}
	
	
	
	
	public Date getTransEntryDate() {
		return transEntryDate;
	}
	public void setTransEntryDate(Date transEntryDate) {
		this.transEntryDate = transEntryDate;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getSupplierInvoice() {
		return supplierInvoice;
	}
	public void setSupplierInvoice(String supplierInvoice) {
		this.supplierInvoice = supplierInvoice;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public Double getPurchaseRate() {
		return purchaseRate;
	}
	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}
	public Double getGst() {
		return gst;
	}
	public void setGst(Double gst) {
		this.gst = gst;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getNetCost() {
		return netCost;
	}
	public void setNetCost(Double netCost) {
		this.netCost = netCost;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	


	public DoubleProperty getNetCostProperty() {
		if(null!=netCost) {
			netCostProperty.set(netCost);
		}
		return netCostProperty;
	}




	public void setNetCostProperty(DoubleProperty netCostProperty) {
		this.netCostProperty = netCostProperty;
	}




	public DoubleProperty getAmountProperty() {
		if(null!=amount) {
			amountProperty.set(amount);
		}
		return amountProperty;
	}




	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}




	public DoubleProperty getGstProperty() {
		if(null!=gst) {
			gstProperty.set(gst);
		}
		return gstProperty;
	}




	public void setGstProperty(DoubleProperty gstProperty) {
		this.gstProperty = gstProperty;
	}




	public DoubleProperty getPurchaseRateProperty() {
		if(null!=purchaseRate) {
			purchaseRateProperty.set(purchaseRate);
		}
		return purchaseRateProperty;
	}




	public void setPurchaseRateProperty(DoubleProperty purchaseRateProperty) {
		this.purchaseRateProperty = purchaseRateProperty;
	}




	



	public StringProperty getExpiryDateProperty() {
		if(null!=expiryDate) {
			expiryDateProperty.set(SystemSetting.UtilDateToString(expiryDate, "yyyy-MM-dd"));
		}
		return expiryDateProperty;
	}




	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}




	public StringProperty getInvoiceDateProperty() {
		if(null!=invoiceDate) {
			invoiceDateProperty.set(SystemSetting.UtilDateToString(invoiceDate, "yyyy-MM-dd"));
		}
		return invoiceDateProperty;
	}




	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}




	public StringProperty getTransEntryDateProperty() {
		if(null!=transEntryDate) {
			transEntryDateProperty.set(SystemSetting.UtilDateToString(transEntryDate, "yyyy-MM-dd"));
		}
		return transEntryDateProperty;
	}




	public void setTransEntryDateProperty(StringProperty transEntryDateProperty) {
		this.transEntryDateProperty = transEntryDateProperty;
	}




	public StringProperty getVoucherNumberProperty() {
		if(null!=voucherNumber) {
			voucherNumberProperty.set(voucherNumber);
		}
		return voucherNumberProperty;
	}




	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}




	public StringProperty getSupplierInvoiceProperty() {
		if(null!=supplierInvoice) {
			supplierInvoiceProperty.set(supplierInvoice);
		}
		return supplierInvoiceProperty;
	}




	public void setSupplierInvoiceProperty(StringProperty supplierInvoiceProperty) {
		this.supplierInvoiceProperty = supplierInvoiceProperty;
	}




	public StringProperty getSupplierProperty() {
		if(null!=supplier) {
			supplierProperty.set(supplier);
		}
		return supplierProperty;
	}




	public void setSupplierProperty(StringProperty supplierProperty) {
		this.supplierProperty = supplierProperty;
	}




	public StringProperty getDescriptionProperty() {
		if(null!=description) {
			descriptionProperty.set(description);
		}
		return descriptionProperty;
	}




	public void setDescriptionProperty(StringProperty descriptionProperty) {
		this.descriptionProperty = descriptionProperty;
	}




	public StringProperty getItemCodeProperty() {
		if(null!=itemCode) {
			itemCodeProperty.set(itemCode);
		}
		return itemCodeProperty;
	}




	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}




	public StringProperty getGroupProperty() {
		if(null!=group) {
			groupProperty.set(group);
		}
		return groupProperty;
	}




	public void setGroupProperty(StringProperty groupProperty) {
		this.groupProperty = groupProperty;
	}




	public StringProperty getBatchProperty() {
		if(null!=batch) {
			batchProperty.set(batch);
		}
		return batchProperty;
	}




	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}




	public StringProperty getPoNumberProperty() {
		if(null!=poNumber) {
			poNumberProperty.set(poNumber);
		}
		return poNumberProperty;
	}




	public void setPoNumberProperty(StringProperty poNumberProperty) {
		this.poNumberProperty = poNumberProperty;
	}




	public StringProperty getUserProperty() {
		if(null!=user) {
			userProperty.set(user);
		}
		return userProperty;
	}




	public void setUserProperty(StringProperty userProperty) {
		this.userProperty = userProperty;
	}




	public Double getQty() {
		return qty;
	}




	public void setQty(Double qty) {
		this.qty = qty;
	}




	@Override
	public String toString() {
		return "PharmacyLocalPurchaseDetailsReport [transEntryDate=" + transEntryDate + ", invoiceDate=" + invoiceDate
				+ ", voucherNumber=" + voucherNumber + ", supplierInvoice=" + supplierInvoice + ", supplier=" + supplier
				+ ", description=" + description + ", itemCode=" + itemCode + ", group=" + group + ", batch=" + batch
				+ ", expiryDate=" + expiryDate + ", poNumber=" + poNumber + ", qty=" + qty + ", purchaseRate="
				+ purchaseRate + ", gst=" + gst + ", amount=" + amount + ", netCost=" + netCost + ", user=" + user
				+ ", netCostProperty=" + netCostProperty + ", amountProperty=" + amountProperty + ", gstProperty="
				+ gstProperty + ", purchaseRateProperty=" + purchaseRateProperty + ", qtyProperty=" + qtyProperty
				+ ", expiryDateProperty=" + expiryDateProperty + ", invoiceDateProperty=" + invoiceDateProperty
				+ ", transEntryDateProperty=" + transEntryDateProperty + ", voucherNumberProperty="
				+ voucherNumberProperty + ", supplierInvoiceProperty=" + supplierInvoiceProperty + ", supplierProperty="
				+ supplierProperty + ", descriptionProperty=" + descriptionProperty + ", itemCodeProperty="
				+ itemCodeProperty + ", groupProperty=" + groupProperty + ", batchProperty=" + batchProperty
				+ ", poNumberProperty=" + poNumberProperty + ", userProperty=" + userProperty + "]";
	}




	public DoubleProperty getQtyProperty() {
		if(null!=qty) {
			qtyProperty.set(qty);
		}
		return qtyProperty;
	}




	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}





	
}
