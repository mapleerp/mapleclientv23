package com.maple.report.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ServiceInReport {
	
	 Double qty;
	 String model;
	 String serialId;
	 String complaints;
	 String underWarranty;
	 String observation;
	 String itemName;
	 String brandName;
	 String productName;
	 String customer;
	 
	 
	 
	 public ServiceInReport() {
		this.customerProperty = new SimpleStringProperty("");
		this.serialIdProperty = new SimpleStringProperty("");
		this.complaintsProperty = new SimpleStringProperty("");
		this.observationProperty = new SimpleStringProperty("");
		this.underWarrantyProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.brandNameProperty = new SimpleStringProperty("");
		this.productNameProperty = new SimpleStringProperty("");
		this.modelProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty();
		this.voucherDate = new SimpleObjectProperty<LocalDate>();


	}

	@JsonIgnore
		private StringProperty customerProperty;
	 
	 @JsonIgnore
		private StringProperty serialIdProperty;
	 
	 @JsonIgnore
		private StringProperty complaintsProperty;
	 
	 @JsonIgnore
		private StringProperty observationProperty;
	 
	 @JsonIgnore
		private StringProperty underWarrantyProperty;
	 
	 @JsonIgnore
		private StringProperty itemNameProperty;
	 
	 @JsonIgnore
		private StringProperty brandNameProperty;
	 
	 @JsonIgnore
		private StringProperty productNameProperty;
	 
	 @JsonIgnore
		private StringProperty modelProperty;
		
		@JsonIgnore
		private DoubleProperty qtyProperty;
	 
	 @JsonIgnore
		private SimpleObjectProperty<LocalDate> voucherDate;
	 
		@JsonProperty("voucherDate")
		public java.sql.Date getvoucherDate( ) {
			if(null==this.voucherDate.get() ) {
				return null;
			}else {
				return Date.valueOf(this.voucherDate.get() );
			}	
		}

	   public void setvoucherDate (java.sql.Date voucherDateProperty) {
							if(null!=voucherDateProperty)
			this.voucherDate.set(voucherDateProperty.toLocalDate());
		}
	   public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
			return voucherDate;
		}

		public void setvoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
			this.voucherDate = (SimpleObjectProperty<LocalDate>) voucherDate;
		}
		
		
		
		

		public Double getQty() {
			return qty;
		}

		public void setQty(Double qty) {
			this.qty = qty;
		}

		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}

		public String getSerialId() {
			return serialId;
		}

		public void setSerialId(String serialId) {
			this.serialId = serialId;
		}

		public String getComplaints() {
			return complaints;
		}

		public void setComplaints(String complaints) {
			this.complaints = complaints;
		}

		public String getUnderWarranty() {
			return underWarranty;
		}

		public void setUnderWarranty(String underWarranty) {
			this.underWarranty = underWarranty;
		}

		public String getObservation() {
			return observation;
		}

		public void setObservation(String observation) {
			this.observation = observation;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public String getBrandName() {
			return brandName;
		}

		public void setBrandName(String brandName) {
			this.brandName = brandName;
		}

		public String getProductName() {
			return productName;
		}

		public void setProductName(String productName) {
			this.productName = productName;
		}

		public String getCustomer() {
			return customer;
		}

		public void setCustomer(String customer) {
			this.customer = customer;
		}

		public StringProperty getCustomerProperty() {
			customerProperty.set(customer);
			return customerProperty;
		}

		public void setCustomerProperty(StringProperty customerProperty) {
			this.customerProperty = customerProperty;
		}

		public StringProperty getSerialIdProperty() {
			serialIdProperty.set(serialId);
			return serialIdProperty;
		}

		public void setSerialIdProperty(StringProperty serialIdProperty) {
			this.serialIdProperty = serialIdProperty;
		}

		public StringProperty getComplaintsProperty() {
			complaintsProperty.set(complaints);
			return complaintsProperty;
		}

		public void setComplaintsProperty(StringProperty complaintsProperty) {
			this.complaintsProperty = complaintsProperty;
		}

		public StringProperty getObservationProperty() {
			observationProperty.set(observation);
			return observationProperty;
		}

		public void setObservationProperty(StringProperty observationProperty) {
			this.observationProperty = observationProperty;
		}

		public StringProperty getUnderWarrantyProperty() {
			underWarrantyProperty.set(underWarranty);
			return underWarrantyProperty;
		}

		public void setUnderWarrantyProperty(StringProperty underWarrantyProperty) {
			this.underWarrantyProperty = underWarrantyProperty;
		}

		public StringProperty getItemNameProperty() {
			itemNameProperty.set(itemName);
			return itemNameProperty;
		}

		public void setItemNameProperty(StringProperty itemNameProperty) {
			this.itemNameProperty = itemNameProperty;
		}

		public StringProperty getBrandNameProperty() {
			brandNameProperty.set(brandName);
			return brandNameProperty;
		}

		public void setBrandNameProperty(StringProperty brandNameProperty) {
			this.brandNameProperty = brandNameProperty;
		}

		public StringProperty getProductNameProperty() {
			productNameProperty.set(productName);
			return productNameProperty;
		}

		public void setProductNameProperty(StringProperty productNameProperty) {
			this.productNameProperty = productNameProperty;
		}

		public StringProperty getModelProperty() {
			modelProperty.set(model);
			return modelProperty;
		}

		public void setModelProperty(StringProperty modelProperty) {
			this.modelProperty = modelProperty;
		}

		public DoubleProperty getQtyProperty() {
			qtyProperty.set(qty);
			return qtyProperty;
		}

		public void setQtyProperty(DoubleProperty qtyProperty) {
			this.qtyProperty = qtyProperty;
		}
		
		
		
}
