package com.maple.report.entity;

import java.sql.Date;
import java.time.LocalDate;

import javax.swing.JScrollBar;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DailySalesReturnReportDtl {
	
	String voucherNumber;
	String customerName;
	Double amount;
	String customerType;
	
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;

	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;
	
	
	public DailySalesReturnReportDtl() {
		
		this.voucherNumberProperty = new SimpleStringProperty();
		this.customerNameProperty =new SimpleStringProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.voucherDate = new SimpleObjectProperty<LocalDate>();
	}
@JsonProperty

	public String getVoucherNumber() {
	return voucherNumber;
}
public void setVoucherNumber(String voucherNumber) {
	this.voucherNumber = voucherNumber;
}
public String getCustomerName() {
	return customerName;
}
public void setCustomerName(String customerName) {
	this.customerName = customerName;
}
public Double getAmount() {
	return amount;
}
public void setAmount(Double amount) {
	this.amount = amount;
}
@JsonIgnore
public StringProperty getvoucherNumberProperty() {
	voucherNumberProperty.set(voucherNumber);
	return voucherNumberProperty;
}

public void setvoucherNumberProperty(String voucherNumber) {
	this.voucherNumber = voucherNumber;
}
@JsonIgnore
public StringProperty getcustomerNameProperty() {
	customerNameProperty.set(customerName);
	return customerNameProperty;
}

public void setcustomerNameProperty(String customerName) {
	this.customerName = customerName;
}

@JsonIgnore
public DoubleProperty getamountProperty() {
	amountProperty.set(amount);
	return amountProperty;
}

public String getCustomerType() {
	return customerType;
}
public void setCustomerType(String customerType) {
	this.customerType = customerType;
}
public void setamountProperty(Double amount) {
	this.amount = amount;
}
@JsonProperty("voucherDate")
public java.sql.Date getvoucherDate( ) {
	if(null==this.voucherDate.get() ) {
		return null;
	}else {
		return Date.valueOf(this.voucherDate.get() );
	}
 
	
}

public void setvoucherDate (java.sql.Date voucherDateProperty) {
					if(null!=voucherDateProperty)
	this.voucherDate.set(voucherDateProperty.toLocalDate());
}
public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
	return voucherDate;
}

public void setsourceVoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
	this.voucherDate = (SimpleObjectProperty<LocalDate>) voucherDate;
}

@Override
public String toString() {
	return "DailySalesReportDtl [voucherNumber=" + voucherNumber + ", customerName=" + customerName + ", amount="
			+ amount + ", customerType=" + customerType + ", voucherNumberProperty=" + voucherNumberProperty
			+ ", customerNameProperty=" + customerNameProperty + ", amountProperty=" + amountProperty + "]";
}

	
	

}
