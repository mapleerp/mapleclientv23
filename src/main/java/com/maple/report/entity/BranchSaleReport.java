package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BranchSaleReport {
	
	
	private Date voucherDate;
	private String voucherNumber;
	private String customerName;
	private String toBranchName;
	private Double invoiceTotal;
	
	private String itemName;
	private String unitName;
	private Double qty;
	private Double amount;
	private String batch;
	
	private String branchVoucherNo;

	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private StringProperty toBranchNameProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private DoubleProperty invoiceTotalProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	
	@JsonIgnore
	private StringProperty branchVoucherNoProperty;
	
	public BranchSaleReport() {
		this.voucherNumberProperty =new SimpleStringProperty();
		this.customerNameProperty =new SimpleStringProperty();
		this.toBranchNameProperty =new SimpleStringProperty();
		this.itemNameProperty =new SimpleStringProperty();
		this.unitNameProperty =new SimpleStringProperty();
		this.batchProperty =new SimpleStringProperty();
		this.voucherDateProperty =new SimpleStringProperty();
		this.branchVoucherNoProperty =new SimpleStringProperty();


		this.qtyProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.invoiceTotalProperty = new SimpleDoubleProperty();

		
	}
	
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getToBranchName() {
		return toBranchName;
	}
	public void setToBranchName(String toBranchName) {
		this.toBranchName = toBranchName;
	}
	public Double getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	
	
	
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}
	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}
	public StringProperty getToBranchNameProperty() {
		toBranchNameProperty.set(toBranchName);
		return toBranchNameProperty;
	}
	public void setToBranchNameProperty(StringProperty toBranchNameProperty) {
		this.toBranchNameProperty = toBranchNameProperty;
	}
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}
	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}
	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}
	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}
	public DoubleProperty getInvoiceTotalProperty() {
		invoiceTotalProperty.set(invoiceTotal);
		return invoiceTotalProperty;
	}
	public void setInvoiceTotalProperty(DoubleProperty invoiceTotalProperty) {
		this.invoiceTotalProperty = invoiceTotalProperty;
	}
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	



	public String getBranchVoucherNo() {
		return branchVoucherNo;
	}


	public void setBranchVoucherNo(String branchVoucherNo) {
		this.branchVoucherNo = branchVoucherNo;
	}


	public StringProperty getBranchVoucherNoProperty() {
		branchVoucherNoProperty.set(branchVoucherNo);
		return branchVoucherNoProperty;
	}


	public void setBranchVoucherNoProperty(StringProperty branchVoucherNoProperty) {
		this.branchVoucherNoProperty = branchVoucherNoProperty;
	}


	@Override
	public String toString() {
		return "BranchSaleReport [voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber + ", customerName="
				+ customerName + ", toBranchName=" + toBranchName + ", invoiceTotal=" + invoiceTotal + ", itemName="
				+ itemName + ", unitName=" + unitName + ", qty=" + qty + ", amount=" + amount + ", batch=" + batch
				+ ", branchVoucherNo=" + branchVoucherNo + ", voucherNumberProperty=" + voucherNumberProperty
				+ ", customerNameProperty=" + customerNameProperty + ", toBranchNameProperty=" + toBranchNameProperty
				+ ", itemNameProperty=" + itemNameProperty + ", unitNameProperty=" + unitNameProperty
				+ ", batchProperty=" + batchProperty + ", voucherDateProperty=" + voucherDateProperty
				+ ", invoiceTotalProperty=" + invoiceTotalProperty + ", qtyProperty=" + qtyProperty
				+ ", amountProperty=" + amountProperty + ", branchVoucherNoProperty=" + branchVoucherNoProperty + "]";
	}
	
	

}
