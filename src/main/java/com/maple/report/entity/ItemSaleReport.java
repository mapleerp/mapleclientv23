package com.maple.report.entity;

import org.springframework.stereotype.Component;

public class ItemSaleReport {
	
	String itemName;
	String categoryName;
	String unitName;
	Double totalQty;
	Double availableQty;
	Double SoldOutQty;
	Double rate;
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(Double totalQty) {
		this.totalQty = totalQty;
	}
	public Double getAvailableQty() {
		return availableQty;
	}
	public void setAvailableQty(Double availableQty) {
		this.availableQty = availableQty;
	}
	public Double getSoldOutQty() {
		return SoldOutQty;
	}
	public void setSoldOutQty(Double soldOutQty) {
		SoldOutQty = soldOutQty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		return "ItemSaleReport [itemName=" + itemName + ", categoryName=" + categoryName + ", unitName=" + unitName
				+ ", totalQty=" + totalQty + ", availableQty=" + availableQty + ", SoldOutQty=" + SoldOutQty + ", rate="
				+ rate + "]";
	}
	
	
	
	
	


}
