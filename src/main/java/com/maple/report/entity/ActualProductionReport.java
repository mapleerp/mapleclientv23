package com.maple.report.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ActualProductionReport {
	
	String id;
	String batch;
	Double actualQty;
	String itemName;
	String unitName;
	String voucherNo;
	
	
	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private DoubleProperty actualQtyProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private StringProperty voucherNoProperty;
	
	
	public ActualProductionReport() {
		
		this.voucherDate = new SimpleObjectProperty<LocalDate>(null);
		this.batchProperty = new SimpleStringProperty();
		this.actualQtyProperty =new SimpleDoubleProperty();
		this.itemNameProperty =new SimpleStringProperty();
		this.unitNameProperty = new SimpleStringProperty();
		this.voucherNoProperty = new SimpleStringProperty();
	}

	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getActualQty() {
		return actualQty;
	}
	public void setActualQty(Double actualQty) {
		this.actualQty = actualQty;
	}
	public String getItemName() {
		return itemName;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	@JsonProperty
	public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
		return voucherDate;
	}
 
	public void setvoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
		this.voucherDate = voucherDate;
	}
	@JsonProperty("voucherDate")
	public java.sql.Date getvoucherDate ( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}
	
   public void setvoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}
	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	@JsonIgnore
	public DoubleProperty getactualQtyProperty() {
		actualQtyProperty.set(actualQty);
		return actualQtyProperty;
	}

	public void setactualQtyProperty(Double actualQty) {
		this.actualQty = actualQty;
	}
	@JsonIgnore
	public StringProperty getunitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}

	public void setunitNameProperty(String unitName) {
		this.unitName = unitName;
	}
	@JsonIgnore
	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setbatchProperty(String batch) {
		this.batch = batch;
	}
	@JsonIgnore
	public StringProperty getvoucherNoProperty() {
		voucherNoProperty.set(voucherNo);
		return voucherNoProperty;
	}

	public void setvoucherNoProperty(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	@Override
	public String toString() {
		return "ActualProductionReport [id=" + id + ", batch=" + batch + ", actualQty=" + actualQty + ", itemName="
				+ itemName + ", unitName=" + unitName + ", voucherNo=" + voucherNo + ", voucherDate=" + voucherDate
				+ ", batchProperty=" + batchProperty + ", actualQtyProperty=" + actualQtyProperty
				+ ", itemNameProperty=" + itemNameProperty + ", unitNameProperty=" + unitNameProperty
				+ ", voucherNoProperty=" + voucherNoProperty + "]";
	}

	
	
	
}
