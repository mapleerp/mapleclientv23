package com.maple.report.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class RetailSalesDetailReport {
	
	String invoiceDate;
	String voucherNum;
	String customerName;
	String patientName;
	String patientID;
	String itemName;
	String groupName;
	String expiryDate;
	String batchCode;
	String itemCode;
	Double quantity;
	Double rate;
	Double taxRate;
	Double saleValue;
	Double cost;
	Double costValue;
	
	@JsonIgnore
	private StringProperty invoiceDateProperty;
	@JsonIgnore
	private StringProperty voucherNumProperty;
	@JsonIgnore
	private StringProperty customerNameProperty;
	@JsonIgnore
	private StringProperty patientNameProperty;
	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty groupNameProperty;
	@JsonIgnore
	private StringProperty expiryDateProperty;
	@JsonIgnore
	private StringProperty batchCodeProperty;
	@JsonIgnore
	private DoubleProperty quantityProperty;
	@JsonIgnore
	private DoubleProperty rateProperty;
	@JsonIgnore
	private DoubleProperty taxRateProperty;
	@JsonIgnore
	private DoubleProperty saleValueProperty;
	@JsonIgnore
	private DoubleProperty costProperty;
	@JsonIgnore
	private DoubleProperty costValueProperty;
	
	public RetailSalesDetailReport() {
		
		this.invoiceDateProperty = new SimpleStringProperty("");
		this.voucherNumProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.patientNameProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.groupNameProperty = new SimpleStringProperty("");
		this.expiryDateProperty = new SimpleStringProperty("");
		this.batchCodeProperty = new SimpleStringProperty("");
		
		this.quantityProperty = new SimpleDoubleProperty(0.0);
		this.rateProperty = new SimpleDoubleProperty(0.0);
		this.taxRateProperty = new SimpleDoubleProperty(0.0);
		this.saleValueProperty = new SimpleDoubleProperty(0.0);
		this.costProperty = new SimpleDoubleProperty(0.0);
		this.costValueProperty = new SimpleDoubleProperty(0.0);

	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getVoucherNum() {
		return voucherNum;
	}

	public void setVoucherNum(String voucherNum) {
		this.voucherNum = voucherNum;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientID() {
		return patientID;
	}

	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getBatchCode() {
		return batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getSaleValue() {
		return saleValue;
	}

	public void setSaleValue(Double saleValue) {
		this.saleValue = saleValue;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getCostValue() {
		return costValue;
	}

	public void setCostValue(Double costValue) {
		this.costValue = costValue;
	}

	@JsonIgnore
	public StringProperty getInvoiceDateProperty() {
		if(null != invoiceDate) {
			invoiceDateProperty.set(invoiceDate);
		}
		return invoiceDateProperty;
	}

	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}

	@JsonIgnore
	public StringProperty getVoucherNumProperty() {
		if(null != voucherNum) {
			voucherNumProperty.set(voucherNum);
		}
		return voucherNumProperty;
	}

	public void setVoucherNumProperty(StringProperty voucherNumProperty) {
		this.voucherNumProperty = voucherNumProperty;
	}

	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		if(null != customerName) {
			customerNameProperty.set(customerName);
		}
		return customerNameProperty;
	}

	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}

	@JsonIgnore
	public StringProperty getPatientNameProperty() {
		if(null != patientName) {
			patientNameProperty.set(patientName);
		}
		return patientNameProperty;
	}

	public void setPatientNameProperty(StringProperty patientNameProperty) {
		this.patientNameProperty = patientNameProperty;
	}

	@JsonIgnore
	public StringProperty getItemNameProperty() {
		if(null != itemName) {
			itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	
	@JsonIgnore
	public StringProperty getGroupNameProperty() {
		if(null != groupName) {
			groupNameProperty.set(groupName);
		}
		return groupNameProperty;
	}

	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}

	@JsonIgnore
	public StringProperty getExpiryDateProperty() {
		if(null != expiryDate) {
			expiryDateProperty.set(expiryDate);
		}
		return expiryDateProperty;
	}

	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}

	@JsonIgnore
	public StringProperty getBatchCodeProperty() {
		if(null != batchCode) {
			batchCodeProperty.set(batchCode);
		}
		return batchCodeProperty;
	}

	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}

	@JsonIgnore
	public DoubleProperty getQuantityProperty() {
		if(null != quantity) {
			quantityProperty.set(quantity);
		}
		return quantityProperty;
	}

	public void setQuantityProperty(DoubleProperty quantityProperty) {
		this.quantityProperty = quantityProperty;
	}

	@JsonIgnore
	public DoubleProperty getRateProperty() {
		if(null != rate) {
			rateProperty.set(rate);
		}
		return rateProperty;
	}

	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	@JsonIgnore
	public DoubleProperty getTaxRateProperty() {
		if(null != taxRate) {
			taxRateProperty.set(taxRate);
		}
		return taxRateProperty;
	}

	public void setTaxRateProperty(DoubleProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}

	@JsonIgnore
	public DoubleProperty getSaleValueProperty() {
		if(null != saleValue) {
			saleValueProperty.set(saleValue);
		}
		return saleValueProperty;
	}

	public void setSaleValueProperty(DoubleProperty saleValueProperty) {
		this.saleValueProperty = saleValueProperty;
	}

	@JsonIgnore
	public DoubleProperty getCostProperty() {
		if(null != cost) {
			costProperty.set(cost);
		}
		return costProperty;
	}

	public void setCostProperty(DoubleProperty costProperty) {
		this.costProperty = costProperty;
	}

	@JsonIgnore
	public DoubleProperty getCostValueProperty() {
		if(null != costValue) {
			costValueProperty.set(costValue);
		}
		return costValueProperty;
	}

	public void setCostValueProperty(DoubleProperty costValueProperty) {
		this.costValueProperty = costValueProperty;
	}

	@Override
	public String toString() {
		return "RetailSalesDetailReport [invoiceDate=" + invoiceDate + ", voucherNum=" + voucherNum + ", customerName="
				+ customerName + ", patientName=" + patientName + ", patientID=" + patientID + ", itemName=" + itemName
				+ ", groupName=" + groupName + ", expiryDate=" + expiryDate + ", batchCode=" + batchCode + ", itemCode="
				+ itemCode + ", quantity=" + quantity + ", rate=" + rate + ", taxRate=" + taxRate + ", saleValue="
				+ saleValue + ", cost=" + cost + ", costValue=" + costValue + ", invoiceDateProperty="
				+ invoiceDateProperty + ", voucherNumProperty=" + voucherNumProperty + ", customerNameProperty="
				+ customerNameProperty + ", patientNameProperty=" + patientNameProperty + ", itemNameProperty="
				+ itemNameProperty + ", groupNameProperty=" + groupNameProperty + ", expiryDateProperty="
				+ expiryDateProperty + ", batchCodeProperty=" + batchCodeProperty + ", quantityProperty="
				+ quantityProperty + ", rateProperty=" + rateProperty + ", taxRateProperty=" + taxRateProperty
				+ ", saleValueProperty=" + saleValueProperty + ", costProperty=" + costProperty + ", costValueProperty="
				+ costValueProperty + "]";
	}

	
	
	

}
