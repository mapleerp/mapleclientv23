package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PendingAmountReport {

	
	  private Date currentDate;
	  private Date endDate;
	  private String account;
	  private String accountName;
	  private String memberId;
	  private String memberName;
	  private String familyName;
	  private String headOfFamily;
	  private String orgId;
	  private String email;
	  private String mobileNo;
	  private String companyName;
	  private String branchName;
	  
	  
	  
		@JsonIgnore
		private StringProperty memberIdProperty;
		@JsonIgnore
		private StringProperty memberNameProperty;
		@JsonIgnore
		private StringProperty familyNameProperty;
		@JsonIgnore
		private StringProperty headOfFamilyProperty;
		
		@JsonIgnore
		private StringProperty mobileNoProperty;
		@JsonIgnore
		private StringProperty emailProperty;
		
			
		
				
		public PendingAmountReport() {		
		
			this.memberIdProperty = new SimpleStringProperty();
			this.memberNameProperty = new SimpleStringProperty();
			this.familyNameProperty = new SimpleStringProperty();
			this.headOfFamilyProperty = new SimpleStringProperty();
			this.mobileNoProperty = new SimpleStringProperty();
			this.emailProperty = new SimpleStringProperty();
		}		
		
		
	public String getAccountName() {
			return accountName;
		}
		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}
		public String getMemberId() {
			return memberId;
		}
		public void setMemberId(String memberId) {
			this.memberId = memberId;
		}
		public String getMemberName() {
			return memberName;
		}
		public void setMemberName(String memberName) {
			this.memberName = memberName;
		}
		public String getFamilyName() {
			return familyName;
		}
		public void setFamilyName(String familyName) {
			this.familyName = familyName;
		}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	
	public String getHeadOfFamily() {
		return headOfFamily;
	}


	public void setHeadOfFamily(String headOfFamily) {
		this.headOfFamily = headOfFamily;
	}


	public String getOrgId() {
		return orgId;
	}


	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}


	public StringProperty getHeadOfFamilyProperty() {
		headOfFamilyProperty.set(headOfFamily);
		return headOfFamilyProperty;
	}


	public void setHeadOfFamilyProperty(StringProperty headOfFamilyProperty) {
		this.headOfFamilyProperty = headOfFamilyProperty;
	}



	public StringProperty getMobileNoProperty() {
		mobileNoProperty.set(mobileNo);
		return mobileNoProperty;
	}


	public void setMobileNoProperty(StringProperty mobileNoProperty) {
		this.mobileNoProperty = mobileNoProperty;
	}


	public StringProperty getMemberIdProperty() {
		memberIdProperty.set(memberId);
		return memberIdProperty;
	}
	public void setMemberIdProperty(StringProperty memberIdProperty) {
		this.memberIdProperty = memberIdProperty;
	}
	public StringProperty getMemberNameProperty() {
		memberNameProperty.set(memberName);
		return memberNameProperty;
	}
	public void setMemberNameProperty(StringProperty memberNameProperty) {
		this.memberNameProperty = memberNameProperty;
	}
	public StringProperty getFamilyNameProperty() {
		familyNameProperty.set(familyName);
		return familyNameProperty;
	}
	public void setFamilyNameProperty(StringProperty familyNameProperty) {
		this.familyNameProperty = familyNameProperty;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public StringProperty getEmailProperty() {
		emailProperty.set(email);
		return emailProperty;
	}


	public void setEmailProperty(StringProperty emailProperty) {
		this.emailProperty = emailProperty;
	}


	public String getCompanyName() {
		return companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	  
	
	  
	  
}
