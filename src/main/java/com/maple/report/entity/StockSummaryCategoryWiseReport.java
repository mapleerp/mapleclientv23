package com.maple.report.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockSummaryCategoryWiseReport {
	String itemName;
	Double qty;
	Double rate;
	Double value;
//	Date transDate;
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	@JsonIgnore
	private DoubleProperty valueProperty;

	@JsonIgnore
	private ObjectProperty<LocalDate> transDate;
	

	public StockSummaryCategoryWiseReport() {
		
		this.itemNameProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.rateProperty = new SimpleDoubleProperty();
		this.valueProperty =new SimpleDoubleProperty();
		this.transDate = new SimpleObjectProperty<LocalDate>(null);
	}
	@JsonProperty
	public ObjectProperty<LocalDate> getTransDateProperty( ) {
		return transDate;
	}
 
	public void setTransDateProperty(ObjectProperty<LocalDate> transDate) {
		this.transDate = transDate;
	}
	@JsonProperty("transDate")
	public java.sql.Date gettransDate ( ) {
		if(null==this.transDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.transDate.get() );
		}
	 
		
	}
	
   public void settransDate (java.sql.Date transDateProperty) {
						if(null!=transDateProperty)
		this.transDate.set(transDateProperty.toLocalDate());
	}

	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	@JsonIgnore
	public DoubleProperty getrateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}

	public void setrateProperty(Double rate) {
		this.rate = rate;
	}
	@JsonIgnore
	public DoubleProperty getvalueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}

	public void setvalueProperty(Double value) {
		this.value = value;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "StockSummaryCategoryWiseReport [itemName=" + itemName + ", qty=" + qty + ", rate=" + rate + ", value="
				+ value + ", itemNameProperty=" + itemNameProperty + ", qtyProperty=" + qtyProperty + ", rateProperty="
				+ rateProperty + ", valueProperty=" + valueProperty + ", transDate=" + transDate + "]";
	}
	

}
