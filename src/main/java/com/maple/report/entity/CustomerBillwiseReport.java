package com.maple.report.entity;

import java.util.Date;

public class CustomerBillwiseReport {

	String companyName;
	String branchName;
	String branchEmail;
	String branchAddress1;
	String branchAddress2;
	String branchPhoneNo;
	String branchcode;
	String branchWebsite;
    String website;
	String customerName;
	String debit;
	String credit;
	Date startDate;
	Date endDate;
	public String getDebit() {
		return debit;
	}
	public void setDebit(String debit) {
		this.debit = debit;
	}
	public String getCredit() {
		return credit;
	}
	public void setCredit(String credit) {
		this.credit = credit;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	String customerAccount;
	String customerAddress;
	Date fromDate;
	Date toDate;
	String branchCode;
	Date date;
	String RefNo;
	

	Double openingAmount;
	 Double pendingAmount;
	 Date dueOnDate;
	 String overdue;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchName() {
		return branchName;
	}
	
	
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchAddress1() {
		return branchAddress1;
	}
	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}
	public String getBranchAddress2() {
		return branchAddress2;
	}
	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}
	public String getBranchPhoneNo() {
		return branchPhoneNo;
	}
	public void setBranchPhoneNo(String branchPhoneNo) {
		this.branchPhoneNo = branchPhoneNo;
	}
	public String getBranchcode() {
		return branchcode;
	}
	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerAccount() {
		return customerAccount;
	}
	public void setCustomerAccount(String customerAccount) {
		this.customerAccount = customerAccount;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getRefNo() {
		return RefNo;
	}
	public void setRefNo(String RefNo) {
		this.RefNo = RefNo;
	}

	public Double getOpeningAmount() {
		return openingAmount;
	}
	public void setOpeningAmount(Double openingAmount) {
		this.openingAmount = openingAmount;
	}
	public Double getPendingAmount() {
		return pendingAmount;
	}
	public void setPendingAmount(Double pendingAmount) {
		this.pendingAmount = pendingAmount;
	}
	public Date getDueOnDate() {
		return dueOnDate;
	}
	public void setDueOnDate(Date dueOnDate) {
		this.dueOnDate = dueOnDate;
	}
	public String getOverdue() {
		return overdue;
	}
	public void setOverdue(String overdue) {
		this.overdue = overdue;
	}
	@Override
	public String toString() {
		return "CustomerBillwiseReport [companyName=" + companyName + ", branchName=" + branchName + ", branchEmail="
				+ branchEmail + ", branchAddress1=" + branchAddress1 + ", branchAddress2=" + branchAddress2
				+ ", branchPhoneNo=" + branchPhoneNo + ", branchcode=" + branchcode + ", branchWebsite=" + branchWebsite
				+ ", website=" + website + ", customerName=" + customerName + ", customerAccount=" + customerAccount
				+ ", customerAddress=" + customerAddress + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", branchCode=" + branchCode + ", date=" + date + ", openingAmount=" + openingAmount
				+ ", pendingAmount=" + pendingAmount + ", dueOnDate=" + dueOnDate + ", overdue=" + overdue + "]";
	}
	
	

}
