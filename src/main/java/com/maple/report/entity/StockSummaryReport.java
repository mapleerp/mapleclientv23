package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockSummaryReport {
	String id;
	 String particulars;
	 Double value;
	 
	 
	 @JsonIgnore
	 private StringProperty particularsProperty;
	 
	 @JsonIgnore
	 private DoubleProperty valueProperty;

	public StockSummaryReport() {
		this.particularsProperty = new SimpleStringProperty();
		this.valueProperty =new SimpleDoubleProperty();
	}
	 
	@JsonIgnore
	public StringProperty getparticularsProperty() {
		particularsProperty.set(particulars);
		return particularsProperty;
	}

	public void setparticularsProperty(String particulars) {
		this.particulars = particulars;
	}
	@JsonIgnore
	public DoubleProperty getvalueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}

	public void setvalueProperty(Double value) {
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "StockSummaryReport [id=" + id + ", particulars=" + particulars + ", value=" + value
				+ ", particularsProperty=" + particularsProperty + ", valueProperty=" + valueProperty + "]";
	}	 

}
