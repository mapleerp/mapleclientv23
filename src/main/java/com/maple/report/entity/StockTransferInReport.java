package com.maple.report.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

 

public class StockTransferInReport implements Serializable{

	
	 
    private String id;
    String voucherNumber;
	Date invoiceDate;

	String fromBranch;
	String itemName;
	String category;
	String itemCode;
	String batch;
	Date expiryDate;
	Double qty;
	String unitName;
	Double rate;
	Double amount;
	String userName;
	
	String comapnyName;
	String branchName;
	String branchEmail;
	String branchPhone;
	String branchAddress;
	String branchState;
	String branchGst;
	String branchWebsite;
	
	
	
	public StockTransferInReport() {
		this.voucherNumberProperty = new SimpleStringProperty("");
		this.invoiceDateProperty = new SimpleStringProperty("");
		this.fromBranchProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.categoryProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.expiryDateProperty = new SimpleStringProperty("");
		this.unitNameProperty = new SimpleStringProperty("");
		this.userNameProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty();
		this.rateProperty = new SimpleDoubleProperty();
		this.qtyProperty = new SimpleDoubleProperty();

	}
	@JsonIgnore
	StringProperty voucherNumberProperty;
	
	@JsonIgnore
	StringProperty invoiceDateProperty;
	
	@JsonIgnore
	StringProperty fromBranchProperty;
	
	@JsonIgnore
	StringProperty itemNameProperty;
	
	@JsonIgnore
	StringProperty categoryProperty;
	
	@JsonIgnore
	StringProperty itemCodeProperty;
	
	@JsonIgnore
	StringProperty batchProperty;
	
	@JsonIgnore
	StringProperty expiryDateProperty;
	
	@JsonIgnore
	DoubleProperty qtyProperty;
	
	@JsonIgnore
	StringProperty unitNameProperty;
	
	@JsonIgnore
	DoubleProperty rateProperty;
	
	@JsonIgnore
	DoubleProperty amountProperty;
	
	@JsonIgnore
	StringProperty userNameProperty;	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getFromBranch() {
		return fromBranch;
	}
	public void setFromBranch(String fromBranch) {
		this.fromBranch = fromBranch;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	public String getComapnyName() {
		return comapnyName;
	}
	public void setComapnyName(String comapnyName) {
		this.comapnyName = comapnyName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchState() {
		return branchState;
	}
	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	public StringProperty getInvoiceDateProperty() {
		if(null != invoiceDate)
		{
		invoiceDateProperty.set(SystemSetting.UtilDateToString(invoiceDate, "yyyy-MM-dd"));
		}
		return invoiceDateProperty;
	}
	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}
	public StringProperty getFromBranchProperty() {
		fromBranchProperty.set(fromBranch);
		return fromBranchProperty;
	}
	public void setFromBranchProperty(StringProperty fromBranchProperty) {
		this.fromBranchProperty = fromBranchProperty;
	}
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public StringProperty getCategoryProperty() {
		categoryProperty.set(category);
		return categoryProperty;
	}
	public void setCategoryProperty(StringProperty categoryProperty) {
		this.categoryProperty = categoryProperty;
	}
	public StringProperty getItemCodeProperty() {
		itemCodeProperty.set(itemCode);
		return itemCodeProperty;
	}
	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}
	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}
	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}
	public StringProperty getExpiryDateProperty() {
		if(null != expiryDate)
		{
		expiryDateProperty.set(SystemSetting.UtilDateToString(expiryDate, "yyyy-MM-dd"));
		}
		return expiryDateProperty;
	}
	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setUnitNameProperty(StringProperty unitNameProperty) {
		this.unitNameProperty = unitNameProperty;
	}
	public DoubleProperty getRateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}
	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	public StringProperty getUserNameProperty() {
		userNameProperty.set(userName);
		return userNameProperty;
	}
	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}
	@Override
	public String toString() {
		return "StockTransferInReport [id=" + id + ", voucherNumber=" + voucherNumber + ", invoiceDate=" + invoiceDate
				+ ", fromBranch=" + fromBranch + ", itemName=" + itemName + ", category=" + category + ", itemCode="
				+ itemCode + ", batch=" + batch + ", expiryDate=" + expiryDate + ", qty=" + qty + ", unitName="
				+ unitName + ", rate=" + rate + ", amount=" + amount + ", userName=" + userName + ", comapnyName="
				+ comapnyName + ", branchName=" + branchName + ", branchEmail=" + branchEmail + ", branchPhone="
				+ branchPhone + ", branchAddress=" + branchAddress + ", branchState=" + branchState + ", branchGst="
				+ branchGst + ", branchWebsite=" + branchWebsite + ", voucherNumberProperty=" + voucherNumberProperty
				+ ", invoiceDateProperty=" + invoiceDateProperty + ", fromBranchProperty=" + fromBranchProperty
				+ ", itemNameProperty=" + itemNameProperty + ", categoryProperty=" + categoryProperty
				+ ", itemCodeProperty=" + itemCodeProperty + ", batchProperty=" + batchProperty
				+ ", expiryDateProperty=" + expiryDateProperty + ", qtyProperty=" + qtyProperty + ", unitNameProperty="
				+ unitNameProperty + ", rateProperty=" + rateProperty + ", amountProperty=" + amountProperty
				+ ", userNameProperty=" + userNameProperty + "]";
	}
	
	
	
	
	
	
	

}
