package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class HsnWisePurchaseDtlReport {

	Date voucherDate;
	String itemName;
	String hsnCode;
	String unitName;
	Double qty;
	Double value;
	Double rate;
	Double taxRate;
	
	@JsonIgnore
	StringProperty voucherDateProperty;
	
	@JsonIgnore
	StringProperty itemNameProperty;
	
	@JsonIgnore
	StringProperty hsnCodeProperty;
	
	@JsonIgnore
	StringProperty unitNameProperty;
	
	@JsonIgnore
	DoubleProperty qtyProperty;
	
	
	@JsonIgnore
	DoubleProperty valueProperty;
	
	@JsonIgnore 
	DoubleProperty rateProperty;
	
	@JsonIgnore
	DoubleProperty taxRateProperty;
	
	
	public HsnWisePurchaseDtlReport() {
		this.voucherDateProperty = new SimpleStringProperty();
		this.itemNameProperty =new SimpleStringProperty();
		this.hsnCodeProperty =new SimpleStringProperty();
		this.unitNameProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.valueProperty =  new SimpleDoubleProperty();
		this.rateProperty =  new SimpleDoubleProperty();
		this.taxRateProperty =  new SimpleDoubleProperty();
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	
	public StringProperty getVoucherDateProperty() {
		if(null != voucherDate)
		{
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate,"yyyy-MM-dd"));
		}
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	public StringProperty getHsnCodeProperty() {
		hsnCodeProperty.set(hsnCode);
		return hsnCodeProperty;
	}
	public void setHsnCodeProperty(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	public void setUnitNameProperty(String unitName) {
		this.unitName = unitName;
	}
	public DoubleProperty getQtyProperty() {
		if(null != qty)
		{
			qtyProperty.set(qty);
		}
		return qtyProperty;
	}
	public void setQtyProperty(Double qty) {
		this.qty = qty;
	}
	public DoubleProperty getValueProperty() {
		if(null != value)
		{
			valueProperty.set(value);
		}
		return valueProperty;
	}
	public void setValueProperty(Double value) {
		this.value = value;
	}
	public DoubleProperty getRateProperty() {
		if(null != rate)
		{
			rateProperty.set(rate);
		}
		return rateProperty;
	}
	public void setRateProperty(Double rate) {
		this.rate = rate;
	}
	public DoubleProperty getTaxRateProperty() {
		if(null != taxRate)
		{
			taxRateProperty.set(taxRate);
		}
		return taxRateProperty;
	}
	public void setTaxRateProperty(Double taxRate) {
		this.taxRate = taxRate;
	}
	@Override
	public String toString() {
		return "HsnWisePurchaseDtlReport [voucherDate=" + voucherDate + ", itemName=" + itemName + ", hsnCode="
				+ hsnCode + ", unitName=" + unitName + ", qty=" + qty + ", value=" + value + ", rate=" + rate
				+ ", taxRate=" + taxRate + "]";
	}
	
}
