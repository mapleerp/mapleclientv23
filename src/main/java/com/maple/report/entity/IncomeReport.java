package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class IncomeReport {
	String branchCode;
	String branchName;

	String accountId;
	String accountName;

	int slNo;

	public int getSlNo() {
		return slNo;
	}

	public void setSlNo(int slNo) {
		this.slNo = slNo;
	}

	String companyName;

	Date endDate;

	Double amount;
	

	public IncomeReport() {
		this.accountNameProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty(0.0);
		this.date = new SimpleObjectProperty<LocalDate>();
		this.slnoProperty = new SimpleStringProperty();
	}

	@JsonIgnore
	StringProperty slnoProperty;

	@JsonIgnore
	StringProperty accountNameProperty;

	@JsonIgnore
	DoubleProperty amountProperty;


	@JsonIgnore
	private ObjectProperty<LocalDate> date;

	@JsonProperty("date")
	public java.sql.Date getDate() {
		if (null == this.date.get()) {
			return null;
		} else {
			return java.sql.Date.valueOf(this.date.get());
		}

	}

	public void setDate(java.sql.Date dateProperty) {
		if (null != dateProperty)
			this.date.set(dateProperty.toLocalDate());
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	
	public StringProperty getSlnoProperty() {
		slnoProperty.set(slNo+"");
		return slnoProperty;
	}

	public void setSlnoProperty(StringProperty slnoProperty) {
		this.slnoProperty = slnoProperty;
	}

	public StringProperty getAccountNameProperty() {
		return accountNameProperty;
	}

	public void setAccountNameProperty(StringProperty accountNameProperty) {
		this.accountNameProperty = accountNameProperty;
	}

	public DoubleProperty getAmountProperty() {
		if(null != amount)
		{
			amountProperty.set(amount);
		}
		
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	@Override
	public String toString() {
		return "IncomeReport [branchCode=" + branchCode + ", branchName=" + branchName + ", accountId=" + accountId
				+ ", accountName=" + accountName + ", slNo=" + slNo + ", companyName=" + companyName + ", endDate="
				+ endDate + ", amount=" + amount + ", slnoProperty=" + slnoProperty + ", accountNameProperty="
				+ accountNameProperty + ", amountProperty=" + amountProperty + ", date=" + date + "]";
	}

	

	
	

}
