package com.maple.report.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductionDtlsReport {
	String itemName;
	Double qty;
	String unitName;
	String branchCode;
	
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private SimpleObjectProperty<LocalDate> voucherDate;

	
	
	
	public ProductionDtlsReport() {
		
		this.itemNameProperty = new SimpleStringProperty();
		this.unitNameProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.voucherDate = new SimpleObjectProperty<LocalDate>(null);
	}



	@JsonProperty("voucherDate")
	public java.sql.Date getvoucherDate( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}

   public void setvoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}
   public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
		return voucherDate;
	}

	public void setvoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
		this.voucherDate = (SimpleObjectProperty<LocalDate>) voucherDate;
	}
	

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public StringProperty getunitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}
	

	public void setunitProperty(String unitName) {
		this.unitName = unitName;
	}

	public void setitemNameProperty(String itemName) 
	{
		this.itemName = itemName;
	}
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setitemNameProperty(Double qty) 
	{
		this.qty = qty;
	}
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	

	@Override
	public String toString() {
		return "ProductionDtlsReport [itemName=" + itemName + ", qty=" + qty + ", unitName=" + unitName
				+ ", branchCode=" + branchCode + "]";
	}

	

}
