package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockSummaryDtlReport {
	
	Date rDate;
	String itemName;
	Date fromDate;
	Date toDate;
	String particulars;
	String voucherNo;
	Double inwardQty;
	Double inwardValue;
	Double outwardQty;
	Double outwardValue;
	Double closingQty;
	Double closingValue;
	String unitName;
	String companyName;
	String branchCode;
	Double openingStock;
	String voucherNumber;
	Date updatedTime;
	
	
	@JsonIgnore
	private StringProperty particularsProperty;
	
	@JsonIgnore
	private StringProperty invoiceProperty;
	
	@JsonIgnore
	private DoubleProperty inwardQtyProperty;
	
	@JsonIgnore
	private DoubleProperty inwardValueProperty;
	
	@JsonIgnore
	private DoubleProperty outWardQtyProperty;
	
	@JsonIgnore
	private DoubleProperty outWardValueProperty;
	
	@JsonIgnore
	private DoubleProperty closingQtyProperty;
	
	@JsonIgnore
	private DoubleProperty closingValueProperty;
	
	
	
	public StockSummaryDtlReport() {
		
		this.particularsProperty = new SimpleStringProperty();
		this.invoiceProperty = new SimpleStringProperty();
		this.inwardQtyProperty =  new SimpleDoubleProperty();
		this.inwardValueProperty =  new SimpleDoubleProperty();
		this.outWardQtyProperty =  new SimpleDoubleProperty();
		this.outWardValueProperty =  new SimpleDoubleProperty();
		this.closingQtyProperty = new SimpleDoubleProperty();
		this.closingValueProperty =  new SimpleDoubleProperty();
	}
	
	@JsonIgnore
	public void setparticularsProperty(String particulars) {
		this.particulars = particulars;
	}
	public StringProperty getparticularsProperty() {
		particularsProperty.set(particulars);
		return particularsProperty;
	}
	
	
	@JsonIgnore
	public void setinvoiceProperty(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public StringProperty getinvoiceProperty() {
		invoiceProperty.set(voucherNo);
		return invoiceProperty;
	}
	
	@JsonIgnore
	public void setinwardQtyProperty(Double inwardQty) {
		this.inwardQty = inwardQty;
	}
	public DoubleProperty getinwardQtyProperty() {
		inwardQtyProperty.set(inwardQty);
		return inwardQtyProperty;
	}
	

	@JsonIgnore
	public void setinwardValueProperty(Double inwardValue) {
		this.inwardValue = inwardValue;
	}
	public DoubleProperty getinwardValueProperty() {
		inwardValueProperty.set(inwardValue);
		return inwardValueProperty;
	}
	
	@JsonIgnore
	public void setoutWardQtyProperty(Double outwardQty) {
		this.outwardQty = outwardQty;
	}
	public DoubleProperty getoutWardQtyProperty() {
		outWardQtyProperty.set(outwardQty);
		return outWardQtyProperty;
	}
	
	@JsonIgnore
	public void setoutWardValueProperty(Double outwardValue) {
		this.outwardValue = outwardValue;
	}
	public DoubleProperty getoutWardValueProperty() {
		outWardValueProperty.set(outwardValue);
		return outWardValueProperty;
	}
	
	@JsonIgnore
	public void setclosingQtyProperty(Double closingQty) {
		this.closingQty = closingQty;
	}
	public DoubleProperty getclosingQtyProperty() {
		closingQtyProperty.set(closingQty);
		return closingQtyProperty;
	}
	
	@JsonIgnore
	public void setclosingValueProperty(Double closingValue) {
		this.closingValue = closingValue;
	}
	public DoubleProperty getclosingValueProperty() {
		closingValueProperty.set(closingValue);
		return closingValueProperty;
	}
	

	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	

	public Double getOpeningStock() {
		return openingStock;
	}
	public void setOpeningStock(Double openingStock) {
		this.openingStock = openingStock;
	}
	public Date getrDate() {
		return rDate;
	}
	public void setrDate(Date rDate) {
		this.rDate = rDate;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Double getInwardQty() {
		return inwardQty;
	}
	public void setInwardQty(Double inwardQty) {
		this.inwardQty = inwardQty;
	}
	public Double getInwardValue() {
		return inwardValue;
	}
	public void setInwardValue(Double inwardValue) {
		this.inwardValue = inwardValue;
	}
	public Double getOutwardQty() {
		return outwardQty;
	}
	public void setOutwardQty(Double outwardQty) {
		this.outwardQty = outwardQty;
	}
	public Double getOutwardValue() {
		return outwardValue;
	}
	public void setOutwardValue(Double outwardValue) {
		this.outwardValue = outwardValue;
	}
	public Double getClosingQty() {
		return closingQty;
	}
	public void setClosingQty(Double closingQty) {
		this.closingQty = closingQty;
	}
	public Double getClosingValue() {
		return closingValue;
	}
	public void setClosingValue(Double closingValue) {
		this.closingValue = closingValue;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	@Override
	public String toString() {
		return "StockSummaryDtlReport [rDate=" + rDate + ", itemName=" + itemName + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + ", particulars=" + particulars + ", voucherNo=" + voucherNo + ", inwardQty="
				+ inwardQty + ", inwardValue=" + inwardValue + ", outwardQty=" + outwardQty + ", outwardValue="
				+ outwardValue + ", closingQty=" + closingQty + ", closingValue=" + closingValue + ", unitName="
				+ unitName + ", companyName=" + companyName + ", branchCode=" + branchCode + ", openingStock="
				+ openingStock + ", voucherNumber=" + voucherNumber + ", updatedTime=" + updatedTime
				+ ", particularsProperty=" + particularsProperty + ", invoiceProperty=" + invoiceProperty
				+ ", inwardQtyProperty=" + inwardQtyProperty + ", inwardValueProperty=" + inwardValueProperty
				+ ", outWardQtyProperty=" + outWardQtyProperty + ", outWardValueProperty=" + outWardValueProperty
				+ ", closingQtyProperty=" + closingQtyProperty + ", closingValueProperty=" + closingValueProperty + "]";
	}
	

}
