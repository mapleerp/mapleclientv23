package com.maple.report.entity;

import java.sql.Date;

public class CustomerwiseReportGst {
	
	
	String customer;
	Date date;
	String billNo;
	String amount;
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "CustomerwiseReportGst [customer=" + customer + ", date=" + date + ", billNo=" + billNo + ", amount="
				+ amount + "]";
	}
	
	
	

}
