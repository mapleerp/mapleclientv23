package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PhysicalStockReport {

	private String batch;
	private Double qty;
	private Double mrp;
	private Double amount;

	private String itemName;
	private Date expiryDate;
	private Date manufactureDate;
	private String voucherNumber;
	Date voucherDate;
	
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty mrpProperty;
	
	@JsonIgnore
	private StringProperty expiryDateProperty;
	
	@JsonIgnore
	private StringProperty manufactureDateProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;

	public PhysicalStockReport() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.voucherNumberProperty = new SimpleStringProperty("");
		this.expiryDateProperty = new SimpleStringProperty("");
		this.manufactureDateProperty = new SimpleStringProperty("");
		this.voucherDateProperty = new SimpleStringProperty("");

		this.qtyProperty = new SimpleDoubleProperty();
		this.mrpProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();


	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}

	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}

	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public DoubleProperty getMrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}

	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}

	public StringProperty getExpiryDateProperty() {
		
		if(null != manufactureDate)
		{
		expiryDateProperty.set(SystemSetting.UtilDateToString(expiryDate, "yyyy-MM-dd"));
		}
		return expiryDateProperty;
	}

	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}

	public StringProperty getManufactureDateProperty() {
		if(null != manufactureDate)
		{
		manufactureDateProperty.set(SystemSetting.UtilDateToString(manufactureDate, "yyyy-MM-dd"));
		}
		return manufactureDateProperty;
	}

	public void setManufactureDateProperty(StringProperty manufactureDateProperty) {
		this.manufactureDateProperty = manufactureDateProperty;
	}

	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		return voucherDateProperty;
	}

	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}

	
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	@Override
	public String toString() {
		return "PhysicalStockReport [batch=" + batch + ", qty=" + qty + ", mrp=" + mrp + ", itemName=" + itemName
				+ ", expiryDate=" + expiryDate + ", manufactureDate=" + manufactureDate + ", voucherNumber="
				+ voucherNumber + ", voucherDate=" + voucherDate + ", itemNameProperty=" + itemNameProperty
				+ ", batchProperty=" + batchProperty + ", voucherNumberProperty=" + voucherNumberProperty
				+ ", qtyProperty=" + qtyProperty + ", mrpProperty=" + mrpProperty + ", expiryDateProperty="
				+ expiryDateProperty + ", manufactureDateProperty=" + manufactureDateProperty + ", voucherDateProperty="
				+ voucherDateProperty + "]";
	}
	
	
	

}
