package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class RetailSalesSummaryReport {
	
	String invoiceDate;
	String voucherNum;
	String customerName;
	String patientName;
	String patientID;
	String doctorName;
	Double beforeGST;
	Double gst;
	Double invoiceAmount;
	Double totalPaid;
	Double insurance;
	Double credit;
	Double cardReceipt;
	Double cashReceipt;
	String user;
	
	
	@JsonIgnore
	private StringProperty invoiceDateProperty;
	@JsonIgnore
	private StringProperty voucherNumProperty;
	@JsonIgnore
	private StringProperty customerNameProperty;
	@JsonIgnore
	private StringProperty patientNameProperty;
	@JsonIgnore
	private StringProperty doctorNameProperty;
	@JsonIgnore
	private DoubleProperty beforeGSTProperty;
	@JsonIgnore
	private DoubleProperty gstProperty;
	@JsonIgnore
	private DoubleProperty invoiceAmountProperty;
	@JsonIgnore
	private DoubleProperty totalPaidProperty;
	@JsonIgnore
	private DoubleProperty insuranceProperty;
	@JsonIgnore
	private DoubleProperty creditProperty;
	@JsonIgnore
	private DoubleProperty cardReceiptProperty;
	@JsonIgnore
	private DoubleProperty cashReceiptProperty;
	@JsonIgnore
	private StringProperty userProperty;
	
	
	public RetailSalesSummaryReport() {
		
		this.invoiceDateProperty = new SimpleStringProperty("");
		this.voucherNumProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.patientNameProperty = new SimpleStringProperty("");
		this.doctorNameProperty = new SimpleStringProperty("");
		this.userProperty = new SimpleStringProperty("");
				
		this.beforeGSTProperty = new SimpleDoubleProperty(0.0);
		this.gstProperty = new SimpleDoubleProperty(0.0);
		this.invoiceAmountProperty = new SimpleDoubleProperty(0.0);
		this.totalPaidProperty = new SimpleDoubleProperty(0.0);
		this.insuranceProperty = new SimpleDoubleProperty(0.0);
		this.creditProperty = new SimpleDoubleProperty(0.0);
		this.cardReceiptProperty = new SimpleDoubleProperty(0.0);
		this.cashReceiptProperty = new SimpleDoubleProperty(0.0);

	}


	public String getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public String getVoucherNum() {
		return voucherNum;
	}


	public void setVoucherNum(String voucherNum) {
		this.voucherNum = voucherNum;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public String getPatientName() {
		return patientName;
	}


	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}


	public String getPatientID() {
		return patientID;
	}


	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}


	public String getDoctorName() {
		return doctorName;
	}


	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}


	public Double getBeforeGST() {
		return beforeGST;
	}


	public void setBeforeGST(Double beforeGST) {
		this.beforeGST = beforeGST;
	}


	public Double getGst() {
		return gst;
	}


	public void setGst(Double gst) {
		this.gst = gst;
	}


	public Double getInvoiceAmount() {
		return invoiceAmount;
	}


	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}


	public Double getTotalPaid() {
		return totalPaid;
	}


	public void setTotalPaid(Double totalPaid) {
		this.totalPaid = totalPaid;
	}


	public Double getInsurance() {
		return insurance;
	}


	public void setInsurance(Double insurance) {
		this.insurance = insurance;
	}


	public Double getCredit() {
		return credit;
	}


	public void setCredit(Double credit) {
		this.credit = credit;
	}


	public Double getCardReceipt() {
		return cardReceipt;
	}


	public void setCardReceipt(Double cardReceipt) {
		this.cardReceipt = cardReceipt;
	}


	public Double getCashReceipt() {
		return cashReceipt;
	}


	public void setCashReceipt(Double cashReceipt) {
		this.cashReceipt = cashReceipt;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public StringProperty getInvoiceDateProperty() {
		if(null != invoiceDate) {
			invoiceDateProperty.set(invoiceDate);
		}
		return invoiceDateProperty;
	}


	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}


	public StringProperty getVoucherNumProperty() {
		if(null != voucherNum) {
			voucherNumProperty.set(voucherNum);
		}
		return voucherNumProperty;
	}


	public void setVoucherNumProperty(StringProperty voucherNumProperty) {
		this.voucherNumProperty = voucherNumProperty;
	}


	public StringProperty getCustomerNameProperty() {
		if(null != customerName) {
			customerNameProperty.set(customerName);
		}
		return customerNameProperty;
	}


	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}


	public StringProperty getPatientNameProperty() {
		if(null != patientName) {
			patientNameProperty.set(patientName);
		}
		return patientNameProperty;
	}


	public void setPatientNameProperty(StringProperty patientNameProperty) {
		this.patientNameProperty = patientNameProperty;
	}


	public StringProperty getDoctorNameProperty() {
		if(null != doctorName) {
			doctorNameProperty.set(doctorName);
		}
		return doctorNameProperty;
	}


	public void setDoctorNameProperty(StringProperty doctorNameProperty) {
		this.doctorNameProperty = doctorNameProperty;
	}


	public DoubleProperty getBeforeGSTProperty() {
		if(null != beforeGST) {
			beforeGSTProperty.set(beforeGST);
		}
		return beforeGSTProperty;
	}


	public void setBeforeGSTProperty(DoubleProperty beforeGSTProperty) {
		this.beforeGSTProperty = beforeGSTProperty;
	}


	public DoubleProperty getGstProperty() {
		if(null != gst) {
			gstProperty.set(gst);
		}
		return gstProperty;
	}


	public void setGstProperty(DoubleProperty gstProperty) {
		this.gstProperty = gstProperty;
	}


	public DoubleProperty getInvoiceAmountProperty() {
		if(null != invoiceAmount) {
			invoiceAmountProperty.set(invoiceAmount);
		}
		return invoiceAmountProperty;
	}


	public void setInvoiceAmountProperty(DoubleProperty invoiceAmountProperty) {
		this.invoiceAmountProperty = invoiceAmountProperty;
	}


	public DoubleProperty getTotalPaidProperty() {
		if(null != totalPaid) {
			totalPaidProperty.set(totalPaid);
		}
		return totalPaidProperty;
	}


	public void setTotalPaidProperty(DoubleProperty totalPaidProperty) {
		this.totalPaidProperty = totalPaidProperty;
	}


	public DoubleProperty getInsuranceProperty() {
		if(null != insurance) {
			insuranceProperty.set(insurance);
		}
		return insuranceProperty;
	}


	public void setInsuranceProperty(DoubleProperty insuranceProperty) {
		this.insuranceProperty = insuranceProperty;
	}


	public DoubleProperty getCreditProperty() {
		if(null != credit) {
			creditProperty.set(credit);
		}
		return creditProperty;
	}


	public void setCreditProperty(DoubleProperty creditProperty) {
		this.creditProperty = creditProperty;
	}


	public DoubleProperty getCardReceiptProperty() {
		if(null != cardReceipt) {
			cardReceiptProperty.set(cardReceipt);
		}
		return cardReceiptProperty;
	}


	public void setCardReceiptProperty(DoubleProperty cardReceiptProperty) {
		this.cardReceiptProperty = cardReceiptProperty;
	}


	public DoubleProperty getCashReceiptProperty() {
		if(null != cashReceipt) {
			cashReceiptProperty.set(cashReceipt);
		}
		return cashReceiptProperty;
	}


	public void setCashReceiptProperty(DoubleProperty cashReceiptProperty) {
		this.cashReceiptProperty = cashReceiptProperty;
	}


	public StringProperty getUserProperty() {
		if(null != user) {
			userProperty.set(user);
		}
		return userProperty;
	}


	public void setUserProperty(StringProperty userProperty) {
		this.userProperty = userProperty;
	}


	@Override
	public String toString() {
		return "RetailSalesSummaryReport [invoiceDate=" + invoiceDate + ", voucherNum=" + voucherNum + ", customerName="
				+ customerName + ", patientName=" + patientName + ", patientID=" + patientID + ", doctorName="
				+ doctorName + ", beforeGST=" + beforeGST + ", gst=" + gst + ", invoiceAmount=" + invoiceAmount
				+ ", totalPaid=" + totalPaid + ", insurance=" + insurance + ", credit=" + credit + ", cardReceipt="
				+ cardReceipt + ", cashReceipt=" + cashReceipt + ", user=" + user + ", invoiceDateProperty="
				+ invoiceDateProperty + ", voucherNumProperty=" + voucherNumProperty + ", customerNameProperty="
				+ customerNameProperty + ", patientNameProperty=" + patientNameProperty + ", doctorNameProperty="
				+ doctorNameProperty + ", beforeGSTProperty=" + beforeGSTProperty + ", gstProperty=" + gstProperty
				+ ", invoiceAmountProperty=" + invoiceAmountProperty + ", totalPaidProperty=" + totalPaidProperty
				+ ", insuranceProperty=" + insuranceProperty + ", creditProperty=" + creditProperty
				+ ", cardReceiptProperty=" + cardReceiptProperty + ", cashReceiptProperty=" + cashReceiptProperty
				+ ", userProperty=" + userProperty + "]";
	}
	
	
	

}
