package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TableWaiterReport {
	String tableName;
	String waiterName;
	String customiseSalesMode;
	
	@JsonIgnore
	private StringProperty tableNameProperty;
	
	@JsonIgnore
	private StringProperty waiterNameProperty;

	public TableWaiterReport() {
	
		this.tableNameProperty = new SimpleStringProperty();
		this.waiterNameProperty = new SimpleStringProperty();
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getWaiterName() {
		return waiterName;
	}

	public void setWaiterName(String waiterName) {
		this.waiterName = waiterName;
	}
	@JsonIgnore
	public StringProperty gettableNameProperty() {

		tableNameProperty.set(tableName);
		return tableNameProperty;
	}
	

	public String getCustomiseSalesMode() {
		return customiseSalesMode;
	}

	public void setCustomiseSalesMode(String customiseSalesMode) {
		this.customiseSalesMode = customiseSalesMode;
	}

	public void settableNameProperty(String tableName) {
		this.tableName = tableName;
	}
	
	@JsonIgnore
	public StringProperty getwaiterNameProperty() {

		waiterNameProperty.set(waiterName);
		return waiterNameProperty;
	}

	public void setwaiterNameProperty(String waiterName) {
		this.waiterName = waiterName;
	}

	@Override
	public String toString() {
		return "TableWaiterReport [tableName=" + tableName + ", waiterName=" + waiterName + ", tableNameProperty="
				+ tableNameProperty + ", waiterNameProperty=" + waiterNameProperty + "]";
	}

}
