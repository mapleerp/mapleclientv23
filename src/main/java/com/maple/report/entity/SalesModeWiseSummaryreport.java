package com.maple.report.entity;

import java.util.Date;

public class SalesModeWiseSummaryreport {

	String id;
	String paymentMode;
	Double amount;
	 Double totalAmount;
	 Date date;
	 Double advanceAmount;
	 String branchName;
		String companyName;
	public String getBranchName() {
			return branchName;
		}
		public void setBranchName(String branchName) {
			this.branchName = branchName;
		}
		public String getCompanyName() {
			return companyName;
		}
		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getAdvanceAmount() {
		return advanceAmount;
	}
	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	 
@Override
public String toString() {
	return "SalesModeWiseSummaryreport [id=" + id + ", paymentMode=" + paymentMode + ", amount=" + amount
			+ ", totalAmount=" + totalAmount + ", date=" + date + ", advanceAmount=" + advanceAmount + ", branchName="
			+ branchName + ", companyName=" + companyName + "]";
}

}
