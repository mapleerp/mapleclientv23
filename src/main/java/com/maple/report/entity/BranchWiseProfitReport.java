package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BranchWiseProfitReport {
	
	String branchName;
	Double profit;
	String itemName;
	String customerName;

	
	public BranchWiseProfitReport() {
		this.branchNameProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.profitProperty = new SimpleDoubleProperty(0.0);
		this.customerNameProperty = new SimpleStringProperty("");

	}

	@JsonIgnore
	StringProperty branchNameProperty;
	
	@JsonIgnore
	StringProperty itemNameProperty;
	
	@JsonIgnore
	DoubleProperty profitProperty;
	
	@JsonIgnore
	StringProperty customerNameProperty;
	

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}

	public StringProperty getBranchNameProperty() {
		branchNameProperty.set(branchName);
		return branchNameProperty;
	}

	public void setBranchNameProperty(StringProperty branchNameProperty) {
		this.branchNameProperty = branchNameProperty;
	}

	public DoubleProperty getProfitProperty() {
		profitProperty.set(profit);
		return profitProperty;
	}

	public void setProfitProperty(DoubleProperty profitProperty) {
		this.profitProperty = profitProperty;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}

	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}
	
	


}
