package com.maple.report.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PoliceReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	String id;
	String invoiceDate;
	String voucherNumber;
	String customerName;
	String mpsType;
	String patientID;
	private Double cashPaid;
	private Double cardPaid;
	private Double insuranceAmount;
	private Double invoiceAmount;
	private Double creditAmount;
	
	
	
	@JsonIgnore
	private StringProperty invoiceDateProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private StringProperty mpsTypeProperty;
	
	@JsonIgnore
	private StringProperty patientIDProperty;
	
	
	@JsonIgnore
	private DoubleProperty cashPaidProperty;
	
	@JsonIgnore
	private DoubleProperty cardPaidProperty;
	
	@JsonIgnore
	private DoubleProperty invoiceAmountProperty;
	@JsonIgnore
	private DoubleProperty creditAmountProperty;
	
	
	@JsonIgnore
	private DoubleProperty insuranceAmountProperty;
	

	public PoliceReport() {
		
		this.invoiceDateProperty = new SimpleStringProperty("");
		this.voucherNumberProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.mpsTypeProperty = new SimpleStringProperty("");
		this.patientIDProperty = new SimpleStringProperty("");
		
		this.cashPaidProperty = new SimpleDoubleProperty(0.0);
		this.cardPaidProperty = new SimpleDoubleProperty(0.0);
		this.invoiceAmountProperty = new SimpleDoubleProperty(0.0);
		this.creditAmountProperty = new SimpleDoubleProperty(0.0);
		this.insuranceAmountProperty = new SimpleDoubleProperty(0.0);

	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public String getMpsType() {
		return mpsType;
	}


	public void setMpsType(String mpsType) {
		this.mpsType = mpsType;
	}


	public String getPatientID() {
		return patientID;
	}


	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}


	public Double getCashPaid() {
		return cashPaid;
	}


	public void setCashPaid(Double cashPaid) {
		this.cashPaid = cashPaid;
	}


	public Double getCardPaid() {
		return cardPaid;
	}


	public void setCardPaid(Double cardPaid) {
		this.cardPaid = cardPaid;
	}


	public Double getInsuranceAmount() {
		return insuranceAmount;
	}


	public void setInsuranceAmount(Double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}


	public Double getInvoiceAmount() {
		return invoiceAmount;
	}


	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}


	public Double getCreditAmount() {
		return creditAmount;
	}


	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}


	@JsonIgnore

	public StringProperty getInvoiceDateProperty() {
		invoiceDateProperty.set(invoiceDate);
		return invoiceDateProperty;
	}
	

	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}

	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}


	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}

	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}


	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}

	@JsonIgnore
	public StringProperty getMpsTypeProperty() {
		mpsTypeProperty.set(mpsType);
		return mpsTypeProperty;
	}


	public void setMpsTypeProperty(StringProperty mpsTypeProperty) {
		this.mpsTypeProperty = mpsTypeProperty;
	}

	@JsonIgnore
	public StringProperty getPatientIDProperty() {
		patientIDProperty.set(patientID);
		return patientIDProperty;
	}


	public void setPatientIDProperty(StringProperty patientIDProperty) {
		this.patientIDProperty = patientIDProperty;
	}

	@JsonIgnore
	public DoubleProperty getCashPaidProperty() {
		cashPaidProperty.set(cashPaid);
		return cashPaidProperty;
	}


	public void setCashPaidProperty(DoubleProperty cashPaidProperty) {
		this.cashPaidProperty = cashPaidProperty;
	}

	@JsonIgnore
	public DoubleProperty getCardPaidProperty() {
		cardPaidProperty.set(cardPaid);
		return cardPaidProperty;
	}


	public void setCardPaidProperty(DoubleProperty cardPaidProperty) {
		this.cardPaidProperty = cardPaidProperty;
	}
	@JsonIgnore

	public DoubleProperty getInvoiceAmountProperty() {
		invoiceAmountProperty.set(invoiceAmount);
		return invoiceAmountProperty;
	}


	public void setInvoiceAmountProperty(DoubleProperty invoiceAmountProperty) {
		this.invoiceAmountProperty = invoiceAmountProperty;
	}

	@JsonIgnore
	public DoubleProperty getCreditAmountProperty() {
		if(null != creditAmount) {
		creditAmountProperty.set(creditAmount);
		}
		return creditAmountProperty;
	}


	public void setCreditAmountProperty(DoubleProperty creditAmountProperty) {
		this.creditAmountProperty = creditAmountProperty;
	}

	@JsonIgnore
	public DoubleProperty getInsuranceAmountProperty() {
		insuranceAmountProperty.set(insuranceAmount);
		return insuranceAmountProperty;
	}


	public void setInsuranceAmountProperty(DoubleProperty insuranceAmountProperty) {
		this.insuranceAmountProperty = insuranceAmountProperty;
	}


	@Override
	public String toString() {
		return "PoliceReport [id=" + id + ", invoiceDate=" + invoiceDate + ", voucherNumber=" + voucherNumber
				+ ", customerName=" + customerName + ", mpsType=" + mpsType + ", patientID=" + patientID + ", cashPaid="
				+ cashPaid + ", cardPaid=" + cardPaid + ", insuranceAmount=" + insuranceAmount + ", invoiceAmount="
				+ invoiceAmount + ", creditAmount=" + creditAmount + ", invoiceDateProperty=" + invoiceDateProperty
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", customerNameProperty=" + customerNameProperty
				+ ", mpsTypeProperty=" + mpsTypeProperty + ", patientIDProperty=" + patientIDProperty
				+ ", cashPaidProperty=" + cashPaidProperty + ", cardPaidProperty=" + cardPaidProperty
				+ ", invoiceAmountProperty=" + invoiceAmountProperty + ", creditAmountProperty=" + creditAmountProperty
				+ ", insuranceAmountProperty=" + insuranceAmountProperty + "]";
	}




}
