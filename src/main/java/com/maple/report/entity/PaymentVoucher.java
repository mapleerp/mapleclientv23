package com.maple.report.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PaymentVoucher {
	
	String id;
	String branchName;
	String voucherNO;
//	Date voucherDate;
	String remark;
	Double amount;
	String companyName;
	String accountName;
	Double totalAmount;
	String instrumentNumber;
	Date instrumentDate;
	String modeOfPayment;
	String branchEmail;
	String branchWebsite;
	String memberId;
	String familyName;
	
	@JsonIgnore
	private StringProperty accountNameProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private StringProperty voucherNoProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;
	
	
	public PaymentVoucher() {
		
		this.accountNameProperty = new SimpleStringProperty();
		this.amountProperty =new SimpleDoubleProperty();
		this.voucherNoProperty = new SimpleStringProperty();
		this.voucherDate = new SimpleObjectProperty<LocalDate>();
	}

	@JsonProperty("voucherDate")
	public java.sql.Date getvoucherDate( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}
	


public void setvoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}
   public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
		return voucherDate;
	}

	public void setvoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
		this.voucherDate = voucherDate;
	}
	@JsonIgnore
	public StringProperty getaccountNameProperty() {
		accountNameProperty.set(accountName);
		return accountNameProperty;
	}
	

	public void setaccountNameProperty(String accountName) {
		this.accountName = accountName;
	}
	@JsonIgnore
	public StringProperty getvoucherNoProperty() {
		voucherNoProperty.set(voucherNO);
		return voucherNoProperty;
	}
	

	public void setvoucherNoProperty(String voucherNO) {
		this.voucherNO = voucherNO;
	}
	@JsonIgnore
	public DoubleProperty getamountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	

	public void setamountProperty(Double amount) {
		this.amount = amount;
	}
	
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public Date getInstrumentDate() {
		return instrumentDate;
	}
	public void setInstrumentDate(Date instrumentDate) {
		this.instrumentDate = instrumentDate;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getVoucherNO() {
		return voucherNO;
	}
	public void setVoucherNO(String voucherNO) {
		this.voucherNO = voucherNO;
	}
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "PaymentVoucher [branchName=" + branchName + ", voucherNO=" + voucherNO + ", remark=" + remark
				+ ", amount=" + amount + ", companyName=" + companyName + ", accountName=" + accountName
				+ ", totalAmount=" + totalAmount + ", instrumentNumber=" + instrumentNumber + ", instrumentDate="
				+ instrumentDate + ", modeOfPayment=" + modeOfPayment + ", branchEmail=" + branchEmail
				+ ", branchWebsite=" + branchWebsite + ", memberId=" + memberId + ", familyName=" + familyName
				+ ", accountNameProperty=" + accountNameProperty + ", amountProperty=" + amountProperty
				+ ", voucherNoProperty=" + voucherNoProperty + ", voucherDate=" + voucherDate + "]";
	}
	
}
