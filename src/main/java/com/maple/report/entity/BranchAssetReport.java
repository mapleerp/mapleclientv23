package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BranchAssetReport {
	
	private String name;
	private Double amount;
	
	
	
	public BranchAssetReport() {
		this.nameProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty(0.0);

	}


	@JsonIgnore
	StringProperty nameProperty;
	
	@JsonIgnore
	DoubleProperty amountProperty;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public StringProperty getNameProperty() {
		nameProperty.set(name);
		return nameProperty;
	}
	public void setNameProperty(StringProperty nameProperty) {
		this.nameProperty = nameProperty;
	}
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	
	
	@Override
	public String toString() {
		return "BranchAssetReport [name=" + name + ", amount=" + amount + ", nameProperty=" + nameProperty
				+ ", amountProperty=" + amountProperty + "]";
	}
	
	
	

}
