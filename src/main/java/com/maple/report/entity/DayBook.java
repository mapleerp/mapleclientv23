package com.maple.report.entity;

import java.time.LocalDate;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DayBook {
	String id;
	String voucheNumber;
	Date voucherDate;
	String sourceVoucheNumber;
	String sourceVoucherType;
	String narration;
	
	Double drAmount;
	Double crAmount;
	
	String crAccountName;
	String drAccountName;
	
	String branchCode;
	String userId;

	
	@JsonIgnore
	private StringProperty sourceVoucherNumberProperty;
	
	@JsonIgnore
	private SimpleObjectProperty<LocalDate> sourceVoucherDate;
	
	@JsonIgnore
	private StringProperty sourceVoucherTypeProperty;
	
	@JsonIgnore
	private StringProperty narrationProperty;
	
	@JsonIgnore
	private StringProperty crAccountNameProperty;
	

	@JsonIgnore
	private StringProperty drAccountNameProperty;
	

	@JsonIgnore
	private DoubleProperty drAmountProperty;
	

	@JsonIgnore
	private DoubleProperty crAmountProperty;


	public DayBook() {
		
		this.sourceVoucherNumberProperty = new SimpleStringProperty();
		this.sourceVoucherDate = new SimpleObjectProperty<LocalDate>(null);
		this.sourceVoucherTypeProperty = new SimpleStringProperty();
		this.narrationProperty =  new SimpleStringProperty();
		this.crAccountNameProperty =  new SimpleStringProperty();
		this.drAccountNameProperty =  new SimpleStringProperty();
		this.drAmountProperty =  new SimpleDoubleProperty();
		this.crAmountProperty = new SimpleDoubleProperty();
	}
	public StringProperty getsourceVoucherNumberProperty() {
		sourceVoucherNumberProperty.set(sourceVoucheNumber);
		return sourceVoucherNumberProperty;
	}
	

	public void setsourceVoucherNumberProperty(String sourceVoucheNumber) {
		this.sourceVoucheNumber = sourceVoucheNumber;
	}
	public StringProperty getsourceVoucherTypeProperty() {
		sourceVoucherTypeProperty.set(sourceVoucherType);
		return sourceVoucherTypeProperty;
	}
	

	public void setsourceVoucherTypeProperty(String sourceVoucherType) {
		this.sourceVoucherType = sourceVoucherType;
	}
	public StringProperty getnarrationProperty() {
		narrationProperty.set(narration);
		return narrationProperty;
	}
	

	public void setnarrationProperty(String narration) {
		this.narration = narration;
	}
	public StringProperty getcrAccountNameProperty() {
		crAccountNameProperty.set(crAccountName);
		return crAccountNameProperty;
	}
	

	public void setcrAccountNameProperty(String crAccountName) {
		this.crAccountName = crAccountName;
	}
	
	public StringProperty getdrAccountNameProperty() {
		drAccountNameProperty.set(drAccountName);
		return drAccountNameProperty;
	}
	

	public void setdrAccountNameProperty(String drAccountName) {
		this.drAccountName = drAccountName;
	}
	
	public DoubleProperty getdrAmountProperty() {
		drAmountProperty.set(drAmount);
		return drAmountProperty;
	}
	

	public void setdrAmountProperty(Double drAmount) {
		this.drAmount = drAmount;
	}
	public DoubleProperty getcrAmountProperty() {
		crAmountProperty.set(crAmount);
		return crAmountProperty;
	}
	

	public void setcrAmountProperty(Double crAmount) {
		this.crAmount = crAmount;
	}
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getVoucheNumber() {
		return voucheNumber;
	}


	public void setVoucheNumber(String voucheNumber) {
		this.voucheNumber = voucheNumber;
	}


	public java.util.Date getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(Date date) {
		this.voucherDate = date;
	}


	public String getSourceVoucheNumber() {
		return sourceVoucheNumber;
	}


	public void setSourceVoucheNumber(String sourceVoucheNumber) {
		this.sourceVoucheNumber = sourceVoucheNumber;
	}


	public String getSourceVoucherType() {
		return sourceVoucherType;
	}


	public void setSourceVoucherType(String sourceVoucherType) {
		this.sourceVoucherType = sourceVoucherType;
	}


	public String getNarration() {
		return narration;
	}


	public void setNarration(String narration) {
		this.narration = narration;
	}


	public Double getDrAmount() {
		return drAmount;
	}


	public void setDrAmount(Double drAmount) {
		this.drAmount = drAmount;
	}


	public Double getCrAmount() {
		return crAmount;
	}


	public void setCrAmount(Double crAmount) {
		this.crAmount = crAmount;
	}


	public String getCrAccountName() {
		return crAccountName;
	}


	public void setCrAccountName(String crAccountName) {
		this.crAccountName = crAccountName;
	}


	public String getDrAccountName() {
		return drAccountName;
	}


	public void setDrAccountName(String drAccountName) {
		this.drAccountName = drAccountName;
	}


	public String getBranchCode() {
		return branchCode;
	}


	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}

	@JsonProperty("sourceVoucherDate")
	public java.sql.Date getsourceVoucherDate( ) {
		if(null==this.sourceVoucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.sourceVoucherDate.get() );
		}
	 
		
	}
	
   public void setsourceVoucherDate (java.sql.Date sourceVoucherDateProperty) {
						if(null!=sourceVoucherDateProperty)
		this.sourceVoucherDate.set(sourceVoucherDateProperty.toLocalDate());
	}
   public ObjectProperty<LocalDate> getsourceVoucherDateProperty( ) {
		return sourceVoucherDate;
	}

	public void setsourceVoucherDateProperty(ObjectProperty<LocalDate> sourceVoucherDate) {
		this.sourceVoucherDate = (SimpleObjectProperty<LocalDate>) sourceVoucherDate;
	}
	


@Override
public String toString() {
	return "DayBook [id=" + id + ", voucheNumber=" + voucheNumber + ", voucherDate=" + voucherDate
			+ ", sourceVoucheNumber=" + sourceVoucheNumber + ", sourceVoucherType=" + sourceVoucherType + ", narration="
			+ narration + ", drAmount=" + drAmount + ", crAmount=" + crAmount + ", crAccountName=" + crAccountName
			+ ", drAccountName=" + drAccountName + ", branchCode=" + branchCode + ", userId=" + userId
			+ ", sourceVoucherNumberProperty=" + sourceVoucherNumberProperty + ", sourceVoucherDate="
			+ sourceVoucherDate + ", sourceVoucherTypeProperty=" + sourceVoucherTypeProperty + ", narrationProperty="
			+ narrationProperty + ", crAccountNameProperty=" + crAccountNameProperty + ", drAccountNameProperty="
			+ drAccountNameProperty + ", drAmountProperty=" + drAmountProperty + ", crAmountProperty="
			+ crAmountProperty + "]";
}
 
	
	
	
}
