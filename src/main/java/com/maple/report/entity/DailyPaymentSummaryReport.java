package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DailyPaymentSummaryReport {

	String paymentMode;
	Double totalCash;
	String creditAccount;
	
	
	
	public DailyPaymentSummaryReport() {
		this.paymentModeProperty = new SimpleStringProperty("");
		this.creditAccountProperty = new SimpleStringProperty("");
		this.totalCashProperty = new SimpleDoubleProperty(0.0);

	}
	@JsonIgnore
	private StringProperty paymentModeProperty;
	
	@JsonIgnore
	private StringProperty creditAccountProperty;
	
	@JsonIgnore
	private DoubleProperty totalCashProperty;
	
	
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Double getTotalCash() {
		return totalCash;
	}
	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}
	public String getCreditAccount() {
		return creditAccount;
	}
	public void setCreditAccount(String creditAccount) {
		this.creditAccount = creditAccount;
	}
	public StringProperty getPaymentModeProperty() {
		paymentModeProperty.set(paymentMode);
		return paymentModeProperty;
	}
	public void setPaymentModeProperty(StringProperty paymentModeProperty) {
		this.paymentModeProperty = paymentModeProperty;
	}
	public StringProperty getCreditAccountProperty() {
		creditAccountProperty.set(creditAccount);
		return creditAccountProperty;
	}
	public void setCreditAccountProperty(StringProperty creditAccountProperty) {
		this.creditAccountProperty = creditAccountProperty;
	}
	public DoubleProperty getTotalCashProperty() {
		totalCashProperty.set(totalCash);
		return totalCashProperty;
	}
	public void setTotalCashProperty(DoubleProperty totalCashProperty) {
		this.totalCashProperty = totalCashProperty;
	}
	@Override
	public String toString() {
		return "DailyPaymentSummaryReport [paymentMode=" + paymentMode + ", totalCash=" + totalCash + ", creditAccount="
				+ creditAccount + ", paymentModeProperty=" + paymentModeProperty + ", creditAccountProperty="
				+ creditAccountProperty + ", totalCashProperty=" + totalCashProperty + "]";
	}
	
	
}
