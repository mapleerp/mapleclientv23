package com.maple.report.entity;

import java.math.BigDecimal;

import com.maple.mapleclient.entity.BranchMst;


public class PurchaseSummaryReport {

	private String branchMst;
	private BigDecimal branchPurchaseAmount;
	private BigDecimal companyPurchaseAmount;
	
	
	public BigDecimal getBranchPurchaseAmount() {
		return branchPurchaseAmount;
	}
	public String getBranchMst() {
		return branchMst;
	}
	public void setBranchMst(String branchMst) {
		this.branchMst = branchMst;
	}
	public void setBranchPurchaseAmount(BigDecimal branchPurchaseAmount) {
		this.branchPurchaseAmount = branchPurchaseAmount;
	}
	public BigDecimal getCompanyPurchaseAmount() {
		return companyPurchaseAmount;
	}
	public void setCompanyPurchaseAmount(BigDecimal companyPurchaseAmount) {
		this.companyPurchaseAmount = companyPurchaseAmount;
	}
	@Override
	public String toString() {
		return "PurchaseSummaryReport [branchMst=" + branchMst + ", branchPurchaseAmount=" + branchPurchaseAmount
				+ ", companyPurchaseAmount=" + companyPurchaseAmount + "]";
	}
	
}
