package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FinishedProduct {
	Double actualQty;
	Double planingQty;
	String product;
	Double costPrice;
	Double value;
	String batch;
	Date voucherDate;
	
	
	public FinishedProduct() {
		this.productProperty = new SimpleStringProperty();
		this.dateProperty = new SimpleStringProperty();

		this.batchProperty = new SimpleStringProperty();
		this.actualQtyProperty = new SimpleDoubleProperty();
		this.planingQtyProperty = new SimpleDoubleProperty();
		this.costPriceProperty = new SimpleDoubleProperty();
		this.valueProperty = new SimpleDoubleProperty();
	}

	@JsonIgnore
	private StringProperty productProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private DoubleProperty actualQtyProperty;
	
	@JsonIgnore
	private DoubleProperty planingQtyProperty;
	
	@JsonIgnore
	private DoubleProperty costPriceProperty;
	
	@JsonIgnore
	private DoubleProperty valueProperty;
	
	@JsonIgnore
	private StringProperty dateProperty;

	public Double getActualQty() {
		return actualQty;
	}

	public void setActualQty(Double actualQty) {
		this.actualQty = actualQty;
	}

	public Double getPlaningQty() {
		return planingQty;
	}

	public void setPlaningQty(Double planingQty) {
		this.planingQty = planingQty;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public StringProperty getProductProperty() {
		productProperty.set(product);
		return productProperty;
	}

	public void setProductProperty(StringProperty productProperty) {
		
		
		this.productProperty = productProperty;
	}

	public StringProperty getBatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public DoubleProperty getActualQtyProperty() {
		actualQtyProperty.set(actualQty);
		return actualQtyProperty;
	}

	public void setActualQtyProperty(DoubleProperty actualQtyProperty) {
		this.actualQtyProperty = actualQtyProperty;
	}

	public DoubleProperty getPlaningQtyProperty() {
		planingQtyProperty.set(planingQty);
		return planingQtyProperty;
	}

	public void setPlaningQtyProperty(DoubleProperty planingQtyProperty) {
		this.planingQtyProperty = planingQtyProperty;
	}

	public DoubleProperty getCostPriceProperty() {
		costPriceProperty.set(costPrice);
		return costPriceProperty;
	}

	public void setCostPriceProperty(DoubleProperty costPriceProperty) {
		this.costPriceProperty = costPriceProperty;
	}

	public DoubleProperty getValueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}

	public void setValueProperty(DoubleProperty valueProperty) {
		this.valueProperty = valueProperty;
	}

	
	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public StringProperty getDateProperty() {
		dateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		return dateProperty;
	}

	public void setDateProperty(StringProperty dateProperty) {
		this.dateProperty = dateProperty;
	}

	@Override
	public String toString() {
		return "FinishedProduct [actualQty=" + actualQty + ", planingQty=" + planingQty + ", product=" + product
				+ ", costPrice=" + costPrice + ", value=" + value + ", batch=" + batch + ", voucherDate=" + voucherDate
				+ ", productProperty=" + productProperty + ", batchProperty=" + batchProperty + ", actualQtyProperty="
				+ actualQtyProperty + ", planingQtyProperty=" + planingQtyProperty + ", costPriceProperty="
				+ costPriceProperty + ", valueProperty=" + valueProperty + ", dateProperty=" + dateProperty + "]";
	}


	
	
	
	
	
}
