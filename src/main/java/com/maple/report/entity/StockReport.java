package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockReport {
	String companyName;
	String date;
	Integer slNo;
	String itemName;
	Double qty;
	String branchName;
	String branchAddress;
	String branchPlace;
	String branchPhone;
	String branchGst;
	String branchState;
	Double standardPrice;
	String category;
	String unitName;
	Double totalAmount;
	Date expiryDate;
	String batch;
	
	
	
	
	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public StringProperty getCategoryProperty() {
		if (null != category) {
			categoryProperty.set(category);
		}
		return categoryProperty;
	}

	public void setCategoryProperty(StringProperty categoryProperty) {
		this.categoryProperty = categoryProperty;
	}

	public DoubleProperty getStandardPriceProperty() {
		if (null != standardPrice) {
			standardPriceProperty.set(standardPrice);
		}
		return standardPriceProperty;
	}

	public void setStandardPriceProperty(DoubleProperty standardPriceProperty) {
		this.standardPriceProperty = standardPriceProperty;
	}

	public DoubleProperty getTotalAmountProperty() {
		if (null != totalAmount) {
			totalAmountProperty.set(totalAmount);
		}
		return totalAmountProperty;
	}

	public void setTotalAmountProperty(DoubleProperty totalAmountProperty) {
		this.totalAmountProperty = totalAmountProperty;
	}

	public StringProperty getBranchNameProperty() {
		if (null != branchName) {
			branchNameProperty.set(branchName);
		}
		return branchNameProperty;
	}

	public void setBranchNameProperty(StringProperty branchNameProperty) {
		this.branchNameProperty = branchNameProperty;
	}

	String store;

	public StockReport() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.categoryProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty(0.0);
		this.unitProperty = new SimpleStringProperty("");
		this.branchNameProperty = new SimpleStringProperty("");
		this.standardPriceProperty = new SimpleDoubleProperty(0.0);
		this.totalAmountProperty = new SimpleDoubleProperty(0.0);

		this.totalAmountProperty=new SimpleDoubleProperty(0.0);
		this.storeNameProperty = new SimpleStringProperty("");
	}
	

	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty categoryProperty;
	@JsonIgnore
	private DoubleProperty qtyProperty;

	@JsonIgnore
	private StringProperty unitProperty;

	@JsonIgnore
	private DoubleProperty standardPriceProperty;

	@JsonIgnore
	private StringProperty storeNameProperty;

	@JsonIgnore
	private DoubleProperty totalAmountProperty;

	@JsonIgnore
	private StringProperty branchNameProperty;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Integer getSlNo() {
		return slNo;
	}

	public void setSlNo(Integer slNo) {
		this.slNo = slNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public String getBranchPlace() {
		return branchPlace;
	}

	public void setBranchPlace(String branchPlace) {
		this.branchPlace = branchPlace;
	}

	public String getBranchPhone() {
		return branchPhone;
	}

	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}

	public String getBranchGst() {
		return branchGst;
	}

	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}

	public String getBranchState() {
		return branchState;
	}

	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}

	public Double getStandardPrice() {

		return standardPrice;
	}

	public void setStandardPrice(Double standardPrice) {
		this.standardPrice = standardPrice;
	}

	public StringProperty getItemNameProperty() {
		if (null != itemName) {
			itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getcategoryProperty() {
		if (null != category) {
			categoryProperty.set(category);
		}
		return categoryProperty;
	}

	public void setcategoryProperty(String category) {
		this.category = category;
	}

	public DoubleProperty getQtyProperty() {
		if (null != qty) {
			qtyProperty.set(qty);
		}
		return qtyProperty;
	}

	public void setQtyProperty(DoubleProperty qtyProperty) {

		this.qtyProperty = qtyProperty;
	}

	public DoubleProperty getstandardPriceProperty() {
		if (null != standardPrice) {
			standardPriceProperty.set(standardPrice);
		}
		return standardPriceProperty;
	}

	public void setstandardPriceProperty(Double standardPrice) {

		this.standardPrice = standardPrice;
	}

	public StringProperty getUnitProperty() {
		if (null != unitName) {
			unitProperty.set(unitName);
		}
		return unitProperty;
	}

	public void setUnitProperty(StringProperty unitProperty) {
		this.unitProperty = unitProperty;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public StringProperty getStoreNameProperty() {
		if (null != store) {
			storeNameProperty.set(store);
		}
		return storeNameProperty;
	}

	public void setStoreNameProperty(StringProperty storeNameProperty) {
		this.storeNameProperty = storeNameProperty;
	}

	@Override
	public String toString() {
		return "StockReport [companyName=" + companyName + ", date=" + date + ", slNo=" + slNo + ", itemName="
				+ itemName + ", qty=" + qty + ", branchName=" + branchName + ", branchAddress=" + branchAddress
				+ ", branchPlace=" + branchPlace + ", branchPhone=" + branchPhone + ", branchGst=" + branchGst
				+ ", branchState=" + branchState + ", standardPrice=" + standardPrice + ", category=" + category
				+ ", unitName=" + unitName + ", totalAmount=" + totalAmount + ", store=" + store + ", itemNameProperty="
				+ itemNameProperty + ", categoryProperty=" + categoryProperty + ", qtyProperty=" + qtyProperty
				+ ", unitProperty=" + unitProperty + ", standardPriceProperty=" + standardPriceProperty
				+ ", storeNameProperty=" + storeNameProperty + ", totalAmountProperty=" + totalAmountProperty
				+ ", branchNameProperty=" + branchNameProperty + "]";
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}
	
	

}
