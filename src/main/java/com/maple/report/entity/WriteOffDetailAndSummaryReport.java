package com.maple.report.entity;


import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WriteOffDetailAndSummaryReport {
	
	 String voucherNumber;
	 String date;
	 Double amount;
	 String department;
	 String user;
	 String remarks;
	 String itemName;
	 String itemCode;
	 String itemGroup;
	 String batch;
	 String expiryDate;
	 Double quantity;
	 Double rate;
	
	 
	 public WriteOffDetailAndSummaryReport() {
			this.voucherNumberProperty = new SimpleStringProperty("");
			this.dateProperty = new SimpleStringProperty("");
			this.departmentProperty = new SimpleStringProperty("");
			this.userProperty = new SimpleStringProperty("");
			this.remarksProperty = new SimpleStringProperty("");
			this.itemNameProperty = new SimpleStringProperty("");
			this.itemCodeProperty = new SimpleStringProperty("");
			this.itemGroupProperty = new SimpleStringProperty("");
			this.batchProperty = new SimpleStringProperty("");
			this.expiryDateProperty = new SimpleStringProperty("");
			this.amountProperty = new SimpleDoubleProperty();
			this.quantityProperty = new SimpleDoubleProperty();
			this.rateProperty = new SimpleDoubleProperty();
	 }
	 
	 
	private StringProperty voucherNumberProperty;
	private StringProperty dateProperty;
	private StringProperty departmentProperty;
	private StringProperty userProperty;
	private StringProperty remarksProperty;
	private StringProperty itemNameProperty;
	private StringProperty itemCodeProperty;
	private StringProperty itemGroupProperty;
	private StringProperty batchProperty;
	private StringProperty expiryDateProperty;
	private DoubleProperty amountProperty;
	private DoubleProperty quantityProperty;
	private DoubleProperty rateProperty;
	
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemGroup() {
		return itemGroup;
	}
	public void setItemGroup(String itemGroup) {
		this.itemGroup = itemGroup;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public StringProperty getVoucherNumberProperty() {
		if(null != voucherNumber) {
			voucherNumberProperty.set(voucherNumber);
		}
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	public StringProperty getDateProperty() {
		if(null != date) {
			dateProperty.set(date);
		}
		return dateProperty;
	}
	public void setDateProperty(StringProperty dateProperty) {
		this.dateProperty = dateProperty;
	}
	public StringProperty getDepartmentProperty() {
		if(null != department) {
			departmentProperty.set(department);
		}
		return departmentProperty;
	}
	public void setDepartmentProperty(StringProperty departmentProperty) {
		this.departmentProperty = departmentProperty;
	}
	public StringProperty getUserProperty() {
		if(null != user) {
			userProperty.set(user);
		}
		return userProperty;
	}
	public void setUserProperty(StringProperty userProperty) {
		this.userProperty = userProperty;
	}
	public StringProperty getRemarksProperty() {
		if(null != remarks) {
			remarksProperty.set(remarks);
		}
		return remarksProperty;
	}
	public void setRemarksProperty(StringProperty remarksProperty) {
		this.remarksProperty = remarksProperty;
	}
	public StringProperty getItemNameProperty() {
		if(null != itemName) {
			itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public StringProperty getItemCodeProperty() {
		if(null != itemCode) {
			itemCodeProperty.set(itemCode);
		}
		return itemCodeProperty;
	}
	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}
	public StringProperty getItemGroupProperty() {
		if(null != itemGroup) {
			itemGroupProperty.set(itemGroup);
		}
		return itemGroupProperty;
	}
	public void setItemGroupProperty(StringProperty itemGroupProperty) {
		this.itemGroupProperty = itemGroupProperty;
	}
	public StringProperty getBatchProperty() {
		if(null != batch) {
			batchProperty.set(batch);
		}
		return batchProperty;
	}
	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}
	public StringProperty getExpiryDateProperty() {
		if(null != expiryDate) {
			expiryDateProperty.set(expiryDate);
		}
		return expiryDateProperty;
	}
	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}
	public DoubleProperty getAmountProperty() {
		if(null != amount) {
			amountProperty.set(amount);
		}
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	public DoubleProperty getQuantityProperty() {
		if(null != quantity) {
			quantityProperty.set(quantity);
		}
		return quantityProperty;
	}
	public void setQuantityProperty(DoubleProperty quantityProperty) {
		this.quantityProperty = quantityProperty;
	}
	public DoubleProperty getRateProperty() {
		if(null != rate) {
			rateProperty.set(rate);
		}
		return rateProperty;
	}
	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}
	public Double getAmount() {
		if(null != amount) {
			amountProperty.set(amount);
		}
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDepartment() {
		if(null != department) {
			departmentProperty.set(department);
		}
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getUser() {
		if(null != user) {
			userProperty.set(user);
		}
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getRemarks() {
		if(null != remarks) {
			remarksProperty.set(remarks);
		}
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Override
	public String toString() {
		return "WriteOffDetailAndSummaryReport [voucherNumber=" + voucherNumber + ", date=" + date + ", amount="
				+ amount + ", department=" + department + ", user=" + user + ", remarks=" + remarks + ", itemName="
				+ itemName + ", itemCode=" + itemCode + ", itemGroup=" + itemGroup + ", batch=" + batch
				+ ", expiryDate=" + expiryDate + ", quantity=" + quantity + ", rate=" + rate
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", dateProperty=" + dateProperty
				+ ", departmentProperty=" + departmentProperty + ", userProperty=" + userProperty + ", remarksProperty="
				+ remarksProperty + ", itemNameProperty=" + itemNameProperty + ", itemCodeProperty=" + itemCodeProperty
				+ ", itemGroupProperty=" + itemGroupProperty + ", batchProperty=" + batchProperty
				+ ", expiryDateProperty=" + expiryDateProperty + ", amountProperty=" + amountProperty
				+ ", quantityProperty=" + quantityProperty + ", rateProperty=" + rateProperty + "]";
	}
	
	

}
