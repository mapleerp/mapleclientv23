package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SettledAmountDtlReport {
	String voucherNumber;
	Date voucherDate;
	Double amount;
	
	@JsonProperty
	StringProperty voucherNumberProperty;
	
	@JsonProperty
	StringProperty voucherDateProperty;
	
	@JsonProperty
	DoubleProperty amountProperty;
	
	
	
	public SettledAmountDtlReport() {
	
		this.voucherNumberProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();
		this.amountProperty =new SimpleDoubleProperty();
	}
	

	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyy-MM-dd"));
		return voucherDateProperty;
	}
	
	public void setamountProperty(Double amount) {
		this.amount = amount;
	}
	public DoubleProperty getamountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	
	
	public Date getVoucherDate() {
		return voucherDate;
	}


	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}


	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "SettledAmountDtlReport [voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", amount="
				+ amount + "]";
	}
	

}
