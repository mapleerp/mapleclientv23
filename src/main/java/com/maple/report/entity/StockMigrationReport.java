package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StockMigrationReport {
	
	String id;
	String itemId;
	
	
	
	String itemName;
	String barCode;
	String batch;
	Double qtyIn;
	Double qtyOut;
	Double rate;
    String particulars;
	
	
	
	public StockMigrationReport() {
		
		this.itemNameProperty = new SimpleStringProperty("");
		this.barCodeProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.qtyInProperty = new SimpleDoubleProperty(0.0);
		this.qtyOutProperty = new SimpleDoubleProperty(0.0);
		this.rateProperty = new SimpleDoubleProperty(0.0);
		
	}
	
	@JsonIgnore
	StringProperty itemNameProperty;
	
	@JsonIgnore
	StringProperty barCodeProperty;
	
	@JsonIgnore
	StringProperty batchProperty;
	
	@JsonIgnore
	DoubleProperty qtyInProperty;
	
	@JsonIgnore
	DoubleProperty qtyOutProperty;
	
	@JsonIgnore
	DoubleProperty rateProperty;



	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public StringProperty getItemNameProperty() {
		
		if(null!=itemName) {
		
		itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getBarCodeProperty() {
		
		if(null!=barCode) {
		
		barCodeProperty.set(barCode);
		}
		return barCodeProperty;
	}

	public void setBarCodeProperty(StringProperty barCodeProperty) {
		this.barCodeProperty = barCodeProperty;
	}

	public StringProperty getBatchProperty() {
		if(null!=batch) {
		
		batchProperty.set(batch);
		}
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}




	public DoubleProperty getRateProperty() {
		if(null!=rate) {
		
		rateProperty.set(rate);
		}
		return rateProperty;
	}

	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Double getQtyIn() {
		return qtyIn;
	}

	public void setQtyIn(Double qtyIn) {
		this.qtyIn = qtyIn;
	}

	public Double getQtyOut() {
		return qtyOut;
	}

	public void setQtyOut(Double qtyOut) {
		this.qtyOut = qtyOut;
	}

	public DoubleProperty getQtyInProperty() {
		if(null!=qtyIn) {
		
		qtyInProperty.set(qtyIn);
		}
		return qtyInProperty;
	}

	public void setQtyInProperty(DoubleProperty qtyInProperty) {
		this.qtyInProperty = qtyInProperty;
	}

	public DoubleProperty getQtyOutProperty() {
		
		if(null!=qtyOut) {
		
		qtyOutProperty.set(qtyOut);
		}
		return qtyOutProperty;
	}

	public void setQtyOutProperty(DoubleProperty qtyOutProperty) {
		this.qtyOutProperty = qtyOutProperty;
	}

	public String getParticulars() {
		return particulars;
	}

	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}

	@Override
	public String toString() {
		return "StockMigrationReport [id=" + id + ", itemId=" + itemId + ", itemName=" + itemName + ", barCode="
				+ barCode + ", batch=" + batch + ", qtyIn=" + qtyIn + ", qtyOut=" + qtyOut + ", rate=" + rate
				+ ", particulars=" + particulars + ", itemNameProperty=" + itemNameProperty + ", barCodeProperty="
				+ barCodeProperty + ", batchProperty=" + batchProperty + ", qtyInProperty=" + qtyInProperty
				+ ", qtyOutProperty=" + qtyOutProperty + ", rateProperty=" + rateProperty + "]";
	}

	
	

	

}
