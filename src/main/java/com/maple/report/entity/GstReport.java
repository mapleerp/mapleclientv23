package com.maple.report.entity;

import java.util.Date;

public class GstReport {
	
	
	private Date invoiceDate;
	
	private String invoiceNumber;
	
	private String supplierName;
	
	private String supplierTin;
	
	private String branch;
	
	private String branchActivity;
	
	private Double gstPercent;
	
	private Double TotalPurchaseExcludingGst;
	
	private Double gstAmount;
	
	private Double totalPurchase;
	
	private Double excemptedPurchase;

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierTin() {
		return supplierTin;
	}

	public void setSupplierTin(String supplierTin) {
		this.supplierTin = supplierTin;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBranchActivity() {
		return branchActivity;
	}

	public void setBranchActivity(String branchActivity) {
		this.branchActivity = branchActivity;
	}

	public Double getGstPercent() {
		return gstPercent;
	}

	public void setGstPercent(Double gstPercent) {
		this.gstPercent = gstPercent;
	}

	public Double getTotalPurchaseExcludingGst() {
		return TotalPurchaseExcludingGst;
	}

	public void setTotalPurchaseExcludingGst(Double totalPurchaseExcludingGst) {
		TotalPurchaseExcludingGst = totalPurchaseExcludingGst;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getTotalPurchase() {
		return totalPurchase;
	}

	public void setTotalPurchase(Double totalPurchase) {
		this.totalPurchase = totalPurchase;
	}

	public Double getExcemptedPurchase() {
		return excemptedPurchase;
	}

	public void setExcemptedPurchase(Double excemptedPurchase) {
		this.excemptedPurchase = excemptedPurchase;
	}

	@Override
	public String toString() {
		return "GstReport [invoiceDate=" + invoiceDate + ", invoiceNumber=" + invoiceNumber + ", supplierName="
				+ supplierName + ", supplierTin=" + supplierTin + ", branch=" + branch + ", branchActivity="
				+ branchActivity + ", gstPercent=" + gstPercent + ", TotalPurchaseExcludingGst="
				+ TotalPurchaseExcludingGst + ", gstAmount=" + gstAmount + ", totalPurchase=" + totalPurchase
				+ ", excemptedPurchase=" + excemptedPurchase + "]";
	}

}
