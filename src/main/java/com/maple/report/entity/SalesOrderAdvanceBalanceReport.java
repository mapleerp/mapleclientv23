package com.maple.report.entity;

import java.util.Date;

public class SalesOrderAdvanceBalanceReport {
	
	 String companyName;
     String rdate;
	 public String getRdate() {
		return rdate;
	}
	public void setRdate(String rdate) {
		this.rdate = rdate;
	}
	String voucherNumber;
	 String branchName;
	 String branchAddress1;
	 String branchAddress2;
	 String branchEmail;
	 String branchGst;
	 String branchPhNo;	 
	Date currentDate;
	String receiptMode;
	 String voucherDate;
	 
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public String getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(String voucherDate) {
		this.voucherDate = voucherDate;
	}
	String orderNo;
	String invoiceNo;
	String customerName;
	Double orderInvoiceTotal;
	Double advance;

	Double credit;
	Double creditCard;
	Double debitCard;
	Double online;
	Double paytm;
	Double sodexo;
	Double balance;
	Double netTotal;
	
	
	
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress1() {
		return branchAddress1;
	}
	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}
	public String getBranchAddress2() {
		return branchAddress2;
	}
	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	public String getBranchPhNo() {
		return branchPhNo;
	}
	public void setBranchPhNo(String branchPhNo) {
		this.branchPhNo = branchPhNo;
	}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getOrderInvoiceTotal() {
		return orderInvoiceTotal;
	}
	public void setOrderInvoiceTotal(Double orderInvoiceTotal) {
		this.orderInvoiceTotal = orderInvoiceTotal;
	}
	public Double getAdvance() {
		return advance;
	}
	public void setAdvance(Double advance) {
		this.advance = advance;
	}
	
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(Double creditCard) {
		this.creditCard = creditCard;
	}
	public Double getDebitCard() {
		return debitCard;
	}
	public void setDebitCard(Double debitCard) {
		this.debitCard = debitCard;
	}
	public Double getOnline() {
		return online;
	}
	public void setOnline(Double online) {
		this.online = online;
	}
	public Double getPaytm() {
		return paytm;
	}
	public void setPaytm(Double paytm) {
		this.paytm = paytm;
	}
	public Double getSodexo() {
		return sodexo;
	}
	public void setSodexo(Double sodexo) {
		this.sodexo = sodexo;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public Double getNetTotal() {
		return netTotal;
	}
	public void setNetTotal(Double netTotal) {
		this.netTotal = netTotal;
	}
	
	
	
	
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@Override
	public String toString() {
		return "SalesOrderAdvanceBalanceReport [companyName=" + companyName + ", rdate=" + rdate + ", voucherNumber="
				+ voucherNumber + ", branchName=" + branchName + ", branchAddress1=" + branchAddress1
				+ ", branchAddress2=" + branchAddress2 + ", branchEmail=" + branchEmail + ", branchGst=" + branchGst
				+ ", branchPhNo=" + branchPhNo + ", currentDate=" + currentDate + ", voucherDate=" + voucherDate
				+ ", orderNo=" + orderNo + ", invoiceNo=" + invoiceNo + ", customerName=" + customerName
				+ ", orderInvoiceTotal=" + orderInvoiceTotal + ", advance=" + advance + ", credit=" + credit
				+ ", creditCard=" + creditCard + ", debitCard=" + debitCard + ", online=" + online + ", paytm=" + paytm
				+ ", sodexo=" + sodexo + ", balance=" + balance + ", netTotal=" + netTotal + "]";
	}
	
	
	
	

	

}
