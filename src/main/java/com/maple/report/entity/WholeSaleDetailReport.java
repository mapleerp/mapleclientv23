package com.maple.report.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WholeSaleDetailReport implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String invoiceDate;
	String voucherNumber;
	String customerName;
	String itemName;
	String groupName;
	String batchCode;
	String itemCode;
	String expiryDate;
	Double quantity;
	Double rate;
	Double taxRate;
	Double saleValue;
	Double cost;
	Double costValue;
	Double totalInvoiceAmount;
	Double gst;
	Double beforeGST;
	String salesMan;
	String userName;
	String customerPO;
	String tinNumber;
	
	
	
	public Double getTotalInvoiceAmount() {
		return totalInvoiceAmount;
	}


	public void setTotalInvoiceAmount(Double totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}


	public Double getGst() {
		return gst;
	}


	public void setGst(Double gst) {
		this.gst = gst;
	}


	public Double getBeforeGST() {
		return beforeGST;
	}


	public void setBeforeGST(Double beforeGST) {
		this.beforeGST = beforeGST;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getCustomerPO() {
		return customerPO;
	}


	public void setCustomerPO(String customerPO) {
		this.customerPO = customerPO;
	}


	public String getTinNumber() {
		return tinNumber;
	}


	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}


	@JsonIgnore
	private StringProperty invoiceDateProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty groupNameProperty;
	
	@JsonIgnore
	private StringProperty batchCodeProperty;
	
	@JsonIgnore
	private StringProperty itemCodeProperty;
	
	@JsonIgnore
	private StringProperty expiryDateProperty;
	
	@JsonIgnore
	private DoubleProperty quantityProperty;
	
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	@JsonIgnore
	private DoubleProperty taxRateProperty;
	
	@JsonIgnore
	private DoubleProperty saleValueProperty;
	
	@JsonIgnore
	private DoubleProperty costProperty;
	
	@JsonIgnore
	private DoubleProperty costValueProperty;
	
	@JsonIgnore
	private StringProperty salesManProperty;
	
	@JsonIgnore
	private StringProperty userNameProperty;
	
	@JsonIgnore
	private StringProperty customerPOProperty;
	
	@JsonIgnore
	private StringProperty tinNumberProperty;
	
	@JsonIgnore
	private DoubleProperty totalInvoiceAmountProperty;
	
	@JsonIgnore
	private DoubleProperty gstProperty;
	
	@JsonIgnore
	private DoubleProperty beforeGSTProperty;
	
	
	public WholeSaleDetailReport() {
		
		this.invoiceDateProperty = new SimpleStringProperty("");
		this.voucherNumberProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.groupNameProperty = new SimpleStringProperty("");
		this.batchCodeProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.expiryDateProperty = new SimpleStringProperty("");
		this.quantityProperty = new SimpleDoubleProperty(0.0);
		this.rateProperty = new SimpleDoubleProperty(0.0);
		this.taxRateProperty = new SimpleDoubleProperty(0.0);
		this.saleValueProperty = new SimpleDoubleProperty(0.0);
		this.costProperty=new SimpleDoubleProperty(0.0);
		this.costValueProperty=new SimpleDoubleProperty(0.0);
		this.salesManProperty = new SimpleStringProperty("");
		this.customerPOProperty=new SimpleStringProperty("");
		this.tinNumberProperty=new SimpleStringProperty("");
		this.beforeGSTProperty=new SimpleDoubleProperty(0.0);
		this.gstProperty=new SimpleDoubleProperty(0.0);
		this.totalInvoiceAmountProperty=new SimpleDoubleProperty(0.0);
		this.userNameProperty=new SimpleStringProperty("");

	}


	public String getInvoiceDate() {
		return invoiceDate;
	}


	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}


	public StringProperty getUserNameProperty() {
		userNameProperty.set(userName);
		return userNameProperty;
	}


	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}


	public StringProperty getCustomerPOProperty() {
		if(null != customerPO) {
		customerPOProperty.set(customerPO);
		}
		return customerPOProperty;
	}


	public void setCustomerPOProperty(StringProperty customerPOProperty) {
		this.customerPOProperty = customerPOProperty;
	}


	public StringProperty getTinNumberProperty() {
		tinNumberProperty.set(tinNumber);
		return tinNumberProperty;
	}


	public void setTinNumberProperty(StringProperty tinNumberProperty) {
		this.tinNumberProperty = tinNumberProperty;
	}


	public DoubleProperty getTotalInvoiceAmountProperty() {
		totalInvoiceAmountProperty.set(totalInvoiceAmount);
		return totalInvoiceAmountProperty;
	}


	public void setTotalInvoiceAmountProperty(DoubleProperty totalInvoiceAmountProperty) {
		this.totalInvoiceAmountProperty = totalInvoiceAmountProperty;
	}


	public DoubleProperty getGstProperty() {
		gstProperty.set(gst);
		return gstProperty;
	}


	public void setGstProperty(DoubleProperty gstProperty) {
		this.gstProperty = gstProperty;
	}


	public DoubleProperty getBeforeGSTProperty() {
		beforeGSTProperty.set(beforeGST);
		return beforeGSTProperty;
	}


	public void setBeforeGSTProperty(DoubleProperty beforeGSTProperty) {
		this.beforeGSTProperty = beforeGSTProperty;
	}


	public String getVoucherNumber() {
		return voucherNumber;
	}


	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getBatchCode() {
		return batchCode;
	}


	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}


	public Double getQuantity() {
		return quantity;
	}


	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}


	public Double getTaxRate() {
		return taxRate;
	}


	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}


	public Double getSaleValue() {
		return saleValue;
	}


	public void setSaleValue(Double saleValue) {
		this.saleValue = saleValue;
	}


	public Double getCost() {
		return cost;
	}


	public void setCost(Double cost) {
		this.cost = cost;
	}


	public Double getCostValue() {
		return costValue;
	}


	public void setCostValue(Double costValue) {
		this.costValue = costValue;
	}


	public String getSalesMan() {
		return salesMan;
	}


	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}


	@JsonIgnore
	public StringProperty getInvoiceDateProperty() {
		invoiceDateProperty.set(invoiceDate);
		return invoiceDateProperty;
	}


	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}

	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}


	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}

	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}


	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}

	@JsonIgnore
	public StringProperty getItemNameProperty() {
		if(null != itemName) {
		itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}


	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	@JsonIgnore
	public StringProperty getGroupNameProperty() {
		groupNameProperty.set(groupName);
		return groupNameProperty;
	}


	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}

	@JsonIgnore
	public StringProperty getBatchCodeProperty() {
		batchCodeProperty.set(batchCode);
		return batchCodeProperty;
	}


	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}

	@JsonIgnore
	public StringProperty getItemCodeProperty() {
		itemCodeProperty.set(itemCode);
		return itemCodeProperty;
	}


	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	@JsonIgnore
	public StringProperty getExpiryDateProperty() {
		expiryDateProperty.set(expiryDate);
		return expiryDateProperty;
	}


	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}

	@JsonIgnore
	public DoubleProperty getQuantityProperty() {
		quantityProperty.set(quantity);
		return quantityProperty;
	}


	public void setQuantityProperty(DoubleProperty quantityProperty) {
		this.quantityProperty = quantityProperty;
	}

	@JsonIgnore
	public DoubleProperty getRateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}


	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	@JsonIgnore
	public DoubleProperty getTaxRateProperty() {
		taxRateProperty.set(taxRate);
		return taxRateProperty;
	}


	public void setTaxRateProperty(DoubleProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}

	@JsonIgnore
	public DoubleProperty getSaleValueProperty() {
		saleValueProperty.set(saleValue);
		return saleValueProperty;
	}


	public void setSaleValueProperty(DoubleProperty saleValueProperty) {
		this.saleValueProperty = saleValueProperty;
	}

	@JsonIgnore
	public DoubleProperty getCostProperty() {
		if(null!=cost) {
		costProperty.set(cost);
		}
		return costProperty;
	}


	public void setCostProperty(DoubleProperty costProperty) {
		this.costProperty = costProperty;
	}

	@JsonIgnore
	public DoubleProperty getCostValueProperty() {
		if(null!=costValue) {
		costValueProperty.set(costValue);
		}
		return costValueProperty;
	}


	public void setCostValueProperty(DoubleProperty costValueProperty) {
		this.costValueProperty = costValueProperty;
	}

	@JsonIgnore
	public StringProperty getSalesManProperty() {
		salesManProperty.set(salesMan);
		return salesManProperty;
	}


	public void setSalesManProperty(StringProperty salesManProperty) {
		this.salesManProperty = salesManProperty;
	}


	@Override
	public String toString() {
		return "WholeSaleDetailReport [invoiceDate=" + invoiceDate + ", voucherNumber=" + voucherNumber
				+ ", customerName=" + customerName + ", itemName=" + itemName + ", groupName=" + groupName
				+ ", batchCode=" + batchCode + ", itemCode=" + itemCode + ", expiryDate=" + expiryDate + ", quantity="
				+ quantity + ", rate=" + rate + ", taxRate=" + taxRate + ", saleValue=" + saleValue + ", cost=" + cost
				+ ", costValue=" + costValue + ", salesMan=" + salesMan + ", invoiceDateProperty=" + invoiceDateProperty
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", customerNameProperty=" + customerNameProperty
				+ ", itemNameProperty=" + itemNameProperty + ", groupNameProperty=" + groupNameProperty
				+ ", batchCodeProperty=" + batchCodeProperty + ", itemCodeProperty=" + itemCodeProperty
				+ ", expiryDateProperty=" + expiryDateProperty + ", quantityProperty=" + quantityProperty
				+ ", rateProperty=" + rateProperty + ", taxRateProperty=" + taxRateProperty + ", saleValueProperty="
				+ saleValueProperty + ", costProperty=" + costProperty + ", costValueProperty=" + costValueProperty
				+ ", salesManProperty=" + salesManProperty + "]";
	}
	
	

}
