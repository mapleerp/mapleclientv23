package com.maple.report.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyDailySalesTransactionReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	String id;
	String date;
	String voucherNumber;
	String customerName;
	
	String tinNumber;
	String salesMan;
	private Double invoiceDiscount;
	private Double invoiceAmount;
	
	@JsonIgnore
	private StringProperty dateProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private StringProperty tinNumberProperty;
	
	@JsonIgnore
	private StringProperty salesManProperty;
	
	
	
	@JsonIgnore
	private DoubleProperty invoiceAmountProperty;
	@JsonIgnore
	private DoubleProperty invoiceDiscountProperty;
	
	
	

	public PharmacyDailySalesTransactionReport() {
		
		this.dateProperty = new SimpleStringProperty("");
		this.voucherNumberProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.tinNumberProperty = new SimpleStringProperty("");
		this.salesManProperty = new SimpleStringProperty("");
		this.invoiceAmountProperty = new SimpleDoubleProperty(0.0);
		this.invoiceDiscountProperty = new SimpleDoubleProperty(0.0);

	}




	public String getId() {
		return id;
	}




	public void setId(String id) {
		this.id = id;
	}




	public String getDate() {
		return date;
	}




	public void setDate(String date) {
		this.date = date;
	}




	public String getVoucherNumber() {
		return voucherNumber;
	}




	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}




	public String getCustomerName() {
		return customerName;
	}




	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}




	public String getTinNumber() {
		return tinNumber;
	}




	public void setTinNumber(String tinNumber) {
		this.tinNumber = tinNumber;
	}




	public String getSalesMan() {
		return salesMan;
	}




	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}




	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}




	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}




	public Double getInvoiceAmount() {
		return invoiceAmount;
	}




	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}



	@JsonIgnore
	public StringProperty getDateProperty() {
		dateProperty.set(date);
		return dateProperty;
	}



	public void setDateProperty(StringProperty dateProperty) {
		this.dateProperty = dateProperty;
	}



	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}



	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}



	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}




	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}



	@JsonIgnore
	public StringProperty getTinNumberProperty() {
		tinNumberProperty.set(tinNumber);
		return tinNumberProperty;
	}




	public void setTinNumberProperty(StringProperty tinNumberProperty) {
		this.tinNumberProperty = tinNumberProperty;
	}


	@JsonIgnore

	public StringProperty getSalesManProperty() {
		salesManProperty.set(salesMan);
		return salesManProperty;
	}




	public void setSalesManProperty(StringProperty salesManProperty) {
		this.salesManProperty = salesManProperty;
	}



	@JsonIgnore
	public DoubleProperty getInvoiceAmountProperty() {
		invoiceAmountProperty.set(invoiceAmount);
		return invoiceAmountProperty;
	}




	public void setInvoiceAmountProperty(DoubleProperty invoiceAmountProperty) {
		this.invoiceAmountProperty = invoiceAmountProperty;
	}


	@JsonIgnore

	public DoubleProperty getInvoiceDiscountProperty() {
		invoiceDiscountProperty.set(invoiceDiscount);
		return invoiceDiscountProperty;
	}




	public void setInvoiceDiscountProperty(DoubleProperty invoiceDiscountProperty) {
		this.invoiceDiscountProperty = invoiceDiscountProperty;
	}




	@Override
	public String toString() {
		return "PharmacyDailySalesTransactionReport [id=" + id + ", date=" + date + ", voucherNumber=" + voucherNumber
				+ ", customerName=" + customerName + ", tinNumber=" + tinNumber + ", salesMan=" + salesMan
				+ ", invoiceDiscount=" + invoiceDiscount + ", invoiceAmount=" + invoiceAmount + ", dateProperty="
				+ dateProperty + ", voucherNumberProperty=" + voucherNumberProperty + ", customerNameProperty="
				+ customerNameProperty + ", tinNumberProperty=" + tinNumberProperty + ", salesManProperty="
				+ salesManProperty + ", invoiceAmountProperty=" + invoiceAmountProperty + ", invoiceDiscountProperty="
				+ invoiceDiscountProperty + "]";
	}
	
	
}
