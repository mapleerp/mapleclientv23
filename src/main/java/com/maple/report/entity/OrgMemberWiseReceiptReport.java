package com.maple.report.entity;

import java.util.Date;

public class OrgMemberWiseReceiptReport {
	
	
	
	String id;
	String companyName;
	String orgName;
	String memberId;
	String familyName;
	String memberName;
	String account;
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	Double totalAmount;
	Date startDate;
	Date endDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "OrgMemberWiseReceiptReport [id=" + id + ", companyName=" + companyName + ", orgName=" + orgName
				+ ", memberId=" + memberId + ", familyName=" + familyName + ", memberName=" + memberName + ", account="
				+ account + ", totalAmount=" + totalAmount + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}
	
	

}
