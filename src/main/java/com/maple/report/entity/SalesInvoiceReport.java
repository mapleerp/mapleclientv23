package com.maple.report.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ibm.icu.text.StringPrep;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesInvoiceReport {

	String id;
	String branchName;
	String branchAddress1;
	String branchAddress2;
	String branchPlace;
	String branchTelNo;
	String branchGst;
	String branchState;
	String branchEmail;
	String bankName;
	String accountNumber;
	String bankBranch;
	String ifsc;
	Date voucherDate;
	String voucherNumber;
	String customerName;
	String customerPlace;
	String customerPhone;
	String customerPhone2;
	String customerState;
	String customerAddress;
	String customerGst;
	String itemName;
	String hsnCode;
	Double rate;
	Double qty;
	String unitName;
	String discount;
	Double amount;
	Double invoiceDiscount;
	Double taxRate;
	Double sgstRate;
	Double cgstRate;
	Double sgstAmount;
	Double cgstAmount;
	Double igstRate;
	Double igstAmount;
	Double taxableAmount;
	Double cessRate;
	Double cessAmount;

	String companyName;
	String addressLine1;
	String addressLine2;
	String companyGst;
	String branchWebsite;
	String itemCode;
	Date expiryDate;

	String localCustomerName;
	String localCustomerPlace;
	String localCustomerPhone;
	String localCustomerState;
	String localCustomerAddres;
	String localCustomerPhone2;
	String discountPercent;
	Double invAmt;
	Double standrdPrice;
	String salesMode;
	Double mrp;
	String batch;

	String warranty;
	
	Double listPrice;


	
	@JsonIgnore
	private StringProperty itemNameProperty;

	@JsonIgnore
	private StringProperty voucherProperty;

	@JsonIgnore
	private StringProperty voucherDateProperty;

	@JsonIgnore
	private DoubleProperty amountProperty;

	@JsonIgnore
	private DoubleProperty qtyProperty;

	@JsonIgnore
	StringProperty batchProperty;
	private DoubleProperty rateProperty;

	private DoubleProperty taxAmountProperty;

	@JsonIgnore
	private StringProperty customerNameProperty;

	public SalesInvoiceReport() {
		this.itemNameProperty = new SimpleStringProperty("");
		this.voucherProperty = new SimpleStringProperty("");
		this.voucherDateProperty = new SimpleStringProperty("");
		this.amountProperty = new SimpleDoubleProperty();

		this.qtyProperty = new SimpleDoubleProperty();
		this.batchProperty = new SimpleStringProperty("");
		this.rateProperty = new SimpleDoubleProperty();
		this.taxAmountProperty = new SimpleDoubleProperty();

		this.customerNameProperty = new SimpleStringProperty("");

	}

	public Double getStandrdPrice() {
		return standrdPrice;
	}

	public void setStandrdPrice(Double standrdPrice) {
		this.standrdPrice = standrdPrice;
	}

	public StringProperty getItemNameProperty() {

		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getVoucherProperty() {
		voucherProperty.set(voucherNumber);
		return voucherProperty;
	}

	public void setVoucherProperty(StringProperty voucherProperty) {
		this.voucherProperty = voucherProperty;
	}

	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setbatchProperty(String batch) {
		this.batch = batch;
	}

	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "yyyy-MM-dd"));
		return voucherDateProperty;
	}

	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}

	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public DoubleProperty getTaxProperty() {
		taxAmountProperty.set(igstAmount);
		return taxAmountProperty;
	}

	public void settaxAmountProperty(Double igstAmount) {
		this.igstAmount = igstAmount;
	}

	public DoubleProperty getrateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}

	public void setrateProperty(Double rate) {
		this.rate = rate;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public String getSalesMode() {
		return salesMode;
	}

	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}

	public Double getCessRate() {
		return cessRate;
	}

	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}

	public Double getCessAmount() {
		return cessAmount;
	}

	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}

	public Double getInvAmt() {
		return invAmt;
	}

	public void setInvAmt(Double invAmt) {
		this.invAmt = invAmt;
	}

	public String getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(String discountPercent) {
		this.discountPercent = discountPercent;
	}

	public Double getTaxableAmount() {
		return taxableAmount;
	}

	public void setTaxableAmount(Double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchAddress1() {
		return branchAddress1;
	}

	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}

	public String getBranchAddress2() {
		return branchAddress2;
	}

	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}

	public String getBranchPlace() {
		return branchPlace;
	}

	public void setBranchPlace(String branchPlace) {
		this.branchPlace = branchPlace;
	}

	public String getBranchTelNo() {
		return branchTelNo;
	}

	public void setBranchTelNo(String branchTelNo) {
		this.branchTelNo = branchTelNo;
	}

	public String getBranchGst() {
		return branchGst;
	}

	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}

	public String getBranchState() {
		return branchState;
	}

	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}

	public String getBranchEmail() {
		return branchEmail;
	}

	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPlace() {
		return customerPlace;
	}

	public void setCustomerPlace(String customerPlace) {
		this.customerPlace = customerPlace;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getCustomerGst() {
		return customerGst;
	}

	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public Double getInvoiceDiscount() {
		return invoiceDiscount;
	}

	public void setInvoiceDiscount(Double invoiceDiscount) {
		this.invoiceDiscount = invoiceDiscount;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getSgstRate() {
		return sgstRate;
	}

	public void setSgstRate(Double sgstRate) {
		this.sgstRate = sgstRate;
	}

	public Double getCgstRate() {
		return cgstRate;
	}

	public void setCgstRate(Double cgstRate) {
		this.cgstRate = cgstRate;
	}

	public Double getSgstAmount() {
		return sgstAmount;
	}

	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}

	public Double getCgstAmount() {
		return cgstAmount;
	}

	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public Double getIgstRate() {
		return igstRate;
	}

	public void setIgstRate(Double igstRate) {
		this.igstRate = igstRate;
	}

	public Double getIgstAmount() {
		return igstAmount;
	}

	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCompanyGst() {
		return companyGst;
	}

	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getBranchWebsite() {
		return branchWebsite;
	}

	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}

	public String getLocalCustomerName() {
		return localCustomerName;
	}

	public void setLocalCustomerName(String localCustomerName) {
		this.localCustomerName = localCustomerName;
	}

	public String getLocalCustomerPlace() {
		return localCustomerPlace;
	}

	public void setLocalCustomerPlace(String localCustomerPlace) {
		this.localCustomerPlace = localCustomerPlace;
	}

	public String getLocalCustomerPhone() {
		return localCustomerPhone;
	}

	public void setLocalCustomerPhone(String localCustomerPhone) {
		this.localCustomerPhone = localCustomerPhone;
	}

	public String getLocalCustomerState() {
		return localCustomerState;
	}

	public void setLocalCustomerState(String localCustomerState) {
		this.localCustomerState = localCustomerState;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getLocalCustomerAddres() {
		return localCustomerAddres;
	}

	public void setLocalCustomerAddres(String localCustomerAddres) {
		this.localCustomerAddres = localCustomerAddres;
	}

	public String getLocalCustomerPhone2() {
		return localCustomerPhone2;
	}

	public void setLocalCustomerPhone2(String localCustomerPhone2) {
		this.localCustomerPhone2 = localCustomerPhone2;
	}

	public String getCustomerPhone2() {
		return customerPhone2;
	}

	public void setCustomerPhone2(String customerPhone2) {
		this.customerPhone2 = customerPhone2;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getWarranty() {
		return warranty;
	}

	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}

	public StringProperty getCustomerNameProperty() {

		if (null != customerName) {
			customerNameProperty.set(customerName);

		}

		return customerNameProperty;
	}

	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}

	
	
	
	public Double getListPrice() {
		return listPrice;
	}

	public void setListPrice(Double listPrice) {
		this.listPrice = listPrice;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "SalesInvoiceReport [id=" + id + ", branchName=" + branchName + ", branchAddress1=" + branchAddress1
				+ ", branchAddress2=" + branchAddress2 + ", branchPlace=" + branchPlace + ", branchTelNo=" + branchTelNo
				+ ", branchGst=" + branchGst + ", branchState=" + branchState + ", branchEmail=" + branchEmail
				+ ", bankName=" + bankName + ", accountNumber=" + accountNumber + ", bankBranch=" + bankBranch
				+ ", ifsc=" + ifsc + ", voucherDate=" + voucherDate + ", voucherNumber=" + voucherNumber
				+ ", customerName=" + customerName + ", customerPlace=" + customerPlace + ", customerPhone="
				+ customerPhone + ", customerPhone2=" + customerPhone2 + ", customerState=" + customerState
				+ ", customerAddress=" + customerAddress + ", customerGst=" + customerGst + ", itemName=" + itemName
				+ ", hsnCode=" + hsnCode + ", rate=" + rate + ", qty=" + qty + ", unitName=" + unitName + ", discount="
				+ discount + ", amount=" + amount + ", invoiceDiscount=" + invoiceDiscount + ", taxRate=" + taxRate
				+ ", sgstRate=" + sgstRate + ", cgstRate=" + cgstRate + ", sgstAmount=" + sgstAmount + ", cgstAmount="
				+ cgstAmount + ", igstRate=" + igstRate + ", igstAmount=" + igstAmount + ", taxableAmount="
				+ taxableAmount + ", cessRate=" + cessRate + ", cessAmount=" + cessAmount + ", companyName="
				+ companyName + ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", companyGst="
				+ companyGst + ", branchWebsite=" + branchWebsite + ", itemCode=" + itemCode + ", expiryDate="
				+ expiryDate + ", localCustomerName=" + localCustomerName + ", localCustomerPlace=" + localCustomerPlace
				+ ", localCustomerPhone=" + localCustomerPhone + ", localCustomerState=" + localCustomerState
				+ ", localCustomerAddres=" + localCustomerAddres + ", localCustomerPhone2=" + localCustomerPhone2
				+ ", discountPercent=" + discountPercent + ", invAmt=" + invAmt + ", standrdPrice=" + standrdPrice
				+ ", salesMode=" + salesMode + ", mrp=" + mrp + ", batch=" + batch + ", warranty=" + warranty
				+ ", listPrice=" + listPrice + ", itemNameProperty=" + itemNameProperty + ", voucherProperty="
				+ voucherProperty + ", voucherDateProperty=" + voucherDateProperty + ", amountProperty="
				+ amountProperty + ", qtyProperty=" + qtyProperty + ", batchProperty=" + batchProperty
				+ ", rateProperty=" + rateProperty + ", taxAmountProperty=" + taxAmountProperty
				+ ", customerNameProperty=" + customerNameProperty + "]";
	}

	
}
