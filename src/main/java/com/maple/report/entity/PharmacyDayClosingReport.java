package com.maple.report.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class PharmacyDayClosingReport implements Serializable {
	private static final long serialVersionUID = 1L;
	String Id;
	private String branchCode;
	private Double totalcashSale;
	private Double totalcardSale;
	private Double totalcreditsale;
	private Double physicalCash;
	private Double totalbanksales;
	private Double totalinsurancesales;
	private Double grandtotal;
	private Double diffenceamount;
	
	@JsonIgnore
	private StringProperty branchProperty;

	@JsonIgnore
	private DoubleProperty totalCashSalesProperty;

	@JsonIgnore
	private DoubleProperty totalCardSalesProperty;

	@JsonIgnore
	private DoubleProperty totalCreditSalesProperty;

	@JsonIgnore
	private DoubleProperty totalBankSalesProperty;

	@JsonIgnore
	private DoubleProperty totalInsuranceSalesProperty;

	@JsonIgnore
	private DoubleProperty grandTotalProperty;

	@JsonIgnore
	private DoubleProperty physicalCashProperty;
	@JsonIgnore
	private DoubleProperty diffenceamountProperty;

	public PharmacyDayClosingReport() {
		
		
		this.branchProperty = new SimpleStringProperty("");
		this.totalCashSalesProperty = new SimpleDoubleProperty(0.0);
		this.totalCardSalesProperty = new SimpleDoubleProperty(0.0);
		this.totalCreditSalesProperty = new SimpleDoubleProperty(0.0);
		this.totalBankSalesProperty = new SimpleDoubleProperty(0.0);
		this.totalInsuranceSalesProperty=new SimpleDoubleProperty(0.0);
		this.grandTotalProperty=new SimpleDoubleProperty(0.0);
		this.physicalCashProperty=new SimpleDoubleProperty(0.0);
		this.diffenceamountProperty=new SimpleDoubleProperty(0.0);
	}

	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Double getTotalcashSale() {
		return totalcashSale;
	}
	public void setTotalcashSale(Double totalcashSale) {
		this.totalcashSale = totalcashSale;
	}
	public Double getTotalcardSale() {
		return totalcardSale;
	}
	public void setTotalcardSale(Double totalcardSale) {
		this.totalcardSale = totalcardSale;
	}
	public Double getTotalcreditsale() {
		return totalcreditsale;
	}
	public void setTotalcreditsale(Double totalcreditsale) {
		this.totalcreditsale = totalcreditsale;
	}
	public Double getPhysicalCash() {
		return physicalCash;
	}
	public void setPhysicalCash(Double physicalCash) {
		this.physicalCash = physicalCash;
	}
	public Double getTotalbanksales() {
		return totalbanksales;
	}
	public void setTotalbanksales(Double totalbanksales) {
		this.totalbanksales = totalbanksales;
	}
	public Double getTotalinsurancesales() {
		return totalinsurancesales;
	}
	public void setTotalinsurancesales(Double totalinsurancesales) {
		this.totalinsurancesales = totalinsurancesales;
	}
	public Double getGrandtotal() {
		return grandtotal;
	}
	public void setGrandtotal(Double grandtotal) {
		this.grandtotal = grandtotal;
	}
	public Double getDiffenceamount() {
		return diffenceamount;
	}
	public void setDiffenceamount(Double diffenceamount) {
		this.diffenceamount = diffenceamount;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public StringProperty getBranchProperty() {
		if(null != branchCode) {
			branchProperty.set(branchCode);
			}
		return branchProperty;
	}

	public void setBranchProperty(StringProperty branchProperty) {
		this.branchProperty = branchProperty;
	}

	public DoubleProperty getTotalCashSalesProperty() {
		if(null != totalcashSale) {
			totalCashSalesProperty.set(totalcashSale);
			}
		return totalCashSalesProperty;
	}

	public void setTotalCashSalesProperty(DoubleProperty totalCashSalesProperty) {
		this.totalCashSalesProperty = totalCashSalesProperty;
	}

	public DoubleProperty getTotalCardSalesProperty() {
		if(null != totalcardSale) {
			totalCardSalesProperty.set(totalcardSale);
			}
		return totalCardSalesProperty;
	}

	public void setTotalCardSalesProperty(DoubleProperty totalCardSalesProperty) {
		this.totalCardSalesProperty = totalCardSalesProperty;
	}

	public DoubleProperty getTotalCreditSalesProperty() {
		if(null != totalcreditsale) {
			totalCreditSalesProperty.set(totalcreditsale);
			}
		return totalCreditSalesProperty;
	}

	public void setTotalCreditSalesProperty(DoubleProperty totalCreditSalesProperty) {
		this.totalCreditSalesProperty = totalCreditSalesProperty;
	}

	public DoubleProperty getTotalBankSalesProperty() {
		if(null != totalbanksales) {
			totalBankSalesProperty.set(totalbanksales);
			}
		return totalBankSalesProperty;
	}

	public void setTotalBankSalesProperty(DoubleProperty totalBankSalesProperty) {
		this.totalBankSalesProperty = totalBankSalesProperty;
	}

	public DoubleProperty getTotalInsuranceSalesProperty() {
		if(null != totalinsurancesales) {
			totalInsuranceSalesProperty.set(totalinsurancesales);
			}
		return totalInsuranceSalesProperty;
	}

	public void setTotalInsuranceSalesProperty(DoubleProperty totalInsuranceSalesProperty) {
		this.totalInsuranceSalesProperty = totalInsuranceSalesProperty;
	}

	public DoubleProperty getGrandTotalProperty() {
		if(null != grandtotal) {
			grandTotalProperty.set(grandtotal);
			}
		return grandTotalProperty;
	}

	public void setGrandTotalProperty(DoubleProperty grandTotalProperty) {
		this.grandTotalProperty = grandTotalProperty;
	}

	public DoubleProperty getPhysicalCashProperty() {
		if(null != physicalCash) {
			physicalCashProperty.set(physicalCash);
			}
		return physicalCashProperty;
	}

	public void setPhysicalCashProperty(DoubleProperty physicalCashProperty) {
		this.physicalCashProperty = physicalCashProperty;
	}

	public DoubleProperty getDiffenceamountProperty() {
		if(null != diffenceamount) {
			diffenceamountProperty.set(diffenceamount);
			}
		return diffenceamountProperty;
	}

	public void setDiffenceamountProperty(DoubleProperty diffenceamountProperty) {
		this.diffenceamountProperty = diffenceamountProperty;
	}

	@Override
	public String toString() {
		return "PharmacyDayClosingReport [Id=" + Id + ", branchCode=" + branchCode + ", totalcashSale=" + totalcashSale
				+ ", totalcardSale=" + totalcardSale + ", totalcreditsale=" + totalcreditsale + ", physicalCash="
				+ physicalCash + ", totalbanksales=" + totalbanksales + ", totalinsurancesales=" + totalinsurancesales
				+ ", grandtotal=" + grandtotal + ", diffenceamount=" + diffenceamount + ", branchProperty="
				+ branchProperty + ", totalCashSalesProperty=" + totalCashSalesProperty + ", totalCardSalesProperty="
				+ totalCardSalesProperty + ", totalCreditSalesProperty=" + totalCreditSalesProperty
				+ ", totalBankSalesProperty=" + totalBankSalesProperty + ", totalInsuranceSalesProperty="
				+ totalInsuranceSalesProperty + ", grandTotalProperty=" + grandTotalProperty + ", physicalCashProperty="
				+ physicalCashProperty + ", diffenceamountProperty=" + diffenceamountProperty + "]";
	}
	
	
	
}
