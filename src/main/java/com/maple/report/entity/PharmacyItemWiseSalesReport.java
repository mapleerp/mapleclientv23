package com.maple.report.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyItemWiseSalesReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String id;
	String invoiceDate;
	String itemName;
	String customerName;
	Double qty;
	Double unitPrice;
	String batch;
	
	@JsonIgnore
	private StringProperty invoiceDateProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	@JsonIgnore
	private DoubleProperty unitPriceProperty;
	
public PharmacyItemWiseSalesReport() {
		
		this.invoiceDateProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.customerNameProperty = new SimpleStringProperty("");
		this.qtyProperty = new SimpleDoubleProperty(0.0);
		this.unitPriceProperty = new SimpleDoubleProperty(0.0);

	}

public String getId() {
	return id;
}

public Double getQty() {
	return qty;
}

public void setQty(Double qty) {
	this.qty = qty;
}

public DoubleProperty getQtyProperty() {
	qtyProperty.set(qty);
	return qtyProperty;
}

public void setQtyProperty(DoubleProperty qtyProperty) {
	this.qtyProperty = qtyProperty;
}

public void setId(String id) {
	this.id = id;
}

public String getInvoiceDate() {
	return invoiceDate;
}

public void setInvoiceDate(String invoiceDate) {
	this.invoiceDate = invoiceDate;
}

public String getItemName() {
	return itemName;
}

public void setItemName(String itemName) {
	this.itemName = itemName;
}

public String getCustomerName() {
	return customerName;
}

public void setCustomerName(String customerName) {
	this.customerName = customerName;
}



public Double getUnitPrice() {
	return unitPrice;
}

public void setUnitPrice(Double unitPrice) {
	this.unitPrice = unitPrice;
}
@JsonIgnore
public StringProperty getInvoiceDateProperty() {
	invoiceDateProperty.set(invoiceDate);
	return invoiceDateProperty;
}

public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
	this.invoiceDateProperty = invoiceDateProperty;
}
@JsonIgnore
public StringProperty getItemNameProperty() {
	itemNameProperty.set(itemName);
	return itemNameProperty;
}

public void setItemNameProperty(StringProperty itemNameProperty) {
	this.itemNameProperty = itemNameProperty;
}
@JsonIgnore
public StringProperty getCustomerNameProperty() {
	customerNameProperty.set(customerName);
	return customerNameProperty;
}

public void setCustomerNameProperty(StringProperty customerNameProperty) {
	this.customerNameProperty = customerNameProperty;
}

@JsonIgnore
public DoubleProperty getUnitPriceProperty() {
	unitPriceProperty.set(unitPrice);
	return unitPriceProperty;
}

public void setUnitPriceProperty(DoubleProperty unitPriceProperty) {
	this.unitPriceProperty = unitPriceProperty;
}



public String getBatch() {
	return batch;
}

public void setBatch(String batch) {
	this.batch = batch;
}

@Override
public String toString() {
	return "PharmacyItemWiseSalesReport [id=" + id + ", invoiceDate=" + invoiceDate + ", itemName=" + itemName
			+ ", customerName=" + customerName + ", qty=" + qty + ", unitPrice=" + unitPrice + ", batch=" + batch
			+ ", invoiceDateProperty=" + invoiceDateProperty + ", itemNameProperty=" + itemNameProperty
			+ ", customerNameProperty=" + customerNameProperty + ", qtyProperty=" + qtyProperty + ", unitPriceProperty="
			+ unitPriceProperty + "]";
}


}
