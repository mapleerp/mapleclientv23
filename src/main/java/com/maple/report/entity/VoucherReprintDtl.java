package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class VoucherReprintDtl {

	
	
	String branchName;
	String voucherNumber;
	String customerName;
	Double amount;
	String itemName;
	Double qty;
	Double taxRate;
	Double mrp;
	Double cessRate;
    String unit;
	
	@JsonIgnore
	private StringProperty customerNameProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;

	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private DoubleProperty taxRateProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	@JsonIgnore
	private DoubleProperty mrpProperty;

	@JsonIgnore
	private DoubleProperty cessRateProperty;
	@JsonIgnore
	private StringProperty unitProperty;
	@JsonIgnore
	private StringProperty branchNameProperty;

public VoucherReprintDtl() {
	
	
	    
	
	this.branchNameProperty = new SimpleStringProperty();
	this.customerNameProperty =new SimpleStringProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.itemNameProperty = new SimpleStringProperty();
		this.taxRateProperty = new SimpleDoubleProperty();
        this.qtyProperty = new SimpleDoubleProperty();
		this.mrpProperty = new SimpleDoubleProperty();
		 this.cessRateProperty = new SimpleDoubleProperty();
			this.unitProperty = new SimpleStringProperty();
	}

	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getMrp() {
		return mrp;
	}
	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}
	public Double getCessRate() {
		return cessRate;
	}
	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}
	
	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	


	public StringProperty getCustomerNameProperty() {
		customerNameProperty.set(customerName);
		return customerNameProperty;
	}


	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}


	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}


	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}


	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}


	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}


	public DoubleProperty getTaxRateProperty() {
		taxRateProperty.set(taxRate);
		return taxRateProperty;
	}


	public void setTaxRateProperty(DoubleProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}


	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}


	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}


	public DoubleProperty getMrpProperty() {
		mrpProperty.set(mrp);
		return mrpProperty;
	}


	public void setMrpProperty(DoubleProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}


	public DoubleProperty getCessRateProperty() {
		//cessRateProperty.set(cessRate);
		return cessRateProperty;
	}


	public void setCessRateProperty(DoubleProperty cessRateProperty) {
		this.cessRateProperty = cessRateProperty;
	}




	public StringProperty getUnitProperty() {
		unitProperty.set(unit);
		return unitProperty;
	}


	public void setUnitProperty(StringProperty unitProperty) {
		this.unitProperty = unitProperty;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public StringProperty getBranchNameProperty() {
		branchNameProperty.set(branchName);
		return branchNameProperty;
	}


	public void setBranchNameProperty(StringProperty branchNameProperty) {
		this.branchNameProperty = branchNameProperty;
	}


	@Override
	public String toString() {
		return "VoucherReprintDtl [branchName=" + branchName + ", voucherNumber=" + voucherNumber + ", customerName="
				+ customerName + ", amount=" + amount + ", itemName=" + itemName + ", qty=" + qty + ", taxRate="
				+ taxRate + ", mrp=" + mrp + ", cessRate=" + cessRate + ", unit=" + unit + ", customerNameProperty="
				+ customerNameProperty + ", amountProperty=" + amountProperty + ", itemNameProperty=" + itemNameProperty
				+ ", taxRateProperty=" + taxRateProperty + ", qtyProperty=" + qtyProperty + ", mrpProperty="
				+ mrpProperty + ", cessRateProperty=" + cessRateProperty + ", unitProperty=" + unitProperty
				+ ", branchNameProperty=" + branchNameProperty + "]";
	}



	
}
