package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ReceiptModeReport {
	
	String receiptMode;
	Double amount;
	
	Double saleOrderAmount;
	Double prevSaleOrderAmount;
	
	@JsonIgnore
	StringProperty receiptModeProperty;
	
	@JsonIgnore
	DoubleProperty amountProperty;
	
	
	public ReceiptModeReport() {
		this.receiptModeProperty = new SimpleStringProperty();
		this.amountProperty =new SimpleDoubleProperty();
	}
	
	@JsonIgnore
	public StringProperty getreceiptModeProperty() {
		receiptModeProperty.set(receiptMode);
		return receiptModeProperty;
	}
	

	public void setreceiptModeProperty(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	
	@JsonIgnore
	public DoubleProperty getamountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	

	public void setamountProperty(Double amount) {
		this.amount = amount;
	}
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	
	

	public Double getSaleOrderAmount() {
		return saleOrderAmount;
	}

	public void setSaleOrderAmount(Double saleOrderAmount) {
		this.saleOrderAmount = saleOrderAmount;
	}

	public Double getPrevSaleOrderAmount() {
		return prevSaleOrderAmount;
	}

	public void setPrevSaleOrderAmount(Double prevSaleOrderAmount) {
		this.prevSaleOrderAmount = prevSaleOrderAmount;
	}

	public StringProperty getReceiptModeProperty() {
		return receiptModeProperty;
	}

	public void setReceiptModeProperty(StringProperty receiptModeProperty) {
		this.receiptModeProperty = receiptModeProperty;
	}

	public DoubleProperty getAmountProperty() {
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	@Override
	public String toString() {
		return "ReceiptModeReport [receiptMode=" + receiptMode + ", amount=" + amount + ", saleOrderAmount="
				+ saleOrderAmount + ", prevSaleOrderAmount=" + prevSaleOrderAmount + ", receiptModeProperty="
				+ receiptModeProperty + ", amountProperty=" + amountProperty + "]";
	}
	
	
	

}
