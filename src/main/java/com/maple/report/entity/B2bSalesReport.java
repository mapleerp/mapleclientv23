package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class B2bSalesReport {
	
	
	String date;
	String customerName;
	String branchGst;
	String gstIn;
	String salesMan;
	String billNo;
	String voucherNo;
	Double paytm;
	Double advance;
	Double cash;
	Double credit;
	Double cardPayment;
	Double netTotal;
	String branchCode;
	String branchName;
	String branchAddress;
	String branchPhone;
	String companyName;
	String branchEmail;
	String branchWebsite;
	Date voucherDate;
	Double taxRate;
	Double taxableValue;
	String saBillNo;
	Double total;
	String customerGst;
	String svoucherDate;
	String salesMode;

	Double cessAmount;
	Double cgstAmount;
	Double sgstAmount;
	Double taxAmount;
	Double igstAmount;
	Double amount;
	Double taxableAmount;
	@JsonIgnore
	private StringProperty salesModeProperty;
	
	@JsonIgnore
	private DoubleProperty taxRateProperty;
	
	@JsonIgnore
	private DoubleProperty taxableValueProperty;
	
	@JsonIgnore
	private DoubleProperty cgstAmountProperty;
	
	@JsonIgnore
	private DoubleProperty sgstAmountProperty;
	
	@JsonIgnore
	private DoubleProperty cessRateProperty;
	
	@JsonIgnore
	private DoubleProperty igstAmountProperty;
	
	@JsonIgnore
	private DoubleProperty totalAmountProperty;
	
	@JsonIgnore
	private DoubleProperty taxAmountProperty;
	
	
	
	
	
	public B2bSalesReport() {
		this.salesModeProperty = new SimpleStringProperty();
		this.taxRateProperty = new SimpleDoubleProperty();
		this.taxableValueProperty = new SimpleDoubleProperty();
		this.cgstAmountProperty = new SimpleDoubleProperty();
		this.sgstAmountProperty =new SimpleDoubleProperty();
		this.cessRateProperty = new SimpleDoubleProperty();
		this.igstAmountProperty = new SimpleDoubleProperty();
		this.totalAmountProperty = new SimpleDoubleProperty();
		this.taxAmountProperty = new SimpleDoubleProperty();
	}

	public Double getCessAmount() {
		return cessAmount;
	}


	public void setCessAmount(Double cessAmount) {
		this.cessAmount = cessAmount;
	}


	public Double getCgstAmount() {
		return cgstAmount;
	}


	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}


	public Double getSgstAmount() {
		return sgstAmount;
	}


	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}


	public Double getTaxAmount() {
		return taxAmount;
	}


	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}


	public Double getIgstAmount() {
		return igstAmount;
	}


	public void setIgstAmount(Double igstAmount) {
		this.igstAmount = igstAmount;
	}


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public Double getTaxableAmount() {
		return taxableAmount;
	}


	public void setTaxableAmount(Double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}


	public void setSalesModeProperty(StringProperty salesModeProperty) {
		this.salesModeProperty = salesModeProperty;
	}


	public void setTaxRateProperty(DoubleProperty taxRateProperty) {
		this.taxRateProperty = taxRateProperty;
	}

	
	
	public String getSalesMode() {
		return salesMode;
	}


	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}


	public StringProperty getSalesModeProperty() {
		salesModeProperty.set(salesMode);
		return salesModeProperty;
	}


	public void setSalesModeProperty(String salesMode) {
		this.salesMode = salesMode;
	}


	public DoubleProperty getTaxRateProperty() {
		if(null != taxRate)
		{
			taxRateProperty.set(taxRate);
		}
		return taxRateProperty;
	}


	public void setTaxRateProperty(Double taxRate) {
		this.taxRate = taxRate;
	}


	public DoubleProperty getTaxableValueProperty() {
		if(null != taxableAmount)
		{
			taxableValueProperty.set(taxableAmount);
		}
		return taxableValueProperty;
	}


	public void setTaxableValueProperty(Double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}


	public DoubleProperty getCgstAmountProperty() {
		if(null != cgstAmount)
		{
			cgstAmountProperty.set(cgstAmount);
		}
		return cgstAmountProperty;
	}


	public void setCgstAmountProperty(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}


	public DoubleProperty getSgstAmountProperty() {
		if(null != sgstAmount)
		{
			sgstAmountProperty.set(sgstAmount);
		}
		return sgstAmountProperty;
	}


	public void setSgstAmountProperty(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}


	public DoubleProperty getCessRateProperty() {
		if(null != cessAmount)
		{
			cessRateProperty.set(cessAmount);
		}
		return cessRateProperty;
	}


	public void setCessRateProperty(Double cessAmount) {
		this.cessAmount = cessAmount;
	}


	public DoubleProperty getIgstAmountProperty() {
		if(null != igstAmount)
		{
			igstAmountProperty.set(igstAmount);
		}
		return igstAmountProperty;
	}


	public void setIgstAmountProperty(Double igstAmount) {
		this.igstAmount = igstAmount;
	}


	public DoubleProperty getTotalAmountProperty() {
		if(null != amount)
		{
			totalAmountProperty.set(amount);
		}
		return totalAmountProperty;
	}


	public void setTotalAmountProperty(Double amount) {
		this.amount = amount;
	}


	public DoubleProperty getTaxAmountProperty() {
		if(null != taxAmount)
		{
			taxAmountProperty.set(taxAmount);
		}
		return taxAmountProperty;
	}


	public void setTaxAmountProperty(Double taxAmount) {
		this.taxAmount = taxAmount;
	}


	public String getSvoucherDate() {
		return svoucherDate;
	}
	public void setSvoucherDate(String svoucherDate) {
		this.svoucherDate = svoucherDate;
	}
	public String getCustomerGst() {
		return customerGst;
	}
	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getTaxableValue() {
		return taxableValue;
	}
	public void setTaxableValue(Double taxableValue) {
		this.taxableValue = taxableValue;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public Double getAdvance() {
		return advance;
	}
	public void setAdvance(Double advance) {
		this.advance = advance;
	}
	public Double getCash() {
		return cash;
	}
	public void setCash(Double cash) {
		this.cash = cash;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getCardPayment() {
		return cardPayment;
	}
	public void setCardPayment(Double cardPayment) {
		this.cardPayment = cardPayment;
	}
	public Double getNetTotal() {
		return netTotal;
	}
	public void setNetTotal(Double netTotal) {
		this.netTotal = netTotal;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public Double getPaytm() {
		return paytm;
	}
	public void setPaytm(Double paytm) {
		this.paytm = paytm;
	}
	
	
	
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	
	
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	
	
	public String getGstIn() {
		return gstIn;
	}
	public void setGstIn(String gstIn) {
		this.gstIn = gstIn;
	}
	
	
	public String getSaBillNo() {
		return saBillNo;
	}
	public void setSaBillNo(String saBillNo) {
		this.saBillNo = saBillNo;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	@Override
	public String toString() {
		return "B2bSalesReport [date=" + date + ", customerName=" + customerName + ", branchGst=" + branchGst
				+ ", gstIn=" + gstIn + ", salesMan=" + salesMan + ", billNo=" + billNo + ", voucherNo=" + voucherNo
				+ ", paytm=" + paytm + ", advance=" + advance + ", cash=" + cash + ", credit=" + credit
				+ ", cardPayment=" + cardPayment + ", netTotal=" + netTotal + ", branchCode=" + branchCode
				+ ", branchName=" + branchName + ", branchAddress=" + branchAddress + ", branchPhone=" + branchPhone
				+ ", companyName=" + companyName + ", branchEmail=" + branchEmail + ", branchWebsite=" + branchWebsite
				+ ", saBillNo=" + saBillNo + ", total=" + total + "]";
	}
	


}
