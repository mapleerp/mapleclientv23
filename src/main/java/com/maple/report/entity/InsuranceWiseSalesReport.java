package com.maple.report.entity;



import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;

import javafx.beans.property.SimpleDoubleProperty;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class InsuranceWiseSalesReport implements Serializable{
	
	private static  final long serialVersionUID= 1L;
	
	private String id;
	String  invoiceDate;
	String vocherNumber;
	String customerName;
	String patientId;
	String insComapany;
	String insuCard;
	String policyType;
	Double creditAmount;
	Double cashPaid;
	Double cardPaid;
	Double insuAmount;
	Double invoiceAmount;
	String usrName;
	
	public InsuranceWiseSalesReport()
	{
		this.invoiceDateProperty = new  SimpleStringProperty();
		this.vocherNumberProperty = new SimpleStringProperty();
		this.customerNameProperty = new SimpleStringProperty();
		this.patientIdProperty = new SimpleStringProperty();
		this.insComapanyProperty = new SimpleStringProperty();
		this.insuCardProperty = new SimpleStringProperty();
		this.policyTypeProperty = new SimpleStringProperty();
		this.creditAmountProperty = new SimpleDoubleProperty();
		this.cashPaidProperty = new SimpleDoubleProperty();
		this.cardPaidProperty = new SimpleDoubleProperty();
		this.insuAmountProperty = new SimpleDoubleProperty();
		this.invoiceAmountProperty = new SimpleDoubleProperty();
		
		
	}
	
	@JsonIgnore
	StringProperty vocherNumberProperty;
	@JsonIgnore
	StringProperty customerNameProperty;
	@JsonIgnore
	StringProperty patientIdProperty;
	@JsonIgnore
	StringProperty insComapanyProperty;
	@JsonIgnore
	StringProperty insuCardProperty;
	@JsonIgnore
	StringProperty policyTypeProperty;
	@JsonIgnore
	DoubleProperty creditAmountProperty;
	@JsonIgnore
	DoubleProperty cashPaidProperty;
	@JsonIgnore
	DoubleProperty cardPaidProperty;
	@JsonIgnore
	DoubleProperty insuAmountProperty;
	@JsonIgnore
	DoubleProperty invoiceAmountProperty;
	@JsonIgnore
	StringProperty invoiceDateProperty;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getVocherNumber() {
		return vocherNumber;
	}
	public void setVocherNumber(String vocherNumber) {
		this.vocherNumber = vocherNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getInsComapany() {
		return insComapany;
	}
	public void setInsComapany(String insComapany) {
		this.insComapany = insComapany;
	}
	public String getInsuCard() {
		return insuCard;
	}
	public void setInsuCard(String insuCard) {
		this.insuCard = insuCard;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public Double getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Double creditAmount) {
		this.creditAmount = creditAmount;
	}
	public Double getCashPaid() {
		return cashPaid;
	}
	public void setCashPaid(Double cashPaid) {
		this.cashPaid = cashPaid;
	}
	public Double getCardPaid() {
		return cardPaid;
	}
	public void setCardPaid(Double cardPaid) {
		this.cardPaid = cardPaid;
	}
	public Double getInsuAmount() {
		return insuAmount;
	}
	public void setInsuAmount(Double insuAmount) {
		this.insuAmount = insuAmount;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getUsrName() {
		return usrName;
	}
	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}
	@JsonIgnore
	public StringProperty getVocherNumberProperty() {
		if(null != vocherNumber)
		{
		vocherNumberProperty.set(vocherNumber);
		}
		return vocherNumberProperty;
	}
	public void setVocherNumberProperty(StringProperty vocherNumberProperty) {
		this.vocherNumberProperty = vocherNumberProperty;
	}
	@JsonIgnore
	public StringProperty getCustomerNameProperty() {
		if(null != customerName)
		{
		customerNameProperty.set(customerName);
		}
		return customerNameProperty;
		
	}
	public void setCustomerNameProperty(StringProperty customerNameProperty) {
		this.customerNameProperty = customerNameProperty;
	}
	@JsonIgnore
	public StringProperty getPatientIdProperty() {
		if(null != patientId)
		{
		patientIdProperty.set(patientId);
		}
		return patientIdProperty;
	}
	public void setPatientIdProperty(StringProperty patientIdProperty) {
		this.patientIdProperty = patientIdProperty;
	}
	@JsonIgnore
	public StringProperty getInsComapanyProperty() {
		if(null != insComapany)
		{
		insComapanyProperty.set(insComapany);
		}
		return insComapanyProperty;
	}
	public void setInsComapanyProperty(StringProperty insComapanyProperty) {
		this.insComapanyProperty = insComapanyProperty;
	}
	@JsonIgnore
	public StringProperty getInsuCardProperty() {
		if(null != insuCard)
		{
		insuCardProperty.set(insuCard);
		}
		return insuCardProperty;
	}
	public void setInsuCardProperty(StringProperty insuCardProperty) {
		this.insuCardProperty = insuCardProperty;
	}
	@JsonIgnore
	public StringProperty getPolicyTypeProperty() {
		if(null != policyType)
		{
		policyTypeProperty.set(policyType);
		}
		return policyTypeProperty;
	}
	public void setPolicyTypeProperty(StringProperty policyTypeProperty) {
		this.policyTypeProperty = policyTypeProperty;
	}
	@JsonIgnore
	public DoubleProperty getCreditAmountProperty() {
		if(null != creditAmount)
		{
		creditAmountProperty.set(creditAmount);
		}
		return creditAmountProperty;
	}
	public void setCreditAmountProperty(DoubleProperty creditAmountProperty) {
		this.creditAmountProperty = creditAmountProperty;
	}
	@JsonIgnore
	public DoubleProperty getCashPaidProperty() {
		if(null != cashPaid)
		{
		cashPaidProperty.set(cashPaid);
		}
		return cashPaidProperty;
	}
	public void setCashPaidProperty(DoubleProperty cashPaidProperty) {
		this.cashPaidProperty = cashPaidProperty;
	}
	@JsonIgnore
	public DoubleProperty getCardPaidProperty() {
		if(null != cardPaid)
		{
		cardPaidProperty.set(cardPaid);
		}
		return cardPaidProperty;
	}
	public void setCardPaidProperty(DoubleProperty cardPaidProperty) {
		this.cardPaidProperty = cardPaidProperty;
	}
	@JsonIgnore
	public DoubleProperty getInsuAmountProperty() {
		if(null != insuAmount)
		{
		insuAmountProperty.set(insuAmount);
		}
		return insuAmountProperty;
	}
	public void setInsuAmountProperty(DoubleProperty insuAmountProperty) {
		this.insuAmountProperty = insuAmountProperty;
	}
	@JsonIgnore
	public DoubleProperty getInvoiceAmountProperty() {
		if(null != invoiceAmount)
		{
		invoiceAmountProperty.set(invoiceAmount);
		}
		return invoiceAmountProperty;
	}
	public void setInvoiceAmountProperty(DoubleProperty invoiceAmountProperty) {
		this.invoiceAmountProperty = invoiceAmountProperty;
	}
	@JsonIgnore
	public StringProperty getInvoiceDateProperty() {
		if(null != invoiceDate)
		{
		invoiceDateProperty.set(invoiceDate);
		}
		return invoiceDateProperty;
	}
	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}
	@Override
	public String toString() {
		return "InsuranceWiseSalesReport [id=" + id + ", invoiceDate=" + invoiceDate + ", vocherNumber=" + vocherNumber
				+ ", customerName=" + customerName + ", patientId=" + patientId + ", insComapany=" + insComapany
				+ ", insuCard=" + insuCard + ", policyType=" + policyType + ", creditAmount=" + creditAmount
				+ ", cashPaid=" + cashPaid + ", cardPaid=" + cardPaid + ", insuAmount=" + insuAmount
				+ ", invoiceAmount=" + invoiceAmount + ", usrName=" + usrName + ", vocherNumberProperty="
				+ vocherNumberProperty + ", customerNameProperty=" + customerNameProperty + ", patientIdProperty="
				+ patientIdProperty + ", insComapanyProperty=" + insComapanyProperty + ", insuCardProperty="
				+ insuCardProperty + ", policyTypeProperty=" + policyTypeProperty + ", creditAmountProperty="
				+ creditAmountProperty + ", cashPaidProperty=" + cashPaidProperty + ", cardPaidProperty="
				+ cardPaidProperty + ", insuAmountProperty=" + insuAmountProperty + ", invoiceAmountProperty="
				+ invoiceAmountProperty + ", invoiceDateProperty=" + invoiceDateProperty + "]";
	}
	
	
	
}