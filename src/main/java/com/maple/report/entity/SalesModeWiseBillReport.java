package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SalesModeWiseBillReport {
	
	String branchCode;
	Long minVoucherNumericNo;
	Long maxVoucherNumericNo;
	String salesMode;
	Date voucherDate;
	int count;
	
	
	public SalesModeWiseBillReport() {
		this.branchCodeProperty = new SimpleStringProperty("");
		this.salesModeProperty = new SimpleStringProperty("");

		this.minNoProperty = new SimpleStringProperty("");
		this.countProperty = new SimpleStringProperty("");

		this.maxNoProperty = new SimpleStringProperty("");

	}
	@JsonIgnore
	private StringProperty branchCodeProperty;
	
	@JsonIgnore
	private StringProperty salesModeProperty;
	
	@JsonIgnore
	private StringProperty minNoProperty;
	
	@JsonIgnore
	private StringProperty maxNoProperty;
	
	@JsonIgnore
	private StringProperty countProperty;
	
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Long getMinVoucherNumericNo() {
		return minVoucherNumericNo;
	}
	public void setMinVoucherNumericNo(Long minVoucherNumericNo) {
		this.minVoucherNumericNo = minVoucherNumericNo;
	}
	public Long getMaxVoucherNumericNo() {
		return maxVoucherNumericNo;
	}
	public void setMaxVoucherNumericNo(Long maxVoucherNumericNo) {
		this.maxVoucherNumericNo = maxVoucherNumericNo;
	}
	public String getSalesMode() {
		return salesMode;
	}
	public void setSalesMode(String salesMode) {
		this.salesMode = salesMode;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public StringProperty getBranchCodeProperty() {
		branchCodeProperty.set(branchCode);
		return branchCodeProperty;
	}
	public void setBranchCodeProperty(StringProperty branchCodeProperty) {
		this.branchCodeProperty = branchCodeProperty;
	}
	public StringProperty getSalesModeProperty() {
		salesModeProperty.set(salesMode);
		return salesModeProperty;
	}
	public void setSalesModeProperty(StringProperty salesModeProperty) {
		this.salesModeProperty = salesModeProperty;
	}
	public StringProperty getMinNoProperty() {
		minNoProperty.set(minVoucherNumericNo+"");
		return minNoProperty;
	}
	public void setMinNoProperty(StringProperty minNoProperty) {
		this.minNoProperty = minNoProperty;
	}
	public StringProperty getMaxNoProperty() {
		maxNoProperty.set(maxVoucherNumericNo+"");
		return maxNoProperty;
	}
	public void setMaxNoProperty(StringProperty maxNoProperty) {
		this.maxNoProperty = maxNoProperty;
	}
	public StringProperty getCountProperty() {
		countProperty.set(count+"");
		return countProperty;
	}
	public void setCountProperty(StringProperty countProperty) {
		this.countProperty = countProperty;
	}
	@Override
	public String toString() {
		return "SalesModeWiseBillReport [branchCode=" + branchCode + ", minVoucherNumericNo=" + minVoucherNumericNo
				+ ", maxVoucherNumericNo=" + maxVoucherNumericNo + ", salesMode=" + salesMode + ", voucherDate="
				+ voucherDate + ", count=" + count + ", branchCodeProperty=" + branchCodeProperty
				+ ", salesModeProperty=" + salesModeProperty + ", minNoProperty=" + minNoProperty + ", maxNoProperty="
				+ maxNoProperty + ", countProperty=" + countProperty + "]";
	}

	
	
}
