package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class BalanceSheetAssetReport {
	
	
	String Id;
	String branchName;
	String accountId;
	String serialNumber;
	String accountName;
	
	Date endDate;
	
	Double amount;
	
	
	
	public BalanceSheetAssetReport() {
		this.serialNumberProperty=new SimpleStringProperty();
		this.accountNameProperty=new SimpleStringProperty();
		this.accountIdProperty=new SimpleStringProperty();
		this.branchNameProperty=new SimpleStringProperty();		
		this.date = new SimpleObjectProperty<LocalDate>();		
		this.amountProperty=new SimpleDoubleProperty();
		
		
		
	}
	
	@JsonIgnore
	StringProperty serialNumberProperty;
	@JsonIgnore
	StringProperty accountNameProperty;
	@JsonIgnore
	StringProperty accountIdProperty;
	@JsonIgnore
	StringProperty branchNameProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> date;
	
	@JsonIgnore
	DoubleProperty amountProperty;
	

	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public StringProperty getSerialNumberProperty() {
		return serialNumberProperty;
	}
	public void setSerialNumberProperty(StringProperty serialNumberProperty) {
		this.serialNumberProperty = serialNumberProperty;
	}
	public StringProperty getAccountNameProperty() {
		return accountNameProperty;
	}
	public void setAccountNameProperty(StringProperty accountNameProperty) {
		this.accountNameProperty = accountNameProperty;
	}
	public StringProperty getAccountIdProperty() {
		return accountIdProperty;
	}
	public void setAccountIdProperty(StringProperty accountIdProperty) {
		this.accountIdProperty = accountIdProperty;
	}
	public StringProperty getBranchNameProperty() {
		return branchNameProperty;
	}
	public void setBranchNameProperty(StringProperty branchNameProperty) {
		this.branchNameProperty = branchNameProperty;
	}
	
	@JsonProperty("date")
	public java.sql.Date getDate( ) {
		if(null==this.date.get() ) {
			return null;
		}else {
			return java.sql.Date.valueOf(this.date.get() );
		}	
	}

	public void setDate (java.sql.Date dateProperty) {
		if(null!=dateProperty)
		this.date.set(dateProperty.toLocalDate());
	}
	public ObjectProperty<LocalDate> getdateProperty( ) {
		return date;
	}

	public void setdateProperty(ObjectProperty<LocalDate> date) {
		this.date = (SimpleObjectProperty<LocalDate>) date;
	}
	
	public DoubleProperty getAmountProperty() {
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	@Override
	public String toString() {
		return "BalanceSheetAssetReport [Id=" + Id + ", branchName=" + branchName + ", accountId=" + accountId
				+ ", serialNumber=" + serialNumber + ", accountName=" + accountName + ", endDate=" + endDate
				+ ", amount=" + amount + ", serialNumberProperty=" + serialNumberProperty + ", accountNameProperty="
				+ accountNameProperty + ", accountIdProperty=" + accountIdProperty + ", branchNameProperty="
				+ branchNameProperty + ", date=" + date + ", amountProperty=" + amountProperty + "]";
	}
}
