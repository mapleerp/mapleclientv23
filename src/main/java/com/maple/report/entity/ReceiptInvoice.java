package com.maple.report.entity;



import java.sql.Date;
import java.time.LocalDate;


import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class ReceiptInvoice {
	
	
	String id;
	String receiptNumber;
	String invoiceNumber;
	Double recievedAmount;
    String companyName;
    String state;
    String companyGst;
    String voucherNumber;
    String account;
    String remark;
    String modeOfpayment;
	Date instrumentDate;
	String instrumentNumber;
	String branchName;
	String branchAddress;
	String branchTelNo;
	String branchGst;
	String instrumentNo;
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private StringProperty accountNameProperty;
	
	
	
	public ReceiptInvoice() {
		this.voucherNumberProperty =new SimpleStringProperty();
		this.voucherDate = new SimpleObjectProperty<LocalDate>();
		this.amountProperty = new SimpleDoubleProperty();
		this.accountNameProperty = new SimpleStringProperty();
		this.account = "";
	}
	

	@JsonProperty("voucherDate")
	public java.sql.Date getvoucherDate( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}
	


public void setvoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}
   public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
		return voucherDate;
	}

	public void setvoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
		this.voucherDate = voucherDate;
	}
	

	   public String getInstrumentNo() {
			return instrumentNo;
		}


		public void setInstrumentNo(String instrumentNo) {
			this.instrumentNo = instrumentNo;
		}

	@JsonIgnore
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	

	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@JsonIgnore
	public StringProperty getacountNameProperty() {
		accountNameProperty.set(account);
		return accountNameProperty;
	}
	

	public void setacountNameProperty(String account) {
		this.account = account;
	}
	@JsonIgnore
	
	public DoubleProperty getamountProperty() {
		amountProperty.set(recievedAmount);
		return amountProperty;
	}
	

	public void setamountProperty(Double recievedAmount) {
		this.recievedAmount = recievedAmount;
	}
	String familyName;
	String memberName;
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	
	public String getId()
    {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Double getRecievedAmount() {
		return recievedAmount;
	}
	public void setRecievedAmount(Double recievedAmount) {
		this.recievedAmount = recievedAmount;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCompanyGst() {
		return companyGst;
	}
	public void setCompanyGst(String companyGst) {
		this.companyGst = companyGst;
	}
	

	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getModeOfpayment() {
		return modeOfpayment;
	}
	public void setModeOfpayment(String modeOfpayment) {
		this.modeOfpayment = modeOfpayment;
	}
	public Date getInstrumentDate() {
		return instrumentDate;
	}
	public void setInstrumentDate(Date instrumentDate) {
		this.instrumentDate = instrumentDate;
	}
	
	
	public String getInstrumentNumber() {
		return instrumentNumber;
	}
	public void setInstrumentNumber(String instrumentNumber) {
		this.instrumentNumber = instrumentNumber;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchTelNo() {
		return branchTelNo;
	}
	public void setBranchTelNo(String branchTelNo) {
		this.branchTelNo = branchTelNo;
	}
	public String getBranchGst() {
		return branchGst;
	}
	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}
	
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@Override
	public String toString() {
		return "ReceiptInvoice [id=" + id + ", receiptNumber=" + receiptNumber + ", invoiceNumber=" + invoiceNumber
				+ ", recievedAmount=" + recievedAmount + ", companyName=" + companyName + ", state=" + state
				+ ", companyGst=" + companyGst + ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate
				+ ", account=" + account + ", remark=" + remark + ", modeOfpayment=" + modeOfpayment
				+ ", instrumentDate=" + instrumentDate + ", instrumentNumber=" + instrumentNumber + ", branchName="
				+ branchName + ", branchAddress=" + branchAddress + ", branchTelNo=" + branchTelNo + ", branchGst="
				+ branchGst + ", familyName=" + familyName + ", memberName=" + memberName + "]";
	}

}
