package com.maple.report.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class BranchStockReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	String id;
	String groupName;
	String itemName;
	
	private Double amount;
	String batchCode;
	String itemCode;
	String expiryDate;
	Double quantity;
	Double rate;
	
	
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty groupNameProperty;
	
	@JsonIgnore
	private StringProperty batchCodeProperty;
	
	@JsonIgnore
	private StringProperty itemCodeProperty;
	
	@JsonIgnore
	private StringProperty expiryDateProperty;
	
	@JsonIgnore
	private DoubleProperty quantityProperty;
	
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	
	
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	

	public BranchStockReport() {
		
		this.itemNameProperty = new SimpleStringProperty("");
		this.groupNameProperty = new SimpleStringProperty("");
		this.batchCodeProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.expiryDateProperty = new SimpleStringProperty("");
		this.quantityProperty = new SimpleDoubleProperty(0.0);
		this.rateProperty = new SimpleDoubleProperty(0.0);
		this.amountProperty = new SimpleDoubleProperty(0.0);
		

	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getItemName() {
		return itemName;
	}


	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	


	public Double getAmount() {
		return amount;
	}


	public void setAmount(Double amount) {
		this.amount = amount;
	}


	public String getBatchCode() {
		return batchCode;
	}


	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getExpiryDate() {
		return expiryDate;
	}


	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}


	public Double getQuantity() {
		return quantity;
	}


	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}


	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}

	@JsonIgnore
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}


	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	@JsonIgnore
	public StringProperty getGroupNameProperty() {
		groupNameProperty.set(groupName);
		return groupNameProperty;
	}


	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}

	@JsonIgnore
	public StringProperty getBatchCodeProperty() {
		batchCodeProperty.set(batchCode);
		return batchCodeProperty;
	}


	public void setBatchCodeProperty(StringProperty batchCodeProperty) {
		this.batchCodeProperty = batchCodeProperty;
	}

	@JsonIgnore
	public StringProperty getItemCodeProperty() {
		itemCodeProperty.set(itemCode);
		return itemCodeProperty;
	}


	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	@JsonIgnore
	public StringProperty getExpiryDateProperty() {
		expiryDateProperty.set(expiryDate);
		return expiryDateProperty;
	}


	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}

	@JsonIgnore
	public DoubleProperty getQuantityProperty() {
		quantityProperty.set(quantity);
		return quantityProperty;
	}


	public void setQuantityProperty(DoubleProperty quantityProperty) {
		this.quantityProperty = quantityProperty;
	}

	@JsonIgnore
	public DoubleProperty getRateProperty() {
		rateProperty.set(rate);
		return rateProperty;
	}


	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}

	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}


	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}


	@Override
	public String toString() {
		return "BranchStockReport [id=" + id + ", groupName=" + groupName + ", itemName=" + itemName + ", amount="
				+ amount + ", batchCode=" + batchCode + ", itemCode=" + itemCode + ", expiryDate=" + expiryDate
				+ ", quantity=" + quantity + ", rate=" + rate + ", itemNameProperty=" + itemNameProperty
				+ ", groupNameProperty=" + groupNameProperty + ", batchCodeProperty=" + batchCodeProperty
				+ ", itemCodeProperty=" + itemCodeProperty + ", expiryDateProperty=" + expiryDateProperty
				+ ", quantityProperty=" + quantityProperty + ", rateProperty=" + rateProperty + ", amountProperty="
				+ amountProperty + "]";
	}


	
	

}
