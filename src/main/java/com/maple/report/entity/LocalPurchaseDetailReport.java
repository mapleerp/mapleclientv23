package com.maple.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LocalPurchaseDetailReport {
	String supplierName;
	String supplierInvNo;
	String voucherNumber;
	Date voucherDate;
	String itemName;
	Double qty;
	Double purchseRate;
	Double taxRate;
	String unit;
	Double amount;
	Double amountTotal;
	String supplierGst;
	Double taxAmount;

	StringProperty supNameProperty;
	StringProperty voucherDateProperty;
	StringProperty itemNameProperty;
	StringProperty supInvProperty;
	DoubleProperty qtyProperty;
	DoubleProperty amountProperty;
	DoubleProperty purchaseRateProperty;
	StringProperty voucherNumberProperty;
	
	public  LocalPurchaseDetailReport() {
		this.supNameProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();
		this.itemNameProperty =new SimpleStringProperty();
		this.supInvProperty = new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.purchaseRateProperty = new SimpleDoubleProperty();
		this.voucherNumberProperty = new SimpleStringProperty();	
	}
	Double taxSplit;
	public Double getTaxSplit() {
		return taxSplit;
	}
	public void setTaxSplit(Double taxSplit) {
		this.taxSplit = taxSplit;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierInvNo() {
		return supplierInvNo;
	}
	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getPurchseRate() {
		return purchseRate;
	}
	public void setPurchseRate(Double purchseRate) {
		this.purchseRate = purchseRate;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getAmountTotal() {
		return amountTotal;
	}
	public void setAmountTotal(Double amountTotal) {
		this.amountTotal = amountTotal;
	}
	public String getSupplierGst() {
		return supplierGst;
	}
	public void setSupplierGst(String supplierGst) {
		this.supplierGst = supplierGst;
	}
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public StringProperty getSupNameProperty() {
		supNameProperty.set(supplierName);
		return supNameProperty;
	}
	public void setSupNameProperty(StringProperty supNameProperty) {
		this.supNameProperty = supNameProperty;
	}
	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.SqlDateTostring(voucherDate));
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public StringProperty getSupInvProperty() {
		supInvProperty.set(supplierInvNo);
		return supInvProperty;
	}
	public void setSupInvProperty(StringProperty supInvProperty) {
		this.supInvProperty = supInvProperty;
	}
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	public DoubleProperty getPurchaseRateProperty() {
		purchaseRateProperty.set(purchseRate);
		return purchaseRateProperty;
	}
	public void setPurchaseRateProperty(DoubleProperty purchaseRateProperty) {
		this.purchaseRateProperty = purchaseRateProperty;
	}
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	@Override
	public String toString() {
		return "LocalPurchaseDetailReport [supplierName=" + supplierName + ", supplierInvNo=" + supplierInvNo
				+ ", voucherNumber=" + voucherNumber + ", voucherDate=" + voucherDate + ", itemName=" + itemName
				+ ", qty=" + qty + ", purchseRate=" + purchseRate + ", taxRate=" + taxRate + ", unit=" + unit
				+ ", amount=" + amount + ", amountTotal=" + amountTotal + ", supplierGst=" + supplierGst
				+ ", taxAmount=" + taxAmount + ", supNameProperty=" + supNameProperty + ", voucherDateProperty="
				+ voucherDateProperty + ", itemNameProperty=" + itemNameProperty + ", supInvProperty=" + supInvProperty
				+ ", qtyProperty=" + qtyProperty + ", amountProperty=" + amountProperty + ", purchaseRateProperty="
				+ purchaseRateProperty + ", voucherNumberProperty=" + voucherNumberProperty + "]";
	}

}
