package com.maple.report.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GSTInputDtlAndSmryReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	String id;
	String invoiceDate;
	String invoiceNumber;
	String supplierName;
	String supplierGst;
	String branch;
	Double totalPurchaseExcludingGst;
	Double gstAmount;
	Double excemptedPurchase;
	Double totalPurchase;
	String gstPercent;
	
	
	@JsonIgnore
	private StringProperty invoiceDateProperty;
	
	@JsonIgnore
	private StringProperty invoiceNumberProperty;
	
	@JsonIgnore
	private StringProperty supplierNameProperty;
	
	@JsonIgnore
	private StringProperty supplierGstProperty;
	
	@JsonIgnore
	private StringProperty branchProperty;
	
	@JsonIgnore
	private DoubleProperty totalPurchaseExcludingGstProperty;
	
	@JsonIgnore
	private DoubleProperty gstAmountProperty;
	
	@JsonIgnore
	private DoubleProperty excemptedPurchaseProperty;
	
	@JsonIgnore
	private DoubleProperty totalPurchaseProperty;
	
	@JsonIgnore
	private StringProperty gstPercentProperty;
	
	public GSTInputDtlAndSmryReport() {
		
		this.invoiceDateProperty = new SimpleStringProperty("");
		this.invoiceNumberProperty = new SimpleStringProperty("");
		this.supplierNameProperty = new SimpleStringProperty("");
		this.supplierGstProperty = new SimpleStringProperty("");
		this.branchProperty = new SimpleStringProperty("");
		this.totalPurchaseExcludingGstProperty = new SimpleDoubleProperty(0.0);
		this.gstAmountProperty = new SimpleDoubleProperty(0.0);
		this.excemptedPurchaseProperty = new SimpleDoubleProperty(0.0);
		this.totalPurchaseProperty = new SimpleDoubleProperty(0.0);
		this.gstPercentProperty=new SimpleStringProperty("");

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getInvoiceDate() {
		return invoiceDate;
	}
	
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	public String getSupplierName() {
		return supplierName;
	}
	
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	
	public String getSupplierGst() {
		return supplierGst;
	}
	
	public void setSupplierGst(String supplierGst) {
		this.supplierGst = supplierGst;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public Double getTotalPurchaseExcludingGst() {
		return totalPurchaseExcludingGst;
	}
	
	public void setTotalPurchaseExcludingGst(Double totalPurchaseExcludingGst) {
		this.totalPurchaseExcludingGst = totalPurchaseExcludingGst;
	}
	
	public Double getGstAmount() {
		return gstAmount;
	}
	
	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}
	
	public Double getExcemptedPurchase() {
		return excemptedPurchase;
	}
	
	public void setExcemptedPurchase(Double excemptedPurchase) {
		this.excemptedPurchase = excemptedPurchase;
	}
	
	public Double getTotalPurchase() {
		return totalPurchase;
	}
	
	public void setTotalPurchase(Double totalPurchase) {
		this.totalPurchase = totalPurchase;
	}
	
	public String getGstPercent() {
		return gstPercent;
	}
	
	public void setGstPercent(String gstPercent) {
		this.gstPercent = gstPercent;
	}
	
	@JsonIgnore
	public StringProperty getInvoiceDateProperty() {
		invoiceDateProperty.set(invoiceDate);
		return invoiceDateProperty;
	}
	
	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}
	
	@JsonIgnore
	public StringProperty getInvoiceNumberProperty() {
		invoiceNumberProperty.set(invoiceNumber);
		return invoiceNumberProperty;
	}
	
	public void setInvoiceNumberProperty(StringProperty invoiceNumberProperty) {
		this.invoiceNumberProperty = invoiceNumberProperty;
	}
	
	@JsonIgnore
	public StringProperty getSupplierNameProperty() {
		supplierNameProperty.set(supplierName);
		return supplierNameProperty;
	}
	
	public void setSupplierNameProperty(StringProperty supplierNameProperty) {
		this.supplierNameProperty = supplierNameProperty;
	}
	
	@JsonIgnore
	public StringProperty getSupplierGstProperty() {
		supplierGstProperty.set(supplierGst);
		return supplierGstProperty;
	}
	
	public void setSupplierGstProperty(StringProperty supplierGstProperty) {
		this.supplierGstProperty = supplierGstProperty;
	}
	
	@JsonIgnore
	public StringProperty getBranchProperty() {
		branchProperty.set(branch);
		return branchProperty;
	}
	
	public void setBranchProperty(StringProperty branchProperty) {
		this.branchProperty = branchProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getTotalPurchaseExcludingGstProperty() {
		if(null != totalPurchaseExcludingGst)
		{
			totalPurchaseExcludingGstProperty.set(totalPurchaseExcludingGst);

		}
		return totalPurchaseExcludingGstProperty;
	}
	
	public void setTotalPurchaseExcludingGstProperty(DoubleProperty totalPurchaseExcludingGstProperty) {
		this.totalPurchaseExcludingGstProperty = totalPurchaseExcludingGstProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getGstAmountProperty() {
		if(null != gstAmount)
		{
			gstAmountProperty.set(gstAmount);

		}
		return gstAmountProperty;
	}
	
	public void setGstAmountProperty(DoubleProperty gstAmountProperty) {
		this.gstAmountProperty = gstAmountProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getExcemptedPurchaseProperty() {
		if(null != excemptedPurchase)
		{
			excemptedPurchaseProperty.set(excemptedPurchase);

		}
		return excemptedPurchaseProperty;
	}
	
	public void setExcemptedPurchaseProperty(DoubleProperty excemptedPurchaseProperty) {
		this.excemptedPurchaseProperty = excemptedPurchaseProperty;
	}
	
	@JsonIgnore
	public DoubleProperty getTotalPurchaseProperty() {
		if(null != totalPurchase)
		{
			totalPurchaseProperty.set(totalPurchase);

		}
		return totalPurchaseProperty;
	}
	
	public void setTotalPurchaseProperty(DoubleProperty totalPurchaseProperty) {
		this.totalPurchaseProperty = totalPurchaseProperty;
	}
	
	@JsonIgnore
	public StringProperty getGstPercentProperty() {
		if(null != gstPercent)
		{
			gstPercentProperty.set(gstPercent);

		}
		return gstPercentProperty;
	}
	
	public void setGstPercentProperty(StringProperty gstPercentProperty) {
		this.gstPercentProperty = gstPercentProperty;
	}

	@Override
	public String toString() {
		return "GSTInputDtlAndSmryReport [id=" + id + ", invoiceDate=" + invoiceDate + ", invoiceNumber="
				+ invoiceNumber + ", supplierName=" + supplierName + ", supplierGst=" + supplierGst + ", branch="
				+ branch + ", totalPurchaseExcludingGst=" + totalPurchaseExcludingGst + ", gstAmount=" + gstAmount
				+ ", excemptedPurchase=" + excemptedPurchase + ", totalPurchase=" + totalPurchase + ", gstPercent="
				+ gstPercent + ", invoiceDateProperty=" + invoiceDateProperty + ", invoiceNumberProperty="
				+ invoiceNumberProperty + ", supplierNameProperty=" + supplierNameProperty + ", supplierGstProperty="
				+ supplierGstProperty + ", branchProperty=" + branchProperty + ", totalPurchaseExcludingGstProperty="
				+ totalPurchaseExcludingGstProperty + ", gstAmountProperty=" + gstAmountProperty
				+ ", excemptedPurchaseProperty=" + excemptedPurchaseProperty + ", totalPurchaseProperty="
				+ totalPurchaseProperty + ", gstPercentProperty=" + gstPercentProperty + "]";
	}

	
	
	
	
	

}
