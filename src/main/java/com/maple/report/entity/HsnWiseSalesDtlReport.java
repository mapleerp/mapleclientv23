package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class HsnWiseSalesDtlReport {

	String itemName;
	String hsnCode;
	String unitName;
	Double qty;
	Double rate;
	Double value;
	Double cgstAmount;
	Double sgstAmount;
	Double taxRate;
	Double cessRate;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty hsnCodeProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	@JsonIgnore
	private DoubleProperty valueProperty;
	
	@JsonIgnore
	private DoubleProperty cgstAmountProperty;
	
	@JsonIgnore
	private DoubleProperty sgstAmountProperty;
	
	@JsonIgnore
	private DoubleProperty taxRateProperty;
	
	@JsonIgnore
	private DoubleProperty cessRateProperty;
	
	public HsnWiseSalesDtlReport() {
		this.itemNameProperty = new SimpleStringProperty();
		this.hsnCodeProperty =  new SimpleStringProperty();
		this.unitNameProperty =  new SimpleStringProperty();
		this.qtyProperty = new SimpleDoubleProperty();
		this.rateProperty = new SimpleDoubleProperty();
		this.valueProperty = new SimpleDoubleProperty();
		this.cgstAmountProperty = new SimpleDoubleProperty();
		this.sgstAmountProperty = new SimpleDoubleProperty();
		this.taxRateProperty = new SimpleDoubleProperty();
		this.cessRateProperty = new SimpleDoubleProperty();
	}
	
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}


	public void setItemNameProperty(String itemName) {
		this.itemName = itemName;
	}


	public StringProperty getHsnCodeProperty() {
		hsnCodeProperty.set(hsnCode);
		return hsnCodeProperty;
	}


	public void setHsnCodeProperty(String hsnCode) {
		this.hsnCode = hsnCode;
	}


	public StringProperty getUnitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}


	public void setUnitNameProperty(String unitName) {
		this.unitName = unitName;
	}


	public DoubleProperty getQtyProperty() {
		if(null != qty)
		{
			qtyProperty.set(qty);
		}
		return qtyProperty;
	}


	public void setQtyProperty(Double qty) {
		this.qty = qty;
	}


	public DoubleProperty getRateProperty() {
		if(null != rate)
		{
			rateProperty.set(rate);
		}
		return rateProperty;
	}


	public void setRateProperty(Double rate) {
		this.rate = rate;
	}


	public DoubleProperty getValueProperty() {
		if(null != value)
		{
			valueProperty.set(value);
		}
		return valueProperty;
	}


	public void setValueProperty(Double value) {
		this.value = value;
	}


	public DoubleProperty getCgstAmountProperty() {
		if(null != cgstAmount)
		{
			cgstAmountProperty.set(cgstAmount);
		}
		return cgstAmountProperty;
	}


	public void setCgstAmountProperty(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}


	public DoubleProperty getSgstAmountProperty() {
		if(null != sgstAmount)
		{
			sgstAmountProperty.set(sgstAmount);
		}
		return sgstAmountProperty;
	}


	public void setSgstAmountProperty(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}


	public DoubleProperty getTaxRateProperty() {
		if(null != taxRate)
		{
			taxRateProperty.set(taxRate);
		}
		return taxRateProperty;
	}


	public void setTaxRateProperty(Double taxRate) {
		this.taxRate = taxRate;
	}


	public DoubleProperty getCessRateProperty() {
		if(null != cessRate)
		{
			cessRateProperty.set(cessRate);
		}
		return cessRateProperty;
	}


	public void setCessRateProperty(Double cessRate) {
		this.cessRate = cessRate;
	}


	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getCgstAmount() {
		return cgstAmount;
	}
	public void setCgstAmount(Double cgstAmount) {
		this.cgstAmount = cgstAmount;
	}
	public Double getSgstAmount() {
		return sgstAmount;
	}
	public void setSgstAmount(Double sgstAmount) {
		this.sgstAmount = sgstAmount;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public Double getCessRate() {
		return cessRate;
	}
	public void setCessRate(Double cessRate) {
		this.cessRate = cessRate;
	}
	@Override
	public String toString() {
		return "HsnWiseSalesDtlReport [itemName=" + itemName + ", hsnCode=" + hsnCode + ", unitName=" + unitName
				+ ", qty=" + qty + ", rate=" + rate + ", value=" + value + ", cgstAmount=" + cgstAmount
				+ ", sgstAmount=" + sgstAmount + ", taxRate=" + taxRate + ", cessRate=" + cessRate + "]";
	}
	
	
	
}
