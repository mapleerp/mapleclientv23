package com.maple.report.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PharmacyPurchaseReturnDetailReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	String id;
	String returnvoucherDate;
	String purchasevoucherNo;
	String supplierName;
	String returnvoucherNo;
	String itemName;
	String groupName;
	String itemCode;
	String batch;
	String expiryDate;
	String userName;
	private Double qty;
	private Double purchaseRate;
	private Double amount;
	Double beforeGst;
	Double gst;
	Double total;
	
	
	@JsonIgnore
	private StringProperty returnvoucherDateProperty;
	
	@JsonIgnore
	private StringProperty purchasevoucherNoProperty;
	
	@JsonIgnore
	private StringProperty supplierNameProperty;
	
	@JsonIgnore
	private StringProperty returnvoucherNoProperty;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	
	@JsonIgnore
	private StringProperty groupNameProperty;
	
	@JsonIgnore
	private StringProperty itemCodeProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private StringProperty expiryDateProperty;
	
	@JsonIgnore
	private StringProperty userNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty purchaseRateProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	
	
	
	
public PharmacyPurchaseReturnDetailReport() {
		
		this.returnvoucherDateProperty = new SimpleStringProperty("");
		this.purchasevoucherNoProperty = new SimpleStringProperty("");
		this.supplierNameProperty = new SimpleStringProperty("");
		this.itemNameProperty = new SimpleStringProperty("");
		this.userNameProperty = new SimpleStringProperty("");
		this.itemCodeProperty = new SimpleStringProperty("");
		this.batchProperty = new SimpleStringProperty("");
		this.expiryDateProperty = new SimpleStringProperty("");
		this.groupNameProperty = new SimpleStringProperty("");
		this.returnvoucherNoProperty = new SimpleStringProperty("");
		
		
		this.qtyProperty = new SimpleDoubleProperty(0.0);
		this.purchaseRateProperty = new SimpleDoubleProperty(0.0);
		this.amountProperty = new SimpleDoubleProperty(0.0);

	}
	
	
	
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReturnvoucherDate() {
		return returnvoucherDate;
	}

	public void setReturnvoucherDate(String returnvoucherDate) {
		this.returnvoucherDate = returnvoucherDate;
	}

	public String getPurchasevoucherNo() {
		return purchasevoucherNo;
	}

	public void setPurchasevoucherNo(String purchasevoucherNo) {
		this.purchasevoucherNo = purchasevoucherNo;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getReturnvoucherNo() {
		return returnvoucherNo;
	}

	public void setReturnvoucherNo(String returnvoucherNo) {
		this.returnvoucherNo = returnvoucherNo;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@JsonIgnore
	public StringProperty getReturnvoucherDateProperty() {
		return returnvoucherDateProperty;
	}

	public void setReturnvoucherDateProperty(StringProperty returnvoucherDateProperty) {
		this.returnvoucherDateProperty = returnvoucherDateProperty;
	}
	@JsonIgnore
	public StringProperty getPurchasevoucherNoProperty() {
		return purchasevoucherNoProperty;
	}

	public void setPurchasevoucherNoProperty(StringProperty purchasevoucherNoProperty) {
		this.purchasevoucherNoProperty = purchasevoucherNoProperty;
	}
	@JsonIgnore
	public StringProperty getSupplierNameProperty() {
		return supplierNameProperty;
	}

	public void setSupplierNameProperty(StringProperty supplierNameProperty) {
		this.supplierNameProperty = supplierNameProperty;
	}
	@JsonIgnore
	public StringProperty getReturnvoucherNoProperty() {
		if(null!=returnvoucherNo) {
			returnvoucherNoProperty.set(returnvoucherNo);
		}
		return returnvoucherNoProperty;
	}

	public void setReturnvoucherNoProperty(StringProperty returnvoucherNoProperty) {
		this.returnvoucherNoProperty = returnvoucherNoProperty;
	}
	@JsonIgnore
	public StringProperty getItemNameProperty() {
		if(null!=itemName) {
			itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	@JsonIgnore
	public StringProperty getGroupNameProperty() {
		if(null!=groupName) {
			groupNameProperty.set(groupName);
		}
		return groupNameProperty;
	}

	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}
	@JsonIgnore
	public StringProperty getItemCodeProperty() {
		if(null!=itemCode) {
			itemCodeProperty.set(itemCode);
		}
		return itemCodeProperty;
	}

	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}
	@JsonIgnore
	public StringProperty getBatchProperty() {
		if(null!=batch) {
			batchProperty.set(batch);
		}
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}
	@JsonIgnore
	public StringProperty getExpiryDateProperty() {
		if(null!=expiryDate)
		{
			expiryDateProperty.set(expiryDate);	
		}
		return expiryDateProperty;
	}

	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}
	@JsonIgnore
	public StringProperty getUserNameProperty() {
		if(null!=userName) {
			userNameProperty.set(userName);
		}
		return userNameProperty;
	}

	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}
	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		if(null!=qty) {
			qtyProperty.set(qty);
		}
		return qtyProperty;
	}

	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	@JsonIgnore
	public DoubleProperty getPurchaseRateProperty() {
		if(null!=purchaseRate) {
			purchaseRateProperty.set(purchaseRate);
		}
		return purchaseRateProperty;
	}

	public void setPurchaseRateProperty(DoubleProperty purchaseRateProperty) {
		this.purchaseRateProperty = purchaseRateProperty;
	}
	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		if(null!=amount) {
			amountProperty.set(amount);
		}
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	@Override
	public String toString() {
		return "PharmacyPurchaseReturnDetailReport [id=" + id + ", returnvoucherDate=" + returnvoucherDate
				+ ", purchasevoucherNo=" + purchasevoucherNo + ", supplierName=" + supplierName + ", returnvoucherNo="
				+ returnvoucherNo + ", itemName=" + itemName + ", groupName=" + groupName + ", itemCode=" + itemCode
				+ ", batch=" + batch + ", expiryDate=" + expiryDate + ", userName=" + userName + ", qty=" + qty
				+ ", purchaseRate=" + purchaseRate + ", amount=" + amount + ", returnvoucherDateProperty="
				+ returnvoucherDateProperty + ", purchasevoucherNoProperty=" + purchasevoucherNoProperty
				+ ", supplierNameProperty=" + supplierNameProperty + ", returnvoucherNoProperty="
				+ returnvoucherNoProperty + ", itemNameProperty=" + itemNameProperty + ", groupNameProperty="
				+ groupNameProperty + ", itemCodeProperty=" + itemCodeProperty + ", batchProperty=" + batchProperty
				+ ", expiryDateProperty=" + expiryDateProperty + ", userNameProperty=" + userNameProperty
				+ ", qtyProperty=" + qtyProperty + ", purchaseRateProperty=" + purchaseRateProperty
				+ ", amountProperty=" + amountProperty + "]";
	}
	

}
