package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ConsumptionReport {


	String itemName;
	String reason;
	Double qty;
	
	String unit;
	Double amount;
	
	Double mrp;

	String categoryName;
	Date voucherDate;
	
	
	String voucherNumber;
	String date;
	String department;
	String userName;
	
	String batch;
	String expiry;
	Double rate;
	
	
	@JsonIgnore
	private StringProperty departmentProperty;
	@JsonIgnore
	private StringProperty dateProperty;
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	@JsonIgnore
	private StringProperty userNameProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	@JsonIgnore
	private StringProperty expiryProperty;
	@JsonIgnore
	private DoubleProperty rateProperty;
	
	
	
	
	@JsonIgnore
	private StringProperty unitProperty;
	

	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;

	@JsonIgnore
	private StringProperty mrpProperty;

	@JsonIgnore
	private StringProperty itemNameProperty;
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private StringProperty categoryNameProperty;
	@JsonIgnore
	private StringProperty reasonProperty;
    public ConsumptionReport() {
	
		this.amountProperty = new SimpleDoubleProperty();
		this.itemNameProperty = new SimpleStringProperty();
        this.qtyProperty = new SimpleDoubleProperty();
		this.mrpProperty = new SimpleStringProperty();
		this.unitProperty = new SimpleStringProperty();
		this.reasonProperty=new SimpleStringProperty();
		this.categoryNameProperty=new SimpleStringProperty();
		this.voucherDateProperty=new SimpleStringProperty();
        this.voucherNumberProperty =new SimpleStringProperty();
        this.dateProperty = new SimpleStringProperty();
        this.departmentProperty =new SimpleStringProperty();
        
        this.rateProperty = new SimpleDoubleProperty();
        this.batchProperty = new SimpleStringProperty();
        this.expiryProperty = new SimpleStringProperty();
        this.userNameProperty = new SimpleStringProperty();
        
        
	}
	
    
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public StringProperty getBatchProperty() {
		if(null != batch) {
			batchProperty.set(batch);
		}
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public StringProperty getExpiryProperty() {
		if(null != expiry) {
			expiryProperty.set(expiry);
		}
		return expiryProperty;
	}

	public void setExpiryProperty(StringProperty expiryProperty) {
		this.expiryProperty = expiryProperty;
	}

	public DoubleProperty getRateProperty() {
		if(null != rate) {
			rateProperty.set(rate);
		}
		return rateProperty;
	}

	public void setRateProperty(DoubleProperty rateProperty) {
		this.rateProperty = rateProperty;
	}


	public Date getVoucherDate() {
		return voucherDate;
	}


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}


	public StringProperty setUnitProperty(StringProperty unitProperty) {
		
		unitProperty.set(unit);
		return unitProperty;
		
	}


	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}



	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}


	public void setMrpProperty(StringProperty mrpProperty) {
		this.mrpProperty = mrpProperty;
	}


	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getReasonProperty(StringProperty reasonProperty) {
		
		return reasonProperty;
	}


	public StringProperty getcategoryNameProperty() {
		categoryNameProperty.set(categoryName);
		return categoryNameProperty;
	}
	

	public void setcategoryNameProperty(String categoryName) {
		this.categoryName = categoryName;
	}
	
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "dd-MM-yyyy"));
		return voucherDateProperty;
	}
	

	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	@JsonIgnore
	public StringProperty getReasonProperty() {
		reasonProperty.set(reason);
		return reasonProperty;
	}


	@JsonIgnore
	public StringProperty getMrpProperty() {
		mrpProperty.set(String.valueOf(mrp));
		return mrpProperty;
	}


	@JsonIgnore
	public DoubleProperty getAmountProperty() {
		if(null != amount) {
			
		amountProperty.set(amount);
		}
		return amountProperty;
	}

	@JsonIgnore
	public DoubleProperty getQtyProperty() {
		if(null != qty) {
		qtyProperty.set(qty);
		}
		return qtyProperty;
	}
	public void setReasonProperty(StringProperty reasonProperty) {
		this.reasonProperty = reasonProperty;
	}



	@JsonIgnore
	public StringProperty getItemNameProperty() {
		if(null!=itemName) {
		itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}
	@JsonIgnore
	public StringProperty getUnitProperty() {
		if(null!=unit) {
		unitProperty.set(unit);
		}
		return unitProperty;
	}



	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getMrp() {
		return mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	@JsonIgnore
	public StringProperty getDepartmentProperty() {
		if(null!=department) {
			departmentProperty.set(department);
		}
		return departmentProperty;
	}

	public void setDepartmentProperty(StringProperty departmentProperty) {
		this.departmentProperty = departmentProperty;
	}
	@JsonIgnore
	public StringProperty getDateProperty() {
		if(null!=date) {
			dateProperty.set(date);
		}
		return dateProperty;
	}

	public void setDateProperty(StringProperty dateProperty) {
		this.dateProperty = dateProperty;
	}
	@JsonIgnore
	public StringProperty getVoucherNumberProperty() {
		if(null!=voucherNumber) {
			voucherNumberProperty.set(voucherNumber);
		}
		
		return voucherNumberProperty;
	}

	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	@JsonIgnore
	public StringProperty getVoucherDateProperty() {
		
		return voucherDateProperty;
	}

	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}
	@JsonIgnore
	public StringProperty getCategoryNameProperty() {
		
		if(null!=categoryName) {
			categoryNameProperty.set(categoryName);
		}
		
		return categoryNameProperty;
	}

	public void setCategoryNameProperty(StringProperty categoryNameProperty) {
		this.categoryNameProperty = categoryNameProperty;
	}

	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public StringProperty getUserNameProperty() {
		
		if(null!=userName) {
			userNameProperty.set(userName);
		}
		return userNameProperty;
	}

	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}


	@Override
	public String toString() {
		return "ConsumptionReport [itemName=" + itemName + ", reason=" + reason + ", qty=" + qty + ", unit=" + unit
				+ ", amount=" + amount + ", mrp=" + mrp + ", categoryName=" + categoryName + ", voucherDate="
				+ voucherDate + ", voucherNumber=" + voucherNumber + ", date=" + date + ", department=" + department
				+ ", userName=" + userName + ", batch=" + batch + ", expiry=" + expiry + ", rate=" + rate
				+ ", departmentProperty=" + departmentProperty + ", dateProperty=" + dateProperty
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", userNameProperty=" + userNameProperty
				+ ", batchProperty=" + batchProperty + ", expiryProperty=" + expiryProperty + ", rateProperty="
				+ rateProperty + ", unitProperty=" + unitProperty + ", qtyProperty=" + qtyProperty + ", amountProperty="
				+ amountProperty + ", mrpProperty=" + mrpProperty + ", itemNameProperty=" + itemNameProperty
				+ ", voucherDateProperty=" + voucherDateProperty + ", categoryNameProperty=" + categoryNameProperty
				+ ", reasonProperty=" + reasonProperty + "]";
	}

	


	
	
}
