package com.maple.report.entity;

import java.util.Date;

import com.maple.mapleclient.entity.CompanyMst;

 
 

public class DailySaleSummaryReport {

	 
	    private String id;
		Date recordDate;
		String openingBillNo;
		String closingBillNo;
		Integer totalBillNo;
		Double openingPettyCash;
		Double cashReceived;
		Double expensePaid;
		Double closingBalance;
		Double cashSalesOrder;
		Double onlineSalesOrder;
		Double debitCardSalesOrder;
		String branchCode;
		Double b2BSales;
		Double cardSale;
		Double uberSale;
		Double swiggy;
		Double zomoto;
		Double foodPanda;
		Double sodexo;
		Double paytm;
		Double credit;
		Double cashSale;
		
		private CompanyMst companyMst;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public Date getRecordDate() {
			return recordDate;
		}
		public void setRecordDate(Date recordDate) {
			this.recordDate = recordDate;
		}
		public String getOpeningBillNo() {
			return openingBillNo;
		}
		public void setOpeningBillNo(String openingBillNo) {
			this.openingBillNo = openingBillNo;
		}
		public String getClosingBillNo() {
			return closingBillNo;
		}
		public void setClosingBillNo(String closingBillNo) {
			this.closingBillNo = closingBillNo;
		}
		public Integer getTotalBillNo() {
			return totalBillNo;
		}
		public void setTotalBillNo(Integer totalBillNo) {
			this.totalBillNo = totalBillNo;
		}
		public Double getOpeningPettyCash() {
			return openingPettyCash;
		}
		public void setOpeningPettyCash(Double openingPettyCash) {
			this.openingPettyCash = openingPettyCash;
		}
		public Double getCashReceived() {
			return cashReceived;
		}
		public void setCashReceived(Double cashReceived) {
			this.cashReceived = cashReceived;
		}
		public Double getExpensePaid() {
			return expensePaid;
		}
		public void setExpensePaid(Double expensePaid) {
			this.expensePaid = expensePaid;
		}
		public Double getClosingBalance() {
			return closingBalance;
		}
		public void setClosingBalance(Double closingBalance) {
			this.closingBalance = closingBalance;
		}
		public Double getCashSalesOrder() {
			return cashSalesOrder;
		}
		public void setCashSalesOrder(Double cashSalesOrder) {
			this.cashSalesOrder = cashSalesOrder;
		}
		public Double getOnlineSalesOrder() {
			return onlineSalesOrder;
		}
		public void setOnlineSalesOrder(Double onlineSalesOrder) {
			this.onlineSalesOrder = onlineSalesOrder;
		}
		public Double getDebitCardSalesOrder() {
			return debitCardSalesOrder;
		}
		public void setDebitCardSalesOrder(Double debitCardSalesOrder) {
			this.debitCardSalesOrder = debitCardSalesOrder;
		}
		public String getBranchCode() {
			return branchCode;
		}
		public void setBranchCode(String branchCode) {
			this.branchCode = branchCode;
		}
		public Double getB2BSales() {
			return b2BSales;
		}
		public void setB2BSales(Double b2bSales) {
			b2BSales = b2bSales;
		}
		public Double getCardSale() {
			return cardSale;
		}
		public void setCardSale(Double cardSale) {
			this.cardSale = cardSale;
		}
		public Double getUberSale() {
			return uberSale;
		}
		public void setUberSale(Double uberSale) {
			this.uberSale = uberSale;
		}
		public Double getSwiggy() {
			return swiggy;
		}
		public void setSwiggy(Double swiggy) {
			this.swiggy = swiggy;
		}
		public Double getZomoto() {
			return zomoto;
		}
		public void setZomoto(Double zomoto) {
			this.zomoto = zomoto;
		}
		public Double getFoodPanda() {
			return foodPanda;
		}
		public void setFoodPanda(Double foodPanda) {
			this.foodPanda = foodPanda;
		}
		public Double getSodexo() {
			return sodexo;
		}
		public void setSodexo(Double sodexo) {
			this.sodexo = sodexo;
		}
		public Double getPaytm() {
			return paytm;
		}
		public void setPaytm(Double paytm) {
			this.paytm = paytm;
		}
		public Double getCredit() {
			return credit;
		}
		public void setCredit(Double credit) {
			this.credit = credit;
		}
		public CompanyMst getCompanyMst() {
			return companyMst;
		}
		public void setCompanyMst(CompanyMst companyMst) {
			this.companyMst = companyMst;
		}
		public Double getCashSale() {
			return cashSale;
		}
		public void setCashSale(Double cashSale) {
			this.cashSale = cashSale;
		}
		
		
}
