package com.maple.report.entity;

public class PharmacyDayEndReport {
	private String voucherNumber;
	private String voucherType;
	private String customer;
	private String insurancePolicy;
	private Double invoiceAmount;
	private Double credit;
	
	Double openingCash;
	Double cashSales;
	Double cashReceived;
	Double cashPaid;
	Double closingCashBalance;
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherType() {
		return voucherType;
	}
	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getInsurancePolicy() {
		return insurancePolicy;
	}
	public void setInsurancePolicy(String insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getOpeningCash() {
		return openingCash;
	}
	public void setOpeningCash(Double openingCash) {
		this.openingCash = openingCash;
	}
	public Double getCashSales() {
		return cashSales;
	}
	public void setCashSales(Double cashSales) {
		this.cashSales = cashSales;
	}
	public Double getCashReceived() {
		return cashReceived;
	}
	public void setCashReceived(Double cashReceived) {
		this.cashReceived = cashReceived;
	}
	public Double getCashPaid() {
		return cashPaid;
	}
	public void setCashPaid(Double cashPaid) {
		this.cashPaid = cashPaid;
	}
	public Double getClosingCashBalance() {
		return closingCashBalance;
	}
	public void setClosingCashBalance(Double closingCashBalance) {
		this.closingCashBalance = closingCashBalance;
	}
	@Override
	public String toString() {
		return "PharmacyDayEndReport [voucherNumber=" + voucherNumber + ", voucherType=" + voucherType + ", customer="
				+ customer + ", insurancePolicy=" + insurancePolicy + ", invoiceAmount=" + invoiceAmount + ", credit="
				+ credit + ", openingCash=" + openingCash + ", cashSales=" + cashSales + ", cashReceived="
				+ cashReceived + ", cashPaid=" + cashPaid + ", closingCashBalance=" + closingCashBalance + "]";
	}


}
