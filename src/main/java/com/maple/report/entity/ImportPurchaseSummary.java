package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ImportPurchaseSummary {
	
	

	
	private String invoiceNumber;
	
	private String supplierName;
	
	private String supplierInvNo;
	
	private Date invoiceDate;
	
	private String  purchaseOrder;
	
	private Double  itemTotal;
	
	private Double itemTotalFc;
	
	private Double expense;
	
	private Double fcExpense;

	
	private Double notIncludedExp;
	
	private Double includedExp;
	
	private Double expenseTotal;
	
	private Double grandTotal;
	
	private Double importDuty;
	
	private String currencyName;
	
	
	
	@JsonIgnore
	private StringProperty invoiceNumberProperty;
	@JsonIgnore
	private StringProperty supplierNameProperty;
	@JsonIgnore
	private StringProperty supplierInvNoProperty;
	@JsonIgnore
	private DoubleProperty itemTotalProperty;
	@JsonIgnore
	private DoubleProperty itemTotalFcProperty;
	@JsonIgnore
	private DoubleProperty expenseProperty;
	@JsonIgnore
	private DoubleProperty fcExpenseProperty;
	@JsonIgnore
	private DoubleProperty notIncludedExpProperty;
	@JsonIgnore
	private DoubleProperty grandTotalExpProperty;
	@JsonIgnore
	private DoubleProperty includedExpProperty;                   
	@JsonIgnore
	private DoubleProperty importDutyProperty;

	@JsonIgnore
	private StringProperty dateProperty;

	public ImportPurchaseSummary()
	{
	this.invoiceNumberProperty = new SimpleStringProperty(); 
	this.supplierNameProperty = new SimpleStringProperty();
	this.supplierInvNoProperty =new SimpleStringProperty();
	this.itemTotalProperty     =new SimpleDoubleProperty();
	this.itemTotalFcProperty   =new SimpleDoubleProperty();
	this.expenseProperty     =new SimpleDoubleProperty();
	this.fcExpenseProperty    =new SimpleDoubleProperty();
	this.notIncludedExpProperty =new SimpleDoubleProperty();
	this.grandTotalExpProperty  =new SimpleDoubleProperty();
	this.importDutyProperty   =new SimpleDoubleProperty();
	this.includedExpProperty  =new SimpleDoubleProperty();
	this.dateProperty  =new SimpleStringProperty();

	
	
	
	}



	


	public String getInvoiceNumber() {
		return invoiceNumber;
	}






	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}






	public String getSupplierName() {
		return supplierName;
	}






	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}






	public String getSupplierInvNo() {
		return supplierInvNo;
	}






	public void setSupplierInvNo(String supplierInvNo) {
		this.supplierInvNo = supplierInvNo;
	}






	public Date getInvoiceDate() {
		return invoiceDate;
	}






	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}






	public String getPurchaseOrder() {
		return purchaseOrder;
	}






	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}






	public Double getItemTotal() {
		return itemTotal;
	}






	public void setItemTotal(Double itemTotal) {
		this.itemTotal = itemTotal;
	}






	public Double getItemTotalFc() {
		return itemTotalFc;
	}






	public void setItemTotalFc(Double itemTotalFc) {
		this.itemTotalFc = itemTotalFc;
	}






	public Double getExpense() {
		return expense;
	}






	public void setExpense(Double expense) {
		this.expense = expense;
	}






	public Double getFcExpense() {
		return fcExpense;
	}






	public void setFcExpense(Double fcExpense) {
		this.fcExpense = fcExpense;
	}






	public Double getNotIncludedExp() {
		return notIncludedExp;
	}






	public void setNotIncludedExp(Double notIncludedExp) {
		this.notIncludedExp = notIncludedExp;
	}






	public Double getIncludedExp() {
		return includedExp;
	}






	public void setIncludedExp(Double includedExp) {
		this.includedExp = includedExp;
	}






	public Double getExpenseTotal() {
		return expenseTotal;
	}






	public void setExpenseTotal(Double expenseTotal) {
		this.expenseTotal = expenseTotal;
	}






	public Double getGrandTotal() {
		return grandTotal;
	}






	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}






	public Double getImportDuty() {
		return importDuty;
	}






	public void setImportDuty(Double importDuty) {
		this.importDuty = importDuty;
	}






	public String getCurrencyName() {
		return currencyName;
	}






	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}





	@JsonIgnore
	public StringProperty getInvoiceNumberProperty() {
		if(null!=invoiceNumber) {
			invoiceNumberProperty.set(invoiceNumber);
		}
		return invoiceNumberProperty;
	}






	public void setInvoiceNumberProperty(StringProperty invoiceNumberProperty) {
		this.invoiceNumberProperty = invoiceNumberProperty;
	}





	@JsonIgnore
	public StringProperty getSupplierNameProperty() {
		
		if(null!= supplierName) {
			supplierNameProperty.set(supplierName);
		}
		return supplierNameProperty;
	}






	public void setSupplierNameProperty(StringProperty supplierNameProperty) {
		this.supplierNameProperty = supplierNameProperty;
	}





	@JsonIgnore
	public StringProperty getSupplierInvNoProperty() {
		if(null!= supplierInvNo) {
			supplierInvNoProperty.set(supplierInvNo);
		}
		return supplierInvNoProperty;
	}






	public void setSupplierInvNoProperty(StringProperty supplierInvNoProperty) {
		this.supplierInvNoProperty = supplierInvNoProperty;
	}





	@JsonIgnore
	public DoubleProperty getItemTotalProperty() {
		if(null!= itemTotal) {
			itemTotalProperty.set(itemTotal);
		}
		return itemTotalProperty;
	}






	public void setItemTotalProperty(DoubleProperty itemTotalProperty) {
		this.itemTotalProperty = itemTotalProperty;
	}





	@JsonIgnore
	public DoubleProperty getItemTotalFcProperty() {
		if(null!= itemTotalFc) {
			itemTotalFcProperty.set(itemTotalFc);
		}
		return itemTotalFcProperty;
	}






	public void setItemTotalFcProperty(DoubleProperty itemTotalFcProperty) {
		this.itemTotalFcProperty = itemTotalFcProperty;
	}





	@JsonIgnore
	public DoubleProperty getFcExpenseProperty() {
		if(null!=fcExpense) {
			fcExpenseProperty.set(fcExpense);
		}
		return fcExpenseProperty;
	}






	public void setFcExpenseProperty(DoubleProperty fcExpenseProperty) {
		this.fcExpenseProperty = fcExpenseProperty;
	}





	@JsonIgnore
	public DoubleProperty getNotIncludedExpProperty() {
		if(null!=notIncludedExp) {
			notIncludedExpProperty.set(notIncludedExp);
		}
		return notIncludedExpProperty;
	}






	public void setNotIncludedExpProperty(DoubleProperty notIncludedExpProperty) {
		this.notIncludedExpProperty = notIncludedExpProperty;
	}





	@JsonIgnore
	public DoubleProperty getGrandTotalExpProperty() {
		if(null!=grandTotal) {
			grandTotalExpProperty.set(grandTotal);
		}
		return grandTotalExpProperty;
	}






	public void setGrandTotalExpProperty(DoubleProperty grandTotalExpProperty) {
		this.grandTotalExpProperty = grandTotalExpProperty;
	}





	@JsonIgnore
	public DoubleProperty getIncludedExpProperty() {
		if(null!= includedExp) {
			includedExpProperty.set(includedExp);
		}
		return includedExpProperty;
	}






	public void setIncludedExpProperty(DoubleProperty includedExpProperty) {
		this.includedExpProperty = includedExpProperty;
	}





	@JsonIgnore
	public DoubleProperty getImportDutyProperty() {
		if(null!= importDuty) {
			importDutyProperty.set(importDuty);
		}
		return importDutyProperty;
	}






	public void setImportDutyProperty(DoubleProperty importDutyProperty) {
		this.importDutyProperty = importDutyProperty;
	}





	@JsonIgnore
	public StringProperty getDateProperty() {
		
		return dateProperty;
	}






	public void setDateProperty(StringProperty dateProperty) {
		this.dateProperty = dateProperty;
	}






	public void setExpenseProperty(DoubleProperty expenseProperty) {
		this.expenseProperty = expenseProperty;
	}






	@JsonIgnore
	public DoubleProperty getExpenseProperty() {
		if(null!=expense) {
		expenseProperty.set(expense);
		}
		return expenseProperty;
	}






	@Override
	public String toString() {
		return "ImportPurchaseSummary [invoiceNumber=" + invoiceNumber + ", supplierName=" + supplierName
				+ ", supplierInvNo=" + supplierInvNo + ", invoiceDate=" + invoiceDate + ", purchaseOrder="
				+ purchaseOrder + ", itemTotal=" + itemTotal + ", itemTotalFc=" + itemTotalFc + ", expense=" + expense
				+ ", fcExpense=" + fcExpense + ", notIncludedExp=" + notIncludedExp + ", includedExp=" + includedExp
				+ ", expenseTotal=" + expenseTotal + ", grandTotal=" + grandTotal + ", importDuty=" + importDuty
				+ ", currencyName=" + currencyName + ", invoiceNumberProperty=" + invoiceNumberProperty
				+ ", supplierNameProperty=" + supplierNameProperty + ", supplierInvNoProperty=" + supplierInvNoProperty
				+ ", itemTotalProperty=" + itemTotalProperty + ", itemTotalFcProperty=" + itemTotalFcProperty
				+ ", expenseProperty=" + expenseProperty + ", fcExpenseProperty=" + fcExpenseProperty
				+ ", notIncludedExpProperty=" + notIncludedExpProperty + ", grandTotalExpProperty="
				+ grandTotalExpProperty + ", includedExpProperty=" + includedExpProperty + ", importDutyProperty="
				+ importDutyProperty + ", dateProperty=" + dateProperty + "]";
	}


	








}
