package com.maple.report.entity;

import java.util.Date;

import org.springframework.stereotype.Component;
public class PatientSalesReport {
	String id;
	String patientName;
	String nationalId;
	String address1;
	String address2;
	String hospitalId;
	String contactPerson;
	String insuranceCompanyId;
	String insuranceCard;
	String policyType;
	String gender;
	String percentageCoverage;
	String phoneNumber;
	Date dateOfBirth;
	String nationality;
	String patientType;
	Date insuranceCardExpiry;
	private String  processInstanceId;
	private String taskId;
}
