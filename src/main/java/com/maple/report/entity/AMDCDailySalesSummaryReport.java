package com.maple.report.entity;

import java.io.Serializable;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AMDCDailySalesSummaryReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
String Id;	
String branch;
Double totalCashSales;
Double totalCardSales;
Double totalCreditSales;
Double totalBankSales;
Double totalInsuranceSales;
Double grandTotal;


@JsonIgnore
private StringProperty branchProperty;

@JsonIgnore
private DoubleProperty totalCashSalesProperty;

@JsonIgnore
private DoubleProperty totalCardSalesProperty;

@JsonIgnore
private DoubleProperty totalCreditSalesProperty;

@JsonIgnore
private DoubleProperty totalBankSalesProperty;

@JsonIgnore
private DoubleProperty totalInsuranceSalesProperty;

@JsonIgnore
private DoubleProperty grandTotalProperty;



public AMDCDailySalesSummaryReport() {
	
	
	this.branchProperty = new SimpleStringProperty("");
	this.totalCashSalesProperty = new SimpleDoubleProperty(0.0);
	this.totalCardSalesProperty = new SimpleDoubleProperty(0.0);
	this.totalCreditSalesProperty = new SimpleDoubleProperty(0.0);
	this.totalBankSalesProperty = new SimpleDoubleProperty(0.0);
	this.totalInsuranceSalesProperty=new SimpleDoubleProperty(0.0);
	this.grandTotalProperty=new SimpleDoubleProperty(0.0);
}



public String getBranch() {
	return branch;
}



public void setBranch(String branch) {
	this.branch = branch;
}



public Double getTotalCashSales() {
	return totalCashSales;
}



public void setTotalCashSales(Double totalCashSales) {
	this.totalCashSales = totalCashSales;
}



public Double getTotalCardSales() {
	return totalCardSales;
}



public void setTotalCardSales(Double totalCardSales) {
	this.totalCardSales = totalCardSales;
}



public Double getTotalCreditSales() {
	return totalCreditSales;
}



public void setTotalCreditSales(Double totalCreditSales) {
	this.totalCreditSales = totalCreditSales;
}



public Double getTotalBankSales() {
	return totalBankSales;
}



public void setTotalBankSales(Double totalBankSales) {
	this.totalBankSales = totalBankSales;
}



public Double getTotalInsuranceSales() {
	return totalInsuranceSales;
}



public void setTotalInsuranceSales(Double totalInsuranceSales) {
	this.totalInsuranceSales = totalInsuranceSales;
}



public Double getGrandTotal() {
	return grandTotal;
}



public void setGrandTotal(Double grandTotal) {
	this.grandTotal = grandTotal;
}


@JsonIgnore
public StringProperty getBranchProperty() {
	if(null != branch) {
	branchProperty.set(branch);
	}
	return branchProperty;
}



public void setBranchProperty(StringProperty branchProperty) {
	this.branchProperty = branchProperty;
}


@JsonIgnore
public DoubleProperty getTotalCashSalesProperty() {
	if(null != totalCashSales) {
	totalCashSalesProperty.set(totalCashSales);
	}
	return totalCashSalesProperty;
}



public void setTotalCashSalesProperty(DoubleProperty totalCashSalesProperty) {
	this.totalCashSalesProperty = totalCashSalesProperty;
}


@JsonIgnore
public DoubleProperty getTotalCardSalesProperty() {
	if(null != totalCardSales) {
	totalCardSalesProperty.set(totalCardSales);
	}
	return totalCardSalesProperty;
}



public void setTotalCardSalesProperty(DoubleProperty totalCardSalesProperty) {
	this.totalCardSalesProperty = totalCardSalesProperty;
}


@JsonIgnore
public DoubleProperty getTotalCreditSalesProperty() {
	if(null != totalCreditSales) {
	totalCreditSalesProperty.set(totalCreditSales);
	}
	return totalCreditSalesProperty;
}



public void setTotalCreditSalesProperty(DoubleProperty totalCreditSalesProperty) {
	this.totalCreditSalesProperty = totalCreditSalesProperty;
}


@JsonIgnore
public DoubleProperty getTotalBankSalesProperty() {
	if(null != totalBankSales) {
	totalBankSalesProperty.set(totalBankSales);
	}
	return totalBankSalesProperty;
}



public void setTotalBankSalesProperty(DoubleProperty totalBankSalesProperty) {
	this.totalBankSalesProperty = totalBankSalesProperty;
}


@JsonIgnore
public DoubleProperty getTotalInsuranceSalesProperty() {
	if(null != totalInsuranceSales) {
	totalInsuranceSalesProperty.set(totalInsuranceSales);
	}
	return totalInsuranceSalesProperty;
}



public void setTotalInsuranceSalesProperty(DoubleProperty totalInsuranceSalesProperty) {
	this.totalInsuranceSalesProperty = totalInsuranceSalesProperty;
}


@JsonIgnore
public DoubleProperty getGrandTotalProperty() {
	if(null != grandTotal) {
	grandTotalProperty.set(grandTotal);
	}
	return grandTotalProperty;
}



public void setGrandTotalProperty(DoubleProperty grandTotalProperty) {
	this.grandTotalProperty = grandTotalProperty;
}




public String getId() {
	return Id;
}



public void setId(String id) {
	Id = id;
}



@Override
public String toString() {
	return "AMDCDailySalesSummaryReport [Id=" + Id + ", branch=" + branch + ", totalCashSales=" + totalCashSales
			+ ", totalCardSales=" + totalCardSales + ", totalCreditSales=" + totalCreditSales + ", totalBankSales="
			+ totalBankSales + ", totalInsuranceSales=" + totalInsuranceSales + ", grandTotal=" + grandTotal
			+ ", branchProperty=" + branchProperty + ", totalCashSalesProperty=" + totalCashSalesProperty
			+ ", totalCardSalesProperty=" + totalCardSalesProperty + ", totalCreditSalesProperty="
			+ totalCreditSalesProperty + ", totalBankSalesProperty=" + totalBankSalesProperty
			+ ", totalInsuranceSalesProperty=" + totalInsuranceSalesProperty + ", grandTotalProperty="
			+ grandTotalProperty + "]";
}







}
