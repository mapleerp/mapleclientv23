package com.maple.report.entity;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PurchaseReturnDetailAndSummaryReport {
	
	String returnVoucherDate;
	String purchaseReturnVoucher;
	String supplierName;
	String returnVoucherNum;
	String itemName;
	String groupName;
	String itemCode;
	String batch;
	String expiryDate;
	Double qty;
	Double purchaseRate;
	Double amount;
	String userName;
	Double beforeGST;
	Double gst;
	Double total;
	
	
	StringProperty returnVoucherDateProperty;
	StringProperty purchaseReturnVoucherProperty;
	StringProperty supplierNameProperty;
	StringProperty returnVoucherNumProperty;
	StringProperty itemNameProperty;
	StringProperty groupNameProperty;
	StringProperty itemCodeProperty;
	StringProperty batchProperty;
	StringProperty expiryDateProperty;
	StringProperty userNameProperty;
	
	DoubleProperty qtyProperty;
	DoubleProperty purchaseRateProperty;
	DoubleProperty amountProperty;
	DoubleProperty beforeGSTProperty;
	DoubleProperty gstProperty;
	DoubleProperty totalProperty;
	
	public PurchaseReturnDetailAndSummaryReport() {
		
		this.returnVoucherDateProperty = new SimpleStringProperty();
		this.purchaseReturnVoucherProperty = new SimpleStringProperty();
		this.supplierNameProperty =new SimpleStringProperty();
		this.returnVoucherNumProperty = new SimpleStringProperty();
		this.itemNameProperty = new SimpleStringProperty();
		this.groupNameProperty = new SimpleStringProperty();
		this.itemCodeProperty = new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
		this.expiryDateProperty = new SimpleStringProperty();
		this.userNameProperty = new SimpleStringProperty();
		
		this.qtyProperty = new SimpleDoubleProperty();
		this.purchaseRateProperty = new SimpleDoubleProperty();
		this.amountProperty = new SimpleDoubleProperty();
		this.beforeGSTProperty = new SimpleDoubleProperty();
		this.gstProperty = new SimpleDoubleProperty();
		this.totalProperty = new SimpleDoubleProperty();
		
	}

	public String getReturnVoucherDate() {
		return returnVoucherDate;
	}

	public void setReturnVoucherDate(String returnVoucherDate) {
		this.returnVoucherDate = returnVoucherDate;
	}

	public String getPurchaseReturnVoucher() {
		return purchaseReturnVoucher;
	}

	public void setPurchaseReturnVoucher(String purchaseReturnVoucher) {
		this.purchaseReturnVoucher = purchaseReturnVoucher;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getReturnVoucherNum() {
		return returnVoucherNum;
	}

	public void setReturnVoucherNum(String returnVoucherNum) {
		this.returnVoucherNum = returnVoucherNum;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Double getBeforeGST() {
		return beforeGST;
	}

	public void setBeforeGST(Double beforeGST) {
		this.beforeGST = beforeGST;
	}

	public Double getGst() {
		return gst;
	}

	public void setGst(Double gst) {
		this.gst = gst;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public StringProperty getReturnVoucherDateProperty() {
		if(null != returnVoucherDate) {
			returnVoucherDateProperty.set(returnVoucherDate);
		}
		return returnVoucherDateProperty;
	}

	public void setReturnVoucherDateProperty(StringProperty returnVoucherDateProperty) {
		this.returnVoucherDateProperty = returnVoucherDateProperty;
	}

	public StringProperty getPurchaseReturnVoucherProperty() {
		if(null != purchaseReturnVoucher) {
			purchaseReturnVoucherProperty.set(purchaseReturnVoucher);
		}
		return purchaseReturnVoucherProperty;
	}

	public void setPurchaseReturnVoucherProperty(StringProperty purchaseReturnVoucherProperty) {
		this.purchaseReturnVoucherProperty = purchaseReturnVoucherProperty;
	}

	public StringProperty getSupplierNameProperty() {
		if(null != supplierName) {
			supplierNameProperty.set(supplierName);
		}
		return supplierNameProperty;
	}

	public void setSupplierNameProperty(StringProperty supplierNameProperty) {
		this.supplierNameProperty = supplierNameProperty;
	}

	public StringProperty getReturnVoucherNumProperty() {
		if(null != returnVoucherNum) {
			returnVoucherNumProperty.set(returnVoucherNum);
		}
		return returnVoucherNumProperty;
	}

	public void setReturnVoucherNumProperty(StringProperty returnVoucherNumProperty) {
		this.returnVoucherNumProperty = returnVoucherNumProperty;
	}

	public StringProperty getItemNameProperty() {
		if(null != itemName) {
			itemNameProperty.set(itemName);
		}
		return itemNameProperty;
	}

	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}

	public StringProperty getGroupNameProperty() {
		if(null != groupName) {
			groupNameProperty.set(groupName);
		}
		return groupNameProperty;
	}

	public void setGroupNameProperty(StringProperty groupNameProperty) {
		this.groupNameProperty = groupNameProperty;
	}

	public StringProperty getItemCodeProperty() {
		if(null != itemCode) {
			itemCodeProperty.set(itemCode);
		}
		return itemCodeProperty;
	}

	public void setItemCodeProperty(StringProperty itemCodeProperty) {
		this.itemCodeProperty = itemCodeProperty;
	}

	public StringProperty getBatchProperty() {
		if(null != batch) {
			batchProperty.set(batch);
		}
		return batchProperty;
	}

	public void setBatchProperty(StringProperty batchProperty) {
		this.batchProperty = batchProperty;
	}

	public StringProperty getExpiryDateProperty() {
		if(null != expiryDate) {
			expiryDateProperty.set(expiryDate);
		}
		return expiryDateProperty;
	}

	public void setExpiryDateProperty(StringProperty expiryDateProperty) {
		this.expiryDateProperty = expiryDateProperty;
	}

	public StringProperty getUserNameProperty() {
		if(null != userName) {
			userNameProperty.set(userName);
		}
		return userNameProperty;
	}

	public void setUserNameProperty(StringProperty userNameProperty) {
		this.userNameProperty = userNameProperty;
	}

	public DoubleProperty getQtyProperty() {
		if(null != qty) {
			qtyProperty.set(qty);
		}
		return qtyProperty;
	}

	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}

	public DoubleProperty getPurchaseRateProperty() {
		if(null != purchaseRate) {
			purchaseRateProperty.set(purchaseRate);
		}
		return purchaseRateProperty;
	}

	public void setPurchaseRateProperty(DoubleProperty purchaseRateProperty) {
		this.purchaseRateProperty = purchaseRateProperty;
	}

	public DoubleProperty getAmountProperty() {
		if(null != amount) {
			amountProperty.set(amount);
		}
		return amountProperty;
	}

	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}

	public DoubleProperty getBeforeGSTProperty() {
		if(null != beforeGST) {
			beforeGSTProperty.set(beforeGST);
		}
		return beforeGSTProperty;
	}

	public void setBeforeGSTProperty(DoubleProperty beforeGSTProperty) {
		this.beforeGSTProperty = beforeGSTProperty;
	}

	public DoubleProperty getGstProperty() {
		if(null != gst) {
			gstProperty.set(gst);
		}
		return gstProperty;
	}

	public void setGstProperty(DoubleProperty gstProperty) {
		this.gstProperty = gstProperty;
	}

	public DoubleProperty getTotalProperty() {
		if(null != total) {
			totalProperty.set(total);
		}
		return totalProperty;
	}

	public void setTotalProperty(DoubleProperty totalProperty) {
		this.totalProperty = totalProperty;
	}

	@Override
	public String toString() {
		return "PurchaseReturnDetailAndSummaryReport [returnVoucherDate=" + returnVoucherDate
				+ ", purchaseReturnVoucher=" + purchaseReturnVoucher + ", supplierName=" + supplierName
				+ ", returnVoucherNum=" + returnVoucherNum + ", itemName=" + itemName + ", groupName=" + groupName
				+ ", itemCode=" + itemCode + ", batch=" + batch + ", expiryDate=" + expiryDate + ", qty=" + qty
				+ ", purchaseRate=" + purchaseRate + ", amount=" + amount + ", userName=" + userName + ", beforeGST="
				+ beforeGST + ", gst=" + gst + ", total=" + total + ", returnVoucherDateProperty="
				+ returnVoucherDateProperty + ", purchaseReturnVoucherProperty=" + purchaseReturnVoucherProperty
				+ ", supplierNameProperty=" + supplierNameProperty + ", returnVoucherNumProperty="
				+ returnVoucherNumProperty + ", itemNameProperty=" + itemNameProperty + ", groupNameProperty="
				+ groupNameProperty + ", itemCodeProperty=" + itemCodeProperty + ", batchProperty=" + batchProperty
				+ ", expiryDateProperty=" + expiryDateProperty + ", userNameProperty=" + userNameProperty
				+ ", qtyProperty=" + qtyProperty + ", purchaseRateProperty=" + purchaseRateProperty
				+ ", amountProperty=" + amountProperty + ", beforeGSTProperty=" + beforeGSTProperty + ", gstProperty="
				+ gstProperty + ", totalProperty=" + totalProperty + "]";
	}
	
	
	

}
