package com.maple.report.entity;

import java.util.Date;

public class SaleOrderDueReport {
	
	Date dueDate;
	String day;
	String time;
	String otName;
	String paymentMode;
	Double advance;
	Double balance;
	Double total;
	String messageToPrint;
	
	
	public String getMessageToPrint() {
		return messageToPrint;
	}
	public void setMessageToPrint(String messageToPrint) {
		this.messageToPrint = messageToPrint;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getOtName() {
		return otName;
	}
	public void setOtName(String otName) {
		this.otName = otName;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Double getAdvance() {
		return advance;
	}
	public void setAdvance(Double advance) {
		this.advance = advance;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "SaleOrderDueReport [dueDate=" + dueDate + ", day=" + day + ", time=" + time + ", otName=" + otName
				+ ", paymentMode=" + paymentMode + ", advance=" + advance + ", balance=" + balance + ", total=" + total
				+ ", messageToPrint=" + messageToPrint + "]";
	}

	
	
	

}
