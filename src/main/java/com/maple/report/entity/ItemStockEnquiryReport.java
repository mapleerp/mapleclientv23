package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemStockEnquiryReport {
	
	private String itemName;
	private String voucherNumber;
	private Date voucherDate;
	private Double qty;
	private Double amount;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	@JsonIgnore
	private DoubleProperty amountProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	
	
	public ItemStockEnquiryReport() {
		this.itemNameProperty = new SimpleStringProperty();
		this.voucherNumberProperty = new SimpleStringProperty();
		this.voucherDateProperty = new SimpleStringProperty();

		this.qtyProperty =  new SimpleDoubleProperty();
		this.amountProperty =  new SimpleDoubleProperty();


	}
	
	
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	
	

	
	
	public StringProperty getItemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public StringProperty getVoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}
	public void setVoucherNumberProperty(StringProperty voucherNumberProperty) {
		this.voucherNumberProperty = voucherNumberProperty;
	}
	public StringProperty getVoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate, "dd-MM-yyyy"));
		return voucherDateProperty;
	}
	public void setVoucherDateProperty(StringProperty voucherDateProperty) {
		this.voucherDateProperty = voucherDateProperty;
	}
	public DoubleProperty getAmountProperty() {
		amountProperty.set(amount);
		return amountProperty;
	}
	public void setAmountProperty(DoubleProperty amountProperty) {
		this.amountProperty = amountProperty;
	}
	public DoubleProperty getQtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}
	public void setQtyProperty(DoubleProperty qtyProperty) {
		this.qtyProperty = qtyProperty;
	}
	
	
	@Override
	public String toString() {
		return "ItemStockEnquiryReport [itemName=" + itemName + ", voucherNumber=" + voucherNumber + ", voucherDate="
				+ voucherDate + ", qty=" + qty + ", amount=" + amount + ", itemNameProperty=" + itemNameProperty
				+ ", voucherNumberProperty=" + voucherNumberProperty + ", voucherDateProperty=" + voucherDateProperty
				+ ", amountProperty=" + amountProperty + ", qtyProperty=" + qtyProperty + "]";
	}
	
	
	
	

	

}
