package com.maple.report.entity;

import java.time.LocalDate;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemPropertyIntent {
	
	
	private String propertyName;
	private String value;
	
	@JsonIgnore
	StringProperty propertyNameProperty;
	
	@JsonIgnore
	StringProperty valueProperty;
	
	
	public ItemPropertyIntent() {
		
		this.valueProperty = new SimpleStringProperty();
		this.propertyNameProperty = new SimpleStringProperty();

	}
	public String getPropertyName() {
		return propertyName;
	}
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	

	public StringProperty getPropertyNameProperty() {
		
		propertyNameProperty.set(propertyName);
		return propertyNameProperty;
	}
	public void setPropertyNameProperty(StringProperty propertyNameProperty) {
		this.propertyNameProperty = propertyNameProperty;
	}
	public StringProperty getValueProperty() {
		valueProperty.set(value);
		return valueProperty;
	}
	public void setValueProperty(StringProperty valueProperty) {
		this.valueProperty = valueProperty;
	}
	
	

	
}
