package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CategoryWiseStockMovement {
	
	String category;
	Double openingStock;
	Double inWardQty;
	Double outWardQty;
	Double closingStock;
	Date voucherDate;
	
	@JsonIgnore
	private StringProperty categoryNameProperty;
	
	@JsonIgnore
	private DoubleProperty openingStockProperty;
	
	@JsonIgnore
	private DoubleProperty inWardQtyProperty;
	
	@JsonIgnore
	private DoubleProperty outWardQtyProperty;

	@JsonIgnore
	private DoubleProperty closingStockProperty;
	
	@JsonIgnore
	private StringProperty voucherDateProperty;
	
	public CategoryWiseStockMovement() {
		
		this.categoryNameProperty =new SimpleStringProperty();
		this.openingStockProperty = new SimpleDoubleProperty(0.0);
		this.inWardQtyProperty = new SimpleDoubleProperty(0.0);
		this.outWardQtyProperty = new SimpleDoubleProperty(0.0);
		this.closingStockProperty =new SimpleDoubleProperty(0.0);
		this.voucherDateProperty = new SimpleStringProperty();
	}
	
	
	@JsonIgnore
	public void setcategoryNameProperty(String category) {
		this.category = category;
	}
	public StringProperty getcategoryNameProperty() {
		categoryNameProperty.set(category);
		return categoryNameProperty;
	}
	
	@JsonIgnore
	public void setvoucherDateProperty(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(SystemSetting.UtilDateToString(voucherDate));
		return voucherDateProperty;
	}
	
	@JsonIgnore
	public void setinWardQtyProperty(Double inwardQty) {
		this.inWardQty = inwardQty;
	}
	public DoubleProperty getinWardQtyProperty() {
		inWardQtyProperty.set(inWardQty);
		return inWardQtyProperty;
	}
	
	@JsonIgnore
	public void setopeningStockProperty(Double openingStock) {
		this.openingStock = openingStock;
	}
	public DoubleProperty getopeningStockProperty() {
		openingStockProperty.set(openingStock);
		return openingStockProperty;
	}
	
	
	@JsonIgnore
	public void setoutWardQtyProperty(Double outWardQty) {
		this.outWardQty = outWardQty;
	}
	public DoubleProperty getoutWardQtyProperty() {
		outWardQtyProperty.set(outWardQty);
		return outWardQtyProperty;
	}
	
	@JsonIgnore
	public void setclosingStockProperty(Double closingStock) {
		this.closingStock = closingStock;
	}
	public DoubleProperty getclosingStockProperty() {
		closingStockProperty.set(closingStock);
		return closingStockProperty;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Double getOpeningStock() {
		return openingStock;
	}
	public void setOpeningStock(Double openingStock) {
		this.openingStock = openingStock;
	}
	public Double getInWardQty() {
		return inWardQty;
	}
	public void setInWardQty(Double inWardQty) {
		this.inWardQty = inWardQty;
	}
	public Double getOutWardQty() {
		return outWardQty;
	}
	public void setOutWardQty(Double outWardQty) {
		this.outWardQty = outWardQty;
	}
	public Double getClosingStock() {
		return closingStock;
	}
	public void setClosingStock(Double closingStock) {
		this.closingStock = closingStock;
	}
	@Override
	public String toString() {
		return "CategoryWiseStockMovement [category=" + category + ", openingStock=" + openingStock + ", inWardQty="
				+ inWardQty + ", outWardQty=" + outWardQty + ", closingStock=" + closingStock + "]";
	}
	
	

}
