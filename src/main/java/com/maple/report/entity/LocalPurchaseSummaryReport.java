package com.maple.report.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maple.maple.util.SystemSetting;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LocalPurchaseSummaryReport {

    private Date date;
	
	private String invoiceNumber;
	
	private String supplier;
	
	private String tinNo;
	
	private String supplierInvoiceNumber;
	
	private Date invoiceDate;
	
	private String pONumber;
	
	private Double netInvoice;
	
	private Double beforeGst;
	
	private Double gst;
	
	private String user;
	
	@JsonIgnore
	private StringProperty invoiceNumberProperty;
	@JsonIgnore
	private StringProperty supplierProperty;
	@JsonIgnore
	private StringProperty tinNoProperty;
	@JsonIgnore
	private StringProperty supplierInvoiceNumberProperty;
	@JsonIgnore
	private StringProperty invoiceDateProperty;
	@JsonIgnore
	private StringProperty pONumberProperty;
	@JsonIgnore
	private DoubleProperty netInvoiceProperty;
	@JsonIgnore
	private DoubleProperty beforeGstProperty;
	@JsonIgnore
	private DoubleProperty gstProperty;
	@JsonIgnore
	private StringProperty userProperty;
	
	
	public LocalPurchaseSummaryReport()
	{
		this.invoiceNumberProperty = new SimpleStringProperty();
		this.supplierProperty= new SimpleStringProperty();
		this.tinNoProperty = new SimpleStringProperty();
		this.supplierInvoiceNumberProperty =new SimpleStringProperty();
		this.invoiceDateProperty = new SimpleStringProperty();
		this.pONumberProperty = new SimpleStringProperty();
		this.netInvoiceProperty = new SimpleDoubleProperty();
		this.beforeGstProperty = new SimpleDoubleProperty();
		this.gstProperty = new SimpleDoubleProperty();
		this.userProperty = new SimpleStringProperty();
		
	}
	

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getTinNo() {
		return tinNo;
	}

	public void setTinNo(String tinNo) {
		this.tinNo = tinNo;
	}

	public String getSupplierInvoiceNumber() {
		return supplierInvoiceNumber;
	}

	public void setSupplierInvoiceNumber(String supplierInvoiceNumber) {
		this.supplierInvoiceNumber = supplierInvoiceNumber;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getpONumber() {
		return pONumber;
	}

	public void setpONumber(String pONumber) {
		this.pONumber = pONumber;
	}

	public Double getNetInvoice() {
		return netInvoice;
	}

	public void setNetInvoice(Double netInvoice) {
		this.netInvoice = netInvoice;
	}

	public Double getBeforeGst() {
		return beforeGst;
	}

	public void setBeforeGst(Double beforeGst) {
		this.beforeGst = beforeGst;
	}

	public Double getGst() {
		return gst;
	}

	public void setGst(Double gst) {
		this.gst = gst;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	

	public StringProperty getInvoiceNumberProperty() {
		if(null!=invoiceNumber) {
			invoiceNumberProperty.set(invoiceNumber);
		}
		
		return invoiceNumberProperty;
	}

	public void setInvoiceNumberProperty(StringProperty invoiceNumberProperty) {
		this.invoiceNumberProperty = invoiceNumberProperty;
	}

	public StringProperty getSupplierProperty() {
		
		if(null!=supplier) {
			supplierProperty.set(supplier);
		}
		return supplierProperty;
	}

	public void setSupplierProperty(StringProperty supplierProperty) {
		this.supplierProperty = supplierProperty;
	}

	public StringProperty getTinNoProperty() {
		if(null!=tinNo) {
			tinNoProperty.set(tinNo);
		}
		
		return tinNoProperty;
	}

	public void setTinNoProperty(StringProperty tinNoProperty) {
		this.tinNoProperty = tinNoProperty;
	}

	public StringProperty getSupplierInvoiceNumberProperty() {
		if(null!=supplierInvoiceNumber) {
			supplierInvoiceNumberProperty.set(supplierInvoiceNumber);
		}
		return supplierInvoiceNumberProperty;
	}

	public void setSupplierInvoiceNumberProperty(StringProperty supplierInvoiceNumberProperty) {
		this.supplierInvoiceNumberProperty = supplierInvoiceNumberProperty;
	}

	public StringProperty getInvoiceDateProperty() {
		if(null!=invoiceDate) {
			invoiceDateProperty.set(SystemSetting.UtilDateToString(invoiceDate, "yyyy-MM-dd"));
		}
		return invoiceDateProperty;
	}

	public void setInvoiceDateProperty(StringProperty invoiceDateProperty) {
		this.invoiceDateProperty = invoiceDateProperty;
	}

	public StringProperty getpONumberProperty() {
		if(null!=pONumber) {
			pONumberProperty.set(pONumber);
		}
		return pONumberProperty;
	}

	public void setpONumberProperty(StringProperty pONumberProperty) {
		this.pONumberProperty = pONumberProperty;
	}

	public DoubleProperty getNetInvoiceProperty() {
		if(null!=netInvoice) {
			netInvoiceProperty.set(netInvoice);
		}
		return netInvoiceProperty;
	}

	public void setNetInvoiceProperty(DoubleProperty netInvoiceProperty) {
		this.netInvoiceProperty = netInvoiceProperty;
	}

	public DoubleProperty getBeforeGstProperty() {
		if(null!=beforeGst) {
			beforeGstProperty.set(beforeGst);
		}
		return beforeGstProperty;
	}

	public void setBeforeGstProperty(DoubleProperty beforeGstProperty) {
		this.beforeGstProperty = beforeGstProperty;
	}

	public DoubleProperty getGstProperty() {
		if(null!=gst) {
			gstProperty.set(gst);
		}
		return gstProperty;
	}

	public void setGstProperty(DoubleProperty gstProperty) {
		this.gstProperty = gstProperty;
	}

	public StringProperty getUserProperty() {
		if(null!=user) {
			userProperty.set(user);
		}
		return userProperty;
	}

	public void setUserProperty(StringProperty userProperty) {
		this.userProperty = userProperty;
	}

	@Override
	public String toString() {
		return "LocalPurchaseSummaryReport [date=" + date + ", invoiceNumber=" + invoiceNumber + ", supplier="
				+ supplier + ", tinNo=" + tinNo + ", supplierInvoiceNumber=" + supplierInvoiceNumber + ", invoiceDate="
				+ invoiceDate + ", pONumber=" + pONumber + ", netInvoice=" + netInvoice + ", beforeGst=" + beforeGst
				+ ", gst=" + gst + ", user=" + user + ", invoiceNumberProperty=" + invoiceNumberProperty
				+ ", supplierProperty=" + supplierProperty + ", tinNoProperty=" + tinNoProperty
				+ ", supplierInvoiceNumberProperty=" + supplierInvoiceNumberProperty + ", invoiceDateProperty="
				+ invoiceDateProperty + ", pONumberProperty=" + pONumberProperty + ", netInvoiceProperty="
				+ netInvoiceProperty + ", beforeGstProperty=" + beforeGstProperty + ", gstProperty=" + gstProperty
				+ ", userProperty=" + userProperty + "]";
	}

	
}
