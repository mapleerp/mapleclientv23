package com.maple.report.entity;

import java.util.Date;

public class DamageEntryReport {
	String companyName;
	String branchName;
	String branchCode;
	
	Date toDate;
	Date fromDate; 
	
	String itemName;
	Double qty;
	String batchCode;
	Date voucherDate;
	String narration;
	
	String voucheNumber;
	Double amount;
	String department;
	String user;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getBatchCode() {
		return batchCode;
	}
	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}
	public Date getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getVoucheNumber() {
		return voucheNumber;
	}
	public void setVoucheNumber(String voucheNumber) {
		this.voucheNumber = voucheNumber;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "DamageEntryReport [companyName=" + companyName + ", branchName=" + branchName + ", branchCode="
				+ branchCode + ", toDate=" + toDate + ", fromDate=" + fromDate + ", itemName=" + itemName + ", qty="
				+ qty + ", batchCode=" + batchCode + ", voucherDate=" + voucherDate + ", narration=" + narration
				+ ", voucheNumber=" + voucheNumber + ", amount=" + amount + ", department=" + department + ", user="
				+ user + "]";
	}
	
	

	

}
