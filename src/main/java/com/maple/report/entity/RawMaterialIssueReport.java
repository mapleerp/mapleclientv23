package com.maple.report.entity;

import java.sql.Date;
import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class RawMaterialIssueReport {

	String itemName;
	String batch;
	Double qty;
	String voucherNumber;
	String unitName;
	

	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private StringProperty batchProperty;
	
	@JsonIgnore
	private StringProperty voucherNumberProperty;
	
	@JsonIgnore
	private StringProperty unitNameProperty;
	
	@JsonIgnore
	private DoubleProperty qtyProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;
	
	
	
	
	public RawMaterialIssueReport() {
		
		this.itemNameProperty = new SimpleStringProperty();
		this.batchProperty = new SimpleStringProperty();
		this.voucherNumberProperty =new SimpleStringProperty();
		this.unitNameProperty = new SimpleStringProperty();
		this.qtyProperty =new SimpleDoubleProperty();
		this.voucherDate = new SimpleObjectProperty<LocalDate>(null);
	}
	
	@JsonProperty
	public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
		return voucherDate;
	}
 
	public void setvoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
		this.voucherDate = voucherDate;
	}
	@JsonProperty("voucherDate")
	public java.sql.Date getvoucherDate ( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}
	
   public void setvoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}

	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@JsonIgnore
	public StringProperty getitemNameProperty() {
		itemNameProperty.set(itemName);
		return itemNameProperty;
	}

	public void setitemNameProperty(String itemName) {
		this.itemName = itemName;
	}
	@JsonIgnore
	public StringProperty getbatchProperty() {
		batchProperty.set(batch);
		return batchProperty;
	}

	public void setbatchProperty(String batch) {
		this.batch = batch;
	}
	@JsonIgnore
	public StringProperty getvoucherNumberProperty() {
		voucherNumberProperty.set(voucherNumber);
		return voucherNumberProperty;
	}

	public void setvoucherNumberProperty(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	@JsonIgnore
	public StringProperty getunitNameProperty() {
		unitNameProperty.set(unitName);
		return unitNameProperty;
	}

	public void setunitNameProperty(String unitName) {
		this.unitName = unitName;
	}
	@JsonIgnore
	public DoubleProperty getqtyProperty() {
		qtyProperty.set(qty);
		return qtyProperty;
	}

	public void setqtyProperty(Double qty) {
		this.qty = qty;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	
	@Override
	public String toString() {
		return "RawMaterialIssueReport [itemName=" + itemName + ", batch=" + batch + ", qty=" + qty + ", voucherNumber="
				+ voucherNumber + ", voucherDate=" + voucherDate + ", unitName=" + unitName + "]";
	}
	
	
}
