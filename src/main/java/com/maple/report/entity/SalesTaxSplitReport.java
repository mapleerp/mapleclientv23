package com.maple.report.entity;

import java.util.Date;
import java.util.List;

 

public class SalesTaxSplitReport {

	Double taxableValue;
	Double taxAmount;
	Double taxRate;

	public Double getTaxableValue() {
		return taxableValue;
	}
	public void setTaxableValue(Double taxableValue) {
		this.taxableValue = taxableValue;
	}
	public Double getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	@Override
	public String toString() {
		return "SalesTaxSplitReport [taxableValue=" + taxableValue + ", taxAmount=" + taxAmount + ", taxRate=" + taxRate
				+ "]";
	}
	
	
}
