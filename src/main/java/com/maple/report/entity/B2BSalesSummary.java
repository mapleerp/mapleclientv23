package com.maple.report.entity;

import java.util.Date;

public class B2BSalesSummary {
	
	String branchCode;
	String branchName;
	String branchAddress;
	String branchPhone;
	
	String companyName;
	String branchEmail;
	String branchWebsite;
	
	
	
	Date date;
	String customerName;
	 String gstIn;
	 
	 String salesMan;
	 String saBillNo;
	 Double advance;
	 Double cash;
	 Double credit;
	 
	 Double cardPayment;
	 Double netTotal;
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getBranchPhone() {
		return branchPhone;
	}
	public void setBranchPhone(String branchPhone) {
		this.branchPhone = branchPhone;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getBranchEmail() {
		return branchEmail;
	}
	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}
	public String getBranchWebsite() {
		return branchWebsite;
	}
	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getGstIn() {
		return gstIn;
	}
	public void setGstIn(String gstIn) {
		this.gstIn = gstIn;
	}
	public String getSalesMan() {
		return salesMan;
	}
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}
	public String getSaBillNo() {
		return saBillNo;
	}
	public void setSaBillNo(String saBillNo) {
		this.saBillNo = saBillNo;
	}
	public Double getAdvance() {
		return advance;
	}
	public void setAdvance(Double advance) {
		this.advance = advance;
	}
	public Double getCash() {
		return cash;
	}
	public void setCash(Double cash) {
		this.cash = cash;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public Double getCardPayment() {
		return cardPayment;
	}
	public void setCardPayment(Double cardPayment) {
		this.cardPayment = cardPayment;
	}
	public Double getNetTotal() {
		return netTotal;
	}
	public void setNetTotal(Double netTotal) {
		this.netTotal = netTotal;
	}
	@Override
	public String toString() {
		return "B2BSalesSummary [branchCode=" + branchCode + ", branchName=" + branchName + ", branchAddress="
				+ branchAddress + ", branchPhone=" + branchPhone + ", companyName=" + companyName + ", branchEmail="
				+ branchEmail + ", branchWebsite=" + branchWebsite + ", date=" + date + ", customerName=" + customerName
				+ ", gstIn=" + gstIn + ", salesMan=" + salesMan + ", saBillNo=" + saBillNo + ", advance=" + advance
				+ ", cash=" + cash + ", credit=" + credit + ", cardPayment=" + cardPayment + ", netTotal=" + netTotal
				+ "]";
	}
	 
	

}
