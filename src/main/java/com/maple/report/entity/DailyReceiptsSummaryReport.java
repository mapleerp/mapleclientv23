package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class DailyReceiptsSummaryReport {

	String receiptMode;
	Double totalCash;
	String debitAccount;
	
	
	
	public DailyReceiptsSummaryReport() {
		this.receiptModeProperty = new SimpleStringProperty("");
		this.debitAccountProperty = new SimpleStringProperty("");
		this.totalCashProperty = new SimpleDoubleProperty(0.0);
	}
	@JsonIgnore
	private StringProperty receiptModeProperty;
	
	@JsonIgnore
	private StringProperty debitAccountProperty;
	
	@JsonIgnore
	private DoubleProperty totalCashProperty;
	
	
	
	public String getReceiptMode() {
		return receiptMode;
	}
	public void setReceiptMode(String receiptMode) {
		this.receiptMode = receiptMode;
	}
	public Double getTotalCash() {
		return totalCash;
	}
	public void setTotalCash(Double totalCash) {
		this.totalCash = totalCash;
	}
	public String getDebitAccount() {
		return debitAccount;
	}
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}
	public StringProperty getReceiptModeProperty() {
		receiptModeProperty.set(receiptMode);
		return receiptModeProperty;
	}
	public void setReceiptModeProperty(StringProperty receiptModeProperty) {
		this.receiptModeProperty = receiptModeProperty;
	}
	public StringProperty getDebitAccountProperty() {
		debitAccountProperty.set(debitAccount);
		return debitAccountProperty;
	}
	public void setDebitAccountProperty(StringProperty debitAccountProperty) {
		this.debitAccountProperty = debitAccountProperty;
	}
	public DoubleProperty getTotalCashProperty() {
		totalCashProperty.set(totalCash);
		return totalCashProperty;
	}
	public void setTotalCashProperty(DoubleProperty totalCashProperty) {
		this.totalCashProperty = totalCashProperty;
	}
	@Override
	public String toString() {
		return "DailyReceiptsSummaryReport [receiptMode=" + receiptMode + ", totalCash=" + totalCash + ", debitAccount="
				+ debitAccount + ", receiptModeProperty=" + receiptModeProperty + ", debitAccountProperty="
				+ debitAccountProperty + ", totalCashProperty=" + totalCashProperty + "]";
	}
	
	
	
	
}
