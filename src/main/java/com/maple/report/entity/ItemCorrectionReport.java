package com.maple.report.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemCorrectionReport {

	
	String itemId;
	String itemName;
	Double salesCount;
	Double stockCount;
	Double purchaseCount;
	
	@JsonIgnore
	private StringProperty itemNameProperty;
	
	@JsonIgnore
	private DoubleProperty salesCountProperty;
	@JsonIgnore
	private DoubleProperty stockCountProperty;
	@JsonIgnore
	private DoubleProperty purchaseCountProperty;
	
	public ItemCorrectionReport()
	{
		this.itemNameProperty = new SimpleStringProperty("");
		this.purchaseCountProperty =new SimpleDoubleProperty(0.0);
		this.salesCountProperty =new SimpleDoubleProperty(0.0);
		this.stockCountProperty =new SimpleDoubleProperty(0.0);
	}
	
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getSalesCount() {
		return salesCount;
	}
	public void setSalesCount(Double salesCount) {
		this.salesCount = salesCount;
	}
	public Double getStockCount() {
		return stockCount;
	}
	public void setStockCount(Double stockCount) {
		this.stockCount = stockCount;
	}
	public Double getPurchaseCount() {
		return purchaseCount;
	}
	public void setPurchaseCount(Double purchaseCount) {
		this.purchaseCount = purchaseCount;
	}
	
	public StringProperty getItemNameProperty() {
		if(null != itemName) {
			itemNameProperty.set(itemName);
			}
		
		return itemNameProperty;
	}
	public void setItemNameProperty(StringProperty itemNameProperty) {
		this.itemNameProperty = itemNameProperty;
	}
	public DoubleProperty getSalesCountProperty() {
		
		if(null != salesCount) {
			salesCountProperty.set(salesCount);
			}
		return salesCountProperty;
	}
	public void setSalesCountProperty(DoubleProperty salesCountProperty) {
		this.salesCountProperty = salesCountProperty;
	}
	public DoubleProperty getStockCountProperty() {
		if(null != stockCount) {
			stockCountProperty.set(stockCount);
			}
		return stockCountProperty;
	}
	public void setStockCountProperty(DoubleProperty stockCountProperty) {
		this.stockCountProperty = stockCountProperty;
	}
	public DoubleProperty getPurchaseCountProperty() {
		if(null != purchaseCount) {
			purchaseCountProperty.set(purchaseCount);
			}
		return purchaseCountProperty;
	}
	public void setPurchaseCountProperty(DoubleProperty purchaseCountProperty) {
		this.purchaseCountProperty = purchaseCountProperty;
	}
	@Override
	public String toString() {
		return "ItemCorrectionReport [itemId=" + itemId + ", itemName=" + itemName + ", salesCount=" + salesCount
				+ ", stockCount=" + stockCount + ", purchaseCount=" + purchaseCount + ", itemNameProperty="
				+ itemNameProperty + ", salesCountProperty=" + salesCountProperty + ", stockCountProperty="
				+ stockCountProperty + ", purchaseCountProperty=" + purchaseCountProperty + "]";
	}
	
	
	
	
	
}
