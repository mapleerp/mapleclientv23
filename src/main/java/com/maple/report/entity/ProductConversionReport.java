package com.maple.report.entity;

import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductConversionReport {

	String fromItemName;
	String toItemName;
	Double fromItemQty;
	Double toItemQty;
	
	Double fromAmount;
	Double toAmount;
	
	@JsonIgnore
	private StringProperty fromItemNameProperty;
	
	@JsonIgnore
	private StringProperty toItemNameProperty;
	
	@JsonIgnore
	private DoubleProperty fromQtyProperty;
	
	@JsonIgnore
	private DoubleProperty toQtyProperty;
	
	@JsonIgnore
	private DoubleProperty fromAmountProperty;

	@JsonIgnore
	private DoubleProperty toAmountProperty;
	
	@JsonIgnore
	private ObjectProperty<LocalDate> voucherDate;
	
	
	

	public ProductConversionReport() {
		
		this.fromItemNameProperty = new SimpleStringProperty();
		this.toItemNameProperty =  new SimpleStringProperty();
		this.fromQtyProperty = new SimpleDoubleProperty();
		this.toQtyProperty = new SimpleDoubleProperty();
		this.fromAmountProperty = new SimpleDoubleProperty();
		this.toAmountProperty = new SimpleDoubleProperty();
		this.voucherDate =new SimpleObjectProperty<LocalDate>(null);
	}
	public String getFromItemName() {
		return fromItemName;
	}
	public void setFromItemName(String fromItemName) {
		this.fromItemName = fromItemName;
	}
	public String getToItemName() {
		return toItemName;
	}
	public void setToItemName(String toItemName) {
		this.toItemName = toItemName;
	}
	
	public Double getFromItemQty() {
		return fromItemQty;
	}
	public void setFromItemQty(Double fromItemQty) {
		this.fromItemQty = fromItemQty;
	}
	public Double getToItemQty() {
		return toItemQty;
	}
	public void setToItemQty(Double toItemQty) {
		this.toItemQty = toItemQty;
	}
	public Double getFromAmount() {
		return fromAmount;
	}
	public void setFromAmount(Double fromAmount) {
		this.fromAmount = fromAmount;
	}
	public Double getToAmount() {
		return toAmount;
	}
	public void setToAmount(Double toAmount) {
		this.toAmount = toAmount;
	}
	@JsonProperty("voucherDate")
	public java.sql.Date getvoucherDate( ) {
		if(null==this.voucherDate.get() ) {
			return null;
		}else {
			return Date.valueOf(this.voucherDate.get() );
		}
	 
		
	}
	
   public void setvoucherDate (java.sql.Date voucherDateProperty) {
						if(null!=voucherDateProperty)
		this.voucherDate.set(voucherDateProperty.toLocalDate());
	}
   public ObjectProperty<LocalDate> getvoucherDateProperty( ) {
		return voucherDate;
	}

	public void setsourceVoucherDateProperty(ObjectProperty<LocalDate> voucherDate) {
		this.voucherDate = (SimpleObjectProperty<LocalDate>) voucherDate;
	}
	
	
	public StringProperty getfromItemNameProperty() {
		fromItemNameProperty.set(fromItemName);
		return fromItemNameProperty;
	}
	

	public void setfromItemNameProperty(String fromItemName) {
		this.fromItemName = fromItemName;
	}
	
	
	public StringProperty gettoItemNameProperty() {
		toItemNameProperty.set(toItemName);
		return toItemNameProperty;
	}
	

	public void settoItemNameProperty(String toItemName) {
		this.toItemName = toItemName;
	}
	
	
	public DoubleProperty getfromQtyProperty() {
		fromQtyProperty.set(fromItemQty);
		return fromQtyProperty;
	}
	

	public void setfromQtyProperty(Double fromItemQty) {
		this.fromItemQty = fromItemQty;
	}
	
	public DoubleProperty gettoQtyProperty() {
		toQtyProperty.set(toItemQty);
		return toQtyProperty;
	}
	

	public void settoQtyProperty(Double toItemQty) {
		this.toItemQty = toItemQty;
	}
	
	
	public DoubleProperty getfromAmountProperty() {
		fromAmountProperty.set(fromAmount);
		return fromAmountProperty;
	}
	

	public void setfromAmountProperty(Double fromAmount) {
		this.fromAmount = fromAmount;
	}
	
	public DoubleProperty gettoAmountProperty() {
		toAmountProperty.set(toAmount);
		return toAmountProperty;
	}
	

	public void settoAmountProperty(Double toAmount) {
		this.toAmount = toAmount;
	}
	
}
