package com.maple.mapleclient.controllers;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.maple.maple.util.SystemSetting;
import com.sun.net.httpserver.Authenticator.Success;

class possWindowCtlTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	final void testAddItemOnEnter() {
		//fail("Not yet implemented"); // TODO
		assert(1==1);
	}

	@Test
	final void transaction_all_same_date__failure() {
	 
		possWindowCtl var_possWindowCtl = new possWindowCtl();
		Date transaction = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		
		Date applicationDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		Date lastDayEndDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		
		
		assertFalse(var_possWindowCtl.allowTransactionForTheDate(transaction, applicationDate, lastDayEndDate));
	}
	
 
	@Test
	final void transaction_trdate_lates_and_other_date_old_success() {
	
		possWindowCtl var_possWindowCtl = new possWindowCtl();
		Date transaction = SystemSetting.StringToSqlDateSlash(
				"2022-08-09", "yyyy-dd-MM");
		
		
		Date applicationDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		Date lastDayEndDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		
		
		assertTrue(var_possWindowCtl.allowTransactionForTheDate(transaction, applicationDate, lastDayEndDate));
	}
	
 
	
	@Test
	final void   successTransactionEqualtoApplicationDateTransactionLessThanApplDate() {
	 
		possWindowCtl var_possWindowCtl = new possWindowCtl();
		Date transaction = SystemSetting.StringToSqlDateSlash(
				"2022-08-09", "yyyy-dd-MM");
		
		
		Date applicationDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-09", "yyyy-dd-MM");
		
		Date lastDayEndDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-08", "yyyy-dd-MM");
		
		
		
		assertTrue(var_possWindowCtl.allowTransactionForTheDate(transaction, applicationDate, lastDayEndDate));
	}
	
	
	
	
	///-----------------------------loginDate Controller-----------------
	
	
	
	
	@Test
	final void LoginDateGreaterthanApplicationDateDayEndDateLessThanApplDate() {
	 
		LoginController loginController = new LoginController();
		Date login = SystemSetting.StringToSqlDateSlash(
				"2022-08-10", "yyyy-dd-MM");
		
		
		Date applicationDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-08", "yyyy-dd-MM");
		
		Date lastDayEndDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-07", "yyyy-dd-MM");
		
		
		
		assertTrue(loginController.allowLoginForTheDate(login, applicationDate, lastDayEndDate));
	}
	@Test
	final void alldateEqualfailure() {
	 
		LoginController loginController = new LoginController();
		Date login = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		
		Date applicationDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		Date lastDayEndDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		
		
		assertFalse(loginController.allowLoginForTheDate(login, applicationDate, lastDayEndDate));
	}
	
 
	@Test
	final void LoginDateGreater() {
	 
		LoginController loginController = new LoginController();
		Date login = SystemSetting.StringToSqlDateSlash(
				"2022-08-09", "yyyy-dd-MM");
		
		
		Date applicationDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		Date lastDayEndDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-01", "yyyy-dd-MM");
		
		
		
		assertTrue(loginController.allowLoginForTheDate(login, applicationDate, lastDayEndDate));
	}
	

	@Test
	final void   successLoginEqualtoApplicationDate() {
	 
		LoginController loginController = new LoginController();
		Date login = SystemSetting.StringToSqlDateSlash(
				"2022-08-09", "yyyy-dd-MM");
		
		
		Date applicationDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-09", "yyyy-dd-MM");
		
		Date lastDayEndDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-08", "yyyy-dd-MM");
		
		
		
		assertTrue(loginController.allowLoginForTheDate(login, applicationDate, lastDayEndDate));
	}
	
	@Test
	final void   failureLoginGreaterThanDayEndAndLessThanApplDate() {
	 
		LoginController loginController = new LoginController();
		Date login = SystemSetting.StringToSqlDateSlash(
				"2022-08-09", "yyyy-dd-MM");
		
		
		Date applicationDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-10", "yyyy-dd-MM");
		
		Date lastDayEndDate = SystemSetting.StringToSqlDateSlash(
				"2022-08-07", "yyyy-dd-MM");
		
		
		
		assertFalse(loginController.allowLoginForTheDate(login, applicationDate, lastDayEndDate));
	}
	
	 
	
	
	
	
	
	
	
	
	
	
}
