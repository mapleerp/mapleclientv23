package com.maple.mapleclient;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.maple.maple.util.SystemSetting;
import com.maple.mapleclient.entity.AccountHeads;
import com.maple.mapleclient.entity.ItemMst;
import com.maple.mapleclient.entity.SalesDtl;
import com.maple.mapleclient.entity.SalesReceipts;
import com.maple.mapleclient.entity.SalesTransHdr;
import com.maple.mapleclient.restService.RestCaller;

@SpringBootTest
class MapleclientApplicationTests {
//
//	@Test
//	void contextLoads() {
//	}
//	
//	@Test
//	void posInvoice() {
//		
//		SystemSetting.systemDate = SystemSetting.StringToSqlDateSlash("2021-06-23", "yyyy-MM-dd");
//		SystemSetting.systemBranch = "CFE";
//		
//		
//		   		List<Object> items = new ArrayList();
//			
//				items = RestCaller.SearchItemByNameWithComp("m");
//
//				Iterator itr = items.iterator();
//
//			   ArrayList itemList = new ArrayList();
//	 
//				while (itr.hasNext()) {
//	 			List element = (List) itr.next();
//					String itemName = (String) element.get(0);
//		 			String itemId = (String) element.get(6);
//	 				itemList.add(itemId);
//					
//				
//				}
//				
//				
//		
//		// Array to hold number of test invoice
//		int[] invoiceCount = {5,7,20,40,10,20,43,32,5,7,20,40,10};
//		for (int ii : invoiceCount) {
//			
//			int numberOfItems = ii;
//			
//			// Generate a voucher Hdr
//			SalesTransHdr salesTransHdr = new SalesTransHdr();
//			//Set DefaultValue
//
//			ResponseEntity<CustomerMst> customerMstResponse = RestCaller.getCustomerByNameAndBarchCode("POS",
//					"CFE");
//			CustomerMst customerMst = customerMstResponse.getBody();
//
//			salesTransHdr.setCustomerMst(customerMst);
//
//			salesTransHdr.setInvoiceAmount(0.0);
//			salesTransHdr.setUserId(SystemSetting.getUserId());
//			salesTransHdr.setBranchCode("CFE");
//			salesTransHdr.getId();
//			salesTransHdr.setCustomerId(customerMst.getId());
//			salesTransHdr.setSalesMode("POS");
//			salesTransHdr.setVoucherType("SALESTYPE");
//			salesTransHdr.setCreditOrCash("CASH");
//			salesTransHdr.setIsBranchSales("N");
//		 
//
//			ResponseEntity<SalesTransHdr> respentity = RestCaller.saveSalesHdr(salesTransHdr);
//			salesTransHdr = respentity.getBody();
//			
//			double grandTotal = 0;
//			
//			for(int j = 0; j<numberOfItems;j++) {
//					
//					int maxItem = items.size();
//					
//				   int random =  (int) (Math.random() * (maxItem - 1 + 1) + 1)  ;
//				   
//				   if(random==50) {
//					   random=49;
//				   }
//				   
//				   System.out.println("itemList  ="+itemList.size());
//
//				   System.out.println("RANDOM  ="+random);
//				   String itemId =(String) itemList.get(random);
//				   ResponseEntity<ItemMst> itemResp = RestCaller.getitemMst(itemId);
//				   ItemMst item = itemResp.getBody();
//		   
//				   SalesDtl salesDtl = new SalesDtl();
//				   salesDtl.setSalesTransHdr(salesTransHdr);
//				   salesDtl.setItemId(itemId);
//				   salesDtl.setItemName(item.getItemName());
//				   double amount = item.getStandardPrice();
//				   double taxRaet = item.getTaxRate();
//				   double cgst = amount * taxRaet/200;
//				   double sgst = amount * taxRaet/200;
//				   
//				   
//				   salesDtl.setRate(amount);
//				   
//				   salesDtl.setTaxRate(taxRaet);
//				   
//				   salesDtl.setAddCessRate(0.0);
//				   salesDtl.setCessAmount(0.0);
//				   salesDtl.setCessRate(0.0);
//				   
//				   salesDtl.setCgstTaxRate(taxRaet/2);
//				   salesDtl.setSgstTaxRate(taxRaet/2);
//				   
//				   salesDtl.setDiscount(0.0);
//				   salesDtl.setIgstAmount(0.0);
//				   salesDtl.setIgstTaxRate(taxRaet);
//				   
//				   
//				   salesDtl.setMrp(item.getStandardPrice());
//				   salesDtl.setUnitId(item.getUnitId());
//				   
//				   salesDtl.setQty(1.0);
//				   
//				   
//				   
//				   salesDtl.setAmount(amount);
//				   
//				   salesDtl.setCgstAmount(cgst);
//				   salesDtl.setSgstAmount(sgst);
//				   
//				   salesDtl.setBatch("NOBATCH");
//				   salesDtl.setBarcode(item.getBarCode());
//				   grandTotal = grandTotal+ amount+cgst+sgst;
//				   
//				  
//					ResponseEntity<SalesDtl> respentitySales = RestCaller.saveSalesDtl(salesDtl);
//					
//				   
//			}
//			
//			salesTransHdr.setInvoiceAmount(grandTotal);
//			
//			String salesVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
//			 
//			salesTransHdr.setVoucherNumber(salesVoucherNo);
//			
//			SalesReceipts salesReceipts = new SalesReceipts();
//			salesReceipts.setSalesTransHdr(salesTransHdr);
//			salesReceipts.setAccountId(salesVoucherNo);
//			
//			ResponseEntity<AccountHeads> accountHeads = RestCaller
//					.getAccountHeadByName(SystemSetting.systemBranch + "-" + "CASH");
//			salesReceipts.setAccountId(accountHeads.getBody().getId());
//			salesReceipts.setReceiptMode(salesTransHdr.getBranchCode() + "-" + "CASH");
//			
//			String salesRcpVoucherNo = RestCaller.getVoucherNumber(SystemSetting.getSystemBranch());
//			
//			salesReceipts.setVoucherNumber(salesRcpVoucherNo);
//			
//			salesReceipts.setReceiptAmount(grandTotal);
//			
//			salesReceipts.setUserId("CFE000001");
//			salesReceipts.setBranchCode(SystemSetting.systemBranch);
////
////		LocalDate date = LocalDate.now();
////		java.util.Date udate = SystemSetting.localToUtilDate(date);
//			salesReceipts.setReceiptDate(SystemSetting.systemDate);
//			salesReceipts.setSalesTransHdr(salesTransHdr);
//			 
//			ResponseEntity<SalesReceipts> respEntity = RestCaller.saveSalesReceipts(salesReceipts);
//			
//			
//			
//		}
//		
//	}
//	
}
