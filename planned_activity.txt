Date	Name	Descrption								Activity
5-07-2021	Surya	KOT - POS Save and Add Item Code refractor			Coding
5-07-2021	Surya	TAKE ORDER - Save nad Add Item Code Refractor			Coding
5-07-2021	Sharon	Online Save - KIT, Category, Customer , Supplier 		Coding
5-07-2021	Ananthu	Online Save - KIT, Category, Customer , Supplier - Testing	Testing
5-07-2021	Sibi	Copy Report URL from Client to Cloud-5 Reports			Coding
			
6-07-2021	Ananthu	KOT POS								Testing
6-07-2021	Ananthu	TAKE ORDER							Testing
6-07-2021	Ananthu	PURCASE								Testing
6-07-2021	Ananthu	STOCK TRANSFER							Testing
6-07-2021	Surya	STOCK TRANSFER ACCEPT						Testing
6-07-2021	Surya	POS								Testing
6-07-2021	Surya	AMDC Test- Import Purchase					Testing
6-07-2021	Surya	AMDC Test- , Item Batch Price					Testing
6-07-2021	Surya	AMDC Test- , Pharmacy Retail					Testing
6-07-2021	Surya	AMDC Test- , Wholesale						Testing
6-07-2021	Surya	AMDC Test- , Corporate Sale					Testing
6-07-2021	Sharon	AMDC Test- , Local Purchase					Testing
6-07-2021	Sharon	AMDC Test- , Reports - Sales Daily Sales			Testing
6-07-2021	Sharon	AMDC Test- , Reports - Purchase Reports				Testing
6-07-2021	Sharon	AMDC Test- , Reports - Batch Wise Stock				Testing
6-07-2021	Ananthu	AMDC Test- , Reports - Batch Wise Sales				Testing
6-07-2021	Ananthu	AMDC Test- , Reports - Batch Wise Purchase			Testing
6-07-2021	Ananthu	AMDC Test- , Reports - Batch Wise Profit			Testing
6-07-2021	Ananthu	AMDC Test- , Reports - Category  Wise Stock			Testing
6-07-2021	Sibi	AMDC Test- , Reports - Cagtegoery  Wise Sales			Testing
6-07-2021	Sibi	AMDC Test- , Reports - Cagtegoery  Wise Purchase		Testing
6-07-2021	Sibi	AMDC Test- , Reports - Cagtegoery Wise Stock			Testing
6-07-2021	Sibi	AMDC Test- , Reports - Cagtegoery Wise Profit			Testing
6-07-2021	Sibi	Copy Report URL from client to Cloud				Coding
6-07-2021	Surya	Release of client and server to production for Lekshmi Bakery	Release
6-07-2021	Surya	Trail release for AMDC						Release
			
7-07-2021	Surya	Godown Management for Stock Transfer				Coding
7-07-2021	Surya	Godown Management for   Billing					Coding
7-07-2021	Surya	Godown Management for   Purchase				Coding
7-07-2021	Sharon	Godown Management for   Damage					Coding
7-07-2021	Surya	Godown Management for   Production				Coding
7-07-2021	Anathu	Godown Management for   Write Off/Consumption			Coding
7-07-2021	Anathu	Godown Management for  Reporting				Coding
7-07-2021	Sibi	Copy Report URL from client to Cloud				Coding
			
8-07-2021	Sharon	Godown Management for Stock Transfer				Testing
8-07-2021	Sharon	Godown Management for   Billing					Testing
8-07-2021	Sharon	Godown Management for   Purchase				Testing
8-07-2021	Surya	Godown Management for   Damage					Testing
8-07-2021	Sharon	Godown Management for   Production				Testing
8-07-2021	Surya	Godown Management for   Write Off/Consumption			Testing
8-07-2021	Sibi	Godown Management for  Reporting				Testing
8-07-2021	Sibi	Copy Report URL from client to Cloud				Testing
			
9-07-2021	Sharon	Other Branch Purchase						Testing
9-07-2021	Surya	GRN -At branch							Coding
9-07-2021	Sharon	GRN Receive Message						Coding
9-07-2021	Sibi	GRM Publish Message						Coding
			
9-07-2021	Ananthu	KOT POS								Testing
9-07-2021	Sharon	TAKE ORDER							Testing
9-07-2021	Sibi	PURCASE	Testing
9-07-2021	Ananthu	STOCK TRANSFER							Testing
9-07-2021	Surya	STOCK TRANSFER ACCEPT						Testing
9-07-2021	Surya	POS								Testing

